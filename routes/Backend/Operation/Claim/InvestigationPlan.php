<?php
/**
/**
 * Investigation Plans 
 */
Route::group([
	'namespace' => 'Operation\Claim',
	'as' => 'claim.',
	'prefix' => 'claim'
], function() {
	Route::group(['prefix' => 'investigations',  'as' => 'investigations.'], function() {
		Route::get('/', 'InvestigationPlanController@index')->name('index');
		Route::get('create', 'InvestigationPlanController@create')->name('create');
		Route::post('store', 'InvestigationPlanController@store')->name('store');
		Route::get('profile/{plan_id}', 'InvestigationPlanController@profile')->name('profile');
		Route::get('edit/{plan_id}', 'InvestigationPlanController@edit')->name('edit');
		Route::put('update/{plan_id}','InvestigationPlanController@update')->name('update');
		Route::get('submit_for_review/{plan_id}', 'InvestigationPlanController@submitForReview')->name('submit_for_review');

		Route::get('defaults', 'InvestigationPlanController@defaults')->name('defaults');
		Route::post('defaults', 'InvestigationPlanController@saveDefaults')->name('defaults');

		Route::post('assign_investigators', 'InvestigationPlanController@assignPlanInvestigators')->name('assign_investigators');
		Route::get('investigators_datatable/{plan_id}', 'InvestigationPlanController@investigatorsProfileDatatable')->name('investigators_datatable');
		
		Route::get('assign_files/{plan_id}', 'InvestigationPlanController@assignFilesView')->name('assign_files');
		Route::put('assign_files/{plan_id}', 'InvestigationPlanController@assignFilesSave')->name('assign_files');
		Route::get('remove_assigned_file/{plan_file_id}', 'InvestigationPlanController@removeAssignedFiles')->name('remove_assigned_file');

		Route::get('get_files_allocation/{plan_id}', 'InvestigationPlanController@getFilesAllocation')->name('get_files_allocation');
		Route::get('get_allocated_files/{plan_id}', 'InvestigationPlanController@getAllocatedFiles')->name('get_allocated_files');
		Route::put('get_user_allocated_files/{user_id}', 'InvestigationPlanController@returnUserAllocatedFilesDatatable')->name('get_user_allocated_files');
		Route::put('get_user_pending_allocated_files/{user_id}', 'InvestigationPlanController@returnUserPendingAllocatedFilesDatatable')->name('get_user_pending_allocated_files');


	});
});