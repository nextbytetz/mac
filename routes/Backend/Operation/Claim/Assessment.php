<?php

/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

    Route::group(['namespace' => 'Claim', 'prefix' => 'assessment', 'as' => 'assessment.'], function() {
        //menu
        Route::get('/', 'AssessmentController@menu')->name('menu');
        Route::get('/pd_calculator', 'AssessmentController@pdCalculator')->name('pd_calculator');
        Route::get('/performance_report', 'AssessmentController@performanceReport')->name('performance_report');

        Route::group(['prefix' => 'impairment', 'as' => 'impairment.'], function() {
            Route::get('/', 'AssessmentController@allImpairment')->name('all');
            Route::get('/choose', 'AssessmentController@chooseImpairment')->name('choose');
            Route::get('/{incident}/dashboard', 'AssessmentController@impairmentDashboard')->name('dashboard');
            Route::get('{incident}/create', 'AssessmentController@createImpairment')->name('create');
            Route::post('{incident}/create', 'AssessmentController@createImpairmentPost')->name('create.post');
            Route::get('/{impairment}/edit', 'AssessmentController@editImpairment')->name('edit');
            Route::delete('/{impairment}/delete', 'AssessmentController@deleteImpairment')->name('delete');
            Route::post('/{impairment}/update', 'AssessmentController@updateImpairment')->name('update');
        });

        Route::group(['prefix' => 'hcp_hsp', 'as' => 'hcp_hsp.'], function() {
            Route::get('menu', 'HspHcpBillingController@menu')->name('menu');
            Route::get('index', 'HspHcpBillingController@index')->name('index');
            Route::get('get', 'HspHcpBillingController@getForDatatable')->name('get');
            Route::get('profile/{billing_id}', 'HspHcpBillingController@profile')->name('profile');

            Route::get('get_auth_billing/{auth_no}', 'HspHcpBillingController@getForAuthBillingDataTable')->name('get_auth_billing');
            Route::get('billings', 'HspHcpBillingController@billings')->name('billings');
            Route::get('get_billing', 'HspHcpBillingController@getForBillingDataTable')->name('get_billing');
            Route::get('billing_profile/{billing_id}', 'HspHcpBillingController@billingProfile')->name('billing_profile');
            Route::get('initiate_workflow/{hsp_billing_id}', 'HspHcpBillingController@initiateVettingWorkflow')->name('initiate_workflow');
            Route::get('get_vetting_billing/{billing_id}', 'HspHcpBillingController@getForVettingBillingDataTable')->name('get_vetting_billing');
            Route::get('vetting_profile/{billing_detail_id}', 'HspHcpBillingController@vettingProfile')->name('vetting_profile');


            /* post vettings */
            Route::post('post_bill_services/{service_id}/{detail_id}', 'HspHcpBillingController@postBillServices')->name('post_bill_services');
            Route::get('calculate_total_service/{detail_id}', 'HspHcpBillingController@calculateTotalService')->name('calculate_total_service');
            Route::post('post_inclusion_services/{service_id}/{detail_id}', 'HspHcpBillingController@calculateTotalDetail')->name('post_inclusion_services');
            Route::get('get_total_bill_items/{detail_id}', 'HspHcpBillingController@getTotalBillItems')->name('get_total_bill_items');
            Route::get('post_total_bill_items/{detail_id}', 'HspHcpBillingController@postTotalBillItems')->name('post_total_bill_items');
            Route::post('post_remarks/{service_id}/{detail_id}', 'HspHcpBillingController@postRemarks')->name('post_remarks');

            Route::get('initiate_workflow/{hsp_billing_id}', 'HspHcpBillingController@initiateVettingWorkflow')->name('initiate_workflow');
            Route::get('facilities', 'HspHcpBillingController@hspFacilities')->name('facilities');
            Route::get('get_facilities', 'HspHcpBillingController@getFacilities')->name('get_facilities');
            Route::get('facility/{facility_id}', 'HspHcpBillingController@viewFacility')->name('facility');
            Route::get('get_facility_items/{facility_id}', 'HspHcpBillingController@getFacilityItems')->name('get_facility_items');
            Route::get('update_payment_advice/{hsp_billing_id}/{level}', 'HspHcpBillingController@updatePaymentAdvice')->name('update_payment_advice');
            Route::post('payment_advice/{hsp_billing_id}', 'HspHcpBillingController@previewPaymentAdvice')->name('payment_advice');


        });

    });

});