<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    CLAIM - GROUP ROUTES
    /*
    * Claim modules
    */
    Route::group(['namespace' => 'Claim', 'prefix' => 'claim', 'as' => 'claim.'], function() {
        /*claim menu*/
        Route::get('/', function () {
            return view('backend.operation.claim.menu');
        })->name('menu');
        /**
         * -----NOTIFICATION REPORT===================
         */
        //get a list of notification
        Route::get('search_registered_notification', 'NotificationReportController@searchRegisteredNotification')->name('notification_report.search_registered_notification');
        //administration
        Route::get('administration', 'NotificationReportController@administration')->name('notification_report.administration');
        //choose employee
        Route::get('notification_report/choose_employee', 'NotificationReportController@chooseEmployee')->name('notification_report.choose_employee');
        //post choose employee
        Route::post('notification_report/choose_employee', 'NotificationReportController@postChooseEmployee')->name('notification_report.choose_employee.post');
        //create
        Route::get('notification_report/{employee}', 'NotificationReportController@register')->name('notification_report.register')->where('employee', '[0-9]+');
        //store
        Route::post('notification_report/{employee}/store', 'NotificationReportController@storeNew')->name('notification_report.store_new');
        //modify
        Route::get('notification_report/{incident}/modify', 'NotificationReportController@modify')->name('notification_report.modify')->where('incident', '[0-9]+');
        //modify progressive
        Route::get('notification_report/{incident}/modify_progressive', 'NotificationReportController@modifyProgressive')->name('notification_report.modify_progressive')->where('incident', '[0-9]+');
        //Send file to DMS
        Route::get('notification_report/{notification_report}/send_to_dms', 'NotificationReportController@sendToDms')->name('notification_report.send_to_dms');
        //update
        Route::put('notification_report/update/{incident}', 'NotificationReportController@update')->name('notification_report.update');
        //update progressive
        Route::put('notification_report/update_progressive/{incident}', 'NotificationReportController@updateProgressive')->name('notification_report.update_progressive');
        //edit -->Doctors Modification
        Route::get('notification_report/{incident}/doctor_modify', 'NotificationReportController@editNatureOfIncident')->name('notification_report.doctors_modification')->where('incident', '[0-9]+');
        //update -->doctors update
        Route::put('notification_report/doctor_update/{notification_report}', 'NotificationReportController@updateNatureOfIncident')->name('notification_report.doctors_update');

        /*Get for data table : Recall notifications */
        Route::get('recall', 'NotificationReportController@recall')->name('notification_report.recall');

        Route::post('get', 'NotificationReportController@getForDataTable')->name('notification_report.get');

        //notification report created
        Route::get('notification_report/created/{incident}', 'NotificationReportController@created')->name('notification_report.created')->where('incident', '[0-9]+');
        //profile
        Route::get('notification_report/profile/{incident}', 'NotificationReportController@profile')->name('notification_report.profile')->where('incident', '[0-9]+');
        /*post default by reference*/
        Route::get('allocate_payroll_defaults', 'NotificationReportController@allocationForPayrollDefaults')->name('allocate_payroll_defaults');
        Route::post('post_defaults_by_reference/{reference}', 'NotificationReportController@postDefaultsByReference')->name('post_defaults_by_reference');
        //notification defaults
        Route::get('notification_report/defaults', 'NotificationReportController@defaults')->name('notification_report.defaults');
        //notification checklist allocation
        Route::get('notification_report/checklist_allocation', 'NotificationReportController@checklistAllocation')->name('notification_report.checklist_allocation');
        //update notification defaults
        Route::post('notification_report/post_checklist_allocation/{mode}', 'NotificationReportController@postChecklistAllocation')->name('notification_report.post_checklist_allocation');
        //get view checklist allocation region
        Route::post('notification_report/checklist_allocation_region', 'NotificationReportController@checklistRegionAllocationShow')->name('notification_report.checklist_allocation_region');
        //resource allocation
        Route::get('notification_report/allocation', 'NotificationReportController@allocation')->name('notification_report.allocation');
        //filter allocation
        Route::put('notification_report/get_allocation', 'NotificationReportController@getAllocation')->name('notification_report.allocation.get');
        //resource allocation employer group
        Route::post('notification_report/allocation_employer_group', 'NotificationReportController@getAllocationEmployerGroup')->name('notification_report.allocation_employer_group');
        //resource allocation staff group
        Route::post('notification_report/allocation_staff_group', 'NotificationReportController@getAllocationStaffGroup')->name('notification_report.allocation_staff_group');
        //assign allocation
        Route::put('notification_report/assign_allocation', 'NotificationReportController@assignAllocation')->name('notification_report.allocation.assign');
        //get the stage details (for progressive notifications)
        Route::post('notification_report/stage_comment/{incident}', 'NotificationReportController@getStageComment')->name('notification_report.stage_comment')->where('incident', '[0-9]+');
        //get attend notification
        Route::get('notification_report/attend/{incident}', 'NotificationReportController@attend')->name('notification_report.attend')->where('incident', '[0-9]+');
        //post attend notification
        Route::post('notification_report/attend/{incident}', 'NotificationReportController@postAttend')->name('notification_report.attend.post')->where('incident', '[0-9]+');
        //get attend for profile
        Route::get('notification_report/attend_profile/{incident}', 'NotificationReportController@attendProfile')->name('notification_report.attend_profile')->where('incident', '[0-9]+');
        //acknowledgement letter
        Route::get('notification_report/ackwlgletter/{incident}', 'NotificationReportController@ackwlgletter')->name('notification_report.ackwlgletter')->where('incident', '[0-9]+');
        //save acknowledgement letter
        Route::put('notification_report/ackwlgletter/{notification_report}/save', 'NotificationReportController@saveAckwlgletter')->name('notification_report.ackwlgletter.save');
        //issue acknowledgement letter
        Route::post('notification_report/ackwlgletter/{notification_report}/issue', 'NotificationReportController@issueAckwlgletter')->name('notification_report.ackwlgletter.issue');
        //get update notification checklist
        Route::get('notification_report/update_checklist/{incident}', 'NotificationReportController@updateChecklist')->name('notification_report.update_checklist')->where('incident', '[0-9]+');
        //post update notification checklist
        Route::put('notification_report/update_checklist/{incident}', 'NotificationReportController@postUpdateChecklist')->name('notification_report.update_checklist.put')->where('incident', '[0-9]+');
        //get update notification details
        Route::get('notification_report/update_details/{incident}', 'NotificationReportController@updateDetails')->name('notification_report.update_details')->where('incident', '[0-9]+');
        //post update notification details
        Route::put('notification_report/update_details/{incident}', 'NotificationReportController@postUpdateDetails')->name('notification_report.update_details.put')->where('incident', '[0-9]+');
        //get update death notification details
        Route::get('notification_report/update_death_details/{incident}', 'NotificationReportController@updateDeathDetails')->name('notification_report.update_death_details')->where('incident', '[0-9]+');
        //post update death notification details
        Route::put('notification_report/update_death_details/{incident}', 'NotificationReportController@postUpdateDeathDetails')->name('notification_report.update_death_details.put')->where('incident', '[0-9]+');

        //complete notification checklist
        Route::post('notification_report/complete_checklist/{incident}', 'NotificationReportController@completeChecklist')->name('notification_report.complete_checklist')->where('incident', '[0-9]+');
        //update employee for checklist (updateEmployeeForChecklist)
        Route::get('notification_report/update_employee_checklist/{incident}', 'NotificationReportController@updateEmployeeForChecklist')->name('notification_report.update_employee_checklist')->where('incident', '[0-9]+');
        //get treatment entries for checklist
        Route::post('notification_report/checklist_treatment_entries', 'NotificationReportController@treatmentEntries')->name('notification_report.checklist_treatment_entries');
        //get mae sub expense entry
        Route::post('notification_report/mae_sub_entries', 'NotificationReportController@maeSubEntries')->name('notification_report.mae_sub_entries');
        //get eligible summary entry
        Route::post('notification_report/{eligible}/eligible_computation_summary', 'NotificationReportController@eligibleComputationSummary')->name('notification_report.eligible_computation_summary');
        //update investigation feedback
        //Route::get('notification_report/update_investigation/{incident}', 'NotificationReportController@updateInvestigation')->name('notification_report.update_investigation')->where('incident', '[0-9]+');
        //post update investigation feedback
        //Route::put('notification_report/post_investigation/{incident}', 'NotificationReportController@postInvestigation')->name('notification_report.post_investigation')->where('incident', '[0-9]+');
        //upload investigation document
        Route::post('notification_report/upload_investigation_document/{incident}', 'NotificationReportController@uploadInvestigationDocument')->name('notification_report.upload_investigation_document')->where('incident', '[0-9]+');
        //delete investigation document
        Route::post('notification_report/delete_investigation_document/{incident}/{id}', 'NotificationReportController@deleteInvestigationDocument')->name('notification_report.delete_investigation_document')->where(['incident' => '[0-9]+', 'id' => '[0-9]+']);
        //download investigation document
        Route::get('notification_report/download_investigation_document/{incident}/{id}', 'NotificationReportController@downloadInvestigationDocument')->name('notification_report.download_investigation_document')->where(['incident' => '[0-9]+', 'id' => '[0-9]+']);
        //complete investigation report
        Route::post('notification_report/complete_investigation_report/{incident}', 'NotificationReportController@completeInvestigationReport')->name('notification_report.complete_investigation_report')->where(['incident' => '[0-9]+']);
        //edit documents
        Route::get('notification_report/edit_documents/{incident}', 'NotificationReportController@editDocuments')->name('notification_report.edit_documents')->where('incident', '[0-9]+');
        Route::get('notification_report/{incident}/close', 'NotificationReportController@getCloseIncident')->name('notification_report.close_incident');
        Route::put('notification_report/{incident}/close', 'NotificationReportController@postCloseIncident')->name('notification_report.close_incident.post');
        //Do and Undo Manual Payments
        Route::get('notification_report/{incident}/do_manual_payment', 'NotificationReportController@doManualPayment')->name('notification_report.do_manual_payment');
        Route::put('notification_report/{incident}/do_manual_payment', 'NotificationReportController@postDoManualPayment')->name('notification_report.do_manual_payment.post');
        Route::post('notification_report/{incident}/undo_manual_payment', 'NotificationReportController@postUndoManualPayment')->name('notification_report.undo_manual_payment.post');
        //Online Notification
        Route::get('notification_report/online_requests', 'NotificationReportController@onlineRequests')->name('notification_report.online_requests');
        //Online Notification Account Validation
        Route::get('notification_report/online_account_validation', 'NotificationReportController@onlineAccountValidation')->name('notification_report.online_account_validation');
        //Online Notification Account Validated
        Route::get('notification_report/online_account_validated', 'NotificationReportController@onlineAccountValidated')->name('notification_report.online_account_validated');
        //Online Notification Account Validation Profile
        Route::get('notification_report/online_account_validation_profile/{claim}', 'NotificationReportController@onlineAccountValidationProfile')->name('notification_report.online_account_validation_profile');
        Route::get('notification_report/online_account_validation/{claim}/documents_library', 'NotificationReportController@showClaimAccountDocumentLibrary')->name('notification_report.online_account_document_library');
        //validate action
        Route::post('notification_report/online_account_validate_action/{claim}/{action}', 'NotificationReportController@onlineAccountValidateAction')->name('notification_report.online_account_validate_action');
        //undo_approval
        Route::post('notification_report/{incident}/undo_approval', 'NotificationReportController@undoApproval')->name('notification_report.undo_approval');
        //undo_benefit
        Route::post('notification_report/{incident}/undo_benefit', 'NotificationReportController@undoBenefit')->name('notification_report.undo_benefit');
        //information dashboard
        Route::get('notification_report/information_dashboard', 'NotificationReportController@informationDashboard')->name('notification_report.information_dashboard');
        //performance report
        Route::get('notification_report/performance_report', 'NotificationReportController@performanceReport')->name('notification_report.performance_report');
        //view performance report
        Route::get('notification_report/view_performance_report/{configurable}', 'NotificationReportController@viewPerformanceReport')->name('notification_report.view_performance_report');
        //staff performance report
        Route::get('notification_report/staff_performance_report', 'NotificationReportController@staffPerformanceReport')->name('notification_report.staff_performance_report');

        //post documents
        //Route::post('notification_report/update_documents/{notification_report}', 'NotificationReportController@updateDocuments')->name('notification_report.update_documents');
        //Auto Sync Documents with E-Office
        Route::get('notification_report/reload_documents/{notification_report}', 'NotificationReportController@reloadDocuments')->name('notification_report.reload_documents');
        //upload documents
        Route::post('notification_report/upload_documents/{notification_report}', 'NotificationReportController@uploadDocuments')->name('notification_report.upload_documents');
        //show Document Library
        Route::get('notification_report/documents_library/{notification_report}', 'NotificationReportController@showDocumentLibrary')->name('notification_report.documents_library')->where('notification_report', '[0-9]+');
        //show Document Library for Progressive Notifications (Not currently used ...)
        Route::get('notification_report/progressive_documents_library/{incident}', 'NotificationReportController@showProgressiveDocumentLibrary')->name('notification_report.progressive_documents_library')->where('incident', '[0-9]+');
        //get base64 document
        Route::post('notification_report/current_base64_document/{incident}/{id}', 'NotificationReportController@currentBase64Document')->name('notification_report.current_base64_document')->where('incident', '[0-9]+');
        //get other base64 document
        Route::post('notification_report/other_base64_document/{incident}/{id}/{sign}', 'NotificationReportController@otherBase64Document')->name('notification_report.other_base64_document')->where('incident', '[0-9]+');
        //get base64 document
        Route::post('notification_report/current_document_from_path/{incident}/{id}', 'NotificationReportController@currentDocumentFromPath')->name('notification_report.current_document_from_path')->where('incident', '[0-9]+');
        //get base64 document
        Route::post('notification_report/current_online_document_from_path/{incident}/{id}', 'NotificationReportController@currentOnlineDocumentFromPath')->name('notification_report.current_online_document_from_path')->where('incident', '[0-9]+');
        //get base64 document
        Route::post('notification_report/current_online_account_document_from_path/{claim}/{id}', 'NotificationReportController@currentOnlineAccountDocumentFromPath')->name('notification_report.current_online_account_document_from_path')->where('incident', '[0-9]+');
        //verify document
        Route::post('notification_report/verify_online_document/{incident}/{id}/{send}/{status}', 'NotificationReportController@verifyOnlineDocument')->name('notification_report.verify_online_document')->where('incident', '[0-9]+');
        //verify online document
        Route::post('notification_report/verify_branch_online_document/{incident}/{id}/{send}/{status}', 'NotificationReportController@verifyBranchOnlineDocument')->name('notification_report.verify_branch_online_document')->where('incident', '[0-9]+');
        //delete document
        Route::post('notification_report/destroy_document/{notification_report}', 'NotificationReportController@destroyDocument')->name('notification_report.destroy_document');
        //download document
        Route::get('notification_report/download_document/{notificationReport}/{documentId}', 'NotificationReportController@downloadNotificationReportDocument')->name('notification_report.download_document');
        Route::get('notification_report/download_document_dms/{notificationReport}/{documentId}', 'NotificationReportController@downloadNotificationReportDocumentDms')->name('notification_report.download_document_dms');
        Route::get('notification_report/download_document_general/{notificationReport}/{documentId}', 'NotificationReportController@downloadNotificationReportDocumentGeneral')->name('notification_report.download_document_general');
        //open document
        Route::get('notification_report/open_document/{notificationReport}/{documentId}', 'NotificationReportController@openNotificationReportDocument')->name('notification_report.open_document');
        Route::get('notification_report/open_document_dms/{notificationReport}/{documentId}', 'NotificationReportController@openNotificationReportDocumentDms')->name('notification_report.open_document_dms');
        Route::get('notification_report/open_document_general/{notificationReport}/{documentId}', 'NotificationReportController@openNotificationReportDocumentGeneral')->name('notification_report.open_document_general');
        //initiate notification report workflow (special case for progressive)
        Route::post('notification_report/initiate_workflow', 'NotificationReportController@initiateWorkflow')->name('notification_report.initiate_workflow');
        //initiate benefit payment assessment
        Route::get('notification_report/initiate_benefit_payment/{incident}/{benefit}', 'NotificationReportController@initiateBenefitPayment')->name('notification_report.initiate_benefit_payment');
        //edit benefit payment assessment
        Route::get('notification_report/edit_benefit_payment/{incident}/{benefit}/{eligible}', 'NotificationReportController@editBenefitPayment')->name('notification_report.edit_benefit_payment');
        //cancel benefit payment assessment
        Route::post('notification_report/cancel_benefit_payment/{incident}/{benefit}/{eligible}', 'NotificationReportController@cancelBenefitPayment')->name('notification_report.cancel_benefit_payment');
        //assess mae refund payment
        Route::get('notification_report/assess_mae_refund_member/{incident}/{benefit}/{eligible}', 'NotificationReportController@assessMaeRefundMember')->name('notification_report.assess_mae_refund_member');
        //assess pd payment
        Route::get('notification_report/assess_pd_payment/{incident}/{benefit}/{eligible}', 'NotificationReportController@assessPdPayment')->name('notification_report.assess_pd_payment');
        //assess td payment
        Route::get('notification_report/assess_td_payment/{incident}/{benefit}/{eligible}', 'NotificationReportController@assessTdPayment')->name('notification_report.assess_td_payment');
        //assess td refund payment
        Route::get('notification_report/assess_td_refund_payment/{incident}/{benefit}/{eligible}', 'NotificationReportController@assessTdRefundPayment')->name('notification_report.assess_td_refund_payment');
        //delete benefit payment assessment
        Route::delete('notification_report/delete_benefit_payment/{incident}/{eligible}', 'NotificationReportController@deleteBenefitPayment')->name('notification_report.delete_benefit_payment');
        //initialize benefit payment workflow
        Route::post('notification_report/initiate_benefit_workflow', 'NotificationReportController@initiateBenefitWorkflow')->name('notification_report.initiate_benefit_workflow');
        //store mae_refund_member benefit payment assessment
        Route::put('notification_report/store_mae_refund_member/{incident}/{benefit}', 'NotificationReportController@storeMaeRefundMember')->name('notification_report.store_mae_refund_member');
        //store mae_refund_member benefit payment assessment for multiple entries
        Route::put('notification_report/store_mae_refund_member_multiple/{incident}/{benefit}', 'NotificationReportController@storeMaeRefundMemberMultiple')->name('notification_report.store_mae_refund_member_multiple');
        //update mae_refund_member benefit payment assessment
        Route::put('notification_report/update_mae_refund_member/{incident}/{eligible}', 'NotificationReportController@updateMaeRefundMember')->name('notification_report.update_mae_refund_member');
        //update mae_refund_member_multiple benefit payment assessment
        Route::put('notification_report/update_mae_refund_member_multiple/{incident}/{eligible}', 'NotificationReportController@updateMaeRefundMemberMultiple')->name('notification_report.update_mae_refund_member_multiple');
        //store td benefit assessment
        Route::put('notification_report/store_td/{incident}/{benefit}', 'NotificationReportController@storeTd')->name('notification_report.store_td');
        //update td benefit assessment
        Route::put('notification_report/update_td/{incident}/{eligible}', 'NotificationReportController@updateTd')->name('notification_report.update_td');
        //get td assessment
        Route::get('notification_report/td_assessment/{incident}/{benefit}/{eligible}', 'NotificationReportController@tdAssessment')->name('notification_report.td_assessment');
        //update td assessment
        Route::put('notification_report/update_td_assessment/{incident}/{eligible}', 'NotificationReportController@updateTdAssessment')->name('notification_report.update_td_assessment');
        Route::post('notification_report/add_td_assessment/{eligible}', 'NotificationReportController@addTdAssessment')->name('notification_report.add_td_assessment');
        Route::post('notification_report/list_td_assessment/{eligible}', 'NotificationReportController@listTdAssessment')->name('notification_report.list_td_assessment');
        Route::post('notification_report/delete_td_assessment/{eligible}/{assessment}', 'NotificationReportController@deleteTdAssessment')->name('notification_report.delete_td_assessment');
        //get pd assessment
        Route::get('notification_report/pd_assessment/{incident}/{benefit}/{eligible}', 'NotificationReportController@pdAssessment')->name('notification_report.pd_assessment');
        Route::post('notification_report/pd_assessment/occupations', 'NotificationReportController@getPdOccupations')->name('notification_report.pd_assessment.occupations');
        Route::post('notification_report/pd_assessment/amputation', 'NotificationReportController@getPdAmputation')->name('notification_report.pd_assessment.amputation');
        Route::post('notification_report/pd_assessment/schedule_one_calculator', 'NotificationReportController@scheduleOneCalculator')->name('notification_report.pd_assessment.schedule_one_calculator');
        //update pd assessment
        Route::put('notification_report/update_pd_assessment/{incident}/{eligible}', 'NotificationReportController@updatePdAssessment')->name('notification_report.update_pd_assessment');
        //process benefit for payment
        Route::post('notification_report/process_benefit_payment/{incident}/{eligible}', 'NotificationReportController@processBenefitPayment')->name('notification_report.process_benefit_payment');
        //undo approved assessment
        Route::post('notification_report/undo_approved_assessment/{incident}/{eligible}', 'NotificationReportController@undoApprovedAssessment')->name('notification_report.undo_approved_assessment');
        //process mae for payment
        Route::post('notification_report/process_mae_payment/{incident}/{eligible}', 'NotificationReportController@processMaePayment')->name('notification_report.process_mae_payment');
        //add_contribution_for_notification
        Route::get('notification_report/add_contribution/{incident}', 'NotificationReportController@addContribution')->name('notification_report.add_contribution')->where('incident', '[0-9]+');
        //add document used for td
        Route::post('notification_report/add_td_document_used/{eligible}', 'NotificationReportController@addTdDocumentUsed')->name('notification_report.add_td_document_used')->where('eligible', '[0-9]+');
        //delete document used for td
        Route::post('notification_report/delete_td_document_used/{eligible}/{assessment_document}', 'NotificationReportController@deleteTdDocumentUsed')->name('notification_report.delete_td_document_used')->where('assessment_document', '[0-9]+');
        //list document used for td
        Route::post('notification_report/list_td_document_used/{eligible}', 'NotificationReportController@listTdDocumentUsed')->name('notification_report.list_td_document_used')->where('eligible', '[0-9]+');
        //add td eligible assessment
        Route::post('notification_report/add_td_eligible_assessment/{eligible}', 'NotificationReportController@addTdEligibleAssessment')->name('notification_report.add_td_eligible_assessment')->where('eligible', '[0-9]+');
        //post_contribution_for_notification
        Route::put('notification_report/post_contribution/{incident}', 'NotificationReportController@postContribution')->name('notification_report.post_contribution')->where('incident', '[0-9]+');
        //edit_contribution_for_notification
        Route::get('notification_report/edit_contribution/{incident}', 'NotificationReportController@editContribution')->name('notification_report.edit_contribution')->where('incident', '[0-9]+');
        //register employee for notification
        Route::get('notification_report/register_employee/{incident}', 'NotificationReportController@registerEmployee')->name('notification_report.register_employee')->where('incident', '[0-9]+');
        //post register employee for notification
        Route::put('notification_report/post_register_employee/{incident}', 'NotificationReportController@postRegisterEmployee')->name('notification_report.post_register_employee')->where('incident', '[0-9]+');
        //reload benefit payment status (checking for document completion)
        Route::get('notification_report/recheck_benefit_status/{incident}', 'NotificationReportController@recheckBenefitStatus')->name('notification_report.recheck_benefit_status')->where('incident', '[0-9]+');
        //download report excels
        Route::get('notification_report/download_configurable_report/{report_id}/{fin_year_id}', 'NotificationReportController@downloadConfigurableReport')->name('notification_report.download_configurable_report');
        //medical-expense-overview
        Route::get('notification_report/profile/medical_expenses_overview/{notification_report}', 'NotificationReportController@getMedicalExpensesForDatatable')->name('notification_report.medical_expenses_overview')->where('notification_report', '[0-9]+');
        /*Medical expense entities*/
        Route::get('notification_report/profile/assessment_opener/{notification_report}', 'NotificationReportController@getAssessmentOpener')->name('notification_report.assessment_opener')->where('notification_report', '[0-9]+');
        //Create Medical Expense
        Route::get('notification_report/medical_expense/{incident}', 'NotificationReportController@createMedicalExpense')->name('notification_report.create_medical_expense')->where('incident', '[0-9]+');
        Route::post('notification_report/medical_expense/{notification_report}/store', 'NotificationReportController@storeMedicalExpense')->name('notification_report.store_medical_expense');
        //Edit Medical Expense
        Route::get('notification_report/medical_expense/{medical_expense}/edit', 'NotificationReportController@editMedicalExpense')->name('notification_report.edit_medical_expense')->where('medical_expense', '[0-9]+');
        Route::put('notification_report/medical_expense/update/{notification_report}', 'NotificationReportController@updateMedicalExpense')->name('notification_report.update_medical_expense');

        /* Update Monthly Earning*/
        Route::get('notification_report/monthly_earning/{notification_report}/edit', 'NotificationReportController@editMonthlyEarning')->name('notification_report.edit_monthly_earning')->where('notification_report', '[0-9]+');
        Route::put('notification_report/monthly_earning/update/{notification_report}', 'NotificationReportController@updateMonthlyEarning')->name('notification_report.update_monthly_earning');




        /**
         *
         * NOTIFICATION CONTRIBUTION TRACKS : MAIN ROUTES
         *
         */

        Route::resource('notification_contribution_track', 'NotificationContributionTrackController', ['except' => ['show']]);
        /* get pending */
        Route::get('notification_contribution_tracks/get_pending', 'NotificationContributionTrackController@getPendingForDataTable')->name('notification_contribution_track.get_pending');
        Route::get('notification_contribution_tracks/get/{notification_report}', 'NotificationContributionTrackController@getForDataTable')->name('notification_contribution_track.get')->where('notification_report', '[0-9]+');
        /*profile*/
        Route::get('notification_contribution_track/profile/{notification_report}', 'NotificationContributionTrackController@profile')->name('notification_contribution_track.profile');
        /*  End of contribution tracks */








        /**
         * CLAIM / COMPENSATION ROUTES ===========
         */
        //APPROVE TO CLAIM
        Route::get('notification_report/approve_to_claim/{notification_report}', 'NotificationReportController@approveToClaim')->name('notification_report.approve_to_claim')->where('notification_report', '[0-9]+');

        /**
         * Get for Datatable
         */
        Route::post('pending_claim_summary', 'NotificationReportController@getPendingClaimForDataTable')->name('pending_claim_summary.get');

        //REJECTION
        Route::post('notification_report/reject/{notification_report}', 'NotificationReportController@reject')->name('notification_report.reject');

        //UNDO
        Route::get('notification_report/undo/{notification_report}', 'NotificationReportController@destroy')->name('notification_report.undo')->where('notification_report', '[0-9]+');

        //REVERSE STATUS
        Route::get('notification_report/reverse_status/{notification_report}', 'NotificationReportController@reverseStatus')->name('notification_report.reverse_status')->where('notification_report', '[0-9]+');

        //APPROVE REJECTION APPEAL
        Route::get('notification_report/approve_rejection_appeal_page/{incident}', 'NotificationReportController@approveRejectionAppealPage')->name('notification_report.approve_rejection_appeal_page')->where('incident', '[0-9]+');
        Route::post('notification_report/approve_rejection_appeal/{notification_report}', 'NotificationReportController@approveRejectionAppeal')->name('notification_report.approve_rejection_appeal')->where('notification_report', '[0-9]+');

        //VERIFY
        Route::get('notification_report/verify/{notification_report}', 'NotificationReportController@verify')->name('notification_report.verify')->where('notification_report', '[0-9]+');


        Route::get('notification_report/claim_assessment_checklist/{medical_expense}', 'NotificationReportController@claimAssessmentChecklist')->name('notification_report.claim_assessment_checklist')->where('medical_expense', '[0-9]+');

        //Claim assessment
        Route::put('notification_report/claim_assessment/{medical_expense_id}', 'NotificationReportController@claimAssessment')->name('notification_report.claim_assessment');

        //------END CLAIM------------------


        /**
         * INVESTIGATION ROUTES =============================
         */

//        Activate Investigation
        Route::get('notification_report/investigate/{notification_report}/', 'NotificationReportController@investigate')->name('notification_report.investigate')->where('notification_report', '[0-9]+');

//        Cancel Investigation
        Route::get('notification_report/cancel_investigation/{notification_report}/', 'NotificationReportController@cancelInvestigation')->name('notification_report.cancel_investigation')->where('notification_report', '[0-9]+');

        /**
         * Assign Invetigators
         */
        Route::get('notification_report/assign_investigator/{incident}', 'NotificationReportController@assignInvestigators')->name('notification_report.assign_investigators')->where('incident', '[0-9]+');
        Route::post('notification_report/investigators/{notification_report}/store', 'NotificationReportController@storeInvestigators')->name('notification_report.store_investigators');

        //Change investigator
        Route::get('notification_report/investigator/{notification_investigator}/edit', 'NotificationReportController@editInvestigator')->name('notification_report.edit_investigator')->where('notification_investigator', '[0-9]+');
        Route::put('notification_report/investigator/{notification_investigator}/change', 'NotificationReportController@changeInvestigator')->name('notification_report.change_investigator');
//  Delete
        Route::delete('notification_report/investigator/{notification_investigator}/delete', 'NotificationReportController@deleteInvestigator')->name('notification_report.delete_investigator');

        //--end investigators

        /**
         * investigation feedbacks / report
         */
        Route::get('get_investigation_feedbacks/{notification_report}', 'NotificationReportController@getInvestigationFeedbacksForDatatable')->name('notification_report.get_investigation_feedbacks')->where('notification_report', '[0-9]+');

        Route::get('get_investigators/{notification_report}', 'NotificationReportController@getInvestigatorsForDatatable')->name('notification_report.get_investigators')->where('notification_report', '[0-9]+');
        //edit investigation feedback
        Route::get('notification_report/investigation_report/{incident}/edit', 'NotificationReportController@editInvestigationReport')->name('notification_report.edit_investigation_report')->where('incident', '[0-9]+');
        //update
        Route::put('notification_report/update_investigation_report/{incident}', 'NotificationReportController@updateInvestigationReport')->name('notification_report.update_investigation_report')->where('incident', '[0-9]+');

        //get disease agents
        Route::get('notification_report/return_disease_agent/{agent_id}', 'NotificationReportController@getDiseaseByAgent')->name('return_disease_agent');
        //return target organs
        Route::get('notification_report/return_disease_organ/{organ_id}', 'NotificationReportController@getDiseaseByOrgan')->name('return_disease_organ');
        //return accident cause
        Route::get('notification_report/return_accident_cause/{accident_id}', 'NotificationReportController@getAccidentById')->name('return_accident_cause');

        // --END INVESTIGATION-----------------------------
// ---END Notification report ----------------------

        /**
         * HEALTH PROVIDER SERVICE =====================
         */
        //Create
        Route::get('notification_report/create_health_provider_service/{incident}', 'NotificationReportController@createHealthProviderService')->name('notification_report.create_health_provider_service')->where('incident', '[0-9]+');
        Route::post('notification_report/health_provider_service/store/{notification_report}', 'NotificationReportController@storeHealthProviderService')->name('notification_report.store_health_provider_service');

        //Update
        Route::get('notification_report/health_provider_service/{notification_health_provider}/edit', 'NotificationReportController@editHealthProviderService')->name('notification_report.edit_health_provider_service')->where('notification_health_provider', '[0-9]+');
        Route::put('notification_report/health_provider_service/update/{notification_health_provider}', 'NotificationReportController@updateHealthProviderService')->name('notification_report.update_health_provider_service');
        /**
         * get notification health provider services
         */
        Route::get('get_notification_health_provider_services/{notification_report}', 'NotificationReportController@getHealthProviderServicesForDatatable')->name('notification_report.get_notification_health_provider_services')->where('notification_report', '[0-9]+');

        //--END HEALTH PROVIDER SERVICE----------------

        /**
         * --CURRENT EMPLOYEE STATE - NOTIFICATION REPORT =====================
         */
        //store
        Route::get('notification_report/create_current_employee_state/{incident}', 'NotificationReportController@createCurrentEmployeeState')->name('notification_report.create_current_employee_state')->where('incident', '[0-9]+');
        Route::post('notification_report/store_current_employee_state/{notification_report}', 'NotificationReportController@storeCurrentEmployeeState')->name('notification_report.store_current_employee_state');
        //update
        Route::get('notification_report/current_employee_state/{medical_expense}/edit', 'NotificationReportController@editCurrentEmployeeState')->name('notification_report.edit_current_employee_state')->where('medical_expense', '[0-9]+');
        Route::put('notification_report/update_current_employee_state/{notification_report}', 'NotificationReportController@updateCurrentEmployeeState')->name('notification_report.update_current_employee_state');

        //get disability state
        Route::get('get_disability_states/{notification_report}', 'NotificationReportController@getDisabilityStateForDatatable')->name('notification_report.get_disability_states')->where('notification_report', '[0-9]+');
        //get health state
        Route::get('get_health_state/{notification_report}', 'NotificationReportController@getHealthStateForDatatable')->name('notification_report.get_health_state')->where('notification_report', '[0-9]+');


        //----END Current State --------------------------
        /**
         *
         * MEDICAL PRACTITIONER MAIN ROUTES====================
         */
        Route::resource('medical_practitioner', 'MedicalPractitionerController', ['except' => ['show']]);
        Route::get('medical_practitioners', 'MedicalPractitionerController@getRegisteredPractitioners')->name('medical_practitioners');

//------End medical practitioner-------------


        /**
         *
         * HEALTH PROVIDER MAIN ROUTES---------------
         */
        Route::resource('health_provider', 'HealthProviderController', ['except' => ['show']]);
        Route::get('health_providers', 'HealthProviderController@getHealthProviders')->name('health_providers');
        Route::get('registered_health_providers', 'HealthProviderController@getRegisteredHealthProviders')->name('registered_health_providers');
        /*upload bulk health providers */
        Route::get('health_provider/upload_bulk_page', 'HealthProviderController@uploadBulkPage')->name('health_provider.upload_bulk_page');
        Route::post('health_provider/upload_bulk', 'HealthProviderController@uploadBulk')->name('health_provider.upload_bulk');
        // ===============================



        /**
         *
         * INSURANCES MAIN ROUTES---------------
         */
        Route::resource('insurance', 'InsuranceController', ['except' => ['show']]);
        Route::get('insurances', 'InsuranceController@getForDataTable')->name('insurance.get');
        // ===============================



        /**
         * ACCIDENT WITNESS ROUTES =================
         *
         */
        //create
        Route::get('notification_report/create_accident_witness/{incident}', 'NotificationReportController@createAccidentWitness')->name('notification_report.create_accident_witness')->where('incident', '[0-9]+');
        //store
        Route::post('notification_report/store_accident_witness/{notification_report}', 'NotificationReportController@storeAccidentWitness')->name('notification_report.store_accident_witness');
        //edit
        Route::get('notification_report/accident_witness/{witness}/edit', 'NotificationReportController@editAccidentWitness')->name('notification_report.edit_accident_witness')->where('witness', '[0-9]+');
        //update
        Route::put('notification_report/update_accident_witness/{witness}', 'NotificationReportController@updateAccidentWitness')->name('notification_report.update_accident_witness');
        //  Delete
        Route::delete('notification_report/accident_witness/{witness}/delete', 'NotificationReportController@deleteAccidentWitness')->name('notification_report.delete_accident_witness');

        //---End Accident Witness ----------------


        /**
         *
         * Update BANK DETAILS===============
         */
        Route::put('notification_report/update_bank_details/{notification_report}', 'NotificationReportController@updateBankDetails')->name('notification_report.update_bank_details');
        //END --BANK DETAILS -------------------

        /*Pending claim cca*/
        Route::post('notification_report/get_claim_pending_cca', 'NotificationReportController@getClaimsPendingCcaForDataTable')->name('notification_report.get_claim_pending_cca');
        /*end pending cca*/


//        Update Oshdata
        Route::get('{incident}/update_osh_data', 'NotificationReportController@updateOshData')->name('notification.update_osh_data');
        Route::post('{incident}/post_osh_data', 'NotificationReportController@postOshData')->name('notification.post_osh_data');

        Route::get('document_select_option/{notification_report}/{document_id}', 'NotificationReportController@returnNotificationDocumentSelectOptions')->name('notification.document_select_option');

        Route::get('return_append_inputs/{eligible}/{disability_type_id?}', 'NotificationReportController@returnAppendInputs')->name('notification.return_append_inputs');

    });

});