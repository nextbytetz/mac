<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    CLAIM - GROUP ROUTES
    /*
    * Claim modules
    */
    Route::group(['namespace' => 'Claim', 'prefix' => 'claim', 'as' => 'claim.'], function() {
        /**
         * --------Start Manual Notification -------------------
         */
        Route::group([   'prefix' => 'manual_system_file',  'as' => 'manual_system_file.'], function() {

            Route::get('index', 'ManualSystemFileController@index')->name('index');
            Route::post('get_notification_reports', 'ManualSystemFileController@getNotificationReportsForDataTable')->name('get_notification_reports');
            Route::get('create/{notification_report}', 'ManualSystemFileController@create')->name('create');
            Route::post('store/{notification_report}', 'ManualSystemFileController@store')->name('store');
            Route::get('death_incidents', 'ManualSystemFileController@deathIncidents')->name('death_incidents');
            Route::post('get_death_incidents', 'ManualSystemFileController@getDeathIncidentsForDataTable')->name('get_death_incidents');
            Route::get('survivors/{notification_report}', 'ManualSystemFileController@survivorsForDeathIncidentFile')->name('survivors');
            Route::post('get_survivors/{notification_report}', 'ManualSystemFileController@getSurvivorsForDataTable')->name('get_survivors');
            /*dependent*/
            Route::get('create_dependent/{notification_report}', 'ManualSystemFileController@createDependent')->name('create_dependent');
            Route::post('store_dependent/{notification_report}', 'ManualSystemFileController@storeDependent')->name('store_dependent');
            Route::get('edit_existing_dependent/{dependent}/{employee}', 'ManualSystemFileController@editExistingDependent')->name('edit_existing_dependent');

            /*enrolled beneficiaries*/
            Route::get('enrolled_survivors_page', 'ManualSystemFileController@enrolledSurvivorsPage')->name('enrolled_survivors_page');
            Route::post('get_enrolled_survivors', 'ManualSystemFileController@getEnrolledSurvivorsForDataTable')->name('get_enrolled_survivors');
            Route::get('enrolled_pensioners_page', 'ManualSystemFileController@enrolledPensionersPage')->name('enrolled_pensioners_page');
            Route::post('get_enrolled_pensioners', 'ManualSystemFileController@getEnrolledPensionersForDataTable')->name('get_enrolled_pensioners');


        });

            /*------end --Manual System File --------------*/


    });

});