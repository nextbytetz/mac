<?php
/**
/**
 * Investment portfolio 
 */
Route::group([
	'namespace' => 'Operation\Claim',
	'as' => 'claim.'
], function() {

	Route::group(['prefix' => 'claim_accrual',  'as' => 'claim_accrual.'], function() {

		Route::get('recall', 'ClaimAccrualController@recall')->name('recall');
		Route::post('get_recall_datatable', 'ClaimAccrualController@getForDataTable')->name('get_recall_datatable');
		Route::get('profile/{incident}', 'ClaimAccrualController@profileAccrual')->name('profile')->where('incident', '[0-9]+');
		Route::get('profile_workflow/{accrual_notification_report_id}', 'ClaimAccrualController@profileAccrualFromWorkflow')->name('profile_workflow')->where('incident', '[0-9]+');



		Route::get('initiate_workflow/{id}/{type}', 'ClaimAccrualController@initiateWorkflowAccrual')->name('initiate_workflow');
		// Route::get('test/{id}', 'ClaimAccrualController@testAccrual')->name('test');
		Route::post('get', 'ClaimAccrualController@getForDataTable')->name('accrual_notification_report.get');


        //medical-expense-overview
		Route::get('notification_report/profile/medical_expenses_overview/{notification_report}', 'NotificationReportController@getMedicalExpensesForDatatable')->name('notification_report.medical_expenses_overview')->where('notification_report', '[0-9]+');
        //Create Medical Expense
		Route::get('notification_report/medical_expense/{incident}', 'ClaimAccrualController@createMedicalExpense')->name('notification_report.create_medical_expense')->where('incident', '[0-9]+');
		Route::post('notification_report/medical_expense/{notification_report}/store', 'ClaimAccrualController@storeMedicalExpense')->name('notification_report.store_medical_expense');
        //Edit Medical Expense
		Route::get('notification_report/medical_expense/{medical_expense}/edit', 'ClaimAccrualController@editMedicalExpense')->name('notification_report.edit_medical_expense')->where('medical_expense', '[0-9]+');
		Route::put('notification_report/medical_expense/update/{notification_report}', 'ClaimAccrualController@updateMedicalExpense')->name('notification_report.update_medical_expense');


        //Create
		Route::get('notification_report/create_health_provider_service/{incident}', 'ClaimAccrualController@createHealthProviderService')->name('notification_report.create_health_provider_service')->where('incident', '[0-9]+');
		Route::post('notification_report/health_provider_service/store/{notification_report}', 'ClaimAccrualController@storeHealthProviderService')->name('notification_report.store_health_provider_service');

        //Update
		Route::get('notification_report/health_provider_service/{notification_health_provider}/edit', 'ClaimAccrualController@editHealthProviderService')->name('notification_report.edit_health_provider_service')->where('notification_health_provider', '[0-9]+');
		Route::put('notification_report/health_provider_service/update/{notification_health_provider}', 'ClaimAccrualController@updateHealthProviderService')->name('notification_report.update_health_provider_service');


		/*Medical expense entities*/
		Route::get('notification_report/profile/assessment_opener/{notification_report}', 'ClaimAccrualController@getAssessmentOpener')->name('notification_report.assessment_opener')->where('notification_report', '[0-9]+');

		Route::get('notification_report/claim_assessment_checklist/{medical_expense}', 'ClaimAccrualController@claimAssessmentChecklist')->name('notification_report.claim_assessment_checklist')->where('medical_expense', '[0-9]+');


        // ld eds
		Route::get('notification_report/create_current_employee_state/{incident}', 'ClaimAccrualController@createCurrentEmployeeState')->name('notification_report.create_current_employee_state')->where('incident', '[0-9]+');
		Route::post('notification_report/store_current_employee_state/{notification_report}', 'ClaimAccrualController@storeCurrentEmployeeState')->name('notification_report.store_current_employee_state');
        //update
		Route::get('notification_report/current_employee_state/{medical_expense}/edit', 'ClaimAccrualController@editCurrentEmployeeState')->name('notification_report.edit_current_employee_state')->where('medical_expense', '[0-9]+');
		Route::put('notification_report/update_current_employee_state/{notification_report}', 'ClaimAccrualController@updateCurrentEmployeeState')->name('notification_report.update_current_employee_state');

        //Claim assessment
		Route::put('notification_report/claim_assessment/{medical_expense_id}', 'ClaimAccrualController@claimAssessment')->name('notification_report.claim_assessment');

		      //medical-expense-overview
        Route::get('notification_report/profile/medical_expenses_overview/{notification_report}', 'ClaimAccrualController@getMedicalExpensesForDatatable')->name('notification_report.medical_expenses_overview')->where('notification_report', '[0-9]+');

               /**
         * get notification health provider services
         */
        Route::get('get_notification_health_provider_services/{notification_report}', 'ClaimAccrualController@getHealthProviderServicesForDatatable')->name('notification_report.get_notification_health_provider_services')->where('notification_report', '[0-9]+');

        //get disability state
        Route::get('get_disability_states/{notification_report}', 'ClaimAccrualController@getDisabilityStateForDatatable')->name('notification_report.get_disability_states')->where('notification_report', '[0-9]+');

        //get health state
        Route::get('get_health_state/{notification_report}', 'ClaimAccrualController@getHealthStateForDatatable')->name('notification_report.get_health_state')->where('notification_report', '[0-9]+');

		Route::get('checker_datatable', 'ClaimAccrualController@getCheckerDataTable')->name('checker_datatable');

		//dependents

		Route::get('employee/dependent/{employee}/{notification_report_id}/create', 'ClaimAccrualController@create_new')->name('dependent.create_new');
		Route::post('employee/dependent/{employee}/{notification_report_id}/store', 'ClaimAccrualController@store_new')->name('dependent.store_new');

		Route::get('get_dependents/{employee}/{notification_report_id}', 'ClaimAccrualController@getDependentsForDataTable')->name('employee.get_dependents'); // Dependents
		Route::get('employee/dependent/{dependent}/edit/{employee}/{notification_report_id}', 'ClaimAccrualController@editDependent')->name('dependent.edit_dependent');

		Route::put('employee/dependent/{dependent}/{notification_report_id}', 'ClaimAccrualController@updateDependent')->name('dependent.update');

		Route::get('notification_report/accrual_dates', 'ClaimAccrualController@accrualDates')->name('notification_report.accrual_dates');
		Route::post('notification_report/update_accrual_dates', 'ClaimAccrualController@updateAccrualDates')->name('notification_report.update_accrual_dates');

		Route::get('notification_report/remove_accrual/{notification_report_id}', 'ClaimAccrualController@removeAccrual')->name('notification_report.remove_accrual');

	});
});