<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    CLAIM - GROUP ROUTES
    /*
    * Claim modules
    */
    Route::group(['namespace' => 'Claim', 'prefix' => 'claim', 'as' => 'claim.'], function() {
        /**
         * --------Start Manual Notification -------------------
         */
        Route::group([   'prefix' => 'manual_notification',  'as' => 'manual_notification.'], function() {

            Route::get('index', 'ManualNotificationReportController@index')->name('index');
            Route::get('sync_member_page/{notification_report}/{option?}', 'ManualNotificationReportController@syncMemberPage')->name('sync_member_page');
            Route::put('sync_member/{notification_report}', 'ManualNotificationReportController@syncMember')->name('sync_member');
            Route::get('complete_registration_page/{notification_report}', 'ManualNotificationReportController@openCompleteRegistrationPage')->name('complete_registration_page');
            Route::put('complete_registration/{notification_report}', 'ManualNotificationReportController@completeRegistration')->name('complete_registration');
            Route::get('upload_page', 'ManualNotificationReportController@uploadPage')->name('upload_page');
            Route::get('upload_all', 'ManualNotificationReportController@uploadAll')->name('upload_all');
            Route::post('upload_all', 'ManualNotificationReportController@postUploadAll')->name('upload_all');
            Route::get('{incident}/dashboard', 'ManualNotificationReportController@dashboard')->name('dashboard');
            Route::get('{incident}/updateinfo', 'ManualNotificationReportController@updateInfo')->name('updateinfo');
            Route::get('{incident}/update_osh_data', 'ManualNotificationReportController@updateOshData')->name('update_osh_data');
            Route::get('{incident}/do_manual_payment', 'ManualNotificationReportController@doManualPayment')->name('do_manual_payment');
            Route::post('{incident}/updateinfo', 'ManualNotificationReportController@postUpdateInfo')->name('postupdateinfo');
            Route::post('{incident}/do_manual_payment', 'ManualNotificationReportController@postManualPayment')->name('post_manual_payment');
            Route::post('{incident}/post_osh_data', 'ManualNotificationReportController@postOshData')->name('post_osh_data');
            Route::get('list_all', 'ManualNotificationReportController@listAll')->name('list_all');
            Route::post('upload_bulk', 'ManualNotificationReportController@uploadManualFile')->name('upload_bulk');
            Route::post('get_for_datatable', 'ManualNotificationReportController@getAllForDataTable')->name('get_for_datatable');
            Route::post('get_for_payroll_datatable', 'ManualNotificationReportController@getAllForPayrollForDataTable')->name('get_for_payroll_datatable');
            Route::get('pending_synchronizations', 'ManualNotificationReportController@openPendingSyncPage')->name('pending_synchronizations');
            Route::post('get_pending_sync_datatable', 'ManualNotificationReportController@getPendingSyncForDataTable')->name('get_pending_sync_datatable');
            Route::get('remove/{notification_report}', 'ManualNotificationReportController@destroy')->name('remove');
            Route::get('update_has_payroll/{manual_notification_report}/{status}', 'ManualNotificationReportController@updateHasPayroll')->name('update_has_payroll');
            Route::get('update_attendant/{manual_notification_report}/{action_type}', 'ManualNotificationReportController@updateAttendantThisFile')->name('update_attendant');
            Route::get('update_status/{manual_notification_report}/{status}', 'ManualNotificationReportController@updateStatus')->name('update_status');
            /*add employee for sync*/
            Route::get('create_employee/{notification_report}', 'ManualNotificationReportController@createEmployee')->name('create_employee');
            Route::post('store_employee/{notification_report}', 'ManualNotificationReportController@storeEmployee')->name('store_employee');

            /*E-office related*/
            Route::get('resend_eoffice/{manual_notification_report}', 'ManualNotificationReportController@sendToDms')->name('resend_eoffice');
            Route::get('documents_library/{manual_notification_report}', 'ManualNotificationReportController@showDocumentLibrary')->name('documents_library')->where('manual_notification_report', '[0-9]+');
            Route::post('preview_doc_from_dms/{document_pivot_id}', 'ManualNotificationReportController@previewDocumentFromDms')->name('preview_doc_from_dms');
        });
        /*------end --Manual Notification --------------*/


    });

});