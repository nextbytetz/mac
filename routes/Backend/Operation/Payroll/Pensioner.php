<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PENSIONERS routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {
        Route::group(['prefix' => 'pensioner',  'as' => 'pensioner.'], function() {
            /**/
            Route::get('index', 'PensionerController@index')->name('index');
            Route::get('get_for_datatable', 'PensionerController@getPensionersForDataTable')->name('get_for_datatable');
            Route::post('get_ready_for_payroll_datatable', 'PensionerController@getReadyForPayrollDataTable')->name('get_ready_for_payroll_datatable');
            Route::get('get_active_for_datatable', 'PensionerController@getActivePensionersForDataTable')->name('get_active_for_datatable');
            Route::get('profile/{pensioner}', 'PensionerController@profile')->name('profile');
            Route::get('get_for_select', 'PensionerController@getPensionersForSelect')->name('get_for_select');
            Route::post('get_constant_cares_by_pensioner_dt/{pensioner}', 'PensionerController@getConstantCaresByPensionerForDt')->name('get_constant_cares_by_pensioner_dt');


            /*merging manual pensioner*/
            Route::get('pensioners_ready_for_merge_manual', 'PensionerController@pensionerReadyForMergeManual')->name('pensioners_ready_for_merge_manual');
            Route::post('get_ready_for_merge_manual_pensioners', 'PensionerController@getManualMergeReadyForPayrollForDataTable')->name('get_ready_for_merge_manual_pensioners');
            Route::get('pensioners_already_merged_payroll', 'PensionerController@alreadyMergedManualPensionersPage')->name('pensioners_already_merged_payroll');
            Route::post('get_already_merged_manual_pensioners', 'PensionerController@getManualAlreadyMergedForPayrollForDataTable')->name('get_already_merged_manual_pensioners');
            Route::get('merge_manual_pensioner_page/{pensioner}', 'PensionerController@mergeManualPensionerPage')->name('merge_manual_pensioner_page');
            Route::put('merge_manual_pensioner/{pensioner}', 'PensionerController@mergeManualPensioner')->name('merge_manual_pensioner');
            /*Status action - 0 => not paid, 1 => paid, 2 => paid and terminate, 3 => reset*/
            Route::get('update_manualpay_status/{pensioner}/{employee}/{status}', 'PensionerController@updateManualPayStatus')->name('update_manualpay_status');
        });


    });

});