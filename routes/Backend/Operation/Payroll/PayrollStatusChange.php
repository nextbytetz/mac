<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * uNCLAIM routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {
        Route::group(['prefix' => 'status_change',  'as' => 'status_change.'], function() {
            /**/
            Route::resource('/', 'PayrollStatusChangeController', ['except' => ['show', 'index', 'create', 'edit', 'update']]);

            Route::get('/create/{member_type}/{resource}/{employee}/{status_change_type}', 'PayrollStatusChangeController@create')->name('create');
            Route::get('/edit/{status_change}', 'PayrollStatusChangeController@edit')->name('edit');
            Route::put('/update/{status_change}', 'PayrollStatusChangeController@update')->name('update');
            Route::get('/profile/{status_change}', 'PayrollStatusChangeController@profile')->name('profile');
            Route::get('/undo/{status_change}', 'PayrollStatusChangeController@destroy')->name('undo');
            Route::get('/get_activations_for_beneficiary/{member_type_id}/{resource_id}/{employee}', 'PayrollStatusChangeController@getActivationsByBeneficiaryForDataTable')->name('get_activations_for_beneficiary');
            Route::get('/get_suspensions_reinstates_for_beneficiary/{member_type_id}/{resource_id}/{employee}', 'PayrollStatusChangeController@getSuspensionsReinstatesByBeneficiaryForDataTable')->name('get_suspensions_reinstates_for_beneficiary');


            /*Child extension*/
            Route::get('/fill_child_disability_assess/{status_change}', 'PayrollStatusChangeController@fillChildDisabilityAssessment')->name('fill_child_disability_assess');
            Route::put('/store_child_disability_assess/{status_change}', 'PayrollStatusChangeController@storeChildDisabilityAssessment')->name('store_child_disability_assess');

            Route::get('/update_letter_prepared_status/{status_change}', 'PayrollStatusChangeController@updateLetterPreparedStatus')->name('update_letter_prepared_status');

        });


    });

});