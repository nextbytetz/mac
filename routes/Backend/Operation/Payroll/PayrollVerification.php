<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * ARREAR routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {
        Route::group(['prefix' => 'verification',  'as' => 'verification.'], function() {
            /**/
            Route::resource('/', 'PayrollVerificationController', ['except' => ['show', 'index', 'create']]);
            Route::get('/index/{member_type}', 'PayrollVerificationController@index')->name('index');
            Route::get('/create/{member_type}/{resource}/{employee}', 'PayrollVerificationController@create')->name('create');
            Route::get('/edit/{payroll_verification}', 'PayrollVerificationController@edit')->name('edit');
            Route::put('/update/{payroll_verification}', 'PayrollVerificationController@update')->name('update');
            Route::post('/get_verifications_by_beneficiary/{member_type_id}/{resource_id}', 'PayrollVerificationController@getVerificationsByBeneficiaryForDataTable')->name('get_verifications_by_beneficiary');
            Route::post('/get_pensioners_verification_alerts', 'PayrollVerificationController@getPensionersVerificationAlertsForDt')->name('get_pensioners_verification_alerts');
            Route::post('/get_dependents_verification_alerts', 'PayrollVerificationController@getDependentsVerificationAlertsForDt')->name('get_dependents_verification_alerts');
        });


    });

});