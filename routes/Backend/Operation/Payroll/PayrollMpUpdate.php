<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * uNCLAIM routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {
        Route::group(['prefix' => 'mp_update',  'as' => 'mp_update.'], function() {
            /**/


            Route::get('/create/{member_type}/{resource}/{employee}', 'PayrollMpUpdateController@create')->name('create');
            Route::post('/store', 'PayrollMpUpdateController@store')->name('store');
            Route::get('/edit/{payroll_mp_update}', 'PayrollMpUpdateController@edit')->name('edit');
            Route::put('/update/{payroll_mp_update}', 'PayrollMpUpdateController@update')->name('update');
            Route::get('/profile/{payroll_mp_update}', 'PayrollMpUpdateController@profile')->name('profile');
            Route::get('/undo/{payroll_mp_update}', 'PayrollMpUpdateController@undo')->name('undo');
            Route::post('/get_by_beneficiary/{member_type_id}/{resource_id}/{employee}', 'PayrollMpUpdateController@getByBeneficiariesForDt')->name('get_by_beneficiary');
        });


    });

});