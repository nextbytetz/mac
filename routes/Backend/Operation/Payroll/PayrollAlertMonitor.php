<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {


        Route::group([   'prefix' => 'alert_monitor',  'as' => 'alert_monitor.'], function() {
            Route::get('index/{alert_reference}', 'PayrollAlertMonitorController@index')->name('index');
            Route::post('get_beneficiaries_missing_details/{member_type}', 'PayrollAlertMonitorController@getBeneficiariesMissingDetailsForDataTable')->name('get_beneficiaries_missing_details');
            Route::get('reset_alerts', 'PayrollAlertMonitorController@resetAlerts')->name('reset_alerts');
            Route::get('refresh_alert_task_checker', 'PayrollAlertMonitorController@refreshAllPayrollAlertTaskChecker')->name('refresh_alert_task_checker');
            Route::get('export_excel/{alert_index}', 'PayrollAlertMonitorController@exportAlertMonitor')->name('export_excel');

        });


    });

});