<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {

        Route::get('data_migration', 'PayrollController@openDataMigrationMenu')->name('data_migration.menu');

        Route::group([   'prefix' => 'manual_member',  'as' => 'manual_member.'], function() {



            Route::get('index', 'ManualPayrollMemberController@index')->name('index');
            Route::get('upload_page/{member_type}', 'ManualPayrollMemberController@uploadPage')->name('upload_page');
            Route::post('upload_bulk', 'ManualPayrollMemberController@uploadManualFile')->name('upload_bulk');
            Route::post('get_for_datatable', 'ManualPayrollMemberController@getAllForDataTable')->name('get_for_datatable');
            Route::post('get_uploaded_for_datatable/{member_type}', 'ManualPayrollMemberController@getUploadedPerMemberTypeDataTable')->name('get_uploaded_for_datatable');
            Route::post('get_members_with_pending_arrears', 'ManualPayrollMemberController@getMembersWithPendingArrearsDataTable')->name('get_members_with_pending_arrears');
            Route::get('dependent', 'ManualPayrollMemberController@dependentsPage')->name('dependents_page');
            Route::get('create_dependent/{dependent}', 'ManualPayrollMemberController@createDependent')->name('create_dependent');
            Route::post('store_dependent/{employee}', 'ManualPayrollMemberController@storeDependent')->name('store_dependent');
        });
    });










});