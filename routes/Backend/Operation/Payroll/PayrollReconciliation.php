<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * ARREAR routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {
        Route::group(['prefix' => 'reconciliation',  'as' => 'reconciliation.'], function() {
            /**/
            Route::resource('/', 'PayrollReconciliationController', ['except' => ['show']]);
            Route::get('/profile/{payroll_reconciliation}', 'PayrollReconciliationController@profile')->name('profile');
            Route::post('/get_all_for_datatable', 'PayrollReconciliationController@getAllForDataTable')->name('get_all_for_datatable');
            Route::post('initiate/{payroll_reconciliation}', 'PayrollReconciliationController@initiateReconciliation')->name('initiate');
            Route::get('undo/{payroll_reconciliation}', 'PayrollReconciliationController@undo')->name('undo');
            /*Process reconciliation manually*/
            Route::get('process', 'PayrollReconciliationController@processReconciliationJob')->name('process');
        });


    });

});