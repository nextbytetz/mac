<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll\Retiree', 'prefix' => 'payroll/retiree/mp_update',  'as' => 'payroll.retiree.mp_update.'], function() {

        /**
         * Update Bank Details ---for Pensioner and Dependents---Start--
         */
        Route::get('create/{payroll_retiree_followup}/{member_type}/{resource}/{employee}', 'PayrollRetireeMpUpdateController@create')->name('create');
        Route::post('store', 'PayrollRetireeMpUpdateController@store')->name('store');
        Route::get('edit/{payroll_retiree_mp_update}', 'PayrollRetireeMpUpdateController@edit')->name('edit');
        Route::put('update/{payroll_retiree_mp_update}', 'PayrollRetireeMpUpdateController@update')->name('update');
        Route::get('profile/{payroll_retiree_mp_update}', 'PayrollRetireeMpUpdateController@profile')->name('profile');
        Route::get('undo/{payroll_retiree_mp_update}', 'PayrollRetireeMpUpdateController@delete')->name('undo');
        Route::get('index/{member_type_id}/{resource_id}/{employee}', 'PayrollRetireeMpUpdateController@getAllByMemberForDt')->name('index');
        Route::post('get_mp_updates_by_member_dt/{member_type_id}/{resource_id}/{employee}', 'PayrollRetireeMpUpdateController@getMpUpdatesByMemberForDt')->name('get_mp_updates_by_member_dt');
        /*end --update bank details--*/


    });

});