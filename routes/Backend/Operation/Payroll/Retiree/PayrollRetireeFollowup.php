<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll\Retiree', 'prefix' => 'payroll/retiree/followup',  'as' => 'payroll.retiree.followup.'], function() {

        /**
         * Update Bank Details ---for Pensioner and Dependents---Start--
         */
        Route::get('create/{member_type}/{resource}/{employee}', 'PayrollRetireeFollowupController@create')->name('create');
        Route::post('store', 'PayrollRetireeFollowupController@store')->name('store');
        Route::get('edit/{payroll_retiree_followup}', 'PayrollRetireeFollowupController@edit')->name('edit');
        Route::put('update/{payroll_retiree_followup}', 'PayrollRetireeFollowupController@update')->name('update');
        Route::get('profile/{payroll_retiree_followup}', 'PayrollRetireeFollowupController@profile')->name('profile');
        Route::get('undo/{payroll_retiree_followup}', 'PayrollRetireeFollowupController@delete')->name('undo');
        Route::get('index/{member_type_id}/{resource_id}/{employee}', 'PayrollRetireeFollowupController@getAllByMemberForDt')->name('index');
        Route::post('get_alerts_by_member_type_dt/{member_type_id}', 'PayrollRetireeFollowupController@getRetireeAlertByMemberTypeForDt')->name('get_alerts_by_member_type_dt');
        Route::post('get_followups_by_member_dt/{member_type_id}/{resource_id}/{employee_id}', 'PayrollRetireeFollowupController@getRetireeFollowupsByMemberForDt')->name('get_followups_by_member_dt');
        /*end --update bank details--*/

    });

});