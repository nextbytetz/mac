<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {

        /**
         * Update Bank Details ---for Pensioner and Dependents---Start--
         */
        Route::get('create_new_bank/{member_type}/{resource}/{employee}', 'PayrollBankUpdateController@create')->name('create_new_bank');
        Route::post('store_new_bank/{resource}', 'PayrollBankUpdateController@store')->name('store_new_bank');
        Route::get('edit_new_bank/{bank_update}/{employee}', 'PayrollBankUpdateController@edit')->name('edit_new_bank');
        Route::put('update_new_bank/{bank_update}', 'PayrollBankUpdateController@update')->name('update_new_bank');
        Route::get('bank_update/profile/{bank_update}', 'PayrollBankUpdateController@profile')->name('bank_update.profile');
        Route::get('bank_update/undo/{bank_update}/{employee}', 'PayrollBankUpdateController@destroy')->name('bank_update.undo');
        Route::get('bank_update/get_for_beneficiary/{member_type_id}/{resource_id}', 'PayrollBankUpdateController@getBankUpdatesByBeneficiaryForDataTable')->name('bank_update.get_for_beneficiary');
        Route::get('erase_current_bank/{member_type}/{resource}/{employee}', 'PayrollBankUpdateController@eraseCurrentBankDetails')->name('erase_current_bank');
        /*end --update bank details--*/


    });

});