<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {
        /*Menu administration*/
        Route::get('menu', 'PayrollController@index')->name('menu');

        // run pension payroll
        Route::get('run', 'PayrollController@run')->name('pension.run');
        //process
        Route::get('process', 'PayrollController@process')->name('pension.process');

        //export payroll runs - Monthly pension
        Route::get('export_by_member_type/{beneficiary_type}/{payroll_run_approval}', 'PayrollController@exportMpByMemberType')->name('pension.export_by_member_type');
        /**
         * RUN APPROVAL
         */
        Route::get('run_approval/index', 'PayrollController@runApprovalIndex')->name('pension.run_approval.index');
        /*pension run approval profile*/
        Route::get('run_approval/profile/{payroll_run_approval}', 'PayrollController@profileRunApproval')->name('pension.run_approval.profile');
        /*initiate payroll approval*/
        Route::post('run_approval/initiate/{payroll_run_approval}', 'PayrollController@initiateRunApproval')->name('pension.run_approval.initiate');
        /*undo*/
        Route::get('run_approval/undo/{payroll_run_approval}', 'PayrollController@undoRunApproval')->name('pension.run_approval.undo');
        /*Process payment*/
        Route::get('run_approval/process_payment/{payroll_run_approval}', 'PayrollController@processPayment')->name('pension.run_approval.process_payment');
        /*Print payment voucher*/
        Route::get('run_approval/print_payment_voucher/{bank}/{payroll_run_approval}', 'PayrollController@printPaymentVoucherPerBank')->name('pension.run_approval.print_payment_voucher');

        /*get for datatable*/
        Route::post('run_approval/get_for_datatable', 'PayrollController@getPayrollRunApprovalForDataTable')->name('pension.run_approval.get_for_datatable');

        /* Datatables for tabs on payroll run approval profile */
        Route::post('run_approval/get_mp_payments/{member_type}/{payroll_run_approval}/{is_constant_care}', 'PayrollController@getMpByRunApprovalForDataTable')->name('pension.run_approval.get_mp_payments');
        Route::post('run_approval/get_suspended_payments/{member_type}/{payroll_run_approval}', 'PayrollController@getSuspendedMpByRunApprovalForDataTable')->name('pension.run_approval.get_suspended_payments');
        Route::post('run_approval/get_new_payees_for_datatable/{member_type}/{payroll_run_approval}/{is_constant_care}', 'PayrollController@getNewPayeesInPayrollForDataTable')->name('pension.run_approval.get_new_payees_for_datatable');

        Route::post('run_approval/get_terminated_pensioners_for_datatable/{member_type}/{payroll_run_approval}', 'PayrollController@getDeactivatedPensionersBeforeThisPayrollForDataTable')->name('pension.run_approval.get_terminated_pensioners_for_datatable');
        Route::post('run_approval/get_terminated_dependents_for_datatable/{member_type}/{payroll_run_approval}/{is_constant_care}', 'PayrollController@getDeactivatedDependentsBeforeThisPayrollForDataTable')->name('pension.run_approval.get_terminated_dependents_for_datatable');

        Route::post('run_approval/get_suspended_pensioners_for_datatable/{member_type}/{payroll_run_approval}', 'PayrollController@getSuspendedPensionersBeforeThisPayrollForDataTable')->name('pension.run_approval.get_suspended_pensioners_for_datatable');
        Route::post('run_approval/get_suspended_dependents_for_datatable/{member_type}/{payroll_run_approval}/{is_constant_care}', 'PayrollController@getSuspendedDependentsBeforeThisPayrollForDataTable')->name('pension.run_approval.get_suspended_dependents_for_datatable');
        Route::post('run_approval/get_members_for_supplier_id/{payroll_run_approval}', 'PayrollController@getBeneficiariesWithNoErpSupplierIdForDataTable')->name('pension.run_approval.get_members_for_supplier_id');

        Route::post('run_approval/get_members_with_changed_bank/{payroll_run_approval}', 'PayrollController@getMembersWithChangedBankForDt')->name('pension.run_approval.get_members_with_changed_bank');
        Route::post('run_approval/get_payees_with_arrears_dt/{payroll_run_approval}', 'PayrollController@getPayeesWithArrearsForDataTable')->name('pension.run_approval.get_payees_with_arrears_dt');
        Route::post('run_approval/get_child_overage_no_extension_dt/{payroll_run_approval}', 'PayrollController@getChildrenWithNoExtensionApprovalDt')->name('pension.run_approval.get_child_overage_no_extension_dt');
        Route::post('run_approval/get_otherdep_not_eligible_dt/{payroll_run_approval}', 'PayrollController@getOtherDependentsFinishedTheirCyclesStillOnPayrollDt')->name('pension.run_approval.get_otherdep_not_eligible_dt');

        /*export*/
        Route::get('run_approval/export_payees_by_bank/{payroll_run_approval}/{bank_id}', 'PayrollController@exportPayeesByBankPayrollRunApproval')->name('pension.run_approval.export_payees_by_bank');
        /*--end of run approval----


/*open beneficiary profile*/
        Route::get('beneficiary_profile/{member_type}/{resource}/{employee}', 'PayrollController@openBeneficiaryProfile')->name('pension.beneficiary_profile');
        /**
         * DataTables for tabs on pensioner / dependents profile---start----
         */
        /*Get monthly pension for beneficiary per notification report*/
        Route::post('pensions/get_for_beneficiary/{member_type}/{resource}/{employee}', 'PayrollController@getMpByBeneficiaryForDataTable')->name('pensions.get_for_beneficiary');
        Route::post('suspended_pensions/get_for_beneficiary/{member_type}/{resource}/{employee}', 'PayrollController@getSuspendedMpByBeneficiaryForDataTable')->name('suspended_pensions.get_for_beneficiary');


        /*----end DataTables ------------*/


        /*E-office routes --*/
        Route::post('beneficiary/update_documents/{member_type}/{resource}', 'PayrollController@updateBeneficiaryDocument')->name('update_beneficiary_document');
        /*post beneficiary to e-office*/
        Route::get('beneficiary/post_to_dms/{member_type}/{resource}/{employee}', 'PayrollController@postBeneficiaryToDms')->name('beneficiary.post_to_dms');
        Route::get('beneficiary/re_post_to_dms/{member_type}/{resource}/{employee}', 'PayrollController@repostBeneficiaryToDms')->name('beneficiary.re_post_to_dms');
        /*post pending beneficiaries bulk who are pending*/
        Route::get('beneficiary/post_bulk_to_dms', 'PayrollController@postBulkBeneficiariesToDms')->name('beneficiary.post_bulk_to_dms');
        /*re-post payroll file to e-office */
        Route::get('post_to_dms/{payroll_run_approval}', 'PayrollController@postPayrollToDms')->name('post_to_dms');


        /*Documents  routes*/
        Route::get('beneficiary/document_validated/{member_type}/{resource}/{document_type}', 'PayrollController@showValidatedBeneficiaryDocumentTreePerType')->name('beneficiary.document_validated');
        Route::post('beneficiary/current_base64_document/{member_type}/{resource}/{document_pivot_id}', 'PayrollController@currentBase64Document')->name('notification_report.current_base64_document')->where('incident', '[0-9]+');
        Route::get('beneficiary/document_center/{member_type}/{resource}', 'PayrollController@showAllBeneficiaryDocumentsTree')->name('beneficiary.document_center');
        /*end documents*/

        /*Payroll alert moitor routes*/
        Route::post('get_pending_beneficiary_documents/{document_type}', 'PayrollController@getPendingDocsForBeneficiaryUpdateDataTable')->name('get_pending_beneficiary_documents');
        Route::get('get_pending_beneficiary_documents_action/{member_type}/{resource}/{document_type}', 'PayrollController@getPendingDocsForBeneficiaryAction')->name('get_pending_beneficiary_documents_action');
        Route::post('get_all_suspended_beneficiaries_dt', 'PayrollController@getAllSuspendedBeneficiariesForDt')->name('get_all_suspended_beneficiaries_dt');
        /**
         * Update Bank Details ---for Pensioner and Dependents---Start--
         */
//        Route::get('edit_bank_info', 'PayrollBankUpdateController@edit')->name('edit_bank');
//        Route::post('update_bank_info', 'PayrollBankUpdateController@update')->name('update_bank');

        /*end --update bank details--*/

        /*Hold/Release hold status of beneficiary*/
        Route::get('beneficiary/open_hold_page/{member_type}/{resource}/{employee}', 'PayrollController@openHoldBeneficiaryPage')->name('beneficiary.open_hold_page');
        Route::put('beneficiary/hold', 'PayrollController@updateHoldBeneficiary')->name('beneficiary.hold');
        Route::get('beneficiary/release_hold/{member_type}/{resource}/{employee}', 'PayrollController@releaseHoldBeneficiary')->name('beneficiary.release_hold');
        Route::post('beneficiary/get_held_for_dt', 'PayrollController@getHeldBeneficiariesForDt')->name('beneficiary.get_held_for_dt');





        /**
         *
         * PENSIONERS MAIN ROUTES =================================
         *
         */

        Route::get('pensioners', 'PensionerController@getRegisteredPensioners')->name('pensioners');


        //---End Pensioners -------------------------------

        /**
         *
         * GENERAL MAIN ROUTES =================================
         *
         */

        Route::post('pending_workflow', 'PayrollController@payrollPendingWorkflows')->name('pending_workflow');

        Route::get('update_beneficiary_doc_use_status/{doc_id}/{status}', 'PayrollController@updateBeneficiaryDocUseStatus')->name('update_beneficiary_doc_use_status');
        Route::get('update_beneficiary_doc_use_pending_status/{doc_id}/{status}/{pending_status}', 'PayrollController@updateBeneficiaryDocUsePendingStatus')->name('update_beneficiary_doc_use_pending_status');

        Route::get('document_usage_status/{member_type}/{resource}/{employee}', 'PayrollController@manageDocumentUsageStatusForApproval')->name('beneficiary.document_usage_status');
        //---End Pensioners -------------------------------



        /*Suspended runs/payments*/
        Route::group([ 'prefix' => 'suspended_runs',  'as' => 'suspended_runs.'], function() {

            Route::get('reason_remove_pending/{member_type_id}/{resource_id}/{employee_id}', 'PayrollController@reasonForRemovingSuspendedPayments')->name('reason_remove_pending');
            Route::post('remove_pending', 'PayrollController@removeSuspendedPayments')->name('remove_pending');
        });

    });

});