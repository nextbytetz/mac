<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {


        Route::group([   'prefix' => 'manual_payroll_run',  'as' => 'manual_payroll_run.'], function() {



            Route::get('index', 'ManualPayrollRunController@index')->name('index');
            Route::get('upload_page/{member_type}', 'ManualPayrollRunController@uploadPage')->name('upload_page');
            Route::post('store_upload', 'ManualPayrollRunController@storeUploadManualPayrollRuns')->name('store_upload');
            Route::post('get_by_member_type_dt/{member_type}', 'ManualPayrollRunController@getByMemberTypeForDt')->name('get_by_member_type_dt');

            /*sync routes*/
            Route::get('open_sync_page', 'ManualPayrollRunController@openSyncPage')->name('open_sync_page');
            Route::put('sync_manual_pensions_to_member', 'ManualPayrollRunController@syncManualPensionsToMember')->name('sync_manual_pensions_to_member');
            Route::post('get_beneficiary_from_manual_dt', 'ManualPayrollRunController@getBeneficiaryFromManualPayrollDt')->name('get_beneficiary_from_manual_dt');
            Route::post('get_payroll_runs_by_request_dt', 'ManualPayrollRunController@getManualPayrollRunsByRequestForDt')->name('get_payroll_runs_by_request_dt');
            Route::post('get_pensions_by_beneficiary_dt/{member_type_id}/{resource_id}/{employee_id}', 'ManualPayrollRunController@getManualPensionByBeneficiaryDt')->name('get_pensions_by_beneficiary_dt');

        });
    });










});