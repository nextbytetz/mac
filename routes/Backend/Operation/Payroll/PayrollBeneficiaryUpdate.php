<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {

        /**
         * Update beneficiary Details ---for Pensioner and Dependents---Start--
         */
        Route::group([   'prefix' => 'beneficiary_update',  'as' => 'beneficiary_update.'], function() {
            Route::resource('/', 'PayrollBeneficiaryUpdateController', ['except' => ['show', 'index', 'create', 'edit', 'update']]);
            Route::get('create/{member_type}/{resource}/{employee}', 'PayrollBeneficiaryUpdateController@create')->name('create');
            Route::get('edit/{payroll_beneficiary_update}/{employee}', 'PayrollBeneficiaryUpdateController@edit')->name('edit');
            Route::put('/update/{payroll_beneficiary_update}', 'PayrollBeneficiaryUpdateController@update')->name('update');
            Route::get('/profile/{payroll_beneficiary_update}', 'PayrollBeneficiaryUpdateController@profile')->name('profile');
            Route::get('/undo/{payroll_beneficiary_update}/{employee}', 'PayrollBeneficiaryUpdateController@destroy')->name('undo');
            Route::post('/get_for_beneficiary/{member_type_id}/{resource_id}', 'PayrollBeneficiaryUpdateController@getBeneficiaryUpdatesForDataTable')->name('get_for_beneficiary');
        });
        /*end --update--*/


    });

});