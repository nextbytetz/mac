<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {

        /**
         * Payroll child suspension
         */
        Route::group([ 'namespace' => 'Suspension',  'prefix' => 'retiree_suspension',  'as' => 'retiree_suspension.'], function() {

            Route::post('get_for_datatable', 'PayrollRetireeSuspensionController@getForDataTable')->name('get_for_datatable');
            Route::post('get_pending_for_datatable', 'PayrollRetireeSuspensionController@getPendingForDataTable')->name('get_pending_for_datatable');
            Route::post('get_child_alert_datatable', 'PayrollChildSuspensionController@getChildLimitAgeAlertsForDataTable')->name('get_child_alert_datatable');

            /*end --payroll child suspension--*/

        });
    });

});