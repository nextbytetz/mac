<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {

        /**
         * Payroll manual suspension
         */
        Route::group([ 'namespace' => 'Suspension',  'prefix' => 'suspension',  'as' => 'suspension.'], function() {

            Route::get('index', 'PayrollManualSuspensionController@index')->name('index');
            Route::get('profile/{payroll_manual_suspension}', 'PayrollManualSuspensionController@profile')->name('profile');
            Route::post('get_for_datatable', 'PayrollManualSuspensionController@getForDataTable')->name('get_for_datatable');
            Route::post('get_suspensions_by_manual_for_dt/{payroll_manual_suspension}', 'PayrollManualSuspensionController@getSuspensionsByManualForDt')->name('get_suspensions_by_manual_for_dt');
            Route::get('create', 'PayrollManualSuspensionController@create')->name('create');
            Route::post('process', 'PayrollManualSuspensionController@process')->name('process');

            /*end --payroll manual suspension--*/

        });
    });

});