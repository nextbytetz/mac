<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * PAYROLL routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {

        /**
         * Payroll child suspension
         */
        Route::group([ 'namespace' => 'Suspension',  'prefix' => 'child_suspension',  'as' => 'child_suspension.'], function() {

            Route::post('get_for_datatable', 'PayrollChildSuspensionController@getForDataTable')->name('get_for_datatable');
            Route::post('get_pending_for_datatable', 'PayrollChildSuspensionController@getPendingForDataTable')->name('get_pending_for_datatable');
            Route::post('get_child_alert_datatable', 'PayrollChildSuspensionController@getChildLimitAgeAlertsForDataTable')->name('get_child_alert_datatable');
            Route::post('get_child_alert_edu_ext_dt', 'PayrollChildSuspensionController@getChildWithEducationExtensionDeadlineAlertsForDt')->name('get_child_alert_edu_ext_dt');
            Route::post('get_child_alert_ext_dt/{child_continuation_cv_ref}', 'PayrollChildSuspensionController@getChildWithExtensionDeadlineAlertsForDt')->name('get_child_alert_ext_dt');

            /*end --payroll child suspension--*/

        });
    });

});