<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


    /*
    * ARREAR routes
    */
    Route::group(['namespace' => 'Payroll', 'prefix' => 'payroll',  'as' => 'payroll.'], function() {
        Route::group(['prefix' => 'recovery',  'as' => 'recovery.'], function() {
            /**/
            Route::resource('/', 'PayrollRecoveryController', ['except' => ['show', 'index', 'create', 'edit', 'update']]);
            Route::get('/create_manual_arrears/{manual_payroll_member}', 'PayrollRecoveryController@createManualArrearsManualMembers')->name('create_manual_arrears');//Manual members do not exist on system
            Route::get('/create_manual_arrears_system_members/{member_type}/{resource}/{employee}/{arrears_months}', 'PayrollRecoveryController@createManualArrearsSystemMembers')->name('create_manual_arrears_system_members');
            Route::get('/create/{member_type}', 'PayrollRecoveryController@create')->name('create');
//            Route::post('/store/{resource}', 'PayrollRecoveryController@store')->name('store_new');
            Route::get('/edit/{payroll_recovery}', 'PayrollRecoveryController@edit')->name('edit');
            Route::put('/update/{payroll_recovery}', 'PayrollRecoveryController@update')->name('update');
            Route::get('/show_choose_type_form/{member_type}/{resource}/{employee}', 'PayrollRecoveryController@showChooseTypeForm')->name('show_choose_type_form');
            Route::post('/choose_recovery_type', 'PayrollRecoveryController@chooseType')->name('choose_recovery_type');
            Route::get('/profile/{payroll_recovery}', 'PayrollRecoveryController@profile')->name('profile');
            Route::get('/undo/{payroll_recovery}', 'PayrollRecoveryController@destroy')->name('undo');
            Route::get('/open_cancel_page/{payroll_recovery}', 'PayrollRecoveryController@openCancelPage')->name('open_cancel_page');
            Route::put('/cancel/{payroll_recovery}', 'PayrollRecoveryController@cancelApprovedRecovery')->name('cancel');
            Route::get('/get_for_beneficiary/{member_type_id}/{resource_id}/{employee}', 'PayrollRecoveryController@getRecoveriesByBeneficiaryForDataTable')->name('get_for_beneficiary');
            /*Get approved recoveries for datatable by beneficiary*/
            Route::post('/arrears/get_approved_for_beneficiary/{member_type_id}/{resource_id}/{employee}', 'PayrollRecoveryController@getApprovedArrearsByBeneficiaryForDataTable')->name('arrears.get_approved_for_beneficiary');
            Route::post('/unclaims/get_approved_for_beneficiary/{member_type_id}/{resource_id}/{employee}', 'PayrollRecoveryController@getApprovedUnclaimsByBeneficiaryForDataTable')->name('unclaims.get_approved_for_beneficiary');
            Route::post('/deductions/get_approved_for_beneficiary/{member_type_id}/{resource_id}/{employee}', 'PayrollRecoveryController@getApprovedDeductionsByBeneficiaryForDataTable')->name('deductions.get_approved_for_beneficiary');
            /*get pending recoveries*/
            Route::post('/get_pending_for_datatable', 'PayrollRecoveryController@getPendingRecoveriesForDataTable')->name('get_pending_for_datatable');
        });


    });

});