<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member'], function() {



//            Route::post('status', 'EmployerController@employerStatusShow')->name('show.status');
//
//            Route::get('payesdl', 'EmployerController@payesdl')->name('payesdl');
//
//            Route::post('payesdl', 'EmployerController@payesdlShow')->name('show.payesdl');
//
//            Route::get('newemployer', 'EmployerController@newEmployer')->name('newemployer');
//
//            Route::post('newemployer', 'EmployerController@newEmployerShow')->name('show.newemployer');
//
//            Route::get('closed', 'EmployerController@closedBusiness')->name('closed');
//
//            Route::post('closed', 'EmployerController@closedBusinessShow')->name('show.closed');
//
//            Route::get('LargeContributor','EmployerController@viewLargeContributor')->name('LargeContributor');
//
//             Route::get('LargeContributorCertificate','EmployerController@viewLargeContributorCertificates')->name('LargeContributorCertificate');
//
//
//
//
//            //End of ADES service routes========================================
//
////        EMPLOYERS ==================================
//            Route::resource('employer', 'EmployerController', ['except' => ['show']]);
//            Route::get('employer/profile/{employer}', 'EmployerController@profile')->name('employer.profile');
//            Route::get('agent/agentprofile/{agent}', 'EmployerController@agentProfile')->name('agent.agentprofile');
//            Route::get('employer/merge', 'EmployerController@mergeEmployers')->name('employer.merge');
//            Route::put('employer/merge/post', 'EmployerController@postMergeEmployers')->name('employer.post_merge');
//            Route::get('employer/unmerge', 'EmployerController@unMergeEmployers')->name('employer.unmerge');
//            Route::put('employer/unmerge/post', 'EmployerController@postUnMergeEmployers')->name('employer.post_unmerge');
//            Route::get('employer/{employer}/verification', 'EmployerController@onlineVerification')->name('employer.verification');
//            Route::get('employer/{verification}/verification_profile', 'EmployerController@onlineVerificationProfile')->name('employer.verification_profile');
//            Route::get('employer/verification/{verification}/documents_library', 'EmployerController@showVerificationDocumentLibrary')->name('employer.verification_document_library');
//            Route::get('employer/verification/{verification}/{documentId}/download_document', 'EmployerController@downloadVerificationDocument')->name('employer.verification_download_document');
//            Route::get('employer/verification/{verification}/{documentId}/open_document', 'EmployerController@openVerificationDocument')->name('employer.verification_open_document');
//            Route::get('get', 'EmployerController@get')->name('employer.get');
//            Route::get('get_bookings/{employer}', 'EmployerController@getBookingsForDataTable')->name('employer.bookings');
//            Route::get('get_contributions/{employer}', 'EmployerController@getContributionsForDataTable')->name('employer.contributions');
//            Route::get('get_legacy_contributions/{employer}', 'EmployerController@getLegacyContributionsForDataTable')->name('employer.legacy_contributions');
//            Route::get('get_interests/{employer}', 'EmployerController@getInterestsForDataTable')->name('employer.interests');
//            Route::get('get_inactive_receipts/{employer}', 'EmployerController@getInactiveReceiptsForDataTable')->name('employer.inactive_receipts');
//            Route::get('employers', 'EmployerController@getRegisterEmployers')->name('employers');
//            Route::post('employers_tin', 'EmployerController@getRegisterEmployersTin')->name('employers.tin');
//            Route::post('stored_employers', 'EmployerController@getAllStoredEmployers')->name('stored_employers');
//            /*get beneficiary employees only */
//            Route::get('beneficiary_employers', 'EmployerController@getBeneficiaryEmployers')->name('beneficiary_employers');
//            /* All employers without global scope*/
//            Route::get('all_employers', 'EmployerController@getAllEmployers')->name('all_employers');
//            /* Employee Counts */
//            Route::get('get_employee_count/{employer}', 'EmployerController@getEmployeeCountsForDataTable')->name('employer.employee_count_get');
//            /*  Employer Interest selected */
//            Route::get('employer/profile/interest/{booking_interest}', 'EmployerController@viewSelectedInterest')->name('employer.interest_profile');
//            /* Get All employees for this employer */
//            Route::get('employer/employees/{employer}', 'EmployerController@getRegisteredEmployees')->name('employer.employees');
//
//            /* Start : Employer Closures */
//            Route::get('employer/{employer}/close', 'EmployerController@getCloseBusiness')->name('employer.close_business');
//            Route::put('employer/{employer}/close', 'EmployerController@postCloseBusiness')->name('employer.close_business.post');
//            Route::get('closed_business', 'EmployerController@getClosedBusinessTRA')->name('closed_business');
//            Route::post('closed_business/get', 'EmployerController@getClosedBusinessTRAForDatatable')->name('closed_business.datatable');
//            Route::get('closed_business/{closed_business}/show', 'EmployerController@showClosedBusinessTRA')->name('closed_business.show');
//            Route::post('closed_business/{closed_business}/associate', 'EmployerController@associateClosedBusinessTRA')->name('closed_business.associate');
//            Route::delete('closed_business/remove', 'EmployerController@deleteClosedBusinessTRA')->name('closed_business.delete');
//            Route::get('employer/{employer}/open', 'EmployerController@getOpenBusiness')->name('employer.open_business');
//            Route::post('employer/{employer}/open', 'EmployerController@postOpenBusiness')->name('employer.open_business.post');
//            //Pending, querying all business closures of a particular employer
//
//            /* End : Employer Closures */
//
//
//            /*
//             * ADJUST EMPLOYER INTEREST SELECTED
//             */
//            Route::get('employer/profile/interest_adjust/{booking_interest}', 'EmployerController@editInterest')->name('employer.adjust_interest');
//            Route::put('employer/profile/interest_adjust/{booking_interest}', 'EmployerController@adjustInterest')->name('employer.adjust_interest');
//
//
//            /* Employer with Interest Overpayment Report*/
//
//            Route::get('employer/interest_overpayment', 'EmployerController@getEmployerInterestOverpayment')->name('employer.interest_overpayment');
//
//
//
//            /*END interest Refund---*/
//
//
//            /*
//             * WRITE OFF EMPLOYER INTEREST
//             */
//            Route::get('employer/profile/interest_write_off/{employer}', 'EmployerController@writtingOffInterestPage')->name('employer.write_off_interest_page');
//            Route::put('employer/profile/interest_write_off/{employer}', 'EmployerController@writeOffInterest')->name('employer.write_off_interest');
//// route page to approve interest written off
//            Route::get('employer/profile/interest_write_off_overview/{employer}', 'EmployerController@getInterestWrittenOffOverview')->name('employer.write_off_interest_overview');
//            Route::get('employer/profile/interest_write_off_approve/{interest_write_off}', 'EmployerController@ApproveInterestWrittenOff')->name('employer.write_off_interest_approve');
//            Route::get('employer/profile/interests_written_off/{interest_write_off}', 'EmployerController@getInterestsWrittenOffForDataTable')->name('employer.written_off_interests');
//            //Modify/ edit interest written offs
//            Route::get('employer/profile/interest_write_off_edit/{interest_write_off}', 'EmployerController@writtenOffInterestsEdit')->name('employer.written_off_interest_edit');
//            Route::put('employer/profile/interest_write_off_update/{interest_write_off}', 'EmployerController@writtenOffInterestsUpdate')->name('employer.write_off_interest_update');
////            Interest adjust overview
//            Route::get('employer/profile/interest_adjust_overview/{employer}', 'EmployerController@getInterestAdjustedOverview')->name('employer.adjust_interest_overview');
//            /*
//             * CONTRIBUTION
//             */
//            //Form to search receipt by rctno from compliance menu
//            Route::get('receipt', 'ContributionController@index')->name('receipt.search');
//            //Route::get('receipt/get', 'ContributionTableController')->name('receipt.get');
//
//
//            /**
//             *   CONTRIBUTION TRACKS
//             */
//            Route::resource('contribution_track', 'ContributionTrackController', ['except' => ['show']]);
//// create new track
//            Route::get('contribution_track/{receipt_code}/create', 'ContributionTrackController@create_new')->name('contribution_track.create_new');
//
//
//            /**
//             * BOOKING PROFILE=====================
//             */
////         Employer booking selected
//            Route::get('employer/profile/booking/{booking}', 'EmployerController@bookingProfile')->name('employer.booking_profile');
//// open booking adjustment page
//            Route::get('employer/profile/booking_adjust/{booking}', 'EmployerController@bookingAdjustPage')->name('employer.booking_adjust');
//// open booking adjustment page
//            Route::put('employer/profile/booking_adjust/{booking}', 'EmployerController@bookingAdjust')->name('employer.booking_adjust');            //END --Booking-profile =--------------
//
//
//
//
//            /**
//             * BOOKING INTEREST REFUND ROUTES============start=========
//             */
//
//            Route::resource('booking_interest', 'BookingInterestController', ['except' => ['show']]);
//
//
//            Route::get('booking_interest/refund_overview/{employer}', 'BookingInterestController@interestRefund')->name('booking_interest.refund_overview');
//
//
//            Route::get('booking_interest/refund_page/{employer}', 'BookingInterestController@refundPage')->name('booking_interest.refunding_page');
//
//
//            Route::get('booking_interest/interest_refund_overview/{employer}', 'BookingInterestController@getInterestRefundOverviewForDataTable')->name('booking_interest.get_refunds');
//
//            Route::post('booking_interest/get_eligible_for_refund/{employer}', 'BookingInterestController@getInterestEligibleForRefundingForDataTable')->name('booking_interest.get_eligible_for_refund');
//            /*Refund selected*/
//            Route::post('booking_interest/refund_selected', 'BookingInterestController@refundSelected')->name('booking_interest.refund_selected');
//
//            /*Interest refund profile*/
//            Route::get('booking_interest/refund_profile/{interest_refund}', 'BookingInterestController@interestRefundProfile')->name('booking_interest.refund_profile');
//
//            /*Interest refunded*/
//            Route::get('booking_interest/interest_refunded/{interest_refund}', 'BookingInterestController@getInterestRefundedForDataTable')->name('booking_interest.get_interest_refunded');
//
//            /*Update Bank details*/
//            Route::put('booking_interest/interest_refund/update_bank_details/{interest_refund}', 'BookingInterestController@updateBankDetails')->name('booking_interest.refund.update_bank_details');
//
//            /*Get Interest for modifying interest refund*/
//            Route::post('booking_interest/get_eligible_for_modifying_refund/{employer}/{interest_refund}', 'BookingInterestController@getInterestEligibleForModifyRefundForDataTable')->name('booking_interest.get_eligible_for_modifying_refund');
//
//            /*Modify interest refund*/
//            Route::get('booking_interest/refund/edit/{interest_refund}', 'BookingInterestController@modifyInterestRefund')->name('booking_interest.refund.edit');
//
//            /*Add interest refund*/
//            Route::get('booking_interest/interest_refund/add_interest/{booking_interest}/{interest_refund}', 'BookingInterestController@addInterestRefund')->name('booking_interest.refund.add_interest');
//            /*Remove interest from refund batch*/
//            Route::get('booking_interest/interest_refund/remove_interest/{booking_interest}/{interest_refund}', 'BookingInterestController@removeInterestRefund')->name('booking_interest.refund.remove_interest');
//
//            /*Undo interest refund */
//            Route::get('booking_interest/interest_refund/undo/{interest_refund}', 'BookingInterestController@undoInterestRefund')->name('booking_interest.refund.undo');
//
//
//            /* Initiate / Approve Interest Refund*/
//            Route::get('booking_interest/interest_refund/initiate/{interest_refund}', 'BookingInterestController@initiateInterestRefund')->name('booking_interest.refund.initiate');
//
//            /* End of Booking Interest  REFUND Routes ----------end --------*/
//            /*------------ END EMPLOYER ROUTE--------------------------------*/
//
//
//
//
//
//
//
//            /**
//             * EMPLOYEE ROUTES==========================
//             */
//
//            /* =====EMPLOYEE ROUTES -*/
//            Route::resource('employee', 'EmployeeController', ['except' => ['show']]);
//            Route::get('employee/profile/{employee}', 'EmployeeController@profile')->name('employee.profile'); //Profile
//            Route::get('get_employee', 'EmployeeController@get')->name('employee.get'); //All employees
//            //get employers
//            Route::get('get_employers', 'EmployeeController@getEmployers')->name('employee.get_employers');
//
//            Route::get('get_employee_contributions/{employee}', 'EmployeeController@getContributionsForDataTable')->name('employee.contributions');  //Contributions
//            Route::get('get_employee_legacy_contributions/{employee}', 'EmployeeController@getLegacyContributionsForDataTable')->name('employee.legacy_contributions');  //Contributions
//            Route::get('employees', 'EmployeeController@getRegisterEmployees')->name('employees');
//            /*get beneficiary employees only */
//            Route::get('beneficiary_employees', 'EmployeeController@getBeneficiaryEmployees')->name('beneficiary_employees');
//            /* add individual contribution */
//            Route::get('employee/add_contribution/{employee}', 'EmployeeController@addContribution')->name('employee.add_contribution');
//            Route::post('employee/store_contribution/{employee}', 'EmployeeController@storeContribution')->name('employee.store_contribution');
//
//
//
//            /*  DEPENDENTS ROUTES ====================================================== */
//
//            Route::resource('employee/dependent', 'DependentController', ['except' => ['show']]);
//            /*edit dependent*/
//            Route::get('employee/dependent/{dependent}/edit/{employee}', 'DependentController@editDependent')->name('dependent.edit_dependent');
//            Route::get('employee/dependent/{employee}/create', 'DependentController@create_new')->name('dependent.create_new');
//            Route::post('employee/dependent/{employee}/store', 'DependentController@store_new')->name('dependent.store_new');
//
//            Route::get('dependents', 'DependentController@getRegisteredDependents')->name('dependents');
//            /*get dependents eligible for gratuity*/
//            Route::get('gratuity_dependents', 'DependentController@getGratuityDependents')->name('gratuity_dependents');
//            // ---END DEPENDENTS ------------------------------------------------------
//
//
//
//
//
//
//
//            /*CLAIM / NOTIFICATION=====================================*/
//            Route::get('get_accidents/{employee}', 'EmployeeController@getAccidentNotificationsForDatatable')->name('employee.get_accident_notifications'); // accident notifications for an employee
//
//            Route::get('get_diseases/{employee}', 'EmployeeController@getDiseaseNotificationsForDatatable')->name('employee.get_disease_notifications'); // disease notifications for an employee
//
//            Route::get('get_death/{employee}', 'EmployeeController@getDeathNotificationForDatatable')->name('employee.get_death_notification'); // death notifications for an employee
//
//            Route::get('get_employment_histories/{employee}', 'EmployeeController@getEmploymentHistoryForDataTable')->name('employee.get_employment_histories'); // employment Histories
//
//            Route::get('get_employment_old_compensations/{employee}', 'EmployeeController@getOldCompensationsForDataTable')->name('employee.get_employment_old_compensations'); // old compensations
//
//            Route::get('get_dependents/{employee}', 'EmployeeController@getDependentsForDataTable')->name('employee.get_dependents'); // Dependents
//
//
//            /*
//             * employment history ADD / EDIT
//             */
//            Route::get('employee/employment_history/{employee}/create', 'EmployeeController@createEmploymentHistory')->name('employee.create_employment_history');
//// create
//            Route::post('employee/employment_history/{employee}', 'EmployeeController@storeEmploymentHistory')->name('employee.store_employment_history');
//            //edit
//            Route::get('employee/employment_history/{employment_history}/edit', 'EmployeeController@editEmploymentHistory')->name('employee.edit_employment_history');
//            Route::put('employee/employment_history/update/{employment_history}', 'EmployeeController@updateEmploymentHistory')->name('employee.update_employment_history');//update
////  Delete
//            Route::delete('employee/employment_history/{employment_history}', 'EmployeeController@deleteEmploymentHistory')->name('employee.delete_employment_history');
//
//
//
//            /*
//              *
//              * Compensation history -> Old Compensations======================
//              */
//            //create
//            Route::get('employee/old_compensation/{employee}/create', 'EmployeeController@createOldCompensation')->name('employee.create_old_compensation');
//            Route::post('employee/old_compensation/{employee}', 'EmployeeController@storeOldCompensation')->name('employee.store_old_compensation');
//
//            //edit
//            Route::get('employee/old_compensation/{employment_old_compensation}/edit', 'EmployeeController@editEmployeeOldCompensation')->name('employee.edit_old_compensation');
//            Route::put('employee/old_compensation/update/{employment_old_compensation}', 'EmployeeController@updateOldCompensation')->name('employee.update_old_compensation');//update
////  Delete
//            Route::delete('employee/old_compensation/{employment_old_compensation}', 'EmployeeController@deleteEmployeeOldCompensation')->name('employee.delete_old_compensation');
//
//
//            // end compensatation history ------------------------
//
//// --New notification report ---> chooses incident type
//
//            Route::get('employee/choose_incident_type/{employee}', 'EmployeeController@chooseIncidentType')->name('employee.choose_incident_type');
//

//            --End Claim -------------------------------------------










            /*============REGISTRATION EMPLOYERS ===========ROUTES ---------*/
//            Route::resource('employer_registration', 'EmployerRegistrationController', ['except' => ['show']]);
//
//            /*name check before adding new*/
//            Route::get('employer_registration/name_check', 'EmployerRegistrationController@nameCheck')->name('employer_registration.name_check');
//            /*get all employers (unregistered + registered)*/
//            Route::get('employer_registration/name_check/getAllEmployers', 'EmployerRegistrationController@getAllEmployers')->name('employer_registration.name_check.get_all_employer');
//            /*profile*/
//            Route::get('employer_registration/profile/{employer_registration}', 'EmployerRegistrationController@profile')->name('employer_registration.profile');
////           /* register Employer Information */
//            Route::post('employer_registration/register', 'EmployerRegistrationController@register')->name('employer_registration.register');
//
//            /*show Document Library*/
//            Route::get('employer_registration/documents_library/{employer_registration}', 'EmployerRegistrationController@showDocumentLibrary')->name('employer_registration.documents_library');
//            /*download document*/
//            Route::get('employer_registration/download_document/{employer_registration}/{documentId}', 'EmployerRegistrationController@downloadDocument')->name('employer_registration.download_document');
//            /*open document*/
//            Route::get('employer_registration/open_document/{employer_registration}/{documentId}', 'EmployerRegistrationController@openDocument')->name('employer_registration.open_document');
//            /* Download upload error */
//            Route::get('employer_registration/error/{employer_registration}', 'EmployerRegistrationController@downloadError')->name("employer_registration.download_error");
//
//            /*   Upload employee list / wcr3 */
//            Route::get('employer_registration/{employer}/upload_employee', 'EmployerRegistrationController@employeeListFile')->name('employer_registration.employee_list_file');
//            Route::post('employer_registration/{employer}/upload/store', 'EmployerRegistrationController@uploadEmployeeList')->name('employer_registration.upload_employee_list');
//            Route::get('employer_registration/{employer}/load/store', 'EmployerRegistrationController@loadEmployeeList')->name('employer_registration.load_employee_list');
//            /*Undo loaded employees*/
//            Route::get('employer_registration/{employer}/undo_loaded_employees', 'EmployerRegistrationController@undoLoadedEmployees')->name('employer_registration.undo_loaded_employees');
//            /* Upload progress */
//            Route::post('employer_registration/upload_progress/{employer}', 'EmployerRegistrationController@uploadProgress')->name("employer_registration.upload_progress");
//
//            //=============================================================== MAC GEPG
//            Route::get('return_employees/{employer}', 'EmployerRegistrationController@returnEmployees')->name('return_employees');
//            Route::get('generate_number/{employer}', 'EmployeeUploadController@generateNumber');
//            Route::resource('online_uploaded_employees', 'EmployeeUploadController');
//
//            //================================================== MAC - GEPG
//
//
//            /*Approve Registration*/
//            Route::get('employer_registration/approve/{employer_registration}', 'EmployerRegistrationController@approveRegistration')->name('employer_registration.approve');
//            /*Print Certificate of Registration*/
//            Route::get('employer_registration/print_certificate/{employer_registration}', 'EmployerRegistrationController@printCertificate')->name('employer_registration.print_certificate');
//            /* Reissue Certificate */
//            Route::get('employer_registration/reprint_certificate/{employer_registration}', 'EmployerRegistrationController@reprintCertificate')->name('employer_registration.reprint_certificate');
//            /*Print Issuance of Registration No*/
//            Route::get('employer_registration/print_issuance/{employer_registration}', 'EmployerRegistrationController@printIssuance')->name('employer_registration.print_issuance');
//
//            /*  Undo / Reject Registration */
//            Route::get('employer_registration/undo/{employer_registration}', 'EmployerRegistrationController@destroy')->name('employer_registration.undo');
//            /* Name clearance */
//            Route::get('employer_registration/get_name_clearance/{employer_registration}', 'EmployerRegistrationController@getNameClearance')->name("employer_registration.get_name_clearance");
//
//            /* ----------END OF REGISTRATION --------------------*/
//
//
//            /*============UNREGISTERED EMPLOYERS ===========ROUTES ---------*/
//            Route::resource('unregistered_employer', 'UnregisteredEmployerController', ['except' => ['show']]);
//
//            /* Profile */
//            Route::get('unregistered_employer/profile/{unregistered_employer}', 'UnregisteredEmployerController@profile')->name('unregistered_employer.profile');
//            /* Register / approve */
//            Route::get('unregistered_employer/register/{unregistered_employer}', 'UnregisteredEmployerController@register')->name('unregistered_employer.register');
//
//            /* Undo / delete */
//            Route::get('unregistered_employer/undo/{unregistered_employer}', 'UnregisteredEmployerController@destroy')->name('unregistered_employer.undo');
//
//            /* Mark as registered */
//            Route::get('unregistered_employer/mark_registered/{unregistered_employer}', 'UnregisteredEmployerController@markRegistered')->name('unregistered_employer.mark_registered');
//
//
//            /* Upload Bulk Unregistered */
//            Route::get('unregistered_employer/upload_bulk', 'UnregisteredEmployerController@uploadBulk')->name('unregistered_employer.upload_bulk');
//            Route::post('unregistered_employer/store_uploaded_bulk', 'UnregisteredEmployerController@storeUploadedBulk')->name('unregistered_employer.store_uploaded_bulk');
//
//            /* Add new taxpayers to unregistered */
//            Route::get('unregistered_employer/add_tra', 'UnregisteredEmployerController@addTra')->name('unregistered_employer.add_tra');
//            Route::post('unregistered_employer/add_tra', 'UnregisteredEmployerController@addTraPost')->name('unregistered_employer.add_tra_post');
//            Route::post('unregistered_employer/get_tra', 'UnregisteredEmployerController@getTra')->name('unregistered_employer.get_tra');
//            Route::delete('unregistered_employer/remove_tra', 'UnregisteredEmployerController@removeTra')->name('unregistered_employer.remove_tra');
//
//
//            /* upload bulk Inactive*/
//            Route::get('unregistered_employer/upload_bulk_inactive', 'UnregisteredEmployerController@uploadBulkInActive')->name('unregistered_employer.upload_bulk_inactive');
//            Route::post('unregistered_employer/store_uploaded_bulk_inactive', 'UnregisteredEmployerController@storeUploadedBulkInactive')->name('unregistered_employer.store_uploaded_bulk_inactive');
//
//            /* Get unregistered unassigned employers */
//            Route::get('unregistered_employers', 'UnregisteredEmployerController@getUnregisteredUnassignedEmployers')->name('unregistered_unassigned_employers');
//
//            /**
//             * Follow ups Routes===========
//             */
//
////            get / index
//            Route::get('unregistered_employer/get_follow_ups/{unregistered_employer}', 'UnregisteredEmployerController@getFollowUpsForDataTable')->name('unregistered_employer.follow_up_get');
//
//            //create
//            Route::get('unregistered_employer/follow_up/{unregistered_employer}/create', 'UnregisteredEmployerController@createFollowUp')->name('unregistered_employer.follow_up_create');
////            store
//            Route::post('unregistered_employer/store_follow_up/{unregistered_employer}', 'UnregisteredEmployerController@storeFollowUp')->name('unregistered_employer.follow_up_store');
////            edit
//            Route::get('unregistered_employer/follow_up/{unregistered_employer}/edit', 'UnregisteredEmployerController@editFollowUp')->name('unregistered_employer.follow_up_edit');
////            update
//            Route::put('unregistered_employer/follow_up_update/{unregistered_employer_follow_up}', 'UnregisteredEmployerController@updateFollowUp')->name('unregistered_employer.follow_up_update');
////          delete
//            Route::delete('unregistered_employer/delete_follow_up/{unregistered_employer_follow_up}', 'UnregisteredEmployerController@deleteFollowUp')->name('unregistered_employer.follow_up_delete');
//
//            /*------End of Follow ups-----------*/
//
//
//
//            /* Staff Assignment----------- */
//
////            Self staff assign
//            Route::get('unregistered_employer/self_assign_staff/{unregistered_employer}', 'UnregisteredEmployerController@selfAssignStaff')->name('unregistered_employer.self_assign_staff');
//            /**Assign staff by supervisor**/
//            Route::get('unregistered_employer/assign_staff_page/{unregistered_employer}', 'UnregisteredEmployerController@assignStaffPage')->name('unregistered_employer.assign_staff_page');
//            Route::put('unregistered_employer/assign_staff/{unregistered_employer}', 'UnregisteredEmployerController@assignStaff')->name('unregistered_employer.assign_staff');
//            /**Change assigned staff **/
//            Route::get('unregistered_employer/edit_assigned_staff/{unregistered_employer}', 'UnregisteredEmployerController@editAssignedStaff')->name('unregistered_employer.assigned_staff_edit');
//            Route::put('unregistered_employer/update_assigned_staff/{unregistered_employer}', 'UnregisteredEmployerController@updateAssignedStaff')->name('unregistered_employer.assigned_staff_update');
//            /* end of staff assignment ---------------*/



            /*------End of UnRegistered  Employer Routes-------------------------*/

//            //ADES service routes-----------------------------------------------------
//            Route::get('status', 'EmployerController@employerStatus')->name('status');
//
//            Route::post('status', 'EmployerController@employerStatusShow')->name('show.status');
//
//            Route::get('payesdl', 'EmployerController@payesdl')->name('payesdl');
//
//            Route::post('payesdl', 'EmployerController@payesdlShow')->name('show.payesdl');
//
//            Route::get('newemployer', 'EmployerController@newEmployer')->name('newemployer');
//
//            Route::post('newemployer', 'EmployerController@newEmployerShow')->name('show.newemployer');
//
//            Route::get('closed', 'EmployerController@closedBusiness')->name('closed');
//
//            Route::post('closed', 'EmployerController@closedBusinessShow')->name('show.closed');
//
//            Route::get('LargeContributor','EmployerController@viewLargeContributor')->name('LargeContributor');
//
//             Route::get('LargeContributorCertificate','EmployerController@viewLargeContributorCertificates')->name('LargeContributorCertificate');
//
//
//
//
//            //End of ADES service routes========================================
//
////        EMPLOYERS ==================================
//            Route::resource('employer', 'EmployerController', ['except' => ['show']]);
//            Route::get('employer/profile/{employer}', 'EmployerController@profile')->name('employer.profile');
//            Route::get('employer/merge', 'EmployerController@mergeEmployers')->name('employer.merge');
//            Route::put('employer/merge/post', 'EmployerController@postMergeEmployers')->name('employer.post_merge');
//            Route::get('employer/unmerge', 'EmployerController@unMergeEmployers')->name('employer.unmerge');
//            Route::put('employer/unmerge/post', 'EmployerController@postUnMergeEmployers')->name('employer.post_unmerge');
//            Route::get('employer/{employer}/verification', 'EmployerController@onlineVerification')->name('employer.verification');
//            Route::get('employer/{verification}/verification_profile', 'EmployerController@onlineVerificationProfile')->name('employer.verification_profile');
//            Route::get('employer/verification/{verification}/documents_library', 'EmployerController@showVerificationDocumentLibrary')->name('employer.verification_document_library');
//            Route::get('employer/verification/{verification}/{documentId}/download_document', 'EmployerController@downloadVerificationDocument')->name('employer.verification_download_document');
//            Route::get('employer/verification/{verification}/{documentId}/open_document', 'EmployerController@openVerificationDocument')->name('employer.verification_open_document');
//            Route::get('get', 'EmployerController@get')->name('employer.get');
//            Route::get('get_bookings/{employer}', 'EmployerController@getBookingsForDataTable')->name('employer.bookings');
//            Route::get('get_contributions/{employer}', 'EmployerController@getContributionsForDataTable')->name('employer.contributions');
//            Route::get('get_legacy_contributions/{employer}', 'EmployerController@getLegacyContributionsForDataTable')->name('employer.legacy_contributions');
//            Route::get('get_interests/{employer}', 'EmployerController@getInterestsForDataTable')->name('employer.interests');
//            Route::get('get_inactive_receipts/{employer}', 'EmployerController@getInactiveReceiptsForDataTable')->name('employer.inactive_receipts');
//            Route::get('employers', 'EmployerController@getRegisterEmployers')->name('employers');
//            Route::post('employers_tin', 'EmployerController@getRegisterEmployersTin')->name('employers.tin');
//            Route::post('stored_employers', 'EmployerController@getAllStoredEmployers')->name('stored_employers');
//            /*get beneficiary employees only */
//            Route::get('beneficiary_employers', 'EmployerController@getBeneficiaryEmployers')->name('beneficiary_employers');
//            /* All employers without global scope*/
//            Route::get('all_employers', 'EmployerController@getAllEmployers')->name('all_employers');
//            /* Employee Counts */
//            Route::get('get_employee_count/{employer}', 'EmployerController@getEmployeeCountsForDataTable')->name('employer.employee_count_get');
//            /*  Employer Interest selected */
//            Route::get('employer/profile/interest/{booking_interest}', 'EmployerController@viewSelectedInterest')->name('employer.interest_profile');
//            /* Get All employees for this employer */
//            Route::get('employer/employees/{employer}', 'EmployerController@getRegisteredEmployees')->name('employer.employees');
//
//            /* Start : Employer Closures */
//            Route::get('employer/{employer}/close', 'EmployerController@getCloseBusiness')->name('employer.close_business');
//            Route::put('employer/{employer}/close', 'EmployerController@postCloseBusiness')->name('employer.close_business.post');
//            Route::get('closed_business', 'EmployerController@getClosedBusinessTRA')->name('closed_business');
//            Route::post('closed_business/get', 'EmployerController@getClosedBusinessTRAForDatatable')->name('closed_business.datatable');
//            Route::get('closed_business/{closed_business}/show', 'EmployerController@showClosedBusinessTRA')->name('closed_business.show');
//            Route::post('closed_business/{closed_business}/associate', 'EmployerController@associateClosedBusinessTRA')->name('closed_business.associate');
//            Route::delete('closed_business/remove', 'EmployerController@deleteClosedBusinessTRA')->name('closed_business.delete');
//            Route::get('employer/{employer}/open', 'EmployerController@getOpenBusiness')->name('employer.open_business');
//            Route::post('employer/{employer}/open', 'EmployerController@postOpenBusiness')->name('employer.open_business.post');
//            //Pending, querying all business closures of a particular employer
//
//            /* End : Employer Closures */
//
//
//            /*
//             * ADJUST EMPLOYER INTEREST SELECTED
//             */
//            Route::get('employer/profile/interest_adjust/{booking_interest}', 'EmployerController@editInterest')->name('employer.adjust_interest');
//            Route::put('employer/profile/interest_adjust/{booking_interest}', 'EmployerController@adjustInterest')->name('employer.adjust_interest');
//
//
//            /* Employer with Interest Overpayment Report*/
//
//            Route::get('employer/interest_overpayment', 'EmployerController@getEmployerInterestOverpayment')->name('employer.interest_overpayment');
//
//
//
//            /*END interest Refund---*/
//
//
//            /*
//             * WRITE OFF EMPLOYER INTEREST
//             */
//            Route::get('employer/profile/interest_write_off/{employer}', 'EmployerController@writtingOffInterestPage')->name('employer.write_off_interest_page');
//            Route::put('employer/profile/interest_write_off/{employer}', 'EmployerController@writeOffInterest')->name('employer.write_off_interest');
//// route page to approve interest written off
//            Route::get('employer/profile/interest_write_off_overview/{employer}', 'EmployerController@getInterestWrittenOffOverview')->name('employer.write_off_interest_overview');
//            Route::get('employer/profile/interest_write_off_approve/{interest_write_off}', 'EmployerController@ApproveInterestWrittenOff')->name('employer.write_off_interest_approve');
//            Route::get('employer/profile/interests_written_off/{interest_write_off}', 'EmployerController@getInterestsWrittenOffForDataTable')->name('employer.written_off_interests');
//            //Modify/ edit interest written offs
//            Route::get('employer/profile/interest_write_off_edit/{interest_write_off}', 'EmployerController@writtenOffInterestsEdit')->name('employer.written_off_interest_edit');
//            Route::put('employer/profile/interest_write_off_update/{interest_write_off}', 'EmployerController@writtenOffInterestsUpdate')->name('employer.write_off_interest_update');
////            Interest adjust overview
//            Route::get('employer/profile/interest_adjust_overview/{employer}', 'EmployerController@getInterestAdjustedOverview')->name('employer.adjust_interest_overview');
//            /*
//             * CONTRIBUTION
//             */
//            //Form to search receipt by rctno from compliance menu
//            Route::get('receipt', 'ContributionController@index')->name('receipt.search');
//            //Route::get('receipt/get', 'ContributionTableController')->name('receipt.get');
//
//
//            /**
//             *   CONTRIBUTION TRACKS
//             */
//            Route::resource('contribution_track', 'ContributionTrackController', ['except' => ['show']]);
//// create new track
//            Route::get('contribution_track/{receipt_code}/create', 'ContributionTrackController@create_new')->name('contribution_track.create_new');
//
//
//            /**
//             * BOOKING PROFILE=====================
//             */
////         Employer booking selected
//            Route::get('employer/profile/booking/{booking}', 'EmployerController@bookingProfile')->name('employer.booking_profile');
//// open booking adjustment page
//            Route::get('employer/profile/booking_adjust/{booking}', 'EmployerController@bookingAdjustPage')->name('employer.booking_adjust');
//// open booking adjustment page
//            Route::put('employer/profile/booking_adjust/{booking}', 'EmployerController@bookingAdjust')->name('employer.booking_adjust');            //END --Booking-profile =--------------
//
//
//
//
//            /**
//             * BOOKING INTEREST REFUND ROUTES============start=========
//             */
//
//            Route::resource('booking_interest', 'BookingInterestController', ['except' => ['show']]);
//
//
//            Route::get('booking_interest/refund_overview/{employer}', 'BookingInterestController@interestRefund')->name('booking_interest.refund_overview');
//
//
//            Route::get('booking_interest/refund_page/{employer}', 'BookingInterestController@refundPage')->name('booking_interest.refunding_page');
//
//
//            Route::get('booking_interest/interest_refund_overview/{employer}', 'BookingInterestController@getInterestRefundOverviewForDataTable')->name('booking_interest.get_refunds');
//
//            Route::post('booking_interest/get_eligible_for_refund/{employer}', 'BookingInterestController@getInterestEligibleForRefundingForDataTable')->name('booking_interest.get_eligible_for_refund');
//            /*Refund selected*/
//            Route::post('booking_interest/refund_selected', 'BookingInterestController@refundSelected')->name('booking_interest.refund_selected');
//
//            /*Interest refund profile*/
//            Route::get('booking_interest/refund_profile/{interest_refund}', 'BookingInterestController@interestRefundProfile')->name('booking_interest.refund_profile');
//
//            /*Interest refunded*/
//            Route::get('booking_interest/interest_refunded/{interest_refund}', 'BookingInterestController@getInterestRefundedForDataTable')->name('booking_interest.get_interest_refunded');
//
//            /*Update Bank details*/
//            Route::put('booking_interest/interest_refund/update_bank_details/{interest_refund}', 'BookingInterestController@updateBankDetails')->name('booking_interest.refund.update_bank_details');
//
//            /*Get Interest for modifying interest refund*/
//            Route::post('booking_interest/get_eligible_for_modifying_refund/{employer}/{interest_refund}', 'BookingInterestController@getInterestEligibleForModifyRefundForDataTable')->name('booking_interest.get_eligible_for_modifying_refund');
//
//            /*Modify interest refund*/
//            Route::get('booking_interest/refund/edit/{interest_refund}', 'BookingInterestController@modifyInterestRefund')->name('booking_interest.refund.edit');
//
//            /*Add interest refund*/
//            Route::get('booking_interest/interest_refund/add_interest/{booking_interest}/{interest_refund}', 'BookingInterestController@addInterestRefund')->name('booking_interest.refund.add_interest');
//            /*Remove interest from refund batch*/
//            Route::get('booking_interest/interest_refund/remove_interest/{booking_interest}/{interest_refund}', 'BookingInterestController@removeInterestRefund')->name('booking_interest.refund.remove_interest');
//
//            /*Undo interest refund */
//            Route::get('booking_interest/interest_refund/undo/{interest_refund}', 'BookingInterestController@undoInterestRefund')->name('booking_interest.refund.undo');
//
//
//            /* Initiate / Approve Interest Refund*/
//            Route::get('booking_interest/interest_refund/initiate/{interest_refund}', 'BookingInterestController@initiateInterestRefund')->name('booking_interest.refund.initiate');
//
//            /* End of Booking Interest  REFUND Routes ----------end --------*/
//            /*------------ END EMPLOYER ROUTE--------------------------------*/
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//            /*============REGISTRATION EMPLOYERS ===========ROUTES ---------*/
//            Route::resource('employer_registration', 'EmployerRegistrationController', ['except' => ['show']]);
//
//            /*name check before adding new*/
//            Route::get('employer_registration/name_check', 'EmployerRegistrationController@nameCheck')->name('employer_registration.name_check');
//            /*get all employers (unregistered + registered)*/
//            Route::get('employer_registration/name_check/getAllEmployers', 'EmployerRegistrationController@getAllEmployers')->name('employer_registration.name_check.get_all_employer');
//            /*profile*/
//            Route::get('employer_registration/profile/{employer_registration}', 'EmployerRegistrationController@profile')->name('employer_registration.profile');
////           /* register Employer Information */
//            Route::post('employer_registration/register', 'EmployerRegistrationController@register')->name('employer_registration.register');
//
//            /*show Document Library*/
//            Route::get('employer_registration/documents_library/{employer_registration}', 'EmployerRegistrationController@showDocumentLibrary')->name('employer_registration.documents_library');
//            /*download document*/
//            Route::get('employer_registration/download_document/{employer_registration}/{documentId}', 'EmployerRegistrationController@downloadDocument')->name('employer_registration.download_document');
//            /*open document*/
//            Route::get('employer_registration/open_document/{employer_registration}/{documentId}', 'EmployerRegistrationController@openDocument')->name('employer_registration.open_document');
//            /* Download upload error */
//            Route::get('employer_registration/error/{employer_registration}', 'EmployerRegistrationController@downloadError')->name("employer_registration.download_error");
//
//            /*   Upload employee list / wcr3 */
//            Route::get('employer_registration/{employer}/upload_employee', 'EmployerRegistrationController@employeeListFile')->name('employer_registration.employee_list_file');
//            Route::post('employer_registration/{employer}/upload/store', 'EmployerRegistrationController@uploadEmployeeList')->name('employer_registration.upload_employee_list');
//            Route::get('employer_registration/{employer}/load/store', 'EmployerRegistrationController@loadEmployeeList')->name('employer_registration.load_employee_list');
//            /*Undo loaded employees*/
//            Route::get('employer_registration/{employer}/undo_loaded_employees', 'EmployerRegistrationController@undoLoadedEmployees')->name('employer_registration.undo_loaded_employees');
//            /* Upload progress */
//            Route::post('employer_registration/upload_progress/{employer}', 'EmployerRegistrationController@uploadProgress')->name("employer_registration.upload_progress");
//
//            //=============================================================== MAC GEPG
//            Route::get('return_employees/{employer}', 'EmployerRegistrationController@returnEmployees')->name('return_employees');
//            Route::get('generate_number/{employer}', 'EmployeeUploadController@generateNumber');
//            Route::resource('online_uploaded_employees', 'EmployeeUploadController');
//
//            //================================================== MAC - GEPG
//
//
//            /*Approve Registration*/
//            Route::get('employer_registration/approve/{employer_registration}', 'EmployerRegistrationController@approveRegistration')->name('employer_registration.approve');
//            /*Print Certificate of Registration*/
//            Route::get('employer_registration/print_certificate/{employer_registration}', 'EmployerRegistrationController@printCertificate')->name('employer_registration.print_certificate');
//            /* Reissue Certificate */
//            Route::get('employer_registration/reprint_certificate/{employer_registration}', 'EmployerRegistrationController@reprintCertificate')->name('employer_registration.reprint_certificate');
//            /*Print Issuance of Registration No*/
//            Route::get('employer_registration/print_issuance/{employer_registration}', 'EmployerRegistrationController@printIssuance')->name('employer_registration.print_issuance');
//
//            /*  Undo / Reject Registration */
//            Route::get('employer_registration/undo/{employer_registration}', 'EmployerRegistrationController@destroy')->name('employer_registration.undo');
//            /* Name clearance */
//            Route::get('employer_registration/get_name_clearance/{employer_registration}', 'EmployerRegistrationController@getNameClearance')->name("employer_registration.get_name_clearance");
//
//            /* ----------END OF REGISTRATION --------------------*/
//
//
//            /*============UNREGISTERED EMPLOYERS ===========ROUTES ---------*/
//            Route::resource('unregistered_employer', 'UnregisteredEmployerController', ['except' => ['show']]);
//
//            /* Profile */
//            Route::get('unregistered_employer/profile/{unregistered_employer}', 'UnregisteredEmployerController@profile')->name('unregistered_employer.profile');
//            /* Register / approve */
//            Route::get('unregistered_employer/register/{unregistered_employer}', 'UnregisteredEmployerController@register')->name('unregistered_employer.register');
//
//            /* Undo / delete */
//            Route::get('unregistered_employer/undo/{unregistered_employer}', 'UnregisteredEmployerController@destroy')->name('unregistered_employer.undo');
//
//            /* Mark as registered */
//            Route::get('unregistered_employer/mark_registered/{unregistered_employer}', 'UnregisteredEmployerController@markRegistered')->name('unregistered_employer.mark_registered');
//
//
//            /* Upload Bulk Unregistered */
//            Route::get('unregistered_employer/upload_bulk', 'UnregisteredEmployerController@uploadBulk')->name('unregistered_employer.upload_bulk');
//            Route::post('unregistered_employer/store_uploaded_bulk', 'UnregisteredEmployerController@storeUploadedBulk')->name('unregistered_employer.store_uploaded_bulk');
//
//            /* Add new taxpayers to unregistered */
//            Route::get('unregistered_employer/add_tra', 'UnregisteredEmployerController@addTra')->name('unregistered_employer.add_tra');
//            Route::post('unregistered_employer/add_tra', 'UnregisteredEmployerController@addTraPost')->name('unregistered_employer.add_tra_post');
//            Route::post('unregistered_employer/get_tra', 'UnregisteredEmployerController@getTra')->name('unregistered_employer.get_tra');
//            Route::delete('unregistered_employer/remove_tra', 'UnregisteredEmployerController@removeTra')->name('unregistered_employer.remove_tra');
//
//
//            /* upload bulk Inactive*/
//            Route::get('unregistered_employer/upload_bulk_inactive', 'UnregisteredEmployerController@uploadBulkInActive')->name('unregistered_employer.upload_bulk_inactive');
//            Route::post('unregistered_employer/store_uploaded_bulk_inactive', 'UnregisteredEmployerController@storeUploadedBulkInactive')->name('unregistered_employer.store_uploaded_bulk_inactive');
//
//            /* Get unregistered unassigned employers */
//            Route::get('unregistered_employers', 'UnregisteredEmployerController@getUnregisteredUnassignedEmployers')->name('unregistered_unassigned_employers');
//
//            /**
//             * Follow ups Routes===========
//             */
//
////            get / index
//            Route::get('unregistered_employer/get_follow_ups/{unregistered_employer}', 'UnregisteredEmployerController@getFollowUpsForDataTable')->name('unregistered_employer.follow_up_get');
//
//            //create
//            Route::get('unregistered_employer/follow_up/{unregistered_employer}/create', 'UnregisteredEmployerController@createFollowUp')->name('unregistered_employer.follow_up_create');
////            store
//            Route::post('unregistered_employer/store_follow_up/{unregistered_employer}', 'UnregisteredEmployerController@storeFollowUp')->name('unregistered_employer.follow_up_store');
////            edit
//            Route::get('unregistered_employer/follow_up/{unregistered_employer}/edit', 'UnregisteredEmployerController@editFollowUp')->name('unregistered_employer.follow_up_edit');
////            update
//            Route::put('unregistered_employer/follow_up_update/{unregistered_employer_follow_up}', 'UnregisteredEmployerController@updateFollowUp')->name('unregistered_employer.follow_up_update');
////          delete
//            Route::delete('unregistered_employer/delete_follow_up/{unregistered_employer_follow_up}', 'UnregisteredEmployerController@deleteFollowUp')->name('unregistered_employer.follow_up_delete');
//
//            /*------End of Follow ups-----------*/
//
//
//
//            /* Staff Assignment----------- */
//
////            Self staff assign
//            Route::get('unregistered_employer/self_assign_staff/{unregistered_employer}', 'UnregisteredEmployerController@selfAssignStaff')->name('unregistered_employer.self_assign_staff');
//            /**Assign staff by supervisor**/
//            Route::get('unregistered_employer/assign_staff_page/{unregistered_employer}', 'UnregisteredEmployerController@assignStaffPage')->name('unregistered_employer.assign_staff_page');
//            Route::put('unregistered_employer/assign_staff/{unregistered_employer}', 'UnregisteredEmployerController@assignStaff')->name('unregistered_employer.assign_staff');
//            /**Change assigned staff **/
//            Route::get('unregistered_employer/edit_assigned_staff/{unregistered_employer}', 'UnregisteredEmployerController@editAssignedStaff')->name('unregistered_employer.assigned_staff_edit');
//            Route::put('unregistered_employer/update_assigned_staff/{unregistered_employer}', 'UnregisteredEmployerController@updateAssignedStaff')->name('unregistered_employer.assigned_staff_update');
//            /* end of staff assignment ---------------*/
//
//
//
//            /*------End of UnRegistered  Employer Routes-------------------------*/
        });

});

});