<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member\Employer'], function() {


            Route::group(['prefix' => 'employer/change_particular', 'as' => 'employer.change_particular.'], function() {
                Route::get('/index/{employer}', 'EmployerParticularChangeController@index')->name('index');
                Route::get('/create/{employer}', 'EmployerParticularChangeController@create')->name('create');
                Route::post('/store', 'EmployerParticularChangeController@store')->name('store');
                Route::get('/edit/{employer_particular_change}', 'EmployerParticularChangeController@edit')->name('edit');
                Route::put('/update/{employer_particular_change}', 'EmployerParticularChangeController@update')->name('update');
                Route::get('/profile/{employer_particular_change}', 'EmployerParticularChangeController@profile')->name('profile');
                Route::get('/undo/{employer_particular_change}', 'EmployerParticularChangeController@undo')->name('undo');
                Route::post('/initiate_wf/{employer_particular_change}', 'EmployerParticularChangeController@initiateApproval')->name('initiate_wf');
                Route::post('/get_by_employer/{employer}', 'EmployerParticularChangeController@getByEmployerForDt')->name('get_by_employer');
                /*Documents routes*/
                Route::get('attach/document/{employer_particular_change}', 'EmployerParticularChangeController@attachDocument')->name('attach_document');
                Route::post('store/document', 'EmployerParticularChangeController@storeDocument')->name('store_document');
                Route::get('edit/document/{doc_pivot_id}', 'EmployerParticularChangeController@editDocument')->name('edit_document');
                Route::put('update_document', 'EmployerParticularChangeController@updateDocument')->name('update_document');
                Route::get('delete/document/{doc_pivot_id}', 'EmployerParticularChangeController@deleteDocument')->name('delete_document');
                Route::post('preview/document/{doc_pivot_id}', 'EmployerParticularChangeController@previewDocument')->name('preview_document');
            });

        });

    });

});