<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member\Employer'], function() {


            Route::group(['prefix' => 'employer_closure/extension', 'as' => 'employer_closure.extension.'], function() {


                Route::get('create/{employer_closure}', 'EmployerClosureExtensionController@create')->name('create');
                Route::post('store', 'EmployerClosureExtensionController@store')->name('store');
                Route::get('edit/{employer_closure_extension}', 'EmployerClosureExtensionController@edit')->name('edit');
                Route::put('update/{employer_closure_extension}', 'EmployerClosureExtensionController@update')->name('update');
                Route::get('/delete/{employer_closure_extension}','EmployerClosureExtensionController@delete')->name('delete');
                Route::get('/profile/{employer_closure_extension}','EmployerClosureExtensionController@profile')->name('profile');
                Route::get('/get_all_for_dt','EmployerClosureExtensionController@getAllForDt')->name('get_all_for_dt');
                Route::post('/initiate_approval/{employer_closure_extension}','EmployerClosureExtensionController@initiateApproval')->name('initiate_approval');


            });

        });

    });

});