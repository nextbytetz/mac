<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member\Employer'], function() {


            Route::group(['prefix' => 'employer_closure/open', 'as' => 'employer_closure.open.'], function() {

//                Route::get('index/{employer}', 'EmployerClosureReopenController@index')->name('index');
                Route::get('create/{employer}', 'EmployerClosureReopenController@create')->name('create');
                Route::post('store', 'EmployerClosureReopenController@store')->name('store');
                Route::get('edit/{employer_closure_reopen}', 'EmployerClosureReopenController@edit')->name('edit');
                Route::put('update/{employer_closure_reopen}', 'EmployerClosureReopenController@update')->name('update');
                Route::get('profile/{employer_closure_reopen}', 'EmployerClosureReopenController@profile')->name('profile');
                Route::get('undo/{employer_closure_reopen}', 'EmployerClosureReopenController@undo')->name('undo');
                Route::post('initiate_approval/{employer_closure_reopen}', 'EmployerClosureReopenController@initiateApproval')->name('initiate_approval');
                Route::get('attach_document/{employer_closure_reopen}', 'EmployerClosureReopenController@attachDocument')->name('attach_document');
//                Route::post('get_for_datatable/{employer}/employer', 'EmployerClosureOpenController@getClosuresByEmployerForDataTable')->name('get_for_datatable');


            });

        });

    });

});