<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        Route::group(['namespace' => 'Member'], function() {

            //contribution modification
            Route::group(['prefix' => 'employer/contrib_modification', 'as' => 'employer.contrib_modification.'], function() {
                Route::get('/index/{employer}', 'ContributionModificationController@index')->name('index');
                Route::get('/datatable/{employer}', 'ContributionModificationController@returnRequestsDatatable')->name('datatable');
                Route::get('/create/{employer}', 'ContributionModificationController@create')->name('create');
                Route::post('/store', 'ContributionModificationController@store')->name('store');
                Route::get('/profile/{request_id}', 'ContributionModificationController@profile')->name('profile');
                Route::get('/return_rct/{employer}', 'ContributionModificationController@returnAllReceiptNo')->name('return_rct');
                Route::get('/rct_details/{rct_id}/{rct_type}', 'ContributionModificationController@returnReceiptDetails')->name('rct_details');
                Route::get('/form_fields/{rct_id}/{type}/{rct_type}', 'ContributionModificationController@formFields')->name('form_fields');
                Route::get('/add_form_fields/{number_of_fields}', 'ContributionModificationController@addRedisitributionFields')->name('add_form_fields');
                Route::get('/edit/{request_id}', 'ContributionModificationController@edit')->name('edit');
                Route::put('/update/{request_id}', 'ContributionModificationController@update')->name('update');
                Route::get('/submit_for_review/{request_id}', 'ContributionModificationController@submitForReview')->name('submit_for_review');
                Route::post('/attachment_preview/{modification_id}', 'ContributionModificationController@previewAttachment')->name('attachment_preview');
                Route::get('/return_request_fields/{request_id}', 'ContributionModificationController@returnEditformFields')->name('return_request_fields');
                Route::put('/update_request/{request_id}', 'ContributionModificationController@update')->name('update_request');
                Route::post('/cancel_request', 'ContributionModificationController@cancelRequest')->name('cancel_request');
            });


            //contribution interest rate
            Route::group(['prefix' => 'employer/rates', 'as' => 'employer.rates.'], function() {
                Route::get('/index', 'ContributionInterestRateController@index')->name('index');
                Route::get('/create', 'ContributionInterestRateController@create')->name('create');
                Route::get('/datatable', 'ContributionInterestRateController@dataTable')->name('datatable');
                Route::get('/profile/{rate_request_id}', 'ContributionInterestRateController@profile')->name('profile');
                Route::post('/post', 'ContributionInterestRateController@post')->name('post');
                Route::get('/current_rate/{fin_code_id}/{employer_cat}', 'ContributionInterestRateController@returnCurrentRate')->name('current_rate');
                Route::post('/preview_attachment/{rate_request_id}', 'ContributionInterestRateController@previewRateAttachment')->name('preview_attachment');
                Route::get('/submit_for_approval/{request_id}', 'ContributionInterestRateController@submitForApproval')->name('submit_for_approval');
                Route::put('/update/{request_id}', 'ContributionInterestRateController@update')->name('update');

            });

        });

    });

});