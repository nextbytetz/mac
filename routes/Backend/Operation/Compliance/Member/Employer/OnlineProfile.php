<?php
/**
/**
 * Operation
 */

Route::group([
	'namespace' => 'Operation',
], function() {

	Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

	// MEMBERS GROUP
		Route::group(['namespace' => 'Member','prefix' => 'online_profile',  'as' => 'online_profile.'], function() {

			Route::get('users', 'OnlineProfileController@getAllUsers')->name('users');
			Route::get('get_payroll/{employer_id}', 'OnlineProfileController@getPayrollId')->name('get_payroll');
			Route::post('add_user/{employer_id}', 'OnlineProfileController@addUser')->name('add_user');
			Route::get('user_details/{user_id}', 'OnlineProfileController@getUserDetails')->name('user_details');


			//to be activated
			/*Route::get('profile/{employer}', 'OnlineProfileController@index')->name('profile');
			Route::get('get_arrears/{employer}', 'OnlineProfileController@getContributionsArrears')->name('get_arrears');
			Route::get('add_arrears_form/{employer}', 'OnlineProfileController@ContributionsArrearsform')->name('add_arrears_form');
			Route::post('add_arrears/{employer}', 'OnlineProfileController@addContributionsArrears')->name('add_arrears');
			Route::post('clear_arrears/{arrear}', 'OnlineProfileController@clearArrears')->name('clear_arrears');
			Route::post('upload_arrear/{arrear}', 'OnlineProfileController@uploadEmployeesArrear')->name('upload_arrear');

			Route::get('users/{employer}', 'OnlineProfileController@users')->name('users');
			Route::get('return_user/{user_id}', 'OnlineProfileController@returnUser')->name('return_user');
			Route::post('reset_password', 'OnlineProfileController@resetPassword')->name('reset_password');
			Route::get('change_user_account/{user_id}', 'OnlineProfileController@changeAccountType')->name('change_user_account');

			Route::get('deactivate_user_form/{user_id}/{employer}','OnlineProfileController@deactivateUserform')->name('deactivate_user_form');
			Route::post('deactivate_user/{user_id}/{employer}','OnlineProfileController@deactivateOnlineEmployerUser')->name('deactivate_user');*/

		});

	});

});
