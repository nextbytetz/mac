<?php
/**
/**
 * Operation
 */

Route::group([
    'namespace' => 'Operation',
], function() {

    Route::group(['namespace' => 'Compliance\Member', 'prefix' => 'compliance/interest_adjustment',  'as' => 'compliance.interest_adjustment.'], function() {

        Route::get('create/{receipt}', 'InterestAdjustmentController@create')->name('create');
        Route::post('store', 'InterestAdjustmentController@store')->name('store');
        Route::get('edit/{interest_adjustment}', 'InterestAdjustmentController@edit')->name('edit');
        Route::put('update/{interest_adjustment}', 'InterestAdjustmentController@update')->name('update');
        Route::get('profile/{interest_adjustment}', 'InterestAdjustmentController@profile')->name('profile');
        Route::post('initiate_approval/{interest_adjustment}', 'InterestAdjustmentController@initiateApproval')->name('initiate_approval');
        Route::get('undo/{interest_adjustment}', 'InterestAdjustmentController@undo')->name('undo');
        Route::post('get_interests_adjusted_dt/{interest_adjustment}', 'InterestAdjustmentController@getInterestAdjustedForDt')->name('get_interests_adjusted_dt');

    });

});
