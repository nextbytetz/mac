<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member\Employer'], function() {


            Route::group(['prefix' => 'employer/staff_relation', 'as' => 'employer.staff_relation.'], function() {

                Route::get('index', 'StaffEmployerController@index')->name('index');
                Route::get('configuration', 'StaffEmployerController@configuration')->name('configuration');
                Route::put('update_configuration', 'StaffEmployerController@updateConfiguration')->name('update_configuration');
                Route::get('create_allocation', 'StaffEmployerController@createAllocationNonLarge')->name('create_allocation');
                Route::post('store_allocation', 'StaffEmployerController@storeAllocationNonLarge')->name('store_allocation');
                Route::get('create_allocation_large', 'StaffEmployerController@createAllocationLarge')->name('create_allocation_large');
                Route::post('store_allocation_large', 'StaffEmployerController@storeAllocationLarge')->name('store_allocation_large');
                Route::get('create_allocation_bonus', 'StaffEmployerController@createAllocationBonus')->name('create_allocation_bonus');
                Route::post('store_allocation_bonus', 'StaffEmployerController@storeAllocationBonus')->name('store_allocation_bonus');
                Route::get('create_allocation_treasury', 'StaffEmployerController@createAllocationTreasury')->name('create_allocation_treasury');
                Route::post('store_allocation_treasury', 'StaffEmployerController@storeAllocationTreasury')->name('store_allocation_treasury');
                Route::get('create_allocation_intern', 'StaffEmployerController@createAllocationIntern')->name('create_allocation_intern');
                Route::post('store_allocation_intern', 'StaffEmployerController@storeAllocationIntern')->name('store_allocation_intern');
                Route::get('intern_allocation/upload_excel_page/{staff_employer_allocation}', 'StaffEmployerController@openUploadInternAssignmentPage')->name('upload_excel_page_intern');
                Route::post('intern_allocation/upload_excel', 'StaffEmployerController@storeUploadInternAssignment')->name('upload_excel_intern');
                Route::get('edit_allocation/{staff_employer_allocation}', 'StaffEmployerController@editAllocation')->name('edit_allocation');
                Route::put('update_allocation/{staff_employer_allocation}', 'StaffEmployerController@updateAllocation')->name('update_allocation');
                Route::get('allocations', 'StaffEmployerController@allocationsList')->name('allocations');
                Route::get('remove_staff_from_allocation', 'StaffEmployerController@removeStaffFromAllocation')->name('remove_staff_from_allocation');
                                Route::post('get_allocations_for_datatable', 'StaffEmployerController@getStaffEmployerAllocationsForDataTable')->name('get_allocations_for_datatable');
                Route::get('allocation_profile/{staff_employer_allocation}', 'StaffEmployerController@allocationProfile')->name('allocation_profile');
                Route::get('approve_allocation/{staff_employer_allocation}', 'StaffEmployerController@approveAllocation')->name('approve_allocation');
                Route::get('complete_allocation/{staff_employer_allocation}', 'StaffEmployerController@completeAllocation')->name('complete_allocation');
                Route::get('undo_allocation/{staff_employer_allocation}', 'StaffEmployerController@undoAllocation')->name('undo_allocation');
                Route::get('revoke_allocation/{staff_employer_allocation}', 'StaffEmployerController@revokeAllocation')->name('revoke_allocation');
                Route::post('get_staff_allocated_overview_dt/{staff_employer_allocation}', 'StaffEmployerController@getStaffAllocatedByAllocationForDataTable')->name('get_staff_allocated_overview_dt');
                Route::get('open_allocate_new_staff_page/{staff_employer_allocation}', 'StaffEmployerController@openAllocateNewStaffPage')->name('open_allocate_new_staff_page');
                Route::post('allocate_new_staff', 'StaffEmployerController@allocateNewStaff')->name('allocate_new_staff');
                Route::get('replace_small_employers/{staff_employer_allocation}', 'StaffEmployerController@replaceSmallMediumEmployers')->name('replace_small_employers');
                Route::get('allocate_medium_for_large_staff/{staff_employer_allocation}', 'StaffEmployerController@allocateMediumEmployersForLargeStaff')->name('allocate_medium_for_large_staff');
                Route::get('reallocate_bonus_for_unassigned_staff/{staff_employer_allocation}', 'StaffEmployerController@reallocateBonusEmployersForUnassignedStaff')->name('reallocate_bonus_for_unassigned_staff');
                /*Allocate regions*/
                Route::get('create_allocation_staff_region', 'StaffEmployerController@createAllocationStaffRegion')->name('create_allocation_staff_region');
                Route::post('store_allocation_staff_region', 'StaffEmployerController@storeAllocationStaffRegion')->name('store_allocation_staff_region');
                /*Reallocate*/
                Route::get('reallocation_page', 'StaffEmployerController@reallocationPage')->name('reallocation_page');
                Route::put('reallocate', 'StaffEmployerController@reallocate')->name('reallocate');
                Route::put('get_allocations_for_reallocation_dt', 'StaffEmployerController@getStaffAllocationsForReallocateForDt')->name('get_allocations_for_reallocation_dt');
                /*Replace*/
                Route::get('replace_page', 'StaffEmployerController@replacePage')->name('replace_page');
                Route::put('replace_employers', 'StaffEmployerController@replaceEmployers')->name('replace_employers');
                Route::put('remove_employers', 'StaffEmployerController@removeEmployers')->name('remove_employers');
                Route::put('get_allocations_for_replacement_dt', 'StaffEmployerController@getStaffAllocationsForReplacementForDt')->name('get_allocations_for_replacement_dt');
                /*My employers routes*/
                Route::get('my_employers_for_active_allocations/{user}/{isbonus}/{contact}', 'StaffEmployerController@openMyEmployersForActiveAllocationPage')->name('my_employers_for_active_allocations');
                Route::get('my_employers_for_general/{user}/{dt_script}', 'StaffEmployerController@openMyEmployersForGeneralPage')->name('my_employers_for_general');
                Route::post('get_employers_for_active_allocations/{user}/{isbonus}/{contact}', 'StaffEmployerController@getEmployersAllocatedByStaffForActiveAllocationDt')->name('get_employers_for_active_allocations');
                Route::post('get_employers_for_active_allocations_by_category/{user}/{category}/{contact}', 'StaffEmployerController@getEmployersAllocatedByStaffForActiveAllocationByCategoryDt')->name('get_employers_for_active_allocations_by_category');
                Route::get('all_employers_for_active_allocations', 'StaffEmployerController@openAllEmployersPage')->name('all_employers_for_active_allocations');
                Route::post('get_all_employers_for_active_allocations', 'StaffEmployerController@getAllEmployersAllocatedForDt')->name('get_all_employers_for_active_allocations');
                Route::post('get_employers_for_general/{user}/{dt_script}', 'StaffEmployerController@getEmployersAllocatedByStaffForGeneralDt')->name('get_employers_for_general');
                Route::get('export_all_allocated_employers', 'StaffEmployerController@exportAllAllocatedEmployers')->name('export_all_allocated_employers');
                /*end my employer*/
                Route::get('staff_employer_profile/{staff_employer}', 'StaffEmployerController@staffEmployerProfile')->name('staff_employer_profile');
                Route::get('staff_employer/update_attendance_status/{staff_employer}/{status}', 'StaffEmployerController@updateAttendanceStatus')->name('update_attendance_status');
                Route::post('preview/document/{doc_pivot_id}', 'StaffEmployerController@previewDocument')->name('preview_document');
                /*Export*/
                Route::get('export_employers_allocated/{user_id}/{isbonus}/{contact}/{dt_script_id}', 'StaffEmployerController@exportToExcelMyEmployers')->name('export_employers_allocated');
                Route::get('export_employer_follow_ups/{staff_employer}', 'StaffEmployerController@exportEmployerFollowUps')->name('export_employer_follow_ups');

                /*follow up*/
                Route::get('follow_up/create/{staff_employer}', 'StaffEmployerController@createFollowUp')->name('create_follow_up');
                Route::get('follow_up/create_follow_up_general/{employer}', 'StaffEmployerController@createGeneralFollowUp')->name('create_follow_up_general');
                Route::post('follow_up/store', 'StaffEmployerController@storeFollowUp')->name('store_follow_up');
                Route::post('follow_up/store_multiple', 'StaffEmployerController@storeFollowUpMultiple')->name('store_multiple');
                Route::get('follow_up/edit/{follow_up}', 'StaffEmployerController@editFollowUp')->name('edit_follow_up');
                Route::put('follow_up/update', 'StaffEmployerController@updateFollowUp')->name('update_follow_up');
                Route::post('follow_up/get_for_datatable/{staff_employer}', 'StaffEmployerController@getStaffEmployerFollowupsForDt')->name('get_follow_up_for_datatable');
                Route::post('follow_up/get_by_employer_for_datatable/{employer}', 'StaffEmployerController@getStaffEmployerFollowupsByEmployerForDt')->name('get_by_employer_for_datatable');
                Route::get('follow_up/delete/{follow_up}', 'StaffEmployerController@deleteFollowUp')->name('delete_follow_up');
                Route::get('follow_up/pending_for_review', 'StaffEmployerController@pendingFollowUpsForReview')->name('pending_follow_ups_for_review');
                Route::post('follow_up/get_for_review_datatable', 'StaffEmployerController@getPendingFollowUpsForReviewForDt')->name('get_for_review_datatable');
                Route::post('follow_up/get_my_for_review_datatable', 'StaffEmployerController@getMyPendingFollowUpsForReviewForDt')->name('get_my_for_review_datatable');
                Route::post('follow_up/get_my_for_correction_datatable', 'StaffEmployerController@getMyPendingFollowUpsForCorrectionForDt')->name('get_my_for_correction_datatable');
                Route::get('follow_up/assigned_pending_for_review', 'StaffEmployerController@assignedPendingFollowUpForReviewPage')->name('assigned_pending_for_review');
                Route::get('follow_up/reversed_for_correction', 'StaffEmployerController@reversedPendingFollowUpsForCorrectionPage')->name('reversed_for_correction');
                Route::get('follow_up/review_page/{follow_up}', 'StaffEmployerController@openReviewFollowUpPage')->name('review_follow_up');
                Route::put('follow_up/store_review/{follow_up}', 'StaffEmployerController@storeFollowUpReview')->name('store_follow_up_review');
                Route::get('follow_up/add_multiple_page', 'StaffEmployerController@addMultipleFollowups')->name('add_multiple_page');
                Route::get('get_employers_by_user_select', 'StaffEmployerController@getStaffEmployersByUserForSelect')->name('get_employers_by_user_select');
                Route::get('follow_up/load_existing_follow_ups_user', 'StaffEmployerController@loadFollowupsContentByUserDate')->name('load_existing_follow_ups_user');

                /*followup staff reminders*/
                Route::get('follow_up/staff_missing_target/index', 'StaffEmployerController@staffMissingFollowupTargetReminderPage')->name('followup.staff_missing_target');
                Route::post('follow_up/staff_missing_target/get_list_reminders', 'StaffEmployerController@getUserMissingTargetRemindersForDt')->name('followup.staff_missing_target.get_list_reminders');
                Route::get('follow_up/staff_missing_target/export_excel', 'StaffEmployerController@exportMissingFollowupReminders')->name('followup.staff_missing_target.export_excel');

                /*Dashboard*/
                Route::get('performance_dashboard/contributing_employers', 'StaffEmployerController@staffEmployerPerformanceDashboard')->name('performance_dashboard');
                Route::get('performance_intern_dashboard/contributing_employers', 'StaffEmployerController@staffEmployerPerformanceInternDashboard')->name('performance_intern_dashboard');
                Route::post('get_performance_for_datatable', 'StaffEmployerController@getStaffEmployerPerformanceForDt')->name('get_performance_for_datatable');
                Route::get('performance_dashboard/bonus_employers', 'StaffEmployerController@staffEmployerBonusPerformanceDashboard')->name('performance_dashboard.bonus_employers');
                Route::get('performance_dashboard/treasury_employers', 'StaffEmployerController@staffEmployerTreasuryPerformanceDashboard')->name('performance_dashboard.treasury_employers');

                Route::get('open_test_simulation', 'StaffEmployerController@openTestSimulation')->name('open_test_simulation');
                Route::get('test_simulation/{type}', 'StaffEmployerController@testSimulation')->name('test_simulation');
            });

        });

    });

});