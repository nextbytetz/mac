<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member\Employer'], function() {


            Route::group(['prefix' => 'employer/closure', 'as' => 'employer.closure.'], function() {

                Route::get('index/{employer}', 'EmployerClosureController@index')->name('index');
                Route::get('create/{employer}', 'EmployerClosureController@create')->name('create');
                Route::post('store', 'EmployerClosureController@store')->name('store');
                Route::get('edit/{employer_closure}', 'EmployerClosureController@edit')->name('edit');
                Route::put('update/{employer_closure}', 'EmployerClosureController@update')->name('update');
                Route::get('profile/{employer_closure}', 'EmployerClosureController@profile')->name('profile');
                Route::get('undo/{employer_closure}', 'EmployerClosureController@undo')->name('undo');
                Route::post('initiate_approval/{employer_closure}', 'EmployerClosureController@initiateApproval')->name('initiate_approval');
                Route::post('get_for_datatable/{employer}/employer', 'EmployerClosureController@getClosuresByEmployerForDataTable')->name('get_for_datatable');

                Route::post('reverse_online/{employer_closure}', 'EmployerClosureController@reverseOnline')->name('reverse_online');


                /*Portal arrears contrib*/
                Route::get('open_page_confirm_payrolls/{employer_closure}', 'EmployerClosureController@openPageToConfirmEmployerPayroll')->name('open_page_confirm_payrolls');
                Route::put('confirm_payrolls/{employer_closure}', 'EmployerClosureController@confirmEmployerPayrollsForThisClosure')->name('confirm_payrolls');

          //status followup
                Route::post('get_tempo_closed_followup_for_dt', 'EmployerClosureController@getTemporaryClosedForFollowStatusForDt')->name('get_tempo_closed_followup_for_dt');
                Route::get('tempo_closure_status_followup_page', 'EmployerClosureController@tempoClosureStatusFollowUpsPage')->name('tempo_closure_status_followup_page');
                Route::get('fill_status_followup/{employer_closure}', 'EmployerClosureController@fillStatusFollowUp')->name('fill_status_followup');
                Route::put('store_status_followup/{employer_closure}', 'EmployerClosureController@storeStatusFollowUp')->name('store_status_followup');

                /*Documents routes*/
                Route::get('attach/document/{employer_closure}', 'EmployerClosureController@attachDocument')->name('attach_document');
                Route::post('store/document', 'EmployerClosureController@storeDocument')->name('store_document');
                Route::get('attach_wcp1/document/{employer_closure}', 'EmployerClosureController@attachWcp1Document')->name('attach_wcp1_document');
                Route::get('edit/document/{doc_pivot_id}', 'EmployerClosureController@editDocument')->name('edit_document');
                Route::put('update_document', 'EmployerClosureController@updateDocument')->name('update_document');
                Route::get('delete/document/{doc_pivot_id}', 'EmployerClosureController@deleteDocument')->name('delete_document');
                Route::post('preview/document/{doc_pivot_id}', 'EmployerClosureController@previewDocument')->name('preview_document');

                Route::post('preview/follow_up_document/{doc_pivot_id}', 'EmployerClosureController@previewFollowUpDocument')->name('preview_follow_up_document');
                /*FOLLOW UP ROUTES*/
                Route::post('get_follow_ups_for_datatable/{employer_closure}', 'EmployerClosureController@getFollowUpsByClosureForDataTable')->name('get_follow_ups_for_datatable');
                Route::get('follow_up/create/{employer_closure}', 'EmployerClosureController@createFollowUp')->name('follow_up.create');
                Route::post('follow_up/store', 'EmployerClosureController@storeFollowUp')->name('follow_up.store');
                Route::get('follow_up/edit/{employer_closure_follow_up}', 'EmployerClosureController@editFollowUp')->name('follow_up.edit');
                Route::put('follow_up/update', 'EmployerClosureController@updateFollowUp')->name('follow_up.update');
                Route::get('follow_up/delete/{employer_closure_follow_up}', 'EmployerClosureController@deleteFollowUp')->name('follow_up.delete');

                /*eoffice integration*/
                Route::get('eoffice_alerts', 'EmployerClosureController@eofficeAlertsListingPage')->name('eoffice_alerts');
                Route::post('get_pending_eoffice_for_datatable', 'EmployerClosureController@getPendingClosuresFromEofficeForDataTable')->name('get_pending_eoffice_for_datatable');
                Route::get('open_sync_closure_employer_page/{eoffice_employer_closure_id}', 'EmployerClosureController@openSyncClosureToEmployerPage')->name('open_sync_closure_employer_page');
                Route::put('sync_closure_employer_page/{eoffice_employer_closure_id}', 'EmployerClosureController@syncClosureToEmployer')->name('sync_closure_employer_page');
                Route::post('store_from_eoffice', 'EmployerClosureController@storeClosureFromEoffice')->name('store_closure_from_eoffice');
                Route::post('store_supporting_doc_from_eoffice', 'EmployerClosureController@storeClosureSupportingDocFromEoffice')->name('store_supporting_doc_from_eoffice');
            });

});

});

});