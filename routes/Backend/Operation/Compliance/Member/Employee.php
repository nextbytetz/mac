<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member'], function() {
/**
 * EMPLOYEE ROUTES==========================
 */

/* =====EMPLOYEE ROUTES -*/
Route::resource('employee', 'EmployeeController', ['except' => ['show']]);
Route::get('employee/profile/{employee}', 'EmployeeController@profile')->name('employee.profile'); //Profile
Route::get('get_employee', 'EmployeeController@get')->name('employee.get'); //All employees
//get employers
Route::get('get_employers', 'EmployeeController@getEmployers')->name('employee.get_employers');

Route::get('get_employee_contributions/{employee}', 'EmployeeController@getContributionsForDataTable')->name('employee.contributions');  //Contributions
Route::get('get_employee_legacy_contributions/{employee}', 'EmployeeController@getLegacyContributionsForDataTable')->name('employee.legacy_contributions');  //Contributions
Route::get('employees', 'EmployeeController@getRegisterEmployees')->name('employees');
Route::post('employees_notification', 'EmployeeController@getRegisterEmployeesForNotification')->name('employees_notification');
Route::post('employees_notification_employer', 'EmployeeController@getRegisterEmployeesForNotificationEmployer')->name('employees_notification_employer');
/*get beneficiary employees only */
Route::get('beneficiary_employees', 'EmployeeController@getBeneficiaryEmployees')->name('beneficiary_employees');
/* add individual contribution */
Route::get('employee/add_contribution/{employee}', 'EmployeeController@addContribution')->name('employee.add_contribution');
Route::post('employee/store_contribution/{employee}', 'EmployeeController@storeContribution')->name('employee.store_contribution');









/*CLAIM / NOTIFICATION=====================================*/
Route::get('get_accidents/{employee}', 'EmployeeController@getAccidentNotificationsForDatatable')->name('employee.get_accident_notifications'); // accident notifications for an employee

Route::get('get_diseases/{employee}', 'EmployeeController@getDiseaseNotificationsForDatatable')->name('employee.get_disease_notifications'); // disease notifications for an employee

Route::get('get_death/{employee}', 'EmployeeController@getDeathNotificationForDatatable')->name('employee.get_death_notification'); // death notifications for an employee
Route::get('get_manual_notifications/{employee}', 'EmployeeController@getManualNotificationsForDataTable')->name('employee.get_manual_notifications');

Route::get('get_employment_histories/{employee}', 'EmployeeController@getEmploymentHistoryForDataTable')->name('employee.get_employment_histories'); // employment Histories

Route::get('get_employment_old_compensations/{employee}', 'EmployeeController@getOldCompensationsForDataTable')->name('employee.get_employment_old_compensations'); // old compensations

Route::get('get_dependents/{employee}', 'EmployeeController@getDependentsForDataTable')->name('employee.get_dependents'); // Dependents


/*
 * employment history ADD / EDIT
 */
Route::get('employee/employment_history/{employee}/create', 'EmployeeController@createEmploymentHistory')->name('employee.create_employment_history');
// create
Route::post('employee/employment_history/{employee}', 'EmployeeController@storeEmploymentHistory')->name('employee.store_employment_history');
//edit
Route::get('employee/employment_history/{employment_history}/edit', 'EmployeeController@editEmploymentHistory')->name('employee.edit_employment_history');
Route::put('employee/employment_history/update/{employment_history}', 'EmployeeController@updateEmploymentHistory')->name('employee.update_employment_history');//update
//  Delete
Route::delete('employee/employment_history/{employment_history}', 'EmployeeController@deleteEmploymentHistory')->name('employee.delete_employment_history');



/*
  *
  * Compensation history -> Old Compensations======================
  */
//create
Route::get('employee/old_compensation/{employee}/create', 'EmployeeController@createOldCompensation')->name('employee.create_old_compensation');
Route::post('employee/old_compensation/{employee}', 'EmployeeController@storeOldCompensation')->name('employee.store_old_compensation');

//edit
Route::get('employee/old_compensation/{employment_old_compensation}/edit', 'EmployeeController@editEmployeeOldCompensation')->name('employee.edit_old_compensation');
Route::put('employee/old_compensation/update/{employment_old_compensation}', 'EmployeeController@updateOldCompensation')->name('employee.update_old_compensation');//update
//  Delete
Route::delete('employee/old_compensation/{employment_old_compensation}', 'EmployeeController@deleteEmployeeOldCompensation')->name('employee.delete_old_compensation');


// end compensatation history ------------------------

// --New notification report ---> chooses incident type

Route::get('employee/choose_incident_type/{employee}', 'EmployeeController@chooseIncidentType')->name('employee.choose_incident_type')->where('employee', '[0-9]+');


//            --End Claim -------------------------------------------

Route::get('get_removals/{employee}', 'EmployeeController@getRemovalDatatable')->name('employee.get_removals'); 



});

});

});