<?php
///**
// * Created by PhpStorm.
// * User: mluhanjo
// * Date: 1/18/19
// * Time: 11:41 AM
// */
//
//
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member'], function() {

            /*  DEPENDENTS ROUTES ====================================================== */

            Route::resource('employee/dependent', 'DependentController', ['except' => ['show']]);
            /*edit dependent*/
            Route::get('employee/dependent/{dependent}/edit/{employee}', 'DependentController@editDependent')->name('dependent.edit_dependent');
            Route::get('employee/dependent/{employee}/create', 'DependentController@create_new')->name('dependent.create_new');
            Route::post('employee/dependent/{employee}/store', 'DependentController@store_new')->name('dependent.store_new');
            Route::get('dependent/get_for_select', 'DependentController@getDependentsForSelect')->name('dependent.get_for_select');
            Route::get('dependents', 'DependentController@getRegisteredDependents')->name('dependents');
            /*get dependents eligible for gratuity*/
            Route::get('gratuity_dependents', 'DependentController@getGratuityDependents')->name('gratuity_dependents');



            /*PAYROLL*/
            Route::group(['prefix' => 'dependent',  'as' => 'dependent.'], function() {

                Route::get('index', 'DependentController@index')->name('index');
                Route::get('get_for_datatable', 'DependentController@getDependentsForDataTable')->name('get_for_datatable');
                Route::post('get_ready_for_payroll_datatable', 'DependentController@getReadyForPayrollForDataTable')->name('get_ready_for_payroll_datatable');
                Route::post('get_pensionable_for_datatable', 'DependentController@getPensionableDependentsForDataTable')->name('get_pensionable_for_datatable');
                Route::get('get_other_by_employee_datatable/{dependent_employee}', 'DependentController@getOtherByEmployeeForDataTable')->name('getOtherByEmployeeForDataTable');
                Route::get('profile/{dependent_employee}', 'DependentController@profile')->name('profile');

                /*Merge dependent exist on te system but paid manually*/
                Route::get('dependents_ready_for_merge_manual', 'DependentController@dependentReadyForMergeManual')->name('dependents_ready_for_merge_manual');
                Route::post('get_ready_for_merge_manual_dependents', 'DependentController@getManualMergeReadyForPayrollForDataTable')->name('get_ready_for_merge_manual_dependents');
                Route::get('dependents_already_merged_payroll', 'DependentController@dependentAlreadyMergedManualPayroll')->name('dependents_already_merged_payroll');
                Route::post('get_already_merged_payroll', 'DependentController@getManualAlreadyMergedForPayrollForDataTable')->name('get_already_merged_payroll');
                Route::get('merge_manual_dependent_page/{dependent}/{employee}', 'DependentController@mergeManualDependentPage')->name('merge_manual_dependent_page');
                Route::put('merge_manual_dependent/{employee}', 'DependentController@mergeManualDependent')->name('merge_manual_dependent');
                /*Status action - 0 => not paid, 1 => paid, 2 => paid and terminate, 3 => reset*/
                Route::get('update_manualpay_status/{dependent}/{employee}/{status}', 'DependentController@updateManualPayStatus')->name('update_manualpay_status');
                /*Rectify mp*/
                Route::get('rectify_mp/{dependent}/{employee}', 'DependentController@rectifyMp')->name('rectify_mp');

                Route::get('recalculate_mp_distribution_manual/{incident}', 'DependentController@redistributeMpAllocationForManualSystemFile')->name('recalculate_mp_distribution_manual');
            });

            /*Export to erp as supplier*/
            Route::group(['prefix' => 'dependent',  'as' => 'dependent.'], function() {
                Route::get('ready_for_export_to_erp', 'DependentController@viewDependentsExportToErp')->name('ready_for_export_to_erp');
                Route::post('get_dependent_for_export_erp', 'DependentController@getDependentReadyToBeExportedToErpForDataTable')->name('get_dependent_for_export_erp');
                Route::get('create_erp_supplier/{dependent_employee}', 'DependentController@returnSupplierForm')->name('create_erp_supplier');
                Route::post('store_erp_supplier', 'DependentController@storeErpSupplier')->name('store_erp_supplier');
            });

// ---END DEPENDENTS ------------------------------------------------------

        });

    });

});