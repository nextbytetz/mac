<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //        MEMBERS GROUP
        Route::group(['namespace' => 'Member'], function() {

            //ADES service routes-----------------------------------------------------
            Route::get('status', 'EmployerController@employerStatus')->name('status');

            Route::post('status', 'EmployerController@employerStatusShow')->name('show.status');

            Route::get('payesdl', 'EmployerController@payesdl')->name('payesdl');

            Route::post('payesdl', 'EmployerController@payesdlShow')->name('show.payesdl');

            Route::get('newemployer', 'EmployerController@newEmployer')->name('newemployer');

            Route::post('newemployer', 'EmployerController@newEmployerShow')->name('show.newemployer');

            Route::get('closed', 'EmployerController@closedBusiness')->name('closed');

            Route::post('closed', 'EmployerController@closedBusinessShow')->name('show.closed');

            Route::get('LargeContributor','EmployerController@viewLargeContributor')->name('LargeContributor');

            Route::get('LargeContributorCertificate','EmployerController@viewLargeContributorCertificates')->name('LargeContributorCertificate');




            //End of ADES service routes========================================

//        EMPLOYERS ==================================
            Route::resource('employer', 'EmployerController', ['except' => ['show']]);
            Route::get('employer/profile/{employer}', 'EmployerController@profile')->name('employer.profile');
            Route::get('employer/edit_gen_info/{employer}', 'EmployerController@editGeneralInfo')->name('employer.edit_gen_info');
            Route::post('employer/update_gen_info', 'EmployerController@updateGeneralInfo')->name('employer.update_gen_info');

        //*******Online Profile of employers********//
            // Route::get('employer/online/profile/{employer}', 'EmployerController@onlineprofile')->name('employer.online_profile');

            Route::group(['prefix' => 'employer/online',  'as' => 'employer.online.'], function() {

                Route::get('profile/{employer}', 'OnlineProfileController@index')->name('profile');
                
                Route::get('get_arrears/{employer}', 'OnlineProfileController@getContributionsArrears')->name('get_arrears');
                Route::get('add_arrears_form/{employer}', 'OnlineProfileController@ContributionsArrearsform')->name('add_arrears_form');
                Route::post('add_arrears/{employer}', 'OnlineProfileController@addContributionsArrears')->name('add_arrears');
                Route::post('clear_arrears/{arrear}', 'OnlineProfileController@clearArrears')->name('clear_arrears');
                Route::post('upload_arrear/{arrear}', 'OnlineProfileController@uploadEmployeesArrear')->name('upload_arrear');

                Route::get('users/{employer}', 'OnlineProfileController@users')->name('users');
                Route::get('return_user/{user_id}', 'OnlineProfileController@returnUser')->name('return_user');
                Route::post('reset_password', 'OnlineProfileController@resetPassword')->name('reset_password');
                Route::get('change_user_account/{user_id}', 'OnlineProfileController@changeAccountType')->name('change_user_account');

                Route::get('deactivate_user_form/{user_id}/{employer}','OnlineProfileController@deactivateUserform')->name('deactivate_user_form');
                Route::post('deactivate_user/{user_id}/{employer}','OnlineProfileController@deactivateUser')->name('deactivate_user');
                Route::post('activate_user/{user_id}/{employer}','OnlineProfileController@activateUser')->name('activate_user');

                //payroll
                Route::get('verified_payroll/{employer_id}', 'OnlineProfileController@employerPayrollsDatatable')->name('verified_payroll');



                //payments
                Route::get('payment/{employer}/{payroll_id}', 'OnlineProfileController@payment')->name('payment');
                Route::post('generate_employer_bill', 'OnlineProfileController@generateEmployerBill')->name('generate_employer_bill');
                Route::get('get_employees/{employer_id}/{payroll_id}', 'OnlineProfileController@getEmployees')->name('get_employees');

                Route::get('annual_earnings/{employer}', 'OnlineProfileController@annualEarnings')->name('annual_earnings');
                Route::post('reverse_annual_earnings', 'OnlineProfileController@reverseAnnualEarning')->name('reverse_annual_earnings');

                Route::get('commitments/{employer}', 'OnlineProfileController@commitments')->name('commitments');
                Route::get('checker_commitments', 'OnlineProfileController@commitmentsChecker')->name('checker_commitments');
                Route::get('view_commitment/{commitment_id}', 'OnlineProfileController@viewCommitment')->name('view_commitment');
                Route::get('already_paid_datatable/{commitment_id}', 'OnlineProfileController@alreadyPaidCommitmentDatatable')->name('already_paid_datatable');
                Route::get('already_mou_datatable/{commitment_id}', 'OnlineProfileController@alreadyMouCommitmentDatatable')->name('already_mou_datatable');
                Route::get('before_due_date_datatable/{commitment_id}', 'OnlineProfileController@beforeDueDateCommitmentDatatable')->name('before_due_date_datatable');
                Route::get('appointment_datatable/{commitment_id}', 'OnlineProfileController@appointmentCommitmentDatatable')->name('appointment_datatable');

                Route::get('return_update_commitment_modal/{commitment_id}', 'OnlineProfileController@returnUpdateCommitmentModal')->name('return_update_commitment_modal');
                Route::get('return_commitment_resolution/{commitment_id}', 'OnlineProfileController@returnCommitmentResolution')->name('return_commitment_resolution');
                Route::post('update_commitment', 'OnlineProfileController@updateCommitment')->name('update_commitment');
                Route::get('installments/{employer}', 'OnlineProfileController@installments')->name('installments');
                Route::get('view_installment/{installment_id}', 'OnlineProfileController@showInstallment')->name('view_installment');
                Route::get('instalment_details/{instalment_id}', 'OnlineProfileController@instalmentDetails')->name('instalment_details');
                Route::get('get_due_date/{instalment_id}/{phase}', 'OnlineProfileController@getDueDate')->name('get_due_date');
                Route::post('save_instalment_due_date/{instalment_id}', 'OnlineProfileController@saveInstalmentDueDate')->name('save_instalment_due_date');



                

                // mou_template
                
                Route::get('mou_template/{instalment_id}', 'OnlineProfileController@mouTemplate')->name('mou_template');
                Route::get('unsigned_mou/{instalment_id}', 'OnlineProfileController@unsignedMou')->name('unsigned_mou');
                Route::post('save_mou_agreement_date/{instalment_id}', 'OnlineProfileController@saveMouAgreementDate')->name('save_mou_agreement_date');

                //instalment agreement
                Route::get('instalment_agreement_template/{instalment_id}', 'OnlineProfileController@instalmentAgreementTemplate')->name('instalment_agreement_template');

                //transmitter letter 
                Route::post('save_letter_details/{instalment_id}', 'OnlineProfileController@saveLetterDetails')->name('save_letter_details');
                Route::get('transmitter_letter_template/{instalment_id}', 'OnlineProfileController@transmitterLetterTemplate')->name('transmitter_letter_template');


                //commitnet bank receipt 
                Route::get('commitment_bank_attachments/{employer_id}/{commitment_id}', 'OnlineProfileController@commitmentBankPdf')->name('commitment_bank_attachments');
                Route::get('commitment_mou_attachments/{employer_id}/{commitment_id}', 'OnlineProfileController@commitmentMouPdf')->name('commitment_mou_attachments');
                Route::get('checker_instalments', 'OnlineProfileController@instalmentChecker')->name('checker_instalments');
            });

Route::group(['prefix' => 'employer/payroll_merge', 'as' => 'employer.payroll_merge.'], function ()
{
    Route::get('profile/{merge_request_id}', 'EmployerPayrollMergeController@index')
    ->name('profile');
    Route::get('merge_requests/{employer_id}', 'EmployerPayrollMergeController@mergeRequests')
    ->name('merge_requests');
    Route::post('post', 'EmployerPayrollMergeController@store')
    ->name('store');
    Route::post('preview_document/{merge_request_id}', 'EmployerPayrollMergeController@previewDocument')
    ->name('preview_document');
    Route::get('{merge_request_id}/payroll_summary/{payroll_id}/{type}', 'EmployerPayrollMergeController@getPayrollSummary')
    ->name('payroll_summary');
    Route::get('{merge_request_id}/summary_details/{payroll_id}/{type}/{what_to_return}', 'EmployerPayrollMergeController@getViewDetails')
    ->name('summary_details');

});


Route::get('agent/agentprofile/{agent}', 'EmployerController@agentProfile')->name('agent.agentprofile');
Route::get('employer/merge', 'EmployerController@mergeEmployers')->name('employer.merge');
Route::put('employer/merge/post', 'EmployerController@postMergeEmployers')->name('employer.post_merge');
Route::get('employer/unmerge', 'EmployerController@unMergeEmployers')->name('employer.unmerge');
Route::put('employer/unmerge/post', 'EmployerController@postUnMergeEmployers')->name('employer.post_unmerge');
Route::get('employer/{employer}/verification', 'EmployerController@onlineVerification')->name('employer.verification');
Route::get('employer/{verification}/verification_profile', 'EmployerController@onlineVerificationProfile')->name('employer.verification_profile');
Route::get('employer/{employer}/advance_payment', 'EmployerController@advancePayment')->name('employer.advance_payment');
Route::get('employer/{advance_payment}/advance_payment_profile', 'EmployerController@advancePaymentProfile')->name('employer.advance_payment_profile');
Route::get('employer/{employer}/incident', 'EmployerController@onlineIncident')->name('employer.incident');
Route::get('employer/{incident}/incident_profile', 'EmployerController@onlineIncidentProfile')->name('employer.incident_profile');
Route::post('employer/{incident}/incident_optional_docs/{check}/{documentId}', 'EmployerController@onlineIncidentOptionalDocs')->name('employer.incident_optional_docs');
Route::post('employer/{incident}/notification_report_optional_docs/{check}/{documentId}', 'EmployerController@onlineNotificationReportOptionalDocs')->name('employer.notification_report_optional_docs');
Route::post('employer/{incident}/post_incident_comment', 'EmployerController@postIncidentComment')->name('employer.post_incident_comment');
Route::get('employer/verification/{verification}/documents_library', 'EmployerController@showVerificationDocumentLibrary')->name('employer.verification_document_library');
Route::get('employer/verification/{verification}/{documentId}/download_document', 'EmployerController@downloadVerificationDocument')->name('employer.verification_download_document');
Route::get('employer/verification/{verification}/{documentId}/open_document', 'EmployerController@openVerificationDocument')->name('employer.verification_open_document');
Route::get('employer/advance_payment/{advance_payment}/documents_library', 'EmployerController@showAdvancePaymentDocumentLibrary')->name('employer.advance_payment_document_library');
Route::get('employer/advance_payment/{advance_payment}/{documentId}/download_document', 'EmployerController@downloadAdvancePaymentDocument')->name('employer.advance_payment_download_document');
Route::get('employer/advance_payment/{advance_payment}/{documentId}/open_document', 'EmployerController@openAdvancePaymentDocument')->name('employer.advance_payment_open_document');
Route::get('employer/incident/{incident}/documents_library', 'EmployerController@showIncidentDocumentLibrary')->name('employer.incident_document_library');
Route::get('employer/notification_report/{incident}/documents_library', 'EmployerController@showNotificationReportDocumentLibrary')->name('employer.notification_report_document_library');
Route::get('employer/incident/{incident}/{documentId}/download_document', 'EmployerController@downloadIncidentDocument')->name('employer.incident_download_document');
Route::get('employer/incident/{incident}/{documentId}/open_document', 'EmployerController@openIncidentDocument')->name('employer.incident_open_document');
Route::get('get', 'EmployerController@get')->name('employer.get');
Route::get('get_bookings/{employer}', 'EmployerController@getBookingsForDataTable')->name('employer.bookings');
Route::get('get_bookings_for_missing_months/{employer}', 'EmployerController@getBookingsForMissingMonthsForDataTable')->name('employer.get_bookings_for_missing_months');
Route::get('get_contributions/{employer}', 'EmployerController@getContributionsForDataTable')->name('employer.contributions');
Route::post('employer/{incident}/merge_incident', 'EmployerController@postMergeIncident')->name('employer.merge_incident');
/* Online Generated arrears routes */
            // Route::get('clear_contributions_arrears/{arrear}', 'EmployerController@clearContributionsArrears')->name('clear.contributions.arrears');


            // Route::post('clear_contributions_arrears/{arrear}', 'EmployerController@clearContributionsArrears')->name('clear.contributions.arrears');

            // Route::post('add_contributions_arrears/{employer}', 'EmployerController@addContributionsArrears')->name('add.contributions.arrears');

/* End of Online Generated arrears routes */

/*Online Employer*/
Route::get('get_employer_bills/{employer}', 'EmployerController@getEmployerBills')->name('employer.bills');

Route::get('print_employee_bills/{bill}/{employer}', 'EmployerController@printEmployeeBills')->name('print.employee.bills');



Route::post('get_employer_notification_user/{employer}', 'EmployerController@getEmployerNotificationUsers')->name('employer_notification.user');
Route::get('deactivate_notification_user_form/{claim_id}','EmployerController@deactivateNotificationUserform')->name('deactivate_notification_user_form');
Route::post('deactivate_notification_user/{claim_id}','EmployerController@deactivateNotificationUser')->name('deactivate_notification_user');





/*End of bills generated routes*/

Route::get('get_legacy_contributions/{employer}', 'EmployerController@getLegacyContributionsForDataTable')->name('employer.legacy_contributions');
Route::get('get_interests/{employer}', 'EmployerController@getInterestsForDataTable')->name('employer.interests');
Route::get('get_inactive_receipts/{employer}', 'EmployerController@getInactiveReceiptsForDataTable')->name('employer.inactive_receipts');
Route::get('employers', 'EmployerController@getRegisterEmployers')->name('employers');
Route::get('notification_employers', 'EmployerController@getNotificationRegisterEmployers')->name('notification_employers');
Route::post('employers_tin', 'EmployerController@getRegisterEmployersTin')->name('employers.tin');
Route::post('stored_employers', 'EmployerController@getAllStoredEmployers')->name('stored_employers');
/*get beneficiary employees only */
Route::get('beneficiary_employers', 'EmployerController@getBeneficiaryEmployers')->name('beneficiary_employers');
/* All employers without global scope*/
Route::get('all_employers', 'EmployerController@getAllEmployers')->name('all_employers');
/* Employee Counts */
Route::get('get_employee_count/{employer}', 'EmployerController@getEmployeeCountsForDataTable')->name('employer.employee_count_get');
/*  Employer Interest selected */
Route::get('employer/profile/interest/{booking_interest}', 'EmployerController@viewSelectedInterest')->name('employer.interest_profile');
Route::get('employer/interest/adjusted_manually/{booking_interest}', 'EmployerController@completeWfForInterestAdjustedManually')->name('employer.interest.adjusted_manually');
/* Get All employees for this employer */
Route::get('employer/employees/{employer}', 'EmployerController@getRegisteredEmployees')->name('employer.employees');

/* Start : Employer Closures */
Route::get('employer/{employer}/close', 'EmployerController@getCloseBusiness')->name('employer.close_business');
Route::put('employer/{employer}/close', 'EmployerController@postCloseBusiness')->name('employer.close_business.post');
Route::get('closed_business', 'EmployerController@getClosedBusinessTRA')->name('closed_business');
Route::post('closed_business/get', 'EmployerController@getClosedBusinessTRAForDatatable')->name('closed_business.datatable');
Route::get('closed_business/{closed_business}/show', 'EmployerController@showClosedBusinessTRA')->name('closed_business.show');
Route::post('closed_business/{closed_business}/associate', 'EmployerController@associateClosedBusinessTRA')->name('closed_business.associate');
Route::delete('closed_business/remove', 'EmployerController@deleteClosedBusinessTRA')->name('closed_business.delete');
Route::get('employer/{employer}/open', 'EmployerController@getOpenBusiness')->name('employer.open_business');
Route::post('employer/{employer}/open', 'EmployerController@postOpenBusiness')->name('employer.open_business.post');
            //Pending, querying all business closures of a particular employer

/* End : Employer Closures */


            /*
             * ADJUST EMPLOYER INTEREST SELECTED
             */
            Route::get('employer/profile/interest_adjust/{booking_interest}', 'EmployerController@editInterest')->name('employer.adjust_interest');
            Route::put('employer/profile/interest_adjust/{booking_interest}', 'EmployerController@adjustInterest')->name('employer.adjust_interest');


            /* Employer with Interest Overpayment Report*/

            Route::get('employer/interest_overpayment', 'EmployerController@getEmployerInterestOverpayment')->name('employer.interest_overpayment');



            /*END interest Refund---*/


            /*
             * WRITE OFF EMPLOYER INTEREST
             */
            Route::get('employer/profile/interest_write_off/{employer}', 'EmployerController@writtingOffInterestPage')->name('employer.write_off_interest_page');
            Route::put('employer/profile/interest_write_off/{employer}', 'EmployerController@writeOffInterest')->name('employer.write_off_interest');
// route page to approve interest written off
            Route::get('employer/profile/interest_write_off_overview/{employer}', 'EmployerController@getInterestWrittenOffOverview')->name('employer.write_off_interest_overview');
            Route::get('employer/profile/interest_write_off_approve/{interest_write_off}', 'EmployerController@ApproveInterestWrittenOff')->name('employer.write_off_interest_approve');
            Route::get('employer/profile/interests_written_off/{interest_write_off}', 'EmployerController@getInterestsWrittenOffForDataTable')->name('employer.written_off_interests');
            //Modify/ edit interest written offs
            Route::get('employer/profile/interest_write_off_edit/{interest_write_off}', 'EmployerController@writtenOffInterestsEdit')->name('employer.written_off_interest_edit');
            Route::put('employer/profile/interest_write_off_update/{interest_write_off}', 'EmployerController@writtenOffInterestsUpdate')->name('employer.write_off_interest_update');
//            Interest adjust overview
            Route::get('employer/profile/interest_adjust_overview/{employer}', 'EmployerController@getInterestAdjustedOverview')->name('employer.adjust_interest_overview');
            /*
             * CONTRIBUTION
             */
            //Form to search receipt by rctno from compliance menu
            Route::get('receipt', 'ContributionController@index')->name('receipt.search');
            //Route::get('receipt/get', 'ContributionTableController')->name('receipt.get');


            /**
             *   CONTRIBUTION TRACKS
             */
            Route::resource('contribution_track', 'ContributionTrackController', ['except' => ['show']]);
// create new track
            Route::get('contribution_track/{receipt_code}/create', 'ContributionTrackController@create_new')->name('contribution_track.create_new');


            /*Dishonoured cheques*/

            Route::post('get_dishonoured_cheques_for_dt/{employer}', 'EmployerController@getDishonouredChequesByEmployerForDt')->name('get_dishonoured_cheques_for_dt');



            /**
             * BOOKING PROFILE=====================
             */
//         Employer booking selected
            Route::get('employer/profile/booking/{booking}', 'EmployerController@bookingProfile')->name('employer.booking_profile');
// open booking adjustment page
            Route::get('employer/profile/booking_adjust/{booking}', 'EmployerController@bookingAdjustPage')->name('employer.booking_adjust');
// open booking adjustment page
            Route::put('employer/profile/booking_adjust/{booking}', 'EmployerController@bookingAdjust')->name('employer.booking_adjust');            //END --Booking-profile =--------------




            /**
             * BOOKING INTEREST REFUND ROUTES============start=========
             */

            Route::resource('booking_interest', 'BookingInterestController', ['except' => ['show']]);


            Route::get('booking_interest/refund_overview/{employer}', 'BookingInterestController@interestRefund')->name('booking_interest.refund_overview');


            Route::get('booking_interest/refund_page/{employer}', 'BookingInterestController@refundPage')->name('booking_interest.refunding_page');


            Route::get('booking_interest/interest_refund_overview/{employer}', 'BookingInterestController@getInterestRefundOverviewForDataTable')->name('booking_interest.get_refunds');

            Route::post('booking_interest/get_eligible_for_refund/{employer}', 'BookingInterestController@getInterestEligibleForRefundingForDataTable')->name('booking_interest.get_eligible_for_refund');
            /*Refund selected*/
            Route::post('booking_interest/refund_selected', 'BookingInterestController@refundSelected')->name('booking_interest.refund_selected');

            /*Interest refund profile*/
            Route::get('booking_interest/refund_profile/{interest_refund}', 'BookingInterestController@interestRefundProfile')->name('booking_interest.refund_profile');

            /*Interest refunded*/
            Route::get('booking_interest/interest_refunded/{interest_refund}', 'BookingInterestController@getInterestRefundedForDataTable')->name('booking_interest.get_interest_refunded');

            /*Update Bank details*/
            Route::put('booking_interest/interest_refund/update_bank_details/{interest_refund}', 'BookingInterestController@updateBankDetails')->name('booking_interest.refund.update_bank_details');

            /*Get Interest for modifying interest refund*/
            Route::post('booking_interest/get_eligible_for_modifying_refund/{employer}/{interest_refund}', 'BookingInterestController@getInterestEligibleForModifyRefundForDataTable')->name('booking_interest.get_eligible_for_modifying_refund');

            /*Modify interest refund*/
            Route::get('booking_interest/refund/edit/{interest_refund}', 'BookingInterestController@modifyInterestRefund')->name('booking_interest.refund.edit');

            /*Add interest refund*/
            Route::get('booking_interest/interest_refund/add_interest/{booking_interest}/{interest_refund}', 'BookingInterestController@addInterestRefund')->name('booking_interest.refund.add_interest');
            /*Remove interest from refund batch*/
            Route::get('booking_interest/interest_refund/remove_interest/{booking_interest}/{interest_refund}', 'BookingInterestController@removeInterestRefund')->name('booking_interest.refund.remove_interest');

            /*Undo interest refund */
            Route::get('booking_interest/interest_refund/undo/{interest_refund}', 'BookingInterestController@undoInterestRefund')->name('booking_interest.refund.undo');

            /*Recalculate interest for employer*/
            Route::get('booking_interest/recalculate/{employer_id}', 'BookingInterestController@recalculateInterestForEmployer')->name('booking_interest.recalculate');


            /* Initiate / Approve Interest Refund*/
            Route::get('booking_interest/interest_refund/initiate/{interest_refund}', 'BookingInterestController@initiateInterestRefund')->name('booking_interest.refund.initiate');

            /*Incidents with paystatus*/
            Route::post('get_incidents_for_dt/{employer}', 'EmployerController@getIncidentsWithPayStatusForDt')->name('employer.get_incidents_for_dt');
            /* End of Booking Interest  REFUND Routes ----------end --------*/
            /*------------ END EMPLOYER ROUTE--------------------------------*/






            /*============REGISTRATION EMPLOYERS ===========ROUTES ---------*/
            Route::resource('employer_registration', 'EmployerRegistrationController', ['except' => ['show']]);

            /*name check before adding new*/
            Route::get('employer_registration/name_check', 'EmployerRegistrationController@nameCheck')->name('employer_registration.name_check');
            /*get all employers (unregistered + registered)*/
            Route::get('employer_registration/name_check/getAllEmployers', 'EmployerRegistrationController@getAllEmployers')->name('employer_registration.name_check.get_all_employer');
            /*profile*/
            Route::get('employer_registration/profile/{employer_registration}', 'EmployerRegistrationController@profile')->name('employer_registration.profile');
//           /* register Employer Information */
            Route::post('employer_registration/register', 'EmployerRegistrationController@register')->name('employer_registration.register');

            /*show Document Library*/
            Route::get('employer_registration/documents_library/{employer_registration}', 'EmployerRegistrationController@showDocumentLibrary')->name('employer_registration.documents_library');
            /*download document*/
            Route::get('employer_registration/download_document/{employer_registration}/{documentId}', 'EmployerRegistrationController@downloadDocument')->name('employer_registration.download_document');
            /*open document*/
            Route::get('employer_registration/open_document/{employer_registration}/{documentId}', 'EmployerRegistrationController@openDocument')->name('employer_registration.open_document');
            /* Download upload error */
            Route::get('employer_registration/error/{employer_registration}', 'EmployerRegistrationController@downloadError')->name("employer_registration.download_error");

            /*   Upload employee list / wcr3 */
            Route::get('employer_registration/{employer}/upload_employee', 'EmployerRegistrationController@employeeListFile')->name('employer_registration.employee_list_file');
            Route::post('employer_registration/{employer}/upload/store', 'EmployerRegistrationController@uploadEmployeeList')->name('employer_registration.upload_employee_list');
            Route::get('employer_registration/{employer}/load/store', 'EmployerRegistrationController@loadEmployeeList')->name('employer_registration.load_employee_list');
            /*Undo loaded employees*/
            Route::get('employer_registration/{employer}/undo_loaded_employees', 'EmployerRegistrationController@undoLoadedEmployees')->name('employer_registration.undo_loaded_employees');
            /* Upload progress */
            Route::post('employer_registration/upload_progress/{employer}', 'EmployerRegistrationController@uploadProgress')->name("employer_registration.upload_progress");

            //=============================================================== MAC GEPG
            Route::get('return_employees/{employer}', 'EmployerRegistrationController@returnEmployees')->name('return_employees');
            Route::get('generate_number/{employer}', 'EmployeeUploadController@generateNumber');
            Route::resource('online_uploaded_employees', 'EmployeeUploadController');

            //================================================== MAC - GEPG


            /*Approve Registration*/
            Route::get('employer_registration/approve/{employer_registration}', 'EmployerRegistrationController@approveRegistration')->name('employer_registration.approve');
            /*Print Certificate of Registration*/
            Route::get('employer_registration/print_certificate/{employer_registration}', 'EmployerRegistrationController@printCertificate')->name('employer_registration.print_certificate');
            /* Reissue Certificate */
            Route::get('employer_registration/reprint_certificate/{employer_registration}', 'EmployerRegistrationController@reprintCertificate')->name('employer_registration.reprint_certificate');
            /*Print Issuance of Registration No*/
            Route::get('employer_registration/print_issuance/{employer_registration}', 'EmployerRegistrationController@printIssuance')->name('employer_registration.print_issuance');

            /*  Undo / Reject Registration */
            Route::get('employer_registration/undo/{employer_registration}', 'EmployerRegistrationController@destroy')->name('employer_registration.undo');
            /* Name clearance */
            Route::get('employer_registration/get_name_clearance/{employer_registration}', 'EmployerRegistrationController@getNameClearance')->name("employer_registration.get_name_clearance");

            /* ----------END OF REGISTRATION --------------------*/


            /*============UNREGISTERED EMPLOYERS ===========ROUTES ---------*/
            Route::resource('unregistered_employer', 'UnregisteredEmployerController', ['except' => ['show']]);

            /* Profile */
            Route::get('unregistered_employer/profile/{unregistered_employer}', 'UnregisteredEmployerController@profile')->name('unregistered_employer.profile');
            /* Register / approve */
            Route::get('unregistered_employer/register/{unregistered_employer}', 'UnregisteredEmployerController@register')->name('unregistered_employer.register');

            /* Undo / delete */
            Route::get('unregistered_employer/undo/{unregistered_employer}', 'UnregisteredEmployerController@destroy')->name('unregistered_employer.undo');

            /* Mark as registered */
            Route::get('unregistered_employer/mark_registered/{unregistered_employer}', 'UnregisteredEmployerController@markRegistered')->name('unregistered_employer.mark_registered');


            /* Upload Bulk Unregistered */
            Route::get('unregistered_employer/upload_bulk', 'UnregisteredEmployerController@uploadBulk')->name('unregistered_employer.upload_bulk');
            Route::post('unregistered_employer/store_uploaded_bulk', 'UnregisteredEmployerController@storeUploadedBulk')->name('unregistered_employer.store_uploaded_bulk');

            /* Add new taxpayers to unregistered */
            Route::get('unregistered_employer/add_tra', 'UnregisteredEmployerController@addTra')->name('unregistered_employer.add_tra');
            Route::post('unregistered_employer/add_tra', 'UnregisteredEmployerController@addTraPost')->name('unregistered_employer.add_tra_post');
            Route::post('unregistered_employer/get_tra', 'UnregisteredEmployerController@getTra')->name('unregistered_employer.get_tra');
            Route::delete('unregistered_employer/remove_tra', 'UnregisteredEmployerController@removeTra')->name('unregistered_employer.remove_tra');


            /* upload bulk Inactive*/
            Route::get('unregistered_employer/upload_bulk_inactive', 'UnregisteredEmployerController@uploadBulkInActive')->name('unregistered_employer.upload_bulk_inactive');
            Route::post('unregistered_employer/store_uploaded_bulk_inactive', 'UnregisteredEmployerController@storeUploadedBulkInactive')->name('unregistered_employer.store_uploaded_bulk_inactive');

            /* Get unregistered unassigned employers */
            Route::get('unregistered_employers', 'UnregisteredEmployerController@getUnregisteredUnassignedEmployers')->name('unregistered_unassigned_employers');

            /**
             * Follow ups Routes===========
             */

//            get / index
            Route::get('unregistered_employer/get_follow_ups/{unregistered_employer}', 'UnregisteredEmployerController@getFollowUpsForDataTable')->name('unregistered_employer.follow_up_get');

            //create
            Route::get('unregistered_employer/follow_up/{unregistered_employer}/create', 'UnregisteredEmployerController@createFollowUp')->name('unregistered_employer.follow_up_create');
//            store
            Route::post('unregistered_employer/store_follow_up/{unregistered_employer}', 'UnregisteredEmployerController@storeFollowUp')->name('unregistered_employer.follow_up_store');
//            edit
            Route::get('unregistered_employer/follow_up/{unregistered_employer}/edit', 'UnregisteredEmployerController@editFollowUp')->name('unregistered_employer.follow_up_edit');
//            update
            Route::put('unregistered_employer/follow_up_update/{unregistered_employer_follow_up}', 'UnregisteredEmployerController@updateFollowUp')->name('unregistered_employer.follow_up_update');
//          delete
            Route::delete('unregistered_employer/delete_follow_up/{unregistered_employer_follow_up}', 'UnregisteredEmployerController@deleteFollowUp')->name('unregistered_employer.follow_up_delete');

            /*------End of Follow ups-----------*/



            /* Staff Assignment----------- */

//            Self staff assign
            Route::get('unregistered_employer/self_assign_staff/{unregistered_employer}', 'UnregisteredEmployerController@selfAssignStaff')->name('unregistered_employer.self_assign_staff');
            /**Assign staff by supervisor**/
            Route::get('unregistered_employer/assign_staff_page/{unregistered_employer}', 'UnregisteredEmployerController@assignStaffPage')->name('unregistered_employer.assign_staff_page');
            Route::put('unregistered_employer/assign_staff/{unregistered_employer}', 'UnregisteredEmployerController@assignStaff')->name('unregistered_employer.assign_staff');
            /**Change assigned staff **/
            Route::get('unregistered_employer/edit_assigned_staff/{unregistered_employer}', 'UnregisteredEmployerController@editAssignedStaff')->name('unregistered_employer.assigned_staff_edit');
            Route::put('unregistered_employer/update_assigned_staff/{unregistered_employer}', 'UnregisteredEmployerController@updateAssignedStaff')->name('unregistered_employer.assigned_staff_update');
            /* end of staff assignment ---------------*/



            /*------End of UnRegistered  Employer Routes-------------------------*/
        });

});

});