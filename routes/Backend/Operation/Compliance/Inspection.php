<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {

//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        //INSPECTION ROUTES
        Route::group(['namespace' => 'Inspection','prefix' => 'inspection',  'as' => 'inspection.'], function() {
            Route::get('dishonoured_cheques', 'DishonouredChequesController@getDishonouredCheques')->name('get.dishonoured_cheques');
            Route::get('current', 'InspectionController@current')->name('current');
            Route::get('system_raised', 'InspectionController@systemRaised')->name('system_raised');
            Route::get('{inspection}/cancel', 'InspectionController@cancel')->name('cancel');
            Route::get('{inspection}/complete', 'InspectionController@complete')->name('complete');
            Route::get('{employer}/get_employer', 'InspectionController@getEmployerListForDataTable')->name('employer.get');
            Route::get('allocation/{inspection}', 'InspectionController@allocation')->name('allocation');
            Route::put('get_allocation/{inspection}', 'InspectionController@getAllocation')->name('allocation.get');
            Route::put('assign_allocation/{inspection}', 'InspectionController@assignAllocation')->name('allocation.assign');
            Route::post('allocation_inspection_type_group/{inspection}', 'InspectionController@getAllocationInspectionTypeGroup')->name('allocation_inspection_type_group');
            Route::post('allocation_staff_group/{inspection}', 'InspectionController@getAllocationStaffGroup')->name('allocation_staff_group');
            Route::get('{inspection}/assign_inspectors', 'InspectionController@assignInspectors')->name('assign_inspectors');
            Route::post('{inspection}/post_inspectors', 'InspectionController@postInspectors')->name('post_inspectors');
            Route::delete('{inspection}/remove_inspectors', 'InspectionController@removeInspectors')->name('remove_inspectors');
            Route::post('{inspection}/get_inspectors_datatable', 'InspectionController@getInspectorForDatatable')->name('get_inspectors_datatable');
            Route::post('initiate_workflow', 'InspectionController@initiateWorkflow')->name('initiate_workflow');
            Route::resource('task', 'InspectionTaskController', ['except' => [
                'index',
                'create',
            ]]);
            Route::resource('profile', 'InspectionProfileController');
            Route::group(['as' => 'profile.'], function() {
                Route::get('{profile}/get_profile', 'InspectionProfileController@getEmployerListForDataTable')->name('get');
                Route::get('{profile}/get_unregistered_employers', 'InspectionProfileController@getUnregisteredEmployerListForDataTable')->name
                ('get_unregistered_employers');
            });
            Route::group(['as' => 'task.'], function() {
                Route::get('{task}/download', 'InspectionTaskController@downloadEmployers')->name('download_employers');
                Route::get('{inspection}/assign', 'InspectionTaskController@assignTask')->name('assign');
            });
            Route::group(['as' => 'task.', 'prefix' => 'task'], function() {
                Route::post('get', 'InspectionTaskController@getListForDataTable')->name('get');
                Route::get('{inspection}/create', 'InspectionTaskController@create')->name('create');
                Route::post('{task}/get_employers_datatable', 'InspectionTaskController@getEmployersForDatatable')->name('get_employers_datatable');
                Route::get('users/search', 'InspectionTaskController@searchUsers')->name('users.search');
                Route::put('update_employer/{employer_inspection_task}', 'InspectionTaskController@updateEmployer')->name('update_employer');
                Route::post('initiate_workflow', 'InspectionTaskController@initiateWorkflow')->name('initiate_workflow');
            });
            Route::group(['as' => 'employer_task.', 'prefix' => 'employer_task'], function() {
                Route::get('{employer_task}', 'EmployerInspectionTaskController@show')->name('show');
                Route::get('{employer_task}/attend', 'EmployerInspectionTaskController@attend')->name('attend');
                Route::post('{employer_task}/attend_post', 'EmployerInspectionTaskController@postAttend')->name('attend.post');
                Route::get('{employer_task}/update_visitation', 'EmployerInspectionTaskController@updateVisitation')->name('update_visitation');
                Route::post('{employer_task}/update_visitation', 'EmployerInspectionTaskController@postUpdateVisitation')->name('post_update_visitation');
                Route::post('initiate_workflow', 'EmployerInspectionTaskController@initiateWorkflow')->name('initiate_workflow');
                Route::get('{employer_task}/{inspection_task_payroll}/assessment_template', 'EmployerInspectionTaskController@assessmentTemplate')->name('assessment_template');
                Route::get('{employer_task}/upload_assessment_template', 'EmployerInspectionTaskController@uploadAssessmentTemplate')->name('upload_assessment_template');
                Route::put('{employer_task}/post_upload_assessment_template', 'EmployerInspectionTaskController@postUploadAssessmentTemplate')->name('post_upload_assessment_template');
                Route::get('{employer_task}/{inspection_task_payroll}/download_assessment_schedule', 'EmployerInspectionTaskController@downloadAssessmentSchedule')->name('download_assessment_schedule');
                Route::get('{employer_task}/download_assessment_error', 'EmployerInspectionTaskController@downloadAssessmentError')->name('download_assessment_error');
                Route::get('{employer_task}/manage_payroll', 'EmployerInspectionTaskController@managePayroll')->name('manage_payroll');
                Route::post('{employer_task}/post_payroll', 'EmployerInspectionTaskController@postPayroll')->name('post_payroll');
                Route::post('{employer_task}/payroll_entries', 'EmployerInspectionTaskController@payrollEntries')->name('payroll_entries');
                Route::post('{employer_task}/skip_stage', 'EmployerInspectionTaskController@skipStage')->name('skip_stage');
                Route::post('initiate_workflow', 'EmployerInspectionTaskController@initiateWorkflow')->name('initiate_workflow');
                Route::get('{employer_task}/attach_document', 'EmployerInspectionTaskController@attachDocument')->name('attach_document');
                Route::get('{doc_pivot_id}/edit_document', 'EmployerInspectionTaskController@editDocument')->name('edit_document');
                Route::post('{doc_pivot_id}/preview_document', 'EmployerInspectionTaskController@previewDocument')->name('preview_document');
                Route::get('{doc_pivot_id}/delete_document', 'EmployerInspectionTaskController@deleteDocument')->name('delete_document');
                Route::post('store_document', 'EmployerInspectionTaskController@storeDocument')->name('store_document');
                Route::put('update_document', 'EmployerInspectionTaskController@updateDocument')->name('update_document');
            });
        });
        Route::group(['namespace' => 'Inspection'], function() {
            Route::resource('inspection', 'InspectionController');
        });

    });

});