<?php
/**
/**
 * Operation
 */
Route::group([
    /* 'prefix' => 'operation',*/
    'namespace' => 'Operation',
], function() {


//    COMPLIANCE - GROUP ROUTES
    /*
    * COMPLIANCE modules
    */
    Route::group(['namespace' => 'Compliance', 'prefix' => 'compliance',  'as' => 'compliance.'], function() {

        Route::get('/', function () {
            return view('backend.operation.compliance.menu');
        })->name('menu');
        //compliance defaults
        Route::get('defaults', 'ComplianceController@defaults')->name('defaults');
        //update compliance defaults
        Route::post('post_defaults/{reference}', 'ComplianceController@postDefaults')->name('post_defaults');
        Route::resource('booking_group', 'BookingGroupController', ['except' => ['edit', 'update', 'create', 'show', 'destroy']]);
        Route::group(['prefix' => 'booking_group',  'as' => 'booking_group.'], function() {
            Route::get('upload_manual', 'BookingGroupController@uploadManual')->name('upload_manual');
            Route::get('download_manual_error', 'BookingGroupController@downloadManualError')->name('download_manual_error');
            Route::put('upload_manual_post', 'BookingGroupController@postUploadManual')->name('upload_manual_post');
            Route::delete('destroy', 'BookingGroupController@destroy')->name('destroy');
            Route::get('pending_upload', 'BookingGroupController@pendingUpload')->name('pending_upload');
            Route::post('datatable', 'BookingGroupController@getBookingGroupsForDatatable')->name('datatable');
        });

    });

});