<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13-Apr-17
 * Time: 11:22 AM
 */
Route::group([
    'prefix' => 'legal',
    'as' => 'legal.',
    'namespace' => 'Legal',
], function() {
    Route::get('/', function () {
        return view('backend/legal/menu');
    })->name('menu');
    Route::resource('case', 'CaseController');

    Route::group(['prefix' => 'case',  'as' => 'case.'], function() {
        Route::post('get', 'CaseController@get')->name('get');
        Route::get('{case}/close', 'CaseController@close')->name('close');
        Route::resource('hearing', 'CaseHearingController', ['except' => [
            'index',
            'create',
        ]]);
        Route::group(['prefix' => 'hearing',  'as' => 'hearing.'], function() {
            Route::post('get_next', 'CaseHearingController@getNext')->name('get_next');
            Route::post('{case}/get', 'CaseHearingController@get')->name('get');
            Route::post('{lawyer}/get/lawyer', 'CaseHearingController@getLawyer')->name('get.lawyer');
            Route::get('{case}/create', 'CaseHearingController@create')->name('create');
        });
        Route::resource('mention', 'CaseMentionController', ['except' => [
            'index',
            'create',
        ]]);
        Route::group(['prefix' => 'mention',  'as' => 'mention.'], function() {
            Route::post('{case}/get', 'CaseMentionController@get')->name('get');
            Route::post('{lawyer}/get/lawyer', 'CaseMentionController@getLawyer')->name('get.lawyer');
            Route::get('{case}/create', 'CaseMentionController@create')->name('create');
        });
    });
    Route::resource('lawyer', 'LawyerController');
    Route::group(['prefix' => 'lawyer',  'as' => 'lawyer.'], function() {
        Route::post('get', 'LawyerController@get')->name('get');
    });
    /*
     * Route::get('/', function () {
        return view('backend.legal.index');
    })->name('menu');
    */

    /*
    Route::get('get', 'LegalController@get')->name('cases.get');
    Route::get('lawyers/get', 'LawyerController@get')->name('lawyers.get');
    Route::get('get/{case}', 'CaseHearingController@get')->name('case_hearings.get');
    Route::get('case_hearing/{id}/edit', 'CaseHearingController@editCaseHearing')->name('case_hearings.edit');
    Route::get('lawyer/get/{lawyer}', 'CaseHearingController@getLawyerHearings')->name('lawyer_hearings.get');
    Route::delete('delete/{case}', 'LegalController@caseDelete')->name('case_delete');

    Route::get('case/profile/{id}', 'LegalController@profile')->name('case.profile');
    Route::get('lawyer/profile/{id}', 'LawyerController@profile')->name('lawyer.profile');
    Route::get('case/add_hearing_date/{id}', 'LegalController@hearing')->name('case.hearing_date');
    Route::get('case/close/{id}', 'LegalController@hearing')->name('case.close');

    Route::resource('case', 'LegalController', ['except' => ['show']]);
    Route::resource('lawyer', 'LawyerController', ['except' => ['show']]);
    Route::resource('case_hearing', 'CaseHearingController', ['except' => ['show']]);

    Route::post('case/hearing_date', 'CaseHearingController@storeCaseHearing')->name('case.store_hearing_date');

    Route::get('case/add_hearing/{case}', 'CaseHearingController@addCaseHearing')->name('case.hearing_date.add');
    Route::get('case/add_mentioning/{case}', 'CaseHearingController@addCaseMentioning')->name('case.mentioning_date.add');
    */

});

