<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

Route::get('/xyz', function() {

    $date  = \Carbon\Carbon::parse(strtotime('1558957303'))->format('Y-m-d h:i:s');
    dd($date);
    $year = '2018'; 
    $month = '04';
    $TIN = '127510091';
    $monthlycontribution = DB::table('main.contributions')->select('memberno','employers.tin','firstname','middlename','lastname','contributions.salary','contributions.grosspay','receipt_codes.contrib_month')
    ->join('employees','employees.id','=','contributions.employee_id')
    ->join('employers','employers.id','=','contributions.employer_id')
    ->join('receipt_codes','receipt_codes.id','=','contributions.receipt_code_id')
    ->whereYear('receipt_codes.contrib_month','=', $year)
    ->whereMonth('receipt_codes.contrib_month','=', $month)
    ->where('employers.tin',$TIN)
    ->get();
    return $monthlycontribution;

});

//Route::get('/debug', function () {
//    return view('debug');
//});


// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

/*
Auth::routes();
 */

/*
 * Frontend Routes
// * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'middleware' => 'web'], function () {
    includeRouteFiles(__DIR__.'/Frontend/');
    Route::get('/debug', function () {
     return view('debug');
 });

    Route::get('/debugs', function () {
        return view('debug');
    });
});

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'as' => 'backend.', 'middleware' => ['web', 'backend']], function () {
    Route::get('/editor', 'HomeController@editor')->name('editor');
    Route::get('/home/statistics', 'HomeController@generalStatistics')->name('home.general_statistics');
    includeRouteFiles(__DIR__.'/Backend/');
});

Route::group(['namespace' => 'Backend', 'as' => 'backend.', 'middleware' => ['web', 'blank']], function () {
    Route::get('/blank', 'HomeController@blank')->name('blank');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::group(['namespace' => 'Access',], function() {
        Route::get('users/self_revoke_substitute/{user}', 'UserController@selfRevokeSubstitute')->name('users.self_revoke_substitute');
    });
});

//Api Routes
includeRouteFiles(__DIR__.'/Api/');


/* all routes which need a free access and do not need to go through any middleware */
require (__DIR__ . '/Open.php');
