<?php

//TODO ASK MIHAYO IF IT DOING ANYTING - IT BRINGS ERROR
//Route::get('anything','Controller@DiplayAnything');
Route::group(['namespace' => 'Auth'], function () {

    /*
     * These routes require the user to be logged in
     */
    Route::group(['middleware' => 'auth'], function () {
        Route::get('logout', 'LoginController@logout')->name('logout');
    });

    /*
        * These routes require no user to be logged in
        */
    Route::group(['middleware' => 'guest'], function () {
        // Authentication Routes
        Route::get('/', 'LoginController@showLoginForm')->name('login');
        Route::post('/login', 'LoginController@login');
         Route::get('/testmail', 'LoginController@testMail');
    });
});

/**
 * Frontend Access Controllers
 * All route names are prefixed with 'frontend.auth'.
 */
Route::group(['as' => 'frontend.'], function () {


});