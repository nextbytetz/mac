<?php

use App\Models\Location\Region;
use App\Models\Legal\District;
use App\Models\Finance\BankBranch;
use App\Models\Operation\Claim\MedicalPractitioner;
use App\Models\Operation\Compliance\Member\Employer;
use Illuminate\Support\Facades\DB;
use App\Services\Scopes\IsApprovedScope;
use App\Models\Finance\Bank;
use App\Models\Sysdef\CodeValueCode;
use App\Models\Sysdef\CodeValue;
use Illuminate\Http\Request;

Route::get('getbankbranch', function() {
    return view('includes.partials.bank_branches')
		    ->with("bank_branch", BankBranch::query()->where(request()->all())->get());
});
Route::get('getMedicalPractitioners', function() {
    return view('includes.partials.medical_practitioners')
        ->with("medical_practitioners", MedicalPractitioner::query()->where(request()->all())->get());
});
Route::get('guide', function() {
    return view('documentation/guide/index');
})->name("guide");

Route::get('guide_print', function() {
    return view('documentation/guide/index_print');
})->name("guide_print");

// Get regions
Route::get('getRegions', function() {
    return view('includes.partials.regions')
        ->with("regions", Region::query()->where(request()->all())->orderBy('name', 'asc')->get());
});

// Get districts
Route::get('getDistricts', function() {
    return view('includes.partials.districts')
        ->with("districts", District::query()->where(request()->all())->orderBy('name', 'asc')->get());
});

// Get code_values
Route::post('getCodeValues', function() {
    $input = request()->all();
    $code = CodeValueCode::query()->where($input)->first()->code;
    return view('includes.partials.code_values')
        ->with("code_values", CodeValue::query()->where("code_id", $code->id)->orderBy('name', 'asc')->get());
});

// Get List for Lost Body Parties
Route::get('getLostBodyParties', function() {
    $input = request()->all();
    $list = $input['body_party_injury'];
    if (empty($list) Or $list == "null") {
        $data = [];
    } else {
        $data = explode(",", $list);
    }
    return view('includes.partials.lost_body_parties')
        ->with("lost_body_parties", (new \App\Repositories\Backend\Operation\Claim\BodyPartInjuryRepository())->getForInjuryGroup($data));
});

// Get Parent Employers
Route::get('getParentEmployers', function() {
    return view('includes.partials.parent_employers')
    ->with("parent_employers", Employer::query()->where(DB::raw("regexp_replace(" . DB::raw('tin') . " , '[^a-zA-Z0-9]', '', 'g')"), '=', request()->input('tin'))->whereNotNull('tin')->withoutGlobalScopes([IsApprovedScope::class])->get());
});

Route::group(['namespace' => 'Backend\System'], function () {
    Route::get('/factory', 'FactoryController')->name('factory');
});

// Get districts
Route::get('getFinCodes', function() {
    $bank = Bank::query()->where(request()->all())->first();
    return view('includes.partials.fin_codes')
        ->with("fin_codes", $bank->finCodes()->select([
            DB::raw("fin_codes.id"),
            DB::raw("fin_codes.name")
        ])->get());
});

Route::post('getModuleGroup', function() {
    $id = '';
    $module = request()->input('module');
    if (!empty($module)) {
        $module_group = \App\Models\Workflow\WfModule::query()->select(['wf_module_group_id'])->where("id", $module);
        if ($module_group->count()) {
            $id = $module_group->first()->wf_module_group_id;
        }
    }
    return response()->json(['wf_module_group_id' => $id, 'wf_module_id' => (int) $module]);
});

Route::any('ViewerJS/{all?}', function(){
    return view('ViewerJS.index');

});

//get wards of certain district
Route::get('wards/{district_id}',function($district_id){
    $wards = DB::table('postcodes')
    ->select('ward_name','postcode')
    ->where('district_id',$district_id)
    ->get();
    return $wards;
});

//check if already have workplace
Route::post('check_workplace/{postcode}',function(Request $request,$postcode){
       $postcodes = DB::table('workplaces')
       ->select('id','workplace_regno','workplace_name')
       ->get();

       $array = array();
       foreach ($postcodes as $key => $value) {
        $post = explode('-', $value->workplace_regno);
        $employer = $post[0];
        $postc = $post[2];
        if ($employer == $request->employer_id && $postcode == $postc) {
            $array[] = $value;
        }
           
       }
       return $array;


});