<?php

/*
 * Backend Routes for APIs
 * Namespaces indicate folder structure
 */
//Updating claim documents from e-office
Route::group(['namespace' => 'Backend\Operation\Claim', 'prefix' => 'claim', 'as' => 'backend.', 'middleware' => 'api_dms'], function () {
    Route::post('notification_report/update_documents/{file_number}', 'NotificationReportController@updateDocuments')->name('claim.notification_report.update_documents');
    Route::post('notification_report/sync_incident/{incident}', 'NotificationReportController@syncIncident')->name('claim.notification_report.sync_incident');
    Route::post('notification_report/delete_dms_document/{incident}', 'NotificationReportController@deleteEofficeDocument')->name('claim.notification_report.delete_dms_document');
    Route::post('notification_report/document_uploaded_online', 'NotificationReportController@documentUploadedOnline')->name('claim.notification_report.document_uploaded_online');
    Route::post('notification_report/compensation_summary_api', 'NotificationReportController@compensationSummaryApi')->name('claim.notification_report.compensation_summary_api');
});

//Creating Receipt from Portal
Route::group(['namespace' => 'Backend\Finance', 'prefix' => 'finance', 'as' => 'backend.', 'middleware' => 'api_dms'], function () {
    Route::post('receipt/electronic', 'ReceiptController@storeElectronic')->name('finance.receipt.store_electronic');
    Route::post('administrative_receipt/electronic', 'ReceiptController@storeElectronicAdministrative')->name('finance.receipt.store_electronic_administrative');
      Route::post('employee/nin/verify', 'GotIntergrationController@fingerPrintVerification')->name('employee.nin.verify');

});

//Getting Notification from Portal
Route::group(['namespace' => 'Backend\Operation\Compliance\Member', 'prefix' => 'compliance', 'as' => 'backend.compliance.', 'middleware' => 'api_dms'], function () {
    Route::post('employer/incident_stage_update', 'EmployerController@onlineIncidentStageUpdate')->name('employer.incident_stage_update');
});