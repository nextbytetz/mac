<?php

return [	

	'bill' => [
		'waiting'=>1,
		'pending'=>2,
		'paid'=>3,
		'cancelled'=>4,
	],

	'MAC_ERP' => [
		'GL_TO_Q' => 'http://192.168.1.8/erpapi/public/api/gl/post_to_q/',
		'AP_TO_Q' => 'http://192.168.1.8/erpapi/public/api/ap/post_to_q/',
		'REFUND_TO_Q' => 'http://192.168.1.8/erpapi/public/api/refund/post_to_q/',
		'SUPPLIER_TO_Q' => 'http://192.168.1.8/erpapi/public/api/post_supplier_to_q/',
		'EMPLOYER_CR_GFS'=>'01.001.AC.000000',
		'EMPLOYER_DB_GFS'=>'01.001.AC.000000',
		'STAFF_CR_HQ'=>'01.001',
		'STAFF_CR_AC'=>'000',
		'STAFF_CR_FT1'=>'000',
		'STAFF_CR_FT2'=>'000',
		'THIRDPARTY_CR_GFS'=>'01.001.00.000000',
		'THIRDPARTY_DB_GFS'=>'01.001.00.000000',
		'GL_POSTED_Q'=>'200',
		'GL_POSTED_ERP'=>'400',
		'SP_GFS_CODE'=>'31221116',
		'SF_GFS_CODE'=>'31221115',
		'AP_POSTED_Q'=>'200',
		'AP_POSTED_ERP'=>'400',
	],

	'MOF' => [
		'CLIENT_PASSWORD'=>'apisecret',
		'CLIENT'=>'WCF',
		'GRANT_TYPE'=>'client_credentials',
		// 'BASE_URL'=> 'http://154.118.230.147',
		'BASE_URL'=> 'gsppapi.mof.go.tz',
		'TOKEN_URL' => 'identityserver.mof.go.tz/connect/token',
		'CONTRIBUTION_HEADER_URL' => '/api/contributions/getcontributionheader?checkDate=',
		'CONTRIBUTION_DETAILS_URL' => '/api/contributions/getcontributiondetails?checkDate=',
		'EMPLOYEES_HEADER_URL' => '/api/employees/getemployeeheader?checkdate=',
		'EMPLOYEES_BIODATA_URL' => '/api/employees/getemployeedetails?checkdate=',
	],

	'user'=>[
		'daily_audit_trail_path'=>'public/audit_trail/',
	],
	'mou_file'=>[
		'portal_instalment_mou'=>env('PORTAL_INSTALMENT_MOU', ''),
	],

	'MAC_GEPG' => [
		'BILL_TO_Q' => env('BILL_URL', ''),
	],
	'MAC_ERMS' => [
		'erms_api' => env('ERMS_API', ''),
	],
];
