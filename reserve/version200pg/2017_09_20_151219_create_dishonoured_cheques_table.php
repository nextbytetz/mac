<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDishonouredChequesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dishonoured_cheques', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('receipt_id')->unsigned()->index()->comment('Receipt associated with the cheque');
			$table->integer('new_bank_id')->unsigned()->nullable()->index()->comment('Bank of the replaced cheque');
			$table->integer('old_bank_id')->unsigned()->index()->comment('Bank of the dishonoured cheque');
			$table->string('new_cheque_no', 20)->nullable()->comment('Replaced cheque no');
			$table->string('old_cheque_no', 20)->comment('Dishonoured cheque no');
			$table->integer('dishonour_user_id')->unsigned()->index();
			$table->string('dishonour_reason', 100)->comment('Reasons for dishonouring cheque');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('replacer_user_id')->unsigned()->nullable()->index()->comment('user who replaces the cheque');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dishonoured_cheques');
	}

}
