<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBookingInterestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking_interests', function(Blueprint $table)
		{
			$table->foreign('booking_id')->references('id')->on('bookings')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('interest_write_off_id')->references('id')->on('interest_write_offs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking_interests', function(Blueprint $table)
		{
			$table->dropForeign('booking_interests_booking_id_foreign');
			$table->dropForeign('booking_interests_interest_write_off_id_foreign');
		});
	}

}
