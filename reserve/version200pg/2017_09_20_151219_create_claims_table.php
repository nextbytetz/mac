<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClaimsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claims', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_report_id')->unsigned()->index();
			$table->integer('incident_type_id')->unsigned()->index();
			$table->decimal('monthly_earning', 14)->comment('recent salary amount.');
			$table->date('contrib_month')->comment('Contribution month of gross monthly earning before incident date');
			$table->tinyInteger('day_hours')->nullable()->comment('hours worked per day by a member');
			$table->integer('exit_code_id')->unsigned()->nullable()->index();
			$table->decimal('pd', 10, 7)->nullable()->default(0.0000000)->comment('Percentage of permanent disability for this claim notification');
			$table->tinyInteger('iscomplete')->default(0)->comment('set whether is complete or not, 1 - claim process is complete, 0 - claim process is not complete');
			$table->integer('user_id')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claims');
	}

}
