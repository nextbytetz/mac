<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentNotificationReportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_notification_report', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_report_id')->unsigned()->index();
			$table->integer('document_id')->unsigned()->index();
			$table->string('name', 150)->nullable()->comment('original name of the document');
			$table->text('description', 65535)->nullable();
			$table->string('ext', 20)->nullable()->comment('file extension of the uploaded document');
			$table->float('size', 10, 0)->nullable()->comment('size of the uploaded file');
			$table->string('mime', 150)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_notification_report');
	}

}
