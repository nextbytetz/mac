<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 150)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->text('description', 65535)->nullable();
			$table->tinyInteger('active')->nullable()->default(1)->comment('determine whether the role is active or not, 1 - active, 0 - not active');
			$table->tinyInteger('all_functions')->nullable()->default(0)->comment('determine whether the role is comprised of all permissions');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles');
	}

}
