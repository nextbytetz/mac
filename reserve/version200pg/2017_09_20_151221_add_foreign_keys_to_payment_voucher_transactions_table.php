<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPaymentVoucherTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payment_voucher_transactions', function(Blueprint $table)
		{
			$table->foreign('benefit_type_id')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('payment_voucher_id')->references('id')->on('payment_vouchers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payment_voucher_transactions', function(Blueprint $table)
		{
			$table->dropForeign('payment_voucher_transactions_benefit_type_id_foreign');
			$table->dropForeign('payment_voucher_transactions_payment_voucher_id_foreign');
		});
	}

}
