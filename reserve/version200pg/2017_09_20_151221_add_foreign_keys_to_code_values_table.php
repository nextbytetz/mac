<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCodeValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('code_values', function(Blueprint $table)
		{
			$table->foreign('code_id')->references('id')->on('codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('code_values', function(Blueprint $table)
		{
			$table->dropForeign('code_values_code_id_foreign');
		});
	}

}
