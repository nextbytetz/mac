<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePensionersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pensioners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('employee_id')->unsigned()->index();
			$table->integer('benefit_type_id')->unsigned()->index();
			$table->decimal('monthly_pension_amount', 14)->comment('amount of monthly pension the pensioner is eligible to be paid.');
			$table->integer('compensation_payment_type_id')->unsigned()->index();
			$table->integer('recycl')->nullable()->comment('in months, number of cycled this compensation will be paid');
			$table->integer('pay_period')->nullable()->comment('in months, number of months this compensation will be paid');
			$table->tinyInteger('isactive')->default(0)->comment('specify whether the pensioners is active to receive monthly pension');
			$table->tinyInteger('isresponded')->default(0)->comment('specify whether the pensioner has responded to the claim compensation award to confirm bank details and his/her information');
			$table->tinyInteger('firstpay_flag')->default(0)->comment('specify whether the pensioner has already started to receive monthly pension');
			$table->tinyInteger('suspense_flag')->default(0)->comment('set whether pensioner is suspended or not, 1 - suspended, 0 - not suspended.');
			$table->dateTime('lastresponse')->comment('last verified date');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pensioners');
	}

}
