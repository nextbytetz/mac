<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receipts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rctno')->unsigned()->nullable()->comment('Special Receipt number');
			$table->integer('employer_id')->unsigned()->nullable()->index()->comment('Employer who has paid for the receipt');
			$table->string('payer', 200)->nullable()->comment('any other payer who has made the payments.');
			$table->text('description', 65535)->nullable()->comment('any specific description about this payment.');
			$table->string('chequeno', 50)->nullable()->comment('chequeno from the bank in case payment made in bank by cheque');
			$table->date('rct_date')->index()->comment('Receipt date');
			$table->string('bank_reference', 20)->nullable()->comment('to control for double entry of the same receipt.');
			$table->decimal('amount', 14)->comment('exact amount paid');
			$table->integer('currency_id')->unsigned()->index();
			$table->integer('payment_type_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->index();
			$table->integer('office_id')->unsigned()->default(1)->index();
			$table->integer('bank_id')->unsigned()->index();
			$table->integer('fin_code_id')->unsigned()->nullable()->index();
			$table->tinyInteger('iscancelled')->default(0)->comment('specify whether this receipt is cancelled or not');
			$table->text('cancel_reason', 65535)->nullable()->comment('reason for cancelling receipt');
			$table->integer('cancel_user_id')->unsigned()->nullable()->comment('user who has cancelled the receipt');
			$table->dateTime('cancel_date')->nullable()->comment('date which the receipt has been canceled');
			$table->tinyInteger('ssyes')->default(0)->comment('specify whether data has been exported to the finance software');
			$table->tinyInteger('isdishonoured')->default(0)->comment('Specify whether the receipt has been dishonored.');
			$table->tinyInteger('iscomplete')->default(0)->comment('Specify whether the receipt has reached the final stage including contribution upload');
			$table->tinyInteger('isverified')->nullable()->default(0)->comment('Set whether this receipt has already been verified or not');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receipts');
	}

}
