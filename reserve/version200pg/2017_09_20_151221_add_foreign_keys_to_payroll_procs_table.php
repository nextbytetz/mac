<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPayrollProcsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payroll_procs', function(Blueprint $table)
		{
			$table->foreign('payroll_proc_type_id')->references('id')->on('payroll_proc_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payroll_procs', function(Blueprint $table)
		{
			$table->dropForeign('payroll_procs_payroll_proc_type_id_foreign');
		});
	}

}
