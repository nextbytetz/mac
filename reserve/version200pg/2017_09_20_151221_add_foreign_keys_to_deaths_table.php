<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDeathsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('deaths', function(Blueprint $table)
		{
			$table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('death_cause_id')->references('id')->on('death_causes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('deaths', function(Blueprint $table)
		{
			$table->dropForeign('deaths_notification_report_id_foreign');
			$table->dropForeign('deaths_death_cause_id_foreign');
			$table->dropForeign('deaths_district_id_foreign');
		});
	}

}
