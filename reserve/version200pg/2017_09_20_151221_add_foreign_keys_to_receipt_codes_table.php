<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReceiptCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receipt_codes', function(Blueprint $table)
		{
			$table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fin_code_id')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('booking_id')->references('id')->on('bookings')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receipt_codes', function(Blueprint $table)
		{
			$table->dropForeign('receipt_codes_receipt_id_foreign');
			$table->dropForeign('receipt_codes_fin_code_id_foreign');
			$table->dropForeign('receipt_codes_booking_id_foreign');
		});
	}

}
