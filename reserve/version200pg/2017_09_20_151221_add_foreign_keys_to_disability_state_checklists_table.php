<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDisabilityStateChecklistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('disability_state_checklists', function(Blueprint $table)
		{
			$table->foreign('benefit_type_id')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('disability_state_checklists', function(Blueprint $table)
		{
			$table->dropForeign('disability_state_checklists_benefit_type_id_foreign');
		});
	}

}
