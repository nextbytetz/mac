<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('text_messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('phone', 45);
			$table->text('message', 65535);
			$table->tinyInteger('status')->default(0)->comment('set the sent status, 1 - sent, 0 - not sent/pending');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('text_messages');
	}

}
