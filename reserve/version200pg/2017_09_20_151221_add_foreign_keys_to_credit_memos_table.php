<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCreditMemosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('credit_memos', function(Blueprint $table)
		{
			$table->foreign('booking_id')->references('id')->on('bookings')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('credit_memos', function(Blueprint $table)
		{
			$table->dropForeign('credit_memos_booking_id_foreign');
		});
	}

}
