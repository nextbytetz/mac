<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLegacyReceiptsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('legacy_receipts_codes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('legacy_receipt_id')->unsigned()->index();
			$table->decimal('amount', 14)->comment('contribution amount');
			$table->date('contrib_month')->nullable();
			$table->integer('rows_imported')->nullable()->comment('Total number of rows from the imported excel file which has already been stored in the database table (contribution_temps), if this receipt is for monthly contribution');
			$table->decimal('amount_imported', 14)->nullable()->comment('total amount computed from the gross pay in the uploaded file. After the linked file has been uploaded.');
			$table->integer('member_count')->nullable()->comment('total number of members being paid for in case receipt for contribution');
			$table->string('mime', 150)->nullable()->comment('mime type of the uploaded contribution file');
			$table->text('upload_error', 65535)->nullable()->comment('store error report when the file upload failed from the queue jobs');
			$table->smallInteger('isuploaded')->nullable()->default(0)->comment('store the status whether the contributions for this receipt has already been uploaded or not yet, 1 - already updated, 0 - not yet uploaded completely');
			$table->smallInteger('error')->nullable()->default(0)->comment('show whether the associated uploaded file has error or not');
			$table->float('size', 10, 0)->nullable()->comment('size of the uploaded contribution file');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('legacy_receipts_codes');
	}

}
