<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPensionersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pensioners', function(Blueprint $table)
		{
			$table->foreign('benefit_type_id')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('compensation_payment_type_id')->references('id')->on('compensation_payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pensioners', function(Blueprint $table)
		{
			$table->dropForeign('pensioners_benefit_type_id_foreign');
			$table->dropForeign('pensioners_compensation_payment_type_id_foreign');
            $table->dropForeign('pensioners_employee_id_foreign');
		});
	}

}
