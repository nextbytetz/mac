<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserWfDefinitionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_wf_definition', function(Blueprint $table)
		{
			$table->foreign('wf_definition_id')->references('id')->on('wf_definitions')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_wf_definition', function(Blueprint $table)
		{
			$table->dropForeign('user_wf_definition_wf_definition_id_foreign');
		});
	}

}
