<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployerInspectionTaskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employer_inspection_task', function(Blueprint $table)
		{
			$table->foreign('inspection_task_id')->references('id')->on('inspection_tasks')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employer_inspection_task', function(Blueprint $table)
		{
			$table->dropForeign('employer_inspection_task_inspection_task_id_foreign');
            $table->dropForeign('employer_inspection_task_user_id_foreign');
            $table->dropForeign('employer_inspection_task_employer_id_foreign');
		});
	}

}
