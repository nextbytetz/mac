<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationDisabilityStateAssessmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_disability_state_assessments', function(Blueprint $table)
		{
			$table->foreign('notification_disability_state_id')->references('id')->on('notification_disability_states')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_disability_state_assessments', function(Blueprint $table)
		{
			$table->dropForeign('notification_disability_state_assessments_notification_disability_state_id_foreign');
            $table->dropForeign('notification_disability_state_assessments_user_id_foreign');
		});
	}

}
