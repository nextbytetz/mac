<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receipts', function(Blueprint $table)
		{
			$table->foreign('payment_type_id')->references('id')->on('payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('office_id')->references('id')->on('offices')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('currencies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fin_code_id')->references('id')->on('fin_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receipts', function(Blueprint $table)
		{
			$table->dropForeign('receipts_payment_type_id_foreign');
			$table->dropForeign('receipts_office_id_foreign');
			$table->dropForeign('receipts_bank_id_foreign');
			$table->dropForeign('receipts_currency_id_foreign');
			$table->dropForeign('receipts_fin_code_id_foreign');
            $table->dropForeign('receipts_employer_id_foreign');
		});
	}

}
