<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAccidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('accidents', function(Blueprint $table)
		{
			$table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('accident_type_id')->references('id')->on('accident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('accidents', function(Blueprint $table)
		{
			$table->dropForeign('accidents_notification_report_id_foreign');
			$table->dropForeign('accidents_accident_type_id_foreign');
            $table->dropForeign('accidents_user_id_foreign');
		});
	}

}
