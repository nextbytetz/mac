<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPaymentVouchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payment_vouchers', function(Blueprint $table)
		{
			$table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('payroll_proc_id')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payment_vouchers', function(Blueprint $table)
		{
			$table->dropForeign('payment_vouchers_bank_branch_id_foreign');
			$table->dropForeign('payment_vouchers_bank_id_foreign');
			$table->dropForeign('payment_vouchers_payroll_proc_id_foreign');
		});
	}

}
