<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaseHearingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_hearings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('case_personnel_id')->unsigned()->nullable()->index();
			$table->string('firstname', 40)->comment('first name of the case personnel during the hearing');
			$table->string('middlename', 40)->nullable()->comment('middle name of the case personnel during the hearing');
			$table->string('lastname', 40)->comment('lastname name of the case personnel during the hearing');
			$table->integer('case_id')->unsigned()->index();
			$table->date('hearing_date')->nullable();
			$table->time('hearing_time');
			$table->integer('lawyer_id')->unsigned()->index();
			$table->decimal('fee', 14)->nullable();
			$table->date('filling_date')->nullable();
			$table->smallInteger('status')->default(0)->comment('set the status whether the case has been heard or not, 1 - case has been heard, 0 case has not been heard., 2 - case has been cancelled.');
			$table->text('status_description', 65535)->nullable()->comment('More explanations about the status selected');
			$table->integer('user_id')->unsigned()->nullable()->comment('system user who has created a case hearing');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_hearings');
	}

}
