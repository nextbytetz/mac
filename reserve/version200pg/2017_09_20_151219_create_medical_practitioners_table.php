<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicalPractitionersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medical_practitioners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('firstname', 45);
			$table->string('middlename', 45)->nullable();
			$table->string('lastname', 45);
			$table->string('address', 150)->nullable();
			$table->string('national_id', 45)->nullable();
			$table->string('phone', 45);
			$table->string('external_id', 150);
			$table->timestamps();
			$table->unique(['phone','external_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medical_practitioners');
	}

}
