<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLegacyReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('legacy_receipts', function(Blueprint $table)
		{
			$table->foreign('payment_type_id')->references('id')->on('payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('legacy_receipts', function(Blueprint $table)
		{
			$table->dropForeign('legacy_receipts_payment_type_id_foreign');
            $table->dropForeign('legacy_receipts_user_id_foreign');
            $table->dropForeign('legacy_receipts_employer_id_foreign');
		});
	}

}
