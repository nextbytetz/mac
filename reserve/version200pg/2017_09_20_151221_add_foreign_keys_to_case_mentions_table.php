<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCaseMentionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('case_mentions', function(Blueprint $table)
		{
			$table->foreign('case_id')->references('id')->on('cases')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('case_personnel_id')->references('id')->on('case_personnels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('lawyer_id')->references('id')->on('lawyers')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('case_mentions', function(Blueprint $table)
		{
			$table->dropForeign('case_mentions_case_id_foreign');
			$table->dropForeign('case_mentions_case_personnel_id_foreign');
			$table->dropForeign('case_mentions_lawyer_id_foreign');
            $table->dropForeign('case_mentions_user_id_foreign');
		});
	}

}
