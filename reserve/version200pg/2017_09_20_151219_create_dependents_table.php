<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDependentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dependents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('employee_id')->unsigned()->index();
			$table->string('firstname', 45);
			$table->string('middlename', 45)->nullable();
			$table->string('lastname', 45);
			$table->integer('dependent_type_id')->unsigned()->index();
			$table->integer('identity_id')->unsigned()->index();
			$table->string('id_no', 45);
			$table->string('address', 150)->nullable();
			$table->date('dob');
			$table->integer('gender_id')->unsigned()->index();
			$table->integer('country_id')->unsigned()->index();
			$table->integer('location_type_id')->unsigned()->default(2)->index();
			$table->text('unsurveyed_area', 65535)->nullable();
			$table->string('street', 150)->nullable();
			$table->string('road', 150)->nullable();
			$table->string('plot_no', 150)->nullable();
			$table->string('block_no', 150)->nullable();
			$table->text('surveyed_extra', 65535)->nullable();
			$table->string('phone', 45)->nullable();
			$table->string('telephone', 45)->nullable();
			$table->string('fax', 45)->nullable();
			$table->string('email', 45)->nullable();
			$table->integer('bank_branch_id')->unsigned()->nullable()->index();
            $table->string('accountno', 45)->nullable();
			$table->decimal('survivor_pension_percent', 5)->nullable();
			$table->decimal('survivor_pension_amount', 15)->nullable()->comment('amount of pension the dependent is eligible to be paid.');
			$table->decimal('survivor_gratuity_percent', 5)->nullable();
			$table->tinyInteger('survivor_gratuity_flag')->default(0)->comment('Determine whether is receiving gratuity / lump sum or not, 1 - yes receiving, 0 - not receiving ');
			$table->tinyInteger('survivor_gratuity_pay_flag')->default(0)->comment('Determine whether has already been paid gratuity / lump sum or not, 1 - processed / paid, 0 - not yet paid / processed ');
			$table->tinyInteger('suspense_flag')->default(0)->comment('set whether dependent is suspended or not, 1 - suspended, 0 - not suspended.');
			$table->tinyInteger('survivor_pension_flag')->default(0)->comment('	Determine whether is receiving pension or not, e.g children taken care by an administrator might not be receiving pension. 1 - yes receiving, 0 - not receiving ');
			$table->decimal('funeral_grant_percent', 5)->nullable();
			$table->decimal('funeral_grant', 14)->nullable();
			$table->tinyInteger('funeral_grant_pay_flag')->default(0)->comment('set whether dependent has already been paid funeral grant or not, 1 - paid, 0 - not paid yet');
			$table->decimal('survivor_gratuity_amount', 14)->nullable()->comment('amount of gratuity the dependent is eligible to be paid.');
			$table->decimal('dependency_percentage', 5)->nullable()->comment('Percentage of dependency of Other dependents i.e dependents other than children and spouses');
			$table->dateTime('lastresponse')->comment('last verified date');
			$table->tinyInteger('isactive')->default(0)->comment('specify whether the dependent is active to receive monthly pension');
			$table->tinyInteger('isresponded')->default(0)->comment('specify whether the dependent has responded to the claim compensation award to confirm bank details and his/her information');
			$table->tinyInteger('firstpay_flag')->default(0)->comment('specify whether the dependent has already started to receive monthly pension');
			$table->integer('parent_id')->unsigned()->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dependents');
	}

}
