<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receipt_codes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('receipt_id')->unsigned();
			$table->integer('fin_code_id')->unsigned()->index();
			$table->decimal('amount', 14);
			$table->integer('member_count')->nullable()->comment('total number of members being paid for in case receipt for contribution');
			$table->date('contrib_month')->nullable()->index()->comment('month of the contribution in case receipt for contribution');
			$table->integer('booking_id')->unsigned()->nullable()->index();
			$table->string('linked_file', 25)->nullable()->comment('Linked file attached to a receipt code i.e. contributions');
			$table->smallInteger('rows_imported')->nullable()->default(0)->comment('Total number of rows from the imported excel file which has already been stored in the database table (contribution_temps), if this receipt is for monthly contribution');
			$table->smallInteger('total_rows')->nullable()->default(0)->comment('Total number of rows from the imported excel file, if this receipt is for monthly contribution');
			$table->string('mime', 150)->nullable()->comment('mime type of the uploaded contribution file');
			$table->decimal('amount_imported', 14)->nullable()->default(0.00)->comment('total amount computed from the gross pay in the uploaded file. After the linked file has been uploaded.');
			$table->text('upload_error', 65535)->nullable()->comment('store error report when the file upload failed from the queue jobs');
			$table->tinyInteger('isuploaded')->nullable()->default(0)->comment('store the status whether the contributions for this receipt has already been uploaded or not yet, 1 - already updated, 0 - not yet uploaded completely');
			$table->tinyInteger('error')->nullable()->default(0)->comment('show whether the associated uploaded file has error or not');
			$table->timestamps();
			$table->softDeletes();
			$table->float('size', 10, 0)->nullable()->comment('size of the uploaded contribution file');
			$table->index(['receipt_id','fin_code_id','booking_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receipt_codes');
	}

}
