<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentVouchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_vouchers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->decimal('amount', 14);
			$table->integer('bank_id')->unsigned()->index();
			$table->integer('bank_branch_id')->unsigned()->index();
			$table->string('accountno', 45);
			$table->string('reference', 45)->nullable()->comment('Payment Voucher reference');
			$table->tinyInteger('is_paid')->default(0)->comment('Flag to show whether this payment voucher has already been dispatched / paid to payee');
			$table->integer('payroll_proc_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->index()->comment('Staff user who has created this payment voucher');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_vouchers');
	}

}
