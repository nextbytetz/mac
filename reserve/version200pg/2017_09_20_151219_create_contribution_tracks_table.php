<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContributionTracksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contribution_tracks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('receipt_code_id')->unsigned()->index();
			$table->integer('user_id')->unsigned();
			$table->text('comments', 65535)->nullable()->comment('comment about the tracking.');
			$table->text('todo', 65535)->nullable()->comment('possible steps to be taken following the comment which has been suggested.');
			$table->tinyInteger('status')->default(0)->comment('set the status of the contribution tracking, 1 - already worked, 0 - not yet worked.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contribution_tracks');
	}

}
