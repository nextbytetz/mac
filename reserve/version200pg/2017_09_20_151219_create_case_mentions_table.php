<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaseMentionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_mentions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('case_personnel_id')->unsigned()->index();
			$table->integer('case_id')->unsigned()->index();
			$table->integer('lawyer_id')->unsigned()->index();
			$table->string('firstname', 40)->comment('first name of the case personnel during the mention');
			$table->string('middlename', 40)->nullable()->comment('middle name of the case personnel during the mention');
			$table->string('lastname', 40)->comment('lastname name of the case personnel during the mention');
			$table->date('mention_date');
			$table->time('mention_time');
			$table->text('description', 65535)->nullable()->comment('Any associated description concerning the case mentioning');
			$table->integer('user_id')->unsigned()->nullable()->comment('system user who has created a case mention');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_mentions');
	}

}
