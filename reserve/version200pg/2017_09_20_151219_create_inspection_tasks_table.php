<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inspection_tasks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('inspection_id')->unsigned()->index();
			$table->text('name', 65535)->comment('name of the task');
			$table->text('deliverable', 65535)->nullable()->comment('deliverable of the specified task');
			$table->integer('inspection_profile_id')->unsigned()->nullable()->index()->comment('in case this task was selected for inspection profile');
			$table->integer('employer_count')->nullable()->comment('number of employers in case the inspection task was selected from a inspection profile');
			$table->integer('last_id')->nullable()->comment('last employer_id of the selected count of employers in case inspection profile was selected for this inspection task');
			$table->smallInteger('target_employer')->nullable()->comment('Check the target employers concerned in this inspection task, 1 - for Inspection Profile, 0 - Individual Employers, 2 - None, employers who are not registered to WCF by the time of this task where selected');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inspection_tasks');
	}

}
