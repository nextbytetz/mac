<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('username', 45)->nullable()->unique();
            $table->string('samaccountname', 45)->nullable()->unique()->comment("Microsoft Active Directory Username");
            $table->string('password', 100)->nullable();
            $table->string('firstname', 45)->nullable();
            $table->string('lastname', 45)->nullable();
            $table->string('middlename', 45)->nullable();
			$table->string('email', 45)->nullable();
			$table->string('phone', 45)->nullable();
			$table->integer('unit_id')->unsigned()->nullable()->index();
            $table->integer('designation_id')->unsigned()->nullable()->index();
			$table->integer('gender_id')->unsigned()->nullable()->index();
			$table->string('external_id', 20)->nullable()->comment('any unique reference used to refer for this user');
			$table->tinyInteger('active')->default(1)->comment('set whether a user is active or not, 1 - active, 0 - not active');
			$table->tinyInteger('available')->default(1)->comment('set whether a user is available in an office or not, 1 available, 0 not available');
			$table->dateTime('last_login')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created')->nullable();
			$table->integer('modified_by')->nullable();
			$table->dateTime('modified')->nullable();
			$table->integer('task_id')->default(0);
            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
