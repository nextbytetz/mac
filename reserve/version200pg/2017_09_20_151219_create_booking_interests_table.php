<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingInterestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_interests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('booking_id')->unsigned()->index();
			$table->date('miss_month')->comment('missing month which the interest is being charged.');
			$table->integer('late_months')->comment('number of months which the contributions has been delayed.');
			$table->decimal('interest_percent', 4)->comment('interest percent charged for this penalty');
			$table->decimal('amount', 14);
			$table->tinyInteger('isadjusted')->default(0)->comment('set whether this penalty is waived or not, 1 - waived, 0 - not waived.');
			$table->tinyInteger('ispaid')->default(0)->comment('specify whether the interest amount has been paid or not.');
			$table->tinyInteger('hasreceipt')->nullable()->default(0)->comment('Flag to show if booking has a receipt / paid');
			$table->dateTime('billing_date')->nullable()->comment('date which the interest has been billed');
			$table->integer('iswrittenoff')->unsigned()->nullable()->default(0)->comment('Flag to imply interest is written off');
			$table->integer('interest_write_off_id')->unsigned()->nullable()->index()->comment('Batch id for interest written off');
			$table->decimal('adjust_amount', 14)->nullable()->comment('Specify amount which has been waived.');
			$table->text('adjust_note', 65535)->nullable()->comment('Note on the waived amount');
			$table->integer('adjust_user_id')->unsigned()->nullable()->comment('staff who has waived the interest amount.');
			$table->date('new_payment_date')->nullable()->comment('New modified  payment date of receipt for contribution of this booking.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_interests');
	}

}
