<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeCountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_counts', function(Blueprint $table)
		{
			$table->foreign('employee_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gender_id')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_counts', function(Blueprint $table)
		{
			$table->dropForeign('employee_counts_employee_category_cv_id_foreign');
			$table->dropForeign('employee_counts_gender_id_foreign');
            $table->dropForeign('employee_counts_employer_id_foreign');
		});
	}

}
