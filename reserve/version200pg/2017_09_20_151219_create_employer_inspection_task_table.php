<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployerInspectionTaskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employer_inspection_task', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('employer_id')->unsigned()->nullable()->index();
			$table->string('employer_name', 191)->nullable();
			$table->integer('inspection_task_id')->unsigned()->index();
			$table->date('visit_date')->nullable()->index();
			$table->integer('user_id')->unsigned()->nullable()->index()->comment('Staff user who has been assigned to do the employer inspection task');
			$table->text('resolutions', 65535)->nullable()->comment('a formal expression of opinion or intention agreed after inspection');
			$table->text('findings', 65535)->nullable()->comment('Extra findings that might have been discovered during inspection, which were not part pf the inspection tasks and targets.');
			$table->tinyInteger('status')->default(0)->comment('set the status whether the inspection task has completed or not, 1 - inspection task has completed, 0 - inspection task has not yet completed.');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employer_inspection_task');
	}

}
