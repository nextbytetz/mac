<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100)->unique()->comment('Name of the report');
			$table->string('url_name', 100)->unique()->comment('URL name field to be included in route url to call method for this report i.e /reports/finance/{url_name}');
			$table->integer('report_type_id')->unsigned()->index();
			$table->integer('report_category_id')->unsigned()->index();
			$table->text('description', 65535)->comment('description of the report and result to be retrieved');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reports');
	}

}
