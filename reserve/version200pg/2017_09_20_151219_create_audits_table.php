<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuditsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('audits', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable()->index();
			$table->string('event', 191);
			$table->integer('auditable_id')->unsigned();
			$table->string('auditable_type', 191);
			$table->text('old_values', 65535)->nullable();
			$table->text('new_values', 65535)->nullable();
			$table->string('url', 191)->nullable();
			$table->string('ip_address', 45)->nullable();
			$table->string('user_agent', 191)->nullable();
			$table->timestamps();
			$table->index(['auditable_id','auditable_type']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('audits');
	}

}
