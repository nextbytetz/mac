<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHealthProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('health_providers', function(Blueprint $table)
		{
			$table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('health_providers', function(Blueprint $table)
		{
			$table->dropForeign('health_providers_district_id_foreign');
            $table->dropForeign('health_providers_user_id_foreign');
		});
	}

}
