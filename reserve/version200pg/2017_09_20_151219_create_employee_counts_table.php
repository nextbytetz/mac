<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeCountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_counts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('employer_id')->unsigned()->nullable();
			$table->integer('employee_category_cv_id')->unsigned()->nullable()->index();
			$table->integer('gender_id')->unsigned()->nullable()->index();
			$table->integer('count')->nullable();
			$table->decimal('annual_earning', 14)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_counts');
	}

}
