<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employers', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 150);
            $table->string('reg_no', 500)->nullable();
            $table->dateTime('doc')->nullable()->comment('date of commencement of the business');
            $table->string('tin', 100)->nullable()->comment('TRA TIN');
            $table->string('phone', 45)->nullable();
            $table->string('email', 45)->nullable();
            $table->string('fax', 45)->nullable();
            $table->string('vote', 100)->nullable();
            $table->decimal('annual_earning', 14)->nullable();
            $table->integer('district_id')->unsigned()->nullable()->index();
            $table->integer('employer_category_cv_id')->unsigned()->nullable()->index();
            $table->integer('bank_branch_id')->unsigned()->nullable()->index();
            $table->string('accountno', 45)->nullable();
            $table->integer('location_type_id')->unsigned()->nullable()->index();
            $table->string('road', 150)->nullable();
            $table->string('street', 45)->nullable();
            $table->string('plot_no', 45)->nullable();
            $table->string('block_no', 45)->nullable();
            $table->text('surveyed_extra', 65535)->nullable();
            $table->text('unsurveyed_area', 65535)->nullable();
			$table->integer('region_id')->nullable();
			$table->string('district', 45)->nullable();
			$table->integer('representer_id')->unsigned()->nullable()->index();
			$table->integer('created_by')->nullable();
			$table->integer('modified_by')->nullable();
			$table->dateTime('modified')->nullable();
			$table->integer('approval_id')->nullable()->default(2);
			$table->text('reject_reason', 65535)->nullable();
			$table->integer('emp_approved_by')->nullable();
			$table->date('emp_approved_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employers');
	}

}
