<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reports', function(Blueprint $table)
		{
			$table->foreign('report_category_id')->references('id')->on('report_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('report_type_id')->references('id')->on('report_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reports', function(Blueprint $table)
		{
			$table->dropForeign('reports_report_category_id_foreign');
			$table->dropForeign('reports_report_type_id_foreign');
		});
	}

}
