<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationHealthProviderServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_health_provider_services', function(Blueprint $table)
		{
			$table->foreign('notification_health_provider_id')->references('id')->on('notification_health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('health_service_checklist_id')->references('id')->on('health_service_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_health_provider_services', function(Blueprint $table)
		{
			$table->dropForeign('notification_health_provider_services_notification_health_provider_id_foreign');
			$table->dropForeign('notification_health_provider_services_health_service_checklist_id_foreign');
            $table->dropForeign('notification_health_provider_services_user_id_foreign');
		});
	}

}
