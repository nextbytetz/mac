<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentVoucherTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_voucher_transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('benefit_resource_id')->unsigned()->comment('Primary key of the benefit resource table, a table which is being tracked for.');
			$table->integer('member_type_id')->unsigned()->index()->comment('which member type this compensation is concerned');
			$table->integer('resource_id')->unsigned()->comment('Primary key of the resource table, a table which is being tracked for.');
			$table->integer('benefit_type_id')->unsigned()->nullable()->index();
			$table->decimal('amount', 14);
			$table->tinyInteger('is_exported')->default(0)->comment('Flag to show whether this payment voucher has already been exported to finance system');
			$table->tinyInteger('is_processed')->default(0)->comment('Flag to show whether this payment voucher transaction has already been processed to Payment Voucher');
			$table->integer('payment_voucher_id')->unsigned()->nullable()->index();
			$table->integer('user_id')->unsigned()->index()->comment('Staff user who has created this payment voucher');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_voucher_transactions');
	}

}
