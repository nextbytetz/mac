<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationDisabilityStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notification_disability_states', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('medical_expense_id')->unsigned()->index();
			$table->integer('disability_state_checklist_id')->unsigned()->index();
			$table->dateTime('from_date')->nullable()->comment('date from which the member has had the health state outlined.');
			$table->dateTime('to_date')->nullable()->comment('date from which the member has had the health state outlined.');
			$table->integer('days')->nullable()->comment('No of Days for each disability state. i.e actual hospitalization days.');
			$table->decimal('percent_of_ed_ld', 6)->nullable()->comment('Percentage of Excuse duty (ED) / Light duties (LD)');
			$table->integer('user_id')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notification_disability_states');
	}

}
