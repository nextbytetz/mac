<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationDisabilityStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_disability_states', function(Blueprint $table)
		{
			$table->foreign('medical_expense_id')->references('id')->on('medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('disability_state_checklist_id')->references('id')->on('disability_state_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_disability_states', function(Blueprint $table)
		{
			$table->dropForeign('notification_disability_states_medical_expense_id_foreign');
			$table->dropForeign('notification_disability_states_disability_state_checklist_id_foreign');
            $table->dropForeign('notification_disability_states_user_id_foreign');
		});
	}

}
