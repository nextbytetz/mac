<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmploymentHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employment_histories', function(Blueprint $table)
		{
			$table->foreign('job_title_id')->references('id')->on('job_titles')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('insurance_id')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employer_id')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employment_histories', function(Blueprint $table)
		{
			$table->dropForeign('employment_histories_job_title_id_foreign');
			$table->dropForeign('employment_histories_insurance_id_foreign');
            $table->dropForeign('employment_histories_employee_id_foreign');
            $table->dropForeign('employment_histories_employer_id_foreign');
		});
	}

}
