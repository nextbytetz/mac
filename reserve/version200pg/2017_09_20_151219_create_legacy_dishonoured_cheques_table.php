<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLegacyDishonouredChequesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('legacy_dishonoured_cheques', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('legacy_receipt_id')->nullable();
			$table->integer('status')->default(0);
			$table->integer('assigned_to')->nullable();
			$table->date('assigned_date')->nullable();
			$table->text('updates', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('legacy_dishonoured_cheques');
	}

}
