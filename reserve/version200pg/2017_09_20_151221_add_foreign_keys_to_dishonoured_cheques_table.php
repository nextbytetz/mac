<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDishonouredChequesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dishonoured_cheques', function(Blueprint $table)
		{
			$table->foreign('receipt_id')->references('id')->on('receipts')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dishonoured_cheques', function(Blueprint $table)
		{
			$table->dropForeign('dishonoured_cheques_receipt_id_foreign');
		});
	}

}
