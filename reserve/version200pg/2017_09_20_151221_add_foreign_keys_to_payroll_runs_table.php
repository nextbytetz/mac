<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPayrollRunsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payroll_runs', function(Blueprint $table)
		{
			$table->foreign('payroll_proc_id')->references('id')->on('payroll_procs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_id')->references('id')->on('banks')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payroll_runs', function(Blueprint $table)
		{
			$table->dropForeign('payroll_runs_payroll_proc_id_foreign');
			$table->dropForeign('payroll_runs_bank_id_foreign');
			$table->dropForeign('payroll_runs_bank_branch_id_foreign');
			$table->dropForeign('payroll_runs_member_type_id_foreign');
		});
	}

}
