<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeOldCompensationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_old_compensations', function(Blueprint $table)
		{
			$table->foreign('incident_type_id')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_old_compensations', function(Blueprint $table)
		{
			$table->dropForeign('employee_old_compensations_incident_type_id_foreign');
            $table->dropForeign('employee_old_compensations_employee_id_foreign');
		});
	}

}
