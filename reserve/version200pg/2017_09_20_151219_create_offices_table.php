<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOfficesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100)->unique();
			$table->integer('parent_id')->unsigned()->nullable()->index();
			$table->string('external_id', 100)->nullable()->unique()->comment('Any special reference for the office.');
			$table->date('opening_date')->nullable()->comment('Date office opened.');
			$table->integer('region_id')->unsigned()->nullable()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offices');
	}

}
