<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributionTempsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contribution_temps', function(Blueprint $table)
		{
			$table->foreign('receipt_code_id')->references('id')->on('receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contribution_temps', function(Blueprint $table)
		{
			$table->dropForeign('contribution_temps_receipt_code_id_foreign');
		});
	}

}
