<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('rcv_date')->comment('Receivable date, date which the receivable is to be collected');
			$table->integer('employer_id')->unsigned()->nullable()->index()->comment('in case that this booking is for employer');
			$table->integer('fin_code_id')->unsigned()->index();
			$table->decimal('amount', 14)->comment('amount which has been booked');
			$table->tinyInteger('ssyes')->default(0)->comment('determine whether booking entry has been exported to finance software');
			$table->integer('user_id')->unsigned()->nullable()->index();
			$table->integer('booking_group_id')->unsigned()->nullable()->index()->comment('in case that this booking is for a group');
			$table->integer('member_count')->comment('total number of members who are expected to be paid in receivable.');
			$table->text('description', 65535)->nullable();
			$table->tinyInteger('iscomplete')->default(0)->comment('Set whether the booking is complete or not.');
			$table->tinyInteger('isadjusted')->nullable()->default(0)->comment('Flag to show whether this booking has already been adjusted; has finished workflow');
			$table->decimal('adjust_amount', 14)->nullable()->comment('Specify amount which has been adjusted.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings');
	}

}
