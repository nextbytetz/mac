<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDependentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dependents', function(Blueprint $table)
		{
			$table->foreign('dependent_type_id')->references('id')->on('dependent_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('identity_id')->references('id')->on('identities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gender_id')->references('id')->on('genders')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('country_id')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('location_type_id')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('parent_id')->references('id')->on('dependents')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dependents', function(Blueprint $table)
		{
			$table->dropForeign('dependents_dependent_type_id_foreign');
			$table->dropForeign('dependents_identity_id_foreign');
			$table->dropForeign('dependents_gender_id_foreign');
			$table->dropForeign('dependents_country_id_foreign');
			$table->dropForeign('dependents_location_type_id_foreign');
			$table->dropForeign('dependents_bank_branch_id_foreign');
			$table->dropForeign('dependents_parent_id_foreign');
            $table->dropForeign('dependents_employee_id_foreign');
		});
	}

}
