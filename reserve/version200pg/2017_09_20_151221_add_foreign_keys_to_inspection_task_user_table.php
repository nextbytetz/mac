<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInspectionTaskUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inspection_task_user', function(Blueprint $table)
		{
			$table->foreign('inspection_task_id')->references('id')->on('inspection_tasks')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inspection_task_user', function(Blueprint $table)
		{
			$table->dropForeign('inspection_task_user_inspection_task_id_foreign');
            $table->dropForeign('inspection_task_user_user_id_foreign');
		});
	}

}
