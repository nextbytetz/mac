<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicalExpensesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medical_expenses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_report_id')->unsigned()->index();
			$table->integer('member_type_id')->unsigned()->index();
			$table->integer('resource_id')->unsigned()->nullable()->index();
			$table->decimal('amount', 14)->default(0.00);
			$table->integer('user_id')->unsigned()->index()->comment('staff who has created this medical expense');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medical_expenses');
	}

}
