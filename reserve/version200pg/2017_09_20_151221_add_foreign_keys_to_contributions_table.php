<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contributions', function(Blueprint $table)
		{
			$table->foreign('receipt_code_id')->references('id')->on('receipt_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('contribution_temp_id')->references('id')->on('contribution_temps')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contributions', function(Blueprint $table)
		{
			$table->dropForeign('contributions_receipt_code_id_foreign');
			$table->dropForeign('contributions_contribution_temp_id_foreign');
            $table->dropForeign('contributions_user_id_foreign');
            $table->dropForeign('contributions_employee_id_foreign');
		});
	}

}
