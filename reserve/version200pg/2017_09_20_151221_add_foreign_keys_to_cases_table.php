<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cases', function(Blueprint $table)
		{
			$table->foreign('court_category_id')->references('id')->on('court_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('parent_id')->references('id')->on('cases')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cases', function(Blueprint $table)
		{
			$table->dropForeign('cases_court_category_id_foreign');
			$table->dropForeign('cases_district_id_foreign');
			$table->dropForeign('cases_parent_id_foreign');
            $table->dropForeign('cases_user_id_foreign');
		});
	}

}
