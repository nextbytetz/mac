<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationHealthProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_health_providers', function(Blueprint $table)
		{
			$table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('health_provider_id')->references('id')->on('health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('medical_expense_id')->references('id')->on('medical_expenses')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_health_providers', function(Blueprint $table)
		{
			$table->dropForeign('notification_health_providers_notification_report_id_foreign');
			$table->dropForeign('notification_health_providers_health_provider_id_foreign');
			$table->dropForeign('notification_health_providers_medical_expense_id_foreign');
		});
	}

}
