<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employers', function(Blueprint $table)
		{
			$table->foreign('district_id')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bank_branch_id')->references('id')->on('bank_branches')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('location_type_id')->references('id')->on('location_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('representer_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign('employer_category_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employers', function(Blueprint $table)
		{
			$table->dropForeign('employers_district_id_foreign');
			$table->dropForeign('employers_bank_branch_id_foreign');
			$table->dropForeign('employers_location_type_id_foreign');
			$table->dropForeign('employers_representer_id_foreign');
            $table->dropForeign('employers_employer_category_cv_id_foreign');
		});
	}

}
