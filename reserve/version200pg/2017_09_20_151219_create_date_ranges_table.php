<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDateRangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('date_ranges', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->date('from_date')->comment('Min date for reports');
			$table->date('to_date')->comment('Max date for reports');
			$table->timestamps();
			$table->integer('bank_id')->unsigned()->index();
			$table->integer('payment_type_id')->unsigned()->index();
			$table->integer('employer_id')->unsigned()->index();
			$table->integer('employee_id')->unsigned()->index();
			$table->integer('insurance_id')->unsigned()->index();
			$table->integer('dependent_id')->unsigned()->index();
			$table->integer('pensioner_id')->unsigned()->index();
			$table->integer('member_type_id')->unsigned()->index();
			$table->integer('resource_id')->unsigned()->index();
			$table->integer('incident_type_id')->unsigned()->index();
			$table->integer('region_id')->unsigned()->index();
			$table->integer('business_id')->unsigned()->index();
			$table->integer('payroll_proc_id')->unsigned()->index();
			$table->integer('status_id')->index();
			$table->integer('type')->comment('General type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('date_ranges');
	}

}
