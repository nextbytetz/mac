<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBenefitTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('benefit_types', function(Blueprint $table)
		{
			$table->foreign('benefit_type_group_id')->references('id')->on('benefit_type_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fin_account_code_id')->references('id')->on('fin_account_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('benefit_types', function(Blueprint $table)
		{
			$table->dropForeign('benefit_types_benefit_type_group_id_foreign');
			$table->dropForeign('benefit_types_fin_account_code_id_foreign');
		});
	}

}
