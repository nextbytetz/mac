<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCheckersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('checkers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('resource_id')->comment('primary key value of the affected table, table which change has been registered and awaiting for approval.');
			$table->string('table_name', 50)->comment('name of the table having resource_id');
			$table->text('command', 65535);
			$table->tinyInteger('status')->default(0)->comment('set the status of the checker, 0 - awaiting approval, 1 - approved and the change has been applied');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('checkers');
	}

}
