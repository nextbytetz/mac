<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_reports', function(Blueprint $table)
		{
			$table->foreign('body_part_injury_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('incident_exposure_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('initial_expense_member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('nature_of_incident_cv_id')->references('id')->on('code_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('incident_type_id')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('pay_through_insurance_id')->references('id')->on('insurances')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_reports', function(Blueprint $table)
		{
			$table->dropForeign('notification_reports_body_part_injury_cv_id_foreign');
			$table->dropForeign('notification_reports_incident_exposure_cv_id_foreign');
			$table->dropForeign('notification_reports_initial_expense_member_type_id_foreign');
			$table->dropForeign('notification_reports_nature_of_incident_cv_id_foreign');
			$table->dropForeign('notification_reports_incident_type_id_foreign');
			$table->dropForeign('notification_reports_pay_through_insurance_id_foreign');
            $table->dropForeign('notification_reports_employee_id_foreign');
		});
	}

}
