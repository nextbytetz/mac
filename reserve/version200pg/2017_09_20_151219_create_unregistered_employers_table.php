<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnregisteredEmployersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unregistered_employers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('name', 65535)->nullable();
			$table->text('address', 65535)->nullable();
			$table->text('plot_number', 65535)->nullable();
			$table->text('street', 65535)->nullable();
			$table->text('postal_city', 65535)->nullable();
			$table->text('location', 65535)->nullable();
			$table->text('business_activity', 65535)->nullable();
			$table->text('last_remittance', 65535)->nullable();
			$table->integer('state')->default(0);
			$table->string('phone', 100)->nullable();
			$table->string('email', 100)->nullable();
			$table->date('updated')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('assign_to')->default(0);
			$table->date('date_assigned')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unregistered_employers');
	}

}
