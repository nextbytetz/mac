<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permissions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('permission_group_id')->unsigned()->index();
			$table->string('name', 100)->unique();
			$table->string('display_name', 100);
			$table->text('description', 65535)->nullable();
			$table->tinyInteger('ischecker')->default(0)->comment('determine whether this permission is for checker, needs a second person approval.');
			$table->integer('checker_parent')->unsigned()->nullable()->index()->comment('parent permission for this maker checker');
			$table->tinyInteger('can_maker_checker')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permissions');
	}

}
