<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inspections', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('inspection_type_id')->unsigned()->index();
			$table->text('comments', 65535)->nullable();
			$table->date('start_date')->index()->comment('date when the inspection will be started.');
			$table->date('end_date')->nullable();
			$table->integer('user_id')->unsigned();
			$table->tinyInteger('iscancelled')->default(0)->comment('set whether the inspection has been cancelled or not, 1 - inspection cancelled, 0 - inspection not cancelled.');
			$table->text('cancel_reason', 65535)->nullable()->comment('set the reason for canceling the inspection entity.');
			$table->integer('cancel_user')->unsigned()->nullable()->comment('user who have canceled the inspection.');
			$table->dateTime('cancel_date')->nullable();
			$table->tinyInteger('complete_status')->nullable()->default(0)->comment('set the complete status of the inspection, 1 - inspection has been completed, 0 - inspection has not yet been completed.');
			$table->date('complete_date')->nullable();
			$table->integer('complete_user')->unsigned()->nullable();
			$table->text('delay_reasons', 65535)->nullable()->comment('any reason which have made the inspection to delay to complete');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inspections');
	}

}
