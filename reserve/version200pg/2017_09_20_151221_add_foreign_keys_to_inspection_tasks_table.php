<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInspectionTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inspection_tasks', function(Blueprint $table)
		{
			$table->foreign('inspection_id')->references('id')->on('inspections')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('inspection_profile_id')->references('id')->on('inspection_profiles')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inspection_tasks', function(Blueprint $table)
		{
			$table->dropForeign('inspection_tasks_inspection_id_foreign');
			$table->dropForeign('inspection_tasks_inspection_profile_id_foreign');
		});
	}

}
