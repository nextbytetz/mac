<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationHealthProviderPractitionersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_health_provider_practitioners', function(Blueprint $table)
		{
			$table->foreign('notification_health_provider_id')->references('id')->on('notification_health_providers')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('medical_practitioner_id')->references('id')->on('medical_practitioners')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_health_provider_practitioners', function(Blueprint $table)
		{
			$table->dropForeign('notification_health_provider_practitioners_notification_health_provider_id_foreign');
			$table->dropForeign('notification_health_provider_practitioners_medical_practitioner_id_foreign');
		});
	}

}
