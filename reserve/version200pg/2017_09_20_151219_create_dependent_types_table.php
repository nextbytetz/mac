<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDependentTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dependent_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->tinyInteger('survivor_pension')->default(1)->comment('determine if the dependent type is eligible to receive survivor pension.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dependent_types');
	}

}
