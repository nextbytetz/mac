<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBenefitTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('benefit_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('benefit_type_group_id')->unsigned()->index();
			$table->string('name', 150);
			$table->string('shortcode', 20);
			$table->integer('fin_account_code_id')->unsigned()->nullable()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('benefit_types');
	}

}
