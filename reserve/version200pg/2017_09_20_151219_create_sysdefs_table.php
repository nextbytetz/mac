<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSysdefsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sysdefs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('total_allowed_contrib_months')->nullable();
			$table->decimal('minimum_monthly_pension', 14)->nullable()->comment('Minimum payable amount per month i.e pension, ttd, tpd');
			$table->decimal('maximum_monthly_pension', 14)->nullable()->comment('Maximum payable amount per month i.e pension, ttd, tpd');
			$table->decimal('minimum_lumpsum_amount', 14)->nullable();
			$table->decimal('maximum_lumpsum_amount', 14)->nullable();
			$table->decimal('funeral_grant_amount', 14)->nullable();
			$table->decimal('percentage_of_earning', 4)->nullable();
			$table->decimal('assistant_percent', 4)->nullable();
			$table->decimal('wife_spouse_percent', 4)->nullable();
			$table->decimal('child_percent_with_spouse', 4)->nullable()->comment('Default survivor pension percent for child when there is wife or spouse');
			$table->decimal('child_percent_without_spouse', 4)->nullable()->comment('Default survivor pension percent for when there is no wife or spouse');
			$table->integer('constant_factor_compensation')->nullable();
			$table->decimal('percentage_imparement_scale', 4)->nullable();
			$table->integer('permanent_partial_mp_period')->nullable();
			$table->decimal('contribution_penalty_percent', 4)->nullable();
			$table->integer('contribution_grace_period')->unsigned()->nullable();
			$table->decimal('public_contribution_percent', 4);
			$table->decimal('private_contribution_percent', 4);
			$table->integer('cancel_receipt_days')->comment('time period allowed for a user to cancel a receipt registered');
			$table->timestamps();
			$table->tinyInteger('employee_number_length')->nullable()->default(8);
			$table->tinyInteger('receipt_number_length')->nullable()->default(8);
			$table->integer('minimum_allowed_contribution')->nullable()->default(1000);
			$table->integer('no_of_days_for_a_month')->unsigned()->nullable()->default(30);
			$table->decimal('no_of_days_for_a_month_in_assessment', 14, 4)->unsigned()->nullable()->comment('No of days in a month to be used in claim assessment');
			$table->integer('min_days_for_ttd_tpd')->nullable()->comment('Minimum Days eligible to compensate Temporary Disablement');
			$table->integer('day_hours')->nullable()->comment('Hours worked per day by employee to be used in claim assessment');
			$table->integer('inspection_years_period')->nullable()->default(3);
			$table->integer('case_years_period')->nullable()->default(2);
			$table->string('payment_voucher_reference', 45)->nullable()->comment('Payment Voucher reference');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sysdefs');
	}

}
