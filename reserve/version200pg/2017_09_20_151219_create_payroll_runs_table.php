<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePayrollRunsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payroll_runs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('payroll_proc_id')->unsigned()->index();
			$table->integer('member_type_id')->unsigned()->index();
			$table->integer('resource_id')->unsigned()->index();
			$table->decimal('amount', 14);
			$table->integer('months_paid')->unsigned();
			$table->decimal('monthly_pension', 14);
			$table->integer('bank_id')->unsigned()->index();
			$table->string('accountno', 45);
			$table->integer('bank_branch_id')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payroll_runs');
	}

}
