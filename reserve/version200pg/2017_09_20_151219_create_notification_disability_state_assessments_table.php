<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationDisabilityStateAssessmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notification_disability_state_assessments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_disability_state_id')->unsigned()->index();
			$table->integer('days')->nullable()->comment('Days assessed for each disability state. i.e actual hospitalization days.');
			$table->integer('user_id')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notification_disability_state_assessments');
	}

}
