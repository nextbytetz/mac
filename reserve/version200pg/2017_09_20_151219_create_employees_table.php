<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('memberno');
            $table->string('firstname', 100);
            $table->string('middlename', 100)->nullable();
            $table->string('lastname', 100);
            $table->string('email', 45)->nullable();
            $table->string('phone', 45)->nullable();
            $table->date('dob')->nullable();
            $table->decimal('salary', 14)->nullable();
            $table->decimal('allowance', 14)->nullable();
            $table->string('nid', 50)->nullable()->comment('national identity card');
			$table->integer('identity_id')->unsigned()->nullable()->index();
			$table->integer('job_title_id')->unsigned()->nullable()->index();
			$table->integer('gender_id')->unsigned()->nullable()->index();
			$table->integer('marital_status_cv_id')->unsigned()->nullable()->index();
            $table->integer('employee_category_cv_id')->unsigned()->nullable()->index();
            $table->integer('bank_branch_id')->unsigned()->nullable()->index();
            $table->integer('location_type_id')->unsigned()->nullable()->index();
			$table->string('street', 150)->nullable();
			$table->string('road', 150)->nullable();
			$table->string('plot_no', 150)->nullable();
			$table->string('block_no', 150)->nullable();
			$table->text('surveyed_extra', 65535)->nullable();
            $table->text('unsurveyed_area', 65535)->nullable();
            $table->text('department', 65535)->nullable();
			$table->string('fax', 45)->nullable();
            $table->string('accountno', 45)->nullable();
			$table->string('sex', 10)->nullable();
			$table->string('job_title', 150)->nullable();
			$table->string('emp_cate', 150)->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
