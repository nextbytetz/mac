<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLawyersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lawyers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('employer_id')->unsigned()->nullable()->index();
			$table->string('firstname', 50)->nullable();
			$table->string('middlename', 50)->nullable();
			$table->string('lastname', 50)->nullable();
			$table->string('external_id', 30)->nullable()->unique()->comment('any special reference relating to the lawyer');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lawyers');
	}

}
