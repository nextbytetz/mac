<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cases', function(Blueprint $table)
		{
			$table->increments('id');
			$table->tinyInteger('status')->nullable()->default(0)->comment('check the status of the case whether is open or closed. 1 - Closed, 0 - Open');
			$table->integer('parent_id')->unsigned()->nullable()->index();
			$table->string('title', 250)->comment('title of the title');
			$table->string('number', 50)->comment('specific number of the case');
			$table->string('complainant', 100)->comment('name of the complainant of the case');
			$table->text('complainant_address', 65535)->nullable()->comment('Address of the complainant');
			$table->integer('district_id')->unsigned()->index()->comment('district location of the case');
			$table->string('location', 200)->nullable()->comment('specific location of the case');
			$table->smallInteger('court_category_id')->unsigned()->index();
			$table->string('court', 200)->nullable()->comment('name of the court which the case has been listened.');
			$table->decimal('fee', 14)->nullable()->comment('amount of money associated with the case');
			$table->text('liability', 65535)->nullable()->comment('Any liability associated with the case');
			$table->text('respondent', 65535)->nullable()->comment('Respondent of the case');
			$table->text('respondent_address', 65535)->nullable()->comment('Address of the respondent');
			$table->text('fact', 65535)->nullable()->comment('Any brief fact associated with the case.');
			$table->tinyInteger('is_archived')->default(0)->comment('Set whether the case has been set in an archive mode or not, 1 - case is in archive mode, 0 case not in an archive mode.');
			$table->dateTime('filling_date')->nullable()->comment('date and time which the case has been filled.');
			$table->integer('user_id')->unsigned()->nullable()->comment('system user who has created a case in the system');
			$table->integer('user_closed')->unsigned()->nullable()->comment('system user who has marked the case as closed');
			$table->timestamps();
			$table->softDeletes();
			$table->tinyInteger('liability_type')->nullable()->comment('State whether a case is contentious or non contentious. In case of contentious, either you owe or owed. 1 Contentious, 2 Non contentious');
			$table->tinyInteger('contentious_type')->nullable()->comment(' 1 - we owe money, 2 - we owed money');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cases');
	}

}
