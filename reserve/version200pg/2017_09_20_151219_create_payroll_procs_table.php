<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePayrollProcsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payroll_procs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('description', 65535);
			$table->integer('user_id')->unsigned();
			$table->string('bquarter', 30)->comment('specific period which the payroll is being run');
			$table->integer('payroll_proc_type_id')->unsigned()->index()->comment('Payment proc type i.e lupm sum, monthly pension');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payroll_procs');
	}

}
