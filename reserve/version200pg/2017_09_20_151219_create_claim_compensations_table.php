<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClaimCompensationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claim_compensations', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('claim_id')->unsigned()->index();
			$table->integer('member_type_id')->unsigned()->index()->comment('which member type this compensation is conserned');
			$table->integer('resource_id')->unsigned()->nullable()->index()->comment('in case employer is to be paid');
			$table->integer('benefit_type_id')->unsigned()->index();
			$table->decimal('amount', 14)->comment('amount to be compensated');
			$table->integer('user_id');
			$table->tinyInteger('ispaid')->default(0)->comment('set the paid status, 1 - is already paid, 0 - not yet paid');
			$table->integer('compensation_payment_type_id')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claim_compensations');
	}

}
