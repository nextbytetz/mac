<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAccidentWitnessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('accident_witnesses', function(Blueprint $table)
		{
			$table->foreign('accident_id')->references('id')->on('accidents')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('witness_id')->references('id')->on('witnesses')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('accident_witnesses', function(Blueprint $table)
		{
			$table->dropForeign('accident_witnesses_accident_id_foreign');
			$table->dropForeign('accident_witnesses_witness_id_foreign');
            $table->dropForeign('accident_witnesses_user_id_foreign');
		});
	}

}
