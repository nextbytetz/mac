<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestigationFeedbacksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investigation_feedbacks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_report_id')->unsigned()->index();
			$table->integer('investigation_question_id')->unsigned();
			$table->text('feedback', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('investigation_feedbacks');
	}

}
