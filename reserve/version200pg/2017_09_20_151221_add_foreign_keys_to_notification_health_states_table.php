<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationHealthStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_health_states', function(Blueprint $table)
		{
			$table->foreign('health_state_checklist_id')->references('id')->on('health_service_checklists')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('notification_report_id')->references('id')->on('notification_reports')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_health_states', function(Blueprint $table)
		{
			$table->dropForeign('notification_health_states_health_state_checklist_id_foreign');
			$table->dropForeign('notification_health_states_notification_report_id_foreign');
		});
	}

}
