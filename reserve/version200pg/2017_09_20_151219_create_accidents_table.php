<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accidents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notification_report_id')->unsigned()->index();
			$table->date('accident_date')->nullable();
			$table->string('accident_place', 150)->nullable();
			$table->time('accident_time')->comment('Time when accident occured');
			$table->date('reporting_date')->comment('date of reporting accident to employer');
			$table->date('receipt_date')->comment('date of receipt of notification by employer');
			$table->text('activity_performed', 65535)->nullable()->comment('activity/duty performed at the time of accident');
			$table->text('description', 65535)->comment('description on how accident occurred');
			$table->integer('user_id')->unsigned()->index();
			$table->integer('accident_type_id')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accidents');
	}

}
