<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContributionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contributions', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('receipt_code_id')->unsigned()->index();
			$table->decimal('employer_amount', 14)->nullable();
			$table->decimal('employee_amount', 14);
			$table->decimal('grosspay', 14)->nullable();
			$table->integer('employee_id')->unsigned()->index();
			$table->integer('user_id')->unsigned();
			$table->tinyInteger('status')->default(1)->comment('Specify the status of the contribution, 1 - amount okay, 0 - amount not okay ...');
			$table->tinyInteger('isactive')->default(1)->comment('set whether the contribution is active or not, 1 - active, 0 - not active.');
			$table->bigInteger('contribution_temp_id')->unsigned()->nullable()->index();
			$table->timestamps();
			$table->softDeletes();
			$table->decimal('basicpay', 14)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contributions');
	}

}
