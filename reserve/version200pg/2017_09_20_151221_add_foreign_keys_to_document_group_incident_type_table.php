<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentGroupIncidentTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_group_incident_type', function(Blueprint $table)
		{
			$table->foreign('document_group_id')->references('id')->on('document_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('incident_type_id')->references('id')->on('incident_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_group_incident_type', function(Blueprint $table)
		{
			$table->dropForeign('document_group_incident_type_document_group_id_foreign');
			$table->dropForeign('document_group_incident_type_incident_type_id_foreign');
		});
	}

}
