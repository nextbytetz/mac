<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCodeValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('code_values', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('code_id')->unsigned()->index();
			$table->string('name', 500);
			$table->text('description', 65535)->nullable();
			$table->string('reference', 20)->nullable()->unique()->comment('Code value reference id which will be unique for every code value');
			$table->tinyInteger('sort')->nullable()->comment('The order which the code value appear on the user view');
			$table->tinyInteger('is_active')->default(1)->comment('set whether this code value is active or not, 1 code value is active, 0 code value is not active');
			$table->tinyInteger('is_mandatory')->default(0)->comment('1 - is mandatory, 0 - is not mandatory (System defined flag)');
			$table->timestamps();
			$table->softDeletes();
			$table->decimal('allowance', 14)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('code_values');
	}

}
