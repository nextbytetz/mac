<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClaimCompensationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('claim_compensations', function(Blueprint $table)
		{
			$table->foreign('claim_id')->references('id')->on('claims')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('benefit_type_id')->references('id')->on('benefit_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('member_type_id')->references('id')->on('member_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('compensation_payment_type_id')->references('id')->on('compensation_payment_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('claim_compensations', function(Blueprint $table)
		{
			$table->dropForeign('claim_compensations_claim_id_foreign');
			$table->dropForeign('claim_compensations_benefit_type_id_foreign');
			$table->dropForeign('claim_compensations_member_type_id_foreign');
			$table->dropForeign('claim_compensations_compensation_payment_type_id_foreign');
            $table->dropForeign('claim_compensations_user_id_foreign');
		});
	}

}
