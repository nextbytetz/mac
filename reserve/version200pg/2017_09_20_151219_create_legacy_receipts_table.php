<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLegacyReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('legacy_receipts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('rctno', 100)->nullable();
			$table->date('rct_date')->nullable();
			$table->decimal('amount', 14)->nullable();
			$table->string('chequeno', 50)->nullable();
			$table->integer('bank_id')->unsigned()->nullable()->index();
			$table->integer('payment_type_id')->unsigned()->nullable()->index();
			$table->date('contrib_month')->nullable();
			$table->integer('employer_id')->unsigned()->nullable()->index();
			$table->integer('status')->nullable()->default(0);
			$table->text('comments', 65535)->nullable();
			$table->integer('user_id')->unsigned()->nullable()->index();
			$table->integer('modified_by')->nullable();
			$table->date('modified_date')->nullable();
			$table->integer('contr_approval')->nullable()->default(0);
			$table->text('reason', 65535)->nullable();
			$table->integer('approved_by')->nullable();
			$table->date('approved_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('legacy_receipts');
	}

}
