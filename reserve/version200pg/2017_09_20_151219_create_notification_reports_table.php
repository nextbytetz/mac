<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notification_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('incident_type_id')->unsigned()->index();
			$table->integer('employee_id')->unsigned()->index();
			$table->tinyInteger('status')->comment('store the status of the notification, 0 - Pending, 1 - Approved, 2 - Rejected.');
			$table->tinyInteger('is_acknowledgment_sent')->default(0)->comment('1 - sent, 0 - not sent');
			$table->tinyInteger('is_pensioner')->default(0)->comment('0 - not pensioner, 1 - is pensioner.');
			$table->tinyInteger('investigation_validity')->default(0)->comment('0 - not yet valid, 1 - investigation is valid');
			$table->tinyInteger('isverified')->default(0)->comment('set whether a notification is verified or not, 1 - verified, 0 - not verified.');
			$table->tinyInteger('need_investigation')->default(0)->comment('set whether notification needs investigation or not, 1 - needs investigation, 0 - does not need investigation.');
			$table->tinyInteger('need_verification')->default(1)->comment('set whether the notification needs to be verified or not, 1 - needs verification, 0 - do not need verification.');
			$table->integer('user_id')->unsigned()->index();
			$table->integer('initial_expense_member_type_id')->unsigned()->nullable()->index()->comment('Member type who paid initial expense for this notification report');
			$table->integer('initial_expense_resource_id')->unsigned()->nullable()->comment('Primary key of member type who paid initial expense for this notification report');
			$table->integer('pay_through_insurance_id')->unsigned()->nullable()->index()->comment('Insurance used to pay initial expense for this notification report');
			$table->tinyInteger('is_cash')->default(0)->comment('Flag to show whether initial expense was paid by member i.e employee or employer.');
			$table->integer('body_part_injury_cv_id')->unsigned()->nullable()->index();
			$table->integer('incident_exposure_cv_id')->unsigned()->nullable()->index();
			$table->integer('nature_of_incident_cv_id')->unsigned()->nullable()->index();
			$table->tinyInteger('wf_done')->default(0)->comment('specify whether the notification report workflow has been completed or not.');
			$table->date('wf_done_date')->nullable()->comment('date which workflow was completed');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notification_reports');
	}

}
