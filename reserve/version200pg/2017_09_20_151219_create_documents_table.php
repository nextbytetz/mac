<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 150)->unique();
			$table->integer('document_group_id')->unsigned()->index();
			$table->text('description', 65535)->nullable();
			$table->tinyInteger('isrecurring')->nullable()->default(0)->comment('set whether this document type is recurring for each incident or not.');
			$table->tinyInteger('ismandatory')->nullable()->default(1)->comment('set whether the document is mandatory or not, 1 - mandatory, 0 - not mandatory');
			$table->tinyInteger('isactive')->nullable()->default(1)->comment('set whether the document is active in use or not, 1 - active, 0 - not active.');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documents');
	}

}
