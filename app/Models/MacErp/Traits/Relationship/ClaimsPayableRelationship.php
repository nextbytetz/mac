<?php

namespace App\Models\MacErp\Traits\Relationship;
use App\Models\Finance\BankBranch;
use App\Models\Finance\PaymentVoucherTransaction;
use App\Models\Reporting\DishonouredCheque;


trait ClaimsPayableRelationship {


     public function pvTran()
    {
        return $this->belongsTo(PaymentVoucherTransaction::class, 'pv_tran_id', 'id');
    }

}
