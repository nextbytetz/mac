<?php

namespace App\Models\MacErp;

use Illuminate\Database\Eloquent\Model;

class ErpSupplier extends Model
{
  protected $connection = 'pgmain';
  protected $table = 'erp_suppliers';

  protected $fillable = ['trx_id','supplier_name','supplier_id', 'vendor_site_code', 
	'address_line1','address_line2','city','liability_account'];

}
