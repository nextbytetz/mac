<?php

namespace App\Models\MacErp;

use App\Models\MacErp\Traits\Relationship\ClaimsPayableRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimsPayable extends Model
{
    //
    use ClaimsPayableRelationship, SoftDeletes;

	protected $connection = 'pgmain';
	protected $table = 'claims_payable';

	protected $fillable = ['trx_id','member_type','notification_id', 'payee_name', 
	'supplier_id','benefit_type','debit_account_expense','credit_account_receivable','accountno','bank_name',
	'swift_code','bank_branch_id','bank_address','country_name_bank','city_of_bank','state_or_region','amount','isposted','status_code', 'pv_tran_id', 'isclaim', 'payment_resource_id'];

}
