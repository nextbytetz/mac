<?php

namespace App\Models\MacErp;

use Illuminate\Database\Eloquent\Model;

class ReceiptsGl extends Model
{
    //
	protected $connection = 'pgmain';
	protected $table = 'receipts_gl';

	protected $fillable = ['trx_id','trx_type','rctno', 'rct_date', 
	'payment_description','payment_date','employer_id','thirdparty_id','payee_name','hr_staff_id',
	'debitaccount_bankaccount','creditaccount_receivableaccount','control_number','spare_three','currency','amount','isposted','status_code'];
}
