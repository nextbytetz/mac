<?php

namespace App\Models\Organization;

use App\Models\Organization\Traits\Attribute\FiscalYearAttribute;
use App\Models\Organization\Traits\Relationship\FiscalYearRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Session
 * package App.
 */
class FiscalYear extends Model
{

    use FiscalYearRelationship , FiscalYearAttribute;
    /**
     * @var array
     */
    protected $guarded = [];
    public $timestamps = true;
}
