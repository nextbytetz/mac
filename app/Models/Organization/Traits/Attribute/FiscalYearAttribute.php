<?php

namespace App\Models\Organization\Traits\Attribute;

use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

trait FiscalYearAttribute {



    /* Get Monthly Contribution Target */

    public function monthlyTarget($reference)
    {
        $codeValue = new CodeValueRepository();
        $target_type = $codeValue->findByReference($reference);
        $target_total = 0;
        if(isset($this->annualTargetTypes()->where('annual_target_type_cv_id', $target_type->id)->first)){

            $target_total = $this->annualTargetTypes()->where('annual_target_type_cv_id', $target_type->id)->first
            ()->pivot->target_total;
        }

        return $target_total / 12;
    }

}
