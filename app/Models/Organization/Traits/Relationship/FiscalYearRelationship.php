<?php

namespace App\Models\Organization\Traits\Relationship;

use App\Models\Sysdef\CodeValue;

trait FiscalYearRelationship {

    /**
     * @return mixed
     */
//Relation to the ANNUAL Target Types
    //sectors
    public function annualTargetTypes()
    {
        return $this->belongsToMany(CodeValue::class, 'fiscal_year_target_type', 'fiscal_year_id', 'annual_target_type_cv_id')->withPivot('id', 'target_total');
    }

}
