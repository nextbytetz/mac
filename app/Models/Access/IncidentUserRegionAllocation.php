<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;

class IncidentUserRegionAllocation extends Model
{

    protected $guarded = [];
    public $timestamps = true;
}
