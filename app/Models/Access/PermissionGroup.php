<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\Relationship\PermissionGroupRelationship;

/**
 * Class Permission
 */
class PermissionGroup extends Model {
    
    use PermissionGroupRelationship;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

}
