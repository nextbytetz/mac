<?php

namespace App\Models\Access\Relationship;

use App\Models\Access\Permission;
use App\Models\Auth\User;

/**
 * Class RoleRelationship
 * @package App\Models\Access\Relationship
 */
trait RoleRelationship
{
    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function permissions()
    {

        return $this->belongsToMany(Permission::class)
            ->orderBy('name', 'asc')->withTimestamps();

    }
}