<?php

namespace App\Models\Access\Relationship;

use App\Models\Access\Role;
use App\Models\Access\PermissionGroup;
use App\Models\Access\PermissionDependency;
use App\Models\Auth\User;

/**
 * Class PermissionRelationship
 * @package App\Models\Access\Relationship
 */
trait PermissionRelationship
{
    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function dependencies() {
        return $this->hasMany(PermissionDependency::class);
    }

    /**
     * @return mixed
     */
    public function backDependencies(){
        return $this->hasMany(PermissionDependency::class, 'dependency_id');
    }

    /**
     * @return mixed
     */
    public function users() {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function permissionGroup() {
        return $this->belongsTo(PermissionGroup::class);
    }

}