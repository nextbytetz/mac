<?php

namespace App\Models\Access\Relationship;

use App\Models\Access\Permission;

/**
 * Class PermissionGroupRelationship
 * @package App\Models\Access\Relationship
 */
trait PermissionGroupRelationship
{
    /**
     * return @mixed
     */
    public function permissions(){
        return $this->hasMany(Permission::class)->orderBy('name', 'asc');;
    }
}