<?php

namespace App\Models\Access\Relationship;

use App\Models\Access\Permission;

/**
 * Class PermissionDependencyRelationship
 * @package App\Models\Access\Relationship
 */
trait PermissionDependencyRelationship
{

    /**
     * @return mixed
     */
    public function permission() {
        return $this->hasOne(Permission::class, 'id', 'dependency_id');
    }

    /**
     * @return mixed
     */
    public function backPermission(){
        return $this->hasOne(Permission::class, 'id', 'permission_id');
    }

}