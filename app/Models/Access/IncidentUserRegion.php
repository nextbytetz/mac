<?php

namespace App\Models\Access;

use App\Models\Location\District;
use App\Models\Location\Region;
use Illuminate\Database\Eloquent\Model;

class IncidentUserRegion extends Model
{

    protected $guarded = [];
    public $timestamps = true;

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function user()
    {
        return $this->belongsTo(IncidentUser::class, 'incident_user_id');
    }

}
