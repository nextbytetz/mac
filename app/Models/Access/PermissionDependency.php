<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\Relationship\PermissionDependencyRelationship;

/**
 * Class PermissionDependency
 */
class PermissionDependency extends Model {

    use PermissionDependencyRelationship;

    protected $fillable = [
        'permission_id',
        'dependency_id'
    ];
    protected $guarded = [];

}