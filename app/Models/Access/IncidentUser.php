<?php

namespace App\Models\Access;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class IncidentUser extends Model
{

    protected $guarded = [];
    public $timestamps = true;

    public function regions()
    {
        return $this->hasMany(IncidentUserRegion::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
