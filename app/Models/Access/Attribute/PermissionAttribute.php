<?php

namespace App\Models\Access\Attribute;

/**
 * Class PermissionAttribute
 * @package App\Models\Access\Attribute
 */
trait PermissionAttribute
{


    /**
     * @return int
     * Cancel receipt permission
     */
    public function getCancelReceiptAttribute() {
        return $this->id = 6;
    }

    /**
     * @return int
     * Register receipt permission
     */
    public function getRegisterReceiptAttribute() {
        return $this->id = 2;
    }

}