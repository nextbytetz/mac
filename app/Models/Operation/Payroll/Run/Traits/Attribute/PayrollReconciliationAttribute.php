<?php

namespace App\Models\Operation\Payroll\Run\Traits\Attribute;

use App\Models\Operation\Payroll\PayrollRun;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use Carbon\Carbon;

trait PayrollReconciliationAttribute{



    // Get status label
    public function getStatusLabelAttribute() {
        if($this->wf_done == 1){
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
        }elseif($this->status == 1)
        {
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Initiated' . "'>" . 'Initiated' . "</span>";
        }elseif($this->status == 0){
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
        }elseif($this->status == 2){
            return  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Rejected' . "'>" . 'Rejected' . "</span>";
        }
    }



// Get type label
    public function getTypeLabelAttribute() {
        if($this->type == 1)
        {
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'UNDERPAID' . "'>" . 'UNDERPAID' . "</span>";
        }else{
            return  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'OVERPAID' . "'>" . 'OVERPAID' . "</span>";
        }
    }




    /*Created at formatted short date*/
    public function getCreatedAtFormattedAttribute()
    {
        return short_date_format($this->created_at);
    }

    /**
     * @return bool
     * Workflow is complete
     */
    public function isWfComplete()
    {
        return $this->wf_done == 1;
    }


    /*get Resource name for wf*/
    public function getResourceNameAttribute()
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($this->member_type_id, $this->resource_id);
        return $resource->name;
    }


}