<?php

namespace App\Models\Operation\Payroll\Run\Traits\Attribute;

use App\Models\Operation\Payroll\PayrollRun;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

trait PayrollRunApprovalAttribute{



    // Get status label
    public function getStatusLabelAttribute() {
        if($this->wf_done == 1){
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
        }elseif($this->status == 1)
        {
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Initiated' . "'>" . 'Initiated' . "</span>";
        }elseif($this->status == 0){
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
        }elseif($this->status == 2){
            return  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Rejected' . "'>" . 'Rejected' . "</span>";
        }
    }



// Get status label
    public function getWfStatusLabelAttribute() {
        if($this->isWfComplete())
        {
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
        }else{
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
        }
    }


    // GetJob Run status label
    public function getRunStatusLabelAttribute() {
        if($this->isRunComplete())
        {
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Complete' . "'>" . 'Complete' . "</span>";
        }else{
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . ' - '. number_0_format($this->calculatePayrollCompletePercentage()) . '%'. "</span>";
        }
    }

    /*Created at formatted short date*/
    public function getCreatedAtFormattedAttribute()
    {
        return short_date_format($this->created_at);
    }

    /**
     * @return bool
     * Workflow is complete
     */
    public function isWfComplete()
    {
        return $this->wf_done == 1;
    }

    /*Run Job is complete*/
    public function isRunComplete()
    {
        return $this->run_status == 1;
    }

    /*Get Payroll MOnths*/
    public function getPayrollMonthAttribute()
    {
        $run_date = $this->payrollProc->run_date;
        $run_date = Carbon::parse($run_date)->endOfMonth();
        return standard_date_format($run_date);
    }


    /**
     * @param $id
     * calculate payroll run completion status i.e. get percentage
     */
    public function calculatePayrollCompletePercentage()
    {
        $id = $this->id;
        $payroll_runs = new PayrollRunRepository();
        $payroll_suspended_runs = new PayrollSuspendedRunRepository();
        $no_of_eligible_active = $this->no_of_pensioners + $this->no_of_dependents + $this->no_of_constant_cares;
        $no_of_eligible_suspended = $this->no_of_suspended_pensioners + $this->no_of_suspended_dependents  + $this->no_of_suspended_constant_cares;
        $no_of_eligible_active_processed = $payroll_runs->query()->where('payroll_run_approval_id', $id)->count();
        $no_of_eligible_suspended_processed = $payroll_suspended_runs->query()->where('payroll_run_approval_id', $id)->count();

        $no_of_all_eligible = $no_of_eligible_active + $no_of_eligible_suspended;//active and suspended
        $no_of_all_processed = $no_of_eligible_active_processed + $no_of_eligible_suspended_processed;//active and suspended processed
        $ratio =  ($no_of_all_eligible > 0) ?  ($no_of_all_processed / $no_of_all_eligible) : 1;
        $complete_percentage = $ratio * 100;
        return $complete_percentage;
    }

    /**
     * @param $member_type_id
     * Find net amount payable by member type
     */
    public function getTotalNetAmountByMemberType($member_type_id)
    {
        $payroll_runs = new PayrollRunRepository();
        $net_amount_payable = $payroll_runs->query()->where('payroll_run_approval_id', $this->id)->where('member_type_id', $member_type_id)->sum('amount');
        return $net_amount_payable;
    }


    /**
     * @param $member_type_id
     * Find net amount payable for pensioners
     */
    public function getTotalNetAmountPensioners()
    {
        $payroll_runs = new PayrollRunRepository();
        $net_amount_payable = $payroll_runs->query()->where('payroll_run_approval_id', $this->id)->where('member_type_id', 5)->sum('amount');
        return $net_amount_payable;
    }

    /**
     * @param $member_type_id
     * Find net amount payable for survivors
     */
    public function getTotalNetAmountSurvivors()
    {
        $payroll_runs = new PayrollRunRepository();
        $net_amount_payable = $payroll_runs->query()->where('payroll_run_approval_id', $this->id)->where('member_type_id', 4)->where('isconstantcare', 0)->sum('amount');
        return $net_amount_payable;
    }


    /**
     * @param $member_type_id
     * Find net amount payable for constant care
     */
    public function getTotalNetAmountConstantCare()
    {
        $payroll_runs = new PayrollRunRepository();
        $net_amount_payable = $payroll_runs->query()->where('payroll_run_approval_id', $this->id)->where('member_type_id', 4)->where('isconstantcare', 1)->sum('amount');
        return $net_amount_payable;
    }


    /*get Resource name for wf*/
    public function getResourceNameAttribute()
    {
        return $this->payrollProc->bquarter;
    }


    /**
     * Get all banks of beneficiary in payroll payments (Payroll runs)
     */
    public function getBanksInPayrollRunQuery($id){
        $bank_repo = new BankRepository();
        $banks = $bank_repo->query()->whereHas('payrollRuns', function ($query) use($id){
            $query->where('payroll_run_approval_id', $id);
        })->orderBy('name', 'asc');

        return $banks;
    }

    /**
     * @param $id = payroll_run_approval_id
     * @param $bank_id
     * @return mixed
     */

    public function getAmountByBankInPayrollRun($id, $bank_id){
        $payroll_run_repo = new PayrollRunRepository();
        $amount = $payroll_run_repo->query()->where('payroll_run_approval_id',$id)->where('bank_id', $bank_id)->sum('amount');
        return $amount;
    }

    /*Get amount by bank in payroll by benefit type*/
    public function getAmountByBankInPayrollRunByBenefitType($id, $bank_id, $benefit_type_id){
        $payroll_run_repo = new PayrollRunRepository();
        switch($benefit_type_id){
            case 5:
                /*constant care*/
                $amount = $payroll_run_repo->query()->where('payroll_run_approval_id',$id)->where('bank_id', $bank_id)->where('member_type_id', 4)->where('isconstantcare',1)->sum('amount');
                break;

            case 6:
                /*Dependent monthly pension*/
                $amount = $payroll_run_repo->query()->where('payroll_run_approval_id',$id)->where('bank_id', $bank_id)->where('member_type_id', 4)->where('isconstantcare',0)->sum('amount');
                break;

            case 18:
                /*PTD and PPD pension*/
                $amount = $payroll_run_repo->query()->where('payroll_run_approval_id',$id)->where('bank_id', $bank_id)->where('member_type_id', 5)->where('isconstantcare',0)->sum('amount');
                break;
        }

        return $amount;
    }


    /**
     * @return mixed
     * New payees count for this payroll run approval
     */
    public function getNewPayeesCountAttribute()
    {
        $payroll_run_repo = new PayrollRunRepository();
        return $payroll_run_repo->query()->where('new_payee_flag',1)->where('payroll_run_approval_id', $this->id)->count();
    }






    /*EOFFICE FILES LABELS*/

    public function getFileSubjectAttribute()
    {
        $run_date = $this->payrollProc->run_date;
        $run_date_formatted = Carbon::parse($run_date)->format('M Y');
        $subject = 'MONTHLY PENSION - ' . $run_date_formatted . ' - ' . $this->filename;
        return $subject;
    }

    public function getFileDescriptionAttribute()
    {
        $run_date = $this->payrollProc->run_date;
        $run_date_formatted = Carbon::parse($run_date)->format('M Y');
        $description = 'Monthly Pension' . ' - ' . $run_date_formatted;
        return $description;
    }


}