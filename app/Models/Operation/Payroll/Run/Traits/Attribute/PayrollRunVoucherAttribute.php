<?php

namespace App\Models\Operation\Payroll\Run\Traits\Attribute;

use App\Models\Operation\Payroll\PayrollRun;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use Carbon\Carbon;

trait PayrollRunVoucherAttribute{


    /**
     * @param $id
     * @return string
     * voucher particular description.
     */
    public function voucherDescription()
    {
        $payroll_run_approval = $this->payrollRunApproval;
        $payroll_proc = $payroll_run_approval->payrollProc;
        $run_date = $payroll_proc->run_date;
        $run_date_format =  Carbon::parse($run_date)->format('M y');
        $description = 'Being Payment to the above named payee as Monthly Pension of WCF Beneficiaries for payroll of '. $run_date_format;
        return $description;
    }

    public function getIdFormattedAttribute(){
        return str_pad($this->id, 7, "0", STR_PAD_LEFT);
    }



    public function getAmountFormattedAttribute()
    {
        return number_2_format($this->amount);
    }
}