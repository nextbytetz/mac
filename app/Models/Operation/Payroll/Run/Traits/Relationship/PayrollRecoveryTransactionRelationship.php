<?php

namespace App\Models\Operation\Payroll\Run\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Finance\BankBranch;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;


/**
 * Class PayrollRecoveryTransactionRelationship
 */
trait PayrollRecoveryTransactionRelationship{



    //Relation to payroll run approval
    public function payrollRunApproval(){
        return $this->belongsTo(PayrollRunApproval::class);
    }


    /*Relation to payroll arrears*/
    public function payrollArrear(){
        return $this->belongsTo(PayrollArrear::class, 'payroll_recovery_id', 'payroll_recovery_id');
    }


    /*Relation to payroll deductions*/
    public function payrollDeduction(){
        return $this->belongsTo(PayrollDeduction::class, 'payroll_recovery_id', 'payroll_recovery_id');
    }

    /*Relation to payroll unclaims*/
    public function payrollUnclaim(){
        return $this->belongsTo(PayrollUnclaim::class, 'payroll_recovery_id', 'payroll_recovery_id');
    }

}