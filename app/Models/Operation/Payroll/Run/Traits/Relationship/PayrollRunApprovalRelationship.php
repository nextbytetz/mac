<?php

namespace App\Models\Operation\Payroll\Run\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Finance\BankBranch;
use App\Models\Finance\PayrollProc;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\Run\PayrollRecoveryTransaction;
use App\Models\Operation\Payroll\Run\PayrollRunVoucher;
use App\Models\Operation\Payroll\Run\PayrollSuspendedRun;
use App\Models\Workflow\WfTrack;


trait PayrollRunApprovalRelationship{


    //Relation to payroll runs
    public function payrollRuns(){
        return $this->hasMany(PayrollRun::class);
    }

    //Relation to payroll suspended runs
    public function payrollSuspendedRuns(){
        return $this->hasMany(PayrollSuspendedRun::class);
    }


    //Relation to payroll recovery transactions
    public function payrollRecoveryTransactions(){
        return $this->hasMany(PayrollRecoveryTransaction::class);
    }


    //Relation to payroll proc
    public function payrollProc(){
        return $this->belongsTo(PayrollProc::class);
    }

    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

    //Relation to payroll run vouchers
    public function payrollRunVouchers(){
        return $this->hasMany(PayrollRunVoucher::class);
    }



}