<?php

namespace App\Models\Operation\Payroll\Run\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Finance\BankBranch;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Models\Workflow\WfTrack;


trait PayrollSuspendedRunRelationship{



    //Relation to member type
    public function memberType(){
        return $this->belongsTo(MemberType::class);
    }

    //Relation to payroll run approval
    public function payrollRunApproval(){
        return $this->belongsTo(PayrollRunApproval::class);
    }

    //Relation to notification reports
    public function notificationReport(){
        return $this->belongsTo(NotificationReport::class);
    }
}