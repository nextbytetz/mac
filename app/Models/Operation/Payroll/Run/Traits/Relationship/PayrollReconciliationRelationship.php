<?php

namespace App\Models\Operation\Payroll\Run\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Finance\BankBranch;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;



trait PayrollReconciliationRelationship{




    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

}