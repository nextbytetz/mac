<?php

namespace App\Models\Operation\Payroll\Run\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Finance\Bank;
use App\Models\Finance\BankBranch;
use App\Models\Finance\PayrollProc;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\Run\PayrollRecoveryTransaction;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Models\Operation\Payroll\Run\PayrollRunVoucher;
use App\Models\Operation\Payroll\Run\PayrollSuspendedRun;
use App\Models\Workflow\WfTrack;


trait PayrollRunVoucherRelationship{


    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }



    public function payrollRunApproval()
    {
        return $this->belongsTo(PayrollRunApproval::class);
    }

}