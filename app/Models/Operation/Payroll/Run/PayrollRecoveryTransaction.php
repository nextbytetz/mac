<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Payroll\Run;

use App\Models\Operation\Payroll\Run\Traits\Relationship\PayrollRecoveryTransactionRelationship;
use App\Models\Operation\Payroll\Traits\Relationship\PayrollRunRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PayrollRecoveryTransaction extends Model implements AuditableContract
{
    //
    use  PayrollRecoveryTransactionRelationship, SoftDeletes,Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];
}