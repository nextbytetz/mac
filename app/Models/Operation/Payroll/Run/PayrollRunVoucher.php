<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Payroll\Run;

use App\Models\Operation\Payroll\Run\Traits\Attribute\PayrollRunApprovalAttribute;
use App\Models\Operation\Payroll\Run\Traits\Attribute\PayrollRunVoucherAttribute;
use App\Models\Operation\Payroll\Run\Traits\Relationship\PayrollRunApprovalRelationship;
use App\Models\Operation\Payroll\Run\Traits\Relationship\PayrollRunVoucherRelationship;
use App\Models\Operation\Payroll\Traits\Relationship\PayrollRunRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PayrollRunVoucher extends Model implements AuditableContract
{
    //
    use  SoftDeletes, PayrollRunVoucherRelationship, PayrollRunVoucherAttribute   ,Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];
}