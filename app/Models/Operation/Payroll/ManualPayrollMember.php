<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Payroll;

use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Payroll\Traits\Attribute\ManualPayrollMemberAttribute;
use App\Models\Operation\Payroll\Traits\Attribute\PensionerAttribute;
use App\Models\Operation\Payroll\Traits\Relationship\PensionerRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ManualPayrollMember extends Model implements AuditableContract
{
    //
    use SoftDeletes, Auditable, ManualPayrollMemberAttribute;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];




    /**
     *
     * relationship
     */

    //Relation to the MEMBER type
    public function memberType(){
        return $this->belongsTo(MemberType::class);
    }


    //Relation to the manual notification
    public function manualNotificationReport($incident_type){
        return $this->belongsTo(ManualNotificationReport::class, 'case_no', 'case_no')->where('incident_type_id', $incident_type);
    }
}