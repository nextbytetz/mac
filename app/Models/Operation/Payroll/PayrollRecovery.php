<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Payroll;

use App\Models\Operation\Payroll\Traits\Attribute\PayrollRecoveryAttribute;
use App\Models\Operation\Payroll\Traits\Attribute\PensionerAttribute;
use App\Models\Operation\Payroll\Traits\Relationship\PayrollRecoveryRelationship;
use App\Models\Operation\Payroll\Traits\Relationship\PensionerRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PayrollRecovery extends Model implements AuditableContract
{
    //
    use SoftDeletes, Auditable, PayrollRecoveryRelationship, PayrollRecoveryAttribute;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}