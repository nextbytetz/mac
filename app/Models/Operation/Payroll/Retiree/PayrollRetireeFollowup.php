<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Payroll\Retiree;

use App\Models\Auth\User;
use App\Models\Operation\Payroll\Traits\Attribute\PensionerAttribute;
use App\Models\Operation\Payroll\Traits\Relationship\PayrollAlertTaskRelationship;
use App\Models\Operation\Payroll\Traits\Relationship\PayrollArrearRelationship;
use App\Models\Operation\Payroll\Traits\Relationship\PensionerRelationship;
use App\Models\Sysdef\CodeValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PayrollRetireeFollowup extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;




    /**
     * RELATIONSHIP
     */

    public function retireeFeedback()
    {
        return $this->belongsTo(CodeValue::class, 'retiree_feedback_cv_id');
    }

    public function followupType()
    {
        return $this->belongsTo(CodeValue::class, 'follow_up_type_cv_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payrollRetireeMpUpdate()
    {
        return $this->hasOne(PayrollRetireeMpUpdate::class);
    }

    /*---Relationship---*/
}
