<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Payroll\Retiree;

use App\Models\Auth\User;
use App\Models\Operation\Payroll\PayrollMpChangeTrack;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\Traits\Attribute\PensionerAttribute;
use App\Models\Operation\Payroll\Traits\Relationship\PayrollAlertTaskRelationship;
use App\Models\Operation\Payroll\Traits\Relationship\PayrollArrearRelationship;
use App\Models\Operation\Payroll\Traits\Relationship\PensionerRelationship;
use App\Models\Workflow\WfTrack;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PayrollRetireeMpUpdate extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * ATTRIBUTE
     */

// Get status label
    public function getStatusLabelAttribute() {
        if($this->wf_done == 1)
        {
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
        }else{
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
        }
    }
    /*get Resource name for wf*/
    public function getResourceNameAttribute()
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($this->member_type_id, $this->resource_id);
        return $resource->name;
    }

    public function getDeductionArrearAbsAttribute()
    {
        if($this->deduction_amount >= 0)
        {
            return $this->deduction_amount;
        }else{
            return abs($this->deduction_amount);
        }
    }
    /*END Attribute*/

    /**
     * RELATIONSHIP
     */


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payrollRetireeFollowup()
    {
        return $this->belongsTo(PayrollRetireeFollowup::class);
    }


    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

    public function payrollRecovery(){
        return $this->morphOne(PayrollRecovery::class, 'entity_resource');
    }

    public function payrollMpChangeTracks(){
        return $this->morphMany(PayrollMpChangeTrack::class, 'trans_resource');
    }
    /*---Relationship---*/



}
