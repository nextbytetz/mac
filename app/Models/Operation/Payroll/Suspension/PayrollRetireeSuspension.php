<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Payroll\Suspension;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Dependent;
use App\Models\Operation\Payroll\Pensioner;
use App\Models\Operation\Payroll\Traits\Attribute\PensionerAttribute;
use App\Models\Operation\Payroll\Traits\Relationship\PensionerRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PayrollRetireeSuspension extends Model implements AuditableContract
{
    //
    use  Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    /**
     * --------RELATIONSHIP-------
     */
    /*Relation to pensioner*/
    public function pensioner(){
        return $this->belongsTo(Pensioner::class);
    }

}