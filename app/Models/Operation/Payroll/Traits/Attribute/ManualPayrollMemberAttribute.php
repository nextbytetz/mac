<?php

namespace App\Models\Operation\Payroll\Traits\Attribute;

use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use Carbon\Carbon;

trait ManualPayrollMemberAttribute{




// Get status label
    public function getStatusLabelAttribute() {
        if($this->issynced == 1)
        {
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Synchronized' . "'>" . 'Synchronized' . "</span>";
        }else{
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' ."'>" . 'Pending'. "</span>" ;
        }
    }


    /**
     * @return string
     *  Add arrears into payroll recovery through system
     */
    public function getAddArrearsButtonAttribute() {

        if($this->issynced == 1 && $this->hasarrears == 1){
                  return '<a href="' . route('backend.payroll.recovery.create_manual_arrears',[ 'manual_payroll_member' => $this->id,] ) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Add Arrears' . '"></i> Add Arrears </a> ';
        }else{
//            return $this->getStatusLabelAttribute();
        }
    }

    /**
     * @return string
     *  Add survivor into the system
     */
    public function getAddButtonAttribute() {

        if($this->issynced == 0 && $this->isSurvivorReadyToCreate()){
            return '<a href="' . route('backend.payroll.manual_member.create_dependent', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Add Survivor </a> ';
        }else{
            return $this->getStatusLabelAttribute();
        }
            }




    public function getActionButtonAttribute(){

        return $this->getAddButtonAttribute();
    }


    public function isSurvivorReadyToCreate(){
        $count = 0;
        if($this->member_type_id == 4){
            /*for death check if survivor is uploaded and synced*/
            $count_notification_exist = $this->manualNotificationReport($this->incident_type_id)->count();
            $count_synced = $this->manualNotificationReport($this->incident_type_id)->where('has_payroll', 1)->count();
            return ($count_notification_exist > 0 && $count_synced > 0) ? true : false;
        }
        return true;
    }


}