<?php

namespace App\Models\Operation\Payroll\Traits\Attribute;

use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use Carbon\Carbon;
/**
 * Class trait PayrollStatusChangeAttribute{

 */
trait PayrollStatusChangeAttribute{



// Get status label
    public function getStatusLabelAttribute() {
        if($this->isWfComplete())
        {
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
        }else{
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
        }
    }

    /*Created at formatted short date*/
    public function getCreatedAtFormattedAttribute()
    {
        return short_date_format($this->created_at);
    }

    /**
     * @return bool
     * Workflow is complete
     */
    public function isWfComplete()
    {
        return $this->wf_done == 1;
    }

    /*get Resource name for wf*/
    public function getResourceNameAttribute()
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($this->member_type_id, $this->resource_id);
          return $resource->name;
    }
}