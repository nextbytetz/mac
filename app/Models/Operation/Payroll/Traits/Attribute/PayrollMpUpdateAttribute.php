<?php

namespace App\Models\Operation\Payroll\Traits\Attribute;

use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use Carbon\Carbon;
/**
 * Class trait PayrollBankInfoUpdateAttribute{

 */
trait PayrollMpUpdateAttribute{



// Get status label
    public function getStatusLabelAttribute() {
        if ($this->wf_done == 0  && $this->wfTracks()->count()  == 0)
            return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending' . "</span>";
        elseif($this->wf_done == 0 && $this->wfTracks()->count()  > 0){
            return "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='" . 'Initiated'. "'>" . 'Initiated' . "</span>";
        }elseif($this->wf_done == 1){
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
        }elseif($this->wf_done == 2){
            return "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . 'Declined' . "'>" . 'Declined' . "</span>";
        }
    }

    /*Created at formatted short date*/
    public function getCreatedAtFormattedAttribute()
    {
        return short_date_format($this->created_at);
    }



    /*get Resource name for wf*/
    public function getResourceNameAttribute()
    {

        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($this->member_type_id, $this->resource_id);
        return $resource->name;
    }

}