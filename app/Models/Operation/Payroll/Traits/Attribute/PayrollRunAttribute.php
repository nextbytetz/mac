<?php

namespace App\Models\Operation\Payroll\Traits\Attribute;

use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use Carbon\Carbon;
/**
 * Class trait PayrollRecoveryAttribute{

 */
trait PayrollRunAttribute{


    public function getBenefitTypeIdAttribute()
    {
        $member_type_id = $this->member_type_id;
        $isconstantcare = $this->isconstantcare;

        if($member_type_id == 5)
        {
            /*pensioners*/
            return 18;

        }elseif($member_type_id == 4 && $isconstantcare == 0)
        {
            /*dependents*/
            return 6;
        }elseif($member_type_id == 4 && $isconstantcare == 1)
        {
            /*constant care*/
            return 5;
        }
    }

}