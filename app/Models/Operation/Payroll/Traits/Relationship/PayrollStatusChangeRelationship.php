<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Operation\Payroll\PayrollChildExtension;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;



/**
 * Class PayrollBankInfoUpdateRelationship
 */
trait PayrollStatusChangeRelationship{



    //Relation to status change type
    public function statusChangeType(){
        return $this->belongsTo(CodeValue::class, 'status_change_cv_id');
    }



    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }


    //Relation to User
    public function user(){
        return $this->belongsTo(User::class);
    }

    //Relation to payroll verification
    public function payrollVerification(){
        return $this->hasOne(PayrollVerification::class);
    }

    //Relation to payroll child extension
    public function payrollChildExtension(){
        return $this->hasOne(PayrollChildExtension::class);
    }

}