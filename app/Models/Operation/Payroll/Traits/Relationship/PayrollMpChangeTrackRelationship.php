<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;



/**
 * Class PayrollMpUpdateRelationship
 */
trait PayrollMpChangeTrackRelationship{



    //Relation to workflow
    public function transResource(){
        return $this->morphTo();
    }




}