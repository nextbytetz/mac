<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Finance\Bank;
use App\Models\Finance\BankBranch;
use App\Models\Location\District;
use App\Models\Location\Region;
use App\Models\Operation\Claim\Claim;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Payroll\ManualPayrollMember;
use App\Models\Operation\Payroll\ManualPayrollRun;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Models\Operation\Payroll\PayrollBeneficiaryUpdate;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeFollowup;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeMpUpdate;
use App\Models\Operation\Payroll\Run\PayrollSuspendedRun;


/**
 * Class PensionerRelationship
 */
trait PensionerRelationship{





    //Relation to the employee
    public function employee(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class);
    }


    //Relation to the member type
    public function memberType(){
        return $this->belongsTo(\App\Models\Operation\Claim\MemberType::class);
    }


    /*relation to notification report */
    public function notificationReport(){
        return $this->belongsTo(NotificationReport::class);
    }


    //Relation to the bank branch
    public function bankBranch(){
        return $this->belongsTo(BankBranch::class);
    }

    //Relation to the bank
    public function bank(){
        return $this->belongsTo(Bank::class);
    }

    /*Relation to recoveries where payroll recovery member_type_id = 5*/
    public function payrollRecoveries()
    {
        return $this->hasMany(PayrollRecovery::class, 'resource_id', 'id')->where('member_type_id', 5);

    }
    public function payrollBankUpdates()
    {
        return $this->hasMany(PayrollBankInfoUpdate::class, 'resource_id', 'id')->where('member_type_id', 5);

    }

    public function payrollBeneficiaryUpdates()
    {
        return $this->hasMany(PayrollBeneficiaryUpdate::class, 'resource_id', 'id')->where('member_type_id', 5);

    }
    public function payrollVerifications()
    {
        return $this->hasMany(PayrollVerification::class, 'resource_id', 'id')->where('member_type_id', 5);

    }

    public function payrollStatusChanges()
    {

        return $this->hasMany(PayrollStatusChange::class, 'resource_id', 'id')->where('member_type_id', 5);
    }
    /*Payroll runs*/
    public function payrollRuns()
    {
        return $this->hasMany(PayrollRun::class, 'resource_id', 'id')->where('member_type_id', 5);

    }


    public function payrollSuspendedRuns()
    {
        return $this->hasMany(PayrollSuspendedRun::class, 'resource_id', 'id')->where('member_type_id', 5);

    }

    /*Relation to claim through notification report*/
    public function claim()
    {
        return $this->belongsTo(Claim::class, 'notification_report_id', 'notification_report_id');
    }



    public function documents()
    {
        return   $this->belongsToMany(Document::class, "document_payroll_beneficiary", 'resource_id')->wherePivot('member_type_id', 5)->withPivot('id', 'name','description','doc_date', 'doc_receive_date', 'doc_create_date', 'eoffice_document_id', 'folio', 'isused', 'ispending')->withTimestamps();
    }

    /*Relation to manual notification report*/
    public function manualNotificationReport()
    {
        return $this->belongsTo(ManualNotificationReport::class);
    }

    /*Relation to manual payroll member*/
    public function manualPayrollMember()
    {
        return $this->hasOne(ManualPayrollMember::class, 'resource_id','id')->where('member_type_id', 5);
    }


    /*Relation to manual payroll runs*/
    public function manualPayrollRuns()
    {
        return $this->hasMany(ManualPayrollRun::class, 'resource_id','id')->where('member_type_id', 5);
    }


    public function region(){
        return $this->belongsTo(Region::class);
    }


    public function district(){
        return $this->belongsTo(District::class);
    }


    public function retireeFollowups(){
        return $this->hasMany(PayrollRetireeFollowup::class, 'resource_id')->where('member_type_id', 5);
    }

    public function retireeMpUpdates(){
        return $this->hasMany(PayrollRetireeMpUpdate::class, 'resource_id')->where('member_type_id', 5);
    }
}