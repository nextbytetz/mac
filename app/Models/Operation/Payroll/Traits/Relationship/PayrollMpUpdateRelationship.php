<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;



/**
 * Class PayrollMpUpdateRelationship
 */
trait PayrollMpUpdateRelationship{



    //Relation to workflow
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }


    //Relation to User
    public function user(){
        return $this->belongsTo(User::class);
    }


}