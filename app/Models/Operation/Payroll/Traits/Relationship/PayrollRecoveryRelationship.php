<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;


/**
 * Class PayrollRecoveryRelationship
 */
trait PayrollRecoveryRelationship{


    //Relation to the deduction
    public function payrollDeduction(){
        return $this->hasOne(PayrollDeduction::class);
    }

    //Relation to the Arrears
    public function payrollArrear(){
        return $this->hasOne(PayrollArrear::class);
    }

    //Relation to the unclaims
    public function PayrollUnclaim(){
        return $this->hasOne(PayrollUnclaim::class);
    }

    //Relation to the recovery types - code value
    public function recoveryType(){
        return $this->belongsTo(CodeValue::class, 'recovery_type_cv_id');
    }

    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

    /*Relation to user*/
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function entityResource()
    {
        return $this->morphTo();
    }
}