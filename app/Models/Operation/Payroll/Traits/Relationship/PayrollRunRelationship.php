<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;


/**
 * Class PayrollRunRelationship
 */
trait PayrollRunRelationship{


    //Relation to the payroll run approval
    public function payrollRunApproval(){
        return $this->belongsTo(PayrollRunApproval::class);
    }


    //Relation to the bank branch
    public function bankBranch(){
        return $this->belongsTo(\App\Models\Finance\BankBranch::class);
    }

    //Relation to the member type
    public function memberType(){
        return $this->belongsTo(\App\Models\Operation\Claim\MemberType::class);
    }
}