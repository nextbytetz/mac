<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Legal\District;
use App\Models\Location\Region;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Sysdef\CodeValue;
use App\Models\Task\Checker;
use App\Models\Workflow\WfTrack;



trait PayrollAlertTaskRelationship{


    /*Relation to user*/
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function memberType(){
        return $this->belongsTo(MemberType::class);
    }

    public function stage(){
        return $this->belongsTo(CodeValue::class, 'staging_cv_id');
    }


    public function checkers()
    {
        return $this->morphMany(Checker::class, "resource");
    }
}