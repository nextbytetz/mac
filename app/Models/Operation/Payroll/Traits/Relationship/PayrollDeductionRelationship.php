<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;



trait PayrollDeductionRelationship{


    /*Relation to payroll recovery*/
    public function payrollRecovery(){
        return $this->belongsTo(PayrollRecovery::class);
    }
}