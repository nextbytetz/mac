<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Legal\District;
use App\Models\Location\Region;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;


/**
 * Class PayrollRecoveryRelationship
 */
trait PayrollBeneficiaryUpdateRelationship{


    //Relation to the member type
    public function memberType(){
        return $this->belongsTo(MemberType::class);
    }


    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

    /*Relation to user*/
    public function user(){
        return $this->belongsTo(User::class);
    }

    /*Relation to region*/
    public function region(){
        return $this->belongsTo(Region::class);
    }

    /*Relation to district*/
    public function district(){
        return $this->belongsTo(District::class);
    }
}