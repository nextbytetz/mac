<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Finance\BankBranch;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Workflow\WfTrack;



trait ManualPayrollRunRelationship{


    //Relation to member type
    public function memberType(){
        return $this->belongsTo(MemberType::class);
    }

    //Relation to employee
    public function employee(){
        return $this->belongsTo(Employee::class);
    }

}