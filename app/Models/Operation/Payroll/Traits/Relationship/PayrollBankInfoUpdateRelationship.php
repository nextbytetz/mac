<?php

namespace App\Models\Operation\Payroll\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Finance\Bank;
use App\Models\Finance\BankBranch;
use App\Models\Workflow\WfTrack;


/**
 * Class PayrollBankInfoUpdateRelationship
 */
trait PayrollBankInfoUpdateRelationship{


    //Relation to the bank
    public function newBank(){
        return $this->belongsTo(Bank::class, 'new_bank_id');
    }

    public function newBankBranch(){
        return $this->belongsTo(BankBranch::class, 'new_bank_branch_id');
    }

    //Relation to old the bank branch
    public function oldBankBranch(){
        return $this->belongsTo(BankBranch::class, 'old_bank_branch_id');
    }

    public function oldBank(){
        return $this->belongsTo(Bank::class, 'old_bank_id');
    }

    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

    /*Relation to user*/
    public function user(){
        return $this->belongsTo(User::class);
    }

}