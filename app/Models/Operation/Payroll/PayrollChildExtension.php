<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Payroll;

use App\Models\Operation\Payroll\Traits\Attribute\PayrollStatusChangeAttribute;
use App\Models\Operation\Payroll\Traits\Attribute\PensionerAttribute;
use App\Models\Operation\Payroll\Traits\Relationship\PayrollStatusChangeRelationship;
use App\Models\Operation\Payroll\Traits\Relationship\PensionerRelationship;
use App\Models\Sysdef\CodeValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PayrollChildExtension extends Model implements AuditableContract
{
    //
    use SoftDeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    /**
     * ATTRIBUTE
     */
    public function getPeriodProvisionTypeLabelAttribute()
    {
        switch($this->period_provision_type){
            case 1:
                return 'Specific Period';
                break;

            case 2:
                return 'Lifetime';
                break;
        }
    }

    /*---Attribute------*/



    /**
     *
     * RELATIONSHIP
     */

    /*child continuation type for child reinstate*/
    public function childContinuationReason(){
        return $this->belongsTo(CodeValue::class, 'child_continuation_reason_cv_id');
    }

    public function educationLevel(){
        return $this->belongsTo(CodeValue::class, 'education_level_cv_id');
    }

}