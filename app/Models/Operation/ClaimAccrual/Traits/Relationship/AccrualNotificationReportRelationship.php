<?php

namespace App\Models\Operation\ClaimAccrual\Traits\Relationship;

use Carbon\Carbon;
/**
 * Class AccrualNotificationReportRelationship
 */
trait AccrualNotificationReportRelationship {

    public function notificationReport()
    {
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
    }

    public function checkers()
    {
        return $this->morphMany(\App\Models\Task\Checker::class, "resource");
    }

    public function wfTracks()
    {
        return $this->morphMany(\App\Models\Workflow\WfTrack::class, 'resource');
    }

    public function accrualNotificationBenefits(){
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualNotificationBenefit::class);
    }

    public function accrualBenefitDisabilities(){
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualBenefitDisability::class);
    }

    public function accrualNotificationBenefit(){
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualNotificationBenefit::class);
    }

    // public function accrualBenefitDisabilities(){
    //     return $this->hasManyThrough(\App\Models\Operation\ClaimAccrual\AccrualBenefitDisability::class, \App\Models\Operation\ClaimAccrual\AccrualNotificationBenefit::class);
    // }



}