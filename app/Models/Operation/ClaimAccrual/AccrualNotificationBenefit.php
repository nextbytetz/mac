<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\ClaimAccrual;

// use App\Models\Operation\Claim\Traits\Attribute\AccrualNotificationBenefitAttribute;
// use App\Models\Operation\Claim\Traits\Relationship\AccrualNotificationBenefitRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class AccrualNotificationBenefit extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    // protected $auditableEvents = [
    //     'deleted',
    //     'updated',
    //     'restored',
    // ];

    public function AccrualNotificationReport()
    {
        return $this->belongsTo(\App\Models\Operation\ClaimAccrual\AccrualNotificationReport::class);
    }
    
    public function accrualBenefitDisabilities(){
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualBenefitDisability::class);
    }
    

}


