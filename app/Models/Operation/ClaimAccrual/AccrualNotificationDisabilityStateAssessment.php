<?php

namespace App\Models\Operation\ClaimAccrual;

use Illuminate\Database\Eloquent\Model;


use App\Models\Operation\Claim\Traits\Attribute\NotificationDisabilityStateAttribute;
use App\Models\Operation\Claim\Traits\Relationship\NotificationDisabilityStateAssessmentRelationship;
use App\Models\Operation\Claim\Traits\Relationship\NotificationDisabilityStateRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class AccrualNotificationDisabilityStateAssessment extends Model
{
     use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    // protected $auditableEvents = [
    //     'deleted',
    //     'updated',
    //     // 'restored',
    // ];

    //Relation to the notification disability state
    public function accrualNotificationDisabilityState(){
        return $this->belongsTo(\App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityState::class);
    }
}