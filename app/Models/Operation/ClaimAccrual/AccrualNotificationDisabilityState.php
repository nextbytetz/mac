<?php

namespace App\Models\Operation\ClaimAccrual;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AccrualNotificationDisabilityState extends Model
{
	use Softdeletes;

	protected $guarded = [];
	public $timestamps = true;

        //Relation to the disability state checklis
	public function disabilityStateChecklist(){
		return $this->belongsTo(\App\Models\Operation\Claim\DisabilityStateChecklist::class);
	}

    //Relation to the medicla Expense
	public function accrualMedicalExpense(){
		return $this->belongsTo(\App\Models\Operation\ClaimAccrual\AccrualMedicalExpense::class);
	}

    //Relation to the notification disability state assessment
	public function accrualNotificationDisabilityStateAssessment(){
		return $this->hasOne(\App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityStateAssessment::class)->withDefault([
			'days' => NULL,
			'remarks' => NULL,
		]);
	}

	public function benefit()
	{
		return $this->belongsTo(NotificationEligibleBenefit::class, "notification_eligible_benefit_id");
	}

	// Get from date formatted
    public function getFromDateFormattedAttribute() {
        return  Carbon::parse($this->from_date)->format('d-M-Y');
    }

// Get to date formatted
    public function getToDateFormattedAttribute() {
        return  Carbon::parse($this->to_date)->format('d-M-Y');
    }
}
