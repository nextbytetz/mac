<?php

namespace App\Models\Operation\ClaimAccrual;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccrualMedicalExpense extends Model
{
	use Softdeletes;

	protected $guarded = [];
	public $timestamps = true;

     //Relation to the Notification report
	public function notificationReport(){
		return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
	}

	    //Relation to the Member Type
	public function memberType(){
		return $this->belongsTo(\App\Models\Operation\Claim\MemberType::class);
	}

    //Relation to the employer
	public function employer(){
		return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class, 'resource_id', 'id');
	}

    //Relation to the Insurance
	public function insurance(){
		return $this->belongsTo(\App\Models\Operation\Claim\Insurance::class, 'resource_id', 'id');
	}

	public function employee(){
		return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class, 'resource_id', 'id');
	}

	public function maeSubs()
	{
		return $this->hasMany(MaeSub::class);
	}

    //Relation to the Notification HEALTH provider services
	public function accrualNotificationHealthProviderServices(){
		return $this->hasManyThrough(\App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderService::class, \App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProvider::class);
	}

    //Relation to the Notification HEALTH provider
	public function accrualNotificationHealthProviders() {
		return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProvider::class);
	}



    //Relation to the notification disability states
	public function accrualNotificationDisabilityStates(){
		return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityState::class);
	}


    //Relation to the notification disability states assessment
	public function accrualNotificationDisabilityStateAssessments(){
		return $this->hasManyThrough(\App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityStateAssessment::class,\App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityState::class);
	}

	public function getCompensatedEntityNameAttribute(){
        //employer
		if ($this->getMemberType() == 1){
			return !is_null($this->employer) ? $this->employer->name : null;
		} elseif ($this->getMemberType() == 3){
			return !is_null($this->insurance) ? $this->insurance->name : null;
		}elseif($this->getMemberType() == 2){
			return !is_null($this->employee) ? $this->employee->name : null;
		}elseif($this->getMemberType() == 4){
			return !is_null($this->dependent) ? $this->dependent->name : null;
		}
	}


	public function getAmountFormattedAttribute() {
		$amount = number_format($this->amount , 2 , '.' , ',' );
		return $amount;
	}

	public function getAssessedAmountFormattedAttribute()
	{
		$return = "-";
		if (!is_null($this->assessed_amount)) {
			$return = number_2_format($this->assessed_amount);
		}
		return $return;
	}
    /**
     * @return mixed
     * Get Name of compensated  with amount
     * @deprecated
     */
    public function getCompensatedEntityNameWithAmountAttribute(){

    	return $this->getCompensatedEntityNameAttribute() . ' (' . number_2_format($this->amount) . ')';
    }

    public function getMemberType(){
    	return $this->member_type_id;
    }

    public function getAssessButtonAttribute() {

    	return '<a href="' . route('backend.claim.notification_report.claim_assessment_checklist', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-list-alt" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.assess') . '"></i></a> ';

    }
    
    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
    	return $this->getAssessButtonAttribute();

    }

    public function getAccrualActionButtonsAttribute() {
    	return '<a href="' . route('backend.claim.claim_accrual.notification_report.claim_assessment_checklist', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-list-alt" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.assess') . '"></i></a> ';

    }

}
