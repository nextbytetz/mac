<?php

namespace App\Models\Operation\ClaimAccrual;

use Illuminate\Database\Eloquent\Model;

class AccrualDeaultDateTrigger extends Model
{
    protected $guarded = [];
	public $timestamps = true;
}
