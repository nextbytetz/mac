<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\ClaimAccrual;

use App\Models\Operation\Payroll\Traits\Attribute\PensionerAttribute;
use App\Models\Operation\Payroll\Traits\Relationship\PensionerRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Repositories\Backend\Operation\Payroll\PayrollArrearRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollDeductionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollUnclaimRepository;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use App\Models\Finance\Bank;
use App\Models\Finance\BankBranch;
use App\Models\Location\District;
use App\Models\Location\Region;
use App\Models\Operation\Claim\Claim;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Payroll\ManualPayrollMember;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Models\Operation\Payroll\PayrollBeneficiaryUpdate;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Models\Operation\Payroll\Run\PayrollSuspendedRun;

class AccrualPensioner extends Model
{
    //
	use SoftDeletes, Auditable;

	protected $guarded = [];
	public $timestamps = true;


    /**
     * @var array
     */
    // protected $auditableEvents = [
    //     'deleted',
    //     'updated',
    //     'restored',
    // ];

    use FileHandler;

    //Relation to the employee
    public function employee(){
    	return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class);
    }


    //Relation to the member type
    public function memberType(){
    	return $this->belongsTo(\App\Models\Operation\Claim\MemberType::class);
    }


    /*relation to notification report */
    public function notificationReport(){
    	return $this->belongsTo(NotificationReport::class);
    }


    //Relation to the bank branch
    public function bankBranch(){
    	return $this->belongsTo(BankBranch::class);
    }

    //Relation to the bank
    public function bank(){
    	return $this->belongsTo(Bank::class);
    }

    /*Relation to recoveries where payroll recovery member_type_id = 5*/
    public function payrollRecoveries()
    {
    	return $this->hasMany(PayrollRecovery::class, 'resource_id', 'id')->where('member_type_id', 5);

    }
    public function payrollBankUpdates()
    {
    	return $this->hasMany(PayrollBankInfoUpdate::class, 'resource_id', 'id')->where('member_type_id', 5);

    }

    public function payrollBeneficiaryUpdates()
    {
    	return $this->hasMany(PayrollBeneficiaryUpdate::class, 'resource_id', 'id')->where('member_type_id', 5);

    }
    public function payrollVerifications()
    {
    	return $this->hasMany(PayrollVerification::class, 'resource_id', 'id')->where('member_type_id', 5);

    }

    /*Payroll runs*/
    public function payrollRuns()
    {
    	return $this->hasMany(PayrollRun::class, 'resource_id', 'id')->where('member_type_id', 5);

    }


    public function payrollSuspendedRuns()
    {
    	return $this->hasMany(PayrollSuspendedRun::class, 'resource_id', 'id')->where('member_type_id', 5);

    }

    /*Relation to claim through notification report*/
    public function claim()
    {
    	return $this->belongsTo(Claim::class, 'notification_report_id', 'notification_report_id');
    }



    public function documents()
    {
    	return   $this->belongsToMany(Document::class, "document_payroll_beneficiary", 'resource_id')->wherePivot('member_type_id', 5)->withPivot('id', 'name','description','doc_date', 'doc_receive_date', 'doc_create_date', 'eoffice_document_id', 'folio', 'isused', 'ispending')->withTimestamps();
    }

    /*Relation to manual notification report*/
    public function manualNotificationReport()
    {
    	return $this->belongsTo(ManualNotificationReport::class);
    }

    /*Relation to manual payroll member*/
    public function manualPayrollMember()
    {
    	return $this->hasOne(ManualPayrollMember::class, 'resource_id','id')->where('member_type_id', 5);
    }


    public function region(){
    	return $this->belongsTo(Region::class);
    }


    public function district(){
    	return $this->belongsTo(District::class);
    }

// Get from date formatted
    public function getNameAttribute() {
    	return ucfirst(strtolower($this->firstname)) . " " . ucfirst(strtolower($this->middlename)) . " " . ucfirst(strtolower($this->lastname));
    }

    /*Created at formatted short date*/
    public function getCreatedAtFormattedAttribute()
    {
    	return short_date_format($this->created_at);
    }

    /*Dob formatted short date*/
    public function getDobFormattedAttribute()
    {
    	return  isset($this->dob) ?  short_date_format($this->dob) : ' ';
    }

//    Age
    public function getAgeAttribute()
    {
    	$today =    Carbon::now();
    	return age($this->dob, $today);
    }

    /*Age label*/
    public function getAgeLabelAttribute()
    {
    	return $this->getDobFormattedAttribute() . ' (' . $this->getAgeAttribute() . ')';
    }

    /*Last response formatted short date*/
    public function getLastResponseFormattedAttribute()
    {
    	return ($this->lastresponse) ? short_date_format($this->lastresponse) : ' ';
    }


    public function getDaysLeftNextVerificationAttribute()
    {
    	$last_response =  isset($this->lastresponse) ? Carbon::parse($this->lastresponse) : null;
    	$cut_off_date = isset($last_response) ? $last_response->addMonthNoOverflow(payroll_verification_limit_months()) : Carbon::now();
    	return ((comparable_date_format($cut_off_date) > comparable_date_format(Carbon::now())) ? Carbon::now()->diffInDays($cut_off_date) : 'Passed Deadline');
    }

    public function getMpFormattedAttribute()
    {
    	return number_2_format($this->monthly_pension_amount);
    }

    /*address*/
    public function address()
    {
    	return ' ';
    }

    /*Employer name*/
    public function getEmployerNameAttribute()
    {
    	$name = isset($this->notification_report_id) ? $this->notificationReport->employer->name : $this->manualNotificationReport->employer->name;
    	return $name;
    }

    /*Get status of the pensioner*/
    public function getStatusLabelAttribute()
    {
    	if($this->isDeactivated()){
    		return   "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Deactivated'. "'>" . 'Deactivated'. "</span>";
    	}elseif ($this->isSuspended()){
    		return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Suspended'. "'>" . 'Suspended'. "</span>";
    	}elseif($this->isActive()){
    		return   "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active'. "'>" . 'Active'. "</span>";
    	}
    	elseif($this->isInactive()){
    		return   "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Inactive'. "'>" . 'Inactive'. "</span>";
    	}else{
    		return   "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>";
    	}
    }

    /*Get First pay flag status of the pensioner*/
    public function getFirstPayStatusLabelAttribute()
    {
    	if ($this->isFirstPaid()){
    		$manual_status = ($this->firstpay_manual == 1) ? ' - Started from manual' : '';
    		return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid'. "'>" . 'Paid'. "</span>" . $manual_status;
    	}else{
    		return   "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>";
    	}
    }


    public function isSuspended()
    {
    	return $this->suspense_flag == 1;
    }

    public function isActive()
    {
    	return $this->isactive == 1;
    }

    public function isInactive()
    {
    	return $this->isactive == 0;
    }


    public function isDeactivated()
    {
    	return $this->isactive == 2;
    }


    public function isFirstPaid()
    {
    	return $this->firstpay_flag == 1;
    }

    /*CHeck if has payroll on the system*/
    public function getFirstPayFlagSystemAttribute($employee_id)
    {
    	$check_if_has_system_payroll = (new PayrollRunRepository())->getMpForDataTable()->where('payroll_runs.member_type_id', 5)->where('payroll_runs.resource_id', $this->id)->where('payroll_runs.employee_id', $employee_id)->count();
    	$first_pay_flag_system = ($check_if_has_system_payroll == 0) ? 0 : 1;
    	return $first_pay_flag_system;
    }

    /**
     * Get Start payroll payment date i.e. death date and date of mmi
     */
    public function getStartPayDateAttribute()
    {
    	$payroll_runs = new PayrollRunRepository();
    	$start_pay_date = (isset($this->notification_report_id)) ?   $payroll_runs->getStartPayDate($this->notificationReport) : $payroll_runs->getStartPayDateManualFile($this->manualNotificationReport);
    	return $start_pay_date;
    }



    /*Check if has pending payroll recovery -- start---*/

    /*Get Active payroll arrears*/
    public function getActivePayrollArrearsCountAttribute()
    {
    	$payroll_arrears = new PayrollArrearRepository();
    	$count = $payroll_arrears->getActiveArrearsCount(5, $this->id);
    	return $count;
    }


    /*Get Active payroll deductions*/
    public function getActivePayrollDeductionsCountAttribute()
    {
    	$payroll_deductions = new PayrollDeductionRepository();
    	$count = $payroll_deductions->getActiveDeductionsCount(5, $this->id);
    	return $count;
    }


    /*Get Active payroll unclaims*/
    public function getActivePayrollUnclaimsCountAttribute()
    {
    	$payroll_unclaims = new PayrollUnclaimRepository();
    	$count = $payroll_unclaims->getActiveUnclaimsCount(5, $this->id);
    	return $count;
    }

    /*---end payroll recovery---*/


    /*Get status label for mp payment balance if overpaid / underpaid*/
    public function getMpPaymentStatusLabelAttribute()
    {
    	$payroll_runs = new PayrollRunRepository();
    	$mp_balance = $payroll_runs->getMpBalanceForMember(5, $this->id, $this->employee_id);
    	if($mp_balance > 0)
    	{
    		/*overpaid*/
    		return  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'OVERPAID'. "'>" . 'OVERPAID'. "</span>";
    	}elseif($mp_balance < 0){
    		/*underpaid*/
    		return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'UNDERPAID'. "'>" . 'UNDERPAID'. "</span>";
    	}

    }


    /*E-OFFICE FILE LABELS*/

    public function getFilenameDmsAttribute()
    {
    	$filename = "";
    	$filename = $this->getNameAttribute() . '/'. $this->id;
    	return $filename;
    }

    public function getFileSubjectAttribute()
    {
    	$subject = "";
    	$subject ='PENSIONER - ' . $this->getNameAttribute() . '/'. $this->id ;
    	return $subject;
    }


    /**
     * @return string
     * Get pensioner directory for documents
     */
    public function getDirectoryAttribute()
    {
    	$payroll_dir =  $this->real(payroll_dir() . DIRECTORY_SEPARATOR);
    	$pensioner_dir = $payroll_dir . DIRECTORY_SEPARATOR . 'pensioner' . DIRECTORY_SEPARATOR . $this->id;
    	return $pensioner_dir;
    }


    public function getUrlAttribute()
    {
    	$payroll_url =  payroll_url();
    	$pensioner_url = $payroll_url . DIRECTORY_SEPARATOR . 'pensioner' . DIRECTORY_SEPARATOR . $this->id;
    	return $pensioner_url;
    }


    /**
     * @param $document_type
     * Get pending document for validation in payroll approval action
     */
    public function getPendingDocumentForValidationPerType($document_type)
    {
    	$payroll_repo = new PayrollRepository();
    	$documents = $payroll_repo->getBeneficiaryDocumentsPendingValidation(5,$this->id, $document_type);
    	return $documents;
    }

    /**
     * @param $document_type
     * Get pending document for validation in payroll approval action
     */
    public function getDocumentUsedForValidationPerType($document_type)
    {
    	$payroll_repo = new PayrollRepository();
    	$documents = $payroll_repo->getBeneficiaryDocumentValidatedPerType(5,$this->id, $document_type);
    	return $documents;
    }

    /**
     * @return string
     * Get missing details alert
     */
    public function getMissingDetailsAlertAttribute(){
    	$dob = !isset($this->dob) ? 'Dob not filled, ' : '';
    	$phone = !isset($this->phone) ? 'Phone not filled, ' : '';
    	$bank = (!isset($this->accountno) || !isset($this->bank_id) || $this->bank_id == 5 || $this->bank_id == 6)  ? 'Bank details not filled' : '';
    	return $dob . $phone . $bank;
    }


    public function getSuspendedDateFormattedAttribute()
    {
    	if($this->suspense_flag == 1){
    		$suspended_date = $this->suspended_date;
    	}
    	return (isset($suspended_date) ? short_date_format($suspended_date) : '');
    }


    /*Last verification date attribute*/
    public function getLastVerificationDateAttribute()
    {
    	$check= $this->payrollVerifications()->count();
    	if($check > 0)
    	{
    		return $this->lastresponse;
    	}else{
    		return null;
    	}
    }

}