<?php
namespace App\Models\Operation\ClaimAccrual;

// use App\Models\Operation\Claim\Traits\Attribute\AccrualNotificationReportAttribute;
use App\Models\Operation\ClaimAccrual\Traits\Relationship\AccrualNotificationReportRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class AccrualNotificationReport extends Model
{
    use AccrualNotificationReportRelationship, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    // protected $auditableEvents = [
    //     'deleted',
    //     'updated',
    //     'restored',
    // ];


    public function getResourceNameAttribute()
    {
        $return = '';
        $notification_report = \DB::table('main.notification_reports')->where('id',$this->notification_report_id)->first();
        if (count($notification_report)) {
            $return = strtoupper($notification_report->filename);
        }
        return $return;
    }
}