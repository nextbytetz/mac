<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\ClaimAccrual;

use App\Models\Operation\Compliance\Member\Traits\Attribute\DependentAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\DependentRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Operation\Payroll\Suspension\PayrollChildSuspension;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollArrearRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollDeductionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollUnclaimRepository;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

use App\Models\Finance\Bank;
use App\Models\Location\District;
use App\Models\Location\Region;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Payroll\ManualPayrollMember;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Models\Operation\Payroll\PayrollBeneficiaryUpdate;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Models\Operation\Payroll\Run\PayrollSuspendedRun;
use App\Models\Sysdef\Gender;

class AccrualDependent extends Model
{
    //
	use SoftDeletes;
	protected $guarded = [];
	public $timestamps = true;


    // /**
    //  * @var array
    //  */
    // protected $auditableEvents = [
    //     'deleted',
    //     'updated',
    //     'restored',
    // ];

	use FileHandler;

	public function employee(){
		return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class);
	}

	/* Relation to employees*/
	public function employees(){
		return $this->belongsToMany(\App\Models\Operation\Compliance\Member\Employee::class)->withPivot('id','dependent_type_id', 'survivor_pension_percent', 'survivor_pension_amount', 'survivor_pension_flag', 'survivor_gratuity_percent', 'survivor_gratuity_flag', 'survivor_gratuity_pay_flag', 'funeral_grant_percent', 'funeral_grant', 'funeral_grant_pay_flag', 'survivor_gratuity_amount', 'dependency_percentage', 'isactive', 'isresponded', 'firstpay_flag', 'parent_id', 'notification_report_id', 'manual_notification_report_id', 'hold_reason', 'isotherdep','other_dependency_type', 'pay_period_months','recycles_pay_period')->withTimestamps();
	}

	/*Relation to gender*/
	public function gender()
	{
		return $this->belongsTo(Gender::class);
	}

    //Relation to the Bank Branch
	public function bankBranch(){
		return $this->belongsTo(\App\Models\Finance\BankBranch::class);
	}

    //Relation to the Bank
	public function bank(){
		return $this->belongsTo(Bank::class);
	}

	public function documents()
	{
		return $this->belongsToMany(Document::class, "document_payroll_beneficiary", 'resource_id')->wherePivot('member_type_id', 4)->withPivot('id', 'name','description','doc_date', 'doc_receive_date', 'doc_create_date', 'eoffice_document_id', 'folio', 'isused', 'ispending')->withTimestamps();
	}



	/*Relation to recoveries where payroll recovery member_type_id = 4*/
	public function payrollRecoveries()
	{
		return $this->hasMany(PayrollRecovery::class, 'resource_id', 'id')->where('member_type_id', 4);
	}

	public function payrollBankUpdates()
	{
		return $this->hasMany(PayrollBankInfoUpdate::class, 'resource_id', 'id')->where('member_type_id', 4);
	}

	public function payrollBeneficiaryUpdates()
	{
		return $this->hasMany(PayrollBeneficiaryUpdate::class, 'resource_id', 'id')->where('member_type_id', 4);
	}

	public function payrollVerifications()
	{
		return $this->hasMany(PayrollVerification::class, 'resource_id', 'id')->where('member_type_id', 4);
	}


	/*Payroll runs*/
	public function payrollRuns()
	{
		return $this->hasMany(PayrollRun::class, 'resource_id', 'id')->where('member_type_id', 4);

	}

	public function payrollSuspendedRuns()
	{
		return $this->hasMany(PayrollSuspendedRun::class, 'resource_id', 'id')->where('member_type_id', 4);

	}

	/*Relation to manual payroll member*/
	public function manualPayrollMember()
	{
		return $this->hasOne(ManualPayrollMember::class, 'resource_id','id')->where('member_type_id', 4);
	}

	public function region(){
		return $this->belongsTo(Region::class);
	}


	public function district(){
		return $this->belongsTo(District::class);
	}

    /*
        * Formatting tables column
        */
    public function getDobFormattedAttribute() {
    	return ($this->dob) ?  Carbon::parse($this->dob)->format('d-M-Y') : ' ';
    }


    public function getNameAttribute() {
    	return ucfirst(strtolower($this->firstname)) . " " . ucfirst(strtolower($this->middlename)) . " " . ucfirst(strtolower($this->lastname));
    }


    //    Age
    public function getAgeAttribute()
    {
    	$today =    Carbon::now();
    	return age($this->dob, $today);
    }

    /*address*/
    public function address()
    {
    	return $this->address;
    }

    /*Created at formatted short date*/
    public function getCreatedAtFormattedAttribute()
    {
    	return short_date_format($this->created_at);
    }

    /*Last response formatted short date*/
    public function getLastResponseFormattedAttribute()
    {

    	return ($this->lastresponse) ? short_date_format($this->lastresponse) : ' ';
    }

    public function getMpFormattedAttribute($employee_id)
    {
    	$mp = $this->getMpAttribute($employee_id);
    	return number_2_format($mp);
    }

    public function getMpAttribute($employee_id)
    {
    	$dependent_employee = $this->employees()->where('employee_id', $employee_id)->first();
    	return $dependent_employee->pivot->survivor_pension_amount;
    }

    /*get pivot - dependent employee*/
    public function getDependentEmployee($employee_id)
    {
    	$dependent_employee = $this->employees()->where('employee_id', $employee_id)->first()->pivot;
    	return $dependent_employee;
    }

    public function getProfileUrl($employee_id)
    {
    	$dependent_employee = $this->getDependentEmployee($employee_id);
    	return 'compliance/dependent/profile/' . $dependent_employee->id;
    }

    public function getProfileRoute($employee_id)
    {
    	$dependent_employee = $this->getDependentEmployee($employee_id);
    	$profile_route = 'backend.compliance.dependent.profile';
    	$parameter = $dependent_employee->id;
    	return  ['route' => $profile_route, 'parameter' => $parameter];
    }

    /*Get status of the pensioner*/
    public function getStatusLabelAttribute($employee_id)
    {
    	if($this->isDeactivated($employee_id)){
    		return   "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Deactivated'. "'>" . 'Deactivated'. "</span>";
    	}elseif($this->isSuspended()){
    		return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Suspended'. "'>" . 'Suspended'. "</span>";
    	}elseif($this->isActive($employee_id)){
    		return   "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active'. "'>" . 'Active'. "</span>";
    	}
    	elseif($this->isInactive($employee_id)){
    		return   "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Inactive'. "'>" . 'Inactive'. "</span>";
    	}else{
    		return   "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>";
    	}
    }

    /*Get MP First pay flag status of the pensioner*/
    public function getFirstPayStatusLabelAttribute($employee_id)
    {

    	if ($this->isFirstPaid($employee_id)){
    		$manual_status = ($this->firstpay_manual == 1) ? ' - Started from manual' : '';
    		return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid'. "'>" . 'Paid'. "</span>" . $manual_status;
    	}else{
    		return   "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>";
    	}
    }


    /*Get Gratuity pay flag status of the pensioner*/
    public function getGratuityPayStatusLabelAttribute($employee_id)
    {
    	if ($this->isGratuityPaid($employee_id)){
    		return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid'. "'>" . 'Paid'. "</span>";
    	}else{
    		return   "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>";
    	}
    }


    /*Get funeral grant pay flag status of the pensioner*/
    public function getFuneralGrantPayStatusLabelAttribute($employee_id)
    {
    	if ($this->isFuneralGrantPaid($employee_id)){
    		return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid'. "'>" . 'Paid'. "</span>";
    	}else{
    		return   "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>";
    	}
    }


    /*Dependent is suspended*/
    public function isSuspended()
    {

    	return $this->suspense_flag == 1;
    }
    /*Dependent is Active*/

    public function isActive($employee_id)
    {
    	$dependent_employee = $this->employees()->where('employee_id', $employee_id)->first();
    	return $dependent_employee->pivot->isactive == 1;
    }

    /*Dependent is Inactive - not activated yet*/
    public function isInactive($employee_id)
    {
    	$dependent_employee = $this->employees()->where('employee_id', $employee_id)->first();
    	return $dependent_employee->pivot->isactive == 0;
    }

    /*Dependent is deactivated*/
    public function isDeactivated($employee_id)
    {
    	$dependent_employee = $this->employees()->where('employee_id', $employee_id)->first();
    	return $dependent_employee->pivot->isactive == 2;
    }



    /*MP is paid at least once*/
    public function isFirstPaid($employee_id)
    {
    	$dependent_employee = $this->employees()->where('employee_id', $employee_id)->first();
    	return $dependent_employee->pivot->firstpay_flag == 1;
    }

    /*CHeck if has payroll on the system*/
    public function getFirstPayFlagSystemAttribute($employee_id)
    {
    	$check_if_has_system_payroll = (new PayrollRunRepository())->getMpForDataTable()->where('payroll_runs.member_type_id', 4)->where('payroll_runs.resource_id', $this->id)->where('payroll_runs.employee_id', $employee_id)->count();
    	$first_pay_flag_system = ($check_if_has_system_payroll == 0) ? 0 : 1;
    	return $first_pay_flag_system;
    }


    /*Gratuity is paid */
    public function isGratuityPaid($employee_id)
    {
    	$dependent_employee = $this->employees()->where('employee_id', $employee_id)->first();
    	return $dependent_employee->pivot->survivor_gratuity_pay_flag == 1;
    }

    /*Funeral grant is paid */
    public function isFuneralGrantPaid($employee_id)
    {
    	$dependent_employee = $this->employees()->where('employee_id', $employee_id)->first();
    	return $dependent_employee->pivot->funeral_grant_pay_flag == 1;
    }


    /**
     * @return int
     * Get no of days left for child to reach 18yrsold
     */
    public function getChildLimitAgeDaysLeftAttribute()
    {
    	$dob_parse =Carbon::parse($this->dob);
    	$today = Carbon::now();
        $limit_age = sysdefs()->data()->payroll_child_limit_age;//18
        $limit_date = $dob_parse->addYears($limit_age);
        $diff_days = (comparable_date_format($limit_date) > comparable_date_format($today)) ? $limit_date->diffInDays($today) : 0;
        return $diff_days;
    }



    /**
     * Get Start payroll payment date i.e. death date and date of mmi
     */
    public function getStartPayDateAttribute($employee_id)
    {
    	$payroll_runs = new PayrollRunRepository();
    	$dependent_employee = $this->getDependentEmployee($employee_id);
    	$start_pay_date =  (isset($dependent_employee->notification_report_id))  ?  $payroll_runs->getStartPayDate((new NotificationReportRepository())->find($dependent_employee->notification_report_id)) : $payroll_runs->getStartPayDateManualFile((new ManualNotificationReportRepository())->find($dependent_employee->manual_notification_report_id));
    	return $start_pay_date;
    }



    /*Check if has pending payroll recovery -- start---*/

    /*Get Active payroll arrears*/
    public function getActivePayrollArrearsCountAttribute()
    {
    	$payroll_arrears = new PayrollArrearRepository();
    	$count = $payroll_arrears->getActiveArrearsCount(4, $this->id);
    	return $count;
    }


    /*Get Active payroll deductions*/

    public function getActivePayrollDeductionsCountAttribute()
    {
    	$payroll_deductions = new PayrollDeductionRepository();
    	$count = $payroll_deductions->getActiveDeductionsCount(4, $this->id);
    	return $count;
    }



    /*Get Active payroll unclaims*/

    public function getActivePayrollUnclaimsCountAttribute()
    {
    	$payroll_unclaims = new PayrollUnclaimRepository();
    	$count = $payroll_unclaims->getActiveUnclaimsCount(4, $this->id);
    	return $count;
    }

    /*---end payroll recovery---*/


    /*Get status label for mp payment balance if overpaid / underpaid*/
    public function getMpPaymentStatusLabel($employee_id)
    {
    	$payroll_runs = new PayrollRunRepository();
    	$mp_balance = $payroll_runs->getMpBalanceForMember(4, $this->id, $employee_id);
    	if($mp_balance > 0)
    	{
    		/*overpaid*/
    		return  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'OVERPAID'. "'>" . 'OVERPAID'. "</span>";
    	}elseif($mp_balance < 0){
    		/*underpaid*/
    		return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'UNDERPAID'. "'>" . 'UNDERPAID'. "</span>";
    	}


    }


    /*E-OFFICE FILE LABELS*/

    public function getFilenameDmsAttribute()
    {
    	$filename = "";
    	$filename = $this->getNameAttribute() . '/'. $this->id;
    	return $filename;
    }

    public function getFileSubjectAttribute($employee_id)
    {
    	$dependent_repo = new DependentRepository();
    	$notification_report =  $dependent_repo->findNotificationReport($this->id, $employee_id);
    	$subject = "";
    	$subject =  'DEPENDENT - ' .  $this->getNameAttribute() . '/'. $this->id;
    	return $subject;
    }



    /**
     * @return string
     * Get dependent directory for documents
     */
    public function getDirectoryAttribute()
    {
    	$payroll_dir =  $this->real(payroll_dir() . DIRECTORY_SEPARATOR);
    	$dependent_dir = $payroll_dir . DIRECTORY_SEPARATOR . 'dependent' . DIRECTORY_SEPARATOR . $this->id;
    	return $dependent_dir;
    }

    public function getUrlAttribute()
    {
    	$payroll_url =  payroll_url();
    	$dependent_url = $payroll_url . DIRECTORY_SEPARATOR . 'dependent' . DIRECTORY_SEPARATOR . $this->id;
    	return $dependent_url;
    }


    /**
     * @param $document_type
     * Get pending document for validation in payroll approval action
     */
    public function getPendingDocumentForValidationPerType($document_type)
    {
    	$payroll_repo = new PayrollRepository();
    	$documents = $payroll_repo->getBeneficiaryDocumentsPendingValidation(4,$this->id, $document_type);
    	return $documents;
    }

    /**
     * @param $document_type
     * Get pending document for validation in payroll approval action
     */
    public function getDocumentUsedForValidationPerType($document_type)
    {
    	$payroll_repo = new PayrollRepository();
    	$documents = $payroll_repo->getBeneficiaryDocumentValidatedPerType(4,$this->id, $document_type);
    	return $documents;
    }

    /**
     * @return string
     * Get missing details alert
     */
    public function getMissingDetailsAlertAttribute(){
    	$dob = (!$this->dob) ? 'Dob not filled, ' : '';
    	$phone = (!$this->phone) ? 'Phone not filled, ' : '';
    	$bank = ((!$this->accountno) || (!$this->bank_id) || $this->bank_id == 5 || $this->bank_id == 6)  ? 'Bank details not filled' : '';
    	return $dob . $phone . $bank;
    }


    /**
     * @param $member
     * @param $member_type_id
     * @param $employee_id
     * @return string
     * Get Member no dependent for ErpApi
     */
    public function getMemberNoForErpApi($employee_id)
    {
    	$employee = $this->employees()->where('employee_id', $employee_id)->first();
    	$dependent_employee = $employee->pivot;
    	$dependent_type = $dependent_employee->dependent_type_id;
    	$memberno = $employee->memberno . '_' .$dependent_type . '_' .  $this->id;

    	return $memberno;
    }
    /*Member no for erp with no underscore delimiter*/
    public function getMemberNoForErpApiWithNoDelimiter($employee_id)
    {
    	$employee = $this->employees()->where('employee_id', $employee_id)->first();
    	$dependent_employee = $employee->pivot;
    	$dependent_type = $dependent_employee->dependent_type_id;
    	$memberno = $employee->memberno  .$dependent_type .  $this->id;

    	return $memberno;
    }


    public function getSuspendedDateFormattedAttribute()
    {
    	if($this->suspense_flag == 1){
    		$suspended_date = $this->suspended_date;
    		if(!isset($suspended_date)){
    			$child_suspension = PayrollChildSuspension::query()->where('dependent_id', $this->id)->first();
    			$suspended_date = (isset($suspended_date) ? $child_suspension->created_at : null);
    		}
    	}
    	return (isset($suspended_date) ? short_date_format($suspended_date) : '');
    }


    public function getRectifyMpButtonAttribute($employee_id)
    {
    	return '<a href="' . route('backend.compliance.dependent.rectify_mp', $this->id, $employee_id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Rectify MP' . '"></i></a> ';
    }



    /*Last verification date attribute*/
    public function getLastVerificationDateAttribute()
    {
    	$check= $this->payrollVerifications()->count();
    	if($check > 0)
    	{
    		return $this->lastresponse;
    	}else{
    		return null;
    	}
    }

    /*Check if age of dependent is eligible by dependent type*/
    public function getCheckIfAgeIsEligible($employee_id)
    {
    	$dependent_employee = $this->getDependentEmployee($employee_id);
    	$check = true;
    	switch($dependent_employee->dependent_type_id){
    		case 3:
    		/*child*/
    		$check = (new DependentRepository())->checkIfChildUnderAgeLimits($this->id);
    		break;
    	}
    	return $check;
    }

}