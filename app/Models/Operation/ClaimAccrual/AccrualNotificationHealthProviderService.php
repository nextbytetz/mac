<?php

namespace App\Models\Operation\ClaimAccrual;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AccrualNotificationHealthProviderService extends Model
{
	use Softdeletes;

	protected $guarded = [];
	public $timestamps = true;

    //Relation to the healthService checklist
	public function healthServiceChecklist(){
		return $this->belongsTo(\App\Models\Operation\Claim\HealthServiceChecklist::class);
	}

    //Relation to the notification_health provider
	public function accrualNotificationHealthProvider(){
		return $this->belongsTo(\App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProvider::class);
	}

    // Get from date formatted
	public function getFromDateFormattedAttribute()
	{
		$return = "-";
		if (!is_null($this->from_date)) {
			$return  = Carbon::parse($this->from_date)->format('d-M-Y');
		}
		return $return;
	}

// Get to date formatted
	public function getToDateFormattedAttribute()
	{
		$return = "-";
		if (!is_null($this->to_date)) {
			$return  = Carbon::parse($this->to_date)->format('d-M-Y');
		}
		return $return;
	}

	public function getAmountFormattedAttribute()
	{
		$amount = number_format( $this->amount , 2 , '.' , ',' );
		return $amount;
	}
}
