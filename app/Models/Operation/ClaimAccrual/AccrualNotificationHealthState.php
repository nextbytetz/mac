<?php

namespace App\Models\Operation\ClaimAccrual;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccrualNotificationHealthState extends Model
{
    use Softdeletes;

    protected $guarded = [];
    public $timestamps = true;

     //Relation to the health state checklist
    public function healthStateChecklist(){
        return $this->belongsTo(\App\Models\Operation\Claim\HealthStateChecklist::class);
    }

    //Relation to the medical expense
    public function medicalExpense(){
        return $this->belongsTo(\App\Models\Operation\Claim\MedicalExpense::class);
    }
}
