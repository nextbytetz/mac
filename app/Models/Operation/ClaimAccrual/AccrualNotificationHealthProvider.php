<?php

namespace App\Models\Operation\ClaimAccrual;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccrualNotificationHealthProvider extends Model
{
	use Softdeletes;

	protected $guarded = [];
	public $timestamps = true;


    //Relation to the notification health provider practitioner
	public function accrualNotificationHealthProviderPractitioner(){
		return $this->hasOne(\App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderPractitioner::class);
	}

    //Relation to the notification health provider services
	public function accrualNotificationHealthProviderServices(){
		return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderService::class);
	}

    //Relation to the Medical expens
	public function accrualMedicalExpense(){
		return $this->belongsTo(\App\Models\Operation\ClaimAccrual\AccrualMedicalExpense::class);
	}

    //Relation to the Notification Report
	public function notificationReport(){
		return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
	}

    //Relation to the health provider
	public function healthProvider(){
		return $this->belongsTo(\App\Models\Operation\Claim\healthProvider::class);
	}

	public function accrualHealthProviderWithTrashed(){
		return $this->belongsTo(\App\Models\Operation\Claim\HealthProvider::class, 'health_provider_id', 'id');
	}

	/**
     * @return string
     */
	public function getAttendDateFormattedAttribute()
	{
		$return = "-";
		if (!is_null($this->attend_date)) {
			$return = Carbon::parse($this->attend_date)->format('d-M-Y');
		}
		return $return;
	}

    /**
     * @return string
     */
    public function getDismissDateFormattedAttribute()
    {
    	$return = "-";
    	if (!is_null($this->dismiss_date)) {
    		$return = Carbon::parse($this->dismiss_date)->format('d-M-Y');
    	}
    	return $return;
    }
}
