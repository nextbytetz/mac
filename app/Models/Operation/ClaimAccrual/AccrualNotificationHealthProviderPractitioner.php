<?php

namespace App\Models\Operation\ClaimAccrual;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccrualNotificationHealthProviderPractitioner extends Model
{
    use Softdeletes;

    protected $guarded = [];
    public $timestamps = true;

    //Relation to the notification health provider practitioner
    public function medicalPractitioner(){
        return $this->belongsTo(\App\Models\Operation\Claim\MedicalPractitioner::class);
    }
}
