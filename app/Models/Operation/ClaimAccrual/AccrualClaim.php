<?php

namespace App\Models\Operation\ClaimAccrual;

use App\Models\Operation\Claim\Traits\Attribute\ClaimAttribute;
use App\Models\Operation\Claim\Traits\Relationship\ClaimRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class AccrualClaim extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    // protected $auditableEvents = [
    //     'deleted',
    //     'updated',
    //     'restored',
    // ];

    public function notificationReport()
    {
        return $this->belongsTo(NotificationReport::class);
    }

    public function getPdFormattedAttribute() {
        return  ($this->pd) ? $this->pd : 0;
    }

    public function getContribMonthFormattedAttribute() {
        return  Carbon::parse($this->contrib_month)->format('M-Y');
    }

}