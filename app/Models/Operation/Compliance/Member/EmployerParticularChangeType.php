<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerClosureAttribute;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerParticularChangeAttribute;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmployerParticularChangeType extends Model
{


//    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * RELATIONSHIP
     */


    public function particularChange()
    {
        return $this->belongsTo(EmployerParticularChange::class, 'employer_particular_change_id');
    }


    public function nameChangeType()
    {
        return $this->belongsTo(CodeValue::class, 'name_change_type_cv_id');
    }


}
