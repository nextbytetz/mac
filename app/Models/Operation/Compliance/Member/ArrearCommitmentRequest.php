<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Auth\User;
use App\Models\Workflow\WfTrack;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ArrearCommitmentRequest extends Model implements AuditableContract
{


    use Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';
    protected $table = "commitment_requests";

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    public function employer()
    {
        return $this->belongsTo(App\Models\Operation\Compliance\Member\Employer::class);
    }


    public function getResourceNameAttribute()
    {
        $requested_commintment = DB::table('portal.commitment_requests')->where('id', $this->id)
        ->join('portal.commitment_options','commitment_options.id','=','commitment_requests.commitment_option_id')
        ->first();
        return count($requested_commintment) ? ucwords(strtolower($requested_commintment->name)) : '';
    }   


}
