<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
use DB;
trait OnlineEmployerVerificationAttribute
{
    public function getDocFormattedAttribute()
    {
        return Carbon::parse($this->doc)->format("d-M-Y");
    }


    public function getPayrollDocFormattedAttribute()
    {
        // return Carbon::parse($this->doc)->format("d-M-Y");
        switch ($this->commencement_source) {
            case 2:
            //branch or project
            return Carbon::parse($this->doc)->format("d-M-Y");
            break;

            case 3:
            //Payroll
            $start_date = !empty($this->project_start_date) ? Carbon::parse($this->project_start_date)->format("d-M-Y") : ' ';
            // $end_date = !empty($this->project_end_date) ? Carbon::parse($this->project_end_date)->format("d-M-Y") : ' ';
            return $start_date;
            break;
            
            default:
            return Carbon::parse($this->doc)->format("d-M-Y");
            break;
        }
    }

    public function getCreatedAtFormattedAttribute()
    {
        return Carbon::parse($this->created_at)->format("d-M-Y g:i:s A");
    }

    public function getStatusFormattedAttribute()
    {
        $return = "";
        switch ($this->status) {
            case 0:
                //Pending for verification
            $return = "<span class='tag tag-info white_color' style='font-size: 13px;'>" . trans('labels.general.pending_approval') . "</span>";
            break;
            case 1:
                //Approved
            $return = "<span class='tag tag-success white_color' style='font-size: 13px;'>" . trans('labels.general.approved') . "</span>";
            break;
            case 2:
                //Rejected
            $return = "<span class='tag tag-danger white_color' style='font-size: 13px;'>" . trans('labels.general.rejected') . "</span>";
            break;
        }
        return $return;
    }

    public function getUserFormattedAttribute()
    {
        return $this->user->name . " (" . $this->user->codeValuePortal->name . "), " . $this->getOwnStatusAttribute();
    }

    public function getOwnStatusAttribute()
    {
        $return = "";
        switch ($this->isown) {
            case 0:
            $return = trans("labels.backend.member.employer.facilitator");
            break;
            case 1:
            $return = trans("labels.backend.member.employer.owner");
            break;
        }
        return $return;
    }

    public function getTinExistingLabelAttribute()
    {
        if ($this->has_tin_existing) {
            //if verified
            return "<span class='tag tag-success white_color'>" . trans('labels.general.existing') . "</span>";
        } else {
            return "<span class='tag tag-warning white_color'>" . trans('labels.general.no_existing') . "</span>";

        }
    }

    public function getResourceNameAttribute()
    {
        return $this->employer->name;
    }

    public function getPayrollDescriptionFormattedAttribute()
    {
        if(!empty($this->employer_user_id)){
            $employer_user = DB::table('portal.employer_user')->where('id',$this->employer_user_id)->first();
        }else{
            $employer_user = DB::table('portal.employer_user')->where('user_id',$this->user_id)
            ->where('employer_id',$this->employer_id)->where('payroll_id',$this->payroll_id)->first();
        }
        return !empty($employer_user->payroll_description) ? $employer_user->payroll_description : ($this->payroll_id == 1 ? 'Main Payroll': ' ');
    }

    public function getPayrollTypeAttribute()
    {
        switch ($this->commencement_source) {
            case 1:
            case 2:
            $return = 'Payroll or Branch';
            break;
            case 3:
            $return = 'Project';
            break;
            default:
            $return = '';
            break;
        }
        return $return;
    }

}