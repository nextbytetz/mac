<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class EmploymentHistoryAttribute
 */
trait EmploymentHistoryAttribute{

    /*
        * Formatting tables column
        */
    public function getFromDateFormattedAttribute() {
        return  Carbon::parse($this->from_date)->format('d-M-Y');
    }

    public function getToDateFormattedAttribute() {
        return  Carbon::parse($this->to_date)->format('d-M-Y');
    }


    //          ACTION COLUMNS ADDED TO DATATABLE
    public function getActionButtonsAttribute() {
        return $this->getDeleteButtonAttribute();
    }

    public function getDeleteButtonAttribute() {
        return '<a class="btn btn-xs btn-primary delete_button" ><i class="icon fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
    }



}