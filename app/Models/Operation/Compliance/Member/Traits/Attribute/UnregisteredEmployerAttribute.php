<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class EmployeeAttribute
 */
trait UnregisteredEmployerAttribute{

    /*
     * Formatting tables column
     */
    public function getDateAssignedFormattedAttribute() {
        return  Carbon::parse($this->date_assigned)->format('d-M-Y');

    }
    public function getDateOfCommencementFormattedAttribute() {
        return  Carbon::parse($this->doc)->format('d-M-Y');
    }


    /* Name Upper Case */
    public function getNameFormattedAttribute() {
        return strtoupper($this->name);
    }



    //Get registration status label
    public function getRegisterStatusLabelAttribute()
    {
        if ($this->isRegistered())
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.member.registered') . "'>" . trans('labels.backend.member.registered') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.member.not_registered') . "'>" . trans('labels.backend.member.not_registered') . "</span>";
    }





    //Get Assigned staff status label
    public function getAssignStatusLabelAttribute()
    {
        if ($this->isAssigned())
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.member.assigned') . "'>" . trans('labels.backend.member.assigned') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.member.not_assigned') . "'>" . trans('labels.backend.member.not_assigned') . "</span>";
    }


    public function getUnRegisteredLabelAttribute()
    {
        return "<span class='tag tag-warning white_color'>" . trans('labels.general.unregistered') . "</span>";
    }


    public function isRegistered(){
        return $this->is_registered == 1;
    }


    public function isAssigned(){
        return $this->assign_to > 0;
    }

}