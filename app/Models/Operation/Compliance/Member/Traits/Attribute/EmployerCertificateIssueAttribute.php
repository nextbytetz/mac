<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class EmployerSizeTypeAttribute
 */
trait EmployerCertificateIssueAttribute{


    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

    //Get Issue status label
    public function getIssueStatusLabelAttribute()
    {
        if ($this->isReissue())
            return "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.reissue') . "'>" . trans('labels.general.reissue') . "</span>";
        return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.issue') . "'>" . trans('labels.general.issue') . "</span>";
    }




    public function isReissue(){
        return $this->is_reissue == 1;
    }

}