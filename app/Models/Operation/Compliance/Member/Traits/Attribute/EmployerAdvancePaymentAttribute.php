<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;

trait EmployerAdvancePaymentAttribute
{

    public function getResourceNameAttribute()
    {
        return $this->employer->name;
    }

    public function getCreatedAtFormattedAttribute()
    {
        return Carbon::parse($this->created_at)->format("d-M-Y g:i:s A");
    }

    public function getStartMonthFormattedAttribute()
    {
        return Carbon::parse($this->start_month)->format("F Y");
    }

    public function getStatusFormattedAttribute()
    {
        $return = "";
        switch ($this->status) {
            case 0:
                //Pending for verification
                $return = "<span class='tag tag-info white_color' style='font-size: 13px;'>" . trans('labels.general.pending_approval') . "</span>";
                break;
            case 1:
                //Approved
                $return = "<span class='tag tag-success white_color' style='font-size: 13px;'>" . trans('labels.general.approved') . "</span>";
                break;
            case 2:
                //Rejected
                $return = "<span class='tag tag-danger white_color' style='font-size: 13px;'>" . trans('labels.general.rejected') . "</span>";
                break;
        }
        return $return;
    }


    public function getOwnStatusAttribute()
    {
        $return = "";
        switch ($this->isown) {
            case 0:
                $return = trans("labels.backend.member.employer.facilitator");
                break;
            case 1:
                $return = trans("labels.backend.member.employer.owner");
                break;
        }
        return $return;
    }

    public function getUserFormattedAttribute()
    {
        return $this->user->name . " (" . $this->user->codeValuePortal->name . "), " . $this->getOwnStatusAttribute();
    }

}