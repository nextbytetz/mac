<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class EmployerClosureAttribute
 */
trait EmployerClosureReopenAttribute{


    public function getOpenDateFormattedAttribute() {
        return  isset($this->open_date) ? short_date_format($this->open_date) : ' ';
    }


    public function getApprovedDateFormattedAttribute() {
        return   isset($this->wf_done_date) ? short_date_format($this->wf_done_date) : ' ';
    }


    /*get Resource name for wf*/
    public function getResourceNameAttribute()
    {
        return   ($this->employer_closure_id) ? $this->employerClosure->closedEmployer->name : $this->employer->name;
    }


    //Get status label
    public function getStatusLabelAttribute()
    {

            if ($this->status == 0)
                return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending' . "</span>";
            elseif($this->status == 1 && $this->wf_done == 0){
                return "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='" . 'Initiated'. "'>" . 'Initiated' . "</span>";
            }elseif($this->status == 1 && $this->wf_done == 1){
                return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
            }elseif($this->status == 1 && $this->wf_done == 2){
                return "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . 'Declined' . "'>" . 'Declined' . "</span>";
            }



    }




}