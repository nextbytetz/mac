<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
use DB;
/**
 * Class EmployeeAttribute
 */
trait EmployeeAttribute{


    /*
        * Formatting tables column
        */
    public function getDobFormattedAttribute() {
        $return = "-";
        if (!is_null($this->dob)) {
            $return = Carbon::parse($this->dob)->format('d-M-Y');;
        }
        return  $return;
    }

    public function getNameAttribute()
    {
        $return = "";
        if (is_null($this->id)) {
            //this is the default attribute which is defined when a relationship to this employee is not found.
            $return = $this->attributes['name'];
        } else {
            $return = ucfirst($this->firstname) . " " . ucfirst($this->middlename) . " " . ucfirst($this->lastname);
        }
        return $return;
    }

    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }


    /*address*/
    public function address()
    {
        $region =  ($this->employers()->first()->region()->count()) ? $this->employers()->first()->region->name : ' ';
        return $region;
    }


    public function getIsRemovedAttribute()
    {
        $removed = DB::table('main.employee_employer')->where('employee_id',$this->id)->whereNotNull('deleted_at')->count();
        return $removed > 0 ? true : false;
    }

}