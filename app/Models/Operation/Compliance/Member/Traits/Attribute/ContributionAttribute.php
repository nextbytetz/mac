<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class ContributionTrackAttribute
 */
trait ContributionAttribute
{




    public function getGrosspayFormattedAttribute() {
               return number_format( $this->grosspay , 2 , '.' , ',' );
    }

    public function getBasicpayFormattedAttribute() {
        return number_format( $this->salary , 2 , '.' , ',' );
    }


    public function getMemberAmountFormattedAttribute()
    {
        return number_format( $this->employee_amount , 2 , '.' , ',' );
    }

    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }


    public function getContribMonthFormattedAttribute() {
        return  Carbon::parse($this->contrib_month)->format('M-Y');
    }



}