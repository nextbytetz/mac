<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionInterestRateRepository;

/**
 * Class EmployerAttribute
 */
trait EmployerAttribute{

    /*
     * Formatting tables column
     */
    public function getDateOfCommencementFormattedAttribute() {
        return  Carbon::parse($this->doc)->format('d-M-Y');
    }

    /*Date registered*/
    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }


    /* Get Location */
    public function getLocationSurveyedAttribute() {
        return  trans('labels.general.plot_no') . ': ' . $this->plot_no . ', ' . trans('labels.general.block_no') . ': ' . $this->block_no  . ', ' . $this->street . ', ' . $this->road;
    }


    /* Get address */
    public function getFullAddressAttribute() {
        return  trans('labels.general.box_no') . ' ' . $this->box_no . ', '  . ($this->region_id) ? $this->region->name : ''  . ', ' .  ($this->region_id) ? $this->region->country->name : ''  ;
    }

    /**
     * @return string
     */
    public function getLocationFormattedAttribute()
    {
        $location = "";
        if ($this->district_id) {
            $district = $this->district->name;
            $region = $this->district->region->name;
            $location = $district . " - " . $region;
        } else {
            if ($this->region_id) {
                $location = $this->region->name;
            }
        }
        return $location;
    }

    public function getLocationUnsurveyedAttribute() {
        return $this->unsurveyed_area;
    }
    /* Annual Earning */
    public function getAnnualEarningFormattedAttribute() {
        return number_format( $this->annual_earning , 2 , '.' , ',' );
    }

    /*Reg No. Formatted*/
    public function getIdFormattedAttribute() {
        return str_pad($this->id, 6, "0", STR_PAD_LEFT);
    }

    /* Name Upper Case */
    public function getNameFormattedAttribute() {
        return strtoupper($this->name);
    }
    /* Name Upper Case with reg no */
    public function getNameWithRegNoAttribute() {
        return strtoupper($this->name) . ' - ' . $this->reg_no;
    }

    /*address*/
    public function address()
    {
        return ($this->region_id) ? $this->region->name : ' ';
    }

    //  ACTION COLUMNS ADDED TO DATA TABLE
    public function getActionButtonsAttribute() {
        return $this->getEditButtonAttribute();
    }

    public function getEditButtonAttribute() {
        return '<a href="' . route('backend.compliance.member.employer.profile', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    // Get status label
    public function getStatusLabelAttribute()
    {
        if ($this->status() == 2) {
            //if pending
            return "<span class='tag tag-warning warning_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.pending') . "'>" . '.' . "</span>";

        } elseif ($this->status() == 1) {
            //If approved
            return "<span class='tag tag-success success_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.approved') . "'>" . '.' . "</span>";
        } elseif ($this->status() == 3)  {
            //if rejected
            return "<span class='tag tag-danger danger_color_custom'  data-toggle='tooltip' data-html='true' title='" . trans('labels.general.rejected') . "'>" . '.' . "</span>";
        }elseif ($this->status() == 4)  {
            //if initiated
            return "<span class='tag tag-info '  data-toggle='tooltip' data-html='true' title='" . trans('labels.general.initiated') . "'>" . '.' . "</span>";
        }elseif ($this->status() == 5)  {
            //if pending online
            return "<span class='tag tag-primary '  data-toggle='tooltip' data-html='true' title='" . trans('labels.general.pending_online') . "'>" . '.' . "</span>";
        }
    }

    // Get TIn Verification label
    public function getTinVerificationLabelAttribute()
    {
        if ($this->isTinVerified() == 1) {
            //if verified
            return "<span class='tag tag-success white_color'>" . trans('labels.general.verified') . "</span>";
        } else {
            return "<span class='tag tag-warning white_color'>" . trans('labels.general.unverified') . "</span>";
        }
    }

    public function getRegisteredLabelAttribute()
    {
        return "<span class='tag tag-success white_color'>" . trans('labels.general.registered') . "</span>";
    }


    public function getTreasuryLabelAttribute()
    {
        if($this->is_treasury == true){
            return "<span class='tag tag-info white_color'>" . 'Pay through Treasury' . "</span>";
        }else{
            return '';
        }

    }

//    Check status of employer registration  if Pending, approval, rejected
    public function status(){
        return $this->approval_id;
    }

    public function isTinVerified(){
        return $this->has_tin_verified;
    }

    public function isTinExisting(){
        return $this->has_tin_existing;
    }

    public function getResourceNameAttribute()
    {
        return $this->name;
    }

    public function employeeFile()
    {
        $document = new DocumentRepository();
        $employee_list_document_id = $document->getEmployeeFileDocument();
        $uploadedDocument = $this->documents()->where("document_id", $employee_list_document_id)->first();

        if(isset($uploadedDocument)){
            return employer_registration_dir() . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR .         $employee_list_document_id  . '.' .  $uploadedDocument->pivot->ext;
        }else{
            return null;
        }

    }

    public function uploadEmployeeFilePercent()
    {
        $document = new DocumentRepository();
        $employee_list_document_id = $document->getEmployeeFileDocument();
        $uploadedDocument = $this->documents()->where("document_id", $employee_list_document_id)->first()->pivot;
        $row_imported = (($uploadedDocument->rows_imported)) ?: 0;
        $total_rows = (($uploadedDocument->total_rows)) ?: 1;
        $percent = ($row_imported/$total_rows) * 100;
        return round($percent) . "%";
    }

    /**
     * @return string
     * Anaylyse employee file upload
     */
    public function analysisStatus()
    {
        $errors = $this->employeeTemps()->where('error', 1)->first();
        $error = ($errors) ? 1 : 0;
        $employeeTemps = $this->employeeTemps()->select(['id'])->count();
        if ($this->uploadEmployeeFilePercent() == '100%' And $error == 0 And ($employeeTemps))
            return "<label class='tag tag-success'><i class=\"icon fa fa-check\" aria-hidden=\"true\"></i>&nbsp;&nbsp; <i></i></label>";
        return "<label class='tag tag-default'><i class=\"icon fa fa-remove\" aria-hidden=\"true\"></i>&nbsp;&nbsp; <i class='text-white' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.incomplete_analysis_helper') . "'>" . trans('labels.backend.finance.receipt.incomplete_analysis') . "</i></label>";
    }

    public function getLinkedFileStatusLabelAttribute() {

        return  "</span>&nbsp;&nbsp;, ". trans("labels.general.uploaded") . "&nbsp;&nbsp;<i id='employer_" . $this->id . "' class='upload_percent'>" . $this->uploadEmployeeFilePercent() . "</i>";

    }

    public function allowToLoadEmployeeLive()
    {
        $errors = $this->employeeTemps()->where('error', 1)->first();
        $error = ($errors) ? 1 : 0;
        $employeeTemps = $this->employeeTemps()->select(['id'])->count();
        if ($error == 0 and $this->uploadEmployeeFilePercent() == '100%' And ($employeeTemps)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getUserFormattedAttribute()
    {
        $return = "";
        $user = $this->onlineUser()->first();
        $isown = $user->pivot->isown;
        $return =  $user->name . " (" . $user->codeValuePortal->name . "), ";
        switch ($isown) {
            case 0:
            $return .= trans("labels.backend.member.employer.facilitator");
            break;
            case 1:
            $return .= trans("labels.backend.member.employer.owner");
            break;
        }
        return $return;
    }


    public function getSourceFormattedAttribute()
    {
        $return = "";
        switch ($this->source) {
            case 1:
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.branch') . "</span>";
            break;
            case 2:
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.online') . "</span>";
            break;
        }
        return $return;
    }

    public function getEmployerStatusFormattedAttribute()
    {
        $return = "";
        switch ($this->employer_status) {
            case 1:
            $return = "<span class='tag tag-success white_color'>Active</span>";
            break;
            case 2:
            $return = "<span class='tag tag-warning white_color'>Dormant</span>";
            break;
            case 3:
            $closure = $this->getRecentEmployerClosure();
            $return = "<span class='tag tag-danger white_color'>Closed Business</span>" . ((isset($closure)) ?  ' - ' .  $closure->close_type_name : '');
            break;
            case 4:
            $closure = $this->getRecentEmployerClosure();
            $return = "<span class='tag tag-warning white_color'>Dormant</span>" . ((isset($closure)) ?  ' - ' .  $closure->close_type_name : '');
            break;
        }
        return $return;
    }


    /*closure type*/
    public function getRecentEmployerClosure()
    {
        $closure = $this->closures()->where('employer_closures.id', $this->employer_closure_id)->first();
        return $closure;
    }


    public function employeesCount()
    {
        return $this->employees()->count();
    }

    public function getRegionLabelAttribute() {
        $region = "";
        if (!is_null($this->region_id)) {
            $region = $this->region->name;
        } elseif (!is_null($this->district_id)) {
            $region = $this->district->region->name;
        }
        return $region;
    }


    /*Get relation officer from Debt collection module*/
    public function getComplianceRelationOfficerAttribute()
    {
        $officer = $this->complianceOfficers()->where('main.staff_employer.isactive', 1)->first();
        return $officer;
    }

    /*Get All contact person from PORTAL*/
    public function getAllPortalContactPersonAttribute()
    {
        $portal_users = (new EmployerRepository())->getPortalContactPerson($this->id);
        return $portal_users;
    }


    public function letterSalutation($lang = "en")
    {
        $salutation = $this->getSalutations()[0][$lang];
        if ($this->is_treasury == "t") {
            $salutation = $this->getSalutations()[1][$lang];
        }

        return $salutation;
    }

    public function getSalutations($lang = "en")
    {
        return [
            0 => [
                'en' => 'Managing Director',
                'sw' => 'Mkurugenzi Mtendaji',
            ],
            1 => [
                'en' => 'Director General',
                'sw' => 'Mkurugenzi Mtendaji (W)',
            ],
        ];
    }


//Last contribution
    public function getLastContributionAttribute(){
        $last_contrib = DB::table('employer_contributions')->where('employer_id', $this->id)->first();
        $last_contrib_month = null;
        $last_contrib_amount = 0;

        if(isset($last_contrib)){
            $last_contrib_month = $last_contrib->contrib_month;
            $last_contrib_amount = DB::table('employer_contributions')->where('employer_id', $this->id)->whereMonth('contrib_month','=', Carbon::parse($last_contrib_month)->format('n'))->whereYear('contrib_month','=', Carbon::parse($last_contrib_month)->format('Y'))->sum('contrib_amount');
        }

        return ['contrib_month' => $last_contrib_month, 'contrib_amount' => $last_contrib_amount];

    }

    public function contributionInterestRate($month,$fin_code_id)
    {
        $query = (new ContributionInterestRateRepository())->query()->where('start_date','<=',$month)
        ->whereNull('end_date')
        ->where('employer_category_cv_id',$this->employer_category_cv_id)
        ->where('fin_code_id',$fin_code_id)
        ->where('wf_status',2)
        ->whereNotNull('wf_done_date')
        ->first();

        if (!empty($query)){
           return (float)$query->rate / 100;
       }
       else{
         $second = (new ContributionInterestRateRepository())->query()->where('start_date','<=',$month)
         ->where('end_date','>=',$month)
         ->where('employer_category_cv_id',$this->employer_category_cv_id)
         ->where('fin_code_id',$fin_code_id)
         ->where('wf_status',2)
         ->whereNotNull('wf_done_date')
         ->first();
         return !empty($second) ? (float)$second->rate / 100 : 0;}
     }
 }