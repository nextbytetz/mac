<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureExtensionRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class EmployerClosureAttribute
 */
trait EmployerClosureExtensionAttribute{



    /*Get Resource name*/
    public function getResourceNameAttribute()
    {
           return $this->employerClosure->closedEmployer->name;
    }

    /*Get Letter reference title attribute*/
    public function getLetterReferenceAttribute($lang = 'en')
    {
        $letter_ref = ($lang == 'en') ? 'TEMPORARY DE-REGISTRATION EXTENSION' : 'MAOMBI YA KUONGEZA KUSITISHA USAJILI KWA MUDA';
        return  $letter_ref;
    }
    /*Get Response Letter reference title attribute*/
    public function getLetterReferenceNoAttribute($letter_cv_reference)
    {
        return (new EmployerClosureExtensionRepository())->getLetterReferenceNo($letter_cv_reference);
    }


    /*get Folio no*/
    public function getLetterFolioNoAttribute($letter_cv_reference)
    {
        return (new EmployerClosureExtensionRepository())->getLetterFolioNo($letter_cv_reference);
    }



    /*Status to check if letter is initiated*/
    public function getStatusIfLetterInitiatedAttribute()
    {
        $check_if_need_response = (new EmployerClosureExtensionRepository())->checkIfNeedResponseLetter($this);
        $check = $this->closureExtensionLetter()->where('isinitiated', '<>', 0)->count();
        if ($check > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*CHeck if need response letter*/
    public function getCheckIfNeedResponseLetterAttribute()
    {
        $check_if_need_response = (new EmployerClosureExtensionRepository())->checkIfNeedResponseLetter($this);
        return $check_if_need_response;
    }



}