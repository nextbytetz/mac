<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class ContributionTrackAttribute
 */
trait ContributionTrackAttribute
{



    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }


    public function getStatusLabelAttribute()
    {
        if ($this->iscompleted()) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.complete') . "</span>";
        }
        return "<span class='tag tag-info white_color'>" . trans('labels.general.to_start') . "</span>";
    }


    public function iscompleted()
    {
        return $this->status == 1;
    }

    public function getEditButtonAttribute()
    {
//
        if ($this->iscompleted()) {
            return '';
        }
        return '<a href="' . route('backend.compliance.contribution_track.edit',
                $this->id) . '" class="btn btn-xs site-btn save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }


    public function getActionButtonsAttribute()
    {
        return
            $this->getEditButtonAttribute();

    }

}