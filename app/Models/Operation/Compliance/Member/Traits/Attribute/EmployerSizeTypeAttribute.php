<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class EmployerSizeTypeAttribute
 */
trait EmployerSizeTypeAttribute{

    /*
        * Formatting tables column
        */
    public function getEmployerCountAttribute() {


//        {{--if MIn and Max Null--}}
        if((!$this->min_range) And (!$this->min_range)){
            $data =   DB::select('select  a.id from employers a  left join employee_counts b on a.id = b.employer_id where b.employer_id is null and 
approval_id = 1 group by(a.id) ', []);

            return count($data);
        }

//                            {{--when min is null--}}
        elseif (!$this->min_range){

            $data =   DB::select('select  a.employer_id from employee_counts  a join employers c on a.employer_id = c.id where (select sum(b.count) from employee_counts b where a.employer_id = b.employer_id) <= :max_range and approval_id = 1 group by(a.employer_id)', ['max_range' => $this->max_range]);
            return count($data);

        }


//                            {{--when max is null--}}
        elseif (!$this->max_range){
            $data =   DB::select('select  a.employer_id from employee_counts a join employers c on a.employer_id = c.id where (select sum(b.count) from employee_counts b where a.employer_id = b.employer_id) >= :min_range and c.approval_id = 1 group by(a.employer_id) ', ['min_range' => $this->min_range]);
            return count($data);
        }


//                            {{--when min and max have values--}}
        elseif ($this->min_range && $this->max_range ){
            $data =   DB::select('select  a.employer_id from employee_counts a  join employers c on a.employer_id = c.id   where (select sum(b.count) from employee_counts b  where a.employer_id = b.employer_id) >= :min_range  and (select sum(b.count) from employee_counts b where a.employer_id = b.employer_id) <= :max_range and approval_id = 1 group by(a.employer_id)', ['min_range' => $this->min_range, 'max_range' => $this->max_range]);

            return count($data);
        }







    }



}