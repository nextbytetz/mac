<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class EmployeeAttribute
 */
trait UnregisteredEmployerFollowUpAttribute{

    /*
     * Formatting tables column
     */
    public function getDateOfFollowUpFormattedAttribute() {
        return  Carbon::parse($this->date_of_follow_up)->format('d-M-Y');
    }




}