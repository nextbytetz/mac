<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class EmployerClosureAttribute
 */
trait EmployerClosureAttribute
{


    public function getOpenDateFormattedAttribute()
    {
        return isset($this->open_date) ? short_date_format($this->open_date) : ' ';
    }

    public function getCloseDateFormattedAttribute()
    {
        return isset($this->close_date) ? short_date_format($this->close_date) : ' ';
    }


    public function getApprovedDateFormattedAttribute()
    {
        return isset($this->wf_done_date) ? short_date_format($this->wf_done_date) : ' ';
    }

    //Get status label
    public function getStatusLabelAttribute()
    {
        if ($this->is_legacy == 0) {
            if ($this->wf_status == 0)
                return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
            elseif ($this->wf_status == 1 && $this->wf_done == 0) {
                return "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='" . 'Initiated' . "'>" . 'Initiated' . "</span>";
            } elseif ($this->wf_status == 1 && $this->wf_done == 1) {
                return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
            } elseif ($this->wf_status == 1 && $this->wf_done == 2) {
                return "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . 'Declined' . "'>" . 'Declined' . "</span>";
            }

        } else {
            /*legacy*/
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
        }


    }

    public function getCloseTypeNameAttribute()
    {
        if ($this->closure_type == 1) {
            /*Temporary*/
            return 'Temporary De-Registration';
        } elseif ($this->closure_type == 2) {
            /*Permanent*/
            return 'Permanent De-Registration';
        }
    }


    /*get Resource name for wf*/
    public function getResourceNameAttribute()
    {
        return $this->closedEmployer->name;
    }


    /*Get closure doc path file url*/
    public function getClosureDocPathFileUrl($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $file_path = employer_closure_path() . DIRECTORY_SEPARATOR . $uploaded_doc->employer_id . DIRECTORY_SEPARATOR . $uploaded_doc->external_id . DIRECTORY_SEPARATOR . $doc_pivot_id . '.' . $uploaded_doc->ext; //closure.
        return $file_path;

    }


    /*Get wcp1 document attached by year*/
    public function getWcp1DocAttachedByYear($year)
    {
        $employer = $this->closedEmployer;
        $wcp_doc_id = 72;
        $docs_attached = $employer->documents()->where('document_employer.external_id', $this->id)->where('document_id', $wcp_doc_id)->whereYear('date_reference', $year)->orderBy('date_reference')->get();
        return $docs_attached;
    }


    /*Get wcp1 document attached by month*/
    public function getWcp1DocAttachedByMonth($month)
    {
        $employer = $this->closedEmployer;
        $wcp_doc_id = 72;
        $docs_attached = $employer->documents()->where('document_employer.external_id', $this->id)->where('document_id', $wcp_doc_id)->whereRaw("to_char(date_reference, 'YYYY-MM')  = ? ", [$month])->orderBy('date_reference')->get();
        return $docs_attached;
    }


    /*Get status followup for Temporary closure*/
    public function getStatusFollowUpEligibilityAttribute()
    {
        $return = false;
        $closure_type = $this->closure_type;
        if ($this->wf_done == 1 && $closure_type == 1 && $this->open()->count() == 0) {
            $ref_date = isset($this->followup_ref_date) ? $this->followup_ref_date : $this->close_date_internal;
            $period_months = (new EmployerClosureRepository())->getTemporaryClosurePeriodMonths();
            $today = Carbon::parse(getTodayDate());
            $limit_date = $today->subMonthsNoOverflow($period_months);
            $limit_date = standard_date_format($limit_date);
            if (comparable_date_format($limit_date) > comparable_date_format($ref_date)) {
                $return = true;
            }
        }
//dd($ref_date);
        return $return;

    }

    /*Temporary type reference*/
    public function getSubTypeReferenceCvAttribute()
    {
        if (isset($this->sub_type_cv_id)) {
            return $this->subType->reference;
        } else {
            return null;
        }
    }

    /**
     * LETTER GENERATION
     */


    /*Status to check if letter is initiated*/
    public function getStatusIfLetterInitiatedAttribute()
    {
        $check_if_need_response = (new EmployerClosureRepository())->checkIfNeedResponseLetter($this);
        $check = $this->closureLetter()->where('isinitiated', '<>', 0)->count();
        if ($check > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*CHeck if need response letter*/
    public function getCheckIfNeedResponseLetterAttribute()
    {
        $check_if_need_response = (new EmployerClosureRepository())->checkIfNeedResponseLetter($this);
        return $check_if_need_response;
    }

    /*Get Response Letter reference title attribute*/
    public function getLetterReferenceNoAttribute($letter_cv_reference)
    {
        $closure_type_id = $this->closure_type;
        return (new EmployerClosureRepository())->getLetterReferenceNo($letter_cv_reference, $closure_type_id);
    }


    /*get Folio no*/
    public function getLetterFolioNoAttribute($letter_cv_reference)
    {
        $closure_type_id = $this->closure_type;
        return (new EmployerClosureRepository())->getLetterFolioNo($letter_cv_reference, $closure_type_id);
    }


    /*Get Letter acknowledge closure type*/
    public function getLetterClosureTypeAcknlgAttribute()
    {
        $closure_type = $this->getCloseTypeNameAttribute();
        $letter_acknlg = strtolower($closure_type);
        return $letter_acknlg;
    }

    /*Get Letter reference title by letter reference attribute*/
    public function getLetterReferenceByLetterReferenceAttribute($letter_cv_reference)
    {
        $closure_type = $this->getCloseTypeNameAttribute();
        switch ($letter_cv_reference) {
            case 'CLIEMPCLOSURE':
                $letter_ref = 'APPROVAL OF ' . strtoupper($closure_type) . ' OF BUSINESS';
                break;
            case 'CLIEMPCLOSSETT':
                $letter_ref = 'SETTLEMENT OF DEBT BEFORE ' . strtoupper($closure_type) . ' OF BUSINESS';
                break;
        }

        return $letter_ref;
    }


    /*Get Letter reference title attribute*/
    public function getLetterReferenceAttribute($lang = 'en')
    {
        $closure_type = $this->getCloseTypeNameAttribute();
        $closure_type_id = $this->closure_type;
        switch ($closure_type_id) {
            case 1:
                /*Temporary*/
                $letter_ref = ($lang == 'en') ? 'TEMPORARY DE-REGISTRATION' : 'MAOMBI YA KUONDOLEWA KWENYE ORODHA YA WAAJIRI KWA MUDA MFUPI';
                if (isset($this->sub_type_cv_id)) {
                    if ($this->subType->reference == 'ETCLPERMSETTL') {
                        //TODO swahili for settlement <sw>
                        $letter_ref = ($lang == 'en') ? 'SETTLEMENT OF DEBT BEFORE PERMANENT DE-REGISTRATION' : 'KULIPA DENI NA KUWASILISHA NYARAKA KABLA YA KUFUTA USAJILI';
                    }
                }

                break;
            case 2:
                /*Permanent*/
                //TODO swahili for PERMANENT <sw>
                $letter_ref = ($lang == 'en') ? 'PERMANENT DE-REGISTRATION' : 'MAOMBI YA KUFUTA USAJILI';
                break;
        }

        return $letter_ref;
    }

    /*Check if Eligible For Extensions*/
    public function checkIfEligibleForExtension($employer_id)
    {
        $last_closure = (new EmployerClosureRepository())->query()->where('employer_id', $employer_id)->where('wf_done', 1)->orderBy('close_date', 'desc')->first();
        $pending_extensions = $this->closureExtensions()->where('employer_closure_extensions.wf_done', 0)->count();
        if (($last_closure->id == $this->id) && $pending_extensions == 0 && $this->closure_type == 1  && $this->open()->count() == 0) {
            return true;
        }else{
            return false;
        }
    }


    /**
     * @return string
     * Get Status followup outcome
     */
    public function getStatusFollowupOutcomeAttribute()
    {
        return (new EmployerClosureRepository())->getStatusFollowupOutcome($this->followup_outcome);
    }

}