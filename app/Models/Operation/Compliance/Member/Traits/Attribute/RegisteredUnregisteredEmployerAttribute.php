<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class RegisteredUnregisteredEmployerAttribute
 */
trait RegisteredUnregisteredEmployerAttribute{


    // Get
    public function geStatusLabelAttribute()
    {

        if ($this->status == 1){
            if ($this->approval_id == 1){
                return  "<span class='tag tag-success white_color'>" . trans('labels.general.registered') . "</span>" ;
            }else{
                return  "<span class='tag tag-warning white_color'>" . trans('labels.general.pending') . "</span>" ;
            }
        }else{

            return "<span class='tag tag-warning white_color'>" . trans('labels.general.unregistered') . "</span>" ;
        }


    }



}