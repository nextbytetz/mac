<?php

namespace App\Models\Operation\Compliance\Member\Traits\Attribute;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class EmployerParticularChangeAttribute
 */
trait EmployerParticularChangeAttribute{



    public function getApprovedDateFormattedAttribute() {
        return   isset($this->wf_done_date) ? short_date_format($this->wf_done_date) : ' ';
    }
    //Get status label
    public function getStatusLabelAttribute()
    {
            if ($this->wf_done == 0  && $this->wfTracks()->count()  == 0)
                return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending' . "</span>";
            elseif($this->wf_done == 0 && $this->wfTracks()->count()  > 0){
                return "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='" . 'Initiated'. "'>" . 'Initiated' . "</span>";
            }elseif($this->wf_done == 1){
                return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
            }elseif($this->wf_done == 2){
                return "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . 'Declined' . "'>" . 'Declined' . "</span>";
            }

    }



    /*get Resource name for wf*/
    public function getResourceNameAttribute()
    {
        return $this->employer->name;
    }



    /*Get employer change docs path file url*/
    public function getDocPathFileUrl($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $file_path = employer_particular_change_path() . DIRECTORY_SEPARATOR. $uploaded_doc->employer_id . DIRECTORY_SEPARATOR .  $uploaded_doc->external_id . DIRECTORY_SEPARATOR . $doc_pivot_id . '.' .$uploaded_doc->ext; //closure.
        return $file_path;

    }

}