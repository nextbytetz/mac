<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Operation\Compliance\Member\Employee;


/**
 * Class ContributionRelationship
 */
trait ContributionRelationship{



//Relation to the receipt_code
    public function receiptCode(){
        return $this->belongsTo(\App\Models\Finance\Receipt\ReceiptCode::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }



//Relation to the legacy receipt_code
    public function legacyReceiptCode(){
        return $this->belongsTo(\App\Models\Finance\Receipt\LegacyReceiptCode::class);
    }


}
