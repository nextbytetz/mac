<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;
use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Workflow\WfTrack;


/**
 * Class EmployeeCountRelationship
 */
trait EmployerClosureReopenRelationship
{


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * RELATIONSHIP
     */


    public function employerClosure()
    {
        return $this->belongsTo(EmployerClosure::class);
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

}
