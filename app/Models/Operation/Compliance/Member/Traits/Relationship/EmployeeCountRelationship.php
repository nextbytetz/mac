<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;
use App\Models\Sysdef\CodeValue;


/**
 * Class EmployeeCountRelationship
 */
trait EmployeeCountRelationship
{


    //Relation to the employer
    public function employer()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class);
    }


    //Relation to the employee_category
    public function employeeCategory()
    {
        return $this->belongsTo(CodeValue::class, 'employee_category_cv_id', 'id');
    }

    //Relation to the gender
    public function gender()
    {
        return $this->belongsTo(\App\Models\Sysdef\Gender::class);
    }


}
