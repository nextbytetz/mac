<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\UnregisteredEmployer;
use App\Models\Operation\Compliance\Member\UnregisteredEmployerFollowUp;
use App\Models\Sysdef\CodeValue;


/**
 * Class UnregisteredEmployerFollowUpRelationship
 */
trait UnregisteredEmployerFollowUpRelationship {



//Relation to follow up type (code values)
    public function followUpType(){
        return $this->belongsTo(CodeValue::class, 'follow_up_type_cv_id', 'id');
    }
//Relation to user
    public function user(){
        return $this->belongsTo(User::class);
    }

    //Relation to unregistered employer
    public function unregisteredEmployer(){
        return $this->belongsTo(UnregisteredEmployer::class);
    }

}
