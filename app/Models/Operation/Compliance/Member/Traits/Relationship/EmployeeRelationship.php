<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Finance\BankBranch;
use App\Models\Location\Country;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Sysdef\CodeValue;
use App\Services\Scopes\IsApprovedScope;


/**
 * Class EmployeeRelationship
 */
trait EmployeeRelationship {


    //Relation to the employer
    public function employers(){
        return $this->belongsToMany(\App\Models\Operation\Compliance\Member\Employer::class)->withPivot('id', 'basicpay', 'grosspay', 'employee_category_cv_id', 'job_title', 'datehired', 'department')->withTimestamps();
    }

    public function employersWithoutScopes(){
        return $this->belongsToMany(\App\Models\Operation\Compliance\Member\Employer::class)->withouGlobalScope(IsApprovedScope::class)->withTimestamps();
    }



    //Relation to the employee_category
    public function employeeCategory(){
        return $this->belongsTo(CodeValue::class, 'employee_category_cv_id', 'id');
    }

    //Relation to the job title
    public function jobTitle(){
        return $this->belongsTo(\App\Models\Sysdef\JobTitle::class);
    }

    //Relation to the gender
    public function gender(){
        return $this->belongsTo(\App\Models\Sysdef\Gender::class);
    }



    //Relation to the marital status
    public function maritalStatus(){
        return $this->belongsTo(CodeValue::class, 'marital_status_cv_id', 'id');
    }

    //Relation to the contributions
    public function contributions(){
        return $this->hasMany(\App\Models\Operation\Compliance\Member\Contribution::class);
    }

    //Relation to the bank branch
    public function bankBranch(){
        return $this->belongsTo(BankBranch::class);
    }




/* CLAIM RELATIONSHIP =-============================*/

    //Relation to the Notification reports
    public function notificationReports(){
        return $this->hasMany(\App\Models\Operation\Claim\NotificationReport::class);
    }


    //Relation to the Accidents
    public function accidents(){
        return $this->hasManyThrough(\App\Models\Operation\Claim\Accident::class,\App\Models\Operation\Claim\NotificationReport::class);
    }

    //Relation to the disease
    public function diseases(){
        return $this->hasManyThrough(\App\Models\Operation\Claim\Disease::class,\App\Models\Operation\Claim\NotificationReport::class);
    }

    //Relation to the death
    public function death(){
        return $this->hasOne(\App\Models\Operation\Claim\NotificationReport::class);
    }

    //Relation to the claims
    public function claims(){
        return $this->hasManyThrough(\App\Models\Operation\Claim\Claim::class,\App\Models\Operation\Claim\NotificationReport::class);
    }

    //Relation to the Manual Notification reports
    public function manualNotificationReports(){
        return $this->hasMany(ManualNotificationReport::class);
    }


    //Relation to the claims
    public function documents(){
        return $this->hasManyThrough(\App\Models\Operation\Claim\Document::class,\App\Models\Operation\Claim\NotificationReport::class);
    }



    //Relation to the employee old compensation (compensation history)
    public function employeeOldCompensations(){
        return $this->hasMany(\App\Models\Operation\Claim\EmployeeOldCompensation::class);
    }



    //Relation to the employment history
    public function employmentHistories(){
        return $this->hasMany(\App\Models\Operation\Compliance\Member\EmploymentHistory::class);
    }




    //Relation to the dependents
    public function dependents(){

        return $this->belongsToMany(\App\Models\Operation\Compliance\Member\Dependent::class)->withPivot('dependent_type_id', 'survivor_pension_percent', 'survivor_pension_amount', 'survivor_pension_flag', 'survivor_gratuity_percent', 'survivor_gratuity_flag', 'survivor_gratuity_pay_flag', 'funeral_grant_percent', 'funeral_grant', 'funeral_grant_pay_flag', 'survivor_gratuity_amount', 'dependency_percentage', 'isactive', 'isresponded', 'firstpay_flag', 'parent_id', 'notification_report_id', 'manual_notification_report_id', 'hold_reason', 'isotherdep','other_dependency_type', 'pay_period_months','recycles_pay_period', 'deadline_date')->withTimestamps();

    }


    //Relation to the pensioners
    public function pensioners(){
        return $this->hasMany(\App\Models\Operation\Payroll\Pensioner::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

      //Relation to the dependents
    public function accrualDependents(){
        return $this->belongsToMany(\App\Models\Operation\ClaimAccrual\AccrualDependent::class)->withPivot('dependent_type_id', 'survivor_pension_percent', 'survivor_pension_amount', 'survivor_pension_flag', 'survivor_gratuity_percent', 'survivor_gratuity_flag', 'survivor_gratuity_pay_flag', 'funeral_grant_percent', 'funeral_grant', 'funeral_grant_pay_flag', 'survivor_gratuity_amount', 'dependency_percentage', 'isactive', 'isresponded', 'firstpay_flag', 'parent_id', 'notification_report_id', 'manual_notification_report_id', 'hold_reason', 'isotherdep','other_dependency_type', 'pay_period_months','recycles_pay_period')->withTimestamps();
    }

    //Relation to the pensioners
    public function accrualPensioners(){
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualPensioner::class);
    }

    //--END CLAIM ---------------------------------------------
}
