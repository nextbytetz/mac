<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Auth\PortalUser;
use App\Models\Auth\User;
use App\Models\Finance\BankBranch;
use App\Models\Finance\Receipt\LegacyReceipt;
use App\Models\Finance\Receivable\InterestWriteOff;
use App\Models\Finance\Receivable\Portal\ContributionArrear;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\PortalIncident;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Operation\Compliance\Member\EmployeeCount;
use App\Models\Operation\Compliance\Member\EmployeeTemp;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerAdvancePayment;
use App\Models\Operation\Compliance\Member\EmployerCertificateIssue;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\OnlineEmployerVerification;
use App\Models\Sysdef\Business;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfModule;
use App\Services\Scopes\IsApprovedScope;
use App\Models\Workflow\WfTrack;

/**
 * Class EmployerRelationship
 */
trait EmployerRelationship{


    //Relation to the employees
    public function employees(){
        return $this->belongsToMany(\App\Models\Operation\Compliance\Member\Employee::class)->withPivot('basicpay', 'grosspay', 'user_id', 'datehired', 'job_title')->withTimestamps();
    }

//Relation to district
    public function district(){
        return $this->belongsTo(\App\Models\Location\District::class);
    }

// relation to region
    public function region(){
        return $this->belongsTo(\App\Models\Location\Region::class);
    }

    //Relation to receipts
    public function receipts(){
        return $this->hasMany(\App\Models\Finance\Receipt\Receipt::class);
    }

    //Relation to legacy receipts
    public function legacyReceipts(){
        return $this->hasMany(LegacyReceipt::class);
    }


    //Relation to receiptWithTrashed
    public function receiptWithTrashed(){
        return $this->hasMany(\App\Models\Finance\Receipt\Receipt::class)->withTrashed();
    }

    //Relation to receipt_codes
    public function receiptCodes(){
        return $this->hasManyThrough(\App\Models\Finance\Receipt\ReceiptCode::class,
            \App\Models\Finance\Receipt\Receipt::class);
//        return $this->hasMany(\App\Models\Finance\Receipt\ReceiptCode::class);
    }

    //Relation to bookings
    public function bookings(){
        return $this->hasMany(\App\Models\Finance\Receivable\Booking::class);
    }

    //Relation to booking interest
    public function bookingInterests(){
        return $this->hasManyThrough(\App\Models\Finance\Receivable\BookingInterest::class,\App\Models\Finance\Receivable\Booking::class);
    }

    public function bookingGroupEmployer(){
        return $this->belongsTo(\App\Models\Finance\Receivable\BookingGroupEmployer::class);
    }

    public function interestWriteOff()
    {
        return $this->belongsTo(InterestWriteOff::class);
    }

    public function businesses()
    {
//        return $this->belongsToMany(Business::class, 'business_activities');
        return $this->belongsToMany(CodeValue::class, 'employer_sectors', 'employer_id', 'business_sector_cv_id')->withTimestamps();
    }

    public function inspectionTasks()
    {
        return $this->belongsToMany(InspectionTask::class)->withTimestamps();
    }

    //Relation to employer category
    public function employerCategory() {
        return $this->belongsTo(CodeValue::class, 'employer_category_cv_id', 'id');
    }

    //Relation to employer category
    public function employeeCounts(){
        return $this->hasMany(EmployeeCount::class);
    }

    //documents
    public function documents()
    {
        return $this->belongsToMany(Document::class)->withPivot('id','document_id', 'name', 'size', 'mime', 'ext', 'upload_error', 'isuploaded', 'rows_imported', 'total_rows', 'success', 'hasfileerror', 'hasinputerror', 'description')->withTimestamps();
    }

    //sectors
    public function sectors()
    {
        return $this->belongsToMany(CodeValue::class, 'employer_sectors', 'employer_id', 'business_sector_cv_id')->withTimestamps();
    }

    //Parent Employer
    public function parentEmployer()
    {
        return $this->belongsTo(Employer::class, 'parent_id')->withoutGlobalScopes([IsApprovedScope::class]);
    }


    /* notification reports*/
    public function notificationReports(){
        return $this->hasMany(NotificationReport::class);
    }



    //created By
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /* relation to employeeTemps */
    public function employeeTemps()
    {
        return $this->hasMany(EmployeeTemp::class);
    }

    //Relation to the bank branch
    public function bankBranch(){
        return $this->belongsTo(BankBranch::class);
    }

    /**
     * @return mixed
     */
    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource');
    }


    /* relation to registration certificate issues  */
    public function employerCertificateIssues()
    {
        return $this->hasMany(EmployerCertificateIssue::class);
    }

    public function onlineVerifications()
    {
        return $this->hasMany(OnlineEmployerVerification::class);
    }

    public function onlineIncidents()
    {
        return $this->hasMany(PortalIncident::class, "employer_id");
    }

    /**
     * @return mixed
     */
    public function advancePayments()
    {
        return $this->hasMany(EmployerAdvancePayment::class);
    }

    public function onlineUser()
    {
        return $this->belongsToMany(PortalUser::class, pg_mac_portal() . ".employer_user", "employer_id", "user_id")->withPivot('isown', 'stage')->withTimestamps();
    }

    public function closures()
    {
        return $this->hasMany(EmployerClosure::class);
    }

    /**
     * @return mixed
     */
    public function wfModule()
    {
        return $this->belongsTo(WfModule::class, "wf_module_id");
    }

    /*Staff employer table*/
    public function complianceOfficers()
    {
        return $this->belongsToMany(User::class, 'staff_employer', 'employer_id')->where('staff_employer.unit_id', 5)->withPivot('employer_id', 'user_id', 'id', 'isbonus');
    }

    /*Portal - Contrib Arrears*/
    public function portalContribArrears()
    {
        return $this->hasMany(ContributionArrear::class, 'employer_id');
    }

    public function payrollMerges(){
        return $this->hasMany(\App\Models\Operation\Compliance\Member\Online\EmployerPayrollMerge::class);
    }


}
