<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Location\District;
use App\Models\Location\Region;
use App\Models\Notifications\Letter;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosureExtension;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Operation\Compliance\Member\EmployerClosureReopen;
use App\Models\Operation\Compliance\Member\UnregisteredEmployer;
use App\Models\Operation\Compliance\Member\UnregisteredEmployerFollowUp;
use App\Models\Operation\Compliance\Member\UnregisteredFollowupInspectionTask;
use App\Models\Sysdef\CodeValue;
use App\Models\Task\Checker;
use App\Models\Workflow\WfTrack;
use App\Repositories\Backend\Sysdef\CodeValueRepository;


/**
 * Class EmployeeRelationship
 */
trait EmployerClosureRelationship {



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * RELATIONSHIP
     */


    public function employer()
    {
        return $this->hasOne(Employer::class);
    }

    public function closedEmployer()
    {
        return $this->belongsTo(Employer::class, 'employer_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*Follow up to check status tempo closure*/
    public function followupUser()
    {
        return $this->belongsTo(User::class,'followup_by' );
    }

    /*Follow ups*/
    public function employerClosureFollowUps()
    {
        return $this->hasMany(EmployerClosureFollowUp::class);
    }


    //Relation to workflow
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }


    /*Relation to employer_closure_open*/
    public function open()
    {
        return $this->hasOne(EmployerClosureReopen::class, 'employer_closure_id');
    }

    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }


    /*Closure letter*/
    public function closureLetter()
    {
        $codeValue = new CodeValueRepository();
        //Inspection Notice Letter
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->findIdByReference('CLIEMPCLOSURE'));
    }

    /*Closure Settlement dbt letter*/
    public function settlementLetter()
    {
        $codeValue = new CodeValueRepository();
        //Inspection Notice Letter
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->findIdByReference('CLIEMPCLOSSETT'));
    }


    /*Temporary closure sub type*/
    public function subType()
    {
        return $this->belongsTo(CodeValue::class,'sub_type_cv_id' );
    }

    /*Relation to authority source for dormant de-registrations*/
    public function dormantSource()
    {
        return $this->belongsTo(CodeValue::class,'dormant_source_cv_id' );
    }

    /*Closure extensions*/
    public function closureExtensions()
    {
        return $this->hasMany(EmployerClosureExtension::class,'employer_closure_id' );
    }


    public function checkers()
    {
        return $this->morphMany(Checker::class, "resource");
    }


}
