<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Location\District;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Models\Operation\Compliance\Member\UnregisteredEmployer;
use App\Models\Operation\Compliance\Member\UnregisteredEmployerFollowUp;
use App\Models\Operation\Compliance\Member\UnregisteredFollowupInspectionTask;
use App\Models\Sysdef\CodeValue;


/**
 * Class EmployeeRelationship
 */
trait UnregisteredEmployerRelationship {


//Relation to follow ups
    public function unregisteredEmployerFollowUps(){
        return $this->hasMany(UnregisteredEmployerFollowUp::class);
    }

//Relation to user / assigned staff
    public function assignedStaff(){
        return $this->belongsTo(User::class, 'assign_to', 'id');
    }

    //Relation to district
    public function district(){
        return $this->belongsTo(District::class);
    }


    //Relation to region
    public function region(){
        return $this->belongsTo(Region::class);
    }



    /* relation to unregistered followup inspection task */
    public function unregisteredFollowupInspectionTask()
    {
        return $this->belongsTo(UnregisteredFollowupInspectionTask::class);
    }


    /* relation to unregistered followup inspection task */
    public function inspectionTask()
    {
        return $this->belongsTo(InspectionTask::class);
    }

}
