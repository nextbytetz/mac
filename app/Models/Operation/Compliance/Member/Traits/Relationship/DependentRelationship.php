<?php
namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Finance\Bank;
use App\Models\Location\District;
use App\Models\Location\Region;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Payroll\ManualPayrollMember;
use App\Models\Operation\Payroll\ManualPayrollRun;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Models\Operation\Payroll\PayrollBeneficiaryUpdate;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeFollowup;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeMpUpdate;
use App\Models\Operation\Payroll\Run\PayrollSuspendedRun;
use App\Models\Sysdef\Gender;
use Carbon\Carbon;
/**
 * Class DependentRelationship
 */
trait DependentRelationship{


//    //Relation to the dependent
//    public function dependentType(){
//        return $this->belongsTo(\App\Models\Operation\Compliance\Member\DependentType::class);
//    }


    /**
     * @return mixed
     * Depreciated
     */
    public function employee(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class);
    }

    /* Relation to employees*/
    public function employees(){

        return $this->belongsToMany(\App\Models\Operation\Compliance\Member\Employee::class)->withPivot('id','dependent_type_id', 'survivor_pension_percent', 'survivor_pension_amount', 'survivor_pension_flag', 'survivor_gratuity_percent', 'survivor_gratuity_flag', 'survivor_gratuity_pay_flag', 'funeral_grant_percent', 'funeral_grant', 'funeral_grant_pay_flag', 'survivor_gratuity_amount', 'dependency_percentage', 'isactive', 'isresponded', 'firstpay_flag', 'parent_id', 'notification_report_id', 'manual_notification_report_id', 'hold_reason', 'isotherdep','other_dependency_type', 'pay_period_months','recycles_pay_period', 'deadline_date')->withTimestamps();
    }

    /*Relation to gender*/
    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    //Relation to the Bank Branch
    public function bankBranch(){
        return $this->belongsTo(\App\Models\Finance\BankBranch::class);
    }

    //Relation to the Bank
    public function bank(){
        return $this->belongsTo(Bank::class);
    }

    public function documents()
    {
       return $this->belongsToMany(Document::class, "document_payroll_beneficiary", 'resource_id')->wherePivot('member_type_id', 4)->withPivot('id', 'name','description','doc_date', 'doc_receive_date', 'doc_create_date', 'eoffice_document_id', 'folio', 'isused', 'ispending')->withTimestamps();
    }



    /*Relation to recoveries where payroll recovery member_type_id = 4*/
    public function payrollRecoveries()
    {
        return $this->hasMany(PayrollRecovery::class, 'resource_id', 'id')->where('member_type_id', 4);
    }

    public function payrollBankUpdates()
    {
        return $this->hasMany(PayrollBankInfoUpdate::class, 'resource_id', 'id')->where('member_type_id', 4);
    }

    public function payrollBeneficiaryUpdates()
    {
        return $this->hasMany(PayrollBeneficiaryUpdate::class, 'resource_id', 'id')->where('member_type_id', 4);
    }

    public function payrollVerifications()
    {
        return $this->hasMany(PayrollVerification::class, 'resource_id', 'id')->where('member_type_id', 4);
    }

    public function payrollStatusChanges()
    {
        return $this->hasMany(PayrollStatusChange::class, 'resource_id', 'id')->where('member_type_id', 4);
    }

    /*Payroll runs*/
    public function payrollRuns()
    {
        return $this->hasMany(PayrollRun::class, 'resource_id', 'id')->where('member_type_id', 4);

    }

    public function payrollSuspendedRuns()
    {
        return $this->hasMany(PayrollSuspendedRun::class, 'resource_id', 'id')->where('member_type_id', 4);

    }

    /*Relation to manual payroll member*/
    public function manualPayrollMember()
    {
        return $this->hasOne(ManualPayrollMember::class, 'resource_id','id')->where('member_type_id', 4);
    }

    /*Relation to manual payroll runs*/
    public function manualPayrollRuns()
    {
        return $this->hasMany(ManualPayrollRun::class, 'resource_id','id')->where('member_type_id', 4);
    }

    public function region(){
        return $this->belongsTo(Region::class);
    }


    public function district(){
        return $this->belongsTo(District::class);
    }

    public function retireeFollowups(){
        return $this->hasMany(PayrollRetireeFollowup::class, 'resource_id')->where('member_type_id', 4);
    }

    public function retireeMpUpdates(){
        return $this->hasMany(PayrollRetireeMpUpdate::class, 'resource_id')->where('member_type_id', 4);
    }

}