<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;




/**
 * Class ContributionRelationship
 */
trait ContributionTrackRelationship{



//Relation to the user
    public function user(){
        return $this->belongsTo(\App\Models\Auth\User ::class);
    }

    //Relation to the receipt code
    public function receipt_code(){
        return $this->belongsTo(\App\Models\Finance\Receipt\ReceiptCode ::class);
    }
}
