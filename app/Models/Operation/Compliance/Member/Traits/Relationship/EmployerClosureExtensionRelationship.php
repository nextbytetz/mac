<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Location\District;
use App\Models\Location\Region;
use App\Models\Notifications\Letter;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Operation\Compliance\Member\EmployerClosureReopen;
use App\Models\Operation\Compliance\Member\UnregisteredEmployer;
use App\Models\Operation\Compliance\Member\UnregisteredEmployerFollowUp;
use App\Models\Operation\Compliance\Member\UnregisteredFollowupInspectionTask;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;
use App\Repositories\Backend\Sysdef\CodeValueRepository;


/**
 * Class EmployeeRelationship
 */
trait EmployerClosureExtensionRelationship {


    public function user()
    {
        return $this->belongsTo(User::class );
    }

    public function employerClosure()
    {
        return $this->belongsTo(EmployerClosure::class );
    }

    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }


    /*Closure extension letter*/
    public function closureExtensionLetter()
    {
        $codeValue = new CodeValueRepository();
        //Inspection Notice Letter
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->findIdByReference('CLIEMPCLREXT'));
    }

    //Relation to workflow
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

}
