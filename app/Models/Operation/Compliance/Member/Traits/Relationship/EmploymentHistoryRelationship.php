<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use Carbon\Carbon;
/**
 * Class EmploymentHistoryRelationship
 */
trait EmploymentHistoryRelationship{


    //Relation to the job title
    public function jobTitle(){
        return $this->belongsTo(\App\Models\Sysdef\JobTitle::class);
    }


    //Relation to the employer
    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class);
    }

    //Relation to the employee
    public function employee(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class);
    }

    //Relation to the insurance
    public function insurance(){
        return $this->belongsTo(\App\Models\Operation\Claim\Insurance::class);
    }

}