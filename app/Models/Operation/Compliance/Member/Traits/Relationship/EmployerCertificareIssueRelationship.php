<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;
use App\Models\Auth\User;


/**
 * Class EmployeeCountRelationship
 */
trait EmployerCertificareIssueRelationship
{


    //Relation to the employer
    public function employer()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class);
    }


   //Relation to the user
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
