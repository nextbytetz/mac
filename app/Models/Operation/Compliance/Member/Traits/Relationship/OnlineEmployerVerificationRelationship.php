<?php

namespace App\Models\Operation\Compliance\Member\Traits\Relationship;

use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;

trait OnlineEmployerVerificationRelationship
{
    public function documents()
    {
        return $this->belongsToMany(Document::class)->withPivot('id', 'name', 'size', 'mime', 'ext')->withTimestamps();
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class)->withTrashed();
    }

    public function user()
    {
        return $this->morphTo();
    }

}