<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployeeAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\EmployeeRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Employee extends Model implements AuditableContract
{
    use  EmployeeAttribute, EmployeeRelationship, SoftDeletes, Auditable;

    protected $guarded = [];

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}
