<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\OnlineEmployerVerificationAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\OnlineEmployerVerificationRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnlineEmployerVerification extends Model
{
    use OnlineEmployerVerificationRelationship, OnlineEmployerVerificationAttribute, Notifiable, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
