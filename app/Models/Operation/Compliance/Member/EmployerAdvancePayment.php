<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerAdvancePaymentAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\EmployerAdvancePaymentRelationship;
use Illuminate\Database\Eloquent\Model;

class EmployerAdvancePayment extends Model
{
    use EmployerAdvancePaymentAttribute, EmployerAdvancePaymentRelationship;

    protected $guarded = [];
    public $timestamps = true;

}
