<?php

namespace App\Models\Operation\Compliance\Member\Online;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Auth\User;
use App\Models\Notifications\Letter;

class EmployerPayrollMerge extends Model
{
    //
	// protected $fillable = ['employer_id','old_values','new_values','user_id','merge_reason','wf_done','wf_done_date','date_received','source','user_type','status','old_users','new_user','attachment','letter_date','letter_reference','arrears','employees','receipts'];
	protected $guarded = [];

	public function getStatusLabelAttribute()
	{
		switch ($this->status)
		{
			case 0:
			$label = '<span class="tag square-tag bg-warning " data-toggle="tooltip" title="Pending">Pending</span>';
			break;
			case 1:
			$label = '<span class="tag square-tag bg-info " data-toggle="tooltip" title="On Progress">Initiated</span>';
			break;
			case 2:
			$label = '<span class="tag square-tag bg-success " data-toggle="tooltip" title="Approved">Approved</span>';
			break;
			case 3:
			$title = $this->wf_done ? 'Rejected' : 'Reversed';
			$label = '<span class="tag square-tag bg-danger " data-toggle="tooltip" title="'.$title.'">'.$title.'</span>';
			break;
			case 4:
			$label = '<span class="tag square-tag bg-danger " data-toggle="tooltip" title="Cancelled">Cancelled</span>';
			break;
			default:
			$label = '';
			break;
		}
		return $label;
	}

	public function getUserNameAttribute()
	{
		if ($this->source == 1) {
			//MAC
			$user = DB::table('main.users')->select(DB::raw("CONCAT(users.firstname,' ',users.lastname) as name"))
			->where('id',$this->user_id)->first();
			$return = !empty($user->name) ? ucwords(strtolower($user->name)) : '';
		} else {
			//online
			$user = DB::table('portal.users')->select('name')
			->where('id',$this->user_id)->first();
			$return = !empty($user->name) ? ucwords(strtolower($user->name)) : '';
		}
		
		return $return;
	}

	public function getCreatedAtFormattedAttribute()
	{
		return !empty($this->created_at) ? Carbon::parse($this->created_at)->format('M d, Y H:i A') : ' ';
	}

	public function getApprovalDateAttribute()
	{
		return !empty($this->wf_done_date) ? Carbon::parse($this->wf_done_date)->format('M d, Y') : ' ';
	}

	
	public function employer(){
		return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class);
	}

	public function wfTracks()
	{
		return $this->morphMany(\App\Models\Workflow\WfTrack::class, 'resource');
	}

	public function getResourceNameAttribute()
	{
		return $this->employer->name.' Payroll Merging';
	}	

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function letters()
	{
		return $this->morphMany(Letter::class, 'resource');
	}

	public function getSourceFormattedAttribute()
	{
		$return = "";
		switch ($this->source) {
			case 1:
			$return = "<span class='tag tag-success white_color'>" . trans('labels.general.branch') . "</span>";
			break;
			case 2:
			$return = "<span class='tag tag-success white_color'>" . trans('labels.general.online') . "</span>";
			break;
		}
		return $return;
	}

	public function getCreatedByFormattedAttribute()
	{
		$return = "";
		switch ($this->source) {
			case 1:
			$return = $this->user->firstname.' '.$this->user->lastname;
			break;
			case 2:
			$return = "";
			break;
		}
		return $return;
	}

	public function getNewUserFormattedAttribute()
	{
		$user_id = (unserialize($this->new_user))[0];
		if ($this->merge_type == 1) {
			$user = DB::table('portal.employer_user')
			->join('portal.users', 'users.id', '=', 'employer_user.user_id')
			->where('user_id', $user_id)
			->where('employer_id',$this->employer_id)->first();
		}else{
			$user = DB::table('portal.users')
			->where('id', $user_id)->first();
			return $user->name.' - '.(unserialize($this->new_values))[0];
		}

		return $user->name.' - '.$user->payroll_id;
	}

	public function getNewUserPayrollAttribute()
	{
		$user_id = (unserialize($this->new_user))[0];
		if ($this->merge_type == 1) {
			$user = DB::table('portal.employer_user')
			->join('portal.users', 'users.id', '=', 'employer_user.user_id')
			->where('user_id', $user_id)
			->where('employer_id',$this->employer_id)->first();
		}else{
			return (unserialize($this->new_values))[0];
		}
		
		return $user->payroll_id;
	}

	public function getOldUserFormattedAttribute($key)
	{
		$user_id = (unserialize($this->old_users))[$key];
		$user = DB::table('portal.employer_user')
		->join('portal.users', 'users.id', '=', 'employer_user.user_id')
		->where('user_id', $user_id)
		->where('employer_id',$this->employer_id)->first();
		return $user->name.' - '.$user->payroll_id;

	}

	public function getOldUSerPayroll($key)
	{
		$user_id = (unserialize($this->old_users))[$key];
		$user = DB::table('portal.employer_user')
		->join('portal.users', 'users.id', '=', 'employer_user.user_id')
		->where('user_id', $user_id)
		->where('employer_id',$this->employer_id)->first();
		return $user->payroll_id;
	}

	public function getMergedUsersEmailsAttribute()
	{
		$user_id = (unserialize($this->old_users));
		$user = DB::table('portal.employer_user')
		->select('email')
		->join('portal.users', 'users.id', '=', 'employer_user.user_id')
		->whereIn('user_id', $user_id)
		->where('employer_id',$this->employer_id)->get();
		return count($user) > 0 ? json_decode(json_encode($user->toArray()), true) : [];
	}

	public function getKeepUserEmailAttribute()
	{
		$user_id = (unserialize($this->new_user))[0];
		$user = DB::table('portal.employer_user')
		->select('email')
		->join('portal.users', 'users.id', '=', 'employer_user.user_id')
		->where('user_id', $user_id)
		->where('employer_id',$this->employer_id)->first();
		return count($user) > 0 ? $user->email : '';
	}
	

}
