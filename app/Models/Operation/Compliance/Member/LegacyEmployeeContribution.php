<?php

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LegacyEmployeeContribution extends Model
{

    public function getBasicpayFormattedAttribute() {
        return number_format( $this->salary , 2 , '.' , ',' );
    }

    public function getContribMonthFormattedAttribute() {
        return  Carbon::parse($this->contribution_month)->format('M-Y');
    }

}