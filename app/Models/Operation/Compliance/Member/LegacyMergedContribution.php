<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Finance\Receipt\LegacyReceipt;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LegacyMergedContribution extends Model
{
    //

    public function getContribMonthFormattedAttribute() {
        return  Carbon::parse($this->contrib_month)->format('d-M-Y');
    }

    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }


    public function getRctnoFormattedAttribute(){
        return str_pad($this->rctno, 7, "0", STR_PAD_LEFT);
    }

    public function getRctDateFormattedAttribute() {
        return  Carbon::parse($this->rct_date)->format('d-M-Y');
    }


    public function getAmountFormattedAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }


    public function legacyReceipt()
    {
        return $this->belongsTo(LegacyReceipt::class, 'rctno', 'rctno');
    }

}
