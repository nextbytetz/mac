<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerClosureAttribute;
use App\Models\Workflow\WfTrack;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class StaffEmployerAllocation extends Model implements AuditableContract
{


    use    Auditable, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * RELATIONSHIP
     */


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /*end of relationship*/
    /**
     * ATTRIBUTES
     */

    public function getApprovalStatusAttribute()
    {
        if($this->isapproved == 0){
            return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending' . "</span>";

        }else{
            /*approved*/
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>";
        }
    }

    /*status label*/
    public function getStatusLabelAttribute()
    {
        if($this->run_status == 0 && $this->isapproved  == 0)
        {
            return   'Run status : ' .  $this->getRunStatusLabelAttribute();
        }elseif($this->status == 2){
            return  'Status : '.   "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Completed' . "'>" . 'Completed' . "</span>";
        }else{
            return  'Status : '.   $this->getProgressStatusLabelAttribute();
        }
    }


    public function getRunStatusLabelAttribute()
    {
        if($this->run_status == 0 && $this->isapproved == 0){
            return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending' . "</span>";
        }else{
            /*approved*/
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Completed' . "'>" . 'Completed' . "</span>";
        }
    }

    /*get title of allocation*/
    public function getTitleAttribute()
    {
        $string = 'Allocation for ';
        if($this->category == 2){
            return $string . 'Non Large contributing employers';
        }elseif($this->category == 1){
            return $string . 'Large contributing employers';
        }elseif($this->category == 3){
            return $string . 'Bonus employers';
        }elseif($this->category == 4){
            return $string . 'Treasury employers';
        }elseif($this->category == 5){
            return $string . 'Intern employers';
        }
    }


    /*Get no of employers allocated*/
    public function getNoOfEmployersAllocatedAttribute()
    {
        return DB::table('staff_employer')->where('staff_employer.isactive',1)->where('staff_employer_allocation_id', $this->id)->count();
    }



    public function getProgressStatusLabelAttribute()
    {

        $today = Carbon::now();
        if($this->isapproved == 1){

            if(comparable_date_format($today)  > comparable_date_format($this->end_date)){
                return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Complete'. "'>" . 'Complete' . "</span>";
            }else{
                return "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='" . 'On Progress'. "'>" . 'On Progress' . "</span>";
            }

        }else{
            return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending for Approval'. "'>" . 'Pending for Approval' . "</span>";
        }

    }




}
