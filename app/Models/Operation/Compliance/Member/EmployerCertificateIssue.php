<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerCertificateIssueAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\EmployerCertificareIssueRelationship;
use Illuminate\Database\Eloquent\Model;

class EmployerCertificateIssue extends Model
{

    use EmployerCertificareIssueRelationship, EmployerCertificateIssueAttribute;

    protected $guarded = [];
    public $timestamps = true;





}
