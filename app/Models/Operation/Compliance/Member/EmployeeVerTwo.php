<?php

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeVerTwo extends Model
{
    //
    use  SoftDeletes;
    protected $primaryKey = 'employee_id';
    protected $guarded = [];
    public $timestamps = true;
}
