<?php

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeUpload extends Model
{
    //
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $connection = 'pgportal';
}
