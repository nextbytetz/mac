<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\RegisteredUnregisteredEmployerAttribute;
use Illuminate\Database\Eloquent\Model;

class RegisteredUnregisteredEmployer extends Model
{

use RegisteredUnregisteredEmployerAttribute;

    protected $guarded = [];




}