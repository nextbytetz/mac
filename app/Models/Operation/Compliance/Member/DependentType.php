<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace  App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DependentType extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;



}