<?php

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;

class PortalAdvancePayment extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'advance_payments';
}
