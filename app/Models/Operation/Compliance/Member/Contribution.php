<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\ContributionAttribute;
use App\Services\Scopes\IsactiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Operation\Compliance\Member\Traits\Relationship\ContributionRelationship;

class Contribution extends Model
{
    //
    use ContributionAttribute ,ContributionRelationship, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    protected $fillable = ['receipt_code_id','legacy_receipt_code_id', 'employee_amount', 'grosspay', 'employee_id','employer_id', 'user_id', 'contribution_temp_id','legacy_contribution_temp_id', 'salary', 'contrib_month' ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IsactiveScope);
    }


}