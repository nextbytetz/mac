<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Compliance\Member;


use App\Models\Operation\Compliance\Member\Traits\Relationship\EmployeeCountRelationship;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmployeeCount extends Model implements AuditableContract
{
    //
    use   EmployeeCountRelationship, Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var arrayistrat
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}