<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerClosureAttribute;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerClosureReopenAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\EmployerClosureReopenRelationship;
use App\Models\Workflow\WfTrack;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmployerClosureReopen extends Model implements AuditableContract
{


    use    Auditable, SoftDeletes, EmployerClosureReopenRelationship, EmployerClosureReopenAttribute;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];





}
