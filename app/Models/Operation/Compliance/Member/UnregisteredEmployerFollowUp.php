<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\UnregisteredEmployerFollowUpAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\UnregisteredEmployerFollowUpRelationship;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class UnregisteredEmployerFollowUp extends Model implements AuditableContract
{
    //
    use  UnregisteredEmployerFollowUpRelationship, UnregisteredEmployerFollowUpAttribute, Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var arrayistrat
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}