<?php

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Carbon\Carbon;
use App\Models\Notifications\Letter;
use App\Models\Auth\User;
use App\Repositories\Backend\Access\UserRepository;


class ContributionInterestRate extends Model implements AuditableContract
{
  use SoftDeletes, Auditable;

  protected $guarded = [];
  public $timestamps = true;
  protected $connection = 'pgmain';

  protected $auditableEvents = [
    'deleted',
    'updated',
    'restored'];

    public function getRateNameAttribute()
    {
     return $this->fin_code_id == 1 ? "Interest Rate" : "Contribution Rate";
   }

   public function getSectorNameAttribute()
   {
     return $this->employer_category_cv_id == 36 ? $return .' Public ' : $return .' Private ';
   }

   public function getCreatedDateFormattedAttribute() {
    return  !empty($this->created_at) ? Carbon::parse($this->created_at)->format('d F, Y') : '';
  }

  public function getLongNameAttribute()
  {
    return $this->name.' for '.$this->sector_name.' Sector';
  }

  public function getNameAttribute()
  {
   return $this->fin_code_id == 1 ? "Interest" : "Contribution";
 }

 public function getStageLabelAttribute()
 {
  switch ($this->wf_status)
  {
    case 1:
    $label = '<span class="tag square-tag bg-info " data-toggle="tooltip" title="On Progress">On Progress</span>';
    break;
    case 2:
    $label = '<span class="tag square-tag bg-success " data-toggle="tooltip" title="Approved">Approved</span>';
    break;
    case 3:
    $title = $this->wf_done ? 'Rejected' : 'Reversed';
    $label = '<span class="tag square-tag bg-danger " data-toggle="tooltip" title="'.$title.'">'.$title.'</span>';
    break;
    case 4:
    $label = '<span class="tag square-tag bg-danger " data-toggle="tooltip" title="Cancelled">Cancelled</span>';
    break;
    default:
    $label = '<span class="tag square-tag bg-warning " data-toggle="tooltip" title="Pending">Not Submited</span>';
    break;
  }
  return $label;
}


public function parent(){
  return $this->belongsTo(\App\Models\Operation\Compliance\Member\ContributionInterestRate::class,'parent_id');
}

public function getStartDateFormattedAttribute() {
  return  !empty($this->start_date) ? Carbon::parse($this->start_date)->firstOfMonth()->format('F, Y') : '';
}

public function getEndDateFormattedAttribute() {
  return  !empty($this->end_date) ? Carbon::parse($this->end_date)->firstOfMonth()->format('F, Y') : '';
}

public function wfTracks()
{
  return $this->morphMany(\App\Models\Workflow\WfTrack::class, 'resource');
}


public function getResourceNameAttribute()
{
  return $this->rate_name.' for '.$this->sector_name.' Sector';
}


public function user()
{
  return $this->belongsTo(User::class);
}




}
