<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerSizeTypeAttribute;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class EmployerSizeType extends Model
{

   use EmployerSizeTypeAttribute;
    //
    protected $guarded = [];
    public $timestamps = true;
}
