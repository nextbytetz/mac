<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\EmploymentHistoryAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\EmploymentHistoryRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmploymentHistory extends Model implements AuditableContract
{
    //
    use  EmploymentHistoryAttribute, EmploymentHistoryRelationship, SoftDeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}