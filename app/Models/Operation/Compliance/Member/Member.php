<?php

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{

    protected $primaryKey = "member_id";

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'legacy';

    protected $fillable = ['firstname', 'lastname', 'middlename' , 'dob', 'salary', 'employer_id', 'allowance', 'member_no', 'date_created', 'created_by'];

}
