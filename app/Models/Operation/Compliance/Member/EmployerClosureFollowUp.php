<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Auth\User;
use App\Models\Sysdef\CodeValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmployerClosureFollowUp extends Model implements AuditableContract
{


    use    Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    /**
     * ATTRIBUTE
     */

    public function getDateOfFollowUpFormattedAttribute()
    {
        return isset($this->date_of_follow_up) ? short_date_format($this->date_of_follow_up) : ' ';
    }





    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * RELATIONSHIP
     */


    public function employerClosure()
    {
        return $this->belongsTo(EmployerClosure::class);
    }

    public function followUpType()
    {
        return $this->belongsTo(CodeValue::class, 'follow_up_type_cv_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
