<?php

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ContributionTemp extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['employeeno', 'firstname', 'middlename', 'lastname', 'basicpay', 'grosspay', 'dob', 'receipt_code_id', 'error', 'error_report'];

}