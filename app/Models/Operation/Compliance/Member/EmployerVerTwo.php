<?php

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class EmployerVerTwo extends Model
{
    //
    use  SoftDeletes;
    protected $primaryKey = 'employer_id';
    protected $guarded = [];
    public $timestamps = true;
}
