<?php

namespace App\Models\Operation\Compliance\Member\ContributionModification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Repositories\Backend\Access\UserRepository;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class contribModificationRedistribution extends Model implements AuditableContract
{

    use SoftDeletes,Auditable;
    
    protected $guarded = [];
    public $timestamps = true;

    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    public function modification()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\ContributionModification\ContribModification::class);
    }
}
