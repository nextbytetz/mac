<?php

namespace App\Models\Operation\Compliance\Member\ContributionModification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ContribModificationMonth extends Model implements AuditableContract
{
    //
    use SoftDeletes,Auditable;
    
    protected $guarded = [];
    public $timestamps = true;

    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    public function modification()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\ContributionModification\ContribModification::class);
    }

    public function receiptCode(){
        // dd($this->modification())->receipt_type);
        if ($this->receipt_code_type == 2) {
            return $this->belongsTo(\App\Models\Finance\Receipt\LegacyReceiptCode::class,'receipt_code_id');
        } else {
            return $this->belongsTo(\App\Models\Finance\Receipt\ReceiptCode::class,'receipt_code_id');
        }
    }
}
