<?php

namespace App\Models\Operation\Compliance\Member\ContributionModification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Repositories\Backend\Access\UserRepository;
use App\Models\Notifications\Letter;
use App\Models\Auth\User;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ContribModification extends Model implements AuditableContract
{

    use SoftDeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;

    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    public function getRequestTypeFormattedAttribute()
    {
        switch ($this->request_type) {
            case 1:
            $return = 'Change Contribution Month';
            break;
            case 2:
            $return = 'Redistribute Contribution Amount';
            break;
            case 3:
            $return = 'Underpaid Amount';
            break;
            default:
            $return = '';
            break;
        }

        return $return;
    }

    public function getStageLabelAttribute()
    {
        switch ($this->status)
        {
            case 1:
            $label = '<span class="tag square-tag bg-info " data-toggle="tooltip" title="On Progress">On Progress</span>';
            break;
            case 2:
            $label = '<span class="tag square-tag bg-success " data-toggle="tooltip" title="Approved">Approved</span>';
            break;
            case 3:
            $title = $this->wf_done ? 'Rejected' : 'Reversed';
            $label = '<span class="tag square-tag bg-danger " data-toggle="tooltip" title="'.$title.'">'.$title.'</span>';
            break;
            case 4:
            $label = '<span class="tag square-tag bg-danger " data-toggle="tooltip" title="Cancelled">Cancelled</span>';
            break;
            default:
            $label = '<span class="tag square-tag bg-warning " data-toggle="tooltip" title="Pending">Pending</span>';
            break;
        }
        return $label;
    }

    public function getCreatedByNameAttribute()
    {
        $user = (new UserRepository)->find($this->user_id);
        return count($user) ? trim($user->name) : '';
    }

    public function getRequestDateFormattedAttribute() {
        return  !empty($this->date_received) ? Carbon::parse($this->date_received)->format('d F, Y') : '';
    }

    public function getCreatedDateFormattedAttribute() {
        return  !empty($this->created_at) ? Carbon::parse($this->created_at)->format('d F, Y') : '';
    }

    public function months()
    {
        //contrib modification months relationship
        return $this->hasMany(\App\Models\Operation\Compliance\Member\ContributionModification\ContribModificationMonth::class);
    }

    public function redistributions()
    {
        //contrib modification redistribution relationship
        return $this->hasMany(\App\Models\Operation\Compliance\Member\ContributionModification\contribModificationRedistribution::class);
    }

    public function wfTracks()
    {
        return $this->morphMany(\App\Models\Workflow\WfTrack::class, 'resource');
    }

    public function getResourceNameAttribute()
    {
        return strtoupper($this->employer->name).' - '.$this->rctno;
    }

    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class);
    }

    public function receipt(){
        if ($this->receipt_type == 2) {
            return $this->belongsTo(\App\Models\Finance\Receipt\LegacyReceipt::class,'receipt_id');
        } else {
            return $this->belongsTo(\App\Models\Finance\Receipt\Receipt::class);
        }
    }

    

}
