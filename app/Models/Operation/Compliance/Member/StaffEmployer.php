<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerClosureAttribute;
use App\Models\Workflow\WfTrack;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class StaffEmployer extends Model implements AuditableContract
{


    use    Auditable, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;
    protected $table = 'staff_employer';


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];








}
