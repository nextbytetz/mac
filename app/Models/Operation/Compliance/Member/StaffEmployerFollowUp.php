<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerClosureAttribute;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class StaffEmployerFollowUp extends Model implements AuditableContract
{


    use    Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * RELATIONSHIP
     */


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*Reviewed by*/
    public function reviewedBy()
    {
        return $this->belongsTo(User::class, 'reviewed_by');
    }

    /*Assigned reviewer*/
    public function assignedReviewer()
    {
        return $this->belongsTo(User::class, 'assigned_reviewer');
    }

    /*follow up type*/
    public function followUpType()
    {
        return $this->belongsTo(CodeValue::class, 'follow_up_type_cv_id');
    }

    /*feedback up type*/
    public function feedBack()
    {
        return $this->belongsTo(CodeValue::class, 'feedback_cv_id');
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }
}
