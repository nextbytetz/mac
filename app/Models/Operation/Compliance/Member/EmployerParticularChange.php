<?php

namespace App\Models\Operation\Compliance\Member;

use App\Models\Auth\User;
use App\Models\Auth\PortalUser;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerClosureAttribute;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerParticularChangeAttribute;
use App\Models\Workflow\WfTrack;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmployerParticularChange extends Model implements AuditableContract
{


    use    Auditable, SoftDeletes, EmployerParticularChangeAttribute;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * RELATIONSHIP
     */


    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function onlineUser()
    {
       return $this->source == 2 ? $this->belongsTo(PortalUser::class,'user_id') : null;
   }


    //Relation to workflows
   public function wfTracks(){
    return $this->morphMany(WfTrack::class, 'resource');
}

/*Has particular changes*/
public function changeTypes()
{
    return $this->hasMany(EmployerParticularChangeType::class,'employer_particular_change_id');
}


}
