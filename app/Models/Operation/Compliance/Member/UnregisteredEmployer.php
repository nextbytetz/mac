<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Traits\Attribute\UnregisteredEmployerAttribute;
use App\Models\Operation\Compliance\Member\Traits\Relationship\UnregisteredEmployerRelationship;
use App\Services\Scopes\IsactiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class UnregisteredEmployer extends Model implements AuditableContract
{
    //
    use    UnregisteredEmployerAttribute, UnregisteredEmployerRelationship,Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var arrayistrat
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IsactiveScope());
    }




}