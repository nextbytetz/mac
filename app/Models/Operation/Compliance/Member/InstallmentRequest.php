<?php

namespace App\Models\Operation\Compliance\Member;

// use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerAdvancePaymentAttribute;
// use App\Models\Operation\Compliance\Member\Traits\Relationship\EmployerAdvancePaymentRelationship;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Workflow\WfTrack;

class InstallmentRequest extends Model
{
    // use EmployerAdvancePaymentAttribute, EmployerAdvancePaymentRelationship;
    protected $connection = "pgmain";

	protected $guarded = [];
	public $timestamps = true;

	public function wfTracks()
	{
		return $this->morphMany(WfTrack::class, 'resource');
	}

}
