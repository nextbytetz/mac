<?php

namespace App\Models\Operation\Compliance\Member;

use App\Services\Scopes\IsApprovedScope;
use Illuminate\Database\Eloquent\Model;
use App\Models\Operation\Compliance\Member\Traits\Relationship\EmployerRelationship;
use App\Models\Operation\Compliance\Member\Traits\Attribute\EmployerAttribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Notifications\Notifiable;

class Employer extends Model implements AuditableContract
{
    //
    use EmployerRelationship, EmployerAttribute,  SoftDeletes, Auditable, Notifiable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IsApprovedScope);
    }



}
