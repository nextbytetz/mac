<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Compliance\Member;

use Illuminate\Database\Eloquent\Model;
use App\Models\Operation\Compliance\Member\Traits\Relationship\ContributionTrackRelationship;
use App\Models\Operation\Compliance\Member\Traits\Attribute\ContributionTrackAttribute;

class ContributionTrack extends Model
{
    //
    use   ContributionTrackRelationship,ContributionTrackAttribute;

    protected $guarded = [];
    public $timestamps = true;


}