<?php

namespace App\Models\Operation\Compliance\Inspection;

use App\Models\Workflow\WfModule;
use App\Models\Workflow\WfTrack;
use Illuminate\Database\Eloquent\Model;

class EmployerInspectionTaskWorkflow extends Model
{

    protected $guarded = [];
    public $timestamps = true;

    public function getResourceNameAttribute()
    {
        $return = $this->employerTask->employer->name;
        return $return;
    }

    /**
     * @return mixed
     */
    public function wfModule()
    {
        return $this->belongsTo(WfModule::class);
    }

    /**
     * @return mixed
     */
    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource')->orderBy("wf_tracks.id", "asc");
    }

    public function employerTask()
    {
        return $this->belongsTo(EmployerInspectionTask::class, "employer_inspection_task_id");
    }

}
