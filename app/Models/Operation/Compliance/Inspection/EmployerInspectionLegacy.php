<?php

namespace App\Models\Operation\Compliance\Inspection;

use Illuminate\Database\Eloquent\Model;

class EmployerInspectionLegacy extends Model
{

    protected $guarded = [];
    public $timestamps = true;

    public $primaryKey = "id";

    protected $table = "employer_inspection_legacy";
}
