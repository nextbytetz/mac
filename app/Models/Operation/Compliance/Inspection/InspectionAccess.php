<?php

namespace App\Models\Operation\Compliance\Inspection;

/**
 * Class InspectionAccess
 * @package App\Models\Operation\Compliance\Inspection
 */
trait InspectionAccess
{
    /**
     * Save inputted users (Allocated to do the inspection)
     *
     * @param $inputUsers
     */
    public function saveUsers($inputUsers)
    {
        if (! empty($inputUsers)) {
            $this->users()->sync($inputUsers);
        } else {
            $this->users()->detach();
        }
    }

    /**
     * Attach user to current inspection.
     *
     * @param $user
     */
    public function attachUser($user)
    {
        if (is_object($user)) {
            $user = $user->getKey();
        }

        if (is_array($user)) {
            $user = $user['id'];
        }

        $this->users()->attach($user);
    }

    /**
     * Detach user form current inspection.
     *
     * @param $user
     */
    public function detachUser($user)
    {
        if (is_object($user)) {
            $user = $user->getKey();
        }

        if (is_array($user)) {
            $user = $user['id'];
        }

        $this->users()->detach($user);
    }

    /**
     * Attach multiple users to current inspection.
     *
     * @param $users
     */
    public function attachUsers($users)
    {
        foreach ($users as $user) {
            $this->attachUser($user);
        }
    }

    /**
     * Detach multiple users from current inspection.
     *
     * @param $users
     */
    public function detachUsers($users)
    {
        foreach ($users as $user) {
            $this->detachUser($user);
        }
    }
}