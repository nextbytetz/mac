<?php

namespace App\Models\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\Attribute\InspectionProfileAttribute;
use App\Models\Operation\Compliance\Inspection\Relationship\InspectionProfileRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InspectionProfile extends Model
{
    use InspectionProfileAttribute, InspectionProfileRelationship, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    protected $fillable = ['name', 'query', 'user_id'];
}