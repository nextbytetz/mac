<?php

namespace App\Models\Operation\Compliance\Inspection;

use Illuminate\Database\Eloquent\Model;

class InspectionAssessmentTemplate extends Model
{

    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inspection_assessment_template';

}
