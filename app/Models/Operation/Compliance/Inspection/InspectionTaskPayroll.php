<?php

namespace App\Models\Operation\Compliance\Inspection;

use App\Models\Notifications\Letter;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Database\Eloquent\Model;

class InspectionTaskPayroll extends Model
{

    protected $guarded = [];
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inspection_task_payrolls';

    public function getHasAssessmentLabelAttribute()
    {
        if ($this->hasassessment)
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='Assessment Uploaded'>Assessment Uploaded</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='Assessment Not Uploaded'>Assessment Not Uploaded</span>";
    }

    public function getResponseLetterStatusLabelAttribute()
    {
        $return = "";
        $letterQuery = $this->responseLetter();
        $print_status = "<span class='tag tag-warning white_color'>Not Printed</span>";
        if ($letterQuery->count()) {
            if ($letterQuery->first()->isprinted) {
                $print_status = "<span class='tag tag-success white_color'>Printed</span>";
            }
        }
        $return = "&nbsp; Response Letter Status &nbsp;:&nbsp;{$print_status}" ;
        return $return;
    }

    public function getReminderLetterStatusLabelAttribute()
    {
        $return = "";
        $letterQuery = $this->reminderLetter();
        $print_status = "<span class='tag tag-warning white_color'>Not Printed</span>";
        if ($letterQuery->count()) {
            if ($letterQuery->first()->isprinted) {
                $print_status = "<span class='tag tag-success white_color'>Printed</span>";
            }
        }
        $return = "&nbsp; Reminder Letter Status &nbsp;:&nbsp;{$print_status}" ;
        return $return;
    }

    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }

    public function responseLetter()
    {
        $codeValue = new CodeValueRepository();
        //Inspection Response Letter ( Demand Notice Letter (Inspection) or Letter of Compliance (Inspection) )
        return $this->hasOne(Letter::class, "resource_id")->whereIn("cv_id", [$codeValue->CLDNDNOLTR(), $codeValue->CLLTRFCMPLNC()]);
    }

    public function reminderLetter()
    {
        $codeValue = new CodeValueRepository();
        //Employer Reminder Letter (Inspection)
        return $this->hasOne(Letter::class, "resource_id")->whereIn("cv_id", [$codeValue->CLEMPRMDLTR()]);
    }

}
