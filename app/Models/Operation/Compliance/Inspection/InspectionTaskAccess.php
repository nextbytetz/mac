<?php

namespace App\Models\Operation\Compliance\Inspection;

/**
 * Class InspectionTaskAccess
 * @package App\Models\Operation\Compliance\Inspection
 */
trait InspectionTaskAccess
{
    /**
     * Save the inputted employers.
     *
     * @param $inputEmployers
     */
    public function saveEmployers($inputEmployers)
    {
        if (! empty($inputEmployers)) {
            $this->employers()->sync($inputEmployers);
        } else {
            $this->employers()->detach();
        }
    }

    /**
     * Attach employer to current inspection task.
     *
     * @param $employer
     */
    public function attachEmployer($employer)
    {
        if (is_object($employer)) {
            $employer = $employer->getKey();
        }

        if (is_array($employer)) {
            $employer = $employer['id'];
        }

        $this->employers()->attach($employer);
    }

    /**
     * Detach employer form current inspection task.
     *
     * @param $employer
     */
    public function detachEmployer($employer)
    {
        if (is_object($employer)) {
            $employer = $employer->getKey();
        }

        if (is_array($employer)) {
            $employer = $employer['id'];
        }

        $this->employers()->detach($employer);
    }

    /**
     * Attach multiple employers to current inspection task.
     *
     * @param $employers
     */
    public function attachEmployers($employers)
    {
        foreach ($employers as $employer) {
            $this->attachEmployer($employer);
        }
    }

    /**
     * Detach multiple employers from current inspection task.
     *
     * @param $employers
     */
    public function detachEmployers($employers)
    {
        foreach ($employers as $employer) {
            $this->detachEmployer($employer);
        }
    }

    /**
     * Save the inputted users.
     *
     * @param $inputUsers
     */
    public function saveUsers($inputUsers)
    {
        if (! empty($inputUsers)) {
            $this->users()->sync($inputUsers);
        } else {
            $this->users()->detach();
        }
    }

    /**
     * Attach user to current inspection task.
     *
     * @param $user
     */
    public function attachUser($user)
    {
        if (is_object($user)) {
            $user = $user->getKey();
        }

        if (is_array($user)) {
            $user = $user['id'];
        }

        $this->users()->attach($user);
    }

    /**
     * Detach user form current inspection task.
     *
     * @param $user
     */
    public function detachUser($user)
    {
        if (is_object($user)) {
            $user = $user->getKey();
        }

        if (is_array($user)) {
            $user = $user['id'];
        }

        $this->users()->detach($user);
    }

    /**
     * Attach multiple users to current inspection task.
     *
     * @param $users
     */
    public function attachUsers($users)
    {
        foreach ($users as $user) {
            $this->attachUser($user);
        }
    }

    /**
     * Detach multiple users from current inspection task.
     *
     * @param $users
     */
    public function detachUsers($users)
    {
        foreach ($users as $user) {
            $this->detachUser($user);
        }
    }

}