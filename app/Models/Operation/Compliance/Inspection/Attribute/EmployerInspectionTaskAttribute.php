<?php

namespace App\Models\Operation\Compliance\Inspection\Attribute;

use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait EmployerInspectionTaskAttribute
{

    public function getStageLabelAttribute()
    {
        $return = "";
        $stage = "";
        if ($this->stage()->count()) {
            $stage = $this->stage->name;
        }
        switch ($this->staging_cv_id) {
            default:
                $return = "<span class='tag square-tag spice-color'  data-toggle='tooltip' data-html='true' title='" . $stage . "'>" . $stage . "</span>";
                break;
        }
        return $return;
    }

    public function getStatusLabelAttribute()
    {
        $status = $this->status;
        if ($status == '1')
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.complete') . "'>" . trans('labels.general.complete') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.incomplete') . "'>" . trans('labels.general.incomplete') . "</span>";
    }

    /**
     * @return string
     */
    public function getNextStageAttribute()
    {
        $return = "";
        if ($this->stages()->count()) {
            $pivot = $this->stages()->first()->pivot;
            if ($pivot->require_attend And !$pivot->attended) {
                $comments = json_decode($pivot->comments);
                $nextStage = $comments->{'nextStage'};
                $return = "<span style=\"font-size: medium;\"><b>&#x27A5;&nbsp;</b>&nbsp;nextStage:&nbsp;<span class='underline'>{$nextStage}</span></span>";
            }
        }
        return $return;
    }

    public function getVisitDateFormattedAttribute()
    {
        if (!$this->visit_date) {
            $return = "-";
        } else {
            $return = Carbon::parse($this->visit_date)->format('d-M-Y');
        }
        return $return;
    }

    public function getExitDateFormattedAttribute()
    {
        if (!$this->exit_date) {
            $return = "-";
        } else {
            $return = Carbon::parse($this->exit_date)->format('d-M-Y');
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getDocFormattedAttribute()
    {
        if (!$this->doc) {
            $return = $this->employer->date_of_commencement_formatted;
        } else {
            $return = Carbon::parse($this->doc)->format('d-M-Y');
        }
        return $return;
    }

    public function getLastInspectionDateFormattedAttribute()
    {
        if (!$this->last_inspection_date) {
            $return = "-";
        } else {
            $return = Carbon::parse($this->last_inspection_date)->format('d-M-Y');
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getReviewStartDateFormattedAttribute()
    {
        if (!$this->review_start_date) {
            $return = "-";
        } else {
            $return = Carbon::parse($this->review_start_date)->format('d-M-Y');
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getReviewEndDateFormattedAttribute()
    {
        if (!$this->review_end_date) {
            $return = "-";
        } else {
            $return = Carbon::parse($this->review_end_date)->format('d-M-Y');
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getLastReviewEndDateLabelAttribute()
    {
        $return = "-";
        if ($this->last_review_end_date) {
            $return = Carbon::parse($this->last_review_end_date)->format('M-Y');
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getInitializeTaskCancellationLinkAttribute()
    {
        $wfModule = new WfModuleRepository();
        $type = $wfModule->employerInspectionCancellationType()["type"];
        $group = $wfModule->employerInspectionCancellationType()["group"];
        $route = route("backend.compliance.inspection.employer_task.initiate_workflow");
        return "<span>" . link_to('#', "<i class=\"icon fa fa-info\" aria-hidden=\"true\"></i>&nbsp;Initiate Employer Task Cancellation Workflow", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Employer Task Cancellation Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
    }

    /**
     * @return string
     */
    public function getInitializeRefundOverPaymentLinkAttribute()
    {
        $wfModule = new WfModuleRepository();
        $type = $wfModule->employerInspectionRefundOverPaymentType()["type"];
        $group = $wfModule->employerInspectionRefundOverPaymentType()["group"];
        $route = route("backend.compliance.inspection.employer_task.initiate_workflow");
        return "<span>" . link_to('#', "<i class=\"icon fa fa-info\" aria-hidden=\"true\"></i>&nbsp;Initiate Refund OverPayment Workflow", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Refund OverPayment Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
    }

    /**
     * @return string
     */
    public function getInitializePaymentInstallmentLinkAttribute()
    {
        $wfModule = new WfModuleRepository();
        $type = $wfModule->employerInspectionInstalmentPaymentType()["type"];
        $group = $wfModule->employerInspectionInstalmentPaymentType()["group"];
        $route = route("backend.compliance.inspection.employer_task.initiate_workflow");
        return "<span>" . link_to('#', "<i class=\"icon fa fa-info\" aria-hidden=\"true\"></i>&nbsp;Initiate Payment Instalment Workflow", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Payment Instalment Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
    }

    /**
     * @return string
     */
    public function getInitializeDefaultedEmployerLinkAttribute()
    {
        $wfModule = new WfModuleRepository();
        $type = $wfModule->employerInspectionDefaulterType()["type"];
        $group = $wfModule->employerInspectionDefaulterType()["group"];
        $route = route("backend.compliance.inspection.employer_task.initiate_workflow");
        return "<span>" . link_to('#', "<i class=\"icon fa fa-info\" aria-hidden=\"true\"></i>&nbsp;Initiate Defaulted Employer Workflow", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Defaulted Employer Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
    }

    public function getInitializeWorkflowAttribute()
    {
        //Enforceable Workflow
        $return = "";
        $codeValue = new CodeValueRepository();
        $wfModule = new WfModuleRepository();
        $route = route("backend.compliance.inspection.employer_task.initiate_workflow");
        switch ($this->staging_cv_id) {
            case $codeValue->EITPUPIASSC():
                //Uploaded Inspection Assessment Schedule
                $type = $wfModule->employerInspectionAssessmentReviewType()["type"];
                $group = 21;
                $return = "<span>" .
                    link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Inspection Assessment Review Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Inspection Assessment Review Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                    "</span>";
                break;
            case $codeValue->EITPDFLTDEMPLY():
                //Defaulted Employer ==> Default Employer Workflow
                $type = $wfModule->employerInspectionDefaulterType()["type"];
                $group = 21;
                $return = "<span>" .
                    link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Defaulters (Employer) Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Defaulters (Employer) Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                    "</span>";
                break;
            case $codeValue->EITPISUEDIRSLR():
            case $codeValue->EITPISSREMLTR():
                //Issued Inspection Response Letter
                //Issuing Reminder Letter : Payment By Installment Workflow
                $type = $wfModule->employerInspectionInstalmentPaymentType()["type"];
                $group = 21;
                $return = "<span>" .
                    link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Instalment Payment Request (Employer) Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Instalment Payment Request (Employer) Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                    "</span>";
                break;
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getFileNameLabelAttribute()
    {
        $fileName = "";
        switch (true) {
            default:
                //Employer Inspection Task Filename
                $fileName = $this->employer->name;
                break;
        }
        return $fileName;
    }

    /*Get closure doc path file url*/
    public function getEmployerTaskPathFileUrl($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer_inspection_task')->where('id', $doc_pivot_id)->first();
        $file_path = inspection_employer_task_path() . DIRECTORY_SEPARATOR. $uploaded_doc->employer_inspection_task_id . DIRECTORY_SEPARATOR . $doc_pivot_id . '.' . $uploaded_doc->ext; //closure.
        return $file_path;
    }

}