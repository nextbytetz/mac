<?php

namespace App\Models\Operation\Compliance\Inspection\Attribute;

use App\Services\Storage\DataFormat;

trait InspectionProfileAttribute
{

    public function getInfoAttribute()
    {
        $return = "";
        $query = $this->newQuery();
        $dataFormat = new DataFormat();
        $return = $dataFormat->dbArrayInfo($this->query);
        return $return;
    }





    public function getEmployersCountAttribute()
    {
        $return = "";
        $dataFormat = new DataFormat();
        $return = $dataFormat->dbToArrayQuery($this->query)->count();
        return $return;
    }



    /**
     * @return string
     * Un assigned unregistered employers count
     */
    public function getUnregisteredEmployersCountUnassignedAttribute()
    {
        $return = "";
        $dataFormat = new DataFormat();
        $return = $dataFormat->dbToArrayQueryFollowup($this->query)->where('assign_to', 0)->count();
        return $return;
    }


    /* Get unregistered employers count assigned to followup inspection */
    public function getUnregisteredEmployersCountAssignedAttribute()
    {
        $return = "";
//        $query = $this->newQuery();
        $dataFormat = new DataFormat();
        $return = $dataFormat->dbToArrayQueryFollowup($this->query)->where('assign_to','>', 0)->count();
        return $return;
    }


    public function getNameWithUnregisteredCountAttribute()
    {
        return $this->name . ' (' . $this->getUnregisteredEmployersCountUnassignedAttribute() . ')';
    }

    /* name with count of employers in this profile (Registered Employers) */
    public function getNameWithEmployerCountAttribute()
    {
            return $this->name . ' (' . $this->getEmployersCountAttribute() . ')';

    }




}