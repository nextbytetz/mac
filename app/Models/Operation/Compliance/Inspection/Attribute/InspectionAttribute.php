<?php

namespace App\Models\Operation\Compliance\Inspection\Attribute;

use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use Carbon\Carbon;
use Carbon\CarbonInterval;

trait InspectionAttribute
{

    public function getStatusLabelAttribute()
    {
        $return = "";
        $stage = "";
        if ($this->stage()->count()) {
            $stage = $this->stage->name;
        }
        switch ($this->staging_cv_id) {
            default:
                $return = "<span class='tag square-tag spice-color'  data-toggle='tooltip' data-html='true' title='" . $stage . "'>" . $stage . "</span>";
                break;
        }
        return $return;
    }

    public function getCreatedAtFormattedAttribute() {
//        return  Carbon::parse($value)->diffForHumans();
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

    public function getCancelDateFormattedAttribute() {
        return  Carbon::parse($this->cancel_date)->format('d-M-Y');
    }

    public function getStartDateFormattedAttribute($value) {
        return  Carbon::parse($value)->format('d-M-Y');
    }

    public function getEndDateFormattedAttribute()
    {
        if (is_null($this->end_date)) {
            $return = "-";
        } else {
            $return = Carbon::parse($this->end_date)->format('d-M-Y');
        }
        return  $return;
    }

    public function isCancelled() {
        return $this->iscancelled == 1;
    }

    public function getDurationFormattedAttribute()
    {
        if (is_null($this->end_date)) {
            $return = "-";
        } else {
            $start = Carbon::parse($this->start_date);
            $end = Carbon::parse($this->end_date);
            $now = Carbon::now();
            if ($end->gt($now) && $now->gt($start)) {
                $left_length = $end->diffInWeekdays($now);
                $left_days = ", ". $left_length . " " . trans('labels.backend.compliance_menu.inspection.days_left');
            } else {
                $left_days = "";
            }
            $working_length = $end->diffInWeekdays($start);
            $length = $end->diffInDays($start);
            $return = CarbonInterval::days($length). " (" . $working_length . " " . trans('labels.backend.compliance_menu.inspection.days') . $left_days . ")";
        }
        return $return;
    }


    /**
     * @return string
     */
    public function getNextStageAttribute()
    {
        $return = "";
        if ($this->stages()->count()) {
            $pivot = $this->stages()->first()->pivot;
            if ($pivot->require_attend And !$pivot->attended) {
                $comments = json_decode($pivot->comments);
                $nextStage = $comments->{'nextStage'};
                $return = "<span style=\"font-size: medium;\"><b>&#x27A5;&nbsp;</b>&nbsp;nextStage:&nbsp;<span class='underline'>{$nextStage}</span></span>";
            }
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getFileNameLabelAttribute()
    {
        $fileName = "";
        switch (true) {
            default:
                //Inspection Filename
                $fileName = $this->filename;
                break;
        }
        return $fileName;
    }

    public function getIscancelledLabelAttribute()
    {
        if ($this->iscancelled == 0)
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.active') . "'>" . trans('labels.general.active') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.compliance_menu.inspection.cancelled') . "'>" . trans('labels.backend.compliance_menu.inspection.cancelled') . "</span>";
    }

    public function getCompleteStatusLabelAttribute() {
        if ($this->complete_status == '1')
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.complete') . "'>" . trans('labels.general.complete') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.incomplete') . "'>" . trans('labels.general.incomplete') . "</span>";

    }

    public function getCompletedLabelAttribute() {
        $task_count_registered = 0;
        $task_count_unregistered = 0;
        $task_count_registered_complete = 0;
        $task_count_unregistered_complete = 0;
        foreach ($this->inspectionTasks as $task) {
            $task_count_registered += $task->employers->count();
            $task_count_registered_complete += $task->employers()->where("status", '1')->count();
            $task_count_unregistered += $task->unregisteredEmployers()->count();
            $task_count_unregistered_complete += $task->unregisteredEmployers()->where("is_registered", '1')->count();
        }
        $tasks = $task_count_registered + $task_count_unregistered;
        if ($tasks == 0) {
            $tasks = 1;
        }
        $tasks_complete = $task_count_registered_complete + $task_count_unregistered_complete;
        $percent = round((($tasks_complete/$tasks)*100), 2);
        return "<span style='background-color: #c9dae1;padding: 3px;border-radius: 2px;font-weight: bold;'>" . $percent . "%</span>";
    }

    public function getInitializeWorkflowAttribute()
    {
        $return = "";
        $codeValue = new CodeValueRepository();
        $wfModule = new WfModuleRepository();
        $route = route("backend.compliance.inspection.initiate_workflow");
        switch ($this->staging_cv_id) {
            case $codeValue->IOPISPCRTD():
                //Inspection Created
                $type = $wfModule->employerInspectionPlanType()["type"];
                $group = 20;
                $return = "<span>" .
                    link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Inspection Approval Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Inspection Plan Approval Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                    "</span>";
                break;
        }
        return $return;
    }

    public function getCanAssignInspectorsAttribute()
    {
        $return = true;
/*        $codeValue = new CodeValueRepository();
        switch ($this->staging_cv_id) {
            case $codeValue->IOPOISPPAW():
            case $codeValue->IOPAPPRVDIPLN():
            case $codeValue->IOPPLNVIDNLTR():
                //On Inspection Plan Approval Workflow
                //Approved Inspection Plan
                //Planning Visit, Issuing Demand Notice Letter
                $return = true;
                break;
        }*/
        return $return;
    }

    public function getCanDisplayWorkflowAttribute()
    {
        $return = false;
        $codeValueRepo = new CodeValueRepository();
        switch ($this->staging_cv_id) {
            case $codeValueRepo->IOPISPCRTD():
            case $codeValueRepo->IOPOISPPAW():
                //Inspection Created
                //On Inspection Plan Approval Workflow
                $return = true;
                break;
        }
        return $return;
    }

    public function getCanEditTaskAttribute()
    {
        $return = true;
        $codeValueRepo = new CodeValueRepository();
        switch ($this->staging_cv_id) {
            case $codeValueRepo->IOPISPCRTD():
            case $codeValueRepo->IOPOISPPAW():
                //Inspection Created
                //On Inspection Plan Approval Workflow
                $return = true;
                break;
        }
        return $return;
    }

    public function getCanCancelAttribute()
    {
        $return = false;
        $codeValueRepo = new CodeValueRepository();
        switch ($this->staging_cv_id) {
            case $codeValueRepo->IOPISPCRTD():
                //Inspection Created
                $return = true;
                break;
        }
        return $return;
    }

    public function getCanCompleteAttribute()
    {
        $return = false;
        $codeValueRepo = new CodeValueRepository();
        switch ($this->staging_cv_id) {
            case $codeValueRepo->IOPCOMPLTDIPCN():
                //Completed Inspection
                $return = true;
                break;
        }
        return $return;
    }

    public function getCanEditAttribute()
    {
        $return = true; //to be controlled later
        $codeValueRepo = new CodeValueRepository();
        switch ($this->staging_cv_id) {
            case $codeValueRepo->IOPISPCRTD():
                //Inspection Created
                $return = true;
                break;
        }
        return $return;
    }

    public function getResourceNameAttribute()
    {
        $return = $this->inspectionType->name;
        return $return;
    }

}