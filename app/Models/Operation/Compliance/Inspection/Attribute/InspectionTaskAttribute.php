<?php

namespace App\Models\Operation\Compliance\Inspection\Attribute;


trait InspectionTaskAttribute
{

    public function getCompletedLabelAttribute()
    {
        $task_count_registered = 0;
        $task_count_unregistered = 0;
        $task_count_registered_complete = 0;
        $task_count_unregistered_complete = 0;
        $task_count_registered += $this->employers->count();
        $task_count_registered_complete += $this->employers()->where("status", '1')->count();
        $task_count_unregistered += $this->unregisteredEmployers()->count();
        $task_count_unregistered_complete += $this->unregisteredEmployers()->where("is_registered", '1')->count();
        $tasks = $task_count_registered + $task_count_unregistered;
        if ($tasks == 0) {
            $tasks = 1;
        }
        $tasks_complete = $task_count_registered_complete + $task_count_unregistered_complete;
        $percent = round((($tasks_complete/$tasks)*100), 2);
        return "<span style='background-color: #c9dae1;padding: 3px;border-radius: 2px;font-weight: bold;'>" . $percent . "%</span>";
    }

    public function getDeliverableLabelAttribute()
    {
        if (is_null($this->deliverable)) {
            $return = "-";
        } else {
            $return = $this->deliverable;
        }
        return $return;
    }

}