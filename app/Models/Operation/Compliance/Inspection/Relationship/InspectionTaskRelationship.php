<?php

namespace App\Models\Operation\Compliance\Inspection\Relationship;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Inspection\EmployerInspectionTask;
use App\Models\Operation\Compliance\Inspection\Inspection;
use App\Models\Operation\Compliance\Inspection\InspectionProfile;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\UnregisteredEmployer;
use App\Models\Sysdef\CodeValue;

trait InspectionTaskRelationship
{
    public function employers()
    {
        return $this->belongsToMany(Employer::class)->withPivot('id', 'visit_date', 'findings', 'resolutions', 'status')->withTimestamps();
    }

    public function otherEmployers()
    {
        return $this->hasMany(EmployerInspectionTask::class)->whereNull("employer_id");
    }

    public function employerTasks()
    {
        return $this->hasMany(EmployerInspectionTask::class, "inspection_task_id");
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function allocations()
    {
        return $this->belongsToMany(User::class, "employer_inspection_task", "inspection_task_id", "allocated")->withPivot('employer_id', 'visit_date', 'findings', 'resolutions', 'status', "exit_date", "doc", "last_inspection_date", "review_start_date", "review_end_date")->withTimestamps();
    }

    public function inspection()
    {
        return $this->belongsTo(Inspection::class);
    }


    public function inspectionProfile()
    {
        return $this->belongsTo(InspectionProfile::class);
    }


    public function unregisteredEmployers()
    {
        return $this->hasMany(UnregisteredEmployer::class);
    }

    public function inspectionType()
    {
        return $this->belongsTo(CodeValue::class, "inspection_type_cv_id");
    }

}