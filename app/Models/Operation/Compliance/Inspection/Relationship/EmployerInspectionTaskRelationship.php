<?php

namespace App\Models\Operation\Compliance\Inspection\Relationship;

use App\Models\Auth\User;
use App\Models\Notifications\Letter;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Inspection\EmployerInspectionTaskWorkflow;
use App\Models\Operation\Compliance\Inspection\InspectionAssessment;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Models\Operation\Compliance\Inspection\InspectionTaskPayroll;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Sysdef\CodeValue;
use App\Models\Task\Checker;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

trait EmployerInspectionTaskRelationship
{

    public function inspectionTask()
    {
        return $this->belongsTo(InspectionTask::class, "inspection_task_id");
    }

    public function task()
    {
        return $this->belongsTo(InspectionTask::class, "inspection_task_id");
    }

    public function workflows()
    {
        return $this->hasMany(EmployerInspectionTaskWorkflow::class, "employer_inspection_task_id")->orderBy("employer_inspection_task_workflows.id", "asc");
    }

    /**
     * @return mixed
     * @description Workflow Processes
     */
    public function processes()
    {
        return $this->hasMany(EmployerInspectionTaskWorkflow::class)->orderByDesc("employer_inspection_task_workflows.id");
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }


    public function allocatedUser()
    {
        return $this->belongsTo(User::class, "allocated");
    }


    public function stage()
    {
        return $this->belongsTo(CodeValue::class, "staging_cv_id");
    }

    /**
     * @return mixed
     */
    public function stages()
    {
        //return $this->hasMany(NotificationStage::class);
        return $this->belongsToMany(CodeValue::class, "employer_inspection_task_stages", "employer_inspection_task_id", "staging_cv_id")->whereIn("code_id", [38, 39])->withPivot('id', 'comments', 'allow_repeat', 'attended', 'require_attend', 'isdeleted')->withTimestamps()->orderByDesc("employer_inspection_task_stages.id");
    }

    public function checkers()
    {
        return $this->morphMany(Checker::class, "resource");
    }

    public function assessments()
    {
        return $this->hasMany(InspectionAssessment::class, "employer_inspection_task_id");
    }

    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }

    public function noticeLetter()
    {
        $codeValue = new CodeValueRepository();
        //Inspection Notice Letter
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->CLINNOLTR());
    }

    public function demandLetter()
    {
        $codeValue = new CodeValueRepository();
        //Demand Notice Letter (Inspection)
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->CLDNDNOLTR());
    }

    public function complianceLetter()
    {
        $codeValue = new CodeValueRepository();
        //Letter of Compliance (Inspection)
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->CLLTRFCMPLNC());
    }

    public function reminderLetter()
    {
        $codeValue = new CodeValueRepository();
        //Employer Reminder Letter (Inspection)
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->CLEMPRMDLTR());
    }

    public function refundLetter()
    {
        $codeValue = new CodeValueRepository();
        //Refund Overpayment
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->CLEMPRMDLTR());
    }

    public function mouAgreement()
    {
        $codeValue = new CodeValueRepository();
        //Employer MoU Payment Agreement
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->CLEMPMOUAGRMNT());
    }


    public function documents()
    {
        return $this->belongsToMany(Document::class, "document_employer_inspection_task")->withPivot('id','document_id', 'name', 'size', 'mime', 'ext', 'description')->withTimestamps();
    }

    public function payrolls()
    {
        return $this->hasMany(InspectionTaskPayroll::class, "employer_inspection_task_id");
    }

}