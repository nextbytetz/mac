<?php

namespace App\Models\Operation\Compliance\Inspection\Relationship;

use App\Models\Auth\User;

trait InspectionProfileRelationship
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}