<?php

namespace App\Models\Operation\Compliance\Inspection\Relationship;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Inspection\EmployerInspectionTask;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Models\Operation\Compliance\Inspection\InspectionType;
use App\Models\Sysdef\CodeValue;
use App\Models\Task\Checker;
use App\Models\Workflow\WfModule;
use App\Models\Workflow\WfTrack;

trait InspectionRelationship
{
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function inspectionType()
    {
        return $this->belongsTo(CodeValue::class, "inspection_type_cv_id");
    }

    public function inspectionTasks()
    {
        return $this->hasMany(InspectionTask::class);
    }

    public function employerTasks()
    {
        return $this->hasManyThrough(EmployerInspectionTask::class, InspectionTask::class, 'inspection_id', 'inspection_task_id');
    }

    /**
     * @return mixed
     */
    public function stages()
    {
        return $this->belongsToMany(CodeValue::class, "inspection_stages", "inspection_id", "staging_cv_id")->whereIn("code_id", [33,34,35])->withPivot('id', 'comments', 'allow_repeat', 'attended', 'require_attend', 'isdeleted')->withTimestamps()->orderByDesc("inspection_stages.id");
    }

    public function checkers()
    {
        return $this->morphMany(Checker::class, "resource");
    }

    public function wfModule()
    {
        return $this->belongsTo(WfModule::class, "wf_module_id");
    }

    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource');
    }


    public function stage()
    {
        return $this->belongsTo(CodeValue::class, "staging_cv_id");
    }

    public function allocatedUser()
    {
        return $this->belongsTo(User::class, "allocated");
    }


}