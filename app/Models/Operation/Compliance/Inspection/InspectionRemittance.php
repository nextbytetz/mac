<?php

namespace App\Models\Operation\Compliance\Inspection;

use Illuminate\Database\Eloquent\Model;

class InspectionRemittance extends Model
{

    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inspection_remittance';

}
