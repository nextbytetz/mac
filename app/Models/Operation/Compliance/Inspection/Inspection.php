<?php

namespace App\Models\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\Attribute\InspectionAttribute;
use App\Models\Operation\Compliance\Inspection\Relationship\InspectionRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inspection extends Model
{
    use InspectionAccess, InspectionAttribute, InspectionRelationship, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

}