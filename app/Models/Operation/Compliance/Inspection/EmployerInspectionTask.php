<?php

namespace App\Models\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\Attribute\EmployerInspectionTaskAttribute;
use App\Models\Operation\Compliance\Inspection\Relationship\EmployerInspectionTaskRelationship;
use Illuminate\Database\Eloquent\Model;

class EmployerInspectionTask extends Model
{
    use EmployerInspectionTaskRelationship, EmployerInspectionTaskAttribute;

    protected $guarded = [];
    public $timestamps = true;

    public $primaryKey = "id";

    protected $table = "employer_inspection_task";

}