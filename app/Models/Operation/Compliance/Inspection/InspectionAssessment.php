<?php

namespace App\Models\Operation\Compliance\Inspection;

use Illuminate\Database\Eloquent\Model;

class InspectionAssessment extends Model
{
    protected $guarded = [];
    public $timestamps = true;

    public function employerTask()
    {
        return $this->belongsTo(EmployerInspectionTask::class, "employer_inspection_task_id");
    }

}
