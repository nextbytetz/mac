<?php

namespace App\Models\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\Attribute\InspectionTaskAttribute;
use App\Models\Operation\Compliance\Inspection\Relationship\InspectionTaskRelationship;
use Illuminate\Database\Eloquent\Model;

class InspectionTask extends Model
{
    use InspectionTaskAccess, InspectionTaskAttribute, InspectionTaskRelationship;

    protected $guarded = [];
    public $timestamps = true;

}