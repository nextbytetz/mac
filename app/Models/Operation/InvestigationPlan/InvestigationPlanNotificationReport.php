<?php

namespace App\Models\Operation\InvestigationPlan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class InvestigationPlanNotificationReport extends Model implements AuditableContract
{
	use SoftDeletes,Auditable;

	protected $fillable = ["investigation_plan_id","notification_report_id","assigned_to","added_by","removed_by","investigation_plan_user_id","deleted_at","removal_reason","is_investigated","old_notification_investigator"];

	public function plan()
	{
		return $this->belongsTo(\App\Models\Operation\InvestigationPlan\InvestigationPlan::class);
	}

	public function checkers()
	{
		return $this->morphMany(\App\Models\Task\Checker::class, "resource");
	}
}
