<?php

namespace App\Models\Operation\InvestigationPlan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class InvestigationPlanUser extends Model implements AuditableContract
{
	use SoftDeletes,Auditable;

	protected $fillable = ["investigation_plan_id","user_id","added_by","removed_by","deleted_at"];

	public function plan()
	{
		return $this->belongsTo(\App\Models\Operation\InvestigationPlan\InvestigationPlan::class);
	}
}
