<?php

namespace App\Models\Operation\InvestigationPlan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Carbon\Carbon;
use App\Repositories\Backend\Access\UserRepository;

class InvestigationPlan extends Model implements AuditableContract
{
	use SoftDeletes,Auditable;

	protected $fillable = ["plan_name","investigation_category_cv_id","start_date","end_date","investigation_reason","number_of_files","number_of_investigators","user_id","duration","investigation_type_cv_id","individual_mode","wf_initiated_at","wf_initiator","reminder_sent_date","is_reminder_sent","is_escalation_sent","escalation_sent_date","total_budget","budget_attachment","budget_uploaded_name"];

	//attributes
	public function getPlanTypeAttribute() {
		$type = (new CodeValueRepository)->find($this->investigation_type_cv_id);
		return  !empty($type->name) ? $type->name : '';
	}

	public function getPlanCategoryAttribute() {
		$category = (new CodeValueRepository)->find($this->investigation_category_cv_id);
		return  !empty($category->name) ? $category->name : '';
	}

	public function getStartDateFormattedAttribute() {
		return  !empty($this->start_date) ? Carbon::parse($this->start_date)->format('d F, Y') : '';
	}

	public function getEndDateFormattedAttribute() {
		return  !empty($this->end_date) ? Carbon::parse($this->end_date)->format('d F, Y') : '';
	}


	public function getCreatedByNameAttribute()
	{
		$user = (new UserRepository)->find($this->user_id);
		return count($user) ? trim($user->name) : '';
	}

	public function getCanAssignAttribute()
	{
		$return = false;
		if ($this->user_id == access()->user()->id && (($this->plan_status == 0 || ($this->plan_status == 3 && $this->wf_done == 0)))) {
			$return = true;
		} 
		return $return;
	}

	public function getIndividualConductionModeAttribute()
	{
		$return = 0;
		if (!empty($this->individual_mode))	{
			$return = $this->individual_mode == 1 ? 'Alone' : 'With Others';
		} 
		return $return;
	}

	public function getPlanStageLabelAttribute()
	{
		switch ($this->plan_status)
		{
			case 0:
			$label = '<span class="tag square-tag bg-warning " data-toggle="tooltip" title="Pending">Pending</span>';
			break;
			case 1:
			$label = '<span class="tag square-tag bg-info " data-toggle="tooltip" title="On Progress">On Progress</span>';
			break;
			case 2:
			$label = '<span class="tag square-tag bg-success " data-toggle="tooltip" title="Approved">Approved</span>';
			break;
			case 3:
			$title = $this->wf_done ? 'Rejected' : 'Reversed';
			$label = '<span class="tag square-tag bg-danger " data-toggle="tooltip" title="'.$title.'">'.$title.'</span>';
			break;
			case 4:
			$label = '<span class="tag square-tag bg-danger " data-toggle="tooltip" title="Cancelled">Cancelled</span>';
			break;
			default:
			$label = '';
			break;
		}

		switch ($this->is_complete)
		{
			case 1:
			$label .= '&nbsp;<span class="tag square-tag bg-success " data-toggle="tooltip" title="All files in this plan have been investigated">Completed</span>';
			break;
			default:
			$label .= '&nbsp;<span class="tag square-tag bg-warning " data-toggle="tooltip" title="All or some files in this plan have not been investigated">In Complete</span>';
			break;
		}

		return $label;
	}


	public function getResourceNameAttribute()
	{
		return $this->plan_name;
	}	



	//relationships
	public function user()
	{
		return $this->belongsTo(\App\Models\Auth\User::class);
	}

	public function wfTracks()
	{
		return $this->morphMany(\App\Models\Workflow\WfTrack::class, 'resource');
	}

	public function investigators()
	{
		return $this->hasMany(\App\Models\Operation\InvestigationPlan\InvestigationPlanUser::class);
	}

	public function notifications()
	{
		return $this->hasMany(\App\Models\Operation\InvestigationPlan\InvestigationPlanNotificationReport::class);
	}




}
