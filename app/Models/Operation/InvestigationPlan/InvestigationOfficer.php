<?php

namespace App\Models\Operation\InvestigationPlan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class InvestigationOfficer extends Model implements AuditableContract
{
	use SoftDeletes,Auditable;

	protected $guarded = [];
	public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
    	'deleted',
    	'updated',
    	'restored',
    ];

}
