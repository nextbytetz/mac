<?php

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\DocumentAttribute;
use App\Models\Operation\Claim\Traits\Relationship\DocumentRelationship;
use App\Services\Scopes\IsactiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Document extends Model  implements AuditableContract
{
    use DocumentRelationship, DocumentAttribute,Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'main.documents';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IsactiveScope());
    }

}