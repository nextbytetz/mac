<?php

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Pd\Occupation;
use App\Models\Operation\Claim\Pd\PdAmputation;
use App\Models\Operation\Claim\Pd\PdImpairment;
use App\Models\Operation\Claim\Pd\PdInjury;
use Illuminate\Database\Eloquent\Model;

class IncidentAssessment extends Model
{
    protected $guarded = [];
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function occupation()
    {
        return $this->belongsTo(Occupation::class);
    }

    public function pdInjury()
    {
        return $this->belongsTo(PdInjury::class);
    }

    public function pdAmputation()
    {
        return $this->belongsTo(PdAmputation::class);
    }

    public function pdImpairment()
    {
        return $this->belongsTo(PdImpairment::class);
    }

}
