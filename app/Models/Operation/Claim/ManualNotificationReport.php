<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\ManualNotificationReportAttribute;
use App\Models\Operation\Claim\Traits\Attribute\MedicalExpenseAttribute;
use App\Models\Operation\Claim\Traits\Relationship\ManualNotificationReportRelationship;
use App\Models\Operation\Claim\Traits\Relationship\MedicalExpenseRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ManualNotificationReport extends Model implements AuditableContract
{
    //
    use  SoftDeletes, Auditable, ManualNotificationReportRelationship, ManualNotificationReportAttribute;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}