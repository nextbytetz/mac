<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class MaeSub extends Model
{
    protected $guarded = [];
    public $timestamps = true;

    public function healthProvider()
    {
        return $this->belongsTo(HealthProvider::class);
    }

    public function expense()
    {
        return $this->belongsTo(MedicalExpense::class, "medical_expense_id");
    }

    public function services()
    {
        return $this->belongsToMany(HealthServiceChecklist::class, "mae_services");
    }

    public function getAmountFormattedAttribute() {
        $return = 0;
        if (!is_null($this->amount)) {
            $return = number_format( $this->amount , 2 , '.' , ',' );
        }
        return $return;
    }

    public function getAssessedAmountFormattedAttribute() {
        $amount = 0;
        if (!is_null($this->assessed_amount)) {
            $amount = number_format( $this->assessed_amount , 2 , '.' , ',' );
        }
        return $amount;
    }

}
