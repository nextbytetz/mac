<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Relationship\BenefitTypeRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BenefitType extends Model
{
    //
    use BenefitTypeRelationship,SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    public function documents()
    {
        return $this->belongsToMany(Document::class, "document_benefits")->withPivot("member_type_id", "incident_type_id");
    }

}