<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class DocumentResource extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'document_resource';

    protected $guarded = [];
    public $timestamps = true;

    public function resource()
    {
        return $this->morphTo();
    }

    public function document()
    {
        return $this->belongsTo(Document::class);
    }

}
