<?php

namespace App\Models\Operation\Claim\HspBilling;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class HspBillDetail extends Model implements AuditableContract
{


    use    Auditable, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    public function hspBillSummary()
    {
        return $this->belongsTo(\App\Models\Operation\Claim\HspBilling\HspBillSummary::class, 'hsp_summary_id');
    }
    
    
}
