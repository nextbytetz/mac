<?php

namespace App\Models\Operation\Claim\HspBilling;

use App\Models\Auth\User;
use App\Models\Workflow\WfTrack;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Operation\Claim\HspBilling\HspBillSummary;
use App\Services\Workflow\Workflow;

class ApiStakeholder extends Model implements AuditableContract
{


    use    Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'updated',
    ];


    public function hspBillSummary()
    {
        return $this->hasMany(HspBillSummary::class,'stakeholder_id');
    }


}
