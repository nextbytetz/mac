<?php

namespace App\Models\Operation\Claim\HspBilling;

use App\Models\Auth\User;
use App\Models\Workflow\WfTrack;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Operation\Claim\HspBilling\HspBillDetail;
use App\Services\Workflow\Workflow;

class HspBillSummary extends Model implements AuditableContract
{


    use    Auditable, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    //Relation to workflows
    public function wfTracks(){
        return $this->morphMany(WfTrack::class, 'resource');
    }

    public function getResourceNameAttribute()
    {
        return $this->id.' '.$this->month.' '.$this->year;
    }   

    public function hspBillDetails()
    {
        return $this->hasMany(HspBillDetail::class,'hsp_summary_id');
    }


    public function getCurrentWfLevelAttribute()
    {
        $check = workflow([['wf_module_group_id' => 35, 'resource_id' => $this->id, 'type' => 0]])->checkIfHasWorkflow();
        $return = 0;
        if ($check)
        {
            $workflow = new Workflow(['wf_module_id' => 79, 'resource_id' => $this->id]);
            $return = $workflow->currentLevel(); 
        }
        return (int)$return;
    }   

    public function ApiStakeholder()
    {
        return $this->belongsTo(\App\Models\Operation\Claim\HspBilling\ApiStakeholder::class, 'stakeholder_id');
    }

    
}
