<?php

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\NotificationWorkflowAttribute;
use App\Models\Operation\Claim\Traits\Relationship\NotificationWorkflowRelationship;
use Illuminate\Database\Eloquent\Model;

class NotificationWorkflow extends Model
{
    use NotificationWorkflowRelationship, NotificationWorkflowAttribute;

    protected $guarded = [];
    public $timestamps = true;

}
