<?php

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Relationship\NotificationLetterRelationship;
use Illuminate\Database\Eloquent\Model;

class NotificationLetter extends Model
{
    use NotificationLetterRelationship;

    protected $guarded = [];
    public $timestamps = true;
}
