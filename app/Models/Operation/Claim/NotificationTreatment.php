<?php

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\NotificationTreatmentAttribute;
use App\Models\Operation\Claim\Traits\Relationship\NotificationTreatmentRelationship;
use Illuminate\Database\Eloquent\Model;

class NotificationTreatment extends Model
{
    use NotificationTreatmentAttribute, NotificationTreatmentRelationship;

    protected $guarded = [];
    public $timestamps = true;
}
