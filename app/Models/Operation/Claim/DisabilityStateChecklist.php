<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Relationship\DisabilityStateChecklistRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisabilityStateChecklist extends Model
{
    //
    use DisabilityStateChecklistRelationship,SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;



}