<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class AccidentType extends Model
{
    //
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    protected $guarded = [];
    public $timestamps = true;

}