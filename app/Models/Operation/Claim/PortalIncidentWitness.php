<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class PortalIncidentWitness extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'incident_witnesses';
}
