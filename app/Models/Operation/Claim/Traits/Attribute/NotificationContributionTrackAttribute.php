<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use App\Models\Auth\User;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Sysdef\Unit;
use Carbon\Carbon;


trait NotificationContributionTrackAttribute{

    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }
    public function getForwardDateFormattedAttribute() {
        return  Carbon::parse($this->forward_date)->format('d-M-Y');
    }
}