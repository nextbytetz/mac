<?php

namespace App\Models\Operation\Claim\Traits\Attribute;


trait ManualNotificationReportAttribute
{




// Get status label
    public function getStatusLabelAttribute() {
        if($this->issynced == 1)
        {
            return  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Synchronized' . "'>" . 'Synchronized' . "</span>";
        }else{
            return  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending -' . $this->pendingStatus() . "'>" . 'Pending'. "</span>" . ' - ' .$this->pendingStatus();
        }
    }

    /*Manual Status Label*/
    public function getStageStatusLabelAttribute()
    {
        return "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . $this->status->name. "'>" . $this->status->name . "</span>";
    }

    public function pendingStatus()
    {
        $status = null;
        if($this->ismembersynced == 0){
            $status = 'Member not synced';
        }else{
            if($this->incident_type_id != 3) {
                if ($this->isPensionerUploaded() == false) {
                    $status = 'Pensioner / Employee not uploaded';
                }
            }else{
                $status = 'Survivor not uploaded / synced';
            }
        }
        $status = (!isset($status)) ? 'Complete registration' : $status;
        return $status;
    }
    /**
     * @return string
     */
    public function getEditButtonAttribute() {
        if($this->issynced == 0){
            return '<a href="' . route('backend.system.code.edit_value', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
        }
    }

    public function getSyncMemberButtonAttribute() {
        if($this->ismembersynced == 0){
            return '<a href="' . route('backend.claim.manual_notification.sync_member_page', $this->id) . '" class="btn btn-xs btn-primary save_button" target="" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Sync Member </a> ';
        }
    }


    public function getCompleteButtonAttribute() {
        if($this->incident_type_id != 3){
            if($this->isReadyForCompletion()){
                return '<a href="' . route('backend.claim.manual_notification.complete_registration_page', $this->id) . '" class="btn btn-xs btn-success" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Complete' . '"></i> Complete</a> ';
            }
        }else{
            if($this->issynced == 0 && $this->isReadyForCompletion()){
                return '<a href="' . route('backend.claim.manual_notification.complete_registration_page', $this->id) . '" class="btn btn-xs btn-success" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Complete' . '"></i> Complete</a> ';
            }
        }

    }


    public function getActionButtonAttribute(){
        return $this->getCompleteButtonAttribute();
    }


    public function getRemoveButtonAttribute(){
        if($this->issynced == 0){
            return '<br/>  <a href="' . route('backend.claim.manual_notification.remove', $this->id) . '" class="btn btn-xs btn-danger" ><i class="icon fa fa-close" data-toggle="tooltip" data-placement="top" title="' . 'Remove' . '"></i> Remove</a> ';
        }
    }

    /**
     * Check if is ready for completing registration
     */
    public function isReadyForCompletion()
    {
        $return = 1;
        if($this->ismembersynced == 0){
            $return = 0;
        }

        if($this->incident_type_id != 3) {
            if (!$this->isPensionerUploaded()) {
                $return = 0;
            }

            if ($this->isPensionerSynced()) {
                $return = 0;
            }
        }
//        if(!$this->isSurvivorSynced() &&  $this->incident_type_id == 3){
//            $return = 0;
//        }
        return ($return == 0) ? false : true;

    }


    public function isPensionerUploaded(){
        $count = 0;
        if($this->incident_type_id != 3){
            /*for non death check if pensioner is uploaded*/
            $count = $this->manualPayrollMembers($this->incident_type_id)->count();
            return ($count > 0) ? true : false;
        }
        return true;
    }

    public function isPensionerSynced(){
        $count = 0;
        if($this->incident_type_id != 3){
            /*for non death check if pensioner is uploaded*/
            $count_exist = $this->manualPayrollMembers($this->incident_type_id)->count();
            $count_not_synced = $this->manualPayrollMembers($this->incident_type_id)->where('issynced', 0)->count();
            return ($count_not_synced > 0 || $count_exist == 0) ? false : true;
        }
        return true;
    }

    public function isSurvivorSynced(){
        $count = 0;
        if($this->incident_type_id == 3){
            /*for death check if survivor is uploaded and synced*/
            $count_exist = $this->manualPayrollMembers($this->incident_type_id)->count();
            $count_not_synced = $this->manualPayrollMembers($this->incident_type_id)->where('issynced', 0)->count();
            return ($count_not_synced > 0 || $count_exist == 0) ? false : true;
        }
        return true;
    }



    public function getFilenameAttribute()
    {
        $prefix = '';
        if($this->incident_type_id == 1)
        {
            $prefix = 'OAC';
        }elseif($this->incident_type_id == 2){
            $prefix = 'ODS';
        }elseif($this->incident_type_id == 3){
            $prefix = 'ODT';
        }

        return $prefix . '/' . $this->case_no;
    }

    public function getFileSubjectSpecificAttribute()
    {
        $subject = "";
        $id = "";
        $name = $this->employee_name;
        $id =  "AB1/457/" . $this->case_no;
        switch ($this->incident_type_id) {
            case 1:
                //Accident
                $id =  "AB1/457/" . $this->case_no;
                break;
            case 2:
                //Disease
                $id =  "AB1/457/" . $this->case_no;
                break;
            case 3:
                //Death
                $id =  "AB1/457/" . $this->case_no;
                break;
        }
        $id =  "AB1/457/" . $this->case_no . "/" . $this->incident_type;
        $subject = $name . "/" . $id;
        return $subject;
    }

    /*File name specific label for sending to E-office */
    public function getFileNameSpecificLabelAttribute()
    {
        $fileName = "";
        $employeeno = $this->employee->memberno;
        switch ($this->incident_type_id) {
            case 1:
                //Accident
                $fileName = "OAC/" . $employeeno . "/" . $this->case_no;
                break;
            case 2:
                //Disease
                $fileName = "ODS/" . $employeeno . "/" . $this->case_no;
                break;
            case 3:
                //Death
                $fileName = "ODT/" . $employeeno . "/" . $this->case_no;
                break;
        }
        return $fileName;
    }
}