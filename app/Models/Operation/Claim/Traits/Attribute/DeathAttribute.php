<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class DiseaseAttribute
 */
trait DeathAttribute{
    /**
     * @return string
     */
    public function getDeathDateFormattedAttribute() {
        return  Carbon::parse($this->death_date)->format('d-M-Y');
    }

    /**
     * @return string
     */
    public function getReportingDateFormattedAttribute() {
        return  Carbon::parse($this->reporting_date)->format('d-M-Y');
    }

    /**
     * @return string
     */
    public function getReceiptDateFormattedAttribute() {
        return  Carbon::parse($this->receipt_date)->format('d-M-Y');
    }

    /**
     * @return string
     */
    public function getIncidentDateFormattedAttribute() {
        return  Carbon::parse($this->incident_date)->format('d-M-Y');
    }

    /**
     * @return string
     */
    public function getConfirmDateFormattedAttribute() {
        $return = "-";
        if (!is_null($this->confirm_date)) {
            $return = Carbon::parse($this->confirm_date)->format('d-M-Y');
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getIncidentDateDescriptionAttribute() {
        $return = "";
        switch ($this->death_cause_id) {
            case 1:
                //Occupational accident
                $return = "Date of Accident";
                break;
            case 2:
                //Occupational disease
                $return = "Date of Diagnosis";
                break;
            default:
                $return = "Incident Date";
                break;
        }
        return $return;
    }

}