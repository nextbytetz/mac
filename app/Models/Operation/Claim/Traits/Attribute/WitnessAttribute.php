<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class trait WitnessAttribute{

 */
trait WitnessAttribute{

// Get from date formatted
    public function getFullnameAttribute() {
        return $this->name . " ( " . $this->phone . " )" ;
    }

}