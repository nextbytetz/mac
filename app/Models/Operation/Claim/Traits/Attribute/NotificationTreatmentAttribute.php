<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;

trait NotificationTreatmentAttribute
{
    public function getPractitionersListAttribute()
    {
        $return = "-";
        if ($this->practitioners()->count()) {
            //$return = "<ul>";
            $return = "";
            foreach ($this->practitioners as $practitioner) {
                $return .= "{$practitioner->fullname_with_external_id}<br/>";
            }
            //$return .= "</ul>";
        }
        return $return;
    }

    public function getInitialTreatmentDateFormattedAttribute() {
        return  Carbon::parse($this->initial_treatment_date)->format('d-M-Y');
    }

    public function getLastTreatmentDateFormattedAttribute() {
        $return = "-";
        if (!is_null($this->last_treatment_date)) {
            $return = Carbon::parse($this->last_treatment_date)->format('d-M-Y');
        }
        return $return;
    }

    public function getHasEdStatusAttribute()
    {
        if ($this->has_ed) {
            //has excuse from duty
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            //does not have excuse from duty
            $return = "<span class='tag square-tag light-red-color'>" . trans('labels.general.no') . "</span>";
        }
        return $return;
    }

    public function getHasHospitalizationStatusAttribute()
    {
        if ($this->has_hospitalization) {
            //has hospitalization
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            //does not have hospitalization
            $return = "<span class='tag square-tag light-red-color'>" . trans('labels.general.no') . "</span>";
        }
        return $return;
    }

    public function getHasLdStatusAttribute()
    {
        if ($this->has_ld) {
            //has light duties
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            //does not have light duties
            $return = "<span class='tag square-tag light-red-color'>" . trans('labels.general.no') . "</span>";
        }
        return $return;
    }

}