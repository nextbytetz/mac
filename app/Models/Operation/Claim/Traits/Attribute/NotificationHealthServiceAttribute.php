<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class trait NofiticationDisabilityStateAttribute{

 */
trait NotificationHealthServiceAttribute{



// Get from date formatted
    public function getFromDateFormattedAttribute()
    {
        $return = "-";
        if (!is_null($this->from_date)) {
            $return  = Carbon::parse($this->from_date)->format('d-M-Y');
        }
        return $return;
    }

// Get to date formatted
    public function getToDateFormattedAttribute()
    {
        $return = "-";
        if (!is_null($this->to_date)) {
            $return  = Carbon::parse($this->to_date)->format('d-M-Y');
        }
        return $return;
    }

    public function getAmountFormattedAttribute()
    {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }
}