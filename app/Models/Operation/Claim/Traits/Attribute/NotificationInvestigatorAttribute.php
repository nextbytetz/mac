<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;

trait NotificationInvestigatorAttribute
{
    /**
     * @return string
     */
    public function getTypeFormattedAttribute()
    {
        $return = "";
        switch ($this->type) {
            case 1:
                //Phone call investigation...
                $return = "Phone Call Investigation";
                break;
            case 2:
                //Physical Investigation...
                $return = "Physical Investigation";
                break;
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getUpdatedAtFormattedAttribute() {
        $return = "-";
        if (!is_null($this->updated_at)) {
            $return =  Carbon::parse($this->updated_at)->format('d-M-Y');
        }
        return $return;
    }

}