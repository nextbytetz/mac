<?php

namespace App\Models\Operation\Claim\Traits\Attribute;


use App\Services\Scopes\IsApprovedScope;
use Illuminate\Support\Facades\DB;

trait DocumentAttribute
{
    public function link($notification_report_id)
    {
        $notification = $this->notificationReports()->where("notification_reports.id", $notification_report_id)->first();
        $uploaded = $notification->pivot;
        return trim(preg_replace('/\s+/', '', notification_dir() . DIRECTORY_SEPARATOR . $notification_report_id . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR. $uploaded->id . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".")));
    }

    public function linkName($notification_report_id)
    {
        $uploaded = $this->notificationReports()->where("notification_reports.id", $notification_report_id)->first()->pivot;
        return $uploaded->name;
    }



// General Links
    public function linkEmployer($employer_id)
    {
        $employer = $this->employers()->where("employers.id", $employer_id)->withoutGlobalScopes([IsApprovedScope::class])->first();
        $uploaded = $employer->pivot;
        return trim(preg_replace('/\s+/', '', employer_registration_dir() . DIRECTORY_SEPARATOR . $employer_id . DIRECTORY_SEPARATOR . $this->id  . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".")));
    }

    public function linkEmployerVerification($id)
    {
        $verification = $this->onlineEmployerVerifications()->where("online_employer_verifications.id", $id)->first();
        $uploaded = $verification->pivot;
        return trim(preg_replace('/\s+/', '', employer_verification_dir() . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $this->id  . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".")));
    }

    /**
     * @param $id
     * @param null $docid
     * @return string
     * @description link to online notification applications
     */
    public function linkIncident($id, $docid = NULL)
    {
        if (is_null($docid)) {
            $docid = $this->id;
            $incident = $this->onlineIncidents()->where(pg_mac_portal() . ".incidents.id", $id)->first();
            $uploaded = $incident->pivot;
            //$incident = $this->onlineIncidents()->where(pg_mac_portal() . ".incidents.id", $id)->first();
            //$uploaded = $incident->pivot;
        } else {
            $uploaded = DB::table(pg_mac_portal() . ".document_incident")->where('id', $docid)->first();
        }
        return trim(preg_replace('/\s+/', '', incident_dir() . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $docid  . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".")));
    }

    public function linkEmployerAdvancePayment($id)
    {
        $advance_payment = $this->onlineEmployerAdvancePayments()->where("employer_advance_payments.id", $id)->first();
        $uploaded = $advance_payment->pivot;
        return trim(preg_replace('/\s+/', '', employer_advance_payment_dir() . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $this->id  . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".")));
    }

    public function linkNameEmployer($employer_id)
    {
        $uploaded = $this->employers()->where("employers.id", $employer_id)->withoutGlobalScopes([IsApprovedScope::class])->first()->pivot;
        return $uploaded->name;
    }

    public function linkNameEmployerVerification($id)
    {
        $uploaded = $this->onlineEmployerVerifications()->where("online_employer_verifications.id", $id)->first()->pivot;
        return $uploaded->name;
    }

    public function linkNameEmployerAdvancePayment($id)
    {
        $uploaded = $this->onlineEmployerAdvancePayments()->where("employer_advance_payments.id", $id)->first()->pivot;
        return $uploaded->name;
    }

    public function linkNameIncident($id)
    {
        $uploaded = $this->onlineIncidents()->where(pg_mac_portal() . ".incidents.id", $id)->first()->pivot;
        return $uploaded->name;
    }

}