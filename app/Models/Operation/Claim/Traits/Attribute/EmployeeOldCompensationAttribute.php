<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class EmployeeOldCompensationAttribute
 */
trait EmployeeOldCompensationAttribute{

    /*
        * Formatting tables column
        */
    public function getPaymentDateFormattedAttribute() {
        return  Carbon::parse($this->payment_date)->format('d-M-Y');
    }

    public function getAmountFormattedAttribute() {
               return number_format( $this->amount , 2 , '.' , ',' );
    }



    //          ACTION COLUMNS ADDED TO DATATABLE
    public function getActionButtonsAttribute() {
        return $this->getDeleteButtonAttribute();
    }

    public function getDeleteButtonAttribute() {
//        return '<a href="' . route('backend.compliance.employee.delete_old_compensation', $this->id) . '" class="btn btn-xs btn-primary delete_button" ><i class="icon fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';

        return '<a href="' . route('backend.compliance.employee.delete_old_compensation', $this->id) . '" class="btn btn-xs btn-danger" data-trans-button-cancel="'. trans('buttons.general.cancel') .'" data-trans-button-confirm="'. trans('buttons.general.confirm') .'" data-trans-title="'. trans('labels.general.warning') .'" data-trans-text="'. trans('strings.backend.general.delete_message') .'" data-method="delete"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
    }

}