<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class DiseaseAttribute
 */
trait DiseaseAttribute{


    public function getDiagnosisDateFormattedAttribute() {
        return  Carbon::parse($this->diagnosis_date)->format('d-M-Y');
    }

    public function getReportingDateFormattedAttribute() {
        return  Carbon::parse($this->reporting_date)->format('d-M-Y');
    }

    public function getReceiptDateFormattedAttribute() {
        return  Carbon::parse($this->receipt_date)->format('d-M-Y');
    }

    public function getReturnToWorkFormattedAttribute()
    {
        if ($this->return_to_work) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
        }
    }

    public function getSalaryBeingContinuedStatusAttribute()
    {
        if ($this->salary_being_continued) {
            //needs physical investigation
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            //needs phone call investigation
            $return = "<span class='tag square-tag light-red-color'>" . trans('labels.general.no') . "</span>";
        }
        return $return;
    }

    public function getSalaryFullContinuedStatusAttribute()
    {
        if ($this->salary_full_continued) {
            //needs physical investigation
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            //needs phone call investigation
            $return = "<span class='tag square-tag light-red-color'>" . trans('labels.general.no') . "</span>";
        }
        return $return;
    }

    public function getLostBodyPartStatusAttribute()
    {
        if ($this->lost_body_part) {
            //needs physical investigation
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            //needs phone call investigation
            $return = "<span class='tag square-tag light-red-color'>" . trans('labels.general.no') . "</span>";
        }
        return $return;
    }

    public function getReturnToWorkDateFormattedAttribute() {
        $return = "-";
        if (!is_null($this->return_to_work_date)) {
            $return = Carbon::parse($this->return_to_work_date)->format('d-M-Y');
        }
        return $return;
    }

}