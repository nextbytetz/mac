<?php

namespace App\Models\Operation\Claim\Traits\Attribute;


/**
 * Class ClaimCompensationAttribute
 */
trait ClaimCompensationAttribute{





    public function getAmountFormattedAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }

    /**
     * @return mixed
     * Get Name of compensated entity
     * @deprecated
     */

    public function getCompensatedEntityNameAttribute(){
        //employer
        if ($this->getMemberType() == 1){
            return $this->employer->name;
        } elseif ($this->getMemberType() == 3){
            return $this->insurance->name;
        }elseif($this->getMemberType() == 2){
            return $this->employee->name;
        }elseif($this->getMemberType() == 4){
            return $this->dependent->name;
        }
    }


    public function getMemberType(){
        return $this->member_type_id;
    }




}