<?php
/**
 * Created by PhpStorm.
 * User: gwanchi
 * Date: 12/11/18
 * Time: 12:02 PM
 */

namespace App\Models\Operation\Claim\Traits\Attribute;


trait NotificationWorkflowAttribute
{

    public function getResourceNameAttribute()
    {
        return $this->notificationReport->resource_name;
    }

}