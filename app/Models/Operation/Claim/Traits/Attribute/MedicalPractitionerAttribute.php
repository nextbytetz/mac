<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

/**
 * Class MedicalPractitionerAttribute
 */
trait MedicalPractitionerAttribute
{


// Get Name with external_id
    public function getFullnameWithExternalIdAttribute()
    {
        return $this->firstname . ' ' . $this->middlename . ' ' . $this->lastname . '-' . $this->external_id;
    }


// Get name
    public function getFullnameAttribute()
    {
        return $this->firstname . ' ' . $this->middlename . ' ' . $this->lastname;
    }
}