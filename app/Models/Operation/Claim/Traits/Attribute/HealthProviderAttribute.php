<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

/**
 * Class HealthProviderAttribute
 */
trait HealthProviderAttribute
{


// Get Full location (District - Ward - Street)
    public function getLocationAttribute()
    {
        return $this->region . ', ' . $this->district . ', ' . $this->ward . ', ' . $this->street;
    }



    public function getFullnameInfoAttribute()
    {
        return $this->external_id . ': ' . $this->name . ' (  '. $this->common_name .' ) ' . 'of ' . $this->getLocationAttribute();
    }

}