<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class ClaimAttribute
 */
trait ClaimAttribute{

    /*
        * Formatting tables column
        */
    public function getPdFormattedAttribute() {
        return  ($this->pd) ? $this->pd : 0;
    }

    public function getContribMonthFormattedAttribute() {
        return  Carbon::parse($this->contrib_month)->format('M-Y');
    }

    public function getNeedCcaLabelAttribute()
    {
        if (!$this->need_cca) {
            return "<span class='tag tag-info white_color'> No </span>";
        } else {
            return "<span class='tag tag-success white_color'> Yes </span>";
        }
    }

    public function getNeedRehabilitationLabelAttribute()
    {
        if (!$this->need_rehabilitation) {
            return "<span class='tag tag-info white_color'> No </span>";
        } else {
            return "<span class='tag tag-success white_color'> Yes </span>";
        }
    }

}