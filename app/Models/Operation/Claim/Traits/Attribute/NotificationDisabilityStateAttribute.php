<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class trait NofiticationDisabilityStateAttribute{

 */
trait NotificationDisabilityStateAttribute{



    // Get from date formatted
    public function getFromDateFormattedAttribute() {
        if ($this->from_date) {
            $return =  Carbon::parse($this->from_date)->format('d-M-Y');
        } else {
            $return = "-";
        }
        return $return;
    }

// Get to date formatted
    public function getToDateFormattedAttribute() {
        if ($this->to_date) {
            $return =  Carbon::parse($this->to_date)->format('d-M-Y');
        } else {
            $return = "-";
        }
        return $return;
    }



}