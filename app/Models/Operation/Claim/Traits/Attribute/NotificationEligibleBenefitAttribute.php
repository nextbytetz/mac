<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use Carbon\Carbon;

trait NotificationEligibleBenefitAttribute
{
    public function getProcessedLabelAttribute()
    {
        $return = "";
        switch ($this->processed) {
            case 0:
                //Pending
                $return = "<span class='tag square-tag soft-orange-color' data-toggle='tooltip' data-html='true' title='Pending Approval'>Pending Approval</span>";
                break;
            case 1:
                //Processed
                $return = "<span class='tag square-tag dodger-blue-color' data-toggle='tooltip' data-html='true' title='Approved'>Approved</span>";
                break;
            case 2:
                //Declined
                $return = "<span class='tag square-tag light-red-color' data-toggle='tooltip' data-html='true' title='Declined'>Declined</span>";
                break;
            case 3:
                //Cancelled by User
                $return = "<span class='tag square-tag light-red-color' data-toggle='tooltip' data-html='true' title='Declined'>Cancelled by User</span>";
                break;
        }
        return $return;
    }

    public function getLetterStatusLabelAttribute()
    {
        $return = "";
        switch (true) {
            case ($this->iszeropaid):
            case ($this->ispayprocessed):
                $letterQuery = $this->letter();
                $print_status = "<span class='tag tag-warning white_color'>Not Printed</span>";
                if ($letterQuery->count()) {
                   if ($letterQuery->first()->isprinted) {
                       $print_status = "<span class='tag tag-success white_color'>Printed</span>";
                   }
                }
                $return = "&nbsp; Award Letter Status &nbsp;:&nbsp;{$print_status}" ;
                break;
        }
        return $return;
    }

    public function getIsinitiatedLabelAttribute()
    {
        $return = "";
        switch ($this->isinitiated) {
            case 0:
                //Not initiated
                $return = "<span class='tag square-tag spice-color' data-toggle='tooltip' data-html='true' title='Not Initiated'>Not Initiated</span>";
                break;
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

}