<?php

namespace App\Models\Operation\Claim\Traits\Attribute;

use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use Carbon\Carbon;

/**
 * Class NotificationReportAttribute
 */
trait NotificationReportAttribute {

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        $return = "";
        switch ($this->status()) {
            case 0:
                //if pending
                $return = "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.pending') . "'>" . trans('labels.general.pending') . "</span>";
                break;
            case 1:
                //if approved
                $paystatus = "";
                if ($this->haspaidmanual) {
                    $paystatus = " (Paid Manually)";
                }
                $return = "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.approved') . "'>" . trans('labels.general.approved') . $paystatus . "</span>";
                break;
            case 2:
                //if rejected
                $return = "<span class='tag tag-danger'  data-toggle='tooltip' data-html='true' title='" . trans('labels.general.rejected') . "'>" . trans('labels.general.rejected') . "</span>";
                break;
            case 3:
                //if progressive notification
                $stage = "";
                if ($this->stage()->count()) {
                    $stage = $this->stage->name;
                }
                switch ($this->notification_staging_cv_id) {
                    default:
                        $return = "<span class='tag square-tag spice-color'  data-toggle='tooltip' data-html='true' title='" . $stage . "'>" . $stage . "</span>";
                        break;
                }
                break;
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getNextStageAttribute()
    {
        $return = "";
        if ($this->stages()->count()) {
            $pivot = $this->stages()->first()->pivot;
            if ($pivot->require_attend And !$pivot->attended) {
                $comments = json_decode($pivot->comments);
                $nextStage = $comments->{'nextStage'};
                $return = "<span style=\"font-size: medium;\"><b>&#x27A5;&nbsp;</b>&nbsp;nextStage:&nbsp;<span class='underline'>{$nextStage}</span></span>";
            }
        }
        return $return;
    }

    public function getCanModifyNotificationAttribute()
    {
        $return = false;
        $codeValueRepo = new CodeValueRepository();
        switch ($this->notification_staging_cv_id) {
            case $codeValueRepo->NSNREJSYBS():
            case $codeValueRepo->NSOPRFLI():
            case $codeValueRepo->NSOPFRFLI():
            case $codeValueRepo->NSPPRMC():
            case $codeValueRepo->ONSPPRMCW():
            case $codeValueRepo->NSPCRFC():
            case $codeValueRepo->NSPCRFI():
            case $codeValueRepo->NSRINC():
            case $codeValueRepo->NSPCRFNFI():
            case $codeValueRepo->NSNREJSYOT():
            case $codeValueRepo->NSPPRUM():
                //Notification Rejection by System (Before the WCF Statutory Period)
                //On Progress (Re-Filling Investigation)
                //On Progress (Finished Re-Filling Investigation)
                //Pending Partial Registration (Missing Contribution)
                //On Pending Partial Registration (Missing Contribution) Workflow
                //Pending Complete Registration (Filling Checklist)
                //Pending Complete Registration (Filling Investigation)
                //Rejected Incident from Notification Approval
                //Pending Complete Registration (Finished Filling Investigation)
                //Notification Rejection by System (Out of Time)
                //Pending Partial Registration (Unregistered Member)
                $return = true;
                break;
        }
        /*if ($this->wf_done) {
            $return = false;
        }*/
        return $return;
    }

    /**
     * @return string
     */
    public function getModifyNotificationButtonAttribute()
    {
        $return = "";
        if ($this->getCanModifyNotificationAttribute()) {
            $return = "<span>" .
                link_to_route('backend.claim.notification_report.modify_progressive', "<i class=\"icon fa fa-pencil-square-o\"></i>&nbsp;" . __('buttons.backend.claim.modify_notification'), [$this->id], ['class' => 'btn btn-primary site-btn nav_button']) .
                "</span>";
        }
        return $return;
    }

    public function getOnlineApplicationProfileButtonAttribute()
    {
        $return = "";
        $return = "<span>" .
            link_to_route('backend.compliance.employer.incident_profile', "<i class=\"icon icon fa fa-cloud\"></i>&nbsp;" . "Online Application Profile", [$this->incident_id], ['class' => 'btn btn-primary site-btn nav_button']) .
            "</span>";
        return $return;
    }

    /**
     * @return bool
     */
    public function getCanReInvestigateAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->NSRINC());
    }

    /**
     * @return bool
     */
    public function getCanAddContributionAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->ONSPPRMCW());
    }

    /**
     * @return bool
     */
    public function getCanEditContributionAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->ONSPPRICW());
    }

    /**
     * @return bool
     */
    public function getCanEditEmployeeAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->ONSPPRIEIFW());
    }

    /**
     * @return bool
     */
    public function getCanRegisterEmployeeAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->ONSPPRUMW());
    }

    /**
     * @return bool
     */
    public function getCanCloseIncidentAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->NSOPALP());
    }

    /**
     * @return bool
     */
    public function getCanStartReassessmentAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->NSCINC());
    }

    public function getCanFillInvestigationAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->NSPCRFI() Or $this->notification_staging_cv_id == $codeValueRepo->NSOPRFLI());
    }

    /**
     * @return bool
     */
    public function getCanEditInvestigationAttribute()
    {
        return ($this->progressive_stage < 4);
    }

    /**
     * @return bool
     */
    public function getCanCompleteChecklistAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->NSPCRFC() And $this->progressive_stage == 1 And ($this->has_checklist == 1 Or sysdefs()->data()->should_skip_notification_checklist));
    }

    /**
     * @return bool
     */
    public function getCanCreateBenefitAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        //return ($this->notification_staging_cv_id == $codeValueRepo->NSOPIA() Or $this->notification_staging_cv_id == $codeValueRepo->NSOPALP());

        //return ( ($this->notification_staging_cv_id != $codeValueRepo->ONSPPRICW()) && ($this->stage->code_id == 13) && ($this->progressive_stage >= 4) && ($this->td()->count() Or $this->pd_ready Or $this->survivor_ready Or $this->mae()->count()));
        return ( ($this->stage->code_id == 13) && ($this->progressive_stage >= 4) && ($this->td()->count() Or $this->pd_ready Or $this->survivor_ready Or $this->mae()->count()));
    }

    /**
     * @return bool
     */
    public function getCanIssueRejectionLetterAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        return ($this->notification_staging_cv_id == $codeValueRepo->NSCSYSREJ() Or $this->notification_staging_cv_id == $codeValueRepo->NSCAPPRREJ());
    }

    /**
     * @return string
     */
    public function getInitializeRejectionFromApprovalWorkflowAttribute()
    {
        $return = "";
        $codeValueRepo = new CodeValueRepository();
        switch ($this->notification_staging_cv_id) {
            case $codeValueRepo->NSRINC():
                //Rejected Incident from Notification Approval
                return "<span>" .
                    "Rejection from Notification Approval Workflow" .
                    "</span>";
                break;
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getInitializeIncorrectEmployeeInfoWorkflowAttribute()
    {
        //For IncorrectEmployeeInfo Workflow
        //$return = "";
        $return = false;
        $codeValue = new CodeValueRepository();
/*        switch ($this->notification_staging_cv_id) {
            case $codeValue->NSOPRFLI():
            case $codeValue->NSOPFRFLI():
            case $codeValue->NSPCRFC():
            case $codeValue->NSPCRFI():
            case $codeValue->NSRINC():
            case $codeValue->NSPCRFNFI():
            case $codeValue->NSPPRMC():
                //On Progress (Re-Filling Investigation)
                //On Progress (Finished Re-Filling Investigation)
                //Pending Complete Registration (Filling Checklist)
                //Pending Complete Registration (Filling Investigation)
                //Rejected Incident from Notification Approval
                //Pending Complete Registration (Finished Filling Investigation)
                //Pending Partial Registration (Missing Contribution)
                //$return = $this->getInitializeIncorrectEmployeeInfoButton();
                $return = true;
                break;
        }*/
        $return = true;
        return $return;
    }

    /**
     * @return string
     */
    public function getInitializeInterruptingWorkflowAttribute()
    {
        return ($this->getInitializeIncorrectContributionWorkflowAttribute() || $this->getInitializeIncorrectEmployeeInfoWorkflowAttribute());
    }

    /**
     * @return bool
     */
    public function getInitializeIncorrectContributionWorkflowAttribute()
    {
        //For IncorrectContribution & IncorrectEmployeeInfo Workflow
        //$return = "";
        $return = false;
        $codeValue = new CodeValueRepository();
/*        switch ($this->notification_staging_cv_id) {
            case $codeValue->NSOPRFLI():
            case $codeValue->NSOPFRFLI():
            case $codeValue->NSPCRFC():
            case $codeValue->NSPCRFI():
            case $codeValue->NSRINC():
            case $codeValue->NSPCRFNFI():
                //Both
                //On Progress (Re-Filling Investigation)
                //On Progress (Finished Re-Filling Investigation)
                //Pending Complete Registration (Filling Checklist)
                //Pending Complete Registration (Filling Investigation)
                //Rejected Incident from Notification Approval
                //Pending Complete Registration (Finished Filling Investigation)
                //$return = $this->getInitializeIncorrectContributionLinkAttribute();
                $return = true;
                break;
        }*/
        $return = true;
        return $return;
    }

    /**
     * @return string
     */
    public function getInitializeIncorrectContributionLinkAttribute()
    {
        $wfModule = new WfModuleRepository();
        $group = 3;
        $codeValue = new CodeValueRepository();
        if ($this->progressive_stage >= 4 && $this->notification_staging_cv_id == $codeValue->NSOCBW()) { //Benefit Process Ready ...
            $type = $wfModule->incorrectContributionExecutiveNotificationType()["type"];
        } else {
            $type = $wfModule->incorrectContributionNotificationType()["type"];
        }

        $route = route("backend.claim.notification_report.initiate_workflow");
        return "<span>" . link_to('#', "<i class=\"icon fa fa-money\" aria-hidden=\"true\"></i>&nbsp;Initiate Incorrect Contribution Workflow", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Incorrect Contribution Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
    }

    public function getShouldTransferDeathAttribute()
    {
        return ($this->progressive_stage >= 4 && in_array($this->incident_type_id, [1,2]));
    }

    public function getInitializeTransferToDeathLinkAttribute()
    {
        $return = "";
        if ($this->getShouldTransferDeathAttribute()) {
            $wfModule = new WfModuleRepository();
            $group = 3;
            $type = $wfModule->transferToDeathIncidentApproval()["type"];
            $route = route("backend.claim.notification_report.initiate_workflow");
            $return = "<span>" . link_to('#', "<i class=\"icon fa fa-stop\" aria-hidden=\"true\"></i>&nbsp;Initiate Approve Notification Transfer to Death Workflow", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Approve Notification Transfer to Death Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getInitializeIncorrectEmployeeInfoLinkAttribute()
    {
        //can be disabled as all employee information are filled during notification checklist
        $wfModule = new WfModuleRepository();
        $type = $wfModule->incorrectEmployeeInfoNotificationType()["type"];
        $group = 3;
        $route = route("backend.claim.notification_report.initiate_workflow");
        return "<span>" . link_to('#', "<i class=\"icon fa fa-info\" aria-hidden=\"true\"></i>&nbsp;Initiate Incorrect Employee Info Workflow", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Incorrect Employee Info Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
    }

    /**
     * @return string
     */
    public function getInitializeVoidNotificationLinkAttribute()
    {
        $wfModule = new WfModuleRepository();
        $type = $wfModule->voidNotificationType()["type"];
        $group = 3;
        $route = route("backend.claim.notification_report.initiate_workflow");
        return "<span>" . link_to('#', "<i class=\"icon fa fa-bomb\" aria-hidden=\"true\"></i>&nbsp;Void Notification", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Void Notification Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
    }

    /**
     * @return string
     */
    public function getInitializeUndoNotificationRejectionLinkAttribute()
    {
        $codeValueRepo = new CodeValueRepository();
        switch (true) {
            //case ($this->notification_staging_cv_id == $codeValueRepo->NSCSYSREJ()):
            //case ($this->notification_staging_cv_id == $codeValueRepo->NSCAPPRREJ()):
            //case ($this->progressive_stage >= 3 And $this->stage->code_id == 23):
            case ($this->stage->code_id == 23):
                //Closed Rejected Incident By System
                //Closed Rejected Incident from Notification Approval
                $wfModule = new WfModuleRepository();
                $type = $wfModule->undoNotificationRejectionType()["type"];
                $group = 4;
                $route = route("backend.claim.notification_report.initiate_workflow");
                $return = "<span>" . link_to('#', "<i class=\"icon fa fa-undo\" aria-hidden=\"true\"></i>&nbsp;Undo Rejection", ['class' => 'dropdown-item initiate_workflow', 'data-description' => 'Initiate Undo Rejection Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) . "</span>";
                break;
            default:
                $return = "";
        }
        return $return;
    }

    /**
     * @return string
     * @description Initialize Enforceable Workflow
     */
    public function getInitializeWorkflowAttribute()
    {
        $return = "";
        $codeValue = new CodeValueRepository();
        $wfModule = new WfModuleRepository();
        $route = route("backend.claim.notification_report.initiate_workflow");
        switch ($this->status()) {
            case 3:
                //progressive notification
                switch ($this->notification_staging_cv_id) {
                    case $codeValue->NSNREJSYBS():
                    case $codeValue->NSNREJSYOT():
                        //Notification Rejection by System (Before WCF Statutory Period) | System Rejected Notification Workflow
                        //Notification Rejection by System (Out of Time, more than 12 months) | System Rejected Notification Workflow
                        //Initiate System Rejected Notification workflow
                        $type = $wfModule->systemRejectedNotificationType()["type"];
                        $group = 4;
                        $return = "<span>" .
                            link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate System Rejected Notification Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate System Rejected Notification Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                                    "</span>";
                        break;
                    case $codeValue->NSPPRUM():
                        //Pending Partial Registration (Unregistered Member) | Unregistered Member Notification Workflow
                        //Initiate Unregistered Member Notification workflow
                        $type = $wfModule->unregisteredMemberNotificationType()["type"];
                        $group = 3;
                        $return = "<span>" .
                            link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Unregistered Member Notification Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Unregistered Member Notification Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                            "</span>";
                        break;
                    case $codeValue->NSPPRMC():
                        //Pending Partial Registration (Missing Contribution) | Missing Contribution Notification
                        //Initiate Missing Contribution Notification workflow
                        $type = $wfModule->missingContributionNotificationType()["type"];
                        $group = 3;
                        $return = "<span>" .
                            link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Missing Contribution Notification Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Missing Contribution Notification Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                            "</span>";
                        break;
                    case $codeValue->NSOPFRFLI():
                    case $codeValue->NSPCRFNFI():
                        //On Progress (Finished Re-Filling Investigation)
                        //Pending Complete Registration (Finished Filling Investigation)
                        //Initiate Notification Approval workflow
                        $type = $wfModule->notificationIncidentApproval()["type"];
                        $group = 3;
                        $return = "<span>" .
                            link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Notification Approval Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Notification Approval Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                            "</span>";
                        break;
                    /*case $codeValue->NSOPIA():
                    case $codeValue->NSOPALP():
                        //On Progress (Incident Approved)
                        //On Progress (At least Payed)
                        //Initiate Claim Benefits workflow
                        $type = $wfModule->claimBenefitType()["type"];
                        $group = 3;
                        $return = "<span>" .
                            link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Claim Benefits Workflow", ['class' => 'btn btn-primary site-btn nav_button', 'id' => "initiate_workflow", 'data-description' => 'Initiate Claim Benefits Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                            "</span>";
                        break;*/
                    case $codeValue->NSRINC():
                        //Rejected Incident from Notification Approval
                        //Initiate Rejection from Notification Approval Workflow
                        $type = $wfModule->rejectionNotificationApprovalType()["type"];
                        $group = 4;
                        $return = "<span>" .
                            link_to('#', "<i class=\"icon fa fa-inbox\"></i>&nbsp;Initiate Rejection from Notification Approval Workflow", ['class' => 'btn btn-primary site-btn nav_button initiate_workflow', 'data-description' => 'Initiate Rejection from Notification Approval Workflow', 'data-group' => $group, 'data-type' => $type, 'data-resource' => $this->id, 'data-route' => $route]) .
                            "</span>";
                        break;
                }
                break;
        }
        return $return;
    }

    public function getNameLabelAttribute()
    {
        return "Notification_#: " . $this->id;
    }

    //get status of acknowledgement
    public function getAcknowledgementStatusAttribute()
    {
        if ($this->isAcknowledged()) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";

        }else {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
        }
    }

    public function getContribOnStatusAttribute()
    {
        if ($this->contrib_on) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";

        } else {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
        }
    }

    public function getContribBeforeStatusAttribute()
    {
        if ($this->contrib_before) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";

        } else {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
        }
    }

    //get verification status
    public function getVerificationStatusAttribute() {
        if ($this->isVerified()) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        }else {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
        }
    }

    public function getAcknowledgementLetterStatusAttribute() {
        if ($this->is_acknowledgment_sent) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        }else {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
        }
    }

    //get investigation validity status
    public function getInvestigationValidityStatusAttribute() {
        if ($this->investigationValidity()) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
        }
    }

    public function getNeedInvestigationStatusAttribute()
    {
        if ($this->need_investigation) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
        }
    }

    /**
     * @return bool
     */
    public function getNeedUploadInvestigationImagesAttribute()
    {
        //$return = ($this->need_investigation And $this->progressive_stage >= 2 And ($this->incident_type_id == 1 Or $this->incident_type_id == 2));
        $return = ($this->progressive_stage >= 2 And ($this->incident_type_id == 1 Or $this->incident_type_id == 2));
        return $return;
    }

    public function getChecklistScoreStatusAttribute()
    {
        if ($this->checklist_score > 4) {
            //needs physical investigation
            $return = "<span class='tag tag-success white_color'>" . $this->checklist_score . "</span>";
        } else {
            //needs phone call investigation
            $return = "<span class='tag square-tag light-red-color'>" . $this->checklist_score . "</span>";
        }
        return $return;
    }

    public function getPaidMonthBeforeStatusAttribute()
    {
        if ($this->paid_month_before) {
            //needs physical investigation
            $return = "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>";
        } else {
            //needs phone call investigation
            $return = "<span class='tag square-tag light-red-color'>" . trans('labels.general.no') . "</span>";
        }
        return $return;
    }

    public function getBodyPartInjuryListAttribute()
    {
        $return = "-";
        if ($this->injuries()->count()) {
            $array = $this->injuries()->pluck("body_part_injuries.name")->all();
            $return = implode(", ", $array);
        }
        return $return;
    }

    public function getLostBodyPartsListAttribute()
    {
        $return = "-";
        if ($this->lostBodyParties()->count()) {
            $array = $this->lostBodyParties()->pluck("body_part_injuries.name")->all();
            $return = implode(", ", $array);
        }
        return $return;
    }

    public function getMonthlyEarningChecklistFormattedAttribute()
    {
        $return = "-";
        if (!is_null($this->monthly_earning_checklist)) {
            $return = number_2_format($this->monthly_earning_checklist);
        }
        return $return;
    }

    public function getMonthlyEarningFormattedAttribute()
    {
        $return = "-";
        if ($this->monthly_earning) {
            $return = number_2_format($this->monthly_earning);
        }
        return $return;
    }

    /**
     * Check status of notification if Pending, approval, rejected
     *
     * @return mixed
     */
    public function status()
    {
        return $this->status;
    }

//Check acknowledgement of notification if Pending, approval, rejected
    public function isAcknowledged(){
        return $this->is_acknowledgement_sent;
    }

    //Check verification status of notification if Pending, approval, rejected
    public function isVerified(){
        return $this->isverified;
    }

    //Check Investigation validation status of notification
    public function investigationValidity(){
        return $this->investigation_validity;
    }

    public function getResourceNameAttribute()
    {
        $return = $this->employee_name;
        if ($this->employee_id) {
            $return = $this->employee->name;
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getIncidentDateFormattedAttribute() {
        return  Carbon::parse($this->incident_date)->format('d-M-Y');
    }

    public function getReportingDateFormattedAttribute() {
        return  Carbon::parse($this->reporting_date)->format('d-M-Y');
    }


    public function getReceiptDateFormattedAttribute() {
        return  Carbon::parse($this->receipt_date)->format('d-M-Y');
    }

    public function getDatehiredFormattedAttribute()
    {
        $return = "-";
        if (!is_null($this->datehired)) {
            $return = Carbon::parse($this->datehired)->format('d-M-Y');
        }
        return $return;
    }

    public function getLastWorkDateFormattedAttribute()
    {
        $return = "-";
        if (!is_null($this->last_work_date)) {
            $return = Carbon::parse($this->last_work_date)->format('d-M-Y');
        }
        return $return;
    }

    public function pendingWfTrack()
    {
        if ($this->status == 1) {
            /*Approved*/
           $wf_track =  workflow([['wf_module_group_id' => 3, 'resource_id' => $this->id]])->currentWfTrack();
        } elseif ($this->status == 2) {
            /*Rejected*/
            $wf_track =   workflow([['wf_module_group_id' => 4, 'resource_id' => $this->id]])->currentWfTrack();
        }

        return $wf_track;
    }

    public function getFileNameLabelAttribute()
    {
        $fileName = "";
        $employeeno = $this->employee->memberno;
        switch ($this->incident_type_id) {
            case 1:
            case 4:
                //Accident
                $fileName = "OAC/" . $employeeno . "/" . $this->id;
                break;
            case 2:
            case 5:
                //Disease
                $fileName = "ODS/" . $employeeno . "/" . $this->id;
                break;
            case 3:
                //Death
                $fileName = "ODT/" . $employeeno . "/" . $this->id;
                break;
        }
        return $fileName;
    }

    public function getFileNameSpecificLabelAttribute()
    {
        $fileName = "";
        $employeeno = $this->employee->memberno;
        switch ($this->incident_type_id) {
            case 1:
            case 4:
                //Accident
                $fileName = "OAC/" . $employeeno . "/" . $this->accident->oacno;
                break;
            case 2:
            case 5:
                //Disease
                $fileName = "ODS/" . $employeeno . "/" . $this->disease->odsno;
                break;
            case 3:
                //Death
                $fileName = "ODT/" . $employeeno . "/" . $this->death->odtno;
                break;
        }
        return $fileName;
    }

    public function getFileNameLabelUnderscoreAttribute()
    {
        $fileName = "";
        $employeeno = $this->employee->memberno;
        switch ($this->incident_type_id) {
            case 1:
            case 4:
                //Accident
                $fileName = "OAC_" . $employeeno . "_" . $this->id;
                break;
            case 2:
            case 5:
                //Disease
                $fileName = "ODS_" . $employeeno . "_" . $this->id;
                break;
            case 3:
                //Death
                $fileName = "ODT_" . $employeeno . "_" . $this->id;
                break;
        }
        return $fileName;
    }

    public function getFileNameSpecificLabelUnderscoreAttribute()
    {
        $fileName = "";
        $employeeno = $this->employee->memberno;
        switch ($this->incident_type_id) {
            case 1:
            case 4:
                //Accident
                $fileName = "OAC_" . $employeeno . "_" . $this->accident->oacno;
                break;
            case 2:
            case 5:
                //Disease
                $fileName = "ODS_" . $employeeno . "_" . $this->disease->odsno;
                break;
            case 3:
                //Death
                $fileName = "ODT_" . $employeeno . "_" . $this->death->odtno;
                break;
        }
        return $fileName;
    }

    public function getFileSubjectAttribute()
    {
        $subject = "";
        $name = $this->employee->name;
        $subject = $name . "/" . $this->id;
        return $subject;
    }

    public function getFileSubjectSpecificAttribute()
    {
        $subject = "";
        $id = "";
        $name = $this->employee->name;
        switch ($this->incident_type_id) {
            case 1:
            case 4:
                //Accident
                $id =  "OAC/" . $this->accident->oacno;
                break;
            case 2:
            case 5:
                //Disease
                $id =  "ODS/" . $this->disease->odsno;
                break;
            case 3:
                //Death
                $id =  "ODT/" . $this->death->odtno;
                break;
        }
        $subject = $name . "/" . $id;
        return $subject;
    }

    public function getContribMonthFormattedAttribute() {
        $return = "-";
        if (!is_null($this->contrib_month)) {
            $return = Carbon::parse($this->contrib_month)->format('M-Y');
        }
        return  $return;
    }

    /**
     * @param null $id Id of the pivot table
     * @return array
     */
    public function dirInvestigationDocuments($id = NULL)
    {
        $docId = 52;
        $docArray = [];
        if (is_null($id)) {
            $documents = $this->investigationDocuments;
        } else {
            $documents = $this->investigationDocuments()->wherePivot('id', $id)->get();
        }

        foreach ($documents as $document) {
            $uploaded = $document->pivot;
            $docArray[] = [
                'source' => trim(preg_replace('/\s+/', '', investigation_dir() . DIRECTORY_SEPARATOR . $uploaded->id . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : "."))),
                'name' => $uploaded->name,
            ];
        }
        return $docArray;
    }

    /**
     * @param null $id Id of the pivot table
     * @return array
     */
    public function assetInvestigationDocuments($id = NULL)
    {
        $docId = 52;
        $docArray = [];
        if (is_null($id)) {
            $documents = $this->investigationDocuments;
        } else {
            $documents = $this->investigationDocuments()->wherePivot('id', $id)->get();
        }

        foreach ($documents as $document) {
            $uploaded = $document->pivot;
            $docArray[] = [
                'caption' => str_replace("." . $uploaded->ext, "", $uploaded->name),
                'size' => $uploaded->size,
                'url' => route("backend.claim.notification_report.delete_investigation_document", ['incident' => $this->id, 'id' => $uploaded->id ]),
                'key' => $uploaded->id,
                'source' => trim(preg_replace('/\s+/', '', investigation_path() . "/" . $uploaded->id . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : "."))),
            ];
        }
        return $docArray;
    }

    public function getSourceFormattedAttribute()
    {
        $return = "";
        switch ($this->source) {
            case 1:
                $return = "<span class='tag tag-success white_color'>" . trans('labels.general.branch') . "</span>";
                break;
            case 2:
                $return = "<span class='tag tag-success white_color'>" . trans('labels.general.online') . "</span>";
                break;
        }
        return $return;
    }



}