<?php

namespace App\Models\Operation\Claim\Traits\Attribute;


/**
 * Class MedicalExpenseAttribute
 */
trait MedicalExpenseAttribute{

    public function getAmountFormattedAttribute() {
        $amount = number_format($this->amount , 2 , '.' , ',' );
        return $amount;
    }

    public function getAssessedAmountFormattedAttribute()
    {
        $return = "-";
        if (!is_null($this->assessed_amount)) {
            $return = number_2_format($this->assessed_amount);
        }
        return $return;
    }

    /**
     * @return mixed
     * Get Name of compensated entity
     * @deprecated
     */
    public function getCompensatedEntityNameAttribute(){
        //employer
        if ($this->getMemberType() == 1){
            return $this->employer->name;
        } elseif ($this->getMemberType() == 3){
            return $this->insurance->name;
        }elseif($this->getMemberType() == 2){
            return $this->employee->name;
        }elseif($this->getMemberType() == 4){
            return $this->dependent->name;
        }
    }

    /**
     * @return mixed
     * Get Name of compensated  with amount
     * @deprecated
     */
    public function getCompensatedEntityNameWithAmountAttribute(){

        return $this->getCompensatedEntityNameAttribute() . ' (' . number_2_format($this->amount) . ')';
    }

    public function getMemberType(){
        return $this->member_type_id;
    }

    public function getAssessButtonAttribute() {

        return '<a href="' . route('backend.claim.notification_report.claim_assessment_checklist', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-list-alt" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.assess') . '"></i></a> ';

    }
    
    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
        return $this->getAssessButtonAttribute();

    }

    public function getAccrualActionButtonsAttribute() {
        return '<a href="' . route('backend.claim.claim_accrual.notification_report.claim_assessment_checklist', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-list-alt" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.assess') . '"></i></a> ';

    }

}