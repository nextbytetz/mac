<?php

namespace App\Models\Operation\Claim\Traits\Relationship;


use App\Models\Operation\Claim\NotificationReport;

trait ClaimRelationship
{

    public function notificationReport()
    {
        return $this->belongsTo(NotificationReport::class);
    }

}