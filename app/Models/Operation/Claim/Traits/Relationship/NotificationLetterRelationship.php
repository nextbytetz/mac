<?php
/**
 * Created by PhpStorm.
 * User: gwanchi
 * Date: 11/28/18
 * Time: 3:03 PM
 */

namespace App\Models\Operation\Claim\Traits\Relationship;


use App\Models\Operation\Claim\NotificationReport;

trait NotificationLetterRelationship
{

    public function notificationReport()
    {
        return $this->belongsTo(NotificationReport::class);
    }

}