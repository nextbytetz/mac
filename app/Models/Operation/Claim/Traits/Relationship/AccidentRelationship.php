<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Operation\Claim\Witness;
use App\Models\Sysdef\CodeValue;
use App\Models\Sysdef\JobTitle;
use Carbon\Carbon;
/**
 * Class AccidentRelationship
 */
trait AccidentRelationship {

    //Relation to the accident type
    public function accidentType()
    {
        return $this->belongsTo(\App\Models\Operation\Claim\AccidentType::class);
    }
    //Relation to the Notification Report
    public function notificationReport()
    {
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
    }
    //Relation to the accident witness
    public function accidentWitnesses()
    {
        return $this->hasMany(\App\Models\Operation\Claim\AccidentWitness::class);
    }

    public function witnesses()
    {
        return $this->belongsToMany(Witness::class, "accident_witnesses")->withTimestamps();
    }

    public function cause()
    {
        return $this->belongsTo(CodeValue::class, "accident_cause_cv_id");
    }

    public function exposure()
    {
        return $this->belongsTo(CodeValue::class, "accident_incident_exposure_cv_id");
    }

    public function jobTitle()
    {
        return $this->belongsTo(JobTitle::class);
    }

}