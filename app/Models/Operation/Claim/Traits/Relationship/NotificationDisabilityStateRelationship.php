<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Operation\Claim\NotificationDisabilityStateAssessment;
use App\Models\Operation\Claim\NotificationEligibleBenefit;

/**
 * Class NotificationDisabilityStateRelationship
 */
trait NotificationDisabilityStateRelationship{



    //Relation to the disability state checklis
    public function disabilityStateChecklist(){
        return $this->belongsTo(\App\Models\Operation\Claim\DisabilityStateChecklist::class);
    }

    //Relation to the medicla Expense
    public function medicalExpense(){
        return $this->belongsTo(\App\Models\Operation\Claim\MedicalExpense::class);
    }

    //Relation to the notification disability state assessment
    public function notificationDisabilityStateAssessment(){
        return $this->hasOne(\App\Models\Operation\Claim\NotificationDisabilityStateAssessment::class)->withDefault([
            'days' => NULL,
            'remarks' => NULL,
        ]);
    }

    public function notificationDisabilityStateAssessments() {
        return $this->hasMany(NotificationDisabilityStateAssessment::class);
    }

    public function benefit()
    {
        return $this->belongsTo(NotificationEligibleBenefit::class, "notification_eligible_benefit_id");
    }

}