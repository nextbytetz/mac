<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Workflow\WfModule;
use App\Models\Workflow\WfTrack;

trait NotificationWorkflowRelationship
{

    /**
     * @return mixed
     */
    public function notificationReport()
    {
        return $this->belongsTo(NotificationReport::class);
    }

    /**
     * @return mixed
     */
    public function wfModule()
    {
        return $this->belongsTo(WfModule::class);
    }

    /**
     * @return mixed
     */
    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource')->orderBy("wf_tracks.id", "asc");
    }

    /**
     * @return mixed
     */
    public function eligibles()
    {
        return $this->hasMany(NotificationEligibleBenefit::class);
    }

}