<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Notifications\Letter;
use App\Models\Operation\Claim\AssessmentDocument;
use App\Models\Operation\Claim\BenefitType;
use App\Models\Operation\Claim\BenefitTypeClaim;
use App\Models\Operation\Claim\ClaimCompensation;
use App\Models\Operation\Claim\DocumentResource;
use App\Models\Operation\Claim\IncidentAssessment;
use App\Models\Operation\Claim\MedicalExpense;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Claim\NotificationDisabilityState;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Claim\NotificationHealthState;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\NotificationWorkflow;
use App\Models\Operation\Claim\NotificationDisabilityStateAssessment;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Models\Operation\ClaimAccrual\AccrualMedicalExpense;

trait NotificationEligibleBenefitRelationship
{

    public function memberType()
    {
        return $this->belongsTo(MemberType::class);
    }

    public function incident()
    {
        return $this->belongsTo(NotificationReport::class, "notification_report_id");
    }

    public function workflow()
    {
        return $this->belongsTo(NotificationWorkflow::class, "notification_workflow_id");
    }

    public function medicalExpense()
    {
        return $this->belongsTo(MedicalExpense::class, "data_id");
    }

    public function benefit()
    {
        return $this->belongsTo(BenefitTypeClaim::class, "benefit_type_claim_id");
    }

    public function benefitTypes()
    {
        return $this->hasManyThrough(BenefitType::class,BenefitTypeClaim::class, "id", "benefit_type_claim_id", "benefit_type_claim_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function notificationDisabilityStates()
    {
        return $this->hasMany(NotificationDisabilityState::class);
    }

    public function notificationHealthStates()
    {
        return $this->hasMany(NotificationHealthState::class);
    }

    public function notificationDisabilityStateAssessments()
    {
        return $this->hasManyThrough(NotificationDisabilityStateAssessment::class,NotificationDisabilityState::class);
    }

    public function childs()
    {
        return $this->hasMany(NotificationEligibleBenefit::class, "parent_id", "id");
    }

    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }

    public function letter()
    {
        $codeValue = new CodeValueRepository();
        //Benefit Award Letter
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->CLBNAWLTTR());
    }

    /**
     * @return mixed
     */
    public function assessment()
    {
        return $this->hasOne(IncidentAssessment::class)->withDefault([
            'pd_impairment_id' => NULL,
            'occupation_id' => NULL,
            'wpi' => NULL,
            'pd_injury_id' => NULL,
            'pd_amputation_id' => NULL,
            'method' => NULL,
            'pd_comments' => NULL,
        ]);
    }

    public function accrualMedicalExpense()
    {
        return $this->belongsTo(AccrualMedicalExpense::class, "data_id");
    }

    public function usedDocuments()
    {
        return $this->hasMany(AssessmentDocument::class, "notification_eligible_benefit_id");
    }

    public function documents()
    {
        return $this->morphMany(DocumentResource::class, 'resource');
    }

    public function claimCompensations()
    {
        return $this->hasMany(ClaimCompensation::class);
    }
}