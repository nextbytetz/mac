<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Operation\Claim\Accident;

trait WitnessRelationship
{
    public function accidents()
    {
        return $this->belongsToMany(Accident::class, "accident_witnesses")->withTimestamps();
    }
}