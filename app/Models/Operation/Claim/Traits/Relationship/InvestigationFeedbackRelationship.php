<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use Carbon\Carbon;
/**
 * Class InvestigationFeedbackRelationship
 */
trait InvestigationFeedbackRelationship{


    //Relation to the investigation questions
    public function investigationQuestion(){
        return $this->belongsTo(\App\Models\Operation\Claim\InvestigationQuestion::class);
    }

    public function code(){
		return $this->belongsTo(\App\Models\Sysdef\Code::class);
	}

	public function codeValue(){
        return $this->belongsTo(\App\Models\Sysdef\CodeValue::class);
    }
}