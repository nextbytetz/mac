<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Operation\Claim\BenefitType;
use App\Models\Operation\Claim\BenefitTypeClaim;

trait DocumentBenefitRelationship
{
    public function benefitType()
    {
        return $this->belongsTo(BenefitType::class, "benefit_type_id");
    }
}