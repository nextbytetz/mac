<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

/**
 * Class NotificationHealthProviderServiceRelationship
 */
trait NotificationHealthProviderPractitionerRelationship{


    //Relation to the notification health provider practitioner
    public function medicalPractitioner(){
        return $this->belongsTo(\App\Models\Operation\Claim\MedicalPractitioner::class);
    }



}