<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Sysdef\Unit;

/**
 * Class NotificationContributionTrackRelationship
 */
trait NotificationContributionTrackRelationship{


    //Relation to the notification report
    public function notificationReport(){
        return $this->belongsTo(NotificationReport::class);
    }


    /* Relation to From user */
    public function fromUser(){
        return $this->belongsTo(User::class, 'from_user_id');
    }

    /* Relation to user */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /* Relation to unit */
    public function unit(){
        return $this->belongsTo(Unit::class);
    }
}