<?php

namespace App\Models\Operation\Claim\Traits\Relationship;
use App\Models\Operation\Claim\InvestigationFeedback;

/**
 * Trait NotificationInvestigatorRelationship
 * @package App\Models\Operation\Claim\Traits\Relationship
 */
trait NotificationInvestigatorRelationship
{

    /**
     * @return mixed
     */
    public function notificationReport()
    {
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\Auth\User::class);
    }

    /**
     * @return mixed
     */
    public function feedbacks()
    {
        return $this->hasMany(InvestigationFeedback::class, "notification_investigator_id");
    }

}