<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

/**
 * Class DisabilityStateChecklistRelationship
 */
trait DisabilityStateChecklistRelationship{


    //Relation to the benefitType
    public function benefitType(){
        return $this->belongsTo(\App\Models\Operation\Claim\BenefitType::class);
    }


}