<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use Carbon\Carbon;
/**
 * Class EmployeeOldCompensationRelationship
 */
trait EmployeeOldCompensationRelationship{


    //Relation to the incident type
    public function incidentType(){
        return $this->belongsTo(\App\Models\Operation\Claim\IncidentType::class);
    }

    //Relation to the employee
    public function employee(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class);
    }

}