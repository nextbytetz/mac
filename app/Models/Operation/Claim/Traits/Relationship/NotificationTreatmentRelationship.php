<?php

namespace App\Models\Operation\Claim\Traits\Relationship;


use App\Models\Operation\Claim\DisabilityStateChecklist;
use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\Insurance;
use App\Models\Operation\Claim\MedicalPractitioner;
use App\Models\Operation\Claim\MemberType;

trait NotificationTreatmentRelationship
{
    /**
     * @return mixed
     */
    public function disabilities()
    {
        return $this->belongsToMany(DisabilityStateChecklist::class, "notification_treatment_disabilities")->withPivot("days")->withTimeStamps();
    }

    /**
     * @return mixed
     */
    public function practitioners()
    {
        return $this->belongsToMany(MedicalPractitioner::class, "notification_treatment_practitioners");
    }

    /**
     * @return mixed
     */
    public function healthProvider()
    {
        return $this->belongsTo(HealthProvider::class);
    }

    /**
     * @return mixed
     */
    public function memberType()
    {
        return $this->belongsTo(MemberType::class, "member_type_id");
    }

    /**
     * @return mixed
     */
    public function insurance()
    {
        return $this->belongsTo(Insurance::class, "member_id");
    }

}