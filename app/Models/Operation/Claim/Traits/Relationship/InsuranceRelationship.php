<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Finance\BankBranch;


/**
 * Class NotificationInvestigatorRelationship
 */
trait InsuranceRelationship{





    //Relation to the bankBranch
    public function bankBranch(){
        return $this->belongsTo(BankBranch::class);
    }


}