<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

/**
 * Class NotificationHealthStateRelationship
 */
trait NotificationHealthStateRelationship{



    //Relation to the health state checklist
    public function healthStateChecklist(){
        return $this->belongsTo(\App\Models\Operation\Claim\HealthStateChecklist::class);
    }

    //Relation to the medical expense
    public function medicalExpense(){
        return $this->belongsTo(\App\Models\Operation\Claim\MedicalExpense::class);
    }
}