<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Location\District;
use Carbon\Carbon;
/**
 * Class EmployeeOldCompensationRelationship
 */
trait HealthProviderRelationship{

    //Relation to the district
    public function dist() {
        return $this->belongsTo(\App\Models\Location\District::class, "district_id");
    }

}