<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

/**
 * Class NotificationHealthProviderServiceRelationship
 */
trait NotificationHealthProviderRelationship{


    //Relation to the notification health provider practitioner
    public function notificationHealthProviderPractitioner(){
        return $this->hasOne(\App\Models\Operation\Claim\NotificationHealthProviderPractitioner::class);
    }

    //Relation to the notification health provider services
    public function notificationHealthProviderServices(){
        return $this->hasMany(\App\Models\Operation\Claim\NotificationHealthProviderService::class);
    }

    //Relation to the Medical expens
    public function medicalExpense(){
        return $this->belongsTo(\App\Models\Operation\Claim\MedicalExpense::class);
    }

    //Relation to the Notification Report
    public function notificationReport(){
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
    }

    //Relation to the health provider
    public function healthProvider(){
        return $this->belongsTo(\App\Models\Operation\Claim\HealthProvider::class);
    }

    public function healthProviderWithTrashed(){
        return $this->belongsTo(\App\Models\Operation\Claim\HealthProvider::class, 'health_provider_id', 'id');
    }
}