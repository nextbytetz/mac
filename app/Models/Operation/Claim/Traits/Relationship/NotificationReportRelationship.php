<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Location\District;
use App\Models\Notifications\Letter;
use App\Models\Operation\Claim\BenefitType;
use App\Models\Operation\Claim\BenefitTypeClaim;
use App\Models\Operation\Claim\BodyPartInjury;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Claim\DocumentGroup;
use App\Models\Operation\Claim\DocumentResource;
use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\ImpairmentAssessment;
use App\Models\Operation\Claim\IncidentClosure;
use App\Models\Operation\Claim\IncidentMae;
use App\Models\Operation\Claim\IncidentTd;
use App\Models\Operation\Claim\ManualSystemFile;
use App\Models\Operation\Claim\NotificationContributionTrack;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Claim\NotificationHealthProvider;
use App\Models\Operation\Claim\NotificationInvestigator;
use App\Models\Operation\Claim\NotificationLetter;
use App\Models\Operation\Claim\NotificationReportAppeal;
use App\Models\Operation\Claim\NotificationTreatment;
use App\Models\Operation\Claim\NotificationWorkflow;
use App\Models\Operation\Claim\PortalIncident;
use App\Models\Operation\Claim\Witness;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Payroll\Pensioner;
use App\Models\Sysdef\CodeValue;
use App\Models\Task\Checker;
use App\Models\Workflow\WfTrack;
use Carbon\Carbon;
use App\Models\Operation\ClaimFollowup\ClaimFollowup;
/**
 * Class NotificationReportRelationship
 */
trait NotificationReportRelationship {

    //Relation to the employee
    public function employee()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class)->withDefault([
            //"id" => 0,
            'name' => $this->employee_name,
            'memberno' => '-',
            'dob' => NULL,

        ]);
    }

    //Relation to the employer
    public function employer()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class)->withDefault([
            'name' => $this->employer_name,
            'reg_no' => '-',
        ]);
    }

    public function employers()
    {
        return $this->belongsToMany(Employer::class, "notification_employers")->withTimestamps();
    }

    //Relation to the incident types
    public function incidentType() {
        return $this->belongsTo(\App\Models\Operation\Claim\IncidentType::class);
    }

    //Relation to the user
    public function user()
    {
        return $this->belongsTo(\App\Models\Auth\User::class);
    }

    public function checklistUser()
    {
        return $this->belongsTo(User::class, "allocated")->withDefault([
            'name' => "",
        ]);
    }

    //Relation to the diseases
    public function disease()
    {
        return $this->hasOne(\App\Models\Operation\Claim\Disease::class);
    }

    //Relation to the accident
    public function accident()
    {
        return $this->hasOne(\App\Models\Operation\Claim\Accident::class);
    }

    //Relation to the death
    public function death()
    {
        return $this->hasOne(\App\Models\Operation\Claim\Death::class);
    }

    //Relation to the medical Expense
    public function medicalExpenses() {
        return $this->hasMany(\App\Models\Operation\Claim\MedicalExpense::class);
    }

    //Relation to the investigation feedbacks
    public function investigationFeedbacks() {
        return $this->hasMany(\App\Models\Operation\Claim\InvestigationFeedback::class)->orderBy("investigation_feedbacks.id", "asc");
    }

    //Relation to the Notification health provider services
    public function notificationHealthProviderServices() {
        return $this->hasManyThrough(\App\Models\Operation\Claim\NotificationHealthProviderService::class, \App\Models\Operation\Claim\NotificationHealthProvider::class);
    }
    /*Relation to Notifiation health providers*/
    public function notificationHealthProviders(){
        return $this->hasMany(NotificationHealthProvider::class);
    }


    //Relation to the claim
    public function claim(){
        return $this->hasOne(\App\Models\Operation\Claim\Claim::class);
    }

    //Relation to the claim compensation
    public function claimCompensations(){
        return $this->hasManyThrough(\App\Models\Operation\Claim\ClaimCompensation::class, \App\Models\Operation\Claim\Claim::class);
    }

    //Relation to the notification health states
    public function notificationHealthStates(){
        return $this->hasMany(\App\Models\Operation\Claim\NotificationHealthState::class);
    }

    //Relation to the notification disability states
    public function notificationDisabilityStates(){
        return $this->hasManyThrough(\App\Models\Operation\Claim\NotificationDisabilityState::class, \App\Models\Operation\Claim\MedicalExpense::class);
    }


    /*Notification report Contribution tracks*/
    public function notificationContributionTracks(){
        return $this->hasMany(NotificationContributionTrack::class);
    }



    //Relation to the Incident Exposures
    public function incidentExposure(){
        return $this->belongsTo(\App\Models\Sysdef\CodeValue::class,'incident_exposure_cv_id', 'id');
    }

    //Relation to the bod part injury
    public function bodyPartInjury(){
        return $this->belongsTo(\App\Models\Sysdef\CodeValue::class,'body_part_injury_cv_id', 'id');
    }

    //Relation to the nature of incident
    public function natureOfIncident(){
        return $this->belongsTo(\App\Models\Sysdef\CodeValue::class,'nature_of_incident_cv_id', 'id');
    }

    //Relation to the paid through Insurance
    public function payThroughInsurance(){
        return $this->belongsTo(\App\Models\Operation\Claim\Insurance::class,'pay_through_insurance_id', 'id');
    }

    public function documents()
    {
        return $this->belongsToMany(Document::class)->withPivot('id', 'name', 'size', 'mime', 'ext','description', 'eoffice_document_id', 'doc_date', 'doc_receive_date', 'doc_create_date', 'folio', 'isreferenced')->withTimestamps();
    }

    public function uploadedDocuments()
    {
        return $this->belongsToMany(Document::class, pg_mac_portal() . ".document_notification_report")->withPivot("name", "description", "ext", "size", "mime", "id", "isverified")->withTimestamps();
    }

    public function additionalDocs()
    {
        return $this->belongsToMany(Document::class, "incident_additional_docs", "notification_report_id");
    }

    public function investigationDocuments()
    {
        return $this->belongsToMany(Document::class, "document_investigation")->withPivot('id', 'name', 'size', 'mime', 'ext','description', 'index')->withTimestamps();
    }

    public function documentGroup()
    {
        return $this->belongsTo(DocumentGroup::class);
    }

    /**
     * @return mixed
     */
    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource');
    }

    public function checkers()
    {
        return $this->morphMany(Checker::class, "resource");
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function notificationReportAppeal()
    {
        return $this->hasOne(NotificationReportAppeal::class);
    }

    public function workflows()
    {
        return $this->hasMany(NotificationWorkflow::class)->orderBy("notification_workflows.id", "asc");
    }

    /**
     * @return mixed
     * @description Workflow Processes
     */
    public function processes()
    {
        return $this->hasMany(NotificationWorkflow::class)->orderByDesc("notification_workflows.id");
    }

    public function letter()
    {
        return $this->hasOne(NotificationLetter::class);
    }

    /**
     * @return mixed
     */
    public function investigations()
    {
        return $this->hasMany(NotificationInvestigator::class);
    }

    /**
     * @return mixed
     */
    public function investigators()
    {
        return $this->belongsToMany(User::class, "notification_investigators")->withPivot('iscomplete', 'attended', 'recycle', 'allocate_status', 'type')->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function investigationStaffs()
    {
        return $this->hasMany(NotificationInvestigator::class)->orderByDesc("notification_investigators.id");
    }

    /**
     * @return mixed
     */
    public function stages()
    {
        //return $this->hasMany(NotificationStage::class);
        return $this->belongsToMany(CodeValue::class, "notification_stages", "notification_report_id", "notification_staging_cv_id")->whereIn("code_id", [13,23,24])->withPivot('id', 'comments', 'allow_repeat', 'attended', 'require_attend', 'isdeleted')->withTimestamps()->orderByDesc("notification_stages.id");
    }

    public function injuries()
    {
        return $this->belongsToMany(BodyPartInjury::class, "notification_injuries", "notification_report_id", "body_part_injury_id")->withTimestamps();
    }

    public function lostBodyParties()
    {
        return $this->belongsToMany(BodyPartInjury::class, "notification_lost_body_parts", "notification_report_id", "body_part_injury_id")->withTimestamps();
    }

    public function eligibleBenefits()
    {
        return $this->belongsToMany(BenefitTypeClaim::class, "notification_eligible_benefits")->withPivot('member_type_id', 'resource_id', 'user_id', 'processed', 'id')->withTimestamps();
    }

    public function benefits()
    {
        return $this->hasMany(NotificationEligibleBenefit::class, "notification_report_id");
    }

    public function treatments()
    {
        return $this->hasMany(NotificationTreatment::class)->orderBy("notification_treatments.id", "asc");
    }

    public function stage()
    {
        return $this->belongsTo(CodeValue::class, "notification_staging_cv_id");
    }

    public function allocatedUser()
    {
        return $this->belongsTo(User::class, "allocated");
    }

    public function pensioner()
    {
        return $this->hasOne(Pensioner::class);
    }

    public function manualSystemFile()
    {
        return $this->hasOne(ManualSystemFile::class);
    }

    public function td()
    {
        return $this->belongsTo(IncidentTd::class, "incident_td_id");
    }

    public function tds()
    {
        return $this->hasMany(IncidentTd::class);
    }

    public function closure()
    {
        return $this->belongsTo(IncidentClosure::class, "incident_closure_id");
    }

    public function closures()
    {
        return $this->hasMany(IncidentClosure::class);
    }

    public function maes()
    {
        return $this->hasMany(IncidentMae::class);
    }

    public function mae()
    {
        return $this->belongsTo(IncidentMae::class, "incident_mae_id");
    }

    public function onlineRequest()
    {
        return $this->belongsTo(PortalIncident::class, "incident_id");
    }

    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }

    public function impairmentAssessments()
    {
        return $this->hasMany(ImpairmentAssessment::class, 'notification_report_id');
    }

    public function documentResources()
    {
        return $this->morphMany(DocumentResource::class, 'resource');
    }

    /*Relation to Health service providers */
    public function hcps()
    {
        return $this->belongsToMany(HealthProvider::class, 'hcp_notification_report', 'notification_report_id', 'health_provider_id')->withTimestamps();
    }

    public function accrual(){
        return $this->hasOne(\App\Models\Operation\ClaimAccrual\AccrualNotificationReport::class);
    }

    //Relation to the accrual medical Expense
    public function accrualMedicalExpenses() {
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualMedicalExpense::class);
    }

    //Relation to the accrual Notification health provider services
    public function accrualNotificationHealthProviderServices() {
        return $this->hasManyThrough(\App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderService::class, \App\Models\Operation\Claim\AccrualNotificationHealthProvider::class);
    }
    /*Relation to accrual  Notifiation health providers*/
    public function accrualNotificationHealthProviders(){
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProvider::class);
    }

    //Relation to accrual the notification health states
    public function accrualNotificationHealthStates(){
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualNotificationHealthState::class);
    }

    //Relation to the accrual notification disability states
    public function accrualNotificationDisabilityStates(){
        return $this->hasManyThrough(\App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityState::class, \App\Models\Operation\ClaimAccrual\AccrualMedicalExpense::class);
    }

     public function accrualNotificationDisabilityStates2(){
        return $this->hasMany(\App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityState::class);
    }

    //Relation to the accrual claim
    public function accrualClaim(){
        return $this->hasOne(\App\Models\Operation\ClaimAccrual\AccrualClaim::class);
    }

    public function claimFollowup()
    {
        return $this->hasOne(ClaimFollowup::class);
    }

}