<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\MedicalPractitioner;
use App\Models\Sysdef\CodeValue;
use Carbon\Carbon;
/**
 * Class DeathRelationship
 */
trait DeathRelationship{


    //Relation to the death
    public function deathCause() {
        return $this->belongsTo(\App\Models\Operation\Claim\DeathCause::class);
    }

    public function accidentCause()
    {
        return $this->belongsTo(CodeValue::class, "accident_cause_cv_id")->withDefault([
            'id' => NULL,
            'name' => NULL,
        ]);
    }

    //Relation to the Notification report
    public function notificationReport(){
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
    }

    /**
     * @return mixed
     */
    public function healthProvider()
    {
        return $this->belongsTo(HealthProvider::class);
    }

    /**
     * @return mixed
     */
    public function medicalPractitioner()
    {
        return $this->belongsTo(MedicalPractitioner::class);
    }

    public function cause()
    {
        return $this->belongsTo(CodeValue::class, "death_cause_cv_id");
    }

    public function exposure()
    {
        return $this->belongsTo(CodeValue::class, "death_incident_exposure_cv_id");
    }

}