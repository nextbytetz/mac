<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use Carbon\Carbon;
/**
 * Class AccidentWitnessRelationship
 */
trait AccidentWitnessRelationship{


    //Relation to the accident witness
    public function witness(){
        return $this->belongsTo(\App\Models\Operation\Claim\Witness::class);
    }
}