<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

/**
 * Class NotificationHealthProviderServiceRelationship
 */
trait NotificationHealthProviderServiceRelationship{



    //Relation to the healthService checklist
    public function healthServiceChecklist(){
        return $this->belongsTo(\App\Models\Operation\Claim\HealthServiceChecklist::class);
    }

    //Relation to the notification_health provider
    public function notificationHealthProvider(){
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationHealthProvider::class);
    }
}