<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Finance\PaymentVoucherTransaction;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Compliance\Member\Dependent;

/**
 * Class ClaimCompensationRelationship
 */
trait ClaimCompensationRelationship{


    //Relation to the Member Type
    public function memberType(){
        return $this->belongsTo(\App\Models\Operation\Claim\MemberType::class);
    }

    //Relation to the employer
    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class, 'resource_id', 'id');
    }

    //Relation to the Insurance
    public function insurance(){
        return $this->belongsTo(\App\Models\Operation\Claim\Insurance::class, 'resource_id', 'id');
    }
// Relation to employee
        public function employee(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class, 'resource_id', 'id');
    }

// Relation to dependent
    public function dependent(){
        return $this->belongsTo(Dependent::class, 'resource_id', 'id');
    }

    //Relation to benefit type
    public function benefitType(){
        return $this->belongsTo(\App\Models\Operation\Claim\BenefitType::class);
    }

    //Relation to compensation payment type
    public function compensationPaymentType(){
        return $this->belongsTo(\App\Models\Operation\Claim\CompensationPaymentType::class);
    }

    //Relation to Claim
    public function claim(){
        return $this->belongsTo(\App\Models\Operation\Claim\Claim::class);
    }


    //Relation to notification eligible benefit
    public function notificationEligibleBenefit(){
        return $this->belongsTo(NotificationEligibleBenefit::class);
    }


    public function paymentVoucherTransaction(){
        return $this->hasOne(PaymentVoucherTransaction::class);
    }

}