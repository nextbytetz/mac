<?php

namespace App\Models\Operation\Claim\Traits\Relationship;
use App\Models\Operation\Claim\MaeSub;

/**
 * Class MedicalExpenseRelationship
 */
trait MedicalExpenseRelationship{


    //Relation to the Member Type
    public function memberType(){
        return $this->belongsTo(\App\Models\Operation\Claim\MemberType::class);
    }

    //Relation to the employer
    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class, 'resource_id', 'id');
    }

    //Relation to the Insurance
    public function insurance(){
        return $this->belongsTo(\App\Models\Operation\Claim\Insurance::class, 'resource_id', 'id');
    }

    public function employee(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class, 'resource_id', 'id');
    }

    //Relation to the Notification report
    public function notificationReport(){
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
    }

    public function maeSubs()
    {
        return $this->hasMany(MaeSub::class);
    }

    //Relation to the Notification HEALTH provider services
    public function notificationHealthProviderServices(){
        return $this->hasManyThrough(\App\Models\Operation\Claim\NotificationHealthProviderService::class, \App\Models\Operation\Claim\NotificationHealthProvider::class);
    }

    //Relation to the Notification HEALTH provider
    public function notificationHealthProviders() {
        return $this->hasMany(\App\Models\Operation\Claim\NotificationHealthProvider::class);
    }



    //Relation to the notification disability states
    public function notificationDisabilityStates(){
        return $this->hasMany(\App\Models\Operation\Claim\NotificationDisabilityState::class);
    }


    //Relation to the notification disability states assessment
    public function notificationDisabilityStateAssessments(){
        return $this->hasManyThrough(\App\Models\Operation\Claim\NotificationDisabilityStateAssessment::class,\App\Models\Operation\Claim\NotificationDisabilityState::class);
    }


}