<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use Carbon\Carbon;
use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\MedicalPractitioner;
use App\Models\Sysdef\JobTitle;
use App\Models\Sysdef\CodeValue;
/**
 * Class DiseaseRelationship
 */
trait DiseaseRelationship{


    //Relation to the Notification Report
    public function notificationReport(){
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationReport::class);
    }

    /**
     * @return mixed
     */
    public function healthProvider()
    {
        return $this->belongsTo(HealthProvider::class);
    }

    /**
     * @return mixed
     */
    public function medicalPractitioner()
    {
        return $this->belongsTo(MedicalPractitioner::class);
    }

    public function cause()
    {
        return $this->belongsTo(CodeValue::class, "disease_cause_cv_id");
    }

    public function exposure()
    {
        return $this->belongsTo(CodeValue::class, "disease_incident_exposure_cv_id");
    }

    public function jobTitle()
    {
        return $this->belongsTo(JobTitle::class);
    }

}