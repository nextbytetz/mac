<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Finance\FinCode;
use Carbon\Carbon;
/**
 * Class BenefitTypeRelationship
 */
trait BenefitTypeRelationship{


    //Relation to the fin account codes
    public function finAccountCode(){
        return $this->belongsTo(\App\Models\Finance\FinAccountCode::class);
    }

    /*Relation to fin code*/
    public function finCode(){
        return $this->belongsTo(FinCode::class);
    }
}