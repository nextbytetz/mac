<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Finance\BankBranch;
use App\Models\Legal\District;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\IncidentType;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Payroll\ManualPayrollMember;
use App\Models\Sysdef\CodeValue;


trait ManualNotificationReportRelationship{





    //Relation to the employee
    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    //Relation to the employer
    public function employer(){
        return $this->belongsTo(Employer::class);
    }


    //Relation to the incident type
    public function incidentType(){
        return $this->belongsTo(IncidentType::class);
    }


    //Relation to the manual payroll member
    public function manualPayrollMembers($incident_type){
        return $this->hasMany(ManualPayrollMember::class, 'case_no', 'case_no')->where('incident_type_id', $incident_type);
    }

    //Relation to the manual pensioner
    public function manualPensioner(){
        return $this->hasOne(ManualPayrollMember::class, 'case_no', 'case_no')->where('member_type_id', 5);
    }


    public function documents()
    {
        return $this->belongsToMany(Document::class)->withPivot('id', 'name', 'size', 'mime', 'ext','description', 'eoffice_document_id', 'doc_date', 'doc_receive_date', 'doc_create_date', 'folio', 'isreferenced')->withTimestamps();
    }


    public function status(){
        return $this->belongsTo(CodeValue::class, 'status_cv_id');
    }

    public function attendedBy(){
        return $this->belongsTo(User::class, 'attended_by');
    }

    public function lastUpdatedBy(){
        return $this->belongsTo(User::class, 'last_updated_by');
    }


    public function accidentCauseType(){
        return $this->belongsTo(CodeValue::class, 'accident_cause_type_cv_id');
    }

    public function accidentCauseAgency(){
        return $this->belongsTo(CodeValue::class, 'accident_cause_agency_cv_id');
    }

    public function incidentDistrict(){
        return $this->belongsTo(District::class, 'district_id');
    }

    public function injuryNature(){
        return $this->belongsTo(CodeValue::class, 'injury_nature_cv_id');
    }

    public function bodilyLocation(){
        return $this->belongsTo(CodeValue::class, 'bodily_location_cv_id');
    }

    /*Relation to Health service providers */
    public function hcps()
    {
        return $this->belongsToMany(HealthProvider::class, 'hcp_manual_notification_report', 'manual_notification_report_id', 'health_provider_id')->withTimestamps();
    }


}