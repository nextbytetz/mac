<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Operation\Claim\DocumentBenefit;
use App\Models\Operation\Claim\DocumentGroup;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\PortalIncident;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerAdvancePayment;
use App\Models\Operation\Compliance\Member\EmployerRegistration;
use App\Models\Operation\Compliance\Member\OnlineEmployerVerification;

trait DocumentRelationship
{

    public function notificationReports()
    {
        return $this->belongsToMany(NotificationReport::class)->withPivot("id", "ext", "name")->withTimestamps();
    }

    public function documentGroup()
    {
        return $this->belongsTo(DocumentGroup::class);
    }

    public function employers()
    {
        return $this->belongsToMany(Employer::class)->withPivot('id', 'name', 'size', 'mime', 'ext')->withTimestamps();
    }

    public function onlineEmployerVerifications()
    {
        return $this->belongsToMany(OnlineEmployerVerification::class)->withPivot('id', 'name', 'size', 'mime', 'ext')->withTimestamps();
    }

    public function onlineEmployerAdvancePayments()
    {
        return $this->belongsToMany(EmployerAdvancePayment::class)->withPivot('id', 'name', 'size', 'mime', 'ext')->withTimestamps();
    }

    public function onlineIncidents()
    {
        return $this->belongsToMany(PortalIncident::class, pg_mac_portal() . ".document_incident", "document_id", "incident_id")->withPivot('id', 'name', 'size', 'mime', 'ext')->withTimestamps();
    }

    public function benefits()
    {
        return $this->hasMany(DocumentBenefit::class);
    }

}