<?php

namespace App\Models\Operation\Claim\Traits\Relationship;


use App\Models\Operation\Claim\Document;
use App\Models\Operation\Claim\IncidentType;

trait DocumentGroupRelationship
{

    public function incidentTypes()
    {
        return $this->belongsToMany(IncidentType::class)->withTimestamps();
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

}