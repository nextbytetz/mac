<?php

namespace App\Models\Operation\Claim\Traits\Relationship;

use App\Models\Operation\Claim\NotificationEligibleBenefit;

/**
 * Trait NotificationDisabilityStateAssessmentRelationship
 * @package App\Models\Operation\Claim\Traits\Relationship
 */
trait NotificationDisabilityStateAssessmentRelationship{

    //Relation to the notification disability state
    public function notificationDisabilityState(){
        return $this->belongsTo(\App\Models\Operation\Claim\NotificationDisabilityState::class);
    }

}