<?php

namespace App\Models\Operation\Claim;

use App\Models\Sysdef\CodeValue;
use Illuminate\Database\Eloquent\Model;

class PortalDeath extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'deaths';

    public function deathCause()
    {
        return $this->belongsTo(DeathCause::class);
    }

    public function incidentOccurence()
    {
        return $this->belongsTo(CodeValue::class, "incident_occurence_cv_id");
    }

}
