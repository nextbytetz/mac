<?php

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\NotificationEligibleBenefitAttribute;
use App\Models\Operation\Claim\Traits\Relationship\NotificationEligibleBenefitRelationship;
use Illuminate\Database\Eloquent\Model;

class NotificationEligibleBenefit extends Model
{
    use NotificationEligibleBenefitAttribute, NotificationEligibleBenefitRelationship;

    protected $guarded = [];
    public $timestamps = true;

}
