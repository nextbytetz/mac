<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class AssessmentDocument extends Model
{
    //

    protected $guarded = [];
    public $timestamps = true;
}
