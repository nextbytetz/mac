<?php

namespace App\Models\Operation\Claim;


trait NotificationReportAccess
{
    /**
     * Save inputted documents
     *
     * @param $inputDocuments
     */
    public function saveDocuments($inputDocuments)
    {
        if (! empty($inputDocuments)) {
            $this->documents()->sync($inputDocuments);
        } else {
            $this->documents()->detach();
        }
    }

    /**
     * Attach document to current notification report.
     *
     * @param $document
     */
    public function attachDocument($document)
    {
        if (is_object($document)) {
            $document = $document->getKey();
        }

        if (is_array($document)) {
            $document = $document['id'];
        }

        $this->documents()->attach($document);
    }

    /**
     * Detach document form current notification report.
     *
     * @param $document
     */
    public function detachDocument($document)
    {
        if (is_object($document)) {
            $document = $document->getKey();
        }

        if (is_array($document)) {
            $document = $document['id'];
        }

        $this->documents()->detach($document);
    }

    /**
     * Attach multiple documents to current notification report.
     *
     * @param $documents
     */
    public function attachDocuments($documents)
    {
        foreach ($documents as $document) {
            $this->attachDocument($document);
        }
    }

    /**
     * Detach multiple documents from current notification report.
     *
     * @param $documents
     */
    public function detachDocuments($documents)
    {
        foreach ($documents as $document) {
            $this->detachDocument($document);
        }
    }
}