<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncidentType extends Model
{
    //
//    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    public function questions()
    {
        return $this->belongsToMany(InvestigationQuestion::class, "investigation_question_incident_type");
    }

}