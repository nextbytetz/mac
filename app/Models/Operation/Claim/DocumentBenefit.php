<?php

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\DocumentBenefitAttribute;
use App\Models\Operation\Claim\Traits\Relationship\DocumentBenefitRelationship;
use Illuminate\Database\Eloquent\Model;

class DocumentBenefit extends Model
{
    use DocumentBenefitAttribute, DocumentBenefitRelationship;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];
}
