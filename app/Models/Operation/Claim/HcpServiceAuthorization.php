<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class HcpServiceAuthorization extends Model implements AuditableContract
{
    //
    use  SoftDeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;
    protected $table = 'hsp_service_authorization';

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    //Relation to the employee
    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    //Relation to the employer
    public function employer(){
        return $this->belongsTo(Employer::class);
    }


    //Relation to the incident type
    public function incidentType(){
        return $this->belongsTo(IncidentType::class);
    }

}
