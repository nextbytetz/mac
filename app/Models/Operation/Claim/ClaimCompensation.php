<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\ClaimCompensationAttribute;
use App\Models\Operation\Claim\Traits\Relationship\ClaimCompensationRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ClaimCompensation extends Model implements AuditableContract
{
    //
    use ClaimCompensationRelationship, ClaimCompensationAttribute, SoftDeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}