<?php

namespace App\Models\Operation\Claim;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class PortalIncidentQueries extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'incident_queries';


    protected $guarded = [];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, "officer_id");
    }

    public function portalUser()
    {
        return $this->belongsTo(User::class, "officer_id");
    }

}
