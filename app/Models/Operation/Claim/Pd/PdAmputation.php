<?php

namespace App\Models\Operation\Claim\Pd;

use Illuminate\Database\Eloquent\Model;

class PdAmputation extends Model
{
    protected $guarded = [];
    public $timestamps = true;

    /**
     * @return string
     */
    public function getNameInfoAttribute()
    {
        return $this->name . " ( " . $this->percent . "% ) ";
    }

}
