<?php

namespace App\Models\Operation\Claim\Pd;

use Illuminate\Database\Eloquent\Model;

class PdAgeAdjustment extends Model
{
    protected $guarded = [];
    public $timestamps = true;
}
