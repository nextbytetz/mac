<?php

namespace App\Models\Operation\Claim\Pd;

use Illuminate\Database\Eloquent\Model;

class PdAgeRange extends Model
{
    protected $guarded = [];
    public $timestamps = true;
}
