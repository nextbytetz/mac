<?php

namespace App\Models\Operation\Claim\Pd;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    protected $guarded = [];
    public $timestamps = true;

    /**
     * @return string
     */
    public function getNameInfoAttribute()
    {
        return $this->name . "(" . $this->industry . ")";
    }

}
