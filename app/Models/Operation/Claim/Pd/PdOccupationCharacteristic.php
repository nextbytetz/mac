<?php

namespace App\Models\Operation\Claim\Pd;

use Illuminate\Database\Eloquent\Model;

class PdOccupationCharacteristic extends Model
{
    protected $guarded = [];
    public $timestamps = true;
}
