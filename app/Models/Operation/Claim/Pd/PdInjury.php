<?php

namespace App\Models\Operation\Claim\Pd;

use Illuminate\Database\Eloquent\Model;

class PdInjury extends Model
{
    protected $guarded = [];
    public $timestamps = true;

    public function getNameFecAttribute()
    {
        return $this->name . ' => ' . $this->rank;
    }

}
