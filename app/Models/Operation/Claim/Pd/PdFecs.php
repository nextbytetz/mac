<?php

namespace App\Models\Operation\Claim\Pd;

use Illuminate\Database\Eloquent\Model;

class PdFecs extends Model
{
    protected $guarded = [];
    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pd_fecs';

}
