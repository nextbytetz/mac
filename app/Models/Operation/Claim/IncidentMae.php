<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class IncidentMae extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'incident_mae';

    protected $guarded = [];
    public $timestamps = true;

}
