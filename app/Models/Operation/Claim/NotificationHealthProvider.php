<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Relationship\NotificationHealthProviderRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Carbon\Carbon;

class NotificationHealthProvider extends Model implements AuditableContract
{
    //
    use NotificationHealthProviderRelationship, SoftDeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;


    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    /**
     * @return string
     */
    public function getAttendDateFormattedAttribute()
    {
        $return = "-";
        if (!is_null($this->attend_date)) {
            $return = Carbon::parse($this->attend_date)->format('d-M-Y');
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getDismissDateFormattedAttribute()
    {
        $return = "-";
        if (!is_null($this->dismiss_date)) {
            $return = Carbon::parse($this->dismiss_date)->format('d-M-Y');
        }
        return $return;
    }

}