<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimFollowupCalculation extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;
}