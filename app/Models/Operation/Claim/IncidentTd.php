<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class IncidentTd extends Model
{
    protected $guarded = [];
    public $timestamps = true;
}
