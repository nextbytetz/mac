<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class PortalAccident extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accidents';

    public function accidentType()
    {
        return $this->belongsTo(AccidentType::class);
    }

}
