<?php

namespace App\Models\Operation\Claim;

use App\Models\Auth\User;
use App\Models\Location\Region;
use Illuminate\Database\Eloquent\Model;

class ImpairmentAssessment extends Model
{

    protected $guarded = [];
    public $timestamps = true;

    public function incident()
    {
        return $this->belongsTo(NotificationReport::class, 'notification_report_id');
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function assessorOfficer()
    {
        return $this->belongsTo(User::class, 'assessor');
    }

    public function officerCreated()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
