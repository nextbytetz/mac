<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class BodyPartInjury extends Model
{

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(BodyPartInjuryGroup::class, "body_part_injury_group_id");
    }

}
