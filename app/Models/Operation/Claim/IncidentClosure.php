<?php

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;

class IncidentClosure extends Model
{
    protected $guarded = [];
    public $timestamps = true;
}
