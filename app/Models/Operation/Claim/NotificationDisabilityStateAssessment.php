<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\NotificationDisabilityStateAttribute;
use App\Models\Operation\Claim\Traits\Relationship\NotificationDisabilityStateAssessmentRelationship;
use App\Models\Operation\Claim\Traits\Relationship\NotificationDisabilityStateRelationship;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class NotificationDisabilityStateAssessment extends Model implements AuditableContract
{
    //
    use  NotificationDisabilityStateAssessmentRelationship, SoftDeletes,  Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    // Get from date formatted
    public function getFromDateFormattedAttribute() {
        if ($this->from_date) {
            $return =  Carbon::parse($this->from_date)->format('d-M-Y');
        } else {
            $return = "-";
        }
        return $return;
    }

// Get to date formatted
    public function getToDateFormattedAttribute() {
        if ($this->to_date) {
            $return =  Carbon::parse($this->to_date)->format('d-M-Y');
        } else {
            $return = "-";
        }
        return $return;
    }

}