<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use App\Models\Operation\Claim\Traits\Attribute\HealthProviderAttribute;
use App\Models\Operation\Claim\Traits\Relationship\HealthProviderRelationship;
use Illuminate\Database\Eloquent\Model;

class HealthProvider extends Model
{
    //
    use HealthProviderAttribute, HealthProviderRelationship;

    protected $guarded = [];
    public $timestamps = true;



}