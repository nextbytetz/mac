<?php

namespace App\Models\Operation\Claim\Portal;

use App\Models\Auth\User;
use App\Models\Sysdef\CodeValuePortal;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ClaimActivity extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'claim_activities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function getCreatedAtFormattedAttribute()
    {
        return Carbon::parse($this->created_at)->format('d-M-Y');
    }

    /**
     * Description of the activity, reference to a model.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function description()
    {
        return $this->belongsTo(CodeValuePortal::class, "claim_activity_cv_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff()
    {
        return $this->belongsTo(User::class, "user_id");
    }

}
