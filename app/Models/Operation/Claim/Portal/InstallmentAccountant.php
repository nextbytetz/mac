<?php

namespace App\Models\Operation\Claim\Portal;

use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

/**
 * Class Claim
 * @package App\Models\Operation\Claim\Portal
 */
class InstallmentAccountant extends Model
{
    use Notifiable;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'installment_accountants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * @return string
     */

}