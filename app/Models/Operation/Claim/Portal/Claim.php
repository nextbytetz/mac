<?php

namespace App\Models\Operation\Claim\Portal;

use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

/**
 * Class Claim
 * @package App\Models\Operation\Claim\Portal
 */
class Claim extends Model
{
    use Notifiable;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'claims';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * @return string
     */
    public function getNameFormattedAttribute()
    {
        $return = "";
        //$return = ucfirst($this->firstname) . " " . ucfirst($this->middlename) . " " . ucfirst($this->lastname);
        $return = ucfirst($this->firstname) . " " . ucfirst($this->lastname);
        return $return;
    }

    /**
     * @return string
     */
    public function getActiveFormattedAttribute()
    {
        return ($this->isactive == 'true') ? $this->showStatus(true, "Active") : $this->showStatus(false, "Inactive");
    }

    /**
     * @return string
     */
    public function getConfirmedFormattedAttribute()
    {
        return ($this->confirmed == 'true') ? $this->showStatus(true, "Confirmed") : $this->showStatus(false, "Not Confirmed");
    }

    /**
     * @return string
     */
    public function getValidatedFormattedAttribute()
    {
        return ($this->validated == 'true') ? $this->showStatus(true, "Validated") : $this->showStatus(false, "Not validated");
    }

    public function getLastLoginFormattedAttribute()
    {
        if (!is_null($this->last_login))
            return Carbon::parse($this->last_login)->format('M d, Y H:i A');
        return "";
    }

    public function getCreatedAtFormattedAttribute()
    {
        return Carbon::parse($this->created_at)->format('M d, Y H:i A');
    }

    /**
     * @param $flag
     * @param $text
     * @return string
     */
    public function showStatus($flag, $text)
    {
        if ($flag) {
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . $text . "'>" . $text . "</span>";
        } else {
            return "<span class='tag square-tag spice-color' data-toggle='tooltip' data-html='true' title='" . $text . "'>" . $text . "</span>";
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function documents()
    {
        return $this->belongsToMany(Document::class, pg_mac_portal() . ".claim_document", "claim_id")->withPivot("name", "description", "ext", "size", "mime", "id", "isdmsposted", "isverified", "created_at", "updated_at", "user_verified")->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employer()
    {
        return $this->belongsTo(Employer::class, "resource_id");
    }

    public function activities()
    {
        return $this->hasMany(ClaimActivity::class);
    }

}