<?php

namespace App\Models\Operation\Claim;

use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use Illuminate\Database\Eloquent\Model;

class BenefitTypeClaim extends Model
{

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function benefitTypes()
    {
        return $this->hasMany(BenefitType::class, "benefit_type_claim_id");
    }

    public function documents()
    {
        $arrList = [];
        $benefitTypes = $this->benefitTypes;
        foreach ($benefitTypes as $benefitType) {
            $arrList += $benefitType->documents()->pluck("documents.id")->all();
        }
        $docs = (new DocumentRepository())->query()->select(['name', 'id'])->where('ismandatory', 1)->whereIn("id", $arrList)->get();
        return $docs;
    }

    /**
     * @return array
     */
    public function repeatedDocs()
    {
        $arrList = [];
        switch ($this->id) {
            case 1:
                //MAE Refund (Employee/Employer)
                ///==> Medical Receipts : 30
                $arrList = [30];
                break;
            case 3:
                //Temporary Disablement (TD)
                ///==> WCP-3 : 19, WCP-5 : 21
                $arrList = [19, 21];
                break;
            case 4:
                //Temporary Disablement Refund (TD Refund)
                ///==> WCC-2B : 18, WCP-4 : 20, .... WCP-3 : 19, WCP-5 : 21
                $arrList = [18, 20, 19, 21];
                break;
        }
        return $arrList;
    }

}
