<?php

namespace App\Models\Operation\Claim;

use App\Models\Auth\PortalUser;
use App\Models\Location\District;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Task\Checker;
use App\Models\Workflow\WfTrack;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PortalIncident extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'incidents';

    public function accident()
    {
        return $this->hasOne(PortalAccident::class, "incident_id");
    }

    public function disease()
    {
        return $this->hasOne(PortalDisease::class, "incident_id");
    }

    public function death()
    {
        return $this->hasOne(PortalDeath::class, "incident_id");
    }

    public function user()
    {
        return $this->belongsTo(PortalUser::class);
    }

    public function incident()
    {
        return $this->belongsTo(NotificationReport::class, "notification_report_id");
    }

    /**
     * @return mixed
     */
    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }

    public function incidentType()
    {
        return $this->belongsTo(IncidentType::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function checkers()
    {
        return $this->morphMany(Checker::class, "resource");
    }

    public function queries()
    {
        return $this->hasMany(PortalIncidentQueries::class, "incident_id");
    }

    public function notificationReport()
    {
        return $this->belongsTo(NotificationReport::class, "notification_report_id");
    }

    public function additionalDocs()
    {
        return $this->belongsToMany(Document::class, pg_mac_portal() . ".incident_additional_docs", "incident_id");
    }

    public function getResourceNameAttribute()
    {
        $return = $this->employee_name;
        if ($this->employee_id) {
            $return = $this->employee->name;
        }
        return $return;
    }

    public function getIncidentDateFormattedAttribute()
    {
        return Carbon::parse($this->incident_date)->format('d-M-Y');
    }

    public function getReportingDateFormattedAttribute()
    {
        return Carbon::parse($this->incident_date)->format('d-M-Y');
    }

    public function getCreatedAtFormattedAttribute()
    {
        return Carbon::parse($this->created_at)->format('d-M-Y');
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute() {
        $return = "";
        switch ($this->status) {
            case 0:
                //Pending
                $return = "<span class='tag tag-default'>Pending for Acknowledgement</span>";
                break;
            case 1:
                //Approved
                $return = "<span class='tag tag-primary'>Acknowledged for further Procedures</span>";
                break;
            case 2:
                //Queried
                $return = "<span class='tag tag-warning'>Queried</span>";
                break;
            case 3:
                //Declined
                $return = "<span class='tag tag-danger'>Declined</span>";
                break;
            case 4:
                //Cancelled
                $return = "<span class='tag tag-danger'>Request Cancelled</span>";
                break;
        }
        return $return;
    }

    public function documents()
    {
        return $this->belongsToMany(Document::class, pg_mac_portal() . ".document_incident", "incident_id")->withPivot("name", "description", "ext", "size", "mime", "id", "isdmsposted", "isverified", "created_at", "updated_at", "user_verified", "id")->withTimestamps();
    }

    public function getUserFormattedAttribute()
    {
        //return $this->user->name . " (" . $this->user->codeValuePortal->name . "), " . $this->getOwnStatusAttribute();
        return $this->user->name_formatted;
    }

    public function getOwnStatusAttribute()
    {
        $return = "";
        switch ($this->isown) {
            case 0:
                $return = trans("labels.backend.member.employer.facilitator");
                break;
            case 1:
                $return = trans("labels.backend.member.employer.owner");
                break;
        }
        return $return;
    }

    public function witnesses()
    {
        return $this->hasMany(PortalIncidentWitness::class, "incident_id");
    }

}
