<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05-May-17
 * Time: 2:09 AM
 */

namespace App\Models\Operation\Claim;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class NotificationReportAppeal extends Model
{
    //
    use  SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;


    /*RELATIONSHIP*/
    public function userApproved(){
        return $this->belongsTo(\App\Models\Auth\User::class, 'approved_by', 'id');
    }


}