<?php

namespace App\Models\Operation\ClaimFollowup;

use Illuminate\Database\Eloquent\Model;
use App\Models\Operation\Claim\Traits\Attribute\AccidentAttribute;
use App\Models\Operation\Claim\Traits\Relationship\AccidentRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Sysdef\CodeValue;
use App\Models\Operation\ClaimFollowup\ClaimFollowup;
use App\Models\Auth\User;


class ClaimFollowupUpdate extends Model
{
	use SoftDeletes;

	protected $guarded = [];
	public $timestamps = true;


	public function followUpType()
	{
		return $this->belongsTo(CodeValue::class, 'follow_up_type_cv_id');
	}

	public function claimFollowup()
    {
        return $this->belongsTo(ClaimFollowup::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}