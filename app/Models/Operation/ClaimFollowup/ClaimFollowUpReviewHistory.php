<?php

namespace App\Models\Operation\ClaimFollowup;

use Illuminate\Database\Eloquent\Model;

class ClaimFollowUpReviewHistory extends Model
{
	protected $guarded = [];
	public $timestamps = true;
}
