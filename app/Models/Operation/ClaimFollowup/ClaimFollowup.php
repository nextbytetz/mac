<?php

namespace App\Models\Operation\ClaimFollowup;

use Illuminate\Database\Eloquent\Model;
use App\Models\Operation\Claim\Traits\Attribute\AccidentAttribute;
use App\Models\Operation\Claim\Traits\Relationship\AccidentRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Operation\ClaimFollowup\ClaimFollowupUpdate;
use App\Models\Auth\User;
use App\Models\Operation\Claim\NotificationReport;


class ClaimFollowup extends Model
{
	use SoftDeletes;

	protected $guarded = [];
	public $timestamps = true;

	public function claimFollowupUpdate()
	{
		return $this->hasMany(ClaimFollowupUpdate::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function notificationReport()
	{
		return $this->belongsTo(NotificationReport::class);
	}
}
