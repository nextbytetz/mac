<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewTaxpayer extends Model
{
    use SoftDeletes;

    public function getNameAttribute()
    {
        $name = (empty($this->taxpayername) ? $this->tradingname : $this->taxpayername);
        if (empty($name) Or is_null($name)) {
            $name = $this->firstname . " " . $this->middlename . " " . $this->lastname;
        }
        return $name;
    }

    public function getDateofregistrationLabelAttribute($value)
    {
        $date = (!is_null($this->dateofregistration)) ? Carbon::parse($this->dateofregistration)->format("d-M-Y") : "";
        return $date;
    }

}
