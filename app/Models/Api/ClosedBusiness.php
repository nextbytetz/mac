<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClosedBusiness extends Model
{
    use SoftDeletes;

    public function getDeregistrationdateLabelAttribute()
    {
        $date = (!is_null($this->deregistrationdate)) ? Carbon::parse($this->deregistrationdate)->format("d-M-Y") : "";
        return $date;
    }

    public function getNameAttribute()
    {
        $name = (empty($this->taxpayername) ? $this->tradingname : $this->taxpayername);
        if (empty($name) Or is_null($name)) {
            $name = $this->firstname . " " . $this->middlename . " " . $this->lastname;
        }
        return $name;
    }

}
