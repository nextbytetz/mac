<?php

namespace App\Models\Investment;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Workflow\WfTrack;
use App\Models\Sysdef\CodeValue;

class InvestmentCallAccount extends Model
{

	use Auditable;

	protected $connection = 'pgmain';
    protected $table ='investment_call_accounts';
	protected $fillable = ['code_value_id','fin_year_id'];

	public function investmentBudget()
	{
		return $this->belongsTo('App\Models\Investment\InvestmentBudget');
	}
	public function codeValue()
	{
		return $this->belongsTo(CodeValue::class);
	}

}
