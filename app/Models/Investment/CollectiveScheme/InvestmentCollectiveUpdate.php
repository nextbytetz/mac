<?php

namespace App\Models\Investment\CollectiveScheme;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class InvestmentCollectiveUpdate extends Model implements AuditableContract
{
	use Auditable;
	protected $connection = 'pgmain';
	protected $fillable = [
		'scheme_id','lot_id','lot_number','category_id','number_of_unit','unit_value','total_unit_amount',
		'value_date','is_update','selling_price','total_unit','selling_date','is_sell','dividend_nav',
		'dividend_income','dividend_receivable','dividend_declaration_date','dividend_payment_date',
		'is_paid','is_dividend'
	];


}
