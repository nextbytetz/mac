<?php

namespace App\Models\Investment\CollectiveScheme;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Support\Facades\DB;

class InvestmentCollectiveScheme extends Model implements AuditableContract
{
	use Auditable;
	protected $connection = 'pgmain';
	protected $fillable = [
		'manager_id', 'scheme_name','scheme_reference'
	];

	public function lots()
	{
		return $this->hasMany('App\Models\Investment\CollectiveScheme\InvestmentCollectiveLot','scheme_id','id');
	}

	public function getTotalUnitsAttribute()
	{
		return DB::table('main.investment_collective_lots')->where('scheme_id',$this->id)->sum('purchased_unit');
	}

	public function getRemainUnitsAttribute()
	{
		return DB::table('main.investment_collective_lots')->where('scheme_id',$this->id)->sum('remaining_unit');
	}

	public function getUnitsSoldAttribute()
	{
		return DB::table('main.investment_collective_updates')->where('scheme_id',$this->id)->where('is_sell',true)->sum('number_of_unit');
	}


	public function getUnitsValueAttribute()
	{
		$latest_price = DB::table('main.investment_collective_updates')->where('scheme_id',$this->id)->where('is_update',true)->orderBy('value_date','desc')->first();

		if (count($latest_price)) {
			$value = $this->remain_units * $latest_price->unit_value;
		} else {
			$value = 0;
		}
		return number_format($value, 2, '.', '');
	}




	public function getManagerAttribute()
	{
		$manager = DB::table('main.employers')->where('id',$this->manager_id)->first();
		return count($manager) ? $manager->name : '';
	}

}
