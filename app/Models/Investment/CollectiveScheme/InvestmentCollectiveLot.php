<?php

namespace App\Models\Investment\CollectiveScheme;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class InvestmentCollectiveLot extends Model implements AuditableContract
{
	use Auditable;
	protected $connection = 'pgmain';
	
	protected $fillable = [
		'scheme_id', 'category_id','lot_number','purchased_unit','purchase_nav','purchase_amount',
		'purchase_date','remaining_unit','disbursment_date','budget_allocation_id','fin_year_id'
	];

	public function scheme()
	{
		return $this->belongsTo('App\Models\Investment\CollectiveScheme\InvestmentCollectiveScheme','scheme_id','id');
	}

}
