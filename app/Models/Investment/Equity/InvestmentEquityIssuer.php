<?php

namespace App\Models\Investment\Equity;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Support\Facades\DB;

class InvestmentEquityIssuer extends Model implements AuditableContract
{
	use Auditable;

	protected $connection = 'pgmain';
	protected $fillable = [
		'issuer_id', 'issuer_name','issuer_reference'
	];

	public function lots()
	{
		return $this->hasMany('App\Models\Investment\Equity\InvestmentEquityLot','equity_issuer_id','id');
	}


	public function getSharesSoldAttribute()
	{
		return DB::table('main.investment_equity_updates')->where('equity_issuer_id',$this->id)
		->where('is_sell',true)->sum('number_of_share');
	}

	public function getTotalSharesAttribute()
	{
		return DB::table('main.investment_equity_lots')->where('equity_issuer_id',$this->id)->sum('remaining_share');
	}

	public function getSharesValueAttribute()
	{
		$latest_price = DB::table('main.investment_equity_updates')->where('equity_issuer_id',$this->id)
		->where('is_update',true)->orderBy('value_date','desc')->first();

		if (count($latest_price)) {
			$value = $this->total_shares * $latest_price->share_value;
		} else {
			$value = 0;
		}
		return number_format($value, 2, '.', '');
	}

}
