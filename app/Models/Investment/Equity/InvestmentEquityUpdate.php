<?php

namespace App\Models\Investment\Equity;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class InvestmentEquityUpdate extends Model implements AuditableContract
{
	use Auditable;
	protected $connection = 'pgmain';

	protected $fillable = [
		'equity_issuer_id','equity_lot_id','lot_number','number_of_share','share_value','value_date','total_share_value','is_update',
		'total_share_available','selling_date','selling_broker','selling_broker_regno','is_sell','commission',
		'dividend_receivable','dividend_declaration_date','dividend_payment_date','is_dividend','is_dividend_paid'
	];


}

