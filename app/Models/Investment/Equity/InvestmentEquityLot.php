<?php

namespace App\Models\Investment\Equity;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class InvestmentEquityLot extends Model implements AuditableContract
{
	use Auditable;
	protected $connection = 'pgmain';
	protected $fillable = [
		'budget_allocation_id','fin_year_id','equity_issuer_id', 'category_id','category_name','lot_number','number_of_share',
		'total_issued_share','price_per_share','total_price','purchase_date','remaining_share','disbursment_date','broker_id',
		'broker_name','subscription_date','allotment_date','commission','total_cost'
	];

	public function issuer()
	{
		return $this->belongsTo('App\Models\Investment\Equity\InvestmentEquityIssuer','equity_issuer_id','id');
	}

}