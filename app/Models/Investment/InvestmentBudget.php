<?php

namespace App\Models\Investment;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Workflow\WfTrack;

class InvestmentBudget extends Model implements AuditableContract
{
	use Auditable;
	protected $connection = 'pgmain';
	protected $fillable = ['fin_year_id', 'amount','user_id','status','wf_done','wf_done_date'];


	public function allocations()
	{
		return $this->hasMany('App\Models\Investment\InvestmentBudgetAllocation');
	}


	public function fin_year()
	{
		return $this->belongsTo('App\Models\Finance\FinYear');
	}


	public function wfTracks()
	{
		return $this->morphMany(WfTrack::class, 'resource');
	}

	public function getResourceNameAttribute()
	{
		return !empty($this->fin_year->name) ? $this->fin_year->name.' investment budget' : 'investment budget';
	}


}
