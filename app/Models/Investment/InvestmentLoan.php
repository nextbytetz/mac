<?php

namespace App\Models\Investment;

use Illuminate\Database\Eloquent\Model;
use App\Models\Investment\InvestmentBudgetAllocation;

class InvestmentLoan extends Model
{
    //
	protected $connection = 'pgmain';
	// protected $table = 'investment_budget_allocation';
	protected $fillable = [
		'loan_type','loan_type_name','reg_no','loanee_name','loan_amount','disbursement_date',
		'maturity_date','tenure','interest_rate','penalty_rate','tax_rate','loan_balance',
		'interest_receivable','interest_income','principle_recovered','amount_due','penalty_due',
		'penalty_paid','portfolio_return','fin_year_id'
	];

	public function InvestmentBudgetAllocation()
	{
		return $this->belongsTo(InvestmentBudgetAllocation::class,'budget_allocation_id','id');
	}
}
