<?php

namespace App\Models\Investment;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Workflow\WfTrack;
use App\Models\Sysdef\CodeValue;

class InvestmentBudgetAllocation extends Model implements AuditableContract
{
	use Auditable;

	protected $connection = 'pgmain';
	protected $table = 'investment_budget_allocation';
	protected $fillable = ['investment_budget_id','code_value_id','percent','amount','fin_year_id','amount_spent','investment_code_type_id','code_value_reference'];

	public function investmentBudget()
	{
		return $this->belongsTo('App\Models\Investment\InvestmentBudget');
	}


	public function codeValue()
	{
		return $this->belongsTo(CodeValue::class);
	}

	public function investmentLoan()
	{
		return $this->hasMany('App\Models\Investment\InvestmentLoan','budget_allocation_id','id');
	}

}

