<?php

namespace App\Models\Investment;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EquityUpdate extends Model implements AuditableContract
{
	use Auditable;
	protected $connection = 'pgmain';
	protected $fillable = [
		'investment_equity_id', 'price_per_share','number_of_shares','share_price_date','total_share_values','selling_date',
		'selling_broker','selling_broker_regno','commission','is_sell'
	];


}
