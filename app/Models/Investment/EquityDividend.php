<?php

namespace App\Models\Investment;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EquityDividend  extends Model implements AuditableContract
{
	use Auditable;
	protected $connection = 'pgmain';
	protected $fillable = [
		'investment_equity_id', 'dividend_per_share','number_of_shares','dividend_amount','dividend_income','tax_rate',
		'declaration_date'
	];


}

