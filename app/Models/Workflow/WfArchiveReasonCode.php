<?php

namespace App\Models\Workflow;

use App\Models\Sysdef\CodeValue;
use Illuminate\Database\Eloquent\Model;

class WfArchiveReasonCode extends Model
{

    /**
     * @var array
     */
    protected $guarded = [];
    public $timestamps = true;

    public function reasons()
    {
        return $this->hasMany(CodeValue::class, 'code_id', 'code_id');
    }

}
