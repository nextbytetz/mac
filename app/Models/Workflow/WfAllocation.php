<?php

namespace App\Models\Workflow;

use App\Models\Workflow\Relationship\WfAllocationRelationship;
use Illuminate\Database\Eloquent\Model;

class WfAllocation extends Model
{
    use WfAllocationRelationship;
    /**
     * @var array
     */
    protected $guarded = [];
    public $timestamps = true;
}
