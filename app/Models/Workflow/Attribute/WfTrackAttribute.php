<?php

namespace App\Models\Workflow\Attribute;

use Carbon\Carbon;

trait WfTrackAttribute
{

    public function getStatusNarrationAttribute(): string
    {
        $return = '';
        if ($this->status == 0) {
            $return = "Pending";
        } elseif ($this->status == 1) {
            if ($this->wfDefinition->is_approval) {
                $return = "Approved";
            } else {
                $return = "Recommended";
            }
        } elseif ($this->status == 3) {
            $return = "Declined";
        } elseif ($this->status == 4) {
            $return = "Seek Legal Advice";
        } elseif ($this->status == 5) {
            $return = "Cancelled by User";
        } elseif ($this->status == 6) {
            $return = "Archived";
        } else {
            $return = "Reversed to level";
        }
        return $return;
    }

    public function getStatusNarrationLabelAttribute(): string
    {
        $return = '';
        if ($this->status == 0) {
            $return = "Pending&nbsp;&nbsp;<span style='color: #2b485c'>&nbsp;&nbsp;<i class='icon fa fa-spinner fa-2x' aria-hidden='true'></i></span>";
        } elseif ($this->status == 1) {
            if ($this->wfDefinition->is_approval) {
                $return = "Approved&nbsp;&nbsp;<span style='color: green;'>&nbsp;&nbsp;<i class='icon fa fa-mail-forward fa-2x' aria-hidden='true'></i></span>";
            } else {
                $return = "Recommended&nbsp;&nbsp;<span style='color: green;'>&nbsp;&nbsp;<i class='icon fa fa-mail-forward fa-2x' aria-hidden='true'></i></span>";
            }
        } elseif ($this->status == 3) {
            $return = "Declined&nbsp;&nbsp;<span style='color: red;'>&nbsp;&nbsp;<i class='icon fa fa-question fa-2x' aria-hidden='true'></i></span>";
        } elseif ($this->status == 4) {
            $return = "Seek Advice&nbsp;&nbsp;<span style='color: lightgreen'>&nbsp;&nbsp;<i class='icon fa fa-question fa-2x' aria-hidden='true'></i></span>";
        } elseif ($this->status == 5) {
            $return = "Cancelled by User&nbsp;&nbsp;<span style='color: darkred;'>&nbsp;&nbsp;<i class='icon fa fa-close fa-2x' aria-hidden='true'></i></span>";
        } elseif ($this->status == 6) {
            $return = "Archived&nbsp;&nbsp;<span style='color: blue;'>&nbsp;&nbsp;<i class='icon fa fa-archive fa-2x' aria-hidden='true'></i></span>";
        } else {
            $return = "Reversed to level&nbsp;&nbsp;<span style='color: yellowgreen;'>&nbsp;&nbsp;<i class='icon fa fa-mail-reply-all fa-2x' aria-hidden='true'></i></span>";
        }
        return $return;
    }


    public function status()
    {
        return $this->status;
    }


    public function getReceiveDateFormattedAttribute(): string
    {
        return  Carbon::parse($this->receive_date)->format('d-M-Y g:i:s A');
    }

    public function getForwardDateFormattedAttribute(): string
    {
        $return = "";
        if (!is_null($this->forward_date)) {
            $return = Carbon::parse($this->forward_date)->format('d-M-Y g:i:s A');
        } else {
            $return = "-";
        }
        return $return;
    }

    public function getAssignStatusAttribute(): string
    {
        $return = "";
        if ($this->assigned) {
            //assigned
            $return = "<span class='tag tag-success white_color'>" . trans('labels.backend.member.assigned') . "</span>";
        } else {
            $return = "<span class='tag tag-info white_color'>" . trans('labels.backend.member.not_assigned') . "</span>";
        }
        return $return;
    }

    /**
     * @return int
     */
    public function getAgingDays(): int
    {
        if (1) {
            return $this->calculateAging();
        } else {
            $wf_date = Carbon::parse($this->receive_date);
            $forward_date = Carbon::parse($this->forward_date);
            return $wf_date->diffInDays($forward_date);
        }
    }

    /**
     * @return int
     */
    public function calculateAging(): int
    {
        $wf_date = Carbon::parse($this->receive_date);
        $forward_date = Carbon::parse($this->forward_date);
        $return = $wf_date->diffInDays($forward_date);
        $offdays = 0;
        if ($this->process_dates) {
            $arrs = json_decode($this->process_dates, true);
            foreach ($arrs as $arr) {
                $from_date = Carbon::parse($arr['from_date']);
                $to_date = Carbon::parse($arr['to_date']);
                if ($to_date->greaterThan($forward_date)) {
                    $to_date = $forward_date;
                }
                $offdays += $from_date->diffInDays($to_date);
            }
            $return -= $offdays;
        }
        return $return;
    }

    /**
     * @return int
     */
    public function getAgingDaysPendingLevel(): int
    {
        return $this->calculateAging();
    }

    public function getUsernameFormattedAttribute(): string
    {
        $return = "";
        if (is_null($this->user_type) Or empty($this->user_type)) {
            $result = 0;
        } else {
            $result = $this->user()->count();
        }
        if ($result) {
            $return = "<span class='tag tag-success'><b>" . $this->user->username . "</b></span><br/><span class=''>" . $this->wfDefinition->designation->name . " - " . $this->wfDefinition->unit->name . "</span>";
        } else {
            $return = "<span class='tag tag-info'><b>Unassigned</b></span><br/><span class=''>" . $this->wfDefinition->designation->name . " - " . $this->wfDefinition->unit->name . "</span>";
        }
        return $return;
    }

    public function getUsernameCompletedFormattedAttribute(): string
    {
        $return = "";
        $return = "<b>" . (($this->user) ? $this->user->username : "") . "</b>&nbsp;&nbsp;<span class=''>" . $this->wfDefinition->designation->name . " - " . $this->wfDefinition->unit->name . "</span>";
        return $return;
    }

}