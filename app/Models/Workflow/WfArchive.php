<?php

namespace App\Models\Workflow;

use App\Models\Auth\User;
use App\Models\Sysdef\CodeValue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WfArchive extends Model
{

    protected $guarded = [];
    public $timestamps = true;
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    public function wfTrack()
    {
        return $this->belongsTo(WfTrack::class);
    }

    public function getPeriodDaysLabelAttribute()
    {
        $from = Carbon::parse($this->from_date);
        $to = Carbon::parse($this->to_date);
        return $from->diffInDays($to);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reason()
    {
        return $this->belongsTo(CodeValue::class, 'archive_reason_cv_id');
    }

}
