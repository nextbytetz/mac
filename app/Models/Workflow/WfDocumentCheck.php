<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;

class WfDocumentCheck extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];
    public $timestamps = true;
}
