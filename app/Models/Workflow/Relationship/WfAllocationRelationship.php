<?php

namespace App\Models\Workflow\Relationship;

use App\Models\Workflow\WfDefinition;

trait WfAllocationRelationship
{

    public function wfDefinition()
    {
        return $this->belongsTo(WfDefinition::class);
    }

}