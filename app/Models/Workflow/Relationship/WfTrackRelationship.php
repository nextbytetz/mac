<?php

namespace App\Models\Workflow\Relationship;

use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Finance\Receivable\InterestWriteOff;
use App\Models\Operation\Claim\Claim;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\NotificationWorkflow;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\OnlineEmployerVerification;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Models\Operation\Payroll\PayrollBeneficiaryUpdate;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Operation\Payroll\Run\PayrollReconciliation;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Models\Workflow\WfArchive;
use App\Models\Workflow\WfDefinition;
use App\Models\Investment\InvestmentBudget;
use App\Models\Operation\ClaimAccrual\AccrualNotificationReport;

trait WfTrackRelationship
{

    public function wfDefinition()
    {
        return $this->belongsTo(WfDefinition::class);
    }

    //Relation to receipts
    public function receipt()
    {
        return $this->belongsTo(\App\Models\Finance\Receipt\Receipt::class, 'resource_id');
    }

    public function claim()
    {
        return $this->belongsTo(Claim::class, 'resource_id');
    }

    public function notificationReport()
    {
        return $this->belongsTo(NotificationReport::class, 'resource_id');
    }

    public function notificationWorkflow()
    {
        return $this->belongsTo(NotificationWorkflow::class, "resource_id");
    }

    public function onlineEmployerVerification()
    {
        return $this->belongsTo(OnlineEmployerVerification::class, 'resource_id');
    }

    public function bookingInterest()
    {
        return $this->belongsTo(BookingInterest::class, 'resource_id');
    }

    public function interestWriteOff()
    {
        return $this->belongsTo(InterestWriteOff::class, 'resource_id');
    }

    public function installmentRequest()
    {
        return $this->belongsTo(InstallmentRequest::class, 'resource_id');
    }


    public function employer()
    {
        return $this->belongsTo(Employer::class, 'resource_id');
    }

    //Relation to users
    /*public function user()
    {
        return $this->belongsTo(\App\Models\Auth\User::class);
    }*/
    public function user()
    {
        return $this->morphTo();
    }

    public function resource()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    //Relation to  legacy receipts
    public function legacyReceipt()
    {
        return $this->belongsTo(\App\Models\Finance\Receipt\LegacyReceipt::class, 'resource_id');
    }

    /*Relation to payroll status change*/
    public function payrollStatusChange()
    {
        return $this->belongsTo(PayrollStatusChange::class, 'resource_id');
    }

    /*Payroll recovery*/
    public function payrollRecovery()
    {
        return $this->belongsTo(PayrollRecovery::class, 'resource_id');
    }

    /*Payroll bank updates*/
    public function payrollBankInfoUpdate()
    {
        return $this->belongsTo(PayrollBankInfoUpdate::class, 'resource_id');
    }

    /*Payroll run approval*/
    public function payrollRunApproval()
    {
        return $this->belongsTo(PayrollRunApproval::class, 'resource_id');
    }


    /*Payroll reconciliation*/
    public function payrollReconciliation()
    {
        return $this->belongsTo(PayrollReconciliation::class, 'resource_id');
    }

    /*Payroll beneficiary update*/
    public function payrollBeneficiaryUpdate()
    {
        return $this->belongsTo(PayrollBeneficiaryUpdate::class, 'resource_id');
    }


    /*Employer closure*/
    public function employerClosure()
    {
        return $this->belongsTo(EmployerClosure::class, 'resource_id');
    }

    public function archives()
    {
        return $this->hasMany(WfArchive::class)->orderByDesc('wf_archives.id');
    }

    public function oshAuditPeriod()
    {
        return $this->belongsTo(OshAuditPeriod::class, 'resource_id');
    }


    public function InvestmentBudget()
    {
        return $this->belongsTo(InvestmentBudget::class, 'resource_id');
    }

    public function AccrualNotificationReport()
    {
        return $this->belongsTo(AccrualNotificationReport::class, 'resource_id');
    }

    public function InvestigationPlan()
    {
        return $this->belongsTo(\App\Models\Operation\InvestigationPlan\InvestigationPlan::class, 'resource_id');
    }

    public function employerPayrollMerge()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Online\EmployerPayrollMerge::class, 'resource_id');
    }

    public function HspBillSummary()
    {
        return $this->belongsTo(\App\Models\Operation\Claim\HspBilling\HspBillSummary::class, 'resource_id');
    }

    public function ContribModification()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\ContributionModification\ContribModification::class, 'resource_id');
    }

    public function ContributionInterestRate()
    {
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\ContributionInterestRate::class, 'resource_id');
    }

    
}