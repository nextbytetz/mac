<?php

namespace App\Models\Reporting;

use App\Models\Reporting\Traits\Relationship\ReportRelationship;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    //
    use ReportRelationship;
    use Softdeletes;


    public function reportCategories()
    {
        return $this->belongsToMany(ReportCategory::class, "report_category_report")->withTimestamps();
    }

    public function getReportCategoriesLabelAttribute()
    {
        $categories = [];
        if ($this->reportCategories()->count() > 0) {
            foreach ($this->reportCategories as $category) {
                array_push($categories, $category->name);
            }
            return implode(", ", $categories);
        } else {
            return '<span class="tag tag-danger">'. trans('labels.general.none') . '</span>';
        }
    }


    /*Get Is new*/
    public function getIsNewAttribute()
    {
        $diff_days = Carbon::parse($this->created_at)->diffInDays(Carbon::now());
        if($diff_days < 45)
        {
            return true;
        }else{
            return false;
        }

    }

}
