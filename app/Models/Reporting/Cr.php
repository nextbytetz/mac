<?php

namespace App\Models\Reporting;

use App\Models\Reporting\Traits\BindsDynamically;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cr
 * @package App\Models\Reporting
 * @description Model for all Configurable reports, table name is set programmatically
 */
class Cr extends Model
{
    //use BindsDynamically;

    protected $guarded = [];
    public $timestamps = true;

    function set($array){
        foreach ($array as $key => $value){
            if ( property_exists ( $this , $key ) ){
                $this->$key= $value;
            }
        }
    }

}
