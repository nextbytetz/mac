<?php

namespace App\Models\Reporting;

use App\Models\Reporting\Traits\Relationship\ReportCategoryRelationship;
use Illuminate\Database\Eloquent\Model;

class ReportCategory extends Model
{
use ReportCategoryRelationship;

}