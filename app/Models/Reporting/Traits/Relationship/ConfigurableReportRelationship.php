<?php

namespace App\Models\Reporting\Traits\Relationship;

use App\Models\Reporting\ConfigurableReport;
use App\Models\Sysdef\Unit;

trait ConfigurableReportRelationship
{
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function childs()
    {
        return $this->hasMany(ConfigurableReport::class, 'parent_id');
    }

}