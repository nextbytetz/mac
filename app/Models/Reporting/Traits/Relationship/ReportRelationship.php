<?php

namespace App\Models\Reporting\Traits\Relationship;



/**
 * Class CaseRelationship
 *
 */
trait ReportRelationship {



    //Relation to Reportcategory
    public function reportCategory(){
        return $this->belongsTo(\App\Models\Reporting\ReportCategory::class);
    }

    public function reportType(){
        return $this->belongsTo(\App\Models\Reporting\ReportType::class);
    }




}
