<?php

namespace App\Models\Reporting;

use App\Models\Reporting\Traits\Relationship\ReportRelationship;
use Illuminate\Database\Eloquent\Model;

class ReportType extends Model
{
    use ReportRelationship;

}