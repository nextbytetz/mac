<?php

namespace App\Models\Reporting;

use Illuminate\Database\Eloquent\Model;

class ConfigurableReportType extends Model
{

    protected $guarded = [];
    public $timestamps = true;
}
