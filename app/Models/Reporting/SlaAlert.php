<?php

namespace App\Models\Reporting;

use Illuminate\Database\Eloquent\Model;

class SlaAlert extends Model
{
    protected $guarded = [];
    public $timestamps = true;

    public function cr()
    {
        return $this->belongsTo(ConfigurableReport::class, "configurable_report_id");
    }

}
