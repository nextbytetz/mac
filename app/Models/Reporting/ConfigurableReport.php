<?php

namespace App\Models\Reporting;

use App\Models\Reporting\Traits\Relationship\ConfigurableReportRelationship;
use Illuminate\Database\Eloquent\Model;

class ConfigurableReport extends Model
{
    use ConfigurableReportRelationship;

    protected $guarded = [];
    public $timestamps = true;

}
