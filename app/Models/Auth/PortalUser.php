<?php

namespace App\Models\Auth;

use Illuminate\Notifications\Notifiable;
use App\Models\Auth\Attribute\PortalUserAttribute;
use App\Models\Auth\Relationship\PortalUserRelationship;
use Illuminate\Database\Eloquent\Model;

class PortalUser extends Model
{
    use PortalUserRelationship, PortalUserAttribute, Notifiable;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * @return string
     */
    public function getNameFormattedAttribute()
    {
        $return = "";
        //$return = ucfirst($this->firstname) . " " . ucfirst($this->middlename) . " " . ucfirst($this->lastname);
        $return = ucfirst($this->firstname) . " " . ucfirst($this->lastname);
        return $return;
    }

}
