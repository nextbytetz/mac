<?php

namespace App\Models\Auth\Attribute;

use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Repositories\Backend\Operation\Claim\NotificationContributionTrackRepository;
use Illuminate\Support\Facades\DB;

trait UserAttribute
{

    public function getRoleLabelAttribute() {
        $roles = [];
        if ($this->roles()->count() > 0) {
            foreach ($this->roles as $role) {
                array_push($roles, $role->name);
            }
            return implode(", ", $roles);
        } else {
            return '<span class="tag tag-danger">'. trans('labels.general.none') . '</span>';
        }
    }

    public function getNameAttribute() {
        return ucfirst($this->firstname) . " " . ucfirst($this->middlename) . " " . ucfirst($this->lastname);
    }

    public function getAvailableLabelAttribute()
    {

    }

    public function getAvailableStatusAttribute()
    {
        $return = "";
        if ($this->available) {
            //is available
            $return = "<span class='tag tag-success white_color'>Available</span>";
        } else {
            $return .= "<span class='tag tag-warning white_color'>Unavailable</span>";
            if ($this->substitution()->count()) {
                $return .= "&nbsp;&nbsp;<span class='tag tag-default white_color'>Substituted By : {$this->substitution->userSubstituting->name} </span>";
            }
        }
        return $return;
    }

    /* Get number of approved / verified contribution by compliance staff */
    public function  verifiedContributions()
    {
        return $this->wfTracks()->where('status', 1)->where('wf_definition_id', 12)->count();
    }

    /* Get number of issued certificates by this user */
    public function certificatesIssued()
    {
        return $this->employerCertificateIssues()->count();
    }

    public function pendingNotificationContributionTrackCount()
    {
        $notificationContributionTrack  = new NotificationContributionTrackRepository();
        return $notificationContributionTrack->getPendingCount();
    }

    /*Get no of relation employers*/
    public function getNoOfRelationEmployers($staff_employer_allocation_id)
    {
        $no_of_employers = DB::table('staff_employer')->where('user_id', $this->id)->where('staff_employer.isactive', 1)->where('staff_employer_allocation_id', $staff_employer_allocation_id)->count();
        return $no_of_employers;
    }

    /*Get name with no of allocated employers*/
    public function getNameWithNoOfAllocatedEmployersAttribute()
    {
        return $this->getNameAttribute() .  ' (' . $this->relationEmployers()->where('staff_employer.isactive', 1)->where('staff_employer.isbonus',0)->count() . ')';
    }


    public function getQueryStaffEmployerAllocationAttribute()
    {
        $staff_employer_allocations = StaffEmployerAllocation::query()
        ->join('staff_employer as s', 's.staff_employer_allocation_id', 'staff_employer_allocations.id')
        ->where('s.user_id', $this->id)
        ->where('s.isactive',1);
        return $staff_employer_allocations;
    }

    public function getSubordinatesIdsAttribute()
    {
        $subordinates_ids = $this->where('report_to',$this->id)->pluck('id')->all();
        return $subordinates_ids;
    }

    public function getSupervisorIdAttribute()
    {
        $supervisor_ids = $this->where('id',$this->report_to)->pluck('id')->first();
        return $supervisor_ids;
    }

}