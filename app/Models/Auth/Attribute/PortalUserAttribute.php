<?php

namespace App\Models\Auth\Attribute;

trait PortalUserAttribute
{

    public function getUsernameAttribute()
    {
        return $this->name;
    }

}