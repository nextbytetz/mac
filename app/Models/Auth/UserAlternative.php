<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class UserAlternative extends Model
{
    protected $guarded = [];

    public function userSet()
    {
        return $this->belongsTo(User::class, "user_set_id");
    }

}
