<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserSub extends Model
{

    protected $guarded = [];

    public function userSet()
    {
        return $this->belongsTo(User::class, "user_set_id");
    }

    public function userRevoked()
    {
        return $this->belongsTo(User::class, "user_revoke_id");
    }

    public function userSubstituting()
    {
        return $this->belongsTo(User::class, "sub");
    }

    public function getFromDateLabelAttribute()
    {
        return  Carbon::parse($this->from_date)->format('d-M-Y g:i:s A');
    }

    public function getToDateLabelAttribute()
    {
        return  Carbon::parse($this->to_date)->format('d-M-Y g:i:s A');
    }

}
