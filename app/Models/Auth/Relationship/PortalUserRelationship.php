<?php

namespace App\Models\Auth\Relationship;

use App\Models\Operation\Compliance\Member\OnlineEmployerVerification;
use App\Models\Sysdef\CodeValuePortal;
use App\Models\Workflow\WfTrack;

trait PortalUserRelationship
{

    /**
     * @return mixed
     */
    public function wfTracks()
    {
        //return $this->hasMany(WfTrack::class);
        return $this->morphMany(WfTrack::class, 'user');
    }

    public function verifications()
    {
        return $this->morphMany(OnlineEmployerVerification::class, "user");
    }

    public function codeValuePortal()
    {
        return $this->belongsTo(CodeValuePortal::class, "user_type_cv_id", "id");
    }

}