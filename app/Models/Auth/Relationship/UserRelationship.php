<?php

namespace App\Models\Auth\Relationship;

use App\Models\Access\Role;
use App\Models\Access\Permission;
use App\Models\Auth\User;
use App\Models\Auth\UserAlternative;
use App\Models\Auth\UserSub;
use App\Models\Location\Region;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Compliance\Inspection\EmployerInspectionTask;
use App\Models\Operation\Compliance\Inspection\Inspection;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerCertificateIssue;
use App\Models\Operation\Compliance\Member\UnregisteredEmployer;
use App\Models\Operation\Compliance\Member\UnregisteredFollowupInspection;
use App\Models\Operation\Compliance\Member\UnregisteredFollowupInspectionTask;
use App\Models\Sysdef\Office;
use App\Models\Task\Checker;
use App\Models\Workflow\WfTrack;
use App\Models\Workflow\WfDefinition;
use App\Models\Sysdef\Unit;
use App\Models\Sysdef\Designation;
use App\Models\Finance\Receipt\Receipt;
use App\Models\Finance\DishonouredCheque;
use App\Models\Operation\Compliance\Member\Contribution;
use App\Models\Operation\ClaimFollowup\ClaimFollowup;


trait UserRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function permissions() {
        return $this->belongsToMany(Permission::class)->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function workflowDefinitions()
    {
        return $this->belongsToMany(WfDefinition::class)->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function wfTracks()
    {
        //return $this->hasMany(WfTrack::class);
        return $this->morphMany(WfTrack::class, 'user');
    }

    /**
     * @return mixed
     */
    public function designationTable()
    {
        return $this->belongsTo(Designation::class, 'designation_id');
    }

    /**
     * @return mixed
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    /**
     * Marked to be removed in future.
     * @return mixed
     */
    public function receipts()
    {
        return $this->hasMany(Receipt::class);
    }

    /**
     * Marked to be removed in future.
     * @return mixed
     */
    public function receiptOnlyTrashed()
    {
        return $this->hasMany(Receipt::class, 'cancel_user_id', 'id')->onlyTrashed();
    }

    public function receiptWithTrashed()
    {
        return $this->hasMany(Receipt::class)->withTrashed();
    }

    /**
     * Marked to be removed in future.
     * @return mixed
     * dishonoured cheques
     */
    public function dishonouredCheques()
    {
        return $this->hasMany(DishonouredCheque::class,'dishonour_user_id','id');
    }

    /**
     * Marked to be removed in future.
     * @return mixed
     */
    public function contributions()
    {
        return $this->hasMany(Contribution::class);
    }

    public function inspectionTasks()
    {
        return $this->belongsToMany(InspectionTask::class, "employer_inspection_task", "allocated", "inspection_task_id")->withTimestamps();
    }

    public function inspections()
    {
        return $this->belongsToMany(Inspection::class)->withTimestamps();
    }

    public function employerInspectionTasks()
    {
        return $this->hasMany(EmployerInspectionTask::class, "allocated");
    }

    /*Relation to unregistered followup inspection*/
    public function unregisteredFollowupInspectionTasks()
    {
        return $this->belongsToMany(UnregisteredFollowupInspectionTask::class);
    }

    public function unregisteredFollowupInspections()
    {
        return $this->belongsToMany(UnregisteredFollowupInspection::class);
    }

    public function assignedUnregisteredEmployers()
    {
        return $this->hasMany(UnregisteredEmployer::class, 'assign_to', 'id');
    }


    public function employerCertificateIssues()
    {
        return $this->hasMany(EmployerCertificateIssue::class);
    }

    public function substitution()
    {
        return $this->belongsTo(UserSub::class, "user_sub_id");
    }

    public function substitutions()
    {
        return $this->hasMany(UserSub::class);
    }

    /**
     * @return mixed
     * @description List of users to work on user's workflows when a user is substituting other user.
     */
    public function alternatives()
    {
        return $this->hasMany(UserAlternative::class);
    }

    public function alternates()
    {
        return $this->hasMany(UserAlternative::class, "alternative");
    }

    public function substitutingUsers()
    {
        return $this->hasMany(User::class, "sub");
    }

    public function substitutingUser()
    {
        return $this->belongsTo(User::class, "sub");
    }

    public function investigators()
    {
        return $this->belongsToMany(NotificationReport::class, "notification_investigators");
    }

    public function checkers()
    {
        return $this->hasMany(Checker::class);
    }


    /*Staff employers*/
    public function relationEmployers()
    {
        return $this->belongsToMany(Employer::class, "staff_employer")->withPivot(['user_id', 'staff_employer_allocation_id']);
    }


    public function office()
    {
        return $this->belongsTo(Office::class);
    }

    /*Relation to staff region*/
    public function allocatedRegions()
    {
        return $this->belongsToMany(Region::class, 'staff_region', 'user_id', 'region_id');
    }

    public function claimFollowup()
    {
        return $this->hasMany(ClaimFollowup::class);
    }


    public function subordinates()
    {
        return $this->hasMany(User::class,'report_to');
    }


    public function supervisor()
    {
        return $this->belongsTo(User::class, "report_to");
    }

    public function employerPayrollMerges()
    {
        return $this->hasMany(\App\Models\Operation\Compliance\Member\Online\EmployerPayrollMerge::class);
    }

}