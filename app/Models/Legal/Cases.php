<?php

namespace App\Models\Legal;

use App\Models\Legal\Traits\Attribute\CaseAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Legal\Traits\Relationship\CaseRelationship;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Cases extends Model implements AuditableContract
{
    use  Auditable, CaseRelationship, CaseAttribute;

    protected $table = 'cases';

    protected $fillable = ['status', 'parent_id', 'title', 'number', 'complainant', 'complainant_address', 'district_id', 'location', 'court_category_id', 'court', 'fee', 'liability', 'respondent', 'respondent_address', 'fact', 'filling_date', "user_id", "liability_type", "contentious_type"];

    protected $guarded = [];
    public $timestamps = true;

}
