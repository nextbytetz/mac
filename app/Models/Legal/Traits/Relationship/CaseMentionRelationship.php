<?php

namespace App\Models\Legal\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Legal\CasePersonnel;
use App\Models\Legal\Cases;
use App\Models\Legal\Lawyer;

trait CaseMentionRelationship
{

    public function casePersonnel()
    {
        return $this->belongsTo(CasePersonnel::class);
    }

    public function cases()
    {
        return $this->belongsTo(Cases::class, "case_id");
    }

    public function lawyer()
    {
        return $this->belongsTo(Lawyer::class);
    }

}