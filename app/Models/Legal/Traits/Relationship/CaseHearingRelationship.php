<?php

namespace App\Models\Legal\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Legal\CasePersonnel;
use App\Models\Legal\Cases;
use App\Models\Legal\Lawyer;

/**
 * Class CaseHearingRelationship
 * @package App\Models\Legal\Traits\Relationship
 */
trait CaseHearingRelationship {

    public function cases()
    {
        return $this->belongsTo(Cases::class, "case_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function lawyer()
    {
        return $this->belongsTo(Lawyer::class);
    }

    public function casePersonnel()
    {
        return $this->belongsTo(CasePersonnel::class);
    }

}
