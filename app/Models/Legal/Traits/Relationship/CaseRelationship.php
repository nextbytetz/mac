<?php

namespace App\Models\Legal\Traits\Relationship;



/**
 * Class CaseRelationship
 *
 */
trait CaseRelationship {



    //Relation to casehearings
    public function caseHearings(){
        return $this->hasMany(\App\Models\Legal\CaseHearing::class,'case_id', 'id');
    }





}
