<?php

namespace App\Models\Legal\Traits\Relationship;

use App\Models\Legal\CaseMention;
use App\Models\Legal\CaseHearing;
use App\Models\Operation\Compliance\Member\Employer;

/**
 * Class LawyerRelationship
 * @package App\Models\Legal\Traits\Relationship
 */
trait LawyerRelationship {

    /**
     * @return mixed
     */
    public function caseHearings(){
        return $this->hasMany(CaseHearing::class);
    }

    /**
     * @return mixed
     */
    public function caseMentionings()
    {
        return $this->hasMany(CaseMention::class);
    }

    /**
     * @return mixed
     */
    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }

}
