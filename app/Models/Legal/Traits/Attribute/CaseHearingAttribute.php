<?php

namespace App\Models\Legal\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class CaseHearingAttribute
 */
trait CaseHearingAttribute {

    /**
     * @return string
     */
    public function getStatusLabelAttribute() {
        switch ($this->status) {
            case 0:
                $return = "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.legal.unheard') . "'>" . trans('labels.backend.legal.unheard') . "</span>";
                break;
            case 1:
                $return = "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.legal.heard') . "'>" . trans('labels.backend.legal.heard') . "</span>";
                break;
            case 2:
                $return = "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.legal.cancelled') . "'>" . trans('labels.backend.legal.cancelled') . "</span>";
                break;
        }
        return $return;
    }

    public function isHeard() {
        return $this->status == 1;
    }

    public function getFullNameAttribute(){
        return ucfirst($this->firstname) . " " . ucfirst($this->middlename) . " " . ucfirst($this->lastname);
    }

    public function getHearingDateLabelAttribute()
    {
        return  Carbon::parse($this->hearing_date)->format('d-M-Y');
    }

    public function getHearingTimeLabelAttribute()
    {
        return Carbon::parse($this->hearing_time)->format('h:i A');
    }

    public function getFeeLabelAttribute()
    {
        if (is_null($this->fee)) {
            $return = "-";
        } else {
            $return = number_format($this->fee , 2 , '.' , ',' );
        }
        return $return;
    }

    public function getFillingDateLabelAttribute()
    {
        if (is_null($this->filling_date)) {
            $return = "-";
        } else {
            $return = Carbon::parse($this->filling_date)->format('d-M-Y');
        }
        return $return;
    }

    public function getStatusDescriptionLabelAttribute()
    {
        if (is_null($this->status_description)) {
            $return = "-";
        } else {
            $return = $this->status_description;
        }
        return $return;
    }

}
