<?php

namespace App\Models\Legal\Traits\Attribute;

use Carbon\Carbon;

/**
 * Class LawyerAttribute
 * @package App\Models\Legal\Traits\Attribute
 */
trait LawyerAttribute {

    /**
     * @return string
     */
    public function getNameLabelAttribute()
    {
        return ucfirst($this->firstname) . " " . ucfirst($this->middlename) . " " . ucfirst($this->lastname);
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        if (is_null($this->employer_id)) {
            $return = $this->getNameLabelAttribute();
        } else {
            if (($this->employer()->count())) {
                $return = $this->employer->name;
            } else {
                $return = "-";
            }
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getExternalIdLabelAttribute()
    {
        if (is_null($this->external_id)) {
            $return = "-";
        } else {
            $return = $this->external_id;
        }
        return $return;
    }

}
