<?php

namespace App\Models\Legal\Traits\Attribute;

use Carbon\Carbon;

trait CaseAttribute
{
    public function getFillingDateFormattedAttribute()
    {
        if (is_null($this->filling_date)) {
            $return = "-";
        } else {
            $return = Carbon::parse($this->filling_date)->format('d-M-Y');
        }
        return  $return;
    }

    public function getFeeFormattedAttribute()
    {
        if (is_null($this->fee)) {
            $return = "-";
        } else {
            $return = number_format( $this->fee , 2 , '.' , ',' );
        }
        return  $return;
    }

    public function getStatusLabelAttribute()
    {
        if ($this->status == 0)
            return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.open') . "'>" . trans('labels.general.open') . "</span>";
        return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.closed') . "'>" . trans('labels.general.closed') . "</span>";
    }

    public function getLiabilityTypeLabelAttribute()
    {
        switch ($this->liability_type) {
            case 1:
                $return = "Contentious";
                break;
            case 2:
                $return = "Non Contentious";
                break;
            default:
                $return = "Non Contentious";
        }
        return $return;
    }

    public function getContentiousTypeLabelAttribute()
    {
        switch ($this->contentious_type) {
            case 1:
                $return = "Obliged to Pay";
                break;
            case 2:
                $return = "To be Paid";
                break;
            default:
                $return = "-";
        }
        return $return;
    }

    public function getLiabilityLabelAttribute()
    {
        if (is_null($this->liability)) {
            $return = "-";
        } else {
            $return = $this->liability;
        }
        return $return;
    }

}