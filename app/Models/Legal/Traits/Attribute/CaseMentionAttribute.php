<?php

namespace App\Models\Legal\Traits\Attribute;

use Carbon\Carbon;

trait CaseMentionAttribute
{

    public function getMentionDateLabelAttribute()
    {
        return  Carbon::parse($this->mention_date)->format('d-M-Y');
    }

    public function getMentionTimeLabelAttribute()
    {
        return Carbon::parse($this->mention_date)->format('h:i A');
    }

    public function getDescriptionLabelAttribute()
    {
        if (is_null($this->description)) {
            $return = "-";
        } else {
            $return = $this->description;
        }
        return $return;
    }

    public function getFullNameAttribute(){
        return ucfirst($this->firstname) . " " . ucfirst($this->middlename) . " " . ucfirst($this->lastname);
    }

}