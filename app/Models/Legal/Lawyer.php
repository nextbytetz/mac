<?php

namespace App\Models\Legal;

use App\Models\Legal\Traits\Attribute\LawyerAttribute;
use App\Models\Legal\Traits\Relationship\LawyerRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Lawyer extends Model implements AuditableContract
{

    use Auditable, LawyerRelationship, LawyerAttribute;

    public $fillable = ['employer_id', 'firstname', 'middlename', 'lastname', 'external_id'];

}
