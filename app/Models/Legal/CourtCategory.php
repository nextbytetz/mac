<?php

namespace App\Models\Legal;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourtCategory extends Model
{


    protected $table = 'court_categories';

    protected $guarded = [];
}
