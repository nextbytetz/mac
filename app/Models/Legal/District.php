<?php

namespace App\Models\Legal;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Model
{
    use SoftDeletes;



    protected $guarded = [];
}
