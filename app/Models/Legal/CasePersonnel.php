<?php

namespace App\Models\Legal;

use Illuminate\Database\Eloquent\Model;

class CasePersonnel extends Model
{

    protected $fillable = ['name'];

    protected $guarded = [];
    public $timestamps = true;

}