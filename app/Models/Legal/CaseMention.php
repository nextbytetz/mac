<?php

namespace App\Models\Legal;


use App\Models\Legal\Traits\Attribute\CaseMentionAttribute;
use App\Models\Legal\Traits\Relationship\CaseMentionRelationship;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CaseMention extends Model implements AuditableContract
{

    use Auditable, CaseMentionAttribute, CaseMentionRelationship;

    protected $fillable = ['case_personnel_id', 'firstname', 'middlename', 'lastname', 'case_id', 'mention_date', 'mention_time', 'lawyer_id', 'description', 'user_id'];

    protected $guarded = [];
    public $timestamps = true;

}