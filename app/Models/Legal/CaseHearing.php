<?php

namespace App\Models\Legal;

use App\Models\Legal\Traits\Attribute\CaseHearingAttribute;
use App\Models\Legal\Traits\Relationship\CaseHearingRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CaseHearing extends Model implements AuditableContract
{

    use Auditable, CaseHearingRelationship, CaseHearingAttribute;

    protected $table = 'case_hearings';

    protected $fillable = ['case_personnel_id', 'firstname', 'middlename', 'lastname', 'case_id', 'hearing_date', 'hearing_time', 'lawyer_id', 'fee', 'filling_date', 'status', 'status_description', 'user_id'];

    protected $guarded = [];
    public $timestamps = true;
}
