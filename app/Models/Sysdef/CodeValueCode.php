<?php

namespace App\Models\Sysdef;

use Illuminate\Database\Eloquent\Model;

class CodeValueCode extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'code_value_code';

    public function code()
    {
        return $this->belongsTo(Code::class, "code_id");
    }

}
