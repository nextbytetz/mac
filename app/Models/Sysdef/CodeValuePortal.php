<?php

namespace App\Models\Sysdef;

use App\Models\Task\Checker;
use Illuminate\Database\Eloquent\Model;

class CodeValuePortal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'code_values';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function code()
    {
        return $this->belongsTo(CodePortal::class, "code_id");
    }

    public function checkers()
    {
        return $this->hasMany(Checker::class, "checker_category_cv_id");
    }

}
