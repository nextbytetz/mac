<?php

namespace App\Models\Sysdef;

use Illuminate\Database\Eloquent\Model;

class CodePortal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'codes';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';

}
