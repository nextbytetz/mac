<?php

namespace App\Models\Notifications;

use App\Models\Auth\PortalUser;
use Illuminate\Database\Eloquent\Model;

class Text extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'phone', 'message', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(PortalUser::class);
    }

}
