<?php

namespace App\Models\Notifications;

use App\Models\Auth\User;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Claim\DocumentResource;
use App\Models\Sysdef\CodeValue;
use App\Models\Task\Checker;
use App\Models\Workflow\WfModule;
use App\Models\Workflow\WfTrack;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Letter extends Model implements AuditableContract
{
    use Auditable;

    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    public function type()
    {
        return $this->belongsTo(CodeValue::class, "cv_id");
    }

    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource');
    }

    public function logs()
    {
        return $this->hasMany(LetterLog::class);
    }

    public function codeValue()
    {
        return $this->belongsTo(CodeValue::class, "cv_id");
    }

    public function wfModule()
    {
        return $this->belongsTo(WfModule::class, "wf_module_id");
    }

    public function getStatusLabelAttribute() {
        if ($this->approved) {
            return "<span class='tag tag-success white_color'>Approved</span>";
        } else {
            return "<span class='tag tag-warning white_color'>Pending Approval</span>";
        }
    }

    public function getPrintStatusLabelAttribute() {
        if ($this->isprinted) {
            return "<span class='tag tag-success white_color'>Printed</span>";
        } else {
            return "<span class='tag tag-warning white_color'>Not Printed</span>";
        }
    }

    /**
     * @return mixed
     */
    public function getLetterDateLabelAttribute()
    {
        $date = $this->letter_date ?: $this->wf_done_date ?: $this->updated_at;
        return Carbon::parse($date)->format("Y-m-d");
    }

    public function getResourceNameAttribute()
    {
        $return = $this->reference;
        return $return;
    }

    public function resource()
    {
        return $this->morphTo();
    }

    public function stage()
    {
        return $this->belongsTo(CodeValue::class, "staging_cv_id");
    }

    /**
     * @return mixed
     */
    public function stages()
    {
        //return $this->hasMany(NotificationStage::class);
        return $this->belongsToMany(CodeValue::class, "letter_stages", "letter_id", "staging_cv_id")->whereIn("code_id", [40])->withPivot('id', 'comments', 'allow_repeat', 'attended', 'require_attend', 'isdeleted')->withTimestamps()->orderByDesc("letter_stages.id");
    }

    public function allocatedUser()
    {
        return $this->belongsTo(User::class, "allocated");
    }

    public function checkers()
    {
        return $this->morphMany(Checker::class, "resource");
    }

    public function documents()
    {
        return $this->morphMany(DocumentResource::class, 'resource');
    }


}
