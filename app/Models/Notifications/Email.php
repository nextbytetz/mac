<?php

namespace App\Models\Notifications;

use App\Models\Auth\PortalUser;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'email', 'title'
    ];

    public function user()
    {
        return $this->belongsTo(PortalUser::class);
    }

}
