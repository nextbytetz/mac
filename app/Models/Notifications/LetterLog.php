<?php

namespace App\Models\Notifications;

use Illuminate\Database\Eloquent\Model;

class LetterLog extends Model
{

    public $timestamps = true;

    /**
     * @var array
     */
    protected $guarded = [];
}
