<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017-07-09
 * Time: 22:16
 */
namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentVoucherType extends Model
{
    //
    use Softdeletes;
}
