<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class FinYear extends Model
{
    protected $guarded = [];
    public $timestamps = true;
}
