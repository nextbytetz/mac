<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;
use App\Models\Finance\Traits\Relationship\CurrencyRelationship;
use App\Models\Finance\Traits\Attribute\CurrencyAttribute;

class Currency extends Model
{
    //
    use CurrencyRelationship, CurrencyAttribute;

    protected $guarded = [];
    public $timestamps = true;
}
