<?php

namespace App\Models\Finance;

use App\Models\Finance\Traits\Relationship\PayrollProcRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PayrollProc extends Model  implements AuditableContract
{
    //
    use Softdeletes, Auditable, PayrollProcRelationship;
    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];
}
