<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class BillMonth extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';

}
