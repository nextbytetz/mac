<?php

namespace App\Models\Finance;

use App\Models\Finance\Receipt\Receipt;
use App\Models\Operation\Compliance\Member\Employer;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Thirdparty extends Model implements AuditableContract
{

    use Auditable;

    public $fillable = ['employer_id', 'firstname', 'middlename', 'lastname', 'external_id', 'isactive'];

    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }

    public function receipts()
    {
        return $this->hasMany(Receipt::class);
    }

    /**
     * @return string
     */
    public function getNameLabelAttribute()
    {
        return ucfirst($this->firstname) . " " . ucfirst($this->middlename) . " " . ucfirst($this->lastname);
    }

    /**
     * @return string
     */
    public function getNameFormattedAttribute()
    {
        if (is_null($this->employer_id)) {
            $return = $this->getNameLabelAttribute();
        } else {
            if (($this->employer()->count())) {
                $return = $this->employer->name;
            } else {
                $return = "-";
            }
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getExternalIdLabelAttribute()
    {
        if (is_null($this->external_id)) {
            $return = "-";
        } else {
            $return = $this->external_id;
        }
        return $return;
    }

}
