<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-May-17
 * Time: 4:23 PM
 */

namespace App\Models\Finance\Receivable;

use Illuminate\Database\Eloquent\Model;

class BookingGroup extends  Model
{
    protected $guarded = [];
    public $timestamps = true;

}