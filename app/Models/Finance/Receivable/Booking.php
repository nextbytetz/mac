<?php

namespace App\Models\Finance\Receivable;

use Illuminate\Database\Eloquent\Model;
use App\Models\Finance\Traits\Attribute\BookingAttribute;
use App\Models\Finance\Traits\Relationship\BookingRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Booking extends Model implements AuditableContract
{
    //
    //
    use BookingAttribute,SoftDeletes,BookingRelationship,Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}
