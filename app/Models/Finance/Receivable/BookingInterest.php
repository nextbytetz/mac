<?php

namespace App\Models\Finance\Receivable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Finance\Traits\Relationship\BookingInterestRelationship;
use App\Models\Finance\Traits\Attribute\BookingInterestAttribute;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class BookingInterest extends Model implements AuditableContract
{
    //
    //
    use BookingInterestRelationship,BookingInterestAttribute,SoftDeletes,Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];
}
