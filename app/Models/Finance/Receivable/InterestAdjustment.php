<?php

namespace App\Models\Finance\Receivable;

use App\Models\Finance\Traits\Attribute\InterestAdjustmentAttribute;
use App\Models\Finance\Traits\Attribute\InterestWriteOffAttribute;
use App\Models\Finance\Traits\Relationship\InterestAdjustmentRelationship;
use App\Models\Finance\Traits\Relationship\InterestWriteOffRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InterestAdjustment extends Model
{
    use  SoftDeletes, InterestAdjustmentRelationship, InterestAdjustmentAttribute;

    protected $guarded = [];
    public $timestamps = true;
}
