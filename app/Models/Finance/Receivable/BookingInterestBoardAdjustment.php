<?php

namespace App\Models\Finance\Receivable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Finance\Traits\Relationship\BookingInterestRelationship;
use App\Models\Finance\Traits\Attribute\BookingInterestAttribute;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class BookingInterestBoardAdjustment extends Model
{
    //
    //

    protected $guarded = [];
    public $timestamps = true;


}
