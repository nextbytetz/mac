<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-May-17
 * Time: 4:22 PM
 */

namespace App\Models\Finance\Receivable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingGroupEmployer extends Model
{


    use SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;


}