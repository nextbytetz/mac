<?php

namespace App\Models\Finance\Receivable;

use App\Models\Finance\Traits\Attribute\InterestWriteOffAttribute;
use App\Models\Finance\Traits\Relationship\InterestWriteOffRelationship;
use App\Models\Operation\Compliance\Member\Employer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NonBookedBooking extends Model
{
    use  SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;



    /**
     * RELATIONSHIP
     */
    public function nonBookedBookingInterest(){
        return $this->hasOne(NonBookedBookingInterest::class);
    }

    public function employer(){
        return $this->belongsTo(Employer::class);
    }

}
