<?php

namespace App\Models\Finance\Receivable\Portal;

use App\Models\Finance\Receivable\Portal\Traits\Relationship\ContributionArrearRelationship;
use Illuminate\Database\Eloquent\Model;
use App\Models\Finance\Traits\Attribute\BookingAttribute;
use App\Models\Finance\Traits\Relationship\BookingRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ContributionArrear extends Model
{

    use ContributionArrearRelationship;
    //
    //

    protected $guarded = [];
    public $timestamps = true;

    protected $table = 'portal.contribution_arrears';

}
