<?php

namespace App\Models\Finance\Receivable\Portal\Traits\Relationship;

use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\InterestWriteOff;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Finance\Receivable\Portal\ContributionArrear;
use App\Models\Operation\Compliance\Member\EmployerContribution;
use Carbon\Carbon;

/**
 * Class BookingRelationship
 * @package
 */
trait ContributionArrearRelationship {


    /*Relation to main booking*/
    public function mainBooking()
    {
        $rcv_date_parsed = Carbon::parse($this->contrib_month);
        return $this->belongsTo(Booking::class, 'employer_id', 'employer_id')->whereRaw("DATE_PART('month', main.bookings.rcv_date::date) =  ? and DATE_PART('year', main.bookings.rcv_date::date) =  ? ", [$rcv_date_parsed->format('m'), $rcv_date_parsed->format('Y')]);

    }

}
