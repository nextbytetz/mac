<?php

namespace App\Models\Finance\Receivable;

use App\Models\Finance\Traits\Attribute\InterestWriteOffAttribute;
use App\Models\Finance\Traits\Relationship\InterestWriteOffRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NonBookedBookingInterest extends Model
{
    use  SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;



    /**
     * RELATIONSHIP
     */
    public function nonBookedBooking(){
        return $this->belongsTo(NonBookedBooking::class);
    }



}
