<?php

namespace App\Models\Finance\Receivable;

use App\Models\Finance\BankBranch;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Finance\Traits\Relationship\BookingInterestRelationship;
use App\Models\Finance\Traits\Attribute\BookingInterestAttribute;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class BookingInterestRefund extends Model implements AuditableContract
{
    //
    //
    use SoftDeletes,Auditable;

    protected $guarded = [];
    public $timestamps = true;



    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


    /*  Relationship to booking interests*/
    public function bookingInterests()
    {
        return $this->hasMany(BookingInterest::class);
    }

    public function bankBranch()
    {
        return $this->belongsTo(BankBranch::class);
    }

    /*Attributes*/

    public function getEmployer()
    {
        $employers = new EmployerRepository();
       $employer =  $employers->query()->whereHas('bookingInterests', function($query)
        {
            $query->whereHas('bookingInterestRefund', function($query){
                $query->where('id', $this->id);
            });
        })->first();
       return $employer;
    }


    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

    public function getAmountFormattedAttribute() {
        return number_format($this->amount , 2 , '.' , ',' );
    }

    public function getStatusLabelAttribute()
    {
        if ($this->isApproved())
            return "<span class='tag tag-success success_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.complete') . "'>" . '.' . "</span>";
        return "<span class='tag tag-warning warning_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.incomplete') . "'>" . '.' . "</span>";
    }


    public function isApproved()
    {
        return $this->wf_done == 1;
    }

}
