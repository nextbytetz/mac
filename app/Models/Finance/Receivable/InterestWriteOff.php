<?php

namespace App\Models\Finance\Receivable;

use App\Models\Finance\Traits\Attribute\InterestWriteOffAttribute;
use App\Models\Finance\Traits\Relationship\InterestWriteOffRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InterestWriteOff extends Model
{
    use  InterestWriteOffRelationship, SoftDeletes, InterestWriteOffAttribute;

    protected $guarded = [];
    public $timestamps = true;
}
