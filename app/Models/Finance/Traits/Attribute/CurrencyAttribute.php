<?php

namespace App\Models\Finance\Traits\Attribute;

/**
 * Class CurrencyAttribute
 */
trait CurrencyAttribute {

    /**
     * @return string
     */
    public function getEditButtonAttribute() {

        return '<a href="' . route('backend.finance.currency.edit', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
//        return '<a href="""' . route('backend.finance.currency.destroy', $this->id) . '" class="btn btn-secondary">Edit</a>';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute() {

        return '<a href="' . route('backend.finance.currency.destroy', $this->id) . '" class="btn btn-xs btn-danger delete_button" data-trans-button-cancel="' . trans('buttons.general.cancel') . '" data-trans-button-confirm="' . trans('buttons.general.confirm') . '" data-trans-title="' . trans('labels.general.warning') . '" data-trans-text="' . trans('strings.backend.general.delete_message') . '" data-method="delete" ><i class="icon fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';




    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
        return $this->getEditButtonAttribute() .
        $this->getDeleteButtonAttribute();
//        return $this->getEditButtonAttribute();
    }

}
