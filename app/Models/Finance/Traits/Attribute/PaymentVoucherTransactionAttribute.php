<?php

namespace App\Models\Finance\Traits\Attribute;

use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;


/**
 * Class PaymentVoucherAttribute
 */
trait PaymentVoucherTransactionAttribute{




    public function getAmountFormattedAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }

    /**
     * @return mixed
     * Get Name of compensated entity
     * @deprecated
     */
    public function getCompensatedEntityNameAttribute(){
        //employer
        if ($this->getMemberType() == 1){
            return $this->employer->name;
        } elseif ($this->getMemberType() == 3){
            return $this->insurance->name;
        }elseif($this->getMemberType() == 2){
            return $this->employee->name;
        }elseif($this->getMemberType() == 4){
            return $this->dependent->name;
        }
    }


    // Get Benefit resource id for this voucher
    public function getBenefitResourceAttribute(){
        //compensation
        $benefit_types = new BenefitTypeRepository();
        if ($this->benefit_type_id == $benefit_types->getInterestRefund())
        {
            return 'Refund No.' . $this->benefit_resource_id;
        }else {
            return 'Claim No.' . $this->benefit_resource_id;
        }


    }




    public function getMemberType(){
        return $this->member_type_id;
    }

    public function getPaymentVoucherType(){
        return $this->payment_voucher_type_id;
    }



}