<?php

namespace App\Models\Finance\Traits\Attribute;

use App\Repositories\Backend\Notifications\LetterRepository;
use App\Services\Receivable\CalculateInterest;
use Carbon\Carbon;
/**
 * Class ReceiptAttribute
 */
trait InterestAdjustmentAttribute {


    public function getResourceNameAttribute()
    {
        return $this->receipt->employer->name;
    }

    /**
     * @return bool
     * LETTER functions-----------start------------
     */
    /*Status to check if letter is initiated*/
    public function getStatusIfLetterInitiatedForAdjustAttribute()
    {
        $check = $this->adjustmentResponseLetter()->where('isinitiated', '<>', 0)->count();
        if ($check > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*Get Letter reference title attribute*/
    public function getLetterReferenceAttribute($lang = 'en')
    {
        $letter_ref = ($lang == 'en') ? 'INTEREST ADJUSTMENT' : 'MAOMBI YA KUSAHIHISHA TOZO';
        return  $letter_ref;
    }
    /*Get Response Letter reference title attribute*/
    public function getLetterReferenceNoAttribute()
    {
        $default = 'AB/INT/000/';
        return (new LetterRepository())->getLetterReferenceNo('CLIEMPINTEADJ', $default);
    }


    /*get Folio no*/
    public function getLetterFolioNoAttribute()
    {
        return (new LetterRepository())->getLetterFolioNo('CLIEMPINTEADJ');
    }

}
