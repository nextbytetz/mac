<?php

namespace App\Models\Finance\Traits\Attribute;

use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Services\Receivable\CalculateInterest;
use Carbon\Carbon;
/**
 * Class ReceiptAttribute
 */
trait BookingInterestAttribute {

    /*
     * Formatting tables column
     */

    public function getMissMonthFormattedAttribute() {
        return  Carbon::parse($this->miss_month)->format('M-Y');
    }

    public function getAmountFormattedAttribute() {
        return number_format($this->amount , 2 , '.' , ',' );
    }
    public function getAdjustAmountFormattedAttribute() {
        return number_format($this->adjust_amount , 2 , '.' , ',' );
    }

    /*
     *
     * Check if booking id has already paid to imply status of auto booking
     */
    public function getContributionStatusLabelAttribute()
    {
        if ($this->hasReceipt())
            return "<span class='tag tag-success white_color'>" . trans('labels.general.received') . "</span>";
        return "<span class='tag tag-warning white_color'>" . trans('labels.general.pending') . "</span>";
    }

    public function getPayStatusAttribute()
    {
        if ($this->ispaid)
            return "<span class='tag tag-success white_color'>" . trans('labels.general.received') . "</span>";
        return "<span class='tag tag-warning white_color'>" . trans('labels.general.pending') . "</span>";
    }


    /* Get rctno label (Rctno paid for this interest)*/
    public function getRctnoLabelAttribute() {
        $rctnos = [];
        $receipt_codes = $this->receiptCodes()->whereHas('receipt', function($query)
        {
            $query->where('isdishonoured',0);
        })->get();

        if ($receipt_codes->count()) {
            foreach ($receipt_codes as $receipt_code) {
                array_push($rctnos, $receipt_code->receipt->rctno);
            }
            return implode(", ", $rctnos);
        } else {
            return '<span class="tag tag-danger">'. trans('labels.general.none') . '</span>';
        }
    }


    /*
  *
  * Check if status of interest adjustment
  */
    public function getAdjustmentStatusLabelAttribute()
    {
        if ($this->isAdjusted())
            return "<span class='tag tag-success white_color'>" . trans('labels.general.complete') . "</span>";
        return "<span class='tag tag-warning white_color'>" . trans('labels.general.incomplete') . "</span>";

    }

//    Flags
    public function hasReceipt() {
        return $this->hasreceipt == 1;
    }

    public function isWrittenOff(){
        return $this->iswrittenoff == 1;
    }


    public function isAdjusted(){
        return $this->isadjusted == 1;
    }

    public function isPaid(){
        return $this->ispaid == 1;
    }

    public function getResourceNameAttribute()
    {
        return $this->booking->employer->name;
    }


    public function getAddRefundButtonAttribute($interest_refund_id) {

        return '<a href="' . route('backend.compliance.booking_interest.refund.add_interest', ['booking_interest' => $this->id, 'interest_refund' => $interest_refund_id])  . '" class="btn btn-xs btn-success " ><i class="icon fa fa-plus-square
" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.add') . '"></i></a> ';
    }

    public function getRemoveRefundButtonAttribute($interest_refund_id) {

        return '<a href="' . route('backend.compliance.booking_interest.refund.remove_interest', ['booking_interest' => $this->id, 'interest_refund' => $interest_refund_id]) . '" class="btn btn-xs btn-warning " ><i class="icon fa fa-minus-square" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.remove') . '"></i></a> ';
    }


    /**
     * @return mixed
     * Get New interest amount
     */
    public function getNewInterestDetailsAttribute()
    {
        $calculate_interest = new CalculateInterest();
        $late_months = $calculate_interest->getLateMonths($this->miss_month,$this->new_payment_date);
        $new_interest_amount = $this->amount;
        if($this->isadjusted == 0){
            $new_interest_amount = $this->amount + $this->adjust_amount;
        }

        return ['interest_amount' => $new_interest_amount, 'late_months' => $late_months];
    }

    /*Interest adjustment*/
    public function getNewInterestDetailsByNewPayDateAttribute($new_payment_date)
    {
        $calculate_interest = new CalculateInterest();
        $late_months = $calculate_interest->getLateMonths($this->miss_month,$new_payment_date);
        $interest_percent = $this->interest_percent;
        $amount_paid = (new ReceiptCodeRepository())->getTotalContributionPaidForBookingId($this->booking_id);
        $new_interest_amount = $calculate_interest->getInterest($interest_percent, $late_months, $amount_paid);
        return ['interest_amount' => $new_interest_amount, 'late_months' => $late_months];
    }


    /**
     * @return bool
     * LETTER functions-----------start------------
     */
    /*Status to check if letter is initiated*/
    public function getStatusIfLetterInitiatedForAdjustAttribute()
    {
        $check = $this->adjustmentResponseLetter()->where('isinitiated', '<>', 0)->count();
        if ($check > 0) {
            return true;
        } else {
            return false;
        }
    }




    /*Get Letter reference title attribute*/
    public function getLetterReferenceAttribute($lang = 'en')
    {
        $letter_ref = ($lang == 'en') ? 'INTEREST ADJUSTMENT' : 'MAOMBI YA KUSAHIHISHA TOZO';
        return  $letter_ref;
    }
    /*Get Response Letter reference title attribute*/
    public function getLetterReferenceNoAttribute()
    {
        $default = 'AB/000/';
        return (new LetterRepository())->getLetterReferenceNo('CLIEMPINTEADJ', $default);
    }


    /*get Folio no*/
    public function getLetterFolioNoAttribute()
    {
        return (new LetterRepository())->getLetterFolioNo('CLIEMPINTEADJ');
    }

}
