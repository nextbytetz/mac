<?php

namespace App\Models\Finance\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class trait LegacyReceiptCodeAttribute {

 */
trait LegacyReceiptCodeAttribute {

    public function getAmountFormattedAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }

    public function amountImported()
    {
        return ($this->amount_imported) ?: 0;
    }

    public function totalRows()
    {
        return ($this->total_rows) ?: 0;
    }

    public function getAmountImportedFormattedAttribute()
    {
        $amount = $this->amountImported();
        return number_format( $amount , 2 , '.' , ',' );
    }

    public function getTotalRowsAttribute($value)
    {
        $amount = ($value) ?: 0;
        return $amount;
    }

    public function getDiffAmountAttribute()
    {
        $amount = $this->amount - $this->amountImported();
        return number_format( $amount , 2 , '.' , ',' );
    }

    public function getDiffMemberAttribute()
    {
        return abs($this->member_count - $this->totalRows());
    }

    public function amountDiffPercent()
    {
        if ($this->amountImported() > $this->amount)
            $amount = $this->amount - $this->amountImported();
        $amount = 0;
        return ($amount/$this->amount) * 100;
    }

    public function amountDiffPercentLabel()
    {
        /* if ($this->amountImported() > $this->amount) */
        $amount = abs($this->amount - $this->amountImported());
        /* $amount = 0; */
        $percent = ($amount/$this->amount) * 100;
        return (($percent > 100) ? 100 : $percent);
    }

    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

    public function amountDiff()
    {
        $amount = (( $this->amount - $this->amountImported() ));
        return number_format( $amount , 2 , '.' , ',' );
    }

    public function getContribMonthFormattedAttribute() {
        return  Carbon::parse($this->contrib_month)->format('M-Y');
    }

    public function linkedFile()
    {
        return contribution_legacy_dir() . DIRECTORY_SEPARATOR . $this->legacyReceipt->id . DIRECTORY_SEPARATOR . $this->linked_file;
    }

    /**
     * @return string
     */
    public function getUploadButtonAttribute() {
        if ($this->isContributionLoaded() == 1) {
            return  '<a href="' . route('backend.finance.legacy_receipt_code.show', $this->id) . '" class="btn btn-xs btn-info" ><i class="icon fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.view') . '"></i>&nbsp;' . trans('buttons.general.view') . '</a> ';
        } elseif (!is_null($this->hasLinkedFile())) {
            return  '<a href="' . route('backend.finance.legacy_receipt_code.show', $this->id) . '" class="btn btn-xs btn-secondary" ><i class="icon fa fa-check-square" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.load') . '"></i>&nbsp;' . trans('buttons.general.load') . '</a> ';
        }  else {
            return  '<a href="' . route('backend.finance.legacy_receipt_code.linked_file', $this->id) . '" class="btn btn-xs btn-primary site-btn save_button" ><i class="icon fa fa-upload" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.upload') . '"></i>&nbsp;' . trans('buttons.general.upload') . '</a> ';
        }

    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
        return
            $this->getUploadButtonAttribute();
    }

//get linked File status of receipt
    public function getLinkedFileStatusLabelAttribute() {
        if (!is_null($this->hasLinkedFile())) {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.yes') . "</span>&nbsp;&nbsp;, ". trans("labels.general.uploaded") . "&nbsp;&nbsp;<i id='receipt_code_" . $this->id . "' class='upload_percent'>" . $this->uploadPercent() . "</i>";
        }
        return "<span class='tag tag-danger white_color'>" . trans('labels.general.no') . "</span>";
    }

    public function uploadPercent()
    {
        $row_imported = (($this->rows_imported)) ?: 0;
        $total_rows = (($this->total_rows)) ?: 1;
        $percent = ($row_imported/$total_rows) * 100;
        return round($percent) . "%";
    }

    public function analysisStatus()
    {
        if ($this->uploadPercent() == '100%' And $this->error == 0)
            return "<label class='tag tag-success'><i class=\"icon fa fa-check\" aria-hidden=\"true\"></i>&nbsp;&nbsp; <i></i></label>";
        return "<label class='tag tag-default'><i class=\"icon fa fa-remove\" aria-hidden=\"true\"></i>&nbsp;&nbsp; <i class='text-white' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.incomplete_analysis_helper') . "'>" . trans('labels.backend.finance.receipt.incomplete_analysis') . "</i></label>";
    }

    //get workflow Status
    public function getWorkflowStatusLabelAttribute()
    {
        if ($this->amount - $this->amountImported() < 0 And $this->totalRows() == $this->rows_imported And $this->totalRows() > 0 And $this->isContributionLoaded() == 0) {
            //Amount Mismatch, amount paid less than the uploaded schedule
            return "<span class='tag tag-danger info_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.mismatch') . "'>" . '.' . "</span>";
        } elseif ($this->isContributionLoaded() == 1) {
            //Contribution has already uploaded
            return "<span class='tag tag-success success_color_custom'  data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.complete') . "'>" . '.' . "</span>";
        } elseif (!is_null($this->hasLinkedFile())) {
            //Has contribution file
            return "<span class='tag tag-info info_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.load_contribution') . "'>" . '.' . "</span>";
        } else {
            return "<span class='tag tag-warning warning_color_custom'  data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.upload_linked_file') . "'>" . '.' . "</span>";
        }
    }

//    Check if has any linked files
    public function hasLinkedFile(){
        return $this->linked_file;
    }

    /**
     * check if contribution is loaded for this receipt code
     *
     */
    public function isContributionLoaded(){
        /**
        $receipt_code = $this->contributions->where('receipt_code_id','=',$this->id)->first();
        if (!is_null($receipt_code))
        return 1;
        return 0;
         */
        return $this->isuploaded;
    }

    public function contributionPercent()
    {
        $contribPercent = 0;
        switch ($this->legacyReceipt->employer->employer_category_cv_id) {
            case 36:
                //Public Employers
                $contribPercent = (sysdefs()->data()->public_contribution_percent / 100);
                break;
            case 37:
                //Private Employers
                $contribPercent = (sysdefs()->data()->private_contribution_percent / 100);
                break;
            default:
                break;
        }
        return $contribPercent;
    }

    public function allowUploadContributionFile()
    {
        if (($this->isuploaded == 0))
            return 1;
        return 0;
    }

    public function allowUploadContribution()
    {
        if (($this->isuploaded == 0) && (!is_null($this->linked_file)) && ($this->error == 0) )
            return 1;
        return 0;
    }

}
