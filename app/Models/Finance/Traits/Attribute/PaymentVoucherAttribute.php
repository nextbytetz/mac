<?php

namespace App\Models\Finance\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class PaymentVoucherAttribute
 */
trait PaymentVoucherAttribute{





    public function getCreatedAtFormattedAttribute() {
//        return  Carbon::parse($value)->diffForHumans();
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

    public function getAmountFormattedAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }


    public function getIdFormattedAttribute(){
        return str_pad($this->id, 7, "0", STR_PAD_LEFT);
    }

    /**
     * @return mixed
     * Get Name of compensated entity
     * @deprecated
     */

        public function getCompensatedEntityNameAttribute(){
        //employer
        if ($this->getMemberType() == 1){
            return $this->employer->name;
        } elseif ($this->getMemberType() == 3){
            return $this->insurance->name;
        }elseif($this->getMemberType() == 2){
            return $this->employee->name;
        }elseif($this->getMemberType() == 4){
            return $this->dependent->name;
        }
    }


    // Get Benefit resource id for this voucher
    public function getBenefitResourceAttribute(){
        //compensation
        return 'Claim No.' . $this->benefit_resource_id;

    }

    public function isPaid(){
        return $this->is_paid == 1;
    }


    public function getMemberType(){
        return $this->member_type_id;
    }

    public function getPaymentVoucherType(){
        return $this->payment_voucher_type_id;
    }


    //Get Piad status label
    public function getPaidStatusLabelAttribute() {
        if ($this->isPaid())
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.paid') . "'>" . trans('labels.general.paid') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.not_paid') . "'>" . trans('labels.general.not_paid') . "</span>";

    }



    public function getPayButtonAttribute() {

        return '<a href="' . route('backend.finance.payment.pay_voucher', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-money" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.pay') . '"></i></a> ';
    }


    public function getPrintButtonAttribute() {
        return '<a href="'. route('backend.finance.payment.print_voucher', $this->id) .'" class="btn btn-xs btn-primary save_button invoice-print" target = "_blank"  ><i class="icon fa fa-print" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.print') . '"></i></a> ';
    }


    //          ACTION COLUMNS ADDED TO DATATABLE
    public function getActionButtonsAttribute() {
        return
            $this->getPayButtonAttribute() .
            $this->getPrintButtonAttribute() ;
    }
}