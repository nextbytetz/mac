<?php

namespace App\Models\Finance\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class ReceiptAttribute
 */
trait LegacyReceiptAttribute {


    public function getContribMonthFormattedAttribute() {
        return  Carbon::parse($this->contrib_month)->format('d-M-Y');
    }

    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

    public function getCaptureDateFormattedAttribute() {
        return  Carbon::parse($this->capture_date)->format('d-M-Y');
    }


    public function getRctnoFormattedAttribute(){
        return str_pad($this->rctno, 7, "0", STR_PAD_LEFT);
    }

    public function getRctDateFormattedAttribute() {
        return  Carbon::parse($this->rct_date)->format('d-M-Y');
    }


    public function getAmountFormattedAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }

//getstatus of receipt
    public function getStatusLabelAttribute() {
        if ($this->isCancelled()) {
            return "<span class='tag tag-danger white_color'>" . trans('labels.general.inactive') . "</span>";
        }elseif ($this->isDishonoured()){
            return "<span class='tag tag-info white_color'>" . trans('labels.general.dishonoured') . "</span>";
        }else {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.active') . "</span>";
        }
    }

    //Get Complete status label
    public function getCompleteStatusLabelAttribute() {
        if ($this->isComplete())
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.complete') . "'>" . trans('labels.backend.finance.receipt.complete') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.incomplete') . "'>" . trans('labels.backend.finance.receipt.incomplete') . "</span>";

    }

    public function getIsverifiedLabelAttribute()
    {
        if ($this->isverified == 1)
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.verified') . "'>" . trans('labels.general.verified') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.unverified') . "'>" . trans('labels.general.unverified') . "</span>";
    }

    public function isCancelled() {
        return $this->iscancelled == 1;
    }

    public function isDishonoured() {
        return $this->isdishonoured == 1;
    }

    public function isComplete() {
        return $this->iscomplete == 1;
    }

    public function isPayFor()
    {
        if (!is_null($this->description)) {
            $return = $this->description;
        } else {
            if (is_null($this->employer_id)) {
                $return = $this->isPayForEmployer();
            } else {
                $return = $this->isPayForAdministrative();
            }
        }
        return $return;
    }

    /**
     * Autogenerate receipt description based on what has been paid
     * @author Erick Chrysostom <e.chrysostom@nextbyte.co.tz>
     * @return string
     */
    public function isPayForEmployer()
    {
        $return = "";
        $description = "";
        $fin_codes = $this->legacyReceiptCodes()->select(['fin_code_id'])->distinct('fin_code_id')->get()->pluck('fin_code_id')->all();
        $contribution_desc = "Contribution for ";
        $interest_desc = "Interest for ";
        /*$contribution_desc = "";
        $interest_desc = "";*/
        arsort($fin_codes);
        foreach ($fin_codes as $key => $fin_code) {
            if ($description != "") {
                $description .= ", ";
            }
            switch($fin_code) {
                case 2:
                    //Monthly Contribution
                $description .= $contribution_desc;
                $contributions = $this->legacyReceiptCodes()->where("fin_code_id", $fin_code)->orderBy("contrib_month", "asc")->get()->pluck('contrib_month')->all();
                break;
                case 1:
                    //Interest on Monthly Contribution
                $description .= $interest_desc;
                //commented because of error == booking id column haipo on legacy receipt codes
                // $contributions = $this->legacyReceiptCodes()->join('booking_interests', 'booking_interests.booking_id', '=', 'legacy_receipt_codes.booking_id')->where("fin_code_id", $fin_code)->select(['miss_month'])->orderBy("miss_month", "asc")->get()->pluck('miss_month')->all();
                $contributions = $this->legacyReceiptCodes()->where("fin_code_id", $fin_code)->select(['contrib_month'])->orderBy("contrib_month", "asc")->get()->pluck('contrib_month')->all();
                break;
            }

            $counter = 0;
            $in_range = 0;
            $loop = 0;
            foreach ($contributions as $value) {
                $loop++;
                $this_date = Carbon::parse($value);
                if ($counter > 0) {
                    $past_date = Carbon::parse($contributions[$counter - 1]);
                    $diff = abs($this_date->diffInMonths($past_date));
                    if ($diff == 1) {
                        $in_range = 1;
                        if (count($contributions) == $loop) {
                            //Last Loop
                            $description = $this->trimComma($description);
                            $description .= "- " . $this_date->format('M y') . " ";
                        }
                    } else {
                        if ($in_range == 1) {
                            $last = substr($description, -2);
                            if (strpos($last, ',') !== false) {
                                $description = substr($description, 0, -2) . " ";
                            }
                            $description .= "- " . $past_date->format('M y') . ", ";
                            $in_range = 0;
                        }
                        $description .= $this_date->format('M y') . ", ";
                    }
                } else {
                    $description .= $this_date->format('M y') . ", ";
                }
                $counter++;
                if (count($contributions) == $loop) {
                    //Last Loop
                    $description = $this->trimComma($description);
                }
            }
        }
        return $description;
    }

    private function trimComma($string)
    {
        $last = substr($string, -2);
        if (strpos($last, ',') !== false) {
            $string = substr($string, 0, -2) . " ";
        }
        return $string;
    }

    public function isPayForAdministrative()
    {
        $return = $this->finCode->name;
        return $return;
    }

    public function getResourceNameAttribute()
    {
        return $this->employer->name;
    }


}
