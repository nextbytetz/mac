<?php

namespace App\Models\Finance\Traits\Attribute;


trait InterestWriteOffAttribute
{

    public function getResourceNameAttribute()
    {
        return $this->bookingInterests()->first()->booking->employer->name;
    }

}