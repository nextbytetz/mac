<?php

namespace App\Models\Finance\Traits\Attribute;

use Carbon\Carbon;
/**
 * Class CaseHearingAttribute
 */
trait DishonouredChequesAttribute {



    //Get Case Hearing Status label
    public function getDishonouredChequeStatusLabelAttribute() {
        if ($this->isReplaced())
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.label.replaced') . "'>" . trans('labels.backend.finance.label.replaced') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.label.pending') . "'>" . trans('labels.backend.finance.label.pending') . "</span>";

    }


    public function isReplaced() {
        return $this->new_cheque_no !== null;
    }


    public function getFullNameAttribute(){

    }

}
