<?php

namespace App\Models\Finance\Traits\Attribute;

use App\Models\Finance\Receivable\Booking;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use Carbon\Carbon;
/**
 * Class ReceiptAttribute
 */
trait BookingAttribute {


    public function getRcvDateFormattedAttribute() {
        return  Carbon::parse($this->rcv_date)->format('d-M-Y');
    }

    public function getCreatedAtFormattedAttribute() {
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

    public function getAmountCommaAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }

    public function getAmountFormattedAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }
    public function getAdjustAmountFormattedAttribute() {
        $amount = number_format( $this->adjust_amount , 2 , '.' , ',' );
        return $amount;
    }

    /*
*
* Check if status of booking adjustment
*/
    public function getAdjustmentStatusLabelAttribute()
    {
        if ($this->isAdjusted())
            return "<span class='tag tag-success white_color'>" . trans('labels.general.complete') . "</span>";
        return "<span class='tag tag-warning white_color'>" . trans('labels.general.incomplete') . "</span>";

    }

    public function isAdjusted(){
        return $this->isadjusted == 1;
    }


    /*Get total amount paid*/
    public function getTotalAmountPaidAttribute()
    {
        $amount = (new BookingRepository())->getPaidAmountWithLegacy($this->id);
        return $amount;
    }
}
