<?php

namespace App\Models\Finance\Traits\Attribute;

use App\Repositories\Backend\Workflow\WfModuleRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class ReceiptAttribute
 */
trait ReceiptAttribute {


    public function getCreatedAtFormattedAttribute() {
//        return  Carbon::parse($value)->diffForHumans();
        return  Carbon::parse($this->created_at)->format('d-M-Y');
    }

    public function getCancelDateFormattedAttribute() {
        return  Carbon::parse($this->cancel_date)->format('d-M-Y');
    }

    public function getDishonourDateFormattedAttribute() {

        return  Carbon::parse($this->dishonour_date)->format('d-M-Y');
    }

    public function getRctDateFormattedAttribute() {
        return  Carbon::parse($this->rct_date)->format('d-M-Y');
    }

    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setChequenoAttribute($value)
    {
        $this->attributes['chequeno'] = strtoupper($value);
    }

    public function getAmountCommaAttribute() {
        $amount = number_format( $this->amount , 2 , '.' , ',' );
        return $amount;
    }

    public function getRctnoAttribute($value){
        return str_pad($value, 7, "0", STR_PAD_LEFT);
    }

//          ACTION COLUMNS ADDED TO DATATABLE
    public function getActionButtonsAttribute() {
        return
            $this->getEditButtonAttribute();
    }
    public function getEditButtonAttribute() {

        return '<a href="' . route('backend.finance.receipt.edit', $this->id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }


    public function getPrintButtonAttribute() {
        return '<a href="'.'" class="btn btn-xs btn-primary dishonor_button invoice-print" ><i class="icon fa fa-print" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.print') . '"></i></a> ';
    }


//getstatus of receipt
    public function getStatusLabelAttribute() {
        if ($this->isCancelled()) {
            return "<span class='tag tag-danger white_color'>" . trans('labels.backend.table.receipt.cancelled') . "</span>";
        }elseif ($this->isDishonoured()){
            return "<span class='tag tag-info white_color'>" . trans('labels.general.dishonoured') . "</span>";
        } else {
            return "<span class='tag tag-success white_color'>" . trans('labels.general.active') . "</span>";
        }
    }

    //get approval status of receipt
    public function getApprovalStatusLabelAttribute() {
        if ($this->isDishonoured()) {
            return "<span class='tag tag-warning white_color'>" . trans('labels.general.dishonoured') . "</span>";
        } elseif ($this->isCancelled()){
            return "<span class='tag tag-danger white_color'>" . trans('labels.backend.table.receipt.cancelled') . "</span>";
        } elseif ($this->isVerified()) {
            return "<span class='tag tag-info white_color'> Approved </span>";
        } else {
            return "<span class='tag tag-success white_color'> Pending Approval </span>";
        }
    }

    public function getDuplicateStatusAttribute()
    {
        $return = 0;
        $check = $this->where("rctno", $this->rctno)->where("id", "<>", $this->id)->where("isverified", 1)->first();
        if ($check) {
            $return = 1;
            return "<span class='tag tag-danger white_color'>" . trans('labels.backend.table.receipt.duplicate') . "</span>";
        }
        return $return;
    }

    //Get Complete status label
    public function getCompleteStatusLabelAttribute() {
        if ($this->isComplete())
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.complete') . "'>" . trans('labels.backend.finance.receipt.complete') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.incomplete') . "'>" . trans('labels.backend.finance.receipt.incomplete') . "</span>";

    }

    public function getIsverifiedLabelAttribute()
    {
        if ($this->isverified == 1)
            return "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.verified') . "'>" . trans('labels.general.verified') . "</span>";
        return "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.unverified') . "'>" . trans('labels.general.unverified') . "</span>";
    }

    public function isCancelled() {
        return $this->iscancelled == 1;
    }

    public function isDishonoured() {
        return $this->isdishonoured == 1;
    }

    public function isVerified() {
        return $this->isverified == 1;
    }

    public function isComplete() {
        return $this->iscomplete == 1;
    }

    /**
     * @return string
     */
    public function isPayFor()
    {
        if (!is_null($this->description)) {
            $return = $this->description;
        } else {
            if (is_null($this->employer_id)) {
                $return = $this->isPayForAdministrative();
            } else {
                $return = $this->isPayForEmployer();
            }
        }
        return $return;
    }

    /**
     * Autogenerate receipt description based on what has been paid
     * @author Erick Chrysostom <e.chrysostom@nextbyte.co.tz>
     * @return string
     */
    public function isPayForEmployer()
    {
        $return = "";
        $description = "";
        $fin_codes = $this->receiptCodes()->select(['fin_code_id'])->distinct('fin_code_id')->get()->pluck('fin_code_id')->all();
        $contribution_desc = "Contribution for ";
        $interest_desc = "Interest for ";
        /*$contribution_desc = "";
        $interest_desc = "";*/
        arsort($fin_codes);
        foreach ($fin_codes as $key => $fin_code) {
            if ($description != "") {
                $description .= ", ";
            }
            switch($fin_code) {
                case 2:
                    //Monthly Contribution
                    $description .= $contribution_desc;
                    $contributions = $this->receiptCodes()->where("fin_code_id", $fin_code)->orderBy("contrib_month", "asc")->get()->pluck('contrib_month')->all();
                    break;
                case 1:
                    //Interest on Monthly Contribution
                    $description .= $interest_desc;
                    $contributions = $this->receiptCodes()->join('booking_interests', 'booking_interests.booking_id', '=', 'receipt_codes.booking_id')->where("fin_code_id", $fin_code)->select(['miss_month'])->orderBy("miss_month", "asc")->get()->pluck('miss_month')->all();
                    break;
            }

            $counter = 0;
            $in_range = 0;
            $loop = 0;
            foreach ($contributions as $value) {
                $loop++;
                $this_date = Carbon::parse($value);
                if ($counter > 0) {
                    $past_date = Carbon::parse($contributions[$counter - 1]);
                    $diff = abs($this_date->diffInMonths($past_date));
                    if ($diff == 1) {
                        $in_range = 1;
                        if (count($contributions) == $loop) {
                            //Last Loop
                            $description = $this->trimComma($description);
                            $description .= "- " . $this_date->format('M y') . " ";
                        }
                    } else {
                        if ($in_range == 1) {
                            $last = substr($description, -2);
                            if (strpos($last, ',') !== false) {
                                $description = substr($description, 0, -2) . " ";
                            }
                            $description .= "- " . $past_date->format('M y') . ", ";
                            $in_range = 0;
                        }
                        $description .= $this_date->format('M y') . ", ";
                    }
                } else {
                    $description .= $this_date->format('M y') . ", ";
                }
                $counter++;
                if (count($contributions) == $loop) {
                    //Last Loop
                    $description = $this->trimComma($description);
                }
            }
        }
        return $description;
    }

    private function trimComma($string)
    {
        $last = substr($string, -2);
        if (strpos($last, ',') !== false) {
            $string = substr($string, 0, -2) . " ";
        }
        return $string;
    }

    public function isPayForAdministrative()
    {
        $return = $this->finCode->name;
        return $return;
    }

    public function getResourceNameAttribute()
    {
        return $this->payer;
    }

    public function getSourceFormattedAttribute()
    {
        $return = "";
        switch ($this->source) {
            case 1:
                $return = "<span class='tag tag-success white_color'>" . trans('labels.general.branch') . "</span>";
                break;
            case 2:
                $return = "<span class='tag tag-success white_color'>WCFePG</span>";
                break;
        }
        return $return;
    }

    public function getArrearStatusLabelAttribute()
    {
        $return = 0;
        if (in_array($this->wf_module_id, (new WfModuleRepository())->receiptVerificationAdvanceModule())) {
            $countMissingReceipt = DB::table('bookings_mview')->where('employer_id', $this->employer_id)->where('ispaid', 0)->count();
            if ($countMissingReceipt) {
                $return = 1;
            }
        }
        return $return;
    }

}
