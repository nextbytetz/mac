<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Operation\Compliance\Member\ContributionTemp;
use App\Models\Operation\Compliance\Member\LegacyContributionTemp;

/**
 * Class LegacyReceiptCodeRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait LegacyReceiptCodeRelationship {



    //Relation to receipt
    public function legacyReceipt(){
        return $this->belongsTo(\App\Models\Finance\Receipt\LegacyReceipt::class);
    }


    //Relation to employee contributions
    public function contributions(){
        return $this->hasMany(\App\Models\Operation\Compliance\Member\Contribution::class);
    }


    //Relation to fin codes
    public function finCode(){
        return $this->belongsTo(\App\Models\Finance\FinCode::class);
    }

    //Relation to employee contribution_temps
    public function legacyContributionTemps(){
        return $this->hasMany(LegacyContributionTemp::class);
    }

    //Relation to booking
    public function booking(){
        return $this->belongsTo(\App\Models\Finance\Receivable\Booking::class);
    }

    public function bookingInterest()
    {
        return $this->hasOne(BookingInterest::class, 'booking_id', 'booking_id');
    }

}
