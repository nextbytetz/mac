<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\Receipt\ReceiptCode;
use App\Models\Finance\Receivable\BookingInterestRefund;
use App\Models\Notifications\Letter;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;
use App\Models\Finance\Receivable\Booking;
use App\Models\Auth\User;
use App\Models\Finance\Receivable\InterestWriteOff;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

/**
 * Class BookingRelationship
 * @package
 */
trait BookingInterestRelationship {

//Relation to the booking
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    //Relation to the user
    public function adjustUser()
    {
        return $this->belongsTo(User::class, 'adjust_user_id', 'id');
    }

//    Relation Interest_write_offs
    public function interestWriteOff()
    {
        return $this->belongsTo(InterestWriteOff::class);
    }

    public function receiptCodes()
    {
        return $this->hasMany(ReceiptCode::class, 'booking_id', 'booking_id')->where("fin_code_id", 1);
    }

/*  Relation to Refunds*/
    public function bookingInterestRefund()
    {
        return $this->belongsTo(BookingInterestRefund::class);
    }


    /**
     * @return mixed
     */
    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource');
    }


    public function reasonType(){
        return $this->belongsTo(CodeValue::class, 'reason_type_cv_id');
    }


    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }

    /*Adjustment repsonse letter*/
    public function adjustmentResponseLetter()
    {
        $codeValue = new CodeValueRepository();
        //Inspection Notice Letter
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->findIdByReference('CLIEMPINTEADJ'));
    }

}
