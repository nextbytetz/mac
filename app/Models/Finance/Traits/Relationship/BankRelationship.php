<?php

namespace App\Models\Finance\Traits\Relationship;
use App\Models\Finance\BankBranch;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Reporting\DishonouredCheque;

/**
 * Class CurrencyRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait BankRelationship {


    public function dishonouredCheques(){
        return $this->hasMany(DishonouredCheque::class);
    }


    public function bankBranches()
    {
        return $this->hasMany(BankBranch::class);
    }


    public function payrollRuns()
    {
        return $this->hasMany(PayrollRun::class);
    }

}
