<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\FinCode;
use App\Models\Finance\Receivable\InterestAdjustment;
use App\Models\Sysdef\Office;
use App\Models\Workflow\WfModule;
use App\Models\Workflow\WfTrack;

/**
 * Class ReceiptRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait ReceiptRelationship {

    //Relation to the employer
    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class);
    }

    public function bank(){
        return $this->belongsTo(\App\Models\Finance\Bank::class);
    }

    public function payment_type(){
        return $this->belongsTo(\App\Models\Finance\PaymentType::class);
    }

    //Relation to currencies
    public function currencies(){
        return $this->belongsTo(\App\Models\Finance\Currency::class, 'currency_id', 'id');
    }

    //Relation to receipt_codes
    public function receiptCodes() {
        return $this->hasMany(\App\Models\Finance\Receipt\ReceiptCode::class, 'receipt_id', 'id');
        //return $this->morphMany(\App\Models\Finance\Receipt\ReceiptCode::class, 'receipt');
    }

    public function finCode() {
        return $this->belongsTo(FinCode::class);
    }

    //Relation to users
    public function user(){
        return $this->belongsTo(\App\Models\Auth\User::class, 'user_id', 'id');
    }

    //Relation to cancel user
    public function cancelUser(){
        return $this->belongsTo(\App\Models\Auth\User::class, 'cancel_user_id', 'id');
    }

    //Relation to dishonour user
    public function dishonourUser(){
        return $this->belongsTo(\App\Models\Auth\User::class, 'dishonour_user_id', 'id');
    }

    public function office()
    {
        return $this->belongsTo(Office::class);
    }

    //Relation to workflows
    public function wfTracks(){
        //return $this->hasMany(\App\Models\Workflow\WfTrack::class, 'resource_id', 'id');
        return $this->morphMany(WfTrack::class, 'resource');
    }

    public function dishonouredCheques(){
        return $this->hasMany(\App\Models\Finance\DishonouredCheque::class);
    }

    public function dishonouredChequesWithTrashed(){
        return $this->hasMany(\App\Models\Finance\DishonouredCheque::class)->withTrashed();
    }


    /**
     * @return mixed
     */
    public function wfModule()
    {
        return $this->belongsTo(WfModule::class, "wf_module_id");
    }

    /**
     * @return mixed
     * OUT OF DATE FUNCTIONS
     */
    //Relation to the bank  : WRONG Banks should be Bank for proper fluency xxxxxxxx
    public function banks(){
        return $this->belongsTo(\App\Models\Finance\Bank::class, 'bank_id', 'id');
    }
    // xxxxxxxxxxxxxxxxxxxxxxxxx

//Relation to payment types WRONG Payment types should be Payment Type for proper fluency xxxxxxxx
    public function payment_types(){
        return $this->belongsTo(\App\Models\Finance\PaymentType::class, 'payment_type_id', 'id');
    }
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


    /**
     * @return mixed
     * Interest adjustment
     */
    public function interestAdjustment()
    {
        return $this->hasOne(InterestAdjustment::class);
    }

    public function contribModification(){
        return $this->hasOne(\App\Models\Operation\Compliance\Member\ContributionModification\ContribModification::class,'receipt_id', 'id');
    }

}
