<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\Bank;
use App\Models\Finance\BankBranch;
use App\Models\Operation\Payroll\PayrollRun;

/**
 * Class CurrencyRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait BankBranchRelationship {


    public function bank(){
        return $this->belongsTo(Bank::class);
    }



    public function payrollRuns(){
        return $this->hasMany(PayrollRun::class);
    }

}
