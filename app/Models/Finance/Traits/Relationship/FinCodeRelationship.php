<?php

namespace App\Models\Finance\Traits\Relationship;

/**
 * Class FinCodeRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait FinCodeRelationship {



    //Relation to receipt codes
    public function receiptCodes(){
        return $this->hasMany(\App\Models\Finance\Receipt\ReceiptCode::class);
    }


    //Relation to fin code group
    public function finCodeGroup(){
        return $this->belongsTo(\App\Models\Finance\FinCodeGroup::class);
    }

    public function receipts(){
        return $this->hasMany(\App\Models\Finance\Receipt\Receipt::class);
    }



}
