<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\InterestWriteOff;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Finance\Receivable\Portal\ContributionArrear;
use App\Models\Operation\Compliance\Member\EmployerContribution;
use Carbon\Carbon;

/**
 * Class BookingRelationship
 * @package
 */
trait BookingRelationship {

//Relation to the booking interest
    public function bookingInterest(){
        return $this->hasOne(BookingInterest::class);
    }
//Relation to employer
    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class);
    }

    //Relation to receipt codes
    public function receiptCodes(){
        return $this->hasMany(ReceiptCode::class);
    }

    //Relation to receipt
    public function receipts(){
        return $this->hasManyThrough(\App\Models\Finance\Receipt\Receipt::class,
            ReceiptCode::class);
    }


//    Relation to bookingGroup
    public function bookingGroup(){
        return $this->belongsTo(\App\Models\Finance\Receivable\BookingGroup::class);
    }

    /*Relation to portal Contribution arrear*/
    public function portalContributionArrears()
    {
        $rcv_date_parsed = Carbon::parse($this->rcv_date);
        return $this->hasMany(ContributionArrear::class, 'employer_id', 'employer_id')->whereRaw("DATE_PART('month', portal.contribution_arrears.contrib_month::date) =  ? and DATE_PART('year', portal.contribution_arrears.contrib_month::date) =  ? ", [$rcv_date_parsed->format('m'), $rcv_date_parsed->format('Y')]);

    }

}
