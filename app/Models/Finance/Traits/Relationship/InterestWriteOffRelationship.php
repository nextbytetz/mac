<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Workflow\WfTrack;
use App\Models\Auth\User;

/**
 * Class InterstWriteOffRelationship
 * @package
 */
trait InterestWriteOffRelationship {

    //Relation to the booking interest
    public function bookingInterests(){
        return $this->hasMany(BookingInterest::class)->withTrashed();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource');
    }

 }
