<?php

namespace App\Models\Finance\Traits\Relationship;

/**
 * Class PaymentVoucherRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait PaymentVoucherRelationship {




    //Relation to the employer
    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class, 'resource_id', 'id');
    }

    //Relation to the Insurance
    public function insurance(){
        return $this->belongsTo(\App\Models\Operation\Claim\Insurance::class, 'resource_id', 'id');
    }

    //Relation to the Pensioner
    public function pensioner(){
        return $this->belongsTo(\App\Models\Operation\Payroll\Pensioner::class, 'resource_id', 'id');
    }

// Relation to employee
    public function employee(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class, 'resource_id', 'id');
    }

// Relation to dependent
    public function dependent(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Dependent::class, 'resource_id', 'id');
    }
    //Relation to the claim compnesation
    public function claimCompensation(){
        return $this->belongsTo(\App\Models\Operation\Claim\ClaimCompensation::class, 'benefit_resource_id', 'id');
    }

    //Relation to the payment voucher transaction
    public function paymentVoucherTransactions(){
        return $this->hasMany(\App\Models\Finance\PaymentVoucherTransaction::class);
    }



}
