<?php

namespace App\Models\Finance\Traits\Relationship;

/**
 * Class DishonouredChequeRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait DishonouredChequeRelationship {



    //Relation to receipt
    public function receipt(){
        return $this->belongsTo(\App\Models\Finance\Receipt\Receipt::class);
    }

    public function receiptWithTrashed(){
        return $this->belongsTo(\App\Models\Finance\Receipt\Receipt::class, 'receipt_id','id')->withTrashed();
    }


    //Relation to fin code group
    public function finCodeGroup(){
        return $this->belongsTo(\App\Models\Finance\FinCodeGroup::class);
    }

    public function newBank(){
        return $this->belongsTo(\App\Models\Finance\Bank::class,'new_bank_id','id');
    }
       public function oldBank(){
        return $this->belongsTo(\App\Models\Finance\Bank::class,'old_bank_id','id');
    }

    public function dishonourUser(){
        return $this->belongsTo(\App\Models\Auth\User::class,'dishonour_user_id','id');
    }

}
