<?php

namespace App\Models\Finance\Traits\Relationship;



/**
 * Class ReceiptCodeRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait DishonouredChequesRelationship {



    //Relation to receipt
    public function receipt(){
        return $this->belongsTo(\App\Models\Finance\Receipt\Receipt::class);
    }



}
