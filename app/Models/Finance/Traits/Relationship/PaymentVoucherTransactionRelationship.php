<?php

namespace App\Models\Finance\Traits\Relationship;
use App\Models\Finance\PaymentVoucher;
use App\Models\MacErp\ClaimsPayable;
use App\Models\Operation\Claim\Claim;

/**
 * Class PaymentVoucherRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait PaymentVoucherTransactionRelationship {



    //Relation to payrollProc
    public function payrollProc(){
        return $this->belongsTo(\App\Models\Finance\PayrollProc::class);
    }
    //Relation to Benefit Type
    public function benefitType(){
        return $this->belongsTo(\App\Models\Operation\Claim\BenefitType::class);
    }
    //Relation to Member Type
    public function memberType(){
        return $this->belongsTo(\App\Models\Operation\Claim\MemberType::class);
    }
    //Relation to the employer
    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class, 'resource_id', 'id');
    }

    //Relation to the Insurance
    public function insurance(){
        return $this->belongsTo(\App\Models\Operation\Claim\Insurance::class, 'resource_id', 'id');
    }

    //Relation to the Pensioner
    public function pensioner(){
        return $this->belongsTo(\App\Models\Operation\Payroll\Pensioner::class, 'resource_id', 'id');
    }

// Relation to employee
    public function employee(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employee::class, 'resource_id', 'id');
    }

// Relation to dependent
    public function dependent(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Dependent::class, 'resource_id', 'id');
    }
    //Relation to the claim compnesation
    public function claimCompensation(){
        return $this->belongsTo(\App\Models\Operation\Claim\ClaimCompensation::class, 'claim_compensation_id', 'id');
    }

    //Relation to the claim
    public function claim(){
        return $this->belongsTo(Claim::class, 'benefit_resource_id', 'id');
    }

    //Relation to the claim payable
    public function claimPayable(){
        return $this->hasOne(ClaimsPayable::class, 'pv_tran_id');
    }

    //Relation to the paymentVoucher
    public function paymentVoucher(){
        return $this->belongsTo(PaymentVoucher::class);
    }
}
