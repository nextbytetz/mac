<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Operation\Compliance\Member\ContributionTemp;

/**
 * Class ReceiptCodeRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait ReceiptCodeRelationship {



    //Relation to receipt
    public function receipt() {
        return $this->belongsTo(\App\Models\Finance\Receipt\Receipt::class);
        //return $this->morphTo()->withoutGlobalScopes();
    }

    //Relation to Active receipt
    public function activeReceipt() {
        return $this->belongsTo(\App\Models\Finance\Receipt\Receipt::class, 'receipt_id')->where('iscancelled', 0)->where('isdishonoured', 0);
        //return $this->morphTo()->withoutGlobalScopes();
    }
    //Relation to Active receipt including dishnoured
    public function activeReceiptWithDishonor() {
        return $this->belongsTo(\App\Models\Finance\Receipt\Receipt::class, 'receipt_id')->where('iscancelled', 0);
        //return $this->morphTo()->withoutGlobalScopes();
    }



    //Relation to employee contributions
    public function contributions(){
        return $this->hasMany(\App\Models\Operation\Compliance\Member\Contribution::class);
    }

    //Relation to employee contribution Tracks
    public function contributionTracks(){
        return $this->hasMany(\App\Models\Operation\Compliance\Member\ContributionTrack::class);
    }


    //Relation to fin codes
    public function finCode(){
        return $this->belongsTo(\App\Models\Finance\FinCode::class);
    }

    //Relation to employee contribution_temps
    public function contributionTemps(){
        return $this->hasMany(ContributionTemp::class);
    }

    //Relation to booking
    public function booking(){
        return $this->belongsTo(\App\Models\Finance\Receivable\Booking::class);
    }

    public function bookingInterest()
    {
        return $this->hasOne(BookingInterest::class, 'booking_id', 'booking_id');
    }

}
