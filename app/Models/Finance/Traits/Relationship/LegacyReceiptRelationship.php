<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\FinCode;
use App\Models\Sysdef\Office;
use App\Models\Workflow\WfTrack;

/**
 * Class LegacyReceiptRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait LegacyReceiptRelationship {

    //Relation to the employer
    public function employer(){
        return $this->belongsTo(\App\Models\Operation\Compliance\Member\Employer::class);
    }

    public function bank(){
        return $this->belongsTo(\App\Models\Finance\Bank::class);
    }

    public function paymentType(){
        return $this->belongsTo(\App\Models\Finance\PaymentType::class);
    }

    //Relation to currencies
    public function currency(){
        return $this->belongsTo(\App\Models\Finance\Currency::class);
    }

    //Relation to legacy receipt_codes
    public function legacyReceiptCodes() {
        return $this->hasMany(\App\Models\Finance\Receipt\LegacyReceiptCode::class);
    }

    public function finCode() {
        return $this->belongsTo(FinCode::class);
    }

    //Relation to users
    public function user(){
        return $this->belongsTo(\App\Models\Auth\User::class);
    }

    //Relation to cancel user
    public function cancelUser(){
        return $this->belongsTo(\App\Models\Auth\User::class, 'cancel_user_id', 'id');
    }

    //Relation to dishonour user
    public function dishonourUser(){
        return $this->belongsTo(\App\Models\Auth\User::class, 'dishonour_user_id', 'id');
    }

    public function office()
    {
        return $this->belongsTo(Office::class);
    }

    //Relation to work flows
    public function wfTracks(){
        //return $this->hasMany(\App\Models\Workflow\WfTrack::class, 'resource_id', 'id');
        return $this->morphMany(WfTrack::class, 'resource');
    }

    public function dishonouredCheques(){
        return $this->hasMany(\App\Models\Finance\DishonouredCheque::class);
    }

    public function dishonouredChequesWithTrashed(){
        return $this->hasMany(\App\Models\Finance\DishonouredCheque::class)->withTrashed();
    }

    public function contribModification(){
        return $this->hasOne(\App\Models\Operation\Compliance\Member\ContributionModification\ContribModification::class);
    }

}
