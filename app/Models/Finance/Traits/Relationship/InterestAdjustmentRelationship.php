<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\Receipt\Receipt;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Models\Finance\Receivable\BookingInterestRefund;
use App\Models\Notifications\Letter;
use App\Models\Sysdef\CodeValue;
use App\Models\Workflow\WfTrack;
use App\Models\Finance\Receivable\Booking;
use App\Models\Auth\User;
use App\Models\Finance\Receivable\InterestWriteOff;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

/**
 * Class BookingRelationship
 * @package
 */
trait InterestAdjustmentRelationship {

//Relation to the receipt
    public function receipt()
    {
        return $this->belongsTo(Receipt::class);
    }


    /**
     * @return mixed
     */
    public function wfTracks()
    {
        return $this->morphMany(WfTrack::class, 'resource');
    }


    public function reasonType(){
        return $this->belongsTo(CodeValue::class, 'reason_type_cv_id');
    }


    public function letters()
    {
        return $this->morphMany(Letter::class, 'resource');
    }

    /*Adjustment response letter*/
    public function adjustmentResponseLetter()
    {
        $codeValue = new CodeValueRepository();
        //Inspection Notice Letter
        return $this->hasOne(Letter::class, "resource_id")->where("cv_id", $codeValue->findIdByReference('CLIEMPINTEADJ'));
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
