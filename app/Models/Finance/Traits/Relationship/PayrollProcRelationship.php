<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Operation\Payroll\Run\PayrollRunApproval;


/**
 * Class ReceiptCodeRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait PayrollProcRelationship {



    //Relation to payroll_run_approval
    public function payrollRunApproval(){
        return $this->hasOne(PayrollRunApproval::class);
    }



}
