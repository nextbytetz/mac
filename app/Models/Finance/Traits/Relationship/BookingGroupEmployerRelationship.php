<?php

namespace App\Models\Finance\Traits\Relationship;

use App\Models\Finance\Receivable\BookingGroup;

/**
 * Class ReceiptCodeRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait BookingGroupEmployerRelationship {



    //Relation to receipt
    public function bookingGroup(){
        return $this->belongsTo(\App\Models\Finance\Receivable\BookingGroup::class);
    }


}
