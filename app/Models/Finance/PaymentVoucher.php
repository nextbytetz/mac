<?php

namespace App\Models\Finance;

use App\Models\Finance\Traits\Attribute\PaymentVoucherAttribute;
use App\Models\Finance\Traits\Relationship\PaymentVoucherRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PaymentVoucher extends Model implements AuditableContract
{
    //
    use PaymentVoucherAttribute, PaymentVoucherRelationship,Softdeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}
