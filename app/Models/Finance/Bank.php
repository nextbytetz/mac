<?php

namespace App\Models\Finance;

use App\Models\Finance\Traits\Relationship\BankRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{

    use Softdeletes, BankRelationship;

    public function finCodes()
    {
        return $this->belongsToMany(FinCode::class, "bank_accounts")->withPivot("isonline");
    }

}
