<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgportal';


    public function user()
	{
		return $this->belongsTo('App\Models\Auth\User');
	}

	public function employer()
	{
		return $this->belongsTo('App\Models\Operation\Compliance\Member\Employer');
	}

	public function payments()
	{
		return $this->hasMany('App\Models\Finance\Payment','bill_id','bill_no');
	}


}
