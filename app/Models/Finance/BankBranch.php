<?php

namespace App\Models\Finance;

use App\Models\Finance\Traits\Relationship\BankBranchRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankBranch extends Model
{
    //
    use  BankBranchRelationship,  Softdeletes;
}
