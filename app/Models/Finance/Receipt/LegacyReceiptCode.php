<?php

namespace App\Models\Finance\Receipt;

use App\Models\Finance\Traits\Attribute\LegacyReceiptCodeAttribute;
use App\Models\Finance\Traits\Relationship\LegacyReceiptCodeRelationship;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class LegacyReceiptCode extends Model  implements AuditableContract
{

    //
    //
    use LegacyReceiptCodeAttribute, LegacyReceiptCodeRelationship, Auditable, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


}