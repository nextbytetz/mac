<?php

namespace App\Models\Finance\Receipt;

use App\Models\Finance\Traits\Attribute\ReceiptRefundAttribute;
use App\Models\Finance\Traits\Relationship\ReceiptRefundRelationship;
use Illuminate\Database\Eloquent\Model;

class ReceiptRefund extends Model
{
    use ReceiptRefundAttribute, ReceiptRefundRelationship;

    protected $guarded = [];
    public $timestamps = true;
}
