<?php

namespace App\Models\Finance\Receipt;

use Illuminate\Database\Eloquent\Model;

class LegacyReceiptTemp extends Model
{
    protected $guarded = [];
    public $timestamps = true;
}
