<?php

namespace App\Models\Finance\Receipt;

use Illuminate\Database\Eloquent\Model;
use App\Models\Finance\Traits\Relationship\ReceiptRelationship;
use App\Models\Finance\Traits\Attribute\ReceiptAttribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Receipt extends Model implements AuditableContract
{
    //
    //
    use ReceiptRelationship, ReceiptAttribute, SoftDeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        'rctno',
        'rctno_old',
    ];

}
