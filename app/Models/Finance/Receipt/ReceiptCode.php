<?php

namespace App\Models\Finance\Receipt;


use Illuminate\Database\Eloquent\Model;
use App\Models\Finance\Traits\Relationship\ReceiptCodeRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Finance\Traits\Attribute\ReceiptCodeAttribute;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ReceiptCode extends Model implements AuditableContract
{
    //
    use ReceiptCodeRelationship,ReceiptCodeAttribute, SoftDeletes,Auditable;

    protected $guarded = [];
    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}
