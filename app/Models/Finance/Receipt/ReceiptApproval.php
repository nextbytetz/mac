<?php

namespace App\Models\Finance\Receipt;

use Illuminate\Database\Eloquent\Model;

class ReceiptApproval extends Model
{
    //

    //Relation to receipt_codes
    public function receiptCodes() {
        return $this->hasMany(\App\Models\Finance\Receipt\ReceiptCode::class, 'receipt_id', 'id');
    }

}
