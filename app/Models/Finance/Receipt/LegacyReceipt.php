<?php

namespace App\Models\Finance\Receipt;

use App\Models\Finance\Traits\Attribute\LegacyReceiptAttribute;
use App\Models\Finance\Traits\Relationship\LegacyReceiptRelationship;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class LegacyReceipt extends Model  implements AuditableContract
{

    //
    //
    use LegacyReceiptRelationship, LegacyReceiptAttribute, Auditable, SoftDeletes;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];


}