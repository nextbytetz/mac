<?php

namespace App\Models\Finance;


use App\Models\Finance\Traits\Attribute\DishonouredChequesAttribute;
use App\Models\Finance\Traits\Relationship\DishonouredChequeRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DishonouredCheque extends Model
{
    //

    use DishonouredChequeRelationship,Softdeletes,DishonouredChequesAttribute;

    protected $guarded = [];
}
