<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class SuccessfulGepgTrx extends Model
{
    protected $table = 'successful_transactions_gepg';
    protected $primaryKey ='control_no';
    public $incrementing = false;
}
