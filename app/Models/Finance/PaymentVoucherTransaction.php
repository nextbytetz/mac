<?php

namespace App\Models\Finance;


use App\Models\Finance\Traits\Attribute\PaymentVoucherTransactionAttribute;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Finance\Traits\Relationship\PaymentVoucherTransactionRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentVoucherTransaction extends Model implements AuditableContract
{
    //
    use PaymentVoucherTransactionAttribute, PaymentVoucherTransactionRelationship,Softdeletes, Auditable;

    protected $guarded = [];
    public $timestamps = true;

    /**
     * @var array
     */
    protected $auditableEvents = [
        'deleted',
        'updated',
        'restored',
    ];

}
