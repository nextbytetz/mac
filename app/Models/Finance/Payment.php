<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //

	protected $connection = 'pgportal';

	
	public function bill()
	{
		return $this->belongsTo(' App\Models\Finance\Bill', 'bill_no');
	}
}
