<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Finance\Traits\Relationship\FinCodeRelationship;

class FinCode extends Model
{
    //
    use FinCodeRelationship, Softdeletes;
}
