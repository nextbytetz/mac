<?php

namespace App\Models\WorkPlaceRiskAssesment;

use Illuminate\Database\Eloquent\Model;
use App\Models\WorkPlaceRiskAssesment\Traits\Relationship\OshAuditFeedbackRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class OshAuditFeedback extends Model implements AuditableContract
{
    //
	use OshAuditFeedbackRelationship, Auditable;

	protected $table ='osh_audit_feedback';
	protected $fillable = ['osh_audit_period_employer_id', 'osh_audit_question_id'];

}
