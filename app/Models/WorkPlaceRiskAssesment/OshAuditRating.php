<?php

namespace App\Models\WorkPlaceRiskAssesment;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class OshAuditRating extends Model implements AuditableContract
{

	use Auditable;

	protected $connection = 'pgmain';
	protected $fillable = [
		'osh_audit_period_id', 'employer_id', 'period','fin_year', 'ohsa',
		'training','ergonomics','personal_protective_equipment','machine_use_maintainance','chemicals',
		'emergency_preparedness', 'adorr','health_welfare','rate'
	];


}
