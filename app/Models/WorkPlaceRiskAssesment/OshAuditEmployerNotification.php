<?php

namespace App\Models\WorkPlaceRiskAssesment;

use Illuminate\Database\Eloquent\Model;

class OshAuditEmployerNotification extends Model
{
    //
	protected $connection = 'pgmain';
	protected $fillable = ['osh_audit_employer_id', 'notification_report_id','is_submitted','quater','lost_days'];
}
