<?php

namespace App\Models\WorkPlaceRiskAssesment;

use Illuminate\Database\Eloquent\Model;

class OshAuditPeriodEmployer extends Model
{
    //
	protected $connection = 'pgmain';
	protected $fillable = [
		'user_id', 'osh_audit_period_id', 'employer_id' , 'district_id', 'number_of_employees', 'total_claims',
		'number_of_fatal','number_of_days_lost','exemption_from_duty','hospitalization','light_duty','is_submitted'
	];

}
