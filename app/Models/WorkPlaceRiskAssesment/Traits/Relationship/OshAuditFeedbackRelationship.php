<?php

namespace App\Models\WorkPlaceRiskAssesment\Traits\Relationship;

use Carbon\Carbon;
/**
 * Class OshAuditFeedbackRelationship
 */
trait OshAuditFeedbackRelationship{


    //Relation to the osh audit questions
    public function oshAuditQuestion(){
        return $this->belongsTo(\App\Models\WorkPlaceRiskAssesment\OshAuditQuestion::class);
    }

}