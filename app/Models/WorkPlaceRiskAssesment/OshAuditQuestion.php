<?php

namespace App\Models\WorkPlaceRiskAssesment;

use Illuminate\Database\Eloquent\Model;

class OshAuditQuestion extends Model
{
    //

	public function oshAuditQuestionGroup(){
		return $this->belongsTo(\App\Models\WorkPlaceRiskAssesment\OshAuditQuestion::class);
	}
}
