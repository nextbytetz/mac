<?php

namespace App\Models\WorkPlaceRiskAssesment;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Workflow\WfTrack;

class OshAuditPeriod extends Model implements AuditableContract
{

	use Auditable;

	protected $connection = 'pgmain';
	protected $table = 'osh_audit_period';
	protected $fillable = ['user_id', 'status', 'wf_done' , 'wf_done_date', 'created_at', 'updated_at'];

	public function wfTracks()
	{
		return $this->morphMany(WfTrack::class, 'resource');
	}


	public function getResourceNameAttribute()
	{
		$period = $this->period == 1 ? 'Jul - Dec ' : 'Jan - Jun';
		return $this->fin_year.' : '.$period;
	}

}
