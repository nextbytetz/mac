<?php

namespace App\Models\Task\Attribute;

trait CheckerAttribute
{
    public function getPriorityStatusAttribute()
    {
        $return = "";
        if ($this->priority) {
            //High priority
            $return = "<span class='tag tag-success'>High</span>";
        } else {
            //Low Priority
            $return = "<span class='tag tag-warning'>Low</span>";
        }
        return $return;
    }
}