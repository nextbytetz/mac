<?php

namespace App\Models\Task;

use App\Models\Task\Attribute\CheckerAttribute;
use App\Models\Task\Relationship\CheckerRelationship;
use Illuminate\Database\Eloquent\Model;

class Checker extends Model
{
    use CheckerRelationship, CheckerAttribute;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'main.checkers';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pgmain';

    protected $guarded = [];
    public $timestamps = true;

}
