<?php

namespace App\Models\Task\Relationship;


use App\Models\Auth\User;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Compliance\Member\ArrearCommitmentRequest;

trait CheckerRelationship
{

    public function notificationReport()
    {
        return $this->belongsTo(NotificationReport::class, "resource_id");
    }

    public function resource()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function arrearCommitmentRequest()
    {
        return $this->belongsTo(ArrearCommitmentRequest::class, "resource_id");
    }

}