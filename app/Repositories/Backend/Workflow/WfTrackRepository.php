<?php

namespace App\Repositories\Backend\Workflow;

use App\Events\ApproveWorkflow;
use App\Events\NewWorkflow;
use App\Events\RejectWorkflow;
use App\Exceptions\GeneralException;
use App\Exceptions\GeneralValidationException;
use App\Exceptions\WorkflowException;
use App\Models\Workflow\WfDocumentCheck;
use App\Models\Workflow\WfTrack;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\DocumentBenefitRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationWorkflowRepository;
use App\Repositories\BaseRepository;
use App\Services\Scopes\IsApprovedScope;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;

/**
 * Class WfTrackRepository
 * @package App\Repositories\Backend\Workflow
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class WfTrackRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = WfTrack::class;

    public function __construct()
    {

    }

    /**
     * @param $resource_id
     * @param $module_id
     * @return mixed
     */
    public function getRecentResourceTrack($module_id, $resource_id)
    {
        $wf_track = $this->query()->where('resource_id', $resource_id)->whereHas('wfDefinition', function ($query) use ($module_id) {
            $query->where('wf_module_id', $module_id);
        })->orderBy('id','desc')->first();
        return $wf_track;
    }

    /**
     * @param $wf_definition_id
     * @return mixed
     */
    public function getWfTrackId($wf_definition_id)
    {
        $wf_track = $this->query()->where('wf_definition_id', $wf_definition_id)->orderBy('id', 'desc')->first();
        return $wf_track->id;
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @param null $type
     * @return mixed
     * @throws GeneralException
     */
    public function getWfTracks($resource_id, $wf_module_group_id, $type = NULL)
    {
        $wf_module = (new Workflow(['wf_module_group_id' => $wf_module_group_id, 'type' => $type]))->getModule();
        $wf_tracks = $this->query()->where('resource_id', $resource_id)->whereHas('wfDefinition', function ($query) use ($wf_module) {
            $query->whereHas('wfModule', function ($query) use ($wf_module)  {
                $query->where('id', $wf_module);
            });
        })->orderBy('id','asc')->get();
        return  $wf_tracks;
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @param null $type
     * @return mixed
     * @throws GeneralException
     */
    public function getPendingWfTracksForDatatable($resource_id, $wf_module_group_id, $type = NULL)
    {
        $wf_module = (new Workflow(['wf_module_group_id' => $wf_module_group_id, 'type' => $type]))->getModule();
        $wf_tracks = $this->query()->where('resource_id', $resource_id)->whereHas('wfDefinition', function ($query) use ($wf_module) {
            $query->whereHas('wfModule', function ($query) use ($wf_module)  {
                $query->where('wf_modules.id', $wf_module);
            });
        })->whereIn('status', [0, 6])->orderBy('id','asc')->get();
        return  $wf_tracks;
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @param null $type
     * @return mixed
     * @throws GeneralException
     */
    public function getCompletedWfTracks($resource_id, $wf_module_group_id, $type = NULL)
    {
        $wf_module = (new Workflow(['wf_module_group_id' => $wf_module_group_id, 'type' => $type]))->getModule();
        $wf_tracks = $this->query()->where('resource_id', $resource_id)->whereHas('wfDefinition', function ($query) use ($wf_module) {
            $query->whereHas('wfModule', function ($query) use ($wf_module)  {
                $query->where('id', $wf_module);
            });
        })->whereIn('status', [1, 2, 3, 4, 5, 6])->orderBy('id','asc')->get();
        return  $wf_tracks;
    }

    /*Get deactivated wf tracks for dataTable*/
    public function getDeactivatedWfTracksForDataTable($resource_id, $wf_module_group_id)
    {
        return $this->query()->onlyTrashed()->where('resource_id',$resource_id)->whereHas('wfDefinition', function ($query) use ($wf_module_group_id){
            $query->whereHas('wfModule', function ($query) use ($wf_module_group_id)  {
                $query->where('wf_module_group_id', $wf_module_group_id);
            });
        })->orderBy('id','asc');
    }

    /**
     * @param $wf_module_id
     * @param $resource_id
     * @param $currentLevel
     * @param null $user
     * @return bool
     */
    public function hasParticipated($wf_module_id, $resource_id, $currentLevel, $user = NULL): bool
    {
        if (!$user) {
            $user = access()->id();
        }
        $query = $this->query()->where(['resource_id' => $resource_id, 'user_id' => $user])->whereHas('wfDefinition', function ($query) use ($wf_module_id, $currentLevel) {
            $query->where('wf_module_id', $wf_module_id)->where('level', '<>', $currentLevel)->where('allow_repeat_participate', 0);
        })->first();
        if ($query) {
            $return = true;
        } else {
            //if already participated in another level, check
            $return = false;
        }
        return $return;
    }

    /**
     * @param $wf_module_id
     * @param $resource_id
     * @param $currentLevel
     * @param null $user
     * @return bool
     */
    public function hasPreviousParticipated($wf_module_id, $resource_id, $currentLevel, $user = NULL): bool
    {
        if (!$user) {
            $user = access()->id();
        }
        $return = false;
        $check = $this->query()->where(['resource_id' => $resource_id, 'user_id' => $user])->whereHas('wfDefinition', function ($query) use ($wf_module_id, $currentLevel) {
            $query->where('wf_module_id', $wf_module_id)->where('level', '<=', (int) $currentLevel);
        })->orderByDesc('wf_tracks.id')->limit(1)->first();
        if ($check) {
            $return = true;
        }
        return $return;
    }

    public function assignStatus($wf_track_id): array
    {
        $wf_track = $this->find($wf_track_id);
        if ($wf_track->assigned == 0) {
            $return = trans("labels.backend.workflow.not_assigned");
            $status = false;
        } else {
            $return = trans("labels.backend.workflow.assigned", ['name' => $wf_track->user->name]);
            $status = true;
        }
        return ['text' => $return, 'status' => $status];
    }

    public function updateWorkflow(Model $wf_track, array $input, $action)
    {
        return DB::transaction(function () use ($wf_track, $input, $action) {
            //logger('reached here ...');
            $wfDefinition = $wf_track->wfDefinition;
            $wfModule = $wfDefinition->wfModule;
            if ($action == 'approve_reject' And is_null($input['comments'])) {
                if ($wfDefinition->is_approval) {
                    $input['comments'] = "Approved";
                } else {
                    $input['comments'] = "Recommended";
                }
            }
            if ($action == 'assign') {
                $user = access()->user();
                $wf_track->user_id = $input['user_id'];
                $wf_track->assigned = $input['assigned'];
                $user->wfTracks()->save($wf_track);
            } else {
                $wf_track->update($input);
            }
            /* Process the workflow level requirements */
            if ($action == 'approve_reject') {
                //logger('reached here');
                if ($input['status'] != 0) {
                    switch ($input['status']) {
                        case '1':
                        case '3':
                        case '4':
                        /* Workflow Approved */
                        /* Workflow Declined */
                        /* Workflow Seeking Advice */
                            //logger('reached here');
                        event(new ApproveWorkflow($wf_track, $input));
                        break;
                        case '2':
                        /* Workflow Rejected */
                        event(new RejectWorkflow($wf_track, $input['level']));
                        break;
                    }
                }
            }
            return $wfModule;
        });
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @throws GeneralException
     */
    public function checkIfCanInitiateAction($resource_id, $wf_module_group_id)
    {
        $input = ['resource_id' => $resource_id, 'wf_module_group_id' => $wf_module_group_id ];
        $workflow = new Workflow($input);
        $wf_track = $this->checkIfHasWorkflow($resource_id, $wf_module_group_id);
        $level = $workflow->currentLevel();

        if (is_null($wf_track) || (($wf_track->status == 0) && ($level == 1)) ){
            // initiate action
        }else{
            throw new GeneralException(trans('exceptions.backend.workflow.can_not_initiate_action'));
        }
    }

    /**
     * @param $resource_id
     * @param $wf_module_id
     * @return mixed
     */
    public function checkIfHasWorkflow($resource_id, $wf_module_id)
    {
        $input = ['resource_id' => $resource_id, 'wf_module_id' => $wf_module_id ];
        $workflow = new Workflow($input);
        $wf_track = $workflow->currentWfTrack();
        return $wf_track;
    }


    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @deprecated
     */
    public function initiateOrRestartWorkflow($resource_id, $wf_module_group_id)
    {
        if (is_null($this->checkIfHasWorkflow($resource_id, $wf_module_group_id))) {
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]));
        }else{
            //event(new ApproveWorkflow($this->checkIfHasWorkflow($resource_id,$wf_module_group_id)));
        }
    }

    public function getWorkflowQuery()
    {
        $workflowQuery = $this->query()->select([
            DB::raw("wf_tracks.id"),
            DB::raw("wf_module_groups.id as module_group_id"),
            DB::raw("wf_module_groups.name as module_group"),
            DB::raw("wf_modules.id as module_id"),
            DB::raw("wf_modules.name as module"),
            DB::raw("wf_definitions.description"),
            DB::raw("wf_definitions.level"),
            DB::raw("wf_tracks.resource_id"),
            DB::raw("wf_tracks.receive_date"),
            DB::raw("wf_tracks.resource_type"),
            DB::raw("wf_tracks.status"),
            DB::raw("wf_tracks.assigned"),
            DB::raw("concat_ws(' ', coalesce(users.firstname, ''), coalesce(users.middlename, ''), coalesce(users.lastname, '')) as user"),
        ])
        ->join("wf_definitions", "wf_definitions.id", "=", "wf_tracks.wf_definition_id")
        ->join("wf_modules", "wf_modules.id", "=", "wf_definitions.wf_module_id")
        ->join("wf_module_groups", "wf_module_groups.id", "=", "wf_modules.wf_module_group_id")
        ->leftJoin("users", "users.id", "=", "wf_tracks.user_id");
            //->whereIn("wf_definitions.id", access()->allWfDefinitions());
        return $workflowQuery;
    }

    public function getPendingClaimSummary()
    {
        $query = $this->query()->select([
            DB::raw("notification_reports.filename"),
            DB::raw("employees.firstname"),
            DB::raw("employees.middlename"),
            DB::raw("employees.lastname"),
            DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as employee"),
            DB::raw("incident_types.name as incident"),
            DB::raw("wf_definitions.level"),
            DB::raw("wf_definitions.description"),
            DB::raw("case when users.username is null then concat_ws(' - ', designations.name, units.name) else users.username end as staff"),
            DB::raw("designations.name as designation"),
            DB::raw("users.username"),
            DB::raw("wf_tracks.receive_date"),
            DB::raw("wf_tracks.resource_id"),
            DB::raw("DATE_PART('day', now() - receive_date) as aging"),
        ])
        ->join("wf_definitions", "wf_definitions.id", "=", "wf_tracks.wf_definition_id")
        ->join("wf_modules", "wf_modules.id", "=", "wf_definitions.wf_module_id")
        ->join("wf_module_groups", "wf_module_groups.id", "=", "wf_modules.wf_module_group_id")
        ->join("designations", "designations.id", "=", "wf_definitions.designation_id")
        ->join("units", "units.id", "=", "wf_definitions.unit_id")
        ->join("notification_reports", "notification_reports.id", "=", "wf_tracks.resource_id")
        ->join("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
        ->join("employees", "employees.id", "=", "notification_reports.employee_id")
        ->leftJoin("users", "users.id", "=", "wf_tracks.user_id")
        ->whereRaw("wf_module_groups.id in (3,4)")
        ->whereRaw("wf_tracks.status = 0")
        ->whereRaw("notification_reports.receipt_date between ? and ?", [$this->getStartSummaryPeriod(), $this->getEndSummaryPeriod()]);
        return $query;
    }

    public function getPendingQuery()
    {
        $pendings = $this->getWorkflowQuery()
        ->whereHas("wfDefinition", function ($query) {
            $query->whereHas("users", function ($subQuery) {
                $subQuery->whereIn("user_wf_definition.user_id", access()->allUsers());
            });
        })
        ->where(function ($query) {
            $query->whereIn('wf_tracks.status', [0,6])
            ->orWhere(['wf_tracks.assigned' => 0]);
        });
        return $pendings;
    }

    public function getUnitPendingQuery($unit_id)
    {
        $pendings = $this->getWorkflowQuery()
                            ->whereHas("wfDefinition", function ($query) use ($unit_id) {
                                $query->where("unit_id", $unit_id);
                            })
                            ->where(function ($query) {
                                $query->where(['wf_tracks.status' => 0])
                                    ->orWhere(['wf_tracks.assigned' => 0]);
                            });
        return $pendings;
    }

    public function getAllPendingQuery()
    {
        $pendings = $this->getWorkflowQuery()
        ->where(function ($query) {
            $query->where(['wf_tracks.status' => 0])
            ->orWhere(['wf_tracks.assigned' => 0]);
        });
        return $pendings;
    }

    /**
     * @param int $status
     * @param int $hasnotify
     * @return mixed
     */
    public function getMyPendingQuery($status = 0, $hasnotify = 0)
    {
        $pendings = $this->getWorkflowQuery()
        ->where(function ($query) use ($status) {
            $query->where(['status' => $status]);
        });
        if ($hasnotify) {
            $pendings->where("wf_modules.notify", 1);
        }
        return $pendings;
    }

    public function getAttendedQuery($status = 1)
    {
        $attended = $this->getWorkflowQuery()
        ->where(function ($query) use ($status) {
            $query->where(['status' => $status]);
        });
        return $attended;
    }

    public function getMyAttendedQuery($status = 1)
    {
        $attended = $this->getAttendedQuery($status)
        ->where(function ($query) {
            $query->whereIn('user_id', access()->allUsers())->where('user_type', 'App\Models\Auth\User');
        });
        return $attended;
    }

    public function getPendingGroupCount($id)
    {
        $pendings = $this->getPendingQuery();
        return $pendings->where("wf_module_groups.id", $id)->count();
    }

    public function getPendingModuleCount($id)
    {
        $pendings = $this->getPendingQuery();
        return $pendings->where("wf_modules.id", $id)->count();
    }

    public function getUnitPendingModuleCount($wf_module_id, $unit_id)
    {
        $pendings = $this->getUnitPendingQuery($unit_id);
        return $pendings->where("wf_modules.id", $wf_module_id)->count();
    }

    public function getMyPendingGroupCount($id)
    {
        $pendings = $this->getPendingQuery()->whereIn("user_id", access()->allUsers());
        return $pendings->where("wf_module_groups.id", $id)->count();
    }

    public function getMyPendingModuleCount($id, $status = 0)
    {
        $pendings = $this->getMyPendingQuery($status)->whereIn("user_id", access()->allUsers());
        return $pendings->where("wf_modules.id", $id)->count();
    }



    public function getMyAttendedGroupCount($id)
    {
        $attended = $this->getMyAttendedQuery();
        return $attended->where("wf_module_groups.id", $id)->count();
    }

    public function getMyAttendedModuleCount($id, $status = 1)
    {
        $attended = $this->getMyAttendedQuery($status);
        return $attended->where('wf_modules.id', $id)->count();
    }

    public function getPendingCount()
    {
        $pendings = $this->getPendingQuery();
        return $pendings->count();
    }

    /**
     * @param int $hasnotify
     * @return mixed
     */
    public function getMyPendingCount($hasnotify = 0)
    {
        $pendings = $this->getMyPendingQuery(0, $hasnotify)->whereIn("user_id", access()->allUsers());
        return $pendings->count();
    }

    public function getForWorkflowDatatable()
    {

        if (request()->has('state')) {
            $state = request()->input('state');

            switch ($state) {
                case "full":
                $pendings = $this->getAllPendingQuery();
                break;
                case "assigned":
                $pendings = $this->getMyPendingQuery()->whereIn("user_id", access()->allUsers());
                break;
                case "attended":
                $pendings = $this->getAttendedQuery();
                break;
                case "archived":
                $pendings = $this->getAttendedQuery(6);
                break;
                default:
                $pendings = $this->getPendingQuery();
            }
            switch ($state) {
                case "attended":
                case "archived":
                switch (request()->input('status')) {
                    case '3':
                    /* Assigned to User */
                    $user_id = request()->input("user_id");
                    $pendings->where('user_id', $user_id);
                    break;
                    case '2':
                    /* By All */
                    break;
                    default:
                    /* Attended by Me */
                    $pendings->where('user_id', access()->id());
                    break;
                }
                break;
                case "assigned":
                break;
                default:
                switch (request()->input("status")) {
                    case '0':
                    /* Not Assigned */
                    $pendings->where("assigned", 0);
                    break;
                    case '1':
                    /* Assigned to Me */
                    $pendings->where("user_id", access()->id());
                    break;
                    case '2':
                    /* All */
                    break;
                    case '3':
                    /* Assigned to User */
                    $user_id = request()->input("user_id");
                    $pendings->where("user_id", $user_id);
                    break;
                    default:
                    break;
                }
                break;
            }
        } else {
            $pendings = $this->getPendingQuery();
        }

        $search = request()->input('search');
        $wf_module_group_id = request()->input('wf_module_group_id');
        $wf_module_id = request()->input('wf_module_id');
        //logger($wf_module_group_id);

        if ($wf_module_id) {
            //Filter By Workflow Module Id
            $pendings->where("wf_modules.id", $wf_module_id);
        }

        if (true) {
            if (true) {
                switch ($wf_module_group_id) {
                    case '1':
                    /* Interest Waiving */
                    if ($search != "") {
                        $pendings->whereHas("interestWriteOff", function ($query) use ($search) {
                            $query->whereHas("bookingInterests", function ($subQuery) use ($search) {
                                $subQuery->whereHas("booking", function ($sub2Query) use ($search) {
                                    $sub2Query->where("employer_id", "=", $search);
                                });
                            });
                        });
                    }
                    break;
                    case '2':
                    /*Interest Adjustment*/
                    if ($search != "") {
                        $pendings->whereHas("bookingInterest", function ($query) use ($search) {
                            $query->whereHas("booking", function ($subQuery) use ($search) {
                                $subQuery->where("employer_id", "=", $search);
                            });
                        });
                    }
                    break;
                    case '3':
                    case '4':
                    /*Claim & Notification Processing*/
                    /* Notification Rejection */
                    $incident = (int) request()->input("incident");
                    if ($incident || $search != "") {
                        $wfModule = (new WfModuleRepository())->find($wf_module_id);
                        $type = $wfModule->type;
                        if ($type >= 4) {
                                //This is progressive notification
                            $pendings->whereHas("notificationWorkflow", function ($query) use ($search, $wf_module_id, $incident) {
                                $query->where("wf_module_id", $wf_module_id);
                                if ($incident Or $search != "") {
                                    $query->whereHas("notificationReport", function ($query) use ($search, $incident) {
                                        if ($incident) {
                                            $query->where("incident_type_id", "=", $incident);
                                        }
                                        if ($search != "") {
                                                //$query->where("employee_id", "=", $search);
                                            $query->where(function ($query) use ($search) {
                                                $query->orWhere("employee_id", "=", $search)->orWhere("employer_id", "=", $search);
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                                //This is legacy notification report
                            $pendings->whereHas("notificationReport", function ($query) use ($search, $incident) {
                                if ($incident) {
                                    $query->where("incident_type_id", "=", $incident);
                                }
                                if ($search != "") {
                                        //$query->where("employee_id", "=", $search);
                                    $query->where(function ($query) use ($search) {
                                        $query->orWhere("employee_id", "=", $search)->orWhere("employer_id", "=", $search);
                                    });
                                }
                            });
                        }
                    }
                        //Include progressive notification and non progressive notification.
                    break;
                    case '5':
                    /* Contribution Receipt */
                    if ($search != "") {
                        $operator = request()->input("operator");
                        $pendings->whereHas("receipt", function ($query) use ($search, $operator) {
                            if ($operator == 'like') {
                                $query->whereRaw("rctno::varchar ~* ?", [$search]);
                            } else {
                                $query->where("rctno", "=", $search);
                            }
                        });
                    }
                    break;
                    case '6':
                    /* Employer registration */
                    if ($search != "") {
                        $pendings->whereHas("employer", function ($query) use ($search) {
                            $query->where("id", "=", $search)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }
                    break;
                    case '7':
                    /* Contribution Receipt Legacy */
                    if ($search != "") {
                        $operator = request()->input("operator");
                        $pendings->whereHas("legacyReceipt", function ($query) use ($search, $operator) {
                            if ($operator == 'like') {
                                $query->whereRaw("rctno::varchar ~* ?", [$search]);
                            } else {
                                $query->where("rctno", "=", $search);
                            }
                        });
                    }
                    break;
                    case '8':
                    if ($search != "") {
                        /* Online Employer Verification */
                        $pendings->whereHas("onlineEmployerVerification", function ($query) use ($search) {
                            $query->whereHas("employer", function ($subQuery) use ($search) {
                                $subQuery->where("employer_id", "=", $search);
                            });
                        });
                    }
                    break;
                    case '9':
                    /* Interest Refund */
                    break;

                    case '10':
                    /* Payroll run Approval */
                    $pendings->has('payrollRunApproval');
                    break;

                    case '11':
                    /* Payroll bank details modification */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollBankInfoUpdate", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{
                        $pendings->has('payrollBankInfoUpdate');
                    }
                    break;

                    case '12':
                    /* Payroll status change */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollStatusChange", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{
                        $pendings->has('payrollStatusChange');
                    }
                    break;
                    case '13':
                    /* Payroll recovery processing */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollRecovery", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{

                        $pendings->has('payrollRecovery');
                    }
                    break;
                    case '14':
                    /* Payroll reconciliation */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollReconciliation", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{

                        $pendings->has('payrollReconciliation');
                    }
                    break;

                    case '15':
                    /* Payroll beneficiary updates */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollBeneficiaryUpdate", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{

                        $pendings->has('payrollBeneficiaryUpdate');
                    }
                    break;
                    default:
                    break;
                }
            }
        }
        /*
         * dd($pendings->get()->toArray());
        dd($pendings->toSql());
        */
        $datatables = app('datatables')
        ->of($pendings)
        ->addColumn("resource_name", function ($query) {
            return ($query->resource) ? $query->resource->resource_name : "";
        })
        ->editColumn("resource_id", function ($query) {
                //return (count($query->resource)) ? $query->resource->id : "";
                //if ($query->resource_type == "App\Models\Operation\Claim\NotificationWorkflow");
            if (is_null($query->resource_type) Or empty($query->resource_type)) {
                $result = 0;
            } else {
                $result = $query->resource()->count();
            }
            switch ($query->resource_type) {
                case "App\Models\Operation\Claim\NotificationWorkflow":
                $return = (($result) ? $query->resource->notificationReport->id : "");
                        //$return = 1;
                break;
                case "App\Models\Operation\Compliance\Inspection\EmployerInspectionTaskWorkflow":
                $return = (($result) ? $query->resource->employerTask->id : "");
                break;
                default:
                $return = (($result) ? $query->resource->id : "");
                        //$return = 1;
                break;
            }
            return $return;
        })
        ->addColumn("receive_date_formatted", function ($query) {
            return $query->receive_date_formatted;
        })
        ->addColumn("assign_status", function ($query) {
            return $query->assign_status;
        })

        ->rawColumns(['assign_status']);

        return $datatables;
    }

    /**
     * @param $from_module_id
     * @param $to_module_id
     * @return bool
     * @description May never be used in future
     * @throws GeneralException
     */
    public function transferWorkflow($from_module_id, $to_module_id, $resource_id = 0)
    {
        $wfTracksQuery = $this->query()->whereHas("wfDefinition", function ($query) use ($from_module_id) {
            $query->where("wf_module_id", $from_module_id);
        });
        if ($resource_id) {
            $wfTracksQuery->where("resource_id", $resource_id);
        }
        $wfTracks = $wfTracksQuery->get();
        $workflow = new Workflow(['wf_module_id' => $to_module_id]);
        foreach ($wfTracks as $wfTrack) {
            $level = $wfTrack->wfDefinition->level;
            $definition = $workflow->levelDefinition($level);
            $wfTrack->wf_definition_id = $definition;
            $wfTrack->save();
        }
        return true;
    }

    /**
     * @param $from_module_id
     * @param $to_module_id
     * @param $to_type
     *
     * @throws GeneralException
     */
    public function transferWorkflowType($from_module_id, $to_group_id, $to_type, $resource_id = 0)
    {
        $wfModuleRepo = new WfModuleRepository();
        $wfModule = $wfModuleRepo->query()->select(['id', 'wf_module_group_id'])->where(['type' => $to_type, 'wf_module_group_id' => $to_group_id])->first();
        if ($wfModule) {
            $wfTracksQuery = $this->query()->whereHas("wfDefinition", function ($query) use ($from_module_id) {
                $query->where("wf_module_id", $from_module_id);
            });
            switch ($wfModule->wf_module_group_id) {
                case 4:
                    //Notification Rejection....
                $wfTracksQuery = $wfTracksQuery->whereIn('resource_id', function($query) use ($to_type) {
                    $query->select('id')->from('notification_reports')->where(['incident_type_id' => $to_type]);
                });
                break;
            }
            if ($resource_id) {
                $wfTracksQuery->where("resource_id", $resource_id);
            }
            $wfTracks = $wfTracksQuery->get();
            $workflow = new Workflow(['wf_module_id' => $wfModule->id]);
            foreach ($wfTracks as $wfTrack) {
                $level = $wfTrack->wfDefinition->level;
                $definition = $workflow->levelDefinition($level);
                if ($definition) {
                    $wfTrack->wf_definition_id = $definition;
                    $wfTrack->save();
                }
            }
        }
    }

    public function transferWorkflowResource()
    {

    }

    /**
     * @param $resource_id
     * @description Get deactivated wf track of notification report for dataTable
     * @return mixed
     */
    public function getDeactivatedClaimWfTracksForDataTable($resource_id)
    {
        $wf_module_group_rejection = 3;
        $wf_module_group_approval = 4;
        return $this->query()->onlyTrashed()->where('resource_id',$resource_id)->whereHas('wfDefinition', function ($query) use ($wf_module_group_rejection, $wf_module_group_approval){
            $query->whereHas('wfModule', function ($query) use ($wf_module_group_rejection, $wf_module_group_approval)  {
                $query->where('wf_module_group_id', $wf_module_group_rejection)->orWhere('wf_module_group_id', $wf_module_group_approval);
            });
        })->orderBy('id','asc');
    }

    /**
     * @param $resource
     * @param $module
     * @return bool
     * @description Check if the workflow resource have had a completed workflow module trip
     */
    public function checkIfExistWorkflowModule($resource, $module)
    {
        $return = false;
        $count = $this->query()->whereHas("wfDefinition", function ($query) use ($module) {
            $query->whereHas("wfModule", function ($query) use ($module) {
                $query->where("wf_modules.id", $module);
            });
        })->where("resource_id", $resource)->count();
        if ($count)
            $return = true;
        return $return;
    }

    /**
     * @param $resource
     * @param $module
     * @return bool
     * @deprecated
     */
    public function checkIfExistDeclinedWorkflowModule($resource, $module)
    {
        $return = false;
        $count = $this->query()->whereHas("wfDefinition", function ($query) use ($module) {
            $query->whereHas("wfModule", function ($query) use ($module) {
                $query->where("wf_modules.id", $module)->where("allow_decline", 1);
            });
        })->where("resource_id", $resource)->count();
        if ($count)
            $return = true;
        return $return;
    }

    /**
     * @param $resource
     * @param $module
     * @return int 1 ==> Declined, 0 ==> Not Declined/Approved, -1 ==> Unexpected Outcome
     */
    public function checkIfModuleDeclined($resource, $module): int
    {
        $return = -1;
        $query = $this->query()->select(['id','status', 'wf_definition_id'])->where('resource_id', $resource)->whereHas('wfDefinition', function ($query) use ($module) {
            $query->whereHas('wfModule', function ($query) use ($module) {
                $query->where('wf_modules.id', $module);
            })->where('is_approval', 1);
        })->orderByDesc('id')->limit(1)->first();

        if ($query) {
            if ($query->status == 3) {
                $return = 1;
            } else {
                $return = 0;
            }
        }
        return $return;
    }

    /**
     * @param $wf_module_id
     * @param $level
     * @return mixed
     */
    public function previousLevelUser($wf_module_id, $level, $resource_id)
    {
        $return = NULL;
        $query = $this->query()->select(['allocated'])->where("resource_id", $resource_id)->whereHas("wfDefinition", function ($query) use ($wf_module_id, $level) {
            $query->whereHas("wfModule", function ($query) use ($wf_module_id) {
                $query->where("wf_modules.id", $wf_module_id);
            })->where("level", $level);
        })->orderByDesc("id")->limit(1)->first();
        if ($query) {
            $return = $query->allocated;
        }
        return $return;
    }

    /**
     * @param array $input
     * @return mixed
     * @throws WorkflowException
     */
    public function assignAllocation(array $input)
    {
        if (!$input['assigned_user']) {
            throw new WorkflowException("The user to assign the workflow has not been selected.");
        }
        return DB::transaction(function () use ($input) {
            if (isset($input['id']) And ($input['id'])) {
                $resources = $this->query()->find($input['id']);
                foreach ($resources as $resource) {
                    if (!$resource->status) {
                        $resource->user_id = $input['assigned_user'];
                        $resource->assigned = 1;
                        $resource->user_type = 'App\Models\Auth\User';
                        $resource->allocated = $input['assigned_user'];
                        $resource->save();
                    }
                }
            } else {
                throw new WorkflowException("Please select at least one workflow entry.");
            }
            return true;
        });
    }

    public function getLastWorkflowModel($resource_id, $wfModule, $status = 0)
    {
        $query = $this->query()->where(['resource_id' => $resource_id])
        ->whereIn("status", [$status])
        ->whereHas("wfDefinition", function ($query) use ($wfModule) {
            $query->whereHas("wfModule", function ($query) use ($wfModule) {
                $query->where("wf_modules.id", $wfModule);
            });
        })->orderBy("id", "desc")->limit(1);
        return $query;
    }

    public function getLastWorkflow($resource_id, $wfModule)
    {
        $query = $this->query()->where(['resource_id' => $resource_id])
        ->whereHas("wfDefinition", function ($query) use ($wfModule) {
            $query->whereHas("wfModule", function ($query) use ($wfModule) {
                $query->where("wf_modules.id", $wfModule);
            });
        })->orderBy("id", "desc")->limit(1)->first();
        return $query;
    }

    /**
     * @param $resource
     * @param $olduser
     * @param $newuser
     */
    public function transferChecklistAllocationWorkflows($resource, $olduser, $newuser)
    {
        $workflows = (new NotificationWorkflowRepository())->query()->where("notification_report_id", $resource->id)->get();
        foreach ($workflows as $workflow) {

            $wftracks = $this->query()
            ->where(["resource_id" => $workflow->id, "status" => 1])
            ->where("resource_type", "App\Models\Operation\Claim\NotificationWorkflow")
            ->whereHas("wfDefinition", function ($query) {
                $query->where("level", 1);
            })
            ->get();
            foreach ($wftracks as $wftrack) {
                $wftrack->allocated = $newuser;
                $wftrack->save();
            }

            $wftracks = $this->query()
                            //->where(["resource_id" => $workflow->id, "status" => 0, "user_id" => $olduser])
            ->where(["resource_id" => $workflow->id, "status" => 0])
            ->where("resource_type", "App\Models\Operation\Claim\NotificationWorkflow")
            ->whereHas("wfDefinition", function ($query) {
                $query->where("level", 1);
            })
            ->get();
            if ($wftracks->count()) {
                foreach ($wftracks as $wftrack) {
                    $wftrack->comments = "Forwarded by system (Notification allocated to new officer)";
                    $wftrack->status = 1;
                    $wftrack->forward_date = Carbon::now();
                    $wftrack->save();
                    //insert new
                    $insert = [
                        'status' => 0,
                        'resource_id' => $workflow->id,
                        'assigned' => 1,
                        'parent_id' => $wftrack->id,
                        'receive_date' => Carbon::now(),
                        'wf_definition_id' => $wftrack->wf_definition_id,
                        'user_type' => 'App\Models\Auth\User',
                        'resource_type' => 'App\Models\Operation\Claim\NotificationWorkflow',
                        'user_id' => $newuser,
                        'allocated' => $newuser,
                    ];
                    $this->query()->create($insert);
                }
            }

        }
    }

    public function initiateWorkflow(array $input)
    {
        return DB::transaction(function () use ($input) {
            $wfModule = new WfModuleRepository();
            $moduleRepo = new WfModuleRepository();
            $group = $input['group'];
            $resource = $input['resource'];
            $type = $input['type'];
            $comments = $input['comments'];
            $module = $wfModule->getModule(['wf_module_group_id' => $group, 'type' => $type]);
            //check if has access level 1
            access()->hasWorkflowModuleDefinition($group, $type, 1);

            switch ($module) {
                /*                case $wfModule->acknowledgementLetterIssuanceModule()[0]:
                                case $wfModule->benefitAwardLetterIssuanceModule()[0]:
                                    $letter = (new LetterRepository())->find($resource);
                                    $letter->isinitiated = 1;
                                    $letter->save();
                                    break;*/
                                    default:
                    //because only letter workflows are using this route, subsequent resources should use case blocks ...
                                    $letter = (new LetterRepository())->find($resource);
                                    $letter->isinitiated = 1;
                                    $letter->save();
                                    (new LetterRepository())->processLetter(1, $resource, $module);
                                    break;
                                }
            //Initialize Workflow
                                event(new NewWorkflow(['wf_module_group_id' => $group, 'resource_id' => $resource, 'type' => $type], [], ['comments' => $comments]));
                                return $input;
                            });
    }

    /**
     * @param Model $wf_track
     * @param Model $archive
     * @param $action
     * @return array|mixed
     */
    public function archiveProcessDates(Model $wf_track, Model $archive, $action)
    {
        $arr = json_decode($wf_track->process_dates, true) ?? [];
        //$collection = collect($arr);
        //old implementation
        /*if (!$arr) {
            $arr[0] = ['from_date' => $wf_track->receive_date, 'to_date' => $archive->from_date];
        } else {
            if (count($arr) == 2 && array_key_exists( $archive->id , $arr )) {
                $arr[0] = ['from_date' => $wf_track->receive_date, 'to_date' => $archive->from_date];
            }
        }
        if ($action) {

        }*/
        $arr[$archive->id] = ['from_date' => $archive->from_date, 'to_date' => $archive->to_date];
        //$arr[] = ['from_date' => $archive->from_date, 'to_date' => $archive->to_date];
        return $arr;
    }

    public function updateArchiveWorkflow(Model $wf_archive, array $input = [])
    {
        return DB::transaction(function () use ($wf_archive, $input) {
            //check for overlap
            $this->archiveOverlapCheck($input, $wf_archive->id);

            $access_id = access()->id();
            $status = ($input['instore']) ? 0 : 1;
            $receiver_user = (!$status) ? $input['receiver_user'] : NULL;
            $wf_archive->update([
                'description' => $input['description'],
                'from_date' => $input['from_date'],
                'to_date' => $input['to_date'],
                'status' => $status,
                'receiver_user' => $receiver_user,
                'archive_reason_cv_id' => $input['archive_reason_cv_id'],
            ]);
            $wf_track = $wf_archive->wfTrack;
            $wf_track->user_id = $access_id;
            $wf_track->process_dates = json_encode($this->archiveProcessDates($wf_track, $wf_archive, 1));
            $wf_track->save();
            return true;
        });
    }

    /**
     * @param array $input
     * @param null $id
     * @throws GeneralValidationException
     */
    public function archiveOverlapCheck(array $input, $id = NULL)
    {
        $archiveRepo = new WfArchiveRepository();
        $overlapCheck = $archiveRepo->query()->where('wf_track_id', $input['wf_track_id'])->where(function ($query) use ($input) {
            $query->whereRaw("'{$input['from_date']}' between from_date and to_date or '{$input['to_date']}' between from_date and to_date or (from_date > '{$input['from_date']}' and to_date < '{$input['to_date']}')");
        });
        //logger($id);
        if ($id) {
            $overlapCheck->where('id', '<>', $id);
        }
        //logger($overlapCheck->toSql());
        if ($overlapCheck->count()) {
            $overlap = $overlapCheck->first();
            throw new GeneralValidationException("Action cancelled, Archive dates overlap with the previous stored dates. Please check archive <span class='font-weight-bold'>from date : {$overlap->from_date} , to date {$overlap->to_date}</span>");
        }
    }

    public function postArchiveWorkflow(Model $wf_track, $action, array $input = [])
    {
        return DB::transaction(function () use ($wf_track, $action, $input) {
            $archiveRepo = new WfArchiveRepository();
            //check for overlap
            if ($action) {
                $this->archiveOverlapCheck($input);
            }
            switch ($action) {
                case 1:
                    //create archive
                $access_id = access()->id();
                $status = ($input['instore']) ? 0 : 1;
                $receiver_user = (!$status) ? $input['receiver_user'] : NULL;
                $archive = $archiveRepo->query()->create([
                    'description' => $input['description'],
                    'from_date' => $input['from_date'],
                    'to_date' => $input['to_date'],
                    'status' => $status,
                    'receiver_user' => $receiver_user,
                    'wf_track_id' => $wf_track->id,
                    'ref_user' => $wf_track->user_id ?? $access_id,
                    'user_id' => $access_id,
                    'archive_reason_cv_id' => $input['archive_reason_cv_id'],
                ]);
                $now = Carbon::now();
                $to_date = Carbon::parse($input['to_date']);
                    //update wf track
                if ($now->diffInDays($to_date, false) > 0) {
                    $wf_track->status = 6;
                    $wf_track->wf_archive_id = $archive->id;
                    $wf_track->user_id = $access_id;
                } else {
                    $archive->restore_date = $input['to_date'];
                    $archive->save();
                }
                $wf_track->process_dates = json_encode($this->archiveProcessDates($wf_track, $archive, $action));
                $wf_track->save();
                break;
                case 0:
                    //remove archive
                    //update to_date with the current date
                    //update restore_date with the current date
                $archive = $archiveRepo->find($wf_track->wf_archive_id);
                $now = Carbon::now();
                $to_date = Carbon::parse($archive->to_date);
                if ($to_date->diffInDays($now, false) < 0) {
                    $archive->to_date = $now->format('Y-m-d');
                }
                $archive->restore_date = $now->format('Y-m-d');
                $archive->save();

                    //update wf_track
                $wf_track->user_id = $archive->ref_user;
                $wf_track->alert_cv_id = NULL;
                $wf_track->wf_archive_id = NULL;
                $wf_track->status = 0;
                $wf_track->process_dates = json_encode($this->archiveProcessDates($wf_track, $archive, $action));
                $wf_track->save();

                break;
            }
            return true;
        });
    }

    public function transferProgressivePdMonthPayment()
    {
        $wfModuleRepo = new WfModuleRepository();

        $module = $wfModuleRepo->claimBenefitModule()[0];
        $communicate_level = $wfModuleRepo->claimBenefitNotifyEmployerLevel($module);
        $wf_definition = (new WfDefinitionRepository())->query()->where(["wf_module_id" => $module, "level" => $communicate_level])->first();
        $wf_definition_id = $wf_definition->id;
        $sign = 1;
        $eligibles = (new NotificationEligibleBenefitRepository())->query()->whereHas("incident", function ($query) {
            $query->whereHas("claim", function ($query) {
                $query->where("pd", ">", sysdefs()->data()->percentage_imparement_scale);
            });
        })->where(["ispayprocessed" => 1])->whereIn("benefit_type_claim_id", [5])->get();
        foreach ($eligibles as $eligible) {
            $workflow_id = $eligible->notification_workflow_id;
            $query_model = $this->query()->where("resource_id", $workflow_id)->whereHas("wfDefinition", function ($query) use ($module, $communicate_level) {
                $query->whereHas("wfModule", function ($query) use ($module) {
                    $query->where("wf_modules.id", $module);
                })->where("level", $communicate_level);
            })->orderByDesc("id");
            if (!$query_model->count()) {
                $workflow = new Workflow(['wf_module_id' => $module, 'resource_id' => $workflow_id]);
                //close current workflow in case it was not closed...
                $track = $this->query()->where("resource_id", $workflow_id)->whereHas("wfDefinition", function ($query) use ($module, $communicate_level) {
                    $query->whereHas("wfModule", function ($query) use ($module) {
                        $query->where("wf_modules.id", $module);
                    })->whereIn("unit_id", [9, 11]);
                })->orderByDesc("id")->limit(1)->first();
                $input = ['status' => 1];
                $track->update($input);
                //forward workflow to notify employer level...
                $data = [
                    'resource_id' => $workflow_id,
                    'sign' => $sign,
                    'wf_definition' => $wf_definition_id,
                ];
                $workflow->forward($data);
            }
        }
    }


    /**
     * @return bool
     * @throws GeneralException
     */
    public function transferProgressiveZeroPayment()
    {
        //transfer pd zero payment
        $wfModuleRepo = new WfModuleRepository();

        $module = $wfModuleRepo->claimBenefitModule()[0];
        $communicate_level = $wfModuleRepo->claimBenefitNotifyEmployerLevel($module);
        $wf_definition = (new WfDefinitionRepository())->query()->where(["wf_module_id" => $module, "level" => $communicate_level])->first();
        $wf_definition_id = $wf_definition->id;
        $sign = 1;

        $eligibles = (new NotificationEligibleBenefitRepository())->query()->where(["iszeropaid" => 1])->whereIn("benefit_type_claim_id", [3,4,5])->get();
        foreach ($eligibles as $eligible) {
            $workflow_id = $eligible->notification_workflow_id;
            $query_model = $this->query()->where("resource_id", $workflow_id)->whereHas("wfDefinition", function ($query) use ($module, $communicate_level) {
                $query->whereHas("wfModule", function ($query) use ($module) {
                    $query->where("wf_modules.id", $module);
                })->where("level", $communicate_level);
            })->orderByDesc("id");
            if (!$query_model->count()) {
                $workflow = new Workflow(['wf_module_id' => $module, 'resource_id' => $workflow_id]);

                //close current workflow in case it was not closed...
                $track = $this->query()->where("resource_id", $workflow_id)->whereHas("wfDefinition", function ($query) use ($module, $communicate_level) {
                    $query->whereHas("wfModule", function ($query) use ($module) {
                        $query->where("wf_modules.id", $module);
                    })->whereIn("unit_id", [9, 14]);
                })->orderByDesc("id")->limit(1)->first();
                $input = ['status' => 1];
                $track->update($input);

                //forward workflow to notify employer level...
                $data = [
                    'resource_id' => $workflow_id,
                    'sign' => $sign,
                    'wf_definition' => $wf_definition_id,
                ];
                $workflow->forward($data);
                //wake up notification eligibles
                $eligible->processed = 0;
                $eligible->save();
                //wake up notification workflows
                $notificationWorkflow = $eligible->workflow;
                $notificationWorkflow->wf_done = 0;
                $notificationWorkflow->save();
            }
        }

        //transfer td zero payment
        $eligibles = (new NotificationEligibleBenefitRepository())->query()->where(["processed" => 0])->whereIn("benefit_type_claim_id", [3,4])->get();
        foreach ($eligibles as $eligible) {
            $workflow_id = $eligible->notification_workflow_id;
            $query_model = $this->query()->where(["resource_id" => $workflow_id, "status" => 0])->whereHas("wfDefinition", function ($query) use ($module, $communicate_level) {
                $query->whereHas("wfModule", function ($query) use ($module) {
                    $query->where("wf_modules.id", $module);
                })->whereIn("unit_id", [8, 12]);
            })->orderByDesc("id");
            if ($query_model->count()) {
                $incident = $eligible->incident;
                $td_total_lumpsum = (new ClaimCompensationRepository())->progressiveCompensationApprove($incident, 0, $eligible->id)['td'][0]['total_lumpsum'];
                if ($td_total_lumpsum == 0) {
                    $workflow = new Workflow(['wf_module_id' => $module, 'resource_id' => $workflow_id]);
                    //close current workflow
                    $wf_track = $query_model->limit(1)->first();
                    $input = ['status' => 1, 'comments' => "Zero Paid", 'forward_date' => Carbon::now()];
                    $wf_track->update($input);

                    //forward workflow to notify employer level...
                    $data = [
                        'resource_id' => $workflow_id,
                        'sign' => $sign,
                        'wf_definition' => $wf_definition_id,
                    ];
                    $workflow->forward($data);

                    $eligible->iszeropaid = 1;
                    $eligible->save();

                }
            }
        }

        return true;
    }

    /**
     * @deprecated
     * @description function not stable, do not use it
     */
    public function updateTdZeroPaidProgressive()
    {
        $wfModuleRepo = new WfModuleRepository();
        $module = $wfModuleRepo->claimBenefitModule()[0];
        $communicate_level = $wfModuleRepo->claimBenefitNotifyEmployerLevel($module);
        //transfer td zero payment
        $eligibles = (new NotificationEligibleBenefitRepository())->query()->whereIn("benefit_type_claim_id", [3,4])->get();
        foreach ($eligibles as $eligible) {
            $workflow_id = $eligible->notification_workflow_id;
            $query_model = $this->query()->where(["resource_id" => $workflow_id])->whereHas("wfDefinition", function ($query) use ($module, $communicate_level) {
                $query->whereHas("wfModule", function ($query) use ($module) {
                    $query->where("wf_modules.id", $module);
                })->whereIn("unit_id", [14])->where("level", "<=", 4);
            })->orderByDesc("id");
            if ($query_model->count()) {
                //$incident = $eligible->incident;
                //$td_total_lumpsum = (new ClaimCompensationRepository())->progressiveCompensationApprove($incident, 0, $eligible->id)['td'][0]['total_lumpsum'];
                //if ($td_total_lumpsum == 0) {
                //$eligible->iszeropaid = 0;
                //$eligible->save();
                //}
            }
        }
    }

    public function closeLastWorkflow($module, $user, $comments)
    {
        $level = (new WfDefinitionRepository())->query()->where("wf_module_id", $module)->max("level");

        $moduleRepo = new WfModuleRepository();

        $count = $this->query()->whereHas("wfDefinition", function ($query) use ($module, $level) {
            $query->whereHas("wfModule", function ($query) use ($module, $level) {
                $query->where("wf_modules.id", $module);
            })->where("level", $level);
        })->where("status", 0)
        /*->toSql();*/
        /*->count();*/
        ->chunk(5000, function ($tracks) use ($moduleRepo, $module, $user, $comments) {
            foreach ($tracks as $track) {

                $track->user_id = $user;
                    //$track->allocated = $user;
                $track->assigned = 1;
                $track->user_type = "App\Models\Auth\User";
                $track->comments = $comments;
                $track->forward_date = Carbon::now();
                $track->status = 5;
                $track->save();
                switch ($module) {
                    case $moduleRepo->receiptVerificationModule()[0]:
                    case $moduleRepo->receiptVerificationModule()[1]:
                    case $moduleRepo->receiptVerificationAdvanceModule()[0]:
                    (new ReceiptRepository())->query()->where("id", $track->resource_id)->update(["iscomplete" => 1, "isverified" => 1]);
                    break;
                }
            }
        });
        //dd($count);
    }

    /**
     * @param $fromModule
     * @param $toModule
     * @return bool
     * @throws GeneralException
     */
    public function transferIntermediateWorkflow($fromModule, $toModule): bool
    {
        $maxlevel = (new WfDefinitionRepository())->query()->where("wf_module_id", $fromModule)->max("level");
        $workflow = new Workflow(['wf_module_id' => $toModule]);

        $this->query()->whereHas("wfDefinition", function ($query) use ($fromModule, $maxlevel) {
            $query->where("wf_module_id", $fromModule)->where("level", "<", $maxlevel);
        })->where("status", 0)->chunk(5000, function($wfTracks) use ($workflow) {
            foreach ($wfTracks as $wfTrack) {
                $level = $wfTrack->wfDefinition->level;
                $definition = $workflow->levelDefinition($level);
                $wfTrack->wf_definition_id = $definition;
                $wfTrack->save();
            }
        });
        return true;
    }

    public function getWfModuleById($instalment_id)
    {
        $level = -1;

        $module = DB::table('main.wf_tracks')->select('wf_definition_id')->where('resource_id',$instalment_id)->where('resource_type','App\Models\Operation\Compliance\Member\InstallmentRequest')->orderBy('id','desc')->first();

        if ($module) {
            $defn = DB::table('main.wf_definitions')->select('level')->where('id',$module->wf_definition_id)->first();
            $level = $defn;
        }

        return $level;
    }

    public function getWfModuleDgApproval($instalment_id)
    {

        $module = DB::table('main.wf_tracks')->select('forward_date')
        ->join('wf_definitions','wf_definitions.id','=','wf_tracks.wf_definition_id')->where('resource_id',$instalment_id)->where('resource_type','App\Models\Operation\Compliance\Member\InstallmentRequest')->whereIn('level',[5.0,5.4])->where('status', 1 )->first();
        if($module){
          return $module->forward_date ? Carbon::parse($module->forward_date)->format('d F, Y') : null;
      }else{
        return null;
    }

}


public function getReversedCount()
{
    $pendings = $this->getReversedQuery();
    return $pendings->count();
}

public function getReversedModuleCount($id)
{
    $pendings = $this->getReversedQuery();
    return $pendings->where("wf_modules.id", $id)->count();
}

public function getReversedQuery()
{
    // $this->getAllTracksWhoseParentHasReversed();
    $pendings = $this->getWorkflowQuery()
    ->join("wf_tracks as parent", "parent.id", "=", "wf_tracks.parent_id")
    ->whereHas("wfDefinition", function ($query) {
        $query->whereHas("users", function ($subQuery) {
            $subQuery->whereIn("user_wf_definition.user_id", access()->allUsers());
        });
    })
    ->where(function ($query) {
        $query->where(['wf_tracks.status' => 0])->whereNotNull('wf_tracks.parent_id');
    })->where('parent.status',2)->where('wf_tracks.user_id',access()->user()->id);
    return $pendings;
}

public function getAllTracksWhoseParentHasReversed()
{
    $all_pending = $this->query()
    ->join("wf_tracks as parent", "parent.id", "=", "wf_tracks.parent_id")
    ->where('wf_tracks.status',0)->where('parent.status',2)
    // ->where('wf_tracks.resource_type','App\Models\Operation\Claim\NotificationWorkflow')
    ->whereNotNull('wf_tracks.parent_id')->where('wf_tracks.user_id',access()->user()->id);


    // dd($all_pending);

}



public function getSubordinatesForWorkflowDatatable($user_id)
{
    if (request()->has('state')) {
        $state = request()->input('state');
        switch ($state) {
            case "full":
            $pendings = $this->getSuborniteQuery($user_id);
            break;
            case "assigned":
            $pendings = $this->getMyPendingQuery()->whereIn("user_id", access()->allUsers());
            break;
            case "attended":
            $pendings = $this->getAttendedQuery();
            break;
            case "archived":
            $pendings = $this->getAttendedQuery(6);
            break;
            default:
            $pendings = $this->getSuborniteQuery($user_id);
        }
        switch ($state) {
            case "attended":
            case "archived":
            switch (request()->input('status')) {
                case '3':
                /* Assigned to User */
                $user_id = request()->input("user_id");
                $pendings->where('user_id', $user_id);
                break;
                case '2':
                /* By All */
                break;
                default:
                /* Attended by Me */
                $pendings->where('user_id', access()->id());
                break;
            }
            break;
            case "assigned":
            break;
            default:
            switch (request()->input("status")) {
                case '0':
                /* Not Assigned */
                $pendings->where("assigned", 0);
                break;
                case '1':
                /* Assigned to Me */
                $pendings->where("user_id", access()->id());
                break;
                case '2':
                /* All */
                break;
                case '3':
                /* Assigned to User */
                $user_id = request()->input("user_id");
                $pendings->where("user_id", $user_id);
                break;
                default:
                break;
            }
            break;
        }
    } else {
        $pendings = $this->getSuborniteQuery($user_id);
    }

    $pendings = $pendings->where('user_id',$user_id);

    $search = request()->input('search');
    $wf_module_group_id = request()->input('wf_module_group_id');
    $wf_module_id = request()->input('wf_module_id');
        //logger($wf_module_group_id);

    if ($wf_module_id) {
            //Filter By Workflow Module Id
        $pendings->where("wf_modules.id", $wf_module_id);
    }

    if (true) {
        if (true) {
            switch ($wf_module_group_id) {
                case '1':
                /* Interest Waiving */
                if ($search != "") {
                    $pendings->whereHas("interestWriteOff", function ($query) use ($search) {
                        $query->whereHas("bookingInterests", function ($subQuery) use ($search) {
                            $subQuery->whereHas("booking", function ($sub2Query) use ($search) {
                                $sub2Query->where("employer_id", "=", $search);
                            });
                        });
                    });
                }
                break;
                case '2':
                /*Interest Adjustment*/
                if ($search != "") {
                    $pendings->whereHas("bookingInterest", function ($query) use ($search) {
                        $query->whereHas("booking", function ($subQuery) use ($search) {
                            $subQuery->where("employer_id", "=", $search);
                        });
                    });
                }
                break;
                case '3':
                case '4':
                /*Claim & Notification Processing*/
                /* Notification Rejection */
                $incident = (int) request()->input("incident");
                if ($incident || $search != "") {
                    $wfModule = (new WfModuleRepository())->find($wf_module_id);
                    $type = $wfModule->type;
                    if ($type >= 4) {
                                //This is progressive notification
                        $pendings->whereHas("notificationWorkflow", function ($query) use ($search, $wf_module_id, $incident) {
                            $query->where("wf_module_id", $wf_module_id);
                            if ($incident Or $search != "") {
                                $query->whereHas("notificationReport", function ($query) use ($search, $incident) {
                                    if ($incident) {
                                        $query->where("incident_type_id", "=", $incident);
                                    }
                                    if ($search != "") {
                                                //$query->where("employee_id", "=", $search);
                                        $query->where(function ($query) use ($search) {
                                            $query->orWhere("employee_id", "=", $search)->orWhere("employer_id", "=", $search);
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                                //This is legacy notification report
                        $pendings->whereHas("notificationReport", function ($query) use ($search, $incident) {
                            if ($incident) {
                                $query->where("incident_type_id", "=", $incident);
                            }
                            if ($search != "") {
                                        //$query->where("employee_id", "=", $search);
                                $query->where(function ($query) use ($search) {
                                    $query->orWhere("employee_id", "=", $search)->orWhere("employer_id", "=", $search);
                                });
                            }
                        });
                    }
                }
                        //Include progressive notification and non progressive notification.
                break;
                case '5':
                /* Contribution Receipt */
                if ($search != "") {
                    $operator = request()->input("operator");
                    $pendings->whereHas("receipt", function ($query) use ($search, $operator) {
                        if ($operator == 'like') {
                            $query->whereRaw("rctno::varchar ~* ?", [$search]);
                        } else {
                            $query->where("rctno", "=", $search);
                        }
                    });
                }
                break;
                case '6':
                /* Employer registration */
                if ($search != "") {
                    $pendings->whereHas("employer", function ($query) use ($search) {
                        $query->where("id", "=", $search)->withoutGlobalScopes([IsApprovedScope::class]);
                    });
                }
                break;
                case '7':
                /* Contribution Receipt Legacy */
                if ($search != "") {
                    $operator = request()->input("operator");
                    $pendings->whereHas("legacyReceipt", function ($query) use ($search, $operator) {
                        if ($operator == 'like') {
                            $query->whereRaw("rctno::varchar ~* ?", [$search]);
                        } else {
                            $query->where("rctno", "=", $search);
                        }
                    });
                }
                break;
                case '8':
                if ($search != "") {
                    /* Online Employer Verification */
                    $pendings->whereHas("onlineEmployerVerification", function ($query) use ($search) {
                        $query->whereHas("employer", function ($subQuery) use ($search) {
                            $subQuery->where("employer_id", "=", $search);
                        });
                    });
                }
                break;
                case '9':
                /* Interest Refund */
                break;

                case '10':
                /* Payroll run Approval */
                $pendings->has('payrollRunApproval');
                break;

                case '11':
                /* Payroll bank details modification */
                $member_type_id = request()->input('member_type_id');
                $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                if ((isset($member_type_id) && isset($resource_id  ))) {
                    $pendings->whereHas("payrollBankInfoUpdate", function ($query) use ($resource_id, $member_type_id) {
                        $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                    });
                }else{
                    $pendings->has('payrollBankInfoUpdate');
                }
                break;

                case '12':
                /* Payroll status change */
                $member_type_id = request()->input('member_type_id');
                $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                if ((isset($member_type_id) && isset($resource_id  ))) {
                    $pendings->whereHas("payrollStatusChange", function ($query) use ($resource_id, $member_type_id) {
                        $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                    });
                }else{
                    $pendings->has('payrollStatusChange');
                }
                break;
                case '13':
                /* Payroll recovery processing */
                $member_type_id = request()->input('member_type_id');
                $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                if ((isset($member_type_id) && isset($resource_id  ))) {
                    $pendings->whereHas("payrollRecovery", function ($query) use ($resource_id, $member_type_id) {
                        $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                    });
                }else{

                    $pendings->has('payrollRecovery');
                }
                break;
                case '14':
                /* Payroll reconciliation */
                $member_type_id = request()->input('member_type_id');
                $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                if ((isset($member_type_id) && isset($resource_id  ))) {
                    $pendings->whereHas("payrollReconciliation", function ($query) use ($resource_id, $member_type_id) {
                        $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                    });
                }else{

                    $pendings->has('payrollReconciliation');
                }
                break;

                case '15':
                /* Payroll beneficiary updates */
                $member_type_id = request()->input('member_type_id');
                $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                if ((isset($member_type_id) && isset($resource_id  ))) {
                    $pendings->whereHas("payrollBeneficiaryUpdate", function ($query) use ($resource_id, $member_type_id) {
                        $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                    });
                }else{

                    $pendings->has('payrollBeneficiaryUpdate');
                }
                break;
                default:
                break;
            }
        }
    }
        /*
         * dd($pendings->get()->toArray());
        dd($pendings->toSql());
        */
        $datatables = app('datatables')
        ->of($pendings)
        ->addColumn("resource_name", function ($query) {
            return ($query->resource) ? $query->resource->resource_name : "";
        })
        ->editColumn("resource_id", function ($query) {
                //return (count($query->resource)) ? $query->resource->id : "";
                //if ($query->resource_type == "App\Models\Operation\Claim\NotificationWorkflow");
            if (is_null($query->resource_type) Or empty($query->resource_type)) {
                $result = 0;
            } else {
                $result = $query->resource()->count();
            }
            switch ($query->resource_type) {
                case "App\Models\Operation\Claim\NotificationWorkflow":
                $return = (($result) ? $query->resource->notificationReport->id : "");
                        //$return = 1;
                break;
                case "App\Models\Operation\Compliance\Inspection\EmployerInspectionTaskWorkflow":
                $return = (($result) ? $query->resource->employerTask->id : "");
                break;
                default:
                $return = (($result) ? $query->resource->id : "");
                        //$return = 1;
                break;
            }
            return $return;
        })
        ->addColumn("receive_date_formatted", function ($query) {
            return $query->receive_date_formatted;
        })
        ->addColumn("assign_status", function ($query) {
            return $query->assign_status;
        })

        ->rawColumns(['assign_status']);

        return $datatables;
    }   


    public function getSubornitesPendingCount()
    {
        $pendings = $this->getSubornitesQuery();
        return $pendings->count();
    }

    public function getSubornitesModuleCount($user_id, $id)
    {
        $pendings = $this->getSuborniteQuery($user_id);
        return $pendings->where("wf_modules.id", $id)->count();
    }


    public function getSuborniteQuery($subordinate_id)
    {
        $pendings = $this->getWorkflowQuery()
        ->whereHas("wfDefinition", function ($query) use($subordinate_id){
            $query->whereHas("users", function ($subQuery) use($subordinate_id) {
                $subQuery->where("user_wf_definition.user_id", $subordinate_id);
            });
        })
        ->where(function ($query) use($subordinate_id) {
            $query->where(['wf_tracks.status' => 0])
            ->orWhere(['wf_tracks.assigned' => 0]);
        })->where('user_id',$subordinate_id);
        return $pendings;
    }

    public function getSubornitesQuery()
    {
        $pendings = $this->getWorkflowQuery()
        ->whereHas("wfDefinition", function ($query) {
            $query->whereHas("users", function ($subQuery) {
                $subQuery->whereIn("user_wf_definition.user_id", access()->user()->SubordinatesIds);
            });
        })
        ->where(function ($query) {
            $query->where(['wf_tracks.status' => 0])
            ->orWhere(['wf_tracks.assigned' => 0]);
        })->whereIn('user_id',access()->user()->SubordinatesIds);
        return $pendings;
    }


    public function getReversedForWorkflowDatatable()
    {
        if (request()->has('state')) {
            $state = request()->input('state');
            switch ($state) {
                case "full":
                $pendings = $this->getAllPendingQuery();
                break;
                case "assigned":
                $pendings = $this->getMyPendingQuery()->whereIn("user_id", access()->allUsers());
                break;
                case "attended":
                $pendings = $this->getAttendedQuery();
                break;
                case "archived":
                $pendings = $this->getAttendedQuery(6);
                break;
                default:
                $pendings = $this->getPendingQuery();
            }
            switch ($state) {
                case "attended":
                case "archived":
                switch (request()->input('status')) {
                    case '3':
                    /* Assigned to User */
                    $user_id = request()->input("user_id");
                    $pendings->where('user_id', $user_id);
                    break;
                    case '2':
                    /* By All */
                    break;
                    default:
                    /* Attended by Me */
                    $pendings->where('user_id', access()->id());
                    break;
                }
                break;
                case "assigned":
                break;
                default:
                switch (request()->input("status")) {
                    case '0':
                    /* Not Assigned */
                    $pendings->where("assigned", 0);
                    break;
                    case '1':
                    /* Assigned to Me */
                    $pendings->where("user_id", access()->id());
                    break;
                    case '2':
                    /* All */
                    break;
                    case '3':
                    /* Assigned to User */
                    $user_id = request()->input("user_id");
                    $pendings->where("user_id", $user_id);
                    break;
                    default:
                    break;
                }
                break;
            }
        } else {
            $pendings = $this->getPendingQuery();
        }

        $pendings = $pendings->join("wf_tracks as parent", "parent.id", "=", "wf_tracks.parent_id")
        ->where('wf_tracks.status',0)->where('parent.status',2)->whereNotNull('wf_tracks.parent_id');

        $search = request()->input('search');
        $wf_module_group_id = request()->input('wf_module_group_id');
        $wf_module_id = request()->input('wf_module_id');
        //logger($wf_module_group_id);

        if ($wf_module_id) {
            //Filter By Workflow Module Id
            $pendings->where("wf_modules.id", $wf_module_id);
        }

        if (true) {
            if (true) {
                switch ($wf_module_group_id) {
                    case '1':
                    /* Interest Waiving */
                    if ($search != "") {
                        $pendings->whereHas("interestWriteOff", function ($query) use ($search) {
                            $query->whereHas("bookingInterests", function ($subQuery) use ($search) {
                                $subQuery->whereHas("booking", function ($sub2Query) use ($search) {
                                    $sub2Query->where("employer_id", "=", $search);
                                });
                            });
                        });
                    }
                    break;
                    case '2':
                    /*Interest Adjustment*/
                    if ($search != "") {
                        $pendings->whereHas("bookingInterest", function ($query) use ($search) {
                            $query->whereHas("booking", function ($subQuery) use ($search) {
                                $subQuery->where("employer_id", "=", $search);
                            });
                        });
                    }
                    break;
                    case '3':
                    case '4':
                    /*Claim & Notification Processing*/
                    /* Notification Rejection */
                    $incident = (int) request()->input("incident");
                    if ($incident || $search != "") {
                        $wfModule = (new WfModuleRepository())->find($wf_module_id);
                        $type = $wfModule->type;
                        if ($type >= 4) {
                                //This is progressive notification
                            $pendings->whereHas("notificationWorkflow", function ($query) use ($search, $wf_module_id, $incident) {
                                $query->where("wf_module_id", $wf_module_id);
                                if ($incident Or $search != "") {
                                    $query->whereHas("notificationReport", function ($query) use ($search, $incident) {
                                        if ($incident) {
                                            $query->where("incident_type_id", "=", $incident);
                                        }
                                        if ($search != "") {
                                                //$query->where("employee_id", "=", $search);
                                            $query->where(function ($query) use ($search) {
                                                $query->orWhere("employee_id", "=", $search)->orWhere("employer_id", "=", $search);
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                                //This is legacy notification report
                            $pendings->whereHas("notificationReport", function ($query) use ($search, $incident) {
                                if ($incident) {
                                    $query->where("incident_type_id", "=", $incident);
                                }
                                if ($search != "") {
                                        //$query->where("employee_id", "=", $search);
                                    $query->where(function ($query) use ($search) {
                                        $query->orWhere("employee_id", "=", $search)->orWhere("employer_id", "=", $search);
                                    });
                                }
                            });
                        }
                    }
                        //Include progressive notification and non progressive notification.
                    break;
                    case '5':
                    /* Contribution Receipt */
                    if ($search != "") {
                        $operator = request()->input("operator");
                        $pendings->whereHas("receipt", function ($query) use ($search, $operator) {
                            if ($operator == 'like') {
                                $query->whereRaw("rctno::varchar ~* ?", [$search]);
                            } else {
                                $query->where("rctno", "=", $search);
                            }
                        });
                    }
                    break;
                    case '6':
                    /* Employer registration */
                    if ($search != "") {
                        $pendings->whereHas("employer", function ($query) use ($search) {
                            $query->where("id", "=", $search)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }
                    break;
                    case '7':
                    /* Contribution Receipt Legacy */
                    if ($search != "") {
                        $operator = request()->input("operator");
                        $pendings->whereHas("legacyReceipt", function ($query) use ($search, $operator) {
                            if ($operator == 'like') {
                                $query->whereRaw("rctno::varchar ~* ?", [$search]);
                            } else {
                                $query->where("rctno", "=", $search);
                            }
                        });
                    }
                    break;
                    case '8':
                    if ($search != "") {
                        /* Online Employer Verification */
                        $pendings->whereHas("onlineEmployerVerification", function ($query) use ($search) {
                            $query->whereHas("employer", function ($subQuery) use ($search) {
                                $subQuery->where("employer_id", "=", $search);
                            });
                        });
                    }
                    break;
                    case '9':
                    /* Interest Refund */
                    break;

                    case '10':
                    /* Payroll run Approval */
                    $pendings->has('payrollRunApproval');
                    break;

                    case '11':
                    /* Payroll bank details modification */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollBankInfoUpdate", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{
                        $pendings->has('payrollBankInfoUpdate');
                    }
                    break;

                    case '12':
                    /* Payroll status change */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollStatusChange", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{
                        $pendings->has('payrollStatusChange');
                    }
                    break;
                    case '13':
                    /* Payroll recovery processing */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollRecovery", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{

                        $pendings->has('payrollRecovery');
                    }
                    break;
                    case '14':
                    /* Payroll reconciliation */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollReconciliation", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{

                        $pendings->has('payrollReconciliation');
                    }
                    break;

                    case '15':
                    /* Payroll beneficiary updates */
                    $member_type_id = request()->input('member_type_id');
                    $resource_id = ($member_type_id == 4) ? request()->input('dependent_id') : request()->input('pensioner_id');
                    if ((isset($member_type_id) && isset($resource_id  ))) {
                        $pendings->whereHas("payrollBeneficiaryUpdate", function ($query) use ($resource_id, $member_type_id) {
                            $query->where("resource_id", "=", $resource_id)->where("member_type_id", "=", $member_type_id)->withoutGlobalScopes([IsApprovedScope::class]);
                        });
                    }else{

                        $pendings->has('payrollBeneficiaryUpdate');
                    }
                    break;
                    default:
                    break;
                }
            }
        }
        /*
         * dd($pendings->get()->toArray());
        dd($pendings->toSql());
        */
        $datatables = app('datatables')
        ->of($pendings)
        ->addColumn("resource_name", function ($query) {
            return ($query->resource) ? $query->resource->resource_name : "";
        })
        ->editColumn("resource_id", function ($query) {
                //return (count($query->resource)) ? $query->resource->id : "";
                //if ($query->resource_type == "App\Models\Operation\Claim\NotificationWorkflow");
            if (is_null($query->resource_type) Or empty($query->resource_type)) {
                $result = 0;
            } else {
                $result = $query->resource()->count();
            }
            switch ($query->resource_type) {
                case "App\Models\Operation\Claim\NotificationWorkflow":
                $return = (($result) ? $query->resource->notificationReport->id : "");
                        //$return = 1;
                break;
                case "App\Models\Operation\Compliance\Inspection\EmployerInspectionTaskWorkflow":
                $return = (($result) ? $query->resource->employerTask->id : "");
                break;
                default:
                $return = (($result) ? $query->resource->id : "");
                        //$return = 1;
                break;
            }
            return $return;
        })
        ->addColumn("receive_date_formatted", function ($query) {
            return $query->receive_date_formatted;
        })
        ->addColumn("assign_status", function ($query) {
            return $query->assign_status;
        })

        ->rawColumns(['assign_status']);

        return $datatables;
    }   


    public function getSubordinatesSummaryDatatable()
    {
        $subordinates = $this->query()
        ->select(DB::raw('users.id as user_id'),DB::raw("concat_ws(' ', users.firstname, users.lastname) as username"),DB::raw('count(wf_tracks.id) as workflows'))
        ->join("users", "wf_tracks.user_id", "=", "users.id")
        ->where('status',0)->whereIn('wf_tracks.user_id',access()->user()->SubordinatesIds)
        ->groupBy(DB::raw("concat_ws(' ', users.firstname, users.lastname)"),'users.id');

        $datatables = app('datatables')->of($subordinates);
        return $datatables;
    }

    /**
     * @param Model $definition
     * @param $resourceid
     * @param $wftrackid
     * @throws WorkflowException
     */
    public function checkWfDefinition(Model $definition, $resourceid, $wftrackid) {
        $wfModuleRepo = new WfModuleRepository();
        $notificationWorkflowRepo = new NotificationWorkflowRepository();
        $notificationEligibleRepo = new NotificationEligibleBenefitRepository();
        $benefitTypeRepo = new BenefitTypeRepository();
        $documentBenefitRepo = new DocumentBenefitRepository();
        $notificationRepo = new NotificationReportRepository();
        $documentRepo = new DocumentRepository();
        switch ($definition->unit_id) {
            case 14:
                //Claim Administration
                $user_id = access()->id();
                //$docreports = [];
                //Check if Benefit Document Read
                if (in_array($definition->wf_module_id, $wfModuleRepo->allClaimBenefitModule())) {
                    //Claim Benefit Document
                    //Check module_id from notification_workflows
                    $eligible = $notificationEligibleRepo->query()->where("notification_workflow_id", $resourceid)->first();
                    $benefit_types = $eligible->benefitTypes()->pluck("benefit_types.id")->all();
                    $incident = $eligible->incident;
                    //logger($benefit_types);
                    $docs = $notificationRepo->getProgressiveDocumentList($incident, $benefit_types);
                    //select all documents that this user has not checked ...
                    //logger($docreports);
                    //throw new WorkflowException("Testing Purpose");
                    //$checkeddocs = WfDocumentCheck::query()->where("wf_track_id", $wftrackid)->pluck("document_id")->all();
                    //$documents = $documentRepo->query()->whereIn("id", $docs)->whereNotIn("id", $checkeddocs)->pluck("name")->all();
                }
                //Check if Validation Document Read
                if (in_array($definition->wf_module_id, $wfModuleRepo->notificationIncidentApprovalModule())) {
                    $notificationWorkflow = $notificationWorkflowRepo->find($resourceid);
                    $incident = $notificationWorkflow->notificationReport;
                    $docs = $notificationRepo->getProgressiveDocumentList($incident, []);
                }
                if ($docs) {
                    $doclist = implode(",", $docs);
                    //logger($doclist);
                    $docreports = collect(DB::select("select '<b>' || dc.name || '</b> [C: <u>' || coalesce(dnr.created_at::date::varchar, '-') || '</u>]' as name from main.document_notification_report dnr join documents dc on dnr.document_id = dc.id where dnr.notification_report_id = {$incident->id} and dnr.document_id in ({$doclist}) and dnr.id not in (select document_notification_report_id from wf_document_checks where user_id = {$user_id})"))->pluck("name")->all();
                    //logger($docreports);
                    if (count($docreports)) {
                        $names = implode(", ", $docreports);
                        throw new WorkflowException("You need to review the following documents, " . $names . "");
                    }
                }
                break;
        }
    }


}
