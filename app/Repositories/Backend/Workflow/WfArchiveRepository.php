<?php

namespace App\Repositories\Backend\Workflow;

use App\Models\Workflow\WfArchive;
use App\Repositories\BaseRepository;

class WfArchiveRepository extends BaseRepository
{
    public const MODEL = WfArchive::class;

}