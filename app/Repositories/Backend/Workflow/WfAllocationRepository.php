<?php

namespace App\Repositories\Backend\Workflow;

use App\Models\Workflow\WfAllocation;
use App\Models\Workflow\WfDefinition;
use App\Models\Workflow\WfTrack;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WfAllocationRepository
 * @package App\Repositories\Backend\Workflow
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz> | Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 */
class WfAllocationRepository extends BaseRepository
{
    const MODEL = WfAllocation::class;

    /**
     * @param Model $definition
     * @return |null
     */
    public function getUserFromPointer(Model $definition)
    {
        $user = NULL;
        $count = $definition->users()->count();
        if ($count) {
            $model = $this->query()->updateOrCreate(
                ['wf_definition_id' => $definition->id]
            );
            $pointer = $model->pointer;
            if ($pointer % $count == 0 Or $pointer > $count) {
                $pointer = 0;
            }
            //logger("Wf definition : " . $definition->id);
            //logger("Pointer : " . $pointer);
            $user = $definition->users()->select(["users.id"])->orderBy("user_wf_definition.id")->offset($pointer)->limit(1)->first()->id;
            $model->pointer = $pointer + 1;
            $model->save();
        }
        return $user;
    }

    /**
     * @param Model $nextDefinition
     * @param null $currentLevel
     * @param null $resourceId
     * @return mixed|null
     */
    public function getAllocation(Model $nextDefinition, $currentLevel = NULL, $resourceId = NULL)
    {
        $wfTrackRepo = new WfTrackRepository();
        if ($nextDefinition->isrestrictive) {
            //check if this level is restrictive and return the same last person participated
            $user = $wfTrackRepo->previousLevelUser($nextDefinition->wf_module_id, $nextDefinition->level, $resourceId);
            if (!$user) {
                //there is no user participated in the same workflow before
                $user = $this->getUserFromPointer($nextDefinition);
                if ($user) {
                    //check if user has participated in some previous workflow ...
                    $return = $wfTrackRepo->hasPreviousParticipated($nextDefinition->wf_module_id, $resourceId, $currentLevel, $user);
                    if ($return) {
                        $new_user = $this->getUserFromPointer($nextDefinition);
                        if ($user == $new_user) {
                            $user = NULL;
                        } else {
                            $user = $new_user;
                        }
                    }
                }
            }
        } else {
            $user = $this->getUserFromPointer($nextDefinition);
        }

        return $user;
    }

    /*Get Allocation by ref level - Get last user participated on ref level for this resource*/
    public function getAllocationByRefLevel(Model $wf_def, $resource_id)
    {
        //$wf_def = WfDefinition::query()->find($wf_definition_id);
        $ref_wf_def_id = WfDefinition::query()->where('level', $wf_def->ref_level)->where('wf_module_id', $wf_def->wf_module_id)->first()->id;
        /*Last user participated on the ref level for this resource*/
        $last_participated_user_id = WfTrack::query()->where('resource_id', $resource_id)->where('wf_definition_id', $ref_wf_def_id)->orderBy('id', 'desc')->first()->user_id;

        return $last_participated_user_id;
    }

}