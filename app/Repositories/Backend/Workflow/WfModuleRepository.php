<?php

namespace App\Repositories\Backend\Workflow;

use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\BaseRepository;
use App\Models\Workflow\WfModule;
use App\Exceptions\GeneralException;

/**
 * Class WfModuleRepository
 * @package App\Repositories\Backend\Workflow
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz|gwanchi@gmail.com>
 */
class WfModuleRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = WfModule::class;

    public function getAllActive()
    {
        return $this->query()->where("isactive", 1)->orderby("name", "asc")->get();
    }


    /**
     * @param array $input
     * @return mixed
     * @throws GeneralException
     */
    public function getModuleInstance(array $input)
    {
        /** sample 1 $input : ['wf_module_group_id' => 4, 'resource_id' => 1] **/
        /** sample 2 $input : ['wf_module_group_id' => 3, 'resource_id' => 1, 'type' => 4] **/
        /** sample 3 $input : ['wf_module_group_id' => 3] **/
        /** sample 4 $input : ['wf_module_group_id' => 3, 'type' => 4] **/
        $module_group = $input['wf_module_group_id'];
        $selectArray = ['id', 'name'];
        $type = 0;
        switch ($module_group) {
            //All specified in case blocks has varieties, module group with more than one modules

            case 2://Interest adjustment
            case 33://Interest adjustment
                $type = 1;//default
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                }
                $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type])->select($selectArray)->orderBy("id", "asc")->first();

                break;
            case 3:
                //Notification & Claim Processing (Depend on independent "type" not directly related to "type" of resource)

                $type = 1;
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                } else {
                    if (isset($input['resource_id'])) {
                        $resource_id = $input['resource_id'];
                        $type = $this->getDefaultClaimType($resource_id);
                    }
                }
                $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type, 'isactive' => 1])->select($selectArray)->orderBy("id", "asc")->first();
                break;
            case 4:
                //Notification Rejection (Depend on independent "type" not directly related to "type" of resource)
                $type = 2;
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                } else {
                    if (isset($input['resource_id'])) {
                        $resource_id = $input['resource_id'];
                        $type = $this->getDefaultNotificationRejectionType($resource_id);
                    }
                }
                $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type, 'isactive' => 1])->select($selectArray)->orderBy("id", "asc")->first();
                break;
            case 5:
                //Contribution Receipt
                $type = 1;
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                } else {
                    if (isset($input['resource_id'])) {
                        $resource_id = $input['resource_id'];
                        $type = $this->getDefaultReceiptVerificationType();
                    }
                }
                $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type, 'isactive' => 1])->select($selectArray)->orderBy("id", "asc")->first();
                break;
            case 10:
                //Payroll run processing
                $type = 1;//default
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                }
                $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type])->select($selectArray)->orderBy("id", "asc")->first();
                break;
            case 12:
                //Payroll status change
                $type = 1;//default
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                }
                $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type])->select($selectArray)->orderBy("id", "asc")->first();
                break;
            case 18:
                //Employer business closure
                $type = 1;//default
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                }
                $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type, 'isactive' => 1])->select($selectArray)->orderBy("id", "asc")->first();
                break;
            case 34:
                //Payroll retiree mp update
                $type = 1;//default
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                }
                $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type, 'isactive' => 1])->select($selectArray)->orderBy("id", "asc")->first();
                break;
            default:
                if (isset($input['type']) And !is_null($input['type']) And $input['type'] > 0) {
                    $type = $input['type'];
                    $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'type' => $type, 'isactive' => 1])->select($selectArray)->orderBy("id", "asc")->first();
                } else {
                    $wf_module = $this->query()->where(['wf_module_group_id' => $module_group, 'isactive' => 1])->select($selectArray)->orderBy("id", "asc")->first();

                }
                break;
        }
        if (is_null($wf_module)) {
            throw new GeneralException(trans('exceptions.backend.workflow.module_not_found'));
        }
        return $wf_module;
    }

    public function getGroup($wf_module_id)
    {
        $wf_module = $this->query()->select(["wf_module_group_id"])->where(['id' => $wf_module_id])->first();
        return $wf_module->wf_module_group_id;
    }

    /**
     * @param array $input
     * @return mixed
     * @throws GeneralException
     */
    public function getModule(array $input)
    {
        return $this->getModuleInstance($input)->id;
    }

    public function getActiveClaimWfModule()
    {
        return 10;
    }

    public function getDefaultClaimType($resource_id)
    {
        //To be based on incident type id based on a specific notification report
        return 1;
    }

    public function getDefaultNotificationRejectionType($resource_id)
    {
        return 2;
    }

    public function getDefaultReceiptVerificationType()
    {
        return 1;
    }

    public function getActiveNotificationType($resource_id)
    {
        $return = 2;
        if (!is_null($resource_id)) {
            //To be based on incident type id based on a specific notification report
            $notificationRepo = new NotificationReportRepository();
            $notification = $notificationRepo->query()->select(['incident_type_id'])->where("id", $resource_id)->first();

            switch($notification->incident_type_id) {
                case 2:
                    $return = 2;
                    break;
                default:
                    $return = 2;
            }
        }
        return $return;
    }

    /**
     * @description Query the active workflow modules which are pending for user's action or to be assigned
     * @param int $unit_id
     * @return mixed
     */
    public function queryActiveUser()
    {
        $modules = $this->query()->select(['id', 'name', 'description', 'wf_module_group_id'])->whereIn("id", function ($query) {
            $query->select("wf_module_id")->from("wf_definitions")
                        ->whereIn("id", function($query) {
                            $query->select("wf_definition_id")->from("wf_tracks")->whereIn("status", [0,6])->whereNull('deleted_at');
                        })
                        ->whereIn("id", function($query) {
                            $query->select("wf_definition_id")->from("user_wf_definition")->whereIn("user_id", access()->allUsers());
                        });
        })->get();
        return $modules;
    }

    public function queryActiveUnit($unit_id)
    {
        $modules = $this->query()->select(['id', 'name', 'description', 'wf_module_group_id'])->whereIn("id", function ($query) use ($unit_id) {
            $query->select("wf_module_id")->from("wf_definitions")->whereIn("id", function($query) {
                $query->select("wf_definition_id")->from("wf_tracks")->where(['status' => 0])->whereNull('deleted_at');
            })->where("unit_id", $unit_id);
        })->get();
        return $modules;
    }

    public function queryActiveAssignedUser($status = 0)
    {
        $modules = $this->query()->select(['id', 'name', 'description', 'wf_module_group_id', 'notify'])->whereIn("id", function ($query) use ($status) {
            $query->select("wf_module_id")->from("wf_definitions")->whereIn("id", function($query) use ($status) {
                $query->select("wf_definition_id")->from("wf_tracks")->where(['status' => $status])->whereNull('deleted_at')->whereIn("wf_tracks.user_id", access()->allUsers());
                /*$query->select("wf_definition_id")->from("wf_tracks")->where(['status' => 0])->whereIn("wf_definition_id", function($query) {
                    $query->select("wf_definition_id")->from("user_wf_definition")->whereIn("user_id", access()->allUsers());
                });*/
            });
        })->get();
        return $modules;
    }

    /**
     * @description Query the attended workflow modules for the logged in user
     * @param int $status
     * @return mixed
     */
    public function queryAttendedUser($status = 1)
    {
        $modules = $this->query()->select(['id', 'name', 'description', 'wf_module_group_id'])->whereIn("id", function ($query) use ($status) {
            $query->select("wf_module_id")->from("wf_definitions")->whereIn("id", function($query) use ($status) {
                $query->select("wf_definition_id")->from("wf_tracks")->where(['status' => $status, 'user_type' => 'App\Models\Auth\User'])->whereNull('deleted_at')->whereIn("user_id", access()->allUsers());
            });
        })->get();
        return $modules;
    }

    /**
     * @description get the group summary of workflow modules which are pending for users's action or to be assigned
     * @param int $unit_id
     * @return array
     */
    public function getActiveUser()
    {
        $modules = $this->queryActiveUser();
        $wfTracks = new WfTrackRepository();
        $groupSummary = [];
        foreach ($modules as $module) {
            $count = $wfTracks->getPendingModuleCount($module->id);
            $groupSummary[] = ['id' => $module->id, 'name' => $module->name, 'group' => $module->wfModuleGroup->name, 'description' => $module->description, 'count' => number_format($count , 0 , '.' , ',' ), 'count_value' => $count];
        }
        return array_sort($groupSummary, 'count_value');
    }

    /**
     * @description get the group summary of workflow modules which has been attended by a logged in user.
     * @param int $status 1 - Attended | 6 - Archived
     * @return array
     */
    public function getMyAttendedActiveUser($status = 1): array
    {
        $modules = $this->queryAttendedUser($status);
        $wfTracks = new WfTrackRepository();
        $groupSummary = [];
        foreach ($modules as $module) {
            $count = $wfTracks->getMyAttendedModuleCount($module->id, $status);
            $groupSummary[] = ['id' => $module->id, 'name' => $module->name, 'group' => $module->wfModuleGroup->name, 'description' => $module->description, 'count' => number_format($count , 0 , '.' , ',' ), 'count_value' => $count];
        }
        return array_sort($groupSummary, 'count_value');
    }

    public function getMyUnitActiveUser($unit_id)
    {
        $modules = $this->queryActiveUnit($unit_id);
        $wfTracks = new WfTrackRepository();
        $groupSummary = [];
        foreach ($modules as $module) {
            $count = $wfTracks->getUnitPendingModuleCount($module->id, $unit_id);
            $groupSummary[] = ['id' => $module->id, 'name' => $module->name, 'group' => $module->wfModuleGroup->name, 'description' => $module->description, 'count' => number_format($count , 0 , '.' , ',' ), 'count_value' => $count];
        }
        return  array_sort($groupSummary, 'count_value');
    }

    /**
     * @description get the group summary of workflow modules which has been assigned to the logged in user.
     * @param int $status
     * @return array
     */
    public function getAssignedActiveUser($status = 0)
    {
        $modules = $this->queryActiveAssignedUser($status);
        $wfTracks = new WfTrackRepository();
        $groupSummary = [];
        foreach ($modules as $module) {
            $count = $wfTracks->getMyPendingModuleCount($module->id, $status);
            $notify = $module->notify;
            $notify_archive = 0;
            if ($status == 6) {
                $notify_archive = (access()->getMyReadyToUnAchiveWorkflows($module->id)) ? 1 : 0;
            }
            $groupSummary[] = ['id' => $module->id, 'name' => $module->name, 'group' => $module->wfModuleGroup->name, 'description' => $module->description, 'count' => number_format($count , 0 , '.' , ',' ), 'notify' => $notify, 'notify_archive' => $notify_archive];
        }
        return $groupSummary;
    }


    ///start: current workflow modules values
    public function notificationIncidentApproval()
    {
        //type changed from 4 --> 16, new including all DMAS Staffs
        return ['type' => 16, 'group' => 3];
    }

    public function transferToDeathIncidentApproval()
    {
        return ['type' => 19, 'group' => 3];
    }

    public function accidentMaeSpecialApprovalType()
    {
        return ['type' => 5, 'group' => 3];
    }

    public function diseaseMaeSpecialApprovalType()
    {
        return ['type' => 6, 'group' => 3];
    }

    public function maeApprovalHcpType()
    {
        return ['type' => 7, 'group' => 3];
    }

    public function maeRefundHcpType()
    {
        return ['type' => 8, 'group' => 3];
    }

    public function maeRefundMemberType()
    {
        return ['type' => 9, 'group' => 3];
    }

    public function claimBenefitType()
    {
        return ['type' => 18, 'group' => 3];
    }

    public function systemRejectedNotificationType()
    {
        return ['type' => 4, 'group' => 4];
    }

    public function unregisteredMemberNotificationType()
    {
        return ['type' => 11, 'group' => 3];
    }

    public function missingContributionNotificationType()
    {
        return ['type' => 12, 'group' => 3];
    }

    public function incorrectContributionExecutiveNotificationType()
    {
        return ['type' => 17, 'group' => 3];
    }

    public function incorrectContributionNotificationType()
    {
        return ['type' => 13, 'group' => 3];
    }

    public function incorrectEmployeeInfoNotificationType()
    {
        return ['type' => 14, 'group' => 3];
    }

    public function voidNotificationType()
    {
        return ['type' => 15, 'group' => 3];
    }

    public function undoNotificationRejectionType()
    {
        return ['type' => 6, 'group' => 4];
    }

    public function rejectionNotificationApprovalType()
    {
        return ['type' => 7, 'group' => 4];
    }

    public function receiptVerificationType()
    {
        return ['type' => 2, 'group' => 5];
    }

    public function receiptVerificationWithAdvanceType()
    {
        return ['type' => 3, 'group' => 5];
    }

    public function acknowledgementLetterIssuanceType()
    {
        return ['type' => 1, 'group' => 19];
    }

    public function reminderLetterIssuanceType()
    {
        return ['type' => 4, 'group' => 19];
    }

    public function benefitAwardLetterIssuanceType()
    {
        return ['type' => 3, 'group' => 19];
    }

    public function complianceLetterIssuanceType()
    {
        return ['type' => 2, 'group' => 19];
    }

    public function employerRegistrationType()
    {
        return ['type' => 2, 'group' => 6];
    }

    public function employerInspectionPlanType()
    {
        return ['type' => 2, 'group' => 20];
    }

    public function employerInspectionAssessmentReviewType()
    {
        return ['type' => 1, 'group' => 21];
    }

    public function employerInspectionDefaulterType()
    {
        return ['type' => 2, 'group' => 21];
    }

    public function employerInspectionInstalmentPaymentType()
    {
        return ['type' => 3, 'group' => 21];
    }

    public function employerInspectionCancellationType()
    {
        return ['type' => 4, 'group' => 21];
    }

    public function employerInspectionRefundOverPaymentType()
    {
        return ['type' => 5, 'group' => 21];
    }

    public function employerInspectionCancellationModule()
    {
        return [52];
    }

    public function employerInspectionRefundOverPaymentModule()
    {
        return [54];
    }

    public function employerInspectionInstalmentPaymentModule()
    {
        return [50];
    }

    public function employerInspectionDefaulterModule()
    {
        return [49];
    }

    public function employerInspectionAssessmentReviewModule()
    {
        return [47];
    }

    public function employerInspectionPlanModule()
    {
        return [46, 57];
    }

    public function employerRegistrationModule()
    {
        return [6, 45];
    }

    public function acknowledgementLetterIssuanceModule()
    {
        return [44];
    }

    public function reminderLetterIssuanceModule()
    {
        return [78];
    }

    public function complianceLetterIssuanceModule()
    {
        return [48];
    }

    public function benefitAwardLetterIssuanceModule()
    {
        return [53];
    }

    public function accidentMaeSpecialApprovalModule()
    {
        return [13];
    }

    public function diseaseMaeSpecialApprovalModule()
    {
        return [14];
    }

    public function notificationIncidentApprovalModule()
    {
        return [12, 34, 40];
    }

    public function transferToDeathIncidentApprovalModule()
    {
        return [77];
    }

    public function maeRefundMemberModule()
    {
        return [17];
    }


    public function employerInspectionPlanAssignInspectorLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 57:
            case 47:
                $level = 2;
                break;
        }
        return $level;
    }

    public function acknowledgementLetterIssuanceApproveLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 44:
                $level = 2;
                break;
        }
        return $level;
    }

    public function reminderLetterIssuanceApproveLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 78:
                $level = 2;
                break;
        }
        return $level;
    }

    public function acknowledgementLetterIssuanceInitiateLevel($module)
    {
        $level = 1;
        switch ($module) {
            case 44:
                $level = 1;
                break;
        }
        return $level;
    }

    public function reminderLetterIssuanceInitiateLevel($module)
    {
        $level = 1;
        switch ($module) {
            case 78:
                $level = 1;
                break;
        }
        return $level;
    }

    public function complianceLetterIssuanceApproveLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 48:
                $level = 2;
                break;
        }
        return $level;
    }

    public function complianceLetterCreateLevel($module)
    {
        $level = 1;
        switch ($module) {
            case 48:
                $level = 1;
                break;
        }
        return $level;
    }

    public function complianceLetterDispatchLevel($module)
    {
        $level = 3;
        switch ($module) {
            case 48:
                $level = 3;
                break;
        }
        return $level;
    }

    public function benefitAwardLetterIssuanceApproveLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 53:
                $level = 3;
                break;
        }
        return $level;
    }

    public function maeRefundMemberAssessmentLevel($module)
    {
        $level = 7;
        switch ($module) {
            case 17:
                $level = 7;
                break;
        }
        return $level;
    }

    public function maeRefundMemberTerminateLevel($module)
    {
        $level = 10;
        switch ($module) {
            case 17:
                $level = 10;
                break;
        }
        return $level;
    }

    public function maeRefundMemberAdministrationLevel($module)
    {
        $level = 1;
        switch ($module) {
            case 17:
                $level = 1;
                break;
        }
        return $level;
    }

    public function maeRefundMemberPaymentLevel($module)
    {
        $level = 14;
        switch ($module) {
            case 17:
                $level = 14;
                break;
        }
        return $level;
    }

    public function claimBenefitModule()
    {
        return [18, 76];
    }

    public function allClaimBenefitModule()
    {
        return array_merge($this->claimBenefitModule(), $this->maeRefundMemberModule());
    }

    public function claimBenefitAdministrationLevel($module)
    {
        $level = 1;
        switch ($module) {
            case 18:
            case 76:
                $level = 1;
                break;
        }
        return $level;
    }

    public function claimBenefitAssessmentLevel($module)
    {
        $level = 7;
        switch ($module) {
            case 18:
            case 76:
                $level = 7;
                break;
        }
        return $level;
    }

    public function receiptVerificationApproveLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 42:
                $level = 2;
                break;
            case 43:
                $level = 2;
                break;
        }
        return $level;
    }

    /**
     * @param $module
     * @return array|int[]
     */
    public function claimBenefitTerminateLevel($module)
    {
        $level = [11];
        switch ($module) {
            case 18:
                $level = [11];
                break;
            case 76:
                $level = [11, 11.4];
                break;
        }
        return $level;
    }

    /**
     * @param $module
     * @return int
     */
    public function employerRegistrationApproveLevel($module)
    {
        $level = 3;
        switch ($module) {
            case 45:
                $level = 3;
                break;
            case 6:
                $level = 2;
                break;
        }
        return $level;
    }

    /**
     * @param $module
     * @return array|int[]
     */
    public function claimBenefitPayrollTerminateLevel($module)
    {
        $level = [11];
        switch ($module) {
            case 18:
                $level = [11];
                break;
            case 76:
                $level = [11, 11.4];
                break;
        }
        return $level;
    }

    /**
     * @param $module
     * @return array|int[]
     */
    public function claimBenefitApproveLevel($module)
    {
        $level = [11];
        switch ($module) {
            case 18:
                $level = [11];
                break;
            case 76:
                $level = [11, 11.4];
                break;
        }
        return $level;
    }

    public function claimBenefitPaymentLevel($module)
    {
        $level = 14;
        switch ($module) {
            case 18:
            case 76:
                $level = 14;
                break;
        }
        return $level;
    }

    public function claimBenefitNotifyEmployerLevel($module)
    {
        $level = 15;
        switch ($module) {
            case 18:
            case 76:
                $level = 15;
                break;
        }
        return $level;
    }

    public function systemRejectedNotificationModule()
    {
        return [33];
    }

    public function unregisteredMemberNotificationModule()
    {
        return [32];
    }

    public function unregisteredMemberNotificationCheckRegisterLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 32:
                $level = 5;
                break;
        }
        return $level;
    }

    public function unregisteredMemberNotificationRegisterLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 32:
                $level = 2;
                break;
        }
        return $level;
    }

    public function missingContributionNotificationModule()
    {
        return [31];
    }

    public function missingContributionNotificationRegisterLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 31:
                $level = 2;
                break;
        }
        return $level;
    }

    public function incorrectContributionNotificationModule()
    {
        return [22,67];
    }

    public function incorrectContributionNotificationRegisterLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 22:
                $level = 2;
                break;
            case 67:
                $level = 5;
                break;
        }
        return $level;
    }

    public function incorrectEmployeeInfoNotificationModule()
    {
        return [24];
    }

    public function incorrectEmployeeInfoNotificationRegisterLevel($module)
    {
        $level = 2;
        switch ($module) {
            case 24:
                $level = 2;
                break;
        }
        return $level;
    }

    public function rejectionNotificationApprovalApprovedLevel($module)
    {
        $level = 5;
        switch ($module) {
            case 23:
            case 62:
                $level = 5;
                break;
        }
        return $level;
    }

    public function voidNotificationModule()
    {
        return [39];
    }

    public function undoNotificationRejectionModule()
    {
        return [41];
    }

    public function rejectionNotificationApprovalModule()
    {
        return [23, 62];
    }

    public function onlineNotificationRequestModule()
    {
        return [36];
    }

    public function legacyNotificationModule()
    {
        return [10];
    }

    public function legacyNotificationRejectionModule()
    {
        return [4];
    }

    public function receiptVerificationModule()
    {
        return [5, 42, 43];
    }

    public function receiptVerificationAdvanceModule()
    {
        return [43];
    }

    ///end: current workflow modules values

    /**
     * @description list of workflow module ids for unregistered member notification incident. The system will be looking for the modeule id from this function.
     * @return array
     */
    public function unregisteredMemberNotificationIds()
    {
        return [20];
    }


    /*Wf modules for Employer closure business module group*/
    public function employerBusinessClosureModules()
    {
        return [37, 38];
    }

    /*Wf modules for Employer closure business module group*/
    public function bookingInterestModules()
    {
        return [2, 68];
    }

    public function interestAdjustmentModules()
    {
        return [68];
    }
    /**
     * @param $group
     * @param $type
     * @return bool
     */
    public function checkModuleRequiringDocuments($group, $type)
    {
        $return = false;
        switch (true) {
            case ($group == 3) And (in_array($type, [$this->notificationIncidentApproval()['type'], $this->transferToDeathIncidentApproval()['type']])):
                //case ($type == $this->claimBenefitType()['type']):
                //case ($type == $this->maeRefundMemberType()['type']):
                //case ($type == $this->maeRefundHcpType()['type']):
                //case ($type = $this->maeApprovalHcpType()):
                //case ($type == $this->diseaseMaeSpecialApprovalType()['type']):
                //case ($type == $this->accidentMaeSpecialApprovalType()['type']):
                //case ($type == $this->notificationIncidentApproval()['type']):
                $return = true;
                break;
        }
        return $return;
    }

    /**
     * @param $module
     * @return int|null
     * @description Remove the specific switch case for debugging in case of error when rendering ...
     */
    public function getCR($module)
    {
        switch ($module) {
            case in_array($module, [37, 38]):
                //1. Employer Closure Workflow, modules 37,38
                $return = 31;
                break;
            case in_array($module, [55, 59]):
                //2. Employer Particular Change, modules 55, 59
                $return = 32;
                break;
            case in_array($module, [28, 63]):
                //3. Payroll Run Approval, modules 28, 63
                $return = 33;
                break;
            case in_array($module, [6, 45]):
                //4. Employers, modules 6, 45
                $return = 34;
                break;
            case in_array($module, [47, 49, 52]):
                //5. Employer Inspection Task, modules 47, 49, 52
                $return = 35;
                break;
            case in_array($module, [2]):
                //6. Booking Interest , modules 2
                $return = 36;
                break;
            case in_array($module, [3, 4, 10, 11]):
                //7. Non Progressive - Notification Reports, modules 3, 4, 10, 11
                $return = 37;
                break;
            case in_array($module, [18,76]):
                //8. Progressive - Notification Reports, modules 18 (Claim Benefits)
                $return = 38;
                break;
            case in_array($module, [12, 17, 19, 21, 22, 23, 24, 31, 33, 34, 39, 40, 41, 62, 67]):
                //9. Progressive - Notification Reports, modules (Notification Approvals, System Rejection, Rejection from Approval, Missing Contribution, Void Notification, Undo Rejection, Incorrect Contribution Notification II), modules 12, 17, 19, 21, 22, 23, 24, 31, 33, 34, 39, 40, 41, 62
                $return = 39;
                break;
            case in_array($module, [20, 32]):
                //10. Progressive - Notification Reports (Unregistered Member), modules 20, 32
                $return = 40;
                break;
            case in_array($module, [8]):
                //11. Online Employer Verification, modules 8
                $return = 41;
                break;
            case in_array($module, [48]):
                //12. Letters, modules 48 (Compliance Departments)
                $return = 42;
                break;
            case in_array($module, [44]):
                //13. Letters, modules 44 (Notifications & Claims) - Acknowledgement Letter
                $return = 43;
                break;
            case in_array($module, [78]):
                //13. Letters, modules 78 (Notifications & Claims) - Reminder Letter
                $return = 92;
                break;
            case in_array($module, [53]):
                //14. Letters, modules 53 (Notifications & Claims) - Benefit Award Letter
                $return = 44;
                break;
            case in_array($module, [5, 42, 43]):
                //15. Receipts, modules 5, 42, 43
                $return = 45;
                break;
            case in_array($module, [26,69,70]):
                //16. Payroll Status Change, modules 26
                $return = 46;
                break;
            case in_array($module, [30]):
                //17. Payroll Beneficiary Update, modules 30
                $return = 47;
                break;
            case in_array($module, [46, 57]):
                //18. Inspection, modules 46, 57
                $return = 48;
                break;
            case in_array($module, [7]):
                //19. Legacy Receipt, modules 7
                $return = 49;
                break;
            case in_array($module, [35]):
                //20. Employer Advance Payment, modules 35
                $return = 50;
                break;
            case in_array($module, [51]):
                //21. Employer Closure Reopen, modules 51
                $return = 51;
                break;
            case in_array($module, [27]):
                //22. Payroll Recovery, modules 27
                $return = 52;
                break;
            case in_array($module, [25]):
                //23. Payroll Bank Info Update, modules 25
                $return = 53;
                break;
            case in_array($module, [36]):
                //24. Portal Incident, modules 36
                $return = 54;
                break;
            case in_array($module, [60]):
                //25. Payment by Installment, modules 60
                $return = 86;
                break;
            case in_array($module, [61]):
                //26. Erroneous Contribution, modules 61
                $return = 87;
                break;
            default:
                $return = 0;
                break;
        }
        logger($return);
        return $return;
    }


    public function getReversedActiveUser()
    {
        $modules = $this->queryReversedActiveUser();
        $wfTracks = new WfTrackRepository();
        $groupSummary = [];
        foreach ($modules as $module) {
            $count = $wfTracks->getReversedModuleCount($module->id);
            $groupSummary[] = ['id' => $module->id, 'name' => $module->name, 'group' => $module->wfModuleGroup->name, 'description' => $module->description, 'count' => number_format($count , 0 , '.' , ',' )];
        }
        return $groupSummary;
    }


    public function queryReversedActiveUser()
    {
        $modules = $this->query()->select(['id', 'name', 'description', 'wf_module_group_id'])->whereIn("id", function ($query) {
            $query->select("wf_definitions.wf_module_id")->from("wf_definitions")->whereIn("id", function($query) {
                $query->select("wf_tracks.wf_definition_id")->from("wf_tracks")
                ->join("wf_tracks as parent", "parent.id", "=", "wf_tracks.parent_id")
                ->where('wf_tracks.status',0)->where('parent.status',2)->whereIn("wf_tracks.wf_definition_id", function($query) {
                    $query->select("user_wf_definition.wf_definition_id")->from("user_wf_definition")->whereIn("user_id", access()->allUsers());
                });
            });
        })->get();
        return $modules;
    }


    public function getActiveSubordinates($user_id)
    {
        $modules = $this->queryActiveSubordinate($user_id);
        $wfTracks = new WfTrackRepository();
        $groupSummary = [];
        foreach ($modules as $module) {
            $count = $wfTracks->getSubornitesModuleCount($user_id,$module->id);
            if ($count) {
               $groupSummary[] = ['id' => $module->id, 'name' => $module->name, 'group' => $module->wfModuleGroup->name, 'description' => $module->description, 'count' => number_format($count , 0 , '.' , ',' )];
           }
       }
       return $groupSummary;
   }


   public function queryActiveSubordinate($user_id)
   {
    $modules = $this->query()->select(['id', 'name', 'description', 'wf_module_group_id'])->whereIn("id", function ($query) use($user_id){
        $query->select("wf_definitions.wf_module_id")->from("wf_definitions")->whereIn("id", function($query) use($user_id){
            $query->select("wf_tracks.wf_definition_id")->from("wf_tracks")
            ->where('wf_tracks.status',0)->whereIn("wf_tracks.wf_definition_id", function($query) use($user_id){
                $query->select("user_wf_definition.wf_definition_id")->from("user_wf_definition")->where("user_id", $user_id);
            });
        });
    })->get();
    return $modules;
}

}