<?php

namespace App\Repositories\Backend\Workflow;

use App\Models\Workflow\WfModuleGroup;
use App\Repositories\BaseRepository;
//use App\Models\Workflow\WfGroup;

class WfGroupRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = WfModuleGroup::class;

}