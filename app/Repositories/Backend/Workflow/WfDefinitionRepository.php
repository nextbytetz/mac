<?php

namespace App\Repositories\Backend\Workflow;

use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Models\Workflow\WfDefinition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
//use App\Repositories\Backend\Workflow\WfTrackRepository;
use Illuminate\Support\Facades\Log;

/**
 * Class WfDefinitionRepository
 * @package App\Repositories\Backend\Workflow
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class WfDefinitionRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = WfDefinition::class;

    /**
     * @param $wf_definition_id
     * @return mixed
     */
    public function getCurrentLevel($wf_definition_id)
    {
        $wf_definition = $this->find($wf_definition_id); // changed
        return $wf_definition;
    }

    /**
     * @param $wf_definition_id
     * @return mixed
     */
    public function getPreviousLevels($wf_definition_id, $resource_id = NULL)
    {
        /*
 Previous Levels Algorithm
+--------------+-----------+--------------+--------------+------------------------------------------------------------+
|Criteria 1                                                                               |
+--------------+--------------+--------------+--------------+---------------------------------------------------------+
/ 1. If the previous level is the same unit group,
    a. If the current designation level is between 3 to 1 inclusive reverse to any one withing the same unit
    b. If the current designation level is greater than 3, reverse to only one step
/ 2. If the previous level is another unit, reverse to one higher designation level
+--------------+-----------+--------------+--------------+------------------------------------------------------------+
 */
        //$user = access()->user();
        $wf_definition = $this->getCurrentLevel($wf_definition_id);
        $processedWfDefinitionIds = (new WfTrackRepository())->query()->where(['resource_id' => $resource_id])->select(['wf_definition_id'])->pluck('wf_definition_id')->all();
        //logger($processedWfDefinitionIds);
        $levelsQuery = $this->query()
            ->select([
                'wf_definitions.id',
                'wf_definitions.unit_id',
                'wf_definitions.designation_id',
                'wf_definitions.level',
                DB::raw("'Level ' || wf_definitions.level || ' ( ' || designations.name || ' - ' || units.name || ' )'  as name")
            ])
            ->join("units", "units.id", "=", "wf_definitions.unit_id")
            ->join("designations", "designations.id", "=", "wf_definitions.designation_id")
            ->where(['wf_module_id' => $wf_definition->wf_module_id])
            ->where("wf_definitions.level", "<", $wf_definition->level)
            ->whereIn("wf_definitions.id", $processedWfDefinitionIds)
            //->where("is_optional", 0)
            ->where("allow_rejection", 1)
            //->orderByDesc("wf_definitions.level")
        ;
        //logger($wf_definition->wf_module_id);
        //logger($wf_definition->level);
        //logger($processedWfDefinitionIds);
        //logger($levelsQuery->toSql());
        if ($levelsQuery->count()) {
            //logger($levelsQuery->count());
            //$prevDefinition  = $this->getNextWfDefinition($wf_definition->wf_module_id, $wf_definition->id, -1);

            $prevDefinition  = with(clone $levelsQuery)->orderByDesc("wf_definitions.level")->first();
            //logger($levelsQuery->toSql());
            $prev_unit_id = $prevDefinition->unit->id;
            $top_relatives = collect(DB::select("with recursive results as( select ct.id, ct.parent_id, array[ct.parent_id] as path from units ct where ct.id = {$prev_unit_id} union all select n.id, n.parent_id, n.parent_id || tp.path from units n inner join results tp on tp.parent_id = n.id) select rs.id::bigint parent, path parent_path from results rs;"))->pluck("parent")->all();
            $down_relatives = collect(DB::select("with recursive results as( select ct.id, array[ct.id] as path from units ct where ct.id = {$prev_unit_id} union all select n.id, n.id || tp.path from units n inner join results tp on tp.id = n.parent_id) select rs.id::bigint child, rs.path parent_path from results rs;"))->pluck("child")->all();
            $all_relatives = array_merge($top_relatives, $down_relatives);

            //logger($all_relatives);
            $designation = $wf_definition->designation;
            if (in_array($wf_definition->unit_id, $all_relatives)) {
                //Same Unit ...

                //logger($designation->level);
                if (in_array($designation->level, [1,2,3,5])) {
                    //logger("I have passed here ...");
                    //if current level is executive ... reverse to any one withing the same unit
                    /*
                    1	Director General
                    2	Head
                    3	Director
                    5	Manager
                     */
                    //logger($all_relatives);

                    $levelsQuery->whereIn("unit_id", $all_relatives)->where("designations.id", "<>", $designation->id)->orderByDesc("wf_definitions.level");
                    //logger($all_relatives);
                    //logger($designation->id);
                } else {
                    //the current level is not executive ... reverse to only one step
                    $levelsQuery->orderByDesc("wf_definitions.level")->limit(1);
                }
            } else {
                //Different Unit ...
                if (in_array($designation->level, [1,2,3,5])) {
                    // Excecutives
                    //TODO: to be reviewed ...
                    $levelsQuery->where("designations.id", "<>", $designation->id)->orderByDesc("wf_definitions.level");
                } else {
                    $levelsQuery->orderByDesc("wf_definitions.level")->orderByDesc("designations.level")->limit(1);
                }
            }
        }
        $levels = $levelsQuery
                        ->get()
                        ->pluck('name', 'level')
                        ->all();
        return $levels;
    }

    /**
     * @param $wf_definition_id
     * @return mixed
     */
    public function getSelectiveLevels($wf_definition_id)
    {
        $wf_definition = $this->getCurrentLevel($wf_definition_id);
        /*$levels = $this->query()->select(['wf_definitions.designation_id', DB::raw("'Level ' || wf_definitions.level || ' ( ' || designations.name || ' - ' || units.name || ' )'  as name")])->join("units", "units.id", "=", "wf_definitions.unit_id")->join("designations", "designations.id", "=", "wf_definitions.designation_id")->where(['wf_module_id' => $wf_definition->wf_module_id])->where("wf_definitions.level", ">", $wf_definition->level)->where("selective", 1)->where(function ($query) use ($wf_definition) {
            $query->where("unit_id", "<>", $wf_definition->unit_id)->where("designation_id", "<>", $wf_definition->designation_id);
        })->distinct()->toSql();*/
            //->pluck('name', 'id')->all();
        $levels = collect(DB::select("select wf_definitions.id, designations.name || ' - ' || units.name  as name from wf_definitions inner join units on units.id = wf_definitions.unit_id inner join designations on designations.id = wf_definitions.designation_id where (wf_module_id = {$wf_definition->wf_module_id}) and wf_definitions.level > {$wf_definition->level} and wf_definitions.level < ({$wf_definition->level} + 1) and selective = 1 and (unit_id, designation_id) not in (select {$wf_definition->unit_id}, {$wf_definition->designation_id})"))->pluck('name', 'id')->all();
        return $levels;
    }

    public function getUsersToSelect($wf_definition_id)
    {
        $wf_definition = $this->getCurrentLevel($wf_definition_id);
        $users = collect(DB::select("select id, concat_ws(' ', firstname, lastname) as name from users where unit_id = {$wf_definition->unit_id}"))->pluck('name', 'id')->all();
        return $users;
    }

    /**
     * @param int $sign | -1 or 1
     * @param $wf_definition_id
     * @param bool $skip | Skip optional levels
     * @return null
     */
    public function getLevel($sign = 1, $wf_definition_id, $skip = false)
    {
        $wf_definition = $this->getCurrentLevel($wf_definition_id);
        if ($sign == 1) {
            if ($skip) {
                //skip optional level
                $nextLevel = $this->query()->where(['wf_module_id' => $wf_definition->wf_module_id, 'is_optional' => 0])->where("level", ">", $wf_definition->level)->orderBy("id", "asc")->limit(1)->first();
            } else {
                //proceed regularly
                //dd($wf_definition->level);
                $nextLevel = $this->query()->where(['wf_module_id' => $wf_definition->wf_module_id])->where("level", ">", $wf_definition->level)->orderBy("id", "asc")->limit(1)->first();
            }
        } else {
            if ($skip) {
                //skip optional level
                $nextLevel = $this->query()->where(['wf_module_id' => $wf_definition->wf_module_id, 'is_optional' => 0])->where("level", "<", $wf_definition->level)->orderBy("id", "desc")->limit(1)->first();
            } else {
                //proceed regularly
                $nextLevel = $this->query()->where(['wf_module_id' => $wf_definition->wf_module_id])->where("level", "<", $wf_definition->level)->orderBy("id", "desc")->limit(1)->first();
            }
        }

        if (empty($nextLevel)){
            $return = null;
        } else {
            $return = $nextLevel->level;
        }
        return $return;
    }

    public function getNextLevel($wf_definition_id, $skip = false)
    {
        return $this->getLevel(1, $wf_definition_id, $skip);
    }

    public function getPrevLevel($wf_definition_id)
    {
        return $this->getLevel(-1, $wf_definition_id);
    }

    public function getLastLevel($module_id)
    {
        $maxLevel = $this->query()->where('wf_module_id', $module_id)->max('level');
        return $maxLevel;
    }

    public function updateDefinitionUsers(Model $definition, array $input)
    {
        $users = $input['users'];

        DB::transaction(function () use ($definition, $users, $input) {
            $definition->users()->sync([]);
            $users = [];
            if (is_array($input['users']) And count($input['users'])) {
                foreach ($input['users'] as $user) {
                    array_push($users, $user);
                }
            }
            $definition->attachUsers($users);
            /*
             * Put audits and logs here for updating a users in the workflow definition
             */
            return true;
        });
    }

    public function hasUsers($wf_definition_id)
    {
        if ($this->find($wf_definition_id)->users()->count()){
            return true;
        } else {
            return false;
        }
    }

    public function getDefinition($module_id, $resource_id)
    {
        $wf_track = new WfTrackRepository();
        $track = $wf_track->getRecentResourceTrack($module_id, $resource_id);

        if (empty($track)) {
            $definition = $this->query()->where(['wf_module_id' => $module_id, 'level' => 1])->first();
            $wf_definition_id = $definition->id;
        } else {
            $wf_definition_id = $track->wf_definition_id;
        }
        //dd($track);
        return $wf_definition_id;
    }

    /**
     * @param $module_id
     * @param $wf_definition_id
     * @param $sign
     * @param bool $skip
     * @return mixed
     */
    public function getNextDefinition($module_id, $wf_definition_id, $sign, $skip = false)
    {
        $nextLevel = $this->getLevel($sign, $wf_definition_id, $skip);
        $definition = $this->query()->where(['wf_module_id' => $module_id, 'level' => $nextLevel])->first();
        return $definition->id;
    }

    /**
     * @param $module_id
     * @param $level
     * @return null
     */
    public function getLevelDefinition($module_id, $level)
    {
        $definition = $this->query()->where(['wf_module_id' => $module_id, 'level' => $level])->first();
        if ($definition) {
            $return = $definition->id;
        } else {
            $return = NULL;
        }
        return $return;
    }

    public function getNextWfDefinition($module_id, $wf_definition_id, $sign, $skip = false)
    {
        $nextLevel = $this->getLevel($sign, $wf_definition_id, $skip);
        //Log::error($nextLevel);
        //Log::info($module_id);
        $definition = $this->query()->where(['wf_module_id' => $module_id, 'level' => $nextLevel])->first();
        //Log::info($definition->toArray());
        return $definition;
    }

    public function userHasAccess($id, $level, $module_id)
    {
        $inArray = array_merge(access()->allUsers(), access()->alternateUsers());
        $users = $this->query()->where(['level' => $level, 'wf_module_id' => $module_id])->first()->users()->whereIn('users.id', $inArray)->first();
        if (!$users) {
            $return = false;
        } else {
            $return = true;
        }

        return $return;
    }

    /**
     * @param $from_module
     * @param $to_module
     * @param array $level_map
     * @return bool
     */
    public function transferUsers($from_module, $to_module, array $level_map)
    {
        foreach ($level_map as $level) {
            $fromdefinition = $this->query()->where(['level' => $level, 'wf_module_id' => $from_module])->first();
            $todefinition = $this->query()->where(['level' => $level, 'wf_module_id' => $to_module])->first();
            if ($fromdefinition && $todefinition) {
                $users = $fromdefinition->users()->pluck("users.id")->all();
                $todefinition->users()->syncWithoutDetaching($users);
            }
        }
        return true;
    }

}