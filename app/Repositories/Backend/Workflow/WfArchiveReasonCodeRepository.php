<?php


namespace App\Repositories\Backend\Workflow;

use App\Models\Workflow\WfArchiveReasonCode;
use App\Repositories\BaseRepository;

/**
 * Class WfArchiveReasonCodeRepository
 * @package App\Repositories\Backend\Workflow
 */
class WfArchiveReasonCodeRepository extends BaseRepository
{
    const MODEL = WfArchiveReasonCode::class;

}