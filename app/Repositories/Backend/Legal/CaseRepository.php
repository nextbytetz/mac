<?php

namespace App\Repositories\Backend\Legal;

use App\Repositories\BaseRepository;
use App\Models\Legal\Cases;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CaseRepository extends  BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Cases::class;

    public $mentions;

    public function __construct()
    {
        parent::__construct();
        $mentions = new CaseMentionRepository();
        $this->mentions = $mentions;
    }

    public function store(array $input)
    {
        if (isset($input['fee'])) {
            $input['filling_date'] = fix_form_date($input['filling_date']);
            $input['fee'] = str_replace(",", "", $input['fee']);
        }
        $case = $this->query()->create($input);
        return $case->id;
    }

    public function update(Model $case, array $input)
    {
        if ($input['liability_type'] == 2) {
            $input['contentious_type'] = NULL;
            $input['liability'] = NULL;
        }
        if (isset($input['fee'])) {
            $input['filling_date'] = fix_form_date($input['filling_date']);
            $input['fee'] = str_replace(",", "", $input['fee']);
        }
        $case->update($input);
    }

    public function destroy(Model $case)
    {
        $case->delete();
    }

    public function close(Model $case)
    {
        $case->status = 1;
        $case->user_closed = access()->id();
        $case->save();
    }

    public function getForDataTable() {
        return $this->getAll();
    }

    /* start: Reports based on time Period, providing details on a monthly bases */

    public function getTotalMonthlyCases()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $sum = $this->query()
                               ->whereMonth("created_at", "=",  $month)
                ->whereYear("created_at", "=", $year)
                ->count();
            $period = $start->format("M y");
            $data[] = [$period, (int) $sum]; //number_format( $sum , 2 , '.' , ',' )
            $start->addMonth();
        }
        $return['graph'] = $data;
        return $return;
    }

    public function getTotalMonthlyClosedCases()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $count = $this->query()
                ->where('status',1)
                ->whereMonth("created_at", "=",  $month)
                ->whereYear("created_at", "=", $year)
                ->count();
            $period = $start->format("M y");
            $data = array_merge($data, [(int) $count]); //$period . " : " . $sum
            $start->addMonth();
        }
        $count = $this->query()->whereRaw("created_at between :start_date and :end_date and status = 1", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])
            ->count();
        $return['closed'] = implode("," , $data);
        $return['closed_total'] =  $count ;
        return $return;
    }

    public function getTotalMonthlyCaseMentions()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $count = $this->mentions->query()
                          ->whereMonth("created_at", "=",  $month)
                ->whereYear("created_at", "=", $year)
                ->count();
            $period = $start->format("M y");
            $data = array_merge($data, [(int) $count]); //$period . " : " . $sum
            $start->addMonth();
        }
        $count = $this->mentions->query()->whereRaw("created_at between :start_date and :end_date", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])
            ->count();
        $return['mentions'] = implode("," , $data);
        $return['mentions_total'] =  $count;
        return $return;
    }

    /* end: Reports based on time Period, providing details on a monthly bases */

}