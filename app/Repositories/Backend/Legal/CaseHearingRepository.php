<?php

namespace App\Repositories\Backend\Legal;

use App\Models\Legal\Cases;
use App\Repositories\BaseRepository;
use App\Models\Legal\CaseHearing;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CaseHearingRepository extends  BaseRepository
{

    const MODEL = CaseHearing::class;

    public function store(array $input)
    {
        if (isset($input['fee'])) {
            $input['filling_date'] = fix_form_date($input['filling_date']);
            $input['fee'] = str_replace(",", "", $input['fee']);
        }
        $input['hearing_date'] = fix_form_date($input['hearing_date']);
        $input['hearing_time'] = Carbon::parse($input['hearing_time'])->format('H:i');
        $hearing = $this->query()->create($input);
        return $hearing;
    }

    public function getForDataTable($id) {

        return $this->query()->whereHas('cases', function($query) use($id){
            $query->where('id',$id);
        });
    }

    public function getNextForDataTable() {
        return $this->query()->where("status", 0)->orderBy('hearing_date', "asc")->orderBy('hearing_time', "asc");
    }

    public function getLawyerHearingForDataTable($id) {
        return $this->query()->where('lawyer_id', $id);
    }

    public function update(Model $hearing, array $input) {
        if (isset($input['fee'])) {
            $input['filling_date'] = fix_form_date($input['filling_date']);
            $input['fee'] = str_replace(",", "", $input['fee']);
        }
        $input['hearing_date'] = fix_form_date($input['hearing_date']);
        $input['hearing_time'] = Carbon::parse($input['hearing_time'])->format('H:i');
        $hearing->update($input);
    }

    public function destroy(Model $hearing)
    {
        $hearing->delete();
    }


}