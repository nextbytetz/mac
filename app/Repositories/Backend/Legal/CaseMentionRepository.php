<?php

namespace App\Repositories\Backend\Legal;

use App\Models\Legal\CaseMention;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CaseMentionRepository extends BaseRepository
{

    const MODEL = CaseMention::class;

    public function store(array $input)
    {
        $input['mention_date'] = fix_form_date($input['mention_date']);
        $input['mention_time'] = Carbon::parse($input['mention_time'])->format('H:i');
        $mention = $this->query()->create($input);
        return $mention;
    }

    public function getForDataTable($id) {
        return $this->query()->whereHas('cases', function($query) use ($id) {
            $query->where('id', $id);
        });
    }

    public function update(Model $mention, array $input) {
        $input['mention_date'] = fix_form_date($input['mention_date']);
        $input['mention_time'] = Carbon::parse($input['mention_time'])->format('H:i');
        $mention->update($input);
    }

    public function destroy(Model $mention)
    {
        $mention->delete();
    }

    public function getLawyerHearingForDataTable($id) {
        return $this->query()->where('lawyer_id', $id);
    }

}