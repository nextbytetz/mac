<?php

namespace App\Repositories\Backend\Legal;

use App\Repositories\BaseRepository;
use App\Models\Legal\Lawyer;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\DB;

class LawyerRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = Lawyer::class;

    public function getFirmsForInput()
    {
        $return = $this->query()->select([DB::raw('employers.name as employer'), 'lawyers.id'])->whereNotNull("lawyers.employer_id")->join('employers', 'employers.id', '=', 'lawyers.employer_id')->get()->pluck("employer", "id")->all();
        return $return;
    }

    public function store($input)
    {
        $lawyer = $this->query()->create($input);
        return $lawyer;
    }

    public function getForDataTable() {
        return $this->getAll();
    }

    public function update(Model $lawyer, $input)
    {
        if ($input['category'] == 1) {

        }
        switch ($input['category']) {
            case 1:
                $input['firstname'] = NULL;
                $input['middlename'] = NULL;
                $input['lastname'] = NULL;
                break;
            case 0:
                $input['employer_id'] = NULL;
                break;
        }
        $lawyer->update($input);
    }

    public function destroy(Model $lawyer)
    {
        $hearings = $lawyer->caseHearings()->count();
        if ($hearings > 0) {
            throw new GeneralException(trans('exceptions.backend.legal.has_hearings'));
        }
        $mentions = $lawyer->caseMentionings()->count();
        if ($mentions > 0) {
            throw new GeneralException(trans('exceptions.backend.legal.has_mentions'));
        }
        $lawyer->delete();
    }

}