<?php

namespace App\Repositories\Backend\Legal;

use App\Models\Legal\CasePersonnel;
use App\Repositories\BaseRepository;

class CasePersonnelRepository extends BaseRepository
{
    const MODEL = CasePersonnel::class;

}