<?php

namespace App\Repositories\Backend\Reporting;


use App\Models\Reporting\SlaAlert;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SlaAlertRepository
 * @package App\Repositories\Backend\Reporting
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 * @description Showing reports complying to Service Level Agreements
 */
class SlaAlertRepository extends BaseRepository
{
    const MODEL = SlaAlert::class;

    /**
     * @return mixed
     */
    public function getMyList()
    {
        $user = access()->user();
        $designation_id = $user->designation_id;
        $unit_id = $user->unit_id;
        $slas = $this->query()->where([
            'designation_id' => $designation_id,
            'unit_id' => $unit_id,
        ])->where("isactive", 1)->get();
        return $slas;
    }

    /**
     * @param Model $sla
     * @return int
     */
    public function getSummaryCount(Model $sla)
    {
        $crRepo = new ConfigurableReportRepository();
        //$slas = $this->getMyList();
        //foreach ($slas as $sla) {
        //$function = "queryArray{$reference}";
        //$queryArray = $this->$function($aging);
        $query_filter = $this->getQueryFilter($sla);
        $cr = $crRepo->find($sla->configurable_report_id);
        $input = [];
        $input['query_filters'] = json_encode($query_filter);
        $whereQuery = $crRepo->getWhereQuery($cr, $input);
        $model = $crRepo->getModalInstance($cr)->whereRaw($whereQuery);
        //logger($model->toSql());
        //logger($whereQuery);
        //logger($input);
        $count = $model->count();
        return $count;
        //}
    }

    /**
     * @param Model $sla
     * @return \array[][]|\array[][][]
     */
    public function getQueryFilter(Model $sla)
    {
        $wfmoduleRepo = new WfModuleRepository();
        $reference = $sla->reference;
        $parent = $this->query()->where("parent_id", $sla->id)->first();
        $days = [$sla->days_accumulative];
        if ($parent) {
            $days[] = $parent->days_accumulative;
        }
        if (count($days) > 1) {
            $aging = ['between', $days];
        } else {
            $aging = ['>=', $days[0]];
        }
        switch ($reference) {
            case 'SLAARIFNASA':
                //Initiate Rejected Incident From Notification Approval Delays ...
                $query_filter = [
                    [
                        'and' => ['reference' => "NSRINC", 'aging' => $aging],
                    ],
                ];
                break;
            case 'SLAAINRJOTINDLY':
                //Initiate Rejected Out of Time Incident Delays and Before Statutory Period ...
                $query_filter = [
                    [
                        'and' => ['reference' => ["in", ["NSNREJSYBS", "NSNREJSYOT"]], 'aging' => $aging],
                    ],
                ];
                break;
            case 'SLAAPTCDFNVDTN':
                //PCADO to CADM Delays for Notification Validation
                $modules = $wfmoduleRepo->notificationIncidentApprovalModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'level' => 2, 'status' => 0],
                    ],
                ];
                break;
            case 'SLAACDMDANVDTN':
                //CADM Delays to Approve Notification Validation
                $modules = $wfmoduleRepo->notificationIncidentApprovalModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'level' => 3, 'status' => 0],
                    ],
                ];
                break;
            case 'SLAAHLSUDANV':
                //HLSU Delays to Approve Notification Validation
                $modules = $wfmoduleRepo->notificationIncidentApprovalModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'unit_id' => 6, 'status' => 0],
                    ],
                ];
                break;
            case 'SLAADASDANVDTN':
                //DAS Delays to Approve Notification Validation
                $modules = $wfmoduleRepo->notificationIncidentApprovalModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'unit_id' => 17, 'status' => 0],
                    ],
                ];
                break;
            case 'SLAAPTCDFBNFWRKLW':
                //PCADO to CADM Delays for Benefit Workflow
                $modules = $wfmoduleRepo->claimBenefitModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'level' => 2, 'status' => 0],
                    ],
                ];
                break;
            case 'SLACDODLTCBNAWRDLTR':
                //CADO Delays to Create Benefit Award Letters
                $modules = $wfmoduleRepo->claimBenefitModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'level' => 15, 'status' => 0, 'hasletter' => 0, 'benefit_type_id' => ["in", [1,3,4,5]]],
                    ],
                ];
                break;
            case 'SLAACDMDABNFWRKLW':
                //CADM Delays to Approve Benefit Workflow
                $modules = $wfmoduleRepo->claimBenefitModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'level' => 3, 'status' => 0],
                    ],
                ];
                break;
            case 'SLAADASDABNFWRWPD':
                //DAS Delays to Approve Benefit Workflow (PD)
                $modules = $wfmoduleRepo->claimBenefitModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'unit_id' => 17, 'status' => 0, 'benefit_type_id' => 5],
                    ],
                ];
                break;
            case 'SLAADASDABNFWRTDMSR':
                //DAS Delays to Approve Benefit Workflow (TD, MAE, Survivors)
                $modules = $wfmoduleRepo->claimBenefitModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'unit_id' => 17, 'status' => 0, 'benefit_type_id' => ["in", [1, 3, 4]]],
                    ],
                ];
                break;
            case 'SLAADFPIDABNFWR':
                //DFPI Delays to Approve Benefit Workflow
                $modules = $wfmoduleRepo->claimBenefitModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'unit_id' => 12, 'status' => 0],
                    ],
                ];
                break;
            case 'SLAAHLSUDABNFWR':
                //HLSU Delays to Approve Benefit Workflow
                $modules = $wfmoduleRepo->claimBenefitModule();
                $query_filter = [
                    [
                        'and' => ['wf_module_id' => ["in", $modules], 'aging' => $aging, 'unit_id' => 6, 'status' => 0],
                    ],
                ];
                break;
            case 'SLAADLYRONNTAPLCTN':
                //Delays to Review New Online Notification Application
                $query_filter = [
                    [
                        'and' => ['reference' => 'ISUPSUPDOC', 'aging' => $aging],
                    ],
                ];
                break;
            case 'SLAADLYAPVNTAPLCTN':
                //Delays to Approve Online Notification Application
                $query_filter = [
                    [
                        'and' => ['aging' => $aging, 'level' => 3, 'status' => 0],
                    ],
                ];
                break;
            case 'SLAARODLYDNTNLTR':
                //RO Delays to Dispatch Benefit Letters
                $query_filter = [
                    [
                        'and' => ['aging' => $aging, 'level' => 4, 'status' => 0],
                    ],
                ];
                break;
            default:
                $query_filter = [
                    [
                        'and' => ['aging' => $aging],
                    ],
                ];
                break;
        }
        return $query_filter;
    }

    /**
     * @return int
     */
    public function checkCount()
    {
        $return = 0;
        $slas = $this->getMyList();
        foreach ($slas as $sla) {
            $count = $this->getSummaryCount($sla);
            if ($count) {
                $return = 1;
                break;
            }
        }
        return $return;
    }

}