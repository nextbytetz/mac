<?php

namespace App\Repositories\Backend\Reporting;

use App\Models\Reporting\ConfigurableReport;
use App\Models\Reporting\Cr;
use App\Repositories\Backend\Finance\FinYearRepository;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class ConfigurableReportRepository
 * @package App\Repositories\Backend\Reporting
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 * @description Do not add dependency on configurable reports views (i.e one configurable view to depend on another configurable view)...
 */
class ConfigurableReportRepository extends BaseRepository
{
    const MODEL = ConfigurableReport::class;

    /**
     * @param $id
     * @return int
     */
    public function getQueryCount($id)
    {
        $count = 0;
        $instance = $this->query()->select(["id", "configurable_report_type_id"])->where("id", $id);
        if ($instance->count()) {
            $query = $instance->first();
            $report_query = file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/t{$query->configurable_report_type_id}/cr{$query->id}.sql");
            $results = DB::select("select count(*) count from ({$report_query}) a");
            $count = $results[0]->count;
        }
        return $count;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getHeaders($name)
    {
        $results = DB::select("SELECT a.attname, pg_catalog.format_type(a.atttypid, a.atttypmod), a.attnotnull FROM pg_attribute a JOIN pg_class t on a.attrelid = t.oid JOIN pg_namespace s on t.relnamespace = s.oid WHERE a.attnum > 0 AND NOT a.attisdropped AND t.relname = '{$name}' AND s.nspname = 'main' ORDER BY a.attnum;");
        //logger($results);
        return collect($results)->pluck('attname')->all();
    }

    /**
     * @param $name
     * @param array $extras
     * @return array
     */
    public function getHeadersExCol($name, array $extras = [])
    {
        $return = [];
        $headers = $this->getHeaders($name);

        //manipulate columns
        foreach ($extras as $exclude) {
            if (isset($headers[$exclude])) {
                unset($headers[$exclude]);
            }
        }

        $excludes = ['user_type', 'resource_type'];

        foreach ($headers as $header) {
            if (in_array($header, $excludes)) {
                continue;
            }
            if (strpos($header, '_id') !== false && $header != 'resource_id') {
                continue;
            }
            $return[] = $header;
        }
        return $return;
    }

    /**
     * @param Model $cr
     * @return array
     */
    public function buildHeadersForDatatable(Model $cr)
    {
        $excludes = (json_decode($cr->excludes, true)) ?? [];

        $headers = $this->getHeadersExCol($cr->view, $excludes);
        if (!count($headers)) {
            //create the report ....
            $this->createViews($cr->id);
            $headers = $this->getHeadersExCol($cr->view, $excludes);
        }
        $columns = $headers;

        $display = (json_decode($cr->display, true)) ?? [];
        $aliases = (json_decode($cr->aliases, true)) ?? [];
        //logger($aliases);
        $dtcolumns = [];
        //logger($columns);
        foreach ($columns as $index => $value) {
            $colname = camelToWord($value);
            if (count($aliases) && array_key_exists($index, $aliases)) {
                $colname = $aliases[$index];
            }
            //logger($display);

            if ((count($display) && in_array($index, $display)) || !count($display)) {
                $dtcolumns[] = ['data' => $value, 'name' => $value, 'colname' => $colname];
            } else {
                //hidden column
                $dtcolumns[] = ['data' => $value, 'name' => $value, 'visible' => false, 'colname' => $colname, 'className' => 'never'];
            }
        }
        //logger($dtcolumns);
        return $dtcolumns;
    }

    /**
     * @param Model $cr
     * @param array $input
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getForDatatable(Model $cr, array $input = [])
    {
        return $this->getModalInstance($cr, $input);
    }

    /**
     * @param Model $cr
     * @param array $input
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getModalInstance(Model $cr, array $input = [])
    {
        $crModel = new Cr();
        $crModel->set(['table' => $cr->view]);
        $filtersql = NULL;
        if (count($input)) {
            $filtersql = $this->getWhereQuery($cr, $input);
        }
        $return = $crModel->newQuery();
        //logger($filtersql);
        if ($filtersql) {
            $return->whereRaw($filtersql);
        }
        return $return;
    }

    /**
     * @param $type
     * @param $column
     * @param array $input
     * @return string
     */
    public function joinWhereQuery($type, $column, array $input)
    {
        $query = '';
        switch ($type) {
            case 'query_filter':
                //used for query filter
                $value = $input[$column];
                if (is_array($value) && count($value)) {
                    $logic = $value[0];
                    $comparator = $value[1];
                    $cast = $value[2] ?? '';
                    switch($logic) {
                        case 'between':
                            $start = $comparator[0];
                            $end = $comparator[1];
                            if ($cast) {
                                $query .= " {$column} between {$start}::{$cast} and {$end}::{$cast} ";
                            } else {
                                $query .= " {$column} between {$start} and {$end} ";
                            }
                            break;
                        case 'in':
                            foreach ($comparator as $index => $item) {
                                if (is_string($item)) {
                                    $comparator[$index] = "'{$item}'";
                                }
                            }
                            $implode = implode(',', $comparator);
                            $query .= " {$column} in ({$implode}) ";
                            break;
                        default:
                            $query .= " {$column} {$logic} '{$comparator}' ";
                            break;
                    }
                } else {
                    $query .= " {$column} = '{$value}' ";
                }
                break;
            case 'select':
            case 'selectlazy':
            case 'int':
                //used for form input control
                $value = $input[$column];
                $query .= " {$column} = '{$value}' ";
                break;
            case 'boolean':
                //used for form input control
                //logger('I am in');
                $value = $input[$column];
                $query .= " {$column} = '{$value}'::bool ";
                break;
            case 'text':
                //used for form input control
                $value = $input[$column];
                $query .= " {$column} ~* '{$value}' ";
                break;
            case 'daterange':
            case 'intrange':
                //used for form input control
                $value_from = $input[$column . '_from'];
                $value_to = $input[$column . '_to'];
                if ($type == 'daterange') {
                    $query .= " {$column}::date between '{$value_from}'::date and '{$value_to}'::date ";
                } else {
                    $query .= " {$column} between '{$value_from}' and '{$value_to}' ";
                }
                break;
        }
        return $query;
    }

    /**
     * @param Model $cr
     * @param array $input
     * @return string
     */
    public function getWhereQuery(Model $cr, array $input)
    {
        //logger($input['status'] ?? 'No status');
        //logger('I am in');
        $filtersql = '';
        $colArrs = json_decode($cr->filter, true) ?? []; //filter optional search parameters
        //$filterArrs = json_decode($cr->query_filter, true) ?? []; //filter mandatory parameters
        //logger($filterArrs);

        //$ors = json_decode($input['query_filter_or'] ?? '[]', true);
        //$ands = json_decode($input['query_filter_and'] ?? '[]', true);
        //$betweens = json_decode($input['query_filter_between'] ?? '[]', true);

        $query_filters = json_decode($input['query_filters'] ?? '[]', true);
        foreach ($query_filters as $query_filter) {
            if (isset($query_filter['or']) && count($query_filter['or'])) {
                $innerfiltersql = '';
                foreach ($query_filter['or'] as $index => $value) {
                    $column = $index;
                    if ($innerfiltersql) {
                        $innerfiltersql .= ' or ';
                    }
                    //$value = is_array($value) ? $value[1] : $value;
                    //$type = is_string($comparator) ? 'varchar' : 'int'; //type of input control ...
                    $innerfiltersql .= $this->joinWhereQuery('query_filter', $column, $query_filter['or']);
                }
                if ($innerfiltersql) {
                    if ($filtersql) {
                        $filtersql .= ' and (';
                    } else {
                        $filtersql .= ' (';
                    }
                    $filtersql .= $innerfiltersql . ') ';
                }
            }
            if (isset($query_filter['and']) && count($query_filter['and'])) {
                $innerfiltersql = '';
                foreach ($query_filter['and'] as $index => $value) {
                    $column = $index;
                    if ($innerfiltersql) {
                        $innerfiltersql .= ' and ';
                    }
                    //$comparator = is_array($value) ? $value[1] : $value;
                    //$type = is_string($comparator) ? 'varchar' : 'int'; //type of input control ...
                    $innerfiltersql .= $this->joinWhereQuery('query_filter', $column, $query_filter['and']);
                }
                if ($innerfiltersql) {
                    if ($filtersql) {
                        $filtersql .= ' and (';
                    } else {
                        $filtersql .= ' (';
                    }
                    $filtersql .= $innerfiltersql . ') ';
                }
            }

        }

        /*if (count($ors)) {
            $innerfiltersql = '';
            foreach ($filterArrs as $filterArr) {
                $condition = (isset($ors[$filterArr['column']]));
                if ($condition) {
                    $column = $filterArr['column'];
                    if ($innerfiltersql) {
                        $innerfiltersql .= ' or ';
                    }
                    $innerfiltersql .= $this->joinWhereQuery($filterArr['type'], $column, $ors);
                }
            }
            if ($innerfiltersql) {
                if ($filtersql) {
                    $filtersql .= ' and (';
                } else {
                    $filtersql .= ' (';
                }
                $filtersql .= $innerfiltersql . ') ';
            }
        }
        if (count($ands)) {
            $innerfiltersql = '';
            foreach ($filterArrs as $filterArr) {
                $condition = (isset($ands[$filterArr['column']]));
                if ($condition) {
                    $column = $filterArr['column'];
                    if ($innerfiltersql) {
                        $innerfiltersql .= ' and ';
                    }
                    $innerfiltersql .= $this->joinWhereQuery($filterArr['type'], $column, $ands);
                }
            }
            if ($filtersql) {
                $filtersql .= ' and (';
            } else {
                $filtersql .= ' (';
            }
            $filtersql .= $innerfiltersql . ') ';
        }*/

        foreach ($colArrs as $colArr) {
            $condition = (isset($input[$colArr['column']])) || (isset($input[$colArr['column'] . '_from']) && isset($input[$colArr['column'] . '_to']));
            if ($condition) {
                $column = $colArr['column'];
                if ($filtersql) {
                    $filtersql .= ' and ';
                }
                $filtersql .= $this->joinWhereQuery($colArr['type'], $column, $input);
            }
        }

        //logger($filtersql);

        return $filtersql;
    }

    public function getQueryCountFinYear($reportId, $finYearId)
    {
        $count = 0;
        $instance = $this->query()->select(["id", "configurable_report_type_id"])->where("id", $reportId);
        if ($instance->count()) {
            $query = $instance->first();
            $report_query = file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/t{$query->configurable_report_type_id}/cr{$query->id}.sql");
            $results = DB::select("select count(*) count from ({$report_query}) a left join fin_years b on ((a.registration_date::date >= b.start_date) AND (a.registration_date::date <= b.end_date)) where b.id = {$finYearId}");
            $count = $results[0]->count;
        }
        return $count;
    }

    public function getViewCountFinYear($reportId, $finYearId)
    {
        $count = 0;
        $instance = $this->query()->select(["view"])->where("id", $reportId);
        if ($instance->count()) {
            $query = $instance->first();
            $view = $query->view;
            $results = DB::select("select count(*) count from {$view} where fin_year_id = {$finYearId}");
            $count = $results[0]->count;
        }
        return $count;
    }

    /**
     * @param $reportId
     * @param int $finYearId
     * @param string $wheresql
     * @return mixed
     */
    public function download($reportId, $finYearId = 0, $wheresql = '')
    {
        if ($finYearId) {
            $finYear = (new FinYearRepository())->query()->where("id", $finYearId)->first();
            $fin_year_name = $finYear->name;
        } else {
            $fin_year_name = "All";
        }
        $results = $this->getResultArray($reportId, $finYearId, $wheresql);
        /* Excel Implementation ... */
        return Excel::create($results['description'] . " (" . $fin_year_name . ")", function ($excel) use ($results) {
            $excel->sheet('mySheet', function ($sheet) use ($results) {
                $sheet->fromArray((array)$results['results']);
            });
        })->download('csv');
        /* Stream Implementation ... */
        //logger("I am using stream ...");
        //logger($results);
        /*$report_name = no_space($results['description']);
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=download_error.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        $callback = function() use ($results)
        {
            $FH = fopen('php://output', 'w');
            foreach ($results['results'] as $result) {
                fputcsv($FH, $result);
            }
            fclose($FH);
        };
        return response()->stream($callback, 200, $headers);*/
    }

    /**
     * @param $reportId
     * @param $finYearId
     * @return array
     */
    public function getResultArray($reportId, $finYearId, $wheresql = '')
    {
        $query = $this->query()->select(["view", "description"])->where("id", $reportId)->first();

        if (!is_null($query->view)) {
            $view = $query->view;
            $headers = implode(',', $this->getHeadersExCol($view));
            if ($finYearId) {
                if ($wheresql) {
                    $wheresql = " and {$wheresql}";
                }
                $results = DB::select("select {$headers} from {$view} where fin_year_id = {$finYearId} {$wheresql}");
            } else {
                if ($wheresql) {
                    $wheresql = " where {$wheresql}";
                }
                $results = DB::select("select {$headers} from {$view} {$wheresql}");
            }
        } else {
            $results = [];
        }
        return ["results" => json_decode(json_encode($results), true), "description" => $query->description];
    }

    public function createViews($id = NULL)
    {
        $reportsModel = $this->query()->whereNotNull("view")->orderBy("id", "asc");
        if ($id) {
            $reportsModel->where('id', $id);
        }
        $reports = $reportsModel->get();
        foreach ($reports as $report) {
            $view = $report->view;
            //TODO: read the query directly from file, stop reading from database, drop column data_query from configurable_reports ...
            $report_query = file_get_contents(base_path() . "/resources/views/backend/report/configurable/scripts/t{$report->configurable_report_type_id}/cr{$report->id}.sql");
            //$report_query = $report->data_query;

            $viewtype = "";
            if ($report->ismaterialized) {
                $viewtype = "MATERIALIZED";
            }
/*            $sql = <<<SQL

SQL;
            DB::unprepared($sql);*/

            if ($report->has_fin_year) {
                $sql = <<<SQL
do $$ 
begin
perform deps_save_and_drop_dependencies('main', '{$view}');

if exists(select 1 from pg_views where schemaname = 'main' and viewname = '{$view}') then
    drop view if exists {$view};
else
    DROP MATERIALIZED VIEW IF EXISTS {$view};
end if;

create {$viewtype} view {$view} as select a.*, b.name fin_year, b.id fin_year_id from ($report_query) a left join fin_years b on ((a.registration_date::date >= b.start_date) AND (a.registration_date::date <= b.end_date));

perform deps_restore_dependencies('main', '{$view}');

end$$;
SQL;
            } else {
                $sql = <<<SQL
do $$ 
begin
perform deps_save_and_drop_dependencies('main', '{$view}');

if exists(select 1 from pg_views where schemaname = 'main' and viewname = '{$view}') then
    drop view if exists {$view};
else
    DROP MATERIALIZED VIEW IF EXISTS {$view};
end if;

create {$viewtype} view {$view} as select a.* from ($report_query) a;

perform deps_restore_dependencies('main', '{$view}');

end$$;
SQL;
            }
            DB::unprepared($sql);
        }

    }

    /**
     * @param array $input
     * @return array
     */
    public function setDataForSelectFilter(array $input)
    {
        //add pluck index in index source for input control with type select
        foreach ($input as $key => $data) {
            $options = [];
            if ($data['type'] == 'select') {
                $options = $data['source']['select']['options'];
                if (count($options)) {
                    //Select with manual data specified (Not Fetched from the database)
                    $input[$key]['source']['pluck'] = $options;
                } else {
                    $selectdata = $data['source']['select']['selectdata'];
                    $sql = '';
                    if (count($selectdata['auto'])) {
                        $auto = $selectdata['auto'];
                        $sql = "select id, name from {$auto['table']} ";
                        if (count($auto['search'])) {
                            $searchsql = ' where ';
                            foreach ($auto['search'] as $searchkey => $searchdata) {
                                if ($searchkey > 0) {
                                    $searchsql .= " {$searchdata['comparison']} ";
                                }
                                switch ($searchdata['logic']) {
                                    case 'in':
                                        $comparators = json_decode($searchdata['comparator']);
                                        $implode = implode(",", $comparators);
                                        $comparator = " ({$implode}) ";
                                        break;
                                    default:
                                        $comparator = ($searchdata['datatype'] == 'varchar') ? "'{$searchdata['comparator']}'" : $searchdata['comparator'];
                                        break;
                                }
                                $searchsql .= " {$searchdata['column']}::{$searchdata['datatype']} {$searchdata['logic']} {$comparator}";
                            }
                            $sql .= $searchsql;
                        }
                    } elseif ($selectdata['raw']) {
                        $sql = $selectdata['raw'];
                    }
                    if ($sql) {
                        $query = DB::select($sql);
                        $collect = collect($query)->pluck('name', 'id')->all();
                        $input[$key]['source']['pluck'] = $collect;
                    }
                }
            }
        }
        return $input;
    }

    /**
     * @param $id
     * @param $resourceid
     * @param $column
     * @return \Illuminate\Database\Eloquent\Builder|Model|null
     */
    public function getReportModel($id, $resourceid, $column)
    {
        $crReport = $this->find($id);
        $crModel = new Cr();
        $crModel->set(['table' => $crReport->view]);
        $cr = $crModel->newQuery()->where($column, $resourceid)->first();
        return $cr;
    }

     public function getReversedForDatatable(Model $cr, array $input)
    {

        $crModel = new Cr();
        $crModel->set(['table' => $cr->view]);
        $filtersql = $this->getWhereQuery($cr, $input);
        $return = $crModel->newQuery();
        //logger($filtersql);
        if ($filtersql) {
            $return->whereRaw($filtersql);
        }

        $return->where('user_id',access()->user()->id)->where('status',0)->whereIn("parent_id", function ($query) {
            $query->select('id')->from('main.wf_tracks')->where('status',2);
        });

        return $return;
    }

    public function getSubordinatesForDatatable(Model $cr,$user_id, array $input)
    {

         $crModel = new Cr();
         $crModel->set(['table' => $cr->view]);
         $filtersql = $this->getWhereQuery($cr, $input);
         $return = $crModel->newQuery();
            //logger($filtersql);
         if ($filtersql) {
            $return->whereRaw($filtersql);
        }
        $return->where('user_id',$user_id);
        return $return;
    }

}