<?php

namespace App\Repositories\Backend\Reporting;

use App\Models\Reporting\ReportCategory;
use App\Repositories\BaseRepository;
use App\Models\Reporting\Report;
use Illuminate\Support\Facades\DB;

class ReportRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = Report::class;

    public function refreshStatitics()
    {
        $sql = <<<SQL
REFRESH MATERIALIZED VIEW fin_year_months;
REFRESH MATERIALIZED VIEW fin_monthly_collection_electronic;
REFRESH MATERIALIZED VIEW fin_monthly_collection_manual;
REFRESH MATERIALIZED VIEW fin_monthly_employer_interest;
REFRESH MATERIALIZED VIEW fin_monthly_receipt;
REFRESH MATERIALIZED VIEW fin_monthly_cancelled;
REFRESH MATERIALIZED VIEW fin_monthly_dishonoured;
REFRESH MATERIALIZED VIEW comp_monthly_contribution_electronic;
REFRESH MATERIALIZED VIEW comp_monthly_contribution_manual;
REFRESH MATERIALIZED VIEW comp_yearly_contributing_employer_electronic;
REFRESH MATERIALIZED VIEW comp_yearly_contributing_employer_manual;
REFRESH MATERIALIZED VIEW comp_monthly_contributing_employer_electronic;
REFRESH MATERIALIZED VIEW comp_monthly_contributing_employer_manual;
REFRESH MATERIALIZED VIEW comp_monthly_inspected_employers;
REFRESH MATERIALIZED VIEW comp_monthly_emergence_inspection;
REFRESH MATERIALIZED VIEW comp_monthly_booking;
REFRESH MATERIALIZED VIEW comp_yearly_employer_registration;
REFRESH MATERIALIZED VIEW comp_monthly_employer_registration;
REFRESH MATERIALIZED VIEW comp_employers_per_region;
REFRESH MATERIALIZED VIEW comp_yearly_certificate_issue;
REFRESH MATERIALIZED VIEW comp_monthly_employer_certificate;
REFRESH MATERIALIZED VIEW comp_yearly_employer_certificate;
REFRESH MATERIALIZED VIEW comp_staff_verified_contribution;
REFRESH MATERIALIZED VIEW comp_large_contributor;
REFRESH MATERIALIZED VIEW comp_employer_business_status;
REFRESH MATERIALIZED VIEW successful_transactions_gepg;

SQL;

        DB::unprepared($sql);
    }

    public function refreshConfigurables($id = NULL)
    {
        $reportRepo = new ConfigurableReportRepository();
        $reportsModel = $reportRepo->query()->whereNotNull("view");
        if ($id) {
            $reportsModel->where('id', $id);
        }
        $reports = $reportsModel->get();
        foreach ($reports as $report) {
            $view = $report->view;
            if ($report->ismaterialized) {
                $sql = <<<SQL
REFRESH MATERIALIZED VIEW {$view};
SQL;
                DB::unprepared($sql);
            }
        }
    }



    /*Refresh all materialized views listed below*/
    public function refreshComplianceReportsMaterializedViews()
    {

        $sql = <<<SQL
REFRESH MATERIALIZED VIEW bookings_mview;
REFRESH MATERIALIZED VIEW bookings_mview_clone;
REFRESH MATERIALIZED VIEW summary_contrib_rcvable_age_analysis;
REFRESH MATERIALIZED VIEW summary_contrib_rcvable_income_mview;
REFRESH MATERIALIZED VIEW staff_employer_contrib_performance;
REFRESH MATERIALIZED VIEW staff_employer_contrib_performance_intern;
SQL;

        DB::unprepared($sql);

//        Log::info(print_r(66,true));
    }


//refresh view by name specified
    public function refreshMaterializedViewByName($view_name)
    {
        $sql = <<<SQL
REFRESH MATERIALIZED VIEW {$view_name};
SQL;
        DB::unprepared($sql);
    }

//refresh view by name specified
    public function refreshMaterializedViewByNameConcurrently($view_name)
    {
        $sql = <<<SQL
REFRESH MATERIALIZED VIEW CONCURRENTLY {$view_name};
SQL;
        DB::unprepared($sql);
    }
    /*Vacuum materi data*/
    public function vacuumMaterializedViewByName($view_name)
    {
        $sql = <<<SQL
vacuum {$view_name};
SQL;
        DB::unprepared($sql);
    }

}