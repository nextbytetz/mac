<?php

namespace App\Repositories\Backend\Reporting;

use App\Models\Operation\Compliance\Member\EmployerSizeType;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\FinYearRepository;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Models\Reporting\Report;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardRepository extends BaseRepository
{

    public function __construct()
    {

    }

    /**
     * COMPLIANCE SUMMARY ======================
     *
     *

    /**
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * Compliance staff performance
     */
    public function complianceStaffPerformance()
    {
        $users = new UserRepository();
        $user = access()->user();
        $designation = $user->designationTable->id;
        $unit = $user->unit->id;
        $compliance_manager = ($designation == 5 && $unit == 15) ? 1 : 0;
        $from_date = $this->getStartSummaryPeriod();
        $to_date = $this->getEndSummaryPeriod();
        $compliance_staffs = $users->getComplianceStaffs();

        //$contribution = DB::table('main.comp_staff_verified_contribution')->select(["username", "contribution"])->where("fin_year_id", fin_year()->finYearID())->where("contribution", ">=", "contribution")->get()->all();

        $data = [];
        foreach ($compliance_staffs as $compliance_staff){
            $verified_contributions = $users->verifiedContributions($compliance_staff->id, $from_date, $to_date);
            $certificates_issued = $users->certificatesIssued($compliance_staff->id, $from_date, $to_date);
            $data[] = [$compliance_staff->username, $verified_contributions, $certificates_issued];
        }

        $return['compliance_staff_performance'] = $data;
        $return['compliance_manager']= $compliance_manager;
        return $return;
    }

    /**
     * @return array
     * @throws \App\Exceptions\GeneralException
     */
    public function getComplianceEmployerSummary()
    {
        $employer = new EmployerRepository();
        $return = [];
        $return = array_merge($return, $employer->getTotalMonthlyRegisteredEmployers());
        $return = array_merge($return, $employer->getTotalRegisteredEmployersPerRegion());
        $return = array_merge($return, $employer->getEmployerBusinessStatus());
        $return = array_merge($return, $this->getRegions());
        $return = array_merge($return, $employer->getCertificateIssues());
        $return = array_merge($return, $this->complianceStaffPerformance());
        $return = array_merge($return, $this->getComplianceEmployerSizeTypes());
        $return = array_merge($return, $this->getComplianceEmployerCategoriesSummary());
        $return = array_merge($return, $this->getComplianceEmployerTinSummary());
        $return = array_merge($return, $this->getNewTaxPayers());
        $return = array_merge($return, $this->getClosedBusinesses());
        return $return;
    }

    /**
     * @return array
     * @throws \App\Exceptions\GeneralException
     * Employer registered Tin Summary
     */
    public function getComplianceEmployerTinSummary()
    {
        $employer = new EmployerRepository();
        $employer_with_tin = DB::select('select count(*) total from employers where approval_id = 1 and tin is not null and replace(trim(tin),\' \',\'\') ~ \'^[0-9\.]+$\' = 
true and length(replace(trim(tin), \' \', \'\'))  >= 9 and duplicate_id is null', []);
        $employer_without_tin = DB::select('select count(*) total  from employers where approval_id = 1 and (tin is  null or replace(trim(tin),\' \',\'\') ~ \'^[0-9\.]+$\' = false or length(replace(trim(tin), \' \', \'\'))  < 9) and duplicate_id is null', []);
        return ['employer_with_tin' => $employer_with_tin[0]->total, 'employer_without_tin' => $employer_without_tin[0]->total];
    }

    /**
     * @return array
     */
    public function getComplianceEmployerSizeTypes(){
        $employerSizeType = new EmployerSizeType();
        $employer_size_types = $employerSizeType->query()->orderBy('id','asc')->get();

        return  ['employer_size_types' => $employer_size_types];
    }

    /**
     * @return array
     * @throws \App\Exceptions\GeneralException
     */
    public function getComplianceEmployerCategoriesSummary(){
        $employer= new EmployerRepository();
        $unregisteredEmployer = new UnregisteredEmployerRepository();
        $public = $employer->query()->select(['id'])->whereHas('employerCategory', function($query){
            $query->where('reference', 'ECPUB');})->count();
        $public_central = $employer->query()->select(['id'])->whereHas('employerCategory', function($query){
            $query->where('reference', 'ECPUB');})->count();

        $private = $employer->query()->select(['id'])->whereHas('employerCategory', function($query){
            $query->where('reference', 'ECPRI');})->count();
        $unregistered = $unregisteredEmployer->query()->where('is_registered', 0)->whereNull("deleted_at")->count();

        $total_registered = $public + $private;
        $overall_total = $unregistered + $total_registered;

        $percent_employer_registered = $this->getPercent($total_registered, $overall_total);
        $percent_employer_unregistered = $this->getPercent($unregistered, $overall_total);
        return ['public_employers' =>  $public, 'private_employers' =>  $private  ,'total_employer_registered' =>  $total_registered , 'unregistered_employers'=> $unregistered  , 'overall_total_employer'=> $overall_total ,
            'employer_registered_percent' => $percent_employer_registered ,
            'employer_unregistered_percent' => $percent_employer_unregistered ];
    }

    /**
     * @return array
     */
    public function getRegions(){
        $region = new RegionRepository();
        $regions = $region->query()->orderBy('name', 'asc')->get();
        return ['regions' => $regions];
    }

    /**
     * @return array
     * FOR GRAPH: Total Monthly Registered Employees
     */
    public function getComplianceEmployeeSummary()
    {
        $employee = new EmployeeRepository();
        $return = [];
        $return = array_merge($return, $employee->getTotalMonthlyRegisteredEmployees());
        $return = array_merge($return, $employee->getEmployeeStatusSummary());

        return $return;
    }

    /**
     * @return mixed
     * Booking - contribution amount summary (Booked vs Actual Collected on particular month - Monthly report)
     */
    public function getBookingContributionVariance()
    {
        $data = [];
        $data_table = [];

        $electronic = DB::table('main.comp_monthly_contribution_electronic')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $manual = DB::table('main.comp_monthly_contribution_electronic')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $booking = DB::table('main.comp_monthly_booking')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $sum_all = 0;
        $x = 0;
        foreach ($electronic as $value) {
            $sum = $value->amount + (isset($manual[$x]->amount) ? $manual[$x]->amount : 0);
            $sum_booked = $booking[$x]->amount;
            $sum_all += $sum;
            $period = $value->period;
            $data[] = [$period, (int) $sum]; //$period . " : " . $sum
            $data_table[] = [$period, $sum, $sum_booked, $this->targetVariancePercentage($sum, $sum_booked)];
            $x++;
        }

        $overall_total = $sum_all;
        $return['booking_contribution_table'] = $data_table;
        $return['actual_contribution_overall_total'] =  ($overall_total);
        $return['actual_contribution_graph'] = $data;
        return $return;
    }

    /**
     * @return mixed
     * New Tax Payers from ADES
     */
    public function getNewTaxPayers()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        $data_table = [];
        /*  Monthly summary - current period*/
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            /*new tax payers monthly */
            $count = DB::table('new_taxpayers')
                ->whereMonth("created_at", "=", $month)
                ->whereYear("created_at", "=", $year)
                ->count();
            $count_attended = DB::table('new_taxpayers')
                ->whereMonth("created_at", "=", $month)
                ->whereYear("created_at", "=", $year)
                ->where('istaken', 1)
                ->count();

            $period = $start->format("M y");
            $data[] = [$period, (int) $count]; //$period . " : " . $sum
            $data_table[] = [$period,$count,$count_attended,];
            $start->addMonth();
        }
        $total = DB::table("new_taxpayers")->whereRaw("created_at between :start_date and :end_date ", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])->count();
        $return['new_taxpayer_table'] = $data_table;
        $return['new_taxpayer_total'] =  ($total);
        $return['new_taxpayer_graph'] = $data;
        return $return;
    }

    /**
     * @return mixed
     * Closed businesses from ADES
     */
    public function getClosedBusinesses()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        $data_table = [];
        /*  Monthly summary - current period*/
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            /* Closed businesses monthly*/
            $count = DB::table('closed_businesses')
                ->whereMonth("created_at", "=", $month)
                ->whereYear("created_at", "=", $year)
                ->count();
            $count_attended = DB::table('closed_businesses')
                ->whereMonth("created_at", "=", $month)
                ->whereYear("created_at", "=", $year)
                ->where('wf_done', 1)
                ->count();

            $period = $start->format("M y");
            $data[] = [$period, (int) $count]; //$period . " : " . $sum
            $data_table[] = [$period,$count,$count_attended,];
            $start->addMonth();
        }
        $total = DB::table("closed_businesses")->whereRaw("created_at between :start_date and :end_date ", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])->count();
        $return['closed_businesses_table'] = $data_table;
        $return['closed_businesses_total'] =  ($total);
        $return['closed_businesses_graph'] = $data;
        return $return;
    }

    /**
     * @return array
     * Get Inspection Summary
     */
    public function getInspectionSummary()
    {
        $inspection = new InspectionRepository();
        $return = [];
        $return = array_merge($return, $inspection->getTotalMonthlyInspectedEmployers());
        $return = array_merge($return, $inspection->getTotalMonthlyInspectedEmployersTable());
        $return = array_merge($return, $inspection->getTotalMonthlyEmergence());

        return $return;
    }
    /* End of Compliance Summary------------------- */

    /**
     * Notification Report Dashboard Summary===============================
     *
     */
    public function getNotificationIncidentTypeReceived()
    {
        /*  Accident */
        $notification = new NotificationReportRepository();
        $accident = [];
        $death = [];
        $disease = [];
        $accident = [
            'accident_graph' => $notification->getTotalMonthlyNotificationsByType(1)['graph'],
            'accident_table' => $notification->getTotalMonthlyNotificationsByType(1)['table'],
            'accident_overall_total' => $notification->getTotalMonthlyNotificationsByType(1)['overall_total']
        ];

        $disease = [
            'disease_graph' => $notification->getTotalMonthlyNotificationsByType(2)['graph'],
            'disease_table' => $notification->getTotalMonthlyNotificationsByType(2)['table'],
            'disease_overall_total' => $notification->getTotalMonthlyNotificationsByType(2)['overall_total']
        ];


        $death = [
            'death_graph' => $notification->getTotalMonthlyNotificationsByType(3)['graph'],
            'death_table' => $notification->getTotalMonthlyNotificationsByType(3)['table'],
            'death_overall_total' => $notification->getTotalMonthlyNotificationsByType(3)['overall_total']
        ];

        return array_merge($accident, $death, $disease);
    }

    /**
     * @return array
     * @deprecated
     */
    public function getPendingClaimsSummary()
    {
        $notification = new NotificationReportRepository();
        //$pending_claims = $notification->query()->where('wf_done', 0)->where('status', '<>', 0)->get();
        return ['pending_claims' => []];
    }

    /**
     * @return array
     */
    public function getClaimSummary($isinfo = 0)
    {
        $fin_year_name = $this->getFinYearName(request()->input('fin_year_id'));
        $notification = new NotificationReportRepository();
        $return = [];
        $return = array_merge($return, $notification->getTotalMonthlyNotifications());
        $return = array_merge($return, $notification->getTotalMonthlyClaim());
        //$return = array_merge($return, $notification->getTotalMonthlyRejectedNotifications());
        //$return = array_merge($return, $notification->getTotalMonthlyProcessedNotifications());
        //$return = array_merge($return, $notification->getNotificationReportStatusSummary());
        $return = array_merge($return, $this->getNotificationIncidentTypeReceived());
        $return = array_merge($return, $this->getTotalWorkplaces());
        $return = array_merge($return, $this->getTotalAuditSummary($fin_year_name));
        $return = array_merge($return, $this->returnTotalEmployersForAudit($fin_year_name));
        $return = array_merge($return, $this->bodyPartInjuries($fin_year_name));
        // dd($this->bodyPartInjuries($fin_year_name));
        //$return = array_merge($return, $dashboard->getPendingClaimsSummary());
        if ($isinfo) {
            $return = array_merge($return, $this->processedClaimSummary());
        }
        return $return;
    }

    /**
    *@return get body parts injuries
    */
    public function bodyPartInjuries($fin_year_name){
        $dates = "";
        if ($fin_year_name == "") {
            $fin_year_name = $this->currentFinancialYear();
            $dates = $this->getDates($fin_year_name);
        } else {
            $dates = $this->getDates($fin_year_name);
        }

        $first_half = Carbon::parse($dates['first_half'])->format('Y-m-d');
        $second_half = Carbon::parse($dates['second_half'])->format('Y-m-d');
        
        $injuries = DB::select(DB::raw("select count(bp.body_part_injury_group_id) as injury_group_count,bg.id as b_part_group_id from
        (select nb.body_part_injury_id,b.name,b.body_part_injury_group_id,nb.incident_date from
        (select nl.body_part_injury_id,n.incident_date from 
        (select notification_reports.id,notification_reports.incident_date from main.notification_reports) n
        left join main.notification_lost_body_parts as nl on nl.notification_report_id=n.id) nb
        left join main.body_part_injuries as b on b.id=nb.body_part_injury_id) bp
        left join main.body_part_injury_groups as bg on bg.id=bp.body_part_injury_group_id where bp.incident_date between '".$first_half."' and '".$second_half."' group by b_part_group_id"));

        $injuries_arr = array();
        if (!empty($injuries)) {
            foreach ($injuries as $key => $value) {
            if ($value->b_part_group_id == 1) {
                $injuries_arr['head'] = $value->injury_group_count;
            } elseif($value->b_part_group_id == 2) {
                $injuries_arr['neck'] = $value->injury_group_count;
            }
             elseif($value->b_part_group_id == 3) {
                $injuries_arr['trunk'] = $value->injury_group_count;
            }
             elseif($value->b_part_group_id == 4) {
                $injuries_arr['lowerlimb'] = $value->injury_group_count;
            }
             elseif($value->b_part_group_id == 5) {
                $injuries_arr['upperlimb'] = $value->injury_group_count;
            }
            elseif($value->b_part_group_id == 6) {
                $injuries_arr['systemic'] = $value->injury_group_count;
            }
        }

        $injuries_arr = $this->issetOnInjuruesArray($injuries_arr);

        } else {
            $injuries_arr = $this->putNullOnInjuriesArray($injuries_arr);
        }
        
       
        return $injuries_arr;
    }

    public function issetOnInjuruesArray($array){
        $array['head'] = isset($array['head']) ? $array['head'] : null;
        $array['neck'] = isset($array['neck']) ? $array['neck'] : null;
        $array['trunk'] = isset($array['trunk']) ? $array['trunk'] : null;
        $array['lowerlimb'] = isset($array['lowerlimb']) ? $array['lowerlimb'] : null;
        $array['upperlimb'] = isset($array['upperlimb']) ? $array['upperlimb'] : null;
        $array['systemic'] = isset($array['systemic']) ? $array['systemic'] : null;

        return $array;
      
    }

    public function putNullOnInjuriesArray($array){
        $array = [
            'head' => null,
            'neck' => null,
            'trunk' => null,
            'lowerlimb' => null,
            'upperlimb' => null,
            'systemic' => null
        ];
        return $array;
    }

    public function getDates($fin_year_name){
       $fin = explode('/', $fin_year_name);
       $first_half = '01-07-'.$fin[0];
       $second_half = '30-06-'.$fin[1]; 
       $dates = [
        'first_half' => Carbon::parse($first_half)->format('Y-m-d'),
        'second_half' => Carbon::parse($second_half)->format('Y-m-d')
       ];
       return $dates;
    }

    /**
    *@return financial year name
    */
    public function getFinYearName($fin_year_id){
        $fin_year_name = DB::table('main.fin_years')->find($fin_year_id);
        return ($fin_year_name) ? $fin_year_name->name : '';
    }

    /**
     * @return mixed
     */
    public function getTotalWorkplaces(){
        $workplaces = DB::table('main.workplaces')->count();
        $workplaces = [
           'workplaces' => $workplaces
        ];
        return $workplaces;
    }

    /**
     * @return get current financial year
     */
     public function currentFinancialYear(){
      
        $this_year = date('Y');
        $next_year = date('Y') + 1;
        $last_year = date('Y') - 1;

        $current_financial_year = '';
        global $current_financial_year;

        if (date('m') > 6) {
       
        $current_financial_year = $this_year.'/'.$next_year;

        } else {
        $current_financial_year = $last_year.'/'.$this_year;
        }

        return $current_financial_year;

    }

    /**
     * @return mixed
     */
    public function getTotalAuditSummary($fin_year_name){
        $workplaces = DB::table('main.osh_audit_ratings')
        ->where('fin_year',$fin_year_name)->count();
        $audit_summary = [
           'audit_summary' => $workplaces
        ];
        return $audit_summary;
    }

    public function returnTotalEmployersForAudit($fin_year_name){
        $total_employer = DB::table('main.osh_audit_period_employers')
        ->join('osh_audit_period','osh_audit_period.id','=','osh_audit_period_employers.osh_audit_period_id')
        ->where('fin_year',$fin_year_name)->count();
        $total_employer_audit = [
           'total_employer_audit' => $total_employer
        ];
        return $total_employer_audit;
    }

    /**
     * @return mixed
     */
    public function processedClaimSummary()
    {
        $configurableRepo = new ConfigurableReportRepository();
        $finYearRepo = new FinYearRepository();

        $financialYears = $finYearRepo->query()->select(["id", "name"])->where("isactive", 1)->get()->toArray();
        $return['fin_years'] = $financialYears;

        $reports = $configurableRepo->query()->select(["id", "name", "description", "isbalance"])->where(["unit_id" => 14, "isdashboard" => 1])->orderBy("isbalance", "desc")->orderBy("sort", "asc")->get()->toArray();
        //dd($reports[3]['id']);
        //$reports[0]['id'] = 73;
        $processed_claims = [];
        $index = 0;
        foreach ($reports as $report) {
            $total = 0;
            foreach ($financialYears as $financialYear) {
                $count = $configurableRepo->getViewCountFinYear($reports[$index]['id'], $financialYear['id']);
                $reports[$index]['fin_year' . $financialYear['id']] = $count;
                $total += $count;
            }
            $reports[$index]['grand_total'] = $total;
            $index+=1;
        }

        $return['processed_claims'] = $reports;

        return $return;
    }

    public function lostTimeInjuries(){
                    $lost_time_injuries=DB::table('main.osh_employers_summary_report')->select('total_days_lost')->sum('total_days_lost');
                     return $lost_time_injuries;

    }

    /*  End of notification report dashboard---------------------------------------- */



}