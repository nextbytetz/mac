<?php

namespace App\Repositories\Backend\Finance;

use App\Repositories\BaseRepository;
use App\Models\Finance\FinCode;

class FinCodeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = FinCode::class;

    public function getAllAdministrative()
    {
        return $this->query()->whereIn("fin_code_group_id", [3,1])->get();
    }

    public function getAllAdministrativeCN()
    {
        return $this->query()->whereIn("id",[6,12,13,14,16,17,18,19,20,21,22,23,24,25,26,38,39,40,49,50,55,57])->get();
    }

    public function getQuery()
    {
       return $this->query();
    }

}