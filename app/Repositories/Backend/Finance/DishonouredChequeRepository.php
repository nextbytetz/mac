<?php

namespace App\Repositories\Backend\Finance;


use App\Models\Finance\DishonouredCheque;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;

class DishonouredChequeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = DishonouredCheque::class;

    public function __construct()
    {

    }
    public function findOrThrowException($id)
    {
        $dishonoured_cheque = $this->query()->find($id);
        if (!is_null($dishonoured_cheque)) {
            return $dishonoured_cheque;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.dishonoured_cheque_not_found'));
    }

    public function create($id,$input,$bank_id,$chequeno)
    {
        return $this->query()->create(['receipt_id'=>$id, 'dishonour_reason' => $input['dishonour_reason'],'dishonour_user_id' => access()->user()->id,'old_bank_id'=>$bank_id,'old_cheque_no'=>$chequeno]);
    }
    public function update($id,$input)
    {

        $this->query()->where('receipt_id',$id)->whereNull('new_cheque_no')->update(['new_cheque_no'=> $input['new_chequeno'],'new_bank_id'=>$input['bank'],'replacer_user_id'=> access()->user()->id]);

    }

    public function getQuery(){
        return $this->query();

    }


}