<?php

namespace App\Repositories\Backend\Finance;

use App\Models\Finance\Bank;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;

class BankRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Bank::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $bank= $this->query()->find($id);
        if (!is_null($bank)) {
            return $bank;
        }
        throw new GeneralException(trans('exceptions.backend.finance.bank_not_found'));
    }

    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }

    public function getWcfBanksForSelect()
    {
        return $this->query()->whereIn("id", [3, 4])->get()->pluck('name', 'id');
    }


    /**
     * @return array
     * Get bank id which process batch on payroll payment to ERP
     */
    public function getBankIdsForPayrollBatchPayment()
    {
        return [3,4];
    }


    public function getActiveBanksForSelect()
    {
        return $this->query()->whereNotIn("id", [5, 6, 2])->orderBy('name')->get()->pluck('name', 'id');
//        return $this->query()->whereNotIn("id", [ 6, 2])->orderBy('name')->get()->pluck('name', 'id');
    }

}