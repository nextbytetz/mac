<?php

namespace App\Repositories\Backend\Finance;

use App\Exceptions\GeneralException;
use App\Jobs\ProcessPayment\ProcessCompensationPayment;
use App\Jobs\ProcessPayment\ProcessFuneralGrant;
use App\Jobs\ProcessPayment\ProcessInterestRefund;
use App\Jobs\ProcessPayment\ProcessSurvivorGratuity;
use App\Models\Finance\PaymentVoucher;
use App\Models\Finance\PaymentVoucherTransaction;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class PaymentVoucherTransactionRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PaymentVoucherTransaction::class;
    protected $claim_compensations;
    protected $payroll_procs;
    protected $member_types;
    protected $dependents;
    protected $benefit_types;
    protected $process_checker;
    protected $chunksize =100;


    public function __construct()
    {
        $this->claim_compensations = new ClaimCompensationRepository();
        $this->payroll_procs = new PayrollProcRepository();
        $this->member_types = new MemberTypeRepository();
        $this->dependents = new DependentRepository();
        $this->benefit_types = new BenefitTypeRepository();
    }

    //find or throwException for fin code group id
    public function findOrThrowException($id)
    {
        $payment_voucher_tran= $this->query()->find($id);
        if (!is_null($payment_voucher_tran)) {
            return $payment_voucher_tran;
        }
        throw new GeneralException(trans('exceptions.backend.finance.payment_voucher_transaction_not_found'));
    }

    /*
     * create new
     */

    public function create($input) {
        $payment_voucher_tran = $this->query()->create($input);
        return $payment_voucher_tran;
    }
    /*
     * update
     */
    public function update($id,$input) {

    }

    /**
     * @param $input
     * @param $payment_voucher_id
     * @return mixed
     * update is_process flag if already processed => Individual
     */
    public function processIndividual($input, $payment_voucher_id) {

        $payment_voucher_trans = $this->query()->where('member_type_id',$input['member_type_id'])->where('resource_id',$input['resource_id'])->where('benefit_resource_id',$input['benefit_resource_id'])->where('is_processed', 0)->update(['is_processed'=> 1, 'payment_voucher_id' => $payment_voucher_id ]);
        return $payment_voucher_trans;
    }


    /**
     * @param $input
     * @param $payment_voucher_id
     * @return mixed
     * update is_process flag if already processed
     */
    public function processBatch($input, $payment_voucher_id) {

        $payment_voucher_trans = $this->query()->where('member_type_id',$input['member_type_id'])->where('resource_id',$input['resource_id'])->where('is_processed', 0)->update(['is_processed'=> 1, 'payment_voucher_id' => $payment_voucher_id ]);
        return $payment_voucher_trans;
    }

    /*
     * Payment vouchers Transactions not yet processed to Payment Vouchers -INDIVIDUAL (employee, employer)
     */
    public function pendingIndividual(){
        return $this->query()->where('is_processed', 0)->where('member_type_id','<>',3);
    }

    /*
 * Payment vouchers Transactions not yet processed to Payment Vouchers -BATCH (insurance)
 */
    public function pendingBatch(){
        return $this->query()->where('is_processed', 0)->where('member_type_id','=',3);
    }



    /**
     * Get Distinct Pending Payment Voucher Transactions general - All member types
     */
    public function getDistinctPendingPVTran(){

        return $this->query()->distinct('benefit_resource_id')->select(['benefit_resource_id','member_type_id', 'resource_id'])->where('is_processed', 0)->orderBy('benefit_resource_id', 'asc');
    }


    /**
     * Get Distinct Pending Payment Voucher Transactions (Not yet ov tran processed OTHER THAN INSURANCE (Batch process))
     */
    public function getDistinctPendingIndividualPVTran(){

        return DB::table('payment_voucher_transactions')->distinct('benefit_resource_id')->select(['benefit_resource_id','member_type_id', 'resource_id'])->where('is_processed', 0)->where('member_type_id','<>',3)->orderBy('benefit_resource_id', 'asc');
    }


    /**
     * Get Distinct Pending Payment Voucher Transactions (Not yet tran processed (INSURANCE transactions (Batch process))
     */
    public function getDistinctPendingBatchPVTran(){
        return DB::table('payment_voucher_transactions')->distinct('benefit_resource_id')->select(['member_type_id', 'resource_id'])->where('is_processed', 0)->where('member_type_id',3)->orderBy('member_type_id', 'asc');
    }



    /**
     * @return mixed
     * process_type : 1 -> Single (Individual Payments i.e Employee and employer)=> LUMP SUM,  2 - Batch Monthly pension
     * processing i.e
     * Insurance
     */

    public function createPayrollProc($process_type, $user_id){
        $run_date = Carbon::parse('now')->format('Y-n-j');
        if ($process_type == 1){
            $input =['description'=> 'LUMP SUM', 'user_id'=>$user_id, 'bquarter'=> 'N/A', 'payroll_proc_type_id' => 1, 'run_date' => $run_date];
        }else if ($process_type == 2){
            $input =['description'=> 'LUMP SUM - MONTHLY BATCH', 'user_id'=>$user_id, 'bquarter'=> 'N/A', 'payroll_proc_type_id'=> 3, 'run_date' => $run_date];
        }

        return  $this->payroll_procs->create($input);
    }





    /**
     * Process all payment completed workflow
     */
    public function processPayment($user_id){
        return DB::transaction(function () use($user_id)  {
            /*Create payroll proc*/

            $payroll_proc_id = $this->createPayrollProc(1, $user_id);
            $this->process_checker = 0;
            $this->processCompensationPaymentChunk($user_id);
            $this->processFuneralGrantChunk($user_id);
            $this->processSurvivorGratuityChunk($user_id);
            $this->processInterestRefundsChunk($user_id);
            //check if there is any thing to process
            $this->checkIfCanProcess();
        });
    }


    /**
     * Check if there is any pending payment need to be processed
     *
     */
    public function checkIfCanProcess(){
        if ($this->process_checker == 0){
            throw new GeneralException(trans('exceptions.general.nothing_to_process'));
        }
    }


    /**
     * Process compensations which have completed workflow and not yet paid
     */
    public function processCompensationPaymentChunk($user_id){

        $this->claim_compensations->query()->where('ispaid', 0)->whereNull('notification_eligible_benefit_id')->chunk($this->chunksize , function ($pending_claim_compensations) use ($user_id){

            if (isset($pending_claim_compensations)) {
                $this->process_checker = 1;

                /*process*/
                dispatch(new ProcessCompensationPayment($pending_claim_compensations, $user_id));
                /*end process*/
            }
        });


    }
    /*Process compensation payment*/
    public function processCompensationPayment($pending_claim_compensations,$user_id)
    {
        $member_types = new MemberTypeRepository();
        $claim_compensations = new ClaimCompensationRepository();
        foreach ($pending_claim_compensations as $pending_claim_compensation){
            $amount = $pending_claim_compensation->amount;
            $member = $member_types->getMember($pending_claim_compensation->member_type_id,$pending_claim_compensation->resource_id);
            $input = ['benefit_resource_id' => $pending_claim_compensation->claim_id, 'resource_id'=> $pending_claim_compensation->resource_id, 'member_type_id'=> $pending_claim_compensation->member_type_id, 'amount' => $amount,'benefit_type_id' => $pending_claim_compensation->benefit_type_id,  'user_id' => $user_id, 'claim_compensation_id' =>$pending_claim_compensation->id];

            if ($amount > 0){
                if (!$this->checkIfPVTranExists($input)){
                    $payment_voucher_tran = $this->create($input);
                }

            }
            // update claim compensation - PAY
            $claim_compensation = $claim_compensations->pay($pending_claim_compensation->id);
        }


    }



    /**
     * Process funeral grants
     */
    public function processFuneralGrantChunk($user_id){
        $this->dependents->pendingFuneralGrantDependents()->chunk($this->chunksize , function ($funeral_grants) use
        ($user_id){
            //$dependents = $this->dependents->query()->chunk($this->chunksize , function ($funeral_grants) use ($user_id){
            if (isset($funeral_grants)){
                /*process*/
                $this->process_checker = 1;
                dispatch(new ProcessFuneralGrant($funeral_grants,$user_id));
            }
        });

    }

    /*Process Funeral Grants */
    public function processFuneralGrant($funeral_grants, $user_id){
        $member_type_id = 4; // dependent
        $benefit_type = $this->benefit_types->findOrThrowException(2); // funeral grant
        $benefit_type_id = $benefit_type->id;
        $dependents = new DependentRepository();
        foreach($funeral_grants as $dependent) {
            /*pivot to employees*/
            $dependent_pivots = $dependents->pendingFuneralGrants()->where('dependent_id', $dependent->id)->get();
            /*Loop on all pivot of dependent for all employee related to*/
            foreach ($dependent_pivots as $dependent_pivot) {
                $notification_report = $dependents->findNotificationReport($dependent_pivot->dependent_id,
                    $dependent_pivot->employee_id);
                if ($notification_report->wf_done == 1) {
                    $claim_id = $notification_report->claim->id;
                    $input = ['benefit_resource_id' => $claim_id, 'resource_id' => $dependent_pivot->dependent_id, 'member_type_id' => $member_type_id, 'amount' => $dependent_pivot->funeral_grant, 'benefit_type_id' => $benefit_type_id, 'user_id' => $user_id, 'claim_compensation_id' => null];
                    if ($dependent_pivot->funeral_grant > 0) {
                        if (!$this->checkIfPVTranExists($input)) {
                            $payment_voucher_tran = $this->create($input);
                        }
                    }
                    /*Set funeral pay flag to 1*/
                    $dependents->updateFuneralPayFlag($dependent_pivot->dependent_id, 1, $dependent_pivot->employee_id);
                }
            }
        }

    }




    /**
     * Process Gratuity / Lumpsum
     */
    public function processSurvivorGratuityChunk($user_id){
        $dependents = $this->dependents->pendingSurvivorGratuityDependents()->chunk($this->chunksize , function
        ($survivor_gratuities) use ($user_id){
            if (isset($survivor_gratuities)){
                /*process*/
                dispatch(new ProcessSurvivorGratuity($survivor_gratuities,$user_id));
                $this->process_checker = 1;
            }

        });
    }


    /*process survivor gratuity*/
    public function processSurvivorGratuity($survivor_gratuities,$user_id){

        $member_type_id = 4; // dependent
        $benefit_type = $this->benefit_types->findOrThrowException(11); // Survival Benefits - lump sum / gratuity
        $benefit_type_id = $benefit_type->id;
        $dependents = new DependentRepository();
        foreach($survivor_gratuities as $dependent) {
            /*pivot to employees*/
            $dependent_pivots = $dependents->pendingSurvivorGratuity()->where('dependent_id', $dependent->id)->get();
            /*Loop on all pivot of dependent for all employee related to*/
            foreach ($dependent_pivots as $dependent_pivot) {
                $notification_report = $dependents->findNotificationReport($dependent_pivot->dependent_id,
                    $dependent_pivot->employee_id);
                /*Check if WF DONE is completed for this notification_report*/
                if($notification_report->wf_done == 1) {
                    $claim_id = $notification_report->claim->id;
                    $input = ['benefit_resource_id' => $claim_id, 'resource_id' => $dependent_pivot->dependent_id, 'member_type_id' => $member_type_id, 'amount' => $dependent_pivot->survivor_gratuity_amount, 'benefit_type_id' => $benefit_type_id, 'user_id' => $user_id, 'claim_compensation_id' => null];
                    if ($dependent_pivot->survivor_gratuity_amount > 0) {
                        if (!$this->checkIfPVTranExists($input)) {
                            $payment_voucher_tran = $this->create($input);
                        }
                    }
                    /*Set survivor gratuity pay flag to 1*/
                    $dependents->updateGratuityPayFlag($dependent_pivot->dependent_id, 1, $dependent_pivot->employee_id);
                }
            }
        }

    }



    /**
     *
     * Process chunk for Interest Refunds
     */
    public function processInterestRefundsChunk($user_id){
        $booking_interests = new BookingInterestRepository();
        $dependents = $booking_interests->getPendingInterestRefundForPayment()->chunk($this->chunksize , function
        ($interest_refunds) use ($user_id){
            if (isset($interest_refunds)){
                /*process*/
                dispatch(new ProcessInterestRefund($interest_refunds,$user_id));
                $this->process_checker = 1;
            }

        });
    }


    public function processInterestRefunds($interest_refunds, $user_id)
    {
        $benefit_types = new BenefitTypeRepository();
        $benefit_type_id = $benefit_types->getInterestRefund(); // Interest Refund = 12
        $member_types = new MemberTypeRepository();
        foreach ($interest_refunds as $interest_refund){
            $amount = $interest_refund->amount;
            $member = $interest_refund->getEmployer();
            $member_type_id = $member_types->getEmployerType();
            $input = ['benefit_resource_id' => $interest_refund->id, 'resource_id'=> $member->id, 'member_type_id'=> $member_type_id, 'amount' => $amount,'benefit_type_id' => $benefit_type_id,  'user_id' => $user_id, 'claim_compensation_id' => null];
            if ($amount > 0){
                if (!$this->checkIfPVTranExists($input)) {
                    $payment_voucher_tran = $this->create($input);
                }
            }
            // update interest refund - PAY
            $interest_refund->update(['is_paid'=>1]);
        }
    }




    /*Check if Payment Voucher tran Already Exist*/
    public function checkIfPVTranExists($input)
    {
        $pv_tran = $this->query()->where('benefit_resource_id',$input['benefit_resource_id'])->where('member_type_id',$input['member_type_id'] )->where('resource_id',$input['resource_id'])->where('benefit_type_id',$input['benefit_type_id'])->where('claim_compensation_id', $input['claim_compensation_id'])->first();
        return $pv_tran;
    }


    /*Update is_exported flag after export to ERP*/
    public function updateExportFlag($id)
    {
        $pv_tran= $this->find($id);
        $pv_tran->update([
            'is_exported' => 1
        ]);
        return $pv_tran;
    }


    /*Update is_duplicate flag after export to ERP*/
    public function updateDuplicateFlag($id)
    {
        $pv_tran= $this->find($id);
        $pv_tran->update([
            'isduplicate' => 1
        ]);
        return $pv_tran;
    }

    /*Rectify duplicate entries*/
    public function rectifyDuplicateEntries()
    {
        $this->query()->whereIn('id', [2155, 2188, 2412, 2691])->update([
            'isduplicate' => 1
        ]);
    }


    /**
     * Get all pending assessed claim pending for export to ERP i.e. claim completed workflow and payment processed on MAC
     */
    public function getPendingExportPvTranForDataTable()
    {
        $benefit_types = new BenefitTypeRepository();
        $claim_benefit_groups = $benefit_types->getClaimBenefitGroupIds();
        $erp_start_date = erp_start_date();
        return $this->query()->where('is_exported', 0)->whereHas('benefitType', function ($query) use($claim_benefit_groups){
            $query->whereIn('benefit_type_group_id',$claim_benefit_groups );
        })->whereDate('created_at','>=', $erp_start_date)->where('isduplicate', 0)->orderBy('benefit_resource_id');
    }


    /**
     * Get all pending assessed claim pending for export to ERP per Benefit approved <NEW>
     */
    public function getPendingExportPvTranPerBenefitForDataTable($notification_workflow_id)
    {
        $benefit_types = new BenefitTypeRepository();
        $claim_benefit_groups = $benefit_types->getClaimBenefitGroupIds();
        return $this->query()->where('is_exported', 0)->whereHas('benefitType', function ($query) use($claim_benefit_groups, $notification_workflow_id){
            $query->whereIn('benefit_type_group_id',$claim_benefit_groups );
        })->whereHas('claimCompensation', function($query)use($notification_workflow_id){
            $query->whereHas('notificationEligibleBenefit', function($query) use($notification_workflow_id){
                $query->where('notification_workflow_id', $notification_workflow_id);
            });
        })->orderBy('benefit_resource_id');
    }


    /**
     * @param Model $pv_tran
     * @return array
     * Get all API Fields for Pending export
     */
    public function getApiFieldsForPendingExports(Model $pv_tran)
    {
        $member_types = new MemberTypeRepository();
        $notification_report =  $pv_tran->claim->notificationReport;
        $member = $member_types->getMember($pv_tran->member_type_id, $pv_tran->resource_id);

        if(isset($member->bank_branch_id)){
            $bank = DB::table('main.bank_branches')
                ->join('main.banks', 'bank_branches.bank_id', '=', 'banks.id')
                ->where('bank_branches.id',$member->bank_branch_id)
                ->first();
            $bank_branch_id = $member->bank_branch_id;
            $bank_name = 'CRDB';
            $accountno = $member->accountno;
            $swift_code = ($bank_name == 'CRDB') ? 'CORUTZTZ' : 'NMIBTZTZ';
        }else{
            $bank_branch_id = 0;
            $bank_name = 'CRDB';
            $accountno = '00';
            $swift_code = 'CORUTZTZ';
        }

        $gfs_code = $pv_tran->benefitType->finCode->gfs_code;
        $employee_id = (isset($pv_tran->claim->notificationReport->employee_id)) ? $pv_tran->claim->notificationReport->employee_id : null;
        $memberno =  $this->getMemberNoForErpApi($member, $pv_tran->member_type_id,$employee_id);
        return [
            'filename' => $notification_report->filename,
            'payee' =>$member->name ,
            'memberno' =>$memberno,
            'member_type' => $pv_tran->memberType->name,
            'benefit_type' => $pv_tran->benefitType->name,
            'amount' =>$pv_tran->amount ,
//            'debit_account_expense' => '01.001.AB.000000.27110104.000.000',
            'debit_account_expense' => '01.001.AB.000000.'. $gfs_code .'.000.000',
            'credit_account_receivable' => '01.001.AB.000000.43181104.000.000',
            'accountno' => $accountno,
            'swift_code' => $swift_code,
            'bank_branch_id' =>$bank_branch_id,
            'bank_name' =>$bank_name,
            'bank_address' =>'Dar es Salaam' ,
            'country_name_bank' => 'Tanzania',
            'city_of_bank' => 'Dar es Salaam' ,
            'state_or_region' => 'Dar es Salaam',
            'is_paid' => 0,
        ];
    }

    /*Get member no according member type specified for Erp API*/
    public function getMemberNoForErpApi($member, $member_type_id, $employee_id = null)
    {
        $memberno = null;
        switch($member_type_id){
            case 1:
                /*employer*/
                $memberno = $member->reg_no;
                break;
            case 2:
                /*employee*/
                $memberno = $member->memberno;
                break;
            case 4:
                /*dependents*/
                $memberno = $member->getMemberNoForErpApi($employee_id);
                break;
            case 5:
                /*pensioners*/
                $memberno = $member->employee->memberno;
                break;
            default :
                $memberno =  $member->id;
                break;
        }

        return $memberno;
    }

    /**
     * @param $pv_tran_id
     * Undo payment voucher tran
     */
    public function undoPaymentVoucherTran($pv_tran_id)
    {
        DB::transaction(function () use ($pv_tran_id) {
            $pv_tran = $this->find($pv_tran_id);

            /*Delete claim payable*/
            $pv_tran->claimPayable()->delete();

            /*Delete payment voucher*/
            $pv_tran->paymentVoucher()->delete();

            /*Delete voucher transaction*/
            $pv_tran->delete();
        });
    }


}