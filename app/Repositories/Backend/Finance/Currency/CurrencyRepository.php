<?php

namespace App\Repositories\Backend\Finance\Currency;

use App\Models\Finance\Currency;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

class CurrencyRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Currency::class;

    public function __construct() {

    }
//
    public function findOrThrowException($id) {

        $currency = $this->find($id);

        if (!is_null($currency)) {
            return $currency;
        }
        throw new GeneralException(trans('exceptions.backend.finance.currencies.not_found'));
    }

    public function getForDataTable() {
        return $this->getAll();
    }

    public function getAllCurrency() {
        return $this->getAll()->pluck('code', 'id');
    }


    public function create($input) {
        $name = $input["name"];
        $currency = $this->query()->create(['name' => $name,'code' => $input['code'],'exchange_rate' => $input['exchange_rate'],]);
    }

    public function destroy($id) {
        $currency = $this->findOrThrowException($id);
         if ($currency->delete()) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    public function update($id, $input) {
        $currency = $this->findOrThrowException($id);
        $this->checkCurrencyByName($input, $currency);
        $this->checkCurrencyByCode($input, $currency);
        if ($currency->update($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    private function checkCurrencyByCode($input, $currency) {
        //Figure out if name is not the same
        if ($currency->code != $input['code']) {
            //Check to see if name exists
            if ($this->checkIfExistByCode($input['code'], $input['id'])) {
                throw new GeneralException(trans('exceptions.backend.finance.currencies.already_exists'));
            }
        }
    }

    private function checkIfExistByCode($code, $id) {
        $code = strtolower(trim(preg_replace('/\s+/', '', $code)));
        if ($this->query()->whereRaw("code =:code and id = :id", ['name' => $code, 'id' => $id])->first()) {
            //throw new GeneralException(trans('exceptions.backend.sysdef.location.branch.exist'));
            return true;
        } else {
            return false;
        }
    }

    private function checkCurrencyByName($input, $currency) {
        //Figure out if name is not the same
        if ($currency->name != $input['name']) {
            //Check to see if name exists
            if ($this->checkIfExist($input['name'], $input['id'])) {
                throw new Exception(trans('exceptions.backend.finance.currencies.not_found'));
              }
        }
    }


    private function checkIfExist($name, $id) {
//        $name = strtolower(trim(preg_replace('/\s+/', '', $name)));
//        if (Currency::whereRaw("lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) = :name and id = :id", ['name' => $name, 'id' => $id])->first()) {
//            //throw new GeneralException(trans('exceptions.backend.sysdef.location.branch.exist'));
//            return true;
//        } else {
//            return false;
//        }

        $name = strtolower(trim(preg_replace('/\s+/', '', $name)));
        if ($this->query()->whereRaw("name = :name and id <> :id", ['name' => $name, 'id' => $id])->first()) {
            //throw new GeneralException(trans('exceptions.backend.sysdef.location.branch.exist'));
            return true;
        } else {
            return false;
        }
    }


}
