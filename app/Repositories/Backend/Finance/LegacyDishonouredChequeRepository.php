<?php

namespace App\Repositories\Backend\Finance;


use App\Models\Finance\DishonouredCheque;
use App\Models\Finance\LegacyDishonouredCheque;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;

class LegacyDishonouredChequeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = LegacyDishonouredCheque::class;

    public function __construct()
    {

    }
    public function findOrThrowException($id)
    {
        $legacy_dishonoured_cheque = $this->query()->find($id);
        if (!is_null($legacy_dishonoured_cheque)) {
            return $legacy_dishonoured_cheque;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.dishonoured_cheque_not_found'));
    }

    public function create($legacy_receipt_id)
    {
        $this->query()->create(['legacy_receipt_id'=>$legacy_receipt_id]);

    }
    public function update($id,$input)
    {


    }

    public function getQuery(){
        return $this->query();

    }


}