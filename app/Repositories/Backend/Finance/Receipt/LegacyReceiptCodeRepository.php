<?php

namespace App\Repositories\Backend\Finance\Receipt;

use App\Exceptions\GeneralException;
use App\Models\Finance\Receipt\LegacyReceipt;
use App\Models\Finance\Receipt\LegacyReceiptCode;
use App\Repositories\BaseRepository;

class LegacyReceiptCodeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = LegacyReceiptCode::class;


    //find or throw Exception for receipt
    public function findOrThrowException($id)
    {
        $legacy_receipt = $this->find($id);
        if (!is_null($legacy_receipt)) {
            return $legacy_receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipt_codes.not_found'));
    }



    public function checkIfAllContributionsHaveLinkedFile($legacy_receipt_id){
        $legacy_receipt = $this->query()->whereHas('legacyReceipt', function ($query) use ($legacy_receipt_id) {
            $query->where('legacy_receipt_id', $legacy_receipt_id);
        })->whereNull('linked_file')->orWhere('error', 1)->first();
        if ($legacy_receipt)
            return false;
        return true;
    }






}