<?php

namespace App\Repositories\Backend\Finance\Receipt;

use App\Jobs\RegisterContribution;
use App\Models\Finance\Receivable\Booking;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\BaseRepository;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Exceptions\GeneralException;
use App\Services\Receivable\CalculateInterest;
use Carbon\Carbon;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReceiptCodeRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = ReceiptCode::class;
    protected $fin_code_groups;

    public function __construct()
    {
        $this->fin_code_groups = new FinCodeGroupRepository();
    }


    public function findOrThrowException($id)
    {
        $receipt_code = $this->query()->find($id);
        if (!is_null($receipt_code)) {
            return $receipt_code;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipt_codes.not_found'));
    }

    /*Query for establishing last contrib month*/
    public function getLastContrbMonthBaseQuery($employer_id){
        return  $this->query()->select(["contrib_month"])->whereHas('receipt', function ($query) use($employer_id) {
            $query->where('employer_id', '=', $employer_id)->where('iscancelled', '=', 0);
        })->whereHas('finCode', function($query) { $query->where('fin_code_group_id','=',  $this->fin_code_groups->getContributionFinCodeGroupId());
    });
    }

    //    get Most latest contributed month by employer_id
    public function getLastContrbMonth($employer_id, $rcv_date = null) {
//
        $query = $this->getLastContrbMonthBaseQuery($employer_id);
        $last_contrib_month = null;
        if (!is_null($rcv_date)) {
            $query_before = $this->getLastContrbMonthBaseQuery($employer_id)->where("contrib_month", "<=", $rcv_date);

            if($query_before->count() > 0){
                /*exist before rcv date*/
                $last_contrib_month = $query_before->orderBy('contrib_month', 'desc')->limit(1)->first();

            }else{
                /*Before not exist take after rcv date*/
                $query_after = $this->getLastContrbMonthBaseQuery($employer_id)->where("contrib_month", ">", standard_date_format($rcv_date));
                $last_contrib_month = $query_after->orderBy('contrib_month', 'asc')->limit(1)->first();
            }
        }

//
        if ($last_contrib_month)
            return $last_contrib_month->contrib_month;
        return  0;
    }


    //    get Most recent Amount contributed by employer_id
    public function getRecentContribution($employer_id, $rcv_date = null) {
        //

        $last_contrib_month = $this->getLastContrbMonth($employer_id, $rcv_date);
        if ($last_contrib_month != 0) {
            $last_contrib_month_parse = Carbon::parse($last_contrib_month);
            $recent_contribution = $this->query()->whereHas('receipt', function
                ($query) use($employer_id) {
                    $query->where('employer_id', '=', $employer_id)->where('iscancelled', '=', 0);
                })->whereHas('finCode', function($query) { $query->where('fin_code_group_id','=',  $this->fin_code_groups->getContributionFinCodeGroupId());
            })->whereRaw("date_part('month', contrib_month) = " . $last_contrib_month_parse->format('m'))->whereRaw("date_part('year', contrib_month) = " . $last_contrib_month_parse->format('Y'))
                // ->whereMonth('contrib_month','=', $last_contrib_month_parse->format('m'))->whereYear('contrib_month','=', $last_contrib_month_parse->format('Y'))
                ->where('grouppaystatus',0)->sum('amount');
            } else {
                $recent_contribution = NULL;
            }
            if (!is_null($recent_contribution))
                return $recent_contribution;
            return  0;
        }

    //    get all active contributions for employer_id
        public function getAllContributions($employer_id)
        {
        //
            $contributions = $this->query()->whereHas('receipt', function ($query) use($employer_id)
            {
                $query->where('iscancelled','=', 0)->where('isdishonoured','=',0)->where('isverified','=', 1);
            })->whereHas('finCode', function($query) {
                $query->where('fin_code_group_id','=',  $this->fin_code_groups->getContributionFinCodeGroupId());
            })->where('employer_id','=', $employer_id)->orderBy("contrib_month", 'asc')->get();
        //
            return  $contributions;
        }

    /*
     * Get contribution Tracks for this receipt code
     */
    public function getContributionTracks($id)
    {
//
        $contribution_tracks = $this->query()->find($id)->contributionTracks->sortByDesc('id');
        return  $contribution_tracks;
    }

    /*
     * Get Total Interest Paid for booking_id
     */

    public function getTotalInterestPaidForBookingId($booking_id)
    {
//
        $total_amount= $this->query()->where('booking_id', '=', $booking_id)->whereHas('finCode', function
            ($query)    {
                $query->where('fin_code_group_id','=',
                    $this->fin_code_groups->getInterestinCodeGroupId());
            })->whereHas('receipt', function ($query)
            {
                $query->where('iscancelled','=', 0)->where('isdishonoured','=',0);
            })->sum('amount');

            return  $total_amount;
        }

    /**
     * @param $booking_id
     * @return mixed
     * Get Total interest paid for employer
     */

    public function getTotalInterestPaidForEmployer($employer_id)
    {
//
        $total_amount= $this->query()->whereHas('finCode', function
            ($query)    {
                $query->where('fin_code_group_id','=',
                    $this->fin_code_groups->getInterestinCodeGroupId());
            })->whereHas('receipt', function ($query) use($employer_id)
            {
                $query->where('iscancelled','=', 0)->where('isdishonoured','=',0)->where('employer_id', $employer_id);
            })->sum('amount');

            return  $total_amount;
        }



    /*
 * Get Total Contribution Paid for booking_id
 */

    public function getTotalContributionPaidForBookingId($booking_id)
    {
//

        $total_amount= $this->query()->where('booking_id', '=', $booking_id)->whereHas('finCode', function
            ($query)    {
                $query->where('fin_code_group_id','=', $this->fin_code_groups->getContributionFinCodeGroupId());
            })->whereHas('receipt', function ($query)
            {
                $query->where('iscancelled','=', 0)->where('isdishonoured','=',0);
            })->where('grouppaystatus', 0)->sum('amount');

            return  $total_amount;
        }




    /*
 * Get Total Interest Paid for booking_id
 */

    public function getTotalContributionPaidForEmployer($employer_id)
    {
//
        $total_amount= $this->query()->whereHas('finCode', function
            ($query)    {
                $query->where('fin_code_group_id','=', $this->fin_code_groups->getContributionFinCodeGroupId());
            })->whereHas('receipt', function ($query) use($employer_id)
            {
                $query->where('iscancelled','=', 0)->where('isdishonoured','=',0)->where('employer_id', '=', $employer_id);
            })->sum('amount');

            return  $total_amount;
        }




        public function getTotalInterestPaidForReceiptId($receipt_id)
        {
//
            $total_interest_amount= $this->query()->where('receipt_id', '=', $receipt_id)->whereHas('finCode', function
                ($query)    {
                    $query->where('fin_code_group_id','=',
                        $this->fin_code_groups->getInterestinCodeGroupId());
                })->whereHas('receipt', function ($query)
                {
                    $query->where('iscancelled','=', 0)->where('isdishonoured','=',0);
                })->sum('amount');

                return  $total_interest_amount;
            }




    /**
     * Register Contribution into live from temp (Chunking)
     *
     */

    public function registerContribution(Model $receipt_code)
    {

        DB::table('contribution_temps')->where('receipt_code_id', $receipt_code->id)->orderBy('id')->chunk(250, function
            ($contribution_temps)  use($receipt_code) {

            // Process the records...
                dispatch(new RegisterContribution($receipt_code,  access()->id(), $contribution_temps));
//            return false;
            });
    }






    /**
     * @param $booking_id
     * @return mixed
     * Workflow Approve
     */

    public function WF_Approve_Contrib($booking_id)
    {
//
        $total_amount= $this->query()->where('booking_id', '=', $booking_id)->whereHas('finCode', function
            ($query)    {
                $query->where('fin_code_group_id','=', $this->fin_code_groups->getContributionFinCodeGroupId());
            })->whereHas('receipt', function ($query)
            {
                $query->where('iscancelled','=', 0)->where('isdishonoured','=',0);
            })->sum('amount');

            return  $total_amount;
        }

        public function checkIfAllContributionsHaveLinkedFile($receipt_id){
            $receipt = $this->query()->whereHas('receipt', function ($query) use ($receipt_id) {
                $query->where('receipt_id', $receipt_id);
            })->whereNull('linked_file')->orWhere('error', 1)->first();
            if ($receipt)
                return false;
            return true;
        }

        public function totalBookingAmount($booking_id)
        {
            $amount = $this->query()->where("booking_id", $booking_id)->sum("amount");
            return $amount;
        }

        public function update(Model $receipt_code, $input)
        {
            return DB::transaction(function () use ($receipt_code, $input) {
                $date = $input['receipt_year']. '-' . str_pad($input['receipt_month'], 2, "0", STR_PAD_LEFT) . '-28';
                $receipt_code->update(['contrib_month' => $date]);
            //Update interest if any with the actual amount
                $calculateInterest = new CalculateInterest();
                $calculateInterest->createInterest($receipt_code->booking_id);
                return true;
            });
        }

    /**
     * @param $rctno
     * @param $employer_id
     * @param $contrib_month
     * @return mixed
     * @throws GeneralException
     */
    public function findIfRctnoExist($rctno, $employer_id, $contrib_month)
    {
        $month = Carbon::parse($contrib_month);
        /*$query = $this->query()->where(['rctno' => $rctno, 'employer_id' => $employer_id])->whereHas("receiptCodes", function ($query) use ($contrib_month) {
            $query->where("contrib_month", $contrib_month);
        })->first();*/

        $receiptInstance = $this->query()->select(['id'])
        // ->whereMonth("contrib_month",'=', $month->format("n"))->whereYear("contrib_month",'=', $month->format("Y"))
        ->whereRaw("date_part('month', contrib_month) = " . $month->format('n'))
        ->whereRaw("date_part('year', contrib_month) = " . $month->format('Y'))
        ->whereHas("receipt", function ($query) use ($rctno, $employer_id) {
            $query->where(['rctno' => $rctno, 'employer_id' => $employer_id]);
        });
        if (!$receiptInstance->count()) {
            $parent_id = (new EmployerRepository())->query()->select(['parent_id'])->where("id", $employer_id)->first()->parent_id;
            $receiptInstance = $this->query()->select(['id'])
            ->whereRaw("date_part('month', contrib_month) = " . $month->format('n'))
            ->whereRaw("date_part('year', contrib_month) = " . $month->format('Y'))
            // ->whereMonth("contrib_month",'=', $month->format("n"))
            // ->whereYear("contrib_month",'=', $month->format("Y"))
            ->whereHas("receipt", function ($query) use ($rctno, $parent_id) {
                $query->where(['rctno' => $rctno, 'employer_id' => $parent_id]);
            });
        }
        $receipt = $receiptInstance->first();
        return $receipt;
    }

    /**
     * @param $receipt_id
     * @param $employer_id
     * @param $contrib_month
     * @return mixed
     */
    public function checkIsDuplicateQuery($receipt_id, $employer_id, $contrib_month)
    {
        $contribDate = Carbon::parse($contrib_month);
        return $this->query()->whereHas("receipt", function ($query) use ($receipt_id, $employer_id) {
            $query->where("iscancelled", 0)->where("isdishonoured", 0);
        })->where("employer_id", $employer_id)->where("receipt_id", "<", $receipt_id)
        ->whereRaw("date_part('month', contrib_month) = " . $contribDate->format('m'))
        ->whereRaw("date_part('year', contrib_month) = " . $contribDate->format('Y'));
        // ->whereMonth("contrib_month",'=', $contribDate->format("m"))->whereYear("contrib_month",'=', $contribDate->format("Y"));
    }

    /**
     * @param $receipt_id
     * @param $employer_id
     * @param $contrib_month
     * @return mixed
     */
    public function checkHasDuplicateQuery($receipt_id, $employer_id, $contrib_month)
    {
        $contribDate = Carbon::parse($contrib_month);
        return $this->query()->whereHas("receipt", function ($query) use ($receipt_id, $employer_id) {
            $query->where("iscancelled", 0)->where("isdishonoured", 0);
        })->where("employer_id", $employer_id)->where("receipt_id", ">", $receipt_id)
        ->whereRaw("date_part('month', contrib_month) = " . $contribDate->format('m'))
        ->whereRaw("date_part('year', contrib_month) = " . $contribDate->format('Y'));
        // ->whereMonth("contrib_month",'=', $contribDate->format("m"))->whereYear("contrib_month",'=', $contribDate->format("Y"));
    }

    /**
     * @param $employer_id
     * @param $contrib_month
     * @return mixed
     */
    public function getRecentReceiptCode($employer_id, $contrib_month)
    {
        $contribDate = Carbon::parse($contrib_month);
        return $this->query()->whereHas("receipt", function ($query) use ($employer_id) {
            $query->where("iscancelled", 0)->where("isdishonoured", 0);
        })->where("employer_id", $employer_id)->where("contrib_month", "<", $contribDate->format("Y-m-d"))->orderByDesc("contrib_month")->limit(1)->first();
    }

    /**
     * @param Model $receiptCode
     * @return null
     */
    public function getDuplicates(Model $receiptCode)
    {
        switch ($receiptCode->pay_status) {
            case 1:
                //Has Duplicate
            $duplicates = $this->checkHasDuplicateQuery($receiptCode->receipt_id, $receiptCode->employer_id, $receiptCode->contrib_month)->get();
            break;
            case 2:
                //Is Duplicate
            $duplicates = $this->checkIsDuplicateQuery($receiptCode->receipt_id, $receiptCode->employer_id, $receiptCode->contrib_month)->get();
            break;
            default:
            $duplicates = NULL;
            break;
        }
        return $duplicates;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function checkExisting(array $input)
    {
        $employer_id = $input['employer_id'];
        $contrib_month = $input['contrib_month'];
        $contribMonth = Carbon::parse($contrib_month);
        return $this->query()->whereHas("receipt", function ($query) {
            $query->where("iscancelled", 0)->where("isdishonoured", 0);
        })->where("employer_id", $employer_id)
        ->whereRaw("date_part('month', contrib_month) = " . $contribMonth->format('m'))
        ->whereRaw("date_part('year', contrib_month) = " . $contribMonth->format('Y'))
        // ->whereMonth("contrib_month",'=', $contribMonth->format("m"))->whereYear("contrib_month",'=', $contribMonth->format("Y"))
        ->count();
    }


    /*Get Employer Monthly Contrib*/
    public function getEmployerMonthlyContrib($employer_id, $contrib_month)
    {
        $contrib_month_parsed = Carbon::parse($contrib_month);
        $contrib_amount= DB::table('employer_contributions')->where('employer_id', $employer_id)
        // ->whereMonth('contrib_month','=', $contrib_month_parsed->format('m'))->whereYear('contrib_month','=', $contrib_month_parsed->format('Y'))
        ->whereRaw("date_part('month', contrib_month) = " . $contrib_month_parsed->format('m'))
        ->whereRaw("date_part('year', contrib_month) = " . $contrib_month_parsed->format('Y'))
        ->sum('contrib_amount');
        return $contrib_amount;
    }

}