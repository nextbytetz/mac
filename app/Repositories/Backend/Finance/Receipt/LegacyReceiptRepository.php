<?php

namespace App\Repositories\Backend\Finance\Receipt;

use App\Events\NewWorkflow;
use App\Exceptions\GeneralException;
use App\Models\Finance\Receipt\LegacyReceipt;
use App\Models\Finance\Receipt\LegacyReceiptCode;
use App\Repositories\Backend\Finance\DishonouredChequeRepository;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Finance\LegacyDishonouredChequeRepository;
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\BaseRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LegacyReceiptRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = LegacyReceipt::class;

    /**
     * @var
     */
    protected $legacy_receipt_codes;
    protected $contributions;
    protected $fin_code_groups;
    protected $legacy_dishonoured_cheques;
    protected $payment_types;

    public function __construct()
    {
        parent::__construct();
        $this->legacy_receipt_codes = new LegacyReceiptCodeRepository();
        $this->contributions = new ContributionRepository();
        $this->fin_code_groups = new FinCodeGroupRepository();
        $this->legacy_dishonoured_cheques = new LegacyDishonouredChequeRepository();
        $this->payment_types = new PaymentTypeRepository();
    }

    //find or throw exception for receipt
    public function findOrThrowException($id)
    {
        $legacy_receipt = $this->find($id);
        if (!is_null($legacy_receipt)) {
            return $legacy_receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.not_found'));
    }

    //find receipt withTrashed
    public function findWithTrashed($id)
    {
        $legacy_receipt = $this->query()->where('id', '=', $id)
        ->withTrashed()
        ->first();

        if (!is_null($legacy_receipt)) {
            return $legacy_receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.not_found'));
    }

    public function getAllContributions($employer_id)
    {
        return $this->query()->where("employer_id", $employer_id)->where(['iscancelled' => 0, 'isdishonoured' => 0])->whereNotNull('rctno')->where(function($query){
            $query->where('contr_approval', 1)->orWhere('contr_approval', 2);
        } );
    }




    public function getContributionForDataTable()
    {
        return $this->query()->where(['iscancelled' => 0, 'isdishonoured' => 0])->where('rctno','<>', '')->whereNotNull('rctno');
    }




    /**
     * @param $id
     * @return mixed
     */
    public function getAllContributionsByReceipt($id) {

        $contributions = $this->legacy_receipt_codes->query()->whereHas('legacyReceipt', function   ($query) use($id) {
            $query->where('iscancelled','=', 0)->where('legacy_receipt_id','=', $id)->where('isdishonoured','=', 0);
        })->whereHas('finCode', function
        ($query)  {  $query->where('fin_code_group_id','=', $this->fin_code_groups->getContributionFinCodeGroupId());
    })->get()->sortBy(function($legacy_receipt_code) {
        return Carbon::parse($legacy_receipt_code->contrib_month);
    });
//

    return  $contributions;
}

    /**
     * @param $id
     * @return mixed
     */
    public function getAllInterestByReceipt($id) {
//
        $interests = $this->legacy_receipt_codes->query()->whereHas('legacyReceipt', function
            ($query)    use($id)    {
                $query->where('iscancelled','=', 0)->where('legacy_receipt_id','=', $id)->where('isdishonoured','=', 0);
            })->whereHas('finCode', function
            ($query)    {
                $query->where('fin_code_group_id','=', $this->fin_code_groups->getInterestinCodeGroupId());
            })->get()->sortBy(function($receipt_code) {
                return Carbon::parse($receipt_code->contrib_month);
            });
//
            return  $interests;
        }




        public function storeEmployerReceipt(array $input)
        {
            $employer = new EmployerRepository();
            $employer_name = $employer->find($input['employer_id'])->name;
            $is_contribution = 0;
            $legacy_receipt = DB::transaction(function () use ($input, $employer_name) {
                $amount = str_replace(",", "", $input['amount']);
                $employer_id = $input['employer_id'];
                $legacy_receipt = self::MODEL;
                $legacy_receipt = new $legacy_receipt();
                $legacy_receipt->employer_id = $input['employer_id'];
                $legacy_receipt->description = $input['description'];
                $legacy_receipt->payer = $employer_name;
                $legacy_receipt->chequeno = $input['chequeno'];
            //$receipt->rct_date = $input['receipt_year'] . '-' . str_pad($input['receipt_month'], 2, "0", STR_PAD_LEFT) . '-' . str_pad($input['receipt_date'], 2, "0", STR_PAD_LEFT);
                $legacy_receipt->rct_date = fix_form_date($input['rct_date']);
                $legacy_receipt->capture_date = fix_form_date($input['capture_date']);
                $legacy_receipt->amount = $amount;
                $legacy_receipt->currency_id = $input['currency_id'];
                $legacy_receipt->payment_type_id = $input['payment_type_id'];
                $legacy_receipt->user_id = access()->user()->id;
                $legacy_receipt->office_id = 1;
                $legacy_receipt->isverified = 0;
                $legacy_receipt->contr_approval = 0;
                $legacy_receipt->rctno = $input['rctno'];
                $legacy_receipt->islegacy = 0;
                $legacy_receipt->save();

                $legacy_receipt_code = new LegacyReceiptCodeRepository();
                $special = [];
                /* start : collect all special keys */
                foreach ($input as $key => $value) {
                    if ( strpos( $key, 'contribution_amount' ) !== false ) {
                        $this_special = substr($key, 19);
                        if (!in_array($this_special, $special, true)) {
                            $special[] = $this_special;
                        }
                    }
                }
                /* end :  collect all special keys */


                /* start : loop through special variable */
                foreach ($special as $value) {


                    if (isset($input['contribution_select'. $value])) {
                        $is_contribution = 1;
                        $contribution_month = $input['contribution_month' . $value];
                        $contribution_year = $input['contribution_year' . $value];
                        $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                    // save receipt codes
                        $amount = str_replace(",", "", $input['contribution_amount' . $value]);

                        $legacy_receipt_code = new LegacyReceiptCode([
                            'fin_code_id' => 2,
                            'amount' => $amount,
                            'member_count' => $input["member_count" . $value],
                            'contrib_month' => $date,
                        ]);
                        $legacy_receipt->legacyReceiptCodes()->save($legacy_receipt_code);

                    }
                }
                /* stop : loop through special variable */
                return $legacy_receipt;
            });
            /* start : Updating Payment Description */
            $legacy_receipt->description = $legacy_receipt->isPayForEmployer();
            $legacy_receipt->save();
            /* end : Updating Payment Description */
            if ($is_contribution = 1) {
            //Create a new workflow event.
                event(new NewWorkflow(['wf_module_group_id' => 7, 'resource_id' => $legacy_receipt->id]));
            }
            return $legacy_receipt;
        }




        public function updateReceipt($legacyReceipt, array $input)
        {
            $legacyReceipt->update([
                'rctno' => $input['rctno'],
                'amount' => str_replace(",", "", $input['amount']),
                'rct_date' => $input['rct_date'],
                'capture_date' => $input['capture_date'],
                'currency_id' => $input['currency_id'],
                'payment_type_id' => $input['payment_type_id'],
                'chequeno' =>  ($input['payment_type_id'] == 2 || $input['payment_type_id'] == 5) ?  $input['chequeno']
                : null
            ]);
        }

        public function updateMonths(Model $legacyReceipt, array $input)
        {
            DB::transaction(function () use ($input, $legacyReceipt) {
                /*update receipt info*/
                $this->updateReceipt($legacyReceipt, $input);

                $special = [];
                /* start : collect all special keys */
                foreach ($input as $key => $value) {
                    if ( strpos( $key, 'contribution_amount' ) !== false ) {
                        $this_special = substr($key, 19);
                        if (!in_array($this_special, $special, true)) {
                            $special[] = $this_special;
                        }
                    }
                }

                /* end :  collect all special keys */


                /* start : loop through special variable */
                if($input['isnew'] == 0) {
                    foreach ($special as $value) {
                        $legacy_receipt_code = $this->legacy_receipt_codes->query()->find($value);
                        if (isset($input['contribution_amount' . $value])) {
                            $amount = str_replace(",", "", $input['contribution_amount' . $value]);
                            /*date*/
                            $contribution_month = $input['contribution_month' . $value];
                            $contribution_year = $input['contribution_year' . $value];
                            $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
//                    save
                            $legacy_receipt_code->contrib_month = $date;
                            $legacy_receipt_code->amount = $amount;
                            $legacy_receipt_code->member_count = $input["member_count" . $value];
                            $legacy_receipt_code->save();

                        }

                    }
                }else{
                    /* start : loop through special variable */
                    foreach ($special as $value) {


                        if (isset($input['contribution_select'. $value])) {
                            $is_contribution = 1;
                            $contribution_month = $input['contribution_month' . $value];
                            $contribution_year = $input['contribution_year' . $value];
                            $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                        // save receipt codes
                            $amount = str_replace(",", "", $input['contribution_amount' . $value]);

                            $legacy_receipt_code = new LegacyReceiptCode([
                                'fin_code_id' => 2,
                                'amount' => $amount,
                                'member_count' => $input["member_count" . $value],
                                'contrib_month' => $date,
                            ]);
                            $legacyReceipt->legacyReceiptCodes()->save($legacy_receipt_code);

                        }
                    }
                    /*update legacy flag*/
                    $legacyReceipt->update([
                        'waslegacy' => 1,
                        'islegacy' => 0,
                        'contrib_month' => null
                    ]);


                    /* stop : loop through special variable */
                }

                /*Reset contrib month if has legacy receipt codes*/
                if($legacyReceipt->legacyReceiptCodes()->whereNotNull('legacy_receipt_codes.contrib_month')->count() > 0){
                    $legacyReceipt->update([
                        'contrib_month' => null,
                        'description' => $legacyReceipt->isPayForEmployer(),
                    ]);
                }
            });
        }


        public function updateModificationMonths(Model $legacyReceipt, array $input)
        {

            DB::transaction(function () use ($input, $legacyReceipt) {

                /*update legacy flag*/
                $legacyReceipt->update([
                    'waslegacy' => 1,
                    'islegacy' => 0,
                    'contrib_month' => null
                ]);


                /* stop : loop through special variable */

                /*Reset contrib month if has legacy receipt codes*/
                if($legacyReceipt->legacyReceiptCodes()->whereNotNull('legacy_receipt_codes.contrib_month')->count() > 0){
                    $legacyReceipt->update([
                        'contrib_month' => null,
                        'description' => $legacyReceipt->isPayForEmployer(),
                    ]);
                }
            });
        }


    /**
     * @param $id
     * @param $input
     */
    public function cancel($id, $input) {
        $legacy_receipt= $this->findOrThrowException($id);
        return DB::transaction(function () use ($id,$input,$legacy_receipt) {
            $legacy_receipt->update(['iscancelled' => 1, 'cancel_user_id' => access()->user()->id, 'cancel_reason' => $input['cancel_reason'], 'cancel_date' => Carbon::now()]);
            $legacy_receipt->delete();
            $this->contributions->deactivateLegacyContributions($id);
            $workflow = new Workflow(['wf_module_group_id' => 7, 'resource_id' => $id]);
            $workflow->wfTracksDeactivate();

        });
    }

    /**
     * @param $id
     * @param $input
     */
    public function dishonour($id, $input) {
        $legacy_receipt= $this->findOrThrowException($id);
        return DB::transaction(function () use ($id,$input,$legacy_receipt) {
            $legacy_receipt->update(['isdishonoured' => 1]);
            $this->contributions->deactivateLegacyContributions($id);
            $this->legacy_dishonoured_cheques->create($id);
        });

    }

    /**
     * @param $id
     * @param $input
     */
    public function replaceCheque($id, $input) {

        $legacy_receipt= $this->findOrThrowException($id);
        return DB::transaction(function () use ($id,$input,$legacy_receipt) {
            $legacy_receipt->update(['bank_id' => $input['bank'], 'chequeno'=> $input['new_chequeno'] ,'isdishonoured' => 0,]);
            $this->contributions->activateLegacyContributions($id);
        });
    }


    public function checkIfAllContributionsAreUploaded($id){
        $legacy_receipt = $this->query()->where('id',$id)->whereHas('legacyReceiptCodes', function ($query){
            $query->where('isuploaded', 0);
        })->first();
        if ($legacy_receipt)
            return false;
        return true;
    }

    /**
     * @param $rctno
     * @param $employer_id
     * @param $contrib_month
     * @return mixed
     * @throws GeneralException
     */
    public function FindIfRctnoExist($rctno, $employer_id, $contrib_month)
    {
        $month = Carbon::parse($contrib_month);
        //$receipt = $this->query()->whereRaw("rctno like :rctno and contr_approval <> 2  ", ['rctno' => "%$rctno%"])->first();
        $receiptInstance = $this->query()->select(['id'])->whereMonth("contrib_month",'=', $month->format("n"))->whereYear("contrib_month",'=', $month->format("Y"))->where(['rctno' => $rctno, 'employer_id' => $employer_id])->where("contr_approval", "<>", 2);
        if (!$receiptInstance->count()) {
            $parent_id = (new EmployerRepository())->query()->select(['parent_id'])->where("id", $employer_id)->first()->parent_id;
            $receiptInstance = $this->query()->select(['id'])->whereMonth("contrib_month",'=', $month->format("n"))->whereYear("contrib_month",'=', $month->format("Y"))->where(['rctno' => $rctno, 'employer_id' => $parent_id])->where("contr_approval", "<>", 2);
        }
        $receipt = $receiptInstance->first();
        return $receipt;
    }

    public function uploadManualContribution($input)
    {

    }


    /**
     * @param Model $employer
     * @return mixed
     * Latest contribution amount
     */
    public function getLatestContributionByEmployer(Model $employer)
    {
        $legacy_receipt = DB::table('employer_contributions')->where('islegacy', 1)->where('employer_id', $employer->id)->orderBy('contrib_month', 'desc')->first();

//        if(!isset($legacy_receipt)){
//            /*Retrieve from Legacy receipt when legacy receipt do not have receipt codes*/
//            $legacy_receipt = LegacyReceipt::query()->whereNotNull('contrib_month')->where('employer_id', $employer->id)->orderBy('contrib_month', 'desc')->first();
//        }
        return $legacy_receipt;
    }


    /*Update exist_in_receipt for all receipts exist in receipts table*/
    public function updateDuplicateFlagExistReceipt()
    {

        DB::statement("UPDATE legacy_receipts
            SET exist_in_receipts = 1
            FROM
            (select l.id from legacy_receipts l
            join receipts r on (l.rctno ) = cast(r.rctno as varchar(20)) and r.amount = l.amount and r.employer_id = l.employer_id and r.iscancelled = 0 and  r.deleted_at is null) as duplicates
            WHERE
            legacy_receipts.id = duplicates.id; ");

    }



}