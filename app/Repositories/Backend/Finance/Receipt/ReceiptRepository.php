<?php

namespace App\Repositories\Backend\Finance\Receipt;

use App\Jobs\Employee\LoadElectronicContribution;
use App\Jobs\Finance\ReconcileReceipt;
use App\Models\Finance\DishonouredCheque;
use App\Events\NewWorkflow;
use App\Models\Finance\Receipt\Receipt;
use App\Models\Sysdef\Code;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\DishonouredChequeRepository;
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\ThirdpartyRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use App\Services\Receivable\CalculateInterest;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Services\Receivable\CreateBooking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Services\Workflow\Workflow;
use Log;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use Exception;
use App\Exceptions\WorkflowException;

class ReceiptRepository extends BaseRepository
{
    use AttachmentHandler, FileHandler;
    /**
     * Associated Repository Model.
     */
    const MODEL = Receipt::class;

    /**
     * @var ReceiptCodeRepository
     */
    protected $receipt_codes;

    /**
     * @var ContributionRepository
     */
    protected $contributions;

    /**
     * @var FinCodeGroupRepository
     */
    protected $fin_code_groups;


    protected $dishonoured_cheques;

    protected $payment_types;

    public function __construct()
    {
        parent::__construct();
        $this->receipt_codes = new ReceiptCodeRepository();
        $this->contributions = new ContributionRepository();
        $this->fin_code_groups = new FinCodeGroupRepository();
        $this->dishonoured_cheques = new DishonouredChequeRepository();
        $this->payment_types = new PaymentTypeRepository();
    }

    //find or throwexception for receipt
    public function findOrThrowException($id)
    {
        $receipt = $this->find($id);
        if (!is_null($receipt)) {
            return $receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.not_found'));
    }


    //find receipt withTrashed
    public function findWithTrashed($id)
    {
        $receipt = $this->query()->where('id', '=', $id)
        ->withTrashed()
        ->first();

        if (!is_null($receipt)) {
            return $receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.not_found'));
    }


    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()->withTrashed();
    }

    /**
     * @return mixed
     */
    public function getActive() {
        return $this->query();
    }

    /**
     * @return mixed
     */
    public function getContributionForDataTable()
    {
        return $this->query()->withTrashed()->whereHas("receiptCodes", function($query) {
            $query->Where(['fin_code_id' => 1])->orWhere(['fin_code_id' => 2]);
        });
    }

    /**
     * @return mixed
     * Update receipt date -> payment date
     */

    public function updatePaymentDate($id, $new_payment_date)
    {
        $receipt = $this->findOrThrowException($id);
        $receipt->update(['rct_date' => $new_payment_date,
            'rct_date_old' => $receipt->rct_date
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function checkIfNotCancelled($id) {
        $receipt = $this->query()->where('id', '=', $id)
        ->where('iscancelled', '=', 0)
        ->first();
        if (!is_null($receipt)) {
            return $receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.not_found_cancelled'));
    }

    /**
     * @param $id
     * @return bool
     * @throws GeneralException
     */
    public function checkTimeLimitToCancelReceipt($id) {
        $receipt = $this->query()->where('id', '=', $id)
        ->where('iscancelled', '=', 0)
        ->first();
        $captured_date = Carbon::parse($receipt->created_at);
        $now = Carbon::now();
        $diff_days = $captured_date->diffInDays($now);
//       get sysdef cancel receipt days period limit
        $cancel_receipt_days = sysdefs()->data()->cancel_receipt_days;
// check if difference in days does not exceed the period allowed to  cancel receipr
        if ($diff_days <= $cancel_receipt_days) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.cancel_receipt_period'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function checkIfHasCheque($id) {
        $receipt = $this->query()->where('id', '=', $id)
        ->where('chequeno', '<>', null)
        ->first();
        if (!is_null($receipt)) {
            return $receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.no_cheque'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function checkIfDishonoured($id) {
        $receipt = $this->query()->where('id', '=', $id)
        ->where('isdishonoured', '=', 0)
        ->first();
        if (!is_null($receipt)) {
            return $receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.already_dishonored'));
    }

    /**
     * @param $id
     * @param $input
     */
    public function cancel($id, $input) {
        $receipt= $this->findOrThrowException($id);
        $bookings = new BookingRepository();
        $this->checkIfNotCancelled($id);
        return DB::transaction(function () use ($id,$input,$receipt, $bookings) {
            $receipt->update(['iscancelled' => 1, 'cancel_user_id' => access()->user()->id, 'cancel_reason' => $input['cancel_reason'], 'cancel_date' => Carbon::now()]);
            $this->recalculateInterest($id); //Recalculate Interest
            $receipt->delete();
            $this->contributions->deactivateContributions($id);
            $bookings->deleteWhenCancelReceipt($id);
            $workflow = new Workflow(['wf_module_group_id' => 5, 'resource_id' => $id]);
            $workflow->wfTracksDeactivate();

        });
    }



    /**
     * @param $id
     * @param $input
     */
    public function dishonour($id, $input) {
        $receipt= $this->findOrThrowException($id);
        $this->checkIfHasCheque($id);
        $this->checkIfDishonoured($id);

        return DB::transaction(function () use ($id,$input,$receipt) {
            $receipt->update(['isdishonoured' => 1]);
            $this->contributions->deactivateContributions($id);
            $dishonoured_cheque = $this->dishonoured_cheques->create($id,$input,$receipt->bank_id,$receipt->chequeno);
            $this->recalculateInterest($id); //Recalculate Interest
            //store attachment here
            $this->saveDishonourSupportingDocument($dishonoured_cheque->id);
        });

    }

    /**
     * @param $id
     * @param $input
     */
    public function replaceCheque($id, $input) {

        $receipt= $this->findOrThrowException($id);
        $this->checkIfHasCheque($id);

        return DB::transaction(function () use ($id,$input,$receipt) {
            $receipt->update(['bank_id' => $input['bank'], 'chequeno'=> $input['new_chequeno'] ,'isdishonoured' => 0,]);
            $this->contributions->activateContributions($id);
            $this->dishonoured_cheques->update($id,$input);
            $this->recalculateInterest($id); //Recalculate Interest
        });
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getAllContributionsByReceipt($id) {
        $contributions = $this->receipt_codes->query()->whereNotNull('booking_id')->whereHas('receipt', function   ($query) use($id) {
            $query->where('iscancelled','=', 0)->where('receipt_id','=', $id)->where('isdishonoured','=', 0);
        })->whereHas('finCode', function
        ($query)  {  $query->where('fin_code_group_id','=', $this->fin_code_groups->getContributionFinCodeGroupId());
    })->orderBy("contrib_month", "asc");
        /*            ->get()->sortBy(function($receipt_code) {
                    return Carbon::parse($receipt_code->contrib_month);
                });
        //*/
                return  $contributions;
            }

    /**
     * @param $id
     * @return mixed
     */
    public function getAllInterestByReceipt($id) {
//
        $interests = $this->receipt_codes->query()->whereHas('receipt', function
            ($query) use ($id) {
                $query->where('iscancelled','=', 0)->where('receipt_id','=', $id)->where('isdishonoured','=', 0);
            })->whereHas('finCode', function
            ($query)    {
                $query->where('fin_code_group_id','=', $this->fin_code_groups->getInterestinCodeGroupId());
            })->get()->sortBy(function($receipt_code) {
                return Carbon::parse($receipt_code->contrib_month);
            });
//
            return  $interests;
        }


    /**
     * @param $rctno
     * @return mixed
     * @throws GeneralException
     */
    public function checkIfExistByRctno($rctno)
    {
        $receipt = $this->query()->where('rctno', '=', $rctno)->first();
        if (!is_null($receipt)) {
            return $receipt;
        }
        throw new GeneralException(trans('exceptions.backend.finance.receipts.not_found'));
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function getByReceiptNo($input){
        $receipt = $this->checkIfExistByRctno($input['rctno']);
        $this->checkIfDishonoured($receipt->id);
        return $receipt;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function storeAdministrativeReceipt(array $input)
    {

        $receipt = DB::transaction(function () use ($input) {
            $amount = str_replace(",", "", $input['amount']);
            $receipt = self::MODEL;
            $receipt = new $receipt();
            $payer = "";
            switch ($input['category']) {
                case 0:
                    //Staff
                $staffRepo = new UserRepository();
                if ($input["source"] == 2) {
                    $userExtract = (new UserRepository())->query()->where("external_id", $input['staff_id']);
                    if ($userExtract->count()) {
                        $input['staff_id'] = $userExtract->first()->id;
                    }
                }
                $query = $staffRepo->query()->where("id", $input['staff_id'])->first();
                $payer = $query->name;
                $receipt->staff_id = $input['staff_id'];
                break;
                case 1:
                    //Third Parties
                $thirdpartyRepo = new ThirdpartyRepository();
                $query = $thirdpartyRepo->query()->where("id", $input['thirdparty_id'])->first();
                $payer = $query->name_formatted;
                $receipt->thirdparty_id = $input['thirdparty_id'];
                break;
            }
            $receipt->payer = $payer;
            $receipt->chequeno = $input['chequeno'];
            if ($input["source"] == 1) {
                $receipt->rct_date = $input['receipt_year'] . '-' . str_pad($input['receipt_month'], 2, "0", STR_PAD_LEFT) . '-' . str_pad($input['receipt_date'], 2, "0", STR_PAD_LEFT);
            } else {
                $receipt->rct_date = $input['rct_date'];
            }

            $receipt->bank_reference = $input['bank_reference'];
            $receipt->amount = $amount;
            $receipt->description = $input['description'];
            $receipt->currency_id = $input['currency_id'];
            $receipt->payment_type_id = $input['payment_type_id'];
            $receipt->fin_code_id = $input['fin_code_id'];
            $receipt->user_id = (isset($input['user_id'])) ? $input['user_id'] : access()->user()->id;
            $receipt->office_id = 1;
            $receipt->source = $input["source"];
            $receipt->bank_id = $input['bank_id'];
            $receipt->bank_code = $input['bank_code'];
            $receipt->external_rctno = (isset($input['external_rctno'])) ? $input['external_rctno'] : NULL;
            $receipt->pay_control_no = (isset($input['pay_control_no'])) ? $input['pay_control_no'] : NULL;
            $receipt->save();

            /**
             * Start : Generate receipt No Implementation 1
             */
            //$this->updateReceiptNo($receipt);
            /**
             * End : Generate receipt No Implementation 1
             */
            /**
             * Start : Generate receipt No Implementation 2
             */
            $this->updateReceiptNo($receipt);
            /**
             * End : Generate receipt No Implementation 2
             */

            //Update Payment Description
            if (is_null($receipt->description)) {
                $receipt->description = $receipt->isPayFor();
                $receipt->save();
            }

            return $receipt;
        });
        return $receipt;
    }


    public function updateMonths(Model $receipt, array $input)
    {
        DB::transaction(function () use ($input, $receipt) {
            $special = [];
            /* start : collect all special keys */
            foreach ($input as $key => $value) {
                if ( strpos( $key, 'contribution_amount' ) !== false ) {
                    $this_special = substr($key, 19);
                    if (!in_array($this_special, $special, true)) {
                        $special[] = $this_special;
                    }
                }
            }
            foreach ($input as $key => $value) {
                if ( strpos( $key, 'interest_amount' ) !== false ) {
                    $this_special = substr($key, 15);
                    if (!in_array($this_special, $special, true)) {
                        $special[] = $this_special;
                    }
                }
            }
            /* end :  collect all special keys */
            $calculateInterest = new CalculateInterest();
            $booking_interest = new BookingInterestRepository();
            $create_booking = new CreateBooking();

            /* start : loop through special variable */
            foreach ($special as $value) {
                $receipt_code = $this->receipt_codes->query()->find($value);
                if (isset($input['contribution_amount'. $value])) {
                    $amount = str_replace(",", "", $input['contribution_amount' . $value]);

                    $contribution_month = $input['contribution_month' . $value];
                    $contribution_year = $input['contribution_year' . $value];
                    $member_count = $input["member_count" . $value];
                    $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                    $booking = $create_booking->createBooking($input['employer_id'], $date, $amount, $member_count);
                    $contrib_month = $receipt_code->contrib_month;

                    $receipt_code->contrib_month = $date;
                    $receipt_code->contrib_month_orig = $contrib_month;
                    $receipt_code->amount = $amount;
                    $receipt_code->member_count = $member_count;
                    $receipt_code->booking_id = $booking->id;
                    $receipt_code->save();

                    $calculateInterest->createInterest($booking->id);
                }
                if (isset($input['interest_amount' . $value])) {
                    $amount = str_replace(",", "", $input['interest_amount' . $value]);
                    $receipt_code->amount = $amount;
                    $receipt_code->save();
                    $interest = $booking_interest->query()->where('booking_id', $receipt_code->booking_id)->first();
                    //Update Interest Paid Attribute = 1 if all amount is paid
                    if ($interest) {

                        $bookingInterest = new BookingInterestRepository();
                        $bookingInterest->recheckPaidStatus($receipt_code->booking_id);

                        /*$dueAmount = $calculateInterest->dueAmount(Carbon::parse($receipt_code->contrib_month)->format("n"), Carbon::parse($receipt_code->contrib_month)->format("Y"), $receipt_code->receipt->employer_id);
                        if ($dueAmount == 0) {
                            $interest->ispaid = 1;
                            $interest->save();
                        }*/

                    } else {
                        $calculateInterest->createInterest($receipt_code->booking_id);
                    }
                }
            }

            //==> Update Payment Description
            //Update Payment Description
            $receipt->description = $receipt->isPayForEmployer();
            $receipt->save();
            /* stop : loop through special variable */

            $contributionQuery = $receipt->receiptCodes()->where("receipt_codes.fin_code_id", 2);

            if ($contributionQuery->count()) {
                ///==> Check if this receipt has advance payment
                $maxDate = $contributionQuery->max("contrib_month");
                $thisMonth = Carbon::now()->format("Y-m-d");
                $wfModuleRepo = new WfModuleRepository();
                if (months_diff($thisMonth, $maxDate) > 0) {
                    //$this has advance payment
                    $group = $wfModuleRepo->receiptVerificationWithAdvanceType()['group'];
                    $type = $wfModuleRepo->receiptVerificationWithAdvanceType()['type'];
                } else {
                    //no advance payment
                    $group = $wfModuleRepo->receiptVerificationType()['group'];
                    $type = $wfModuleRepo->receiptVerificationType()['type'];
                }

                ///Update the workflow module by transferring the existing to the correct ones .....

                $module = $wfModuleRepo->getModule(['wf_module_group_id' => $group, 'type' => $type]);
                $from_workflow_module = $receipt->wf_module_id;
                (new WfTrackRepository())->transferWorkflowType($from_workflow_module, $group, $type, $receipt->id);
                $receipt->wf_module_id = $module;
                $receipt->save();

            }

            //Reconcile Receipt for contribution months (Reconcile Direct), Reverse Reconciliation, for re-checking
            //dispatch(new ReconcileReceipt($receipt));
            $this->reconcile($receipt, 0);

        });
}




public function updateModificationMonths(Model $receipt, array $input)
{
    DB::transaction(function () use ($input, $receipt) {
        $special = [];
        /* start : collect all special keys */
        foreach ($input as $key => $value) {
            if ( strpos( $key, 'contribution_amount' ) !== false ) {
                $this_special = substr($key, 19);
                if (!in_array($this_special, $special, true)) {
                    $special[] = $this_special;
                }
            }
        }
        foreach ($input as $key => $value) {
            if ( strpos( $key, 'interest_amount' ) !== false ) {
                $this_special = substr($key, 15);
                if (!in_array($this_special, $special, true)) {
                    $special[] = $this_special;
                }
            }
        }
        /* end :  collect all special keys */
        $calculateInterest = new CalculateInterest();
        $booking_interest = new BookingInterestRepository();
        $create_booking = new CreateBooking();

        /* start : loop through special variable */
        foreach ($special as $value) {
            $receipt_code = $this->receipt_codes->query()->find($value);
            if (isset($input['contribution_amount'. $value])) {
                $amount = str_replace(",", "", $input['contribution_amount' . $value]);

                $contribution_month = $input['contribution_month' . $value];
                $contribution_year = $input['contribution_year' . $value];
                $member_count = $input["member_count" . $value];
                $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                $booking = $create_booking->createBooking($input['employer_id'], $date, $amount, $member_count);
                $contrib_month = $receipt_code->contrib_month;

                $receipt_code->contrib_month = $date;
                $receipt_code->contrib_month_orig = $contrib_month;
                $receipt_code->amount = $amount;
                $receipt_code->member_count = $member_count;
                $receipt_code->booking_id = $booking->id;
                $receipt_code->save();

                $calculateInterest->createInterest($booking->id);
            }
            if (isset($input['interest_amount' . $value])) {
                $amount = str_replace(",", "", $input['interest_amount' . $value]);
                $contribution_month = $input['contribution_month' . $value];
                $contribution_year = $input['contribution_year' . $value];
                $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';

                $booking = $create_booking->checkIfExist($input['employer_id'], $date);
                if (empty($booking->id)) {
                    $this->createSingleInterest($receipt, $input['employer_id'], $date, $amount);
                }else{
                    $receipt_code->booking_id = $booking->id;  
                }
                $receipt_code->member_count = $input["member_count" . $value];
                $receipt_code->amount = $amount;
                $receipt_code->save();
                $interest = $booking_interest->query()->where('booking_id', $receipt_code->booking_id)->first();
                    //Update Interest Paid Attribute = 1 if all amount is paid
                if ($interest) {

                    $bookingInterest = new BookingInterestRepository();
                    $bookingInterest->recheckPaidStatus($receipt_code->booking_id);

                        /*$dueAmount = $calculateInterest->dueAmount(Carbon::parse($receipt_code->contrib_month)->format("n"), Carbon::parse($receipt_code->contrib_month)->format("Y"), $receipt_code->receipt->employer_id);
                        if ($dueAmount == 0) {
                            $interest->ispaid = 1;
                            $interest->save();
                        }*/

                    } else {
                        $calculateInterest->createInterest($receipt_code->booking_id);
                    }
                }
            }

            //==> Update Payment Description
            //Update Payment Description
            $receipt->description = $receipt->isPayForEmployer();
            $receipt->save();
            /* stop : loop through special variable */

            $contributionQuery = $receipt->receiptCodes()->where("receipt_codes.fin_code_id", 2);

            if ($contributionQuery->count()) {
                ///==> Check if this receipt has advance payment
                $maxDate = $contributionQuery->max("contrib_month");
                $thisMonth = Carbon::now()->format("Y-m-d");
                $wfModuleRepo = new WfModuleRepository();
                if (months_diff($thisMonth, $maxDate) > 0) {
                    //$this has advance payment
                    $group = $wfModuleRepo->receiptVerificationWithAdvanceType()['group'];
                    $type = $wfModuleRepo->receiptVerificationWithAdvanceType()['type'];
                } else {
                    //no advance payment
                    $group = $wfModuleRepo->receiptVerificationType()['group'];
                    $type = $wfModuleRepo->receiptVerificationType()['type'];
                }

                ///Update the workflow module by transferring the existing to the correct ones .....

                $module = $wfModuleRepo->getModule(['wf_module_group_id' => $group, 'type' => $type]);
                $from_workflow_module = $receipt->wf_module_id;
                (new WfTrackRepository())->transferWorkflowType($from_workflow_module, $group, $type, $receipt->id);
                $receipt->wf_module_id = $module;
                $receipt->save();

            }

            //Reconcile Receipt for contribution months (Reconcile Direct), Reverse Reconciliation, for re-checking
            //dispatch(new ReconcileReceipt($receipt));
            $this->reconcile($receipt, 0);

        });
}

public function validateEmployerReceipt(array $input)
{
    $query = $this->query()->where(function ($query) use ($input) {
        $query->where("rct_date", fix_form_date($input['rct_date']))->where(["employer_id" => $input['employer_id'], "iscancelled" => 0]);
    });
    return $this->dbValidator($query, ['chequeno' => $input['chequeno']]);
}

    /**
     * @param array $input
     * @return mixed
     */
    public function storeElectronicAdministrative(array $input)
    {
        $receipt = DB::transaction(function () use ($input) {
            $receipt = $this->storeAdministrativeReceipt($input);
            return $receipt;
        });
        return $receipt;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function storeElectronic(array $input)
    {
        try {
            $employerRepo = new EmployerRepository();
            $pay_control_no = trim($input['pay_control_no']);
            $employer = $employerRepo->query()->where("id", $input['employer_id'])->withTrashed()->first();
            if (!$employer) {
                $message = "Employer for receipt with control number {$pay_control_no} not found";
                $data = ['success' => false, 'id' => '', 'message' => $message];
            } else {
                $employer_name = $employer->name;
                $is_contribution = 0;

                $receipt = DB::transaction(function () use ($input, $employer_name, $pay_control_no) {

                    //Check if control number exists in receipt then return the existing receipt.
                    $query = $this->query()->where("pay_control_no", $pay_control_no)->first();
                    if ($query) {
                        return $query;
                    }

                    $amount = str_replace(",", "", $input['amount']);
                    $employer_id = $input['employer_id'];
                    $receipt = self::MODEL;
                    $receipt = new $receipt();
                    $receipt->employer_id = $input['employer_id'];
                    $receipt->amount = $amount;
                    $receipt->payer = $employer_name;
                    $receipt->rct_date = fix_form_date($input['rct_date']);
                    $receipt->bank_id = $input['bank_id'];
                    $receipt->bank_code=$input['bank_code'];
                    $receipt->payment_type_id = $input['payment_type_id'];
                    $receipt->currency_id = $input['currency_id'];
                    $receipt->source = $input['source'];
                    $receipt->office_id = 1;
                    $receipt->user_id = 13;
                    $receipt->external_rctno = $input['external_rctno'];
                    $receipt->pay_control_no = $pay_control_no;
                    $receipt->payroll_id = $input['payroll_id'];
                    $receipt->isverified = 1;
                    $receipt->save();

                    $billMonthRepo = new BillMonthRepository();
                    $billMonths = $billMonthRepo->query()->where("bill_id", $input['bill_id'])->get();

                    foreach ($billMonths as $billMonth) {
                        if ($billMonth->fin_code_id == 2) {
                            //Monthly contribution
                            $is_contribution = 1;
                            $this->createSingleContribution($receipt, $input['employer_id'], $billMonth->contrib_month, $billMonth->amount, $billMonth->member_count);
                        }
                        if ($billMonth->fin_code_id == 1) {
                            //Interest on monthly contribution
                            $this->createSingleInterest($receipt, $input['employer_id'], $billMonth->contrib_month, $billMonth->amount);
                        }
                        $is_contribution = 0;
                    }

                    /**
                     * Start : Generate receipt No Implementation 2
                     */
                    $this->updateReceiptNo($receipt);

                    //Update Payment Description
                    $receipt->description = $receipt->isPayForEmployer();
                    $receipt->save();

                    //Reconcile Receipt
                    //$this->reconcile($receipt);
                    dispatch(new ReconcileReceipt($receipt));

                    return $receipt;

                });
                $data = ['success' => true, 'id' => $receipt->id, 'message' => 'SUCCESS'];
            }
        } catch (Exception $e) {
            $data = ['success' => false, 'id' => '', 'message' => $e->getMessage()];
        } catch (\Throwable $e) {
            $data = ['success' => false, 'id' => '', 'message' => $e->getMessage()];
        }

        return $data;
    }

    public function createSingleContribution($receipt, $employer_id, $date, $amount, $member_count)
    {
        $calculateInterest = new CalculateInterest();
        $create_booking = new CreateBooking();
        $check_if_month_bookable = (new BookingRepository())->checkIfContribMonthIsBookable($date);//Check if month is bookable (No booking for advance months)

        $booking = ($check_if_month_bookable) ? ($create_booking->createBooking($employer_id, $date, $amount, $member_count)) : null;
        $receipt_code = new ReceiptCode([
            'fin_code_id' => 2,
            'amount' => $amount,
            'member_count' => $member_count,
            'contrib_month' => $date,
            'booking_id' => (isset($booking)) ? $booking->id : null,
            'employer_id' => $employer_id,
        ]);
        $receipt->receiptCodes()->save($receipt_code);
        //Update interest if any with the actual amount
        if($check_if_month_bookable){
            $calculateInterest->createInterest($booking->id);
        }

    }

    public function createSingleInterest($receipt, $employer_id, $date, $amount)
    {
        $create_booking = new CreateBooking();
        $calculateInterest = new CalculateInterest();

        /*if ($is_contribution != 1) {
            $booking = $create_booking->createBooking($employer_id, $date);
        }*/
        $check_if_month_bookable = (new BookingRepository())->checkIfContribMonthIsBookable($date);//Check if month is bookable (No booking for advance months)

        $booking = ($check_if_month_bookable) ? ($create_booking->createBooking($employer_id, $date)) : null;


        $receipt_code = new ReceiptCode([
            'fin_code_id' => 1,
            'amount' => $amount,
            'contrib_month' => ($booking)  ? $booking->rcv_date : $date,
            'booking_id' =>($booking)  ?  $booking->id : null,
            'employer_id' => $employer_id,
        ]);
        $receipt->receiptCodes()->save($receipt_code);

        /*Check if month i bookable to create interest*/
        if($check_if_month_bookable){
            $booking_interest = new BookingInterestRepository();
            $interest = $booking_interest->query()->where('booking_id', $booking->id)->first();
            if ($interest) {

                $bookingInterest = new BookingInterestRepository();
                $bookingInterest->recheckPaidStatus($booking->id);

                //$interest->amount = $interest->amount - $amount;
                //Update Interest Paid Attribute = 1 if all amount is paid

                /*$dateCarbon = Carbon::parse($date);
                $dueAmount = $calculateInterest->dueAmount($dateCarbon->format("m"), $dateCarbon->format("Y"), $employer_id);
                if ($dueAmount == 0) {
                    $interest->ispaid = 1;
                    $interest->save();
                }*/

            } else {
                $calculateInterest->createInterest($booking->id);
            }
        }

    }

    /**
     * @param array $input
     * @return mixed
     * @throws GeneralException
     */
    public function storeEmployerReceipt(array $input)
    {
        $employer = new EmployerRepository();
        $employer_name = $employer->find($input['employer_id'])->name;
        $receipt = DB::transaction(function () use ($input, $employer_name) {
            $is_contribution = 0;
            $has_contribution = 0;
            $amount = str_replace(",", "", $input['amount']);
            $employer_id = $input['employer_id'];
            $receipt = self::MODEL;
            $receipt = new $receipt();
            $receipt->employer_id = $input['employer_id'];
            $receipt->description = $input['description'];
            $receipt->payer = $employer_name;
            $receipt->chequeno = $input['chequeno'];
            //$receipt->rct_date = $input['receipt_year'] . '-' . str_pad($input['receipt_month'], 2, "0", STR_PAD_LEFT) . '-' . str_pad($input['receipt_date'], 2, "0", STR_PAD_LEFT);
            $receipt->rct_date = fix_form_date($input['rct_date']);
            $receipt->bank_reference = $input['bank_reference'];
            $receipt->amount = $amount;
            $receipt->currency_id = $input['currency_id'];
            $receipt->payment_type_id = $input['payment_type_id'];
            $receipt->user_id = access()->user()->id;
            $receipt->office_id = 1;
            $receipt->bank_id = $input['bank_id'];
            $receipt->fin_code_id = $input['fin_code_id'];
            $receipt->bank_code = $input['fin_code_id'];
            $receipt->payroll_id = $input['payroll_id'] ?? 1;

            //---End of checking payroll_id from request.
            $receipt->save();

            $receipt_code = new ReceiptCodeRepository();
            $special = [];
            /* start : collect all special keys */
            foreach ($input as $key => $value) {
                if ( strpos( $key, 'contribution_amount' ) !== false ) {
                    $this_special = substr($key, 19);
                    if (!in_array($this_special, $special, true)) {
                        $special[] = $this_special;
                    }
                }
            }
            foreach ($input as $key => $value) {
                if ( strpos( $key, 'interest_amount' ) !== false ) {
                    $this_special = substr($key, 15);
                    if (!in_array($this_special, $special, true)) {
                        $special[] = $this_special;
                    }
                }
            }
            /* end :  collect all special keys */

            $create_booking = new CreateBooking();

            /* start : loop through special variable */
            foreach ($special as $value) {
                /*                if (isset($input['contribution_select'. $value]) Or isset($input['interest_select' . $value])) {
                                    $contribution_month = $input['contribution_month' . $value];
                                    $contribution_year = $input['contribution_year' . $value];
                                    $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                                }*/
                                $calculateInterest = new CalculateInterest();

                                if (isset($input['contribution_select'. $value])) {
                                    $is_contribution = 1;
                                    $has_contribution = 1;
                                    $contribution_month = $input['contribution_month' . $value];
                                    $contribution_year = $input['contribution_year' . $value];
                                    $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                    // save receipt codes
                                    $amount = str_replace(",", "", $input['contribution_amount' . $value]);
                                    $this->createSingleContribution($receipt, $input['employer_id'], $date, $amount, $input["member_count" . $value]);
                                }

                                if (isset($input['interest_select' . $value])) {
                                    $contribution_month = $input['contribution_month' . $value];
                                    $contribution_year = $input['contribution_year' . $value];
                                    $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                    // save interests and keep status of booking interest payments
                                    $amount = str_replace(",", "", $input['interest_amount' . $value]);
                                    $this->createSingleInterest($receipt, $employer_id, $date, $amount);
                                }
                                $is_contribution = 0;
                            }
                            /* stop : loop through special variable */
                            if ($has_contribution == 1) {
                ///==> Check if this receipt has advance payment
                                $maxDate = $receipt->receiptCodes()->where("receipt_codes.fin_code_id", 2)->max("contrib_month");
                                $thisMonth = Carbon::now()->format("Y-m-d");
                                $wfModuleRepo = new WfModuleRepository();
                                if (months_diff($thisMonth, $maxDate) > 0) {
                    //$this has advance payment
                                    $group = $wfModuleRepo->receiptVerificationWithAdvanceType()['group'];
                                    $type = $wfModuleRepo->receiptVerificationWithAdvanceType()['type'];
                                } else {
                    //no advance payment
                                    $group = $wfModuleRepo->receiptVerificationType()['group'];
                                    $type = $wfModuleRepo->receiptVerificationType()['type'];
                                }
                                $module = $wfModuleRepo->getModule(['wf_module_group_id' => $group, 'type' => $type]);
                                $receipt->wf_module_id = $module;
                                $receipt->save();

                //Create a new workflow event.
                //event(new NewWorkflow(['wf_module_group_id' => 5, 'resource_id' => $receipt->id]));
                                event(new NewWorkflow(['wf_module_group_id' => $group, 'resource_id' => $receipt->id, 'type' => $type]));
                            }
            /**
             * Start : Generate receipt No Implementation 1
             */
            //$this->updateReceiptNo($receipt);
            /**
             * End : Generate receipt No Implementation 1
             */
            /**
             * Start : Generate receipt No Implementation 2
             */
            $this->updateReceiptNo($receipt);
            /**
             * End : Generate receipt No Implementation 2
             */

            //Reconcile Receipt
            //$this->reconcile($receipt);
            dispatch(new ReconcileReceipt($receipt));

            return $receipt;
        });

        //Update Payment Description
$receipt->description = $receipt->isPayForEmployer();
$receipt->save();

return $receipt;
}

private function updateReceiptNo(Model $receipt)
{
        //$next_rctno = $this->getNextRctno();
    $next_rctno = $this->getNextRctnoSysdef();
    $receipt->rctno = $next_rctno;
    $receipt->rctno_old = $next_rctno;
    $receipt->save();
}

    /**
     * @param Model $receipt
     * @param $state 1 - Reconcile, 0 - Reverse Reconciliation
     * @return mixed
     * @description reconcile receipt to check whether the contribution month has been paid multiple times, and the member count is lower than the most recent month
     */
    public function reconcile(Model $receipt, $state = 1)
    {
        return DB::transaction(function() use ($receipt, $state) {
            $receiptCodeRepo = new ReceiptCodeRepository();
            switch ($state) {
                case 0:
                    //Reverse Reconciliation
                $receipt->hasreconciled = 0;
                $receipt->save();
                $receipt->receiptCodes()->update([
                    'pay_status' => 0,
                    'hasmemberdiff' => 3,
                ]);
                break;
                case 1:
                    //Reconcile
                $receipt_codes = $receipt->receiptCodes;
                foreach ($receipt_codes as $receipt_code) {
                    $contrib_month = $receipt_code->contrib_month;
                    $employer_id = $receipt->employer_id;
                    $receipt_id = $receipt->id;
                        ///==>check early same contribution month, if return true skip the rest check and update isduplicate = 1
                    $isduplicateCount = $receiptCodeRepo->checkIsDuplicateQuery($receipt_id, $employer_id, $contrib_month)->count();
                    if ($isduplicateCount) {
                        $receipt_code->pay_status = 2;
                        $receipt_code->save();
                    } else {
                            ///==>check later same contribution month
                        $hasduplicateCount = $receiptCodeRepo->checkHasDuplicateQuery($receipt_id, $employer_id, $contrib_month)->count();
                        if ($hasduplicateCount) {
                            $receipt_code->pay_status = 1;
                            $receipt_code->save();
                        } else {
                            $receipt_code->pay_status = 3;
                            $receipt_code->save();
                        }
                    }
                        ///Check for member count
                    $prevReceiptCode = $receiptCodeRepo->getRecentReceiptCode($employer_id, $contrib_month);
                    if ($prevReceiptCode) {
                        switch (true) {
                            case ($prevReceiptCode->member_count > $receipt_code->member_count):
                                    //Number lowered --> 1
                            $hasmemberdiff = 1;
                            break;
                            case ($prevReceiptCode->member_count < $receipt_code->member_count):
                                    //Number Increased --> 2
                            $hasmemberdiff = 2;
                            break;
                            default:
                                    //No change --> 0
                            $hasmemberdiff = 0;
                            break;
                        }
                    } else {
                            //First Entry --> 4
                        $hasmemberdiff = 4;
                    }
                    $receipt_code->hasmemberdiff = $hasmemberdiff;
                    $receipt_code->save();
                }
                $receipt->hasreconciled = 1;
                $receipt->save();
                break;
            }
            return true;
        });
    }

    /**
     * @param $id
     * @return mixed
     * Update iscomplete status if all contributions are uploaded
     * @deprecated 13/06/2017
     */
    public function updateIfCompleteLevel3($id){
        $receipt = $this->findOrThrowException($id);
        return DB::transaction(function () use ($id,$receipt) {
            if (($this->checkIfAllContributionsAreUploaded($id)) == 1) {
                $receipt->update(['iscomplete' => 1]);
                $input = array("resource_id" => $id, "table_name" => "receipts", "sign" => 1,);
                $input_update = array("user_id" => access()->user()->id, "status" => 1, "comments" => "Approved", "assigned" => 1, "forward_date" => Carbon::now());
                $workflow = new Workflow(['wf_module_group_id' => 5, 'resource_id' => $id]);
                $workflow->wfApprove($input, $input_update);
            }
            return $receipt;
        });
    }

    public function checkIfAllContributionsAreUploaded($id){
        $receipt = $this->query()->where('id',$id)->whereHas('receiptCodes', function ($query){
            $query->where('isuploaded', 0);
        })->first();
        if ($receipt)
            return false;
        return true;
    }

    /**
     * @param $id
     * @return mixed
     * Wf Approve when all contributions have linked file all uploaded with no errors
     * @deprecated 13/06/2017
     */
    public function updateIfCompleteLevel2($id) {
        $receipt = $this->findOrThrowException($id);
        return DB::transaction(function () use ($id,$receipt) {
            if (($this->receipt_codes->checkIfAllContributionsHaveLinkedFile($id)) == 1) {
                $input = array("resource_id" => $id, "table_name" => "receipts", "sign" => 1,);
                $input_update = array("user_id" => access()->user()->id, "status" => 1, "comments" => "Approved", "assigned" => 1, "forward_date" => Carbon::now());
                $workflow = new Workflow(['wf_module_group_id' => 5, 'resource_id' => $id]);
                $workflow->wfApprove($input, $input_update);
            }
            return $receipt;
        });
    }

    /**
     * @return int
     *
     */
    public function getLastRctno() {
        $max_id = $this->query()->withTrashed()->max('id');
        $rct = $this->query()->withTrashed()->where("id", "<=", $max_id)->whereNotNull('rctno')->orderBy("id", "desc")->limit(1)->first();
        /* $max_id -= 1;
        $rct = $this->query()->withTrashed()->where('id', $max_id)->first(); */
        $last_rctno = ($rct) ? $rct->rctno : 0;
        return $last_rctno;
    }

    /**
     * @return string
     *
     */
    public function getNextRctno() {
        $lastno = $this->getLastRctno();
        if ($lastno != 0) {
            $lastno = substr($lastno, 1);
        }
        $next_rctno_with_no_checksum = $lastno + 1;
        $next_rctno = checksum($next_rctno_with_no_checksum, sysdefs()->data()->receipt_number_length);
        return $next_rctno;
    }

    public function getNextRctnoSysdef()
    {
        $sysdef = sysdefs()->data();
        $nextRctno = $sysdef->rctno;
        $sysdef->rctno = $nextRctno + 1;
        $sysdef->save();
        $nextRctno = checksum($nextRctno, $sysdef->receipt_number_length);
        return $nextRctno;
    }

    /**
     * REPORTS METHOD
     */

    /**
     * @return mixed
     * get amount collected over date range
     */
    public function getAmountCollectedDateRange($input)
    {
        $from_date = $input['from_date'];
        $to_date = $input['to_date'];
        $amount_collected = 0;
        if (!is_null($from_date) && !is_null($from_date) ){
            $query = $this->query()->where('isdishonoured', 0)->where('created_at', '>=', $from_date. ' 00:00:00' )->where('created_at', '<=', $to_date . ' 23:59:59');
            //if bank is selected
            if (isset($input['bank_id']) And ($input['bank_id'])){
                $query = $query->where('bank_id',$input['bank_id']);
            };
            $amount_collected = $query->sum('amount');
        }
        return number_format( $amount_collected , 2 , '.' , ',' );
    }

    /**
     * @return mixed
     * get no of receipts receipted over period date range
     */
    public function countReceiptsDateRange($input)
    {
        $from_date = $input['from_date'];
        $to_date = $input['to_date'];
        $no_of_receipts = 0;
        if (!is_null($from_date) && !is_null($from_date) ){
            $query = $this->query()->where('created_at', '>=', $from_date. ' 00:00:00' )->where('created_at', '<=', $to_date . ' 23:59:59');
            //if bank is selected
            if (isset($input['bank_id']) And ($input['bank_id'])){
                $query = $query->where('bank_id',$input['bank_id']);
            };
            $no_of_receipts= $query->count();

        }
        return $no_of_receipts;
    }

    /**
     * @return mixed
     * get no of receipts cancelled over period date range
     */
    public function countCancelledReceiptsDateRange($input)
    {
        $from_date = $input['from_date'];
        $to_date = $input['to_date'];
        $no_of_receipts = 0;
        if (!is_null($from_date) && !is_null($from_date) ){
//
            $query = $this->query()->onlyTrashed()->where('rct_date', '>=', $from_date)->where('rct_date', '<=', $to_date );
            //if bank is selected
            if (isset($input['bank_id']) And ($input['bank_id'])){
                $query = $query->where('bank_id',$input['bank_id']);
            };
            $no_of_receipts= $query->count();
        }
        return $no_of_receipts;
    }

    /**
     * @return mixed
     * get no of receipts cancelled over period date range
     */
    public function countDishonouredChequesDateRange($input)
    {
        $dishonoured_cheques = new DishonouredChequeRepository();
        $from_date = $input['from_date'];
        $to_date = $input['to_date'];
        $no_of_receipts = 0;
        if (!is_null($from_date) && !is_null($from_date) ){
            $query = $dishonoured_cheques->getQuery()->where('created_at', '>=', $from_date. ' 00:00:00' )->where('created_at', '<=', $to_date . ' 23:59:59');
            //if bank is selected
            if (isset($input['bank_id']) And ($input['bank_id'])){
                $query = $query->where('old_bank_id',$input['bank_id']);
            };
            $no_of_receipts= $query->count();

        }
        return $no_of_receipts;
    }



    /**
     * @param $id
     * Recalculate booking interest
     */
    public function recalculateInterest($id){

//        Check if receipt is for Receivable (Contribution and Interest)
        $receipt= $this->findOrThrowException($id);
        if ($receipt->receiptCodes()->count()){

            foreach($receipt->receiptCodes as  $receiptCode){
                if($receiptCode->booking()->count() > 0){
                    $booking = $receiptCode->booking;
                    //            Check if has interest
                    if ($booking->bookingInterest()->count()){
                        $calculate_interest = new CalculateInterest();
                        $calculate_interest->createInterest($booking->id);

                    }
                }

            }

        }
    }


    /**
     * Get payment type with checkno
     *
     * @param $id
     * @return string
     */
    public function getPaymentTypeWithChequeno($id)
    {
        $receipt= $this->findWithTrashed($id);
        return ($receipt->payment_type_id == $this->payment_types->chequePayment()) ? $receipt->payment_type->name . '-' . $receipt->chequeno : $receipt->payment_type->name ;
    }

    /* start: Reports based on time Period, providing details on a monthly bases */

    public function getTotalMonthlyCollection()
    {
        $data = [];
        $data_table = [];

        $electronic = DB::table('main.fin_monthly_collection_electronic')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $manual = DB::table('main.fin_monthly_collection_manual')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $sum_legacy = 0;
        $sum_after_electronic = 0;
        $x = 0;
        foreach ($electronic as $value) {
            $sum = $value->amount + (isset($manual[$x]->amount) ? $manual[$x]->amount : 0);
            $sum_after_electronic += $value->amount;
            $sum_legacy += (isset($manual[$x]->amount) ? $manual[$x]->amount : 0);
            $period = $value->period;
            $data[] = [$period, (int) $sum];
            $data_table[] = [$period, $sum];
            $x++;
        }

        $overall_total = $sum_after_electronic + $sum_legacy;
        $return['monthly_collection_table'] = $data_table;
        $return['collection_overall_total'] = ($overall_total) ;
        $return['graph_monthly_collection'] = $data;
        return $return;
    }

    public function getTotalMonthlyContribution()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $sum = $this->query()
            ->where('iscancelled',0)
            ->where('isdishonoured',0)
            // ->whereMonth("rct_date", "=",  $month)
            // ->whereYear("rct_date", "=", $year)
            ->whereRaw("date_part('month', rct_date) = " . $month)
            ->whereRaw("date_part('year', rct_date) = " . $year)
            ->whereHas("receiptCodes", function ($query) {
                $query->whereRaw("fin_code_id = 2");
            })
            ->sum("amount");
            $period = $start->format("M y");
            $data = array_merge($data, [(int) $sum]); //$period . " : " . $sum
            $start->addMonth();
        }
        $sum = $this->query()->whereRaw("rct_date between :start_date and :end_date and iscancelled = 0 and isdishonoured = 0", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])
        ->whereHas("receiptCodes", function ($query) {
            $query->whereRaw("fin_code_id = 2")->where('grouppaystatus', 0);
        })
        ->sum("amount");
        $return['contribution'] = implode("," , $data);
        $return['contribution_total'] =  $sum ;
        return $return;
    }

    public function getTotalMonthlyInterest()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $sum = $this->query()
            ->where('iscancelled',0)
            ->where('isdishonoured',0)
            // ->whereMonth("rct_date", "=",  $month)
            // ->whereYear("rct_date", "=", $year)
            ->whereRaw("date_part('month', rct_date) = " . $month)
            ->whereRaw("date_part('year', rct_date) = " . $year)
            ->whereHas("receiptCodes", function ($query) {
                $query->whereRaw("fin_code_id = 1");
            })
            ->sum("amount");
            $period = $start->format("M y");
            $data = array_merge($data, [(int) $sum]); //$period . " : " . $sum
            $start->addMonth();
        }
        $sum = $this->query()->whereRaw("rct_date between :start_date and :end_date and iscancelled = 0 and isdishonoured = 0", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])
        ->whereHas("receiptCodes", function ($query) {
            $query->whereRaw("fin_code_id = 1");
        })
        ->sum("amount");
        $return['interest'] = implode("," , $data);
        $return['interest_total'] =  $sum ;
        return $return;
    }

    public function getTotalMonthlyReceipts()
    {
        $data = [];

        $receipt = DB::table('main.fin_monthly_receipt')->select(["period", "total"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $count = 0;
        foreach ($receipt as $value) {
            $period = $value->period;
            $count += $value->total;
            $data = array_merge($data, [(int) $value->total]);
        }

        $return['receipts'] = implode("," , $data);
        $return['receipts_total'] =  $count ;
        return $return;
    }

    public function getTotalMonthlyCancelled()
    {
        $data = [];

        $receipt = DB::table('main.fin_monthly_cancelled')->select(["period", "total"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $count = 0;
        foreach ($receipt as $value) {
            $period = $value->period;
            $count += $value->total;
            $data = array_merge($data, [(int) $value->total]);
        }

        $return['cancelled'] = implode("," , $data);
        $return['cancelled_total'] =  $count ;
        return $return;
    }

    public function getTotalMonthlyDishonoured()
    {
        $data = [];

        $receipt = DB::table('main.fin_monthly_dishonoured')->select(["period", "total"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $count = 0;
        foreach ($receipt as $value) {
            $period = $value->period;
            $count += $value->total;
            $data = array_merge($data, [(int) $value->total]);
        }

        $return['dishonoured'] = implode("," , $data);
        $return['dishonoured_total'] =  $count ;
        return $return;
    }

    public function getTotalMonthlyEmployers()
    {
        $data = [];

        /* Get Total before new Financial Year - Open Count*/
        $data_table = [];

        $count_after_electronic_open = DB::table('main.comp_yearly_contributing_employer_electronic')->select(["employers"])->where("fin_year_id", fin_year()->prevFinYearID())->first();
        $count_after_electronic_open = $count_after_electronic_open->employers;

        $count_legacy_open = DB::table('main.comp_yearly_contributing_employer_manual')->select(["employers"])->where("fin_year_id", fin_year()->prevFinYearID())->first();
        $count_legacy_open = ($count_legacy_open) ? $count_legacy_open->employers : 0;

        $total_open = $count_after_electronic_open +  $count_legacy_open;
        $period_open = fin_year()->prevYear();
        $data_table[] = [$period_open, (int) $total_open];
        /* end open */

        $electronic = DB::table('main.comp_monthly_contributing_employer_electronic')->select(["period", "employers"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $manual = DB::table('main.comp_monthly_contributing_employer_electronic')->select(["period", "employers"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $total  = 0;
        $x = 0;
        foreach ($electronic as $value) {
            $count = $value->employers + (isset($manual[$x]->employers) ? $manual[$x]->employers : 0);
            $total = $count + $total;
            $period = $value->period;
            $data[] = [$period, (int) $count]; //number_format( $sum , 2 , '.' , ',' )
            $data_table[] = [$period, (int) $count];
            $x++;
        }

        $overall_total = $total_open + $total;
        $return['graph_contributing_employers'] = $data;
        $return['monthly_employers_table'] = $data_table;
        $return['total_monthly_employers'] = $overall_total;
        return $return;
    }


    public function getTotalMonthlyEmployerContribution()
    {
        $data = [];

        $electronic = DB::table('main.fin_monthly_collection_electronic')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $manual = DB::table('main.fin_monthly_collection_manual')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $sum_electronic = 0;
        $sum_manual = 0;
        $x = 0;
        foreach ($electronic as $value) {
            $sum = $value->amount + (isset($manual[$x]->amount) ? $manual[$x]->amount : 0);
            $sum_electronic += $value->amount;
            $sum_manual += (isset($manual[$x]->amount) ? $manual[$x]->amount : 0);
            $period = $value->period;
            $data = array_merge($data, [(int) $sum]);
            $x++;
        }

        $sum = $sum_electronic + $sum_manual;
        $return['contribution'] = implode("," , $data);
        $return['contribution_total'] =  $sum ;
        return $return;
    }



    public function getTotalMonthlyContributionTable()
    {

        $data = [];
        $data_table = [];

        /* Get Monthly Target*/
        $fiscalYear = new FiscalYearRepository();
        $fiscal_year = $fiscalYear->findByFiscalYear(financial_year());
        $monthly_target =  ($fiscal_year) ? $fiscal_year->monthlyTarget('ATTCONT') : 0;
        /* end monthly target */

        $electronic = DB::table('main.comp_monthly_contribution_electronic')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $manual = DB::table('main.comp_monthly_contribution_manual')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $sum_electronic = 0;
        $sum_legacy = 0;
        $x = 0;
        foreach ($electronic as $value) {
            $sum = $value->amount + (isset($manual[$x]->amount) ? $manual[$x]->amount : 0);
            $sum_electronic += $value->amount;
            $sum_legacy += (isset($manual[$x]->amount) ? $manual[$x]->amount : 0);
            $period = $value->period;
            $data[] = [$period, (int) $sum]; //$period . " : " . $sum
            $data_table[] = [$period, $sum, $monthly_target, $this->targetVariancePercentage($sum, $monthly_target)];
            $x++;
        }

        $overall_total = $sum_electronic + $sum_legacy;
        $return['contribution_table'] = $data_table;
        $return['contribution_overall_total'] =  ($overall_total);
        $return['contribution_graph'] = $data;
        return $return;
    }

    public function getTotalMonthlyInterestTable()
    {
        $data = [];
        $data_table = [];

        $electronic = DB::table('main.fin_monthly_employer_interest')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $sum_interest = 0;
        foreach ($electronic as $value) {
            $sum = $value->amount;
            $sum_interest += $value->amount;
            $period = $value->period;
            $data_table[] = [$period, $sum]; //$period . " : " . $sum
            $data[] = [$period, (int) $sum];
        }

        $overall_total = $sum_interest;
        $return['interest_table'] = $data_table;
        $return['interest_overall_total'] =  $overall_total ;
        $return['interest_graph'] = $data;
        return $return;
    }



    public function getTotalMonthlyEmployerInterest()
    {
        $data = [];

        $interest = DB::table('main.fin_monthly_employer_interest')->select(["period", "amount"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $sum = 0;
        foreach ($interest as $value) {
            $period = $value->period;
            $sum += $value->amount;
            $data = array_merge($data, [(int) $value->amount]); //Merging Sum
        }

        $return['interest'] = implode("," , $data);
        $return['interest_total'] =  $sum ;
        return $return;
    }






    /* end: Reports based on time Period, providing details on a monthly bases */

    public function queryContribCollectionByEmployerContribCategory($date_type = 1)
    {
        switch ($date_type){
            case 1:
            /*Receipt date*/

            break;

            case 2:
            /*Contrib month*/

            break;

        }

    }


    /*Get Contribution collected on specified dates per employer contribution category i.e. Date is on contrib month*/
    public function getContribCollectionByEmployerContribCategoryForDateRange($employer_contrib_category,$start_date, $end_date, $date_type = 1)
    {
        $query = DB::table('employer_contributions')
        ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'employer_contributions.employer_id')
        ->where('contributing_category_id', $employer_contrib_category);
        switch ($date_type){
            case 1:
            /*Receipt date*/
            $query = $query->whereRaw("receipt_created_at >= ? and receipt_created_at <= ?", [$start_date, $end_date]);
            break;

            case 2:
            /*Contrib month*/
            $query = $query->whereRaw("contrib_month >= ? and contrib_month <= ?", [$start_date, $end_date]);
            break;

        }
        $contrib_amount = $query->sum('contrib_amount');
        $no_of_employers = $query->select('employer_contributions.employer_id')->groupBy('employer_contributions.employer_id')->get()->count();
        return ['contrib_amount' => $contrib_amount, 'no_of_employers' => $no_of_employers];
    }

    /*Get contrib collected for medium contributors employers*/
    public function getContribCollectionByEmployerContribCategoryMediumSmallForDateRange($start_date, $end_date, $date_type = 1)
    {
        $query =DB::table('employer_contributions')
        ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'employer_contributions.employer_id')
        ->where('contributing_category_id', '>=', 3)->where('contributing_category_id', '<=', 4);
        switch ($date_type){
            case 1:
            /*Receipt date*/
            $query = $query->whereRaw("receipt_created_at >= ? and receipt_created_at <= ?", [$start_date, $end_date]);
            break;

            case 2:
            /*Contrib month*/
            $query = $query->whereRaw("contrib_month >= ? and contrib_month <= ?", [$start_date, $end_date]);
            break;

        }
        $contrib_amount = $query->sum('contrib_amount');
        $no_of_employers = $query->select('employer_contributions.employer_id')->groupBy('employer_contributions.employer_id')->get()->count();
        return ['contrib_amount' => $contrib_amount, 'no_of_employers' => $no_of_employers];
    }


    /*Get contribution collection by general type - Public /Private*/
    public function getContribCollectionByEmployerTypeForDateRange($employer_category_cv_id,$start_date, $end_date, $date_type = 1)
    {
        $query = DB::table('employer_contributions')
        ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'employer_contributions.employer_id')
        ->join('employers as e', 'e.id', 'employer_contributions.employer_id')
        ->where('e.employer_category_cv_id', $employer_category_cv_id);
        switch ($date_type){
            case 1:
            /*Receipt date*/
            $query = $query->whereRaw("receipt_created_at >= ? and receipt_created_at <= ?", [$start_date, $end_date]);
            break;

            case 2:
            /*Contrib month*/
            $query = $query->whereRaw("contrib_month >= ? and contrib_month <= ?", [$start_date, $end_date]);
            break;

        }
        $contrib_amount = $query->sum('contrib_amount');
        $no_of_employers = $query->select('employer_contributions.employer_id')->groupBy('employer_contributions.employer_id')->get()->count();
        return ['contrib_amount' => $contrib_amount, 'no_of_employers' => $no_of_employers];
    }

    public function dailyGepgReconciliation()
    {
        $trx_id=DB::table('portal.gepgreconbatch')->select('id')->orderBy('id','DESC')->first();
        $trx_date=Carbon::yesterday()->format('Y-m-d');
        $recon_data=[
            "trx_id"=> $trx_id->id+1,
            "trx_date"=> $trx_date,
            "recon_type"=>1
        ];
        try {
            $client = new Client();

            $response =  $client->request('POST', 'http://172.16.30.14:83/bills/reconcile', ['json' => $recon_data]);
            return $response->getBody();
        }
        catch (ClientError $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return $resp;
        }
        catch (ServerError $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return $resp;
        }
        catch (BadResponse $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return $resp;
        }
        catch( \Exception $e){
            echo "Error Occured!";
        }


    }

}