<?php

namespace App\Repositories\Backend\Finance;


use App\Models\Finance\FinCodeGroup;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;


class FinCodeGroupRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = FinCodeGroup::class;

    public function __construct()
    {

    }

//find or throwexception for fin code group id
    public function findOrThrowException($id)
    {
        $fin_code_group = $this->query()->find($id);
        if (!is_null($fin_code_group)) {
            return $fin_code_group;
        }
        throw new GeneralException(trans('exceptions.backend.finance.fin_code_group_not_found'));
    }


    public function getContributionFinCodeGroupId()
    {
//        fin code group id
        return 3;
          }


    public function getInterestinCodeGroupId()
    {
//        fin code group id
        return 1;
    }

}