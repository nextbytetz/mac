<?php

namespace App\Repositories\Backend\Finance;

use App\Exceptions\GeneralException;
use App\Models\Finance\PaymentVoucher;
use App\Jobs\ProcessPaymentVoucherTransactions\ProcessPVTransactionBatch;
use App\Jobs\ProcessPaymentVoucherTransactions\ProcessPVTransactionIndividual;
use App\Models\Finance\Receivable\BookingInterestRefund;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\ClaimRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PaymentVoucherRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PaymentVoucher::class;
    protected $payment_voucher_transactions;
    protected  $member_types;
    protected $payroll_procs;
    protected  $chunksize =100;

    public function __construct()
    {
        $this->payment_voucher_transactions = new PaymentVoucherTransactionRepository();
        $this->member_types = new MemberTypeRepository();
        $this->payroll_procs = new PayrollProcRepository();
    }

    //find or throwException for fin code group id
    public function findOrThrowException($id)
    {
        $payment_voucher= $this->query()->find($id);
        if (!is_null($payment_voucher)) {
            return $payment_voucher;
        }
        throw new GeneralException(trans('exceptions.backend.finance.payment_voucher_not_found'));
    }

    /*
     * create new
     */

    public function create($input) {
        $payment_voucher = $this->query()->create($input);
        return $payment_voucher;
    }
    /*
     * update
     */
    public function update($id,$input) {

    }


    /*
 * pay voucher
 */
    public function pay($id) {
        $payment_voucher = $this->findOrThrowException($id);
        $payment_voucher->update(['is_paid'=>1]);
        return $payment_voucher;
    }


    /**
     * Payment voucher description only from claim compensation benefits
     * Memo:For Other benefit categories need amendment in Benefit type category.
     */
    public function paymentDescription($id){
        $claims = new ClaimRepository();
        $payment_voucher = $this->findOrThrowException($id);
        $claim_id = $payment_voucher->paymentVoucherTransactions->first()->benefit_resource_id;
        $claim = $claims->findOrThrowException($claim_id);
        $employee = $claim->notificationReport->employee;
        $employer = $claim->notificationReport->employer;
        //Benefit description
        $benefit_type_description_array = [];
        $key = 1;
        foreach ($payment_voucher->paymentVoucherTransactions as $paymentVoucherTransaction){
            $benefit_type_description_array[$key] = $paymentVoucherTransaction->benefitType->name;
            $key += 1;
        }
        $benefit_type_description =implode(',',$benefit_type_description_array);
        $description = trans('labels.backend.finance.being_payment_description') . ' ' . $benefit_type_description . ' '.  trans('labels.general.of') . ' ' . $employee->name . ' ' . trans('labels.general.of') . ' ' . $employer->name;

        return $description;
    }

    /**
     * @param $id
     * Finance Workflow Track approval for this notification report
     */
    public function financeWfApproval($id){
        $claims = new ClaimRepository();
        $notification_reports = new NotificationReportRepository();
        $payment_voucher = $this->findOrThrowException($id);
        $claim_id = $payment_voucher->paymentVoucherTransactions->first()->benefit_resource_id;
        $claim = $claims->findOrThrowException($claim_id);
        $finance_wf_track = $notification_reports->currentWfTrack($claim->notification_report_id);

        return $finance_wf_track;
    }


    /**
     * @param $id
     * New function to get last two finance officers from wf track for pv printout - Applicable to claim compensation benefits vouchers
     */
    public function getFinanceWfTrackForPvPerBenefit($id)
    {
        $wf_track_repo = new WfTrackRepository();
        $payment_voucher = $this->findOrThrowException($id);
        $payment_voucher_tran = $payment_voucher->paymentVoucherTransactions->first();
        $claim_compensation = $payment_voucher_tran->claimCompensation;
        $notification_eligible_benefit = $claim_compensation->notificationEligibleBenefit;
        $notification_workflow = $notification_eligible_benefit->workflow;
        $wf_module_id = $notification_workflow->wf_module_id;
        $unit_finance = 12;
        $last_two_finance_wf_tracks = $wf_track_repo->query()->where('resource_id', $notification_workflow->id)->whereHas('wfDefinition', function($query) use($wf_module_id, $unit_finance){
            $query->where('wf_module_id',$wf_module_id)->where('unit_id', $unit_finance);
        })->orderBy('id', 'desc')->limit(2)->get();
//        foreach($last_two_finance_wf_tracks as $wf_track)
        $officer = $last_two_finance_wf_tracks[0]->user;
        $senior_officer = $last_two_finance_wf_tracks[1]->user;
        return ['officer' => $officer, 'senior_officer' => $senior_officer, 'officer_wf_date' => $last_two_finance_wf_tracks[0]->forward_date ];
    }


    /**
     * @param $id
     * Find respective Notification report - incident
     */
    public function findNotificationReport($id)
    {
        $claims = new ClaimRepository();
        $notification_reports = new NotificationReportRepository();
        $payment_voucher = $this->findOrThrowException($id);
        $claim_id = $payment_voucher->paymentVoucherTransactions->first()->benefit_resource_id;
        $claim = $claims->findOrThrowException($claim_id);
        $notification_report = $notification_reports->find($claim->notification_report_id);
        return $notification_report;
    }

    /*
         * Payee address
         */
    public function payeeAddress($id){
        $payment_voucher = $this->findOrThrowException($id);
    }

    /*
     * Payment vouchers not yet paid
     */
    public function getPendingPaid(){
        return $this->query()->where('is_paid',0);
    }

    /**
     * @return mixed
     * process_type : 1 -> Single (Individual Payments i.e Employee and employer), 2 - Batch Monthly processing i.e Insurance
     */

    public function payrollProc($process_type, $user_id, $run_date = NULL){
        $run_date = Carbon::parse('now')->format('Y-n-j');
        if ($process_type == 1){
            $input =['description'=> 'LUMP SUM', 'user_id'=>$user_id, 'bquarter'=> 'N/A', 'payroll_proc_type_id'=>1, 'run_date' => $run_date];
        }else if ($process_type == 2){
            $this->payroll_procs->checkIfMonthlyPayrollIsRunThisMonth(3, $run_date);
            $input =['description'=> 'LUMP SUM - MONTHLY BATCH', 'user_id'=>$user_id, 'bquarter'=> 'N/A', 'payroll_proc_type_id'=>3, 'run_date' => $run_date];
        }

        return  $this->payroll_procs->create($input);
    }

    /**
     * Process payment voucher transaction individual which are not yet processed
     *
     */
    public function processPaymentVoucherTransactionIndividualChunk($payroll_proc_id, $user_id){

        $this->payment_voucher_transactions->getDistinctPendingIndividualPVTran()->chunk($this->chunksize , function ($pending_payment_voucher_trans) use ($user_id, $payroll_proc_id) {

            dispatch(new ProcessPVTransactionIndividual($pending_payment_voucher_trans,$payroll_proc_id, $user_id));

        });
    }


    /* process payment voucher individual */
    public function processPaymentVoucherTransactionIndividual($pending_payment_voucher_trans,$payroll_proc_id = null, $user_id){
        $payment_voucher_id =NULL;

        foreach ($pending_payment_voucher_trans as $pending_payment_voucher_tran){
            /*check if is Process Status i.e. If Not processed / does not have PV proceed*/
            if( $this->checkIfTranIndividualHasPv($pending_payment_voucher_tran) == false){
                $benefit_types = new BenefitTypeRepository();
                $amount =  $this->payment_voucher_transactions->query()->where('benefit_resource_id',$pending_payment_voucher_tran->benefit_resource_id)->where('member_type_id',$pending_payment_voucher_tran->member_type_id)->where('resource_id',$pending_payment_voucher_tran->resource_id)->sum('amount');
                $reference = sysdefs()->data()->payment_voucher_reference . $pending_payment_voucher_tran->benefit_resource_id;
                $benefit_resource_id = $pending_payment_voucher_tran->benefit_resource_id;

                $member = $this->member_types->getMember($pending_payment_voucher_tran->member_type_id,$pending_payment_voucher_tran->resource_id);

                $bank_branch_id = $member->bank_branch_id;
                $accountno = $member->accountno;

                $input = [ 'amount' => $amount,'bank_branch_id'=>$bank_branch_id, 'accountno'=> $accountno, 'payroll_proc_id'=> $payroll_proc_id, 'reference'=>$reference,  'user_id'=> $user_id];
                if ($amount > 0){
                    $payment_voucher = $this->create($input);
                    $payment_voucher_id =$payment_voucher->id;
                }

                // update payment voucher transactions - process
                $process_input = ['benefit_resource_id' => $pending_payment_voucher_tran->benefit_resource_id,'member_type_id' => $pending_payment_voucher_tran->member_type_id, 'resource_id' =>
                    $pending_payment_voucher_tran->resource_id];
                $this->payment_voucher_transactions->processIndividual($process_input,$payment_voucher_id);

            }

        }

    }


    /**
     * @param $pending_payment_voucher_trans
     * @param null $payroll_proc_id
     * @param $user_id
     * @throws GeneralException
     * Process pv_tran per benefit <NEW>
     */
    public function processPaymentVoucherTransactionIndividualPerBenefit($pending_payment_voucher_trans,$payroll_proc_id = null, $user_id, $notification_workflow_id){
        $payment_voucher_id =NULL;

        foreach ($pending_payment_voucher_trans as $pending_payment_voucher_tran){
            /*check if is Process Status i.e. If Not processed / does not have PV proceed*/
//            if( $this->checkIfTranIndividualHasPv($pending_payment_voucher_tran) == false){
                $benefit_types = new BenefitTypeRepository();
                $amount =  $this->payment_voucher_transactions->query()->where('benefit_resource_id',$pending_payment_voucher_tran->benefit_resource_id)->where('member_type_id',$pending_payment_voucher_tran->member_type_id)->where('resource_id',$pending_payment_voucher_tran->resource_id)->where('is_exported',0)->whereHas('claimCompensation',function($query)use($notification_workflow_id){
                    $query->whereHas('notificationEligibleBenefit', function($query)use($notification_workflow_id){
                        $query->where('notification_workflow_id', $notification_workflow_id);
                    });
                })->sum('amount');
                $reference = sysdefs()->data()->payment_voucher_reference . $pending_payment_voucher_tran->benefit_resource_id;
                $benefit_resource_id = $pending_payment_voucher_tran->benefit_resource_id;

                $member = $this->member_types->getMember($pending_payment_voucher_tran->member_type_id,$pending_payment_voucher_tran->resource_id);

                $bank_branch_id = $member->bank_branch_id;
                $accountno = $member->accountno;

                $input = [ 'amount' => $amount,'bank_branch_id'=>$bank_branch_id, 'accountno'=> $accountno, 'payroll_proc_id'=> $payroll_proc_id, 'reference'=>$reference,  'user_id'=> $user_id, 'is_paid' => 1];
                if ($amount > 0){
                    $payment_voucher = $this->create($input);
                    $payment_voucher_id =$payment_voucher->id;
                }

                // update payment voucher transactions - process
                $process_input = ['benefit_resource_id' => $pending_payment_voucher_tran->benefit_resource_id,'member_type_id' => $pending_payment_voucher_tran->member_type_id, 'resource_id' =>
                    $pending_payment_voucher_tran->resource_id];
                $this->payment_voucher_transactions->processIndividual($process_input,$payment_voucher_id);

            }

//        }

    }



    /*Check if Individual Transaction already processed i.e. Has Payment Voucher Id*/
    public function checkIfTranIndividualHasPv($payment_voucher_tran)
    {
        $payment_voucher_transactions = new PaymentVoucherTransactionRepository();
        $benefit_resource_id = $payment_voucher_tran->benefit_resource_id;
        $member_type_id = $payment_voucher_tran->member_type_id;
        $resource_id = $payment_voucher_tran->resource_id;
        $tran = $payment_voucher_transactions->query()->where('benefit_resource_id', $benefit_resource_id)->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('is_processed', 1)->first();
        if ($tran){
            /*If have PV*/
            return true;
        }else {
            /*If does not have PV*/
            return false;
        }

    }




    /**
     * Process payment voucher transaction Batch (i.e. insurance) which are not yet processed
     *Compensation payment Paid monthly
     */
    public function processPaymentVoucherTransactionBatchChunk($payroll_proc_id, $user_id){

        $this->payment_voucher_transactions->getDistinctPendingBatchPVTran()->chunk($this->chunksize , function ($pending_payment_voucher_trans) use ($user_id, $payroll_proc_id) {
            dispatch(new ProcessPVTransactionBatch($pending_payment_voucher_trans, $payroll_proc_id, $user_id));

        });

    }

    /*process payment voucher trans batch*/
    public function processPaymentVoucherTransactionBatch($pending_payment_voucher_trans,$payroll_proc_id, $user_id){
        $payment_voucher_id = NULL;
        foreach ($pending_payment_voucher_trans as $pending_payment_voucher_tran) {
            /*check if is Process Status i.e. If Not processed / does not have PV proceed*/
            if ($this->checkIfTranBatchHasPv($pending_payment_voucher_tran) == false) {
                $amount = $this->payment_voucher_transactions->query()->where('member_type_id', $pending_payment_voucher_tran->member_type_id)->where('resource_id', $pending_payment_voucher_tran->resource_id)->sum('amount');
                $member = $this->member_types->getMember($pending_payment_voucher_tran->member_type_id, $pending_payment_voucher_tran->resource_id);
                $input = ['amount' => $amount, 'bank_branch_id' => $member->bank_branch_id, 'accountno' => $member->accountno, 'payroll_proc_id' => $payroll_proc_id, 'user_id' => $user_id];
                if ($amount > 0) {
                    $payment_voucher = $this->create($input);
                    $payment_voucher_id = $payment_voucher->id;
                }
                // update payment voucher transactions - process
                $process_input = ['member_type_id' => $pending_payment_voucher_tran->member_type_id, 'resource_id' =>
                    $pending_payment_voucher_tran->resource_id];
                $payment_voucher_trans = $this->payment_voucher_transactions->processBatch($process_input, $payment_voucher_id);

            }
        }

    }



    /*Check if Batch Transaction already processed i.e. Has Payment Voucher Id*/
    public function checkIfTranBatchHasPv($payment_voucher_tran)
    {
        $payment_voucher_transactions = new PaymentVoucherTransactionRepository();

        $member_type_id = $payment_voucher_tran->member_type_id;
        $resource_id = $payment_voucher_tran->resource_id;
        $tran = $payment_voucher_transactions->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('is_processed', 1)->first();
        if ($tran){
            /*If have PV*/
            return true;
        }else {
            /*If does not have PV*/
            return false;
        }

    }



}