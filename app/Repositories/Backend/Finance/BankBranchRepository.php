<?php

namespace App\Repositories\Backend\Finance;

use App\Models\Finance\BankBranch;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;


class BankBranchRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = BankBranch::class;

    public function __construct()
    {

    }

//find or throwException for fin code group id
    public function findOrThrowException($id)
    {
        $bank_branch= $this->query()->find($id);
        if (!is_null($bank_branch)) {
            return $bank_branch;
        }
        throw new GeneralException(trans('exceptions.backend.finance.bank_branch_not_found'));
    }

    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }


}