<?php

namespace App\Repositories\Backend\Finance;

use App\Repositories\BaseRepository;
use App\Models\Finance\PaymentType;

class PaymentTypeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PaymentType::class;

    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAll($order_by = 'name', $sort = 'asc')
    {
        return $this->query()
            ->orderBy($order_by, $sort)
            ->get();
    }


    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function chequePayment()
    {
        return 2;
    }


}