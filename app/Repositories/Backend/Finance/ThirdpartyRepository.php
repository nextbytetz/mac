<?php

namespace App\Repositories\Backend\Finance;

use App\Models\Finance\Thirdparty;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;

class ThirdpartyRepository extends BaseRepository
{

    const MODEL = Thirdparty::class;

    /**
     * @return mixed
     */
    public function getForDataTable() {
        $return = $this->query()->select([DB::raw("coalesce(employers.name, concat_ws(' ', thirdparties.firstname, thirdparties.middlename, thirdparties.lastname)) as name"), 'thirdparties.id', 'thirdparties.external_id'])->leftJoin('employers', 'employers.id', '=', 'thirdparties.employer_id');
        return $return;
    }

    public function getAll()
    {
        return $this->getForDataTable()->where("isactive", 1)->get();
    }

    /**
     * @param $input
     * @return mixed
     */
    public function store($input)
    {
        $thirdparty = $this->query()->create($input);
        return $thirdparty;
    }

    /**
     * @param Model $thirdparty
     * @param $input
     */
    public function update(Model $thirdparty, $input)
    {
        if ($input['category'] == 1) {

        }
        switch ($input['category']) {
            case 1:
                $input['firstname'] = NULL;
                $input['middlename'] = NULL;
                $input['lastname'] = NULL;
                break;
            case 0:
                $input['employer_id'] = NULL;
                break;
        }
        $thirdparty->update($input);
    }

    /**
     * @param Model $thirdparty
     * @throws GeneralException
     */
    public function destroy(Model $thirdparty)
    {
        $receipts = $thirdparty->receipts()->count();
        if ($receipts > 0) {
            throw new GeneralException("Can not delete third party payer with associated receipts");
        }
        $thirdparty->delete();
    }

    public function getReceipt($id)
    {
        $receipt = new ReceiptRepository();
        return $receipt->query()->where(['thirdparty_id' => $id])->select(['receipts.id', 'rctno', 'description', DB::raw("to_char(amount, '999G999G999D99') as amount"), DB::raw('payment_types.name as payment_type')])->join('payment_types', 'receipts.payment_type_id', '=', 'payment_types.id');
    }

}