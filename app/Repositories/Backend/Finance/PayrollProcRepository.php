<?php

namespace App\Repositories\Backend\Finance;

use App\Models\Finance\Bank;
use App\Models\Finance\PayrollProc;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use Carbon\Carbon;

class PayrollProcRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PayrollProc::class;

    public function __construct()
    {

    }

//find or throwException for fin code group id
    public function findOrThrowException($id)
    {
        $payroll_proc= $this->query()->find($id);
        if (!is_null($payroll_proc)) {
            return $payroll_proc;
        }
        throw new GeneralException(trans('exceptions.backend.finance.payroll_proc_not_found'));
    }

    /*
     * create new
     */

    public function create($input) {
        return $this->query()->create($input);
    }
    /*
     * update
     */
    public function update($id,$input) {

    }

    /**
     * @param $id
     * Undo Payroll Proc
     */
    public function undo($id)
    {
        $payroll_proc = $this->find($id);
        $payroll_proc->delete();
    }

    /**
     * @return mixed
     * process_type : 1 -> Single (Individual Payments i.e Employee and employer)=> LUMP SUM,  2 - Pension
     * processing i.e
     * Insurance
     */

    public function createPayrollProc($process_type, $user_id, $run_date = NULL){
        $run_date = Carbon::parse('now')->format('Y-n-j');
        if ($process_type == 1){
            $input =['description'=> 'LUMP SUM', 'user_id'=>$user_id, 'bquarter'=> 'N/A', 'payroll_proc_type_id'=>1, 'run_date' => $run_date];
        }else if ($process_type == 2){
            $this->payroll_procs->checkIfMonthlyPayrollIsRunThisMonth(3, $run_date);
            $input =['description'=> 'LUMP SUM - MONTHLY BATCH', 'user_id'=>$user_id, 'bquarter'=> 'N/A', 'payroll_proc_type_id'=>3, 'run_date' => $run_date];
        }

        return  $this->payroll_procs->create($input);
    }

    /*Get the most recent monthly payroll proc*/
    public function getLastMonthlyPensionPayrollProc(){
        $payroll_proc_type_id = 2;
        $recent_payroll_proc = $this->query()->where('payroll_proc_type_id', $payroll_proc_type_id)->orderBy('id', 'desc')->first();
        return $recent_payroll_proc;
    }

    /*Find Monthly pyroll proc by Run date*/
    public function findMonthlyPayrollProcByRunDate($run_date)
    {
        $payroll_proc_type_id = 2;
        $payroll_proc = $this->query()->where('payroll_proc_type_id', $payroll_proc_type_id)->where('run_date', $run_date)->orderBy('id', 'asc')->first();
        return $payroll_proc;
    }


    /*Get Monthly payroll Run date by $payroll_run_approval_id*/
    public function getPayrollRunDate($payroll_run_approval_id){
        $payroll_proc = $this->query()->whereHas('payrollRunApproval', function($query) use($payroll_run_approval_id){
            $query->where('payroll_run_approvals.id',$payroll_run_approval_id);
        })->first();
        $run_date = $payroll_proc->run_date;
        return $run_date;
    }



    /**
     * Check if Monthly payroll is run this month
     */

    /*General validation on run month specified*/
    public function checkValidationOnRunDateForPayroll($payroll_proc_type_id, $run_date)
    {
        $this->checkIfMonthlyPayrollIsRunThisMonth($payroll_proc_type_id,$run_date);
        $this->checkIfMonthlyPayrollRunDateIsCorrect($payroll_proc_type_id, $run_date);
        $this->checkIfThereIsNoPendingMonthlyPayroll($payroll_proc_type_id);
//        $this->checkIfAllEligibleBeneficiariesHasBankDetails();
    }


    public function checkIfMonthlyPayrollIsRunThisMonth($payroll_proc_type_id, $run_date){
        $month = Carbon::parse($run_date)->format('n');
        $year = Carbon::parse($run_date)->format('Y');
        $payroll_proc = $this->query()->where('payroll_proc_type_id', $payroll_proc_type_id)
            ->whereMonth("run_date", "=",  $month)
            ->whereYear("run_date", "=", $year)->first();
        if ($payroll_proc){
            throw new GeneralException(trans('exceptions.backend.finance.monthly_payroll_already_run'));
        }
    }

    /**
     * @param $payroll_proc_type_id
     * @param $run_date
     * Check if monthly payroll run date is accurate and correct i.e. which is supposed to run
     */
    public function checkIfMonthlyPayrollRunDateIsCorrect($payroll_proc_type_id, $run_date){
        $recent_payroll_proc = $this->query()->where('payroll_proc_type_id', $payroll_proc_type_id)->orderBy('id', 'desc')->first();
        if($recent_payroll_proc){
            $recent_run_date = Carbon::parse($recent_payroll_proc->run_date);
            $run_date = Carbon::parse($run_date);
            if(months_diff($recent_run_date, $run_date) <> 1){
                throw new GeneralException(trans('Payroll run month is not correct! Please check!'));
            }
        }
    }

    /**
     * Check if there is monthly pension payroll pending
     */
    public function checkIfThereIsNoPendingMonthlyPayroll($payroll_proc_type_id)
    {
        $recent_payroll_proc = $this->query()->where('payroll_proc_type_id', $payroll_proc_type_id)->orderBy('id', 'desc')->first();
        if($recent_payroll_proc){
            $payroll_run_approval = $recent_payroll_proc->payrollRunApproval;
            if($payroll_run_approval->wf_done == 0){
                throw new GeneralException(trans('There is pending pension payroll approval! Please check!'));
            }else{
                return true;
            }
        }else{
            return true;
        }

    }

    /**
     * @param $id
     * @return bool
     * Check if all beneficiaries have bank details for eligible for payroll
     */
    public function checkIfAllEligibleBeneficiariesHasBankDetails()
    {
//        $payroll_run_repo = new PayrollRunRepository();
//        $check_who_has_not = $payroll_run_repo->query()->where('payroll_run_approval_id', $id)->where(function ($query){
//            $query->whereNull('bank_id')->orWhereNull('bank_branch_id')->whereIn('bank_id', [5,6]);
//        })->count();
//        if($check_who_has_not > 0){
//            return false;
//        }else{
//            return true;
//        }

        $check_dependents = (new DependentRepository())->getEligibleDependentsForPayroll()->where(function($query){
            $query->whereNull('dependents.bank_id')->orWhereIn('dependents.bank_id', [5,6]);
        })->count();

        $check_pensioners = (new PensionerRepository())->getEligibleForPayroll()->where(function($query){
            $query->whereNull('pensioners.bank_id')->orWhereIn('pensioners.bank_id', [5,6]);
        })->count();

        if($check_pensioners > 0 || $check_dependents > 0)
        {
            throw new GeneralException('There are beneficiaries do not have bank details! Please fill their bank details and re-run payroll again.!');
        }else{
            return true;
        }

    }


}