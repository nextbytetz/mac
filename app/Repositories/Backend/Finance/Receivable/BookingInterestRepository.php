<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02-May-17
 * Time: 12:59 PM
 */

namespace App\Repositories\Backend\Finance\Receivable;

use App\Exceptions\WorkflowException;
use App\Models\Finance\Receivable\BookingInterestRefund;
use App\Models\Operation\Compliance\Member\Employer;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Claim\DocumentGroupRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Models\Finance\Receivable\BookingInterest;
use App\Exceptions\GeneralException;
use App\Services\Receivable\CalculateInterest;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BookingInterestRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = BookingInterest::class;


    //find or throwexception for booking interest
    public function findOrThrowException($id)
    {
        $booking_interest = $this->query()->find($id);
        if (!is_null($booking_interest)) {
            return $booking_interest;
        }
        throw new GeneralException(trans('exceptions.backend.finance.booking_interest.not_found'));
    }



    /*
     * General CRUD Methods  ================
     */


    // End Crud Methods ==================

    /**
     * @param $booking_id
     * @return null
     * Check if booking has interest which is not been paid
     */
    public function checkIfBookingHasInterest($booking_id) {

        $booking_interest = $this->query()->where('booking_id','=',$booking_id)->first();
        if (!is_null($booking_interest)) {
            return $booking_interest; // exist
        }
        return null;
    }


    /**
     * @param $employer_id
     * @return mixed
     * Get Total interest raised to this employer
     */


    public function getTotalEmployerInterest($employer_id)
    {
        $total_interest = $this->query()->where('iswrittenoff', 0)->whereHas("booking",
            function ($query) use
            ($employer_id) {
                $query->where('employer_id','=', $employer_id);
            })->sum('amount');

        return $total_interest;
    }



    /*
*
* get status of interest adjustment
*/
    public function getInterestAdjustLabel($id)
    {
        if (($this->checkIfIsAdjusted($id))== 1)
            return "<span class='tag tag-success success_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.complete') . "'>" . '.' . "</span>";
        return "<span class='tag tag-warning warning_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.incomplete') . "'>" . '.' . "</span>";

    }

    /*
     *
     * check if interest is adjusted completely / completed workflow
     */

    public function checkIfIsAdjusted($id)
    {
        $interest = $this->query()->where(['isadjusted'=> 1, 'id'=>$id])
            ->first();
        if (!is_null($interest)) {
            return 1; // adjusted
        }
        return 0; // not yet adjusted
    }


    /***
     * @param $booking_id
     * Recheck and update Paid Status accordingly
     */
    public function recheckPaidStatus($booking_id){
        $booking_interest = $this->query()->where('booking_id',$booking_id)->first();
        $receipt_codes = new ReceiptCodeRepository();
// update paid status (check paid amount against interest raised)
        if ($booking_interest){
            if (($booking_interest->amount <= $receipt_codes->getTotalInterestPaidForBookingId($booking_id)) && ($booking_interest->amount > 0)) {
                $booking_interest->update(['ispaid' => 1]);
            } else {
                $booking_interest->update(['ispaid' => 0]);
            }
        }

    }




    /**
     * Adjust method ===============
     *
     */

// Adjust interest (update interest)
    public function interestAdjustment($id, $input) {
        $booking_interest = $this->findOrThrowException($id);
        $adjusted_amount = $this->findNewInterest($booking_interest,$input) - $booking_interest->amount ;
        $booking_interest->update((['adjust_note' => $input['comments'], 'adjust_user_id' => access()->user()->id, 'adjust_amount' => $adjusted_amount,'new_payment_date' => $input['new_payment_date'], 'reason_type_cv_id' => $input['reason_type_cv_id'] ]));

    }

    // Find new interest when payment date is ammended
    public function findNewInterest($booking_interest, $input) {
        $receipt_codes = new ReceiptCodeRepository();
        $create_interest = new CalculateInterest();
        $late_months = $create_interest->getLateMonths($booking_interest->miss_month, Carbon::parse($input['new_payment_date']));
        $amount = $receipt_codes->getTotalContributionPaidForBookingId($booking_interest->booking_id);
        $new_interest = $create_interest->getInterest($booking_interest->interest_percent,$late_months,$amount );

        return $new_interest;
    }


// When workflow complete update isdjust flag.
    public function interestAdjustmentApprove($id) {
        $booking_interest = $this->findOrThrowException($id);
        $create_interest = new CalculateInterest();
        $late_months = $create_interest->getLateMonths($booking_interest->miss_month,$booking_interest->new_payment_date);
        $booking_interest->update((['isadjusted' => 1, 'amount'=> ($booking_interest->amount + $booking_interest->adjust_amount), 'late_months' => $late_months  ]));
        //update receipt table receipt date
        $receipts = new ReceiptRepository();
        $bookings = new BookingRepository();
        $receipts->updatePaymentDate($bookings->getFirstPaidReceipt($booking_interest->booking_id)->id,$booking_interest->new_payment_date );

    }


    /*
* get all interests adjusted belong to this employer
*/
    public function getInterestAdjusted($employer_id)
    {
        $interest_adjusted= $this->query()->whereNotNull('adjust_amount')->whereHas("booking",
            function ($query) use
            ($employer_id) {
                $query->where('employer_id','=', $employer_id);
            })->get();
        return $interest_adjusted;
    }


    /*get document attached*/
    public function getDocsAttachedForAdjustment($booking_interest_id)
    {
        $interest = $this->find($booking_interest_id);
        $employer = $interest->booking->employer;
        $docs_all = (new DocumentRepository())->queryDocumentsByGroup(30)->pluck('id')->toArray();
        $docs_attached = $employer->documents()->where('external_id', $booking_interest_id)->whereIn('document_id',$docs_all)->get();
        return $docs_attached;
    }

    /*check and validate if document fo adjustment have been uploaded*/
    public function validateOnDocumentsForAdjust($booking_interest_id)
    {
        $docs_attached =$this->getDocsAttachedForAdjustment($booking_interest_id);
        if($docs_attached->count() > 0){
            /*Attached - ok*/
        }else{
            throw  new GeneralException('Attach Supporting Document to proceed with Interest adjustment');
        }
    }

    /**
     *
     * Start: Letter Methods
     */

    /*Check if can initiate wf letter*/
    public function checkIfCanInitiateLetterWf($current_track)
    {
        $return = false;
        if(isset($current_track)){
            $wf_definition = $current_track->wfDefinition;
            $current_level = $wf_definition->level;
            $wf_module = $wf_definition->wfModule;
            $wf_module_id = $wf_module->id;
            $booking_interest = $this->find($current_track->resource_id);
            $wf_module_group_id = 2;
            $type = $wf_module->type;
            $level_for_letter = $this->getLevelForLetterByWfModule($wf_module_id);
            $access = access()->user()->hasWorkflowModuleDefinition($wf_module_group_id, $type, $level_for_letter);

            if($current_level == $level_for_letter && $access == true && $booking_interest->wf_done == 0)
//            if($current_level == $level_for_letter && $access == true && $arrears == 0)
            {
                //Check if was declined on approval level
                $check_if_declined = (new WfTrackRepository())->checkIfModuleDeclined($current_track->resource_id, $wf_module->id);
                if($check_if_declined == false){
                    //only if approved
                    $return = true;
                }
            }
        }
        return $return;
    }


    /*Get level For Letter*/
    public function getLevelForLetterByWfModule($wf_module_id)
    {
        $module_repo = new WfModuleRepository();
        switch($wf_module_id){
            case $module_repo->bookingInterestModules()[1]://67
                return 6;
                break;

        }
    }

    /*Check if wf letter is initiated*/
    public function checkIfWfLetterIsInitiated($booking_interest, $current_wf_track)
    {
        if($booking_interest->adjustmentResponseLetter()->where('isinitiated','<>', 0)->count() == 0 && $this->checkIfCanInitiateLetterWf($current_wf_track) == true)
        {
            throw new WorkflowException('Workflow for letter not initiated! Initiate to proceed!');
        }
        return true;
    }

    /*Process actions at levels before on approval before wf complete*/
    public function processWfActionAtLevels($current_wf_track, array $input = null)
    {
        DB::transaction(function () use ($current_wf_track, $input) {
            $wf_module_repo = new WfModuleRepository();
            $resource_id = $current_wf_track->resource_id;
            $wf_module_id = $current_wf_track->wfDefinition->wf_module_id;
            $wf_definition = $current_wf_track->wfDefinition;
            $level = $wf_definition->level;
            $booking_interest = $this->find($resource_id);
            $is_approved = ($current_wf_track->status == 1) ? 1 : 0;

            switch ($wf_module_id) {
                case $wf_module_repo->bookingInterestModules()[1]:
                    switch ($level) {

                        case 6:
                            /*level for letter*/

                            $this->checkIfWfLetterIsInitiated($booking_interest, $current_wf_track);

                            break;
                    }

                    break;

            }
        });

    }
    /*Get Letter reference No. attribute*/
    public function getLetterReferenceNo($letter_cv_reference)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference);
        $prev_reference = (isset($prev_letter)) ? $prev_letter->reference : null;
        $next_folio_no = $this->getLetterFolioNo($letter_cv_reference);
        $reference_no = '';
        $default = 'AC.454/468/';//TODO need to update <deploy_extension>
        $reference_no = $this->getFullReferenceNo($prev_reference, $default, $next_folio_no)  ;

        return $reference_no;
    }


    /*Prefix of letter reference no*/
    public function getFullReferenceNo($prev_reference, $default_ref, $next_folio_no)
    {
        if(isset($prev_reference)){
            $ref_no =  remove_after_last_this_character('/',$prev_reference) . '/';
        }else{
            $ref_no = $default_ref;
        }
        return $ref_no  . $next_folio_no;
    }

    /*Letter folio no*/
    public function getLetterFolioNo($letter_cv_reference)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference);
        if(isset($prev_letter)){
            $resources =  json_decode($prev_letter->resources, true);
            $next_folio_no =  $resources[0]['value'] + 1;
        }else{
            $next_folio_no = 1;
        }
        return $next_folio_no;
    }


    /**
     * @param $employer_closure
     * @return int
     * Get module type for workflow when initiating approval workflow
     */
    public function getWfModuleType($booking_interest)
    {
        $type = 2;

        return $type;
    }

    //==========end adjust ======================


    /*
     * WRITE OFF METHODS =======================================
     */
// Check if there is interest eligible to be written off in the range
    public function checkIfThereIsEligibleInterestInDateRange($id, $from_date, $end_date) {
        $interests = $this->query()->where('ispaid', '=', 0)->whereNull('adjust_amount')->whereNull('interest_write_off_id')->whereHas('booking', function ($query) use ($id,$from_date,$end_date)
        {
            $query->where('employer_id', '=', $id)->where('rcv_date','>=', Carbon::parse($from_date)->format('Y-m-d'))->where('rcv_date','<=', Carbon::parse($end_date)->format('Y-m-d'));
        })->first();
        if (!is_null($interests)) {
            return $interests;
        }else {
            throw new GeneralException(trans('exceptions.backend.finance.booking.interest_not_found_date_range'));
        }

    }

    /**
     * check If There Is In Eligible InterestInDateRange
     * @author: Martin
     * @param $employer_id
     * @param $from_date
     * @param $end_date
     * @return string
     * @throws GeneralException
     */
    public function checkIfThereIsInEligibleInterestInDateRange($employer_id, $from_date, $end_date) {
        $interests = $this->query()->whereHas('booking', function ($query) use ($employer_id,$from_date,$end_date)
        {
            $query->where('employer_id', '=', $employer_id)->where('rcv_date','>=', Carbon::parse($from_date)->format('Y-m-d'))->where('rcv_date','<=', Carbon::parse($end_date)->format('Y-m-d'));
        })->where(function ($query){
            $query->where('ispaid', '=', 1)->orWhereNotNull('adjust_amount')->orWhereNotNull('interest_write_off_id') ;
        })->first();

        if (is_null($interests)) {
            return '';
        }else {
            throw new GeneralException(trans('exceptions.backend.finance.booking.interest_not_eligible_date_range'));
        }
    }


    /*
     * Update write off id in the booking interest for given date range when writting off interests
     */
    public function updateWriteOffId($employer_id,$write_off_id, $from_date, $end_date) {
        $interests = $this->query()->whereHas('booking', function ($query) use ($employer_id,$from_date,$end_date)
        {
            $query->where('employer_id', '=', $employer_id)->where('rcv_date','>=', Carbon::parse($from_date)->format('Y-m-d'))->where('rcv_date','<=', Carbon::parse($end_date)->format('Y-m-d'));
        })->update(['interest_write_off_id' => $write_off_id ]);
    }


    /**
     * Approve write off when workflow is complete.
     */
    public function approveInterestWriteOff($interest_write_off_id) {
        $interests = $this->query()->where('interest_write_off_id', $interest_write_off_id)->update(['iswrittenoff' => 1]);
        $deleted_interests =  $this->query()->where('interest_write_off_id', $interest_write_off_id)->delete();
    }

    public function updateWrittenOffInterest($employer_id,$interest_write_off_id, $input) {

        $interests = $this->query()->where('interest_write_off_id',$interest_write_off_id )->update(['interest_write_off_id' => null]);

        $this->updateWriteOffId($employer_id,$interest_write_off_id, $input['min_date'],$input['max_date']);
    }



    public function employerDueAmount($month, $year, $employer_id)
    {
        $receipt_codes = new ReceiptCodeRepository();
        $interest = $this->query()->whereHas('booking', function ($query) use ($month, $year, $employer_id) {
            $query->whereRaw("date_part('month', rcv_date) = :month and date_part('year', rcv_date) = :year and employer_id = :employer_id", ['month' => $month, 'year' => $year, 'employer_id' => $employer_id]);
        })->first();
        if ($interest) {
            if ($interest->receiptCodes()->count()) {
//                $paid = $interest->receiptCodes->sum("amount");
                $paid = $receipt_codes->getTotalInterestPaidForBookingId($interest->booking_id);
            } else {
                $paid = 0;
            }
            $amount = $interest->amount - $paid;
            if ($amount < 0) {
                $amount = 0;
            }
        } else {
            $amount = 0;
        }
        return $amount;
    }


//END write off ==================


    /*
        * INTEREST REFUNDING METHODS ===================start====================
        */

    public function refundSelected(array $input)
    {
        return DB::transaction(function () use ($input) {
            /*Validation*/
            $this->checkIfSelected($input);
            /*create interest refund*/
            $interest_refund = $this->createInterestRefund();

            /*update interests with interest refund id*/
            $this->updateInterestWithRefundId($input, $interest_refund);

            /*update interest refund amount*/
            $interest_refund->update(['amount' => $this->findTotalRefundAmount($interest_refund->id)]);

            return $interest_refund;
        });
    }

    /* Check if Selected*/
    public function checkIfSelected($input)
    {
        if ( !array_key_exists('id', $input) ){
            throw new GeneralException(trans('exceptions.backend.finance.booking.nothing_is_selected'));
        }
    }

    /* Find balance and Check if Interest is Refundable (Has Negative balance) */
    public function findInterestBalance($id)
    {
        $receiptCodes = new ReceiptCodeRepository();
        $interest = $this->findOrThrowException($id);
        $amount_paid = $receiptCodes->getTotalInterestPaidForBookingId($interest->booking_id);
        $balance = $interest->amount - $amount_paid;
        if ($balance >= 0){
            throw new GeneralException('Interest of ' . $interest->miss_month_formatted . ' not refundable! Please check!');
        }
        return (-1 * $balance);
    }


    /*Update Interest with refund batch id*/
    public function updateInterestWithRefundId($input, $interest_refund)
    {
        if (isset($input['id']) And ($input['id'])) {
            $interests = $this->query()->find($input['id']);
            foreach ($interests as $interest) {
                $balance =  $this->findInterestBalance($interest->id);
                $interest->update([
                    'booking_interest_refund_id' => $interest_refund->id,
                    'refund_amount' => $balance
                ]);
            }
        }
    }


    /*Create Interest refund*/
    public function createInterestRefund()
    {
        return DB::transaction(function (){
            $interest_refund = BookingInterestRefund::create([
                'amount' => 0,
                'user_id' => access()->id(),
            ]);
            return $interest_refund;
        });
    }


    /*Find total refund amount for this refund batch*/
    public function findTotalRefundAmount($interest_refund_id)
    {
        $amount = $this->query()->where('booking_interest_refund_id', $interest_refund_id)->sum('refund_amount');
        return $amount;
    }
    /*Update total refund amount*/
    public function updateTotalRefundAmount($interest_refund_id)
    {
        $interest_refund = BookingInterestRefund::find($interest_refund_id);
        $interest_refund->update(['amount' => $this->findTotalRefundAmount($interest_refund_id)]);
    }

    /*Add Interest Refund */
    public function addInterestRefund($id, $interest_refund_id)
    {
        DB::transaction(function () use ($id, $interest_refund_id) {
            $interest = $this->findOrThrowException($id);
            $balance = $this->findInterestBalance($id);
            $interest->update(['booking_interest_refund_id' => $interest_refund_id, 'refund_amount' => $balance]);
            $this->updateTotalRefundAmount($interest_refund_id);
        });
    }

    /*remove Interest Refund */
    public function removeInterestRefund($id, $interest_refund_id)
    {
        DB::transaction(function () use ($id, $interest_refund_id){
            $interest = $this->findOrThrowException($id);
            $interest->update(['booking_interest_refund_id' => NULL, 'refund_amount' => NULL ]);
            /*check if there is at least one interest for this refund batch*/
            $this->checkIfThereIsInterestForThisRefund($interest_refund_id);
            $this->updateTotalRefundAmount($interest_refund_id);
        });
    }


    /*Undo Interest Refund*/
    public function undoInterestRefund($interest_refund_id)
    {
        DB::transaction(function () use ($interest_refund_id){
            $interest_refund = BookingInterestRefund::find($interest_refund_id);
            $this->query()->where('booking_interest_refund_id', $interest_refund_id)->update(['refund_amount' => NULL, 'booking_interest_refund_id' => NULL]);
            $interest_refund->delete();
        });
    }

    /* Initiate Interest refund process*/
    public function initiateInterestRefund($interest_refund_id)
    {
        $this->checkIfBankDetailsAreFilled($interest_refund_id);
    }

    /* Check validation before initiating Interest Refund */
    public function  checkIfBankDetailsAreFilled($interest_refund_id)
    {
        $interest_refund = BookingInterestRefund::find($interest_refund_id);
        if (!$interest_refund->bank_branch_id || !$interest_refund->accountno)
        {
            throw new GeneralException('Fill all bank details to proceed! Please check!');
        }
    }

    /* Interest refund Workflow Complete*/
    public function interestRefundWfComplete($interest_refund_id)
    {
        $this->query()->where('booking_interest_refund_id',$interest_refund_id)->update(['isrefunded' => 1]);
    }



    /*Check validation on removing interest from refund batch*/
    public function checkIfThereIsInterestForThisRefund($interest_refund_id)
    {
        $interest = $this->query()->where('booking_interest_refund_id', $interest_refund_id)->first();
        if (!$interest)
        {
            throw new GeneralException('Can not remove all interest for this refund! Please check!');
        }
    }





    /*Get Interest refunded using refund batch id*/

    public function getInterestRefundedForDataTable($interest_refund_id)
    {
        $interest_refunded = $this->query()->where('booking_interest_refund_id',$interest_refund_id);
        return $interest_refunded;
    }

    public function getInterestRefundOverviewForDataTable($employer_id)
    {
        $interest_refunds = BookingInterestRefund::query()->whereHas('bookingInterests', function ($query) use ($employer_id){
            $query->whereHas('booking', function ($query) use ($employer_id)
            {
                $query->where('employer_id', $employer_id);
            });
        })->get();
        return $interest_refunds;
    }


    public function getInterestEligibleForRefundingForDataTable(Model $employer)
    {
//        $interest_eligible_for_refunds = $employer->bookingInterests->where('isrefunded', 0)->where('hasreceipt', 1);

        $interest_eligible_for_refunds = $this->query()->whereHas('booking', function($query) use($employer){
            $query->where('employer_id',$employer->id);
        })->whereNull('booking_interest_refund_id')
            ->where
            ('hasreceipt', 1)
            ->whereHas('receiptCodes', function($query){
                $query->whereHas('receipt', function ($query){
                    $query->where('isdishonoured', 0);
                });
            });

        return $interest_eligible_for_refunds;
    }

    /*get eligible interest for modifying interest refund*/
    public function getInterestEligibleForModifyRefundForDataTable(Model $employer, $interest_refund_id)
    {
        $interest_eligible_for_modify_refunds = $this->query()->whereHas('booking', function($query) use($employer){
            $query->where('employer_id',$employer->id);
        })->where(function($query) use($interest_refund_id){
            $query->whereNull('booking_interest_refund_id')->orWhere('booking_interest_refund_id',$interest_refund_id);
        })->where('hasreceipt', 1)
            ->whereHas('receiptCodes', function($query){
                $query->whereHas('receipt', function ($query){
                    $query->where('isdishonoured', 0);
                });
            });
        return $interest_eligible_for_modify_refunds;
    }

    /*update bank details*/
    public function updateBankDetails($interest_refund_id, array $input)
    {
        DB::transaction(function () use($interest_refund_id, $input){
            $interest_refund = BookingInterestRefund::query()->find($interest_refund_id);
            $employer = $interest_refund->getEmployer();
            $interest_refund->update(['bank_branch_id' => $input['employer_bank_branch_id'],
                'accountno' => $input['employer_accountno']]);
            /*Update into employer as well*/
            $employer->update(['bank_branch_id' => $input['employer_bank_branch_id'],
                'accountno' => $input['employer_accountno']]);
        });
    }


    /* Get Interest Refund Pending for Payments*/
    public function getPendingInterestRefundForPayment()
    {
        $pending_refunds = BookingInterestRefund::query()->where('wf_done',1 )->where('is_paid', 0);
        return $pending_refunds;
    }

    /* ----------end of interest refund ---------------------end ----------*/


    /*Cancel workklow when interest adjusted manual but workflow started on the system.*/
    public function completeWfForInterestAdjustedManually($booking_interest_id)
    {
        $wf_module_group_id = 2;
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $booking_interest_id]);
        $current_wf_track = $workflow->currentWfTrack();
        $comment = 'Adjusted Manually';
        $current_wf_track->update([
            'assigned' => 1,
            'user_type' => "App\Models\Auth\User",
            'comments' => $comment,
            'forward_date' => Carbon::now(),
            'status' => 5,
            'user_id' => access()->id()
        ]);
    }

}