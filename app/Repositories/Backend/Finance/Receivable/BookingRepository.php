<?php
/**
 * Created by PhpStorm.
 * @Author: Martin L <m.luhanjo@nextbyte.co.tz>
 * Date: 02-May-17
 * Time: 12:56 PM
 */

namespace App\Repositories\Backend\Finance\Receivable;

use App\Models\Finance\Receipt\ReceiptCode;
use App\Models\Finance\Receivable\Booking;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BookingRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Booking::class;
    private $receipt;
    private $fin_code_groups;

    public function __construct(){
        $this->receipt = new ReceiptRepository();
        $this->fin_code_groups =new FinCodeGroupRepository();
    }


    //find or throwexception for booking
    public function findOrThrowException($id)
    {
        $booking = $this->query()->find($id);
        if (!is_null($booking)) {
            return $booking;
        }
        throw new GeneralException(trans('exceptions.backend.finance.booking.not_found'));
    }



//Get first receipt from receipt for this booking Also can be used to check if already paid
    public function getFirstPaidReceipt($booking_id) {
        $receipt = $this->receipt->query()->where('iscancelled','=',0)->whereHas('receiptCodes', function ($query) use
            ($booking_id) {
                $query->where('booking_id', '=', $booking_id)->whereHas('finCode', function
                    ($query)    {
                        $query->where('fin_code_group_id','=',
                            $this->fin_code_groups->getContributionFinCodeGroupId());
                    });
            })->orderBy('rct_date', 'asc')->first();
        if (!is_null($receipt)) {
            return $receipt; //has receipt
        }else {
            return null;  //dont have receipt
        }
    }

    /*Get First Paid Receipt with Legacy*/
    public function getFirstPaidReceiptWithLegacy($booking_id) {
        $booking = $this->find($booking_id);
        $contrib_month = Carbon::parse($booking->rcv_date);
        $receipt = DB::table('employer_contributions')
        ->whereRaw("date_part('month', contrib_month) = " . $contrib_month->format('n'))
        ->whereRaw("date_part('year', contrib_month) = " . $contrib_month->format('Y'))
        // ->whereMonth('contrib_month','=', $contrib_month->format('n'))->whereYear('contrib_month','=',  $contrib_month->format('Y'))
        ->where('employer_id', $booking->employer_id)->orderBy('rct_date', 'asc')->first();
        if (!is_null($receipt)) {
            return $receipt; //has receipt
        }else {
            return null;  //dont have receipt
        }
    }

    /*Get total paid amount by booking id*/
    public function getPaidAmountWithLegacy($booking_id) {
        $booking = $this->find($booking_id);
        $contrib_month = Carbon::parse($booking->rcv_date);
        $paid_amount = DB::table('employer_contributions')
        ->whereRaw("date_part('month', contrib_month) = " . $contrib_month->format('n'))
        ->whereRaw("date_part('year', contrib_month) = " . $contrib_month->format('Y'))
        // ->whereMonth('contrib_month','=',  $contrib_month->format('n'))->whereYear('contrib_month','=',  $contrib_month->format('Y'))
        ->where('employer_id', $booking->employer_id)->sum('contrib_amount');
        return $paid_amount;
    }



    //    get booking amount
    public function getBookedAmount($booking_id) {
        $booked_amount = $this->query()->where('id','=', $booking_id)->first()->amount;
        return $booked_amount;
    }

//get all bookings with late submission or not submitted
    public function getBookingForPenalty($employer_id) {
        $bookings = Booking::where('employer_id','=',$employer_id)->whereRaw("rcv_date::date < ?",[standard_date_format(Carbon::now()->subMonth(1))])->whereDoesntHave('bookingInterest', function($query){
            $query->where('ispaid','=',1)
            ->orWhere('iswrittenoff','=',1);
        })->select(['id'])->get();
        return $bookings;
    }

    public function employerBooking($month, $year, $employer_id)
    {
        $booking = $this->query()->whereRaw("date_part('month', rcv_date) <= :month and date_part('year', rcv_date) <= :year and employer_id = :employer_id", ['month' => $month, 'year' => $year, 'employer_id' => $employer_id])->orderBy("rcv_date", "desc")->first();
        return $booking;
    }



    public function adjust($id, $input) {
        return DB::transaction(function () use ($id,$input) {
            $booking = $this->findOrThrowException($id);
            $adjust_amount = $input['new_booking_amount'] - $booking->amount;
            $booking->update(['adjust_amount'=>$adjust_amount]);
//            $this->checkIfDateRangeEligibleToBeWrittenOff($id, $input['min_date'], $input['max_date']);
//            $interest_written_off = $this->interest_write_offs->query()->create((['user_id' => access()->user()->id, 'description' => $input['comments']]));
//            $this->booking_interests->updateWriteOffId($id, $interest_written_off->id, $input['min_date'], $input['max_date']);
////       initiate workflow

        });
    }


    /**
     * @param $receipt_id
     * Delete booking created on receipting (not by system) when cancel receipt
     *
     */
    public function deleteWhenCancelReceipt($receipt_id)
    {
        $this->query()->where('is_schedule', 0)->whereHas('receiptCodes', function($query) use ($receipt_id)
        {
            $query->where('receipt_id', $receipt_id);
        })->whereDoesntHave('receiptCodes', function($query) use($receipt_id){
            $query->where('receipt_id','<>',$receipt_id)->has('receipt');
        })->delete();
    }


    /**
     * @param Model $booking
     * Get recent contribution amount from electronic receipt and legacy receipt.
     */
    public function getRecentContributionAmount($employer_id, $booking_date)
    {
        $recent_amount=0;

        $recent_amount  =  (new ReceiptCodeRepository())->getRecentContribution($employer_id, $booking_date);
        $employer = (new EmployerRepository())->find($employer_id);

        if($recent_amount == 0){
            /*get from legacy receipt*/
            $last_legacy_receipt =  (new LegacyReceiptRepository())->getLatestContributionByEmployer($employer);
            $recent_amount =(isset($last_legacy_receipt)) ? $last_legacy_receipt->contrib_amount : 0;
        }
        return $recent_amount;
    }


    /**
     * @param Model $employer
     * @param $rcv_date
     * @return bool
     * check if booking date for this employer is already paid in legacy
     */
    public function checkIfBookingDatePaidInLegacyReceipt(Model $employer, $rcv_date)
    {
        $rcv_date_parsed = Carbon::parse($rcv_date);
        $check_count = (new LegacyReceiptRepository())->query()
        ->whereRaw("date_part('month', contrib_month) = " . $rcv_date_parsed->format('m'))
        ->whereRaw("date_part('year', contrib_month) = " . $rcv_date_parsed->format('Y'))
        // ->whereMonth('contrib_month','=', $rcv_date_parsed->format('m'))->whereYear('contrib_month','=', $rcv_date_parsed->format('Y'))
        ->where('employer_id', $employer->id)->count();
        if($check_count > 0)
        {
            /*delete it from live booking which is not paid ye on electronic*/
            Booking::query()->where('employer_id', $employer->id)
            ->whereRaw("date_part('month', rcv_date) = " . $rcv_date_parsed->format('m'))
            ->whereRaw("date_part('year', rcv_date) = " . $rcv_date_parsed->format('Y'))
            // ->whereMonth('rcv_date','=', $rcv_date_parsed->format('m'))->whereYear('rcv_date','=', $rcv_date_parsed->format('Y'))
            ->whereDoesntHave('receiptCodes', function($query){
                $query->whereHas('receipt', function($query){
                    $query->where('iscancelled', 0);
                });
            })->delete();
            return true;
        }else{

            return false;
        }
    }



    /**
     * @param Model $date_array_y_m_d
     * @param $booking_date
     * Check if applicable month was in employer closure
     */
    public function checkIfMonthWasInEmployerClosure(Model $employer, $booking_date){
        if($employer->closures()->count() > 0) {
            $max_date = (comparable_date_format($booking_date) < comparable_date_format(Carbon::now())) ? standard_date_format($booking_date) : Carbon::now();
            $dormant_sub_type_cv_id = (new CodeValueRepository())->findIdByReference('ETCLDORMANT');
            $query = DB::table('main.employer_closures')->select('employer_closures.id')
            ->join('employers', 'employer_closures.employer_id', 'employers.id')
            ->where('employer_closures.employer_id', $employer->id)
            ->whereNull('employer_closures.deleted_at')
            ->where('isbookingskip', 1)
//            ->whereDate('employer_closures.close_date', '<=', standard_date_format(Carbon::parse($booking_date)->startOfMonth()))->whereRaw("coalesce(employer_closures.open_date, NOW()) >= ?", [standard_date_format($max_date)]);
                //TODO need to replace with internal dates after update is done.
            ->whereDate('employer_closures.close_date_internal', '<=', standard_date_format(Carbon::parse($booking_date)->startOfMonth()))->whereRaw("coalesce(employer_closures.open_date_internal, NOW()::date) > ?", [standard_date_format(Carbon::parse($max_date)->startOfMonth())]);


            $check = $query->count();
            if ($check > 0) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    }



    public function checkIfContribMonthPaidInReceiptCode(Model $employer, $booking_date)
    {
        $date_parsed = Carbon::parse($booking_date);
        $check = ReceiptCode::query()->has('activeReceipt')
        ->whereRaw("date_part('month', contrib_month) = " . $date_parsed->format('m'))
        ->whereRaw("date_part('year', contrib_month) = " . $date_parsed->format('Y'))
        // ->whereMonth('contrib_month','=',$date_parsed->format('m') )->whereYear('contrib_month','=', $date_parsed->format('Y'))
        ->count();

        if($check  > 0)
        {
            /*paid already*/
//            Booking::
        }
    }



    /*Get booking with duplicate entries*/
    public function getBookingWithDuplicateEntries()
    {
        $bookings = $this->query()->select('employer_id', 'rcv_date', DB::raw("COUNT(*)"))->groupBy('employer_id', 'rcv_date')->having(DB::raw("COUNT(*) > 1"))->get();
    }



    /*Get Booked amount on specified dates per employer contribution category i.e. Date is on contrib month*/
    public function getBookedAmountByEmployerCategoryForDateRange($employer_contrib_category,$start_date, $end_date)
    {
        $booking_amount = DB::table('bookings_mview')
        ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'bookings_mview.employer_id')
        ->whereRaw("rcv_date >= ? and rcv_date <= ?", [$start_date, $end_date])
        ->where('contributing_category_id', $employer_contrib_category)->sum('booked_amount');
        return $booking_amount;
    }

    /*Get total booked amount for medium and small*/
    public function getBookedAmountByEmployerCategoryMediumSmallForDateRange($start_date, $end_date)
    {
        $booking_amount = DB::table('bookings_mview')
        ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'bookings_mview.employer_id')
        ->whereRaw("rcv_date >= ? and rcv_date <= ?", [$start_date, $end_date])
        ->where('contributing_category_id', '>=', 3)->where('contributing_category_id', '<=', 4)->sum('booked_amount');
        return $booking_amount;
    }


    /*Check if Contrib Month is bookable*/
    public function checkIfContribMonthIsBookable($contrib_month)
    {
        $end_this_month = Carbon::parse(getTodayDate())->endOfMonth();
        if(comparable_date_format($contrib_month) <= comparable_date_format($end_this_month))
        {
            return true;
        }else{
            return false;
        }
    }



}