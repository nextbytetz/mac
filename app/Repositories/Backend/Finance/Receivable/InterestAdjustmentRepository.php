<?php
/**
 * Created by PhpStorm.
 * @Author: Martin L <m.luhanjo@nextbyte.co.tz>
 * Date: 02-May-17
 * Time: 12:56 PM
 */

namespace App\Repositories\Backend\Finance\Receivable;

use App\Exceptions\WorkflowException;
use App\Models\Finance\Receipt\Receipt;
use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Finance\Receivable\InterestAdjustment;
use App\Models\Finance\Receivable\InterestWriteOff;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InterestAdjustmentRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = InterestAdjustment::class;


    public function __construct(){

    }

    public function wfModuleGroupId()
    {
        return 33;
    }

    /*Store*/

    public function store(array $input){
        return  DB::transaction(function () use ($input) {
            $booking_interests = $this->getBookingInterestIdsByReceipt($input['receipt_id']);
            $interest_adjust  = $this->query()->create([
                'receipt_id' => $input['receipt_id'],
                'payment_date' => $input['payment_date'],
                'new_payment_date' => $input['new_payment_date'],
                'reason_type_cv_id' => $input['reason_type_cv_id'],
                'remark' => $input['remark'],
                'booking_interest_ids' => count($booking_interests) ? json_encode($booking_interests) : null,
                'user_id' => access()->id(),

            ]);

            return $interest_adjust;
        });
    }


    /*edit*/

    public function update(Model $interest_adjustment,  array $input){
        return  DB::transaction(function () use ($input, $interest_adjustment) {
            $booking_interests = $this->getBookingInterestIdsByReceipt($input['receipt_id']);
            $interest_adjustment->update([
                'payment_date' => $input['payment_date'],
                'new_payment_date' => $input['new_payment_date'],
                'reason_type_cv_id' => $input['reason_type_cv_id'],
                'remark' => $input['remark'],
                'booking_interest_ids' => count($booking_interests) ? json_encode($booking_interests) : null,

            ]);

            return $interest_adjustment;
        });
    }


    /**
     * @param $receipt_id
     * @return array
     * Get Booking interest ids by receipt
     */
    public function getBookingInterestIdsByReceipt($receipt_id)
    {
        $receipt = Receipt::query()->find($receipt_id);
        $employer_id = $receipt->employer_id;
        $receipt_codes = $receipt->receiptCodes()->where('receipt_codes.fin_code_id', 2)->get();
        $booking_interest_ids = [];
        foreach($receipt_codes as $receipt_code){
            $contrib_month = $receipt_code->contrib_month;
            $contrib_month_parsed = Carbon::parse($contrib_month);
            $booking_interest = BookingInterest::query()->whereHas('booking', function($q) use($employer_id){
                $q->where('employer_id', $employer_id);
            })->whereMonth('miss_month','=', $contrib_month_parsed->format('n'))->whereYear('miss_month','=', $contrib_month_parsed->format('Y'))->first();
            if($booking_interest){
                $array = [$booking_interest->id];
                $booking_interest_ids = array_merge($booking_interest_ids, $array);
            }
        }
        return $booking_interest_ids;
    }

    /*Booking interests adjusted for dt*/
    public function getInterestAdjustedForDt($interest_adjustment_id)
    {
        $interest_adjustment = $this->find($interest_adjustment_id);
        $booking_interest_ids = json_decode($interest_adjustment->booking_interest_ids);
        return (new BookingInterestRepository())->query()->whereIn('id', $booking_interest_ids);
    }


    /*CHeck if receipt has interest*/
    public function checkIfReceiptHasInterest($receipt_id)
    {
        $booking_interests = $this->getBookingInterestIdsByReceipt($receipt_id);
        if(count($booking_interests) == 0){
            throw new GeneralException('This receipt do not have interest(s) to adjust! Please check!');
        }
    }


    /**
     * @param $id
     * Updates on initiate workflow
     */
    public function initiateApproval($id)
    {
        $interest_adj = $this->find($id);
        $this->checkIfCanInitiateAdjustment($interest_adj);

//        $interest_adj->update([
//            'wf_status' => 1
//        ]);
        return $interest_adj;
    }

    /*Undo - delete*/
    public function undoInterestAdjustment(Model $interest_adjustment)
    {
        $interest_adjustment->delete();
    }


    /*Check if can initiate*/
    public function checkIfCanInitiateAdjustment($interest_adj)
    {
        $pending_tasks = $this->getPendingTasksBeforeInitiate($interest_adj);
        if(count($pending_tasks))
        {
            throw new WorkflowException('Complete all pending tasks to proceed! Please check');
        }
    }

    /**
     * @param $interest_adjustment_id
     * Update on workflow complete
     */
    public function updateOnWfComplete($interest_adjustment_id)
    {
        DB::transaction(function () use ($interest_adjustment_id) {
            $interest_adj = $this->find($interest_adjustment_id);
            $new_payment_date = $interest_adj->new_payment_date;
            $payment_date = $interest_adj->payment_date;
            $receipt_id = $interest_adj->receipt_id;
            $interests = $this->getInterestAdjustedForDt($interest_adjustment_id)->get();
            /*Update adjustment for each interest*/
            foreach ($interests as $booking_interest) {
                if($booking_interest->isadjusted == 0){
                    /*update only if not adjusted*/
                    $interest_details = $booking_interest->getNewInterestDetailsByNewPayDateAttribute($new_payment_date);
                    $new_interest = $interest_details['interest_amount'];
                    $adjusted_amount = $new_interest - $booking_interest->amount;
                    $late_months = $interest_details['late_months'];
                    $booking_interest->update(['isadjusted' => 1,
                        'amount' =>$new_interest,
                        'adjust_amount' =>$adjusted_amount,
                        'late_months' => $late_months,
                        'adjust_note' =>$interest_adj->remark,
                        'adjust_user_id' =>$interest_adj->user_id,
                        'new_payment_date' =>$new_payment_date,
                        'reason_type_cv_id' =>$interest_adj->reason_type_cv_id,
                        'old_late_months' =>$booking_interest->late_months,
                        'payment_date_old' =>$payment_date,
//                    'wf_done' =>1,
//                    'wf_done_date' =>standard_date_format(getTodayDate()),
                    ]);

                }


            }

            //update receipt table receipt date
            $receipts = new ReceiptRepository();
            $receipts->updatePaymentDate($receipt_id, $new_payment_date);
        });

    }




    /**
     * @param $employer_closure
     * @return int
     * Get module type for workflow when initiating approval workflow
     */
    public function getWfModuleType(Model $interest_adjustment)
    {
        $type = 1;

        return $type;
    }



    /*Get pending tasks before initiate*/
    public function getPendingTasksBeforeInitiate(Model $interest_adjustment)
    {
        $pending_tasks = [];
        $doc_attached = $this->getDocsAttachedForAdjustment($interest_adjustment->id);
        if($doc_attached->count() == 0){
            $pending_tasks[] = 'Supporting document not attached';
        }
        return $pending_tasks;
    }


    /*get document attached*/
    public function getDocsAttachedForAdjustment($interest_adjustment_id)
    {
        $interest_adj = $this->find($interest_adjustment_id);
        $employer = $interest_adj->receipt->employer;
        $docs_all = (new DocumentRepository())->queryDocumentsByGroup(30)->pluck('id')->toArray();
        $docs_attached = $employer->documents()->where('external_id', $interest_adjustment_id)->whereIn('document_id',$docs_all)->get();
        return $docs_attached;
    }

    /*check and validate if document fo adjustment have been uploaded*/
    public function validateOnDocumentsForAdjust($interest_adjustment_id)
    {
        $docs_attached =$this->getDocsAttachedForAdjustment($interest_adjustment_id);
        if($docs_attached->count() > 0){
            /*Attached - ok*/
        }else{
            throw  new GeneralException('Attach Supporting Document to proceed with Interest adjustment');
        }
    }
    /**
     *
     * Start: Letter Methods
     */

    /*Check if can initiate wf letter*/
    public function checkIfCanInitiateLetterWf($current_track)
    {
        $return = false;
        if(isset($current_track)){
            $wf_definition = $current_track->wfDefinition;
            $current_level = $wf_definition->level;
            $wf_module = $wf_definition->wfModule;
            $wf_module_id = $wf_module->id;
            $interest_adj = $this->find($current_track->resource_id);
            $wf_module_group_id = $this->wfModuleGroupId();
            $type = $wf_module->type;
            $level_for_letter = $this->getLevelForLetterByWfModule($wf_module_id);
            $access = access()->user()->hasWorkflowModuleDefinition($wf_module_group_id, $type, $level_for_letter);

            if($current_level == $level_for_letter && $access == true && $interest_adj->wf_done == 0)
//            if($current_level == $level_for_letter && $access == true && $arrears == 0)
            {
                //Check if was declined on approval level
                $check_if_declined = (new WfTrackRepository())->checkIfModuleDeclined($current_track->resource_id, $wf_module->id);
                if($check_if_declined == false){
                    //only if approved
                    $return = true;
                }
            }
        }
        return $return;
    }


    /*Get level For Letter*/
    public function getLevelForLetterByWfModule($wf_module_id)
    {
        $module_repo = new WfModuleRepository();
        switch($wf_module_id){
            case $module_repo->interestAdjustmentModules()[0]://68
                return 6;
                break;

        }
    }

    /*Check if wf letter is initiated*/
    public function checkIfWfLetterIsInitiated(Model $interest_adjustment, $current_wf_track)
    {
        if($interest_adjustment->adjustmentResponseLetter()->where('isinitiated','<>', 0)->count() == 0 && $this->checkIfCanInitiateLetterWf($current_wf_track) == true)
        {
            throw new WorkflowException('Workflow for letter not initiated! Initiate to proceed!');
        }
        return true;
    }

    /*Process actions at levels before on approval before wf complete*/
    public function processWfActionAtLevels($current_wf_track, array $input = null)
    {
        DB::transaction(function () use ($current_wf_track, $input) {
            $wf_module_repo = new WfModuleRepository();
            $resource_id = $current_wf_track->resource_id;
            $wf_module_id = $current_wf_track->wfDefinition->wf_module_id;
            $wf_definition = $current_wf_track->wfDefinition;
            $level = $wf_definition->level;
            $interest_adjustment = $this->find($resource_id);
            $is_approved = ($current_wf_track->status == 1) ? 1 : 0;

            switch ($wf_module_id) {
                case $wf_module_repo->interestAdjustmentModules()[0]://68
                    switch ($level) {
                        case 5:
                            /*Approval Level*/
                            if($is_approved == 1){
                                $this->updateOnWfComplete($resource_id);
                            }

                            break;
                        case 6:
                            /*level for letter*/
                            $this->checkIfWfLetterIsInitiated($interest_adjustment, $current_wf_track);

                            break;
                    }

                    break;

            }
        });

    }



    /**
     * @param $letter_cv_reference
     * Get Previous letter
     */
    public function getPreviousLetter($letter_cv_reference)
    {
        $cv_repo = new CodeValueRepository();
        $cv_id = $cv_repo->findIdByReference($letter_cv_reference);
        $prev_letter = (new LetterRepository())->queryLettersByReference($cv_id)->orderBy('letters.id', 'desc')->first();
        return $prev_letter;
    }

    /*Get Letter reference No. attribute*/
    public function getLetterReferenceNo($letter_cv_reference)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference);
        $prev_reference = (isset($prev_letter)) ? $prev_letter->reference : null;
        $next_folio_no = $this->getLetterFolioNo($letter_cv_reference);
        $reference_no = '';
        $default = 'AC/INT/000/';//TODO need to update <deploy_interest>
        $reference_no = $this->getFullReferenceNo($prev_reference, $default, $next_folio_no)  ;

        return $reference_no;
    }


    /*Prefix of letter reference no*/
    public function getFullReferenceNo($prev_reference, $default_ref, $next_folio_no)
    {
        if(isset($prev_reference)){
            $ref_no =  remove_after_last_this_character('/',$prev_reference) . '/';
        }else{
            $ref_no = $default_ref;
        }
        return $ref_no  . $next_folio_no;
    }

    /*Letter folio no*/
    public function getLetterFolioNo($letter_cv_reference)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference);
        if(isset($prev_letter)){
            $resources =  json_decode($prev_letter->resources, true);
            $next_folio_no =  $resources[0]['value'] + 1;
        }else{
            $next_folio_no = 1;
        }
        return $next_folio_no;
    }




}