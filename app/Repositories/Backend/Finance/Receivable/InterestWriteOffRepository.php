<?php
/**
 * Created by PhpStorm.
 * @Author: Martin L <m.luhanjo@nextbyte.co.tz>
 * Date: 02-May-17
 * Time: 12:56 PM
 */

namespace App\Repositories\Backend\Finance\Receivable;

use App\Models\Finance\Receivable\InterestWriteOff;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use Carbon\Carbon;

class InterestWriteOffRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = InterestWriteOff::class;


    public function __construct(){

    }


    //find or throwexception for booking
    public function findOrThrowException($id)
    {
        $interest_write_off = $this->query()->find($id);
        if (!is_null($interest_write_off)) {
            return $interest_write_off;
        }
        throw new GeneralException(trans('exceptions.backend.finance.booking.interest_write_off_not_found'));
    }

    /*
 *check if interest write off is written off  (if is complete)->completed workflow
 */

    public function checkIfIsWrittenOff($id)
    {
        $interest_write_off = $this->query()->find($id)->bookingInterests()->where('iswrittenoff', '=', 1)->first();
        if (!is_null($interest_write_off)) {
            return 1; // written off
        }
        return 0; // not written off yet
    }


    /*
 * get all interest write offs belong to this employer
 */
    public function getInterestWriteOffs($employer_id)
    {
        $interestWriteOffs = $this->query()->whereHas("bookingInterests", function ($query) use
        ($employer_id) {
            $query->whereHas('booking', function ($query2) use ($employer_id) {
                $query2->where(['employer_id' => $employer_id]);
            });
        })->get();
        return $interestWriteOffs;
    }


}