<?php

namespace App\Repositories\Backend\Finance;

use App\Models\Finance\FinYear;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class FinYearRepository
{

    const MODEL = FinYear::class;

    /**
     * @return mixed
     */
    public function query()
    {
        return call_user_func(static::MODEL.'::query');
    }


    /*Get quarter months by quarter*/
    public function getQuarterMonths($quarter)
    {
        switch ($quarter){
            case 1:
                return [7,8,9];
                break;

            case 2:
                return [10,11,12];
                break;

            case 3:
                return [1,2,3];
                break;

            case 4:
                return [4,5,6];
                break;
        }
    }


    /**
     * @param $fin_year_id
     * @param $quarter
     * Get date ranges from quarter fin year
     */
    public function getDateRangesFromQuarterFinYear($fin_year_id, $quarter)
    {
        $fin_year = $this->query()->where('id', $fin_year_id)->first();

        $quarter_months = $this->getQuarterMonths($quarter);
        if($quarter < 3){
            /*q 1,2*/
            $start_year = $fin_year->start;
            $end_year = $fin_year->start;
        }else{
            $start_year = $fin_year->end;
            $end_year = $fin_year->end;
        }

        $min_date = '1-' . min($quarter_months) . '-' . $start_year;
        $max_date = '1-' . max($quarter_months) . '-' . $end_year;

        $start_date = Carbon::parse($min_date)->startOfMonth();
        $end_date = Carbon::parse($max_date)->endOfMonth();

        return [
            'start_date' => standard_date_format($start_date),
            'end_date' => standard_date_format($end_date),
        ];
    }


    public function findByDate($date)
    {
        $fin_year = $this->query()->whereDate('start_date','<=', $date)->whereDate('end_date','>=', $date)->first();
        return $fin_year;
    }

}