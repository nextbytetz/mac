<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\NotificationLetter;
use App\Repositories\BaseRepository;

class NotificationLetterRepository extends BaseRepository
{
    const MODEL = NotificationLetter::class;

}