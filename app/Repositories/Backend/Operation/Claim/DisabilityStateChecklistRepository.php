<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\DisabilityStateChecklist;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class DisabilityStateChecklistRepository extends  BaseRepository
{

    const MODEL = DisabilityStateChecklist::class;

    public function __construct()
    {

    }

//find or throwexception for notification report
    public function findOrThrowException($id)
    {
        $disability_state_checklist = $this->query()->find($id);

        if (!is_null($disability_state_checklist)) {
            return $disability_state_checklist;
        }
        throw new GeneralException(trans('exceptions.backend.claim.disability_state_checklist_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }

    public function hospitilization()
    {

    }

    public function getActiveModel()
    {
        return $this->query()->whereNotIn("id", [4]);
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->getActiveModel()->get();
    }

    /**
     * @return mixed
     */
    public function getActiveCount()
    {
        return $this->query()->whereNotIn("id", [4])->count();
    }

}