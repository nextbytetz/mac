<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\Pd\Occupation;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class OccupationRepository extends BaseRepository
{
    const MODEL = Occupation::class;

    /**
     * @param $q
     * @param $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPdOccupations($q, $page)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = trim(preg_replace('/\s+/', '', $q));
        $data['items'] = $this->query()
            ->select([
                DB::raw("id"),
                DB::raw("name || ' - ( ' || industry || ' )' as name"),
            ])
            ->whereRaw("regexp_replace(cast(concat_ws(' ', name, industry) as text), '\s+', '', 'g')  ~* ? ", [$name])
            ->limit($resultCount)
            ->offset($offset)
            ->get()->toArray();
            $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

}