<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Exceptions\GeneralValidationException;
use App\Models\Operation\Claim\AccidentType;
use App\Models\Operation\Claim\AssessmentDocument;
use App\Models\Operation\Claim\Claim;
use App\Models\Operation\Claim\DeathCause;
use App\Repositories\Backend\Operation\Claim\Pd\PdAgeAdjustmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdAgeRangeRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdFecRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdInjuryRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdOccupationAdjustmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdOccupationCharacteristicRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClaimRepository extends  BaseRepository
{

    const MODEL = Claim::class;

    protected  $notification_disability_state_assessments;

    public function __construct()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->notification_disability_state_assessments = new NotificationDisabilityStateAssessmentRepository();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $claim = $this->query()->find($id);

        if (!is_null($claim)) {
            return $claim;
        }
        throw new GeneralException(trans('exceptions.backend.claim.claim_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }

    public function updateOrCreate($input) {

        $claim = $this->query()->updateOrCreate(
            ['notification_report_id' => $input['notification_report_id']] , ['user_id' => access()->user()->id, 'monthly_earning' => $input['monthly_earning'], 'contrib_month' => $input['contrib_month'], 'incident_type_id' => $input['incident_type_id'], 'exit_code_id'=>$input['exit_code']] );
        return $claim;
    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     * @throws GeneralException
     * Update PD information into claim i.e. pd% and date of mmi if Applicable.
     */
    public function updatePd($id,$input) {
        $pd = $input['pd'];
        $date_of_mmi = null;
        if ($pd > sysdefs()->data()->percentage_imparement_scale){
            $date_of_mmi = $input['date_of_mmi'];
        }

        $claim =  $this->findOrThrowException($id)->update(['pd' => $pd, 'date_of_mmi' => $date_of_mmi]);
        return $claim;
    }



    /**
     * @param $medical_expense
     * @param $input
     * @return mixed
     * Claim Assessment
     */
    public function claimAssessment($medical_expense, $input) {
        return DB::transaction(function () use ($medical_expense, $input) {
            $claim = $medical_expense->notificationReport->claim;

            //Notification Disability State Assessment
            foreach ($input as $key => $value) {
                if (strpos($key, 'days') !== false) {
                    $notification_disability_state_id = substr($key, 4);
                    $this->notification_disability_state_assessments->updateOrCreate($notification_disability_state_id, $input);
                }
            }

            //Update PI claim
            if (isset($input['pd'])) {
                $this->updatePd($claim->id, $input);
            }
            //update claim - exit code

            return $medical_expense->notificationReport;

        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @param array $input
     * @return mixed
     */
    public function updatePdAssessment(Model $incident, Model $eligible, array $input)
    {
        return DB::transaction(function () use ($incident, $eligible, $input) {

            $claim = $incident->claim;

            //Update PI claim
            if (isset($input['pd'])) {
                $this->updatePd($claim->id, $input);
            }

            //Save need_rehabilitation, need_cca
            $claim->need_rehabilitation = isset($input['need_rehabilitation']) ? $input['need_rehabilitation'] : 0;
            $claim->need_cca = isset($input['need_cca']) ? $input['need_cca'] : 0;
            $claim->diagnosis = $input['diagnosis'];
            $claim->assessed = 1;
            $claim->save();

            $eligible->assessed = 1;
            $eligible->save();

            //update on notification report
            $incident->nature_of_incident_cv_id = $input['nature_of_incident_cv_id'];
            $incident->save();
            //Synchronize Lost Body Parts
            if ($input['lost_body_part']) {
                //Save lost body parts
                $incident->lostBodyParties()->sync($input['lost_body_part_id']);
            } else {
                $incident->lostBodyParties()->sync([]);
            }
            //Synchronize Body Part Injury
            if (isset($input['body_part_injury_id'])) {
                $incident->injuries()->sync($input['body_part_injury_id']);
            } else {
                $incident->injuries()->sync([]);
            }

            switch ($incident->incident_type_id) {
                case 1:
                    //Accident
                    $accident = $incident->accident;
                    $data = [
                        'lost_body_part' => $input['lost_body_part'],
                    ];
                    $accident->update($data);
                    break;
                case 2:
                    //Disease
                    $disease = $incident->disease;
                    $data = [
                        'lost_body_part' => $input['lost_body_part'],
                    ];
                    $disease->update($data);
                    break;
            }

            //Save assessment parameters
            (new IncidentAssessmentRepository())->query()->updateOrCreate(
                ['notification_eligible_benefit_id' => $eligible->id],
                [
                    'dob' => $input['dob'],
                    'age' => $input['old_val'],
                    'pd_impairment_id' => (isset($input['pd_impairment_id'])) ? $input['pd_impairment_id'] : NULL,
                    'occupation_id' => (isset($input['occupation_id'])) ? $input['occupation_id'] : NULL,
                    'wpi' => (isset($input['wpi'])) ? $input['wpi'] : NULL,
                    'pd_injury_id' => (isset($input['pd_injury_id'])) ? $input['pd_injury_id'] : NULL,
                    'pd_amputation_id' => (isset($input['pd_amputation_id'])) ? $input['pd_amputation_id'] : NULL,
                    'rating' => $input['pd'],
                    'method' => $input['computation_method'],
                    'pd_comments' => $input['pd_comments'],
                ]
            );

            if ($input['pd'] > 0) {
                //create a pd benefit
                /*$incident->benefits()->updateOrCreate(
                    [
                        'notification_report_id' => $incident->id,
                        'benefit_type_claim_id' => 5
                    ],
                    [
                        'member_type_id' => 2,
                        'resource_id' => $incident->employee_id,
                        'user_id' => access()->id(),
                        'parent_id' => NULL,
                    ]
                );*/
            } else {
                //delete a pd benefit
                //$incident->benefits()->where("benefit_type_claim_id", 5)->delete();
            }
            return true;
        });
    }

    /**
     * @param Model $eligible
     * @param array $input
     * @return mixed
     */
    public function addAssessmentProgressive(Model $eligible, array $input)
    {
        return DB::transaction(function () use ($eligible, $input) {
            //check for documents
            $count = AssessmentDocument::query()->where('notification_eligible_benefit_id', $eligible->id)->count();
            if (!$count) {
                throw new GeneralValidationException("Please add at least one document reference");
            }
            //notification_disability_state => nds
            //notification_disability_state_assessments => ndsa
            $ndsRepo = new NotificationDisabilityStateRepository();
            $ndsaRepo = new NotificationDisabilityStateAssessmentRepository();

            /*if ($input['days_manipulated']) {
                $from_date_carbon = Carbon::parse($input['from_date']);
                $to_date_carbon = Carbon::parse($input['to_date']);
            } else {
                $from_date_carbon = NULL;
                $to_date_carbon = NULL;
            }*/
            //$days = $to_date_carbon->diffInDays($from_date_carbon) + 1; //todo: MAY NEED CROSS CHECK
            $days = $input['days']; //todo: MAY NEED CROSS CHECK

            $user_id = access()->id();

            $countModel = $ndsRepo->query()->where([
                'disability_state_checklist_id' => $input['disability_state_checklist_id'],
                'notification_eligible_benefit_id' => $eligible->id,
            ]);
            if ($countModel->count()) {
                $nds = $countModel->first();
            } else {
                $disability_state_checklist = (new DisabilityStateChecklistRepository())->find($input['disability_state_checklist_id']);
                $nds = $ndsRepo->query()->create(
                    [
                        'disability_state_checklist_id' => $input['disability_state_checklist_id'],
                        'notification_eligible_benefit_id' => $eligible->id,
                        'percent_of_ed_ld' => $disability_state_checklist->percent_of_ed_ld,
                        'from_date' => $input['from_date'],
                        'to_date' => $input['to_date'],
                        'days' => $days,
                        'user_id' => $user_id,
                        'isassessmentregister' => 1,
                        'autodate' => 1,
                    ]
                );
            }
            if ($input['days']) {
                //check existing disability date range
                $incident = $eligible->incident;
                $countModelCheck = $ndsaRepo->query()
                    ->select([
                        "notification_disability_states.notification_eligible_benefit_id",
                        "disability_state_checklists.name",
                        DB::raw("concat_ws(' ', 'from date', notification_disability_state_assessments.from_date, 'to date', notification_disability_state_assessments.to_date) as overlap")
                    ])
                    ->join(DB::raw("notification_disability_states"), 'notification_disability_states.id', "=", 'notification_disability_state_assessments.notification_disability_state_id')
                    ->join(DB::raw("disability_state_checklists"), 'disability_state_checklists.id', "=", 'notification_disability_states.disability_state_checklist_id')
                    ->where("notification_disability_state_assessments.hassetrange", 1)
                    ->whereHas("notificationDisabilityState", function ($query) use ($input, $incident) {
                        $query->whereHas("benefit", function ($query) use ($incident) {
                            $query->where("notification_report_id", $incident->id);
                        });
                    })
                    ->where(function($query) use ($input) {
                        $query->whereRaw("'{$input['from_date']}' between notification_disability_state_assessments.from_date and notification_disability_state_assessments.to_date or '{$input['to_date']}' between notification_disability_state_assessments.from_date and notification_disability_state_assessments.to_date");
                    });
                //logger($countModelCheck->toSql());
                $countModel = $countModelCheck->first();
                if ($countModel) {
                    throw new GeneralValidationException("Specified date overlap previous date for {$countModel->notification_eligible_benefit_id} : {$countModel->name} {$countModel->overlap}");
                }
            }

            $ndsa = $ndsaRepo->query()->create([
                'notification_disability_state_id' => $nds->id,
                'days' => $days,
                'user_id' => $user_id,
                'remarks' => $input['remarks'],
                'from_date' => $input['from_date'],
                'to_date' => $input['to_date'],
                'hassetrange' => 1,
            ]);

            $eligible->assessed = 1;
            $eligible->save();

            return $ndsa;
        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @param array $input
     * @return mixed
     */
    public function claimAssessmentProgressive(Model $incident, Model $eligible, array $input)
    {
        return DB::transaction(function () use ($incident, $eligible, $input) {

            $claim = $incident->claim;

            //Notification Disability State Assessment
            foreach ($input as $key => $value) {
                if (strpos($key, 'assessed_days') !== false) {
                    $notification_disability_state_id = substr($key, 13);
                    $this->notification_disability_state_assessments->updateOrCreateProgressive($notification_disability_state_id, $input);
                }
            }

            $eligible->assessed = 1;
            $eligible->save();

            //Update PI claim
            /*if (isset($input['pd'])) {
                $this->updatePd($claim->id, $input);
            }*/

            /*if ($input['pd'] > 0) {
                //create a pd benefit
                $incident->benefits()->updateOrCreate(
                    [
                        'notification_report_id' => $incident->id,
                        'benefit_type_claim_id' => 5
                    ],
                    [
                        'member_type_id' => 2,
                        'resource_id' => $incident->employee_id,
                        'user_id' => access()->id(),
                        'parent_id' => $eligible->id,
                    ]
                );
            } else {
                //delete a pd benefit
                $incident->benefits()->where("benefit_type_claim_id", 5)->delete();
            }*/

            return true;
        });
    }

    /*
         * @param array $input
         * @return mixed
         */
    public function scheduleOneCalculator(array $input)
    {
        return DB::transaction(function () use ($input) {
            try {
                $wpi = $input['wpi'];
                $pd_injury_id = $input['pd_injury_id'];
                $occupation_id = $input['occupation_id'];
                $pd_impairment_id = $input['pd_impairment_id'];
                $age = $input['age'];

                $rank = (new PdInjuryRepository())->query()->select(["rank"])->where("id", $pd_injury_id)->first()->rank;
                $rating = (new PdFecRepository())->query()->select(["rating"])->where(["rank" => $rank, "wpi" => $wpi])->first()->rating;
                $group = (new OccupationRepository())->query()->select(["groupno"])->where("id", $occupation_id)->first()->groupno;
                $characteristic = (new PdOccupationCharacteristicRepository())->query()->select(["characteristic"])->where(["pd_impairment_id" => $pd_impairment_id, "group" => $group])->first()->characteristic;
                $rating_2 = (new PdOccupationAdjustmentRepository())->query()->select(["rating"])->where(["characteristic" => $characteristic, "prev_rating" => $rating])->first()->rating;
                $pd_age_range_id = (new PdAgeRangeRepository())->query()->select(["id"])->whereRaw('? between "from" and "to"', [$age])->first()->id;
                $rating_3 = (new PdAgeAdjustmentRepository())->query()->select(["rating"])->where(["prev_rating" => $rating_2, "pd_age_range_id" => $pd_age_range_id])->first()->rating;
                $return = $rating_3;
                $derived = "{$wpi}%wpi--FEC-->{$rating}%--OCCUPATION-->{$rating_2}%--AGE-->{$rating_3}% Disability.";
            } catch (\Exception $e) {
                Log::error($e);
                $return = "";
                $derived = "";
            }

            $return = ['value' => $return, 'derived' => $derived];
            //logger($return);

            return $return;

        });
    }




    /**
     * Get claims which need Constant care assistant and not yet added
     */
    public function getClaimsPendingCcaForDataTable()
    {
        $benefit_type_claim_id = 5; //For PD
        return $this->query()->where('need_cca', 1)->where('cca_registered', 0)->whereHas('notificationReport', function ($query) use($benefit_type_claim_id){
            $query->whereHas('benefits', function ($query)use($benefit_type_claim_id){
//                $query->where('benefit_type_claim_id', $benefit_type_claim_id)->where('processed',1);
                $query->where('benefit_type_claim_id', $benefit_type_claim_id)->where('ispayprocessed', 1);
            });
        });
    }


}