<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\Insurance;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class InsuranceRepository extends  BaseRepository
{

    const MODEL = Insurance::class;

    public function __construct()
    {

    }

//find or throwexception
    public function findOrThrowException($id)
    {
        $insurance = $this->query()->find($id);

        if (!is_null($insurance)) {
            return $insurance;
        }
        throw new GeneralException(trans('exceptions.backend.claim.insurance_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {
        return DB::transaction(function () use ($input) {
            $insurance = $this->query()->create([
                'name' => $input['name'],
                'external_id' => $input['external_id'],
                'user_id' => $input['user_id'],
            ]);
            return $insurance;
        });
    }
    /*
     * update
     */
    public function update($id,$input) {
        return DB::transaction(function () use ($id, $input) {
            $insurance = $this->findOrThrowException($id);
            $insurance->update([
                'name' => $input['name'],
                'external_id' => $input['external_id'],
                'user_id' => $input['user_id'],
            ]);
            return $insurance;
        });
    }



    /**
     * @param $id
     * @param $input
     * Update bank details from Notification Report profile
     */
    public function updateMultipleInsurancesBankDetails($input){
        foreach ($input as $key => $value) {
            if (strpos($key, 'insurance_bank_id') !== false) {
                $insurance_id = substr($key, 17);
                $insurance = $this->findOrThrowException($insurance_id);
                $insurance->update(['bank_branch_id'=> (isset($input['insurance_bank_branch_id'.$insurance_id]) ? $input['insurance_bank_branch_id'.$insurance_id] : null),'accountno'=> (isset($input['insurance_accountno'.$insurance_id]) ? $input['insurance_accountno'.$insurance_id] : null)]);
//                    }
            }
        }

    }

    /*Get insurances for dataTable*/
    public function getForDataTable()
    {
        return $this->query();
    }



}