<?php

namespace App\Repositories\Backend\Operation\Claim;


use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\Accident;
use App\Models\Operation\Claim\AccidentWitness;
use App\Models\Operation\Claim\Witness;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WitnessRepository extends  BaseRepository
{

    const MODEL = Witness::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $witness = $this->query()->find($id);
        if (!is_null($witness)) {
            return $witness;
        }
        throw new GeneralException(trans('exceptions.backend.claim.witness_not_found'));
    }

    /**
     * @param $notification_report_id
     * @param $input
     * @return mixed
     */
    public function create($notification_report_id, $input)
    {
        return DB::transaction(function () use ($notification_report_id, $input)  {
            $witness = $this->query()->create([
                'name' => $input['witness_name'],
                'phone' => (($input['witness_phone']) ? phone_255($input['witness_phone']) : NULL),
                'supervisor' => $input['witness_supervisor_name'],
                'supervisor_contact' => (($input['witness_supervisor_phone']) ? phone_255($input['witness_supervisor_phone']) : NULL),
                'supervisor_unit' => $input['witness_supervisor_unit'],
                'user_id' => access()->id(),
            ]);
            $notificationRepo = new NotificationReportRepository();
            $notification_report = $notificationRepo->findOrThrowException($notification_report_id);
            $notification_report->accident->witnesses()->attach($witness->id, [
                'user_id' => access()->id(),
            ]);
            return $witness;
        });
    }

    public function createFromOnline($notification_report_id, $input)
    {
        return DB::transaction(function () use ($notification_report_id, $input)  {
            $witness = $this->query()->create([
                'name' => $input['witness_name'],
                'phone' => $input['witness_phone'],
                'user_id' => access()->id(),
            ]);
            $notificationRepo = new NotificationReportRepository();
            $notification_report = $notificationRepo->findOrThrowException($notification_report_id);
            $notification_report->accident->witnesses()->attach($witness->id, [
                'user_id' => access()->id(),
            ]);
            return $witness;
        });
    }

    /**
     * @param Model $witness
     * @param $input
     * @return mixed
     */
    public function update(Model $witness, $input)
    {
        return DB::transaction(function () use ($witness, $input)  {
            $witness->update([
                'name' => $input['witness_name'],
                'phone' => phone_255($input['witness_phone']),
                'supervisor' => $input['witness_supervisor_name'],
                'supervisor_contact' => phone_255($input['witness_supervisor_phone']),
                'supervisor_unit' => $input['witness_supervisor_unit'],
                'user_id' => access()->id(),
            ]);
        });
    }

    /**
     * @param Model $witness
     * @param $accident_id
     * @return Model
     */
    public function delete(Model $witness, $accident_id)
    {
        //delete accident witness
        $witness->accidents()->detach($accident_id);
        return $witness;
    }

}