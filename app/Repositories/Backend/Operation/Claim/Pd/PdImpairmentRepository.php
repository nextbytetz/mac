<?php

namespace App\Repositories\Backend\Operation\Claim\Pd;

use App\Models\Operation\Claim\Pd\PdImpairment;
use App\Repositories\BaseRepository;

class PdImpairmentRepository extends BaseRepository
{
    const MODEL = PdImpairment::class;


}