<?php
namespace App\Repositories\Backend\Operation\Claim\Pd;

use App\Models\Operation\Claim\Pd\PdAgeAdjustment;
use App\Repositories\BaseRepository;

class PdAgeAdjustmentRepository extends BaseRepository
{
    const MODEL = PdAgeAdjustment::class;

}