<?php

namespace App\Repositories\Backend\Operation\Claim\Pd;

use App\Models\Operation\Claim\Pd\PdAgeRange;
use App\Repositories\BaseRepository;

class PdAgeRangeRepository extends BaseRepository
{
    const MODEL = PdAgeRange::class;
}