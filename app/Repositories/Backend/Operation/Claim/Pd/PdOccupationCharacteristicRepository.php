<?php

namespace App\Repositories\Backend\Operation\Claim\Pd;

use App\Models\Operation\Claim\Pd\PdOccupationCharacteristic;
use App\Repositories\BaseRepository;

class PdOccupationCharacteristicRepository extends BaseRepository
{
    const MODEL = PdOccupationCharacteristic::class;

}