<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\AccidentType;
use App\Models\Operation\Claim\DeathCause;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class DeathCauseRepository extends  BaseRepository
{

    const MODEL = DeathCause::class;

    public function __construct()
    {

    }

//find or throwexception for notification report
    public function findOrThrowException($id)
    {
        $death_cause = $this->query()->find($id);

        if (!is_null($death_cause)) {
            return $death_cause;
        }
        throw new GeneralException(trans('exceptions.backend.claim.death_cause_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}