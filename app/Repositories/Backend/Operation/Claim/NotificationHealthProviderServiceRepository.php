<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\NotificationHealthProviderService;
use App\Repositories\BaseRepository;

class NotificationHealthProviderServiceRepository extends  BaseRepository
{

    const MODEL = NotificationHealthProviderService::class;
    protected $notification_health_providers;
    public function __construct()
    {
        $this->notification_health_providers = new NotificationHealthProviderRepository();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $notification_health_provider_service= $this->query()->find($id);

        if (!is_null($notification_health_provider_service)) {
            return $notification_health_provider_service;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_health_provider_service_not_found'));
    }
    /*
     * create new
     */

    public function create($notification_health_provider_id,$health_service_checklist_id, $input) {
        $notification_report_provider_service = $this->query()->create(['notification_health_provider_id' => $notification_health_provider_id, 'health_service_checklist_id' => $health_service_checklist_id, 'feedback'=> !is_null($input['feedback'.$health_service_checklist_id]) ? $input['feedback'.$health_service_checklist_id] : 0,   'amount'=> ($input['feedback'.$health_service_checklist_id] == 1) ? str_replace(",", "", $input['amount'.$health_service_checklist_id]) : 0, 'from_date'=> $input['from_date'.$health_service_checklist_id], 'to_date'=> $input['to_date'.$health_service_checklist_id], 'user_id'=>access()->user()->id]);
        return $notification_report_provider_service;
    }
    /*
     * update
     */
    public function update($notification_health_provider_id, $health_service_checklist_id, $input) {
        $notification_report_provider_service =   $this->query()->where('notification_health_provider_id',$notification_health_provider_id)->where('health_service_checklist_id',$health_service_checklist_id)->first();

        if ($notification_report_provider_service){
            $notification_report_provider_service_id = $notification_report_provider_service->id;
            $notification_report_provider_service = $this->query()->where('notification_health_provider_id',$notification_health_provider_id)->where('health_service_checklist_id',$health_service_checklist_id)->update([ 'feedback'=> $input['feedback'.$health_service_checklist_id] ,   'amount'=> ($input['feedback'.$health_service_checklist_id] == 1) ? str_replace(",", "", $input['amount'.$health_service_checklist_id]) : 0, 'from_date'=> $input['from_date'.$health_service_checklist_id], 'to_date'=> $input['to_date'.$health_service_checklist_id]]);

            //delete if is feedback is set to 0
            $this->deleteIfFeedbackIsZero($notification_report_provider_service_id);

        } else {
            if ($input['feedback'. $health_service_checklist_id] == 1) {
                $this->create($notification_health_provider_id, $health_service_checklist_id, $input);
            }
        }
    }

    public function deleteIfFeedbackIsZero($notification_report_provider_service_id)
    {
        $updated_notification_report_provider_service= $this->query()->where('id',$notification_report_provider_service_id)->where('feedback',0)->first();

        if ($updated_notification_report_provider_service){
            $notification_report_provider_id = $updated_notification_report_provider_service->notification_health_provider_id;
            $updated_notification_report_provider_service->delete();
            //Delete if no health service for this provider
            $this->notification_health_providers->deleteIfNoHealthServices($notification_report_provider_id);
        }

    }



}