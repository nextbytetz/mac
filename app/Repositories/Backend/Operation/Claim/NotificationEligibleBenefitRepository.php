<?php
namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\AssessmentDocument;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\DB;

class NotificationEligibleBenefitRepository extends BaseRepository
{
    const MODEL = NotificationEligibleBenefit::class;

    protected $benefit_types;

    protected $employees;

    public function __construct()
    {
        $this->benefit_types = new BenefitTypeRepository();
        $this->employees = new EmployeeRepository();
    }

    /**
     * @param $benefit
     * @param $type | 1 - claimed, 2 - compensated(Assessed)
     * @return array
     * @throws GeneralException
     * @description Calculate MAE + Temporary Disablements (TTD and TPD)
     */
    public function calculateTdTotals(Model $benefit, $type)
    {
        $incident = $benefit->incident;
        $min_days_eligible_assessment = sysdefs()->data()->days_eligible_for_claim_assessment;

        $worked_hours = $this->findHoursDaysWorkedForTtdTpd($benefit, $type);

        $ttd_days = $worked_hours['ttd_days'];
        $tpd_days = $worked_hours['tpd_days'];

        /*inserted*/
        $ttd_days_inserted = $this->findHoursDaysWorkedForTtdTpd($benefit, $type)['ttd_days_inserted']; // ttd days inserted for assessment
        $tpd_days_inserted = $this->findHoursDaysWorkedForTtdTpd($benefit, $type)['tpd_days_inserted']; // tpd days inserted for assessment

        $total_td_days = $ttd_days + $tpd_days;
        $total_td_days_inserted = $ttd_days_inserted + $tpd_days_inserted;

        /*Check if qualify for TTD and TPD (Combined)*/
//        if (!($total_td_days_inserted >= $min_days_eligible_assessment))
//        {
//
//            $ttd_days = 0;
//            $tpd_days = 0;
//        }

        /*Check if TTD qualify min days for compensation*/
        if(!($ttd_days_inserted >= $min_days_eligible_assessment)){
            $ttd_days = 0;
        }

        /*Check if TPD qualify min days for compensation*/
        if(!($tpd_days_inserted >= $min_days_eligible_assessment)){
            $tpd_days = 0;
        }

        $ttd_amount = $this->findTtdTpdAmount($this->getConstantInputs($incident), $ttd_days, 'ttd');
        $tpd_amount = $this->findTtdTpdAmount($this->getConstantInputs($incident), $tpd_days, 'tpd');
        /*TTD + TPD*/
        return ['ttd_amount' => $ttd_amount, 'tpd_amount' => $tpd_amount, 'total_claimed_expense'=> ($ttd_amount + $tpd_amount),'ttd_days' => $ttd_days, 'tpd_days' => $tpd_days];
    }

    /**
     * @param Model $benefit
     * @param $type
     * @return array
     * @description To be modified later
     */
    public function calculateExpensesForPd(Model $benefit, $type)
    {
        return ['ttd_amount' => 0, 'tpd_amount' => 0, 'total_claimed_expense'=> (0 + 0),'ttd_days' => 0, 'tpd_days' => 0];
    }

    /**
     * @param $benefit
     * @param $type | 1 - claimed, 2 - compensated(Assessed)
     * @return array
     * @throws GeneralException
     * @description Get total hours worked for TTD and TPD
     */
    public function findHoursDaysWorkedForTtdTpd(Model $benefit, $type) {

        //Total hours worked for each benefit type
        $ttd_hours = 0;
        $tpd_hours = 0;
        $ttd_days = 0;
        $tpd_days = 0;
        $ttd_days_inserted = 0; // ttd days inserted for assessment
        $tpd_days_inserted = 0; // tpd days inserted for assessment
        $percent_of_ld = 0;
        //Get notification disability states for this medical expense
        if ($type == 1) {// claimed expense
            $notification_disability_states_model = $benefit->notificationDisabilityStates();
            $notification_disability_state_repository = new NotificationDisabilityStateRepository();

        } elseif ($type == 2){
            $notification_disability_states_model = $benefit->notificationDisabilityStateAssessments();
            $notification_disability_state_repository = new NotificationDisabilityStateAssessmentRepository();
        }

        if ($notification_disability_states_model->count()){

            $notification_disability_states = $notification_disability_states_model->get();

            foreach ($notification_disability_states as $notification_disability_state) {

                if($type == 1){
                    $benefit_type = $notification_disability_state->disabilityStateChecklist->benefit_type_id ;
                } elseif ($type == 2){
                    $benefit_type = $notification_disability_state->notificationDisabilityState->disabilityStateChecklist->benefit_type_id ;
                }
                // For Ttd
                if ($benefit_type == $this->benefit_types->getTtdId()) {
//                    $ttd_hours += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_hours_worked'];
                    $ttd_days += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_days_worked'];
                    $ttd_days_inserted += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_days_inserted'];
                }
                // For Tpd
                if ($benefit_type == $this->benefit_types->getTpdId()) {
//                    $tpd_hours += $notification_disability_state_repository->findTotalHoursDaysWorked($notification_disability_state->id)['total_days_worked'];
                    $tpd_days += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_days_worked'];
                    $tpd_days_inserted += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_days_inserted'];

                }
            }
        }
        return ['ttd_hours' => $ttd_hours, 'tpd_hours' => $tpd_hours,'ttd_days' => $ttd_days, 'tpd_days' => $tpd_days, 'ttd_days_inserted' => $ttd_days_inserted, 'tpd_days_inserted' => $tpd_days_inserted ];
    }

    /**
     * @param $constant_input
     * @param $total_days
     * @param $disablement_type i.e. ttd, tpd
     * @return float|int
     */
    public function  findTtdTpdAmount($constant_input,$total_days, $disablement_type )
    {

        /*Old Formula*/
//        $amount = ($constant_input['percent_of_earning'] * 0.01) * ($total_days) * (($constant_input['gross_monthly_earning'] / ($constant_input['no_of_days_for_a_month'] )));

        $amount = 0;
        $percent_of_earning = $constant_input['percent_of_earning'];
        $no_of_days_for_a_month = $constant_input['no_of_days_for_a_month'];
        /* Get threshold Amounts i.e. Min and Max Against Monthly Earning based on Percentage of earning*/
        $min_compensation_amt = $constant_input['min_compensation_amt'] ;
        $min_salary_payable = $min_compensation_amt / (0.01 * $percent_of_earning);
        $max_compensation_amt = $constant_input['max_compensation_amt'] ;
        $max_salary_payable = $max_compensation_amt / (0.01 * $percent_of_earning);
        $monthly_earning = $constant_input['gross_monthly_earning'];
        /* If monthly earning is less than Minimum Payable i.e. Only for TTD will be checked on Minimum threshold*/
        if (($monthly_earning < $min_salary_payable) && $disablement_type == 'ttd' ){
            $amount = ($min_compensation_amt / $no_of_days_for_a_month) * $total_days;
        }else{

            /* Check when is above the max i.e. Applicable for TTD and TPD*/
            if ($monthly_earning > $max_salary_payable ){
                $amount = ($max_compensation_amt / $no_of_days_for_a_month) * $total_days;
            }else{
                /* If Monthly earning is between Max and Min Payable*/
                $amount = (($monthly_earning  * (0.01 * $percent_of_earning)) / $no_of_days_for_a_month) * $total_days;
            }

        }
        return $amount;
    }

    /**
     * @param $incident
     * @return array
     */
    public function getConstantInputs($incident){
        //$notification_reports = new NotificationReportRepository();
        //$gross_monthly_earning = count($incident->claim) ? $incident->claim->monthly_earning : $this->employees->getMonthlyEarningBeforeIncident($incident->employee_id, $incident->id)['monthly_earning'];
        $gross_monthly_earning = $incident->monthly_earning;
        $percent_of_earning = sysdefs()->data()->percentage_of_earning;
        $constant_factor_compensation =  sysdefs()->data()->constant_factor_compensation;
        $day_hours = sysdefs()->data()->day_hours;
        $no_of_days_for_a_month = sysdefs()->data()->no_of_days_for_a_month_in_assessment;
        $percentage_imparement_scale = sysdefs()->data()->percentage_imparement_scale;
        $assistant_percent = sysdefs()->data()->assistant_percent;
        $min_compensation_amt = sysdefs()->data()->minimum_monthly_pension;
        $max_compensation_amt = sysdefs()->data()->maximum_monthly_pension;
        return ['percent_of_earning' => $percent_of_earning, 'constant_factor_compensation'=> $constant_factor_compensation, 'day_hours'=> $day_hours, 'gross_monthly_earning' => $gross_monthly_earning, 'no_of_days_for_a_month' => $no_of_days_for_a_month, 'percentage_imparement_scale' => $percentage_imparement_scale, 'min_compensation_amt' => $min_compensation_amt, 'max_compensation_amt' => $max_compensation_amt];
    }

    /**
     * @param Model $incident
     * @param int $eligible_id
     * @return array
     */
    public function findTotalHealthServiceCost(Model $incident, $eligible_id = 0)
    {
        $status = "";
        $total_cost = [];
        if ($eligible_id) {
            $eligibles = $incident->benefits()->whereIn("processed", [0, 1])->where("benefit_type_claim_id", 1)->where("notification_eligible_benefits.id", $eligible_id)->orderByDesc("notification_eligible_benefits.id")->get();
        } else {
            $eligibles = $incident->benefits()->whereIn("processed", [0, 1])->where("benefit_type_claim_id", 1)->orderByDesc("notification_eligible_benefits.id")->get();
        }

        foreach ($eligibles as $eligible) {
            $status = $eligible->processed_label;
            $assessed_amount = $eligible->medicalExpense->assessed_amount;
            $total_cost[] = ['assessed_amount' => $assessed_amount, 'eligible' => $eligible->id, 'status' => $status, 'letter_status_label' => $eligible->letter_status_label, 'processed' => $eligible->processed, 'status' => $eligible->processed_label];
        }
        return $total_cost;
    }


    /**
     * @param $id
     * @param array $input
     * @return bool
     * @throws GeneralException
     */
    public function updatePaymentInfoFromErp($id, array $input)
    {
        //TODO: Sync input parameters name with fron ERP <processpayment>
        $eligible_benefit = $this->find($id);
        $ispaid = 0;
        $date = isset($input['payment_date']) ? standard_date_format($input['payment_date']) : null;
        $eligible_benefit->update([
            'payment_date' => $date,
            'payreference' => $input['payment_reference'],
            'paymethod' => $input['payment_method'],
            'ispaid' => 1
        ]);
        //Update Payment Dates
        $id = $eligible_benefit->id;
        $incident_id = $eligible_benefit->notification_report_id;
        switch ($eligible_benefit->benefit_type_claim_id) {
            case 1:
                //MAE Refund (Employee/Employer)
                $maeQuery = (new IncidentMaeRepository())->query()->where("notification_eligible_benefit_id", $id);
                if ($maeQuery->count()) {
                    $mae = $maeQuery->first();
                    $mae->mae_refund_member_pay_date = $date;
                    $mae->save();
                    $ispaid = 1;
                }
                break;
            case 3:
                //Temporary Disablement
                $tdQuery = (new IncidentTdRepository())->query()->where("notification_eligible_benefit_id", $id);
                if ($tdQuery->count()) {
                    $td = $tdQuery->first();
                    $td->td_pay_date = $date;
                    $td->save();
                    $ispaid = 1;
                }
                break;
            case 4:
                //Temporary Disablement Refund
                $tdQuery = (new IncidentTdRepository())->query()->where("notification_eligible_benefit_id", $id);
                if ($tdQuery->count()) {
                    $td = $tdQuery->first();
                    $td->td_refund_pay_date = $date;
                    $td->save();
                    $ispaid = 1;
                }
                break;
            case 5:
                //Permanent Disablement
                $pdQuery = (new NotificationReportRepository())->query()->where(["pd_eligible_id" => $id, "id" => $incident_id]);
                if ($pdQuery->count()) {
                    $pd = $pdQuery->first();
                    $pd->pd_pay_date = $date;
                    $pd->save();
                    $ispaid = 1;
                }
                break;
            case 7:
                //Survivors
                $survivorQuery = (new NotificationReportRepository())->query()->where(["survivor_eligible_id" => $id, "id" => $incident_id]);
                if ($survivorQuery->count()) {
                    $survivor = $survivorQuery->first();
                    $survivor->survivor_pay_date = $date;
                    $survivor->save();
                    $ispaid = 1;
                }
                break;
        }
        //Update at least paid for the file
        if ($ispaid) {
            /*$incident = $eligible_benefit->incident;
            //At least Paid
            $incident->notification_staging_cv_id = (new CodeValueRepository())->NSOPALP();
            $incident->save();
            (new NotificationReportRepository())->initialize($incident);*/
        }
        return true;

    }

    /**
     * @param $id
     * @return mixed
     */
    public function listTdDocumentUsed($id)
    {
        return $this->query()
                ->select([
                    "ad.id",
                    "dnr.description",
                    "dnr.doc_date",
                    "dnr.doc_receive_date",
                    "dnr.doc_create_date",
                    "d.name as document",
                    "hp.name as hospital",
                    "dnr.id as document_notification_report_id",
                ])
                ->join(DB::raw("assessment_documents as ad"),"ad.notification_eligible_benefit_id", "=", "notification_eligible_benefits.id")
                ->join(DB::raw("document_notification_report as dnr"), "dnr.id", "=", "ad.document_notification_report_id")
                ->join(DB::raw("documents as d"), "d.id", "=", "dnr.document_id")
                ->leftJoin(DB::raw("health_providers as hp"), "hp.id", "=", "ad.health_provider_id")
                //->join("employers", "notification_reports.employer_id", "=", "employers.id")
                //->join("accrual_medical_expenses","accrual_medical_expenses.notification_report_id", "=", "notification_reports.id")
                ->where("ad.notification_eligible_benefit_id", $id)
                ->get();
    }

    /**
     * @param Model $eligible
     * @param array $inputs
     * @return mixed
     */
    public function addTdDocumentUsed(Model $eligible, array $inputs)
    {
        return DB::transaction(function () use ($eligible, $inputs) {
            $assessment_document = AssessmentDocument::query()->updateOrCreate(
                [
                    'notification_eligible_benefit_id' => $eligible->id,
                    'document_notification_report_id' => $inputs['document_used'],
                ],
                [
                    'doc_date' => $inputs['document_date'],
                    'health_provider_id' => $inputs['health_provider_id'] ?? NULL,
                ]
            );
            return $assessment_document;
        });
    }

}