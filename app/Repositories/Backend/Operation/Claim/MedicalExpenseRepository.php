<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\MedicalExpense;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;

class MedicalExpenseRepository extends  BaseRepository
{

    const MODEL = MedicalExpense::class;
    protected  $resource_id;
    protected $notification_disability_states;
    protected $benefit_types;
    protected $employees;


    public function __construct()
    {
        $this->notification_disability_states = new NotificationDisabilityStateRepository();
        $this->benefit_types = new BenefitTypeRepository();
        $this->employees = new EmployeeRepository();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $medical_expense = $this->query()->find($id);

        if (!is_null($medical_expense)) {
            return $medical_expense;
        }
        throw new GeneralException(trans('exceptions.backend.claim.medical_expense_not_found'));
    }

    /**
     * @param $notification_report_id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function create($notification_report_id, $input) {
        $resource_id =  $this->getMemberTypeValue($notification_report_id, $input) ;
//        $this->checkIfExist($notification_report_id,$input['member_type_id'],$resource_id);
        if ($input['member_type_id'] == 6) {
            //WCF as Employer
            $input['member_type_id'] = 1;
        }
        $medical_expense = $this->query()->create(['notification_report_id' => $notification_report_id,'member_type_id' =>  $input['member_type_id'], 'resource_id' => $resource_id, 'amount' => str_replace(",", "", $input['amount']),'user_id' => access()->user()->id ]);
        return $medical_expense;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function createBenefit(array $input)
    {
        $medical_expense = $this->query()->create(['notification_report_id' => $input['notification_report_id'],'member_type_id' =>  $input['member_type_id'], 'resource_id' => $input['resource_id'], 'amount' => str_replace(",", "", $input['amount']), 'user_id' => access()->user()->id]);
        return $medical_expense;
    }

    /**
     * @param $id
     * @param array $input
     * @return mixed
     * @throws GeneralException
     */
    public function updateBenefit($id, array $input)
    {
        $medical_expense = $this->findOrThrowException($id);
        $medical_expense->update(['notification_report_id' => $input['notification_report_id'],'member_type_id' =>  $input['member_type_id'], 'resource_id' => $input['resource_id'], 'amount' => str_replace(",", "", $input['amount']), 'user_id' => access()->user()->id,]);
        return $medical_expense;
    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function update($id,$input) {
        $medical_expense = $this->findOrThrowException($id);
        $resource_id =  $this->getMemberTypeValue($medical_expense->notification_report_id, $input) ;
        if ($input['member_type_id'] == 6) {
            //WCF as Employer
            $input['member_type_id'] = 1;
        }
        $medical_expense->update(['amount' => str_replace(",", "", $input['amount']), 'resource_id' => $resource_id, 'member_type_id' =>  $input['member_type_id']]);
        return $medical_expense;
    }

    /**
     * @param $notification_report_id
     * @param $member_type_id
     * @param $resource_id
     * @throws GeneralException
     */
    public function checkIfExist($notification_report_id,$member_type_id,$resource_id){
        $medical_expense = $this->query()->where('notification_report_id',$notification_report_id)->where('member_type_id',$member_type_id)->where('resource_id',$resource_id)->first();
        if (!is_null($medical_expense)) {
            throw new GeneralException(trans('exceptions.backend.claim.medical_expense_already_exist'));
        }
    }

    /**
     * @param $notification_report_id
     * @param $member_type_id
     * Date: 07-Jun-2018
     * Get total expense amount of the notification report
     */
    public function getTotalExpenseByNotification($notification_report_id)
    {
        $total_expense = $this->query()->where('notification_report_id', $notification_report_id)->sum('amount');
        return $total_expense;
    }

    /*Get First Medical expense for Notification i.e. Referral*/
    public function getFirstMedicalExpenseForNotification($notification_report_id)
    {
        $medical_expense = $this->query()->where('notification_report_id', $notification_report_id)->orderBy('id')->first();
        return $medical_expense;
    }


    /*Get Medical Expense with Current employee state*/
    public function getMedicalExpenseWithDisabilityState($notification_report_id)
    {
        $medical_expense = $this->query()->has('notificationDisabilityStates')->where('notification_report_id', $notification_report_id)->first();
        return $medical_expense;
    }

    /**
     * @param $id
     * @return mixed
     * Find Total Health Service cost by Notification report
     * 07-jun-2018
     */
    public function findTotalHealthServiceCost($notification_report_id)
    {
        $health_services = new NotificationHealthProviderServiceRepository();
        $total_cost = $health_services->query()->whereHas('notificationHealthProvider', function($query) use($notification_report_id) {
            $query->where('notification_report_id', $notification_report_id);
        })->sum('amount');
        return $total_cost;
    }


    /**
     * @param $id
     * @return mixed
     * Find Total Health Service cost by member type
     * 07-Jun-2018
     */
    public function findTotalHealthServiceCostByMemberType($notification_report_id, $member_type_id)
    {
        $health_services = new NotificationHealthProviderServiceRepository();
        $total_cost = $health_services->query()->whereHas('notificationHealthProvider', function($query) use($notification_report_id, $member_type_id) {
            $query->whereHas('medicalExpense', function($query) use($notification_report_id, $member_type_id){
                $query->where('notification_report_id',$notification_report_id)->where('member_type_id', $member_type_id);
            });
        })->sum('amount');
        return $total_cost;
    }

    /**
     * @param $notification_report_id
     * @param $input
     * @return int
     * @throws GeneralException
     */
    public function getMemberTypeValue($notification_report_id, $input) {
        //Insurance is selected
        $notification_reports = new NotificationReportRepository();
        if ($input['member_type_id'] == 3) {
            $resource_id = $input['insurance_id'] ;
        }
        //Employer is selected
        if ($input['member_type_id'] == 1) {
            $notification_report = $notification_reports->findOrThrowException($notification_report_id);
            $resource_id = $notification_report->employer_id;
        }
        //Employee is selected
        if ($input['member_type_id'] == 2) {
            $notification_report = $notification_reports->findOrThrowException($notification_report_id);
            $resource_id = $notification_report->employee_id;
        }
        //WCF is selected
        if ($input['member_type_id'] == 6) {
            //WCF Registration Number
            $resource_id = 4038;
        }

        return $resource_id;
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @description Calculate and Update Total Medical Services expense for a selected entity
     */
    public function updateTotalMedicalServicesCost($id){
        $medical_expense = $this->findOrThrowException($id);
        $total_cost = $medical_expense->notificationHealthProviderServices->sum('amount');
        $medical_expense->update(['amount'=>$total_cost]);
        return $total_cost;
    }

    /**
     * @param $id
     * @param $expense_type | 1 - claimed, 2 - compensated(Assessed)
     * @return array
     * @throws GeneralException
     * @description Calculate MAE + Temporary Disablements (TTD and TPD)
     */
    public function calculateExpenses($id, $expense_type)
    {
        $medical_expense = $this->findOrThrowException($id);
        $notification_reports = new NotificationReportRepository();
        $notification_report_id = $medical_expense->notification_report_id;
        $notification_report = $notification_reports->findOrThrowException($notification_report_id);

        $min_days_eligible_assessment = sysdefs()->data()->days_eligible_for_claim_assessment;

        //MAE
//        $mae = $medical_expense->amount;
        $mae = $this->getTotalExpenseByNotification($notification_report_id); //07-Jun-2018
        $mae_assessed = $this->findTotalHealthServiceCost($notification_report_id);//07-Jun-2018

        $ttd_days = $this->findHoursDaysWorkedForTtdTpd($id, $expense_type)['ttd_days'];
        $tpd_days = $this->findHoursDaysWorkedForTtdTpd($id, $expense_type)['tpd_days'];
        /*inserted*/
        $ttd_days_inserted = $this->findHoursDaysWorkedForTtdTpd($id, $expense_type)['ttd_days_inserted']; // ttd days inserted for assessment
        $tpd_days_inserted = $this->findHoursDaysWorkedForTtdTpd($id, $expense_type)['tpd_days_inserted']; // tpd days inserted for assessment

        $total_td_days = $ttd_days + $tpd_days;
        $total_td_days_inserted = $ttd_days_inserted + $tpd_days_inserted;
        /*Check if qualify for TTD and TPD*/
//        if (!($total_td_days_inserted >= $min_days_eligible_assessment))
//        {
//            $ttd_days = 0;
//            $tpd_days = 0;
//        }

        /*Check if TTD qualify min days for compensation*/
        if(!($ttd_days_inserted >= $min_days_eligible_assessment)){
            $ttd_days = 0;
        }

        /*Check if TPD qualify min days for compensation*/
        if(!($tpd_days_inserted >= $min_days_eligible_assessment)){
            $tpd_days = 0;
        }

        $ttd_amount = $this->findTtdTpdAmount($this->getConstantInputs($notification_report),$ttd_days, 'ttd');
        $tpd_amount = $this->findTtdTpdAmount($this->getConstantInputs($notification_report),$tpd_days, 'tpd');
        /*MAE + TTD + TPD*/
        return ['mae' => $mae, 'mae_assessed' => $mae_assessed, 'ttd_amount' => $ttd_amount, 'tpd_amount' => $tpd_amount, 'total_claimed_expense'=> ($mae + $ttd_amount + $tpd_amount),'ttd_days' => $ttd_days, 'tpd_days' => $tpd_days];
    }

    /*
     * Formula for calculating ttd and tpd
     */
    public function  findTtdTpdAmount($constant_input,$total_days, $disablement_type ){

        /*Old Formula*/
//        $amount = ($constant_input['percent_of_earning'] * 0.01) * ($total_days) * (($constant_input['gross_monthly_earning'] / ($constant_input['no_of_days_for_a_month'] )));

        $amount = 0;
        $percent_of_earning = $constant_input['percent_of_earning'];
        $no_of_days_for_a_month = $constant_input['no_of_days_for_a_month'];
        /* Get threshold Amounts i.e. Min and Max Against Monthly Earning based on Percentage of earning*/
        $min_compensation_amt = $constant_input['min_compensation_amt'] ;
        $min_salary_payable = $min_compensation_amt / (0.01 * $percent_of_earning);
        $max_compensation_amt = $constant_input['max_compensation_amt'] ;
        $max_salary_payable = $max_compensation_amt / (0.01 * $percent_of_earning);
        $monthly_earning = $constant_input['gross_monthly_earning'];
        /* If monthly earning is less than Minimum Payable i.e. Only for TTD will be checked on Minimum threshold*/
        if (($monthly_earning < $min_salary_payable) && $disablement_type == 'ttd' ){
            $amount = ($min_compensation_amt / $no_of_days_for_a_month) * $total_days;
        }else{

            /* Check when is above the max i.e. Applicable for TTD and TPD*/
            if ($monthly_earning > $max_salary_payable ){
                $amount = ($max_compensation_amt / $no_of_days_for_a_month) * $total_days;
            }else{
                /* If Monthly earning is between Max and Min Payable*/
                $amount = (($monthly_earning  * (0.01 * $percent_of_earning)) / $no_of_days_for_a_month) * $total_days;
            }

        }

        return $amount;
    }

    /**
     * @param $id
     * @param $expense_type | 1 - claimed, 2 - compensated(Assessed)
     * @return array
     * @throws GeneralException
     * @description Get total hours worked for TTD and TPD
     */
    public function findHoursDaysWorkedForTtdTpd($id, $expense_type)  {
        $medical_expense = $this->findOrThrowException($id);

        //Total hours worked for each benefit type
        $ttd_hours = 0;
        $tpd_hours = 0;
        $ttd_days = 0; //ttd days net to be used for assessment
        $tpd_days = 0; //tpd days net to be used for assessment
        $ttd_days_inserted = 0; // ttd days inserted for assessment
        $tpd_days_inserted = 0; // tpd days inserted for assessment
        $percent_of_ld = 0;
        //Get notification disability states for this medical expense
        if ($expense_type == 1) {// claimed expense

            $notification_disability_states = $medical_expense->notificationDisabilityStates;
            $notification_disability_state_repository = new NotificationDisabilityStateRepository();

        } elseif ($expense_type == 2){
            $notification_disability_states = $medical_expense->notificationDisabilityStateAssessments;
            $notification_disability_state_repository = new NotificationDisabilityStateAssessmentRepository();
        }

        if (($notification_disability_states->count() > 0)){

            foreach ($notification_disability_states as $notification_disability_state) {
                      if($expense_type == 1){
                    $benefit_type = $notification_disability_state->disabilityStateChecklist->benefit_type_id ;
                }elseif ($expense_type == 2){
                    $benefit_type = $notification_disability_state->notificationDisabilityState->disabilityStateChecklist->benefit_type_id ;
                }
                // For Ttd
                if ($benefit_type == $this->benefit_types->getTtdId()) {
//                    $ttd_hours += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_hours_worked'];
                    $ttd_days += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_days_worked'];
                    $ttd_days_inserted += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_days_inserted'];
                }
                // For Tpd
                if ($benefit_type == $this->benefit_types->getTpdId()) {
//                    $tpd_hours += $notification_disability_state_repository->findTotalHoursDaysWorked($notification_disability_state->id)['total_days_worked'];
                    $tpd_days += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_days_worked'];
                    $tpd_days_inserted += $notification_disability_state_repository->findTotalDaysWorked($notification_disability_state->id)['total_days_inserted'];
                }
            }
        }
        return ['ttd_hours' => $ttd_hours, 'tpd_hours' => $tpd_hours,'ttd_days' => $ttd_days, 'tpd_days' => $tpd_days, 'ttd_days_inserted' => $ttd_days_inserted, 'tpd_days_inserted' => $tpd_days_inserted  ];
    }


    /**
     * @param $id
     * @param $expense_type
     * @return array
     * Computation of permanent disablement
     */

    public function getConstantInputs($notification_report){
        $notification_reports = new NotificationReportRepository();
        $gross_monthly_earning = ($notification_report->claim()->count()) ? $notification_report->claim->monthly_earning : $this->employees->getMonthlyEarningBeforeIncident($notification_report->employee_id, $notification_report->id)['monthly_earning'];
        $percent_of_earning = sysdefs()->data()->percentage_of_earning;
        $constant_factor_compensation =  sysdefs()->data()->constant_factor_compensation;
        $day_hours = sysdefs()->data()->day_hours;
        $no_of_days_for_a_month = sysdefs()->data()->no_of_days_for_a_month_in_assessment;
        $percentage_imparement_scale = sysdefs()->data()->percentage_imparement_scale;
        $assistant_percent = sysdefs()->data()->assistant_percent;
        $min_compensation_amt = sysdefs()->data()->minimum_monthly_pension;
        $max_compensation_amt = sysdefs()->data()->maximum_monthly_pension;
        return ['percent_of_earning' => $percent_of_earning, 'constant_factor_compensation'=> $constant_factor_compensation, 'day_hours'=> $day_hours, 'gross_monthly_earning' => $gross_monthly_earning, 'no_of_days_for_a_month' => $no_of_days_for_a_month, 'percentage_imparement_scale' => $percentage_imparement_scale, 'min_compensation_amt' => $min_compensation_amt, 'max_compensation_amt' => $max_compensation_amt];
    }





}