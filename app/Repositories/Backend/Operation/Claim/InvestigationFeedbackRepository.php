<?php

namespace App\Repositories\Backend\Operation\Claim;


use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\InvestigationFeedback;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InvestigationFeedbackRepository extends  BaseRepository
{

    const MODEL = InvestigationFeedback::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $investigation_feedback = $this->query()->find($id);

        if (!is_null($investigation_feedback)) {
            return $investigation_feedback;
        }
        throw new GeneralException(trans('exceptions.backend.claim.investigation_feedback_not_found'));
    }

    /**
     * @param $notification_report_id
     * @param $investigation_question_id
     * @param null $parent_id
     */
    public function create($notification_report_id, $investigation_question_id, $parent_id = NULL) {
        $this->query()->create(['notification_report_id' => $notification_report_id, 'investigation_question_id'=> $investigation_question_id, 'parent_id' => $parent_id] );
    }

    /**
     * @param array $input
     */
    public function createForInvestigator(array $input)
    {
        $this->query()->create($input);
    }

    /**
     * @param $id
     * @param $feedback
     * @return mixed
     * @throws GeneralException
     */
    public function update($id, $feedback) {
        $investigation_feedback = $this->findOrThrowException($id);
        $investigation_feedback->update(['feedback' => $feedback]);
        return $investigation_feedback;
    }


    /**
     * @param $notification_report_id
     */
    public function delete($notification_report_id) {
        $deleted_feedbacks = $this->query()->where('notification_report_id', $notification_report_id )->delete();
    }

    public function deleteForInvestigator(Model $incident)
    {
        $notification_investigator = $incident->investigationStaffs()->select(["id"])->orderByDesc("notification_investigators.id")->where("attended", 0)->limit(1);
        if ($notification_investigator->count()) {
            $this->query()->where(["notification_report_id" => $incident->id, "notification_investigator_id" => $notification_investigator->first()->id])->delete();
        }
    }

}