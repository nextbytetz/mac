<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\Accident;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\ClaimFollowup\ClaimFollowup;
use App\Models\Operation\Claim\NotificationReport; 
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Operation\Claim\ClaimFollowupUpdateRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Auth\User;


/**
 * Class AccidentRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class ClaimFollowUpReportRepository extends  BaseRepository
{

    const MODEL = ClaimFollowup::class;

    public function __construct()
    {
        $this->notification_report = new NotificationReportRepository();
        $this->claim_followup_update = new ClaimFollowupUpdateRepository();


    }

    public function findOrThrowException($id)
    {
        $followup = $this->query()->find($id);
        if (!is_null($followup)) {
            return $followup;
        }
        throw new GeneralException('The followup does not exist');
    }


    public function syncFolloups(){
        $notification_reports = NotificationReport::all();
        $incomplete_documents = DB::table('main.cr10')->pluck('filename')->toArray();
        // dump($notification_reports);
        // dd($incomplete_documents);

        foreach ($notification_reports as $n) {

            if (in_array($n->filename,$incomplete_documents)) {
                // save to followups
                dump('inside');
                $this->saveFollowupClaims($n);
            }else{
                dump('outside');
            }
        }
    }
    public function getClaimFollowupReportForDatatable()
    {
    // dd(1);
    // $already_allocated = $this->allocatedFilesIdToOtherPlan($plan_id);
    // $plan = (new InvestigationPlanRepository())->find($plan_id);
        $follow_up = $this->getClaimFollowupReportQueryFilter()
        ->select($this->getClaimFollowUpReportSelectQuery());

        return $follow_up;
    // $datatables = app('datatables')->of($investigation);
    // return $datatables;
    }

    public function getGeneralsNotificationFollowUpForDataTable()
    {  
  // DB::statement(DB::raw('set row_num()= 0'));
      $followup = $this->query() 
      ->select([('*'),
    // DB::raw(("row_number() over (order by s_no) row")),
        DB::raw("CONCAT_WS(' ', coalesce(a.firstname, ''), coalesce(a.middlename, ''), coalesce(a.lastname, '')) as user"),
        'claim_followups.id as id',
        DB::raw("CONCAT_WS(' ', coalesce(b.firstname, ''), coalesce(b.middlename, ''), coalesce(b.lastname, '')) as contact_user"), 
        'notification_reports.id as notification_id',
        DB::raw("incident_types.name as incident_type"),
        // DB::raw("notification_reports.incident_date as incident_date"),
        DB::raw("regions.name as region"),
        DB::raw("districts.name as district"),


    ])
      ->leftJoin(DB::raw('users a'), "claim_followups.user_id", "=", "a.id")
      ->leftJoin("claim_followup_updates","claim_followup_updates.claim_followup_id", "=", "claim_followups.id")
      ->leftJoin(DB::raw('users b'), "claim_followups.user_id", "=", "b.id")
      ->leftJoin("notification_reports","claim_followups.notification_report_id", "=", "notification_reports.id")
      ->leftJoin("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
      ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
      ->leftJoin("regions", "regions.id", "=", "districts.region_id");


      return $followup;
  }

  public function getClaimFollowupReportQueryFilter()
  {
    // dd(request()->all);
    $codeValue = new CodeValueRepository();
    $followup = $this->getClaimFollowupReportQuery();
    $staff = new ClaimFollowupRepository();

    $incident = (int) request()->input("incident");
    if ($incident) {
        $followup->where("notification_reports.incident_type_id", "=", $incident);
    }
    $employee = request()->input("employee");
    if ($employee) {
        $followup->where("notification_reports.employee_id", "=", $employee);
    }
    $employer = request()->input("employer");
    if ($employer) {
        $followup->where("notification_reports.employer_id", "=", $employer);
    }
    $region = request()->input("region");
    if ($region) {
        $followup->where("regions.id", "=", $region);
    }
    $district = request()->input("district");
    if ($district) {
        $followup->where("districts.id", "=", $district);
    }
    $staff = request()->input('staff');
    if ($staff) {
        // $allocated_case_ids = $this->allocatedFollowupFilesIds();
     $followup->where("notification_reports.allocated", "=", $staff);
 }




        //check followup status
 switch ($staff) {
    case 1:
                //Allocated
        // $followup->whereIn("notification_reports.id", $this->allocatedFilesIds());
    break;
    case 2:
            //Unallocated
        // $followup->whereNotIn("notification_reports.id", $this->allocatedFilesIds());
    break;
    case 3:
                //Allocated to User
    $user_id = request()->input('user_id');
        // $followup->whereIn("notification_reports.id", $this->allocatedFilesIds());
    if($user_id){
            // $followup->whereIn("investigation_plan_notification_reports.assigned_to", [$user_id]);
    }

    break;
}
return $followup;
}

public function getClaimFollowupReportSelectQuery()
{
    $codeValue = new CodeValueRepository();
    $onFollowUp = "({$codeValue->NSPCRFI()}, {$codeValue->NSOPRFLI()})";
    return [
        DB::raw("notification_reports.id as id"),
        DB::raw("notification_reports.filename"),
        DB::raw("incident_types.name as incident_type"),
        DB::raw("case when notification_reports.employee_id is null then notification_reports.employee_name else concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) end as employee"),
        DB::raw("case when notification_reports.employer_id is null then notification_reports.employer_name else employers.name end as employer"),
        DB::raw("notification_reports.incident_date"),
        DB::raw("regions.name region"),
        DB::raw("districts.name as district"),
        DB::raw("claim_followups.id as id"),
        DB::raw("CONCAT_WS(' ', coalesce(a.firstname, ''), coalesce(a.middlename, ''), coalesce(a.lastname, '')) as user"),   
        DB::raw("CONCAT_WS(' ', coalesce(b.firstname, ''), coalesce(b.middlename, ''), coalesce(b.lastname, '')) as contact_user"),
        DB::raw("claim_followup_updates.week_date as week_date"),
        DB::raw("claim_followup_updates.updated_at as updated_at"),
        DB::raw("claim_followup_updates.date_of_follow_up as date_of_follow_up"),
    ];
}


public function getClaimFollowupReportQuery()
{
    $closedIncident = (new CodeValueRepository())->NSCINC();
    $followup = $this->query()

    ->leftJoin(DB::raw('users a'), "claim_followups.user_id", "=", "a.id")
    ->leftJoin("claim_followup_updates","claim_followup_updates.claim_followup_id", "=", "claim_followups.id")
    ->leftJoin(DB::raw('users b'), "claim_followups.user_id", "=", "b.id")
    ->leftJoin("notification_reports","claim_followups.notification_report_id", "=", "notification_reports.id")
    ->leftJoin("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
    ->leftJoin("employees", "notification_reports.employee_id", "=", "employees.id")
    ->leftJoin("employers", "notification_reports.employer_id", "=", "employers.id")
    ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
    ->leftJoin("regions", "regions.id", "=", "districts.region_id")
    ->join("code_values", "notification_reports.notification_staging_cv_id", "=", "code_values.id")
    ->leftJoin(DB::raw('users c'), "notification_reports.allocated", "=", "c.id")
    ->where("notification_staging_cv_id", "<>", $closedIncident);
    return $followup;
}
public function allocatedFollowupFilesIds()
{
   return $this->query()->pluck("notification_report_id")->all();
}


}



