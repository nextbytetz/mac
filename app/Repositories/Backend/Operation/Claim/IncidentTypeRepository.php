<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\IncidentType;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class IncidentTypeRepository extends  BaseRepository
{

    const MODEL = IncidentType::class;

    public function __construct()
    {

    }

//find or throwexception
    public function findOrThrowException($id)
    {
        $incident_type = $this->query()->find($id);

        if (!is_null($incident_type)) {
            return $incident_type;
        }
        throw new GeneralException(trans('exceptions.backend.claim.incident_type_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}