<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\DocumentGroup;
use App\Repositories\BaseRepository;

class DocumentGroupRepository extends BaseRepository
{
    const MODEL = DocumentGroup::class;

    public function getDocumentGroups($incidentTypeId)
    {
        $return = $this->query()->whereHas("incidentTypes", function ($query) use ($incidentTypeId) {
            $query->where("incident_type_id", $incidentTypeId);
        })->get();
        return $return;
    }

    public function getDocumentGroupsForNotificationReport($id) {
        $return = $this->query()->whereHas("documents", function ($query) use ($id)  {
            $query->whereHas("notificationReports", function ($subQuery) use ($id) {
                $subQuery->where("notification_reports.id", $id);
            });
        })->get();
        return $return;
    }

}