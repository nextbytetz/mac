<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\ImpairmentAssessment;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class ImpairmentAssessmentRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class ImpairmentAssessmentRepository extends BaseRepository
{
    const MODEL = ImpairmentAssessment::class;

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function create(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $impairment = $this->query()->create([
                'notification_report_id' => $incident->id,
                'comments' => $input['comments'],
                'assessor' => $input['assessor'],
                'date' => $input['date'],
                'location' => $input['location'],
                'user_id' => access()->id(),
                'region_id' => $input['region_id'],
            ]);
            $incident->impairment_assessment_id = $impairment->id;
            $incident->save();
            return $impairment;
        });
    }

    /**
     * @param Model $impairment
     * @param array $input
     * @return mixed
     */
    public function update(Model $impairment, array $input)
    {
        return DB::transaction(function () use ($impairment, $input) {
            $user = access()->user();
            $impairment->update([
                'comments' => $input['comments'],
                'assessor' => $input['assessor'],
                'date' => $input['date'],
                'location' => $input['location'],
                'user_id' => access()->id(),
                'region_id' => $input['region_id'],
            ]);
            //Track Edits
            $edits = json_decode($impairment->edits, true) ?? [];
            $edits[] = ['user' => $user->name, 'date' => Carbon::now()->format('d-M-Y g:i:s A')];
            $impairment->edits = json_encode($edits);
            $impairment->save();
            return $impairment;
        });
    }

}