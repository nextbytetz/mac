<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\BodyPartInjuryGroup;
use App\Repositories\BaseRepository;

class BodyPartInjuryGroupRepository extends BaseRepository
{

    const MODEL = BodyPartInjuryGroup::class;

}