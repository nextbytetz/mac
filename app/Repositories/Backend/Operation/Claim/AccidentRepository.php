<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\Accident;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class AccidentRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class AccidentRepository extends  BaseRepository
{

    const MODEL = Accident::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $accident = $this->query()->find($id);

        if (!is_null($accident)) {
            return $accident;
        }
        throw new GeneralException(trans('exceptions.backend.claim.accident_not_found'));
    }

    /**
     * @param $notification_report
     * @param $input
     * @return mixed
     */
    public function create($notification_report,$input) {
        return DB::transaction(function () use ($notification_report,$input) {
            $accident = $this->query()->create([
                'notification_report_id' => $notification_report->id,
                'accident_type_id' => $input['accident_type_id'],
                'accident_date' => $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'accident_time' => Carbon::parse($input['accident_time'])->format('H:i'),
                'activity_performed' => $input['activity_performed'],
                'accident_place' => $input['accident_place'],
                'description' => $input['description'],
                'user_id' => access()->user()->id
            ]);
            return $accident;
        });
    }

    public function updateProgressive(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $accident = $this->query()->where('notification_report_id', $incident->id);
            $accident->update([
                'accident_type_id' => $input['accident_type_id'],
                'accident_date' => $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'accident_time' => Carbon::parse($input['accident_time'])->format('H:i'),
                'activity_performed' => $input['activity_performed'],
                'accident_place' => $input['accident_place'],
                'description' => $input['description'],
                'user_id' => access()->user()->id
            ]);
            return $accident;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function saveChecklist(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $accident = $incident->accident;
            $data = [
                'accident_date' => $input['accident_date'],
                'reporting_date' => $input['reporting_date'],
                'salary_being_continued' => $input['salary_being_continued'],
                'accident_cause_cv_id' => $input['accident_cause_cv_id'],
                'lost_body_part' => $input['lost_body_part'],
                'accident_incident_exposure_cv_id' => $input['accident_incident_exposure_cv_id'],
                'on_treatment' => (isset($input['on_treatment']) ? $input['on_treatment'] : 0),
                'return_to_work' => isset($input['return_to_work']) ? $input['return_to_work'] : 0,
                'checklist_user' => access()->id(),
            ];
            if ($input['salary_being_continued']) {
                $data['salary_full_continued'] = $input['salary_full_continued'];
            } else {
                $data['salary_full_continued'] = NULL;
            }
            if ($input['lost_body_part']) {
                //Save lost body parts
                $incident->lostBodyParties()->sync($input['lost_body_part_id']);
            } else {
                $incident->lostBodyParties()->sync([]);
            }
            if (isset($input['return_to_work']) And ($input['return_to_work'])) {
                $data['return_to_work_date'] = $input['return_to_work_date'];
                $data['job_title_id'] = $input['job_title_id'];
            } else {
                $data['return_to_work_date'] = NULL;
                $data['job_title_id'] = NULL;
            }

            $accident->update($data);
            return $accident;
        });
    }

    /*
     * update
     */
    public function update($notification_report,$input,$incident_date,$reporting_date,$receipt_date) {
        return DB::transaction(function () use ($notification_report,$input){
            $accident = $this->query()->where('notification_report_id',$notification_report->id);
            $accident->update(['accident_type_id' =>  $input['accident_type_id'], 'accident_date' =>  $input['incident_date'], 'reporting_date' =>  $input['reporting_date'],'receipt_date' =>  $input['receipt_date'], 'accident_time' => $input['accident_time'], 'activity_performed' => $input['activity_performed'], 'accident_place' => $input['accident_place'], 'description' =>  $input['description']]);
            return $accident;
        });
    }


    /**
     * @param $id
     * @param bool $perm
     * @return bool
     */
    public function delete($id, $perm = false)
    {
        if ($perm) {
            $accident = $this->query()->where("id", $id)->first();
            //delete accident witnesses
            $accident->witnesses()->sync([]);
            //delete accident permanently
            $accident->forceDelete();
        } else {
            $this->query()->where("id", $id)->delete();
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     * Check if accident if on premises or outside office premise
     */
    public function checkIfAccidentIsOnPremises($id){
        /* check if notification is accident */

        $accident = $this->findOrThrowException($id);
        if ($accident->accident_type_id == 1){
            return 1;
        }else{
            return 0;
        }


    }

    /**
     * @param $id
     * Check if has police report document
     */
    public function checkIfHasPoliceReport(Model $notification_report){
        $notificationReport = new NotificationReportRepository();
        $accident = $this->query()->whereHas('notificationReport', function($query) use($notification_report){
            $query->where('notification_report_id', $notification_report->id);
        })->first();
        $document = new DocumentRepository();
        $police_report_id = $document->getPoliceReportDocument();
        if (($this->checkIfAccidentIsOnPremises($accident->id) == 0)){

            /*get police report if exist*/
            $police_report = $notification_report->documents()->where('document_id', $police_report_id)->whereNotNull('document_notification_report.name')->first();

            /*If not available prompt to upload*/
            if(!$police_report){
                throw new GeneralException(trans('exceptions.backend.claim.police_report_not_uploaded'));
            }


        }
    }

}