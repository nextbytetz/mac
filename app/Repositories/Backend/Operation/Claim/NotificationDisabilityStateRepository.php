<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\NotificationDisabilityState;
use Carbon\Carbon;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NotificationDisabilityStateRepository extends  BaseRepository
{

    const MODEL = NotificationDisabilityState::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $notification_disability_state = $this->query()->find($id);

        if (!is_null($notification_disability_state)) {
            return $notification_disability_state;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_disability_state_not_found'));
    }

    /**
     * @param Model $eligible
     * @return bool
     * @throws GeneralException
     */
    public function insertMissingProgressive(Model $eligible)
    {
        return DB::transaction(function () use ($eligible) {
            $disability_state_checklists = $eligible->notificationDisabilityStates()->pluck("notification_disability_states.disability_state_checklist_id")->all();
            $missing_states = (new DisabilityStateChecklistRepository())->getActiveModel()->whereNotIn("id", $disability_state_checklists)->get();
            foreach ($missing_states as $missing_state) {
                $input = [
                    'notification_eligible_benefit_id' => $eligible->id,
                    'percent_of_ed_ld' . $missing_state->id => $missing_state->percent_of_ed_ld,
                    'days' . $missing_state->id => 0,
                    'user_id' => access()->user()->id,
                    'incident_id' => $eligible->notification_report_id,
                    'incident_date' => $eligible->incident->incident_date,
                ];
                $this->create($missing_state->id, $input);
            }
            return true;
        });
    }

    /**
     * @param $disability_state_checklist_id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function create($disability_state_checklist_id, $input) {
        if (isset($input['notification_eligible_benefit_id'])) {
            $input['medical_expense'] = NULL;
        } else {
            $this->checkIfStateExist($disability_state_checklist_id, $input);
            $input['notification_eligible_benefit_id'] = NULL;
        }

        $notification_disability_state = $this->query()->create([
            'medical_expense_id' => $input['medical_expense'],
            'disability_state_checklist_id' => $disability_state_checklist_id,
            'percent_of_ed_ld'=> (isset($input['percent_of_ed_ld'.$disability_state_checklist_id]) And ($input['percent_of_ed_ld'.$disability_state_checklist_id])) ? ($input['percent_of_ed_ld'.$disability_state_checklist_id]) : 50,
            'days'=> $input['days'.$disability_state_checklist_id],
            'from_date' => $input['from_date'.$disability_state_checklist_id] ?? NULL,
            'to_date' => $input['to_date'.$disability_state_checklist_id] ?? NULL,
            'user_id'=>access()->user()->id,
            'notification_eligible_benefit_id' => $input['notification_eligible_benefit_id'],
        ]);
        if (isset($input['notification_eligible_benefit_id'])) {
            $this->autoUpdateFromToDatesProgressive($input);
        } else {
            $this->autoUpdateFromToDates($input);
        }

        return $notification_disability_state;
    }

    /**
     * @param $disability_state_checklist_id
     * @param $input
     * @throws GeneralException
     */
    public function update($disability_state_checklist_id,$input) {
        if (isset($input['notification_eligible_benefit_id'])) {
            $notification_disability_state =   $this->query()->where('notification_eligible_benefit_id', $input['notification_eligible_benefit_id'] )->where('disability_state_checklist_id',$disability_state_checklist_id)->first();
        } else {
            $notification_disability_state =   $this->query()->where('medical_expense_id', $input['medical_expense']  )->where('disability_state_checklist_id',$disability_state_checklist_id)->first();
        }

        if (($notification_disability_state) && $input['disability_state'. $disability_state_checklist_id] == 1 ){
            $notification_disability_state_id = $notification_disability_state->id;
            $notification_disability_state->update([
                'percent_of_ed_ld'=> (isset($input['percent_of_ed_ld'.$disability_state_checklist_id]) And ($input['percent_of_ed_ld'.$disability_state_checklist_id])) ? ($input['percent_of_ed_ld'.$disability_state_checklist_id]) : 50,
                'days'=> $input['days'.$disability_state_checklist_id],
                'from_date' => $input['from_date'.$disability_state_checklist_id] ?? NULL,
                'to_date' => $input['to_date'.$disability_state_checklist_id] ?? NULL,
            ]);
            if (isset($input['notification_eligible_benefit_id'])) {
                $this->autoUpdateFromToDatesProgressive($input);
            } else {
                $this->autoUpdateFromToDates($input);
            }

        }elseif ((($input['disability_state'. $disability_state_checklist_id]) == 0) && ($notification_disability_state) ){
            $notification_disability_state_id = $notification_disability_state->id;
            $this->deleteIfFeedbackIsZero($notification_disability_state_id);
        }else{
            if (($input['disability_state'. $disability_state_checklist_id]) == 1) {
                $this->create($disability_state_checklist_id,$input);
            }
        }
    }

    public function deleteIfFeedbackIsZero($id)
    {
        $updated_notification_disability_state = $this->query()->where('id',$id)->first();
        if ($updated_notification_disability_state){
            $updated_notification_disability_state->delete();
        }
    }

    /**
     * @param $input
     */
    public function autoUpdateFromToDates($input)
    {
        $notification_disability_states = $this->query()->where('medical_expense_id', $input['medical_expense'] )->orderBy('disability_state_checklist_id','asc')->get();
        $from_date = Carbon::parse($input['incident_date']);
//        $to_date = $to_date->subDay(1);
        foreach ($notification_disability_states as $notification_disability_state) {

            $notification_disability_state->update(['from_date' => $from_date]);

            $to_date = $from_date->addDays($notification_disability_state->days);
            $notification_disability_state->update(['to_date'=>$to_date]);
            $from_date = $to_date->addDay(1);
        }
    }

    public function autoUpdateFromToDatesProgressive($input)
    {
        $notification_disability_states = $this->query()->where('notification_eligible_benefit_id', $input['notification_eligible_benefit_id'] )->orderBy('disability_state_checklist_id','asc')->get();

        $prevState = $this->query()->whereHas("benefit", function ($query) use ($input) {
            $query->where("notification_report_id", $input['incident_id']);
        })->orderByDesc("id")->offset(1)->limit(1);

        if ($prevState->count()) {
            $prevtodate = Carbon::parse($prevState->first()->to_date);
            $from_date = $prevtodate->addDay(1);
        } else {
            $from_date = Carbon::parse($input['incident_date']);
        }

        foreach ($notification_disability_states as $notification_disability_state) {

            $notification_disability_state->update(['from_date' => $from_date]);
            $to_date = $from_date->addDays($notification_disability_state->days);
            $notification_disability_state->update(['to_date'=>$to_date]);
            $from_date = $to_date->addDay(1);
        }
    }

    /**
     * @param $disability_state_checklist_id
     * @param $input
     * @throws GeneralException
     */
    public function checkIfStateExist($disability_state_checklist_id, $input){
//        $notification_disability_state = $this->query()->where('medical_expense_id',$input['medical_expense'] )->where('disability_state_checklist_id',$disability_state_checklist_id)->first();

        /* check if checklist exist for notification report - 08-Jun-2018*/
        $medical_expenses = new MedicalExpenseRepository();
        $medical_expense = $medical_expenses->find($input['medical_expense']);
        $notification_report_id = $medical_expense->notification_report_id;
        $notification_disability_state = $this->query()->where('disability_state_checklist_id', $disability_state_checklist_id)->whereHas('medicalExpense', function ($query) use ($notification_report_id) {
            $query->where('notification_report_id', $notification_report_id);
        })->first();
        /*end--*/

        if ($notification_disability_state){
            throw new GeneralException(trans('exceptions.backend.claim.notification_disability_state_already_exist'));
        }

    }

    /**
     * @param $id
     * @return int
     * @throws GeneralException
     */
    public function findTotalDays($id)
    {
        $notification_disability_state = $this->findOrThrowException($id);
        $no_of_days = Carbon::parse($notification_disability_state->from_date)->diffInDays(Carbon::parse($notification_disability_state->to_date));
        return $no_of_days;
    }

    /**
     * @param $id
     * @return array
     * @throws GeneralException
     */
    public function findTotalDaysWorked($id)
    {
        $notification_disability_state = $this->findOrThrowException($id);
        $percent_of_ed_ld = $notification_disability_state->percent_of_ed_ld;
        $total_days_inserted = $this->findTotalDays($id); //Total days inserted without considering percent of ed/ld
        $total_days_worked = $total_days_inserted * $percent_of_ed_ld * 0.01 ;

        return ['total_days_worked' => $total_days_worked, 'total_days_inserted' => $total_days_inserted ];

    }

}