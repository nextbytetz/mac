<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\ClaimCompensation;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Repositories\Backend\Operation\Claim\Traits\ClaimCompensation\CalculateTdProgressiveTrait;
use App\Repositories\Backend\Operation\Claim\Traits\ClaimCompensation\UndoClaimCompensationTrait;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class ClaimCompensationRepository
 * @package App\Repositories\Backend\Operation\Claim
 * @Author Martin Luhanjo <m.luhanjo@nextbyte.co.tz> | Erick Chrysostom <e.chrysostom@nextbyte.co.tz>
 */

class ClaimCompensationRepository extends  BaseRepository
{

    const MODEL = ClaimCompensation::class;
    use CalculateTdProgressiveTrait, UndoClaimCompensationTrait;

    protected $medical_expenses;
    protected $benefit_types;
    protected $employees;
    protected $notification_reports;
    protected $pensioners;
    protected $dependents;

    public function __construct()
    {

        $this->medical_expenses = new MedicalExpenseRepository();
        $this->benefit_types = new BenefitTypeRepository();
        $this->employees = new EmployeeRepository();
        $this->notification_reports = new NotificationReportRepository();
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $claim_compensation = $this->query()->find($id);

        if (!is_null($claim_compensation)) {
            return $claim_compensation;
        }
        throw new GeneralException(trans('exceptions.backend.claim.claim_compensation_not_found'));
    }

    /*
     * create new
     */
    public function create($input)
    {
        $claim_compensation = $this->query()->create($input);
    }

    /*
     * update
     */
    public function update($id, $input)
    {

    }

    public function updateOrCreate($input)
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @description Pay this claim
     */
    public function pay($id)
    {
        $claim_compensation = $this->findOrThrowException($id);
        $claim_compensation->update(['ispaid'=> 1]);
        return $claim_compensation;
    }



    /**
     * Get Distinct Pending compensations (Not yet payment processed)
     */
    public function getDistinctPendingCompensations(){
        return $this->query()->distinct('claim_id')->select(['claim_id','member_type_id', 'resource_id'])->where('ispaid', 0);
    }

    /**
     * Get All Pending compensations (Not yet payment processed)
     */
    public function getPendings(){
        return $this->query()->where('ispaid', 0)->whereNull('notification_eligible_benefit_id');
    }

    /**
     * @param Model $incident
     * @return array
     */
    public function checkMmiDiff(Model $incident)
    {
        $claim = $incident->claim;
        $mmi_date = Carbon::parse($claim->date_of_mmi);
        $now = Carbon::now();
        $diff = $now->diffInDays($mmi_date);
        return ['diff' => $diff, 'mmi_date' => $mmi_date->format("dS F Y")];
    }

    /**
     * @param $td
     * @param $pd
     * @param int $has_accumulation
     * @return string
     */
    public function getAppendSuffixPdNoteTemplate($td, $pd, $has_accumulation = 0) {
        if ($pd >= 30 And $pd <100) {
            //Has monthly pension payment (PPD).
            if ($has_accumulation) {
                //ppd_accumulation_pay
                $td .= "_ppd_accumulation_pay";
            } else {
                //ppd_month
                $td .= "_ppd_month";
            }
        } elseif ($pd == 100) {
            //Has monthly pension payment (PTD)
            if ($has_accumulation) {
                //ptd_accumulation_pay
                $td .= "_ptd_accumulation_pay";
            } else {
                //ptd_month
                $td .= "ptd_month";
            }
        } elseif ($pd > 0) {
            //Has only lump-sum.
            //ppd
            $td .= "_ppd";
        }
        return $td;
    }

    /**
     * @param Model $incident
     * @return string
     * @throws \Throwable
     */
    public function getLegacyPayNoteForApproval(Model $incident)
    {
        $employee = $incident->employee->name;
        $employer = $incident->employer->name;
        $claim = $incident->claim;
        $data = $this->claimCompensationApprove($incident->id, 0);
        $note = "";
        try {
            switch ($incident->incident_type_id) {
                case 1:
                case 2:
                    //Accident
                    //Disease
                    $payable_ttd_amount = (isset($data['payable_ttd_amount'])) ? $data['payable_ttd_amount'] : 0;
                    $payable_tpd_amount = (isset($data['payable_tpd_amount'])) ? $data['payable_tpd_amount'] : 0;
                    $ppd_lumpsum_payable = (isset($data['ppd_lumpsum_payable'])) ? $data['ppd_lumpsum_payable'] : 0;
                    $monthly_pension_payable = (isset($data['monthly_pension_payable'])) ? $data['monthly_pension_payable'] : 0;
                    $total_mae = $data['total_mae'];
                    $pd = $claim->pd;
                    switch (true) {
                        case ($payable_ttd_amount > 0) And  ($payable_tpd_amount > 0):
                            $td = "ttd_tpd";
                            break;
                        case ($payable_ttd_amount > 0) And  ($payable_tpd_amount == 0):
                            $td = "ttd";
                            break;
                        case ($payable_ttd_amount == 0) And  ($payable_tpd_amount > 0):
                            $td = "tpd";
                            break;
                        default:
                            $td = "";
                            break;
                    }
                    $has_accumulation = 0;
                    if ($pd >=30 And $pd <= 100) {
                        $check = $this->checkMmiDiff($incident);
                        if ($check['diff']) {
                            $has_accumulation = 1;
                        }
                        $mmi_date = $check['mmi_date'];
                    }
                    $template = $this->getAppendSuffixPdNoteTemplate($td, $pd, $has_accumulation);
                    $mae = [];
                    $mae[0] = "";
                    $mae[1] = "";
                    $mae[2] = "";
                    $data['td'] = $payable_ttd_amount + $payable_tpd_amount;
                    $data['pd'] = $ppd_lumpsum_payable + $monthly_pension_payable;
                    $data['td_lumpsum'] = $data['td'];
                    $data['pd_lumpsum'] = $data['pd'];

                    $employee_mae_count = 0;
                    if ($data['total_mae'] > 1) {
                        $employee_mae_count = $incident->medicalExpenses()->where("medical_expenses.member_type_id", 2)->count();
                        $has_employee_mae = ($employee_mae_count) ? 1 : 0;
                        $has_td = ($data['td'] > 0) ? 1 : 0;
                        $has_pd = ($data['pd'] > 0) ? 1 : 0;

                        switch (true) {
                            case ($has_employee_mae And $has_td And $has_pd):
                            case ($has_employee_mae And !$has_td And $has_pd):
                                //2-partially-employee
                                if ($pd >= 30) {
                                    $data['pd_lumpsum'] = $data['pd'];
                                    $amount = number_2_format($data['total_mae']);
                                    $mae[2] = " and payment of TZS {$amount} to {$employee} being MAE";
                                } else {
                                    $data['pd_lumpsum'] = $data['pd'] + $data['total_mae'];
                                    $mae[2] = " and MAE";
                                }
                                break;
                            case ($has_employee_mae And $has_td And !$has_pd):
                                //2-fully-employee
                                $amount = number_2_format($data['total_mae']);
                                $mae[2] = " and payment of TZS {$amount} to {$employee} being MAE";
                                break;
                            case ($has_employee_mae And !$has_td And !$has_pd):
                                //1-fully-employee
                                //will use MAE template for employee
                                //Possible templates : {mae_employee}
                                break;
                            case (!$has_employee_mae And !$has_td And $has_pd):
                                //2-fully-employer
                                $amount = number_2_format($data['total_mae']);
                                $mae[2] = " and payment of TZS {$amount} to {$employer} being MAE";
                                break;
                            case (!$has_employee_mae And !$has_td And !$has_pd):
                                //1-fully-employer
                                //will use MAE template for employer
                                //Possible templates : {mae_employer}
                                break;
                            case (!$has_employee_mae And $has_td And $has_pd):
                            case (!$has_employee_mae And $has_td And !$has_pd):
                                //1-partially-employer
                                $data['td_lumpsum'] = $data['td'] + $data['total_mae'];
                                $mae[1] = " and MAE";
                                break;
                        }
                        $amount = number_2_format($data['total_mae']);
                        $mae[0] = " and TZS {$amount} for Medical Aid Expenses (MAE) per the submitted receipts.";
                    }
                    //format all number items in $data variable
                    foreach ($data as $key => $value) {
                        $data[$key] = number_2_format($value);
                    }
                    switch ($template) {
                        case "ppd":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ppd", ['employee' => $employee, 'mae' => $mae, 'ppd' => $data['ppd_lumpsum_payable'], 'ppd_lumpsum' => $data['pd_lumpsum']])->render();
                            break;
                        case "ppd_accumulation_pay":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ppd_accumulation", ['employee' => $employee, 'ppd' => $data['monthly_pension_payable'], 'mmi_date' => $mmi_date, 'mae' => $mae])->render();
                            break;
                        case "ppd_month":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ppd_month", ['employee' => $employee, 'mae' => $mae, 'ppd' => $data['monthly_pension_payable']])->render();
                            break;
                        case "ptd_accumulation_pay":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ptd_accumulation", ['employee' => $employee, 'ptd' => $data['monthly_pension_payable'], 'mmi_date' => $mmi_date, 'mae' => $mae])->render();
                            break;
                        case "ptd_month":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ptd_month", ['employee' => $employee, 'mae' => $mae, 'ptd' => $data['monthly_pension_payable']])->render();
                            break;
                        case "tpd":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.tpd", ['employee' => $employee, 'employer' => $employer, 'tpd' => $data['payable_tpd_amount'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "tpd_ppd":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.tpd_ppd", ['employee' => $employee, 'employer' => $employer, 'tpd' => $data['payable_tpd_amount'], 'ppd' => $data['ppd_lumpsum_payable'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td'], 'ppd_lumpsum' => $data['pd_lumpsum']])->render();
                            break;
                        case "tpd_ppd_accumulation_pay":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.tpd_ppd_accumulation", ['employee' => $employee, 'employer' => $employer, 'tpd' => $data['payable_tpd_amount'], 'ppd' => $data['monthly_pension_payable'], 'mmi_date' => $mmi_date, 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "tpd_ppd_month":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.tpd_ppd_month", ['employee' => $employee, 'employer' => $employer, 'tpd' => $data['payable_tpd_amount'], 'ppd' => $data['monthly_pension_payable'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "tpd_ptd_accumulation_pay":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.tpd_ptd_accumulation", ['employee' => $employee, 'employer' => $employer, 'tpd' => $data['payable_tpd_amount'], 'ptd' => $data['monthly_pension_payable'], 'mmi_date' => $mmi_date, 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "tpd_ptd_month":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.tpd_ptd_month", ['employee' => $employee, 'employer' => $employer, 'tpd' => $data['payable_tpd_amount'], 'ptd' => $data['monthly_pension_payable'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "ttd":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd", ['employee' => $employee, 'mae' => $mae, 'ttd' => $data['payable_ttd_amount'], 'td_total' => $data['td'], 'td_lumpsum' => $data['td_lumpsum'], 'employer' => $employer])->render();
                            break;
                        case "ttd_ppd":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_ppd", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'ppd' => $data['ppd_lumpsum_payable'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td'], 'pd_lumpsum' => $data['pd_lumpsum']])->render();
                            break;
                        case "ttd_ppd_accumulation_pay":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_ppd_accumulation", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'ppd' => $data['monthly_pension_payable'], 'mmi_date' => $mmi_date, 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "ttd_ppd_month":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_ppd_month", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'ppd' => $data['monthly_pension_payable'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "ttd_ptd_accumulation_pay":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_ptd_accumulation", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'ptd' => $data['monthly_pension_payable'], 'mmi_date' => $mmi_date, 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "ttd_ptd_month":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_ptd_month", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'ptd' => $data['monthly_pension_payable'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum'], 'td_total' => $data['td']])->render();
                            break;
                        case "ttd_tpd":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd", ['employee' => $employee, 'employer' => $employer,'ttd' => $data['payable_ttd_amount'], 'tpd' => $data['payable_tpd_amount'], 'td_total' => $data['td'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum']])->render();
                            break;
                        case "ttd_tpd_ppd":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ppd", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'tpd' => $data['payable_tpd_amount'], 'ppd' => $data['ppd_lumpsum'], 'td_total' => $data['td'], 'td_lumpsum' => $data['td_lumpsum'], 'mae' => $mae, 'ppd_lumpsum' => $data['pd_lumpsum']])->render();
                            break;
                        case "ttd_tpd_ppd_accumulation_pay":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ppd_accumulation", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'tpd' => $data['payable_tpd_amount'], 'ppd' => $data['monthly_pension_payable'], 'td_total' => $data['td'], 'mmi_date' => $mmi_date, 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum']])->render();
                            break;
                        case "ttd_tpd_ppd_month":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ppd_month", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'tpd' => $data['payable_tpd_amount'], 'ppd' => $data['monthly_pension_payable'], 'td_total' => $data['td'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum']])->render();
                            break;
                        case "ttd_tpd_ptd_accumulation_pay":
                            //
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ptd_accumulation", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'tpd' => $data['payable_tpd_amount'], 'ptd' => $data['monthly_pension_payable'], 'td_total' => $data['td'], 'mmi_date' => $mmi_date, 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum']])->render();
                            break;
                        case "ttd_tpd_ptd_month":
                            //DONE
                            $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ptd_month", ['employee' => $employee, 'employer' => $employer, 'ttd' => $data['payable_ttd_amount'], 'tpd' => $data['payable_tpd_amount'], 'ptd' => $data['monthly_pension_payable'], 'td_total' => $data['td'], 'mae' => $mae, 'td_lumpsum' => $data['td_lumpsum']])->render();
                            break;
                        default:
                            if ($total_mae > 0) {
                                if ($employee_mae_count) {
                                    //Employee
                                    $note = getLanguageBlock("backend.lang.benefit.note.mae_employee", ['employee' => $employee, 'mae' => $total_mae])->render();
                                } else {
                                    //Employer
                                    $note = getLanguageBlock("backend.lang.benefit.note.mae_employer", ['employer' => $employer, 'mae' => $total_mae, 'employee' => $employee])->render();
                                }
                            }
                            break;
                    }
                    break;
                default:
                    //Death

                    break;
            }
        } catch (\Exception $e) {
            log_error($e);
        } finally {

        }
        return $note;
    }

    /**
     * @param Model $incident
     * @param $eligible_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Throwable
     */
    public function getPayNoteForApproval(Model $incident, $eligible_id)
    {
        $eligible = (new NotificationEligibleBenefitRepository())->find($eligible_id);
        $employee = $incident->employee->name;
        $employer = $incident->employer->name;

        $data = $this->progressiveCompensationApprove($incident, 0, $eligible_id);
        $note = "";
        //return $data;

        try {
            switch ($incident->incident_type_id) {
                case 1:
                case 2:
                    //Accident
                    //Disease
                    switch ($eligible->benefit_type_claim_id) {
                        case 1:
                            //MAE Refund (Employee/Employer)
                            $mae = number_2_format($data['mae'][0]['assessed_amount']);
                            if ($data['mae'][0]['assessed_amount'] != 0) {
                                if ($eligible->member_type_id == 1) {
                                    //Employer
                                    $note = getLanguageBlock("backend.lang.benefit.note.mae_employer", ['employer' => $employer, 'mae' => $mae, 'employee' => $employee])->render();
                                } elseif ($eligible->member_type_id == 2) {
                                    //Employee
                                    $note = getLanguageBlock("backend.lang.benefit.note.mae_employee", ['employee' => $employee, 'mae' => $mae])->render();
                                }
                            }
                            break;
                        case 3:
                        case 4:
                            //Temporary Disablement
                            //Temporary Disablement Refund
                            //dd($data);
                            switch (true) {
                                case (isset($data['td'][0]['ttd_amount'], $data['td'][0]['tpd_amount'], $data['pd']['ppd']) And $data['td'][0]['ttd_amount'] > 0 And $data['td'][0]['tpd_amount'] > 0):
                                    //ttd_tpd_ppd
                                    $ttd_amount = number_2_format($data['td'][0]['ttd_amount']);
                                    $tpd_amount = number_2_format($data['td'][0]['tpd_amount']);
                                    $td_total = number_2_format($data['td'][0]['ttd_amount'] + $data['td'][0]['tpd_amount']);
                                    $ppd = number_2_format($data['pd']['ppd']);
                                    $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ppd", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd_amount, 'tpd' => $tpd_amount, 'ppd' => $ppd, 'td_total' => $td_total, 'td_lumpsum' => $td_total, 'ppd_lumpsum' => $data['ppd']])->render();
                                    break;
                                case (isset($data['td'][0]['ttd_amount'], $data['td'][0]['tpd_amount'], $data['pd']['ppd_month']) And $data['td'][0]['ttd_amount'] > 0 And $data['td'][0]['tpd_amount'] > 0):
                                    $ttd_amount = number_2_format($data['td'][0]['ttd_amount']);
                                    $tpd_amount = number_2_format($data['td'][0]['tpd_amount']);
                                    $ppd = number_2_format($data['pd']['ppd_month']);
                                    $td_total = number_2_format($data['td'][0]['ttd_amount'] + $data['td'][0]['tpd_amount']);
                                    //check the difference of date from MMI
                                    $check = $this->checkMmiDiff($incident);
                                    if ($check['diff']) {
                                        $mmi_date = $check['mmi_date'];
                                        //ttd_tpd_ppd_accumulation
                                        $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ppd_accumulation", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd_amount, 'tpd' => $tpd_amount, 'ppd' => $ppd, 'td_total' => $td_total, 'mmi_date' => $mmi_date, 'td_lumpsum' => $td_total])->render();
                                    } else {
                                        //ttd_tpd_ppd_month
                                        $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ppd_month", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd_amount, 'tpd' => $tpd_amount, 'ppd' => $ppd, 'td_total' => $td_total, 'td_lumpsum' => $td_total])->render();
                                    }
                                    break;
                                case (isset($data['td'][0]['ttd_amount'], $data['td'][0]['tpd_amount'], $data['pd']['ptd']) And $data['td'][0]['ttd_amount'] > 0 And $data['td'][0]['tpd_amount'] > 0):
                                    $ttd = number_2_format($data['td'][0]['ttd_amount']);
                                    $tpd = number_2_format($data['td'][0]['tpd_amount']);
                                    $ptd = number_2_format($data['pd']['ptd']);
                                    $td_total = number_2_format($data['td'][0]['ttd_amount'] + $data['td'][0]['tpd_amount']);
                                    //check the difference of date from MMI
                                    $check = $this->checkMmiDiff($incident);
                                    if ($check['diff']) {
                                        $mmi_date = $check['mmi_date'];
                                        //ttd_tpd_ptd_accumulation
                                        $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ptd_accumulation", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd, 'tpd' => $tpd, 'ptd' => $ptd, 'td_total' => $td_total, 'mmi_date' => $mmi_date, 'td_lumpsum' => $td_total])->render();
                                    } else {
                                        //ttd_tpd_ptd_month
                                        $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd_ptd_month", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd, 'tpd' => $tpd, 'ptd' => $ptd, 'td_total' => $td_total, 'td_lumpsum' => $td_total])->render();
                                    }
                                    break;
                                case (isset($data['td'][0]['tpd_amount'], $data['pd']['ppd']) And $data['td'][0]['tpd_amount'] > 0):
                                    //tpd_ppd
                                    $tpd = number_2_format($data['td'][0]['tpd_amount']);
                                    $ppd = number_2_format($data['pd']['ppd']);
                                    $note = getLanguageBlock("backend.lang.benefit.note.tpd_ppd", ['employee' => $employee, 'employer' => $employer, 'tpd' => $tpd, 'ppd' => $ppd, 'td_lumpsum' => $tpd, 'td_total' => $tpd, 'ppd_lumpsum' => $ppd])->render();
                                    break;
                                case (isset($data['td'][0]['tpd_amount'], $data['pd']['ppd_month']) And $data['td'][0]['tpd_amount'] > 0):
                                    $tpd = number_2_format($data['td'][0]['tpd_amount']);
                                    $ppd = number_2_format($data['pd']['ppd_month']);
                                    //check the difference of date from MMI
                                    $check = $this->checkMmiDiff($incident);
                                    if ($check['diff']) {
                                        $mmi_date = $check['mmi_date'];
                                        //tpd_ppd_accumulation
                                        $note = getLanguageBlock("backend.lang.benefit.note.tpd_ppd_accumulation", ['employee' => $employee, 'employer' => $employer, 'tpd' => $tpd, 'ppd' => $ppd, 'mmi_date' => $mmi_date, 'td_lumpsum' => $tpd, 'td_total' => $tpd])->render();
                                    } else {
                                        //tpd_ppd_month
                                        $note = getLanguageBlock("backend.lang.benefit.note.tpd_ppd_month", ['employee' => $employee, 'employer' => $employer, 'tpd' => $tpd, 'ppd' => $ppd, 'td_lumpsum' => $tpd, 'td_total' => $tpd])->render();
                                    }
                                    break;
                                case (isset($data['td'][0]['tpd_amount'], $data['pd']['ptd']) And $data['td'][0]['tpd_amount'] > 0):
                                    //check the difference of date from MMI
                                    $tpd = number_2_format($data['td'][0]['tpd_amount']);
                                    $ptd = number_2_format($data['pd']['ptd']);
                                    $check = $this->checkMmiDiff($incident);
                                    if ($check['diff']) {
                                        $mmi_date = $check['mmi_date'];
                                        //tpd_ptd_accumulation
                                        $note = getLanguageBlock("backend.lang.benefit.note.tpd_ptd_accumulation", ['employee' => $employee, 'employer' => $employer, 'tpd' => $tpd, 'ptd' => $ptd, 'mmi_date' => $mmi_date, 'td_lumpsum' => $tpd, 'td_total' => $tpd])->render();
                                    } else {
                                        //tpd_ptd_month
                                        $note = getLanguageBlock("backend.lang.benefit.note.tpd_ptd_month", ['employee' => $employee, 'employer' => $employer, 'tpd' => $tpd, 'ptd' => $ptd, 'td_lumpsum' => $tpd, 'td_total' => $tpd])->render();
                                    }
                                    break;
                                case (isset($data['td'][0]['ttd_amount'], $data['pd']['ppd']) And $data['td'][0]['ttd_amount'] > 0):
                                    //ttd_ppd
                                    $ttd = number_2_format($data['td'][0]['ttd_amount']);
                                    $ppd = number_2_format($data['pd']['ppd']);
                                    $note = getLanguageBlock("backend.lang.benefit.note.ttd_ppd", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd, 'ppd' => $ppd,  'td_lumpsum' => $ttd, 'td_total' => $ttd, 'ppd_lumpsum' => $ppd])->render();
                                    break;
                                case (isset($data['td'][0]['ttd_amount'], $data['pd']['ppd_month']) And $data['td'][0]['ttd_amount'] > 0):
                                    $ttd = number_2_format($data['td'][0]['ttd_amount']);
                                    $ppd = number_2_format($data['pd']['ppd_month']);
                                    //check the difference of date from MMI
                                    $check = $this->checkMmiDiff($incident);
                                    if ($check['diff']) {
                                        $mmi_date = $check['mmi_date'];
                                        //ttd_ppd_accumulation
                                        $note = getLanguageBlock("backend.lang.benefit.note.ttd_ppd_accumulation", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd, 'ppd' => $ppd, 'mmi_date' => $mmi_date, 'td_lumpsum' => $ttd, 'td_total' => $ttd])->render();
                                    } else {
                                        //ttd_ppd_month
                                        $note = getLanguageBlock("backend.lang.benefit.note.ttd_ppd_month", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd, 'ppd' => $ppd, 'td_lumpsum' => $ttd, 'td_total' => $ttd])->render();
                                    }
                                    break;
                                case (isset($data['td'][0]['ttd_amount'], $data['pd']['ptd']) And $data['td'][0]['ttd_amount'] > 0):
                                    $ttd = number_2_format($data['td'][0]['ttd_amount']);
                                    $ptd = number_2_format($data['pd']['ptd']);
                                    //check the difference of date from MMI
                                    $check = $this->checkMmiDiff($incident);
                                    if ($check['diff']) {
                                        $mmi_date = $check['mmi_date'];
                                        //ttd_ptd_accumulation
                                        $note = getLanguageBlock("backend.lang.benefit.note.ttd_ptd_accumulation", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd, 'ptd' => $ptd, 'mmi_date' => $mmi_date, 'td_lumpsum' => $ttd, 'td_total' => $ttd])->render();
                                    } else {
                                        //ttd_ptd_month
                                        $note = getLanguageBlock("backend.lang.benefit.note.ttd_ptd_month", ['employee' => $employee, 'employer' => $employer, 'ttd' => $ttd, 'ptd' => $ptd, 'td_lumpsum' => $ttd, 'td_total' => $ttd])->render();
                                    }
                                    break;
                                case (isset($data['td'][0]['ttd_amount'], $data['td'][0]['tpd_amount']) And $data['td'][0]['ttd_amount'] > 0 And $data['td'][0]['tpd_amount'] > 0):
                                    //ttd_tpd
                                    $ttd_amount = number_2_format($data['td'][0]['ttd_amount']);
                                    $tpd_amount = number_2_format($data['td'][0]['tpd_amount']);
                                    $td_total = number_2_format($data['td'][0]['ttd_amount'] + $data['td'][0]['tpd_amount']);

                                    $note = getLanguageBlock("backend.lang.benefit.note.ttd_tpd", ['employee' => $employee, 'employer' => $employer,'ttd' => $ttd_amount, 'tpd' => $tpd_amount, 'td_total' => $td_total, 'td_lumpsum' => $td_total])->render();
                                    break;
                                case (isset($data['td'][0]['ttd_amount']) And $data['td'][0]['ttd_amount'] > 0):
                                    //ttd
                                    $ttd = number_2_format($data['td'][0]['ttd_amount']);

                                    $note = getLanguageBlock("backend.lang.benefit.note.ttd", ['employee' => $employee, 'ttd' => $ttd, 'td_total' => $ttd, 'td_lumpsum' => $ttd, 'employer' => $employer])->render();
                                    break;
                                case (isset($data['td'][0]['tpd_amount']) And $data['td'][0]['tpd_amount'] > 0):
                                    //tpd
                                    $tpd = number_2_format($data['td'][0]['tpd_amount']);

                                    $note = getLanguageBlock("backend.lang.benefit.note.tpd", ['employee' => $employee, 'employer' => $employer, 'tpd' => $tpd, 'td_lumpsum' => $tpd, 'td_total' => $tpd])->render();
                                    break;
                            }
                            break;
                        case 5:
                            //Permanent Disablement
                            switch (true) {
                                case (isset($data['pd']['ppd'])):
                                    //ppd
                                    $ppd = number_2_format($data['pd']['ppd']);
                                    $note = getLanguageBlock("backend.lang.benefit.note.ppd", ['employee' => $employee, 'ppd' => $ppd, 'pd_lumpsum' => $ppd])->render();
                                    break;
                                case (isset($data['pd']['ppd_month'])):
                                    //check the difference of date from MMI
                                    $ppd = number_2_format($data['pd']['ppd_month']);
                                    $check = $this->checkMmiDiff($incident);
                                    if ($check['diff']) {
                                        $mmi_date = $check['mmi_date'];
                                        //ppd_accumulation
                                        $note = getLanguageBlock("backend.lang.benefit.note.ppd_accumulation", ['employee' => $employee, 'ppd' => $ppd, 'mmi_date' => $mmi_date])->render();
                                    } else {
                                        //ppd_month
                                        $note = getLanguageBlock("backend.lang.benefit.note.ppd_month", ['employee' => $employee, 'ppd' => $ppd])->render();
                                    }
                                    break;
                                case (isset($data['pd']['ptd'])):
                                    $ptd = number_2_format($data['pd']['ptd']);
                                    //check the difference of date from MMI
                                    $check = $this->checkMmiDiff($incident);
                                    if ($check['diff']) {
                                        $mmi_date = $check['mmi_date'];
                                        //ptd_accumulation
                                        $note = getLanguageBlock("backend.lang.benefit.note.ptd_accumulation", ['employee' => $employee, 'ptd' => $ptd, 'mmi_date' => $mmi_date])->render();
                                    } else {
                                        //ptd_month
                                        $note = getLanguageBlock("backend.lang.benefit.note.ptd_month", ['employee' => $employee, 'ptd' => $ptd])->render();
                                    }
                                    break;
                            }
                    }
                    break;
                case 3:
                    //Death
                    break;
            }
        } catch (\Exception $e) {
            log_error($e);
        } finally {
            //$note = "";
        }

        return $note;
    }

    /**
     * @param Model $incident
     * @param $type | 1 => Save, 0 => Review
     * @param int $eligible_id |
     * @return mixed
     */
    public function progressiveCompensationApprove(Model $incident, $type = 0, $eligible_id = 0)
    {
        return DB::transaction(function () use ($incident, $type, $eligible_id) {
            $claimRepo = new ClaimRepository();
            $eligibleRepo = new NotificationEligibleBenefitRepository();
            $claim = $incident->claim;
            $employer_id = $incident->employer_id;
            $employee_id = $incident->employee_id;
            $pd = $claim->pd_formatted;
            $ttd_amount = 0;   $tpd_amount = 0; $total_mae = []; $total_td = [];
            $total_days = 0;  $ttd_days = 0;  $tpd_days = 0; $total_lumpsum = 0;
            $ppd_lumpsum = $this->findPpdLumpsumProgressive($incident);
            $input = ['claim_id' => $claim->id, 'user_id' => access()->id() ?? 85];
            $return = []; $temporary_return = []; $permanent_partial_return =[];$permanent_total_return =[]; $death_return =[]; $td_return = [];  $mae_return = []; $total_return = [];
            $permanent_return = [];
            $pd_return = ['pd' => $permanent_return, 'total_lumpsum' => $total_lumpsum, 'eligible' => $eligible_id, 'letter_status_label' => ''];
            $status = '';

            switch ($incident->incident_type_id) {
                case 3:
                case 4:
                case 5:
                   ////==> Process Survivor Benefits.
                    //Death Incident...
                    $death_return =  ['death' => $this->processDeathProgressive($employee_id, $ppd_lumpsum, $type, $incident, $input, $eligible_id)];
                    break;
            }

            switch ($incident->incident_type_id) {
                case 1:
                case 2:
                case 4:
                case 5:
                    //Accident
                    //Disease
                    ////==> Process MAE
                    // Get total Mae
                    $mae = $eligibleRepo->findTotalHealthServiceCost($incident, $eligible_id);
                    $mae_return = ['mae' => $mae] ;
                    //$total_lumpsum = $total_lumpsum + $mae_return['total_mae'];

                    //Process MAE = 10 (For each member type) when type = 1
                    $this->processMaeProgressive($incident, $input, $type,  $eligible_id);

                    ////==> Process Temporary disablement (TD)
                    if ($eligible_id) {
                        //eligible_id is set and the purpose is to save
                        $eligibles = $incident->benefits()->where("notification_eligible_benefits.id", $eligible_id)->whereIn("benefit_type_claim_id", [3,4])->orderByDesc("notification_eligible_benefits.id")->get();
                    } else {
                        $eligibles = $incident->benefits()->whereIn("processed", [0, 1])->whereIn("benefit_type_claim_id", [3,4])->orderByDesc("notification_eligible_benefits.id")->get();
                    }

                    foreach ($eligibles as $eligible) {

                        $total_lumpsum = 0;
                        $temporary_return = [];

                        $tdinput = $eligibleRepo->calculateTdTotals($eligible, 2);
                        // get ttd_amount
                        $ttd_amount = $tdinput['ttd_amount'];
                        //get tpd amount
                        $tpd_amount = $tdinput['tpd_amount'];
                        // get ttd_hours
                        $ttd_days = $tdinput['ttd_days'];
                        // get tpd_hours
                        $tpd_days = $tdinput['tpd_days'];

                        $total_days = $ttd_days + $tpd_days;

                        $status = $eligible->processed_label;

                        // Compute Temporary compensation-----------check if total days exceed min days (3 days)--
                        //Todo reset if condition once zero paid is cleared
//                        if (($ttd_amount > 0 Or $tpd_amount > 0) And (($total_days) > sysdefs()->data()->min_days_for_ttd_tpd) ) {

                        $temporary_return =  $this->processTemporaryDisablementCompensationProgressive($ttd_amount, $tpd_amount, $input, $employer_id, $ttd_days, $tpd_days, $type, $eligible->id);

                        $total_lumpsum += $temporary_return['payable_ttd_amount'] + $temporary_return['payable_tpd_amount'];
//                        }

                        $total_td[] = ['ttd_amount' =>count($temporary_return) ? $temporary_return['payable_ttd_amount'] :  $ttd_amount, 'tpd_amount' =>count($temporary_return) ? $temporary_return['payable_tpd_amount'] :  $tpd_amount, 'ttd_days' => $ttd_days, 'tpd_days' => $tpd_days, 'total_days' => $total_days, 'eligible' => $eligible->id, 'total_lumpsum' => $total_lumpsum, 'status' => $status, 'letter_status_label' => $eligible->letter_status_label, 'processed' => $eligible->processed];

                    }

                    $td_return = ['td' => $total_td];

                    ////==> Process Permanent Disablement (PD)
                    if ($eligible_id) {
                        $eligible = $incident->benefits()->where(function ($query) use ($eligible_id) {
                            $query->where('notification_eligible_benefits.id', $eligible_id)->orWhere('notification_eligible_benefits.parent_id', $eligible_id);
                        })->where('benefit_type_claim_id', 5)->orderByDesc('notification_eligible_benefits.id')->first();
                    } else {
                        $eligible = $incident->benefits()->whereIn('processed', [0, 1])->where('benefit_type_claim_id', 5)->orderByDesc('notification_eligible_benefits.id')->first();
                    }
                    $total_lumpsum = 0;

                    if ($eligible) {
                        $status = $eligible->processed_label;
                        // Compensation Computation for permanent disablement--------
                        // when employee
                        //if ($pd > 0) {
                            //PTD == 4 ----
                            if ($pd == 100) {
                                // ----Functionality for PTD Calculation for monthly pension ----
                                $permanent_return = $this->processPtdCompensationProgressive($pd, $ppd_lumpsum, $input, $employee_id, $type, $incident, $eligible);
                            } else {
                                // PPD == 3----
                                $permanent_return =   $this->processPpdCompensationProgressive($pd, $ppd_lumpsum, $input, $employee_id, $type, $incident, $eligible);
                                $total_lumpsum += $permanent_return['ppd_lumpsum_payable'];
                            }
                        //}
                        $permanent_return['status'] = $status;
                        $permanent_return['eligible'] = $eligible->id;
                        $permanent_return['letter_status_label'] = $eligible->letter_status_label;

                        $pd_return = ['pd' => $permanent_return, 'total_lumpsum' => $total_lumpsum, 'eligible' => $eligible->id, 'letter_status_label' => $eligible->letter_status_label, 'status' => $eligible->processed_label, 'processed' => $eligible->processed];
                    }

                    break;
            }

            $return = array_merge($death_return, $td_return, $pd_return, $mae_return, $total_return);
            return $return;

        });

    }

    /**
     * Claim Compensation after workflow complete
     * Action_type: 1 => Save, 0 => Review
     */

    public function claimCompensationApprove($notification_report_id, $action_type)
    {

        return DB::transaction(function () use ($notification_report_id, $action_type) {
            //logger('reached here ...');
            $notification_reports = new NotificationReportRepository();
            $claim_compensations = new ClaimCompensationRepository();
            $claims = new ClaimRepository();
            $notification_report = $notification_reports->findOrThrowException($notification_report_id);
            $employer_id = $notification_report->employer_id;
            $employee_id = $notification_report->employee_id;
            $pd = ($notification_report->claim) ? $notification_report->claim->pd_formatted : 0;
            $ttd_amount = 0;   $tpd_amount = 0; $total_mae = 0; $total_lumpsum = 0;
            $total_days = 0;  $ttd_days = 0;  $tpd_days = 0;
            $ppd_lumpsum = $this->findPpdLumpsum($notification_report);
            $input = ['claim_id' => $notification_report->claim->id, 'user_id' => access()->id() ?? 85];
            $return =[]; $temporary_return = []; $permanent_partial_return =[];$permanent_total_return =[]; $death_return =[];  $mae_return =[]; $total_return = [];
            //If Incident is DEATH
            if (in_array($notification_report->incident_type_id, [3,4,5])) {
                $death_return =  $this->processDeath($employee_id, $ppd_lumpsum,$action_type, $notification_report);

            }// else {


            //Other incidents Accident and Disease


            // Get total Mae
            $total_mae = $this->medical_expenses->findTotalHealthServiceCost($notification_report_id);
            $mae_return = ['total_mae'=> $total_mae] ;
            $total_lumpsum = $total_lumpsum + $mae_return['total_mae'];

            //Process MAE =10 (For each member type)
            $this->processMAE($notification_report_id, $input, $action_type);



            // Process Temporary disablement
            foreach ($notification_report->medicalExpenses as $medical_expense) {

                $expense_inputs = $this->medical_expenses->calculateExpenses($medical_expense->id, 2);

                // Get total ttd
                $ttd_amount = $ttd_amount + $expense_inputs['ttd_amount'];
                // Get total tpd
                $tpd_amount = $tpd_amount + $expense_inputs['tpd_amount'];

                // get ttd_hours
                $ttd_days = $ttd_days + $expense_inputs['ttd_days'];
                // get tpd_hours
                $tpd_days = $tpd_days + $expense_inputs['tpd_days'];

            }
            // Get total hours
            $total_days = $tpd_days + $ttd_days;


            // Compute Temporary compensation-----------check if total days exceed min days (3 days)--
//            if (($ttd_amount > 0 || $tpd_amount > 0) && (($total_days_inserted) > sysdefs()->data()->min_days_for_ttd_tpd) ) {
            if (($ttd_amount > 0 || $tpd_amount > 0) ) {
                $temporary_return =  $this->processTemporaryDisablementCompensation($ttd_amount, $tpd_amount, $input, $employer_id,$ttd_days,$tpd_days, $action_type);
                $total_lumpsum =  $total_lumpsum + $temporary_return['payable_ttd_amount'] + $temporary_return['payable_tpd_amount'];
            }

            // Compensation Computation for permanent disablement--------
            //            // when employee
            if ($pd > 0) {
                //PTD == 4 ----
                if ($pd == 100) {
                    // ----Functionality for PTD Calculation for monthly pension ----
                    $permanent_total_return = $this->processPTDCompensation($pd, $ppd_lumpsum, $input, $employee_id,$action_type, $notification_report);
                } elseif ($pd > 0 && $pd < 100) {
                    // PPD == 3----
                    $permanent_partial_return =   $this->processPPDCompensation($pd, $ppd_lumpsum, $input, $employee_id,$action_type, $notification_report);
                    $total_lumpsum = $total_lumpsum +  $permanent_partial_return['ppd_lumpsum_payable'];
                }
            }

//            }
            $total_return = ['total_lumpsum' => $total_lumpsum];

            if ($action_type) {
                //Close Claim by updating the date ...
                $notification_report->closure()->update(['close_date' => Carbon::now()]);
            }

            $return = array_merge($death_return,$temporary_return,$permanent_partial_return,$permanent_total_return,$mae_return, $total_return);
            return $return;
        });
    }


    /**
     * @param $notification_report_id
     * @param $input
     * @param $action_type
     * Process Total Medical Aid Expense per each Member type to be compensated
     */
    public function processMAE($notification_report_id,$input,$action_type){
        $claim_compensations = new ClaimCompensationRepository();
        //Get distinct medical expense
        $medical_expenses = $this->medical_expenses->query()->distinct()->select(['notification_report_id','member_type_id', 'resource_id'])->where('notification_report_id',$notification_report_id)->get();//new
        foreach ($medical_expenses as $medical_expense) {
            //Total expense per Member type
//            $mae = $this->medical_expenses->query()->where('notification_report_id',$notification_report_id)->where('member_type_id',$medical_expense->member_type_id)->where('resource_id',$medical_expense->resource_id)->sum('amount');
            $mae = $this->medical_expenses->findTotalHealthServiceCostByMemberType($notification_report_id, $medical_expense->member_type_id); // 07-Jun-2018
            //MAE == 10
            if ($mae > 0) {
                $other_input = [
                    'amount' => $mae,
                    'benefit_type_id' => 10,
                    'member_type_id' => $medical_expense->member_type_id,
                    'resource_id' => $medical_expense->resource_id,
                    'compensation_payment_type_id' => 1
                ];
                if ($action_type == 1) {
                    $claim_compensation = $claim_compensations->create(array_merge($input, $other_input));
                }
            }
        }
    }

    /**
     * @param Model $incident
     * @param $input
     * @param $type
     */
    public function processMaeProgressive(Model $incident, $input, $type, $eligible_id)
    {
        if ($type == 1) {
            if ($eligible_id) {
                $eligibles = $incident->benefits()->where("notification_eligible_benefits.id", $eligible_id)->where("benefit_type_claim_id", 1)->orderByDesc("notification_eligible_benefits.id")->get();
            } else {
                $eligibles = $incident->benefits()->whereIn("processed", [0, 1])->where("benefit_type_claim_id", 1)->orderByDesc("notification_eligible_benefits.id")->get();
            }

            foreach ($eligibles as $eligible) {
                $expense = $eligible->medicalExpense;
                $other_input = [
                    'amount' => $expense->assessed_amount,
                    'benefit_type_id' => 10,
                    'member_type_id' => $expense->member_type_id,
                    'resource_id' => $expense->resource_id,
                    'compensation_payment_type_id' => 1,
                    'notification_eligible_benefit_id' => $eligible->id,
                ];
                if($this->checkIfClaimCompensationExists(array_merge($input, $other_input))) {
                    $this->create(array_merge($input, $other_input));
                }

            }
        }
    }

    public function processTemporaryDisablementCompensationProgressive($ttd_amount, $tpd_amount, $input, $employer_id, $ttd_days, $tpd_days, $type, $eligible_id)
    {
        /*
        * TTD == 8
        */
        $tpd_months =  ($tpd_days) / sysdefs()->data()->no_of_days_for_a_month_in_assessment; // Get months from days ---> in case for change of not using percent_of_ed_ld
        $ttd_months = ($ttd_days) / sysdefs()->data()->no_of_days_for_a_month_in_assessment; // Get months from days
        $other_ttd_input = [
            'amount' => $ttd_amount,
            'benefit_type_id' => 8,
            'compensation_payment_type_id' => 1,
            'member_type_id' => 1,
            'resource_id' => $employer_id,
            'notification_eligible_benefit_id' => $eligible_id,
        ];

        /*Save TTD / preview derived*/
        if ($ttd_amount > 0) {
            $other_input = $other_ttd_input;
            $payable_ttd_amount = $other_input['amount'];
            if ($type == 1) {
                if($this->checkIfClaimCompensationExists(array_merge($input, $other_input))) {
                    $this->create(array_merge($input, $other_input));
                }
            }

        }

        /*<new 21-Nov-2020> TTD If already exists use claim compensation data (OVERWRITE derived amounts)*/
        if($this->checkIfClaimCompensationExists(array_merge($input, $other_ttd_input)) == false) {
            /*exists*/
            $claim_compensation = $this->getClaimCompensationExists(array_merge($input, $other_ttd_input));
            $payable_ttd_amount = $claim_compensation->amount;
        }


        /*
         * TPD == 7
         */

        $other_tpd_input = [
            'amount' => $tpd_amount,
            'benefit_type_id' => 7,
            'compensation_payment_type_id' => 1,
            'member_type_id' => 1,
            'resource_id' => $employer_id,
            'notification_eligible_benefit_id' => $eligible_id,
        ];

        /*If amount is greater than 0 - Save*/
        if ($tpd_amount > 0) {
            $other_input = $other_tpd_input;
            $payable_tpd_amount = $other_input['amount'];
            if ($type == 1) {
                if($this->checkIfClaimCompensationExists(array_merge($input, $other_input))) {
                    $this->create(array_merge($input, $other_input));
                }
            }

        }
        /*<new 21-Nov-2020> TPD If already exists use claim compensation data (OVERWRITE derived amounts)*/
        if($this->checkIfClaimCompensationExists(array_merge($input, $other_tpd_input)) == false) {
            /*exists*/
            $claim_compensation = $this->getClaimCompensationExists(array_merge($input, $other_tpd_input));
            $payable_tpd_amount = $claim_compensation->amount;
        }


        return ['ttd_amount' => $ttd_amount, 'payable_ttd_amount' => isset($payable_ttd_amount) ? $payable_ttd_amount : 0, 'tpd_amount' => $tpd_amount, 'payable_tpd_amount' => isset($payable_tpd_amount) ? $payable_tpd_amount : 0 ];

    }

    /**
     * @param $ttd_amount
     * @param $tpd_amount
     * @param $input
     * @param $employer_id
     * @param $ttd_days
     * @param $tpd_days
     * @param $action_type
     * @return array
     * Computation of Temporary disablement compensation
     */

    public function processTemporaryDisablementCompensation($ttd_amount, $tpd_amount, $input, $employer_id,$ttd_days,$tpd_days,$action_type)
    {
        /*
   * TTD == 8
   */
        $tpd_months =  ($tpd_days) / sysdefs()->data()->no_of_days_for_a_month_in_assessment; // Get months from days ---> in case for change of not using percent_of_ed_ld
        $ttd_months = ($ttd_days) / sysdefs()->data()->no_of_days_for_a_month_in_assessment; // Get months from days

        if ($ttd_amount > 0) {
            $other_input = [
//                'amount' => $this->benefit_types->getTotalDisablementPayableAmount($ttd_amount,$ttd_months),
                'amount' => $ttd_amount,
                'benefit_type_id' => 8,
                'compensation_payment_type_id' => 1,
                'member_type_id' => 1,
                'resource_id' => $employer_id
            ];
            $payable_ttd_amount = $other_input['amount'];
            if ($action_type == 1) {
                $claim_compensation = $this->create(array_merge($input, $other_input));
            }
        }

        /*
         * TPD == 7
         */

        if ($tpd_amount > 0) {
            $other_input = [
//                'amount' => $this->benefit_types->getPartialDisablementPayableAmount($tpd_amount,$tpd_months),
                'amount' => $tpd_amount,
                'benefit_type_id' => 7,
                'compensation_payment_type_id' => 1,
                'member_type_id' => 1,
                'resource_id' => $employer_id
            ];
            $payable_tpd_amount = $other_input['amount'];
            if ($action_type == 1) {
                $claim_compensation = $this->create(array_merge($input, $other_input));
            }
        }

        return ['ttd_amount' => $ttd_amount, 'payable_ttd_amount' => isset($payable_ttd_amount) ? $payable_ttd_amount : 0, 'tpd_amount' => $tpd_amount, 'payable_tpd_amount' => isset($payable_tpd_amount) ? $payable_tpd_amount : 0 ];
    }

    /**
     * @param $pd
     * @param $ppd_lumpsum
     * @param $input
     * @param $employee_id
     * @param $type
     * @param Model $incident
     * @param $eligible_id
     * @return array
     */
    public function processPpdCompensationProgressive($pd, $ppd_lumpsum, $input, $employee_id, $type, Model $incident, $eligible)
    {
        /*
       * PPD == 3
       */
        $other_input = [];
        $payable_per_month=0;
        $payable_lumpsum = 0;
        $benefit_type_id = 3;
        $claim = $incident->claim;
        $need_cca = $claim->need_cca;
        $payable_per_month_assistant = 0;
        $percent_impairment_scale = sysdefs()->data()->percentage_imparement_scale; //30
        if ($pd > 0 && $pd <= $percent_impairment_scale) {
            // ----Functionality for PPD
            // Get ppd lump sum per month
            $constant_factor_compensation = sysdefs()->data()->constant_factor_compensation; // 84 months
            // $ppd_lump sum_per_month = $ppd_lump sum ;

            $payable_lumpsum = $this->benefit_types->getPartialDisablementPayableAmount($ppd_lumpsum,$constant_factor_compensation, $pd);

            // $payable_lump sum = $payable_per_month * $constant_factor_compensation;
            $other_input = ['amount' => $payable_lumpsum, 'benefit_type_id' => $benefit_type_id, 'compensation_payment_type_id' => 1, 'member_type_id' => 2, 'resource_id' => $employee_id, 'notification_eligible_benefit_id' => $eligible->id];
            if ($type == 1) {
                if($this->checkIfClaimCompensationExists(array_merge($input, $other_input))) {
                    $this->create(array_merge($input, $other_input));
                }
            }


        } elseif ($pd > $percent_impairment_scale && $pd < 100) {
            //-----Functionality for monthly pension
            $payable_per_month = $this->benefit_types->getPartialDisablementPayableAmount($ppd_lumpsum,1, $pd);
            $payable_per_month_assistant = ($need_cca) ? ($payable_per_month * (sysdefs()->data()->assistant_percent * 0.01)) : 0;
            $other_input = ['amount' => $payable_per_month, 'benefit_type_id' => $benefit_type_id, 'compensation_payment_type_id' => 3, 'member_type_id' => 2, 'resource_id' => $employee_id, 'notification_report_id' => $incident->id, 'notification_eligible_benefit_id' => $eligible->id];
            if ($type == 1) {
                /*pensioner*/
                $pensioner = $this->pensioners->create(array_merge($input, $other_input));
                //Assistant
                $assistant = $this->dependents->findActiveAssistant($employee_id);

                if (($assistant) && ($need_cca)) {
                    $assistant = $this->dependents->updateMP($assistant->dependent_id, $payable_per_month_assistant, $employee_id, $incident->id, null);
                }
            }
        } else {
            $other_input = ['amount' => 0, 'benefit_type_id' => $benefit_type_id, 'compensation_payment_type_id' => 1, 'member_type_id' => 2, 'resource_id' => $employee_id, 'notification_report_id' => $incident->id, 'notification_eligible_benefit_id' => $eligible->id];
        }
        /*<new 21-Nov-2020> If already exists use claim compensation data (OVERWRITE derived amounts)*/
        if($this->checkIfClaimCompensationExists(array_merge($input, $other_input)) == false) {
            /*exists*/
            $claim_compensation = $this->getClaimCompensationExists(array_merge($input, $other_input));

            $payable_lumpsum = $claim_compensation->amount;
        }


        $return = ['ppd_lumpsum' => $ppd_lumpsum, 'ppd_lumpsum_payable' => $payable_lumpsum, 'monthly_pension'=> $ppd_lumpsum, 'monthly_pension_payable'=> $payable_per_month, 'monthly_pension_assistant' => $payable_per_month_assistant, 'status' => $eligible->processed_label, 'processed' => $eligible->processed, 'letter_status_label' => $eligible->letter_status_label];

        if ($pd > 0 && $pd <= $percent_impairment_scale) {
            $return['ppd'] = $payable_lumpsum;
        } elseif ($pd > $percent_impairment_scale && $pd < 100) {
            $return['ppd_month'] = $payable_per_month;
        }

        return $return;
    }

    /**
     * @param $pd
     * @param $ppd_lumpsum
     * @param $input
     * @param $employee_id
     * @param $action_type
     * @param Model $notification_report
     * @return array
     */
    public function processPPDCompensation($pd, $ppd_lumpsum, $input, $employee_id,$action_type, Model $notification_report)
    {
        /*
   * PPD == 3
   */
        $payable_per_month=0;
        $payable_lumpsum = 0;
        $benefit_type_id = 3;
        $payable_per_month_assistant = 0;
        $claim = $notification_report->claim;
        $need_cca = $claim->need_cca;
        $percent_impairment_scale = sysdefs()->data()->percentage_imparement_scale; //30
        if ($pd > 0 && $pd <= $percent_impairment_scale) {
            // ----Functionality for PPD
            // Get ppd lump sum per month
            $constant_factor_compensation = sysdefs()->data()->constant_factor_compensation; // 84 months
//            $ppd_lump sum_per_month = $ppd_lump sum ;
            $payable_lumpsum = $this->benefit_types->getPartialDisablementPayableAmount($ppd_lumpsum,$constant_factor_compensation, $pd);

//            $payable_lump sum = $payable_per_month * $constant_factor_compensation;
            $other_input = ['amount' => $payable_lumpsum, 'benefit_type_id' => $benefit_type_id, 'compensation_payment_type_id' => 1, 'member_type_id' => 2, 'resource_id' => $employee_id];
            if ($action_type == 1) {
                $claim_compensation = $this->create(array_merge($input, $other_input));
            }

        } elseif ($pd > $percent_impairment_scale && $pd < 100) {
            //-----Functionality for monthly pension
            $payable_per_month = $this->benefit_types->getPartialDisablementPayableAmount($ppd_lumpsum,1,$pd);
            $payable_per_month_assistant = ($need_cca) ? ($payable_per_month * (sysdefs()->data()->assistant_percent * 0.01)) : 0;
            $other_input = ['amount' => $payable_per_month, 'benefit_type_id' => $benefit_type_id, 'compensation_payment_type_id' => 3, 'member_type_id' => 2, 'resource_id' => $employee_id, 'notification_report_id' => $notification_report->id];
            if ($action_type == 1) {
                /*pensioner*/
                $pensioner = $this->pensioners->create(array_merge($input, $other_input));

                //Assistant
                $assistant = $this->dependents->findActiveAssistant($employee_id);

                if (($assistant) && ($need_cca)) {
                    $assistant = $this->dependents->updateMP($assistant->dependent_id, $payable_per_month_assistant, $employee_id, $notification_report->id, null);
                }
            }
        }

        return ['ppd_lumpsum' => $ppd_lumpsum, 'ppd_lumpsum_payable' => $payable_lumpsum,'monthly_pension'=>$ppd_lumpsum, 'monthly_pension_payable'=>$payable_per_month, 'monthly_pension_assistant' => $payable_per_month_assistant];
    }

    /**
     * @param $pd
     * @param $ppd_lumpsum
     * @param $input
     * @param $employee_id
     * @param $type
     * @param Model $incident
     * @return array
     */
    public function processPtdCompensationProgressive($pd, $ppd_lumpsum, $input, $employee_id, $type, Model $incident, $eligible)
    {
        //-----Functionality for monthly pension
        $payable_per_month = $this->benefit_types->getTotalDisablementPayableAmount($ppd_lumpsum,1);
        $payable_per_month_pensioner = $payable_per_month * (100  * 0.01);
        $payable_per_month_assistant = $payable_per_month * (sysdefs()->data()->assistant_percent * 0.01);
        $other_input = ['benefit_type_id' => 3, 'compensation_payment_type_id' => 3, 'member_type_id' => 2, 'resource_id' => $employee_id , 'notification_report_id' => $incident->id, 'notification_eligible_benefit_id' => $eligible->id];

        //Pensioners
        $amount = ['amount' => $payable_per_month_pensioner];
        if ($type == 1) {

            $pensioner = $this->pensioners->create(array_merge($amount, $other_input));

            //Assistant
            $assistant = $this->dependents->findActiveAssistant($employee_id);

            if ($assistant) {
                $assistant = $this->dependents->updateMP($assistant->dependent_id, $payable_per_month_assistant, $employee_id, $incident->id, $eligible->id);
            }
        }
        return ['monthly_pension' => $ppd_lumpsum, 'monthly_pension_payable' => $payable_per_month, 'monthly_pension_pensioner' => $payable_per_month_pensioner, 'monthly_pension_assistant' => $payable_per_month_assistant, 'ptd' => $payable_per_month_pensioner, 'status' => $eligible->processed_label, 'processed' => $eligible->processed, 'letter_status_label' => $eligible->letter_status_label ];
    }

    /**
     * @param $pd
     * @param $ppd_lumpsum
     * @param $input
     * @param $employee_id
     * @param $action_type
     * @param Model $notification_report
     * @return array
     */
    public function processPTDCompensation($pd, $ppd_lumpsum, $input, $employee_id,$action_type, Model $notification_report)
    {
        //-----Functionality for monthly pension
        $payable_per_month = $this->benefit_types->getTotalDisablementPayableAmount($ppd_lumpsum,1);
        $payable_per_month_pensioner = $payable_per_month * (100  * 0.01);
        $payable_per_month_assistant = $payable_per_month * (sysdefs()->data()->assistant_percent * 0.01);
        $other_input = ['benefit_type_id' => 3, 'compensation_payment_type_id' => 3, 'member_type_id' => 2, 'resource_id' => $employee_id , 'notification_report_id' => $notification_report->id];

        //Pensioners
        $amount = ['amount' => $payable_per_month_pensioner];
        if ($action_type == 1) {
            $pensioner = $this->pensioners->create(array_merge($amount, $other_input));
            //Assistant
            $assistant = $this->dependents->findActiveAssistant($employee_id);
            if($assistant){
                $assistant = $this->dependents->updateMP($assistant->dependent_id, $payable_per_month_assistant,$employee_id, $notification_report->id);
            }


        }
        return ['monthly_pension' => $ppd_lumpsum,'monthly_pension_payable'=>$payable_per_month, 'monthly_pension_pensioner'=>$payable_per_month_pensioner,'monthly_pension_assistant' => $payable_per_month_assistant ];
    }

    /**
     * @param $employee_id
     * @param $ptd_pension
     * @param $action_type
     * @param Model $notification_report
     * @return array
     * @throws GeneralException
     * @description Process claim compensation for DEATH
     */
    public function processDeath($employee_id, $ptd_pension, $action_type, Model $notification_report, $eligible_id = null){

        $monthly_pension_payable =  $this->benefit_types->getTotalDisablementPayableAmount($ptd_pension,1);
        $lump_sum_payable =  $this->benefit_types->getLumpsumMaxPayableAmountByPayablePension($ptd_pension);
        if ($action_type == 1){
            $this->processPension($employee_id, $ptd_pension, $notification_report, $eligible_id);
//            $this->processEligibleDependentsGratuity($employee_id, $ptd_pension, $notification_report, $eligible_id);
            $this->processOtherDependentsGratuity($employee_id, $ptd_pension, $notification_report, $eligible_id);
            /*New progressive */
//            if($eligible_id == null){
//                /*Process funeral grants into claim compensation*/
//                $this->processFuneralGrant($employee_id, $ptd_pension, $notification_report, $eligible_id);
//            }
        }

        return ['monthly_pension' => $ptd_pension, 'monthly_pension_payable' => $monthly_pension_payable, 'lump_sum_payable' => $lump_sum_payable];
    }

    /**
     * @param $employee_id
     * @param $ptd_pension
     * @param Model $notification_report
     * @throws GeneralException
     * @description Process Monthly Pension for dependents
     */
    public function processPension($employee_id, $ptd_pension, Model $notification_report,$eligible_id = null)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        $dependents = $this->dependents->dependentsWithPivot($employee_id)->where('survivor_pension_flag',1)->get();
        $monthly_pension_payable =  $this->benefit_types->getTotalDisablementPayableAmount($ptd_pension,1);
        foreach ($dependents as $dependent) {
            $dependent_employee = $employee->dependents()->where('dependent_id', $dependent->dependent_id)->first()->pivot;
//            $monthly_pension_payable = (!isset($dependent_employee->isotherdep)) ? $monthly_pension_payable : $this->benefit_types->getPensionPayableAmountForOtherDep($ptd_pension,1);//New - check if other dependents to get their payable amount
            $survivor_pension_percent = $dependent->survivor_pension_percent;
            $survivor_pension = ($survivor_pension_percent * 0.01) * $monthly_pension_payable;
            $this->dependents->updateMP($dependent->dependent_id, $survivor_pension,$employee_id, $notification_report->id, $eligible_id);
            /*Send dependent file to e-office*/
//            (new PayrollRepository())->postBeneficiaryToDms(4,$dependent->dependent_id, $employee_id);
        }
    }

    /**
     * @param $employee_id
     * @param $ptd_pension
     * @param Model $notification_report
     * @throws GeneralException
     * @desription Process Gratuity / Lumpsum for eligible dependents Wives / Spouses
     */
    public function processEligibleDependentsGratuity($employee_id, $ptd_pension, Model $notification_report, $eligible_id = null)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        $dependents =  $this->dependents->dependentsWithPivot($employee_id)->where('survivor_gratuity_flag',1)->get();
        $ptd_pension_twice = $ptd_pension * 2 ; // TWICE PTD Monthly pension
        $lump_sum_payable =  $this->benefit_types->getLumpsumPayableAmount($ptd_pension_twice);
        foreach ($dependents as $dependent) {
            $survivor_gratuity_percent = $dependent->survivor_gratuity_percent;
            $survivor_gratuity_amount = ($survivor_gratuity_percent * 0.01) * $lump_sum_payable;
            $this->dependents->updateGratuity($dependent->dependent_id, $survivor_gratuity_amount, $employee_id, $notification_report->id, $eligible_id);
        }
    }

    /**
     * @param $employee_id
     * @param $ptd_pension
     * @param Model $notification_report
     * @throws GeneralException
     * @description Process Gratuity / lUMP SUM for Other dependents
     */
    public function processOtherDependentsGratuity($employee_id, $ptd_pension, Model $notification_report,  $eligible_id = null)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        $dependents = $this->dependents->dependentsWithPivot($employee_id)->where('survivor_gratuity_flag',1)->get();
        $lump_sum_payable = $this->benefit_types->getLumpsumMaxPayableAmountByPayablePension($ptd_pension);
        foreach ($dependents as $dependent) {
            $survivor_gratuity_percent = $dependent->survivor_gratuity_percent;
            $survivor_gratuity_amount = ($survivor_gratuity_percent * 0.01) * $lump_sum_payable;
            $this->dependents->updateGratuity($dependent->dependent_id, $survivor_gratuity_amount, $employee_id, $notification_report->id, $eligible_id);
        }
    }


    /**
     * @param $employee_id
     * @param $ptd_pension
     * @param $action_type
     * @param Model $notification_report
     * @param $input
     * @param null $eligible_id
     * @return array
     * @throws GeneralException
     * Process death progressively - <NEW Process>
     */
    public function processDeathProgressive($employee_id, $ptd_pension, $action_type, Model $notification_report, $input,$eligible_id = null){

        $monthly_pension_payable =  $this->benefit_types->getTotalDisablementPayableAmount($ptd_pension,1);
        $lump_sum_payable = $this->benefit_types->getLumpsumMaxPayableAmountByPayablePension($ptd_pension);
        $notification_eligible = ($eligible_id) ? NotificationEligibleBenefit::query()->find($eligible_id) : null;
        $benefit_type_claim_id = $notification_eligible->benefit_type_claim_id ?? 7;
        if ($action_type == 1 && $benefit_type_claim_id == 7){
            /*check if survivor benefits : benefit type*/
            $this->processPension($employee_id, $ptd_pension, $notification_report, $eligible_id);
//            $this->processEligibleDependentsGratuityProgressive($employee_id, $ptd_pension, $notification_report,$input, $eligible_id);
            $this->processOtherDependentsGratuityProgressive($employee_id, $ptd_pension, $notification_report, $input, $eligible_id);
            $this->processFuneralGrantsProgressive($employee_id,$input, $eligible_id);


        }
        $eligibleQuery = $notification_report->benefits()->where("benefit_type_claim_id", 7)->whereNull("reassessment_parent");
        if ($eligibleQuery->count()) {
            $eligible = $eligibleQuery->first();
            $return = ['monthly_pension' => $ptd_pension, 'monthly_pension_payable' => $monthly_pension_payable, 'eligible' => $eligible->id, 'letter_status_label' => $eligible->letter_status_label, 'lump_sum_payable' => $lump_sum_payable, 'status' => $eligible->processed_label, 'processed' => $eligible->processed ];
        } else {
            $return = ['monthly_pension' => $ptd_pension, 'monthly_pension_payable' => $monthly_pension_payable, 'eligible' => 0, 'letter_status_label' => "" , 'lump_sum_payable' => $lump_sum_payable, 'status' => '', 'processed' => 0];
        }

        return $return;
    }



    /**
     * @param $employee_id
     * @param $ptd_pension
     * @param Model $notification_report
     * @throws GeneralException
     * @desription Process Gratuity / Lumpsum for eligible dependents Wives / Spouses - Progressive <NEW>
     */
    public function processEligibleDependentsGratuityProgressive($employee_id, $ptd_pension, Model $notification_report,$input, $eligible_id = null)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        $dependents =  $this->dependents->dependentsWithPivot($employee_id)->where('survivor_gratuity_flag',1)->get();
        $ptd_pension_twice = $ptd_pension * 2 ; // TWICE PTD Monthly pension
        $lump_sum_payable = $this->benefit_types->getLumpsumMaxPayableAmountByPayablePension($ptd_pension);
        foreach ($dependents as $dependent) {
            $survivor_gratuity_percent = $dependent->survivor_gratuity_percent;
            $survivor_gratuity_amount = ($survivor_gratuity_percent * 0.01) * $lump_sum_payable;
            $this->dependents->updateGratuity($dependent->dependent_id, $survivor_gratuity_amount, $employee_id, $notification_report->id, $eligible_id);
            /*Save into claim compensation*/
            $other_input = [
                'amount' => $survivor_gratuity_amount,
                'benefit_type_id' => 11,
                'member_type_id' => 4,
                'resource_id' => $dependent->dependent_id,
                'compensation_payment_type_id' => 1,
                'notification_eligible_benefit_id' => $eligible_id,
            ];
            if($this->checkIfClaimCompensationExists(array_merge($input, $other_input))) {
                $this->create(array_merge($input, $other_input));
            }
        }
    }

    /**
     * @param $employee_id
     * @param $ptd_pension
     * @param Model $notification_report
     * @throws GeneralException
     * @description Process Gratuity / lUMP SUM for Other dependents - Progressive <New>
     */
    public function processOtherDependentsGratuityProgressive($employee_id, $ptd_pension, Model $notification_report,$input,  $eligible_id = null)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        $dependents = $this->dependents->dependentsWithPivot($employee_id)->where('survivor_gratuity_flag',1)->get();
        $ptd_pension_twice = $ptd_pension * 2 ; // TWICE PTD Monthly pension
        $lump_sum_payable = $this->benefit_types->getLumpsumMaxPayableAmountByPayablePension($ptd_pension);
        foreach ($dependents as $dependent) {
            $survivor_gratuity_percent = $dependent->survivor_gratuity_percent;
            $survivor_gratuity_amount = ($survivor_gratuity_percent * 0.01) * $lump_sum_payable;
            $this->dependents->updateGratuity($dependent->dependent_id, $survivor_gratuity_amount, $employee_id, $notification_report->id, $eligible_id);
            /*Save into claim compensation*/
            $other_input = [
                'amount' => $survivor_gratuity_amount,
                'benefit_type_id' => 11,
                'member_type_id' => 4,
                'resource_id' => $dependent->dependent_id,
                'compensation_payment_type_id' => 1,

//                'notification_eligible_benefit_id' => $eligible_id,
            ];
            if($this->checkIfClaimCompensationExists(array_merge($input, $other_input))) {
                $this->create(array_merge($input, $other_input));
            }
        }
    }

    /**
     * @param $employee_id
     * @param $ptd_pension
     * @param Model $notification_report
     * @throws GeneralException
     * @description Process Gratuity / lUMP SUM for Other dependents - Progressive <New>
     */
    public function processFuneralGrantsProgressive($employee_id,$input,  $eligible_id = null)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        $dependents = $this->dependents->dependentsWithPivot($employee_id)->where('funeral_grant_percent','>', 0)->get();

        foreach ($dependents as $dependent) {
            $funeral_grant_amount = $dependent->funeral_grant;
            $this->dependents->updateFuneralPayFlag($dependent->dependent_id, 1, $employee_id);
            /*Save into claim compensation*/
            $other_input = [
                'amount' => $funeral_grant_amount,
                'benefit_type_id' => 2,
                'member_type_id' => 4,
                'resource_id' => $dependent->dependent_id,
                'compensation_payment_type_id' => 1,
                'notification_eligible_benefit_id' => $eligible_id,
            ];
            if($this->checkIfClaimCompensationExists(array_merge($input, $other_input))){
                $this->create(array_merge($input, $other_input));
            }else{
                $claim_compensation = $this->getClaimCompensationExists(array_merge($input, $other_input));
                $claim_compensation->update([
                    'notification_eligible_benefit_id' => $eligible_id
                ]);
            }

        }
    }


    /**
     * @param $notification_report
     * @return float|int
     */
    public function findPpdLumpsum($notification_report)
    {
        $ppd_lumpsum = 0;
        $gross_monthly_earning = ($notification_report->claim()->count()) ? ($notification_report->claim->monthly_earning) :  $this->employees->getMonthlyEarningBeforeIncident($notification_report->employee_id, $notification_report->id)['monthly_earning'];
        $pd = ($notification_report->claim()->count()) ? $notification_report->claim->pd_formatted : 0;
        //If Death
        if (in_array($notification_report->incident_type_id , [3,4,5])) {
            $pd = 100;
        }
        //if pi greater than 30
        if ($pd > sysdefs()->data()->percentage_imparement_scale) {
            $constant_factor_compensation = 1;
        } else {
            $constant_factor_compensation = sysdefs()->data()->constant_factor_compensation; // 84 months
        }
        $percent_of_earning = sysdefs()->data()->percentage_of_earning;
        //if member type is employee
        $ppd_lumpsum = ($percent_of_earning * 0.01) * ($gross_monthly_earning) * $constant_factor_compensation * ($pd * 0.01);

        return $ppd_lumpsum;

    }

    /**
     * @param Model $incident
     * @return float|int
     */
    public function findPpdLumpsumProgressive(Model $incident)
    {
        $ppd_lumpsum = 0;
//        $gross_monthly_earning = $incident->claim->monthly_earning;
        $gross_monthly_earning = $incident->monthly_earning;
        $pd = $incident->claim->pd_formatted;
        //If Death
        if (in_array($incident->incident_type_id , [3, 4,5])) {
            $pd = 100;
        }
        $sysdef = sysdefs()->data();
        if ($pd > $sysdef->percentage_imparement_scale) {
            $constant_factor_compensation = 1;
        } else {
            $constant_factor_compensation = $sysdef->constant_factor_compensation; // 84 months
        }
        $percent_of_earning = $sysdef->percentage_of_earning;

        //if member type is employee
        $ppd_lumpsum = ($percent_of_earning * 0.01) * ($gross_monthly_earning) * $constant_factor_compensation * ($pd * 0.01);

        return $ppd_lumpsum;
    }


    /*Check if claim compensation exist*/
    public function checkIfClaimCompensationExists($input)
    {
        $pv_tran = $this->getClaimCompensationExists($input);
        return isset($pv_tran) ? false : true;
    }

    /*Get claim compensation which already exists using input*/
    public function getClaimCompensationExists($input)
    {
        $return =  $this->query()->where('claim_id',$input['claim_id'])->where('member_type_id',$input['member_type_id'] )->where('resource_id',$input['resource_id'])->where('benefit_type_id',$input['benefit_type_id']);
        /*Option input*/
        //eligible benefit id
        if(isset($input['notification_eligible_benefit_id'])){
            $return = $return->whereRaw('(notification_eligible_benefit_id is null or notification_eligible_benefit_id = ?)',[$input['notification_eligible_benefit_id']]);
        }
        return $return->first();
    }

    /*Get payable monthly pension for death incident*/
    public function findPayableMpForDeathIncident(Model $incident)
    {
        $sysdef = sysdefs()->data();
        $derived_gme = (new EmployeeRepository())->getMonthlyEarningBeforeIncident($incident->employee_id,$incident->id )['monthly_earning'];
        $gme = (isset($incident->claim->monthly_earning)) ? $incident->monthly_earning :   (isset($incident->monthly_earning) ? $incident->monthly_earning : $derived_gme);
        $percent_of_earning = $sysdef->percentage_of_earning;
        $pd = 100;
        $constant_factor_compensation = 1;
        $ptd_pension =       $ppd_lumpsum = ($percent_of_earning * 0.01) * ($gme) * $constant_factor_compensation * ($pd * 0.01);
        $payable_mp = $this->benefit_types->getTotalDisablementPayableAmount($ptd_pension,1);
        return $payable_mp;
    }

}