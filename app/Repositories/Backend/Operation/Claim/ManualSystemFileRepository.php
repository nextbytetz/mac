<?php

namespace App\Repositories\Backend\Operation\Claim;


use App\Exceptions\GeneralException;
use App\Exceptions\JobException;
use App\Models\Operation\Claim\Accident;
use App\Models\Operation\Claim\AccidentWitness;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Operation\Claim\ManualSystemFile;
use App\Models\Operation\Claim\Witness;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\ManualPayrollMemberRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ManualSystemFileRepository extends  BaseRepository
{
    use FileHandler, AttachmentHandler;

    const MODEL = ManualSystemFile::class;


    public function __construct()
    {

    }

    public function checkIfDoesNotExist($notification_report_id)
    {
        $check = $this->query()->where('notification_report_id', $notification_report_id)->count();
        if($check > 0){
            return false;
        }else{
            return true;
        }
    }

    /**
     * @param $id
     * @param array $input
     * @return mixed
     * Store manual system file
     */
    public function store(Model $notification_report, array $input)
    {
        if($this->checkIfDoesNotExist($notification_report->id)){
            return DB::transaction(function () use ($notification_report, $input) {
                $user_id  = access()->id();
                $pensioner = null;





                $this->query()->create([
                    'notification_report_id' => $notification_report->id,
                    'date_of_mmi' =>  (isset( $input['date_of_mmi'])) ? standard_date_format( $input['date_of_mmi']) : null,
                    'user_id' => $user_id,
                    'pd' =>array_key_exists('pd', $input) ?  $input['pd'] : null,

                ]);



                /*Save pensioner for accident and disease*/
                if($input['incident_type'] !=3){
                    $pensioner = $this->savePensioner($notification_report, $input);
                }
                return $pensioner;
            });
        }
    }

    /**
     * @param array $input
     * Save
     */
    public function savePensioner(Model $notification_report,array $input)
    {

        $pensioner_repo = new PensionerRepository();
        $input_data = [
            'resource_id' => $notification_report->employee_id,
            'benefit_type_id' => 3,
            'compensation_payment_type_id' => 3,
            'monthly_pension_amount' =>  isset($input['mp'])  ? str_replace(",", "", $input['mp']) : null,
            'recycl' => null,
            'pay_period' =>  null,
            'bank_id' => $input['bank'],
            'bank_branch_id' => isset($input['bank_branch']) ? $input['bank_branch'] : null,
            'accountno' => $input['accountno'],
            'notification_report_id' => $notification_report->id,
            'ispaid' => (isset($input['ispaid'])) ? $input['ispaid'] : 0,
            'dob' =>  isset($input['dob']) ? standard_date_format($input['dob']) : null,
            'phone' =>  isset($input['phone']) ? $input['phone'] : null,
        ];
        $pensioner =  $pensioner_repo->createManualPensioner($input_data);
        return $pensioner;
    }





    /**
     * @param $employee_id
     * @param array $input
     * @return mixed
     * Save survivor manual processed
     */
    public function createManualDependent(Model $notification_report, array $input)
    {
        $dependent_repo = new DependentRepository();
        return DB::transaction(function () use ($notification_report,$input, $dependent_repo) {
            $employee_id = $notification_report->employee_id;
            $ispaid = isset($input['ispaid']) ? $input['ispaid'] : 0;
            $input['survivor_pension_receiver'] = 1;
            $dependent = $dependent_repo->create($employee_id, $input);
            /*Other dep*/
            $payable_months_data = (new ManualPayrollMemberRepository())->getPayableMonthsData($input);
            $recycles_pay_period = $payable_months_data['recycles_pay_period'];
            $isotherdep = isset($input['isotherdep']) ? 1 : 0;
//            $isactive = ($isotherdep == 1 && $recycles_pay_period == 0) ? 2 : 0;
            /*end other dep*/
            $dependent->employees()->syncWithoutDetaching([$employee_id => [
                'survivor_pension_amount' =>isset($input['mp'])  ? str_replace(",", "", $input['mp']) : null,
                'notification_report_id' => $notification_report->id,
                'firstpay_flag' => $ispaid,
                'funeral_grant_pay_flag' => 1,
                'recycles_pay_period' =>($isotherdep == 1)  ? $recycles_pay_period : null,
//                'isactive' =>$isactive
            ]]);
            /*activate dependent*/
            //TODO After improvement - comment this <activateForPayroll>
//            if(env('TESTING_MODE') == 0){
//                $dependent_repo->activateForPayroll($dependent->id, $employee_id);
//            }

            /*update firsy_manual status*/
            $dependent->update(['firstpay_manual' => $ispaid]);
            return $dependent;
        });
    }

//    /*Payable Months Data*/
//    public function getPayableMonthsData(array $input)
//    {
//        $months_payable = sysdefs()->data()->max_payable_months_otherdep_full;
//        $pending_pay_months = isset($input['pending_pay_months']) ? $input['pending_pay_months'] : 0;
//        $other_dep_months_paid = isset($input['other_dep_months_paid']) ? $input['other_dep_months_paid'] : 0;
//        $recycles_remained =$months_payable - ($other_dep_months_paid + $pending_pay_months);
//        return ['recycles_pay_period' => $recycles_remained];
//    }

    /**
     * @return mixed
     * Get notification reports for datatable
     */
    public function getNotificationReportsForDataTable(){
        $repo = new NotificationReportRepository();
        $notifications = $repo->getForDataTable()
            ->leftJoin('manual_system_files', 'manual_system_files.notification_report_id', 'notification_reports.id')
            ->whereNull('manual_system_files.notification_report_id');

        return $notifications;
    }



    /**
     * Death incidents
     */
    public function getDeathIncidentsForDataTable()
    {
        $notification_repo = new NotificationReportRepository();
        $death_incidents = $notification_repo->getForDataTable()
            ->join('manual_system_files', 'manual_system_files.notification_report_id', 'notification_reports.id')
            ->where('notification_reports.incident_type_id', 3);
        return $death_incidents;

    }

    /**
     * @param $employee_id
     * @return mixed
     * Get dependents for death incident
     */
    public function getDependentsForDeathIncident($employee_id)
    {
        $dependent_repo = new DependentRepository();
        $dependents = $dependent_repo->dependentAllWithPivot()->where('dependent_employee.employee_id', $employee_id);
        return $dependents;
    }

    /**
     * @param $employee_id
     * @return mixed
     * Get enrolled pensioners with manual system files
     */
    public function getEnrolledPensionersForDataTable()
    {
        $pensioner_repo = new PensionerRepository();
        $pensioners = $pensioner_repo->getForDataTable()
            ->join('manual_system_files', 'pensioners.notification_report_id', 'manual_system_files.notification_report_id')->where('pensioners.isactive', 1);
        return $pensioners;
    }


    /**
     * @param $employee_id
     * @return mixed
     * Get enrolled survivors with manual system files
     */
    public function getEnrolledSurvivorsForDataTable()
    {
        $dependent_repo = new DependentRepository();
        $dependents = $dependent_repo->dependentAllWithPivot()
            ->join('manual_system_files', 'dependent_employee.notification_report_id', 'manual_system_files.notification_report_id')
            ->where('dependent_employee.isactive', 1);
        return $dependents;
    }


}