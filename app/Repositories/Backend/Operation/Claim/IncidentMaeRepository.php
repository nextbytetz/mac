<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\IncidentMae;
use App\Repositories\BaseRepository;

class IncidentMaeRepository extends BaseRepository
{
    const MODEL = IncidentMae::class;

}