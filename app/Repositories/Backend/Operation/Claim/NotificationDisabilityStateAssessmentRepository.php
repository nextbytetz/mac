<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use Carbon\Carbon;
use App\Models\Operation\Claim\NotificationDisabilityStateAssessment;
use App\Repositories\BaseRepository;

class NotificationDisabilityStateAssessmentRepository extends  BaseRepository
{

    const MODEL = NotificationDisabilityStateAssessment::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
       $notification_disability_state_assessment = $this->query()->find($id);

        if (!is_null($notification_disability_state_assessment)) {
            return $notification_disability_state_assessment;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_disability_state_assessment_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }

    public function updateOrCreate($notification_disability_state_id, $input) {
        $notification_disability_state_assessment = $this->query()->updateOrCreate(
            ['notification_disability_state_id' => $notification_disability_state_id] , ['user_id' => access()->user()->id, 'days' => $input['days'.$notification_disability_state_id]] );
        return $notification_disability_state_assessment;
    }

    public function updateOrCreateProgressive($notification_disability_state_id, array $input)
    {
        $notification_disability_state_assessment = $this->query()->updateOrCreate(
            ['notification_disability_state_id' => $notification_disability_state_id] , ['user_id' => access()->user()->id, 'days' => $input['assessed_days' . $notification_disability_state_id], 'remarks' => $input['remarks' . $notification_disability_state_id]] );
        return $notification_disability_state_assessment;
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findTotalDays($id)
    {
        $notification_disability_state = $this->findOrThrowException($id);
        $no_of_days = $notification_disability_state->days;
        return $no_of_days;
    }

    /**
     * @param $id
     * @return array
     * @throws GeneralException
     */
    public function findTotalDaysWorked($id)
    {
        $notification_disability_state_assessment = $this->findOrThrowException($id);
        $percent_of_ed_ld = $notification_disability_state_assessment->notificationDisabilityState->percent_of_ed_ld;
        $total_days_inserted = $this->findTotalDays($id); //Total days inserted without considering percent of ed/ld
        $total_days_worked = $total_days_inserted *($percent_of_ed_ld * 0.01);

        return ['total_days_worked'=>$total_days_worked, 'total_days_inserted' => $total_days_inserted];

    }


    /**
     * @param $days
     * @param $percent_of_ed_ld
     * @return array
     * @throws GeneralException
     * @input e.g. [['days' => 1, 'percent_of_ed_ld' => 100], ]  [days => percent, ]
     */
    public function findTotalDaysWorkedProgressive($days, $percent_of_ed_ld)
    {
        $total_days_inserted = $days;
        $total_days_worked = $total_days_inserted *($percent_of_ed_ld * 0.01);
        return ['total_days_worked'=> $total_days_worked, 'total_days_inserted' => $total_days_inserted];
    }
}