<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\MemberType;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Log;

class MemberTypeRepository extends  BaseRepository
{

    const MODEL = MemberType::class;

    protected $employers;
    protected $employees;
    protected $insurances;
    protected $dependents;
    protected $pensioners;
    protected $health_providers;

    /**
     * MemberTypeRepository constructor.
     * @throws GeneralException
     */
    public function __construct()
    {
        $this->employees = new EmployeeRepository();
        $this->employers = new EmployerRepository();
        $this->insurances = new InsuranceRepository();
        $this->dependents = new DependentRepository();
        $this->pensioners = new PensionerRepository();
        $this->health_providers = new HealthProviderRepository();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $member_type = $this->query()->find($id);

        if (!is_null($member_type)) {
            return $member_type;
        }
        throw new GeneralException(trans('exceptions.backend.claim.member_type_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }


    public function getEmployerType()
    {
        return 1;
    }


    /**
     * @param $id
     * @param $resource_id
     * @return mixed
     * @throws GeneralException
     */
    public function getMember($id, $resource_id)
    {
        //Employer
        if ($id == 1) {
            $employer_with_trashed = $this->employers->query()->where('id', $resource_id)->withTrashed()->first();
            if (isset($employer_with_trashed->deleted_at)){
                Log::info(print_r($employer_with_trashed->id,true));
                $member = $this->employers->findOrThrowException($employer_with_trashed->duplicate_id);
            }else{
                $member = $this->employers->findOrThrowException($resource_id);
            }
        }
        //employee
        if ($id == 2){
            $member = $this->employees->findOrThrowException($resource_id);
        }
        //Insurance
        if ($id == 3){
            $member = $this->insurances->findOrThrowException($resource_id);
        }
        //dependent
        if ($id == 4){
            $member = $this->dependents->findOrThrowException($resource_id);
        }

        //pensioner
        if ($id == 5){
            $member = $this->pensioners->findOrThrowException($resource_id);
        }

        //wcf
        if ($id == 6){
            $member = $this->findOrThrowException($id);
        }

        if ($id == 7) {
            $member = $this->health_providers->findOrThrowException($resource_id);
        }

        return $member;
    }

    public function getMedicalAidMemberTypes()
    {
        $query = $this->query()->whereIn('id', [1,2,3,6])->pluck('name','id');
        return $query;
    }

    public function getWcfMemberTypes()
    {
        $query = $this->query()->whereIn('id', [1,2])->pluck('name','id');
        return $query;
    }

    public function getThirdPartyTypes()
    {
        $query = $this->query()->whereIn('id', [3,7])->pluck('name','id');
        return $query;
    }

    /**
     * @param $notification_report_id
     * @param $input
     * @return int
     * @throws GeneralException
     */
    public function getMemberTypeValue($notification_report_id, $input) {
        //Insurance is selected
        $notification_reports = new NotificationReportRepository();
        if ($input['member_type_id'] == 3) {
            $resource_id = $input['insurance_id'] ;
        }
        //Employer is selected
        if ($input['member_type_id'] == 1) {
            $notification_report = $notification_reports->findOrThrowException($notification_report_id);
            $resource_id = $notification_report->employer_id;
        }
        //Employee is selected
        if ($input['member_type_id'] == 2) {
            $notification_report = $notification_reports->findOrThrowException($notification_report_id);
            $resource_id = $notification_report->employee_id;
        }
        //WCF is selected
        if ($input['member_type_id'] == 6) {
            //WCF Registration Number
            $resource_id = 4038;
        }

        return $resource_id;
    }

}