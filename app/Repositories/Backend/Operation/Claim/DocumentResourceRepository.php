<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\DocumentResource;
use App\Models\Operation\Claim\NotificationReport;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\InterestAdjustmentRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\Traits\DocumentResource\DocumentResourceGenTrait;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureExtensionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentResourceRepository
 * @package App\Repositories\Backend\Operation\Claim
 * @author Erick Chrysostom
 */
class DocumentResourceRepository extends BaseRepository
{
    use FileHandler, AttachmentHandler, DocumentResourceGenTrait;

    const MODEL = DocumentResource::class;

    /**
     * @param $reference
     * @return int
     */
    public function getDocumentGroup($reference)
    {
        switch ($reference) {
            case "DRNOTIFCNINCDNT":
                //Notification & Claim
                $docGroup = 3; //General Supportive for Notification & Claim
                break;
            case "DRCLMBNFT":
                //Claim Benefit Attachment
                $docGroup = 3; //General Supportive for Notification & Claim
                break;
            default:
                $docGroup = 23; //Letters
                break;
        }
        return $docGroup;
    }

    /**
     * @param $resourceid
     * @param $reference
     * @return mixed
     */
    public function getResource($resourceid, $reference)
    {
        switch ($reference) {
            case "DRLETTER": //Letter
                $resource = (new LetterRepository())->find($resourceid);
                break;

            case "DREMCLREOP": //CLosure Reopen
                $resource = (new EmployerClosureReopenRepository())->find($resourceid);
                 break;

            case "DRNOTIFCNINCDNT": //Notification & Claim
                $resource = (new NotificationReportRepository())->find($resourceid);
                break;

            case "DRCLMBNFT": //Claim Benefit
                $resource = (new NotificationEligibleBenefitRepository())->find($resourceid);
                break;

            case "DREMCLEXTEN": //Closure Extension
                $resource = (new EmployerClosureExtensionRepository())->find($resourceid);
                break;

            case "DRINTEREADJ": //Interest Adjustment
                $resource = (new InterestAdjustmentRepository())->find($resourceid);
                break;
            default:
                $resource = (new LetterRepository())->find($resourceid);
                break;
        }
        return $resource;
    }

    /**
     * @param Model $resource
     * @param $reference
     * @return array
     */
    public function getResourceData(Model $resource, $reference, $url_selector = 0)
    {
        switch ($reference) {
            case "DRLETTER": //Letter
                $data = [
                    'header_info' => "backend/letters/includes/header_info",
                    'return_url' => route('backend.letter.show', $resource->id),
                    'params' => [
                        'letter' => $resource,
                    ],
                ];
                break;
            case "DRNOTIFCNINCDNT": //Notification & Claim
                switch ($url_selector) {
                    default: //when = 0
                        $return_url = route('backend.assessment.impairment.dashboard', $resource->id);
                        break;
                }
                $data = [
                    'header_info' => "backend/operation/claim/notification_report/includes/header_info",
                    'return_url' => $return_url,
                    'params' => [
                        'notification_report' => $resource,
                    ],
                ];
                break;
            case "DRCLMBNFT": //Claim Benefit
                switch ($url_selector) {
                    default: //when = 0
                        $return_url = route('backend.claim.notification_report.profile', $resource->incident->id);
                        break;
                }
                $data = [
                    'header_info' => "backend/operation/claim/notification_report/includes/header_info",
                    'return_url' => $return_url,
                    'params' => [
                        'notification_report' => $resource->incident,
                        'benefit_name' => $resource->benefit->name,
                    ],
                ];
                break;

            case "DREMCLREOP": //Employer closure reopen
                $employer = isset($resource->employer) ? $resource->employer : $resource->employerClosure->closedEmployer;
                $documents = (new DocumentRepository())->getDocumentsByGroup(26)->pluck('name', 'id');
                $data =[
//                    'resource' => $resource,
                    'return_url' =>'compliance/employer_closure/open/profile/'. $resource->id .'#documents',
                    'header_info' =>'backend.operation.compliance.member.employer.includes.header_info',
                    'header_info_params' => ['employer' => $employer],
                    'doc_table' => 'document_employer',
                    'get_resource_method' => 1,
                    'show_date_reference' => 0,
                    'resource_key_name' => 'employer_id',
                    'resource_id_doc' => $employer->id,
                    'external_id' => $resource->id,
                    'documents' => $documents
                ];
                break;



            case "DREMCLEXTEN":  //Employer closure extension
                $employer =  $resource->employerClosure->closedEmployer;
                $documents = (new DocumentRepository())->getDocumentsByGroup(27)->pluck('name', 'id');
                $data =[
//                    'resource' => $resource,
                    'return_url' =>'compliance/employer_closure/extension/profile/'. $resource->id .'#documents',
                    'header_info' =>'backend.operation.compliance.member.employer.includes.header_info',
                    'header_info_params' => ['employer' => $employer],
                    'doc_table' => 'document_employer',
                    'get_resource_method' => 1,
                    'show_date_reference' => 0,
                    'resource_key_name' => 'employer_id',
                    'resource_id_doc' => $employer->id,
                    'external_id' => $resource->id,
                    'documents' => $documents
                ];
                break;

            case "DRINTEREADJ":  //Interest Adjustment
                $employer =  $resource->receipt->employer;
                $documents = (new DocumentRepository())->getDocumentsByGroup(30)->pluck('name', 'id');
                $data =[
//                    'resource' => $resource,
                    'return_url' =>'compliance/interest_adjustment/profile/'. $resource->id,
                    'header_info' =>'backend.operation.compliance.member.employer.includes.header_info',
                    'header_info_params' => ['employer' => $employer],
                    'doc_table' => 'document_employer',
                    'get_resource_method' => 1,
                    'show_date_reference' => 0,
                    'resource_key_name' => 'employer_id',
                    'resource_id_doc' => $employer->id,
                    'external_id' => $resource->id,
                    'documents' => $documents
                ];
                break;

            default:
                $data = [
                    'header_info' => "backend/letters/includes/header_info",
                    'return_url' => route('backend.letter.show', $resource->id),
                    'params' => [
                        'letter' => $resource,
                    ],
                ];
                break;
        }
        return $data;
    }

    /**
     * @param Model $resource
     * @param $reference
     * @return bool
     */
    public function updateResourceType(Model $resource, $reference)
    {
        switch ($reference) {
            case 'DRNOTIFCNINCDNT': //Notification & Claim
                $incident = (new NotificationReportRepository())->find($resource->resource_id);
                $incident->documentResources()->save($resource);
                break;
            case 'DRCLMBNFT': //Claim Benefit
                $benefit = (new NotificationEligibleBenefitRepository())->find($resource->resource_id);
                $benefit->documents()->save($resource);
                break;
            default:
                //Letter
                $letter = (new LetterRepository())->find($resource->resource_id);
                $letter->documents()->save($resource);
                break;
        }
        return true;
    }

    /**
     * @param $employerTask
     * @param $document_id
     * @return bool
     */
    public function checkIfDocumentExist($resource, $document_id)
    {
        if ($resource instanceof  NotificationReport) {
            $check = $resource->documentResources()->where('document_id', $document_id)->count();
        } else {
            $check = $resource->documents()->where('document_id', $document_id)->count();
        }

        if($check)
        {
            $return = true;
        } else {
            $return = false;
        }
        return $return;
    }

    /**
     * @param Model $resource
     * @param $reference
     * @param int $relmode
     * @return mixed
     */
    public function getDocsAttachedNonRecurring(Model $resource, $reference)
    {
        $documentRepo = new DocumentRepository();
        $docs_all = $documentRepo->query()->where("document_group_id", $this->getDocumentGroup($reference))->pluck("id")->all();

        if ($resource instanceof NotificationReport) {
            $docs_attached = $resource->documentResources()->where('resource_id', $resource->id)->whereIn('document_id', $docs_all)->get();
        } else {
            $docs_attached = $resource->documents()->where('resource_id', $resource->id)->whereIn('document_id', $docs_all)->get();
        }

        return $docs_attached;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function savePivotDocument(array $input)
    {
        $document_id = $input['document_id'];
        $file = request()->file('document_file');
        if ($file->isValid()) {
            $resource = $this->query()->create([
                'document_id' => $document_id,
                'resource_id' => $input['resource_id'],
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
            ]);
            $this->updateResourceType($resource, $input['reference']);
        }
        return $resource->id;
    }

    /**
     * @param array $input
     * @param $doc_resource_id
     * @return mixed
     */
    public function updatePivotDocument(array $input, $doc_resource_id)
    {
        $file = request()->file('document_file');
        if ($file->isValid()) {
            $this->query()->where('id', $doc_resource_id)->update([
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
            ]);
        }
        return $doc_resource_id;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function update(array $input)
    {
        return DB::transaction(function () use ($input) {
            //$document_id = $input['document_id'];
            //$resource = $this->getResource($input['resource_id'], $input['reference']);
            $ext = $this->getDocExtension('document_file');
            //$uploaded_doc = $resource->documents()->where('document_id', $document_id)->orderBy('id', 'desc')->first();
            $doc_resource_id = $this->updatePivotDocument($input, $input['doc_resource_id']);
            /*Attach to storage*/
            $this->attachDocFileToStorage($doc_resource_id, $ext);
        });
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function store(array $input)
    {
        return DB::transaction(function () use ($input) {
            $document_id = $input['document_id'];
            $resource = $this->getResource($input['resource_id'], $input['reference']);
            $check_if_recurring = (new DocumentRepository())->checkIfDocumentIsRecurring($document_id);
            $ext = $this->getDocExtension('document_file');

            if ($check_if_recurring) {
                //It is a recurring document
                /* Save for Recurring Documents */
                $doc_resource_id = $this->savePivotDocument($input);
            } else {
                //It is not recurring document
                $check_if_exist = $this->checkIfDocumentExist($resource, $document_id);
                if ($check_if_exist) {
                    $uploaded_doc = DB::table('document_resource')->where('resource_id', $resource->id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
                    $doc_resource_id = $this->updatePivotDocument($input, $uploaded_doc->id );
                } else {
                    /*if not exists - Attach new*/
                    $doc_resource_id = $this->savePivotDocument($input);
                }
            }
            /*Attach to storage*/
            $this->attachDocFileToStorage($doc_resource_id, $ext);
            return true;
        });
    }

    /**
     * @param $resource
     * @param $document_id
     * @param $ext
     * @throws \App\Exceptions\GeneralException
     */
    public function attachDocFileToStorage($doc_resource_id, $ext)
    {
        /*Attach document to server*/
        //$uploaded_doc = $resource->documents()->where(['document_id' => $document_id, "resource_id" => $resource->id])->orderBy('id', 'desc')->first();
        $path = document_resource_dir() . DIRECTORY_SEPARATOR;
        $filename = $doc_resource_id . '.' . $ext;
        $this->makeDirectory($path);
        $this->saveDocumentBasic('document_file', $filename, $path );
    }

    /**
     * @param $doc_pivot_id
     */
    public function delete($doc_pivot_id)
    {
        $uploaded_doc = $this->find($doc_pivot_id);
        $path = document_resource_dir() . DIRECTORY_SEPARATOR;
        $file_path = $path . $doc_pivot_id . '.' . $uploaded_doc->ext;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        /*delete*/
        $this->query()->where('id', $doc_pivot_id)->delete();
    }

}