<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\InvestigationQuestion;
use App\Repositories\BaseRepository;

class InvestigationQuestionRepository extends  BaseRepository
{

    const MODEL = InvestigationQuestion::class;

    public function __construct()
    {

    }

//find or throwexception
    public function findOrThrowException($id)
    {
        $investigation_question = $this->query()->find($id);

        if (!is_null($investigation_question)) {
            return $investigation_question;
        }
        throw new GeneralException(trans('exceptions.backend.claim.investigation_question_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}