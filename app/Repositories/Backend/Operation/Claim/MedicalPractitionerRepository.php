<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Claim\MedicalPractitioner;
use App\Repositories\BaseRepository;
use App\Services\System\Database\CheckForeignReference;
use Illuminate\Support\Facades\DB;

class MedicalPractitionerRepository extends  BaseRepository
{

    const MODEL = MedicalPractitioner::class;

    public function __construct()
    {

    }

//find or throwexception
    public function findOrThrowException($id)
    {
        $medical_practitioner = $this->query()->find($id);

        if (!is_null($medical_practitioner)) {
            return $medical_practitioner;
        }
        throw new GeneralException(trans('exceptions.backend.claim.medical_practitioner_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {
//        return $this->query()->create($input);
        //Validate Phone number here
        $phone = phone_255($input['phone']);
        $check = $this->query()->where("phone", $phone)->count();
        if ($check) {
            throw new WorkflowException(trans('Another medical practitioner with the same phone number is already registered.'));
        }
        return $this->query()->create([
            'firstname' =>  trim(preg_replace('/\s+/', '', $input['firstname']))  ,
            'middlename' =>  trim(preg_replace('/\s+/', '', $input['middlename']))  ,
            'lastname' => trim(preg_replace('/\s+/', '', $input['lastname']))  ,
            'phone' => (isset($input['phone']) And ($input['phone'])) ? phone_255($input['phone'])  : null,
            'national_id' => $input['national_id'],
            'address' => $input['address'],
            'user_id' => access()->id(),
            'external_id' => $input['external_id'],
        ]);
    }
    /*
     * update
     */
    public function update($id, $input) {
        $medical_practitioner = $this->findOrThrowException($id);
        //Validate Phone number here
        $phone = phone_255($input['phone']);
        $check = $this->query()->where("phone", $phone)->where("id", "<>", $id)->count();
        if ($check) {
            throw new GeneralException(trans('Another medical practitioner with the same number is already registered.'));
        }
        $medical_practitioner->update([
            'firstname' =>  trim(preg_replace('/\s+/', '', $input['firstname']))  ,
            'middlename' =>  trim(preg_replace('/\s+/', '', $input['middlename']))  ,
            'lastname' => trim(preg_replace('/\s+/', '', $input['lastname']))  ,
            'phone' =>(isset($input['phone']) And ($input['phone'])) ? phone_255($input['phone'])  : null,
            'national_id' => $input['national_id'],
            'address' => $input['address'],
            'external_id' => $input['external_id'],
        ]);
        return $medical_practitioner;
    }

    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * Get Registered Medical Practitioners
     */

    public function getRegisteredPractitioners($q, $page)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        //$name = trim(preg_replace('/\s+/', '', $q));
        $name = strtolower($q);
        $data['items'] = $this->query()
            ->select(['id', DB::raw("CONCAT_WS(' ', firstname, coalesce(middlename, ''), lastname, coalesce(phone, ''), ' - ', coalesce(external_id, '')) as full_name")])
            ->whereRaw("cast(concat_ws(' ', firstname, coalesce(middlename, ''), lastname, coalesce(external_id, ''), coalesce(phone, '')) as text)  ~* ?", [$name])
            ->limit($resultCount)
            ->offset($offset)
            ->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);

    }

    /**
    * delete
    */
    public function delete($id) {
        $checkForeign = new CheckForeignReference($this->instance()->getTable(), "id", $id);
        $checkForeign();
        $medical_practitioner = $this->findOrThrowException($id);
        $medical_practitioner->delete();
        return $medical_practitioner;
    }

}