<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\Death;
use App\Exceptions\GeneralException;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DeathRepository extends  BaseRepository
{

    const MODEL = Death::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $death = $this->query()->find($id);

        if (!is_null($death)) {
            return $death;
        }
        throw new GeneralException(trans('exceptions.backend.claim.death_not_found'));
    }

    /**
     * @param $notification_report
     * @param $input
     * @return mixed
     */
    public function create($notification_report, $input) {
        return DB::transaction(function () use ($notification_report,$input) {
            $death = $this->query()->create([
                'notification_report_id' => $notification_report->id,
                'death_place' => $input['death_place'],
                'death_cause_id' => $input['death_cause_id'],
                'district_id' => $input['district_id'],
                'death_date' =>  $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'user_id' => access()->user()->id,
                'health_provider_id' => $input['health_provider_id'],
                'medical_practitioner_id' => $input['medical_practitioner_id'],
                'activity_performed' => $input['activity_performed'],
                'incident_occurrence_cv_id' => $input['incident_occurrence_cv_id'],
                'description' => $input['description'],
            ]);
            return $death;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function updateProgressive(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $death = $this->query()->where('notification_report_id', $incident->id);
            $death->update([
                'death_place' => $input['death_place'],
                'death_cause_id' => $input['death_cause_id'],
                'district_id' => $input['district_id'],
                'death_date' =>  $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'user_id' => access()->user()->id,
                'health_provider_id' => $input['health_provider_id'],
                'medical_practitioner_id' => $input['medical_practitioner_id'],
                'activity_performed' => $input['activity_performed'],
                'incident_occurrence_cv_id' => $input['incident_occurrence_cv_id'],
                'description' => $input['description'],
            ]);
            return $death;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function saveChecklist(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $death = $this->query()->updateOrCreate([
                'notification_report_id' => $incident->id,
            ],[
                'death_date' => $input['death_date'],
                'death_cause_id' => $input['death_cause_id'],
                'accident_cause_cv_id' => ($input['death_cause_id'] == 1) ? $input['accident_cause_cv_id'] : NULL,
                'disease_cause_cv_id' => ($input['death_cause_id'] == 2) ? $input['disease_cause_cv_id'] : NULL,
                'death_incident_exposure_cv_id' => $input['death_incident_exposure_cv_id'],
                'checklist_user' => access()->id(),
                'reporting_date' => $incident->reporting_date,
                'receipt_date' => $incident->receipt_date,
                'burial_permit_no' => $input['burial_permit_no'],
                'district_id' => $input['district_id'],
                'death_certificate_no' => $input['death_certificate_no'],
                'health_provider_id' => isset($input['health_provider_id']) ? $input['health_provider_id'] : NULL,
                'medical_practitioner_id' => isset($input['medical_practitioner_id']) ? $input['medical_practitioner_id'] : NULL,
                'incident_date' => $input['death_incident_date'],
                'confirm_date' => isset($input['confirm_date']) ? $input['confirm_date'] : NULL,
                'death_place' => isset($input['death_place']) ? $input['death_place'] : NULL,
            ]);
/*            $death = $incident->death;
            $data = [
                'death_date' => $input['death_date'],
                'death_cause_id' => $input['death_cause_id'],
                'accident_cause_cv_id' => ($input['death_cause_id'] == 1) ? $input['accident_cause_cv_id'] : NULL,
                'disease_cause_cv_id' => ($input['death_cause_id'] == 2) ? $input['disease_cause_cv_id'] : NULL,
                'death_incident_exposure_cv_id' => $input['death_incident_exposure_cv_id'],
                'checklist_user' => access()->id(),
                'burial_permit_no' => $input['burial_permit_no'],
                'death_certificate_no' => $input['death_certificate_no'],
                'health_provider_id' => isset($input['health_provider_id']) ? $input['health_provider_id'] : NULL,
                'medical_practitioner_id' => isset($input['medical_practitioner_id']) ? $input['medical_practitioner_id'] : NULL,
                'incident_date' => $input['death_incident_date'],
                'confirm_date' => isset($input['confirm_date']) ? $input['confirm_date'] : NULL,
                'death_place' => isset($input['death_place']) ? $input['death_place'] : NULL,
            ];

            $death->update($data);*/
            return $death;
        });
    }

    /**
     * @param $notification_report
     * @param $input
     * @return mixed
     */
    public function update($notification_report, $input) {
        return DB::transaction(function () use ($notification_report,$input) {
            $death = $this->query()->where('notification_report_id', $notification_report->id);
            $death->update([
                'death_place' => $input['death_place'],
                'death_cause_id' => $input['death_cause_id'],
                'certificate_number' => $input['certificate_number'],
                'district_id' => $input['district_id'],
                'death_date' => $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
            ]);
            return $death;
        });
    }


    /**
     * @param $id
     * @param bool $perm
     * @return bool
     */
    public function delete($id, $perm = false)
    {
        if ($perm) {
            $this->query()->where("id", $id)->forceDelete();
        } else {
            $this->query()->where("id", $id)->delete();
        }
        return true;
    }

    /**
     * @param Model $notification_report
     * @throws GeneralException
     */
    public function checkIfHasSpouseMarriageDocument(Model $notification_report){
        $notificationReport = new NotificationReportRepository();
        $dependent = new DependentRepository();
        $document = new DocumentRepository();
        $spouse_marriage_certificate_id = $document->getSpouseMarriageCertificateDocument();
        $death = $this->query()->whereHas('notificationReport', function($query) use($notification_report){
            $query->where('notification_report_id', $notification_report->id);
        })->first();
        if (count($dependent->checkIfHasSpouses($notification_report->employee_id))){
            /*get marriage certificate if exist*/
            $spouse_marriage_certificate = $notification_report->documents()->where('document_id', $spouse_marriage_certificate_id)->whereNotNull('document_notification_report.name')->first();

            /*If not available prompt to upload*/
            if(!$spouse_marriage_certificate){
                throw new GeneralException(trans('exceptions.backend.claim.spouse_marriage_document_not_uploaded'));
            }
        }


    }



}