<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Events\Sockets\Notification\AssignedInvestigation;
use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Claim\NotificationInvestigator;
use App\Models\Operation\Claim\NotificationReport;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class NotificationInvestigatorRepository extends  BaseRepository
{

    const MODEL = NotificationInvestigator::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $notification_investigator = $this->query()->find($id);

        if (!is_null($notification_investigator)) {
            return $notification_investigator;
        }
        throw new GeneralException(trans('exceptions.backend.claim.investigator_not_found'));
    }

    /**
     * @param $notification_report_id
     * @param $input
     * @return mixed
     */
    public function create($notification_report_id, $input) {
        return DB::transaction(function () use ($notification_report_id, $input) {
            $users = [];
            foreach ($input['investigator'] as $investigator) {
                $this->checkIfUserExist($notification_report_id, $investigator);
                $notification_investigator = $this->query()->create(['notification_report_id' => $notification_report_id, 'user_id' => $investigator]);
                $users[] = (int) $investigator;
            }
            event(new AssignedInvestigation($users, $notification_report_id));
        });

    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function update($id,$input) {
        $notification_investigator = $this->findOrThrowException($id);
        $this->checkIfUserExist($notification_investigator->notification_report_id,$input['investigator']) ;
        $notification_investigator->update(['user_id' =>  $input['investigator']]);
        return $notification_investigator;
    }

    /**
     * @param $notification_report_id
     * @param $user_id
     * @return string
     * @throws GeneralException
     */
    public function checkIfUserExist($notification_report_id,$user_id) {

       $user = $this->query()->where('notification_report_id', $notification_report_id)->where('user_id', $user_id)->get();
       if (count($user)> 0){
           throw new GeneralException(trans('exceptions.backend.claim.investigators_already_assigned'));
    }else{
           return '';
        }
    }




}