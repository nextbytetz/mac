<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\NotificationHealthProvider;
use App\Models\Operation\Claim\NotificationHealthProviderPractitioner;
use App\Repositories\BaseRepository;

class NotificationHealthProviderPractitionerRepository extends  BaseRepository
{

    const MODEL = NotificationHealthProviderPractitioner::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $notification_health_provider_practitioner= $this->query()->find($id);

        if (!is_null($notification_health_provider_practitioner)) {
            return $notification_health_provider_practitioner;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_health_provider_practitioner_not_found'));
    }
    /*
     * create new
     */

    public function create($notification_health_provider_id, $input) {
        $notification_report_provider_practitioner = $this->query()->create(['notification_health_provider_id' => $notification_health_provider_id, 'medical_practitioner_id' => $input['medical_practitioner_id']]);
    }

    /*
     * update
     */
    public function update($notification_health_provider_id,$input) {
        $notification_report_provider_practitioner = $this->query()->updateOrCreate(['notification_health_provider_id' => $notification_health_provider_id], ['medical_practitioner_id' => $input['medical_practitioner_id']]);
    }






}