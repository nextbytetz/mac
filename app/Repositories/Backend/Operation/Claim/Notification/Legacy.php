<?php

namespace App\Repositories\Backend\Operation\Claim\Notification;

use App\DataTables\WorkflowTrackDataTable;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Legacy
 * @package App\Repositories\Backend\Operation\Claim\Notification
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 * @description Support the first implementation of Notification Processing with single workflow
 */
trait Legacy
{
    /**
     * @param $incident
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function legacyProfile(Model $incident)
    {
        $banks = new BankRepository();
        $bank_branches = new BankBranchRepository();
        $employees = new EmployeeRepository();
        $id = $incident->id;
        $incident_date = $this->findIncidentDate($id);
        $dob = $employees->getDob($incident->employee_id);
        $employee = $employees->findOrThrowException($incident->employee_id);
        switch($incident->status) {
            case 1:
            case 0:
                $wf_module_group_id = 3;
                break;
            case 2:
                $wf_module_group_id = 4;
                break;
        }
        /* Check workflow */

        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        $assessment_level = $workflow->claimAssessmentLevel();
        $check_pending_assessment_level =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending($assessment_level) : 0;
        /*end of check workflow*/
        $claim_compensations = new ClaimCompensationRepository();
        $compensation_summary = [];
        if ($incident->claim()->count()) {
            $compensation_summary = $claim_compensations->claimCompensationApprove($id, 0);
        }

        //dd($compensation_summary);
        $pending_documents = $this->checkAllMandatoryDocuments($id);
        $dependent_repo = new DependentRepository();
        return view('backend.operation.claim.notification_report.profile')
            ->with("notification_report", $incident)
            ->with("banks", $banks->getAll()->pluck('name', 'id'))
            ->with("bank_branches", $bank_branches->getAll()->pluck('name', 'id'))
            ->with("age", Carbon::parse($dob)->diff(Carbon::parse($incident_date))->y)
            ->with("monthly_earning", $employees->getMonthlyEarningBeforeIncident($incident->employee_id, $incident->id))
            ->with("compensation_summary", $compensation_summary)
            ->with("pending_documents", $pending_documents)
            ->with("check_workflow", $check_workflow)
            ->with("check_if_is_level1_pending", $check_pending_level1)
            ->with("check_if_is_assessment_level_pending", $check_pending_assessment_level)
            ->with("employee", $employee)
            ->with('dependents', $dependent_repo);
    }

}