<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\Document;
use App\Repositories\BaseRepository;

class DocumentRepository extends BaseRepository
{
    const MODEL = Document::class;

//find or throwexception for notification report
    public function findOrThrowException($id)
    {
        $document = $this->query()->find($id);

        if (!is_null($document)) {
            return $document;
        }
        throw new GeneralException(trans('exceptions.backend.claim.document_not_found'));
    }



    public function getEmployeeFileDocument()
    {
        return 37;
    }


    public function getPoliceReportDocument()
    {
        return 10;
    }


    public function getSpouseMarriageCertificateDocument()
    {
        return 44;
    }

    /**
     * @param $id | Incident Type Id
     * @return mixed
     */
    public function getTotalIncidentDocuments($id)
    {
        $count = $this->query()->whereHas('documentGroup', function($query) use ($id) {
            $query->whereHas("incidentTypes", function ($query) use ($id) {
                $query->where("incident_type_id", $id);
            });
        })->count();
        return $count;
    }

    public function getOptionalIncidentDocuments($incident)
    {
        $accident_group = [1,2,3,5,7,28,29,14];
        $disease_group = [1,2,3,4,7,29,14];
        $documents = $this->query()->select(["id", "name"])->where("ismandatory", 0);
        switch ($incident->incident_type_id) {
            case 1:
                //Accident
                $documents->whereIn("document_group_id", $accident_group);
                break;
            case 2:
                //Disease
                $documents->whereIn("document_group_id", $disease_group);
                break;
            case 3:
                //Death
                $death = $incident->death;
                switch ($death->death_cause_id) {
                    case 1:
                        //Occupational Accident
                        $arr = array_merge($accident_group, [6]);
                        break;
                    case 2:
                        //Occupational Disease
                        $arr = array_merge($accident_group, [6]);
                        break;
                }
                $documents->whereIn("document_group_id", $arr);
                break;
        }
        return $documents->get();
    }

    /**
     * @param $id
     * @return bool
     * Check if document is recurrinf
     */
    public function checkIfDocumentIsRecurring($id)
    {
        $document = $this->find($id);
        if($document->isrecurring){
            return true;
        }else{
            return false;
        }
    }

    public function checkIfOthers($id)
    {
        $document = $this->find($id);
        if($document->isother){
            return true;
        }else{
            return false;
        }
    }


    /*Get Documents by group*/
    public function getDocumentsByGroup($doc_group_id)
    {
        return $this->queryDocumentsByGroup($doc_group_id)->get();
    }


    public function queryDocumentsByGroup($doc_group_id)
    {
        return $this->query()->where('document_group_id', $doc_group_id)->where('isactive', 1);
    }


    public function queryDocumentsByArrayIds(array $doc_ids)
    {
        return $this->query()->whereIn('id', $doc_ids)->where('isactive', 1);
    }

}