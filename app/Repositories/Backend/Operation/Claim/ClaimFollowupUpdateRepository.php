<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\Accident;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\ClaimFollowup\ClaimFollowupUpdate;
use App\Models\Operation\Claim\NotificationReport; 
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

/**
 * Class AccidentRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class ClaimFollowupUpdateRepository extends  BaseRepository
{

    const MODEL = ClaimFollowupUpdate::class;

    public function __construct()
    {
        $this->notification_report = new NotificationReportRepository();
        //$this->notification_report = new ClaimFollowupRepository();

    }

    public function findOrThrowException($id)
    {
        $followup = $this->query()->find($id);
        if (!is_null($followup)) {
            return $followup;
        }
        throw new GeneralException('The followup does not exist');
    }


    public function storeFollowupUpdate($input){
        // dd($input);
        $notification_report = NotificationReport::find($input['notification_report_id']);
        $user_id = $notification_report->allocated;
        $input['user_id'] = $user_id;
        unset($input['notification_report_id']);

        $this->query()->create($input);
    }

    public function updateFollowUp($id, $input){
       $followup_cv = (new CodeValueRepository())->findByReference($input['follow_up_type_cv_id']);
       $input['follow_up_type_cv_id'] = $followup_cv->id;
       unset($input['notification_report_id']);
       unset($input['reminder_date']);

       if($input['district'] == null){
        // if ($input['district'] == null) {
        unset($input['district']);
        // }
    }else{
        unset($input['district']);
    } 

    if(isset($input['ward'])){
        if ($input['ward'] != null) {
            $postcode = DB::table('postcodes')->select('id')->where('postcode', $input['ward'])->first();
            if (!is_null($postcode)) {
                $input['postcode_id'] = $postcode->id;
            }
        }

        unset($input['ward']);
    }

    unset($input['action_type']);


    $this->query()->find($id)->update($input);
}

public function deleteFollowUp($id){

    $this->query()->find($id)->delete();
}


}