<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\DiseaseKnowHow;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class DiseaseKnowHowRepository extends  BaseRepository
{

    const MODEL = DiseaseKnowHow::class;

    public function __construct()
    {

    }

//find or throwexception for notification report
    public function findOrThrowException($id)
    {
        $disease_know_how = $this->query()->find($id);

        if (!is_null($disease_know_how)) {
            return $disease_know_how;
        }
        throw new GeneralException(trans('exceptions.backend.claim.disease_know_how_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}