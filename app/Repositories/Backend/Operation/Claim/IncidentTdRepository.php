<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\IncidentTd;
use App\Repositories\BaseRepository;

class IncidentTdRepository extends BaseRepository
{
    const MODEL = IncidentTd::class;

}