<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\AccidentType;
use App\Repositories\BaseRepository;

class AccidentTypeRepository extends  BaseRepository
{

    const MODEL = AccidentType::class;

    public function __construct()
    {

    }

//find or throwexception for notification report
    public function findOrThrowException($id)
    {
        $accident_type = $this->query()->find($id);

        if (!is_null($accident_type)) {
            return $accident_type;
        }
        throw new GeneralException(trans('exceptions.backend.claim.accident_type_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}