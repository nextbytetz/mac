<?php

namespace App\Repositories\Backend\Operation\Claim\Portal;

use App\Models\Operation\Claim\Portal\ClaimActivity;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class ClaimActivityRepository extends BaseRepository
{
    const MODEL = ClaimActivity::class;


}