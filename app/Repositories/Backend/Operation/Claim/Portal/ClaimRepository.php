<?php

namespace App\Repositories\Backend\Operation\Claim\Portal;

use App\Models\Operation\Claim\Portal\Claim;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\BaseRepository;

class ClaimRepository extends BaseRepository
{
    const MODEL = Claim::class;

    public function getPendingValidationForDatatable($validate = "t")
    {
        $user_type_cv_id = (new CodeValuePortalRepository())->getEmployerId();
        return $this->query()
                ->select([
                    'portal.claims.*',
                    'employers.name as employer',
                ])
                ->leftJoin('main.employers', 'main.employers.id','portal.claims.resource_id' )
                ->where("confirmed", "t")
                ->where("validated", "f")
                ->where("isactive", "t")
                ->where("user_type_cv_id", $user_type_cv_id);
    }

    public function getValidatedForDatatable()
    {
        $user_type_cv_id = (new CodeValuePortalRepository())->getEmployerId();
        return $this->query()
                ->select([
                    'portal.claims.*',
                    'employers.name as employer',
                ])
                ->leftJoin('main.employers', 'main.employers.id','portal.claims.resource_id' )
                ->where("confirmed", "t")
                ->where("validated", "t")
                ->where("isactive", "t")
                ->where("user_type_cv_id", $user_type_cv_id);
    }

    public function getUsersForEmployer($employer)
    {
        $user_type_cv_id = (new CodeValuePortalRepository())->getEmployerId();
        return $this->query()->where("user_type_cv_id", $user_type_cv_id)->where("resource_id", $employer);
    }

    public function checkExistOnline($employer)
    {
        return $this->query()->where("resource_id", $employer)->where("validated", "t")->where("isactive", "t")->count();
    }

}