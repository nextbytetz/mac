<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\BenefitType;
use App\Models\Operation\Claim\DiseaseKnowHow;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BenefitTypeRepository extends  BaseRepository
{

    const MODEL = BenefitType::class;

    public function __construct()
    {

    }

//find or throwexception for benefit type
    public function findOrThrowException($id)
    {
        $benefit_type = $this->query()->find($id);

        if (!is_null($benefit_type)) {
            return $benefit_type;
        }
        throw new GeneralException(trans('exceptions.backend.claim.benefit_type_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }


    public function getClaimBenefitGroupIds()
    {
        return [1, 2, 3];
    }

    /*Get Interest Refund id*/
    public function getInterestRefund()
    {
        return 12;
    }


    /*
     * get TTD ID
     */
    public function getTtdId() {
        return 8;
    }

    /*
        * get TPD ID
        */
    public function getTpdId() {
        return 7;
    }

    public function getbenefitIds() {
        return ['ttd' => 8 ,'tpd' => 7 ];
    }


    /**
     * @param $amount
     * @return mixed
     * Check Max Monthly Pension / Payment
     */
    public function checkMaxMonthlyPension($amount,$months, $pd_percent = 100) {
        $max_payable = sysdefs()->data()->maximum_monthly_pension * $months * ($pd_percent * 0.01);
        if ($amount > $max_payable) {
            return $max_payable;
        } else {
            return $amount;
        }
    }

    /**
     * @param $amount
     * @return mixed
     * checkMin Monthly Pension / Payment
     */
    public function checkMinMonthlyPension($amount,$months) {
        $min_payable = sysdefs()->data()->minimum_monthly_pension  * $months;
        if ($amount < $min_payable){
            return $min_payable;
        }else {
            return $amount;
        }
    }


    /**
     * @param $amount
     * @return mixed
     * checkMin Monthly Pension / Payment for other dependents with full dependency
     */
    public function checkMinMonthlyPensionOtherDep($amount,$months) {
        $min_payable = sysdefs()->data()->min_payable_otherdep_full  * $months;
        if ($amount < $min_payable){
            return $min_payable;
        }else {
            return $amount;
        }
    }

    /**
     * @param $amount
     * @return mixed
     * checkMax Monthly Pension / Payment for other dependents with full dependency
     */
    public function checkMaxMonthlyPensionOtherDep($amount,$months) {
        $max_payable = sysdefs()->data()->max_payable_otherdep_full  * $months;
        if ($amount > $max_payable){
            return $max_payable;
        }else {
            return $amount;
        }
    }


    /**
     * @param $amount
     * @return mixed
     * checkMin Lump sum
     */
    public function checkMinLumpsumAmount($amount) {
        if ($amount < sysdefs()->data()->minimum_lumpsum_amount){
            return sysdefs()->data()->minimum_lumpsum_amount;
        }else {
            return $amount;
        }
    }



    /**
     * @param $amount
     * @return mixed
     * checkMin Lump sum
     */
    public function checkMaxLumpsumAmount($amount) {
        if ($amount > sysdefs()->data()->maximum_lumpsum_amount){
            return sysdefs()->data()->maximum_lumpsum_amount;
        }else {
            return $amount;
        }
    }

    /**
     * @param $amount
     * @return mixed
     * Get Payable amount for Total disablement (PTD / TTD)
     * Check for Min amount and Max Amount
     */
    public function getTotalDisablementPayableAmount($amount,$months) {
        $payable_amount = $this->checkMinMonthlyPension($amount,$months);
        $payable_amount= $this->checkMaxMonthlyPension($payable_amount,$months);


        return $payable_amount;
    }

    /**
     * @param $amount
     * @return mixed
     * Get Payable amount for Partial disablement (PPD )
     * Only check for Maximum amount - This function works for Permanent Partial Disablement (update 27 May 2020)
     */
    public function getPartialDisablementPayableAmount($amount,$months, $pd_percent) {
        /*Old check * Does not check for max lumpsum*/

        $payable_amount= $this->checkMaxMonthlyPension($amount,$months, $pd_percent);
//        $payable_amount = $amount;
        return $payable_amount;
    }


    /**
     * @param $amount
     * @return mixed
     * Get Payable amount for Lumpsum
     * Check for Min amount and Max Amount
     */
    public function getLumpsumPayableAmount($amount) {
        $payable_amount = $this->checkMinLumpsumAmount($amount);
        $payable_amount= $this->checkMaxLumpsumAmount($payable_amount);
        return $payable_amount;
    }

    /**
     * @param $amount
     * @return mixed
     * Get Payable amount for Lumpsum
     * Check for  Max Amount
     */
    public function getLumpsumMaxPayableAmount($amount) {
        $payable_amount= $this->checkMaxLumpsumAmount($amount);
        return $payable_amount;
    }

    /**
     * @param $amount
     * @return mixed
     * Get Payable amount for Lumpsum
     * Check for  Max Amount
     */
    public function getLumpsumMaxPayableAmountByPayablePension($ptd_pension) {
        $ptd_pension_twice = $ptd_pension * 2;
        $payable_amount= $this->checkMaxLumpsumAmount($ptd_pension_twice);
        return $payable_amount;
    }


    /**
     * @param $amount
     * @return mixed
     * Get Payable amount for other dependents with full dependency
     * Check for Min amount and Max Amount
     */
    public function getPensionPayableAmountForOtherDep($amount,$months) {
        $payable_amount = $this->checkMinMonthlyPensionOtherDep($amount,$months);
        $payable_amount= $this->checkMaxMonthlyPensionOtherDep($payable_amount,$months);
        return $payable_amount;
    }



}