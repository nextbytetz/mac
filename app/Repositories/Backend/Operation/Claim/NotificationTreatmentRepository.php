<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\NotificationTreatment;
use App\Repositories\BaseRepository;

class NotificationTreatmentRepository extends BaseRepository
{
    const MODEL = NotificationTreatment::class;

}