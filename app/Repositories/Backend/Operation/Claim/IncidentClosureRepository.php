<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\IncidentClosure;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IncidentClosureRepository extends BaseRepository
{
    const MODEL = IncidentClosure::class;

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postCloseIncident(Model $incident, array $input)
    {
        return DB::transaction(function() use ($incident, $input) {
            //update close reason, and close date
            //return true;
            //Log::error($input);
            $incident->closure()->update(['close_date' => Carbon::now(), 'reason' => $input['reason']]);
            //close the pending workflow
            //Close the current workflow track
            $wfTrackRepo = new WfTrackRepository();
            //Update current workflow track
            $wfModule = (new WfModuleRepository())->legacyNotificationModule()[0];
            $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($incident->id, $wfModule);
            if ($wfTrackInstance->count()) {
                $wfTrack = $wfTrackInstance->first();
                $user = access()->id();
                $wfTrack->user_id = $user;
                $wfTrack->allocated = $user;
                $wfTrack->assigned = 1;
                $wfTrack->user_type = "App\Models\Auth\User";
                $wfTrack->comments = $input['reason'] ;
                $wfTrack->forward_date = Carbon::now();
                $wfTrack->status = 5;
                $wfTrack->save();
            }

            //update wf_done for the incident
            $incident->wf_done_date = Carbon::now();
            $incident->wf_done = 1;
            $incident->save();

            return true;
        });
    }

}