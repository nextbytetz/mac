<?php
namespace App\Repositories\Backend\Operation\Claim\HspBilling;

use App\Events\NewWorkflow;
use App\Exceptions\WorkflowException;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use App\Models\Workflow\WfTrack;
use App\Models\Operation\Claim\HspBilling\HspBillSummary;
use App\Models\Operation\Claim\HspBilling\HspBillDetail;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class HspBillSummaryRepository extends BaseRepository
{
    const MODEL = HspBillSummary::class;

    protected $category_code_id;
    public function __construct()
    {
        parent::__construct();
        $this->wf_module_group_id = 35;
    }

    public function findOrThrowException($id)
    {
        $hsp_bill = $this->query()
        ->find($id);
        if (!is_null($hsp_bill))
        {
            return $hsp_bill;
        }
        throw new GeneralException('HSP / HCP Bill not found');
    }


    public function initiateBillVettingWorkflow($hsp_billing_id)
    {
        access()->hasWorkflowModuleDefinition($this->wf_module_group_id,0,1);
        $hsp_bill = $this->findOrThrowException($hsp_billing_id);
        $return = DB::transaction(function () use ($hsp_billing_id,$hsp_bill)
        {
            $this->query()->where('id',$hsp_billing_id)->update(['wf_status' => 1, 'wf_initiator'=>access()->user()->id]);
            $check = workflow([['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $hsp_billing_id, 'type' => 0]])->checkIfHasWorkflow();
            if (!$check)
            {
                event(new NewWorkflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $hsp_billing_id, 'type' =>0]));
            }
            return true;
        });
        return $return;
    }

    public function updateHspBillWorkflow($hsp_billing_id, $level, $wf_status)
    {
     // dd($hsp_billing_id.' '.$level.' '.$wf_status);
       if ($wf_status==1) {
        switch ((int)$level) {
            case 6:
            $all_files_reviewed = $this->allFilesReviewed($hsp_billing_id);
            if (!$all_files_reviewed) {
                throw new WorkflowException('Action Failed! Kindly make sure all files have been reviewed before proceeding');
            }
            $this->query()->where('id',$hsp_billing_id)->update(['payment_advice' => 0]);

            break;
            case 10:
            //DG approval
            $this->query()->where('id',$hsp_billing_id)->update(['wf_status' => 2]);
            break;

            case 13:
            // tengeneza payment advice kwanza
            $hsp_bill = $this->findOrThrowException($hsp_billing_id);
            if ($hsp_bill->payment_advice != 1) {
                throw new WorkflowException('Action Failed! Kindly make sure you have created the payment advice before proceeding');
            }
            break;
            case 14:
            //PICADO print payment advice and update control number
            $hsp_bill = $this->findOrThrowException($hsp_billing_id);
            if ($hsp_bill->payment_advice != 2) {
                throw new WorkflowException('Action Failed! Kindly make sure you have downloaded the payment advice and request for control number/EFD receipt from respective HSP / HCP');
            }
            if (empty($hsp_bill->control_no)) {
                throw new WorkflowException('Action Failed! Kindly make sure you have updated the control number/EFD for this bill before proceeding');
            }
            break;
            case 15:
            //FO payment control number
            $hsp_bill = $this->findOrThrowException($hsp_billing_id);
            if (empty($hsp_bill->payment_status)) {
                throw new WorkflowException('Action Failed! Kindly make sure the payment using control number: '.$hsp_bill->control_no.'  has been effected before proceeding');
            }
            break;
            case 16:
            //PICADO close workflow
            $this->query()->where('id',$hsp_billing_id)->update(['wf_done' => 1, 'wf_done_date'=>Carbon::now()]);
            break;
            default:
            break;
        }
    }
    else{

    }
}

public function checkBeforeInitiateWf($hsp_billing_id)
{

    $hsp_bill =  $this->findOrThrowException($hsp_billing_id);
    $total_files = count($hsp_bill->hspBillDetails);
    if (count($hsp_bill->wfTracks) > 0) {throw new WorkflowException('Action Failed! There is already a workflow associated with this bill');}

    if ($hsp_bill->total_files_claimed != $total_files) {
       throw new WorkflowException('Action Failed! Total files claimed : '.$hsp_bill->total_files_claimed.' not equivalent to total files loaded: '.$total_files.'. Kindly contact ICT Unit');}

       $total_amount = 0;
       foreach ($hsp_bill->hspBillDetails as $file) {
        $total_amount += $file->bill_amount;
    }

    if ($hsp_bill->total_amount_claimed !=  $total_amount){
      throw new WorkflowException('Action Failed! Total claimed amount: '.number_format($hsp_bill->total_amount_claimed,2).' not equivalent to total files amount: '.number_format($total_amount,2).'. Kindly contact ICT Unit');}
  }

  public function allFilesReviewed($hsp_billing_id)
  {
    $hsp_bill = $this->findOrThrowException($hsp_billing_id);
    $unreviewed_files = HspBillDetail::where('hsp_summary_id',$hsp_billing_id)->where('is_reviewed',false)->get();
    $return = false;
    if (count($hsp_bill->hspBillDetails)) {
        $return = count($unreviewed_files) > 0 ? false : true;
    }
    return $return;
}

public function canVettBillDetail($billing_detail_id)
{
 $bill_detail = HspBillDetail::find($billing_detail_id);
 return !empty($bill_detail->hspBillSummary->current_wf_level) ? $bill_detail->hspBillSummary->current_wf_level : 0;   
}

public function getBillWithStakeholders($billing_id)
{
    return $this->query()->select('hsp_bill_summaries.*','hsp_bill_summaries.id as id','api_stakeholders.shdescription')->join('api_stakeholders', 'api_stakeholders.id', '=', 'hsp_bill_summaries.stakeholder_id')->where('hsp_bill_summaries.id', $billing_id)->first();
}

public function getBillPathFileUrl()
{
    if (test_uri()) {
        $file_path = asset('storage'.DIRECTORY_SEPARATOR.'hsp_bills');
    } else {
        $file_path = asset('public'.DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.'hsp_bills');
    }
    return $file_path;
}

public function getBillPath()
{
 return  public_path() . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'hsp_bills';
}

public function returnUserNameAtWfLevel($hsp_billing_id,$level)
{
    $check = workflow([['wf_module_group_id' => 35, 'resource_id' => $hsp_billing_id, 'type' => 0]])->checkIfHasWorkflow();
    $wf_track = null;
    if ($check)
    {
     $wf_track = DB::table('main.wf_tracks')->select(DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(lastname, '')) as name"))
     ->join('wf_definitions', 'wf_definitions.id', '=', 'wf_tracks.wf_definition_id')
     ->join('users', 'users.id', '=', 'wf_tracks.user_id')
     ->where("resource_id", $hsp_billing_id)->where('wf_definitions.level',$level)->where('resource_type','App\Models\Operation\Claim\HspBilling\HspBillSummary')->orderByDesc("wf_tracks.id")->limit(1)->first(); 
     $wf_track = !empty($wf_track) ? trim($wf_track->name) : null;
 }
 return $wf_track;
}

public function updatePaymentAdvice($hsp_billing_id,$level)
{
    $payment_advice = $level == 14 ? 2 : 1;
    $this->query()->where('id',$hsp_billing_id)->update(['payment_advice' => $payment_advice]);
}


}