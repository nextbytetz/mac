<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\BodyPartInjury;
use App\Repositories\BaseRepository;

/**
 * Class BodyPartInjuryRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class BodyPartInjuryRepository extends BaseRepository
{
    const MODEL = BodyPartInjury::class;

    /**
     * @return mixed
     */
    public function getForAccident()
    {
        return $this->query()->where("body_part_injury_group_id", "<>", 6)->get();
    }

    /**
     * @return mixed
     */
    public function getForDisease()
    {
        return $this->query()->where("body_part_injury_group_id", "=", 6)->get();
    }

    /**
     * @return mixed
     */
    public function getForChecklist()
    {
        $return = $this->query()->select(['id'])->whereIn("body_part_injury_group_id", [1,3])->pluck("id")->all();
        return $return;
    }

    /**
     * @param array $input | List of body_part_injury_id
     * @return mixed
     */
    public function getForInjuryGroup(array $input)
    {
        $return = [];
        if (count($input)) {
            $group = $this->query()->whereIn("id", $input)->pluck("body_part_injury_group_id")->all();
            $return = $this->query()->select(['id', 'name'])->whereIn("body_part_injury_group_id", $group)->get();
        }
        return $return;
    }

    /**
     * @param array $input | List of body_part_injury_id
     * @return mixed
     */
    public function getForInjuryGroupPluck(array $input)
    {
        $return = [];
        if (count($input)) {
            $group = $this->query()->whereIn("id", $input)->pluck("body_part_injury_group_id")->all();
            $return = $this->query()->select(['id', 'name'])->whereIn("body_part_injury_group_id", $group)->pluck("name", "id")->all();
        }
        return $return;
    }

}