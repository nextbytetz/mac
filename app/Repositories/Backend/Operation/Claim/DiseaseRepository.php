<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\Disease;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DiseaseRepository extends  BaseRepository
{

    const MODEL = Disease::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $disease = $this->query()->find($id);

        if (!is_null($disease)) {
            return $disease;
        }
        throw new GeneralException(trans('exceptions.backend.claim.disease_not_found'));
    }

    /**
     * @param $notification_report
     * @param $input
     * @return mixed
     */
    public function create($notification_report, $input) {
        return DB::transaction(function () use ($notification_report, $input) {
            $disease = $this->query()->create([
                'notification_report_id' => $notification_report->id,
                'name' => $input['disease_name'],
                'diagnosis_date' =>  $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'user_id' => access()->user()->id,
                'health_provider_id' => $input['health_provider_id'],
                'medical_practitioner_id' => $input['medical_practitioner_id'],
            ]);
            return $disease;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function updateProgressive(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $disease = $this->query()->where('notification_report_id', $incident->id);
            $disease->update([
                'name' => $input['disease_name'],
                'diagnosis_date' =>  $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'user_id' => access()->user()->id,
                'health_provider_id' => $input['health_provider_id'],
                'medical_practitioner_id' => $input['medical_practitioner_id'],
            ]);
            return $disease;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function saveChecklist(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $disease = $incident->disease;
            $data = [
                'diagnosis_date' => $input['diagnosis_date'],
                'reporting_date' => $input['reporting_date'],
                'salary_being_continued' => $input['salary_being_continued'],
                'disease_cause_cv_id' => $input['disease_cause_cv_id'],
                'lost_body_part' => $input['lost_body_part'],
                'disease_incident_exposure_cv_id' => $input['disease_incident_exposure_cv_id'],
                'on_treatment' => (isset($input['on_treatment']) ? $input['on_treatment'] : 0),
                'return_to_work' => isset($input['return_to_work']) ? $input['return_to_work'] : 0,
                'checklist_user' => access()->id(),
            ];
            if ($input['salary_being_continued']) {
                $data['salary_full_continued'] = $input['salary_full_continued'];
            } else {
                $data['salary_full_continued'] = NULL;
            }
            if ($input['lost_body_part']) {
                //Save lost body parts
                $incident->lostBodyParties()->sync($input['lost_body_part_id']);
            } else {
                $incident->lostBodyParties()->sync([]);
            }
            if (isset($input['return_to_work']) And ($input['return_to_work'])) {
                $data['return_to_work_date'] = $input['return_to_work_date'];
                $data['job_title_id'] = $input['job_title_id'];
            } else {
                $data['return_to_work_date'] = NULL;
                $data['job_title_id'] = NULL;
            }

            $disease->update($data);
            return $disease;
        });
    }

    /**
     * @param $notification_report
     * @param $input
     * @return mixed
     */
    public function update($notification_report, $input) {
        return DB::transaction(function () use ($notification_report,$input) {
            $disease = $this->query()->where('notification_report_id', $notification_report->id);
            $disease->update([
                'name' => $input['disease_name'],
                'diagnosis_date' => $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'disease_know_how_id' => $input['disease_know_how_id']
            ]);
            return $disease;
        });
    }

    /**
     * @param $id
     * @param bool $perm
     * @return bool
     */
    public function delete($id, $perm = false)
    {
        if ($perm) {
            $this->query()->where("id", $id)->forceDelete();
        } else {
            $this->query()->where("id", $id)->delete();
        }
        return true;
    }

}