<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\DocumentBenefit;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentBenefitRepository
 * @package App\Repositories\Backend\Operation\Claim
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class DocumentBenefitRepository extends BaseRepository
{
    const MODEL = DocumentBenefit::class;

    public function replicateForTransferToDeath()
    {
        $benefitTypeRepo = new BenefitTypeRepository();
        $documents = $this->getAll();
        $sql = <<<SQL
do $$ 
declare id bigint;
begin
    
    execute 'select coalesce(max(id),0) + 1 from document_benefits' into id;
    execute 'alter sequence document_benefits_id_seq restart with ' || id; 
    
end$$;
SQL;
        DB::unprepared($sql);

        foreach ($documents as $document) {
            //Skipping PD
            $benefit_type = $benefitTypeRepo->find($document->benefit_type_id);
            if ($benefit_type->benefit_type_claim_id == 5) {
                continue;
            }
            $do = false;
            switch ($document->incident_type_id) {
                case 1:
                    //Accident
                    $incident_types = [4];
                    $do = true;
                    break;
                case 2:
                    //Disease
                    $incident_types = [5];
                    $do = true;
                    break;
                case 3:
                    //Disease
                    $incident_types = [4,5];
                    $do = true;
                    break;
            }
            if ($do) {
                foreach ($incident_types as $incident_type) {
                    $this->query()->updateOrCreate([
                        'incident_type_id' => $incident_type,
                        'document_id' => $document->document_id,
                        'benefit_type_id' => $document->benefit_type_id,
                        'member_type_id' => $document->member_type_id,
                    ], [

                    ]);
                }
            }
        }
    }

}