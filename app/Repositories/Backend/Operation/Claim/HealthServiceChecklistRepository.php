<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\HealthServiceChecklist;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

class HealthServiceChecklistRepository extends  BaseRepository
{

    const MODEL = HealthServiceChecklist::class;

    public function __construct()
    {

    }

//find or throwexception
    public function findOrThrowException($id)
    {
        $health_service_checklist = $this->query()->find($id);

        if (!is_null($health_service_checklist)) {
            return $health_service_checklist;
        }
        throw new GeneralException(trans('exceptions.backend.claim.health_service_checklist_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}