<?php

namespace App\Repositories\Backend\Operation\Claim\Traits\ClaimCompensation;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\AccidentType;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;

trait CalculateTdProgressiveTrait
{



    /**
     * @param $days
     * @param $percent_of_ed_ld
     * @return array
     * @throws GeneralException
     * @input e.g. [['days' => 1, 'percent_of_ed_ld' => 100], ]  [days => percent, ]
     */
    public function findTotalDaysWorkedProgressive($days, $percent_of_ed_ld)
    {
        $total_days_inserted = $days;
        $total_days_worked = $total_days_inserted *($percent_of_ed_ld * 0.01);
        return ['total_days_worked'=> $total_days_worked, 'total_days_inserted' => $total_days_inserted];
    }


    /**
     * @param $benefit
     * @param $type | 1 - claimed, 2 - compensated(Assessed)
     * @return array
     * @throws GeneralException
     * @description Get total hours worked for TTD and TPD
     * @disability_checklist_state_input e.g. [ '1' => ['days' => 2, 'percent_of_ed_ld' => 100']]
     */
    public function findHoursDaysWorkedForTtdTpdProgressive(array $disability_checklist_state_input) {

        //Total hours worked for each benefit type
        $ttd_hours = 0;
        $tpd_hours = 0;
        $ttd_days = 0;
        $tpd_days = 0;
        $ttd_days_inserted = 0; // ttd days inserted for assessment
        $tpd_days_inserted = 0; // tpd days inserted for assessment
        $percent_of_ld = 0;

        foreach($disability_checklist_state_input as $key => $disability_checklist){
            $disability_state_checklist_id = $key;
            $percent_of_ed_ld = $disability_checklist['percent_of_ed_ld'];
$days = $disability_checklist['days'];
            if($percent_of_ed_ld == 100){
                /*Get total days for TTD*/
                $ttd_days += $this->findTotalDaysWorkedProgressive($days, $percent_of_ed_ld)['total_days_worked'];
                $ttd_days_inserted +=  $this->findTotalDaysWorkedProgressive($days, $percent_of_ed_ld)['total_days_inserted'];
            }else{
                /*Get total days for TPD*/
                $tpd_days += $this->findTotalDaysWorkedProgressive($days, $percent_of_ed_ld)['total_days_worked'];
                $tpd_days_inserted +=  $this->findTotalDaysWorkedProgressive($days, $percent_of_ed_ld)['total_days_inserted'];
            }
        }

        return ['ttd_days' => $ttd_days, 'tpd_days' => $tpd_days, 'ttd_days_inserted' => $ttd_days_inserted, 'tpd_days_inserted' => $tpd_days_inserted ];
    }



    /**
     * @param $benefit
     * @param $type | 1 - claimed, 2 - compensated(Assessed)
     * @return array
     * @throws GeneralException
     * @description Calculate MAE + Temporary Disablement (TTD and TPD)
     * @disability_checklist_state_input e.g. [ '1' => ['days' => 2, 'percent_of_ed_ld' => 100']]
     */
    public function calculateTdTotalsProgressive($eligible_id, array $disability_state_checklist_input)
    {
        $eligible = (new NotificationEligibleBenefitRepository())->find($eligible_id);
        $incident = $eligible->incident;
        $min_days_eligible_assessment = sysdefs()->data()->days_eligible_for_claim_assessment;

        $days_worked_details = $this->findHoursDaysWorkedForTtdTpdProgressive($disability_state_checklist_input);

        $ttd_days = $days_worked_details['ttd_days'];
        $tpd_days = $days_worked_details['tpd_days'];

        /*inserted*/
        $ttd_days_inserted = $days_worked_details['ttd_days_inserted']; // ttd days inserted for assessment
        $tpd_days_inserted = $days_worked_details['tpd_days_inserted']; // tpd days inserted for assessment

        $total_td_days = $ttd_days + $tpd_days;
        $total_td_days_inserted = $ttd_days_inserted + $tpd_days_inserted;

        /*Check if qualify for TTD and TPD (Combined)*/
//        if (!($total_td_days_inserted >= $min_days_eligible_assessment))
//        {
//
//            $ttd_days = 0;
//            $tpd_days = 0;
//        }

        /*Check if TTD qualify min days for compensation*/
        if(!($ttd_days_inserted >= $min_days_eligible_assessment)){
            $ttd_days = 0;
        }

        /*Check if TPD qualify min days for compensation*/
        if(!($tpd_days_inserted >= $min_days_eligible_assessment)){
            $tpd_days = 0;
        }

        $ttd_amount = $this->findTtdTpdAmountProgressive($this->getConstantInputsProgressive($incident), $ttd_days, 'ttd');
        $tpd_amount = $this->findTtdTpdAmountProgressive($this->getConstantInputsProgressive($incident), $tpd_days, 'tpd');

        /*TTD + TPD*/
        return ['ttd_amount' => $ttd_amount, 'tpd_amount' => $tpd_amount, 'total_claimed_expense'=> ($ttd_amount + $tpd_amount),'ttd_days' => $ttd_days, 'tpd_days' => $tpd_days];
    }


    /**
     * @param $constant_input
     * @param $total_days
     * @param $disablement_type i.e. ttd, tpd
     * @return float|int
     */
    public function  findTtdTpdAmountProgressive($constant_input,$total_days, $disablement_type )
    {

        /*Old Formula*/
//        $amount = ($constant_input['percent_of_earning'] * 0.01) * ($total_days) * (($constant_input['gross_monthly_earning'] / ($constant_input['no_of_days_for_a_month'] )));

        $amount = 0;
        $percent_of_earning = $constant_input['percent_of_earning'];
        $no_of_days_for_a_month = $constant_input['no_of_days_for_a_month'];
        /* Get threshold Amounts i.e. Min and Max Against Monthly Earning based on Percentage of earning*/
        $min_compensation_amt = $constant_input['min_compensation_amt'] ;
        $min_salary_payable = $min_compensation_amt / (0.01 * $percent_of_earning);
        $max_compensation_amt = $constant_input['max_compensation_amt'] ;
        $max_salary_payable = $max_compensation_amt / (0.01 * $percent_of_earning);
        $monthly_earning = $constant_input['gross_monthly_earning'];
        /* If monthly earning is less than Minimum Payable i.e. Only for TTD will be checked on Minimum threshold*/
        if (($monthly_earning < $min_salary_payable) && $disablement_type == 'ttd' ){
            $amount = ($min_compensation_amt / $no_of_days_for_a_month) * $total_days;
        }else{

            /* Check when is above the max i.e. Applicable for TTD and TPD*/
            if ($monthly_earning > $max_salary_payable ){
                $amount = ($max_compensation_amt / $no_of_days_for_a_month) * $total_days;
            }else{
                /* If Monthly earning is between Max and Min Payable*/
                $amount = (($monthly_earning  * (0.01 * $percent_of_earning)) / $no_of_days_for_a_month) * $total_days;
            }

        }
        return $amount;
    }

    /**
     * @param $incident
     * @return array
     */
    public function getConstantInputsProgressive($incident){
        //$notification_reports = new NotificationReportRepository();
        //$gross_monthly_earning = count($incident->claim) ? $incident->claim->monthly_earning : $this->employees->getMonthlyEarningBeforeIncident($incident->employee_id, $incident->id)['monthly_earning'];
        $gross_monthly_earning = $incident->monthly_earning;
        $percent_of_earning = sysdefs()->data()->percentage_of_earning;
        $constant_factor_compensation =  sysdefs()->data()->constant_factor_compensation;
        $day_hours = sysdefs()->data()->day_hours;
        $no_of_days_for_a_month = sysdefs()->data()->no_of_days_for_a_month_in_assessment;
        $percentage_imparement_scale = sysdefs()->data()->percentage_imparement_scale;
        $assistant_percent = sysdefs()->data()->assistant_percent;
        $min_compensation_amt = sysdefs()->data()->minimum_monthly_pension;
        $max_compensation_amt = sysdefs()->data()->maximum_monthly_pension;
        return ['percent_of_earning' => $percent_of_earning, 'constant_factor_compensation'=> $constant_factor_compensation, 'day_hours'=> $day_hours, 'gross_monthly_earning' => $gross_monthly_earning, 'no_of_days_for_a_month' => $no_of_days_for_a_month, 'percentage_imparement_scale' => $percentage_imparement_scale, 'min_compensation_amt' => $min_compensation_amt, 'max_compensation_amt' => $max_compensation_amt];
    }
}