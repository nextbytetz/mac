<?php

namespace App\Repositories\Backend\Operation\Claim\Traits\ClaimCompensation;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\AccidentType;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

trait UndoClaimCompensationTrait
{


    /**
     * @param $notification_eligible_id
     * Undo claim compensation by eligible benefit
     */
    public function undoClaimCompensationByEligibleBenefit($notification_eligible_id)
    {
        DB::transaction(function () use ($notification_eligible_id) {
            $notification_eligible = NotificationEligibleBenefit::query()->find($notification_eligible_id);

            /*Get claim compensations*/
            $claim_compensations = $notification_eligible->claimCompensations;
            foreach ($claim_compensations as $claim_compensation){

                $pv_tran = $claim_compensation->paymentVoucherTransaction;
                /*Undo pv tran*/
                if ($pv_tran) {
                    (new PaymentVoucherTransactionRepository())->undoPaymentVoucherTran($pv_tran->id);
                }

                /*Delete compensation*/
                $claim_compensation->delete();
            }
            $notification_eligible->assessed = 0;
            $notification_eligible->save();

        });
    }

}