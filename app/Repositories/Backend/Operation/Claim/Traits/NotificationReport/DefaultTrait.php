<?php

namespace App\Repositories\Backend\Operation\Claim\Traits\NotificationReport;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\AccidentType;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;

trait DefaultTrait
{


    /**
     * @param $reference
     * @param array $input
     * Post default by reference
     */
    public function postDefaultsByReference($reference, array $input)
    {


        $codeValueRepo = new CodeValueRepository();
        $code = $codeValueRepo->query()->where("reference", $reference)->first();
        switch ($reference) {

            case 'PAYDALEUS':
                /*Payroll officers for Payroll Alert*/
                if (isset($input['payroll_alert_officers'])) {
                    $code->users()->sync($input['payroll_alert_officers']);
                } else {
                    $code->users()->sync([]);
                }
                break;
            default:
                break;
        }
    }



}