<?php

namespace App\Repositories\Backend\Operation\Claim\Traits\DocumentResource;

use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\DocumentResourceRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait DocumentResourceGenTrait{


    /*Get Resource general*/
    public function getResourceInfoGeneral($resource_id, $document_id, $get_resource_method = 1)
    {
        $document = (new DocumentRepository())->find($document_id);
        $document_group_id = $document->document_group_id;
        switch($get_resource_method){
            case 1:
                /*Document group*/
                $resource_Info = $this->getResourceInfoByDocumentGroup($resource_id, $document_group_id);
                break;

            case 2:
                /*Document*/
                $resource_Info = $this->getResourceInfoByDocument($resource_id, $document_id);
                break;

            default:
                $resource_Info = $this->getResourceInfoByDocumentGroup($resource_id, $document_group_id);
                break;
        }
        return $resource_Info;
    }


    /*Get Resource by document*/
    public function getResourceInfoByDocument($resourceid, $document_id)
    {
        $resource = null;
        switch ($document_id) {
            case 1:
                break;
            default:

                break;
        }
        return $resource;
    }

    /*Get Resource by document group*/
    public function getResourceInfoByDocumentGroup($resourceid, $document_group_id, $external_id = null)
    {
        $resource = null;
        switch ($document_group_id) {

            /*Employer closure reopen*/
            case 26:
                $resource = (new EmployerRepository())->find($resourceid);
                $data =[
                    'resource' => $resource,
                    'top_path' => 'member/closure_reopen',
                ];
                break;

            /*Employer closure Extension*/
            case 27:
                $resource = (new EmployerRepository())->find($resourceid);
                $data =[
                    'resource' => $resource,
                    'top_path' => 'member/closure_extension',
                ];
                break;

            /*Interest Adjustment*/
            case 28:
                $resource = (new EmployerRepository())->find($resourceid);
                $data =[
                    'resource' => $resource,
                    'top_path' => 'member/interest_adjustment',
                ];
                break;
            default:

                break;
        }
        return $data;
    }


    /**
     * @param $resource_id
     * @param $document_id
     * @param string $get_resource_method i.e. 1 => Document group, 2 -> document_id
     */
    public function updateResourceTypeGeneral($doc_resource_id,$doc_table,$resource_key_name,   $get_resource_method = 1, $doc_relation = 'documents')
    {
        $doc_pivot = DB::table($doc_table)->where('id', $doc_resource_id)->first();
        $resource = $this->getResourceInfoGeneral($doc_pivot->$resource_key_name, $doc_pivot->document_id, $get_resource_method)['resource'];
        switch($doc_table)
        {
            case 'document_resource':
                $doc_pivot = (new DocumentResourceRepository())->find($doc_resource_id);
                $resource->$doc_relation->save($doc_pivot);
                break;
            default:

                break;
        }

    }



    /**
     * @param array $input
     * @return mixed
     */
    public function savePivotDocumentGeneral(array $input, $doc_table = 'document_resource', $get_resource_method = 1, $doc_relation = 'documents' )
    {
        $document_id = $input['document_id'];
        $resource_id = $input['resource_id'];
        $resource_key_name = $input['resource_key_name'];
        $file = request()->file('document_file');
        if ($file->isValid()) {
            $save_input = [
                'document_id' => $document_id,
                $resource_key_name => $resource_id,
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'external_id' =>isset($input['external_id']) ? $input['external_id'] : null,
                'created_at' => Carbon::now()
            ];
            /*Save of document pivot*/
            $doc_resource_id =    $this->createMassAssignDbBuilder($doc_table, $save_input);
            $this->updateResourceTypeGeneral($doc_resource_id,$doc_table,$resource_key_name, $get_resource_method, $doc_relation);
        }
        return $doc_resource_id;
    }

    /**
     * @param array $input
     * @param $doc_resource_id
     * @return mixed
     */
    public function updatePivotDocumentGeneral($doc_table,$doc_pivot_id,array $input)
    {
        $file = request()->file('document_file');
        if ($file->isValid()) {
            $input = [
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'updated_at' => Carbon::now()
            ];
            $where_input = ['id' => $doc_pivot_id];
            $resource =    $this->updateMassAssignByWhere($doc_table, $where_input,$input );
        }
        return $doc_pivot_id;
    }


    /**
     * @param array $input
     * @return mixed
     */
    public function storeGeneral(array $input)
    {
        return DB::transaction(function () use ($input) {
            $document_id = $input['document_id'];
            $data = json_decode($input['resource_info']);
            $get_resource_method = $data->get_resource_method;
            $doc_table = $data->doc_table;
            $doc_relation = (isset($data->doc_relation)) ? $data->doc_relation : 'documents';
            $resource_id = $data->resource_id;
            $input['resource_key_name'] = $data->resource_key_name;
            $input['resource_id'] = $resource_id;
            $input['external_id'] = $data->external_id;
            $resource_data = $this->getResourceInfoGeneral($resource_id, $document_id, $get_resource_method);
            $resource = $resource_data['resource'];
            $check_if_recurring = (new DocumentRepository())->checkIfDocumentIsRecurring($document_id);
            $ext = $this->getDocExtension('document_file');

            if ($check_if_recurring) {
                //It is a recurring document
                /* Save for Recurring Documents */
                $doc_resource_id = $this->savePivotDocumentGeneral($input,$doc_table, $get_resource_method, $doc_relation);
            } else {
                //It is not recurring document

                $check_if_exist = $this->checkIfDocumentExistGeneral($resource_id, $data->resource_key_name, $document_id, $data->external_id, $doc_table);
                if ($check_if_exist) {
                    $uploaded_doc = $resource->$doc_relation()->where('document_id', $document_id)->orderBy('id', 'desc')->first();

                    $doc_resource_id = $this->updatePivotDocumentGeneral($doc_table, $uploaded_doc->pivot->id, $input );
                } else {
                    /*if not exists - Attach new*/
                    $doc_resource_id = $this->savePivotDocumentGeneral($input,$doc_table, $get_resource_method, $doc_relation);
                }
            }
            /*Attach to storage*/
            $filename = $doc_resource_id .'.'. $ext;
            $path = base_storage_dir() . DIRECTORY_SEPARATOR . $resource_data['top_path'] .DIRECTORY_SEPARATOR .$resource_id ;
            $this->attachDocFileToStorageGeneral($path, $filename);
            return true;
        });
    }


    /**
     * @param array $input
     * @return mixed
     */
    public function updateGeneral(array $input)
    {
        return DB::transaction(function () use ($input) {
            //$document_id = $input['document_id'];
            $data = json_decode($input['resource_info']);
            $doc_pivot_id = $data->doc_pivot_id;
            $get_resource_method = $data->get_resource_method;
            $doc_table = $data->doc_table;
            $doc_relation = (isset($data->doc_relation)) ? $data->doc_relation : 'documents';
            $resource_id = $data->resource_id;

            $doc_pivot = DB::table($doc_table)->where('id', $doc_pivot_id)->first();
            $resource_data = $this->getResourceInfoGeneral($resource_id, $doc_pivot->document_id, $get_resource_method);

            //$resource = $this->getResource($input['resource_id'], $input['reference']);
            $ext = $this->getDocExtension('document_file');
            //$uploaded_doc = $resource->documents()->where('document_id', $document_id)->orderBy('id', 'desc')->first();
            $doc_resource_id = $this->updatePivotDocumentGeneral($doc_table, $doc_pivot_id, $input);
            /*Attach to storage*/
            $filename = $doc_resource_id .'.'. $ext;
            $path = base_storage_dir() . DIRECTORY_SEPARATOR . $resource_data['top_path'] .DIRECTORY_SEPARATOR .$resource_id ;
            $this->attachDocFileToStorageGeneral($path, $filename);
        });
    }



    /*Attach Doc file to storage General*/
    public function attachDocFileToStorageGeneral($path, $filename)
    {
        /*Attach document to server*/
        //$uploaded_doc = $resource->documents()->where(['document_id' => $document_id, "resource_id" => $resource->id])->orderBy('id', 'desc')->first();
        $path = $path . DIRECTORY_SEPARATOR;
        $this->makeDirectory($path);
        $this->saveDocumentBasic('document_file', $filename, $path );
    }


    /*Delete document - General*/
    public function deleteDocumentGen($doc_pivot_id, $resource_id, $reference)
    {
        return DB::transaction(function () use ($doc_pivot_id, $resource_id, $reference) {
            $resource = $this->getResource($resource_id, $reference);
            $data = $this->getResourceData($resource, $reference);
            $doc_table = isset($data['doc_table']) ? $data['doc_table'] : 'document_resource';
            $doc_resource = DB::table($doc_table)->where('id', $doc_pivot_id)->first();
            $resource_doc_id = $data->resource_id_doc;
            $get_resource_method = isset($data['get_resource_method']) ? $data['get_resource_method'] : 1;
            $resource_data = $this->getResourceInfoGeneral($resource_doc_id, $doc_resource->document_id, $get_resource_method);
            $path = base_storage_dir() . DIRECTORY_SEPARATOR . $resource_data['top_path'] . DIRECTORY_SEPARATOR . $resource_doc_id . DIRECTORY_SEPARATOR;
            $file_path = $path . $doc_pivot_id . '.' . $doc_resource->ext;
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            /*delete*/
            DB::table($doc_table)->where('id', $doc_pivot_id)->delete();
            return $data['return_url'];
        });
    }


    /* Check if document exists for closure */
    public function checkIfDocumentExistGeneral($resource_id, $resource_key_name, $document_id, $external_id, $doc_table)
    {

        if($external_id){
            $check = DB::table($doc_table)->where($resource_key_name, $resource_id)->where('document_id', $document_id)->where('external_id', $external_id)->count();
        }else{
            $check = DB::table($doc_table)->where($resource_key_name, $resource_id)->where('document_id', $document_id)->count();
        }

        if($check > 0)
        {
            return true;
        }else{
            return false;
        }
    }
}