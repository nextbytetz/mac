<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\EmployeeOldCompensation;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EmployeeOldCompensationRepository extends  BaseRepository
{

    const MODEL = EmployeeOldCompensation::class;

    public function __construct()
    {

    }

//find or throwexception
    public function findOrThrowException($id)
    {
        $employee_old_compensation = $this->query()->find($id);

        if (!is_null($employee_old_compensation)) {
            return $employee_old_compensation;
        }
        throw new GeneralException(trans('exceptions.backend.claim.employee_old_compensation_not_found'));
    }




    /*
     * create new
     */

    public function create($employee_id, $input) {
        return DB::transaction(function () use ($employee_id, $input) {
            $employee_old_compensation = $this->query()->create([
                'employee_id' => $employee_id,
                'incident_type_id' => $input['incident_type_id'],
                'who_paid' => $input['who_paid'],
                'payment_date' => $input['payment_date'],
                'amount' => $input['amount']
            ]);
        });
    }

    /*
     * update
     */
    public function update($employee_id,$input) {
        return DB::transaction(function () use ($employee_id, $input) {
            $employee_old_compensation = $this->findOrThrowException($employee_id);
            $employee_old_compensation->update([
                'incident_type_id' => $input['incident_type_id'],
                'who_paid' => $input['who_paid'],
                'payment_date' => $input['payment_date'],
                'amount' => $input['amount']
            ]);
            return $employee_old_compensation;
        });
    }








}