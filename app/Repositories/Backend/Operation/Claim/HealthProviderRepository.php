<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\HealthProvider;
use App\Exceptions\GeneralException;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\BaseRepository;
use App\Services\System\Database\CheckForeignReference;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class HealthProviderRepository extends  BaseRepository
{

    const MODEL = HealthProvider::class;

    public function __construct()
    {

    }

//find or throwexception
    public function findOrThrowException($id)
    {
        $health_provider = $this->query()->find($id);

        if (!is_null($health_provider)) {
            return $health_provider;
        }
        throw new GeneralException(trans('exceptions.backend.claim.health_provider_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

        return $this->query()->create([
            'name' => $input['name'],
            'common_name' => $input['common_name'],
            'external_id' => trim(strtolower($input['external_id'])),
            'zone' => $input['zone'],
            'council' => $input['council'],
            'ward' => $input['ward'],
            'street' => $input['street'],
            'facility_type' => $input['facility_type'],
            'ownership' => $input['ownership'],
            'user_id' => access()->id(),
            'is_contracted' => $input['is_contracted'],
            'district_id' => $input['district_id'],
        ]);
    }
    /*
     * update
     */
    public function update($id,$input) {
        $health_provider = $this->findOrThrowException($id);
        $health_provider->update(
            [
                'name' => $input['name'],
                'common_name' => $input['common_name'],
                'external_id' =>  trim(strtolower($input['external_id'])),
                'zone' => $input['zone'],
                'council' => $input['council'],
                'ward' => $input['ward'],
                'street' => $input['street'],
                'facility_type' => $input['facility_type'],
                'ownership' => $input['ownership'],
                'is_contracted' => $input['is_contracted'],
                'district_id' => $input['district_id'],
            ]
        );
        return $health_provider;
    }

    /*
     * delete
     */
    public function delete($id) {
        $checkForeign = new CheckForeignReference($this->instance()->getTable(), "id", $id);
        $checkForeign();
        $health_provider = $this->findOrThrowException($id);
        $health_provider->delete();
        return $health_provider;
    }

    public function getHealthProviders($q, $page)
    {
        return $this->getRegisteredHealthProviders($q, $page);
    }

    public function getRegisteredHealthProviders($q, $page)
    {
        //$name = trim(preg_replace('/\s+/', '', $q));
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = strtolower($q);
        $data['items'] = $this->query()
            ->select([
                'health_providers.id',
                DB::raw("CONCAT_ws(', ', coalesce(external_id, ''), health_providers.name, common_name, coalesce(regions.name, region) , coalesce(districts.name, district) , ward) as full_info"),
            ])
            ->leftJoin("districts", "districts.id", "=", "health_providers.district_id")
            ->leftJoin("regions", "regions.id", "=", "districts.region_id")
            ->whereRaw("cast(concat_ws(' ', health_providers.name, coalesce(common_name, ''), coalesce(external_id, '')) as text)  ~* ?", [$name])
            ->limit($resultCount)
            ->offset($offset)
            ->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);

    }






    /**
     * Upload bulk health providers.
     */
    public function uploadBulk($linked_file)
    {

        $user_id = access()->id();
        $healthProvider = new HealthProviderRepository();
        Excel::load($linked_file, function($reader)  {
            $objWorksheet = $reader->getActiveSheet();

        });

        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($linked_file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['facility_name','facility_number','common_name','zone', 'region','district','council','ward','street','facility_type', 'ownership'];

        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new GeneralException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($linked_file)
            ->chunk(250, function($result) use ($healthProvider,$user_id) {
                $rows = $result->toArray();
                //let's do more processing (change values in cells) here as needed
                //$counter = 0;

                foreach ($rows as $row) {

                    /* Get Region id from Region and Postal city*/
                    $regions = new RegionRepository();
                    $region = $regions->query()->where(function ($query) use ($row){
                        $query->where(DB::raw("LOWER(regexp_replace(" . DB::raw('name') . " , '[^a-zA-Z]', '', 'g'))"),'=',$string = preg_replace('/[^A-Za-z]/', '', strtolower($row['region'])));
                    })->first();

                    $external_id =  trim(strtolower($row['facility_number']));
                    /* Insert into health provider Table */
                    if (isset($row['facility_name']) And ($row['facility_name'])){
                        $health_provider =  $healthProvider->query()->firstOrCreate(['external_id' => $external_id], ['name' => $row['facility_name'], 'external_id' => $external_id, 'common_name' => $row['common_name'], 'zone' => $row['zone'], 'street' => $row['street'], 'facility_type' =>  $row['facility_type'], 'council' => $row['council'], 'ward' => $row['ward'], 'ownership' => $row['ownership'], 'region' => $row['region'], 'district' => $row['district'],'region_id' => ($region) ? $region->id : null , 'user_id' => $user_id ]);
                    }

                }
            }, true);
        /** end : Uploading excel data into the database */

    }






}