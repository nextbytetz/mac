<?php
namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\BenefitTypeClaim;
use App\Repositories\BaseRepository;

class BenefitTypeClaimRepository extends BaseRepository
{
    const MODEL = BenefitTypeClaim::class;

    /**
     * @param $incident_type_id
     * @return mixed
     */
    public function getForIncident($incident_type_id)
    {
        $query = $this->query()->whereNotIn("id", [2])->whereNull("dependency_id")->whereHas("benefitTypes", function ($query) use ($incident_type_id) {
            $query->whereHas("documents", function ($query) use ($incident_type_id) {
                $query->where("document_benefits.incident_type_id", $incident_type_id);
            });
        })->get();
        return $query;
    }

    public function getForIncidentBenefit($incident_type_id, $benefit_type_claim_id) {
        $benefit_types = (new BenefitTypeRepository())->query()->where("benefit_type_claim_id", $benefit_type_claim_id)->pluck("id")->all();
        $query = $this->query()->where("id", "<>", 2)->whereHas("benefitTypes", function ($query) use ($incident_type_id, $benefit_types) {
            $query->whereHas("documents", function ($query) use ($incident_type_id, $benefit_types) {
                $query->where("document_benefits.incident_type_id", $incident_type_id)->whereIn("benefit_type_id", $benefit_types);
            });
        })->get();
        return $query;
    }

    /**
     * @return mixed
     */
    public function getClaimBenefits()
    {
        $query = $this->query()->where("isactive", 1)->whereNull("dependency_id")->get();
        return $query;
    }

}