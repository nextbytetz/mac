<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Claim\NotificationContributionTrack;
use App\Models\Operation\Claim\NotificationReport;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NotificationContributionTrackRepository extends  BaseRepository
{

    const MODEL = NotificationContributionTrack::class;



    public function __construct()
    {

    }

//find or throw exception
    public function findOrThrowException($id)
    {
        $notification_contribution_track = $this->query()->find($id);

        if (!is_null($notification_contribution_track)) {
            return $notification_contribution_track;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_contribution_track_not_found'));
    }
    /*
     * create new
     */

    public function create($input)
    {
        return DB::transaction(function () use ($input) {
            $track = $this->query()->create($input);

            return $track;
        });
    }
    /*
     * update
     */

    public function update($id,$input)
    {
        return DB::transaction(function () use ($id,$input) {
            $track = $this->findOrThrowException($id);

            $track->update([
                'comments' => $input['comments'],
                'user_id' => access()->id(),
                'forward_date' => Carbon::now(),
                'isread' => 1,
                'status'=> 1,
            ]);


            if($track->unit_id == 15){
                $this->forwardTrack($track->unit_id, $track->notification_report_id);
            }else{
                if(array_key_exists('action', $input)){
                    /* If Action is RESPOND; forward track */
                    if($input['action'] == 2){
                        $this->forwardTrack($track->unit_id, $track->notification_report_id);
                    }



                }
            }



        });
    }



    public function forwardTrack($current_unit, $notification_report_id)
    {

        $next_unit = null;
        /*compliance*/
        if ($current_unit == 15){
            $next_unit = 14;
        }else if ($current_unit == 14){
            $next_unit = 15;
        }
        $input = ['notification_report_id' => $notification_report_id, 'from_user_id' => access()->id(), 'unit_id'=> $next_unit];

        $this->create($input);
    }




    public function checkIfCanUpdate($id)
    {
        $track = $this->findOrThrowException($id);
        $current_unit = $track->unit_id;
        $user_unit = access()->user()->unit_id;
        if ($track->comments){
            throw new GeneralException(trans('exceptions.backend.claim.track_is_already_updated'));
        }else{
            if($current_unit <> $user_unit){
                throw new GeneralException(trans('exceptions.backend.claim.not_in_your_department'));
            }
        }
    }



    public function checkIfPendingCompliance(Model $notification_report)
    {
        $track_pending =      $notification_report->notificationContributionTracks()->where('status', 0)->where('unit_id', 15)->first();

        if($track_pending){
           return 1;
        }else{
            return 0;
        }

    }



    public function checkIfTracksCompleted(Model $notification_report)
    {
        $track_pending =      $notification_report->notificationContributionTracks()->where('status', 0)->first();

        if($track_pending){
            throw new GeneralException(trans('exceptions.backend.claim.contribution_tracks_not_completed'));
        }

    }




    public function findRequiredContribution($incident_date)
    {
        return Carbon::parse($incident_date)->subMonth(1)->format('M Y');
    }




    public function getForDataTable($notification_report_id)
    {
        return $this->query()->where('notification_report_id', $notification_report_id)->orderBy('id', 'asc');
    }


    /**
     * Get all pending for dataTable. Pending Notification reports with no contribution
     */
    public function getPendingComplianceForDataTable()
    {
        $notificationReport = new NotificationReportRepository();
        return $notificationReport->query()->whereHas('notificationContributionTracks', function($query){
            $query->where('status', 0)->where('unit_id', 15);
        })->where('status',0);

    }

    /*Get all pending notifications on claim administration level*/
    public function getPendingClaimForDataTable()
    {
        $notificationReport = new NotificationReportRepository();
        return $notificationReport->query()->whereHas('notificationContributionTracks', function($query){
            $query->where('status', 0)->where('unit_id', 14);
        })->where('status',0);

    }

    public function getPendingCount(){
        $notificationReport = new NotificationReportRepository();
        $count = 0;
        if (access()->user()->unit_id == 15){
            $count =  $notificationReport->query()->whereHas('notificationContributionTracks', function($query){
                $query->where('status', 0)->where('unit_id', 15);
            })->where('status',0)->count();
        }elseif(access()->user()->unit_id == 14){
            $count =  $notificationReport->query()->whereHas('notificationContributionTracks', function($query){
                $query->where('status', 0)->where('unit_id', 14);
            })->where('status',0)->count();
        }
        return $count;
    }



}