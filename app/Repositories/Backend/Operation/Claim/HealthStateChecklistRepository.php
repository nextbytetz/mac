<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\HealthStateChecklist;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class HealthStateChecklistRepository extends  BaseRepository
{

    const MODEL = HealthStateChecklist::class;

    public function __construct()
    {

    }

//find or throwexception
    public function findOrThrowException($id)
    {
        $health_state_checklist = $this->query()->find($id);

        if (!is_null($health_state_checklist)) {
            return $health_state_checklist;
        }
        throw new GeneralException(trans('exceptions.backend.claim.health_state_checklist_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}