<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\NotificationWorkflow;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use App\Repositories\Backend\Operation\Claim\HealthProviderRepository;

class NotificationWorkflowRepository extends BaseRepository
{
	const MODEL = NotificationWorkflow::class;


	public function updateDocumentDetails($id, array $input)
	{

		$used = $this->query()->where('document_notification_report_id', $input['document_used'])->where('id','!=',$id)->first();

		if (!empty($used)) {
			throw new GeneralException('The selected Document has already been used for another assesment');
		}

		$this->query()->where('id', $id)->update([
			'document_notification_report_id' => $input['document_used'],
			'document_date' => $input['document_date'],
			'health_provider_id' => $input['hcp_name'],
		]);

	}

	public function returnDocumentDetails($id)
	{
		$return = [
			"document_notification_report_id" => null,
			"document_date" => null,
			"document_id" => null,
			"health_provider_id" => null
		];
		$data = $this->query()->select('document_notification_report_id','document_date','document_notification_report.document_id','health_provider_id')
		->join("main.document_notification_report", "document_notification_report.id", "=", "notification_workflows.document_notification_report_id")
		->where('notification_workflows.id',$id)->first();
		if (!empty($data)) {
			$hcp = (new HealthProviderRepository())->query()->find($data->health_provider_id);
			$hcp_name = $hcp->external_id.','.$hcp->external_id.','.$hcp->common_name.','.$hcp->region.','.$hcp->district.','.$hcp->ward;
			$return = $data->toArray();
			$return['hcp_name'] = $hcp_name;
		}
		return $return;
	}

}