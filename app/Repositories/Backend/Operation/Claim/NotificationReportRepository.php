<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Events\NewWorkflow;
use App\Exceptions\JobException;
use App\Exceptions\RedirectException;
use App\Exceptions\WorkflowException;
use App\Jobs\Claim\DeleteNotificationDms;
use App\Jobs\Claim\PostDocumentDms;
use App\Jobs\Claim\PostNotificationDms;
use App\Jobs\Notifications\SendSmsEga;
use App\Models\Operation\Claim\NotificationReport;
use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\NotificationReportAppeal;
use App\Notifications\Backend\Operation\Claim\Portal\ClaimAccountValidated;
use App\Repositories\Backend\Access\IncidentUserRegionRepository;
use App\Repositories\Backend\Access\IncidentUserRepository;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\Notification\Legacy;
use App\Repositories\Backend\Operation\Claim\Notification\Progressive;
use App\Repositories\Backend\Operation\Claim\Portal\ClaimActivityRepository;
use App\Repositories\Backend\Operation\Claim\Traits\NotificationReport\DefaultTrait;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Sysdef\CodeRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Operation\Claim\NotificationInvestigatorRepository;
use App\Services\Claim\NotificationDocEOfficeAutoSync;
use App\Services\Finance\ProcessPaymentPerBenefit;
use App\Services\Notifications\Api;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Auth\User;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;

class NotificationReportRepository extends  BaseRepository
{
    use Api, Legacy, Progressive, FileHandler, AttachmentHandler, DefaultTrait;


    const MODEL = NotificationReport::class;

    protected $accidents;
    protected $diseases;
    protected $investigation_feedbacks;
    protected $investigation_questions;
    protected $notification_health_providers;
    protected $notification_health_provider_services;
    protected $notification_health_provider_practitioners;
    protected $notification_health_states;
    protected $notification_disability_states;
    protected $death;
    protected $incident_date;
    protected $reporting_date;
    protected $receipt_date;
    protected $claims;
    protected $employees;
    protected $benefit_types;
    protected $medical_expenses;


    public function __construct()
    {
        parent::__construct();
        $this->accidents = new AccidentRepository();
        $this->diseases = new DiseaseRepository();
        $this->death = new DeathRepository();
        $this->investigation_feedbacks = new InvestigationFeedbackRepository();
        $this->investigation_questions = new InvestigationQuestionRepository();
        $this->notification_health_providers = new NotificationHealthProviderRepository();
        $this->notification_health_provider_practitioners = new NotificationHealthProviderPractitionerRepository();
        $this->notification_health_provider_services = new NotificationHealthProviderServiceRepository();
        $this->notification_health_states = new NotificationHealthStateRepository();
        $this->notification_disability_states = new NotificationDisabilityStateRepository();
        $this->claims = new ClaimRepository();
        $this->employees = new EmployeeRepository();
        $this->benefit_types = new BenefitTypeRepository();
        $this->medical_expenses = new MedicalExpenseRepository();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $notification_report = $this->query()->where("id", $id)->first();

        if (!is_null($notification_report)) {
            return $notification_report;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_report_not_found'));
    }

    public function saveAckwlgletter(Model $notification_report, array $input)
    {
        return DB::transaction(function () use ($notification_report, $input) {
            $notification_report->letter()->updateOrCreate(
                ['notification_report_id' => $notification_report->id],
                [
                    'acknowledgement' => $input['acknowledgement_letter'],
                    'acknowledgement_user' => access()->id(),
                ]);
            return true;
        });
    }

    public function issueAckwlgletter(Model $notification_report) {
        return DB::transaction(function () use ($notification_report) {
            $count = $notification_report->is_acknowledgment_sent;
            $notification_report->is_acknowledgment_sent = $count + 1;
            $notification_report->save();
            return true;
        });
    }

    /**
     *
     */
    public function checkUploadedDocuments($id = NULL, $skipchecked = 1)
    {

        $this->checkBenefitStatusGeneral($id, $skipchecked);
        $this->checkBenefitStatusForUploaded($id, $skipchecked);

        //$incidents = $this->query()->where(["isprogressive" => 1])->where("approval_ready", 0)->orWhere("approval_ready_basic", 0);
        $incidents = $this->query()->where(["isprogressive" => 1]);

        if ($id) {
            $incidents->where("id", $id);
        }
        $incidents->chunk(100, function ($incidents) {
            foreach ($incidents as $incident) {
                if (!$incident->approval_ready) {
                    $count = $this->checkAllMandatoryDocumentsProgressive($incident)->count();
                    if (!$count) {
                        $attribute = "doc_receive_date";
                        $docs = $this->getProgressiveDocumentList($incident);
                        $date = $this->getUploadedMaxDate($incident, $docs, $attribute);

                        $incident->approval_ready = 1;
                        $incident->approval_ready_date = $date[$attribute];
                        $incident->approval_ready_mac_date = $date["created_at"];
                        $incident->save();
                    }
                } else {
                    $count = $this->checkAllMandatoryDocumentsProgressive($incident)->count();
                    if ($count) {
                        $incident->approval_ready = 0;
                        $incident->save();
                    }
                }
                if (!$incident->approval_ready_basic) {
                    $count = $this->checkAllMandatoryDocumentsProgressive($incident, false)->count();
                    if (!$count) {

                        $attribute = "doc_receive_date";
                        $docs = $this->getProgressiveDocumentList($incident, [], NULL, false);
                        $date = $this->getUploadedMaxDate($incident, $docs, $attribute);

                        $incident->approval_ready_basic = 1;
                        $incident->approval_ready_basic_date = $date[$attribute];
                        $incident->approval_ready_basic_mac_date = $date["created_at"];
                        $incident->save();
                    }
                } else {
                    $count = $this->checkAllMandatoryDocumentsProgressive($incident, false)->count();
                    if ($count) {
                        $incident->approval_ready_basic = 0;
                        $incident->save();
                    }
                }
            }
        });
    }

    /**
     * @param null $id , $notification_report_id
     * @param int $skipchecked
     */
    public function checkBenefitStatusForUploaded($id = NULL, $skipchecked = 1)
    {
        $incidents = $this->query()->where(["isprogressive" => 1]);
        //->where("progressive_stage", ">=", 4);
        if ($id) {
            $incidents->where("id", $id);
        }

        $benefitClaimRepo = new BenefitTypeClaimRepository();
        $benefitClaims = $benefitClaimRepo->query()->whereNull("dependency_id")->whereNotIn("id", [2])->whereIn("id", [1,3,4])->orderBy("id", "desc")->get();
        //MAE Refund (Employee/Employer)
        //Temporary Disablement
        //Temporary Disablement Refund
        foreach ($benefitClaims as $benefitClaim) {
            $query = with(clone $incidents);
            $benefitClaimId = $benefitClaim->id;
            $query->whereIn("id", function ($query) use ($benefitClaim) {
                $query->select(["document_notification_report.notification_report_id"])->from("document_notification_report")->where("isreferenced", 0)->whereIn("document_notification_report.document_id", $benefitClaim->repeatedDocs());
            })->chunk(1000, function ($incidents) use ($benefitClaim, $benefitClaimId, $skipchecked) {
                foreach ($incidents as $incident) {

                    $return = $this->checkDocumentBenefitStatus($incident, $benefitClaimId);
                    $docStatus = $return['success'];
                    $count = 0;
                    //Log::error($return);
                    //Log::error($benefitClaimId);
                    $docReadyDate = $return['ready_date'];
                    $docReadyMacDate = $return['ready_mac_date'];
                    switch ($incident->incident_type_id) {
                        case 1:
                        case 2:
                        case 4:
                        case 5:
                            //Accident
                            //Disease
                        switch ($benefitClaimId) {
                            case 1:
                                    //MAE Refund (Employee/Employer)
                            $countQuery = $incident->maes()->whereNull("incident_mae.notification_eligible_benefit_id")->orderByDesc("incident_mae.id");
                                    //Log::error($docStatus);
                            if (!$countQuery->count()) {
                                if ($docStatus) {
                                    $mae = $incident->maes()->create([
                                        'mae_refund_member_ready' => 1,
                                        'mae_refund_member_ready_date' => $docReadyDate,
                                        'mae_refund_member_ready_mac_date' => $docReadyMacDate,
                                    ]);
                                    $incident->incident_mae_id = $mae->id;
                                    $incident->save();
                                }
                            } else {
                                if (!$skipchecked && !$docStatus) {
                                    $incident->incident_mae_id = NULL;
                                    $incident->save();
                                    $query = $countQuery->first();
                                    $query->delete();
                                }
                            }
                            break;
                            case 3:
                                    //Temporary Disablement
                            $countQuery = $incident->tds()->whereNull("incident_tds.notification_eligible_benefit_id")->orderByDesc("incident_tds.id");
                            if (!$countQuery->count()) {
                                if ($docStatus) {
                                    $td = $incident->tds()->create([
                                        'td_ready' => 1,
                                        'td_ready_date' => $docReadyDate,
                                        'td_ready_mac_date' => $docReadyMacDate,
                                    ]);
                                    $incident->td_ready = 1;
                                    $incident->incident_td_id = $td->id;
                                    $incident->save();
                                }
                            } else {
                                if (!$skipchecked && !$docStatus) {
                                    $incident->incident_td_id = NULL;
                                    $incident->save();
                                    $query = $countQuery->first();
                                    $query->delete();
                                }
                            }
                            break;
                            case 4:
                                    //Temporary Disablement Refund
                                    //Log::error($docStatus);
                            $countQuery = $incident->tds()->whereNull("incident_tds.notification_eligible_benefit_id")->orderByDesc("incident_tds.id");
                            if (!$countQuery->count()) {
                                if ($docStatus) {
                                    $td = $incident->tds()->create([
                                        'td_refund_ready' => 1,
                                        'td_refund_ready_date' => $docReadyDate,
                                        'td_refund_ready_mac_date' => $docReadyMacDate,
                                    ]);
                                    $incident->td_refund_ready = 1;
                                    $incident->incident_td_id = $td->id;
                                    $incident->save();
                                }
                            } else {
                                if (!$skipchecked && !$docStatus) {
                                    $incident->td_refund_ready = NULL;
                                    $incident->save();
                                    $query = $countQuery->first();
                                    $query->delete();
                                }
                            }
                            break;
                        }
                        break;
                    }
                    //flag off reference on the notification document
                    //$query = $incident->documents()->where("isreferenced", 0)->limit(1);
                    if ($docStatus) {
                        $incident->documents()->newPivotStatement()->whereIn('document_id', $benefitClaim->repeatedDocs())->where("document_notification_report.notification_report_id", $incident->id)->update(['isreferenced' => 1]);
                    }
                }
            });
        }

    }

    /**
     * @param null $id
     * @param int $skipchecked
     */
    public function checkBenefitStatusGeneral($id = NULL, $skipchecked = 1)
    {

        $query = $this->query()->where(["isprogressive" => 1]);
        //->where("progressive_stage", ">=", 4)
        if ($skipchecked) {
            $query->where(function($query) {
                //$query->orWhere("td_ready", 0)->orWhere("td_refund_ready", 0)->orWhere("pd_ready", 0)->orWhere("survivor_ready", 0)->orWhere("mae_refund_member_ready", 0);
                $query->orWhere("pd_ready", 0)->orWhere("survivor_ready", 0);
            });
        }

        if ($id) {
            $query->where("id", $id);
        }

        $query->chunk(1000, function ($incidents) use ($skipchecked) {
            foreach ($incidents as $incident) {

                $return_to_work = 0;
                $benefitClaimId = 0;

                switch ($incident->incident_type_id) {
                    case 1:
                    case 2:
                        //Accident
                        //Disease
                        if ($incident->incident_type_id == 1) {
                                //Accident
                                //$return_to_work = $incident->accident->return_to_work;
                        } else {
                                //Disease
                                //$return_to_work = $incident->disease->return_to_work;
                        }

                        //Check for MAE Refund
                        /*if (!$incident->mae_refund_member_ready) {
                            $benefitClaimId = 1;
                            $this->updateDocumentBenefitStatus($incident, $benefitClaimId, "mae_refund_member_ready");
                        }*/

                        //Check for Temporary Disablement
                        /*if ($return_to_work) {
                            $incident->td_ready = 0;
                            $incident->save();
                            if (!$incident->td_refund_ready) {
                                $benefitClaimId = 4;
                                $this->updateDocumentBenefitStatus($incident, $benefitClaimId, "td_refund_ready");
                            }
                        } else {
                            $incident->td_refund_ready = 0;
                            $incident->save();
                            if (!$incident->td_ready) {
                                $benefitClaimId = 3;
                                $this->updateDocumentBenefitStatus($incident, $benefitClaimId, "td_ready");
                            }
                        }*/
                        //Check for Permanent Disablement
                        //if (!$incident->pd_ready) {
                            $benefitClaimId = 5;
                            $this->updateDocumentBenefitStatus($incident, $benefitClaimId, "pd_ready", false, $skipchecked);
                        //}
                        break;
                        case 3:
                        case 4:
                        case 5:
                        //Death
                        //No return to work date
                        //if (!$incident->survivor_ready) {
                            $benefitClaimId = 7;
                            $this->updateDocumentBenefitStatus($incident, $benefitClaimId, "survivor_ready", false, $skipchecked);
                        //}
                        break;
                    }
                }
            });

        /*        $this->query()->where(["isprogressive" => 1])->where("approval_ready", 0)->chunk(100, function ($incidents) {
                    foreach ($incidents as $incident) {
                        if (!$incident->approval_ready) {
                            $count = $this->checkAllMandatoryDocumentsProgressive($incident)->count();
                            if (!$count) {
                                $incident->approval_ready = 1;
                                $incident->save();
                            }
                        }
                    }
                });*/
            }

    /**
     * @param Model $incident
     * @param $benefitClaimId
     * @param $attribute
     * @param bool $isreferenced
     * @param int $skipchecked
     * @return bool
     */
    public function updateDocumentBenefitStatus(Model $incident, $benefitClaimId, $attribute, $isreferenced = true, $skipchecked = 1)
    {
        $return = $this->checkDocumentBenefitStatus($incident, $benefitClaimId, $isreferenced);
        if ($return['success']) {
            $incident->$attribute = 1;
            $ready_date = $attribute . "_date";
            $ready_mac_date = $attribute . "_mac_date";
            $incident->$ready_date = $return['ready_date'];
            $incident->$ready_mac_date = $return['ready_mac_date'];
            $incident->save();
        } else {
            if (!$skipchecked) {
                $incident->$attribute = 0;
                $ready_date = $attribute . "_date";
                $ready_mac_date = $attribute . "_mac_date";
                $incident->$ready_date = NULL;
                $incident->$ready_mac_date = NULL;
                $incident->save();
            }
        }
        return $return["success"];
    }

    /**
     * @param Model $incident
     * @param $benefitClaimId
     * @param bool $isreferenced
     * @return array
     */
    public function checkDocumentBenefitStatus(Model $incident, $benefitClaimId, $isreferenced = true)
    {
        $return = [];
        $return['success'] = false;
        $return['ready_date'] = Carbon::now();
        $return['ready_mac_date'] = Carbon::now();
        $benefitClaimRepo = new BenefitTypeClaimRepository();
        $benefitClaim = $benefitClaimRepo->find($benefitClaimId);
        $uploaded_documents = $this->getUploadedBenefitDocument($incident, $isreferenced);
        $needed_documents = $benefitClaim->documents()->pluck("id")->all();
        $check = array_intersect($needed_documents, $uploaded_documents);
        //if (count($check) == count($needed_documents) || env('TESTING_MODE')) {
        if (count($check) == count($needed_documents)) {
            $attribute = "doc_receive_date";
            $date = $this->getUploadedMaxDate($incident, $needed_documents, $attribute);
            $return['success'] = true;
            $return['ready_date'] = $date[$attribute] ?? Carbon::now();
            $return['ready_mac_date'] = $date["created_at"] ?? Carbon::now();
        }
        return $return;
    }

    /**
     * @return bool
     */
    public function transferToProgressive()
    {
        /*$this->query()->chunk(10000, function ($incidents) {
            foreach ($incidents as $incident) {
                $this->doTransferToProgressive($incident);
            }
        });*/
        $incidents = $this->query()->where(["isprogressive" => 0, 'status' => 0, 'wf_done' => 0])->get();
        foreach ($incidents as $incident) {
            $this->doTransferToProgressive($incident);
        }
        return true;
    }

    /**
     * @param Model $incident
     * @return mixed
     */
    public function doTransferToProgressive(Model $incident)
    {
        return DB::transaction(function () use ($incident) {

            if (!$incident->isprogressive And !$incident->status And !$incident->wf_done) {

                $codeValueRepo = new CodeValueRepository();
                $stage_comments = NULL;

                $employee_id = $incident->employee_id;

                $incident->status = 3;
                $incident->isprogressive = 1;
                $incident->istransfered = 1;
                $incident->save();

                //Check if it is within time limit
                $return = $this->checkDateLimit($incident);

                if ($return) {
                    //Notification is within time ...
                    //Check if there is contribution before the incident date ....
                    $this->checkContributionStatus($incident);
                }

                //Employee is registered
                ///Save Employer information
                $employee = $this->employees->query()->where("id", $employee_id)->first();
                $incident->employers()->syncWithoutDetaching($employee->employers()->select([DB::raw("employers.id as employer_id")])->pluck("employer_id")->all());

                //Initialize the notification report
                $this->initialize($incident);
            }

            return true;
        });
    }

    /**
     * @param Model $incident
     * @return mixed
     * @throws GeneralException
     */
    public function createFromOnline(Model $incident)
    {
        $employee_id = $incident->employee_id;

        $input = [
            'incident_date' => $incident->incident_date,
            'incident_type_id' => $incident->incident_type_id,
            'reporting_date' => $incident->reporting_date,
            'receipt_date' => $incident->created_at,
            'district_id' => $incident->district_id,
            'employer_id' => $incident->employer_id,
            'employee_id' => $incident->employee_id,
            'allocated' => $incident->allocated,
            'source' => 2,
            'incident_id' => $incident->id,
            'next_of_kin' => NULL,
        ];

        switch ($incident->incident_type_id) {
            case 1:
                //Accident
            $accident = $incident->accident;
            $input['accident_type_id'] = $accident->accident_type_id;
            $input['accident_time'] = $accident->incident_time;
            $input['activity_performed'] = $accident->activity;
            $input['accident_place'] = $accident->place;
            $input['description'] = $incident->description;
            $input['witness_name'] = NULL;
            break;
            case 2:
                //Disease
            $disease = $incident->disease;
            $input['disease_name'] = $disease->name;
            $input['health_provider_id'] = NULL;
            $input['medical_practitioner_id'] = NULL;
            break;
            case 3:
                //Death
            $death = $incident->death;
            $input['death_place'] = $death->death_place;
            $input['death_cause_id'] = $death->death_cause_id;
            $input['health_provider_id'] = NULL;
            $input['medical_practitioner_id'] = NULL;
            $input['activity_performed'] = $death->activity;
            $input['incident_occurrence_cv_id'] = $death->incident_occurrence_cv_id;
            $input['description'] = $incident->description;
            break;
        }

        $created = $this->create($employee_id, $input);

        //Synchronize Validated Documents

        return $created;
    }

    public function searchRegisteredNotification($q, $page)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = strtolower(single_space($q));
        $concat = "CONCAT_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname, notification_reports.filename)";
        $query = $this->query()
        ->select([
            'notification_reports.id',
            DB::raw("{$concat} as name"),
        ])
        ->leftJoin("employees", "employees.id", "=", "notification_reports.employee_id");
        $names = explode(' ', $name);
        foreach ($names as $value) {
            $query->whereRaw("cast({$concat} as text)  ~* ?", [$value]);
        }
        $data['items'] = $query
        ->limit($resultCount)
        ->offset($offset)
        ->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    /**
     * @param $employee_id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function create($employee_id, $input)
    {
        return DB::transaction(function () use ($employee_id, $input) {

            $codeValueRepo = new CodeValueRepository();
            $stage_comments = NULL;

            if ($employee_id) {
                //Employee is registered.
                //check if there is an already active notification
                $checkQuery = $this->query()->where("employee_id", $employee_id)->where(function($query) use ($input) {
                    $query->where("incident_date", $input['incident_date']);
                })->whereHas('stage', function ($query) {
                    $query->whereIn('code_id', [13, 23]);
                });
                if ($checkQuery->count()) {
                    //incident exists, redirect to the already existing incident ...
                    if (isset($input['incident_id'])) {
                        //if request comes from the online incident application ...
                        throw new WorkflowException("Create Failed, this employee already has an active notification registered with the same incident date.");
                    } else {
                        $incident = $checkQuery->first();
                        return $incident;
                    }
                }
                if (!isset($input['incident_id'])) {
                    //also check online incident
                    $checkQuery2 = (new PortalIncidentRepository())->query()->where("employee_id", $employee_id)->where(function($query) use ($input) {
                        $query->where("incident_date", $input['incident_date']);
                    });
                    if ($checkQuery2->count()) {
                        $incident = $checkQuery2->first();
                        throw new RedirectException(route('backend.compliance.employer.incident_profile', $incident->id));
                    }
                }
                //check if this employee has had death notification before.
                $checkQuery = $this->query()->where("employee_id", $employee_id)->where("incident_type_id", 3)->whereIn("wf_done", [0, 1]);
                if ($checkQuery->count()) {
                    //$incident = $checkQuery->first();
                    //return $incident;
                    throw new WorkflowException("Create Failed, this employee has had a death incident before");
                }
            } else {
                //Employee is not registered.
                $checkQuery = $this->query()->whereRaw("cast(employee_name as text)  ~* ?", [$input['employee_name']])->where("incident_date", $input['incident_date']);
                if ($checkQuery->count()) {
                    //incident exists, redirect to the already existing incident ...
                    if (isset($input['incident_id'])) {
                        //if request comes from the online incident application ...
                        throw new WorkflowException("Create Failed, this employee already has an active notification registered with the same incident date.");
                    } else {
                        $incident = $checkQuery->first();
                        return $incident;
                    }
                }
            }

            $data = [
                'incident_type_id' => $input['incident_type_id'],
                'status' => 3,
                'incident_date' => $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'district_id' => $input['district_id'],
                'isprogressive' => 1,
                'source' => (isset($input['source'])) ? $input['source'] : 1,
                'user_id' => access()->user()->id
            ];

            if ($data['source'] == 2) {
                $data['incident_id'] = $input['incident_id'];
            }

            $incident = $this->query()->create($data);

            if ($employee_id) {
                //Employee is registered
                $incident->employee_id = $employee_id;
                $incident->employer_id = $input['employer_id'];
                $incident->save();
                ///Save Date Hired & employee information from employee_employer on incident if not filled when registering
                /*if (isset($input['date_hired']) And !empty($input['date_hired'])) {
                    //Store the date hire
                    $this->employees->storeDateHired($employee_id, $input['employer_id'], $input['date_hired']);
                }
                $datehired = $this->employees->getDateHired($employee_id, $input['employer_id']);
                $incident->datehired = $datehired;
                $incident->save();*/
                $this->storeDateHired($incident, $input);

                //Check if it is within time limit
                $return = $this->checkDateLimit($incident);

                if ($return) {
                    //Notification is within time ...
                    //Check if there is contribution before the incident date ....
                    $this->checkContributionStatus($incident);
                }

                //Employee is registered
                ///Save Employer information
                $employee = $this->employees->query()->where("id", $employee_id)->first();
                $incident->employers()->syncWithoutDetaching($employee->employers()->select([DB::raw("employers.id as employer_id")])->pluck("employer_id")->all());
                ///Save Next of Kin Information if present
                //Check if next of kin index is present
                if (isset($input['next_of_kin']) And !empty($input['next_of_kin'])) {
                    $employee->next_of_kin = $input['next_of_kin'];
                    $employee->next_of_kin_phone = $input['next_of_kin_phone'];
                    $employee->save();
                }

            } else {
                //Employee is not registered
                //This is the partial registration
                $incident->employee_name = $input['employee_name'];
                $incident->has_employee = 0;
                $incident->employer_name = $input['employer_name'];
                if ($input['employer_id']) {
                    $incident->employer_id = $input['employer_id'];
                } else {
                    $incident->has_employer = 0;
                }
                $incident->iscomplete_register = 0;
                //Pending Partial Registration (Unregistered Member)
                $incident->notification_staging_cv_id = $codeValueRepo->NSPPRUM();
                $incident->save();
            }

            //store accident
            if ($input['incident_type_id'] == 1) {
                $accident = $this->accidents->create($incident, $input);
                $this->updateOacNo($accident);
                if ($employee_id) {
                    $witnessRepo = new WitnessRepository();
                    //Employee is registered
                    switch ($incident->source) {
                        case 1:
                            //Registered Branch
                            ///Save Accident Witnesses
                        if (!empty($input['witness_name'])) {
                            $witnessRepo->create($incident->id, $input);
                        }
                        break;
                        case 2:
                            //Registered Online
                        $witnesses = $incident->onlineRequest->witnesses;
                        foreach ($witnesses as $witness) {
                            $witnessRepo->createFromOnline($incident->id, [
                                'witness_name' => $witness->name,
                                'witness_phone' => $witness->phone,
                            ]);
                        }
                        break;
                    }
                }
            }
            //store disease
            if ($input['incident_type_id'] == 2) {
                $desease = $this->diseases->create($incident, $input);
                $this->updateOdsNo($desease);
            }
            //store death
            if ($input['incident_type_id'] == 3) {
                $death = $this->death->create($incident, $input);
                $this->updateOdtNo($death);
            }

            //store closure information
            $incident->closures()->create([
                'register_date' => Carbon::now(),
            ]);

            //store td information (No need to create here, it will be detected after document upload) : to be deleted
            //$incident->tds()->create([]);

            if ($employee_id) {
                //If employee is registered.
                ///If employee is not registered, the following will be updated once the notification is completely registered.

                /* start: Update notification file name */
                $incident->filename = $incident->file_name_specific_label;
                $incident->save();
                /* end: Update notification file name */

                /**
                 * Start:
                 * e-office integration
                 * Job will be needed, which will look for acknowledgement has been received and reschedule for later sending
                 * Create file to e-office {Parameters, fileType = 3, classification = 1, fileNumber = Notification#, fileName = [OAC|ODS|ODT]/EmployeeNo/Notification#, department = 8, subject = EmployeeName/Notification#, description = NULL}
                 */
                //Get employee number
                dispatch(new PostNotificationDms($incident));
                //Check if there is positive response, if not attempt to resend next time.
                /**
                 * End:
                 * e-office integration
                 */
            }

            //Update eligible benefit (MAE) for each notification. MAE is the least benefit which can be paid.
            //$incident->eligibleBenefits()->syncWithoutDetaching([10 => ['member_type_id' => 2]]);

            //Initialize the notification report
            $this->initialize($incident);

            return $incident;
        });
}

public function cancelBenefit(Model $incident, Model $eligible)
{
    return DB::transaction(function() use ($incident, $eligible) {
        $wfTrackRepo = new WfTrackRepository();
        $eligibleRepo = new NotificationEligibleBenefitRepository();
        $codeValueRepo = new CodeValueRepository();
        $user = access()->id();
        $workflow = $eligible->workflow;
        $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id);
        if ($wfTrackInstance->count()) {
            $wfTrack = $wfTrackInstance->first();
            $wfTrack->user_id = $user;
            //$wfTrack->allocated = $user;
            $wfTrack->assigned = 1;
            $wfTrack->user_type = User::class;
            $wfTrack->comments = 'Cancelled Benefit Process';
            $wfTrack->forward_date = Carbon::now();
            $wfTrack->status = 5;
            $wfTrack->save();
        }
        $workflow->wf_done = 5; //Cancelled by User
        $workflow->wf_done_date = Carbon::now();
        $workflow->save();

        $eligible->processed = 3;
        $eligible->save();

        //check if there are other pending benefit initiated workflows
        $count = $eligibleRepo->query()->where(["processed" => 0, "isinitiated" => 1, "notification_report_id" => $incident->id])->count();

        if (!$count) {
            //update notification stage
            //On Progress (Incident Approved)
            //$incident->notification_staging_cv_id = $codeValueRepo->NSOPIA();
            //On Progress (At least Payed)
            $count = $eligibleRepo->query()->where(["processed" => 1, "notification_report_id" => $incident->id])->count();
            if ($count) {
                $notification_staging_cv_id = $codeValueRepo->NSOPALP(); //On Progress (At least Payed)
            } else {
                $notification_staging_cv_id = $codeValueRepo->NSOPIA(); //On Progress (Incident Approved)
            }
            $incident->notification_staging_cv_id = $notification_staging_cv_id;
            $incident->save();
            $this->initialize($incident);
        }

    });
}

public function undoBenefit(Model $incident)
{
    return DB::transaction(function() use ($incident) {
            //
        $eligibleCount = (new NotificationEligibleBenefitRepository())->query()->where(['notification_report_id' => $incident->id])->where(function ($query) {
            $query->where('ispayprocessed', 1)->orWhere('iszeropaid', 1);
        })->count();
        if ($eligibleCount) {
            throw new GeneralException('Can not undo benefit process, at least one of the benefit has already been paid...');
        } elseif ($incident->haspaidmanual) {
            throw new GeneralException('Can not undo approval, this file has already been marked PAID MANUALLY.');
        } else {
                //flag off all active workflows
            $wfTrackRepo = new WfTrackRepository();
            $user = access()->id();
            /*reserve current notification stage*/
                //-->close any possible pending workflow
            $workflows = $incident->processes()->where('notification_workflows.wf_done', 0)->get();
            foreach ($workflows as $workflow) {

                $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id);
                if ($wfTrackInstance->count()) {
                    $wfTrack = $wfTrackInstance->first();
                    $wfTrack->user_id = $user;
                        //$wfTrack->allocated = $user;
                    $wfTrack->assigned = 1;
                    $wfTrack->user_type = User::class;
                    $wfTrack->comments = 'Undo Benefit Process';
                    $wfTrack->forward_date = Carbon::now();
                    $wfTrack->status = 5;
                    $wfTrack->save();
                }
                    $workflow->wf_done = 5; //Cancelled by User
                    $workflow->wf_done_date = Carbon::now();
                    $workflow->save();
                }
            }

            //delete claim information
            //$incident->claim()->delete();

            //update eligibles if exist indicating cancelled by user process...
            $benefits = $incident->benefits;
            foreach ($benefits as $benefit) {
                $benefit->processed = 3;
                $benefit->save();
                switch($benefit->benefit_type_claim_id) {
                    case 1:
                        //MAE Refund (Employee/Employer)
                        $incident->mae_refund_member_ready = 0;
                        $incident->incident_mae_id = NULL;
                        $incident->save();
                        //$incident->documents()->update(["isreferenced" => 0]);
                        DB::table(pg_mac_main() . ".document_notification_report")
                            ->where('notification_report_id', $incident->id)
                            ->update(["isreferenced" => 0]);
                        break;
                    case 3:
                    case 4:
                        //Temporary Disablement
                        //Temporary Disablement Refund
                        $incident->td_ready = 0;
                        $incident->td_refund_ready = 0;
                        $incident->incident_td_id = NULL;
                        $incident->save();
                        //$incident->documents()->update(["isreferenced" => 0]);
                        DB::table(pg_mac_main() . ".document_notification_report")
                            ->where('notification_report_id', $incident->id)
                            ->update(["isreferenced" => 0]);
                        break;
                    case 5:
                        //Permanent Disablement
                        $incident->pd_ready = 0;
                        $incident->pd_eligible_id = NULL;
                        $incident->save();
                        break;
                }
            }
            /*$incident->benefits()->update([
                'processed' => 3,
            ]);*/

            //Update the staging and initialize incident
            $incident->progressive_stage = 3;
            //Pending Complete Registration (Filling Investigation)
            $incident->notification_staging_reserve_cv_id = $incident->notification_staging_cv_id;
            $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFNFI();
            $incident->save();
            $this->initialize($incident);

            return true;
        });
}

    /**
     * @param Model $incident
     * @return mixed
     */
    public function undoApproval(Model $incident)
    {
        return DB::transaction(function() use ($incident) {

            //check if this is the checklist user

            $eligibleCount = (new NotificationEligibleBenefitRepository())->query()->where("notification_report_id", $incident->id)->count();
            if ($eligibleCount) {
                throw new GeneralException('Can not undo approval, benefit process has already been initiated...');
            } elseif ($incident->haspaidmanual) {
                throw new GeneralException('Can not undo approval, this file has already been marked PAID.');
            } else {
                $workflowCount = (new NotificationWorkflowRepository())->query()->whereHas("wfModule", function($query) {
                    $query->where("wf_module_group_id", 4);
                })->where("notification_report_id", $incident->id)->count();
                if ($workflowCount) {
                    throw new GeneralException("Can not undo approval, there is an active workflow pending for users' action");
                }
            }

            //delete claim information
            $incident->claim()->forceDelete();

            //Update the staging and initialize incident
            $incident->progressive_stage = 3;
            //Pending Complete Registration (Filling Investigation)
            $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFNFI();
            $incident->save();
            $this->initialize($incident);

            //check if it is an active workflow (especially for rejection workflow)

            return true;
        });
    }

    /**
     * @param Model $incident
     * @param array $input ['date_hired', 'employer_id', 'date_hired']
     * @return bool
     * @throws GeneralException
     */
    public function storeDateHired(Model $incident, array $input)
    {
        ///Save Date Hired & employee information from employee_employer on incident if not filled when registering
        if (isset($input['date_hired']) And !empty($input['date_hired'])) {
            //Store the date hire
            $this->employees->storeDateHired($incident->employee_id, $input['employer_id'], $input['date_hired']);
        }
        $datehired = $this->employees->getDateHired($incident->employee_id, $input['employer_id']);
        $incident->datehired = $datehired;
        $incident->save();
        return true;
    }

    /**
     * @param Model $incident
     * @return bool
     * @description check if the notification has been registered out of time | Before Statutory Period.
     */
    public function checkDateLimit(Model $incident)
    {
        $return = true;
        $codeValueRepo = new CodeValueRepository();
        //check only for accident and death
        if ($incident->incident_type_id != 2) {
            //Employee is registered.
            //Check : Out of time (Exceeding 12 months after the date of incident) | Receipt date - Incident date > 12 months
            $incident_date = Carbon::parse($incident->incident_date);
            $receipt_date = Carbon::parse($incident->receipt_date);
            $dif = $incident_date->diffInDays($receipt_date, false);
            if ($dif > sysdefs()->data()->max_allowed_reporting_notification * 30.417) {
                //Notification Rejection by System | Out of time (Exceeding 12 months after the date of incident)
                $incident->notification_staging_cv_id = $codeValueRepo->NSNREJSYOT();
                $return = false;
            }
            //Check : Before the statutory period (WCF Start Date) | Incident date < WCF Statutory Period
            $wcf_start_date = Carbon::parse(getWCFStartDate());
            $dif = $wcf_start_date->diffInDays($incident_date, false);
            if ($dif < 0) {
                //Notification Rejection by System | Before the start of the period (WCF Statutory Period)
                $incident->notification_staging_cv_id = $codeValueRepo->NSNREJSYBS();
                $return = false;
            }
            $incident->save();
        }
        return $return;
    }

    /**
     * @param Model $incident
     * @return bool
     * @description Check if contribution exist in a month before the incident date. Determines whether the notification process should continue or move to compliance for contribution registration.
     */
    public function checkContributionStatus(Model $incident)
    {
        $return = false;
        $codeValueRepo = new CodeValueRepository();
        ///Check for contribution (if no contribution, prepare for the partial registration no contribution)
        $earning = $this->employees->getMonthlyEarningBeforeIncident($incident->employee_id, $incident->id);
        $incident->monthly_earning = $earning['monthly_earning'];
        $incident->contrib_month = $earning['contrib_month'];
        if (is_null($earning['monthly_earning'])) {
            //Pending Partial Registration (Missing Contribution)
            $incident->notification_staging_cv_id = $codeValueRepo->NSPPRMC();
        } else {
            //Pending Complete Registration (Filling Checklist)
            $incident->contrib_before = 1;
            $incident->notification_staging_cv_id = $codeValueRepo->NSPCRFC();
            $return = true;
        }
        $incident->save();
        $this->updateContributionStatus($incident);
        return $return;
    }

    /**
     * @param Model $incident
     * @return bool
     */
    public function updateContributionStatus(Model $incident)
    {
        //Update contrib_before & contrib_on on notification report by considering $input['incident_date']
        $contribRepo = new ContributionRepository();
        //check if contributed a month before
        $query = $contribRepo->findIfContributionExistByEmployee($incident->employee_id, $incident->employer_id, $incident->incident_date);
        if ($query) {
            $incident->contrib_on = 1;
            $incident->save();
        }
        $previous_contrib_month = Carbon::parse($incident->incident_date)->subMonth(1);
        $query = $contribRepo->findIfContributionExistByEmployee($incident->employee_id, $incident->employer_id, $previous_contrib_month);
        if ($query) {
            $incident->contrib_before = 1;
            $incident->save();
        }
        return true;
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function updateProgressive(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $codeValueRepo = new CodeValueRepository();
            $stage_comments = NULL;
            $out_of_time = false;

            $employee_id = 0;

            if ($incident->employee_id) {
                $employee_id = $incident->employee_id;
            }

            if (!$employee_id) {
                //Employee is not registered.
                //Check if there is some other incident with the same employee name
                $check = $this->query()->whereRaw("cast(employee_name as text)  ~* ?", [$input['employee_name']])->where("incident_date", $input['incident_date'])->where("id", "<>", $incident->id)->count();
                if ($check) {
                    throw new GeneralException("Update Failed, there is already has an active notification registered with the same employee name and same incident date.");
                }
            } else {
                //Employee is registered.
                //check if there is an already active notification
                $check = $this->query()->where("employee_id", $employee_id)->where("id", "<>", $incident->id)->where(function($query) use ($input) {
                    $query->where("incident_date", $input['incident_date']);
                })->count();
                if ($check) {
                    throw new GeneralException("Update Failed, this employee already has an active notification registered with the same incident date.");
                }
            }

            $data = [
                'incident_date' => $input['incident_date'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],
                'district_id' => $input['district_id'],
                'user_id' => access()->user()->id
            ];

            $incident->update($data);

            if ($employee_id) {
                //Employee is registered
                $incident->employer_id = $input['employer_id'];
                ///Save Date Hired & employee information from employee_employer on incident if not filled when registering
                $this->storeDateHired($incident, $input);
                /*if (isset($input['date_hired']) And !empty($input['date_hired'])) {
                    //Store the date hire
                    $this->employees->storeDateHired($employee_id, $input['employer_id'], $input['date_hired']);
                }
                $datehired = $this->employees->getDateHired($employee_id, $input['employer_id']);
                $incident->datehired = $datehired;
                $incident->save();*/

                //Employee is registered.
                //Check : Out of time (Exceeding 12 months after the date of incident) | Receipt date - Incident date > 12 months
                $incident_date = Carbon::parse($input['incident_date']);
                $receipt_date = Carbon::parse($input['receipt_date']);
                $dif = $incident_date->diffInDays($receipt_date, false);
                //add exception for disease
                if ($incident->incident_type_id == 2) {
                    $dif = 0;
                }
                if ($dif < sysdefs()->data()->max_allowed_reporting_notification * 30.417) {
                    switch ($incident->notification_staging_cv_id) {
                        //case $codeValueRepo->NSOPRFLI():
                        /*case $codeValueRepo->NSPPRMC():
                            //--> check if there is an interrupting stage active then save next stage in reserve if the reserved stage is (Out of Time)
                            //On Progress (Re-Filling Investigation)
                            //Pending Partial Registration (Missing Contribution)
                            if ($incident->notification_staging_reserve_cv_id == $codeValueRepo->NSNREJSYOT()) {
                                $incident->notification_staging_reserve_cv_id = $codeValueRepo->NSPCRFC();
                            }
                            break;*/
                            default:
                            //--> else remove any Notification Rejection by System | Out of time (Exceeding 12 months after the date of incident) Stage
                            //--> set the filling of checklist stage the current stage is (Out of Time)
                            //$this->deleteRecentStage($incident);
                            if ($incident->notification_staging_cv_id == $codeValueRepo->NSNREJSYOT()) {
                                if ($incident->notification_staging_reserve_cv_id) {
                                    $incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                                    $incident->notification_staging_reserve_cv_id = NULL;
                                } else {
                                    $incident->notification_staging_cv_id = $codeValueRepo->NSPCRFC();
                                }
                            }
                            break;
                        }
                    } else {
                    //--> remove any Recent notification stage
                    //$this->deleteRecentStage($incident);
                    //--> update the current evaluated stage
                        $incident->notification_staging_reserve_cv_id = $incident->notification_staging_cv_id;
                        $incident->notification_staging_cv_id = $codeValueRepo->NSNREJSYOT();
                        $out_of_time = true;
                    }
                    $incident->save();
                //Check : Before the statutory period (WCF Start Date) | Incident date < WCF Statutory Period
                    $wcf_start_date = Carbon::parse(getWCFStartDate());
                    $dif = $wcf_start_date->diffInMonths($incident_date, false);
                    if ($dif >= 0) {
                    //check if there is an interrupting stage active then save next stage in reserve
                    //remove any Notification Rejection by System | Before the start of the period (WCF Statutory Period) stage
                        switch ($incident->notification_staging_cv_id) {
                        /*case $codeValueRepo->NSOPRFLI():
                        case $codeValueRepo->NSPPRMC():
                            //--> check if there is an interrupting stage active then save next stage in reserve if the reserved stage is Before the statutory period (WCF Start Date)
                            //On Progress (Re-Filling Investigation)
                            //Pending Partial Registration (Missing Contribution)
                            if ($incident->notification_staging_reserve_cv_id == $codeValueRepo->NSNREJSYBS()) {
                                $incident->notification_staging_reserve_cv_id = $codeValueRepo->NSPCRFC();
                            }
                            break;*/
                            default:
                            //--> else remove any Notification Rejection by System | Out of time (Exceeding 12 months after the date of incident) Stage
                            //--> set the filling of checklist stage the current stage is Before the statutory period (WCF Start Date)
                            //$this->deleteRecentStage($incident);
                            if ($incident->notification_staging_cv_id == $codeValueRepo->NSNREJSYBS()) {
                                if ($incident->notification_staging_reserve_cv_id) {
                                    $incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                                    $incident->notification_staging_reserve_cv_id = NULL;
                                } else {
                                    $incident->notification_staging_cv_id = $codeValueRepo->NSPCRFC();
                                }
                            }
                            break;
                        }
                    } else {
                    //--> remove any Recent notification stage
                    //$this->deleteRecentStage($incident);
                    //--> update the current evaluated stage
                        $incident->notification_staging_reserve_cv_id = $incident->notification_staging_cv_id;
                        $incident->notification_staging_cv_id = $codeValueRepo->NSNREJSYBS();
                        $out_of_time = true;
                    }
                    $incident->save();

                    if (!$out_of_time) {
                    ///Check for contribution (if no contribution, prepare for the partial registration no contribution)
                        $earning = $this->employees->getMonthlyEarningBeforeIncident($employee_id, $incident->id);
                        $incident->monthly_earning = $earning['monthly_earning'];
                        $incident->contrib_month = $earning['contrib_month'];
                        if (is_null($earning['monthly_earning'])) {
                        // Pending Partial Registration (Missing Contribution)
                            $this->interruptingUpdateNotificationStage($incident, $codeValueRepo->NSPPRMC());
                        } else {
                        // There is contribution
                        //If current stage is missing contribution
                            if (($incident->notification_staging_cv_id == $codeValueRepo->NSPPRMC()) Or ($incident->notification_staging_cv_id == $codeValueRepo->ONSPPRMCW())) {
                                if ($incident->notification_staging_cv_id == $codeValueRepo->ONSPPRMCW()) {
                                //Close interrupting workflow
                                    $this->closeInterruptingWorkflow($incident);
                                }
                                if ($incident->notification_staging_reserve_cv_id) {
                                    $incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                                    $incident->notification_staging_reserve_cv_id = NULL;
                                } else {
                                //Filling Checklist
                                    $incident->notification_staging_cv_id = $codeValueRepo->NSPCRFC();
                                }
                                $incident->save();
                            }
                        }
                        $this->updateContributionStatus($incident);
                    }

                } else {
                //Employee is not registered
                //This is the partial registration
                    $incident->employee_name = $input['employee_name'];
                    $incident->employer_name = $input['employer_name'];
                }
                $incident->save();

                if ($employee_id) {
                //Employee is registered
                ///Save Employer information
                    $employee = $this->employees->query()->where("id", $employee_id)->first();
                ///Save Next of Kin Information if present
                //Check if next of kin index is present
                    if (isset($input['next_of_kin']) And !empty($input['next_of_kin'])) {
                        $employee->next_of_kin = $input['next_of_kin'];
                        $employee->next_of_kin_phone = $input['next_of_kin_phone'];
                        $employee->save();
                    }
                }

                $incident->refresh();

            //update accident
                if ($input['incident_type_id'] == 1) {
                    $accident = $this->accidents->updateProgressive($incident, $input);
                }
            //update disease
                if ($input['incident_type_id'] == 2) {
                    $desease = $this->diseases->updateProgressive($incident, $input);
                }
            //update death
                if ($input['incident_type_id'] == 3) {
                    $death = $this->death->updateProgressive($incident, $input);
                }

            //Initialize the notification report
                $this->initialize($incident);

                return $incident;
            });
}

    /**
     * @param Model $incident
     * @return mixed
     */
    public function closeInterruptingWorkflow(Model $incident)
    {
        return DB::transaction(function () use ($incident) {
            //close most recent notification workflow
            $workflow = $incident->processes()->first();

            //update wf_done on notification workflow
            $workflow->wf_done_date = Carbon::now();
            $workflow->wf_done = 1;
            $workflow->save();
            //update wftracks status => 1, to close all pending wftracks
            $workflow->wfTracks()->update(['status' => 1]);

            //TODO: Write the code for closinginterruptingworkflow
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param $next_stage
     * @return bool
     * @description Interrupting Update, modify safely the notification stage, taking consideration of the current stage
     */
    public function interruptingUpdateNotificationStage(Model $incident, $next_stage)
    {
        $codeValueRepo = new CodeValueRepository();
        switch ($next_stage) {
            case $codeValueRepo->NSPPRMC():
                //Pending Partial Registration (Missing Contribution) [Interrupting by system on notification update/modification]
                // Check the current notification stage, and graceful update it.
            switch ($incident->notification_staging_cv_id) {
                    //case $codeValueRepo->NSNREJSYBS():
                case $codeValueRepo->NSOPRFLI():
                case $codeValueRepo->NSOPFRFLI():
                        //case $codeValueRepo->NSPPRMC():
                case $codeValueRepo->NSPCRFC():
                case $codeValueRepo->NSPCRFI():
                case $codeValueRepo->NSRINC():
                case $codeValueRepo->NSPCRFNFI():
                        //case $codeValueRepo->NSNREJSYOT():
                        //case $codeValueRepo->NSPPRUM():
                        //Notification Rejection by System (Before WCF Statutory Period)
                        //On Progress (Re-Filling Investigation)
                        //On Progress (Finished Re-Filling Investigation)
                        //Pending Partial Registration (Missing Contribution)
                        //Pending Complete Registration (Filling Checklist)
                        //Pending Complete Registration (Filling Investigation)
                        //Rejected Incident from Notification Approval
                        //Pending Complete Registration (Finished Filling Investigation)
                        //Notification Rejection by System (Out of Time)
                        //Pending Partial Registration (Unregistered Member)
                $this->reserveNotificationStage($incident, $next_stage);
                break;
            }
            break;
            case $codeValueRepo->ONSPPRICW():
            case $codeValueRepo->ONSPPRIEIFW():
                //On Pending Partial Registration (Incorrect Contribution Amount) Workflow
                //On Pending Partial Registration (Incorrect Employee Info) Workflow
                // Check the current notification stage, and graceful update it.
            switch ($incident->notification_staging_cv_id) {
                case $codeValueRepo->NSOPRFLI():
                case $codeValueRepo->NSOPFRFLI():
                case $codeValueRepo->NSPCRFC():
                case $codeValueRepo->NSPCRFI():
                case $codeValueRepo->NSRINC():
                case $codeValueRepo->NSPCRFNFI():
                case $codeValueRepo->NSPPRMC():
                        //Both
                        //On Progress (Re-Filling Investigation)
                        //On Progress (Finished Re-Filling Investigation)
                        //Pending Complete Registration (Filling Checklist)
                        //Pending Complete Registration (Filling Investigation)
                        //Rejected Incident from Notification Approval
                        //Pending Complete Registration (Finished Filling Investigation)
                        //Pending Partial Registration (Missing Contribution)
                        // Check the current notification stage, and graceful update it.
                $this->reserveNotificationStage($incident, $next_stage);
                break;
            }
            break;
            case $codeValueRepo->NSOPRFLI():
                //On Progress (Re-Filling Investigation)
                // Check the current notification stage, and graceful update it.
            switch ($incident->notification_staging_cv_id) {
                case $codeValueRepo->NSRINC():
                        //Rejected Incident from Notification Approval
                $this->reserveNotificationStage($incident, $next_stage);
                break;
            }
            break;
        }
        return true;
    }

    /**
     * @param Model $incident
     * @param $next_stage
     * @return bool
     */
    public function reserveNotificationStage(Model $incident, $next_stage)
    {
        //$checkerRepo = new CheckerRepository();
        //$codeValueRepo = new CodeValueRepository();
        //--> remove recent stage
        $this->deleteRecentStage($incident);
        //--> remove recent checker
        /*$checkerRepo->deleteRecently([
            'resource_id' => $incident->id,
            'checker_category_cv_id' => $codeValueRepo->CHCNOTN(),
        ]);*/
        $stage = $incident->notification_staging_cv_id;
        //--> update the stage on incident
        $incident->notification_staging_cv_id = $next_stage;
        //--> reserve the current stage
        $incident->notification_staging_reserve_cv_id = $stage;
        $incident->save();
        return true;
    }

    /**
     * @param Model $incident
     * @return bool
     * @description delete any recent notification stage if exists
     */
    public function deleteRecentStage(Model $incident)
    {
        $query = $incident->stages()->orderByDesc("notification_stages.id")->limit(1);
        if ($query->count()) {
            $id = $query->first()->pivot->id;
            $query->newPivotStatementForId($incident->notification_staging_cv_id)->where("notification_stages.id", $id)->update(['isdeleted' => 1]);
        }
        return true;
    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     */
    public function update($id, $input)
    {
        return DB::transaction(function () use ($id, $input) {
            $notification_report = $this->findOrThrowException($id);
            $notification_report->update([
                'incident_exposure_cv_id' => $input['incident_exposure_cv_id'],
                'body_part_injury_cv_id' => array_key_exists('body_part_injury_cv_id', $input) ? (($input['body_part_injury_cv_id']) ? $input['body_part_injury_cv_id'] : null ) : null,
                'initial_expense_member_type_id' => $input['initial_expense_member_type_id'] ,
                'initial_expense_resource_id' => $this->getResourceId($notification_report->employee_id,$input),
                'pay_through_insurance_id' => array_key_exists('pay_through_insurance_id', $input) ? (($input['pay_through_insurance_id']) ? $input['pay_through_insurance_id'] : null ) : null,
                'is_cash' =>array_key_exists('pay_method', $input) ? (($input['pay_method'] == 1) ? 1 : 0 ) : 0,
                'employer_id' => $input['employer_id'],
                'incident_date' => $input['incident_date'],
                'district_id' => $input['district_id'],
                'reporting_date' => $input['reporting_date'],
                'receipt_date' => $input['receipt_date'],

            ]);
            //update accident
            if ($notification_report->incident_type_id == 1) {
                $this->accidents->update($notification_report, $input, $this->incident_date, $this->reporting_date, $this->receipt_date);
            }
            //update disease
            if ($notification_report->incident_type_id == 2) {
                $this->diseases->update($notification_report, $input, $this->incident_date, $this->reporting_date, $this->receipt_date);
            }
            //update death
            if (in_array($notification_report->incident_type_id , [3,4,5])) {
                $this->death->update($notification_report, $input, $this->incident_date, $this->reporting_date, $this->receipt_date);
            }

            //        Store date hired
            $this->employees->storeDateHired($notification_report->employee_id, $input['employer_id'], $input['date_hired']);

            return $notification_report;
        });
    }

    /**
     * @param Model $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws GeneralException
     */
    public function profile(Model $incident)
    {
        if ($incident->isprogressive) {
            //This is the progressive notification
            if (!$incident->attended And ($incident->allocated == access()->id())) {
                return redirect()->route("backend.claim.notification_report.attend_profile", $incident->id);
            }
            $return = $this->progressiveProfile($incident);
        } else {
            //This is not a progressive notification report
            $return = $this->legacyProfile($incident);
        }
        return $return;
    }

    /**
     * @param Model $incident
     * @param int $caller Shows whether a method has been called by system. 0 - System, 1 - User in some other functionality (e.g User select to initiate Investigation from a notification profile page, this won't be a the automatically succession of stages but rather initiated by a user)
     * @param int $isupdate Show whether notification has been updated notification_staging_cv_id and reinitialized for the same stage.
     * @return bool
     * @throws GeneralException
     * @description Called after the change of notification report stage. Responsible for initializing Checker Inbox & Tasks. Allocated staff will be responsible to
     * 1. If Notification Rejection by System (Before the start of WCF), Reject the notification if has status (Notification Rejection by System) ->next: Checker Inbox & Tasks & allocate notification user
     * 2. If Notification Rejection by System (Out of Time), Reject the notification if has status (Notification Rejection by System) ->next: Checker Inbox & Tasks & allocate notification user
     * 3. If Pending Partial Registration (Unregistered Member), can start Pending Partial Registration (Unregistered Member) workflow ->next: Checker Inbox & Tasks & allocate notification user
     * 4. If Pending Partial Registration (Missing Contribution), can start Pending Partial Registration (Missing Contribution) workflow ->next: Checker Inbox & Tasks & allocate notification user
     * 5. If Pending Partial Registration (Incorrect Contribution Amount), can start Pending Partial Registration (Incorrect Contribution Amount) workflow **Incorrect Contribution if the contribution amount is abnormally high. ->next: Checker Inbox & Tasks & allocate notification user
     * 6. If Pending Complete Registration (Filling Checklist), fill the notification checklist ->next: Checker Inbox & Tasks & allocate notification user
     * 7. If Pending Complete Registration (Filling Investigation), initialize the filling of investigation, if it is the case for accident and it only require phone investigation, do not initialize investigation filling ->next: Checker Inbox & Tasks & allocate investigation user. For Physical Investigation, system will enforce upload of Pictures.
     * 8. If On Progress (Notification Approval) ->next: Checker Inbox & Tasks & allocate user for initiating notification approval.
     * 9. If On Progress (Incident Approved) ->next: Checker Inbox & Tasks & allocate user to progressively updating the notification & update the notification staging. Notification will be retrieved on demand.
     * 10. If Rejected Incident from Notification Approval ->next: Checker Inbox & Tasks & allocate notification user
     * 11. If On Progress (At least Payed)
     * 12. If Closed Incident
     * 13. If On Reassessment
     * initialize notification to workflow rejection|notification checklist
     */
    public function initialize(Model $incident, $caller = 0, $isupdate = 0): bool
    {
        $incident->refresh();
        $stage = $incident->notification_staging_cv_id;
        $codeValue = new CodeValueRepository();
        $checkerRepo = new CheckerRepository();
        $userRepo = new UserRepository();
        $user = 0;

        //--> Allocate Notification User
        $user = $this->retrieveNotificationAllocatedUser($incident);
        $prevStage = $this->retrievePreviousStage($incident);
        $prevComment = (($prevStage) ? $prevStage->{'currentStage'} : "Notification Incident Registration");
        $notify = 0;
        $comments = NULL;
        $process = 1;
        $closeChecker = 0;
        $priority = 0;
        $require_attend = 1;
        $shouldcount = 1;

        switch ($stage) {
            case $codeValue->NSNREJSYBS():
                //Notification Rejection by System (Before the WCF Statutory Period) | System Rejected Notification Workflow
            $comments = '{"previousStage":"Notification Incident Registration", "submittedDate":"' . now_date() . '", "currentStage":"Notification Rejected by System (Before WCF Statutory Period)", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for System Rejected Notification Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Notification Rejection by System (Before the start of WCF)", "incidentReview":"Review careful the incident date."}';
            $notify = 1;
            break;
            case $codeValue->NSNREJSYOT():
                //Notification Rejection by System (Out of Time) | System Rejected Notification Workflow
            $comments = '{"previousStage":"Notification Incident Registration", "submittedDate":"' . now_date() . '", "currentStage":"Notification Rejected by System (Out of Time)", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for System Rejected Notification Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Notification Rejection by System (Out of Time)", "incidentReview":"Review careful the incident date and receipt date."}';
            $notify = 1;
            break;
            case $codeValue->NSONNREJSYW():
                //On Notification Rejection by System Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Notification Rejection by System Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Rejected Incident Notification", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Notification Rejected by System, Waiting for Approval", "incidentReview":"-"}';
            break;
            case $codeValue->NSAPRDNREJSYW():
                //Approved (Rejection by System) Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Approved (Rejection by System) Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Rejected Incident Notification, Communicate to Employee and/or Employer", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Notification Rejection by System has been approved", "incidentReview":"-"}';
            break;
            case $codeValue->NSPPRUM():
                //Pending Partial Registration (Unregistered Member) | Unregistered Member Notification Workflow
            $comments = '{"previousStage":"Notification Incident Registration", "submittedDate":"' . now_date() . '", "currentStage":"Partial Registration (Unregistered Member)", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for Unregistered Member Notification Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Employee has not been registered in the system. This Incident will be regarded as partial registration.", "incidentReview":"Check for employee and employer information, once registered this incident will be regarded as full registration"}';
            $notify = 1;
            break;
            case $codeValue->ONSPPRUMW():
                //On Pending Partial Registration (Unregistered Member) Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Pending Partial Registration (Unregistered Member) Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Pending Complete Registration (Filling Checklist)", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Current stage for registration of Employee and/or Employer", "incidentReview":"-"}';
            break;
            case $codeValue->NSPPRMC():
                //Pending Partial Registration (Missing Contribution) | Missing Contribution Notification
            $comments = '{"previousStage":"Notification Incident Registration", "submittedDate":"' . now_date() . '", "currentStage":"Partial Registration (Missing Contribution)", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for Missing Contribution Notification Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Employee has missing contribution in the system. This Incident will be regarded as partial registration.", "incidentReview":"Check for employee contribution(s), once contributions are registered this incident will be regarded as full registration"}';
            $notify = 1;
            break;
            case $codeValue->ONSPPRMCW():
                //On Pending Partial Registration (Missing Contribution) Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Pending Partial Registration (Missing Contribution) Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Pending Complete Registration (Filling Checklist)", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Current stage for registration of employee\'s contribution", "incidentReview":"-"}';
            break;
            case $codeValue->ONSPPRICW():
                //On Pending Partial Registration (Incorrect Contribution Amount) Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Pending Partial Registration (Incorrect Contribution Amount) Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Pending Complete Registration (Filling Checklist)", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Current stage for rectification of employee\'s contribution", "incidentReview":"-"}';
            break;
            case $codeValue->ONSPPRIEIFW():
                //On Pending Partial Registration (Incorrect Employee Info) Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Pending Partial Registration (Incorrect Employee Info) Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Pending Complete Registration (Filling Checklist)", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Current stage for the correction of employee information (Employee name and/or gender and/or date of birth)", "incidentReview":"-"}';
            break;
            case $codeValue->NSPCRFC():
                //Pending Complete Registration (Filling Checklist)
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Complete Registration (Filling Checklist)", "currentStageCvId":' . $stage . ', "nextStage":"Filling Investigation Feedback", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been completely registered.", "incidentReview":"Fill the notification checklist and after finishing click on the button Complete"}';
            break;
            case $codeValue->NSPCRFI():
                //Pending Complete Registration (Filling Investigation)
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Complete Filling of Investigation Feedback", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for Notification Approval Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been completely registered. Finish the investigation to aid the notification approval process", "incidentReview":"Fill the investigation feedback and after finishing click on the button Complete"}';
                //--> Allocate Investigation User
            $userArray = $userRepo->allocateForNotificationInvestigation($incident->id, $caller);
            //dd($userArray);
            if (count($userArray)) {
                $user = $userArray[0];
                    ///--> Create & Set for investigator feedback
                $this->setInvestigatorFeedbacks($incident);
            } else {
                    //Close recent checker entry
                $closeChecker = 1;
            }
            break;
            case $codeValue->NSPCRFNFI():
                //Pending Complete Registration (Finished Filling Investigation)
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Pending Complete Registration (Finished Filling Investigation)", "currentStageCvId":' . $stage . ', "nextStage":"Notification Approval Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"-", "incidentReview":"-"}';
            break;
            case $codeValue->NSOPNAP():
                //On Progress (Notification Approval Workflow)
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Progress (Notification Approval Workflow)", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for MAE or Benefit Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been completely registered", "incidentReview":"Review the assessment made during notification approval.."}';
            break;
            case $codeValue->NSOTRTDEWRK():
                //On Approve Notification Transfer to Death Workflow
                $closeChecker = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Transferring to Death Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for Benefit Workflow (Survivor, MAE, TD)", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "incidentReview":""}';
                break;
            case $codeValue->NSOPIA():
                //On Progress (Notification Approved)
            $priority = 0;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Progress (Notification Approved)", "currentStageCvId":' . $stage . ', "nextStage":"Initiate Benefit Workflow And on Demand Update of Medical Care", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has already been approved to Claim.", "incidentReview":"Update the medical care and followups on employee health state periodically."}';
            break;
            case $codeValue->NSOPRFLI():
                //On Progress (Re-Filling Investigation)
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Progress (Re-Filling Investigation)", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for Notification Approval Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Finish the investigation to aid the notification approval process", "incidentReview":"Fill the investigation feedback and after finishing click on the button Complete"}';
                //--> Allocate Investigation User
            $userArray = $userRepo->allocateForNotificationInvestigation($incident->id, $caller);
            if (count($userArray)) {
                $user = $userArray[0];
                    ///--> Create & Set for investigator feedback
                $this->setInvestigatorFeedbacks($incident);
            } else {
                    //Close recent checker entry
                $closeChecker = 1;
            }
            break;
            case $codeValue->NSOPFRFLI():
                //On Progress (Finished Re-Filling Investigation)
            $require_attend = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Progress (Finished Re-Filling Investigation)", "currentStageCvId":' . $stage . ', "nextStage":"Notification Approval Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"-", "incidentReview":"-"}';
            break;
            case $codeValue->NSRINC():
                //Rejected Incident from Notification Approval
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Rejected Incident from Notification Approval", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for Rejection from Notification Approval Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been rejected during Notification Incident Approval Workflow", "incidentReview":"Check for the rejection reason in the recent workflow and arrange to initiate the rejection workflow."}';
            $notify = 1;
            break;
            case $codeValue->ONSRINCW():
                //On Rejected Incident from Notification Approval Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Rejected Incident from Notification Approval Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Rejected Incident Notification", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Notification Rejected by from Approval", "incidentReview":"-"}';
            break;
            case $codeValue->APRDNSRINCW():
            /* Approved Rejection from Notification Approval Workflow */
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Approved Rejection from Notification Approval Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Rejected Incident Notification, Communicate to Employee and/or Employer", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Notification Rejection from Approval has been approved", "incidentReview":"-"}';
            break;
            case $codeValue->NSOCBW():
                ///==> On Claim Benefit Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Claim Benefit Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for MAE or Another Benefit Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been completely registered", "incidentReview":"Review the assessment made during notification approval."}';
            break;
            case $codeValue->NSOPALP():
                //==> On Progress (At least Payed)
            $priority = 0;
            $shouldcount = 0;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Progress (At least Payed)", "currentStageCvId":' . $stage . ', "nextStage":"Initiate MAE, Benefit Processing Workflow and on demand update the medical care", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has already been approved to Claim.", "incidentReview":"Update the medical care and followups on employee health state periodically."}';
            break;
            case $codeValue->NSCINC():
                //==> Closed Incident
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Closed Incident", "currentStageCvId":' . $stage . ', "nextStage":"Closed Incident", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been completely registered", "incidentReview":"Incident has been closed. It will be opened for reassessment"}';
            break;
            case $codeValue->NSCSYSREJ():
                //==> Closed Incident
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Closed Rejected Incident By System", "currentStageCvId":' . $stage . ', "nextStage":"Closed Incident", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been completely registered", "incidentReview":"Incident has been closed. It will be opened for reconsideration"}';
            break;
            case $codeValue->NSCAPPRREJ():
                //==> Closed Incident
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Closed Rejected Incident from Notification Approval", "currentStageCvId":' . $stage . ', "nextStage":"Closed Incident", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been completely registered", "incidentReview":"Incident has been closed. It will be opened for reconsideration"}';
            break;
            case $codeValue->NSRASSM():
                //==> On Reassessment
            $priority = 0;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Reassessment", "currentStageCvId":' . $stage . ', "nextStage":"Initiate MAE, Benefit Processing Workflow and on demand update the medical care", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification had already been approved to Claim. It has been opened for reassessment.", "incidentReview":"Update the medical care and followups on employee health state periodically."}';
            break;
            case $codeValue->NSATLPMAN():
                //==> At least Paid Manually
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"At least Paid Manually", "currentStageCvId":' . $stage . ', "nextStage":"Probably Closing File", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification had already been paid manually.", "incidentReview":"File Paid Manually"}';
            break;
            case $codeValue->NCOVDNOTWRK():
                //==> On Void Notification Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Void Notification Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Void Notification", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Notification is to be voided", "incidentReview":"-"}';
            break;
            case $codeValue->NCVOIDNOTFCN():
                //==> Void Notification
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"At least Paid Manually", "currentStageCvId":' . $stage . ', "nextStage":"Voided Notification", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"This notification has been voided", "incidentReview":"Void Notification"}';
            break;
            case $codeValue->NRONUNDORJWRK():
                //=>On Undo Rejection Workflow
            $closeChecker = 1;
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Undo Rejection Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Notification Approval", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Notification need to be re-approved", "incidentReview":"Notification needs re-assessment"}';
            break;
            case $codeValue->NSARCHIVED():
                //Incident Archived
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Notification Archived", "currentStageCvId":' . $stage . ', "nextStage":"Benefit Payment Process", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Unarchived incident for further benefit process ", "incidentReview":"Needs to be Unarchived."}';
            break;
            case $codeValue->NSARCHVDNDROPN():
                //Archived, Needs Re-open
            $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Notification Archived, Needs Re-open", "currentStageCvId":' . $stage . ', "nextStage":"Benefit Payment Process", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"Unarchived incident for further benefit process ", "incidentReview":"Needs to be Unarchived."}';
            $notify = 1;
            break;
        }
        logger($stage);

        if ($process And $user) {
            if (!empty($comments)) {
                //Save comment on employer inspection task stages
                $incident->stages()->attach($stage,
                    [
                        'comments' => $comments,
                        'require_attend' => $require_attend,
                    ]);
                $this->updateNotificationStageId($incident);
            }
            if ($closeChecker) {
                //Close recent checker entry
                $input = [
                    'resource_id' => $incident->id,
                    'checker_category_cv_id' => $codeValue->CHCNOTN(),
                ];
                $checkerRepo->closeRecently($input);
            } else {
                //Create checker entry
                $input = [
                    'comments' => $comments,
                    'user_id' => $user,
                    'checker_category_cv_id' => $codeValue->CHCNOTN(),
                    'priority' => $priority,
                    'notify' => $notify,
                    'shouldcount' => $shouldcount,
                ];
                $checkerRepo->create($incident, $input);
            }
        }

        if ($incident->notification_staging_reserve_cv_id) {
            $incident->notification_staging_cv_id = $stage;
            $incident->save();
        }

        if ($user) {
            //TODO: Send Socket Notification to the user about the new allocated notification incident or assigned investigation

        }
        return true;
    }

    /**
     * @param Model $incident
     */
    public function updateNotificationStageId(Model $incident)
    {
        $notification_stage_id = $incident->stages()->orderByDesc("notification_stages.id")->limit(1)->first()->pivot->id;
        $incident->notification_stage_id = $notification_stage_id;
        $incident->save();
    }

    /**
     * @return mixed
     */
    public function getResourceAllocationQuery()
    {
        $closedIncident = (new CodeValueRepository())->NSCINC();
        $allocation = $this->query()
        ->leftJoin("employees", "notification_reports.employee_id", "=", "employees.id")
        ->leftJoin("employers", "notification_reports.employer_id", "=", "employers.id")
        ->join("incident_types", "notification_reports.incident_type_id", "=", "incident_types.id")
        ->leftJoin("districts", "notification_reports.district_id", "=", "districts.id")
        ->leftJoin("regions", "districts.region_id", "=", "regions.id")
        ->leftJoin("notification_investigators", function ($join) {
            $join->on("notification_reports.id", "=", "notification_investigators.notification_report_id");
            $join->where("notification_investigators.iscomplete", "=", "0");
        })
        ->leftJoin(DB::raw('users a'), "notification_investigators.user_id", "=", "a.id")
        ->leftJoin(DB::raw('users b'), "notification_reports.allocated", "=", "b.id")
        ->join("code_values", "notification_reports.notification_staging_cv_id", "=", "code_values.id")
        ->where("isprogressive", 1)
        ->where("notification_staging_cv_id", "<>", $closedIncident);
        return $allocation;
    }

    public function getResourceAllocationSelectQuery()
    {
        $codeValue = new CodeValueRepository();
        //$onInvestigation = (new CodeValueRepository())->NSPCRFI();
        $onInvestigation = "({$codeValue->NSPCRFI()}, {$codeValue->NSOPRFLI()})";
        return [
            DB::raw("notification_reports.id as id"),
            DB::raw("notification_reports.filename"),
            DB::raw("notification_reports.attended"),
            DB::raw("notification_reports.receipt_date"),
            DB::raw("notification_reports.created_at"),
            DB::raw("notification_reports.employer_id"),
            DB::raw("districts.name as district"),
            DB::raw("districts.id as district_id"),
            DB::raw("regions.name region"),
            DB::raw("notification_reports.notification_staging_cv_id"),
            DB::raw("case when notification_reports.employee_id is null then notification_reports.employee_name else concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) end as employee"),
            DB::raw("case when notification_reports.employer_id is null then notification_reports.employer_name else employers.name end as employer"),
            //DB::raw("case when b.id is null then concat_ws(' ', a.firstname, coalesce(a.middlename, ''), a.lastname) else concat_ws(' ', b.firstname, coalesce(b.middlename, ''), b.lastname) end as staff"),
            DB::raw("incident_types.name as incident"),
            DB::raw("case when notification_staging_cv_id in {$onInvestigation} then case when a.id is null then '-' else concat_ws(' ', a.firstname, a.lastname) end else case when b.id is null then '-' else concat_ws(' ', b.firstname, b.lastname) end  end as allocated"),
            DB::raw("code_values.name as stage"),
            DB::raw("case when notification_staging_cv_id in {$onInvestigation} then 'Notification Investigation' else 'Notification Checklist' end as category"),
            DB::raw("case when notification_staging_cv_id in {$onInvestigation} then case when notification_investigators.attended = 1 then 'Yes' else 'No' end else case when notification_reports.attended = 1 then 'Yes' else 'No' end  end as attended_status"),
        ];
    }

    public function getResourceAllocationSelectEmployerGroupQuery()
    {
        return [
            DB::raw("notification_reports.employer_id"),
            DB::raw("employers.name"),
            DB::raw('COUNT(notification_reports.id) as employers'),
        ];
    }

    /**
     * @param $category
     * @return array
     */
    public function getResourceAllocationSelectStaffGroupQuery($category)
    {
        $return = [];
        switch ($category) {
            case '0':
                //Notification Checklist
            $return = [
                    //DB::raw("b.id"),
                DB::raw("concat_ws(' ', b.firstname, b.lastname) staff"),
            ];
            break;
            case '1':
                //Notification Investigation
            $return = [
                    //DB::raw("a.id"),
                DB::raw("concat_ws(' ', a.firstname, a.lastname) staff"),
            ];
            break;
            default:
                //Notification Checklist
            $return = [
                    //DB::raw("b.id"),
                DB::raw("concat_ws(' ', b.firstname, b.lastname) staff"),
            ];
            break;
        }
        $return[] = DB::raw('COUNT(notification_reports.id) as notifications');
        return $return;
    }

    /**
     * @return mixed
     */
    public function getResourceAllocationQueryFilter()
    {
        $codeValue = new CodeValueRepository();
        $allocation = $this->getResourceAllocationQuery();
        $category = request()->input('category');
        switch ($category) {
            case '0':
                //Notification Checklist
            $allocation->whereNotIn("notification_staging_cv_id", [$codeValue->NSPCRFI(), $codeValue->NSOPRFLI()]);
            break;
            case '1':
                //Notification Investigation
            $allocation->whereIn("notification_staging_cv_id", [$codeValue->NSPCRFI(), $codeValue->NSOPRFLI()]);
            break;
            default:
                //All Allocation
                //$allocation = $this->getResourceAllocationQuery();
            break;
        }

        $incident = (int) request()->input("incident");
        if ($incident) {
            $allocation->where("notification_reports.incident_type_id", "=", $incident);
        }
        $employee = request()->input("employee");
        if ($employee) {
            $allocation->where("notification_reports.employee_id", "=", $employee);
        }
        $employer = request()->input("employer");
        if ($employer) {
            $allocation->where("notification_reports.employer_id", "=", $employer);
        }
        $region = request()->input("region");
        if ($region) {
            $allocation->where("regions.id", "=", $region);
        }
        $district = request()->input("district");
        if ($district) {
            $allocation->where("districts.id", "=", $district);
        }
        $stage = request()->input("stage");
        if ($stage) {
            $allocation->where("notification_reports.notification_staging_cv_id", "=", $stage);
        }
        $status = request()->input('status');
        //check allocation status
        switch ($status) {
            case 1:
                //Allocated
            switch ($category) {
                case '0':
                        //Notification Checklist
                $allocation->whereNotNull("b.id");
                break;
                case '1':
                        //Notification Investigation
                $allocation->whereNotNull("a.id");
                break;
                default:
                        //All Allocation
                $allocation->where(function ($query) {
                    $query->whereNotNull("b.id")->orWhereNotNull("a.id");
                });
                break;
            }
            break;
            case 2:
                //Unallocated
            switch ($category) {
                case '0':
                        //Notification Checklist
                $allocation->whereNull("b.id");
                break;
                case '1':
                        //Notification Investigation
                $allocation->whereNull("a.id");
                break;
                default:
                        //All Allocation
                $allocation->where(function ($query) {
                    $query->whereNull("b.id")->whereNull("a.id");
                });
                break;
            }
            break;
            case 3:
                //Allocated to User
            $user_id = request()->input('user_id');
            switch ($category) {
                case '0':
                        //Notification Checklist
                $allocation->where("b.id", $user_id);
                break;
                case '1':
                        //Notification Investigation
                $allocation->where("a.id", $user_id);
                break;
                default:
                        //All Allocation
                $allocation->where(function ($query) use ($user_id) {
                    $query->where("b.id", $user_id)->orWhere("a.id", $user_id);
                });
                break;
            }
            break;
            case 4:
                //Attended (Allocated)
            switch ($category) {
                case '0':
                        //Notification Checklist
                $allocation->whereNotNull("b.id")->where("notification_reports.attended", 1);
                break;
                case '1':
                        //Notification Investigation
                $allocation->whereNotNull("a.id")->where("notification_investigators.attended", 1);
                break;
                default:
                        //All Allocation
                $allocation->where(function ($query) {
                    $query->whereNotNull("b.id")->orWhereNotNull("a.id");
                })->where(function ($query) {
                    $query->where("notification_reports.attended", 1)->orWhere("notification_investigators.attended", 1);
                });
                break;
            }
            break;
            case 5:
                //Unattended (Allocated)
            switch ($category) {
                case '0':
                        //Notification Checklist
                $allocation->whereNotNull("b.id")->where("notification_reports.attended", 0);
                break;
                case '1':
                        //Notification Investigation
                $allocation->whereNotNull("a.id")->where("notification_investigators.attended", 0);
                break;
                default:
                        //All Allocation
                $allocation->where(function ($query) {
                    $query->whereNotNull("b.id")->orWhereNotNull("a.id");
                })->where(function ($query) {
                    $query->where("notification_reports.attended", 0)->orWhere("notification_investigators.attended", 0);
                });
                break;
            }
            break;
        }
        return $allocation;
    }

    /**
     * @return mixed
     */
    public function getResourceAllocationDatatable()
    {
        $allocation = $this->getResourceAllocationQueryFilter()->select($this->getResourceAllocationSelectQuery());
        $datatables = app('datatables')
        ->of($allocation);
        return $datatables;
    }

    /**
     * @return mixed
     */
    public function getResourceAllocationEmployerGroup()
    {
        $allocation = $this->getResourceAllocationQueryFilter()
        ->whereNotNull("notification_reports.employer_id")
        ->select($this->getResourceAllocationSelectEmployerGroupQuery())
        ->groupBy('notification_reports.employer_id', 'employers.name')
        ->orderBy(DB::raw('COUNT(notification_reports.id)'), "desc");
        return $allocation;
    }

    public function getResourceAllocationStaffGroup()
    {
        $category = request()->input('category');
        $allocation = $this->getResourceAllocationQueryFilter()
        ->select($this->getResourceAllocationSelectStaffGroupQuery($category));
        switch ($category) {
            case '0':
                //Notification Checklist
                //$allocation->groupBy("b.id", "concat_ws(' ', b.firstname, coalesce(b.middlename, ''), b.lastname)");
            $allocation->groupBy(DB::raw("concat_ws(' ', b.firstname, b.lastname)"));
            break;
            case '1':
                //Notification Investigation
                //$allocation->groupBy("a.id", "concat_ws(' ', a.firstname, coalesce(a.middlename, ''), a.lastname)");
            $allocation->groupBy(DB::raw("concat_ws(' ', a.firstname, a.lastname)"));
            break;
            default:
                //All Allocation
                //Notification Checklist
                //$allocation->groupBy("b.id", "concat_ws(' ', b.firstname, coalesce(b.middlename, ''), b.lastname)");
            $allocation->groupBy(DB::raw("concat_ws(' ', b.firstname, b.lastname)"));
            break;
        }
        $allocation->orderBy(DB::raw('COUNT(notification_reports.id)'), "desc");
        return $allocation;
    }

    /**
     * @return array
     */
    public function getResourceStatus()
    {
        //suffix _nc means notification checklist
        //suffix _ni means notification investigation
        $codeValue = new CodeValueRepository();
        $output = [];
        $allocation = $this->getResourceAllocationQuery();
        $onInvestigation = [$codeValue->NSPCRFI(), $codeValue->NSOPRFLI()];
        //$onInvestigation = (new CodeValueRepository())->NSPCRFI();
        $output['available_nc'] = with(clone $allocation)->whereNotIn("notification_staging_cv_id", $onInvestigation)->count();
        $output['allocated_nc'] = with(clone $allocation)->whereNotIn("notification_staging_cv_id", $onInvestigation)->whereNotNull("b.id")->count();
        $output['unallocated_nc'] = with(clone $allocation)->whereNotIn("notification_staging_cv_id", $onInvestigation)->whereNull("b.id")->count();
        $output['attended_nc'] = with(clone $allocation)->whereNotIn("notification_staging_cv_id", $onInvestigation)->whereNotNull("b.id")->where("notification_reports.attended", 1)->count();
        $output['unattended_nc'] = with(clone $allocation)->whereNotIn("notification_staging_cv_id", $onInvestigation)->whereNotNull("b.id")->where("notification_reports.attended", 0)->count();
        $output['available_ni'] = with(clone $allocation)->whereIn("notification_staging_cv_id", $onInvestigation)->count();
        $output['allocated_ni'] = with(clone $allocation)->whereIn("notification_staging_cv_id", $onInvestigation)->whereNotNull("a.id")->count();
        $output['unallocated_ni'] = with(clone $allocation)->whereIn("notification_staging_cv_id", $onInvestigation)->whereNull("a.id")->count();
        $output['attended_ni'] = with(clone $allocation)->whereIn("notification_staging_cv_id", $onInvestigation)->whereNotNull("a.id")->where("notification_investigators.attended", 1)->count();
        $output['unattended_ni'] = with(clone $allocation)->whereIn("notification_staging_cv_id", $onInvestigation)->whereNotNull("a.id")->where("notification_investigators.attended", 0)->count();
        return $output;
    }

    /**
     * @param array $input
     * @return mixed
     * @throws WorkflowException
     */
    public function assignAllocation(array $input)
    {
        return DB::transaction(function () use ($input) {

            //logger($input);

            $codeValue = new CodeValueRepository();
            $NSPCRFI = $codeValue->NSPCRFI();
            $NSOPRFLI = $codeValue->NSOPRFLI();
            $category = ((isset($input['assign_for'])) && ($input['assign_for'] != '')) ? (int) $input['assign_for'] : -1;

            if (!(isset($input['id']) && count($input['id']))) {
                $allocation = $this->getResourceAllocationQueryFilter()->select($this->getResourceAllocationSelectQuery());
                $input['id'] = $allocation->pluck('id')->all();
                //$resources = $allocation->cursor();
            } else {
                //$resources = $this->query()->whereIn('id', $input['id'])->cursor();
            }

            /*if (isset($input['id']) && count($input['id'])) {*/

                $resources = $this->query()->whereIn('id', $input['id'])->cursor();

                if ($input['assign_status'] == 1) {
                    if (!$input['assigned_user']) {
                        throw new WorkflowException('The user to assign the incident has not been selected.');
                    }
                //Assign to User
                //$input['id'] => this is array of id e.g. [3,5,9]

                foreach ($resources as $resource) {
                    $olduser = $resource->allocated;
                    if ($category < 0) {
                        switch ($resource->notification_staging_cv_id) {
                            case $NSPCRFI:
                            case $NSOPRFLI:
                                $category = 1;
                                break;
                                default:
                                $category = 0;
                                break;
                            }
                        }
                        switch ($category) {
                            case 1:
                            ///-> Pending Complete Registration (Filling Investigation)
                            ///-> On Progress (Re-Filling Investigation)
                            //Assign for Notification investigation
                            $query = $resource->investigators()->where('iscomplete', 0)->orderByDesc('notification_investigators.id')->limit(1);
                            //^^ Previously was ->where("attended", 0)
                            if ($query->count()) {
                                //There is a user already allocated, just do replacement ...
                                $pivot = $query->first()->pivot;
                                if ($pivot->attended) {
                                    //throw new WorkflowException("This incident has already been attended.");
                                }
                                $user = $pivot->user_id;
                                $update = ['user_id' => $input['assigned_user'], 'allocate_status' => 0, 'iscomplete' => 0, 'updated_at' => DB::raw('now()')];

                                $query->newPivotStatementForId($user)->where('iscomplete', 0)->update($update);
                                //^^ Previously was ->where("attended", 0)
                                //update the respective checker
                                (new CheckerRepository())->update([
                                    'resource_id' => $resource->id,
                                    'checker_category_cv_id' => $codeValue->CHCNOTN(),
                                    'user_id' => $user,
                                    'replacer' => $input['assigned_user'],
                                    'status' => 0,
                                    'notification_staging_cv_id' => $resource->notification_staging_cv_id,
                                ]);
                            } else {
                                //No user had been allocated for investigation before.
                                $user = $input['assigned_user'];
                                /*$resource->investigators()->syncWithoutDetaching([$user => [
                                    'allocate_status' => 0,
                                    'recycle' => 1,
                                    'iscomplete' => 0,
                                    'attended' => 0,
                                ]]);*/
                                (new UserRepository())->createInvestigator($resource, [$user]);
                                $this->initialize($resource, 1);
                            }
                            break;
                            default:
                            //Assign for Notification Checklist
                            if ($resource->attended) {
                                //Temporary Disable Checking whether the incident has been attended ...
                                //throw new WorkflowException("This incident is already attended.");
                            }
                            if (!$resource->allocated) {
                                //Have never been allocated before
                                $user = $resource->allocated;
                                $resource->allocated = $input['assigned_user'];
                                $resource->allocate_status = 0;
                                $resource->save();
                                //Update the respective checker
                                (new CheckerRepository())->update([
                                    'resource_id' => $resource->id,
                                    'checker_category_cv_id' => $codeValue->CHCNOTN(),
                                    'user_id' => $user,
                                    'replacer' => $input['assigned_user'],
                                    'notification_staging_cv_id' => $resource->notification_staging_cv_id,
                                ]);
                            } else {
                                //Have never been allocated before
                                $resource->allocated = $input['assigned_user'];
                                $resource->allocate_status = 0;
                                $resource->save();
                                $this->initialize($resource, 1);
                            }

                            break;
                        }
                    }
                    //transfer workflows
                    if ($input['assigned_user']) {
                        (new WfTrackRepository())->transferChecklistAllocationWorkflows($resource, $olduser, $input['assigned_user']);
                    }
            } elseif ($input['assign_status'] == 2) {
                //Re-allocate Evenly
                    $category = $input['category'];
                    $codeValue = new CodeValueRepository();
                //$NSCINC = $codeValue->NSCINC();
                    $NSPCRFI = $codeValue->NSPCRFI();
                    $NSOPRFLI = $codeValue->NSOPRFLI();
                //logger('I am here ...');
                    /*Log::info($category);*/

                    /*if ($category == 0) {*/
                        if (true) {
                    //Notification Checklist
                    foreach ($resources as $resource) {
                        $user = (new UserRepository())->getAllocateForNotificationChecklist($resource, 1);
                        $olduser = $resource->allocated;
                        if ($user) {
                            //logger($user);
                                    switch ($resource->notification_staging_cv_id) {
                                        case $NSPCRFI:
                                        case $NSOPRFLI:
                                    ///-> Pending Complete Registration (Filling Investigation)
                                    ///-> On Progress (Re-Filling Investigation)
                                        $resource->allocated = $user;
                                        $resource->allocate_status = 0;
                                        $resource->save();
                                        break;
                                        default:
                                    //Notification Checklist
                                        $resource->allocated = $user;
                                        $resource->allocate_status = 0;
                                        $resource->save();
                                        $this->initialize($resource);
                                        break;
                                    }
                                }
                            }
                        } else {
                    //Not Notification Checklist
                            throw new WorkflowException('This option only apply when Allocation Category is set to Notification Checklist');
                        }
                    }
            /*} else {
                throw new WorkflowException('Please select at least one incident.');
            }*/

            return true;
        });
}

    /**
     * @param Model $incident
     * @return int|mixed
     *  @description Retrieve and Save allocated user for notification using Round-Robin
     */
    public function retrieveNotificationAllocatedUser(Model $incident)
    {
        if (is_null($incident->allocated)) {
            $user = (new UserRepository())->getAllocateForNotificationChecklist($incident);
            $incident->allocated = $user;
            $incident->save();
        } else {
            $user = $incident->allocated;
        }
        return $user;
    }

    /**
     * @param Model $incident
     * @return array|mixed
     */
    public function retrievePreviousStage(Model $incident)
    {
        $return = [];
        $query = $incident->stages()->orderBy("notification_stages.id", "desc");
        if ($query->count()) {
            $return = json_decode($query->first()->pivot->comments);
        }
        return $return;
    }

    /**
     * @param Model $incident
     * @return mixed
     */
    public function updateAttendedStatus(Model $incident)
    {
        return DB::transaction(function() use ($incident) {
            $incident->attended = 1;
            $incident->save();
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postUpdateDeathDetails(Model $incident, array $input)
    {
        return DB::transaction(function() use ($incident, $input) {
            switch ($incident->incident_type_id) {
                case 1:
                case 2:
                    //save json ...
                    $death_claim = json_encode($input);
                    $incident->death_claim = $death_claim;
                    $incident->save();
                    break;
                default:
                    $death = $this->death->saveChecklist($incident, $input);
                    break;
            }
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postUpdateDetails(Model $incident, array $input)
    {
        return DB::transaction(function() use ($incident, $input) {
            //update for notification
            $incident->paid_month_before = $input['paid_month_before'];
            $incident->monthly_earning_checklist = ($input['monthly_earning_checklist']) ? str_replace(",","", $input['monthly_earning_checklist']) : NULL;
            $incident->last_work_date = $input['last_work_date'] ?? NULL;
            $incident->datehired = $input['datehired'] ?? NULL;
            $incident->reporting_date = $input['reporting_date'] ?? NULL;
            $incident->has_checklist = 1;
            if (isset($input['district_id']) && $input['district_id']) {
                $incident->district_id = $input['district_id'];
            }
            $incident->save();
            //Save for specific checklist
            switch($incident->incident_type_id) {
                case 1:
                    //Accident
                $accident = $this->accidents->saveChecklist($incident, $input);
                if (isset($input['body_part_injury_id']) And ($input['body_part_injury_id'])) {
                    $incident->injuries()->sync($input['body_part_injury_id']);
                }
                break;
                case 2:
                    //Disease
                $disease = $this->diseases->saveChecklist($incident, $input);
                if (isset($input['body_part_injury_id']) And ($input['body_part_injury_id'])) {
                    $incident->injuries()->sync($input['body_part_injury_id']);
                }
                break;
                case 3:
                    //Death
                $death = $this->death->saveChecklist($incident, $input);
                break;
            }
            return $incident;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postUpdateChecklist(Model $incident, array $input)
    {
        return DB::transaction(function() use ($incident, $input) {

            if ($incident->incident_type_id != 3) {
                $special = [];
                /* start : collect all special keys */
                foreach ($input as $key => $value) {
                    if ( strpos( $key, 'health_provider_id' ) !== false ) {
                        $this_special = substr($key, 18);
                        if (!in_array($this_special, $special, true)) {
                            $special[] = $this_special;
                        }
                    }
                }
                /* end :  collect all special keys */
                $total_td = 0;
            }

            //update for notification
            $incident->paid_month_before = $input['paid_month_before'];
            $incident->monthly_earning_checklist = isset($input['monthly_earning_checklist']) ? remove_thousands_separator($input['monthly_earning_checklist']) : null;
            $incident->last_work_date = $input['last_work_date'] ?? NULL;
            $incident->datehired = $input['datehired'] ?? NULL;
            $incident->has_checklist = 1;
            $incident->save();
            //Save for specific checklist
            switch($incident->incident_type_id) {
                case 1:
                    //Accident
                $accident = $this->accidents->saveChecklist($incident, $input);
                $incident->injuries()->sync($input['body_part_injury_id']);
                break;
                case 2:
                    //Disease
                $disease = $this->diseases->saveChecklist($incident, $input);
                    $incident->injuries()->sync($input['body_part_injury_id']);
                break;
                case 3:
                    //Death
                $death = $this->death->saveChecklist($incident, $input);
                break;
            }

            if ($incident->incident_type_id != 3) {
                //Accident & Disease

                //save medical treatments

                /* start : clean current notification treatments */
                foreach ($incident->treatments as $treatment) {
                    $treatment->disabilities()->sync([]);
                    $treatment->practitioners()->sync([]);
                    $treatment->delete();
                }
                /* end : clean current notification treatments */

                /* start : loop through special variable */
                foreach ($special as $value) {
                    $data = ['member_type_id' => $input['member_type_id' . $value], 'insurance_id' => $input['insurance_id' . $value]];
                    $member_id = (new MemberTypeRepository())->getMemberTypeValue($incident->id, $data);
                    //save treatments records
                    $treatment = $incident->treatments()->create([
                        'health_provider_id' => $input['health_provider_id' . $value],
                        'initial_treatment_date' => $input['initial_treatment_date' . $value],
                        'last_treatment_date' => $input['last_treatment_date' . $value],
                        'has_ed' => $input['has_ed' . $value],
                        'has_hospitalization' => $input['has_hospitalization' . $value],
                        'has_ld' => $input['has_ld' . $value],
                        'member_type_id' => $input['member_type_id' . $value],
                        'member_id' => $member_id,
                    ]);
                    //save heath states
                    $health_state_checklists = (new HealthStateChecklistRepository())->query()->where("isactive", 1)->get();
                    $health_states = [];
                    foreach ($health_state_checklists as $health_state_checklist) {
                        $index = 'health_state' . $health_state_checklist->id . $value;
                        if (isset($input[$index])) {
                            $health_states[$health_state_checklist->id] = $input[$index];
                            $health_states["_" . $health_state_checklist->id] = $health_state_checklist->name;
                        }
                    }
                    //logger($health_states);
                    $treatment->health_states = json_encode($health_states);
                    $treatment->save();
                    //save treatment disabilities
                    if ($input['has_ed' . $value]) {
                        //Excuse from Duty/Day Off ==> 2
                        $treatment->disabilities()->syncWithoutDetaching([
                            2 => [
                                'days' => $input['excuse_from_duty_days' . $value],
                            ],
                        ]);
                        $total_td += $input['excuse_from_duty_days' . $value];
                    }
                    if ($input['has_hospitalization' . $value]) {
                        //Hospitalization ==> 1
                        $treatment->disabilities()->syncWithoutDetaching([
                            1 => [
                                'days' => $input['hospitalization_days' . $value],
                            ],
                        ]);
                        $total_td += $input['hospitalization_days' . $value];
                    }
                    if ($input['has_ld' . $value]) {
                        //Light Duties ==> 3
                        $treatment->disabilities()->syncWithoutDetaching([
                            3 => [
                                'days' => $input['light_duty_days' . $value],
                            ],
                        ]);
                        //$total_td += $input['light_duty_days' . $value];
                    }



                    //optionally save treatment medical practitioners
                    if (isset($input['medical_practitioner_id' . $value]) And ($input['medical_practitioner_id' . $value])) {
                        $treatment->practitioners()->sync($input['medical_practitioner_id' . $value]);
                    }
                }
                if ($input['return_to_work']) {
                    //check if total td + minimum initial treatment date < than return to work date
                    $min_initial_treatment_date = $incident->treatments()->min("initial_treatment_date");
                    //$total_td = $treatment->disabilities()->whereIn("notification_treatment_disabilities.disability_state_checklist_id", [1, 2])->sum("days");
                    $assumed_return_date = Carbon::parse($min_initial_treatment_date)->addDays($total_td);
                    $return_date = Carbon::parse($input['return_to_work_date']);
                    if ($assumed_return_date->greaterThan($return_date)) {
                        throw new WorkflowException("Return to work date can not be less than the least minimum initial treatment date plus total TD days!. Expected return to work date should at least be " . $assumed_return_date->format("d-M-Y") . ", Please Check....");
                    }
                }
            }

            //Compute Checklist Score
            $this->computeChecklistScore($incident);
            return true;
        });
}

    /**
     * @param Model $incident
     * @return int
     */
    public function computeChecklistScore(Model $incident)
    {
        $score = 0;
        if ($incident->incident_type_id == 1) {
            //Accident
            $accident = $incident->accident;
            //==> Body Part Injury
            $query = $incident->injuries();
            //If Has Body Part Injury
            if ($query->count()) {
                //Check if selected body part injuries listed in the predefined injuries for a score
                $idArray = $query->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $checkArray = (new BodyPartInjuryRepository())->getForChecklist();
                if (array_intersect($idArray, $checkArray)) {
                    $score += 1;
                }
            }
            //==> If has lost body part / function
            if ($accident->lost_body_part) {
                $score += 2;
            }
            //==> Get total hospitalization days
            $hospitalization_days = DB::table('notification_treatment_disabilities')
            ->join('notification_treatments', 'notification_treatments.id', '=', 'notification_treatment_disabilities.notification_treatment_id')
            ->where("disability_state_checklist_id", 1)
            ->where("notification_report_id", $incident->id)
            ->sum('days');
            $excuse_from_duty_days = DB::table('notification_treatment_disabilities')
            ->join('notification_treatments', 'notification_treatments.id', '=', 'notification_treatment_disabilities.notification_treatment_id')
            ->where("disability_state_checklist_id", 2)
            ->where("notification_report_id", $incident->id)
            ->sum('days');
            switch (true) {
                case ($hospitalization_days >= 1 And $hospitalization_days <= 7):
                $score += 1;
                break;
                case ($hospitalization_days >= 8 And $hospitalization_days <= 14):
                $score += 2;
                break;
                case ($hospitalization_days > 14):
                $score += 3;
                break;
            }
            switch (true) {
                case ($excuse_from_duty_days >= 1 And $excuse_from_duty_days <= 3):
                $score += 1;
                break;
                case ($excuse_from_duty_days >= 4 And $excuse_from_duty_days <= 14):
                $score += 2;
                break;
                case ($excuse_from_duty_days >= 15 And $excuse_from_duty_days <= 30):
                $score += 3;
                break;
                case ($excuse_from_duty_days > 30):
                $score += 4;
                break;
            }
            //==> If has returned to work
            if (!$incident->return_to_work) {
                $score += 1;
            }
        } else {
            //Other Incident (Physical Investigation is necessary)
            //Disease & Death
            $score += 10;
        }
        //Update Checklist Score
        if (!$incident->isverified) {
            //If notification has not already verified (checklist has not been completed)
            $incident->checklist_score = $score;
            $incident->save();
        }

        return $score;
    }

    /**
     * @param Model $incident
     * @return mixed
     * @throws GeneralException
     */
    public function completeChecklist(Model $incident)
    {
        $should_skip_notification_checklist = sysdefs()->data()->should_skip_notification_checklist;
        if (!$incident->has_checklist And !$should_skip_notification_checklist) {
            throw new GeneralException("Can not complete checklist, checklist has not been updated yet.");
        }
        return DB::transaction(function() use ($incident) {

            $score = $incident->checklist_score;
            $incident->isverified = 1;

            switch (true) {
                case ($score > 0 And $score <= 4):
                    //Phone Call Investigation ...
                $incident->need_investigation = 0;
                break;
                default:
                    //Physical Investigation ...
                $incident->need_investigation = 1;
                break;
            }

            $incident->progressive_stage = 2;

            //Pending Complete Registration (Filling Investigation)
            $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFI();

            if ($incident->investigation_validity) {

            } else {

            }

            //Generate Investigation Questions For Feedback
            //$this->generateInvestigationFeedbacks($incident);

            $incident->save();

            $caller = 0;
            if ($incident->istransfered And $incident->progressive_stage == 2 And $incident->investigators()->count()) {
                $caller = 1;
            }
            $this->initialize($incident, $caller);
            //update investigation type (Phone or Physical)
            //$this->updateInvestigator($incident, ['attended' => 0], ['type' => $type]);
            return true;
        });
    }

    /**
     * @param $id
     * @return mixed
     */
    public function investigate($id)
    {
        return DB::transaction(function () use ($id) {
            $notification_report = $this->findOrThrowException($id);
            $notification_report->update(['need_investigation' => 1]);
            $this->generateInvestigationFeedbacks($notification_report);
            return $notification_report;
        });
    }

    /**
     * @param $id
     * @return mixed
     */
    public function cancelInvestigation($id)
    {
        return DB::transaction(function () use ($id) {
            $notification_report = $this->findOrThrowException($id);
            $notification_report = $notification_report->update(['need_investigation' => 0]);
            $this->removeInvestigationFeedbacks($id);
            return $notification_report;
        });
    }

    /**
     * @param $id
     * @return mixed
     * investigation feedbacks
     */
    public function getInvestigationFeedbacks($id)
    {
        return $this->query()->find($id)->investigationFeedbacks;
    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     */
    public function updateInvestigationFeedbacks($id, $input, $employer_id)
    {
        // dd($input);
        return DB::transaction(function () use ($id, $input, $employer_id) {
            /*if (!$this->checkIfInvestigatorAssigned($id)) {
                throw new GeneralException(trans('exceptions.backend.claim.investigation_report_update_by_assigned_investigators'));
            }*/
            if (isset($input['w_names'])) {
                $workId = (int)$input['w_names'];
            }else{
                $workId = 0;
            }

            foreach ($input as $key => $value) {
                $feedback = false;
                $remarks = false;
                $disease_agent_id = false;
                $code_value_id = false;
                $code_id = false;
                $boolean_value = "";
                if (strpos($key, 'feedback') !== false) {
                    $feedback = true;
                    $investigation_feedback_id = substr($key, 8);
                } elseif (strpos($key, 'date') !== false) {
                    $feedback = true;
                    if (strpos($key, 'investigation_date') !== false) {
                        $investigation_feedback_id = substr($key, 18);
                    } elseif (strpos($key, 'incident_date') !== false) {
                        $investigation_feedback_id = substr($key, 13);
                    } else {
                        $investigation_feedback_id = substr($key, 4);
                    }
                } elseif (strpos($key, 'boolean') !== false) {
                    $feedback = true;
                    $investigation_feedback_id = substr($key, 7);

                }elseif (strpos($key, 'workplace') !== false) {
                    if ($value != null) {
                        $feedback = true;
                        $investigation_feedback_id = substr($key, 9);
                        //save work place id and name
                        if (isset($input['postcode'])) {
                            $postcode = isset($input['postcode']);

                            $work = DB::table('workplaces')
                            ->where('id',$workId)
                            ->first();
                            if (!empty($work)) {
                                $value = $work->workplace_name;

                            }else{
                                $exists = $this->workplaceExists($employer_id,$value);
                                if ($exists[0] == 1) {
                                    //ipo
                                    $value = $exists[1];
                                } else {
                                    //haipo
                                    //get ward
                                    $postcode = $input['postcode'];
                                    $ward_district = $this->getPostcodeDetails($postcode);

                                    //get district
                                    $workplace = DB::table('workplaces')
                                    ->insert([
                                        'workplace_name' => $value,
                                        'employer_id' => $employer_id,
                                        'ward' => $ward_district[0],
                                        'district' => $ward_district[1],
                                        'region' => $ward_district[2],
                                    ]);
                                    $workplace_id = DB::getPdo()->lastInsertId();
                                    $random = $this->randomDigits(3);
                                    $wrkId = $employer_id.'-'.$random.$workplace_id.'-'.$input['postcode'];
                                    $workplace = DB::table('workplaces')
                                    ->where('id',$workplace_id)
                                    ->update([
                                        'workplace_regno' => $wrkId,
                                    ]);

                                    //update workplace id in notification reports
                                    $not_repo = NotificationReport::find($id);
                                    $not_repo->workplace_id = $workplace_id;
                                    $not_repo->save();

                                }

                            }
                        }
                    }
                }
                elseif (strpos($key, 'target_organ') !== false) {
                    $feedback = true;
                    $investigation_feedback_id = substr($key, 12);
                    if ($value !== null) {
                        $spc_organ_id = $input['specific_organ'];
                        $spc_organ_name = $this->specificDisease($spc_organ_id);
                        $code_value_id = $value;
                        $disease_agent_id = $spc_organ_id;
                        $value = $spc_organ_name;

                    }

                }
                elseif (strpos($key, 'disease_agent') !== false) {
                    $feedback = true;
                    $investigation_feedback_id = substr($key, 13);
                    if ($value !== null) {
                        $spc_disease_id = $input['specific_agent'];
                        $spc_disease_name = $this->specificDisease($spc_disease_id);
                        $code_value_id = $value;
                        $disease_agent_id = $spc_disease_id;
                        $value = $spc_disease_name;
                    }

                }
                elseif (strpos($key, 'disease_type') !== false) {
                    $feedback = true;
                    $investigation_feedback_id = substr($key, 12);
                    $code_value_id = $value;
                    $value = $input['disease_name_type'];

                }
                elseif (strpos($key, 'accident_cause') !== false) {
                    $feedback = true;
                    $investigation_feedback_id = substr($key, 14);
                    if ($value !== null) {
                        $spc_accident_id = $input['specific_accident'];
                        $spc_accident_name = $input['accident_name_cause'];
                        $code_value_id = $spc_accident_id;
                        $code_id = $value;
                        $value = $spc_accident_name;
                    }

                } elseif (strpos($key, 'remarks') !== false) {
                    $remarks = true;
                    $investigation_feedback_id = substr($key, 7);
                }

                if ($feedback Or $remarks) {
                    $investigation_feedback = $this->investigation_feedbacks->query()->find($investigation_feedback_id);

                }
                if ($feedback) {
                    $investigation_feedback->update(['feedback' => $value]);
                    if (!is_null($investigation_feedback->parent_id)) {

                    }
                }
                if ($remarks) {
                    $investigation_feedback->update(['remarks' => $value]);
                }
                if ($code_value_id) {
                    $investigation_feedback->update(['code_value_id' => $code_value_id]);
                }
                if ($code_id) {
                    $investigation_feedback->update(['code_id' => $code_id]);
                }
                if ($disease_agent_id) {
                    $investigation_feedback->update(['disease_exposure_agent_id' => $disease_agent_id]);
                }

            }

            $investigation_feedback_childs = $this->investigation_feedbacks->query()->where(["notification_investigator_id" => $input['notification_investigator_id']])->whereNotNull("parent_id")->get();
            foreach ($investigation_feedback_childs as $investigation_feedback_child) {
                $investigation_feedback = $this->investigation_feedbacks->query()->where(["investigation_question_id" => $investigation_feedback_child->parent_id, "notification_investigator_id" => $input['notification_investigator_id']])->first();
                if (trim($investigation_feedback->feedback) == "No") {
                    $investigation_feedback_child->feedback = NULL;
                    $investigation_feedback_child->save();
                }
            }

            //update iscomplete on notification_investigators
            $incident = $this->find($id);
            $this->updateInvestigator($incident, ['attended' => 0], ['attended' => 1]);

            //To be done : Update only if filling investigation for the first time
            $this->updateInvestigationValidity($id);

            return true;
        });
}

public function getPostcodeDetails($postcode){
    $postcode = DB::table('postcodes')
    ->select('ward_name','district_id')
    ->where('postcode',$postcode)
    ->first();
    $post_details = array();
    $ward_name = $postcode->ward_name;
    $district_region = $this->getDistrictRegion($postcode->district_id);
    $post_details[0] = $ward_name;
    $post_details[1] = $district_region[0];
    $post_details[2] = $district_region[1];

    return $post_details;

}

public function getDistrictRegion($district_id){
    $district = DB::table('districts')
    ->find($district_id);
    $district_name = $district->name;

    $region = DB::table('regions')
    ->find($district->region_id);
    $region_name = $region->name;

    $district_region = array();
    $district_region[0] = $district_name;
    $district_region[1] = $region_name;

    return $district_region;
}

public function specificDisease($spc_id){
    $disease = DB::table('disease_exposure_agents')
    ->select('name')
    ->where('id',$spc_id)
    ->first();
    return $disease->name;
}

function randomDigits($length){
    $numbers = range(0,9);
    shuffle($numbers);
    for($i = 0; $i < $length; $i++){
        global $digits;
        $digits .= $numbers[$i];
    }
    return $digits;
}

public function workplaceExists($employer_id, $workplace){
    $postcodes = DB::table('workplaces')
    ->select('id','workplace_regno','workplace_name')
    ->get();

    $array = array();
    $array[0] = 0;
    foreach ($postcodes as $key => $value) {
        $post = explode('-', $value->workplace_regno);
        $employer = $post[0];
        $postc = $post[2];
        if ($employer == $employer_id && strtolower($workplace) == strtolower($value->workplace_name)) {
            $array[0] = 1;
            $array[1] = $value->workplace_name;
        }

    }
    return $array;
}

    /**
     * @param Model $incident
     * @param array $filter
     * @param array $update
     */
    public function updateInvestigator(Model $incident, array $filter, array $update)
    {
        $query = $incident->investigators()->orderByDesc("notification_investigators.id")->limit(1);
        if ($query->count()) {
            $pivot = $query->first()->pivot;
            $user = $pivot->user_id;
            $query->newPivotStatementForId($user)->where($filter)->update($update);
        }
    }

    /**
     * @param Model $incident
     * @return mixed
     */
    public function getAttendedInvestigatorStatus(Model $incident)
    {
        $query = $incident->investigators()->orderByDesc("notification_investigators.id")->limit(1);
        $pivot = $query->first()->pivot;
        $attended = $pivot->attended;
        return $attended;
    }

    /**
     * @param Model $incident
     * @return mixed
     */
    public function setInvestigatorFeedbacks(Model $incident)
    {
        return DB::transaction(function () use ($incident) {
            //Remove if there is any questions in the feedback table
            $this->deleteInvestigatorFeedbacks($incident);
            $notificationInvestigatorInstance = $incident->investigationStaffs()->where("attended", 0)->limit(1);
            if ($notificationInvestigatorInstance->count()) {
                $notificationInvestigator = $notificationInvestigatorInstance->first();
                $investigation_questions = $this->getInvestigationQuestions($incident);
                foreach ($investigation_questions as $investigation_question) {
                    $data = [
                        "notification_report_id" => $incident->id,
                        "investigation_question_id" => $investigation_question->id,
                        "parent_id" => $investigation_question->parent_id,
                        "notification_investigator_id" => $notificationInvestigator->id,
                    ];
                    $this->investigation_feedbacks->createForInvestigator($data);
                }
            } else {
                //for non-progressive notification report & investigation validity = 1 (used during transfering non-progressive to progressive)
                if ($incident->istransfered) {
                    $notificationInvestigatorInstance = $incident->investigationStaffs()->limit(1);
                    //dd($notificationInvestigatorInstance->limit(1)->count());
                    //if ($notificationInvestigatorInstance->count() == 1) {
                    if ($notificationInvestigatorInstance->count()) {
                        $notificationInvestigator = $notificationInvestigatorInstance->first();
                        $notificationInvestigator->iscomplete = 0;
                        $notificationInvestigator->save();
                        $this->investigation_feedbacks->query()->where(["notification_report_id" => $incident->id])->update(["notification_investigator_id" => $notificationInvestigator->id,]);
                    }
                }
            }

            return true;
        });
    }

    /**
     * Generate investigation feedbacks for this notification report if need investigation
     *
     * @param Model $incident
     * @return mixed
     */
    public function generateInvestigationFeedbacks(Model $incident)
    {
        return DB::transaction(function () use ($incident) {
            //Remove if there is any questions in the feedback table
            $this->removeInvestigationFeedbacks($incident->id);
            $investigation_questions = $this->getInvestigationQuestions($incident);
            foreach ($investigation_questions as $investigation_question) {
                $this->investigation_feedbacks->create($incident->id, $investigation_question->id, $investigation_question->parent_id);
            }
        });
    }

    /**
     * @param Model $incident
     * @return mixed
     */
    public function getInvestigationQuestions(Model $incident)
    {
        $investigation_questions = $this->investigation_questions->query()->select(["id", "parent_id"])->where("isactive", 1)->whereHas("incidentTypes", function ($query) use ($incident) {
            $query->where("incident_type_id", $incident->incident_type_id);
        })->orderBy('sort', 'asc')->orderBy('id', 'asc')->get();
        return $investigation_questions;
    }

    /**
     * @param $id
     */
    public function removeInvestigationFeedbacks($id)
    {
        $this->investigation_feedbacks->delete($id);
    }

    /**
     * @param Model $incident
     */
    public function deleteInvestigatorFeedbacks(Model $incident)
    {
        $this->investigation_feedbacks->deleteForInvestigator($incident);
    }

    /**
     * Check if is updated by Assigned Investigators or any of the substituting investigation user.
     *
     * @param $id
     * @return int
     */
    public function checkIfInvestigatorAssigned($id)
    {
        $count = 0;
        $notification_investigators = new NotificationInvestigatorRepository();
        $query = $notification_investigators->query()->select(["user_id"])->where('notification_report_id', $id)->orderByDesc("notification_investigators.id")->first();
        if ((in_array($query->user_id, access()->allUsers()))) {
            $count = 1;
        }
        return $count;
    }

    /**
     * @param $id
     * @throws GeneralException
     * Update Investigation Validity when Investigation report is updated
     */
    public function updateInvestigationValidity($id){

        $notification_report = $this->findOrThrowException($id);
        $investigation_feedback_null = $notification_report->investigationFeedbacks()->whereNull('feedback')->whereNull("parent_id")->count();
        if ($investigation_feedback_null) {
            $notification_report->update(['investigation_validity'=> 0]);
        } else {
            $notification_report->update(['investigation_validity'=> 1]);
        }
    }

    /**
     * @param Model $incident
     * @return mixed
     */
    public function completeInvestigationReport(Model $incident)
    {
        return DB::transaction(function () use ($incident) {
            //check if investigation has been attended by checking attended status in notification_investigators
            //if (!env('TESTING_MODE')) {

            if (!$this->getAttendedInvestigatorStatus($incident)) {
                throw new GeneralException("Can not complete investigation, investigation feedbacks has not been updated!");
            }
            //}
            //validate investigation
            $incident->investigation_validity = 1;
            $incident->save();
            //if (!env('TESTING_MODE')) {
            //update iscomplete on notification_investigators
            $this->updateInvestigator($incident, ['iscomplete' => 0], ['iscomplete' => 1]);
            //}
            //Update the staging and initialize incident
            $incident->progressive_stage = 3;
            //Pending Complete Registration (Filling Investigation)
            $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFNFI();
            $incident->save();

            $this->initialize($incident);

            (new InvestigationPlanRepository())->updateNotificationInvestigationPlanStatus($incident->id);
            return true;
        });
    }

    /**
     * @param $level
     * @param $resource_id
     * @return bool
     */
    public function checkIfHasMissingContributionForWorkflow($level, $resource_id, $module)
    {
        return DB::transaction(function () use ($level, $resource_id, $module) {
            $moduleRepo = new WfModuleRepository();
            $bool = false;
            switch ($module) {
                case $moduleRepo->incorrectContributionNotificationModule()[0]:
                case $moduleRepo->incorrectContributionNotificationModule()[1]:
                $bool = ((int)$level == $moduleRepo->incorrectContributionNotificationRegisterLevel($module));
                break;
                default:
                $bool = ((int)$level == $moduleRepo->missingContributionNotificationRegisterLevel($module));
                break;
            }
            if ($bool) {
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $incident = $workflow->notificationReport;
                if (is_null($incident->monthly_earning)) {
                    //Check if contribution has already been created through Gepg

                    $employee_id = $incident->employee_id;
                    $employer_id = $incident->employer_id;
                    $contrib_month = Carbon::parse($incident->incident_date)->subMonth()->format("Y-n-j");

                    $contribution = (new ContributionRepository())->getContribution($employee_id, $employer_id, $contrib_month)->first();

                    if ($contribution) {
                        //update contribution entry
                        $incident->monthly_earning = $contribution->grosspay;
                        $incident->contrib_month = $contribution->contrib_month;
                        $incident->contrib_before = 1;
                        $incident->save();
                    } else {
                        throw new WorkflowException("Contribution not registered!");
                    }

                }
            }
            return true;
        });
    }

    /**
     * @param $level
     * @param $resource_id
     * @return bool
     */
    public function checkIfHasRegisteredMemberForWorkflow($level, $resource_id, $module)
    {
        return DB::transaction(function () use ($level, $resource_id, $module) {
            $moduleRepo = new WfModuleRepository();
            if ((int)$level == $moduleRepo->unregisteredMemberNotificationCheckRegisterLevel($module)) {
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $incident = $workflow->notificationReport;
                if (is_null($incident->employee_id)) {
                    throw new WorkflowException("Employee information are not completely registered!");
                }
                if (is_null($incident->employer_id)) {
                    throw new WorkflowException("Employer information are not completely registered!");
                }
                //Check also for contribution...
                if (is_null($incident->monthly_earning)) {
                    throw new WorkflowException("Contribution not registered, please check!");
                }
            }
            return true;
        });
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function checkMaeRefundMemberWorkflow(array $input)
    {
        return DB::transaction(function () use ($input) {
            $moduleRepo = new WfModuleRepository();
            $return = true;
            $level = $input['level'];
            $resource_id = $input['resource_id'];
            $module = $input['wf_module_id'];
            $status = $input['status'];

            $jump_level = NULL;

            //Check if MAE has been assessed
            if ((int)$level == $moduleRepo->maeRefundMemberAssessmentLevel($module)) {
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $eligible = $workflow->eligibles()->first();
                $medicalExpense = $eligible->medicalExpense;
                if (is_null($medicalExpense->assessed_amount)) {
                    throw new WorkflowException("Assessment has not been done, please check!");
                }
            }
            //Check if MAE has Zero Assessment and Terminate ...
            if ((int)$level == $moduleRepo->maeRefundMemberTerminateLevel($module)) {
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $eligible = $workflow->eligibles()->first();
                $medicalExpense = $eligible->medicalExpense;
                if ($medicalExpense->assessed_amount == 0) {
                    $eligible->iszeropaid = 1;
                    $eligible->payprocesseddate = Carbon::now();
                    $eligible->save();
                    //$return = false;
                    //Complete the workflow process
                    //$this->completeBenefitApproval($resource_id, $module);
                    $jump_level = $moduleRepo->claimBenefitNotifyEmployerLevel($module);
                }
            }
            //if workflow is declined.
            if ($status == 3) {
                //Declined...
                $return = false;
                //Complete the workflow process
                $this->completeBenefitApproval($resource_id, $module);
            }
            return ['success' => $return, 'jump_level' => $jump_level];
        });
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function checkBenefitPaymentWorkflow(array $input)
    {
        return DB::transaction(function () use ($input) {
            $moduleRepo = new WfModuleRepository();
            $return = true;
            $level = $input['level'];
            $resource_id = $input['resource_id'];
            $module = $input['wf_module_id'];
            $status = $input['status'];

            $jump_level = NULL;

            if ((int)$level == $moduleRepo->claimBenefitAssessmentLevel($module)) {
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $eligible = $workflow->eligibles()->first();
                switch ($eligible->benefit_type_claim_id) {
                    case 3:
                    case 4:
                        //Temporary Disablement;
                        //Temporary Disablement Refund;
                    $count = $eligible->notificationDisabilityStateAssessments()->count();
                    if (!$count) {
                            //Assessment not done
                        throw new WorkflowException('Assessment has not been done, please check!');
                    }
                    break;
                    case 5:
                        //PD Payment
                    $claim = $eligible->incident->claim;
                    if (!$claim->assessed) {
                            //Assessment not done
                        throw new WorkflowException('Assessment has not been done, please check!');
                    }
                    break;
                    case 7:
                        //Survivors Payment
                        //No need for any check here ...
                        //just update assessment flag
                    $eligible->assessed = 1;
                    $eligible->save();
                    break;
                }
            }

            //if ((int)$level == $moduleRepo->claimBenefitTerminateLevel($module)) { //Check for Zero Payments
            if (in_array((int)$level, $moduleRepo->claimBenefitTerminateLevel($module))) { //Check for Zero Payments
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $eligible = $workflow->eligibles()->first();
                switch ($eligible->benefit_type_claim_id) {
                    case 3:
                    case 4:
                        //Temporary Disablement;
                        //Temporary Disablement Refund;
                    $iszeropaid = 0;

                    $days = $eligible->notificationDisabilityStateAssessments()->sum("notification_disability_state_assessments.days");
                    if (!$days) {
                        $iszeropaid = 1;
                    } else {
                        $incident = $eligible->incident;
                        $td_total_lumpsum = (new ClaimCompensationRepository())->progressiveCompensationApprove($incident, 0, $eligible->id)['td'][0]['total_lumpsum'];
                        if ($td_total_lumpsum == 0) {
                            $iszeropaid = 1;
                        }
                    }
                    if ($iszeropaid) {
                        $eligible->iszeropaid = 1;
                        $eligible->payprocesseddate = Carbon::now();
                        $eligible->save();
                            //Assessment has been done with zero days
                            //$return = false;
                        $jump_level = $moduleRepo->claimBenefitNotifyEmployerLevel($module);
                    }

                    break;
                    case 5:
                        //PD Payment
                    $claim = $eligible->incident->claim;
                    if ($claim->pd == 0) {
                            //Assessment has been done with zero pd, no lump-sum payment to be paid
                            //Assessment has been done with monthly pension, no lump-sum payment to be paid. Payment will be processed in Pensioner Payroll.
                        $eligible->iszeropaid = 1;
                        $eligible->payprocesseddate = Carbon::now();
                        $eligible->save();
                            //$return = false;
                            //Complete the workflow process
                            //$this->completeBenefitApproval($resource_id, $module);
                        $jump_level = $moduleRepo->claimBenefitNotifyEmployerLevel($module);
                    }

                    break;
                    case 7:
                        //Survivors Payment
                        //No need for any check here ...
                    break;
                }
            }

            if (in_array((int)$level, $moduleRepo->claimBenefitApproveLevel($module))) { //Check if approval level has been reached
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $eligible = $workflow->eligibles()->whereNull("notification_eligible_benefits.parent_id")->first();
                $claimCompensationRepo = new ClaimCompensationRepository();
                $claimCompensationRepo->progressiveCompensationApprove($eligible->incident, 1, $eligible->id);
            }

            if (in_array((int)$level, $moduleRepo->claimBenefitPayrollTerminateLevel($module))) { //Check if benefit is for payroll benefit
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $eligible = $workflow->eligibles()->first();
                switch ($eligible->benefit_type_claim_id) {
                    case 3:
                    case 4:
                        //Temporary Disablement;
                        //Temporary Disablement Refund;

                    break;
                    case 5:
                        //PD Payment
                    $claim = $eligible->incident->claim;

                    if ($claim->pd > sysdefs()->data()->percentage_imparement_scale) {
                        $claimCompensationRepo = new ClaimCompensationRepository();
                            //$claimCompensationRepo->progressiveCompensationApprove($eligible->incident, 1, $eligible->id);
                            //Process Payment per Benefit
                        $payment = new ProcessPaymentPerBenefit($workflow->id, access()->id());
                        $payment->processPayment();
                            //Update ispayprocessed
                        $eligible->ispayprocessed = 1;
                        $eligible->payprocesseddate = Carbon::now();
                        $eligible->save();
                            //$return = false;
                            //Complete the workflow process
                            //$this->completeBenefitApproval($resource_id, $module);
                        $jump_level = $moduleRepo->claimBenefitNotifyEmployerLevel($module);
                    }

                    break;
                    case 7:
                        //Survivors Payment
                        //No need for any check here ...
                    break;
                }
            }

            //if workflow is declined.
            if ($status == 3) {
                //Declined...
                $return = false;
                //Complete the workflow process
                $this->completeBenefitApproval($resource_id, $module);
            }

            return ['success' => $return, 'jump_level' => $jump_level];
        });
}

public function checkIfRejectionApproved($level, $resource_id, $module)
{
    return DB::transaction(function () use ($level, $resource_id, $module) {
        $moduleRepo = new WfModuleRepository();
        if ((int)$level == $moduleRepo->rejectionNotificationApprovalApprovedLevel($module)) {
            $workflow = (new NotificationWorkflowRepository())->find($resource_id);
            $incident = $workflow->notificationReport;
            switch ($module) {
                case $moduleRepo->systemRejectedNotificationModule()[0]:
                /* Approved (Rejection by System) Workflow */
                $staging_cv_id = (new CodeValueRepository())->NSAPRDNREJSYW();
                $incident->notification_staging_cv_id = $staging_cv_id;
                $incident->save();
                break;
                case $moduleRepo->rejectionNotificationApprovalModule()[0]:
                case $moduleRepo->rejectionNotificationApprovalModule()[1]:
                /* Approved Rejection from Notification Approval Workflow */
                $staging_cv_id = (new CodeValueRepository())->APRDNSRINCW();
                $incident->notification_staging_cv_id = $staging_cv_id;
                $incident->save();
                break;
            }
                //initialize notification
            $this->initialize($incident);
        }
    });
}

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @return mixed
     */
    public function checkIfBenefitPaymentPaid($level, $resource_id, $module)
    {
        return DB::transaction(function () use ($level, $resource_id, $module) {
            $moduleRepo = new WfModuleRepository();
            if ((int)$level == $moduleRepo->claimBenefitPaymentLevel($module)) {
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $eligible = $workflow->eligibles()->first();

                if (!$eligible->ispayprocessed && !$eligible->iszeropaid) {
                    throw new WorkflowException('Payment has not been processed, please click on process payment to proceed..!');
                }
                switch ($eligible->benefit_type_claim_id) {
                    case 3:
                    case 4:
                        //TD Payment;
                    if (!$eligible->ispaid && !$eligible->iszeropaid) {
                        throw new WorkflowException('Payment has not been paid in ERP, please check..!');
                    }
                    break;
                    case 5:
                        //PD Payment
                        //Check if PD is less than 30
                    if (!$eligible->ispaid && !$eligible->iszeropaid) {
                        throw new WorkflowException('Payment has not been paid in ERP, please check..!');
                    }
                    break;
                    case 7:
                        //Survivors Payment
                        //No need for any check here ...
                        //if (!$eligible->ispayprocessed) {
                    if (!$eligible->ispaid) {
                        throw new WorkflowException("Payment has not been processed, please click on Process Payment button to proceed..!");
                    }
                    break;
                    default:
                        //
                    break;
                }
            }
            return true;
        });
    }

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @return mixed
     */
    public function checkIfMaeRefundMemberPaid($level, $resource_id, $module)
    {
        return DB::transaction(function () use ($level, $resource_id, $module) {
            $moduleRepo = new WfModuleRepository();
            if ((int)$level == $moduleRepo->maeRefundMemberPaymentLevel($module)) {
                $workflow = (new NotificationWorkflowRepository())->find($resource_id);
                $eligible = $workflow->eligibles()->first();
                if (!$eligible->ispayprocessed) {
                    throw new WorkflowException("Payment has not been processed, please please click on process payment to proceed..!");
                }
                if (!$eligible->ispaid) {
                    throw new WorkflowException("Payment has not been paid in ERP, please check..!");
                }
            }
        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @return mixed
     */
    public function processBenefitPayment(Model $incident, Model $eligible)
    {
        return DB::transaction(function() use ($incident, $eligible) {
            $claimCompensationRepo = new ClaimCompensationRepository();

            $workflow = (new NotificationWorkflowRepository())->query()->where("id", $eligible->notification_workflow_id)->first();

            //check whether declined or approve and update the stage respectively.
            $check = (new WfTrackRepository())->checkIfModuleDeclined($eligible->notification_workflow_id, $workflow->wf_module_id);

            //Check if declined
            if ($check) {
                //Rejected
                throw new GeneralException("Can not process payment, this benefit has been declined...!");
            } else {
                //Approved

                if (!$eligible->ispayprocessed) {

                    $claimCompensationRepo->progressiveCompensationApprove($incident, 1, $eligible->id);

                    //Check if assessment has been done
                    switch ($eligible->benefit_type_claim_id) {
                        case 1:
                            //MAE Refund (Employee/Employer)
                        break;
                        case 3:
                        case 4:
                            //Temporary Disablement
                            //Temporary Disablement Refund
                        break;
                        case 5:
                            //Permanent Disablement
                        break;
                        case 7:
                            //Survivors
                        break;
                    }

                    //Process Payment
                    switch ($eligible->benefit_type_claim_id) {
                        //case 3:
                        //case 4:
                        //case 5:
                        //case 1:
                        //case 7:
                        default:
                            //TD Payment;
                            //PD Payment
                            //Process Compensations
                            //MAE Refund (Employee/Employer)
                            //Survivors Payment


                            //Process Payment per Benefit
                        $payment = new ProcessPaymentPerBenefit($workflow->id, access()->id());
                        $payment->processPayment();
                        break;
                    }

                    //Update ispayprocessed
                    $eligible->ispayprocessed = 1;
                    $eligible->payprocesseddate = Carbon::now();
                    $eligible->save();

                    //Process Compensations for Childs
                    $childs = $eligible->childs;

                    foreach ($childs as $child) {
                        $child->ispayprocessed = 1;
                        $eligible->payprocesseddate = Carbon::now();
                        $child->save();
                    }
                }

            }

            return true;
        });
    }

    /**
     * @param Model $incident
     * @param array $benefits
     * @param int $member_type_id
     * @param bool $need_conditional check if allowing query for condtitional document
     * @param null $incident_type_id
     * @return array
     * @description Used to check which documents are required for Notification Approval Process
     */
    public function getProgressiveDocumentList(Model $incident, $benefits = [], $member_type_id = 2, $need_conditional = true, $incident_type_id = NULL)
    {
        if (!$member_type_id) {
            $member_type_id = 2;
        }
        if (!$incident_type_id) {
            $incident_type_id = $incident->incident_type_id;
        }
        $codeValue = new CodeValueRepository();
        if (in_array($incident_type_id , [3])) {
            //Death Incident ...
            $death_cause_id = $incident->death->death_cause_id;
            $incident_type_id = $death_cause_id;
        }
        $arrList = [];
        if (count($benefits)) {
            foreach ($benefits as $benefit) {
                switch ($benefit) {
                    case 2:
                        //Funeral Grants
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 3:
                        //Permanent Partial Disablement
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 4:
                        //Permanent Total Disablement
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 5:
                        //Constant Attendance Care Grant
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 7:
                        //Temporary Partial Disablement
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 8:
                        //Temporary Total Disablement
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 9:
                        //Rehabilitation
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 10:
                        //Medical Aid Expenses
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 11:
                        //Survival Benefits
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 14:
                        //Temporary Partial Disablement Refund
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 15:
                        //Temporary Total Disablement Refund
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 16:
                        //Medical Aid Expenses Refund
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                }
            }

        } else {

            //Check if is not incident rejected by system [Out of time | Before Statutory Period]
            switch ($incident->notification_staging_cv_id) {
                case $codeValue->NSNREJSYBS():
                case $codeValue->NSNREJSYOT():
                case $codeValue->NSONNREJSYW():
                case $codeValue->NSCSYSREJ():
                    //Notification Rejection by System (Before the WCF Statutory Period)
                    //Notification Rejection by System (Out of Time)
                    //On Notification Rejection by System Workflow
                    //Closed Rejected Incident By System
                    $array = [27]; //Must be requiring WCN-1 document
                    $arrList += $array;
                    break;
                    default:
                    //MAE Approval (Default Required for Notification Approval)
                    $benefit_type = (new BenefitTypeRepository())->find(10);
                    //$array = [];
                    $array = $benefit_type->documents()->where("documents.ismandatory", 1)->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                    $arrList += $array;
                    //dd($member_type_id);
                    //Add documents for death
                    //switch ($incident->incident_type_id) {
                    switch ($incident_type_id) {
                        case 3:
                        case 4:
                        case 5:
                            //Death Incident
                            //TODO: add to document benefits table for benefit_type_id 10 and death incident.
                            $arrList[] = 12; //Burial Permit
                            $arrList[] = 11; //Death Certificate
                            break;
                        }
                        switch ($incident->incident_type_id) {
                            case 3:
                            case 4:
                            case 5:
                                //Death Incident
                                //TODO: add to document benefits table for benefit_type_id 10 and death incident.
                                $arrList[] = 12; //Burial Permit
                                $arrList[] = 11; //Death Certificate
                                break;
                        }
                        if ($need_conditional) {
                        //Check for additional document depending on the incident nature...
                        //check if employer is foreigner
                            if ($incident->employee_id) {
                                $employee = $incident->employee;
                                if (!is_null($employee->country_id) And $employee->country_id != 1) {
                                $array = [26]; //Must be requiring [Work Permit] Document
                                $arrList[] = 26;
                            }
                        }
                        //Check if is an Accident
                        if ($incident->incident_type_id == 1 Or in_array($incident->incident_type_id , [3,4,5])) {
                            $codeValue = new CodeValueRepository();
                            switch ($incident->incident_type_id) {
                                case 3:
                                case 4:
                                case 5:
                                    //Death
                                $accident_cause_cv_id = $incident->death->accident_cause_cv_id;
                                break;
                                default:
                                    //Accident
                                $accident_cause_cv_id = $incident->accident->accident_cause_cv_id;
                                break;
                            }
                            //This is an accident
                            //check if cause of accident is MTA
                            if ($codeValue->MTA($accident_cause_cv_id)) {
                                $array = [56]; //Must be requiring [PF90] Document
                                $arrList[] = 56;
                            }
                            //if cause of accident is Criminal, Thugs & Robbery
                            if ($accident_cause_cv_id == $codeValue->ACTHGSROBBRY()) {
                                $array = [10]; //Must be requiring [General Police Report] Document
                                $arrList[] = 10;
                            }
                            //If cause of Accident is Marine : requires Captain Report, document_id 55 TODO: add in the cause of accident list

                            //If cause of Accident is Air Traffic Accident : required Air Marshal Report, document_id 54 TODO: add in the cause of accident list
                        }
                    }
                    break;
                }

                $benefits = $incident->eligibleBenefits()->where(['processed' => 0])->get();
                foreach ($benefits as $benefit) {
                    $member_type_id = $benefit->pivot->member_type_id;
                    $benefit_types = $benefit->benefitTypes;
                    foreach ($benefit_types as $benefit_type) {
                        switch ($benefit_type->id) {
                            case 2:
                            //Funeral Grants
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 3:
                            //Permanent Partial Disablement
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 4:
                            //Permanent Total Disablement
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 5:
                            //Constant Attendance Care Grant
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 7:
                            //Temporary Partial Disablement
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 8:
                            //Temporary Total Disablement
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 9:
                            //Rehabilitation
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 10:
                            //Medical Aid Expenses
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 11:
                            //Survival Benefits
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 14:
                            //Temporary Partial Disablement Refund
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 15:
                            //Temporary Total Disablement Refund
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 16:
                            //Medical Aid Expenses Refund
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                        }
                    }
                }
            }
        //$documents = (new DocumentRepository())->query()->select(['name', 'id'])->whereIn("id", $arrList)->get();
        //return $documents;

            return $arrList;
        }

    /**
     * @param Model $incident
     * @param bool $need_conditional
     * @param null $incident_type_id
     * @return mixed
     */
    public function checkAllMandatoryDocumentsProgressive(Model $incident, $need_conditional = true, $incident_type_id = NULL)
    {
        $uploaded_documents = $this->getUploadedDocuments($incident);
        if (!$incident_type_id) {
            $incident_type_id = $incident->incident_type_id;
        }
        $pending_documents = (new DocumentRepository())
        ->query()
            //->where('ismandatory', 1)
        ->whereIn("id", $this->getProgressiveDocumentList($incident, [], NULL, $need_conditional, $incident_type_id))
        ->whereNotIn('id', $uploaded_documents)->orderBy('document_group_id', 'asc')
        ->get();
        return $pending_documents;
    }

    /**
     * @param Model $incident
     * @return mixed
     */
    public function getUploadedDocuments(Model $incident)
    {
        $uploaded_documents = $incident->documents()->whereNotNull('document_notification_report.name')->get()->pluck('id')->all();
        return $uploaded_documents;
    }

    /**
     * @param Model $incident
     * @param bool $isreferenced
     * @return mixed
     */
    public function getUploadedBenefitDocument(Model $incident, $isreferenced = true)
    {
        $query = $incident->documents()->select(["document_notification_report.document_id"])->whereNotNull('document_notification_report.name');
        if ($isreferenced) {
            $query->where("isreferenced", 0);
        }
        $uploaded_documents = $query->get()->pluck('document_id')->all();
        return $uploaded_documents;
    }

    /**
     * @param Model $incident
     * @param array $docs
     * @param $attribute, column to query doc_date | doc_receive_date | doc_create_date
     * @return mixed
     */
    public function getUploadedMaxDate(Model $incident, array $docs, $attribute)
    {
        $return = [];
        $attribute_col = "document_notification_report." . $attribute;
        $created_at = "document_notification_report.created_at";
        $query1 = $incident->documents()->select([$created_at, $attribute_col])->whereNotNull('document_notification_report.name')->whereIn("document_notification_report.document_id", $docs)->where("isreferenced", 0)->whereNotNull($attribute_col)->orderBy($attribute_col, "desc")->limit(1);
        if (!$query1->count()) {
            //$attribute = "created_at";
            $query2 = $incident->documents()->select([$created_at])->whereNotNull('document_notification_report.name')->whereIn("document_notification_report.document_id", $docs)->where("isreferenced", 0)->whereNotNull($created_at)->orderBy($created_at, "desc")->limit(1);
        }
        if ($query1->count()) {
            $pivot = $query1->first()->pivot;
            $return[$attribute] = $pivot->$attribute;
            $return["created_at"] = $pivot->created_at;
        } else {
            if ($query2->first()) {
                $pivot = $query2->first()->pivot;
                $return[$attribute] = $pivot->created_at;
                $return["created_at"] = $pivot->created_at;
            }
        }
        return $return;
    }

    /**
     * @param array $input | group, resource & type
     * @return mixed
     * @description Initialize workflow for Notification Report, this works for Enforceable Workflow And Notification Approval Workflow
     * @throws GeneralException
     */
    public function initiateWorkflow(array $input)
    {
        return DB::transaction(function () use ($input) {

            //Check if has access to initiate this workflow at level 1
            access()->hasWorkflowModuleDefinition($input['group'], $input['type'], 1);

            $wfModule = new WfModuleRepository();
            $moduleRepo = new WfModuleRepository();
            $codeValueRepo = new CodeValueRepository();

            $group = $input['group'];
            $resource = $input['resource'];
            $type = $input['type'];
            $comments = $input['comments'];

            $incident = $this->find($resource);

            $module = $wfModule->getModule(['wf_module_group_id' => $group, 'type' => $type]);

            switch ($module) {
                case $moduleRepo->notificationIncidentApprovalModule()[0]:
                case $moduleRepo->notificationIncidentApprovalModule()[1]:
                case $moduleRepo->notificationIncidentApprovalModule()[2]:
                    //Check if acknowledgement letter has been issued ...
                $checkCount = (new LetterRepository())->query()->where(['cv_id' => $codeValueRepo->CLACKNOWLGMNT(), 'resource_id' => $input['resource'], 'isinitiated' => 1])->count();
                if (!$checkCount) {
                    $checkCount2 = (new NotificationLetterRepository())->query()->where("notification_report_id", $input['resource'])->count();
                    if (!$checkCount2) {
                        throw new GeneralException("Action Failed, Acknowledgement letter for this notification has not been issued or not initiated for approval. Please check ...");
                    }
                }
                break;
                case $moduleRepo->voidNotificationModule()[0]:
                    //Void Notification
                    //Do nothing for now
                break;
                default:
                    //check if initiated by checklist user
                    //access()->assignedForNotification($incident); //no need because workflow user has already been checked at the beginning ...
                    //Check if there is any active notification workflow, restrict
                    $count = $incident->workflows()->where("wf_done", 0)->where("wf_module_id", $module)->count();
                    if ($count) {
                        throw new GeneralException("Can't initiate workflow, there is another active workflow pending for this notification");
                    }
                break;
            }

            //Check if has rights to initialize this workflow
            //access()->hasWorkflowModuleDefinition($group, $type, 1);

            //Check for documents for all group and types requiring documents, depend on some group and type
            //if it is notification approval workflow, check for document
            if ($wfModule->checkModuleRequiringDocuments($group, $type)) {
                $incident_type_id = $incident->incident_type_id;
                if (in_array($module, [$moduleRepo->transferToDeathIncidentApprovalModule()[0]])) {
                    switch ($incident->incident_type_id) {
                        case 1:
                            //Accident
                            $incident_type_id = 4;
                            break;
                        case 2:
                            //Disease
                            $incident_type_id = 5;
                            break;
                    }
                }
                $docs = $this->checkAllMandatoryDocumentsProgressive($incident, true, $incident_type_id);
                if ($docs->count()) {
                    $doc_list = $docs->pluck("name")->all();
                    //if (!env('TESTING_MODE', 0)) {
                    throw new GeneralException('Failure, Please make sure that the following pending documents have been uploaded! ( ' . implode(",", $doc_list) . ' )');
                    // }
                }
            }
            //logger($incident_type_id);
            //throw new GeneralException("Just Testing!");

            //**Check pre conditions where necessary for some group and types., can not appear here but rather in benefit workflow initialization methods which later call this method.**

            switch ($module) {
                case $moduleRepo->transferToDeathIncidentApprovalModule()[0]:
                    //Approve Notification Transfer to Death
                    //=> Check if there is no PD initiated ...
                    $count = $incident->benefits()->where("benefit_type_claim_id", 5)->whereIn("processed", [1,2])->count();
                    if ($count) {
                        throw new GeneralException("Action Cancelled, there is PD payment initiated.");
                    }
                    //=> Check if death details have been registered (claims table) ...
                    if (!$incident->death_claim) {
                        throw new GeneralException("Action Cancelled, please update death details first");
                    }
                    //reserve the stage
                    $incident->notification_staging_reserve_cv_id = $incident->notification_staging_cv_id;
                    $incident->progressive_stage_reserve = $incident->progressive_stage;
                    if ($incident->progressive_stage >= 4) {
                        $incident->progressive_stage = 3; //suppress benefit workflow if any ...
                    }
                    $incident->save();
                    $wfTrackRepo = new WfTrackRepository();
                    $codeValueRepo = new CodeValueRepository();
                    //$workflows = $incident->processes()->where("notification_workflows.wf_done", 0)->get();
                    $this->archiveWorkflows($incident, 'On Notification Transfer to Death Workflow');

                    break;
                case $moduleRepo->incorrectEmployeeInfoNotificationModule()[0]:
                    //Incorrect Employee Info
                break;
                case $moduleRepo->incorrectContributionNotificationModule()[0]:
                case $moduleRepo->incorrectContributionNotificationModule()[1]:
                    //Incorrect Contribution Notification
                /*reserve current notification stage*/
                $incident->notification_staging_reserve_cv_id = $incident->notification_staging_cv_id;
                $incident->progressive_stage_reserve = $incident->progressive_stage;
                if ($incident->progressive_stage >= 4) {
                    $incident->progressive_stage = 3;
                }
                $incident->notification_staging_cv_id = $codeValueRepo->ONSPPRICW();//On (Incorrect Contribution Amount) Workflow
                $incident->save();
                $wfTrackRepo = new WfTrackRepository();
                $codeValueRepo = new CodeValueRepository();
                    //$user = access()->id();
                    //-->archive any possible pending workflow
                $this->archiveWorkflows($incident, 'Incorrect Contribution');
                break;
                case $moduleRepo->voidNotificationModule()[0]:
                    //Void Notification
                $wfTrackRepo = new WfTrackRepository();
                $user = access()->id();
                /*reserve current notification stage*/
                $incident->notification_staging_reserve_cv_id = $incident->notification_staging_cv_id;
                $incident->notification_staging_cv_id = (new CodeValueRepository())->NCOVDNOTWRK();
                $incident->save();
                    //-->close any possible pending workflow
                $workflows = $incident->processes()->where("notification_workflows.wf_done", 0)->get();
                foreach ($workflows as $workflow) {
                    $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id);
                    if ($wfTrackInstance->count()) {
                        $wfTrack = $wfTrackInstance->first();
                        $wfTrack->user_id = $user;
                            //$wfTrack->allocated = $user;
                        $wfTrack->assigned = 1;
                        $wfTrack->user_type = "App\Models\Auth\User";
                        $wfTrack->comments = "Void Notification" ;
                        $wfTrack->forward_date = Carbon::now();
                        $wfTrack->status = 5;
                        $wfTrack->save();
                    }
                    $workflow->wf_done = 5;
                    $workflow->wf_done_date = Carbon::now();
                    $workflow->save();
                        //update eligible to cancel them
                    $workflow->eligibles()->update(["processed" => 3]);
                }
                break;
                case $moduleRepo->undoNotificationRejectionModule()[0]:
                    // Undo Rejection Workflow
                $wfTrackRepo = new WfTrackRepository();
                $user = access()->id();
                /*reserve current notification stage*/
                $incident->notification_staging_reserve_cv_id = $incident->notification_staging_cv_id;
                $incident->notification_staging_cv_id = (new CodeValueRepository())->NRONUNDORJWRK();
                $incident->save();
                    //-->close any possible pending workflow
                $workflows = $incident->processes()->where("notification_workflows.wf_done", 0)->get();
                foreach ($workflows as $workflow) {
                    $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id);
                    if ($wfTrackInstance->count()) {
                        $wfTrack = $wfTrackInstance->first();
                        $wfTrack->user_id = $user;
                            //$wfTrack->allocated = $user;
                        $wfTrack->assigned = 1;
                        $wfTrack->user_type = "App\Models\Auth\User";
                        $wfTrack->comments = "Undo Notification Rejection" ;
                        $wfTrack->forward_date = Carbon::now();
                        $wfTrack->status = 5;
                        $wfTrack->save();
                    }
                    $workflow->wf_done = 5;
                    $workflow->wf_done_date = Carbon::now();
                    $workflow->save();
                }
                break;
            }

            //Insert into Notification Workflow
            $workflow = $incident->workflows()->create([
                'wf_module_id' => $module,
            ]);

            //Initialize Workflow
            event(new NewWorkflow(['wf_module_group_id' => $group, 'resource_id' => $workflow->id, 'type' => $type], [], ['comments' => $comments]));

            //Update Notification Stage
            $this->updateWorkflowStage($incident, $group, $type);

            return $input;
        });
}

    /**
     * @param Model $incident
     * @param $description
     */
    public function archiveWorkflows(Model $incident, $description)
    {
        $wfModule = new WfModuleRepository();
        $moduleRepo = new WfModuleRepository();
        $codeValueRepo = new CodeValueRepository();
        $wfTrackRepo = new WfTrackRepository();
        $workflows = $incident->processes()->where("notification_workflows.wf_done", 0)->get();
        foreach ($workflows as $workflow) {
            $wfTrack = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id)->first();
            if ($wfTrack) {
                $wfTrack->user_type = "App\Models\Auth\User";
                $wfTrack->comments = NULL ;
                $wfTrack->forward_date = Carbon::now();
                $wfTrack->status = 6;
                $wfTrack->save();
                try {
                    //archive workflow
                    (new WfTrackRepository())->postArchiveWorkflow($wfTrack, 1, [
                        'instore' => 1,
                        'description' => $description, //$description
                        'from_date' => Carbon::now()->format('Y-m-d'),
                        'to_date' => Carbon::now()->addWeek(2)->format('Y-m-d'),
                        'archive_reason_cv_id' => $codeValueRepo->CARINSFCNTINF(),
                        'wf_track_id' => $wfTrack->id,
                        'receiver_user' => NULL,
                    ]);
                } catch (\Exception $e) {

                }

            }
            $workflow->wf_done = 6;
            //$workflow->wf_done_date = Carbon::now();
            $workflow->save();
        }
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function initiateBenefitWorkflow(array $input)
    {
        return DB::transaction(function () use ($input) {

            $wfModule = new WfModuleRepository();
            $codeValueRepo = new CodeValueRepository();

            $benefit = $input['benefit'];
            $eligible = $input['eligible'];
            $comments = $input['comments'];

            $checkCount = (new LetterRepository())->query()->where(['cv_id' => $codeValueRepo->CLACKNOWLGMNT(), 'resource_id' => $input['incident'], 'isinitiated' => 1])->count();
            /*if (!$checkCount) {
                throw new GeneralException("Action Failed, Acknowledgement letter for this notification has not been issued or not initiated for approval. Please check ...");
            }*/
            if (!$checkCount) {
                $checkCount2 = (new NotificationLetterRepository())->query()->where("notification_report_id", $input['incident'])->count();
                if (!$checkCount2) {
                    throw new GeneralException("Action Failed, Acknowledgement letter for this notification has not been issued or not initiated for approval. Please check ...");
                }
            }

            //Get the workflow type for the processed benefit
            switch ($benefit) {
                case 1:
                    //MAE Refund (Employee/Employer)
                $type = $wfModule->maeRefundMemberType()['type'];
                break;
                default:
                    //Claim Benefits
                $type = $wfModule->claimBenefitType()['type'];
                break;
            }

            //Check if has access to initiate this workflow at level 1
            access()->hasWorkflowModuleDefinition(3, $type, 1);

            //Check for special approval in case of MAE workflow initialization.
            $incident= $this->query()->find($input['incident']);

            //check if initiated by checklist user
            access()->assignedForNotification($incident);

            //Get the instance of eligible benefit
            $eligible_benefit = (new NotificationEligibleBenefitRepository())->find($eligible);

            //Flag off MAE and TD
            switch ($benefit) {
                case 1:
                    //MAE Refund (Employee/Employer)
                $maeQuery = $incident->mae();
                if ($maeQuery->count()) {
                    $mae = $maeQuery->first();
                    $mae->notification_eligible_benefit_id = $eligible_benefit->id;
                    $mae->save();
                    $incident->incident_mae_id = NULL;
                    $incident->save();
                }
                break;
                case 3:
                case 4:
                    //Temporary Disablement
                    //Temporary Disablement Refund
                $maeQuery = $incident->td();
                if ($maeQuery->count()) {
                    $td = $maeQuery->first();
                    $td->notification_eligible_benefit_id = $eligible_benefit->id;
                    $td->save();
                    $incident->incident_td_id = NULL;
                    $incident->save();
                }
                break;
                case 5:
                    //Permanent Disablement
                $incident->pd_eligible_id = $eligible_benefit->id;
                $incident->save();
                break;
                case 7:
                    //Survivors
                $incident->survivor_eligible_id = $eligible_benefit->id;
                $incident->save();
                break;
            }

            //check for dependants if death
            if ($eligible_benefit->benefit_type_claim_id == 7) {
                $this->checkIfDependentsAdded($incident->id);
                //check if all necessary documents have been uploaded ...
                $dependentRepo = new DependentRepository();
                $return = $dependentRepo->checkHasMandatoryDocument($incident);
                if (!$return['success']) {
                    throw new GeneralException($return['message']);
                }
            }

            //throw new GeneralException('Testing Dependant Document Check ...!');

            $module = $wfModule->getModule(['wf_module_group_id' => 3, 'type' => $type]);

            //Check if has rights to initialize this workflow
            access()->hasWorkflowModuleDefinition(3, $type, 1);

            //Insert into Notification Workflow
            $workflow = $incident->workflows()->create([
                'wf_module_id' => $module,
            ]);

            //update eligible benefit for workflows
            $eligible_benefit->isinitiated = 1;
            $eligible_benefit->notification_workflow_id = $workflow->id;
            $eligible_benefit->save();
            //update eligible benefit for workflows if they exists
            $eligible_benefit->childs()->update(['isinitiated' => 1, 'notification_workflow_id' => $workflow->id]);

            //Initialize Workflow
            event(new NewWorkflow(['wf_module_group_id' => 3, 'resource_id' => $workflow->id, 'type' => $type], [], ['comments' => $comments]));

            //Update Notification Stage
            //Check if the current stage is not on benefit workflow stage
            if ($incident->notification_staging_cv_id != $codeValueRepo->NSOCBW()) {
                //set the current stage as on claim benefit workflow
                $incident->notification_staging_cv_id = $codeValueRepo->NSOCBW();
                $incident->save();
                //Initialize Incident
                $this->initialize($incident);
            }

            return true;
        });
}

    /**
     * @param Model $incident
     * @param $group
     * @param $type
     * @throws GeneralException
     */
    public function updateWorkflowStage(Model $incident, $group, $type)
    {
        $wfModule = new WfModuleRepository();
        $codeValue = new CodeValueRepository();
        $updated = false;
        switch (true) {
            case ($group == $wfModule->notificationIncidentApproval()['group'] And $type == $wfModule->notificationIncidentApproval()['type']):
                //Notification Incident Approval
            $incident->notification_staging_cv_id = $codeValue->NSOPNAP();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->accidentMaeSpecialApprovalType()['group'] And $type == $wfModule->accidentMaeSpecialApprovalType()['type']):
                //Accident Medical Aid Expense (MAE) Special Approval
            $incident->notification_staging_cv_id = $codeValue->NSOPNAP();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->diseaseMaeSpecialApprovalType()['group'] And $type == $wfModule->diseaseMaeSpecialApprovalType()['type']):
                //Disease Medical Aid Expense (MAE) Special Approval
            $incident->notification_staging_cv_id = $codeValue->NSDMAESAW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->maeApprovalHcpType()['group'] And $type == $wfModule->maeApprovalHcpType()['type']):
                //Medical Aid Expense Approval for HCP/HSP
            $incident->notification_staging_cv_id = $codeValue->NSMAEAHCHSW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->maeRefundHcpType()['group'] And $type == $wfModule->maeRefundHcpType()['type']):
                //Medical Aid Expense Refund for HCP/HSP
            $incident->notification_staging_cv_id = $codeValue->NSMAERHCHSW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->maeRefundMemberType()['group'] And $type == $wfModule->maeRefundMemberType()['type']):
                //Medical Aid Expense Refund for Employee/Employer
            $incident->notification_staging_cv_id = $codeValue->NSMAERMEMW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->claimBenefitType()['group'] And $type == $wfModule->claimBenefitType()['type']):
                //Claim Benefits
            $incident->notification_staging_cv_id = $codeValue->NSOCBW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->systemRejectedNotificationType()['group'] And $type == $wfModule->systemRejectedNotificationType()['type']):
                //System Rejected Notification
            $incident->notification_staging_cv_id = $codeValue->NSONNREJSYW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->unregisteredMemberNotificationType()['group'] And $type == $wfModule->unregisteredMemberNotificationType()['type']):
                //Unregistered Member Notification
            $incident->notification_staging_cv_id = $codeValue->ONSPPRUMW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->missingContributionNotificationType()['group'] And $type == $wfModule->missingContributionNotificationType()['type']):
                //Missing Contribution Notification
            $incident->notification_staging_cv_id = $codeValue->ONSPPRMCW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->incorrectContributionNotificationType()['group'] And $type == $wfModule->incorrectContributionNotificationType()['type']):
                //Incorrect Contribution Notification
            $incident->notification_staging_cv_id = $codeValue->ONSPPRICW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->incorrectEmployeeInfoNotificationType()['group'] And $type == $wfModule->incorrectEmployeeInfoNotificationType()['type']):
                //Incorrect Employee Info
            $incident->notification_staging_cv_id = $codeValue->ONSPPRIEIFW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->rejectionNotificationApprovalType()['group'] And $type == $wfModule->rejectionNotificationApprovalType()['type']):
                //Rejection from Notification Approval
            $incident->notification_staging_cv_id = $codeValue->ONSRINCW();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->voidNotificationType()['group'] And $type == $wfModule->voidNotificationType()['type']):
                //On Void Notification Workflow
            $incident->notification_staging_cv_id = $codeValue->NCOVDNOTWRK();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->undoNotificationRejectionType()['group'] And $type == $wfModule->undoNotificationRejectionType()['type']):
                //On Undo Rejection Workflow
            $incident->notification_staging_cv_id = $codeValue->NRONUNDORJWRK();
            $incident->save();
            $updated = true;
            break;
            case ($group == $wfModule->transferToDeathIncidentApproval()['group'] And $type == $wfModule->transferToDeathIncidentApproval()['type']):
                //On Undo Rejection Workflow
                $incident->notification_staging_cv_id = $codeValue->NSOTRTDEWRK();
                $incident->save();
                $updated = true;
                break;
        }
        if ($updated) {
            //Initialize Incident
            $this->initialize($incident);
        }
    }

    /**
     * @param $id
     * @param $module_id
     * @return mixed
     */
    public function completeIncidentApproval($id, $module_id)
    {
        return DB::transaction(function () use ($id, $module_id) {
            $workflow = (new NotificationWorkflowRepository())->find($id);
            $incident = $workflow->notificationReport;

            //update wf_done on notification workflow
            $workflow->wf_done_date = Carbon::now();

            //check whether declined or approve and update the stage respectively.
            $check = (new WfTrackRepository())->checkIfModuleDeclined($id, $module_id);

            switch ($check) {
                case 1:
                    //declined
                    //Rejection from notification approval
                    //Rejected Incident from Notification Approval
                $incident->notification_staging_cv_id = (new CodeValueRepository())->NSRINC();
                $incident->wf_done = 2;
                $incident->wf_done_date = Carbon::now();
                $incident->save();
                $workflow->wf_done = 2;
                    //update workflow model
                $workflow->save();
                    //initialize notification
                $this->initialize($incident);
                break;
                case 0:
                    //approved
                    //Approved
                    //if approved, update progressive stage
                    //On Progress (Incident Approved)
                $incident->notification_staging_cv_id = (new CodeValueRepository())->NSOPIA();
                $incident->progressive_stage = 4;
                $incident->wf_done = 1;
                $incident->wf_done_date = Carbon::now();
                $incident->save();
                    //Upgrade to Claim
                    $monthly_earning = $incident->monthly_earning;//$this->employees->getMonthlyEarningBeforeIncident($incident->employee_id, $incident->id)['monthly_earning'];
                    $contrib_month = $incident->contrib_month;//$this->employees->getMonthlyEarningBeforeIncident($incident->employee_id, $incident->id)['contrib_month'];
                    $exit_code = in_array($incident->incident_type_id , [3,4,5]) ? 1 : null;  //death
                    $input = ['notification_report_id' => $incident->id, 'incident_type_id' => $incident->incident_type_id, 'monthly_earning' => $monthly_earning, 'exit_code' => $exit_code, 'contrib_month'  => $contrib_month];
                    $this->claims->updateOrCreate($input);
                    $workflow->wf_done = 1;
                    //update workflow model
                    $workflow->save();
                    //TODO: check if there was undo benefit process
                    //initialize notification
                    $this->initialize($incident);
                    break;
                    default:
                    //unexpected result
                    break;
                }

            //if incident is death, update on eligible benefit processed = 1,
            if (in_array($incident->incident_type_id , [3,4,5])) {
                //This is death, update for funeral grant & survivor benefit
                //Should first pass through survivor benefit approval first ...
                //$incident->benefits()->update(['processed' => 1]);
                }

                return true;
            });
    }

    /**
     * @param $id
     * @param $module_id
     * @param int $level
     * @return mixed
     */
    public function completeEnforceableWorkflow($id, $module_id, $level = 0)
    {
        return DB::transaction(function () use ($id, $module_id, $level) {
            $moduleRepo = new WfModuleRepository();
            $workflow = (new NotificationWorkflowRepository())->find($id);
            $incident = $workflow->notificationReport;

            //update wf_done on notification workflow
            $workflow->wf_done_date = Carbon::now();
            $workflow->wf_done = 1;
            $workflow->save();

            //update stage respectively
            switch ($module_id) {
                case $moduleRepo->missingContributionNotificationModule()[0]:
                /*Notification Missing Contribution*/

                /**** start : Check if contribution has been added .... ****/
                $employee_id = $incident->employee_id;
                $employer_id = $incident->employer_id;
                    //$contrib_month = Carbon::parse($incident->incident_date)->subMonth()->format("Y-n-j");
                $contrib_month = $incident->contrib_month;

                $contribution = (new ContributionRepository())->getContribution($employee_id, $employer_id, $contrib_month)->first();

                if (($contribution) And $contribution->grosspay > 0) {
                        //update contribution entry
                    $incident->monthly_earning = $contribution->grosspay;
                    $incident->contrib_month = $contribution->contrib_month;
                    $incident->contrib_before = 1;
                    $incident->save();
                } else {
                    if (!is_null($incident->datehired)) {
                        $contrib_month = Carbon::parse($incident->datehired)->format("Y-n-j");
                        $contribution = (new ContributionRepository())->getContribution($employee_id, $employer_id, $contrib_month)->first();
                        if (($contribution) And $contribution->grosspay > 0) {
                                //update contribution entry
                            $incident->monthly_earning = $contribution->grosspay;
                            $incident->contrib_month = $contribution->contrib_month;
                            $incident->contrib_before = 1;
                            $incident->save();
                        } else {
                            throw new WorkflowException("Contribution not registered!");
                        }
                    } else {
                        throw new WorkflowException("Contribution not registered!");
                    }
                }
                /**** end : Check if contribution has been added .... ****/

                    //Pending Complete Registration (Filling Checklist)
                if ($incident->notification_staging_reserve_cv_id) {
                    $incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                    $incident->notification_staging_reserve_cv_id = NULL;
                } else {
                    $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFC();
                }
                $incident->save();

                    //initialize notification
                $this->initialize($incident);
                break;
                case $moduleRepo->unregisteredMemberNotificationModule()[0]:
                /*Unregistered Member Notification*/
                    //Check if notification has been registered.... (May be removed in future)
                $this->checkIfHasRegisteredMemberForWorkflow($level, $id, $module_id);

                    //Send File to e-office
                /* start: Update notification file name */
                $incident->filename = $incident->file_name_specific_label;
                $incident->iscomplete_register = 1;
                $incident->save();
                /* end: Update notification file name */
                dispatch(new PostNotificationDms($incident));

                    //Check if it is within time limit
                $return = $this->checkDateLimit($incident);

                if ($return) {
                        //Notification is within time ...
                        //Check if there is a contribution before the incident date ...
                    $this->checkContributionStatus($incident);
                }

                    //initialize notification
                $this->initialize($incident);
                break;
                case $moduleRepo->systemRejectedNotificationModule()[0]:
                /*System Rejected Notification*/
                    //Closed Rejected Incident By System
                $incident->notification_staging_cv_id = (new CodeValueRepository())->NSCSYSREJ();
                    //update wf_done on notification_report table
                $incident->wf_done_date = Carbon::now();
                $incident->wf_done = 2;
                $incident->save();
                    //initialize notification
                $this->initialize($incident);
                break;
                case $moduleRepo->rejectionNotificationApprovalModule()[0]:
                case $moduleRepo->rejectionNotificationApprovalModule()[1]:
                /*Rejection from Notification Approval*/
                    //Closed Rejected Incident from Notification Approval
                $incident->notification_staging_cv_id = (new CodeValueRepository())->NSCAPPRREJ();
                    //update wf_done on notification_report table
                $incident->wf_done_date = Carbon::now();
                $incident->wf_done = 2;
                $incident->save();
                    //initialize notification
                $this->initialize($incident);
                break;
            }

            return true;
        });
}

    /**
     * @param $id
     * @param $module_id
     * @return mixed
     */
    public function completeInterruptingWorkflow($id, $module_id)
    {
        return DB::transaction(function () use ($id, $module_id) {
            $moduleRepo = new WfModuleRepository();
            $workflow = (new NotificationWorkflowRepository())->find($id);
            $incident = $workflow->notificationReport;

            //update wf_done on notification workflow
            $workflow->wf_done_date = Carbon::now();
            $workflow->wf_done = 1;
            $workflow->save();

            //update stage respectively
            switch ($module_id) {
                case $moduleRepo->incorrectEmployeeInfoNotificationModule()[0]:
                    //Incorrect Employee Info
                break;
                case $moduleRepo->transferToDeathIncidentApprovalModule()[0]:
                    //check whether declined or approve and update the stage respectively.
                    $check = (new WfTrackRepository())->checkIfModuleDeclined($id, $module_id);

                    switch ($check) {
                        case 1:
                            //declined

                            break;
                        case 0:
                            //approved transfer to death
                            $death_claim = $incident->death_claim;
                            $input = json_decode($death_claim, true);
                            $death = $this->death->saveChecklist($incident, $input);
                            $this->updateOdtNo($death);
                            $employeeno = $incident->employee->memberno;
                            $incident->death_filename = "ODT/" . $employeeno . "/" . $death->odtno;
                            switch ($incident->incident_type_id) {
                                case 1:
                                    $incident->incident_type_id = 4;
                                    break;
                                case 2:
                                    $incident->incident_type_id = 5;
                                    break;
                            }
                            $incident->save();
                            break;
                        default:
                            //unexpected result
                            break;
                    }

                    if ($incident->notification_staging_reserve_cv_id) {
                        $incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                        $incident->notification_staging_reserve_cv_id = NULL;
                    }
                    if ($incident->progressive_stage_reserve) {
                        $incident->progressive_stage = $incident->progressive_stage_reserve;
                        $incident->progressive_stage_reserve = NULL;
                    }
                    $incident->save();
                    $this->unArchiveWorkflows($incident);
                    break;
                case $moduleRepo->incorrectContributionNotificationModule()[0]:
                case $moduleRepo->incorrectContributionNotificationModule()[1]:
                    //Incorrect Contribution Notification
                if ($incident->notification_staging_reserve_cv_id) {
                    $incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                    $incident->notification_staging_reserve_cv_id = NULL;
                } else {
                    $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFC();
                }
                    //$incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                if ($incident->progressive_stage_reserve) {
                    $incident->progressive_stage = $incident->progressive_stage_reserve;
                    $incident->progressive_stage_reserve = NULL;
                }
                $incident->save();
                    //$incident->save();
                $this->unArchiveWorkflows($incident);
                break;
                case $moduleRepo->voidNotificationModule()[0]:
                    //check whether declined or approve and update the stage respectively.
                $check = (new WfTrackRepository())->checkIfModuleDeclined($id, $module_id);
                if ($check) {
                        ///==> Declined
                        ///==> Undo Void Notification and restore the reserved stage.
                    $incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                    $incident->notification_staging_reserve_cv_id = NULL;
                    $incident->save();

                    $wfTrackRepo = new WfTrackRepository();
                        //-->open any possible closed workflow by user/system
                    $workflows = $incident->processes()->where("notification_workflows.wf_done", 5)->get();

                    foreach ($workflows as $workflow) {
                        $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id, 5);
                        if ($wfTrackInstance->count()) {
                            $wfTrack = $wfTrackInstance->first();
                            $wfTrack->user_id = $wfTrack->allocated;
                                //$wfTrack->allocated = $user;
                            $wfTrack->assigned = 1;
                            $wfTrack->user_type = "App\Models\Auth\User";
                            $wfTrack->comments = NULL ;
                            $wfTrack->forward_date = NULL;
                            $wfTrack->status = 0;
                            $wfTrack->save();
                        }
                        $workflow->wf_done = 0;
                        $workflow->wf_done_date = NULL;
                        $workflow->save();
                            //update eligible to restore them
                        $workflow->eligibles()->update(["processed" => 0]);
                    }
                } else {
                        ///==> Approved
                    $incident->notification_staging_cv_id = (new CodeValueRepository())->NCVOIDNOTFCN();
                    $incident->notification_staging_reserve_cv_id = NULL;
                    $incident->save();
                }
                break;
                case $moduleRepo->undoNotificationRejectionModule()[0]:
                    //check whether declined or approve and update the stage respectively.
                $check = (new WfTrackRepository())->checkIfModuleDeclined($id, $module_id);
                if ($check) {
                        //Declined
                        //Decline Undo Notification Rejection
                    $incident->notification_staging_cv_id = $incident->notification_staging_reserve_cv_id;
                    $incident->notification_staging_reserve_cv_id = NULL;
                    $incident->save();

                    $wfTrackRepo = new WfTrackRepository();
                        //-->open any possible closed workflow by user/system
                    $workflows = $incident->processes()->where("notification_workflows.wf_done", 5)->get();

                    foreach ($workflows as $workflow) {
                        $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id, 5);
                        if ($wfTrackInstance->count()) {
                            $wfTrack = $wfTrackInstance->first();
                            $wfTrack->user_id = $wfTrack->allocated;
                                //$wfTrack->allocated = $user;
                            $wfTrack->assigned = 1;
                            $wfTrack->user_type = "App\Models\Auth\User";
                            $wfTrack->comments = NULL ;
                            $wfTrack->forward_date = NULL;
                            $wfTrack->status = 0;
                            $wfTrack->save();
                        }
                        $workflow->wf_done = 0;
                        $workflow->wf_done_date = NULL;
                        $workflow->save();
                            //update eligible to restore them
                        $workflow->eligibles()->update(["processed" => 0]);
                    }
                } else {
                        ///==> Approved
                    switch (true) {
                        case ($incident->progressive_stage >= 3):
                                //Pending Complete Registration (Finished Filling Investigation)
                        $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFNFI();
                        break;
                        default:
                        if ($this->checkDateLimit($incident)) {
                            switch ($incident->progressive_stage) {
                                case 1:
                                            // Pending Complete Registration (Filling Checklist)
                                $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFC();
                                break;
                                case 2:
                                            // Pending Complete Registration (Filling Investigation)
                                $incident->notification_staging_cv_id = (new CodeValueRepository())->NSPCRFI();
                                break;
                            }
                        }
                        break;
                    }
                    $incident->notification_staging_reserve_cv_id = NULL;
                    $incident->save();
                }
                break;
            }
            $this->initialize($incident, 1);

            return true;
        });
}

public function unArchiveWorkflows(Model $incident)
{
    $wfTrackRepo = new WfTrackRepository();
    $workflows = $incident->processes()->where("notification_workflows.wf_done", 6)->get();
    foreach ($workflows as $workflow) {
        $wfTrack = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id, 6)->first();
        if ($wfTrack) {
            $wfTrack->user_type = "App\Models\Auth\User";
            $wfTrack->comments = NULL ;
            $wfTrack->forward_date = NULL;
            $wfTrack->status = 0;
            $wfTrack->save();
            try {
                //un-archive workflow
                (new WfTrackRepository())->postArchiveWorkflow($wfTrack, 0, []);
            } catch (\Exception $e) {

            }
        }
        $workflow->wf_done = 0;
        $workflow->wf_done_date = NULL;
        $workflow->save();
    }
}

public function jumpBenefitApproval()
{

}

    /**
     * @param $id
     * @param $module_id
     * @return mixed
     */
    public function completeBenefitApproval($id, $module_id)
    {
        return DB::transaction(function () use ($id, $module_id) {

            $eligibleRepo = new NotificationEligibleBenefitRepository();
            $codeValueRepo = new CodeValueRepository();
            $claimCompensationRepo = new ClaimCompensationRepository();

            $workflow = (new NotificationWorkflowRepository())->find($id);
            $incident = $workflow->notificationReport;
            $eligible = $eligibleRepo->query()->where("notification_workflow_id", $workflow->id)->whereNull("parent_id")->first();

            //update wf_done on notification workflow
            $workflow->wf_done_date = Carbon::now();

            //check whether declined or approve and update the stage respectively.
            $check = (new WfTrackRepository())->checkIfModuleDeclined($id, $module_id);

            if ($check) {
                //Declined
                $eligible->processed = 2;
                $eligible->save();
                $eligible->childs()->update(['processed' => 2]);
                $workflow->wf_done = 2;
            } else {
                //Approved
                $eligible->processed = 1;
                $eligible->save();
                $eligible->childs()->update(['processed' => 1]);
                $workflow->wf_done = 1;

                //Process Compensations (Processed at finance level, presently cancelled...)
                //$claimCompensationRepo->progressiveCompensationApprove($incident, 1, $eligible->id);

                //Process Compensations for Childs
                /*$childs = $eligible->childs;

                foreach ($childs as $child) {
                    $claimCompensationRepo->progressiveCompensationApprove($incident, 1, $child->id);
                }*/

            }

            //update workflow model
            $workflow->save();

            //check if there are other pending benefit initiated workflows
            $count = $eligibleRepo->query()->where(["processed" => 0, "isinitiated" => 1, "notification_report_id" => $incident->id])->count();

            if (!$count) {
                //update notification stage
                //On Progress (Incident Approved)
                //$incident->notification_staging_cv_id = $codeValueRepo->NSOPIA();
                //On Progress (At least Payed)
                if ($incident->progressive_stage == 4) {
                    $incident->progressive_stage = 5; //
                }
                $incident->notification_staging_cv_id = $codeValueRepo->NSOPALP();
                $incident->save();
                $this->initialize($incident);
            }

            return true;
        });
    }

    /**
     * @param NotificationReport $incident
     * @param $benefit
     * @return mixed
     */
    public function initiateBenefitPayment(NotificationReport $incident, $benefit) {
        return DB::transaction(function () use ($incident, $benefit) {
            //check for td & pd if has already processed.
            $check = 0;
            switch (true) {
                case ($benefit == 3):
                    //For TD Payment
                    //==> Check if there is already PD Payments
                    /*$check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", 5)->whereIn("notification_eligible_benefits.processed", [0, 1])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, This notification has already registered PD Payment!");
                    }*/
                    //==> Check if TD Payment has already started
                    $check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", $benefit)->whereIn("notification_eligible_benefits.processed", [0])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, There is already an active registered benefit waiting assessment!");
                    }
                    //==> Check if TD Refund has already processed
                    /*$check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", 4)->whereIn("notification_eligible_benefits.processed", [0, 1])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, This notification has already registered TD Refund Payment!");
                    }*/
                    break;
                    case ($benefit == 4):
                    //For TD Refund Payment
                    //==> Check if there is already PD Payments
                    /*$check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", 5)->whereIn("notification_eligible_benefits.processed", [0, 1])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, This notification has already registered PD Payment!");
                    }*/
                    //==> Check if TD Refund Payment has already started
                    $check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", $benefit)->whereIn("notification_eligible_benefits.processed", [0])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, There is already an active registered benefit waiting assessment!");
                    }
                    //==> Check if TD Payment has already processed
                    /*$check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", 3)->whereIn("notification_eligible_benefits.processed", [0, 1])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, This notification has already registered TD Refund Payment!");
                    }*/
                    break;
                    case ($benefit == 5):
                    //For PD Payment
                    //==> Check if TD Payment has already processed.
                    /*$check = $incident->eligibleBenefits()->whereIn("notification_eligible_benefits.benefit_type_claim_id", [3,4])->whereIn("notification_eligible_benefits.processed", [0, 1])->count();
                    if (!$check) {
                        throw new GeneralException("Action cancelled, TD Payment has not been processed.!");
                    }*/
                    //==> Check if PD Payment has already processed.
                    $check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", $benefit)->whereIn("notification_eligible_benefits.processed", [0, 1])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, PD Payment has already been registered!");
                    }
                    break;
                    case ($benefit == 7):
                    //For Survivor Benefit
                    //==> Check if Survivor Benefit has already processed.
                    $check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", $benefit)->whereIn("notification_eligible_benefits.processed", [0, 1])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, Survivor Benefit has already been registered for this notification!");
                    }
                    break;
                    case ($benefit == 1):
                    //For MAE Refund (Employee/Employer)
                    //==> Check if this benefit is already in progress
                    $check = $incident->eligibleBenefits()->where("notification_eligible_benefits.benefit_type_claim_id", $benefit)->whereIn("notification_eligible_benefits.processed", [0])->count();
                    if ($check) {
                        throw new GeneralException("Action cancelled, there is a pending MAE Refund (Employee/Employer) Payment in progress!");
                    }
                    break;
                }
                return true;
            });
}

    /**
     * @param Model $incident
     * @param Model $benefit
     * @param array $input
     * @return mixed
     */
    public function storeMaeRefundMember(Model $incident, Model $benefit, array $input)
    {
        return DB::transaction(function () use ($incident, $benefit, $input) {
            $service_selected = false;
            foreach ($input as $key => $value) {
                if (strpos($key, 'feedback') !== false) {
                    $health_service_checklist_id = substr($key, 8);
                    if ($input['feedback' . $health_service_checklist_id] == 1) {
                        $service_selected = true;
                        break;
                    }
                }
            }

            if ($service_selected) {
                switch ($input['member_type_id']) {
                    case '1':
                        //Employer
                    $resource_id = $incident->employer_id;
                    break;
                    case '2':
                        //Employee
                    $resource_id = $incident->employee_id;
                    break;
                }

                //Create Medical Expense Entry
                $expense = $this->medical_expenses->createBenefit([
                    'notification_report_id' => $incident->id,
                    'amount' => $input['expense_amount'],
                    'member_type_id' => $input['member_type_id'],
                    'resource_id' => $resource_id,
                ]);
                $input['medical_expense_id'] = $expense->id;

                //Create eligible payment entry
                $id = $this->storeEligibleBenefits($incident, $benefit, [
                    'member_type_id' => $input['member_type_id'],
                    'resource_id' => $resource_id,
                    'data_id' => $expense->id,
                ]);

                // Store notification health provider
                $notification_health_provider = $this->notification_health_providers->create($incident->id, $input);

                if (!is_null($input['medical_practitioner_id'])) {
                    // Store notification health provider practitioner
                    $this->notification_health_provider_practitioners->create($notification_health_provider->id, $input);
                }

                //Store notification health provider services
                foreach ($input as $key => $value) {
                    if (strpos($key, 'feedback') !== false) {
                        $health_service_checklist_id = substr($key, 8);
                        if ($input['feedback' . $health_service_checklist_id] == 1) {
                            $notification_report_provider_service = $this->notification_health_provider_services->create($notification_health_provider->id, $health_service_checklist_id, $input);
                        }
                    }
                }

                //Save total for assessed amount
                $assessed_amount = $notification_health_provider->notificationHealthProviderServices()->sum("amount");
                $expense->assessed_amount = $assessed_amount;
                $expense->save();
            } else {
                throw new GeneralException("Action cancelled, no health service checklist selected");
            }


            return true;
        });
    }

    /**
     * @param array $input
     * @return array
     * @throws WorkflowException
     */
    public function checkMaeMultipleAmounts(array $input)
    {
        $amount = $assessed_amount = $total_sub_amount = $total_sub_assessed_amount = 0;
        $maeSubs = [];
        foreach ($input as $key => $value) {
            if (strpos($key, 'sub_amount') !== false) {
                $id = substr($key, 10);
                $this_sub_amount = $input['sub_amount' . $id];
                if (is_numeric($this_sub_amount)) {
                    $total_sub_amount += $this_sub_amount;
                }
                $this_sub_assessed_amount = $input['sub_assessed_amount' . $id];
                if (is_numeric($this_sub_assessed_amount)) {
                    $total_sub_assessed_amount += $this_sub_assessed_amount;
                }
                $maeSubs[] = [
                    'rctno' => strtolower(trim($input['rctno' . $id])),
                    'health_provider_id' => $input['health_provider_id' . $id],
                    'amount' => $input['sub_amount' . $id],
                    'assessed_amount' => $input['sub_assessed_amount' . $id],
                    'remarks' => isset($input['remarks' . $id]) ? $input['remarks' . $id] : NULL,
                ];
            }
        }

        if (is_numeric($input['assessed_amount'])) {
            $assessed_amount = $input['assessed_amount'];
        }
        if (is_numeric($input['amount'])) {
            $amount = $input['amount'];
        }

        if ($assessed_amount != $total_sub_assessed_amount) {
            throw new WorkflowException("Failed, Total sub assessed amount do not tally with total assessed amount");
        }
        if ($amount != $total_sub_amount) {
            throw new WorkflowException("Failed, Total sub amount do not tally with total amount");
        }
        return $maeSubs;
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @param array $input
     * @return mixed
     */
    public function storeMaeRefundMemberMultiple(Model $incident, Model $benefit, array $input)
    {
        return DB::transaction(function () use ($incident, $benefit, $input) {

            $maeSubs = $this->checkMaeMultipleAmounts($input);

            switch ($input['member_type_id']) {
                case '1':
                    //Employer
                $resource_id = $incident->employer_id;
                break;
                case '2':
                    //Employee
                $resource_id = $incident->employee_id;
                break;
            }

            //Create Medical Expense Entry
            $expense = $this->medical_expenses->query()->create([
                'notification_report_id' => $incident->id,
                'amount' => $input['amount'],
                'member_type_id' => $input['member_type_id'],
                'resource_id' => $resource_id,
                'assessed_amount' => $input['assessed_amount'],
                'user_id' => access()->id(),
                'receive_date' => $input['receive_date'],
                'remarks' => $input['general_remarks'],
                'has_subs' => 1,
            ]);

            //Create eligible payment entry
            $id = $this->storeEligibleBenefits($incident, $benefit, [
                'member_type_id' => $input['member_type_id'],
                'resource_id' => $resource_id,
                'data_id' => $expense->id,
            ]);

            $expense->maeSubs()->createMany($maeSubs);

            return true;

        });
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @param array $input
     * @return mixed
     */
    public function storeTd(Model $incident, Model $benefit, array $input)
    {
        return DB::transaction(function () use ($incident, $benefit, $input) {

            //check for health state checklist combination
            /**
                For TD to store (Health State Checklist Eligible):
                >> 2, 7, 8
             */
            /**
                For TD Refund to store (Health State Checklist Eligible):
                >> 2, 7, 8
             */

            //store eligible benefit
            //Create eligible payment entry
            $id = $this->storeEligibleBenefits($incident, $benefit, [
                'member_type_id' => 1,
                'resource_id' => $incident->employer_id,
                'data_id' => NULL,
            ]);

            $input['notification_eligible_benefit_id'] = $id;
            $input['incident_id'] = $incident->id;

            /*foreach ($input as $key => $value) {
                if (strpos($key, 'health_state') !== false) {
                    $health_state_checklist_id = substr($key, 12);
                    if ($input['health_state' . $health_state_checklist_id] == 1) {
                        $notification_health_state = $this->notification_health_states->create($health_state_checklist_id, $incident->id, $input);
                    }
                }
            }*/
            //Create only one health state ....
            $notification_health_state = $this->notification_health_states->createOne($input['health_state_checklist_id'], $incident->id, $input);

            //Store notification disability state

            foreach ($input as $key => $value) {
                if (strpos($key, 'disability_state') !== false) {
                    $disability_state_checklist_id = substr($key, 16);
                    if ($input['disability_state' . $disability_state_checklist_id] == 1) {
                        $notification_disability_state = $this->notification_disability_states->create($disability_state_checklist_id, $input);
                    }
                }
            }

            return true;
        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @param array $input
     * @return mixed
     */
    public function updateTd(Model $incident, Model $eligible, array $input)
    {
        return DB::transaction(function () use ($incident, $eligible, $input) {

            $input['notification_eligible_benefit_id'] = $eligible->id;
            $input['incident_id'] = $incident->id;

            /*foreach ($input as $key => $value) {
                if (strpos($key, 'health_state') !== false) {
                    $health_state_checklist_id = substr($key, 12);
                    //                    if ($input['feedback'. $health_service_checklist_id]) {
                    $notification_health_state = $this->notification_health_states->update($health_state_checklist_id, $incident->id, $input);
//                    }
                }
            }*/
            $notification_health_state = $this->notification_health_states->updateOne($input['health_state_checklist_id'], $incident->id, $input);

            //Update notification health provider services
            foreach ($input as $key => $value) {
                if (strpos($key, 'disability_state') !== false) {
                    $disability_checklist_id = substr($key, 16);
                    $notification_disability_state = $this->notification_disability_states->update($disability_checklist_id, $input);
                }
            }
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @param array $input
     * @return mixed
     */
    public function updateMaeRefundMemberMultiple(Model $incident, Model $eligible, array $input)
    {
        return DB::transaction(function () use ($incident, $eligible, $input) {

            $maeSubs = $this->checkMaeMultipleAmounts($input);

            switch ($input['member_type_id']) {
                case '1':
                    //Employer
                $resource_id = $incident->employer_id;
                break;
                case '2':
                    //Employee
                $resource_id = $incident->employee_id;
                break;
            }

            //possibly save new member type on eligible benefit
            $eligible->member_type_id = $input['member_type_id'];
            $eligible->resource_id = $resource_id;
            $eligible->user_id = access()->id();
            $eligible->assessed = 1;
            $eligible->save();

            $medicalExpense = $eligible->medicalExpense;

            $medicalExpense->update([
                'amount' => $input['amount'],
                'member_type_id' => $input['member_type_id'],
                'resource_id' => $resource_id,
                'assessed_amount' => $input['assessed_amount'],
                'user_id' => access()->id(),
                'receive_date' => $input['receive_date'],
                'remarks' => $input['general_remarks'],
            ]);

            $medicalExpense->maeSubs()->delete();

            $medicalExpense->maeSubs()->createMany($maeSubs);

            return true;

        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @param array $input
     * @return mixed
     */
    public function updateMaeRefundMember(Model $incident, Model $eligible, array $input)
    {
        return DB::transaction(function () use ($incident, $eligible, $input) {
            $service_selected = false;

            foreach ($input as $key => $value) {
                if (strpos($key, 'feedback') !== false) {
                    $health_service_checklist_id = substr($key, 8);
                    if ($input['feedback' . $health_service_checklist_id] == 1) {
                        $service_selected = true;
                    }
                }
            }

            if ($service_selected) {
                switch ($input['member_type_id']) {
                    case '1':
                        //Employer
                    $resource_id = $incident->employer_id;
                    break;
                    case '2':
                        //Employee
                    $resource_id = $incident->employee_id;
                    break;
                }

                //possibly save new member type on eligible benefit
                $eligible->member_type_id = $input['member_type_id'];
                $eligible->resource_id = $resource_id;
                $eligible->user_id = access()->id();
                $eligible->save();

                $medicalExpense = $eligible->medicalExpense;

                $input['medical_expense_id'] = $medicalExpense->id;

                //Update Medical Expense Entry
                $expense = $this->medical_expenses->updateBenefit($medicalExpense->id, [
                    'notification_report_id' => $incident->id,
                    'amount' => $input['expense_amount'],
                    'member_type_id' => $input['member_type_id'],
                    'resource_id' => $resource_id,
                ]);

                $notificationHealthProvider = $medicalExpense->notificationHealthProviders()->first();

                // Update notification health provider
                $notification_health_provider = $this->notification_health_providers->update($notificationHealthProvider->id, $input);

                if (!is_null($input['medical_practitioner_id'])) {
                    // Update notification health provider practitioner
                    $this->notification_health_provider_practitioners->update($notification_health_provider->id, $input);
                }

                foreach ($input as $key => $value) {
                    if (strpos($key, 'feedback') !== false) {
                        $health_service_checklist_id = substr($key, 8);
                        $notification_report_provider_service = $this->notification_health_provider_services->update($notification_health_provider->id, $health_service_checklist_id, $input);
                    }
                }

                //Save total for assessed amount
                $assessed_amount = $notificationHealthProvider->notificationHealthProviderServices()->sum("amount");
                $medicalExpense->assessed_amount = $assessed_amount;
                $medicalExpense->save();
            } else {
                throw new GeneralException("Action cancelled, no health service checklist selected");
            }

            return true;
        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @return mixed
     */
    public function deleteMaeRefundMember(Model $incident, Model $eligible)
    {
        return DB::transaction(function () use ($incident, $eligible) {

            $medicalExpense = $eligible->medicalExpense;

            if ($medicalExpense->has_subs) {
                //delete sub amounts
                $medicalExpense->maeSubs()->delete();
                //delete medical expense
                $medicalExpense->forceDelete();
                //Delete the benefit entry
                $eligible->delete();
            } else {
                $notificationHealthProvider = $medicalExpense->notificationHealthProviders()->first();
                //delete notification health provider services
                $notificationHealthProvider->notificationHealthProviderServices()->forceDelete();
                //delete notification health provider practitioners
                $notificationHealthProvider->notificationHealthProviderPractitioner()->forceDelete();
                //delete notification health provider
                $notificationHealthProvider->forceDelete();
                //delete medical expense
                $medicalExpense->forceDelete();
                //Delete the benefit entry
                $eligible->delete();
            }

            return true;
        });
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @param array $input
     * @return mixed
     */
    public function storeEligibleBenefits(Model $incident, Model $benefit, array  $input)
    {
        //Create eligible payment entry
        $incident->eligibleBenefits()->attach($benefit->id, [
            'member_type_id' => $input['member_type_id'],
            'resource_id' => $input['resource_id'],
            'user_id' => access()->id(),
            'data_id' => $input['data_id'],
        ]);
        //Get the id of the pivot table
        $pivot = $incident->eligibleBenefits()->where("notification_eligible_benefits.notification_report_id", $incident->id)->where("notification_eligible_benefits.benefit_type_claim_id", $benefit->id)->orderByDesc("notification_eligible_benefits.id")->limit(1)->first()->pivot;
        return $pivot->id;
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @return mixed
     */
    public function createPd(Model $incident, Model $benefit)
    {
        return DB::transaction(function() use ($incident, $benefit) {
            //check for previous created pd which was not declined
            $existingEligibleCount = $incident->benefits()->where("benefit_type_claim_id", 5)->whereHas("workflow", function ($query) {
                $query->where("notification_workflows.wf_done", "<>", 5);
            })->count();
            if ($existingEligibleCount) {
                throw new GeneralException("There is already PD payment initiated");
            } else {
                //create pd benefit
                $incident->benefits()->create(
                /*                    [
                                        'notification_report_id' => $incident->id,
                                        'benefit_type_claim_id' => 5
                                    ],*/
                                    [
                                        'notification_report_id' => $incident->id,
                                        'benefit_type_claim_id' => 5,
                                        'member_type_id' => 2,
                                        'resource_id' => $incident->employee_id,
                                        'user_id' => access()->id(),
                                        'parent_id' => NULL,
                                    ]
                                );
            }
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @return mixed
     */
    public function createSurvivor(Model $incident, Model $benefit)
    {
        return DB::transaction(function() use ($incident, $benefit) {
            //create pd benefit
            $eligible = $incident->benefits()->updateOrCreate(
                [
                    'notification_report_id' => $incident->id,
                    'benefit_type_claim_id' => 7
                ],
                [
                    'member_type_id' => 2,
                    'resource_id' => $incident->employee_id,
                    'user_id' => access()->id(),
                    'parent_id' => NULL,
                ]
            );
            //Insert for Funeral Grant Benefits
            $incident->benefits()->updateOrCreate(
                [
                    'notification_report_id' => $incident->id,
                    'benefit_type_claim_id' => 6
                ],
                [
                    'member_type_id' => 2,
                    'resource_id' => $incident->employee_id,
                    'user_id' => access()->id(),
                    'parent_id' => $eligible->id,
                    'isinitiated' => 0,
                ]
            );

            return true;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     * @description update or store contribution for notification report ...
     */
    public function postContribution(Model $incident, array $input)
    {
        return DB::transaction(function() use ($incident, $input) {
            $employee_id = $incident->employee_id;
            $hired_on_incident_month = $input['hired_on_incident_month'];
            $ishired_on_incident_month = false;
            $contrib_month_expected = Carbon::parse($incident->incident_date)->subMonth()->format("Y-n-28");

            if (isset($input['contrib_month'])) {
                if (($contrib_month_expected != $input['contrib_month']) && is_null($input['remarks'])) {
                    throw new GeneralException("Please add remarks why contribution month for the salary is different");
                }
            }

            if ($incident->incident_type_id == 2) {
                if ($input['employee_still_at_work']) {
                    $input['contrib_month'] = $input['contrib_month_atwork'];
                } else {
                    $input['contrib_month'] = $input['contrib_month_retired'];
                }
            }

            if (!empty($hired_on_incident_month) And $hired_on_incident_month) {
                //option has been selected...!!
                //$input['contrib_month'] = Carbon::parse($incident->incident_date)->format("Y-n-28");
                $ishired_on_incident_month = true;
            }
            if ($ishired_on_incident_month) {
                $input['date_hired'] = $input['datehired'];
                //check if date hired and incident date (Month and Date) matches
                if (Carbon::parse($incident->incident_date)->format("Y-n") != Carbon::parse($input['date_hired'])->format("Y-n")) {
                    throw new GeneralException("Contribution month and year does not match with incident date");
                }
                $this->storeDateHired($incident, $input);
            }
            switch ($input['action']) {
                case 'update':

                    //updating existing contribution
                $contribution = (new EmployeeRepository())->updateContribution($incident->employee_id, $input);
                break;
                default:
                $count = (new ContributionRepository())->doesEmployeeHasContribution($employee_id, $input['employer_id'], $input['contrib_month']);
                if ($count) {
                        //updating existing contribution
                    $contribution = (new EmployeeRepository())->updateContribution($incident->employee_id, $input);
                } else {
                        //storing new contribution
                    $contribution = (new EmployeeRepository())->storeContribution($incident->employee_id, $input, 0);
                }
                break;
            }

            //update contribution entry
            $incident->monthly_earning = $contribution->grosspay;
            $incident->monthly_earning_remarks = $input['remarks'] ?? NULL;
            $incident->contrib_month = $contribution->contrib_month;
            $incident->contrib_before = 1;
            if ($ishired_on_incident_month) {
                $incident->contrib_on = 1;
            } else {
                $incident->contrib_on = 0;
            }
            $incident->save();

            //wait for workflow to complete to update progressive stage
            //throw new GeneralException("Testing");
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postRegisterEmployee(Model $incident, array  $input)
    {
        return DB::transaction(function() use ($incident, $input) {
            $incident->employee_id = $input['employee_id'];
            $incident->employer_id = $input['employer_id'];
            $incident->save();
            ///Save Employer information
            $employee = $this->employees->query()->where("id", $incident->employee_id)->first();
            $incident->employers()->syncWithoutDetaching($employee->employers()->select([DB::raw("employers.id as employer_id")])->pluck("employer_id")->all());
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @return mixed
     */
    public function deletePd(Model $incident, Model $eligible)
    {
        return DB::transaction(function() use ($incident, $eligible) {
            $eligible->delete();
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @return mixed
     */
    public function deleteSurvivor(Model $incident, Model $eligible)
    {
        return DB::transaction(function() use ($incident, $eligible) {
            $eligible->delete();
            return true;
        });
    }

    /**
     * @param Model $claim
     * @param $status
     * @return mixed
     */
    public function onlineAccountValidateAction(Model $claim, $status, array $input)
    {
        return DB::transaction(function() use ($claim, $status, $input) {
            $activityRepo = new ClaimActivityRepository();
            $codeValuePortalRepo = new CodeValuePortalRepository();
            switch ($status) {
                case 1:
                    //validating
                $claim->validated = "t";
                $claim->isactive = "t";
                $claim->save();
                $msg = "Your online compensation account has been validated, you can now log in to the system to proceed with registration.";
                    //Send SMS
                dispatch(new SendSmsEga($msg, $claim->phone));
                    //Send Email
                $claim->notify(new ClaimAccountValidated());
                break;
                default:
                    //de-activating
                $claim->isactive = "f";
                $claim->save();
                $msg = "Your online compensation account with email {$claim->email} did not pass validation, Call us for free: 0800 110028 / 0800 110029 for more information";
                    //Send SMS
                dispatch(new SendSmsEga($msg, $claim->phone));
                break;
            }
            $user_id = access()->id();
            //Store the account activity
            switch ($status) {
                case 1:
                    //validating
                $activity = $activityRepo->query()->create([
                    'user_id' => $user_id,
                    'claim_activity_cv_id' => $codeValuePortalRepo->AVACCVLDT(),
                    'claim_id' => $claim->id,
                ]);
                $claim->claim_activity_id = $activity->id;
                $claim->save();
                break;
                default:
                    //de-activating
                $activity = $activityRepo->query()->create([
                    'user_id' => $user_id,
                    'claim_activity_cv_id' => $input['account_activity_cv_id'],
                    'claim_id' => $claim->id,
                ]);
                $claim->claim_activity_id = $activity->id;
                $claim->save();
                break;
            }
            return true;
        });
    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     * STORE HEALTH PROVIDER SERVICE==================================
     */
    public function storeHealthProviderService($id, $input)
    {
        return DB::transaction(function () use ($id, $input) {
            // Store notification health provider
            $notification_health_provider = $this->notification_health_providers->create($id, $input);
            $notification_health_provider_id = $notification_health_provider->id;
            // Store notification health provider practitioner
            $this->notification_health_provider_practitioners->create($notification_health_provider_id, $input);

            //Store notification health provider services

            foreach ($input as $key => $value) {
                if (strpos($key, 'feedback') !== false) {
                    $health_service_checklist_id = substr($key, 8);
                    if ($input['feedback' . $health_service_checklist_id] == 1) {
                        $notification_report_provider_service = $this->notification_health_provider_services->create($notification_health_provider_id, $health_service_checklist_id, $input);
                    }
                }
            }
//          Update Total medical service cost. MAE
//            $this->medical_expenses->updateTotalMedicalServicesCost($input['medical_expense_id']);
        });

    }


    public function updateHealthProviderService($notification_health_provider_id, $input)
    {

        return DB::transaction(function () use ($notification_health_provider_id, $input) {
            // update notification health provider
            $notification_health_provider = $this->notification_health_providers->update($notification_health_provider_id, $input);

            // update notification health provider practitioner
            $this->notification_health_provider_practitioners->update($notification_health_provider_id, $input);

            //Update notification health provider services

            foreach ($input as $key => $value) {
                if (strpos($key, 'feedback') !== false) {
                    $health_service_checklist_id = substr($key, 8);
                    $notification_report_provider_service = $this->notification_health_provider_services->update($notification_health_provider_id, $health_service_checklist_id, $input);

                }
            }

            //          Update Total medical service cost. MAE
//            $this->medical_expenses->updateTotalMedicalServicesCost($input['medical_expense_id']);

            return $notification_health_provider;
        });

    }


    /**
     * Approve / Elevate to claim===================
     */

    public function approveToClaim($id)
    {
        return DB::transaction(function () use ($id) {
            $this->validateApprovalToClaim($id);
            $notification_report = $this->findOrThrowException($id);
            $employee_id = $notification_report->employee_id;
            $incident_date = $this->findIncidentDate($id);
            $exit_code = in_array($notification_report->incident_type_id , [3,4,5]) ? 1 : null;  //death
            // Get recent salary before incident
            $monthly_earning = $this->employees->getMonthlyEarningBeforeIncident($employee_id, $notification_report->id)['monthly_earning'];
            $contrib_month = $this->employees->getMonthlyEarningBeforeIncident($employee_id, $notification_report->id)['contrib_month'];
            // Create Claim
            $input = ['notification_report_id' => $id, 'incident_type_id' => $notification_report->incident_type_id, 'monthly_earning' => $monthly_earning, 'exit_code' => $exit_code, 'contrib_month'  => $contrib_month];
            $claim = $this->claims->updateOrCreate($input);

            //update status
            $this->updateStatus($id,1);
            // Initiate workflow
            return $claim;
        });

    }

    private function updateOacNo(Model $accident)
    {
        $next_oacno = $this->getNextOacNoSysdef();
        $accident->oacno = $next_oacno;
        $accident->save();
    }

    private function updateOdtNo(Model $death)
    {
        $next_odtno = $this->getNextOdtNoSysdef();
        $death->odtno = $next_odtno;
        $death->save();
    }

    private function updateOdsNo(Model $desease)
    {
        $next_odsno = $this->getNextOdsNoSysdef();
        $desease->odsno = $next_odsno;
        $desease->save();
    }

    public function getNextOacNoSysdef()
    {
        $sysdef = sysdefs()->data();
        $nextOacNo = $sysdef->oacno;
        $sysdef->oacno = $nextOacNo + 1;
        $sysdef->save();
        return $nextOacNo;
    }

    public function getNextOdtNoSysdef()
    {
        $sysdef = sysdefs()->data();
        $nextOdtNo = $sysdef->odtno;
        $sysdef->odtno = $nextOdtNo + 1;
        $sysdef->save();
        return $nextOdtNo;
    }

    public function getNextOdsNoSysdef()
    {
        $sysdef = sysdefs()->data();
        $nextOdsNo = $sysdef->odsno;
        $sysdef->odsno = $nextOdsNo + 1;
        $sysdef->save();
        return $nextOdsNo;
    }

    public function updateSpecificIncidentNo()
    {
        /*$oacno = 1;
        $odsno = 1;
        $odtno = 1;*/
        $notification_reports = $this->query()->orderBy("id", "asc")->get();
        foreach ($notification_reports as $notification) {

            $notification->isdmsposted = 0;
            $notification->save();

            /*switch ($notification->incident_type_id) {
                case 1:
                    //Accident
                    $accident = $notification->accident;
                    $accident->oacno = $oacno;
                    $accident->save();
                    $notification->filename = $notification->file_name_specific_label;
                    $notification->save();
                    $oacno++;
                    break;
                case 2:
                    //Disease
                    $disease = $notification->disease;
                    $disease->odsno = $odsno;
                    $disease->save();
                    $notification->filename = $notification->file_name_specific_label;
                    $notification->save();
                    $odsno++;
                    break;
                case 3:
                    //Death
                    $death = $notification->death;
                    $death->odtno = $odtno;
                    $death->save();
                    $notification->filename = $notification->file_name_specific_label;
                    $notification->save();
                    $odtno++;
                    break;
                }*/
                dispatch(new PostNotificationDms($notification));
            }
        /*$sysdef = sysdefs()->data();
        $sysdef->oacno = $oacno;
        $sysdef->odsno = $odsno;
        $sysdef->odtno = $odtno;
        $sysdef->save();*/
    }

    /**
     * Recall Notifications: Get dataTable
     */
    public function getForDataTable()
    {
        $incidents = $this->query()->select([
            DB::raw("notification_reports.id as case_no"),
            DB::raw("incident_types.name as incident_type"),
            DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as fullname"),
            DB::raw("concat_ws(' ', users.firstname, coalesce(users.middlename, ''), users.lastname) as checklistuser"),
            DB::raw("employees.firstname as firstname"),
            DB::raw("employees.middlename as middlename"),
            DB::raw("employees.lastname as lastname"),
            DB::raw("employers.name as employer"),
            DB::raw("notification_reports.incident_date as incident_date"),
            DB::raw("notification_reports.receipt_date as receipt_date"),
            DB::raw("notification_reports.status as status"),
            DB::raw("notification_reports.filename as filename"),
            DB::raw("coalesce(regions.name, '') as region"),
            DB::raw("notification_staging_cv_id"),
        ])
        ->join("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
        ->join("employers", "employers.id", "=", "notification_reports.employer_id")
        ->join("employees", "employees.id", "=", "notification_reports.employee_id")
        ->leftJoin("users", "users.id", "=", "notification_reports.allocated")
        ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
        ->leftJoin("regions", "regions.id", "=", "districts.region_id");

        $incident = (int) request()->input("incident");
        if ($incident) {
            $incidents->where("notification_reports.incident_type_id", "=", $incident);
        }
        $employee = request()->input("employee");
        if ($employee) {
            $incidents->where("notification_reports.employee_id", "=", $employee);
        }
        $employer = request()->input("employer");
        if ($employer) {
            $incidents->where("notification_reports.employer_id", "=", $employer);
        }
        $region = request()->input("region");
        if ($region) {
            $incidents->where("regions.id", "=", $region);
        }
        $district = request()->input("district");
        if ($district) {
            $incidents->where("districts.id", "=", $district);
        }
        $stage = request()->input("stage");
        if ($stage) {
            $incidents->where("notification_reports.notification_staging_cv_id", "=", $stage);
        }

        return $incidents;
    }

    /**
     * @param $id
     * @return mixed
     * @description Delete / Undo notification report registration
     * @throws GeneralException
     */
    public function delete($id)
    {
        $notification_report = $this->findOrThrowException($id);
        if ($notification_report->accident){
            $this->accidents->delete($notification_report->accident->id);
        }
        if ($notification_report->disease){
            $this->diseases->delete($notification_report->disease->id);
        }
        if ($notification_report->death){
            $this->death->delete($notification_report->disease->id);
        }
        $notification_report->delete();
        return $notification_report;
    }

    /**
     * @param Model $incident
     * @throws GeneralException
     * @throws \Exception
     */
    public function deleteProgressive(Model $incident)
    {
        switch($incident->incident_type_id) {
            case 1:
                //Accident
            $this->accidents->delete($incident->accident->id, true);
            break;
            case 2:
                //Disease
            $this->diseases->delete($incident->disease->id, true);
            break;
            case 3:
                //Death
            $this->death->delete($incident->death->id, true);
            break;
        }
        $incident->forceDelete();
    }

    /**
     * @param $id
     * @param $wf_module_group_id
     * @return mixed
     * @description Undo Notification report processing | Remove notification report when notification is wrongly registered or not needed on the system.
     * @throws GeneralException
     */
    public function undo($id, $wf_module_group_id){
        $notification_report = $this->findOrThrowException($id);

        /*Check workflow*/
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        if ($check_workflow != 1){
            /*Does not have workflow*/
            $notification_report = $this->delete($id);
        }else{
            /*Have a workflow - Should be level 1 pending*/
            if ($check_pending_level1 == 1){
                $claim = $notification_report->claim;
                /*delete claim*/
                if ($claim){
                    $claim->delete();
                }
                /*delete notification report*/
                $notification_report = $this->delete($id);
            }
        }
        return $notification_report;
    }

    /**
     * @param Model $incident
     * @return bool
     * @throws GeneralException
     */
    public function undoProgressive(Model $incident)
    {
        return DB::transaction(function () use ($incident) {
            $codeValue = new CodeValueRepository();
            //Delete treatment records
            /* start : clean current notification treatments */
            $treatments = $incident->treatments;
            foreach ($treatments as $treatment) {
                $treatment->disabilities()->sync([]);
                $treatment->practitioners()->sync([]);
                $treatment->delete();
            }
            /* end : clean current notification treatments */

            //Clear the notification stage before deleting stages
            $incident->notification_stage_id = NULL;
            $incident->save();

            //Delete stage records
            $incident->stages()->sync([]);

            //Delete Checkers
            (new CheckerRepository())->query()->where(['resource_id' => $incident->id, 'checker_category_cv_id' => $codeValue->CHCNOTN()])->delete();
            //Delete workflows
            $workflows = $incident->workflows;
            foreach ($workflows as $workflow) {
                //Delete Workflow Track
                (new WfTrackRepository())->query()->where(['resource_id' => $workflow->id, 'resource_type' => 'App\Models\Operation\Claim\NotificationWorkflow'])->forceDelete();
                //Delete Workflow
                $workflow->delete();
            }

            //Delete attached employers
            $incident->employers()->sync([]);

            //Clear all the attached documents
            $incident->documents()->sync([]);
            //TODO: Remove all attached documents from e-office

            //Remove any notification injuries
            $incident->injuries()->sync([]);

            //Remove any lost body parts
            $incident->lostBodyParties()->sync([]);

            //Remove Document in e-Office
            dispatch(new DeleteNotificationDms($incident->id));

            //delete contribution tracks
            $incident->notificationContributionTracks()->delete();

            //delete disability states

            //delete investigation feedbacks
            $incident->investigationFeedbacks()->forceDelete();

            //delete investigations
            $incident->investigations()->forceDelete();

            //delete notification letter
            $incident->letter()->delete();

            //delete closures
            $incident->closures()->delete();

            //delete tds
            $incident->tds()->delete();

            //delete mae
            $incident->maes()->delete();

            //Delete notification
            $this->deleteProgressive($incident);

            return true;
        });

    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function updateManualPayment(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $wfTrackRepo = new WfTrackRepository();
            $user = access()->id();

            switch ($incident->incident_type_id) {
                case 1:
                case 2:
                    //Accident
                    //Disease
                $incident->man_mae = $input['man_mae'];
                $incident->man_pd = $input['man_pd'];
                $incident->man_ppd = $input['man_ppd'];
                    //$incident->man_ptd = $input['man_ptd'];
                $incident->man_ttd = $input['man_ttd'];
                $incident->man_tpd = $input['man_tpd'];
                break;
                case 3:
                    //Death
                $incident->man_funeral_grant = $input['man_funeral_grant'];
                break;
            }
            $incident->save();

            if (!$incident->haspaidmanual) {
                //Check if progressive
                if ($incident->isprogressive) {
                    //Incident is Progressive
                    //-->Reserve the current stage
                    $notification_staging_cv_id = $incident->notification_staging_cv_id;
                    $incident->paidmanual_reserve_flag = $notification_staging_cv_id;
                    $incident->notification_staging_cv_id = (new CodeValueRepository())->NSATLPMAN();
                    $incident->save();
                    //-->close any possible pending workflow
                    $workflows = $incident->processes()->where("notification_workflows.wf_done", 0)->get();

                    foreach ($workflows as $workflow) {
                        $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id);
                        if ($wfTrackInstance->count()) {
                            $wfTrack = $wfTrackInstance->first();
                            $wfTrack->user_id = $user;
                            //$wfTrack->allocated = $user;
                            $wfTrack->assigned = 1;
                            $wfTrack->user_type = "App\Models\Auth\User";
                            $wfTrack->comments = "File Paid Manually" ;
                            $wfTrack->forward_date = Carbon::now();
                            $wfTrack->status = 5;
                            $wfTrack->save();
                        }
                        $workflow->wf_done = 5;
                        $workflow->wf_done_date = Carbon::now();
                        $workflow->save();
                        $workflow->eligibles()->update([
                            'processed' => 3,
                        ]);
                    }

                    $this->initialize($incident);
                } else {
                    //Incident is not Progressive
                    //-->Reserve the current status
                    $status = $incident->status;
                    $incident->paidmanual_reserve_flag = $status;
                    $incident->status = 1;
                    $incident->save();
                    //-->close any possible pending workflow
                    //-->check current status
                    $wf_module_id = NULL;
                    if ($status == 2) {
                        $wf_module_id = (new WfModuleRepository())->legacyNotificationRejectionModule()[0];
                    } else {
                        $wf_module_id = (new WfModuleRepository())->legacyNotificationModule()[0];
                    }
                    $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($incident->id, $wf_module_id);
                    if ($wfTrackInstance->count()) {
                        $wfTrack = $wfTrackInstance->first();
                        $wfTrack->user_id = $user;
                        //$wfTrack->allocated = $user;
                        $wfTrack->assigned = 1;
                        $wfTrack->user_type = "App\Models\Auth\User";
                        $wfTrack->comments = "File Paid Manually" ;
                        $wfTrack->forward_date = Carbon::now();
                        $wfTrack->status = 5;
                        $wfTrack->save();
                    }
                    //update wf_done for the incident
                    $incident->wf_done_date = Carbon::now();
                    $incident->wf_done = 1;
                    $incident->save();
                }
            }

            $incident->user_set_manual_pay = $user;
            $incident->haspaidmanual = 1;
            $incident->save();

            return true;
        });
}

    /**
     * @param Model $incident
     * @return mixed
     */
    public function undoManualPayment(Model $incident)
    {
        return DB::transaction(function () use ($incident) {
            $wfTrackRepo = new WfTrackRepository();
            $user = access()->id();

            $incident->man_mae = NULL;
            $incident->man_pd = NULL;
            $incident->man_ppd = NULL;
            //$incident->man_ptd = NULL;
            $incident->man_ttd = NULL;
            $incident->man_tpd = NULL;
            $incident->man_funeral_grant = NULL;
            $incident->save();

            if ($incident->haspaidmanual) {
                //Check if progressive
                if ($incident->isprogressive) {
                    //Incident is Progressive
                    //-->restore the reserved stage in from the flag
                    $incident->notification_staging_cv_id = $incident->paidmanual_reserve_flag;
                    $incident->save();
                    //-->open any possible closed workflow by user/system
                    $workflows = $incident->processes()->where("notification_workflows.wf_done", 5)->get();

                    foreach ($workflows as $workflow) {
                        $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($workflow->id, $workflow->wf_module_id, 5);
                        if ($wfTrackInstance->count()) {
                            $wfTrack = $wfTrackInstance->first();
                            $wfTrack->user_id = $wfTrack->allocated;
                            //$wfTrack->allocated = $user;
                            $wfTrack->assigned = 1;
                            $wfTrack->user_type = "App\Models\Auth\User";
                            $wfTrack->comments = NULL ;
                            $wfTrack->forward_date = NULL;
                            $wfTrack->status = 0;
                            $wfTrack->save();
                        }
                        $workflow->wf_done = 0;
                        $workflow->wf_done_date = NULL;
                        $workflow->save();
                    }
                    $this->initialize($incident, 1);
                } else {
                    //Incident is not Progressive
                    //-->restore the reserved status in from the flag
                    $incident->status = $incident->paidmanual_reserve_flag;
                    $incident->save();
                    //-->open any possible closed workflow by user/system
                    //-->check current status
                    $wf_module_id = NULL;
                    if ($incident->status == 2) {
                        $wf_module_id = (new WfModuleRepository())->legacyNotificationRejectionModule()[0];
                    } else {
                        $wf_module_id = (new WfModuleRepository())->legacyNotificationModule()[0];
                    }
                    $wfTrackInstance = $wfTrackRepo->getLastWorkflowModel($incident->id, $wf_module_id, 5);
                    if ($wfTrackInstance->count()) {
                        $wfTrack = $wfTrackInstance->first();
                        $wfTrack->user_id = $wfTrack->allocated;
                        //$wfTrack->allocated = $user;
                        $wfTrack->assigned = 1;
                        $wfTrack->user_type = "App\Models\Auth\User";
                        $wfTrack->comments = NULL ;
                        $wfTrack->forward_date = NULL;
                        $wfTrack->status = 0;
                        $wfTrack->save();
                    }
                    //update wf_done for the incident
                    $incident->wf_done_date = NULL;
                    $incident->wf_done = $incident->status;
                    $incident->save();
                }
            }

            $incident->user_set_manual_pay = NULL;
            $incident->paidmanual_reserve_flag = NULL;
            $incident->haspaidmanual = 0;
            $incident->save();

            return true;
        });
    }

    /**
     * @param Model $incident
     * @param $documentId
     * @param $send
     * @param $status
     * @return mixed
     */
    public function verifyOnlineDocument(Model $incident, $documentId, $send, $status)
    {
        return DB::transaction(function () use ($incident, $documentId, $send, $status) {
            $document = $incident->documents()->where(pg_mac_portal() . ".document_incident.document_id", $documentId)->first();
            if ($document) {
                $incident->documents()->updateExistingPivot($documentId, ["isverified" => $status, "user_verified" => access()->id()]);
                $uploaded = $document->pivot;
            } else {
                logger($documentId);
                DB::table(pg_mac_portal() . ".document_incident")
                    ->where('id', $documentId)
                    ->update(["isverified" => $status, "user_verified" => access()->id()]);
                $uploaded = DB::table(pg_mac_portal() . ".document_incident")->where('id', $documentId)->first();
            }
            if ($send) {
                //$pivot = $incident->documents()->where("documents.id", $documentId)->first()->pivot;
                if ($status == 1 And !$uploaded->isdmsposted) {
                    //Send Document to e-office
                    Log::info("Send Document to e-office");
                    //dispatch(new PostDocumentDms($incident, $documentId));
                    dispatch(new PostDocumentDms($incident, $uploaded->id));
                }
            }
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param $documentId
     * @param $send
     * @param $status
     * @return mixed
     */
    public function verifyBranchOnlineDocument(Model $incident, $documentId, $send, $status)
    {
        return DB::transaction(function () use ($incident, $documentId, $send, $status){
            //$incident->uploadedDocuments()->updateExistingPivot($documentId, ["isverified" => $status, "user_verified" => access()->id()]);
            DB::table(pg_mac_portal() . ".document_notification_report")
                ->where('id', $documentId)
                ->update(["isverified" => $status, "user_verified" => access()->id()]);
            //logger($documentId);
            if ($send) {
                //$pivot = $incident->documents()->where("documents.id", $documentId)->first()->pivot;
                //if ($status == 1 And !$pivot->isdmsposted) {
                    //Send Document to e-office
                    Log::info("Send Branch Online Document to e-office");
                    dispatch(new PostDocumentDms($incident, $documentId, 1));
                //}
            }
            return true;
        });
    }

    public function sendBranchOnlineDocumentToDms(Model $incident, $documentId)
    {
        return DB::transaction(function() use ($incident, $documentId) {
            $uploaded = DB::table(pg_mac_portal() . ".document_notification_report")->where('id', $documentId)->first();
            $document = (new DocumentRepository())->find($uploaded->document_id);
            $url = env('EOFFICE_APP_URL') . "/api/claim/adddocument";
            $documentFrom = $incident->employer->name;
            $createdDate = Carbon::parse($uploaded->updated_at)->format("Y-m-d");
            $referenceNumber = "B" . $uploaded->id; //differentiate from notification reports which were registered online ...
            $documentType = $uploaded->document_id;
            $fileNumber = $incident->id;
            $subject = $incident->file_subject_specific;
            //$file_path = $document->linkIncident($incident->id);
            $file_path = trim(preg_replace('/\s+/', '', notification_report_dir() . DIRECTORY_SEPARATOR . $incident->id . DIRECTORY_SEPARATOR . $uploaded->id  . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".")));;
            $b64Doc = base64_encode(file_get_contents($file_path));
            $postFields = "{ \"documentFrom\":\"{$documentFrom}\", \"createdDate\":\"{$createdDate}\", \"referenceNumber\":\"{$referenceNumber}\", \"subject\":\"{$subject}\", \"documentType\":\"{$documentType}\", \"fileNumber\":\"{$fileNumber}\", \"attachment\":\"{$b64Doc}\" }";
            $response = $this->sendPostJsonEoffice($url, $postFields);
            $data = json_decode($response, true);
            //Log::error($array = json_decode(json_encode($response), true));
            Log::info($response);
            Log::info($data);
            if ($data && count($data)) {
                if ($data['message'] == "SUCCESS") {
                    //update document_notification_report
                    $response = $this->updateDocument($fileNumber, [
                        'source' => 2,
                        'documents' => $document->id,
                        'fileType' => 3,
                        'dateOnDocument' => $createdDate,
                        'receivedDate' => $createdDate,
                        'createdAt' => $createdDate,
                        'folioNumber' => $data['folioNumber'],
                        'documentId' => $data['documentId'],
                        'subject' => $document->name,
                    ]);
                    //update isdmsposted on document_incident on portal
                    //$incident->documents()->updateExistingPivot($documentId, ["isdmsposted" => 1]);
                    DB::table(pg_mac_portal() . ".document_notification_report")
                        ->where('id', $documentId)
                        ->update(["isdmsposted" => 1]);
                }
            } else {
                $failed = $incident->id . "_" . $document->id . "_" . $uploaded->id;
                throw new JobException("Failed to post online branch notification document {$failed} to e-office");
            }
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param $documentId
     * @return bool
     * @throws JobException
     */
    public function sendDocumentToDms(Model $incident, $documentId)
    {
        return DB::transaction(function() use ($incident, $documentId) {
            //logger("I am sending document now");
            if ($incident->notificationReport()->count()) {
                $document = $incident->documents()->where(pg_mac_portal() . ".document_incident.document_id", $documentId)->first() ;
                if ($document) {
                    //logger("Loading Online Document Single");
                    $uploaded = $document->pivot;
                    if ($uploaded) {
                        $file_path = $document->linkIncident($incident->id);
                    }
                } else {
                    //logger("Loading Online Document Multiple");
                    $uploaded = DB::table(pg_mac_portal() . ".document_incident")->where('id', $documentId)->first();
                    $document = (new DocumentRepository())->find($uploaded->document_id);
                    logger($uploaded->id);
                    if ($uploaded) {
                        $file_path = $document->linkIncident($incident->id, $uploaded->id);
                    }
                }
                if ($uploaded) {
                    $url = env('EOFFICE_APP_URL') . "/api/claim/adddocument";
                    $documentFrom = $incident->employer->name;
                    $createdDate = Carbon::parse($uploaded->updated_at)->format("Y-m-d");
                    $referenceNumber = $uploaded->id;
                    $documentType = $uploaded->document_id;
                    $fileNumber = $incident->notification_report_id;
                    $subject = $incident->notificationReport->file_subject_specific;
                    try {
                        $b64Doc = base64_encode(file_get_contents($file_path));
                        $postFields = "{ \"documentFrom\":\"{$documentFrom}\", \"createdDate\":\"{$createdDate}\", \"referenceNumber\":\"{$referenceNumber}\", \"subject\":\"{$subject}\", \"documentType\":\"{$documentType}\", \"fileNumber\":\"{$fileNumber}\", \"attachment\":\"{$b64Doc}\" }";
                        $response = $this->sendPostJsonEoffice($url, $postFields);
                        $data = json_decode($response, true);
                        //Log::error($array = json_decode(json_encode($response), true));
                        Log::info($response);
                        Log::info($data);
                        if ($data && count($data)) {
                            if ($data['message'] == "SUCCESS") {
                                //update document_notification_report
                                $response = $this->updateDocument($fileNumber, [
                                    'source' => 2,
                                    'documents' => $document->id,
                                    'fileType' => 3,
                                    'dateOnDocument' => $createdDate,
                                    'receivedDate' => $createdDate,
                                    'createdAt' => $createdDate,
                                    'folioNumber' => $data['folioNumber'],
                                    'documentId' => $data['documentId'],
                                    'subject' => $document->name,
                                ]);
                                //update isdmsposted on document_incident on portal
                                //$incident->documents()->updateExistingPivot($documentId, ["isdmsposted" => 1]);
                                DB::table(pg_mac_portal() . ".document_incident")
                                    ->where('id', $uploaded->id)
                                    ->update(["isdmsposted" => 1]);
                            }
                        } else {
                            $failed = $incident->id . "_" . $document->id . "_" . $uploaded->id;
                            throw new JobException("Failed to post online incident document {$failed} to e-office");
                        }
                    } catch (\Exception $e) {
                        logger($e->getMessage());
                    }
                }
            }
            return true;
        });
    }

    /**
     * @param array $input
     * @param $mode
     * @return mixed
     */
    public function postChecklistAllocation(array $input, $mode)
    {
        return DB::transaction(function () use ($input, $mode) {
            $userRepo = new UserRepository();
            $incidentUserRepo = new IncidentUserRepository();
            $incidentUserRegionRepo = new IncidentUserRegionRepository();
            //incident user category for notification
            $iuc_cv_id = (new CodeValueRepository())->IUCNOTFCTN();
            switch ($mode) {
                case 1:
                    //save checklist users
                $checklist_users = $input['checklist_users'];
                foreach ($checklist_users as $user) {
                        //Log::info($user);
                    $incidentUserRepo->query()->updateOrCreate(
                        [
                            'user_id' => $user,
                            'iuc_cv_id' => $iuc_cv_id,
                        ],
                        []
                    );
                }
                    //remove region allocation for non-selected users if any
                $usersModel = $incidentUserRepo->query()->where('iuc_cv_id', $iuc_cv_id)->whereNotIn('user_id', $checklist_users);
                $incidentUserRegionRepo->query()->whereIn('incident_user_id', $usersModel->get()->pluck('id')->all())->delete();
                $usersModel->delete();
                break;
                case 2:
                    //save checklist region allocation
                $regionRepo = new RegionRepository();
                $districtRepo = new DistrictRepository();
                $specialRegion = [];
                $specialDistrict = [];
                /* start : collect all specialRegion keys */
                foreach ($input as $key => $value) {
                    if ( strpos( $key, 'regions' ) !== false ) {
                        $this_special = substr($key, 7);
                        if (!in_array($this_special, $specialRegion, true)) {
                            $specialRegion[] = $this_special;
                        }
                    }
                    if ( strpos( $key, 'districts' ) !== false ) {
                        $this_special = substr($key, 9);
                        if (!in_array($this_special, $specialDistrict, true)) {
                            $specialDistrict[] = $this_special;
                        }
                    }
                }
                    //logger($specialRegion);
                    //delete other regions for other users not posted
                $incidentUserRegionRepo->query()->whereNotNull('region_id')->whereNotIn('incident_user_id', function ($query) use ($iuc_cv_id, $specialRegion) {
                    $query->select('id')->from('incident_users')->whereIn('user_id', $specialRegion)->where('iuc_cv_id', $iuc_cv_id);
                })->delete();
                foreach ($specialRegion as $value) {
                    $user = $incidentUserRepo->query()->where('iuc_cv_id', $iuc_cv_id)->where('user_id', $value)->first();
                    $regions = $input['regions' . $value] ?? [];

                        //check regions already allocated to other user
                        $user->regions()->whereNotNull('region_id')->delete(); //substituted
                        if (count($regions)) {
                            $cleanIds = $regions;
                            /*
                            //pull all other regions not belonging to this user
                            $regionIds = $incidentUserRegionRepo->query()->whereIn('incident_user_id', function ($query) use ($value, $iuc_cv_id) {
                                $query->select('id')->from('incident_users')->whereNotIn('user_id', [$value])->where('iuc_cv_id', $iuc_cv_id);
                            })->get()->pluck('region_id')->all();
                            $cleanIds = array_diff($regions, $regionIds);
                            */
                            $toInsert = [];
                            //build regions array
                            if ($cleanIds) {
                                foreach ($cleanIds as $region) {
                                    $toInsert[] = ['region_id' => $region];
                                }
                                $user->regions()->createMany($toInsert);
                            }


                        }
                    }

                    //delete other districts for other users not posted
                    $incidentUserRegionRepo->query()->whereNotNull('district_id')->whereNotIn('incident_user_id', function ($query) use ($iuc_cv_id, $specialDistrict) {
                        $query->select('id')->from('incident_users')->whereIn('user_id', $specialDistrict)->where('iuc_cv_id', $iuc_cv_id);
                    })->delete();
                    foreach ($specialDistrict as $value) {
                        $user = $incidentUserRepo->query()->where('iuc_cv_id', $iuc_cv_id)->where('user_id', $value)->first();
                        $districts = $input['districts' . $value] ?? [];

                        $user->regions()->whereNotNull('district_id')->delete();
                        //check districts already allocated to other user
                        if (count($districts)) {
                            $cleanDistrictIds = $districts;

                            /*
                            //pull all other districts not belonging to this user
                            $allocations = $incidentUserRegionRepo->query()->whereIn('incident_user_id', function ($query) use ($value, $iuc_cv_id) {
                                $query->select('id')->from('incident_users')->whereNotIn('user_id', [$value])->where('iuc_cv_id', $iuc_cv_id);
                            })->get();
                            $districtIds = $allocations->pluck('district_id')->all();
                            */
                            $allocations = $incidentUserRegionRepo->query()->whereIn('incident_user_id', function ($query) use ($value, $iuc_cv_id) {
                                $query->select('id')->from('incident_users')->where('iuc_cv_id', $iuc_cv_id);
                            })->get();
                            $regionIds = $allocations->pluck('region_id')->all();
                            /*
                            //logger($regionIds);
                            $cleanDistrictIds = array_diff($districts, $districtIds);
                            */
                            $regions = $districtRepo->query()->whereIn('id', $cleanDistrictIds)->get()->pluck('region_id')->all();
                            //logger($regions);
                            $cleanRegionIds = array_diff($regions, $regionIds);
                            /*
                            //logger($cleanRegionIds);
                            //finally filter districts
                            */
                            $filteredDistricts = $districtRepo->query()->whereIn('id', $cleanDistrictIds)->whereIn('region_id', $cleanRegionIds)->get(); // ==> Restore this if filtering is enabled
                            $toInsert = [];
                            //build regions array
                            if ($filteredDistricts->count()) {
                                foreach ($filteredDistricts as $district) {
                                    $toInsert[] = [
                                        //'region_id' => $district->region_id,
                                        'region_id' => NULL,
                                        'district_id' => $district->id,
                                    ];
                                }
                                $user->regions()->createMany($toInsert);
                            }
                        }
                    }
                    break;
                }

                return true;
            });
}

    /**
     * @param null $incidentID
     * @return bool
     * @throws JobException
     */
    public function recheckPostingDocumentDms($incidentID = NULL)
    {
        $incidents = (new PortalIncidentRepository())->query()->whereHas("documents", function ($query) {
            $query->where([pg_mac_portal() . ".document_incident.isdmsposted" => 0, pg_mac_portal() . ".document_incident.isverified" => 1]);
        });
        //->whereNotNull("notification_report_id");
        if ($incidentID) {
            $incidents->where(pg_mac_portal() . ".incidents.id", $incidentID);
        }
        foreach ($incidents->get() as $incident) {
            //new implementation
            $uploads = DB::table(pg_mac_portal() . ".document_incident")->where(["isdmsposted" => 0, "isverified" => 1, 'incident_id' => $incident->id])->get();
            foreach ($uploads as $upload) {
                $this->sendDocumentToDms($incident, $upload->id);
            }
            //Log::error($incident->id);
            //TODO: first block to be removed after sometimes, get rid of referring to document_id
            $documents = $incident->documents()->where([pg_mac_portal() . ".document_incident.isdmsposted" => 0, pg_mac_portal() . ".document_incident.isverified" => 1])->get();
            foreach ($documents as $document) {
                $this->sendDocumentToDms($incident, $document->id);
            }
            //logger("I am inside loop of sending documents");
        }
        return true;
    }

    public function recheckPostingBranchOnlineDocumentDms($incidentID = NULL)
    {
        $incidents = $this->query()->whereHas("uploadedDocuments", function ($query) {
            $query->where([pg_mac_portal() . ".document_notification_report.isdmsposted" => 0, pg_mac_portal() . ".document_notification_report.isverified" => 1]);
        });
        if ($incidentID) {
            $incidents->where(pg_mac_main() . ".notification_reports.id", $incidentID);
        }
        foreach ($incidents->get() as $incident) {
            //Log::error($incident->id);
            //$documents = $incident->documents()->where([pg_mac_portal() . ".document_notification_report.isdmsposted" => 0, pg_mac_portal() . ".document_notification_report.isverified" => 1])->get();
            $uploads = DB::table(pg_mac_portal() . ".document_notification_report")->where(["isdmsposted" => 0, "isverified" => 1])->get();
            foreach ($uploads as $upload) {
                $this->sendBranchOnlineDocumentToDms($incident, $upload->id);
            }
        }
        return true;
    }

    /**
     * @param Model $notification_report
     * @param $wf_module_group_id
     * @throws GeneralException
     * Reverse approved / Rejected notification reports i.e. Reverse Approved to Rejected and vice versa
     * Workflow level should be pending level 1
     */
    public function reverseStatus(Model $notification_report, $wf_module_group_id){

        $notification_report->update(['status' => 0, 'is_reversed' => 1]);
        /*delete workflow*/
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $notification_report->id]);
        $workflow->wfTracksDeactivate();
    }

    /**
     * @param Model $notification_report
     * @param $wf_module_group_id
     * Approve appealed rejected Notification report and reverse to initial state for approval to claim.
     */
    public function approveRejectionAppeal(Model $notification_report, $input, $wf_module_group_id)
    {
        $user = access()->user();
        DB::transaction(function () use ($notification_report,$input,$wf_module_group_id,$user) {
            /*Reverse notification status*/
            $notification_report->update(['status' => 0, 'wf_done' => 0, 'wf_done_date' => null]);
            /*store appeal reason TODO: to update this call into the repository  */
            NotificationReportAppeal::query()->create(['reason' => $input['reason'], 'approved_by' => $user->id, 'notification_report_id' => $notification_report->id]);
            /*Deactivate workflow*/
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $notification_report->id]);
            $workflow->wfTracksDeactivate();
        });
    }

    /**
     * @param $employee_id
     * @param $incident_date
     * Auto prompt compliance for contributions not yet uploaded for this employee
     */
    public function promptForContribution(Model $notification_report)
    {
        $next_unit = 15 ;//compliance
        $employee  = new EmployeeRepository();
        $notificationContributionTrack = new NotificationContributionTrackRepository();
        $monthly_earning = $employee->getMonthlyEarningBeforeIncident($notification_report->employee_id, $notification_report->id)['monthly_earning'];
        if ($monthly_earning == 0)
        {
            $contrib_month = Carbon::parse($notification_report->incident_date)->subMonth(1)->format('M Y');
            $input = ['notification_report_id' => $notification_report->id,'from_user_id' =>$notification_report->user_id, 'unit_id'=> $next_unit];
            $track = $notificationContributionTrack->create($input);
        }
    }

    /*
 * update Nature of Incident
 */
    public function updateNatureOfIncident($id, $input)
    {
        $notification_report = $this->findOrThrowException($id);
        $notification_report->update(['nature_of_incident_cv_id' => $input['nature_of_incident']]);
        return $notification_report;
    }

    /*
 * verify
 */
    public function verify($id)
    {
        $notification_report = $this->findOrThrowException($id);
        $notification_report->update(['isverified'=> 1]);
        return $notification_report;
    }

    /*
 * update Status
     * Status: 0=> pending, 1 => approved, 2 => rejected
 */
    public function updateStatus($id, $status)
    {
        $notification_report = $this->findOrThrowException($id);
        $notification_report->update(['status'=> $status]);
        return $notification_report;
    }

    public function updateMonthlyEarning($id, $input)
    {
        DB::transaction(function () use ($id, $input) {
            $notification_report = $this->findOrThrowException($id);
            $notification_report->update(['monthly_earning' =>str_replace(",", "", $input['monthly_earning'])]);
        });

    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     * Update wf-done flag after workflow completion
     */
    public function updateWfDone($id) {
        $notification_report =  $this->findOrThrowException($id)->update(['wf_done' => 1]);
        return $notification_report;
    }

    /**
     * @param $id
     * @return mixed
     * Initial expense Member type
     */
    public function getResourceId($employee_id,$input) {

        $employee = $this->employees->findOrThrowException($employee_id);
//employer
        if ($input['initial_expense_member_type_id'] == 1) {
            $resource_id = $input['employer_id'];
        }
        //employee
        if ($input['initial_expense_member_type_id'] == 2) {
            $resource_id = $employee_id;
        }
        //wcf
        if ($input['initial_expense_member_type_id'] == 6) {
            $resource_id = $input['pay_through_insurance_id'];
        }

        return $resource_id;
    }

// medical expenses
    public function getMedicalExpenses($id)
    {
        return $this->query()->find($id)->medicalExpenses()->orderBy('id');
    }

    // accrual medical expenses
    public function getAccrualMedicalExpenses($id)
    {
        return $this->query()->find($id)->accrualMedicalExpenses()->orderBy('id');
    }

    /*Medical Expenses entities*/
    public function getMedicalExpenseEntities($id)
    {
        return DB::Table('medical_expenses')->select(['notification_report_id'])->distinct('notification_report_id')->where('notification_report_id', $id);
    }

    /*
     * INVESTIGATION ------======================
     */

    // ---end Investigation -----------------------------


    /**
     * @param $id
     * @return mixed
     * GET HEALTH PROVIDER SERVICES / REFERRALS
     */
    public function getHealthProviderServices($id)
    {
        return $this->notification_health_provider_services->query()->whereHas("notificationHealthProvider", function ($query) use ($id) {
            $query->where('notification_report_id', $id);
        })->get();

    }

    // ---END Health provider ------------------------------


    /**
     *
     * --CURRENT EMPLOYEEE STATE (DISABILITY / HEALTH STATES)===========================
     */

    public function storeCurrentEmployeeState($id, $input)
    {
        return DB::transaction(function () use ($id, $input) {
            // Store notification health state
//            $notification_health_state =   $this->notification_health_states->create($id,$input);
//            $notification_health_state_id   = $notification_health_state->id;

//            foreach ($input as $key => $value) {
//                if (strpos($key, 'health_state') !== false) {
//                    $health_state_checklist_id = substr($key, 12);
//                    if ($input['health_state' . $health_state_checklist_id] == 1) {
//                        $notification_health_state = $this->notification_health_states->create($health_state_checklist_id,$id, $input);
//                    }
//                }
//            }
            $input['notification_eligible_benefit_id'] = null;
            $this->notification_health_states->createOne($input['health_state_checklist_id'],$id, $input);
            //Store notification disability state

            foreach ($input as $key => $value) {
                if (strpos($key, 'disability_state') !== false) {
                    $disability_state_checklist_id = substr($key, 16);
                    if ($input['disability_state' . $disability_state_checklist_id] == 1) {
                        $notification_disability_state = $this->notification_disability_states->create($disability_state_checklist_id, $input);
                    }
                }
            }

        });

    }

    public function updateCurrentEmployeeState($id, $input)
    {

        return DB::transaction(function () use ($id, $input) {
            // update notification health state
//            foreach ($input as $key => $value) {
//                if (strpos($key, 'health_state') !== false) {
//                    $health_state_checklist_id = substr($key, 12);
//                    //                    if ($input['feedback'. $health_service_checklist_id]) {
//                    $notification_health_state = $this->notification_health_states->update($health_state_checklist_id,$id, $input);
////                    }
//                }
//            }
            $input['notification_eligible_benefit_id'] = null;
            $this->notification_health_states->updateOneLegacy($input['health_state_checklist_id'],$id, $input);

            //Update notification health provider services

            foreach ($input as $key => $value) {
                if (strpos($key, 'disability_state') !== false) {
                    $disability_checklist_id = substr($key, 16);
                    $notification_disability_state = $this->notification_disability_states->update($disability_checklist_id, $input);
                }
            }

        });

    }


    /**
     * @param $id
     * @return mixed
     * Get Disability State
     */
    public function getDisabilityState($id)
    {
        return $this->notification_disability_states->query()->whereHas('medicalExpense', function ($query) use ($id) {
            $query->where('notification_report_id', $id);
        })->orderBy('disability_state_checklist_id', 'asc')->get();

    }


    /**
     * @param $id
     * @return mixed
     * Get Health State
     */
    public function getHealthState($id)
    {
        return $this->notification_health_states->query()->where('notification_report_id', $id)->get();

    }

    //--End --Current Employee State-------------------

    /**
     * Validate if all necessary information are provided Approve / Elevate to claim=
     */

    public function validateApprovalToClaim($id)
    {
        $notification_report = $this->findOrThrowException($id);
        $incident_date = $this->findIncidentDate($id);
        // check if has contribution before incident
        $this->checkMonthlyEarning($id);
        //check if Investigated
        $this->checkIfInvestigated($id);
        //check if verified
        $this->checkIfVerified($id);
        //check if health services added (disease and accident)
        $this->checkIfHealthServicesAdded($id);
        //check if dependents added for death
        $this->checkIfDependentsAdded($id);
        // check if Bank details updated
        $this->checkIfBankDetailsAreUpdated($id);
        /*Check if all required documents are attached*/
        /* Should be considered to be restored in the future ... */
        //$this->checkIfDocumentsAreUploaded($id);

        /*check if tracks are completed*/
        //$this->checkIfContributionTracksAreCompleted($notification_report);

        /* Check if there is a medical expense with zero amount */
        $this->checkIfHasZeroMedicalExpense($id);

    }

    public function checkIfHasZeroMedicalExpense($id)
    {
        $count = $this->medical_expenses->query()->where(['notification_report_id' => $id, 'amount' => 0])->count();
        if ($count) {
            throw new GeneralException("There are medical some medical expense recorded with 0.00/= amount, please check ...");
        }
    }

    /**
     * @param $id
     * @return mixed
     * Check if monthly earning is filled
     */
    public function checkMonthlyEarning($id){
        $notification_report = $this->findOrThrowException($id);
        $incident_date = $this->findIncidentDate($id);

        // check if has contribution before incident
        $monthly_earning = $this->employees->getMonthlyEarningBeforeIncident($notification_report->employee_id, $notification_report->id);

        if ((!$monthly_earning['contrib_month']) || $monthly_earning['monthly_earning'] == 0  ){
            throw new GeneralException(trans('exceptions.backend.claim.monthly_earning_not_found'));
        }

    }



    /**
     * @param $id
     * @return mixed
     * Check if notification report is updated for incident need investigation
     */
    public function checkIfInvestigated($id){
        $notification_report = $this->findOrThrowException($id);
        if ($notification_report->need_investigation == 1){
            if($notification_report->investigation_validity == 0){
                throw new GeneralException(trans('exceptions.backend.claim.investigation_report_not_updated'));
            }
        }
    }


    /**
     * @param $id
     * @return mixed
     * Check if notification report is verified
     */
    public function checkIfVerified($id){
        $notification_report = $this->findOrThrowException($id);
        if ($notification_report->isverified == 0){
            throw new GeneralException(trans('exceptions.backend.claim.notification_report_not_verified'));
        }
    }

    /**
     * @param $id
     * @return mixed
     * Check if medical care services are updated for disease and accident
     */
    public function checkIfHealthServicesAdded($id){
        $notification_report = $this->findOrThrowException($id);
        //for disease and accident
        if ($notification_report->incident_type_id == 1 || $notification_report->incident_type_id == 2) {
            if (!$notification_report->notificationHealthProviderServices()->count()) {
                throw new GeneralException(trans('exceptions.backend.claim.health_services_not_added'));
            }
        }
    }


    /**
     * @param $id
     * @return mixed
     * Check if dependents are added
     * @throws GeneralException
     */
    public function checkIfDependentsAdded($id){
        $dependents = new DependentRepository();
        $notification_report = $this->findOrThrowException($id);
        $employee = $this->employees->findOrThrowException($notification_report->employee_id);
        //for death
        if (in_array($notification_report->incident_type_id , [3,4,5])) {
            if (!$notification_report->employee->dependents()->count()) {
                throw new GeneralException(trans('exceptions.backend.claim.dependents_not_added'));
            }else{
                $dependents->checkIfFuneralGrantPercentageIs100($employee);
            }
        }

    }



    /**
     * @param $id
     * @return mixed
     * Check if Bank details are updated
     */
    public function checkIfBankDetailsAreUpdated($id){
        $notification_report = $this->findOrThrowException($id);
        $exceptions = 0;
        $insurances = new InsuranceRepository();
        $employers = new EmployerRepository();
        //for employee
        if ($notification_report->incident_type_id == 1 || $notification_report->incident_type_id == 2) {
            if ( !$notification_report->employee->bank_branch_id || !$notification_report->employee->accountno) {
                $exceptions = 1;
            }
        }
        if ( !$notification_report->employer->bank_branch_id || !$notification_report->employer->accountno) {
            $exceptions = 1;
        }
        //medical expense
        $medical_expenses = $this->medical_expenses->query()->distinct()->select(['notification_report_id','member_type_id', 'resource_id'])->where('notification_report_id',$id)->get();//new
        foreach ($medical_expenses as $medical_expense) {
            //for insurances
            if ($medical_expense->member_type_id == 3){
                $insurance = $insurances->findOrThrowException($medical_expense->resource_id);
                if (!$insurance->bank_branch_id || !$insurance->accountno) {
                    $exceptions = 1;
                }
            }


            //for WCF
            if ($medical_expense->member_type_id == 1 && $medical_expense->resource_id == 4038){
                $employer = $employers->findOrThrowException(4038);
                if (!$employer->bank_branch_id || !$employer->accountno) {
                    $exceptions = 1;
                }
            }

        }
        //for dependents on Death
        if (in_array($notification_report->incident_type_id , [3,4,5])) {
            $dependents = $notification_report->employee->dependents()->where(function($query){
                $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
            })->get();
            foreach ($dependents as $dependent) {
                if ( !$dependent->bank_branch_id || !$dependent->accountno) {
                    $exceptions = 1;
                }
            }
        }

        if ($exceptions == 1){
            throw new GeneralException(trans('exceptions.backend.claim.bank_details_not_updated'));
        }
    }


    /**
     * @param $id
     * Check if all mandatory documents are attached
     */
    public function checkIfDocumentsAreUploaded($id){
        $notification_report = $this->findOrThrowException($id);
        /*Check all mandatory document */
        if(count($this->checkAllMandatoryDocuments($id))){
            throw new GeneralException(trans('exceptions.backend.claim.documents_not_uploaded'));
        }

        /*check for accident - If Applicable*/
        if($notification_report->incident_type_id == 1){
            $this->accidents->checkIfHasPoliceReport($notification_report);
        }
        /*check for Death - If Applicable*/
        if (in_array($notification_report->incident_type_id , [3,4,5])) {
            $this->death->checkIfHasSpouseMarriageDocument($notification_report);
        }
    }


    /**
     * @param $id
     * @return mixed
     * Check if all mandatory documents are uploaded
     */
    public function checkAllMandatoryDocuments($id){
        $notification_report = $this->findOrThrowException($id);
        $document = new DocumentRepository();
        $incidentTypeId = $notification_report->incident_type_id;
        $uploaded_documents = $notification_report->documents()->whereNotNull('document_notification_report.name')
        ->get()->pluck('id');

        $pending_documents = $document->query()->where('ismandatory', 1)->whereHas('documentGroup', function($query) use ($incidentTypeId) {
            $query->whereHas("incidentTypes", function ($query) use ($incidentTypeId) {
                $query->where("incident_type_id", $incidentTypeId);
            });
        })->whereNotIn('id', $uploaded_documents)->orderBy('document_group_id', 'asc')->get();

        return $pending_documents;
    }

    public function checkAllMandatoryDocumentsForLetter($id){
        $notification_report = $this->findOrThrowException($id);
        $document = new DocumentRepository();
        $incidentTypeId = $notification_report->incident_type_id;
        $uploaded_documents = $notification_report->documents()->whereNotNull('document_notification_report.name')
        ->get()->pluck('id');
        $uploaded_documents[] = 6; //Omit investigation questionnaire
        $uploaded_documents[] = 27; //Omit investigation questionnaire
        $pending_documents = $document->query()->where('ismandatory', 1)->whereHas('documentGroup', function($query) use ($incidentTypeId) {
            $query->whereHas("incidentTypes", function ($query) use ($incidentTypeId) {
                $query->where("incident_type_id", $incidentTypeId);
            });
        })->whereNotIn('id', $uploaded_documents)->orderBy('document_group_id', 'asc')->get();

        return $pending_documents;
    }


    /**
     * @param Model $notification_report
     * Check if all contributions tracks are completed
     */
    public function checkIfContributionTracksAreCompleted(Model $notification_report)
    {
        $tracks = new NotificationContributionTrackRepository();
        $tracks->checkIfTracksCompleted($notification_report);
    }





    /**
     * Get current Track
     */
    public function currentWfTrack($id){
        return  workflow([['wf_module_group_id' => 3, 'resource_id' => $id]])->currentWfTrack();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findIncidentDate($id)
    {
        $notification_report = $this->findOrThrowException($id);

        $incident_type_id = $notification_report->incident_type_id;
        if (in_array($incident_type_id, [1,4])) {
            //accident
            $incident_date = $notification_report->accident->accident_date;
        } elseif (in_array($incident_type_id , [2,5])) {
            //disease
            $incident_date = $notification_report->disease->diagnosis_date;
        } elseif (in_array($notification_report->incident_type_id , [3])) {
            //death
            $incident_date = $notification_report->death->death_date;
        }
        // convert date into first day of month
//        $incident_date = Carbon::create(Carbon::parse($incident_date)->format('Y'), Carbon::parse($incident_date)->format('m'), 1);
        return $incident_date;
    }

    /**
     * @param Model $incident
     * @return mixed
     */
    public function getIncidentDate(Model $incident)
    {
        $incident_type_id = $incident->incident_type_id;
        switch ($incident_type_id) {
            case 1:
            case 4:
                //accident
            $incident_date = $incident->accident->accident_date;
            break;
            case 2:
            case 5:
                //disease
            $incident_date = $incident->disease->diagnosis_date;
            break;
            case 3:
                //death
            $incident_date = $incident->death->death_date;
            break;
        }
        // convert date into first day of month
//        $incident_date = Carbon::create(Carbon::parse($incident_date)->format('Y'), Carbon::parse($incident_date)->format('m'), 1);
        return $incident_date;
    }

    /**
     * @param $id
     * @return mixed
     * Receipt date : Date notification reported to WCF Fund (Received date)
     */

    public function findReceiptDate($id)
    {
        $notification_report = $this->findOrThrowException($id);

        $incident_type_id = $notification_report->incident_type_id;
        if (in_array($incident_type_id, [1,4])) {
            //accident
            $receipt_date = $notification_report->accident->receipt_date;
        } elseif (in_array($incident_type_id, [2,5])) {
            //disease
            $receipt_date = $notification_report->disease->receipt_date;
        } elseif ($incident_type_id == 3) {
            //death
            $receipt_date = $notification_report->death->receipt_date;
        }
        // convert date into first day of month
//        $incident_date = Carbon::create(Carbon::parse($incident_date)->format('Y'), Carbon::parse($incident_date)->format('m'), 1);

        return $receipt_date;
    }

    public function updateDocument($file_number, array $input)
    {
        /**
         * e-office integration, receive document stored from e-office software, url e-office suppose to use [claim/notification_report/update_documents/{notification_report}] and parameters to send with index [documents] with datatype string, source 2 to signify that it comes from e-office.
         * MAC should return an acknowledgement to e-office
         * folioNumber
         * dateOnDocument
         * receivedDate
         * createdAt
         */
        $input['documents'] = explode_parameter($input['documents']);
        return DB::transaction(function () use ($file_number, $input) {
//          $notificationReport->documents()->sync([]);
            $documents = [];

            if (is_array($input['documents']) And count($input['documents'])) {
                foreach ($input['documents'] as $perm) {
                    if (is_numeric($perm)) {

                        //array_push($documents, [$perm => ['name' => 'e-office']]);
                        //$value = [$perm => ['name' => 'e-office']];
                        //$documents = array_merge($documents, [$perm => ['name' => 'e-office']]);
                        //$documents[] = [$perm => ['name' => $perm]];
                        if ($input['source'] == 2) {
                            //$notificationReport->documents()->syncWithoutDetaching([$perm => ['name' => 'e-office', 'description' => $input['subject'], 'eoffice_document_id' => $input['documentId']]]);
                            //$count = DB::table('document_notification_report')->where(['notification_report_id'=> $notificationReport->id, 'document_id' => $perm, 'eoffice_document_id' => $input['documentId']])->count();

                            $this->updateDocumentFromEofficePerFileType($file_number, $perm, $input);

                        } else {
                            array_push($documents, $perm);
                        }

                    }
                }
            }
            if ($input['source'] == 1) {
                //$notificationReport->documents()->syncWithoutDetaching($documents);
                foreach ($documents as $document) {
//                    $notificationReport->documents()->syncWithoutDetaching([$document => ['name' => 'mac']]);
                }
            } else {

            }
            //$notificationReport->documents()->syncWithoutDetaching($documents);
//          $notificationReport->attachDocuments($documents);
            return true;
        });
    }


    /**
     * @param $file_number
     * @param $document_id
     * @param array $input
     * General update documents from e-office according to fileType - Extension <NEW>
     */
    public function updateDocumentFromEofficePerFileType($file_number,$document_id, array $input)
    {
        if (!isset($input['fileType'])) {
            $input['fileType'] = 3;
        }
        switch ($input['fileType']){
            case 3:

                //TODO Need to confirm flag from e-office for manual files
//                $manual = (strpos($file_number, 'AB1/457/')) ? 1 : 0;
            $manual = isset($input['manual']) ? $input['manual'] : false;
            if(!$manual)
            {
                /*notification report*/
                $this->updateNotificationDocumentFromEoffice($file_number, $document_id, $input);
            } else {
                /*Manual notification report*/
                (new ManualNotificationReportRepository())->updateManualNotificationDocumentFromEoffice($file_number,$document_id, $input);
            }

            break;
            case 4:
            /*Monthly Payroll files*/
            $payroll_repo = new PayrollRepository();
            $payroll_repo->updatePayrollDocument($file_number, $document_id, $input );
            break;
            case 5:
            /*General files*/
            break;

            /*Pensioners/ Dependents files -  6/7*/
            case 6:
            case 7:
            $payroll_repo = new PayrollRepository();
            $payroll_repo->updateBeneficiaryDocument($file_number, $document_id, $input );
            break;
        }

    }
    /**
     * @param $document_id
     * @param array $input
     * Update notification documents from eoffice - Filetype = 3
     */
    public function updateNotificationDocumentFromEoffice($file_number,$document_id, array $input)
    {

        $count = DB::table('document_notification_report')->where(['notification_report_id'=> $file_number, 'eoffice_document_id' => $input['documentId']])->count();

        $dateOnDocument = isset($input['dateOnDocument']) ? $input['dateOnDocument'] : NULL;
        $receivedDate = isset($input['receivedDate']) ? $input['receivedDate'] : NULL;
        $createdAt = isset($input['createdAt']) ? $input['createdAt'] : NULL;
        $folioNumber = isset($input['folioNumber']) ? $input['folioNumber'] : NULL;

        if (!$count) {
            //create
            DB::table('document_notification_report')->insert(['notification_report_id'=> $file_number, 'document_id' => $document_id, 'name' => 'e-office', 'eoffice_document_id' => $input['documentId'],  'description' => $input['subject'], 'created_at' => Carbon::now(), 'doc_date' => $dateOnDocument, 'doc_receive_date' => $receivedDate, 'doc_create_date' => $createdAt, 'folio' => $folioNumber]);
        } else {
            //update
            DB::table('document_notification_report')->where(['notification_report_id'=> $file_number, 'eoffice_document_id' => $input['documentId']])->update(['document_id' => $document_id, 'name' => 'e-office',  'description' => $input['subject'], 'created_at' => Carbon::now(), 'doc_date' => $dateOnDocument, 'doc_receive_date' => $receivedDate, 'doc_create_date' => $createdAt, 'folio' => $folioNumber]);
        }
    }



    /**
     * @param $incident
     * @param array $input
     * @description e-Office Integration
     * @return mixed
     */
    public function deleteEofficeDocument($incident, array $input)
    {
        /**
         * e-office integration, receive document stored from e-office software, url e-office suppose to use [claim/notification_report/update_documents/{notification_report}] and parameters to send with index [documents] with datatype string, source 2 to signify that it comes from e-office.
         * MAC should return an acknowledgement to e-office
         *
         */
        $input['documents'] = explode_parameter($input['documents']);
        return DB::transaction(function () use ($incident, $input) {
            if (is_array($input['documents']) And count($input['documents'])) {
                foreach ($input['documents'] as $perm) {
                    if (is_numeric($perm)) {
                        if ($input['source'] == 2) {
                            DB::table('document_notification_report')->where(['notification_report_id'=> $incident, 'document_id' => $perm, 'eoffice_document_id' => $input['documentId']])->delete();
                        }
                    }
                }
            }
            return true;
        });
    }

    public function onlineOptionalDocs(Model $incident, $check, $documentId)
    {
        return DB::transaction(function () use ($incident, $check, $documentId) {
            if ($check) {
                //insert
                $incident->additionalDocs()->attach($documentId);
            } else {
                //remove
                $incident->additionalDocs()->detach($documentId);
            }
            return true;
        });
    }


    /**
     * @param $id
     * Auto Sync documents with E-office.
     */
    public function reloadDocuments($id){
        $autoSync = new NotificationDocEOfficeAutoSync($id);
        $autoSync->updateDocuments();
    }





//---------------

    /* start: Reports based on time Period, providing details on a monthly bases */

    public function getTotalMonthlyNotifications()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];

        /* Get Total before new Financial Year - Open Total Amount*/
        $prev_fin_year_end = $this->getPrevEndSummaryPeriod();
        $data_table = [];
        $count_open =  $this->query()->select('id')
        ->where("receipt_date", "<=",  $prev_fin_year_end)
        ->count();
        $total_open = $count_open;
        $period_open = Carbon::parse($prev_fin_year_end)->format("M y");
        $data_table[] = [$period_open,  $total_open ];
        /* end open */


        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $count = $this->query()->select('id')
            ->whereMonth("receipt_date", "=",  $month)
            ->whereYear("receipt_date", "=", $year)
            ->count();
            $period = $start->format("M y");
            $data[] = [$period, (int) $count]; //number_format( $sum , 2 , '.' , ',' )
            $data_table[] = [$period,  $count];
            $start->addMonth();
        }
        $sum = $this->query()
        ->where("receipt_date", ">=",  $this->getStartSummaryPeriod())
        ->where("receipt_date", "<=", $this->getEndSummaryPeriod())
        ->count();

        $overall_total = $sum + $total_open;
        $return['graph_notification_received'] = $data;
        $return['overall_total_notification_received'] =  $overall_total ;
        $return['notification_received_table'] = $data_table;
        return $return;
    }

    public function getTotalMonthlyClaim()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $count = $this->query()->select('id')
            ->has("claim")
            ->whereMonth("receipt_date", "=",  $month)
            ->whereYear("receipt_date", "=", $year)
            ->count();


            $period = $start->format("M y");
            $data = array_merge($data, [ (int) $count]); //$period . " : " . $sum
            $start->addMonth();
        }
        $count = $this->query()->select('id')
        ->has("claim")
        ->where("receipt_date", ">=",  $this->getStartSummaryPeriod())
        ->where("receipt_date", "<=", $this->getEndSummaryPeriod())
        ->count();
        $return['claims'] = implode("," , $data);
        $return['claims_total'] =  $count ;
        return $return;
    }

    public function getTotalMonthlyRejectedNotifications()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $count = $this->query()->select('id')
            ->where('status',2)
            ->where('wf_done',1)
            ->where(function ($query) use($month,$year) {
                $query->whereHas('accident', function ($query)  use($month,$year) {
                    $query->whereMonth("receipt_date", "=",  $month)
                    ->whereYear("receipt_date", "=", $year);
                })->orWhereHas('disease', function ($query)  use($month,$year) {
                    $query->whereMonth("receipt_date", "=",  $month)
                    ->whereYear("receipt_date", "=", $year);
                })->orWhereHas('death', function ($query)  use($month,$year) {
                    $query->whereMonth("receipt_date", "=",  $month)
                    ->whereYear("receipt_date", "=", $year);
                });})
            ->count();
            $period = $start->format("M y");
            $data = array_merge($data, [ (int) $count]); //$period . " : " . $sum
            $start->addMonth();
        }
        $count = $this->query()->select('id')
        ->where('status',2)
        ->where('wf_done',1)
        ->where(function ($query) {
            $query->whereHas('accident', function ($query)   {
                $query->where("receipt_date", ">=",  $this->getStartSummaryPeriod())
                ->where("receipt_date", "<=", $this->getEndSummaryPeriod());
            })->orWhereHas('disease', function ($query) {
                $query->where("receipt_date", ">=",  $this->getStartSummaryPeriod())
                ->where("receipt_date", "<=", $this->getEndSummaryPeriod());
            })->orWhereHas('death', function ($query) {
                $query->where("receipt_date", ">=",  $this->getStartSummaryPeriod())
                ->where("receipt_date", "<=", $this->getEndSummaryPeriod());
            });})
        ->count();
        $return['rejected'] = implode("," , $data);
        $return['rejected_total'] =  $count;
        return $return;
    }

    public function getTotalMonthlyProcessedNotifications()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $count = $this->query()->select('id')
            ->where('status',1)
            ->where('wf_done',1)
            ->where(function ($query) use($month,$year) {
                $query->whereHas('accident', function ($query)  use($month,$year) {
                    $query->whereMonth("receipt_date", "=",  $month)
                    ->whereYear("receipt_date", "=", $year);
                })->orWhereHas('disease', function ($query)  use($month,$year) {
                    $query->whereMonth("receipt_date", "=",  $month)
                    ->whereYear("receipt_date", "=", $year);
                })->orWhereHas('death', function ($query)  use($month,$year) {
                    $query->whereMonth("receipt_date", "=",  $month)
                    ->whereYear("receipt_date", "=", $year);
                });})
            ->count();
            $period = $start->format("M y");
            $data = array_merge($data, [ (int) $count]); //$period . " : " . $sum
            $start->addMonth();
        }
        $count = $this->query()->select('id')
        ->whereRaw("created_at between :start_date and :end_date and wf_done = 1 and status = 1", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])
        ->count();
        $return['processed'] = implode("," , $data);
        $return['processed_total'] =  $count ;
        return $return;
    }

    /**
     * @return array
     * get status summary
     */
    public function getNotificationReportStatusSummary()
    {
        $paid = $this->query()->select('id')->whereRaw("wf_done = 1 and status = 1 and wf_done_date between :start_date and :end_date", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])->count();

        $pending = $this->query()->select('id')->whereRaw("wf_done = 0 and created_at between :start_date and :end_date", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])->count();

        $rejected = $this->query()->select('id')->whereRaw("wf_done = 1 and status = 2 and  created_at between :start_date and :end_date", ['start_date' => $this->getStartSummaryPeriod(), 'end_date' => $this->getEndSummaryPeriod()])->count();

        return ['paid_claim' =>  $paid  , 'pending_claim' =>  $pending   , 'rejected_claim' =>  $rejected];

    }

    /* start: Reports based on time Period, providing details on a monthly bases by Incident Type*/

    public function getTotalMonthlyNotificationsByType($incident_type_id)
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];

        /* Get Total before new Financial Year - Open Total Amount*/
        $prev_fin_year_end = $this->getPrevEndSummaryPeriod();
        $data_table = [];
        $count_open =  $this->query()->select('id')
        ->where("receipt_date", "<=",  $prev_fin_year_end)
        ->where('incident_type_id', $incident_type_id)
        ->count();
        $total_open = $count_open;
        $period_open = Carbon::parse($prev_fin_year_end)->format("M y");
        $data_table[] = [$period_open,  $total_open ];
        /* end open */

        for ($x = 1; $x <= $this->getSummaryPeriodDiff(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $count = $this->query()->select('id')
            ->whereMonth("receipt_date", "=",  $month)
            ->whereYear("receipt_date", "=", $year)
            ->where('incident_type_id', $incident_type_id)
            ->count();
            $period = $start->format("M y");
            $data[] = [$period, (int) $count]; //number_format( $sum , 2 , '.' , ',' )
            $data_table[] = [$period,  $count ];
            $start->addMonth();
        }
        $sum = $this->query()
        ->where("receipt_date", ">=",  $this->getStartSummaryPeriod())
        ->where("receipt_date", "<=", $this->getEndSummaryPeriod())
        ->where('incident_type_id', $incident_type_id)
        ->count();

        $overall_total = $sum + $total_open;
        $return['graph'] = $data;
        $return['overall_total'] =  $overall_total;
        $return['table'] = $data_table;
        return $return;
    }

    /* end: Reports based on time Period, providing details on a monthly bases */

    public function getDeseasesByAgent($agent_id){
        // return $agent_id;
        $diseases = DB::table('code_values')
        ->select('disease_exposure_agents.name','disease_exposure_agents.id')
        ->where('code_values.id',$agent_id)
        ->join('disease_exposure_agents','disease_exposure_agents.code_value_id','=','code_values.id')
        ->get();
        return $diseases;
    }

    public function getDeseasesByOrgan($organ_id){
        // return $organ_id;
        $diseases = DB::table('code_values')
        ->select('disease_exposure_agents.name','disease_exposure_agents.id')
        ->where('code_values.id',$organ_id)
        ->join('disease_exposure_agents','disease_exposure_agents.code_value_id','=','code_values.id')
        ->get();
        return $diseases;
    }

    public function getAccident($accident_id){
        // return $organ_id;
        $accidents = DB::table('codes')
        ->select('code_values.name','code_values.id')
        ->where('codes.id',$accident_id)
        ->join('code_values','code_values.code_id','=','codes.id')
        ->get();
        return $accidents;
    }



    /*Post Osh Data*/
    public function postOshData($incident, array $input)
    {
        $return = DB::transaction(function () use ($incident, $input) {
            $incident->update([
//                'hcp_name' => $input['hcp_name'],
                'accident_cause_type_cv_id' => $input['accident_cause_type'] ?? NULL,
                'accident_cause_agency_cv_id' => $input['accident_cause_agency'] ?? NULL,
                'nature_of_incident_cv_id' => $input['injury_nature'] ?? NULL,
                'body_part_injury_cv_id' => $input['bodily_location'] ?? NULL,
            ]);

            /*Disease*/
            if($incident->incident_type_id == 2)
            {
                $incident->disease->update([
                    'name' => ($incident->wf_done == 0) ? ($input['disease_diagnosed'] ?? $incident->disease->name) : $incident->disease->name,
                    'disease_type' =>   $input['disease_type'] ?? null,
                    'disease_caused_by_agent' =>   $input['disease_agent'] ?? null,
                    'disease_caused_by_target_organ' =>   $input['disease_target_organ'] ?? null,
                ]);
            }
            /*sync hcp*/
            $this->syncWithHcp($incident, $input);

            return true;
        });
    }

    public function syncWithHcp(Model $incident, array $input)
    {
        if(isset($input['hcps'])){
            $hcps = $input['hcps'];
            $incident->hcps()->sync($hcps);
        }

    }



    /*Get Summary Mp computation Data used on payroll data migration*/
    public function getMpGeneralDataSummary(Model $incident)
    {
        $monthly_pension_payable = (new ClaimCompensationRepository())->findPayableMpForDeathIncident($incident);
        $derived_gme = (new EmployeeRepository())->getMonthlyEarningBeforeIncident($incident->employee_id,$incident->id )['monthly_earning'];
        $gme = (isset($incident->monthly_earning)) ? $incident->monthly_earning : $derived_gme;

        return ['gme' => $gme, 'monthly_pension' => $monthly_pension_payable];
    }

}