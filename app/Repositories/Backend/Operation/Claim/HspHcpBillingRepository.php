<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\HcpServiceAuthorization;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use function \FluidXml\fluidxml;
use Exception;
use GuzzleHttp\Exception\ConnectException as ConnectException;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use Log;

/**
 * Class PortalIncidentRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class HspHcpBillingRepository extends BaseRepository
{
    const MODEL = HcpServiceAuthorization::class;


    public function findOrThrowException($id)
    {
        $hsp_hcp = $this->query()->find($id);

        if (!is_null($hsp_hcp)) {
            return $hsp_hcp;
        }
        throw new GeneralException(trans('Billing not found!'));
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function getForDatatable()
    {
        $query = $this->query()->select([
            DB::raw("incident_types.name as incident_type"),
            DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as employee"),
            DB::raw("employers.name as employer"),
            DB::raw("notification_reports.filename"),
            "region","district","hsp_service_authorization.memberno as member_no",
            "hsp_service_authorization.authorizationNo as authorization_no",
            "hsp_service_authorization.id as Id",
            "employees.lastname",
            "employees.middlename",
            "employees.firstname",
        ])
        ->leftJoin("notification_reports", "notification_reports.id", "=", "hsp_service_authorization.notification_report_id")
        ->leftJoin("incident_types", "notification_reports.incident_type_id", "=", "incident_types.id")
        ->leftJoin("employers", "employers.id", "=", "hsp_service_authorization.employer_id")
        ->leftJoin("employees", "employees.id", "=", "hsp_service_authorization.employee_id");

        return $query->get();
    }

    public function getForBillingDataTable(){
        $query = DB::table('hsp_bill_summaries')->select("shdescription","price_difference",
            DB::raw("concat_ws('-', year, month) as date"),"total_amount_claimed",
            "total_files_claimed","total_amount_vetted","total_files_vetted",
            "batchno", "vetting_status","hsp_bill_summaries.id as billing_id")
        ->leftJoin("api_stakeholders", "api_stakeholders.id", "=", "hsp_bill_summaries.stakeholder_id");
        return $query;
    }

    public function getForVettingBillingDataTable($billing_id){
        $query = DB::table('hsp_bill_details')->select("memberno",
            "patient_names","wcf_authno","sh_authno","is_reviewed",
            "visit_date", "treatment_file","hsp_bill_details.id as billing_detail_id","bill_amount")
        ->where("hsp_summary_id", $billing_id);
        return $query;
    }

    public function getForAuthBillingDataTable($auth_no){
        $query = DB::table('hsp_bill_details')->select("memberno",
            "patient_names","wcf_authno","sh_authno","is_reviewed",
            "visit_date", "treatment_file","hsp_bill_details.id as billing_detail_id","bill_amount")
        ->where("wcf_authno", $auth_no);
        return $query;
    }

    public function loadMedicalPackages(){
        $facility_codes = DB::table('facility_codes')->get();
        $facilities = array();

        foreach ($facility_codes as $fcode) {
            dump($fcode->facility_code.' start save');
            $nhif_token = $this->getNhifToken();
            $facility_data = $this->getFacilityData($fcode->facility_code, $nhif_token);

            $facilities = [
                'facilityCode' => $fcode->id, 
                'PricePackage' => $facility_data['PricePackage'], 
            ];

            $this->saveFacilitiesToDb($facilities);
            dump($fcode->facility_code.' all saved');
            $facilities = array();

        }

    }

    public function saveFacilitiesToDb($data){
        $facility_code_id = $data['facilityCode'];

        // save price package
        foreach ($data['PricePackage'] as $key => $price) {
            DB::table('medical_price_packages')->updateOrInsert(
                [
                    'facility_code_id' => $facility_code_id,
                    'item_code' => $price['ItemCode']
                ],
                [
                    'price_code' => $price['PriceCode'],
                    'level_price_code' => $price['LevelPriceCode'],
                    'old_item_code' => $price['OldItemCode'],
                    'item_type_id' => $price['ItemTypeId'],
                    'item_name' => $price['ItemName'],
                    'Strength' => $price['Strength'],
                    'dosage' => $price['Dosage'],
                    'package_id' => $price['PackageID'],
                    'scheme_id' => $price['SchemeID'],
                    'facility_level_code' => $price['FacilityLevelCode'],
                    'unit_price' => $price['UnitPrice'],
                    'is_restricted' => $price['IsRestricted'],
                    'Maximum_quantity' => $price['MaximumQuantity'],
                    'Available_Levels' => $price['AvailableInLevels'],
                    'Practitioner_qalifications' => $price['PractitionerQualifications'],
                    'Is_active' => $price['IsActive']
                ]);
        }

    }

    public function getFacilityData($facility_code, $nhif_token){
        try {

            $client = new Client();
            $headers = [
                'Authorization' => 'Bearer ' . $nhif_token['access_token'],        
                'Accept'        => 'application/json',
            ];

            $url = '196.13.105.15/claimsserver/api/v1/Packages/GetPricePackageWithExcludedServices?FacilityCode='.$facility_code;
            $response = $client->request('GET', $url, [
                'headers' => $headers,
            ]);

            $response = $response->getBody()->getContents();
            $json = json_decode($response,true);

            return $json;
        }
        catch( ConnectException $e){
            Log::info('---- Connection error access token-----');
            Log::error(print_r($e->getMessage(),true));
            Log::info('-------------');
        }
        catch (ClientError $e) {
            Log::info('---------Client error access token-----');
            Log::error(print_r($e->getMessage(),true));
            Log::info('-------------');
        }
        catch (\Error $e) {
            Log::info('---------General error access token-----');
            Log::info($e);
            Log::error(print_r($e->getMessage(),true));
            Log::info('-------------');
        }
        catch( \ Exception $e){
            Log::info('---------Exception error -----');
            Log::info($e);
            Log::error(print_r($e->getMessage(),true));
            Log::info('-------------');
        }
        

    }

    public function getNhifToken()
    {

        try{

            $url = '196.13.105.15/claimsserver/token';
            $client = new Client();

            $response = $client->request('POST', $url, [
              'headers' => [
                'content-type' => 'application/x-www-form-urlencoded',
            ],'form_params' => [
                'grant_type' => 'password',
                'username' => 'integrationuser',
                'password' => 'nhif@2018'
            ],
        ]);

            $json = json_decode($response->getBody()->getContents(),true);

            return $json;


        }
        catch( ConnectException $e){
            Log::info('---- Connection error access token-----');
            Log::error(print_r($e->getMessage(),true));
            Log::info('-------------');
        }
        catch (ClientError $e) {
            Log::info('---------Client error access token-----');
            Log::error(print_r($e->getMessage(),true));
            Log::info('-------------');
        }
        catch (\Error $e) {
            Log::info('---------General error access token-----');
            Log::info($e);
            Log::error(print_r($e->getMessage(),true));
            Log::info('-------------');
        }
        catch( \ Exception $e){
            Log::info('---------Exception error -----');
            Log::info($e);
            Log::error(print_r($e->getMessage(),true));
            Log::info('-------------');
        }

    }

    public function getFacilitiesDataTable(){
        $facility_codes = DB::table('facility_codes')->select(['facility_code','name','shdescription','facility_codes.id'])
        ->leftJoin("api_stakeholders", "api_stakeholders.id", "=", "facility_codes.stakeholder_id");
        return $facility_codes;
    }

    public function getFacilityItemsDataTable($facility_code_id){
        $medical_price_packages = DB::table('medical_price_packages')
        ->select(['item_code','item_name','dosage','id','Maximum_quantity','unit_price'])->where('facility_code_id',$facility_code_id);
        return $medical_price_packages;
    }

    public function getFacility($service){
       $facility = DB::table('medical_price_packages')
       ->select('item_code','unit_price','item_name')
       ->join('facility_codes','facility_codes.id', '=', 'medical_price_packages.facility_code_id')
       ->where('facility_code',$service->facility_code)
       ->where('item_code', str_replace(",", "", $service->item_code))
       ->first();

       return $facility;
    }

    public function hasReviewed($detail_id){
        $detail = DB::table('hsp_bill_details')->where('id', $detail_id)->where('is_reviewed',true)->first();
        return $detail;
    }

    public function difference($first, $last){
       $diff = 0;
       $diff = $first - $last;
       return $diff;
    }

    public function fixedShBill($detail_id){
        $bill = 0;
        $detail = DB::table('hsp_bill_details')->select('bill_amount')->where('id', $detail_id)->first();
        $bill = $detail->bill_amount;
        return $bill;
    }

    public function recalculate($detail_id){
        $detail = DB::table('hsp_bill_details')->where('id', $detail_id)->first();
        $summary = DB::table('hsp_bill_summaries')->where('id', $detail->hsp_summary_id)->first();

        $result['total_amount_vetted'] = 0;
        $result['price_difference'] = 0;
        $result['total_files_vetted'] = 0;

        $details = DB::table('hsp_bill_details')->where('hsp_summary_id', $summary->id)->get();

        foreach ($details as $detail) {

            if ($detail->is_reviewed == true) {
                 $result['total_amount_vetted'] += $detail->vetted_amount;
                 $result['price_difference'] += $detail->price_difference;
                 $result['total_files_vetted'] = $result['total_files_vetted'] + 1;
            }
        }

        $update = DB::table('hsp_bill_summaries')->where('id', $summary->id)
        ->update([
            'total_amount_vetted' => $result['total_amount_vetted'],
            'price_difference' => $result['price_difference'],
            'total_files_vetted' => $result['total_files_vetted'],
        ]);
    }

}