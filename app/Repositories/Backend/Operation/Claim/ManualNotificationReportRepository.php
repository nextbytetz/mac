<?php

namespace App\Repositories\Backend\Operation\Claim;


use App\Exceptions\GeneralException;
use App\Exceptions\JobException;
use App\Models\Operation\Claim\Accident;
use App\Models\Operation\Claim\AccidentWitness;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Operation\Claim\Witness;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Reporting\ReportRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\UploadAttributeRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\Request;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_NumberFormat;

class ManualNotificationReportRepository extends  BaseRepository
{
    use FileHandler, AttachmentHandler;

    const MODEL = ManualNotificationReport::class;

    protected $document_name = 'manual_notification_reports.xlsx';

    public function __construct()
    {
        if(env('TESTING_MODE') == 1)
        {
            /*ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);*/
        }
    }

    public function uploadedFileDir()
    {
        return manual_files_dir() . DIRECTORY_SEPARATOR . $this->document_name;
    }


    public function uploadedFileUrl()
    {
        return manual_files_url() . DIRECTORY_SEPARATOR . $this->document_name;
    }

    /*
     * create new
     */

    public function create(array $input)
    {

        return DB::transaction(function () use ($input) {
            $this->query()->create([
//                'case_no' => $input['case_no'],
//                'employee_name' => $input['employee_name'],
//                'employer_name' => $input['employer_name'],
//                'incident_type_id' => $input['incident_type_id'],
//                'incident_date' => $input['incident_date'],
//                'reporting_date' => $input['reporting_date'],
//                'notification_date' => $input['notification_date'],
//                'date_of_mmi' => $input['date_of_mmi'],
//                'user_id' => $input['user_id'],
//                'upload_error' => $input['upload_error']
            ]);
        });
    }



    /*
     * update
     */
    public function update($input) {

        return DB::transaction(function () use ($input) {
            $notification = $this->query()->where('case_no', $input['case_no'])->first();
            /*update if not yet synced*/
            if ($notification->issynced == 0)
                $notification->update([
//                    'case_no' => $input['case_no'],
//                    'employee_name' => $input['employee_name'],
//                    'employer_name' => $input['employer_name'],
//                    'incident_type_id' => $input['incident_type_id'],
//                    'incident_date' => $input['incident_date'],
//                    'reporting_date' => $input['reporting_date'],
//                    'notification_date' => $input['notification_date'],
//                    'date_of_mmi' => $input['date_of_mmi'],
//                    'user_id' => $input['user_id'],
//                    'upload_error' => $input['upload_error']
                ]);
        });
    }


    /**
     * @param $id
     * @param array $input
     * @return mixed
     * Create employee from Manual notification files and auto sync
     */
    public function createEmployee($id, array $input)
    {
        return DB::transaction(function () use ($id, $input) {
            $manual_notification = $this->find($id);
            $employees_repo = new EmployeeRepository();
            $basicpay = (isset($input['basicpay']) And ($input['basicpay'])) ? str_replace(",", "", $input['basicpay']) : 0;
            $allowance = (isset($input['allowance']) And ($input['allowance'])) ? str_replace(",", "", $input['allowance']) : 0;
            $data = [
                'firstname' =>  trim(preg_replace('/\s+/', '', $input['firstname']))  ,
                'middlename' =>  trim(preg_replace('/\s+/', '', $input['middlename']))  ,
                'lastname' => trim(preg_replace('/\s+/', '', $input['lastname']))  ,
                'dob' => standard_date_format($input['dob']),
                'gender_id' => $input['gender_id'],
                'marital_status_cv_id' => array_key_exists('marital_status_cv_id', $input) ? $input['marital_status_cv_id'] : null,
                'employee_category_cv_id' => $input['employee_category_cv_id'],
                'job_title_id' => array_key_exists('job_title_id', $input) ? $input['job_title_id'] : null,
                'identity_id' => array_key_exists('identity_id', $input) ? $input['identity_id'] : null,
                'id_no' => $input['id_no'],
                'nid' => $input['nid'],
                'basicpay' => $basicpay,
                'grosspay' => $employees_repo->findGrosspay($basicpay,$allowance),
                'department' => $input['department'],
                'email' => $input['email'],
                'phone' => (isset($input['phone']) And ($input['phone'])) ? phone_255($input['phone'])  : null,
                'fax' => $input['fax'],
                'country_id' => $input['country_id'],
                'location_type_id' =>array_key_exists('location_type_id', $input) ? $input['location_type_id'] : null,
                'street' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['street'] : null) : null,
                'road' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['road'] : null) : null,
                'plot_no' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['plot_no'] : null) : null,
                'block_no' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['block_no'] : null) : null,
                'unsurveyed_area' =>  array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 2) ? $input['unsurveyed_area'] : null) : null,
                'surveyed_extra' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['surveyed_extra'] : null) : null,
                'memberno' => 0,
                'user_id' =>  access()->id(),
            ];
            if ($input['country_id'] != 1) {
                $data['residence_permit_no'] = $input['residence_permit_no'];
                $data['work_permit_no'] = $input['work_permit_no'];
                $data['passport_no'] = $input['passport_no'];
            }

            $employee = $employees_repo->query()->create($data);
            /* save member no */
            $employee_id = $employee->id;
            $employee->memberno = checksum($employee_id, sysdefs()->data()->employee_number_length);
            $employee->save();
            /*attach employers */
            $employees_repo->attachEmployers($employee, $input);

            /*Auto sync member */
            $sync_data = ['employee' => $employee_id, 'employer' => $input['employer']];
            $this->updateSyncMemberStatus($id, $sync_data);

        });
    }





    /**
     * @param $id
     * @param array $input
     * @return mixed
     * Complete registration of manual file
     */
    public function completeRegistration($id, array $input)
    {
        return DB::transaction(function () use ($id, $input) {
            $user_id  = access()->id();
            $notification = $this->find($id);
            /*update if not yet synced*/
            if ($notification->issynced == 0)
            {
                $notification->update([
                    'incident_type_id' => $input['incident_type'],
                    'incident_date' => (isset( $input['incident_date'])) ? standard_date_format( $input['incident_date']) : null,
                    'reporting_date' =>  (isset( $input['reporting_date'])) ? standard_date_format( $input['reporting_date']) : null,
                    'notification_date' => (isset( $input['notification_date'])) ? standard_date_format( $input['notification_date']) : null,
                    'date_of_mmi' =>  (isset( $input['date_of_mmi'])) ? standard_date_format( $input['date_of_mmi']) : null,
                    'user_id' => $user_id,
                    'man_pd' =>array_key_exists('pd', $input) ?  $input['pd'] : null,
                    'issynced' => 1
                ]);
            }

            /*Save pensioner for accident and disease*/
            if($input['incident_type'] !=3){
                $pensioner = $this->savePensioner($id, $input);
                $this->updateManualPayrollMember($input,$pensioner->id);
            }

        });
    }

    /**
     * @param array $input
     * Save
     */
    public function savePensioner($id,array $input)
    {

        $pensioner_repo = new PensionerRepository();
        $notification = $this->find($id);
        $input_data = [
            'resource_id' => $notification->employee_id,
            'benefit_type_id' => 3,
            'compensation_payment_type_id' => 3,
            'monthly_pension_amount' =>  isset($input['mp'])  ? str_replace(",", "", $input['mp']) : null,
            'recycl' => null,
            'pay_period' =>  null,
            'bank_id' => $input['bank'],
            'bank_branch_id' => isset($input['bank_branch']) ? $input['bank_branch'] : null,
            'accountno' => $input['accountno'],
            'manual_notification_report_id' => $id,
            'ispaid' => (isset($input['ispaid'])) ? $input['ispaid'] : 0,
            'dob' =>  isset($input['dob']) ? standard_date_format($input['dob']) : null,
            'phone' =>  isset($input['phone']) ? $input['phone'] : null,
        ];
        $pensioner =  $pensioner_repo->createManualPensioner($input_data);
        return $pensioner;
    }

    /**
     * @param $id
     * @param array $input
     * Update manual payroll members
     */
    public function updateManualPayrollMember(array $input, $epnsioner_id){

        $member =   DB::table('manual_payroll_members')->where('id', $input['pensioner_id'])->update([
            'monthly_pension' =>  isset($input['mp'])  ? str_replace(",", "", $input['mp']) : null,
            'dob' =>(isset( $input['dob'])) ? standard_date_format( $input['dob']) : null,
            'bank_id' => $input['bank'],
            'issynced' => 1,
            'resource_id' => $epnsioner_id,
            'ispaid' => isset($input['ispaid']) ? $input['ispaid'] : null,
            'hasarrears' => isset($input['hasarrears']) ? $input['hasarrears'] : 0,
            'pending_pay_months' => isset($input['pending_pay_months']) ? $input['pending_pay_months'] : 0,
            'remark' => isset($input['remark']) ? $input['remark'] : null,
        ]);
        return $member;
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postUpdateInfo(Model $incident, array $input)
    {
        $return = DB::transaction(function () use ($incident, $input) {
            $user_id = access()->id();
            $reportRepo = new ReportRepository();
            $incident->update([
                'incident_type_id' => $input['incident_type_id'],
                'reporting_date' => $input['reporting_date'],
                'registration_date' => $input['registration_date'],
                'status_cv_id' => $input['status_cv_id'],
                'incident_date' => $input['incident_date'],
                'notification_date' => $input['notification_date'],
                'district_id' => $input['district_id'],
                'reject_reason_cv_id' => $input['reject_reason_cv_id'],
                'isupdated' => 1,

//                'user_id' => $user_id,
                'last_updated_by' => access()->id(),
            ]);

            //referesh report
            $reportRepo->refreshConfigurables(30);
            return true;
        });
        return $return;
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postManualPayment(Model $incident, array $input)
    {
        $return = DB::transaction(function () use ($incident, $input) {
            switch ($incident->incident_type_id) {
                case 1:
                case 2:
                    //Accident
                    //Disease
                    $incident->man_mae = $input['man_mae'];
                    $incident->man_pd = $input['man_pd'];
                    $incident->man_ppd = $input['man_ppd'];
                    //$incident->man_ptd = $input['man_ptd'];
                    $incident->man_ttd = $input['man_ttd'];
                    $incident->man_tpd = $input['man_tpd'];
                    break;
                case 3:
                    //Death
                    $incident->man_funeral_grant = $input['man_funeral_grant'];
                    break;
            }
            $incident->man_gme = $input['man_gme'] ?? $incident->man_gme;
            $incident->last_updated_by = access()->id();
            $incident->save();
            return true;
        });
        return $return;
    }



    /*Post Osh Data*/
    public function postOshData($incident, array $input)
    {
        $return = DB::transaction(function () use ($incident, $input) {
            $incident->update([
//                'hcp_name' => $input['hcp_name'],
                'accident_cause_type_cv_id' => $input['accident_cause_type'] ?? NULL,
                'accident_cause_agency_cv_id' => $input['accident_cause_agency'] ?? NULL,
                'injury_nature_cv_id' => $input['injury_nature'] ?? NULL,
                'bodily_location_cv_id' => $input['bodily_location'] ?? NULL,
                'disease_diagnosed' => $input['disease_diagnosed'] ?? NULL,
                'disease_type' => $input['disease_type'] ?? NULL,
                'disease_agent' => $input['disease_agent'] ?? NULL,
                'disease_target_organ' => $input['disease_target_organ'] ?? NULL,
                'day_off' => $input['day_off'] ?? NULL,
                'light_duties' => $input['light_duties'] ?? NULL,
                'man_pd' => $input['pd'] ?? NULL,
                'rehabilitation' => $input['rehabilitation'] ?? NULL,
                'isupdate_osh' => 1,
                'last_updated_by' => access()->id(),
            ]);

            /*sync hcp*/
            $this->syncWithHcp($incident, $input);

            return true;
        });
    }

    public function syncWithHcp(Model $incident, array $input)
    {
        $hcps = $input['hcps'];
        $incident->hcps()->sync($hcps);
    }



    /*Reset sync member status to 0 */
    public function resetSyncMemberStatus($id)
    {
        $notification = $this->find($id);
        $notification->update(['ismembersynced' => 0]);
    }


    /*update member sync status*/
    public function updateSyncMemberStatus($id, array $input)
    {
        $notification = $this->find($id);
        $issynced = 0;
        if($notification->has_payroll == 1)
        {
            $issynced = 0;
        }else{
            $issynced = 1;
        }
        $notification->update(['ismembersynced' => 1, 'issynced' => $issynced, 'employee_id' => $input['employee'] , 'employer_id' => $input['employer']]);
    }


    /*Destroy notification*/
    public function destroy($id)
    {
        $notification = $this->find($id);
        $notification->delete();
    }

    /**/
    public function updateHasPayrollFlag($manual_notification_report, $status)
    {
        $manual_notification_report->update([
            'has_payroll' => $status
        ]);
    }

    /*Update attendant who will work on the file*/
    public function updateAttendantForThisFile($manual_notification_report,$user_id, $action_type)
    {
        $user_id = ($action_type == 1) ? $user_id : null;
        $manual_notification_report->update([
            'attended_by' => $user_id,
        ]);
    }


    /**
     * @param $document_id
     * @param array $input
     * Update notification documents from eoffice - Filetype = 3
     */
    public function updateManualNotificationDocumentFromEoffice($file_number,$document_id, array $input)
    {
//        $file_number = str_replace('AB1/457/', '', $file_number);
        $count = DB::table('document_manual_notification_report')->where(['manual_notification_report_id'=> $file_number, 'eoffice_document_id' => $input['documentId']])->count();
        $dateOnDocument = isset($input['dateOnDocument']) ? $input['dateOnDocument'] : NULL;
        $receivedDate = isset($input['receivedDate']) ? $input['receivedDate'] : NULL;
        $createdAt = isset($input['createdAt']) ? $input['createdAt'] : NULL;
        $folioNumber = isset($input['folioNumber']) ? $input['folioNumber'] : NULL;

        if (!$count) {
            //create
            DB::table('document_manual_notification_report')->insert(['manual_notification_report_id'=> $file_number, 'document_id' => $document_id, 'name' => 'e-office', 'eoffice_document_id' => $input['documentId'],  'description' => $input['subject'], 'created_at' => Carbon::now(), 'doc_date' => $dateOnDocument, 'doc_receive_date' => $receivedDate, 'doc_create_date' => $createdAt, 'folio' => $folioNumber]);
        } else {
            //update
            DB::table('document_manual_notification_report')->where(['manual_notification_report_id'=> $file_number, 'eoffice_document_id' => $input['documentId']])->update(['document_id' => $document_id, 'name' => 'e-office',  'description' => $input['subject'], 'created_at' => Carbon::now(), 'doc_date' => $dateOnDocument, 'doc_receive_date' => $receivedDate, 'doc_create_date' => $createdAt, 'folio' => $folioNumber]);
        }
    }


    /*Update status*/
    public function updateStatus(ManualNotificationReport $manualNotificationReport)
    {
        $manualNotificationReport->update([
            ''
        ]);
    }


    /*Get for datatable*/
    public function getForDataTable()
    {
        return $this->query();
    }

    public function getAllForPayrollDataTable()
    {
        return $this->query()->where('has_payroll', 1);
    }

    /**\
     * @param array $input
     * Upload bulk files from excel
     */
    public function saveUploadedDocumentFromExcel(array $input)
    {
        DB::transaction(function () use ($input) {
            $base = manual_files_dir();
            $file = $this->uploadedFileDir();
            $request_file_name = 'document_file';
            /*check if exist*/
            if (request()->hasFile($request_file_name)) {
                /* If has file: remove and replace */
                if (file_exists($file)) {
                    unlink($file);
                }
                /*Save*/
                $document_name = '';
                $this->saveDocumentGeneral($request_file_name, $this->document_name, $base);
                /*Upload manual files into table*/
                $this->uploadBulkManualFiles();
            }
            /* end replace if exist*/

        });
    }

    /**
     * @param array $input
     * Upload files from save excel document
     */
    public function uploadBulkManualFiles()
    {
//        try {
        $user_id = access()->id();
        $uploaded_file = $this->uploadedFileDir();
        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($uploaded_file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();

        $verifyArr = ['employee_name','employer_name','case_no', 'incident', 'incident_date','reporting_date','notification_date', 'date_of_mmi'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new GeneralException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }

        }
        /** end : Check if all excel headers are present */

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($uploaded_file)
            ->chunk(250, function($result) use($user_id, $uploaded_file)  {
                $rows = $result->toArray();
                try {

                    //let's do more processing (change values in cells) here as needed
                    //$counter = 0;
                    foreach ($rows as $row) {
                        /* Changing date from excel format to unix date format ... */
                        $row['incident_date'] = PHPExcel_Style_NumberFormat::toFormattedString($row['incident_date'], 'YYYY-MM-DD');
                        $row['reporting_date'] = PHPExcel_Style_NumberFormat::toFormattedString($row['reporting_date'], 'YYYY-MM-DD');
                        $row['notification_date'] = PHPExcel_Style_NumberFormat::toFormattedString($row['notification_date'], 'YYYY-MM-DD');
                        $row['date_of_mmi'] = PHPExcel_Style_NumberFormat::toFormattedString($row['date_of_mmi'], 'YYYY-MM-DD');

                        /* start: Validating row entries */
                        $error_report = null;
                        $error = 0;
                        foreach ($row as $key => $value) {
                            if (trim($key) == 'incident_date') {
                                if (check_date_format($value) == 0 && isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            } elseif (trim($key) == 'reporting_date') {
                                if (check_date_format($value) == 0 && isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            } elseif (trim($key) == 'notification_date') {
                                if (check_date_format($value) == 0 && isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            } elseif (trim($key) == 'date_of_mmi') {
                                if (check_date_format($value) == 0 && isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            } elseif (in_array(trim($key), ['case_no'], true)) {
                                if (!is_numeric($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }

                            } elseif (trim($key) == 'employee_name') {
                                if (!isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                                }
                            } elseif (trim($key) == 'employer_name') {
                                if (!isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                                }
                            }elseif (in_array(trim($key), ['pd'], true)) {
                                if (!is_numeric($value) && isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            }
                        }
                        $incident_id = null;
                        $incident = strtolower(trim($row['incident']));
                        if ($incident == 'occupational accident' || $incident == 'occupation accident') {
                            $incident_id = 1;
                        } elseif ($incident == 'occupational disease' || $incident == 'occupation disease') {
                            $incident_id = 2;
                        } elseif ($incident == 'occupational death' || $incident == 'occupation death') {
                            $incident_id = 3;
                        }
                        $pd = (isset($row['pd'])) ? trim($row['pd']) : null;
                        /* end: Validating row entries */
                        /* insert into db */
                        $data = ['employer_name' => trim($row['employer_name']), 'employee_name' => trim($row['employee_name']), 'case_no' => trim($row['case_no']), 'incident_type_id' => $incident_id, 'incident_date' => $row['incident_date'], 'reporting_date' => $row['reporting_date'], 'notification_date' => $row['notification_date'], 'date_of_mmi' => $row['date_of_mmi'], 'user_id' => $user_id, 'upload_error' => $error_report,'pd' =>$pd];
                        /*Create notification report*/
                        if ($this->checkIfCaseNoDoesNotExist(trim($row['case_no']), $incident_id)) {
                            $this->query()->create($data);
                        }else{
                            $this->query()->where('case_no', trim($row['case_no']))->where('incident_type_id', $incident_id)->where('issynced',0)->update($data);
                        };
                    }
                } catch (Exception $e) {
                    // an error occurred
                    $error_message = $e->getMessage();
//                    $this->saveExceptionMessage($employer, $error_message);
                    Log::info(print_r($error_message,true));
                }


            }, true);

    }




    /*Check if case no exist*/
    public function checkIfCaseNoDoesNotExist($case_no, $incident_id)
    {
        $check = $this->query()->where('case_no', $case_no)->where('incident_type_id', $incident_id)->count();

        if($check > 0){
            return false;
        }else{
            return true;
        }

    }

    /**
     * @return array
     * Pending summary count
     */
    public function getPendingSummary()
    {
        $pending_sync_death = $this->getAllForPayrollDataTable()->where('manual_notification_reports.incident_type_id', 3)->where('issynced', 0)->count();
        $pending_sync_others = $this->getAllForPayrollDataTable()->where('manual_notification_reports.incident_type_id','<>', 3)->where('manual_notification_reports.issynced', 1)->where('manual_notification_reports.has_payroll', 1)
            ->join('manual_payroll_members as mem', function ($join){
                $join->on('mem.case_no', 'manual_notification_reports.case_no')->whereRaw("manual_notification_reports.incident_type_id = mem.incident_type_id");
            })->where('mem.issynced', 0)
            ->count();
        $pending_sync_member = $this->getAllForPayrollDataTable()->where('ismembersynced',0)->count();

        return ['pending_sync' => $pending_sync_death + $pending_sync_others, 'pending_sync_member' => $pending_sync_member];
    }

    public function getManualKeysForUpload()
    {
        $for_mappings = [
            'employee_name' => 'Employee Name',
            'incident_type' => 'Incident Type',
            'case_no' => 'Claim No',
            'employer_name' => 'Employer Name',
            'district' => 'Notification District',
            'claim_status' => 'Claim Status',
            'reject_reason' => 'Reject Reason',
            'incident_date' => 'Incident Date',
            'reporting_date' => 'Employer Reporting Date',
            'notification_date' => 'WCF Reporting Date',
            'registration_date' => 'Incident Registration Date',
        ];
        return $for_mappings;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function createPreviewFromJson(array $input)
    {
        $return = DB::transaction(function () use ($input) {
            //logger($input['json_data']);

            $view = '';
            $variables = '';
            //logger($input['json_data']);
            if ($input['json_data']) {

                $cv_id = (new CodeValueRepository())->FLUTYP01();
                $collection = collect(json_decode($input['json_data'], true));

                if ($collection->count()) {

                    $attribute = (new UploadAttributeRepository())->query()->create([
                        'data' => $input['json_data'],
                        'reference' => $input['reference'],
                        'file_upload_type_cv_id' => $cv_id,
                    ]);

                    $samples = $collection->take(3);
                    //logger($samples);
                    $keys = array_keys($collection->first());
                    $keysforselect = [];
                    foreach ($keys as $key) {
                        $keysforselect[$key] = $key;
                    }

                    $for_mappings = $this->getManualKeysForUpload();

                    $view = view('backend/operation/claim/manual_notification_report/upload_all/includes/file_preview')
                        ->with('samples', $samples)
                        ->with('keys', $keys)
                        ->with('for_mappings', $for_mappings)
                        ->with('keysforselect', $keysforselect);

                }

            }

            return ['view' => (string) $view, 'variables' => (string) $variables];

        });
        //logger($return);
        return $return;
    }

    public function createFromJson(array $input)
    {
        $return = DB::transaction(function () use ($input) {
            $cv_id = (new CodeValueRepository())->FLUTYP01();
            $has_payroll = $input['has_payroll'];
            //$input2 = $request->only(array_keys($this->getManualKeysForUpload()));
            //Log::info(print_r($input,true));
            //Log::info($input2);

            $attributeModel = (new UploadAttributeRepository())->query()->where('reference', $input['reference']);
            if ($attributeModel->count()) {
                $attribute = $attributeModel->first();
                $data = json_decode($attribute->data, true);
                $collections = collect($data);
                $user_id = access()->id();
                $notifications = [];
                $counter = 1;

                /* $collections = $collections->map(function ($item, $key) {
                     return trim($key);
                 });*/
                //logger($collections);

                foreach ($collections as $collection_value) {
                    //check for date inputs
                    $collection = [];
                    //Log::info($collection_value);
                    foreach ($collection_value as $key => $value) {
                        //Log::info($key);
                        $collection[trim($key)] = $value;
                    }
                    //Log::info($collection);

                    foreach ($input as $key => $value) {
                        //Log::info(print_r($collection,true));
                        if(array_key_exists($value, $collection)) {
                            //logger($value);
                            $collection[$value] = trim($collection[$value]);
                            $collection[$value] = str_replace("'", "''", $collection[$value]);
                            if (strpos($key, 'date') !== false) {
                                try {
                                    if ($collection[$value]) {
                                        $date_string = PHPExcel_Style_NumberFormat::toFormattedString($collection[$value], 'YYYY-MM-DD');
                                        $collection[$value] = Carbon::parse($date_string)->format('Y-m-d');
                                    } else {
                                        $collection[$value] = NULL;
                                    }
                                } catch (Exception $e) {
                                    $collection[$value] = NULL;
                                }
                            }
                            if (strpos($key, 'case_no') !== false) {
                                if (strpos($collection[$value], 'AB1/457/') !== false) {
                                    $collection[$value] = str_replace('AB1/457/', '', $collection[$value]); //return only id
                                    if (!is_numeric($collection[$value])) {
                                        continue; //skip this loop
                                    }
                                } else {
                                    return response()->json(['case_no_map' => ['Invalid case no column, missing AB1/457/']], 422);
                                }
                            }
                        }


                    }
                    $notifications[] = [
                        'id' => $counter,
                        'user_id' => $user_id,
                        'employee_name' => $collection[$input['employee_name_map']],
                        'incident_type' => $collection[$input['incident_type_map']],
                        'case_no' => $collection[$input['case_no_map']],
                        'employer_name' => $collection[$input['employer_name_map']],
                        'district' => $collection[$input['district_map']],
                        'claim_status' => $collection[$input['claim_status_map']],
                        'reject_reason' => $collection[$input['reject_reason_map']],
                        'incident_date' => $collection[$input['incident_date_map']],
                        'reporting_date' => $collection[$input['reporting_date_map']],
                        'notification_date' => $collection[$input['notification_date_map']],
                        'registration_date' => $collection[$input['registration_date_map']],
                        'has_payroll' => $has_payroll,
                    ];
                    ++$counter;
                }
                $this->dbBulkInsert($notifications);
                //post documents to e-office

            }
            $tojson = ['success' => 1, 'redirect_url' => route('backend.claim.manual_notification.list_all'), 'message' => '', 'data' => []];
            return response()->json($tojson);
        });
        return $return;
    }

    /**
     * @param array $notifications
     */
    public function dbBulkInsert(array $notifications)
    {
        if (count($notifications)) {
            $json = json_encode($notifications);

            $sql = <<<SQL
do $$
declare t record;
begin

CREATE TEMPORARY TABLE insert_format (
  id int,
  user_id bigint,
  employee_name varchar(100),
  incident_type varchar(80),
  case_no  int,
  employer_name varchar(100),
  district varchar(50),
  claim_status  varchar(150),
  reject_reason text,
  incident_date date,
  reporting_date date,
  notification_date date,
  registration_date date,
  has_payroll smallint
);

insert into insert_format SELECT * FROM   json_populate_recordset(null::insert_format, json '{$json}');

-- insert new notifications
  insert into manual_notification_reports(user_id, employee_name, incident_type, case_no, employer_name, district, claim_status, reject_reason, incident_date, reporting_date, notification_date, registration_date, has_payroll)
  select user_id, employee_name, incident_type, case_no, employer_name, district, claim_status, reject_reason, incident_date, reporting_date, notification_date, registration_date, has_payroll from insert_format where concat_ws('',case_no, incident_type) not in (select concat_ws('',case_no, incident_type) from manual_notification_reports);

-- updating existing notifications
    update manual_notification_reports set user_id = if.user_id, employee_name = if.employee_name, incident_type = if.incident_type, case_no = if.case_no, employer_name = if.employer_name, district = if.district, claim_status = if.claim_status, reject_reason = if.reject_reason, incident_date = if.incident_date, reporting_date = if.reporting_date, notification_date = if.notification_date, registration_date = if.registration_date from insert_format if where concat_ws('',manual_notification_reports.case_no, manual_notification_reports.incident_type)  = concat_ws('',if.case_no, if.incident_type) and manual_notification_reports.isupdated = 0;

DROP TABLE insert_format;

end$$;
SQL;
            DB::unprepared($sql);
        }

    }

}