<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\Accident;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\ClaimFollowup\ClaimFollowup;
use App\Models\Operation\Claim\NotificationReport; 
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Operation\Claim\ClaimFollowupUpdateRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Auth\User;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Claim\ClaimFollowupCalculation;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;


/**
 * Class AccidentRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class ClaimFollowupRepository extends  BaseRepository
{

    const MODEL = ClaimFollowup::class;

    public function __construct()
    {
        $this->notification_report = new NotificationReportRepository();
        $this->claim_followup_update = new ClaimFollowupUpdateRepository();

    }

    public function findOrThrowException($id)
    {
        $followup = $this->query()->find($id);
        if (!is_null($followup)) {
            return $followup;
        }
        throw new GeneralException('The followup does not exist');
    }

    public function syncFolloups(){
        $followups = $this->followupExistCurrentMonth();
        if (is_null($followups)) {
            $this->saveClaimFilesFollowups();
            $this->saveFollowUpCalculation();
        }else{
            dump('followups already run for this month');
        }
    }

    public function saveFollowUpCalculation($input = null){ 
 
        $collection = array();
        $current_month = Carbon::now()->format('Y-m');
 
        $staff_claims = $this->query()->select('claim_followups.user_id','notification_report_id',DB::raw("COUNT(claim_followups.notification_report_id) as total_notification"))->groupBy('claim_followups.user_id','claim_followups.notification_report_id')->where('current_month', $current_month)->get();
 
        $staff_claims = $this->query()->pluck("user_id as user_name");
        $array = $staff_claims->toArray();
 
        if (count($array) < 1) {
            return $collection;
        }
 
 
 
        $values = array_count_values($array);
 
        $claim_array = array();
 
        $i = 0;
        $sn = 1;
        foreach ($values as $key => $value) {
 
 
            $data['user_id'] = $key;
            for ($i=0; $i < 4 ; $i++) { 
                $data[$this->getTotalFiles($key)[$i]['week_name']] = $this->getTotalFiles($key)[$i]['total_files'];
                $data[$this->getTotalFiles($key)[$i]['current_week_name']] = $this->getTotalFiles($key)[$i]['week_division'];
 
                $data[$this->getTotalFiles($key)[$i]['attended_files_name']] = $this->getTotalFiles($key)[$i]['attended_files'];
                $data[$this->getTotalFiles($key)[$i]['missed_files_name']] = $this->getTotalFiles($key)[$i]['missed_files'];
                $data[$this->getTotalFiles($key)[$i]['tmissed_files_name']] = $this->getTotalFiles($key)[$i]['tmissed_files'];
                $data[$this->getTotalFiles($key)[$i]['overall_files_name']] = $this->getTotalFiles($key)[$i]['overall_files'];
            }
            
            $data['current_month'] = $current_month;
 
            $i++;
            $sn++;
 
            /** save calculation */
            DB::table('claim_followup_calculations')->updateOrInsert(
                ['user_id' => $data['user_id'] , 'current_month' => $current_month] , $data );
 
        }
    }


    public function saveClaimFilesFollowups(){

        $cr_in_followups_array = $this->CRfilesInFollowUps();

        $notification_reports = $this->notificationReportArrayFromFileNames($cr_in_followups_array);

        foreach ($notification_reports as $n) {
            dump('start saving normals');
            $this->saveFollowupClaims($n);
        }

        $cr_intermidiate_followups_array = $this->CRfilesIntermidiateFollowUps();

        $notification_reports = $this->notificationReportArrayFromFileNames($cr_intermidiate_followups_array);

        foreach ($notification_reports as $n) {
           $elgb = new AccrualNotificationReportRepository();
           $is_eligible = $elgb->hasWorkFlowLevel1Accepted($n->id);
           if ($is_eligible > 0) {
            dump('start saving abnormals');
            $this->saveFollowupClaims($n);
        }
    }

}

public function notificationReportArrayFromFileNames($cr_in_followups_array){
    $notification_reports = NotificationReport::whereIn('filename',$cr_in_followups_array)->where('allocated', '!=', null)->get();
    return $notification_reports;
}

public function CRfilesNotInFollowUps(){

    $paid_claims = DB::table('main.cr2')->pluck('filename')->toArray();
    $approved_rejected_claims = DB::table('main.cr3')->pluck('filename')->toArray();
    $pending_rejected_claims = DB::table('main.cr24')->pluck('filename')->toArray();
    $dfpi_archived = DB::table('main.cr26')->pluck('filename')->toArray();
    $dfpi = DB::table('main.cr4')->pluck('filename')->toArray();
    $dmas = DB::table('main.cr6')->pluck('filename')->toArray();
    $dmas_archived = DB::table('main.cr28')->pluck('filename')->toArray();
    $void_notification = DB::table('main.cr21')->pluck('filename')->toArray();

    // dg
    // $incomplete_documents = DB::table('main.cr10')->pluck('filename')->toArray();

    $result = array_merge($paid_claims, $approved_rejected_claims, $pending_rejected_claims, $dfpi_archived, $dfpi, $dmas, $dmas_archived, $void_notification );

    return $result;
}

public function CRfilesIntermidiateFollowUps(){

    $do = DB::table('main.cr5')->pluck('filename')->toArray();
    $do_archived = DB::table('main.cr27')->pluck('filename')->toArray();

    $result = array_merge($do, $do_archived);

    return $result;


}

public function CRfilesInFollowUps(){

    $validated_files = DB::table('main.cr8')->pluck('filename')->toArray();
    $unvalidated_files = DB::table('main.cr22')->pluck('filename')->toArray();
    $validated_files2 = DB::table('main.cr9')->pluck('filename')->toArray();
    $validated_files3 = DB::table('main.cr10')->pluck('filename')->toArray();
    $unvalidated_files2 = DB::table('main.cr11')->pluck('filename')->toArray();
    $unvalidated_files3 = DB::table('main.cr29')->pluck('filename')->toArray();
    $unvalidated_files4 = DB::table('main.cr23')->pluck('filename')->toArray();
    $unvalidated_files5 = DB::table('main.cr12')->pluck('filename')->toArray();
    $unvalidated_files6 = DB::table('main.cr13')->pluck('filename')->toArray();
    $unvalidated_files7 = DB::table('main.cr14')->pluck('filename')->toArray();

    $result = array_merge($validated_files, $unvalidated_files, $validated_files2, $validated_files3, $unvalidated_files2, $unvalidated_files3, $unvalidated_files4, $unvalidated_files5, $unvalidated_files6, $unvalidated_files7 );

    return $result;
}

public function followupEligibility($benefits){
    $eligible = 0;

    foreach ($benefits as $b) {
        $lvl = new AccrualNotificationReportRepository();
        $level = $lvl->checkWorkFlowLevel($b->notification_workflow_id);
        dump('level '.$level);

        if ($level == 1) {
            $eligible += 1;
            dump('**********************************************************************************************************');
        }
    }

    return $eligible;

}

public function saveFollowupClaims($notification_report){
 dump('in saving');
 $claim_followup = $this->query()->where('notification_report_id',$notification_report->id)->first();
 $pending_documents = $this->notification_report->checkAllMandatoryDocumentsProgressive($notification_report);
 $data = [
    'user_id' => $notification_report->allocated,
    'notification_report_id' => $notification_report->id,
    'start_date' => Carbon::now(),
    'current_date' => null,
    'followup_count' => 0,
    'current_month' => Carbon::now()->format('Y-m'),
    'missing_documents' => $pending_documents->count(),
];

$data2 = [
    'user_id' => $notification_report->allocated,
    'current_date' => Carbon::now(),
      'followup_count' => 0,
    'current_month' => Carbon::now()->format('Y-m'),
    'missing_documents' => $pending_documents->count(),
    'review_status' => null,
];

if (is_null($claim_followup)) {
 dump('start create');
 $this->query()->create($data);

}else{
 dump('start update');
 $this->query()->where('notification_report_id',$notification_report->id)->update($data2);
}
dump('saved');
}

public function storeFollowUp($input){
    $followup = $this->query()->where('notification_report_id', $input['notification_report_id'])->first();
    $this->query()->where('notification_report_id', $input['notification_report_id'])->increment('followup_count');

    if ($followup->followup_count == 0) {
        $this->calculateFollowup();
    }
    
    
    $followup_cv = (new CodeValueRepository())->findByReference($input['follow_up_type_cv_id']);
    $input['claim_followup_id'] = $followup->id;
    $input['follow_up_type_cv_id'] = $followup_cv->id;

    unset($input['reminder_date']);

    if($input['district'] == null){
        unset($input['district']);
    }else{
        unset($input['district']);
    } 

    if(isset($input['ward'])){
        if ($input['ward'] != null) {
            $postcode = DB::table('postcodes')->select('id')->where('postcode', $input['ward'])->first();
            if (!is_null($postcode)) {
                $input['postcode_id'] = $postcode->id;
            }
        }

        unset($input['ward']);
    }

    unset($input['action_type']);
    $this->claim_followup_update->storeFollowupUpdate($input);



}

public function getClaimFollowupDt($incident_id){

    $follow_up = $this->query()->where('notification_report_id', $incident_id)->first();

    if (is_null($follow_up)) {
        return [];
    }else{
        return $follow_up->ClaimFollowupUpdate()->select('*','claim_followup_updates.id as followup_id')->get();
    }

}

public function getMyFollowUps($user_id){
    $current_month = Carbon::now()->format('Y-m');
    return $this->query()->where('user_id', $user_id)->where('current_month', $current_month)->get();

}

public function mainMyFollowUps($input = null){
    $collection = array();
    $current_month = Carbon::now()->format('Y-m');

    if (isset($input['follow_up_date'])) {
        $current_month = Carbon::parse($input['follow_up_date'])->format('Y-m');
    }

    $staff_claims = $this->query()->select('claim_followups.user_id','notification_report_id',DB::raw("COUNT(claim_followups.notification_report_id) as total_notification"))->groupBy('claim_followups.user_id','claim_followups.notification_report_id')->where('current_month', $current_month)->get();

    if ($input['staff_id'] == -1) {

        $claim_followups = ClaimFollowupCalculation::where('current_month', $current_month)->get();

    } else {

        $claim_followups = ClaimFollowupCalculation::where('current_month', $current_month)->where('user_id', $input['staff_id'])->get();
    }
    $week_no = $this->weekNo(Carbon::now()->format('Y-m-d H:i:s'));


    $i = 0;
    $sn = 1;
    foreach ($claim_followups as $key => $value) {
        switch ($week_no) {
            case 1:
            $collection[$i]['total_files'] = $value->first_week_total_files;
            $collection[$i]['total_files_per_week'] = $value->first_week_current_files;
            break;
            case 2:
            $collection[$i]['total_files'] = $value->second_week_total_files;
            $collection[$i]['total_files_per_week'] = $value->second_week_current_files;
            break;
            case 3:
            $collection[$i]['total_files'] = $value->third_week_total_files;
            $collection[$i]['total_files_per_week'] = $value->third_week_current_files;
            break;
            case 4:
            $collection[$i]['total_files'] = $value->fourth_week_total_files;
            $collection[$i]['total_files_per_week'] = $value->fourth_week_current_files;
            break;
            
            default:
                # code...
            break;
        }



        $collection[$i]['sn'] = $sn;
        $collection[$i]['user_name'] = $value->user_id;
        $collection[$i]['1st_week_updated'] = $value->first_week_attended_files;
        $collection[$i]['1st_week_missed'] = $value->first_week_missed_files;
        $collection[$i]['1st_week'] = $value->first_week_overall_files;

        $collection[$i]['2nd_week_updated'] = $value->second_week_attended_files;
        $collection[$i]['2nd_week_missed'] = $value->second_week_missed_files;
        $collection[$i]['2nd_week'] = $value->second_week_overall_files;
        
        $collection[$i]['3rd_week_updated'] = $value->third_week_attended_files;
        $collection[$i]['3rd_week_missed'] = $value->third_week_missed_files;
        $collection[$i]['3rd_week'] = $value->third_week_overall_files;

        $collection[$i]['4th_week_updated'] = $value->fourth_week_attended_files;
        $collection[$i]['4th_week_missed'] = $value->fourth_week_missed_files;
        $collection[$i]['4th_week'] = $value->fourth_week_overall_files;

        $collection[$i]['overall'] = round($value->overall_total, 2);

        $i++;
        $sn++;
    }

    return $collection;

}

public function getTotalFiles($user_id){
    $files = array();
    $current_month = Carbon::now()->format('Y-m-d');
    $week_no = $this->weekNo(Carbon::now()->format('Y-m-d H:i:s'));
    

    // $total = $this->query()->select(DB::raw("COUNT(claim_followups.notification_report_id) as first_week_total_files"),'review_status')->groupBy('claim_followups.user_id','review_status')->where('user_id', $user_id)->where(function ($query) {
    //     $query->where('review_status', '=', NULL)->orWhere('review_status', '=' ,0)->orWhere('review_status', '=' ,2)->orWhere('review_status', '=' ,3);
    // })->get();

    $total = $this->query()->select(DB::raw("COUNT(claim_followups.notification_report_id) as first_week_total_files"),'review_status')->groupBy('claim_followups.user_id','review_status')->where('user_id', $user_id)->where('current_month', Carbon::parse($current_month)->format('Y-m'))->get();

    $lqt = ClaimFollowupCalculation::where('user_id', $user_id)->where('current_month', Carbon::parse($current_month)->format('Y-m'))->first();

    $total_files = 0;

    foreach ($total as $t) {
        $total_files = $t->first_week_total_files;
        break;
    }

    $files[0]['week_division'] = $this->weekDivision( $total_files)['first_week'];
    $files[0]['total_files'] = $total_files;
    $files[0]['week_name'] = 'first_week_total_files';
    $files[0]['current_week_name'] = 'first_week_current_files';
    $files[0]['attended_files'] = !is_null($lqt) ? $lqt->first_week_attended_files : 0;
    $files[0]['attended_files_name'] = 'first_week_attended_files';
    $files[0]['missed_files'] = !is_null($lqt) ? $lqt->first_week_missed_files : $this->weekDivision( $total_files)['first_week'];
    $files[0]['missed_files_name'] = 'first_week_missed_files';
    $files[0]['tmissed_files'] = !is_null($lqt) ? $lqt->tfirst_week_missed_files : $this->weekDivision( $total_files)['first_week'];
    $files[0]['tmissed_files_name'] = 'tfirst_week_missed_files';
    $files[0]['overall_files'] = !is_null($lqt) ? $lqt->first_week_overall_files : 0;
    $files[0]['overall_files_name'] = 'first_week_overall_files';

    $data = [
        'last_quotient' => $files[0]['week_division'],
    ];
    $this->query()->where('current_month',Carbon::parse($current_month)->format('Y-m'))->where('user_id', $user_id)->update($data);

    $files[1]['total_files'] = $total_files;
    $files[1]['week_name'] = 'second_week_total_files';
    $files[1]['current_week_name'] = 'second_week_current_files';
    $files[1]['attended_files'] = !is_null($lqt) ? $lqt->second_week_attended_files : 0;
    $files[1]['attended_files_name'] = 'second_week_attended_files';
    $files[1]['missed_files'] = !is_null($lqt) ? $lqt->second_week_missed_files : $this->weekDivision( $total_files)['second_week'];
    $files[1]['missed_files_name'] = 'second_week_missed_files';
    $files[1]['tmissed_files'] = !is_null($lqt) ? $lqt->tsecond_week_missed_files : $this->weekDivision( $total_files)['second_week'];
    $files[1]['tmissed_files_name'] = 'tsecond_week_missed_files';
    $files[1]['overall_files'] = !is_null($lqt) ? $lqt->second_week_overall_files : 0;
    $files[1]['overall_files_name'] = 'second_week_overall_files';

    $files[1]['week_division'] = $this->weekDivision( $total_files)['second_week'];
    $data = [
        'last_quotient' => $files[1]['week_division'],
    ];
    $this->query()->where('current_month',Carbon::parse($current_month)->format('Y-m'))->where('user_id', $user_id)->update($data);


    $files[2]['week_name'] = 'third_week_total_files';
    $files[2]['current_week_name'] = 'third_week_current_files';
    $files[2]['attended_files'] = !is_null($lqt) ? $lqt->third_week_attended_files: 0;
    $files[2]['attended_files_name'] = 'third_week_attended_files';
    $files[2]['missed_files'] = !is_null($lqt) ? $lqt->third_week_missed_files : $this->weekDivision( $total_files)['third_week'];
    $files[2]['missed_files_name'] = 'third_week_missed_files';
    $files[2]['tmissed_files'] = !is_null($lqt) ? $lqt->tthird_week_missed_files : $this->weekDivision( $total_files)['third_week'];
    $files[2]['tmissed_files_name'] = 'tthird_week_missed_files';
    $files[2]['overall_files'] = !is_null($lqt) ? $lqt->third_week_overall_files : 0;
    $files[2]['overall_files_name'] = 'third_week_overall_files';

    $files[2]['total_files'] = $total_files;
    $files[2]['week_division'] = $this->weekDivision( $total_files)['third_week'];
    $data = [
        'last_quotient' => $files[2]['week_division'],
    ];
    $this->query()->where('current_month',Carbon::parse($current_month)->format('Y-m'))->where('user_id', $user_id)->update($data);


    $files[3]['week_name'] = 'fourth_week_total_files';
    $files[3]['current_week_name'] = 'fourth_week_current_files';

    $files[3]['attended_files'] = !is_null($lqt) ? $lqt->fourth_week_attended_files : 0;
    $files[3]['attended_files_name'] = 'fourth_week_attended_files';
    $files[3]['missed_files'] = !is_null($lqt) ? $lqt->fourth_week_missed_files : $this->weekDivision( $total_files)['fourth_week'];
    $files[3]['missed_files_name'] = 'fourth_week_missed_files';
    $files[3]['tmissed_files'] = !is_null($lqt) ? $lqt->tfourth_week_missed_files : $this->weekDivision( $total_files)['fourth_week'];
    $files[3]['tmissed_files_name'] = 'tfourth_week_missed_files';
    $files[3]['overall_files'] = !is_null($lqt) ? $lqt->fourth_week_overall_files : 0;
    $files[3]['overall_files_name'] = 'fourth_week_overall_files';
    $files[3]['total_files'] = $total_files;

    $files[3]['week_division'] = $this->weekDivision( $total_files)['fourth_week'];
    $data = [
        'last_quotient' => $files[3]['week_division'],
    ];
    $this->query()->where('current_month',Carbon::parse($current_month)->format('Y-m'))->where('user_id', $user_id)->update($data);


    return $files;

}

public function calculateFollowup(){
    $current_month = Carbon::now()->format('Y-m-d');
    $calculate = $this->returnFollowupCalculation();
    $week_no = $this->weekNo(Carbon::now()->format('Y-m-d H:i:s'));

    switch ($week_no) {
        case 1:        
        $data = ['first_week_attended_files' => $calculate->first_week_attended_files + 1,];
        $this->updateClaimFollowup($data);
        $calculate = $this->returnFollowupCalculation();
        $percent_case = 'first_percent';
        $percent = $this->calculateFirstWeek($percent_case);
        $missed_files = $calculate->first_week_current_files - $calculate->first_week_attended_files;
        $data = [
            'first_week_overall_files' => $percent,
            'first_week_missed_files' => $missed_files < 0 ? 0 : $missed_files
        ];
        $this->updateClaimFollowup($data);
        $calculate = $this->returnFollowupCalculation();

        $total_percent = $this->returnTotalPercent($calculate); 
        $data = [
            'overall_total' => $total_percent,
        ];
        $this->updateClaimFollowup($data);      

        break;
        case 2:

        $data = ['second_week_attended_files' => $calculate->second_week_attended_files + 1,];
        $this->updateClaimFollowup($data);
        $calculate = $this->returnFollowupCalculation();
        $percent_case = 'second_percent';
        $percent = $this->calculateSecondWeek($percent_case);
        $missed_files = $calculate->second_week_current_files - $calculate->second_week_attended_files;
        $data = [
            'second_week_overall_files' => $percent,
            'second_week_missed_files' => $missed_files < 0 ? 0 : $missed_files
        ];
        $this->updateClaimFollowup($data);
        $calculate = $this->returnFollowupCalculation();

        $total_percent = $this->returnTotalPercent($calculate); 
        $data = [
            'overall_total' => $total_percent,
        ];
        $this->updateClaimFollowup($data); 

        break;
        case 3:
        $data = ['third_week_attended_files' => $calculate->third_week_attended_files + 1,];
        $this->updateClaimFollowup($data);
        $calculate = $this->returnFollowupCalculation();
        $percent_case = 'third_percent';
        $percent = $this->calculateThirdWeek($percent_case);
        $missed_files = $calculate->third_week_current_files - $calculate->third_week_attended_files;
        $data = [
            'third_week_overall_files' => $percent,
            'third_week_missed_files' => $missed_files < 0 ? 0 : $missed_files
        ];
        $this->updateClaimFollowup($data);
        $calculate = $this->returnFollowupCalculation();

        $total_percent = $this->returnTotalPercent($calculate); 
        $data = [
            'overall_total' => $total_percent,
        ];
        $this->updateClaimFollowup($data); 

        break;
        case 4:
        $data = ['fourth_week_attended_files' => $calculate->fourth_week_attended_files + 1,];
        $this->updateClaimFollowup($data);
        $calculate = $this->returnFollowupCalculation();
        $percent_case = 'fourth_percent';
        $percent = $this->calculateFourthWeek($percent_case);
        $missed_files = $calculate->fourth_week_current_files - $calculate->fourth_week_attended_files;
        $data = [
            'fourth_week_overall_files' => $percent,
            'fourth_week_missed_files' => $missed_files < 0 ? 0 : $missed_files
        ];
        $this->updateClaimFollowup($data);
        $calculate = $this->returnFollowupCalculation();

        $total_percent = $this->returnTotalPercent($calculate); 
        $data = [
            'overall_total' => $total_percent,
        ];
        $this->updateClaimFollowup($data); 

        break;

        default:
                        # code...
        break;
    }
}

public function calculateFirstWeek($percent_case){
    $f = $this->followupArray();
    $first_percent = 0;

    $missed1 = $this->missedFiles($f['missed1']);
    $missed2 = $this->missedFiles($f['missed2']);
    $missed3 = $this->missedFiles($f['missed3']);
    $missed4 = $this->missedFiles($f['missed4']);

    if($f['missed1'] > 0){
        $percent = $this->returnPercent($f['total_files1'], 1);
        $first_percent = $percent + $f[$percent_case];
        $data = ['tfirst_week_missed_files' => $missed1];

    }else{

        $percent = $this->returnPercent($f['total_files2'], 1);
        $first_percent = $percent + $f[$percent_case];
        $data = ['tsecond_week_missed_files' => $missed2];

        if ($f['missed2'] < 1) {
            $percent = $this->returnPercent($f['total_files3'], 1);
            $first_percent = $percent + $f[$percent_case];
            $data = ['tthird_week_missed_files' => $missed3];

            if ($f['missed3'] < 1) {
                $percent = $this->returnPercent($f['total_files4'], 1);
                $first_percent = $percent + $f[$percent_case];
                $data = ['tfourth_week_missed_files' => $missed4];
            }
        }
    }

    $this->updateClaimFollowup($data);

    return $first_percent;
}

public function calculateSecondWeek($percent_case){
    $f = $this->followupArray();
    $second_percent = 0;
    $missed2 = $this->missedFiles($f['missed2']);

    if($f['missed2'] > 0){
        $percent = $this->returnPercent($f['total_files2'], 1);
        $second_percent = $percent + $f[$percent_case];
        $data = ['tsecond_week_missed_files' => $missed2];
        $this->updateClaimFollowup($data);

    }else{

        $second_percent = $this->calculateFirstWeek($percent_case);
    }

    return $second_percent;
}

public function calculateThirdWeek($percent_case){
    $f = $this->followupArray();
    $third_percent = 0;
    $missed3 = $this->missedFiles($f['missed3']);

    if($f['missed3'] > 0){
        $percent = $this->returnPercent($f['total_files3'], 1);
        $third_percent = $percent + $f[$percent_case];
        $data = ['tthird_week_missed_files' => $missed3];
        $this->updateClaimFollowup($data);

    }else{

        $third_percent = $this->calculateFirstWeek($percent_case);
    }

    return $third_percent;
}

public function calculateFourthWeek($percent_case){
    $f = $this->followupArray();
    $fourth_percent = 0;
    $missed4 = $this->missedFiles($f['missed4']);

    if($f['missed4'] > 0){
        $percent = $this->returnPercent($f['total_files4'], 1);
        $fourth_percent = $percent + $f[$percent_case];
        $data = ['tfourth_week_missed_files' => $missed4];
        $this->updateClaimFollowup($data);

    }else{

        $fourth_percent = $this->calculateFirstWeek($percent_case);
    }

    return $fourth_percent;
}

public function followupArray(){
    $f = array();
    $followup = $this->returnFollowupCalculation();
    $f['total_files1'] = $followup->first_week_current_files;
    $f['attended1'] = $followup->first_week_attended_files;
    $f['missed1'] = $followup->tfirst_week_missed_files;
    $f['total_files2'] = $followup->second_week_current_files;
    $f['attended2'] = $followup->second_week_attended_files;
    $f['missed2'] = $followup->tsecond_week_missed_files;
    $f['total_files3'] = $followup->third_week_current_files;
    $f['attended3'] = $followup->third_week_attended_files;
    $f['missed3'] = $followup->tthird_week_missed_files;
    $f['total_files4'] = $followup->fourth_week_current_files;
    $f['attended4'] = $followup->fourth_week_attended_files;
    $f['missed4'] = $followup->tfourth_week_missed_files;
    $f['first_percent'] = $followup->first_week_overall_files;
    $f['second_percent'] = $followup->second_week_overall_files;
    $f['third_percent'] = $followup->third_week_overall_files;
    $f['fourth_percent'] = $followup->fourth_week_overall_files;

    return $f;

}

public function missedFiles($missd){
    $missed = 0;
    if ($missd > 0) {
        $missed = $missd - 1;
    }
    return $missed;
}

public function returnTotalPercent($calculate){
    $divisor = $this->divisor($calculate);

    $average_percent = ($calculate->first_week_overall_files + $calculate->second_week_overall_files + $calculate->third_week_overall_files + $calculate->fourth_week_overall_files) / $divisor;
    return round($average_percent, 2);
}

public function divisor($calculate){
    $divisor = 0;
    if ($calculate->first_week_current_files > 0) {
        $divisor++;
    }
    if($calculate->second_week_current_files > 0){
        $divisor++;
    }
    if($calculate->third_week_current_files > 0){
        $divisor++;
    }
    if($calculate->fourth_week_current_files > 0){
        $divisor++;
    }

    return $divisor;
}

public function returnPercent($total, $attended){
    $percent = 0;
    if ($total > 0) {
        $percent = ($attended / $total) * 100;
    }
    return round($percent, 2);
}

public function updateClaimFollowup($data){
    ClaimFollowupCalculation::where('user_id', auth()->user()->id)->where('current_month', Carbon::now()->format('Y-m'))->update($data);
}

public function returnFollowupCalculation(){
    $calculate = ClaimFollowupCalculation::where('user_id', auth()->user()->id)->where('current_month', Carbon::now()->format('Y-m'))->first();
    return $calculate;
}

public function weekDivision( $total_files){
    $week_division = 0;

    if ($total_files % 4 != 0) {
        $remainder = $total_files % 4;
        $divisor = intdiv($total_files, 4);
        $weeks = array();

        switch ($remainder) {

            case 1:
            $weeks['first_week'] = $divisor + 1;
            $weeks['second_week'] = $divisor;
            $weeks['third_week'] = $divisor;
            $weeks['fourth_week'] = $divisor;
            break;
            case 2:
            $weeks['first_week'] = $divisor + 1;
            $weeks['second_week'] = $divisor + 1;
            $weeks['third_week'] = $divisor;
            $weeks['fourth_week'] = $divisor;
            break;
            case 3:
            $weeks['first_week'] = $divisor + 1;
            $weeks['second_week'] = $divisor + 1;
            $weeks['third_week'] = $divisor + 1;
            $weeks['fourth_week'] = $divisor;
            break;

            default:
                # code...
            break;
        }

    }else{
        $divisor = $total_files / 4;
        $weeks['first_week'] = $divisor;
        $weeks['second_week'] = $divisor;
        $weeks['third_week'] = $divisor;
        $weeks['fourth_week'] = $divisor;
    }
    
    return $weeks;

}

function is_decimal( $val )
{
    return is_numeric( $val ) && floor( $val ) != $val;
}

public function checkIfMaxDate($input = null){
    $return = false;
    $date = Carbon::parse($input['follow_up_date']);
    $claim_date = $this->query()->select(DB::raw("MIN(claim_followups.start_date) as min_start_date"), DB::raw("MAX(claim_followups.start_date) as max_start_date"))->first();
    $max_date = Carbon::parse($claim_date->max_start_date)->endOfMonth();
    $min_date = Carbon::parse($claim_date->min_start_date)->startOfMonth();

    if($date->greaterThanOrEqualTo($min_date) and $date->lessThanOrEqualTo($max_date)){
        $return = true;
    }

    return $return;
}

public function getPercentage($total_files, $updated_files){
    $percent = 0;
    if($updated_files > 0){
        $percent = ($updated_files / $total_files) * 100;
        $percent = round($percent, 2);
    }

    return $percent;

}

public function getTotalCurrentFiles($input = null, $user_id){
    $total_files = 0;

    $days = $this->getFirstLastDayOfWeek($input);
    $FirstDay = $days[0];
    $LastDay = $days[1];

    if (isset($input['follow_up_date'])) {

        $days = $this->getCurrentDateWeek($input, $days);
        $FirstDay = $days[0];
        $LastDay = $days[1];
    }

    $claims = $this->query()->where('user_id',$user_id)->get();

    foreach ($claims as $c) {
        if(Carbon::parse($c->start_date)->format('Y-m-d') > Carbon::parse($FirstDay)->format('Y-m-d') and Carbon::parse($c->start_date)->format('Y-m-d') < Carbon::parse($LastDay)->format('Y-m-d')){
                // file in a current week
            $total_files += 1;
        }
    }

    return $total_files;


}

public function getCurrentDateWeek($input, $days){
    $array = explode('-', $input['follow_up_date']);
    $month = $array[1];

    $array1 = explode('-', $days[0]);
    $array1[1] = $month;
    $FirstDay = implode('-', $array1);

    $array2 = explode('-', $days[1]);
    $array2[1] = $month;
    $LastDay = implode('-', $array2);

    $days[0] = $FirstDay;
    $days[1] = $LastDay;

    return $days;
}

public function getWeeklyCount($input = null, $user_id){
    $total_files = array();
    $total_files['1st_week'] = 0;
    $total_files['2nd_week'] = 0;
    $total_files['3rd_week'] = 0;
    $total_files['4th_week'] = 0;

    $total_files['1st_week_updated'] = 0;
    $total_files['2nd_week_updated'] = 0;
    $total_files['3rd_week_updated'] = 0;
    $total_files['4th_week_updated'] = 0;

    $total_files['1st_week_missed'] = 0;
    $total_files['2nd_week_missed'] = 0;
    $total_files['3rd_week_missed'] = 0;
    $total_files['4th_week_missed'] = 0;



    $claims = $this->query()->where('user_id',$user_id)->get();

    $total_files = $this->weeklyCount($input, $total_files, $claims);

    return $total_files;


}

public function weeklyCount($input = null, $total_files, $claims){
 foreach ($claims as $c) {
    $date = $c->start_date;
    if (!isset($input['follow_up_date'])) {
      $is_current = $this->isCurrentMonth($date);
      if ($is_current) {
        $week_no = $this->weekNo($date);

        switch ($week_no) {
            case 1:
            $total_files['1st_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['1st_week_updated'] += 1;
            }else{
                $total_files['1st_week_missed'] += 1;
            }

            break;
            case 2:
            $total_files['2nd_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['2nd_week_updated'] += 1;
            }else{
                $total_files['2nd_week_missed'] += 1;
            }
            break;
            case 3:
            $total_files['3rd_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['3rd_week_updated'] += 1;
            }else{
                $total_files['3rd_week_missed'] += 1;
            }
            break;
            case 4:
            $total_files['4th_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['4th_week_updated'] += 1;
            }else{
                $total_files['4th_week_missed'] += 1;
            }
            break;

            default:
                        # code...
            break;
        }

    }
}else{

    $days = $this->getFirstLastDayOfWeek($input);
    $days = $this->getCurrentDateWeek($input, $days);
    $FirstDay = $days[0];
    $LastDay = $days[1];

    if(Carbon::parse($date)->format('Y-m-d') > Carbon::parse($FirstDay)->format('Y-m-d') and Carbon::parse($date)->format('Y-m-d') < Carbon::parse($LastDay)->format('Y-m-d')){
        $week_no = $this->weekNo($date);

        switch ($week_no) {
            case 1:
            $total_files['1st_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['1st_week_updated'] += 1;
            }

            break;
            case 2:
            $total_files['2nd_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['2nd_week_updated'] += 1;
            }
            break;
            case 3:
            $total_files['3rd_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['3rd_week_updated'] += 1;
            }
            break;
            case 4:
            $total_files['4th_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['4th_week_updated'] += 1;
            }
            break;

            default:
                            # code...
            break;
        }
    }

}

}

return $total_files;

}

public function weekNo($date){
    $timestamp = Carbon::parse($date)->format('Y-m-d H:i:s');
    $date = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, 'Africa/Dar_es_Salaam');
    $date->setTimezone('UTC');
    $week_no = $date->weekOfMonth;

    $week_no = $week_no > 4 ? 4 : $week_no; 
    return $week_no;
}

public function getFirstLastDayOfWeek($input = null){

    //     $timestamp    = strtotime($input['follow_up_date']);
    //     $FirstDay = date('Y-m-01', $timestamp);
    //     $LastDay  = date('Y-m-t', $timestamp); 
    //     $dayofweek = date('w', strtotime($date));

    $FirstDay = date("Y-m-d", strtotime('sunday last week'));  
    $LastDay = date("Y-m-d", strtotime('sunday this week'));

    $days = array();

    $days[0] = $FirstDay;
    $days[1] = $LastDay;

    return $days;
}


public function isCurrentMonth($date){
    $return = false;

    $date = Carbon::parse($date)->format('m');
    $today = Carbon::now()->format('m');

    if ($date == $today) {
        $return = true;
    }

    return $return;
}

public function userFollowUps($user_id){
    $current_month = Carbon::now()->format('Y-m');
    return $this->query()->where('user_id', $user_id)->where('current_month', $current_month)->get();
}

public function exportToExcel($input)
{
    $date = isset($input) ? $input['follow_up_date'] : Carbon::now()->format('Y-m');
    $collection = $this->mainMyFollowUps($input);


    $results = array();
    foreach ($collection as $result) {
        $user = User::find($result['user_name']);
        $result['user_name'] = $user->name;
        $results[] = $result;
    }

    return Excel::create('claim_follow_up:'. $date, function($excel) use ($results) {
        $excel->sheet('mySheet', function($sheet) use ($results)
        {
            $sheet->fromArray((array)$results);
        });
    })->download('csv');

}

public function ifFollowUpDateExceeded($notification){
    $in_followup = $this->isInFollowUp($notification);
    $is_date_exceeded = false;
    if ($in_followup) {
        $followup = $this->query()->where('notification_report_id', $notification->id)->first();
        $is_date_exceeded = $this->isDateExceeded($followup->start_date);
    }
    return $is_date_exceeded;
}

public function isInFollowUp($notification){
    $return = false;
    $followup = $this->query()->where('notification_report_id', $notification->id)->first();
    if (!is_null($followup)) {
       $return = true;
   }
   return $return;
}

public function isDateExceeded($date){
    $exceeded = false;
    $week_no = $this->weekNo($date);
    $end_month = Carbon::now()->endOfMonth();
    $date = Carbon::parse($date);
    $exceeded = $end_month->greaterThanOrEqualTo(Carbon::parse($date)) ? false : true;

    return $exceeded;
}

public function updateReviewStatus($notification_report_id, $status){
    $this->query()->where('notification_report_id', (int)$notification_report_id)->update(['review_status' => (int)$status]);
}

public function followupExistCurrentMonth(){
    $date = Carbon::now()->format('Y-m');
    $followup = $this->query()->where('current_month', $date)->first();
    return $followup;

}


}