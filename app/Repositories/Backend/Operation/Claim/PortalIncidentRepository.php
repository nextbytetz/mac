<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Models\Operation\Claim\PortalIncident;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class PortalIncidentRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class PortalIncidentRepository extends BaseRepository
{
    const MODEL = PortalIncident::class;

    /**
     * @param null $id
     * @return mixed
     */
    public function getForDatatable($id = NULL)
    {
        $query = $this->query()->select([
            "portal.incidents.id",
            DB::raw("main.incident_types.name as incident_type"),
            DB::raw("concat_ws(' ', main.employees.firstname, coalesce(main.employees.middlename, ''), main.employees.lastname) as employee"),
            DB::raw("main.employers.name as employer"),
            DB::raw("main.regions.name as region"),
            DB::raw("main.districts.name as district"),
            DB::raw("portal.incidents.created_at"),
            DB::raw("main.notification_reports.filename caseno"),
            "portal.incidents.status",
            "portal.incidents.incident_date",
            "portal.incidents.reporting_date",
            "main.employees.lastname",
            "main.employees.middlename",
            "main.employees.firstname",
        ])
            ->join("main.incident_types", "portal.incidents.incident_type_id", "=", "main.incident_types.id")
            ->join("main.districts", "portal.incidents.district_id", "=", "main.districts.id")
            ->join("main.regions", "main.districts.region_id", "=", "main.regions.id")
            ->leftJoin("main.employers", "main.employers.id", "=", "portal.incidents.employer_id")
            ->leftJoin("main.notification_reports", "main.notification_reports.incident_id", "=", "portal.incidents.id")
            ->leftJoin("main.employees", "main.employees.id", "=", "portal.incidents.employee_id");
        if ($id) {
            $query->where("portal.incidents.employer_id", $id);
        }

        return $query;
    }


    /**
     * @param Model $incident
     * @param $check
     * @param $documentId
     * @return mixed
     */
    public function onlineIncidentOptionalDocs(Model $incident, $check, $documentId)
    {
        return DB::transaction(function () use ($incident, $check, $documentId) {
            if ($check) {
                //insert
                $incident->additionalDocs()->attach($documentId);
            } else {
                //remove
                $incident->additionalDocs()->detach($documentId);
            }
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postIncidentComment(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            if (!empty($input['comments'])) {
                $user_id = access()->id();
                $query = $incident->queries()->create([
                    'officer_id' => $user_id,
                    'comments' => $input['comments'],
                ]);
                return $query;
            }
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param array $input
     * @return mixed
     */
    public function postMergeIncident(Model $incident, array $input)
    {
        return DB::transaction(function () use ($incident, $input) {
            $incident->status = 5; //Declined Request & Merged with Branch Registered
            $incident->notification_report_id = $input['incident'];
            $incident->incident_staging_cv_id = (new CodeValuePortalRepository())->ISINCDCNDMBRI(); //Incident Declined & Merged with Branch Registered Incident
            $incident->save();
            $notificationReport = (new NotificationReportRepository())->find($input['incident']);
            $notificationReport->incident_id = $incident->id;
            $notificationReport->source = 3; //Merged with Online Application
            $notificationReport->save();
            return true;
        });
    }

    /**
     * @param Model $incident
     * @param int $caller
     */
    public function initialize(Model $incident, $caller = 0)
    {
        $incident->refresh();
        $stage = $incident->incident_staging_cv_id;
        $codeValue = new CodeValuePortalRepository();
        $codeValueMain = new CodeValueRepository();
        $checkerRepo = new CheckerRepository();
        $userRepo = new UserRepository();

        //--> Allocate Incident User
        $user = $this->retrieveIncidentAllocatedUser($incident);
        //$prevStage = $this->retrievePreviousStage($incident);
        //$prevComment = (($prevStage) ? $prevStage->{'currentStage'} : "Inspection Registration");
        $prevComment = '';
        $notify = 0;
        $comments = NULL;
        $process = 1;
        $closeChecker = 0;
        $priority = 0;
        $sendmail = 0;

        switch ($stage) {
            case $codeValue->ISUPSUPDOC():
                //Pending Supporting Documents
                if ($user) {
                    //--> Checker Inbox & Tasks
                    $notify = 1;
                    $sendmail = 1;
                    $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Pending Supporting Documents", "currentStageCvId":' . $stage . ', "nextStage":"Pending Incident Acknowledgement", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "":""}';
                }
                break;
            case $codeValue->ISUPSUDCRVWD():
                //Pending Supporting Documents (Officer Reviewed)
                if ($user) {
                    //--> Checker Inbox & Tasks
                    $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Pending Supporting Documents (Officer Reviewed)", "currentStageCvId":' . $stage . ', "nextStage":"Pending Incident Acknowledgement", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "":""}';
                }
                break;
            case $codeValue->ISREGSTR():
                //Incident Registration
                if ($user) {
                    //--> Checker Inbox & Tasks
                    $notify = 1;
                    $sendmail = 1;
                    $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Inspection Plan Approval Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Pending Supporting Documents", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "":""}';
                }
                break;
            case $codeValue->ISINCAPPRL():
                //Pending Incident Acknowledgement
                $closeChecker = 1;
                break;
            case $codeValue->ISINCAPRVD():
                //Incident Approved
                $closeChecker = 1;
                break;
            case $codeValue->ISINCDCNDMBRI():
                //Incident Declined & Merged with Branch Registered Incident
                $closeChecker = 1;
                break;
            case $codeValue->ISINCDECLND():
                //Incident Declined
                $closeChecker = 1;
                break;
            case $codeValue->ISCANCLLED():
                //Incident Cancelled
                $closeChecker = 1;
                break;
            case $codeValue->ISPEBEDOC():
                //Pending Benefit Documents
                $closeChecker = 1;
                break;
            case $codeValue->ISCANCBYUSR():
                //Cancelled By User
                $closeChecker = 1;
                break;

        }

        if ($process And $user) {
            if (!empty($comments)) {
                //Save comment on inspection stages
                /*
                    $inspection->stages()->attach($stage,
                    [
                        'comments' => $comments
                    ]);
                $this->updateInspectionStageId($inspection);
                */
            }
            if ($closeChecker) {
                //Close recent checker entry
                $input = [
                    'resource_id' => $incident->id,
                    'checker_category_cv_id' => $codeValueMain->CCONTFCAPPCTN(), //Online Notification Application
                ];
                $checkerRepo->closeRecently($input);
            } else {
                //Create checker entry
                $input = [
                    'comments' => $comments,
                    'user_id' => $user,
                    'checker_category_cv_id' => $codeValueMain->CCONTFCAPPCTN(), //Online Notification Application
                    'priority' => $priority,
                    'notify' => $notify,
                    'sendmail' => $sendmail,
                ];
                $checkerRepo->create($incident, $input);
            }
        }

    }

    /**
     * @param Model $incident
     * @return int|mixed|null
     */
    public function retrieveIncidentAllocatedUser(Model $incident)
    {
        if (!$incident->allocated) {
            $user = (new UserRepository())->getAllocatedForOnlineIncident($incident);
            $incident->allocated = $user;
            $incident->save();
        } else {
            $user = $incident->allocated;
        }
        return $user;
    }

}