<?php
namespace App\Repositories\Backend\Operation\InvestigationPlan;

use App\Events\NewWorkflow;
use App\Exceptions\WorkflowException;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use App\Models\Workflow\WfTrack;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\InvestigationPlan\InvestigationPlan;
use App\Models\Operation\InvestigationPlan\InvestigationOfficer;
use App\Models\Operation\InvestigationPlan\InvestigationPlanUser;
use App\Models\Operation\InvestigationPlan\InvestigationPlanNotificationReport;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationOfficerRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanUserRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;
use App\Mail\InvestigationPlanReminder; 
use Mail;
use App\Repositories\Backend\Operation\Claim\NotificationInvestigatorRepository;
use Illuminate\Support\Facades\Storage;

class InvestigationPlanRepository extends BaseRepository
{
    const MODEL = InvestigationPlan::class;

    protected $category_code_id;
    public function __construct()
    {
        parent::__construct();
        $this->category_code_id = 68;
        $this->type_code_id = 67;
        $this->wf_module_group_id = 32;

    }

    public function findOrThrowException($id)
    {
        $plan = $this->query()
        ->find($id);
        if (!is_null($plan))
        {
            return $plan;
        }
        throw new GeneralException('Investigation plan not found');
    }

    public function getInvestigationPlanForDataTable()
    {
        $data = $this->query()
        ->select('*', DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(middlename, ''), coalesce(lastname, '')) as user") , 'investigation_plans.id as id')
        ->join("main.users", "users.id", "=", "investigation_plans.user_id")
        ->join("main.code_values", "code_values.id", "=", "investigation_plans.investigation_category_cv_id");
        return $data;
    }

    public function investigationCategories()
    {
        $investigation_categories = new CodeValueRepository();
        $investigation_categories = $investigation_categories->query()
        ->where('code_id', $this->category_code_id)
        ->pluck('name', 'id')
        ->all();
        return $investigation_categories;
    }

    public function investigationTypes()
    {
        $investigation_categories = new CodeValueRepository();
        $investigation_categories = $investigation_categories->query()
        ->where('code_id', $this->type_code_id)
        ->pluck('name', 'id')
        ->all();
        return $investigation_categories;
    }

    public function planMaximumLimits()
    {
        return ['max_officers' => count((new UserRepository())->all()) , 'max_files' => count((new NotificationReportRepository())
            ->all()) ];
    }

    public function canUserCreateNewPlan($user_id)
    {
        $is_master = (new InvestigationOfficerRepository())->findCordinatorByUserId($user_id);
        if (count($is_master))
        {
            $has_pending = $this->query()
            ->where('user_id', $user_id)->whereIn('plan_status', [0,1,3])
            ->where('wf_done',0)
            ->get();
            $has_not_complete = $this->query()
            ->where('user_id', $user_id)->where('plan_status',2)->where('is_complete', false)
            ->get();
            // if (count($has_pending) || count($has_not_complete))
            if (count($has_pending))
            {
                throw new GeneralException('Access Denied! You still have another pending or incomplete plan');
            }
        }
        else
        {
            throw new GeneralException('Access Denied! You are not authorized to perform that action');
        }
    }

    public function canUserEditPlan($plan_id, $user_id)
    {
        if (!$this->checkIfUserCanEditPlan($plan_id, $user_id))
        {
            throw new GeneralException('Access Denied! This plan can not be edited');
        }

    }

    public function checkIfUserCanEditPlan($plan_id, $user_id)
    {
        $is_pending = $this->query()
        ->where('id',$plan_id)
        ->where('user_id', $user_id)
        ->whereIn('plan_status',[0,3])
        ->where('wf_done',0)
        ->first();
        return count($is_pending) ? true: false;
    }

    public function canUserSubmitForReview($plan_id, $user_id)
    {
        $has_pending = $this->query()
        ->where('user_id', $user_id)->where('id', $plan_id)->first();
        if (count($has_pending))
        {
            if ($has_pending->plan_status > 0 || $has_pending->wf_done)
            {
                throw new GeneralException('Action Failed! This plan has already been submitted');
            }
            else
            {
                $plan_users_id = (new InvestigationPlanUserRepository())->query()
                ->where('investigation_plan_id', $plan_id)->pluck('user_id')
                ->all();
                $plan_notification = (new InvestigationPlanNotificationsRepository())->query()
                ->where('investigation_plan_id', $plan_id)->pluck('assigned_to')
                ->all();

                if (count($plan_users_id) != $has_pending->number_of_investigators)
                {
                    throw new GeneralException('Action Failed! The allocated investigators do not match the plan');
                }

                if (count($plan_notification) != $has_pending->number_of_files)
                {
                    throw new GeneralException('Action Failed! The assigned files ' . count($plan_notification) . ' do not match the planned ' . $has_pending->number_of_files);
                }

                if (count(array_diff($plan_users_id, $plan_notification)))
                {
                    throw new GeneralException('Action Failed! ' . count(array_diff($plan_users_id, $plan_notification)) . ' investigators have not been assigned any files');
                }
            }
        }
        else
        {
            throw new GeneralException('Access Denied! You are not authorized to perform that action');
        }
    }

    public function createPlan($request, array $input)
    {
        $return = DB::transaction(function () use ($request,$input)
        {
            $start = Carbon::parse($input['start_date']);
            $end = Carbon::parse($input['end_date']);
            if ($input['investigation_category'] == 'Individual Investigation')
            {
                $input['individual_mode'] = $input['conduct_option'] == 'alone' ? 1 : 2;
            }
            else
            {
                $input['individual_mode'] = null;
            }

            $total_budget = !empty($input['total_budget']) ? $input['total_budget'] : null;

            $data = ['investigation_type_cv_id' => (int)$input['type'], 'investigation_category_cv_id' => (int)$input['category'], 'start_date' => $input['start_date'], 'end_date' => $input['end_date'], 'investigation_reason' => $input['reason'], 'number_of_files' => $input['number_of_files'], 'number_of_investigators' => $input['number_of_investigators'], 'user_id' => access()->user()->id, 'duration' => $end->diffInDays($start) , 'individual_mode' => $input['individual_mode'],'total_budget'=> $total_budget];
            $plan = $this->query()
            ->create($data);
            $plan_name = $this->returnPlanName($plan->id, (int)$input['category']);

            if (isset($request->budget_attachment)) {
             $file_name_and_ext = $request->file('budget_attachment')->getClientOriginalName();
             $filename = pathinfo($file_name_and_ext, PATHINFO_FILENAME);
             $file_ext = $request->file('budget_attachment')->getClientOriginalExtension();
             $store_file_name = date("Y-m-d").'-'.time().'-'.str_replace('/','-',$plan_name).'.'.$file_ext;
             $path = $request->file('budget_attachment')->storeAs('public/investigation_plan/'.$plan->id.'/',$store_file_name);
         } else {
             $store_file_name == null;
             $file_name_and_ext == null;
         }


         $this->saveUpdatePlan($plan->id, ['plan_name' => $plan_name,'budget_uploaded_name'=>$file_name_and_ext, 'budget_attachment'=>$store_file_name]);
         return $plan;
     });

        return $return;
    }

    public function updatePlan($plan_id, array $input, $request = null)
    {
        // dd($input);
        $plan = $this->query()
        ->find($plan_id);
        $start = Carbon::parse($input['start_date']);
        $end = Carbon::parse($input['end_date']);
        if ($input['investigation_category'] == 'Individual Investigation')
        {
            $input['individual_mode'] = $input['conduct_option'] == 'alone' ? 1 : 2;
        }
        else
        {
            $input['individual_mode'] = null;
        }

        $total_budget = $input['budget_option'] == 1 ? $input['total_budget'] : null;

        $data_to_update = ['investigation_type_cv_id' => (int)$input['type'], 'investigation_category_cv_id' => (int)$input['category'], 'start_date' => $input['start_date'], 'end_date' => $input['end_date'], 'investigation_reason' => $input['reason'], 'number_of_files' => $input['number_of_files'], 'number_of_investigators' => $input['number_of_investigators'], 'user_id' => access()->user()->id, 'duration' => $end->diffInDays($start) , 'individual_mode' => $input['individual_mode'],'total_budget'=>$total_budget];

        if ($input['category'] != $plan->investigation_category_cv_id)
        {
            $plan_name = $this->returnPlanName($plan->id, (int)$input['category']);
            $data_to_update['plan_name'] = $plan_name;
        }

        if (!empty($input['budget_attachment']) && !empty($request)) {
            $pln_name = !empty($plan_name) ? $plan_name : $plan->plan_name;
            $file_name_and_ext = $request->file('budget_attachment')->getClientOriginalName();
            $filename = pathinfo($file_name_and_ext, PATHINFO_FILENAME);
            $file_ext = $request->file('budget_attachment')->getClientOriginalExtension();
            $store_file_name = date("Y-m-d").'-'.time().'-'.str_replace('/','-',$pln_name).'.'.$file_ext;
            $path = $request->file('budget_attachment')->storeAs('public/investigation_plan/'.$plan->id.'/',$store_file_name);

            $data_to_update['budget_uploaded_name'] = $file_name_and_ext;
            $data_to_update['budget_attachment'] = $store_file_name;
        }
        $this->saveUpdatePlan($plan->id, $data_to_update);

        if ($input['individual_mode'] == 1) {
           ( new InvestigationPlanUserRepository())->updateInvestigators($plan->id,[access()->user()->id]);
       }

       return $plan;
   }

   public function saveUpdatePlan($id, array $update_data)
   {
    $this->query()
    ->where('id', $id)->update($update_data);
}


public function getGeneralInvestigationPlanForDataTable()
{  
  // DB::statement(DB::raw('set row_num()= 0'));
  $report = $this->query() 
  ->select([('*'),
    // DB::raw(("row_number() over (order by s_no) row")),
    DB::raw("CONCAT_WS(' ', coalesce(a.firstname, ''), coalesce(a.middlename, ''), coalesce(a.lastname, '')) as user"),
    'investigation_plans.id as id',
    DB::raw("CONCAT_WS(' ', coalesce(b.firstname, ''), coalesce(b.middlename, ''), coalesce(b.lastname, '')) as investigator"), 
    DB::raw("CONCAT_WS(' ', coalesce(employees.firstname, ''), coalesce(employees.middlename, ''), coalesce(employees.lastname, '')) as employees"),
    DB::raw("CONCAT_WS(' ', coalesce(c.firstname, ''), coalesce(c.middlename, ''), coalesce(c.lastname, '')) as checklist_user"), 
    'notification_reports.id as notification_id',
    DB::raw("employers.name as employer"),
    DB::raw("incident_types.name as incident_type"),
    DB::raw("notification_reports.incident_date as incident_date"),
    DB::raw("notification_reports.receipt_date as receipt_date"),
    DB::raw("notification_reports.created_at as registration_date"),
    DB::raw("regions.name as region"),
    DB::raw("districts.name as district"),
    DB::raw("investigation_plan_notification_reports.created_at as created_at"),
    DB::raw("investigation_feedbacks.created_at as feedback"),
     // DB::raw("fin_years.name as year"),



])
  ->leftJoin(DB::raw('users a'), "investigation_plans.user_id", "=", "a.id")
  ->leftJoin("investigation_plan_users","investigation_plan_users.investigation_plan_id", "=", "investigation_plans.id")
  ->leftJoin(DB::raw('users b'), "investigation_plan_users.user_id", "=", "b.id")
  ->leftJoin("investigation_plan_notification_reports","investigation_plan_notification_reports.investigation_plan_id", "=", "investigation_plans.id")
  ->leftJoin("notification_reports","investigation_plan_notification_reports.notification_report_id", "=", "notification_reports.id")
  ->leftJoin("employees", "employees.id", "=", "notification_reports.employee_id")
  ->leftJoin("employers", "employers.id", "=", "notification_reports.employer_id")
  ->leftJoin("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
  ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
  ->leftJoin("regions", "regions.id", "=", "districts.region_id")
  ->leftJoin(DB::raw('users c'), "notification_reports.allocated", "=", "c.id")
  ->leftJoin("investigation_feedbacks","investigation_feedbacks.notification_report_id", "=", "notification_reports.id");
  // ->leftJoin("investigation_questions","investigation_questions.id", "=", "investigation_feedbacks.investigation_question_id");
 // ->groupBy(\DB::raw('investigation_feedbacks'),('investigation_question_id')));

  $employer = request()->input("employer");
  if ($employer) {
    $report->where("notification_reports.employer_id", "=", $employer);
}
$plan_name = request()->input("plan_name");
if ($plane_name) {
    $report->where("plane_name.id", "=", $plane_name);
}
return $report;
}

public function returnPlanName($id, $category_id)
{
    $category = (new CodeValueRepository)->find($category_id);
    $tdy_mnth = Carbon::parse($purchase_date)->format('m');
    $tdy_yr = Carbon::parse($purchase_date)->format('Y');
    $fy = ($tdy_mnth < 10) ? ($tdy_yr - 1) . '/' . $tdy_yr : ($tdy_yr) . '/' . ($tdy_yr + 1);
    $cat = ($category->name == 'Individual Investigation') ? 'IND' : 'GRP';
    return 'INV/' . $cat . '/' . $fy . '/' . $id;
}

public function returnPlanStageLabel($stage_id,$wf_done)
{
    switch ($stage_id)
    {
        case 0:
        $label = '<div style="width:60%;"><span class="btn btn-sm btn-warning  btn-block">Pending</span></div>';
        break;
        case 1:
        $label = '<div style="width:60%;"><span class="btn btn-sm btn-info  btn-block">On Progress</span></div>';
        break;
        case 2:
        $label = '<div style="width:60%;"><span class="btn btn-sm btn-success  btn-block">Approved</span></div>';
        break;
        case 3:
        if ($wf_done) 
        {
         $label = '<div style="width:60%;"><span class="btn btn-sm btn-danger  btn-block">Rejected</span></div>';
     } else {
        $label = '<div style="width:60%;"><span class="btn btn-sm btn-danger  btn-block">Reversed</span></div>';}

        break;
        case 4:
        $label = '<div style="width:60%;"><span class="btn btn-sm btn-danger  btn-block">Cancelled</span></div>';
        break;
        default:
        $label = '';
        break;
    }
    return $label;
}


public function returnPlanStatusLabel($status)
{
    switch ($status)
    {
        case 0:
        $label = '<div style="width:60%;"><span class="btn btn-sm btn-warning ">In Complete</span></div>';
        break;
        case 1:
        $label = '<div style="width:60%;"><span class="btn btn-sm btn-success ">&nbsp;Completed</span></div>';
        break;
        default:
        $label = '';
        break;
    }
    return $label;
}

public function initiatePlanWorkflow($id)
{
    $plan = $this->findOrThrowException($id);
    $return = DB::transaction(function () use ($id,$plan)
    {
        $type = $plan->total_budget < 1 ? 1 : 2;

        $this->saveUpdatePlan($id, ['wf_done' => 0, 'plan_status' => 1, 'wf_initiator'=>access()->user()->id, 'wf_initiated_at' => Carbon::now() ]);
        $check = workflow([['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $id, 'type' => $type]])->checkIfHasWorkflow();
        if (!$check)
        {
            event(new NewWorkflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $id, 'type' =>$type]));
        }

        return true;
    });
    return $return;
}

public function updateInvestigationPlanWorkflow($plan_id, $level, $plan_status)
{

   $return = DB::transaction(function () use ($plan_id, $level, $plan_status)
   {
    $plan = $this->find($plan_id);
    $level = (int)$level;
    $type = $plan->total_budget < 1 ? 1 : 2;    
    $update_data =  $type == 1 ? $this->updateWfWithoutBudget($plan_id, $level, $plan_status) : $this->updateWfWithBudget($plan_id, $level, $plan_status);
    $this->saveUpdatePlan($plan_id, $update_data);
    $plan = $this->find($plan_id);
    if ($plan->plan_status == 2) {
     $this->insertInvestigationInboxAndTask($plan_id);
 }

 if (($plan->wf_done == 1) && ($plan->plan_status == 3)) {
    $files_repo = new InvestigationPlanNotificationsRepository();
    $notification_reports_id = $files_repo->returnPlanFilesId($plan_id);
    foreach ($notification_reports_id as $plan_notification_id) {
       $files_repo->removeInvestigatorPlanFiles($plan_notification_id,2);
   }
}
return true;
});

   if (!$return) {
    throw new WorkflowException('Action Failed! Please try again');
} 

}


public function updateWfWithoutBudget($plan_id, $level, $plan_status)
{
    $update_data = [];

    if ($plan_status < 1)
    {
        //reversed to level
        $update_data = ['wf_done' => 0, 'plan_status' => 3];
    }
    else
    {
        if (($plan_status == 5) && ($level == 2))
        {
            // decline update wf_done=1 and plan_status=3
            $update_data = ['wf_done' => 1, 'plan_status' => 3, 'wf_done_date'=>Carbon::now()];
        }
        elseif (($plan_status == 1) && ($level == 2))
        {
            //approval update wf_done=1 and plan_status=2
            $update_data = ['wf_done' => 1, 'plan_status' => 2, 'wf_done_date'=>Carbon::now()];
            $this->reAssignAllocatedInvestigator($plan_id);
        }
        else
        {
            // update wf_done=0 and plan_status=1
            $this->canUserForwardWorkflow($plan_id, access()->user()->id);
            $update_data = ['wf_done' => 0, 'plan_status' => 1];
        }

    }

    return $update_data;
}


public function updateWfWithBudget($plan_id, $level, $plan_status)
{
    $update_data = [];

    if (($plan_status == 1) && ($level == 4))
    {
        //approval DG
        $update_data = ['wf_done' => 0, 'plan_status' => 2];
        $this->reAssignAllocatedInvestigator($plan_id);
    }

    if (($plan_status == 5) && ($level == 4))
    {
        // DG decline
        $update_data = ['wf_done' => 1, 'plan_status' => 3, 'wf_done_date'=>Carbon::now()];
    }


    if (($plan_status == 0) && ($level == 1)){
        // manager reverse
        $update_data = ['wf_done' => 0, 'plan_status' => 3];
    }
    
    if (($plan_status == 1) && ($level == 1)){
        // user resubmit
        $this->canUserForwardWorkflow($plan_id, access()->user()->id);
        $update_data = ['wf_done' => 0, 'plan_status' => 1];
    }

    if (($plan_status == 1) && ($level == 7))
    {
        // Finance ()
        $update_data = ['wf_done' => 1, 'wf_done_date'=>Carbon::now()];
    }

    return $update_data;
}


public function reAssignAllocatedInvestigator($plan_id)
{
    $plan_files = (new InvestigationPlanNotificationReport())->query()
    ->where('investigation_plan_id',$plan_id)->get();
    foreach ($plan_files as $plan_file) {
        $investigatior_repo = new NotificationInvestigatorRepository();
        $old_investigator =  $investigatior_repo->query()
        ->where('notification_report_id',$plan_file->notification_report_id)->orderBy('id','desc')->first();
        if (!empty($old_investigator->user_id)) {
            $plan_file->update(['old_notification_investigator'=>$old_investigator->user_id]);
            $old_investigator->update(['user_id' => $plan_file->assigned_to]);
        }else{
            $investigatior_repo->create($plan_file->notification_report_id,['investigator' => $plan_file->assigned_to]);
        }
    }
}


public function insertInvestigationInboxAndTask($plan_id)
{
    $checkerRepo = new CheckerRepository();
    $codeValue = new CodeValueRepository();

    $notifications = (new InvestigationPlanNotificationsRepository())->query()
    ->select('*','notification_report_id as id')->where('investigation_plan_id',$plan_id)->get();

    foreach ($notifications as $notification) {
        $checkr =  $checkerRepo->query()->where('resource_id',$notification->notification_report_id)
        ->where('user_id',$notification->assigned_to)
        ->where('resource_type','App\Models\Operation\InvestigationPlan\InvestigationPlan')->where('status',0)->first();
        if (empty($checkr)) {
         $checkerRepo->create($notification, 
           $input = [
            'comments' => '{"comment":"Notification Planned For Investigation"}',
            'user_id' =>  $notification->assigned_to,
            'checker_category_cv_id' => $codeValue->CHINVPLAN(), 
            'priority' => 1,
        ]);
     }
 }
}


public function updateCompletionStatus()
{
    $plan_files_repo = new InvestigationPlanNotificationsRepository();
    $approved_plans = $this->query()->where('plan_status',2)->where('wf_done',1)->where('is_complete',0)->get();
    foreach ($approved_plans as $plan) {
        $not_investigated = [];
        $plan_files_ids = $plan_files_repo->planAllocatedFilesIds($plan->id);
        $all_investigated_files = $plan_files_repo->returnAllInvestigatedNotificationIds();
        foreach ($plan_files_ids as $notification_id) {
           if (in_array($notification_id, $all_investigated_files)) {
            $this->closePlanInboxTask($notification_id);
            $plan_files_repo->query()->where('investigation_plan_id',$plan->id)
            ->where('notification_report_id',$notification_id)->whereNull('deleted_at')->update(['is_investigated'=>1]);
        }else{
            array_push($not_investigated, $notification_id);
            $plan_files_repo->query()->where('investigation_plan_id',$plan->id)
            ->where('notification_report_id',$notification_id)->whereNull('deleted_at')->update(['is_investigated'=>0]);
        }
    }

    // $not_investigated = [1];
    if (empty($not_investigated)) {
       $plan->is_complete = 1;
       $plan->save(); 
   }else{
     $now = Carbon::now()->format('Y-m-d');
     if (empty($plan->is_complete) && ($now > $plan->end_date)) {
        $end_date = Carbon::parse($plan->end_date)->format('Y-m-d');
        $diff = Carbon::parse($end_date)->diffInDays(Carbon::parse($now));
        if ($diff == 2 && empty($plan->is_reminder_sent)) {
            $this->sendInvestigationPlanReminder($plan, 'reminder');
        } 

        if ($diff == 5 && empty($plan->is_escalation_sent)) {
            $this->sendInvestigationPlanReminder($plan, 'escalation');
        } 
    }
}
}

}


public function sendInvestigationPlanReminder($plan, $email_type)
{
    $user_repo = new UserRepository();
    $manager =  $user_repo->query()->where('designation_id',5)->where('unit_id',14)->first();
    $plan_coordinator = $user_repo->find($plan->user_id);

    switch ($email_type) {
        case 'escalation':
        $data = [
         'plan_id' => $plan->id,
         'to_email' => $plan_coordinator->email,
         'name'  => $plan_coordinator->name,
         'message' => 'This is to inform you that the investigation plan  <a href="'.route('backend.claim.investigations.profile', $plan->id).'"> <strong>'.$plan->plan_name.'</strong></a> which has reached its deadline on <strong>'.Carbon::parse($plan->end_date)->format('dS F, Y').'</strong> has not been completely investigated',
         'cc_email' => $plan_coordinator->email
     ];
     $plan->is_escalation_sent = 1;
     $plan->escalation_sent_date = Carbon::now();
     $plan->save();
     break;
     default:
        //reminder
     $data = [
        'plan_id' => $plan->id,
        'to_email' =>  $plan_coordinator->email,
        'name'  => $plan_coordinator->name,
        'message' => 'This is to remind you to fill the investigation feedbacks and upload reports on  investigation plan <a href="'.route('backend.claim.investigations.profile', $plan->id).'"> <strong>'.$plan->plan_name.'</strong></a> which has reached its deadline on <strong>'.Carbon::parse($plan->end_date)->format('dS F, Y').'</strong>',
        'cc_email'=> $plan_coordinator->email
    ];
    $plan->is_reminder_sent = 1;
    $plan->reminder_sent_date = Carbon::now();
    $plan->save();
    break;
}
    // dump($data);
Mail::to($data['to_email'])->cc([$data['cc_email']])->send(new InvestigationPlanReminder($data));

}



public function updateNotificationInvestigationPlanStatus($incident_id)
{
    $plan = $this->query()
    ->select('plan_status','investigation_plan_notification_reports.*','investigation_plan_notification_reports.id as plan_notification_id')
    ->join("investigation_plan_notification_reports", "investigation_plans.id", "=", "investigation_plan_notification_reports.investigation_plan_id")
    ->where('plan_status',2)
    ->where('notification_report_id',$incident_id)
    ->where('is_investigated',0)
    ->first();
    if (!empty($plan->plan_notification_id)) {
        $plan_files_repo = new InvestigationPlanNotificationsRepository();
        $plan_files_repo->query()->where('id',$plan->plan_notification_id)->update(['is_investigated'=>1]);
        $this->closePlanInboxTask($plan->notification_report_id);
    }
}




public function closePlanInboxTask($notification_report_id)
{
  $checkerRepo = new CheckerRepository();
  $codeValue = new CodeValueRepository();
  $checkerRepo->query()->where('checker_category_cv_id',$codeValue->CHINVPLAN())->where('resource_id',$notification_report_id)->update(['status' =>1]);
}



public function canUserForwardWorkflow($plan_id, $user_id)
{
    $has_pending = $this->query()->where('user_id', $user_id)
    ->where('id', $plan_id)->where('plan_status',3)->where('wf_done',0)->first();
    if (count($has_pending))
    {
        $plan_users_id = (new InvestigationPlanUserRepository())->query()
        ->where('investigation_plan_id', $plan_id)->pluck('user_id')
        ->all();
        $plan_notifications = (new InvestigationPlanNotificationsRepository())->query()
        ->where('investigation_plan_id', $plan_id)->pluck('assigned_to')
        ->all();

        if (count($plan_users_id) != $has_pending->number_of_investigators)
        {
            throw new WorkflowException('Action Failed! The allocated investigators do not match the plan');
        }

        if (count($plan_notifications) != $has_pending->number_of_files)
        {
            throw new WorkflowException('Action Failed! The assigned files ' . count($plan_notifications) . ' do not match the planed ' . $has_pending->number_of_files);
        }

        if (count(array_diff($plan_users_id, $plan_notifications)))
        {
            throw new WorkflowException('Action Failed! ' . count(array_diff($plan_users_id, $plan_notifications)) . ' investigators have not been assigned any files');
        }
    }
    else
    {
        throw new WorkflowException('Action Failed! You are not authorized to perform that action');
    }
}
public function returnPlanStatus($status)
{
    switch ($status)
    {
        case 0:
        $label = '<div style="width:60%;">Pending</div>';
        break;
        case 1:
        $label = '<div style="width:60%;">Complete</div>';
        break;
        default:
        $label = '';
        break;
    }
    return $label;
}
}

