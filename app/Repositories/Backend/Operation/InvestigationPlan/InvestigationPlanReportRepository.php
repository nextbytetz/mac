<?php
namespace App\Repositories\Backend\Operation\InvestigationPlan;

use App\Events\NewWorkflow;
use App\Exceptions\WorkflowException;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use App\Models\Workflow\WfTrack;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\InvestigationPlan\InvestigationPlan;
use App\Models\Operation\InvestigationPlan\InvestigationOfficer;
use App\Models\Operation\InvestigationPlan\InvestigationPlanUser;
use App\Models\Operation\InvestigationPlan\InvestigationPlanNotificationReport;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationOfficerRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanUserRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;

class InvestigationPlanReportRepository extends BaseRepository
{


   public function __construct()
   {
     $this->allocation = new InvestigationPlanNotificationsRepository();
 }


 public function getGeneralInvestigationPlanForDataTable()
 {  
  // DB::statement(DB::raw('set row_num()= 0'));
  $report = $this->query() 
  ->select([('*'),
    // DB::raw(("row_number() over (order by s_no) row")),
    DB::raw("CONCAT_WS(' ', coalesce(a.firstname, ''), coalesce(a.middlename, ''), coalesce(a.lastname, '')) as user"),
    'investigation_plans.id as id',
    DB::raw("CONCAT_WS(' ', coalesce(b.firstname, ''), coalesce(b.middlename, ''), coalesce(b.lastname, '')) as investigator"), 
    DB::raw("CONCAT_WS(' ', coalesce(employees.firstname, ''), coalesce(employees.middlename, ''), coalesce(employees.lastname, '')) as employees"),
    DB::raw("CONCAT_WS(' ', coalesce(c.firstname, ''), coalesce(c.middlename, ''), coalesce(c.lastname, '')) as checklist_user"), 
    'notification_reports.id as notification_id',
    DB::raw("employers.name as employer"),
    DB::raw("incident_types.name as incident_type"),
    DB::raw("notification_reports.incident_date as incident_date"),
    DB::raw("notification_reports.receipt_date as receipt_date"),
    DB::raw("notification_reports.created_at as registration_date"),
    DB::raw("regions.name as region"),
    DB::raw("districts.name as district"),
    DB::raw("investigation_plan_notification_reports.created_at as created_at"),
    DB::raw("investigation_feedbacks.created_at as feedback"),
     // DB::raw("fin_years.name as year"),



])
  ->leftJoin(DB::raw('users a'), "investigation_plans.user_id", "=", "a.id")
  ->leftJoin("investigation_plan_users","investigation_plan_users.investigation_plan_id", "=", "investigation_plans.id")
  ->leftJoin(DB::raw('users b'), "investigation_plan_users.user_id", "=", "b.id")
  ->leftJoin("investigation_plan_notification_reports","investigation_plan_notification_reports.investigation_plan_id", "=", "investigation_plans.id")
  ->leftJoin("notification_reports","investigation_plan_notification_reports.notification_report_id", "=", "notification_reports.id")
  ->leftJoin("employees", "employees.id", "=", "notification_reports.employee_id")
  ->leftJoin("employers", "employers.id", "=", "notification_reports.employer_id")
  ->leftJoin("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
  ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
  ->leftJoin("regions", "regions.id", "=", "districts.region_id")
  ->leftJoin(DB::raw('users c'), "notification_reports.allocated", "=", "c.id")
  ->leftJoin("investigation_feedbacks","investigation_feedbacks.notification_report_id", "=", "notification_reports.id");
  // ->leftJoin("investigation_questions","investigation_questions.id", "=", "investigation_feedbacks.investigation_question_id");
 // ->groupBy(\DB::raw('investigation_feedbacks'),('investigation_question_id')));

  $employer = request()->input("employer");
  if ($employer) {
    $report->where("notification_reports.employer_id", "=", $employer);
}
$plan_name = request()->input("plan_name");
if ($plane_name) {
    $report->where("plane_name.id", "=", $plane_name);
}
return $report;
}


public function returnPlanFilesId($plan_id)
{
 return $this->query()
 ->where('investigation_plan_id',$plan_id)
 ->pluck('id')->all();
}

public function allocatedFilesIdToOtherPlan($plan_id)
{
   return $this->query()
   ->where('investigation_plan_notification_reports.investigation_plan_id','!=',$plan_id)
   ->pluck('notification_report_id')->all();
}


public function getInvestigationReportForDatatable()
{
    // dd(1);
    // $already_allocated = $this->allocatedFilesIdToOtherPlan($plan_id);
    // $plan = (new InvestigationPlanRepository())->find($plan_id);
    $investigation = $this->getInvestigationReportQueryFilter()
    ->select($this->getInvestigationReportSelectQuery());

    return $investigation;
    // $datatables = app('datatables')->of($investigation);
    // return $datatables;
}


public function getInvestigationReportQueryFilter()
{
    // dd(request()->all);
    $codeValue = new CodeValueRepository();
    $allocation = $this->getInvestigationReportQuery();
    $plan_notification_repo = new InvestigationPlanNotificationsRepository();

    $incident = (int) request()->input("incident");
    if ($incident) {
        $allocation->where("notification_reports.incident_type_id", "=", $incident);
    }
    $employee = request()->input("employee");
    if ($employee) {
        $allocation->where("notification_reports.employee_id", "=", $employee);
    }
    $employer = request()->input("employer");
    if ($employer) {
        $allocation->where("notification_reports.employer_id", "=", $employer);
    }
    $region = request()->input("region");
    if ($region) {
        $allocation->where("regions.id", "=", $region);
    }
    $district = request()->input("district");
    if ($district) {
        $allocation->where("districts.id", "=", $district);
    }
    $status = request()->input('status');
    $plan_status = request()->input('plan_status');

    if ($plan_status) {
        $allocated_case_ids = (new InvestigationPlanNotificationsRepository())->allocatedFilesIds();
        $allocation = ($plan_status==1) ? $allocation->whereIn("notification_reports.id", $allocated_case_ids) : $allocation->whereNotIn("notification_reports.id", $allocated_case_ids);
    }
    
    $report = request()->input('report');
    if ($report) {
        $report_case_ids = $plan_notification_repo->returnAllNotificationIdsWithReportUploaded();
        $allocation = ($report==1) ? $allocation->whereIn("notification_reports.id",$report_case_ids) : $allocation->whereNotIn("notification_reports.id", $report_case_ids);
    }


    $investigation_status = request()->input('investigation_status');
    if ($investigation_status) {
        $investigated_ids = $plan_notification_repo->returnAllInvestigatedNotificationIds();
        if ($investigation_status == 1) {
         $allocation =  $allocation->whereIn("notification_reports.id",$investigated_ids);
     }
        if ($investigation_status == 2) {

           $partial_investigation = ($report_case_ids = $plan_notification_repo->returnAllNotificationIdsWithReportUploaded() &&

             $feedbak_investigation_ids = $plan_notification_repo->returnAllNotificationIdsWithFeedbackFilled());
             
             // dd($partial_investigation);
         // $allocation =  $allocation->whereIn("notification_reports.id",$investigated_ids);
     }
      else {
        $allocation = $allocation->whereNotIn("notification_reports.id",$investigated_ids);
    }
}


$questionnaire = request()->input('questionnaire');
if ($questionnaire) {
    $feedback_ids = $plan_notification_repo->returnAllNotificationIdsWithFeedbackFilled();
    $allocation = ($questionnaire==1) ? $allocation->whereIn("notification_reports.id",$feedback_ids) : $allocation->whereNotIn("notification_reports.id", $feedback_ids);
}





        //check allocation status
switch ($status) {
    case 1:
                //Allocated
        // $allocation->whereIn("notification_reports.id", $this->allocatedFilesIds());
    break;
    case 2:
            //Unallocated
        // $allocation->whereNotIn("notification_reports.id", $this->allocatedFilesIds());
    break;
    case 3:
                //Allocated to User
    $user_id = request()->input('user_id');
        // $allocation->whereIn("notification_reports.id", $this->allocatedFilesIds());
    if($user_id){
            // $allocation->whereIn("investigation_plan_notification_reports.assigned_to", [$user_id]);
    }

    break;
}
return $allocation;
}


public function getInvestigationReportSelectQuery()
{
    $codeValue = new CodeValueRepository();
    $onInvestigation = "({$codeValue->NSPCRFI()}, {$codeValue->NSOPRFLI()})";
    return [
        DB::raw("notification_reports.id as id"),
        DB::raw("notification_reports.filename"),
        DB::raw("incident_types.name as incident_type"),
        DB::raw("case when notification_reports.employee_id is null then notification_reports.employee_name else concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) end as employee"),
        DB::raw("case when notification_reports.employer_id is null then notification_reports.employer_name else employers.name end as employer"),
        DB::raw("notification_reports.incident_date"),
        DB::raw("notification_reports.receipt_date as notification_date"),
        DB::raw("notification_reports.created_at as registration_date"),
        DB::raw("regions.name region"),
        DB::raw("districts.name as district"),
        DB::raw("CONCAT_WS(' ', coalesce(c.firstname, ''), coalesce(c.middlename, ''), coalesce(c.lastname, '')) as checklist_user"),
    ];
}


public function getInvestigationReportQuery()
{
    $closedIncident = (new CodeValueRepository())->NSCINC();
    $allocation = (new NotificationReportRepository())->query()
    ->leftJoin("employees", "notification_reports.employee_id", "=", "employees.id")
    ->leftJoin("employers", "notification_reports.employer_id", "=", "employers.id")
    ->join("incident_types", "notification_reports.incident_type_id", "=", "incident_types.id")
    ->leftJoin("districts", "notification_reports.district_id", "=", "districts.id")
    ->leftJoin("regions", "districts.region_id", "=", "regions.id")
    ->join("code_values", "notification_reports.notification_staging_cv_id", "=", "code_values.id")
    ->leftJoin(DB::raw('users c'), "notification_reports.allocated", "=", "c.id")
    ->where("notification_staging_cv_id", "<>", $closedIncident);
    return $allocation;
}

public function returnInvestigationStatus()
{

    dd('jamaa');
}


}

