<?php
namespace App\Repositories\Backend\Operation\InvestigationPlan;

use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\InvestigationPlan\InvestigationOfficer;
use App\Repositories\Backend\Access\UserRepository;

class InvestigationOfficerRepository extends BaseRepository
{
    const MODEL = InvestigationOfficer::class;

    public function __construct()
    {
        parent::__construct();
    }

    public function getInvestigationMastersId()
    {
        return $this->query()
        ->where('is_master', true)
        ->pluck("user_id")
        ->all();
    }

    public function getAllInvestigatorsId()
    {
        return $this->query()
        ->pluck("user_id")
        ->all();
    }

    public function findCordinatorByUserId($user_id)
    {
        return $this->query()
        ->where('user_id',$user_id)
        ->where('is_master',true)->first();
    }


    public function updateInvestigationOfficers(array $investigation_officers)
    {
        $return = DB::transaction(function () use ($investigation_officers)
        {
            $all = [];
            if (array_key_exists('authorised_investigators', $investigation_officers))
            {
                $all = array_merge($all, $investigation_officers['authorised_investigators']);
                foreach ($investigation_officers['authorised_investigators'] as $key => $officer_id)
                {
                    $this->addInvestigationOfficer($officer_id);
                }
            }

            if (array_key_exists('investigation_masters', $investigation_officers))
            {
                $all = array_merge($all, $investigation_officers['investigation_masters']);
                foreach ($investigation_officers['investigation_masters'] as $key => $master_id)
                {
                    $this->addInvestigationOfficer($master_id, true);
                }
            }
            $this->removeInvestigationOfficers($all);
            return true;
        });
        return $return;
    }

    public function addInvestigationOfficer($officer_id, $is_master = false)
    {
        $this->query()
        ->withTrashed()
        ->updateOrCreate(['user_id' => $officer_id], ['deleted_at' => NULL, 'is_master' => $is_master, 'added_by' => access()->user()->id, 'removed_by' => NULL, ]);
    }

    public function removeInvestigationOfficers(array $officers_id)
    {
        $this->query()
        ->whereNotIn('user_id', $officers_id)->update(['removed_by' => access()
            ->user()->id]);
        $this->query()
        ->whereNotIn('user_id', $officers_id)->delete();
    }

    public function returnAllInvestigationOfficers()
    {
        $officers_id = $this->getAllInvestigatorsId();
        $all_invesigators = (new UserRepository())->query()
        ->whereIn('id', $officers_id)->get()
        ->pluck("name", "id");
        return  count($all_invesigators) ? $all_invesigators->toArray() : [];

    }

}

