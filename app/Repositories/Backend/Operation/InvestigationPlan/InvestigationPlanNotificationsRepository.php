<?php
namespace App\Repositories\Backend\Operation\InvestigationPlan;


use App\Events\NewWorkflow;
use App\Exceptions\JobException;
use App\Exceptions\RedirectException;
use App\Exceptions\WorkflowException;
use App\Jobs\Claim\DeleteNotificationDms;
use App\Jobs\Claim\PostDocumentDms;
use App\Jobs\Claim\PostNotificationDms;
use App\Jobs\Notifications\SendSmsEga;
use App\Models\Operation\Claim\NotificationReport;
use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\NotificationReportAppeal;
use App\Notifications\Backend\Operation\Claim\Portal\ClaimAccountValidated;
use App\Repositories\Backend\Access\IncidentUserRegionRepository;
use App\Repositories\Backend\Access\IncidentUserRepository;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\Notification\Legacy;
use App\Repositories\Backend\Operation\Claim\Notification\Progressive;
use App\Repositories\Backend\Operation\Claim\Portal\ClaimActivityRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Sysdef\CodeRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Repositories\Backend\Operation\Claim\NotificationInvestigatorRepository;
use App\Services\Claim\NotificationDocEOfficeAutoSync;
use App\Services\Finance\ProcessPaymentPerBenefit;
use App\Services\Notifications\Api;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Auth\User;
use App\Models\Operation\InvestigationPlan\InvestigationPlanUser;
use App\Models\Operation\InvestigationPlan\InvestigationPlanNotificationReport;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanUserRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanReportRepository;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;

class InvestigationPlanNotificationsRepository extends BaseRepository
{
  const MODEL = InvestigationPlanNotificationReport::class;

  public function __construct()
  {
    parent::__construct();
  }


  public function returnPlanFilesId($plan_id)
  {
   return $this->query()
   ->where('investigation_plan_id',$plan_id)
   ->pluck('id')->all();
 }

 public function allocatedFilesIdToOtherPlan($plan_id)
 {
   return $this->query()
   ->where('investigation_plan_notification_reports.investigation_plan_id','!=',$plan_id)
   ->pluck('notification_report_id')->all();
 }


 public function allUserAllocatedFilesId($user_id)
 {
   return $this->query()
   ->where('investigation_plan_notification_reports.assigned_to',$user_id)
   ->pluck('notification_report_id')->all();
 }


 public function allUserPendingAllocatedFilesId($user_id)
 {
   return $this->query()
   ->where('investigation_plan_notification_reports.assigned_to',$user_id)
   ->where('is_investigated',0)
   ->pluck('notification_report_id')->all();
 }




 public function getAllocatedFilesDatatable($investigation_plan_id)
 {
  $allocation = $this->getFilesAllocationQueryFilter()->select($this->getFilesAllocationSelectQuery())
  ->whereIn("notification_reports.id", $this->planAllocatedFilesIds($investigation_plan_id));
  $datatables = app('datatables')->of($allocation);
  return $datatables;
}


public function getFilesAllocationQueryFilter()
{
    // dd(request()->all);
  $codeValue = new CodeValueRepository();
  $allocation = $this->getFilesAllocationQuery();

  $incident = (int) request()->input("incident");
  if ($incident) {
    $allocation->where("notification_reports.incident_type_id", "=", $incident);
  }
  $employee = request()->input("employee");
  if ($employee) {
    $allocation->where("notification_reports.employee_id", "=", $employee);
  }
  $employer = request()->input("employer");
  if ($employer) {
    $allocation->where("notification_reports.employer_id", "=", $employer);
  }
  $region = request()->input("region");
  if ($region) {
    $allocation->where("regions.id", "=", $region);
  }
  $district = request()->input("district");
  if ($district) {
    $allocation->where("districts.id", "=", $district);
  }
  $status = request()->input('status');
        //check allocation status
  switch ($status) {
    case 1:
                //Allocated
    $allocation->whereIn("notification_reports.id", $this->allocatedFilesIds());
    break;
    case 2:
            //Unallocated
    $allocation->whereNotIn("notification_reports.id", $this->allocatedFilesIds());
    break;
    case 3:
                //Allocated to User
    $user_id = request()->input('user_id');
    $allocation->whereIn("notification_reports.id", $this->allocatedFilesIds());
    if($user_id){
      $allocation->whereIn("notification_reports.id", $this->allUserAllocatedFilesId($user_id));
    }
    break;
  }
  return $allocation;
}


public function getFilesAllocationSelectQuery()
{
  $codeValue = new CodeValueRepository();
  $onInvestigation = "({$codeValue->NSPCRFI()}, {$codeValue->NSOPRFLI()})";
  return [
    DB::raw("notification_reports.id as id"),
    DB::raw("notification_reports.id as notification_report_id"),
    DB::raw("notification_reports.filename"),
    DB::raw("notification_reports.receipt_date"),
    DB::raw("notification_reports.created_at"),
    DB::raw("notification_reports.incident_date"),
    DB::raw("notification_reports.reporting_date"),
    DB::raw("notification_reports.created_at as registration_at"),
    DB::raw("notification_reports.employer_id"),
    DB::raw("districts.name as district"),
    DB::raw("districts.id as district_id"),
    DB::raw("regions.name region"),
    DB::raw("notification_reports.notification_staging_cv_id"),
    DB::raw("case when notification_reports.employee_id is null then notification_reports.employee_name else concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) end as employee"),
    DB::raw("case when notification_reports.employer_id is null then notification_reports.employer_name else employers.name end as employer"),
    DB::raw("incident_types.name as incident"),
  ];
}


public function getFilesAllocationQuery()
{
  $closedIncident = (new CodeValueRepository())->NSCINC();
  $allocation = NotificationReport::
  leftJoin("employees", "notification_reports.employee_id", "=", "employees.id")
  ->leftJoin("employers", "notification_reports.employer_id", "=", "employers.id")
  ->join("incident_types", "notification_reports.incident_type_id", "=", "incident_types.id")
  ->leftJoin("districts", "notification_reports.district_id", "=", "districts.id")
  ->leftJoin("regions", "districts.region_id", "=", "regions.id")
  ->join("code_values", "notification_reports.notification_staging_cv_id", "=", "code_values.id");
  // ->where("notification_staging_cv_id", "<>", $closedIncident);
  
  return $allocation;
}

public function allocatedFilesIds()
{
 return $this->query()->pluck("notification_report_id")->all();
}

public function planAllocatedFilesIds($plan_id)
{
  $plan = (new InvestigationPlanRepository())->find($plan_id);

  if ($plan->plan_status > 2 && $plan->wf_done == 1) {
   return $this->query()->where('investigation_plan_id',$plan_id)->where('removal_reason','>',1)->withTrashed()->pluck("notification_report_id")->all();
 } else {
  return $this->query()->where('investigation_plan_id',$plan_id)->pluck("notification_report_id")->all();
}
}

public function saveInvestigatorPlanFiles($plan_id, array $input)
{

  $plan = (new InvestigationPlanRepository())->find($plan_id);
  $total_assigned = $this->query()->where('investigation_plan_id',$plan_id)->count();
  $remain = $plan->number_of_files - $total_assigned;
  if ($remain < count($input['id'])) {
    $message = $remain > 0 ? 'You have already assigned '.$total_assigned.' cases out of '.$plan->number_of_files.'. You can not assign more than '.$remain.' file(s)' : 'You have already assigned '.$total_assigned.' cases out of '.$plan->number_of_files.'. You can not assign any more  file(s)';
    return response()->json(['success' => false, 'message' => $message]);
  }

  $files_assigned_to_other_user = $this->query()
    // ->where('investigation_plan_id',$plan_id)
  ->whereIn('notification_report_id',$input['id'])->where('assigned_to','!=',(int)$input['assigned_user'])->pluck('notification_report_id')->all();
  $unique_files = array_diff($input['id'], $files_assigned_to_other_user);

  $return = DB::transaction(function () use ($plan_id, $input, $unique_files)
  {
    $assigned_to = (int)$input['assigned_user'];
    $plan_user = (new InvestigationPlanUserRepository())->query()->where('investigation_plan_id',$plan_id)->where('user_id',$assigned_to)->first();
    foreach ($unique_files as $notification_report_id) {
     $this->query()
     ->withTrashed()
     ->updateOrCreate([
      'assigned_to' => $assigned_to, 
      'notification_report_id' =>(int)$notification_report_id,
      'investigation_plan_id' =>$plan_id
    ], 
    [
      'deleted_at' => NULL,'added_by' => access()->user()->id, 
      'removed_by' => NULL, 'investigation_plan_user_id' =>$plan_user->id,
      'removal_reason'=>1
    ]);
   }
   return true;
 });

  if (count($files_assigned_to_other_user)) {
    if (count($files_assigned_to_other_user) == count($input['id'])) {
      return response()->json(['success' => false, 'message' => 'Files have already been assigned to other investigators']);
    } else {
     return response()->json(['success' => true, 'message' => count($unique_files).' file(s) have been assigned to selected investigator!']);
   }
 }

 return $return == true ? response()->json(['success' => true, 'message' => 'Success, investigator has been assigned the selected file(s)']) :    response()->json(['success' => false, 'message' => 'Error has occured! Please try again']);

}

public function removeInvestigatorPlanFiles($plan_notification_id,$removal_reason = null)
{
  $file = $this->query()->where('id', $plan_notification_id);
  $file->update(['removed_by' => access()->user()->id,'removal_reason'=>$removal_reason]);
  $file->delete();
  return response()->json(['success' => true, 'message' => 'Success, The assigned file has been removed']);
}


public function checkFileStatusInInvestigationPlan($notification_report_id)
{
  $notification_reports = $this->query()->where('notification_report_id',$notification_report_id)->get();
  $check_array = [];
  foreach ($notification_reports as $notification) {
    $plan = (new InvestigationPlanRepository())->find($notification->investigation_plan_id);
    if (empty($plan->wf_done) && in_array($plan->plan_status,[0,1,3])) {
      array_push($check_array, $notification->investigation_plan_id);
    }
  }
  if (count($check_array)) {
    throw new GeneralException('Action Failed! This File exists in a pending investigation plan');
  }
}


public function returnAssignedUserName($plan_id,$notification_report_id)
{
 return $this->query()->select(DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(middlename, ''), coalesce(lastname, '')) as user_name"), 'users.id as user_id','investigation_plan_notification_reports.id as plan_notification_id')
 ->join("main.users", "users.id", "=", "investigation_plan_notification_reports.assigned_to")
 ->where('investigation_plan_notification_reports.investigation_plan_id',$plan_id)
 ->where('investigation_plan_notification_reports.notification_report_id',$notification_report_id)->first();

}


public function returnRejectedAssignedUserName($plan_id,$notification_report_id)
{
 return $this->query()->select(DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(middlename, ''), coalesce(lastname, '')) as user_name"), 'users.id as user_id','investigation_plan_notification_reports.id as plan_notification_id')
 ->join("main.users", "users.id", "=", "investigation_plan_notification_reports.assigned_to")
 ->where('investigation_plan_notification_reports.investigation_plan_id',$plan_id)
 ->where('investigation_plan_notification_reports.notification_report_id',$notification_report_id)
 ->where('removal_reason',2)
 ->withTrashed()->first();

}

public function returnPlanByFilesId($notification_report_id)
{
 return $this->query()
 ->join('main.investigation_plans','investigation_plan_notification_reports.investigation_plan_id', '=', 'investigation_plans.id')
 ->where('notification_report_id',$notification_report_id)->first();
}


public function getUserPendingAllocatedFilesDatatable($user_id)
{
  $user_allocated = $this->allUserPendingAllocatedFilesId($user_id);
  $allocation = $this->getFilesAllocationQueryFilter()
  ->select($this->getFilesAllocationSelectQuery())->whereIn('notification_reports.id',$user_allocated);
  $datatables = app('datatables')->of($allocation);
  return $datatables;
}


public function getUserAllocatedFilesDatatable($user_id)
{
  $user_allocated = $this->allUserAllocatedFilesId($user_id);
  $allocation = $this->getFilesAllocationQueryFilter()
  ->select($this->getFilesAllocationSelectQuery())->whereIn('notification_reports.id',$user_allocated);
  $datatables = app('datatables')->of($allocation);
  return $datatables;
}


public function returnAllNotificationIdsWithReportUploaded()
{
  $report_ids = DB::table('main.document_notification_report')
  ->where('document_id',6)->whereNull('deleted_at')->pluck('notification_report_id')->all();
  return array_unique($report_ids);
}

public function returnAllNotificationIdsWithFeedbackFilled()
{
  $feedback_ids = DB::table('main.investigation_feedbacks')
  ->whereNotNull('feedback')->pluck('notification_report_id')->all();
  return array_unique($feedback_ids);
}


public function returnAllInvestigatedNotificationIds()
{
  $feedback_ids = $this->returnAllNotificationIdsWithFeedbackFilled();
  $report_ids = $this->returnAllNotificationIdsWithReportUploaded();
  $return = array_unique(array_intersect($feedback_ids,$report_ids));
  return $return;
}


public function returnInvestigationStatusLabel($plan_id, $notification_report_id)
{
  $file = $this->query()->where('investigation_plan_id',$plan_id)->where('notification_report_id',$notification_report_id)->first();

  if (!empty($file->id)) {
    $label =  !empty($file->is_investigated) ? '<span class="tag tag-success white_color">Investigated</span>' : '<span class="tag tag-warning white_color">Not Investigated</span>';
  }else{
    $label = '';
  }
  return $label;
}


public function returnNotificationInvestigationStatus($plan_id, $notification_report_id)
{
  $file = $this->query()->where('investigation_plan_id',$plan_id)->where('notification_report_id',$notification_report_id)->first();

  if (!empty($file->id)) {
    $label =  !empty($file->is_investigated) ? 'Investigated' : 'Not Investigated ';
  }else{
    $label = '';
  }
  return $label;
} 

public function exportExcel($plan_id, $action, $file_from = null)
{
  $plan = (new InvestigationPlanRepository())->find($plan_id);
  switch ($file_from) {
    case 'allocated_files':
    $allocation = $this->getFilesAllocationQueryFilter()->select($this->getFilesAllocationSelectQuery())
    ->whereIn("notification_reports.id", $this->planAllocatedFilesIds($plan_id));
    break;
    default:
    $already_allocated = $this->allocatedFilesIdToOtherPlan($plan_id);
    $allocation = $this->getAllFilesForAllocationQueryFilter()
    ->select($this->getFilesAllocationSelectQuery())->whereNotIn('notification_reports.id',$already_allocated);
    // ->where("code_values.reference","NSPCRFI");
    $allocation =  $plan->plan_category == 'Individual Investigation' ? $allocation->where('notification_reports.allocated',access()->user()->id) : $allocation;
    break;
  }

  if ($action == 'excel') {
    $this->buildExcelFile($allocation,$plan,$file_from)->download('xls');
  } else {
    $this->buildExcelFile($allocation,$plan,$file_from)->download('csv');
  }
}


protected function buildExcelFile($allocation,$plan,$file_from)
{
  /** @var \Maatwebsite\Excel\Excel $excel */
  $excel = app('excel');

  return $excel->create(time(), function (LaravelExcelWriter    $excel) use($allocation,$plan,$file_from) {
    $excel->sheet('exported-data', function (LaravelExcelWorksheet $sheet) use($allocation,$plan,$file_from){
     $allocation->chunk(2000,function($modelInstance) use($sheet,$plan,$file_from) {

      if ($file_from == 'allocated_files') {
        $sheet->row(1, ['Case No#','Incident Type','Employee','Employer','Incident Date','Reporting Date','Receipt Date','Registration Date','District','Region','Assigned Investigator','Investigation Status']); 
      } else {
        $sheet->row(1, ['Case No#','Incident Type','Employee','Employer','Incident Date','Reporting Date','Receipt Date','Registration Date','District','Region','Assigned Investigator']); 
      }
      
      $modelAsArray = $modelInstance->toArray();
      $init_a = 2;
      $model = $modelAsArray;
      for($a = 0; $a < count($model); $a++)
      {
        $sheet->cell('A'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['filename']);
        });
        $sheet->cell('B'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['incident']);
        });
        $sheet->cell('C'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['employee']);
        });
        $sheet->cell('D'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['employer']);
        });
        $sheet->cell('E'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['incident_date']);
        });
        $sheet->cell('F'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['reporting_date']);
        });
        $sheet->cell('G'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['receipt_date']);
        });
        $sheet->cell('H'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['registration_at']);
        });
        $sheet->cell('I'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['district']);
        });
        $sheet->cell('J'.$init_a, function($cell) use ($model, $a) {
          $cell->setValue($model[$a]['region']);
        });

        $sheet->cell('K'.$init_a, function($cell) use ($model, $a,$plan) {
         if ($plan->plan_status > 2 && $plan->wf_done == 1) {
          $user = $this->returnRejectedAssignedUserName($plan->id,$model[$a]['notification_report_id']);
        } else {
          $user = $this->returnAssignedUserName($plan->id,$model[$a]['notification_report_id']);
        }
        count($user) ? $cell->setValue($user->user_name) : $cell->setValue('');
      });

        if ($file_from == 'allocated_files') {  
          $sheet->cell('L'.$init_a, function($cell) use ($model, $a,$plan,$file_from) {
            $cell->setValue($this->returnNotificationInvestigationStatus($plan->id, $model[$a]['notification_report_id'])); 
          }); 
        }
        $init_a ++;
      }
    });
   });
  });
}

public function getFilesForAllocationDatatable($plan_id)
{
  //when allocating
  $already_allocated = $this->allocatedFilesIdToOtherPlan($plan_id);
  $plan = (new InvestigationPlanRepository())->find($plan_id);
  $allocation = $this->getAllFilesForAllocationQueryFilter()
  ->select($this->getFilesAllocationSelectQuery())->whereNotIn('notification_reports.id',$already_allocated);
  // ->where("code_values.reference","NSPCRFI");

  if ($plan->plan_category == 'Individual Investigation') {$allocation = $allocation->where('notification_reports.allocated',access()->user()->id);}

  $datatables = app('datatables')->of($allocation);
  return $datatables;
}

public function notificationReportWithQstnr()
{
  return DB::table('main.document_notification_report')->where('document_id',6)->pluck("notification_report_id")->all();
}

public function getAllFilesForAllocationQuery()
{
  $closedIncident = (new CodeValueRepository())->NSCINC();
  $incidentWithQstnr = $this->notificationReportWithQstnr();
  $allocation = NotificationReport::
  leftJoin("employees", "notification_reports.employee_id", "=", "employees.id")
  ->leftJoin("employers", "notification_reports.employer_id", "=", "employers.id")
  ->join("incident_types", "notification_reports.incident_type_id", "=", "incident_types.id")
  ->leftJoin("districts", "notification_reports.district_id", "=", "districts.id")
  ->leftJoin("regions", "districts.region_id", "=", "regions.id")
  ->join("code_values", "notification_reports.notification_staging_cv_id", "=", "code_values.id")
  ->where('investigation_validity',0)
  ->whereNotIn('notification_reports.id',$incidentWithQstnr)
  ->whereIn('notification_staging_cv_id',[230,231,232,233,235,236]);
  
  return $allocation;
}


public function getAllFilesForAllocationQueryFilter()
{
    // dd(request()->all);
  $codeValue = new CodeValueRepository();
  $allocation = $this->getAllFilesForAllocationQuery();

  $incident = (int) request()->input("incident");
  if ($incident) {
    $allocation->where("notification_reports.incident_type_id", "=", $incident);
  }
  $employee = request()->input("employee");
  if ($employee) {
    $allocation->where("notification_reports.employee_id", "=", $employee);
  }
  $employer = request()->input("employer");
  if ($employer) {
    $allocation->where("notification_reports.employer_id", "=", $employer);
  }
  $region = request()->input("region");
  if ($region) {
    $allocation->where("regions.id", "=", $region);
  }
  $district = request()->input("district");
  if ($district) {
    $allocation->where("districts.id", "=", $district);
  }
  $status = request()->input('status');
        //check allocation status
  switch ($status) {
    case 1:
                //Allocated
    $allocation->whereIn("notification_reports.id", $this->allocatedFilesIds());
    break;
    case 2:
            //Unallocated
    $allocation->whereNotIn("notification_reports.id", $this->allocatedFilesIds());
    break;
    case 3:
                //Allocated to User
    $user_id = request()->input('user_id');
    $allocation->whereIn("notification_reports.id", $this->allocatedFilesIds());
    if($user_id){
      $allocation->whereIn("notification_reports.id", $this->allUserAllocatedFilesId($user_id));
    }
    break;
  }
  return $allocation;
}


}

