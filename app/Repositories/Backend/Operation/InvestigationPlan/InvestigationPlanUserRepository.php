<?php
namespace App\Repositories\Backend\Operation\InvestigationPlan;

use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Repositories\Backend\Access\UserRepository;
use App\Models\Operation\InvestigationPlan\InvestigationPlanUser;
use App\Models\Operation\InvestigationPlan\InvestigationPlanNotificationReport;

class InvestigationPlanUserRepository extends BaseRepository
{
    const MODEL = InvestigationPlanUser::class;

    public function __construct()
    {
        parent::__construct();
    }

    public function returnPlanInvestigatorsId($plan_id)
    {
        return $this->query()
        ->where('investigation_plan_id', $plan_id)->pluck("user_id")
        ->all();
    }

    public function returnPlanInvestigators($plan_id)
    {
       $investigators_id = $this->returnPlanInvestigatorsId($plan_id);
       $plan_investigators = (new UserRepository())->query()->whereIn('users.id',$investigators_id)->get()->pluck('name', 'id');
       return  count($plan_investigators) ? $plan_investigators->toArray() : [];
   }

   public function updateInvestigators($plan_id, array $investigators_id)
   {
    DB::transaction(function () use ($plan_id, $investigators_id)
    {
        $this->addPlanInvestigators($plan_id, $investigators_id);
        $this->removePlanInvestigators($plan_id, $investigators_id);
    });
}

public function addPlanInvestigators($plan_id, array $investigators_id)
{
    foreach ($investigators_id as $officer_id)
    {
        $this->query()
        ->withTrashed()
        ->updateOrCreate(['user_id' => $officer_id, 'investigation_plan_id' => $plan_id], ['deleted_at' => NULL, 'added_by' => access()->user()->id, 'removed_by' => NULL]);
    }
}

public function removePlanInvestigators($plan_id, array $investigators_id)
{
    $not_plan_investigators = $this->query()
    ->where('investigation_plan_id', $plan_id)->whereNotIn('user_id', $investigators_id)->get();
    foreach ($not_plan_investigators as $remove_officer)
    {
        $remove_officer->removed_by = access()->user()->id;
        $remove_officer->save();
        $remove_officer->delete();
    }
    $this->removeFilesWhereNotInInvestigators($plan_id, $investigators_id);

}

public function removeInvestigatorFiles($plan_id, $investigator_id, $notification_ids = [])
{

    $query = InvestigationPlanNotificationReport::where('investigation_plan_id', $plan_id)->where('assigned_to', $investigator_id);

    if (!empty($notification_ids))
    {
        $query = $query->whereIn('notification_report_id', $notification_ids);
    }

    $notifications = $query->get();
    foreach ($notifications as $notification)
    {
        $notification->removed_by = access()->user()->id;
        $notification->save();
        $notification->delete();
    }
}


public function removeFilesWhereNotInInvestigators($plan_id, array $investigator_ids)
{
 $notifications = InvestigationPlanNotificationReport::where('investigation_plan_id', $plan_id)->whereNotIn('assigned_to', $investigator_ids)->get();
 foreach ($notifications as $notification)
 {
    $notification->removed_by = access()->user()->id;
    $notification->save();
    $notification->delete();
}
}

public function returnInvestigatorsForDatatable($plan_id)
{
    $return = $this->query()
    ->select(DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(middlename, ''), coalesce(lastname, '')) as user"), 'users.id as user_id')
    ->join("main.users", "users.id", "=", "investigation_plan_users.user_id")
    ->whereNull('investigation_plan_users.deleted_at')
    ->where('investigation_plan_users.investigation_plan_id',$plan_id)
    ->groupBy(['users.id','users.firstname','users.middlename','users.lastname']);
    return $return;
}

public function allocator()
{
    $investigator =  NotificationReport::select([
        DB::raw("notification_reports.id as case_no"),
        DB::raw("incident_types.name as incident_type"),
        DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as fullname"),
        DB::raw("concat_ws(' ', users.firstname, coalesce(users.middlename, ''), users.lastname) as checklistuser"),
        DB::raw("employees.firstname as firstname"),
        DB::raw("employees.middlename as middlename"),
        DB::raw("employees.lastname as lastname"),
        DB::raw("employers.name as employer"),
        DB::raw("notification_reports.incident_date as incident_date"),
        DB::raw("notification_reports.receipt_date as receipt_date"),
        DB::raw("notification_reports.status as status"),
        DB::raw("notification_reports.filename as filename"),
        DB::raw("coalesce(regions.name, '') as region"),
        DB::raw("notification_staging_cv_id"),
    ])
    ->join("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
    ->join("employers", "employers.id", "=", "notification_reports.employer_id")
    ->join("employees", "employees.id", "=", "notification_reports.employee_id")
    ->leftJoin("users", "users.id", "=", "notification_reports.allocated")
    ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
    ->leftJoin("regions", "regions.id", "=", "districts.region_id")
    ->where('is_accrued', true);
    $investigator = (int) request()->input("incident");
    if ($investigator) {
        $investigator->where("notification_reports.incident_type_id", "=", $investigator);
    }
    $employee = request()->input("employee");
    if ($employee) {
        $investigator->where("notification_reports.employee_id", "=", $employee);
    }
    $employer = request()->input("employer");
    if ($employer) {
        $investigator->where("notification_reports.employer_id", "=", $employer);
    }
    $region = request()->input("region");
    if ($region) {
        $investigator->where("regions.id", "=", $region);
    }

    $stage = request()->input("stage");
    if ($stage) {
        $investigator->where("notification_reports.notification_staging_cv_id", "=", $stage);
    }

    return $investigator;
}


}

