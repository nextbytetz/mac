<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual\Notifications;

use App\DataTables\WorkflowTrackDataTable;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualClaimCompensationRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualDependentRepository;

/**
 * Trait Legacy
 * @package App\Repositories\Backend\Operation\Claim\Notification
 * @author Ockline M. Msungu <ocklinemsungu@gmail.co.tz>
 * @description Support the first implementation of Notification Processing with single workflow
 */
trait AccrualLegacy
{
    // protected $accrual_notification_report;


    // public function __construct()
    // {
    //     $this->notification_report = new NotificationReportRepository();
    // }
    /**
     * @param $incident
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function legacyAccrualProfile(Model $incident)
    {
        // dd($incident->accrual->has_workflow);
        $banks = new BankRepository();
        $bank_branches = new BankBranchRepository();
        $employees = new EmployeeRepository();
        $id = $incident->id;
        $incident_date = $this->findAccrualIncidentDate($id);
        $dob = $employees->getDob($incident->employee_id);
        $employee = $employees->findOrThrowException($incident->employee_id);
        switch($incident->accrual->status) {
            case 1:
            case 0:
            $wf_module_group_id = 3;
            break;
            case 2:
            $wf_module_group_id = 4;
            break;
        }
        /* Check workflow */
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        $assessment_level = $workflow->claimAssessmentLevel();
        $check_pending_assessment_level =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending($assessment_level) : 0;
        /*end of check workflow*/
        $claim_compensations = new AccrualClaimCompensationRepository();
        $compensation_summary = [];
        if ($incident->accrualClaim()->count()) {
            $compensation_summary = $claim_compensations->claimCompensationApprove($id, 0);
        }

        // dd($compensation_summary);

        $notification_report = new NotificationReportRepository();
        $pending_documents = $notification_report->checkAllMandatoryDocuments($id);
        $dependent_repo = new AccrualDependentRepository();

        // $accrual_wf_level = $this->getWorkFlowLevel($incident->accrual->id);

        return view('backend.operation.claim.claim_accrual.profile')
        ->with("notification_report", $incident)
        ->with("banks", $banks->getAll()->pluck('name', 'id'))
        ->with("bank_branches", $bank_branches->getAll()->pluck('name', 'id'))
        ->with("age", Carbon::parse($dob)->diff(Carbon::parse($incident_date))->y)
        ->with("monthly_earning", $employees->getMonthlyEarningBeforeIncidentAccrual($incident->employee_id, $incident->id))
        ->with("compensation_summary", $compensation_summary)
        ->with("pending_documents", $pending_documents)
        ->with("check_workflow", $check_workflow)
        ->with("check_if_is_level1_pending", $check_pending_level1)
        ->with("check_if_is_assessment_level_pending", $check_pending_assessment_level)
        ->with("employee", $employee)
        // ->with("accrual_wf_level", $accrual_wf_level)
        ->with('dependents', $dependent_repo);
    }

    public function getWorkFlowLevel($resource_id){
      $level = null;

        $module = DB::table('main.wf_tracks')->select('wf_definition_id','wf_tracks.status')
        ->where('resource_id',$resource_id)->where('resource_type','App\Models\Operation\ClaimAccrual\AccrualNotificationReport')->orderBy('id','DESC')->first();

        if ($module) {
            if($module->status == 1){
                $defn = DB::table('main.wf_definitions')->select('level')->where('id',$module->wf_definition_id)->first();
                $level = $defn->level;
            }

        }
        // dd($module);

        return $level;

    }


}