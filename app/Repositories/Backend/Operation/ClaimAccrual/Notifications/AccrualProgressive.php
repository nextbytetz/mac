<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual\Notifications;

use App\Exceptions\GeneralException;
use App\Repositories\Backend\Operation\Claim\BenefitTypeClaimRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Trait Progressive
 * @package App\Repositories\Backend\Operation\Claim\Notification
 * @author Ockline M. Msungu <ocklinemsungu@gmail.co.tz>
 * @description Support the implementation of Notification Processing with multiple workflows and
 * progressive management.
 */
trait AccrualProgressive
{
    /**
     * @param Model $incident
     * @return mixed
     * @throws GeneralException
     */
    public function progressiveAccrualProfile(Model $incident)
    {
        $employees = new EmployeeRepository();
        //$wfModuleRepo = new WfModuleRepository();
        //$codeValueRepo = new CodeValueRepository();
        $incident_type_id = $incident->incident_type_id;
        request()->session()->forget('return_url');
        $employee = $incident->employee;
        if ($incident->employee_id) {
            $dob = $employees->getDob($incident->employee_id);
            $incident_date = $this->getAccrualIncidentDate($incident);
            $age = Carbon::parse($dob)->diff(Carbon::parse($incident_date))->y;
            $gender = (($incident->employee->gender()->count()) ? $incident->employee->gender->name : "-");
            $jobtitle = (($incident->employee->jobTitle()->count()) ? $incident->employee->jobTitle->name : "-");

            $monthly_earning = number_2_format($incident->monthly_earning);
            $contrib_month = $incident->contrib_month_formatted;

            /*if ($incident->claim()->count()) {
                $claim = $incident->claim;
                $monthly_earning = number_2_format($claim->monthly_earning);
                $contrib_month = $claim->contrib_month_formatted;
            } else {
                $monthly_earning = number_2_format($incident->monthly_earning);
                $contrib_month = $incident->contrib_month_formatted;
            }*/
        } else {
            $age = "-";
            $gender = "-";
            $jobtitle = "-";
            $monthly_earning = "-";
            $contrib_month = "-";
        }

        //$workflow = new Workflow(["wf_module_group_id" => 3, "resource_id" => $incident->id, "type" => $wfModuleRepo->notificationIncidentApproval()["type"]]);
        //$hasCompletedApprovalWorkflow = $workflow->checkIfExistWorkflowModule();
        //$hasApprovalWorkflow = ($workflow->checkIfHasWorkflow() Or $hasCompletedApprovalWorkflow);
        $pending_documents = $this->checkAllMandatoryDocumentsAccrualProgressive($incident);

        //$workflow = $incident->workflows()->where("wf_done", 0)->orderByDesc("notification_workflows.id")->limit(1);
        $benefits = [];
        $uploaded_documents = [];
        $benefit_approvals_model = NULL;
        if ($incident->progressive_stage >= 4) {
            $benefits = (new BenefitTypeClaimRepository())->getForIncident($incident_type_id);
            $uploaded_documents = $this->getUploadedDocuments($incident);

            //Load Benefit Workflows if any
            $benefit_approvals_model = $incident->benefits()->where("isinitiated", 1)->where("processed", 0)->whereNull("parent_id")->orderByDesc("notification_eligible_benefits.id");
        }
        $compensation_summary = [];
        //if ($incident->claim()->count()) {
        if ($incident->progressive_stage >= 4) {

            $compensation_summary = (new ClaimCompensationRepository())->progressiveCompensationApprove($incident, 0);
        }
        //$hasDeclinedApprovalWorkflow = $workflow->checkIfExistDeclinedWorkflowModule();

        return view('backend.operation.claim.claim_accrual.progressive.profile')
                ->with("incident", $incident)
                ->with("employee", $employee)
                ->with("age", $age)
                ->with("gender", $gender)
                ->with("jobtitle", $jobtitle)
                ->with("monthly_earning", $monthly_earning)
                ->with("contrib_month", $contrib_month)
                //->with("has_approval_workflow", $hasApprovalWorkflow)
                //->with("has_completed_approval_workflow", $hasCompletedApprovalWorkflow)
                ->with("pending_documents", $pending_documents)
                //->with("workflow", $workflow)
                ->with("benefits", $benefits)
                ->with("uploaded_documents", $uploaded_documents)
                ->with("compensation_summary", $compensation_summary)
                ->with("benefit_approvals_model", $benefit_approvals_model)
                ->with("incident_stage_code", $incident->stage->code_id);
    }

}