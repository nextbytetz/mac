<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\NotificationHealthProvider;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProvider;

class AccrualNotificationHealthProviderRepository extends  BaseRepository
{

    const MODEL = AccrualNotificationHealthProvider::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $notification_health_provider= $this->query()->find($id);

        if (!is_null($notification_health_provider)) {
            return $notification_health_provider;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_health_provider_not_found'));
    }

    /**
     * @param $notification_report_id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function create($notification_report_id, $input) {
        if (isset($input['rank'])) {
            $this->checkRankIfExist($notification_report_id,$input);
        }
        return DB::transaction(function () use ($notification_report_id, $input) {
            $notification_report_provider = $this->query()->create([
                'notification_report_id' => $notification_report_id,
                'health_provider_id' => $input['health_provider_id'],
                'attend_date' => $input['attend_date'],
                'dismiss_date' => $input['dismiss_date'],
                'rank' => (isset($input['rank']) ? $input['rank'] : NULL),
                // 'medical_expense_id' => $input['medical_expense_id'],
                'accrual_medical_expense_id' => $input['medical_expense_id'],
                'remarks' => (isset($input['remarks']) ? $input['remarks'] : NULL),
            ]);
            return $notification_report_provider;
        });
    }

    public function updateOrCreate($notification_report_id, $input){
        return DB::transaction(function () use ($notification_report_id, $input) {
            $notification_report_provider = $this->query()->updateOrCreate(
                [
                    'notification_report_id' => $notification_report_id,
                    'health_provider_id' => $input['health_provider_id']
                ],
                [
                    'attend_date' => $input['attend_date'],
                    'dismiss_date' => $input['dismiss_date'],
                    'rank' => (isset($input['rank']) ? $input['rank'] : 0),
                    // 'medical_expense_id' => $input['medical_expense_id'],
                    'accrual_medical_expense_id' => $input['accrual_medical_expense_id'],
                    'remarks' => (isset($input['remarks']) ? $input['remarks'] : NULL),
                ]);
            return $notification_report_provider;
        });
    }


    /*
     * update
     */
    public function update($id,$input) {
        return DB::transaction(function () use ($id,$input) {
            $notification_report_provider = $this->findOrThrowException($id);
            $notification_report_id = $notification_report_provider->notification_report_id;
            if (isset($input['rank'])) {
                $this->checkRankIfExistWhenUpdate($notification_report_id, $input, $id);
            }
            $notification_report_provider->update([
                'health_provider_id' => $input['health_provider_id'],
                'attend_date' => $input['attend_date'],
                'dismiss_date' => $input['dismiss_date'],
                'rank' => (isset($input['rank']) ? $input['rank'] : NULL),
                // 'medical_expense_id' => $input['medical_expense_id'],
                'accrual_medical_expense_id' => $input['medical_expense_id'],
                'remarks' => (isset($input['remarks']) ? $input['remarks'] : NULL),
            ]);
            return $notification_report_provider;
        });
    }

    /**
     * @param $id
     * Remove service provider when there is no health services
     */
    public function deleteIfNoHealthServices($id) {
        $notification_health_provider = $this->findOrThrowException($id);

        $notification_health_provider_services = $notification_health_provider->notificationHealthProviderServices;
        if (($notification_health_provider_services->count() == 0)){
            $notification_health_provider->delete();
        }
    }

    /**
     * @param $notification_report_id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function checkRankIfExist($notification_report_id,$input) {
        $notification_health_provider = $this->query()->where('notification_report_id',$notification_report_id)->where('health_provider_id',$input['health_provider_id'])->where('rank',$input['rank'])->first();

        if (isset($notification_health_provider->id)){
            throw new GeneralException(trans('exceptions.backend.claim.rank_notification_health_provider_exist'));
        }else{
            return $notification_health_provider;
        }
    }


    public function checkRankIfExistWhenUpdate($notification_report_id,$input,$id) {
        $notification_health_provider = $this->query()->where('notification_report_id',$notification_report_id)->where('health_provider_id',$input['health_provider_id'])->where('rank',$input['rank'])->where('id','<>',$id)->first();

        if (isset($notification_health_provider->id)){
            throw new GeneralException(trans('exceptions.backend.claim.rank_notification_health_provider_exist'));
        }else{
            return $notification_health_provider;
        }
    }




}