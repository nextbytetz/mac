<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\MemberType;
use App\Models\Operation\Claim\NotificationHealthState;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthState;
use App\Repositories\BaseRepository;

class AccrualNotificationHealthStateRepository extends  BaseRepository
{

    const MODEL = AccrualNotificationHealthState::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $notification_health_state = $this->query()->find($id);

        if (!is_null($notification_health_state)) {
            return $notification_health_state;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_health_state_not_found'));
    }

    /**
     * @param $health_state_checklist_id
     * @param $notification_report_id
     * @param $input
     * @return mixed
     */
    public function create($health_state_checklist_id, $notification_report_id, $input) {
        // dd($notification_report_id);
        if (!isset($input['notification_eligible_benefit_id'])) {
            $input['notification_eligible_benefit_id'] = NULL;
        }
        $notification_health_state = $this->query()->create(['notification_report_id' => $notification_report_id, 'health_state_checklist_id' => $health_state_checklist_id, 'state'=> !is_null($input['health_state'.$health_state_checklist_id]) ? $input['health_state'.$health_state_checklist_id] : 0,'user_id'=>access()->user()->id, 'notification_eligible_benefit_id' => isset($input['notification_eligible_benefit_id']) ? $input['notification_eligible_benefit_id'] : null ]);
        return $notification_health_state;
    }

    public function updateOrCreate($health_state_checklist_id, $notification_report_id, $input, $state){
        $notification_health_state = $this->query()->updateOrCreate(
            [
                'notification_report_id' => $notification_report_id,
                 'health_state_checklist_id' => $health_state_checklist_id,
            ],
            ['state'=> $state->state,'user_id'=> $state->user_id ]
        );
        return $notification_health_state;
    }

    /**
     * @param $health_state_checklist_id
     * @param $notification_report_id
     * @param $input
     */
    public function update($health_state_checklist_id, $notification_report_id, $input) {

        if (!isset($input['notification_eligible_benefit_id'])) {
            $notification_health_state =   $this->query()->where('notification_report_id', $notification_report_id)->where('health_state_checklist_id',$health_state_checklist_id)->first();
        } else {
            $notification_health_state =   $this->query()->where('notification_eligible_benefit_id', $input['notification_eligible_benefit_id'])->where('health_state_checklist_id',$health_state_checklist_id)->first();
        }


        if ($notification_health_state){
            $notification_health_state_id = $notification_health_state->id;
            $notification_health_state->update([ 'state'=> $input['health_state' . $health_state_checklist_id]]);

            //delete if is feedback is set to 0
            $this->deleteIfFeedbackIsZero($notification_health_state_id);

        } else {
            if ($input['health_state'. $health_state_checklist_id] == 1) {
                $this->create($health_state_checklist_id, $notification_report_id, $input);
            }
        }
    }

    /**
     * @param $id
     */
    public function deleteIfFeedbackIsZero($id)
    {
        $updated_notification_health_state = $this->query()->where('id',$id)->where('state',0)->first();
        if ($updated_notification_health_state){
            $updated_notification_health_state->delete();
        }
    }

}