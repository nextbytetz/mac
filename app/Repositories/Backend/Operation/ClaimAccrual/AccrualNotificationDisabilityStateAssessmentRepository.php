<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual;

use App\Exceptions\GeneralException;
use Carbon\Carbon;
use App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityStateAssessment;
use App\Repositories\BaseRepository;

class AccrualNotificationDisabilityStateAssessmentRepository extends  BaseRepository
{

    const MODEL = AccrualNotificationDisabilityStateAssessment::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
     $notification_disability_state_assessment = $this->query()->find($id);

     if (!is_null($notification_disability_state_assessment)) {
        return $notification_disability_state_assessment;
    }
    throw new GeneralException(trans('exceptions.backend.claim.notification_disability_state_assessment_not_found'));
}
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }

    public function updateOrCreate2($accrual_notification_disability_state_id, $disability_state_checklist_id, $disability_id, $input){
        $notification_health_state = $this->query()->updateOrCreate(
            [
                'accrual_notification_disability_state_id' => $accrual_notification_disability_state_id,
            ],[
                'days'=> $input['days'],
                'user_id'=>isset($input['user_id']) ? $input['user_id'] : 85,
            ]);

        return $notification_health_state;
    }

    public function updateOrCreate($accrual_notification_disability_state_id, $input) {
        $notification_disability_state_assessment = $this->query()->updateOrCreate(
            ['accrual_notification_disability_state_id' => $accrual_notification_disability_state_id] , ['user_id' => access()->user()->id, 'days' => $input['days'.$accrual_notification_disability_state_id]] );
        return $notification_disability_state_assessment;
    }

    public function updateOrCreateProgressive($accrual_notification_disability_state_id, array $input)
    {
        $notification_disability_state_assessment = $this->query()->updateOrCreate(
            ['accrual_notification_disability_state_id' => $accrual_notification_disability_state_id] , ['user_id' => access()->user()->id, 'days' => $input['assessed_days' . $accrual_notification_disability_state_id], 'remarks' => $input['remarks' . $accrual_notification_disability_state_id]] );
        return $notification_disability_state_assessment;
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findTotalDays($id)
    {
        $notification_disability_state = $this->findOrThrowException($id);
        $no_of_days = $notification_disability_state->days;
        return $no_of_days;
    }

    /**
     * @param $id
     * @return array
     * @throws GeneralException
     */
    public function findTotalDaysWorked($id)
    {
        $notification_disability_state_assessment = $this->findOrThrowException($id);
        $percent_of_ed_ld = $notification_disability_state_assessment->accrualNotificationDisabilityState->percent_of_ed_ld;
        $total_days_inserted = $this->findTotalDays($id); //Total days inserted without considering percent of ed/ld
        $total_days_worked = $total_days_inserted *($percent_of_ed_ld * 0.01);

        return ['total_days_worked'=>$total_days_worked, 'total_days_inserted' => $total_days_inserted];

    }



}