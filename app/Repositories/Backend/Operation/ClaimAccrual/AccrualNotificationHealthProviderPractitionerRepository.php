<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\NotificationHealthProvider;
use App\Models\Operation\Claim\NotificationHealthProviderPractitioner;
use App\Repositories\BaseRepository;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderPractitioner;

class AccrualNotificationHealthProviderPractitionerRepository extends  BaseRepository
{

    const MODEL = AccrualNotificationHealthProviderPractitioner::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $notification_health_provider_practitioner= $this->query()->find($id);

        if (!is_null($notification_health_provider_practitioner)) {
            return $notification_health_provider_practitioner;
        }
        throw new GeneralException(trans('exceptions.backend.claim.notification_health_provider_practitioner_not_found'));
    }
    /*
     * create new
     */

    public function create($notification_health_provider_id, $input) {
        $notification_report_provider_practitioner = $this->query()->create(['accrual_notification_health_provider_id' => $notification_health_provider_id, 'medical_practitioner_id' => $input['medical_practitioner_id']]);
    }

    /*
     * update
     */
    public function update($notification_health_provider_id,$input) {
        $notification_report_provider_practitioner = $this->query()->updateOrCreate(['accrual_notification_health_provider_id' => $notification_health_provider_id], ['medical_practitioner_id' => $input['medical_practitioner_id']]);
    }






}