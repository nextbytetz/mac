<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual;

use App\Exceptions\GeneralException;
use App\Models\Operation\ClaimAccrual\AccrualNotificationReport;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\ClaimAccrual\AccrualBenefitDisability;
use App\Models\Operation\Claim\NotificationDisabilityStateAssessment;
use App\Models\Operation\ClaimAccrual\AccrualClaim;
use App\Models\Operation\ClaimAccrual\AccrualNotificationBenefit;
use App\Repositories\Backend\Operation\ClaimAccrual\Notifications\AccrualLegacy;
use App\Repositories\Backend\Operation\ClaimAccrual\Notifications\AccrualProgressive;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Exceptions\WorkflowException;
use App\Events\NewWorkflow;
use App\Services\Workflow\Workflow;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;
use App\Models\Operation\Claim\NotificationWorkflow;
use App\Models\Workflow\WfTrack;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use App\Repositories\Backend\Operation\Claim\MedicalExpenseRepository;
use App\Repositories\Backend\Operation\Claim\MedicalPractitionerRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;

use App\Repositories\Backend\Task\CheckerRepository;

use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthState;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderService;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderPractitioner;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProvider;
use App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityState;
use App\Models\Operation\ClaimAccrual\AccrualMedicalExpense;
use App\Models\Operation\ClaimAccrual\AccrualDeaultDate;
use App\Models\Operation\ClaimAccrual\AccrualDeaultDateTrigger;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationHealthProviderRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationHealthProviderPractitionerRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationHealthProviderServiceRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualMedicalExpenseRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationHealthStateRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationDisabilityStateRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationDisabilityStateAssessmentRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualClaimRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualClaimCompensationRepository;


/**
 * Class AccrualNotificationReportRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class AccrualNotificationReportRepository extends  BaseRepository
{
    use AccrualLegacy, AccrualProgressive;
    const MODEL = AccrualNotificationReport::class;
    protected $member_types;

    function __construct($foo = null)
    {
        parent::__construct();  
        $this->wf_module_group_id = 31;
        $this->assesment_level_admin = '6.00';
        $this->approval_level_admin = '12.00';
        $this->assesment_level_assesment = '1.00';
        $this->approval_level_assesment = '6.00';
        $this->wf_admin = 68;
        $this->wf_assesment = 69;
        $this->member_types = new MemberTypeRepository();
        $this->accrual_medical_expenses = new AccrualMedicalExpenseRepository();
        $this->accrual_notification_health_states = new AccrualNotificationHealthStateRepository();
        $this->accrual_notification_disability_state_assessments =new AccrualNotificationDisabilityStateAssessmentRepository();
        $this->accrual_notification_disability_states = new AccrualNotificationDisabilityStateRepository();
        $this->accrual_notification_health_providers = new AccrualNotificationHealthProviderRepository();
        $this->accrual_notification_health_provider_services = new AccrualNotificationHealthProviderServiceRepository();
        $this->accrual_notification_health_provider_practitioners = new AccrualNotificationHealthProviderPractitionerRepository();
        
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $accrual = $this->query()->find($id);
        if (!is_null($accrual)) {
            return $accrual;
        }
        throw new GeneralException('The notification is not in Accrual list');
    }

    public function findByNotificationReportId($notification_report_id)
    {
        $accrual = $this->query()->where('notification_report_id',$notification_report_id)->first();
        if (!is_null($accrual)) {
            return $accrual;
        }
        throw new GeneralException('The notification is not in Accrual list');
    }

    public function profileAccrual(Model $incident)
    {

            //This is not a progressive claim accrual
        $return = $this->legacyAccrualProfile($incident);

        return $return;
    }

    public function getForDataTable()
    {
        $incidents = NotificationReport::select([
            DB::raw("notification_reports.id as case_no"),
            DB::raw("incident_types.name as incident_type"),
            DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as fullname"),
            DB::raw("concat_ws(' ', users.firstname, coalesce(users.middlename, ''), users.lastname) as checklistuser"),
            DB::raw("employees.firstname as firstname"),
            DB::raw("employees.middlename as middlename"),
            DB::raw("employees.lastname as lastname"),
            DB::raw("employers.name as employer"),
            DB::raw("notification_reports.incident_date as incident_date"),
            DB::raw("notification_reports.receipt_date as receipt_date"),
            DB::raw("notification_reports.status as status"),
            DB::raw("notification_reports.filename as filename"),
            DB::raw("coalesce(regions.name, '') as region"),
            DB::raw("notification_staging_cv_id"),
            DB::raw("current_wf_level"),
        ])
        ->join("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
        ->join("employers", "employers.id", "=", "notification_reports.employer_id")
        ->join("employees", "employees.id", "=", "notification_reports.employee_id")
        ->join("accrual_notification_reports", "accrual_notification_reports.notification_report_id", "=", "notification_reports.id")
        ->leftJoin("users", "users.id", "=", "notification_reports.allocated")
        ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
        ->leftJoin("regions", "regions.id", "=", "districts.region_id")
        ->where('is_accrued', true);

        $incident = (int) request()->input("incident");
        if ($incident) {
            $incidents->where("notification_reports.incident_type_id", "=", $incident);
        }
        $employee = request()->input("employee");
        if ($employee) {
            $incidents->where("notification_reports.employee_id", "=", $employee);
        }
        $employer = request()->input("employer");
        if ($employer) {
            $incidents->where("notification_reports.employer_id", "=", $employer);
        }
        $region = request()->input("region");
        if ($region) {
            $incidents->where("regions.id", "=", $region);
        }
        $district = request()->input("district");
        if ($district) {
            $incidents->where("districts.id", "=", $district);
        }
        $stage = request()->input("stage");
        if ($stage) {
            $incidents->where("notification_reports.notification_staging_cv_id", "=", $stage);
        }

        return $incidents;
    }

    
    public function initiateAccrualWorkflow($id,$type)
    {
        // dd($type);
        $accrual = $this->findOrThrowException($id);
        $this->checkIfCanInitiateWorkflow($accrual,$type);
        $return = DB::transaction(function () use ($id,$type) {
            if ($type > 0 && $type < 3) {
               $this->query()->where('id',$id)->update(['wf_done'=> 0,'has_workflow' => 1,'status' => 0]);
               $check = workflow([['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $id, 'type'=>$type]])->checkIfHasWorkflow();
               if (!$check) {
                event(new NewWorkflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $id, 'type' =>$type]));
            }
        }
        return true;
    });

        return $return;
    }

    public function returnFileWorkflowType($notification_report_id)
    {
        // $accrual = $this->query()->join("main.accrual_notification_benefits","accrual_notification_reports.id", "=", "accrual_notification_benefits.accrual_notification_report_id")
        // ->where('accrual_notification_reports.notification_report_id',$notification_report_id)->get();

        $accrual = $this->query()->where('accrual_notification_reports.notification_report_id',$notification_report_id)->get();
        $type = 0;
        if (count($accrual) == 1) {
            $type = !empty($accrual[0]->notification_wf_level) ? $accrual[0]->notification_wf_level : $notification_wf_level;
        } elseif(count($accrual) > 1) {
            $types = [];
            foreach ($accrual as $acr) {
              if (!empty($acr->notification_wf_level)) {
               array_push($types, $acr->notification_wf_level);
           }
       }
       $type = min($types);
   }
   return $type;
}

public function checkIfCanInitiateWorkflow($accrual,$type)
{

    switch ($type) {
        case 1:
        // $this->canAdminOfficerForwardWorkflow($accrual);
        break;
        case 2:
        // $this->canAssesmentOfficerForwardWorkflow($accrual);
        break;
        case 3:
        // $this->canFinanceOfficerForwardWorkflow($accrual);
        break;
        default:
        // throw new WorkflowException('File has no any benefit');
        break;
    }
}

public function canAdminOfficerForwardWorkflow($accrual)
{
    //check admin columns on benefits 
    if (access()->user()->id == $accrual->notificationReport->allocated) {
       foreach ($accrual->accrualNotificationBenefits as $benefit) {
        switch ($benefit->benefit_type_id) {
            case 7:
            //Temporary Partial Disablement == HOSPITALIZATION AND LIGHT DUTY
            $query_tpd = AccrualBenefitDisability::where('accrual_notification_report_id',$benefit->id)->whereIn('disability_id',[1,3])->whereNull('admin_days')->get();
            if (count($query_tpd)) {
                throw new WorkflowException('Days of disability not filled');
            }
            break;
            case 8:
            //Temporary Total Disablement == ED
            $query_ttd= AccrualBenefitDisability::where('accrual_notification_report_id',$benefit->id)->where('disability_id',2)->whereNull('admin_days')->get();
            if (count($query_ttd)) {
                throw new WorkflowException('Days of disability not filled');
            }
            break;
            default:

            break;
        }
    }
}else{
 throw new WorkflowException('Only assigned user can perform this action');
}


}

public function canAssesmentOfficerForwardWorkflow($accrual)
{

 // if (access()->user()->designation_id == 4 && access()->user()->unit_id == 14) {
 foreach ($accrual->accrualNotificationBenefits as $benefit) {
    switch ($benefit->benefit_type_id) {
        case 7:
            //Temporary Partial Disablement == HOSPITALIZATION AND LIGHT DUTY
        $query_tpd = AccrualBenefitDisability::where('accrual_notification_report_id',$benefit->id)->whereIn('disability_id',[1,3])->whereNull('assessed_days')->get();
        if (count($query_tpd)) {
            throw new WorkflowException('File not assessed: Days of disability not filled');
        }
        break;
        case 8:
            //Temporary Total Disablement == ED
        $query_ttd= AccrualBenefitDisability::where('accrual_notification_report_id',$benefit->id)->where('disability_id',2)->whereNull('assessed_days')->get();
        if (count($query_ttd)) {
            throw new WorkflowException('File not assessed: Days of disability not filled');
        }
        break;
        case 3:
        case 4:
            //PTD and PPD
        if (empty($benefit->pd_percent)) {
            throw new WorkflowException('File not assessed: Percent of disability not filled');
        }
        break;
        default:

        break;
    }
}
// }else{
//  throw new WorkflowException('Only authorized users can perform this action');
// }
}

public function canFinanceOfficerForwardWorkflow($accrual)
{
    //check finance columns on benefits 
   if (access()->user()->designation_id == 4 && access()->user()->unit_id == 12) {

   }else{
     throw new WorkflowException('Only authorized users can perform this action');
 }
}


public function getForCheckerDataTable()
{
    $select = [
        DB::raw("notification_reports.id as case_no"),
        DB::raw("incident_types.name as incident_type"),
        DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as fullname"),
        DB::raw("concat_ws(' ', users.firstname, coalesce(users.middlename, ''), users.lastname) as checklistuser"),
        DB::raw("employees.firstname as firstname"),
        DB::raw("employees.middlename as middlename"),
        DB::raw("employees.lastname as lastname"),
        DB::raw("employers.name as employer"),
        DB::raw("notification_reports.incident_date as incident_date"),
        DB::raw("notification_reports.receipt_date as receipt_date"),
        DB::raw("notification_reports.status as status"),
        DB::raw("notification_reports.filename as filename"),
        DB::raw("coalesce(regions.name, '') as region"),
        DB::raw("notification_staging_cv_id"),
    ];
    $incidents = NotificationReport::select($select)
    ->join("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
    ->join("employers", "employers.id", "=", "notification_reports.employer_id")
    ->join("employees", "employees.id", "=", "notification_reports.employee_id")
    ->leftJoin("users", "users.id", "=", "notification_reports.allocated")
    ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
    ->leftJoin("regions", "regions.id", "=", "districts.region_id")
    ->where('is_accrued', true)->where('users.id',access()->user()->id);
    return $incidents;
}


public function insertInboxAndTask($accrual_id)
{
    $accrual = $this->find($accrual_id);
    $checkerRepo = new CheckerRepository();
    $codeValue = new CodeValueRepository();

    $incident = NotificationReport::select('users.id as user_id')->leftJoin("users", "users.id", "=", "notification_reports.allocated")->where('notification_reports.id',$accrual->notification_report_id)->first();

    if (!empty($incident->user_id)) {
        $checkr = DB::table('main.checkers')->where('resource_id',$accrual->id)->where('user_id',$incident->user_id)
        ->where('resource_type','App\Models\Operation\ClaimAccrual\AccrualNotificationReport')->where('status',0)->first();
        if (empty($checkr)) {
           $checkerRepo->create($accrual, 
             $input = [
                'comments' => '{"comment":"Review and Submit For Accrual"}',
                'user_id' =>  $incident->user_id,
                'checker_category_cv_id' => $codeValue->CHACCRCLM(), 
                'priority' => 1,
            ]);
       }

   }
}


public function updateAccrualNotificationWorkflow($resource_id,$level,$wf_module_id)
{
    if ($wf_module_id == $this->wf_admin) {
       switch ($level) {
        case  $this->assesment_level_admin:
        $accrual = $this->findOrThrowException($resource_id);
        $this->canAssesmentOfficerForwardWorkflow($accrual);
        // die;
        break;
        case  $this->approval_level_admin:
        $this->query()->where('id',$resource_id)->update([
            'wf_done'=> 1,
            'status' => 1,
            'wf_done_date' => Carbon::now()
        ]);
        break;
        default:
        break;
    }   
} elseif($wf_module_id == $this->wf_assesment) {
    switch ($level) {
        case  $this->assesment_level_assesment:
        $accrual = $this->findOrThrowException($resource_id);
        $this->canAssesmentOfficerForwardWorkflow($accrual); 
        break;
        case  $this->assesment_level_approval:
        $this->query()->where('id',$resource_id)->update([
            'wf_done'=> 1,
            'status' => 1,
            'wf_done_date' => Carbon::now()
        ]);
        break;
        default:
        break;
    }   
}   
}
public function accrualNotifications(){

    // DB::table('main.notification_reports')->update(["is_accrued" => false, "accrued_status" => 0]);
    // die;
    /*admin level*/
    $cr_in_accrual_array = $this->CRfilesToAccrualAdmin();
    $notification_reports = $this->notificationReportArrayFromFileNames($cr_in_accrual_array);
    $this->accrualWithWfLevel($notification_reports, 1);

    /*assessment level */
    // $cr_in_accrual_array = $this->CRfilesToAccrualAssessment();
    // $notification_reports = $this->notificationReportArrayFromFileNames($cr_in_accrual_array);
    // $this->accrualWithWfLevel($notification_reports, 2);

    /*finance level */
    // $cr_in_accrual_array = $this->CRfilesToAccrualFinance();
    // $notification_reports = $this->notificationReportArrayFromFileNames($cr_in_accrual_array);
    // $this->accrualWithWfLevel($notification_reports, 3);

}

public function accrualWithWfLevel($cr_array, $level_type){

    foreach ($cr_array as $array) {
        // dump('saving level '.$level_type);
        $this->saveAccruedNotification($array, $level_type);
        // dump('end level '.$level_type);
    }

}


public function notificationReportArrayFromFileNames($cr_in_accrual_array){
    $notification_reports = NotificationReport::whereIn('filename',$cr_in_accrual_array)->where('isprogressive', 1)->get();
    return $notification_reports;
}

public function CRfilesToAccrualAdmin(){
    $cr15 = DB::table('main.cr15')->pluck('filename')->toArray();
    $cr17 = DB::table('main.cr17')->pluck('filename')->toArray();
    $cr5 = DB::table('main.cr5')->where('level', '<=', 4)->pluck('filename')->toArray();
    $result = array_merge($cr15, $cr17, $cr5);
    return $result;
}

public function CRfilesToAccrualAssessment(){
    $cr6 = DB::table('main.cr6')->whereIn('level', [5, 6, 7])->pluck('filename')->toArray();
    $result = $cr6;
    return $result;
}

public function CRfilesToAccrualFinance(){
    $cr4 = DB::table('main.cr4')->whereIn('level', [12, 13, 14])->pluck('filename')->toArray();
    $result = $cr4;
    return $result;
}

public function accrualEligibility($notification, $benefits){
    $return = 0;
    foreach ($benefits as $benefit) {

        $benef_to_accrual = $this->getBenefits($benefit);
        if($benefit->notification_workflow_id == null and $benefit->isinitiated == 0){
            $return += 1;
        }else{
           $level = $this->checkWorkFlowLevel($benefit->notification_workflow_id);
           dump('level in ->'.$level);
           if ($level < 5) {
            dump('level in ->'.$level);
            if ($benefit->benefit_type_claim_id == 3 || $benefit->benefit_type_claim_id == 4 || $benefit->benefit_type_claim_id == 5) {
                $verify = $this->verifyExtraDocumentForAccrual($notification, $benefit);
                if ($verify) {
                    $return += 1;
                }
            }
        }
    }
}

return $return;
}

public function accrualAssessmentEligibility($notification, $benefits){
    $return = 0;
    foreach ($benefits as $benefit) {

        $benef_to_accrual = $this->getBenefits($benefit);

        $level = $this->checkWorkFlowLevel($benefit->notification_workflow_id);
        if ($level >= 5 && $level <= 7 ) {
            if ($benefit->benefit_type_claim_id == 3 || $benefit->benefit_type_claim_id == 4 || $benefit->benefit_type_claim_id == 5) {

                $return += 1;
            }
        }
    }

    return $return;
}

public function accrualSingleAssessmentEligibility($notification, $benefit){
    $return = 0;

    $benef_to_accrual = $this->getBenefits($benefit);

    $level = $this->checkWorkFlowLevel($benefit->notification_workflow_id);
    if ($level >= 5 && $level <= 7 ) {
        if ($benefit->benefit_type_claim_id == 3 || $benefit->benefit_type_claim_id == 4 || $benefit->benefit_type_claim_id == 5) {

            $return += 1;
        }
    }

    return $return;
}

public function countWCP5($check,$array){
 $return = 0;
 for ($i=0; $i < count($array); $i++) { 
    if ($check[0] == $array[$i]) {
        $return += 1;
    }
}
return $return;
}

public function verifyExtraDocumentForAccrual($notification, $benefit){
  $return = false;

  $query = DB::table('main.document_notification_report')
  ->where('notification_report_id',$notification->id)->pluck('document_id')->toArray();

  switch ($benefit->benefit_type_claim_id) {
    case 3:
    case 4:
    case 5:
    /* wcfp-5 */
    $conditions1 = [21];
    $wcfp_5_count = $this->countWCP5($conditions1, $query);
    // dump('wcfp-5 '.$wcfp_5_count);

    if ($wcfp_5_count > 1 ) {
        /* count workflow */
        $wf_count = $this->countWf($notification);
        // dump('wf-count '.$wf_count);
        if ($wcfp_5_count != $wf_count) {
            // save
            $return = true;
        }

    }

    break;
    default:

    break;

}

return $return;
}

public function countWf($notification){
   $count = NotificationEligibleBenefit::where('notification_report_id', $notification->id)->where('isinitiated', 1)->where('notification_workflow_id','!=', null)->whereIn('benefit_type_claim_id', [3,4])->count();

   return $count;
}

public function claimAdmin($notifications){
    foreach ($notifications as $notification) {
        switch ($notification->incident_type_id) {
            case 1:
                        //check accident docs
            $has_docs = $this->verifyDocumentForAccrual($notification);
            if ($has_docs) {

                $this->saveAccruedNotification($notification);    
            }
            break;
            case 2:
                        //check disease docs
            $has_docs = $this->verifyDocumentForAccrual($notification);
            if ($has_docs) {
                $this->saveAccruedNotification($notification);    
            }


            break;
            case 3:
                        //check death docs
            $has_docs = $this->verifyDocumentForAccrual($notification);
            if ($has_docs) {
                $this->saveAccruedNotification($notification);    
            }


            break;

            default:
            die;
            break;
        }
    }
}

public function claimAssessedLevel($notifications){
    foreach ($notifications as $notification) {

        $this->saveAccruedNotificationAssessment($notification);    

    }
}

public function getBenefitsForAccrual($notification){
    $benefits = NotificationEligibleBenefit::select('benefit_type_claim_id','notification_eligible_benefits.id','notification_disability_states.percent_of_ed_ld','notification_disability_states.id as notification_disability_state_id','notification_workflow_id','isinitiated')
    // ->where('notification_workflow_id', null)
    // ->where('isinitiated', 0)
    ->where('notification_report_id', $notification->id)
    ->leftJoin('notification_disability_states','notification_disability_states.notification_eligible_benefit_id','=','notification_eligible_benefits.id')
    ->get();

    return $benefits;
}

public function getBenefitsForAccrualForAssessment($notification){
    $benefits = NotificationEligibleBenefit::select('benefit_type_claim_id','notification_eligible_benefits.id','notification_disability_states.percent_of_ed_ld','notification_disability_states.id as notification_disability_state_id','notification_eligible_benefits.notification_workflow_id')
    ->where('notification_workflow_id', '!=', null)
    ->where('isinitiated', 1)
    ->where('notification_report_id', $notification->id)
    ->leftJoin('notification_disability_states','notification_disability_states.notification_eligible_benefit_id','=','notification_eligible_benefits.id')
    ->get();

    return $benefits;
}

public function getDisabilitiesForAccrual($notification, $benefit){
    $benefits = NotificationEligibleBenefit::select('notification_disability_states.days as admin_days','notification_disability_states.from_date','notification_disability_states.to_date','notification_disability_state_assessments.days as assessment_days','notification_disability_states.disability_state_checklist_id','notification_disability_states.id as notification_disability_state_id','notification_eligible_benefits.notification_workflow_id','notification_eligible_benefits.isinitiated','benefit_type_claim_id')
    ->where('notification_report_id', $notification->id)
    ->where('notification_eligible_benefits.id', $benefit->id)
    ->leftJoin('notification_disability_states','notification_disability_states.notification_eligible_benefit_id','=','notification_eligible_benefits.id')
    ->leftJoin('notification_disability_state_assessments','notification_disability_state_assessments.notification_disability_state_id','=','notification_disability_states.id')
    ->get();

    return $benefits;
}

public function getAccrual($notification){
    $accrual = $this->query()->where('notification_report_id', $notification->id)->first();
    return $accrual;
}

public function accrualdata($notification, $level_type){    
    $data = [
        'fin_year_id' => $this->getCurrentFinYearId(),
        'source' => 0,
        'status' => 0,
        'has_workflow' => null,
        'wf_done' => null,
        'wf_done_date' => null,
        'notification_report_id' => $notification->id,
        'notification_wf_level' => $level_type,
        'current_wf_level' => $level_type,

    ];
    return $data;
}

public function saveAccruedNotification($notification, $level_type){

    DB::transaction(function () use ($notification, $level_type) {

        $this->saveAccruedClaims($notification);

        $data = $this->accrualdata($notification, $level_type);

        $accrual = $this->getAccrual($notification);

        if (is_null($accrual)) {
            $this->query()->create($data);
            $accrual = $this->getAccrual($notification);
        }else{
            $this->query()->where('notification_report_id', $notification->id)->update($data);
        }
        $this->insertInboxAndTask($accrual->id);
        $this->updateAccruedNotificationReport($notification);
        $query = $this->benefitQueries($notification);
        $input = [];

        if($level_type == 2 || $level_type == 3){
           $this->saveDASAccrual($notification, $input, $query, $level_type);           
       }
   });
}

public function saveFINANCEAccrual($notification,$medical_expense, $query){
    $this->storeAssessmentToAccrual($notification,$medical_expense, $query);
}


public function benefitQueries($notification){
    $query = DB::table('notification_disability_states')
    ->select('*','notification_eligible_benefits.user_id as user_created_benefit',
        'notification_disability_states.user_id as user_created_disability',
        'notification_disability_state_assessments.user_id as user_created_disability_assessment',
        'medical_expenses.user_id as user_created_medical',
        'notification_disability_states.days as admin_days',
        'notification_disability_state_assessments.days as assessment_days',
        'notification_disability_states.id as disability_id',
        'notification_disability_state_assessments.id as disability_assessment_id',
        'notification_disability_states.from_date as disability_from_date',
        'notification_disability_states.to_date as disability_to_date',
        'medical_expenses.notification_report_id as medical_notification_id',
        'notification_eligible_benefits.notification_report_id as eligible_notification_id'
    )
    ->leftJoin('notification_eligible_benefits', 'notification_eligible_benefits.id' ,'=' ,'notification_disability_states.notification_eligible_benefit_id')
    ->leftJoin('medical_expenses', 'medical_expenses.id', '=', 'notification_disability_states.medical_expense_id')
    ->leftJoin('notification_disability_state_assessments', 'notification_disability_state_assessments.notification_disability_state_id', '=', 'notification_disability_states.id')
    ->where('medical_expenses.notification_report_id', $notification->id)
    ->orWhere('notification_eligible_benefits.notification_report_id', $notification->id)
    ->get();
    return $query;
}

public function saveDASAccrual($notification, $input, $query, $level_type){
    $medical_expense = NULL;
    if($notification->treatments()->count()){
        foreach($notification->treatments as $treatment){

            $input['amount'] = 0;
            $input['member_type_id'] = $treatment->member_type_id;
            $medicals = $this->getMedicalExpenses($notification);
            dump($medicals);
            if (count($medicals) > 0) {
                foreach ($medicals as $medical) {
                    $input['amount'] = $medical->amount;
                    $input['member_type_id'] = $medical->member_type_id;
                    $input['medical_expense_id'] = $medical->id;
                    $input['user_id'] = $medical->user_id;
                    $medical_expense = $this->storeMedicalExp3($notification->id, $input);
                    $this->storeHealthProviderSvc($notification, $medical_expense, $treatment, $query);
                    if ($level_type == 3) {
                        $this->saveFINANCEAccrual($notification,$medical_expense, $query);
                    }
                    
                }
            }else{
                $medical_expense = $this->storeMedicalExp($notification->id, $input);
                $this->storeHealthProviderSvc($notification, $medical_expense, $treatment, $query);
                if ($level_type == 3) {
                    $this->saveFINANCEAccrual($notification,$medical_expense, $query);
                }
            }
        }

    }else{

        $medicals = $this->getMedicalExpenses($notification);
        dump($medicals);
        if (count($medicals) > 0) {
            foreach ($medicals as $medical) {
                $input['amount'] = $medical->amount;
                $input['member_type_id'] = $medical->member_type_id;
                $input['medical_expense_id'] = $medical->id;
                $input['user_id'] = $medical->user_id;
                $medical_expense = $this->storeMedicalExp3($notification->id, $input);
                $this->storeHealthProviderSvc2($notification, $medical_expense, $query);
                if ($level_type == 3) {
                    $this->saveFINANCEAccrual($notification,$medical_expense, $query);
                }
            }
        }else{
            $medical_expense = $this->storeMedicalExp2($notification->id, $query);
            $this->storeHealthProviderSvc2($notification, $medical_expense, $query);
            if ($level_type == 3) {
                $this->saveFINANCEAccrual($notification,$medical_expense, $query);
            }
        }
    }
}

public function getMedicalExpenses($notification){
   $medical_expenses = DB::table('medical_expenses')->where('notification_report_id', $notification->id)->get();
   return $medical_expenses;
}

public function storeMedicalExp($notification_report_id, $input) {
    $input['amount'] = $this->returnAmount($input);
    $resource_id =  $this->getMemberTypeValue($notification_report_id, $input) ;
//        $this->checkIfExist($notification_report_id,$input['member_type_id'],$resource_id);
    if ($input['member_type_id'] == 6) {
            //WCF as Employer
        $input['member_type_id'] = 1;
    }
    $medical_expense = AccrualMedicalExpense::updateOrCreate(['notification_report_id' => $notification_report_id,'member_type_id' =>  $input['member_type_id']], ['resource_id' => $resource_id, 'amount' => str_replace(",", "", $input['amount']),'user_id' => 85 ]);
    return $medical_expense;
}

public function storeMedicalExp3($notification_report_id, $input) {
    $resource_id =  $this->getMemberTypeValue($notification_report_id, $input) ;
//        $this->checkIfExist($notification_report_id,$input['member_type_id'],$resource_id);
    if ($input['member_type_id'] == 6) {
            //WCF as Employer
        $input['member_type_id'] = 1;
    }
    $medical_expense = AccrualMedicalExpense::updateOrCreate(['medical_expense_id' => $input['medical_expense_id'], 'notification_report_id' => $notification_report_id ,'member_type_id' =>  $input['member_type_id']], ['resource_id' => $resource_id, 'amount' => str_replace(",", "", $input['amount']),'user_id' => $input['user_id'] ]);
    return $medical_expense;
}

public function storeMedicalExp2($notification_report_id, $query) {
    $input = [];
    $medical_expense = null;
    foreach ($query as $q) {
        $input['member_type_id'] = $q->member_type_id;
        $input['amount'] = $this->returnAmount(['amount' => $q->amount]);
        if (!is_null($q->medical_expense_id)) {

            $resource_id =  $this->getMemberTypeValue($notification_report_id, $input) ;
        //        $this->checkIfExist($notification_report_id,$input['member_type_id'],$resource_id);
            if ($input['member_type_id'] == 6) {
            //WCF as Employer
                $input['member_type_id'] = 1;
            }
            $medical_expense = AccrualMedicalExpense::updateOrCreate(['notification_report_id' => $notification_report_id,'member_type_id' =>  $input['member_type_id']], ['resource_id' => $resource_id, 'amount' => str_replace(",", "", $input['amount']),'user_id' => $q->user_created_medical ]);
        }else{
            $medical_expense = $this->storeMedicalExp($notification_report_id, $input);
        }
        
    }
    return $medical_expense;
}

public function storeHealthProviderSvc2($notification, $medical_expense, $query)
{
    // dump($medical_expense);
    return DB::transaction(function () use ($notification, $medical_expense, $query) {
    // dump($query);
        $input = [];
        $input['accrual_medical_expense_id'] = !is_null($medical_expense) ? $medical_expense->id : null;
        foreach ($query as $q) {
         $input['attend_date'] = $q->disability_from_date;
         $input['dismiss_date'] =  $q->disability_to_date;
         $input['amount'] = $q->amount;
         break;
     }
     $input['notification_report_id'] = $notification->id;
     $input['incident_date'] = $notification->incident_date;
     $medical_practitioner_id = NULL;
     $disability = NULL;

     $notification_health_states = DB::table('notification_health_states')
     ->select('health_state_checklist_id','user_id','id as health_state_id','notification_eligible_benefit_id','state')
     ->where('notification_report_id', $notification->id)->get();

     if(count($notification_health_states)){
        foreach ($notification_health_states as $state) {
             // $notification_report_provider_service = $this->accrual_notification_health_provider_services->updateOrCreate($notification_health_provider_id, $state->health_state_checklist_id, $input, $state);

         $notification_health_state = $this->accrual_notification_health_states->updateOrCreate($state->health_state_checklist_id,$notification->id, $input, $state);

     }

 }
 if(count($query)){
    foreach ($query as $disability) {
        $medical_expense = $this->storeMedicalExp2($notification->id, $query);
        $input['accrual_medical_expense_id'] = !is_null($medical_expense) ? $medical_expense->id : null;
        $input['days'] = $disability->admin_days;
        $input['percent_of_ed_ld'] = $disability->percent_of_ed_ld;
        $input['user_id'] = $disability->user_created_disability;
        
        $input['notification_eligible_benefit_id'] = $disability->notification_eligible_benefit_id;

        $notification_report_id = is_null($disability->medical_notification_id) ? 
                                         $disability->eligible_notification_id :
                                         $disability->medical_notification_id;

        if($notification->id == $notification_report_id){
            $input['notification_report_id'] = $notification_report_id;
        }

        $notification_disability_state = $this->accrual_notification_disability_states->updateOrCreate($disability->disability_state_checklist_id, $disability->disability_id, $input);
    }

}

});

}

public function storeAssessmentToAccrual($notification,$medical_expense, $query){
    $input = [];
    $input['incident_date'] = $notification->incident_date;
    
    $input['accrual_medical_expense_id'] = !is_null($medical_expense) ? $medical_expense->id : null;
    if(count($query)){
        foreach ($query as $disability) {
         $input['days'] = $disability->admin_days;
         $input['percent_of_ed_ld'] = $disability->percent_of_ed_ld;
         $input['user_id'] = $disability->user_created_disability;
         $input['notification_report_id'] = $notification->id;
         $input['notification_eligible_benefit_id'] = $disability->notification_eligible_benefit_id;
            //Store notification disability state

         $notification_disability_state = $this->accrual_notification_disability_states->updateOrCreate($disability->disability_state_checklist_id, $disability->disability_id, $input);

         $input['days'] = $disability->assessment_days;
         $input['user_id'] = $disability->user_created_disability_assessment;
          $notification_report_id = is_null($disability->medical_notification_id) ? 
                                         $disability->eligible_notification_id :
                                         $disability->medical_notification_id;
                                         
        if($notification->id == $notification_report_id){
            $input['notification_report_id'] = $notification_report_id;
        }


         $notification_disability_state = $this->accrual_notification_disability_state_assessments->updateOrCreate2($notification_disability_state->id, $disability->disability_state_checklist_id, $disability->disability_id, $input);


     }

 }
}

// public function storeAssessmentToAccrual($notification, $query){
//     $input = [];
//     $input['incident_date'] = $notification->incident_date;
//     $medical_expense = $this->storeMedicalExp2($notification->id, $query);

//     $input['accrual_medical_expense_id'] = !is_null($medical_expense) ? $medical_expense->id : null;
//     if(count($query)){
//         foreach ($query as $disability) {
//          $input['days'] = $disability->admin_days;
//          $input['percent_of_ed_ld'] = $disability->percent_of_ed_ld;
//          $input['user_id'] = $disability->user_created_disability;
//          $input['notification_report_id'] = $notification->id;
//          $input['notification_eligible_benefit_id'] = $disability->notification_eligible_benefit_id;
//             //Store notification disability state

//          $notification_disability_state = $this->accrual_notification_disability_states->updateOrCreate($disability->disability_state_checklist_id, $disability->disability_id, $input);

//          $input['days'] = $disability->assessment_days;
//          $input['user_id'] = $disability->user_created_disability_assessment;
//           $notification_report_id = is_null($disability->medical_notification_id) ? 
//                                          $disability->eligible_notification_id :
//                                          $disability->medical_notification_id;
                                         
//         if($notification->id == $notification_report_id){
//             $input['notification_report_id'] = $notification_report_id;
//         }


//          $notification_disability_state = $this->accrual_notification_disability_state_assessments->updateOrCreate2($notification_disability_state->id, $disability->disability_state_checklist_id, $disability->disability_id, $input);


//      }

//  }
// }

public function storeHealthProviderSvc($notification, $medical_expense, $treatment, $query)
{
    // dump($medical_expense);
  return DB::transaction(function () use ($notification, $medical_expense, $treatment, $query) {

    $medical_practitioner_id = NULL;
    $disability = NULL;
// dump($medical_expense);
    $input['notification_report_id'] = $notification->id;
    $input['health_provider_id'] = $treatment->health_provider_id;
    $input['attend_date'] = $treatment->initial_treatment_date;
    $input['dismiss_date'] = $treatment->last_treatment_date;
    $input['rank'] = $treatment->rank;
    $input['accrual_medical_expense_id'] = !is_null($medical_expense) ? $medical_expense->id : null;

            // Store notification health provider
    $notification_health_provider = $this->accrual_notification_health_providers->updateOrCreate($notification->id, $input);
    $notification_health_provider_id = $notification_health_provider->id;    

            //Store notification health provider services
    if (count($treatment->practitioners)) {
             // dd($treatment->practitioners->disabilities);
        $medical_practitioner = $treatment->practitioners->first();
        $medical_practitioner_id = $medical_practitioner->id;
    }
    $input['medical_practitioner_id'] = $medical_practitioner_id;
    
    $input['incident_date'] = $notification->incident_date;
     // Store notification health provider practitioner
    $this->accrual_notification_health_provider_practitioners->update($notification_health_provider_id, $input);
    $notification_health_states = DB::table('notification_health_states')
    ->select('health_state_checklist_id','user_id','id as health_state_id','notification_eligible_benefit_id','state')
    ->where('notification_report_id', $notification->id)->get();

    if(count($notification_health_states)){
        foreach ($notification_health_states as $state) {
         $notification_report_provider_service = $this->accrual_notification_health_provider_services->updateOrCreate($notification_health_provider_id, $state->health_state_checklist_id, $input, $state);

         $notification_health_state = $this->accrual_notification_health_states->updateOrCreate($state->health_state_checklist_id,$notification->id, $input, $state);

     }

 }

 if(count($query)){
    foreach ($query as $disability) {
        $medical_expense = $this->storeMedicalExp2($notification->id, $query);
        $input['accrual_medical_expense_id'] = !is_null($medical_expense) ? $medical_expense->id : null;
        $input['days'] = $disability->admin_days;
        $input['percent_of_ed_ld'] = $disability->percent_of_ed_ld;
        $input['user_id'] = $disability->user_created_disability;
        $input['notification_report_id'] = $notification->id;
        $input['notification_eligible_benefit_id'] = $disability->notification_eligible_benefit_id;
        $input['accrual_medical_expense_id'] = !is_null($medical_expense) ? $medical_expense->id : null;
         $notification_report_id = is_null($disability->medical_notification_id) ? 
                                         $disability->eligible_notification_id :
                                         $disability->medical_notification_id;
                                         
        if($notification->id == $notification_report_id){
            $input['notification_report_id'] = $notification_report_id;
        }

            //Store notification disability state

        $notification_disability_state = $this->accrual_notification_disability_states->updateOrCreate($disability->disability_state_checklist_id, $disability->disability_id, $input);
    }

}

});

}

public function getMemberTypeValue($notification_report_id, $input) {
    // dump($input);
        //Insurance is selected
    $notification_reports = new NotificationReportRepository();
    // if ($input['member_type_id'] == 3) {
    //     $resource_id = $input['insurance_id'] ;
    // }
        //Employer is selected
    if ($input['member_type_id'] ==  1 || $input['member_type_id'] == 3 || $input['member_type_id'] == null) {
        $notification_report = $notification_reports->findOrThrowException($notification_report_id);
        $resource_id = $notification_report->employer_id;
    }
        //Employee is selected
    if ($input['member_type_id'] == 2) {
        $notification_report = $notification_reports->findOrThrowException($notification_report_id);
        $resource_id = $notification_report->employee_id;
    }
        //WCF is selected
    if ($input['member_type_id'] == 6) {
            //WCF Registration Number
        $resource_id = 4038;
    }

    return $resource_id;
}

public function returnAmount($input){
    // dump('amount'. $input['amount']);
  $amount = $input['amount'] == null ? 0 : $input['amount'];
  return $amount;
}

public function saveAllDisabiliities($disabilities, $benefit){

    foreach ($disabilities as $disability) {
        $data = [
            'admin_days' => $disability->admin_days,
            'admin_from_date' => $disability->from_date ? Carbon::parse($disability->from_date)->format('Y-m-d') : null,
            'admin_to_date' => $disability->to_date ? Carbon::parse($disability->to_date)->format('Y-m-d') : null,
            'assessed_days' => $disability->assessment_days,
            'accrual_notification_benefit_id' => $benefit->id,
            'disability_id' => $disability->disability_state_checklist_id,
            'notification_disability_state_id' => $disability->notification_disability_state_id
        ];
        AccrualBenefitDisability::create($data);
    }
}

public function updateAllDisabiliities($new_disabilities, $benefit){
    $new_disabilities = $new_disabilities->toArray();
    // dump($new_disabilities);
    // dump($new_disabilities[0]['admin_days']);
    // dump($new_disabilities);


    $old_disabilities = AccrualBenefitDisability::where('accrual_notification_benefit_id', $benefit->id)->get();
    // dump(count($old_disabilities));
    $i = 0;
    foreach ($old_disabilities as $disability) {
        $data = [
            'admin_days' => $new_disabilities[$i]['admin_days'],
            'admin_from_date' => $new_disabilities[$i]['from_date'] ? Carbon::parse($new_disabilities[$i]['from_date'])->format('Y-m-d') : null,
            'admin_to_date' => $new_disabilities[$i]['to_date'] ? Carbon::parse($new_disabilities[$i]['to_date'])->format('Y-m-d') : null,
            'assessed_days' => $new_disabilities[$i]['assessment_days'],
            'accrual_notification_benefit_id' => $benefit->id,
            'disability_id' => $new_disabilities[$i]['disability_state_checklist_id'],
            'notification_disability_state_id' => $new_disabilities[$i]['notification_disability_state_id']
        ];
        dump($data);
        $disab = AccrualBenefitDisability::where('id',$disability->id)->update($data);

        $i++;
    }
}

public function getDisability($benef_to_accrual){
    $disability_accrual = AccrualBenefitDisability::where('accrual_notification_benefit_id', $benef_to_accrual->id)->first();
    return $disability_accrual;
}

public function saveDisabilities($disability_to_accrual, $data, $benef_to_accrual){
    if (is_null($disability_to_accrual)) {
        // AccrualBenefitDisability::create($data);
        $id = DB::table('accrual_benefit_disabilities')->insertGetId($data);
        $disability_to_accrual = AccrualBenefitDisability::find($id);
        dump('save admin disability');
    }else{
        $disability_to_accrual = AccrualBenefitDisability::where('accrual_notification_benefit_id', $benef_to_accrual->id)->first();
        $disability_to_accrual->update($data);
        dump('update admin disability');
    }

}

public function saveBenefits($data, $benefit, $notification){
    // dump($benefit);
    // dump($notification);
    $benef_to_accrual = $this->getBenefits($benefit);
    dump($benef_to_accrual);
    if (is_null($benef_to_accrual)) {
        // $benef_to_accrual = AccrualNotificationBenefit::create($data);
        $id = DB::table('accrual_notification_benefits')->insertGetId($data);
        $benef_to_accrual = AccrualNotificationBenefit::find($id);
        dump('save admin benefit');


        $disabilities = $this->getDisabilitiesForAccrual($notification, $benefit);

         // dump($disabilities
                // $disabilities = $this->saveAllDisabiliities($disabilities, $benefit_to_accrual);

                // $benefit = $this->getUnsaveBenefitAccrual($disabilities, $benefit);
                // if ($benefit) {
        $disability = $this->saveAllDisabiliities($disabilities, $benef_to_accrual);
                // } else {
                    // $disability = $this->updateAllDisabiliities($disabilities, $benefit);
                // }
    }else{
        // $benef_to_accrual = AccrualNotificationBenefit::where('eligible_benefit_id', $benefit->id)->update($data);

        $benef_to_accrual = AccrualNotificationBenefit::find($benef_to_accrual->id)->first();
        $benef_to_accrual->update($data);
        dump('update admin benefit');

        $disabilities = $this->getDisabilitiesForAccrual($notification, $benefit);
        $disability = $this->updateAllDisabiliities($disabilities, $benef_to_accrual);
    }

    return $benef_to_accrual;
}

public function getUnsaveBenefitAccrual($disabilities, $benefit){
    $return = false;
    $benef_to_accrual = AccrualNotificationBenefit::where('eligible_benefit_id', $benefit->id)->first();
    return $benefit_to_accrual;

}


public function saveAccruedNotificationAssessment($notification){

    DB::transaction(function () use ($notification) {
        $benefits = $this->getBenefitsForAccrualForAssessment($notification);
        $accrual_eligibility = $this->accrualAssessmentEligibility($notification, $benefits);

        if ($accrual_eligibility > 0) {

            $this->saveAccruedClaims($notification);

            $data = [
                'fin_year_id' => $this->getCurrentFinYearId(),
                'source' => 0,
                'status' => 0,
                'has_workflow' => null,
                'wf_done' => null,
                'wf_done_date' => null,
                'notification_report_id' => $notification->id,

            ];

            $accrual = $this->getAccrual($notification);

            if (is_null($accrual)) {
                $this->query()->create($data);
                $accrual = $this->getAccrual($notification);
            }else{
                $this->query()->where('notification_report_id', $notification->id)->update($data);
            }
            $this->insertInboxAndTask($accrual->id);
            $this->updateAccruedNotificationReport($notification);

            $benefit_assessments = $this->getBenefitsForAccrualForAssessment($notification);

            if (count($benefit_assessments)) {
                foreach ($benefit_assessments as $benefit) {

                     // check workflow level
                    $level = $this->checkWorkFlowLevel($benefit->notification_workflow_id);
                    $is_eligible = $this->accrualSingleAssessmentEligibility($notification, $benefit);

                    if ($is_eligible > 0) {
                // $notification_workflow = $this->returnNotificationWorkflow($benefit->notification_workflow_id);
                        $is_assessed = $this->claimAssessmentLevelApproveProcess($level, $notification->id, 18);

                        $data = [
                            'benefit_type_id' => $benefit->benefit_type_claim_id,
                            'accrual_notification_report_id' => $accrual->id,
                            'eligible_benefit_id' => $benefit->id,
                            'pd_percent' => $benefit->percent_of_ed_ld,
                            'type' => 2,
                            'is_assessed' => $is_assessed,
                        ];

                        // $benef_to_accrual = $this->getBenefits($benefit);

                        $benef_to_accrual = $this->saveBenefits($data,$benefit,$notification);

                        $disabilities = $this->getDisabilitiesForAccrual($notification, $benefit);

                        $disabilities = $this->saveAllDisabiliities($disabilities, $benefit_to_accrual);

                        $benefit = $this->getUnsaveBenefitAccrual($disabilities, $benefit);
                        if ($benefit) {
                            $disability = $this->saveAllDisabiliities($disabilities, $benefit);
                        } else {
                            $disability = $this->updateAllDisabiliities($disabilities, $benefit);
                        }

                    }

            //     if ($level >= 12) {
            //         $is_assessed = $this->claimAssessmentLevelApproveProcess($level, $notification->id, 18);
            //         dump($is_assessed);

            //         $data = [
            //             'benefit_type_id' => $benefit->benefit_type_claim_id,
            //             'accrual_notification_report_id' => $accrual->id,
            //             'eligible_benefit_id' => $benefit->id,
            //             'pd_percent' => $benefit->percent_of_ed_ld,
            //             'type' => 3,
            //             'is_assessed' => $is_assessed,
            //         ];

            //     // $benef_to_accrual = AccrualBenefitDisability::where('eligible_benefit_id', $benefit->id)->first();
            //         $benef_to_accrual = $this->getBenefits($benefit);

            //         if (is_null($benef_to_accrual)) {
            //             AccrualNotificationBenefit::create($data);
            //             $benef_to_accrual = $this->getBenefits($benefit);
            //         }else{
            //             AccrualNotificationBenefit::where('eligible_benefit_id', $benefit->id)->update($data);
            //         }

            //         if (count($disabilities)) {

            //             foreach ($disabilities as $disability) {
            //                $data = [
            //                 'admin_days' => $disability->admin_days,
            //                 'admin_from_date' => $disability->from_date ? Carbon::parse($disability->from_date)->format('Y-m-d') : null,
            //                 'admin_to_date' => $disability->to_date ? Carbon::parse($disability->to_date)->format('Y-m-d') : null,
            //                 'assessed_days' => $disability->assessment_days,
            //                 'accrual_notification_benefit_id' => $benef_to_accrual->id,
            //                 'disability_id' => $disability->disability_state_checklist_id,
            //                 'notification_disability_state_id' => $disability->notification_disability_state_id
            //             ];
            //             $disability_accrual = AccrualBenefitDisability::where('accrual_notification_benefit_id', $benef_to_accrual->id)->first();

            //             if (is_null($disability_accrual)) {
            //                 $disability_accrual = AccrualBenefitDisability::create($data);
            //             }else{
            //                 $disability_accrual = AccrualBenefitDisability::where('accrual_notification_benefit_id', $benef_to_accrual->id)->update($data);
            //             }
            //         }

            //     }

            // }
                }



            }
        }


    });
}

public function getBenefits($benefit){
    $benef_to_accrual = AccrualNotificationBenefit::where('eligible_benefit_id', $benefit->id)->first();

    return $benef_to_accrual;
}

private function claimAssessmentLevelApproveProcess($level, $resource_id, $wf_module_id)
{
    $return = 1;
    $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $resource_id]);
    /* Claim Assessment Doctors Modification */
    $notification_reports = new NotificationReportRepository();
                //check if nature of incident is  on notification reports
    $notification_report = $notification_reports->findOrThrowException($resource_id);
    if (!$notification_report->nature_of_incident_cv_id) {
        $return = 0;
    }
                // check if assessment is done
    if ($notification_report->notificationDisabilityStates()->count()){
        if (!$notification_report->notificationDisabilityStates()->has('notificationDisabilityStateAssessment')->count()){
            $return = 0;
        }
    }

    return $return;
}

public function checkWorkFlowLevel($notification_workflow_id){
    $level = 0;
    $notification_workflow = NotificationWorkflow::where('id',$notification_workflow_id)->first();

    if (!is_null($notification_workflow)) {
        $wf_tarck = WfTrack::select('level')->where('resource_id', $notification_workflow->id)->where('resource_type', 'App\Models\Operation\Claim\NotificationWorkflow')
        ->join('wf_definitions','wf_tracks.wf_definition_id','=','wf_definitions.id')
        ->where('wf_tracks.status','!=',3)
        ->orderBy('wf_tracks.id','desc')->first();

        if (!is_null($wf_tarck)) {
            $level = $wf_tarck->level;
        }
    }

    return $level;

}

public function hasWorkFlowLevel1Accepted($notification_report_id){
    $level = 0;
    $notification_workflows = NotificationWorkflow::where('notification_report_id',$notification_report_id)->get();

    if (count($notification_workflows) > 0) {
        foreach ($notification_workflows as $notification_workflow) {
            $wf_tarck = WfTrack::select('level')->where('resource_id', $notification_workflow->id)->where('resource_type', 'App\Models\Operation\Claim\NotificationWorkflow')
            ->join('wf_definitions','wf_tracks.wf_definition_id','=','wf_definitions.id')
            ->where('wf_tracks.status','!=',3)
            ->orderBy('wf_tracks.id','desc')->first();

            if (!is_null($wf_tarck)) {
                if($wf_tarck->level == 1){
                    $level++;
                }
            } 
        }
    }

    return $level;

}

public function returnNotificationWorkflow($notification_workflow_id){
    $notification_workflow = NotificationWorkflow::findOrFail($notification_workflow_id);
    return $notification_workflow;
}

public function updateAccruedNotificationReport($notification){
    $notification_report = NotificationReport::where('id', $notification->id)
    ->update([
        'is_accrued' => true,
        'accrued_status' => 1,
    ]);
}

public function saveAccruedClaims($notification){
    $claim = AccrualClaim::where('notification_report_id', $notification->id)->first();
    if(is_null($claim)){
        AccrualClaim::create([
            'notification_report_id' => $notification->id,
            'incident_type_id' => $notification->incident_type_id,
            'monthly_earning' => $notification->monthly_earning ? $notification->monthly_earning : 0,
            'contrib_month' => $notification->contrib_month,
            'pd' => $notification->claim ? $notification->claim->pd : 0.00,
            'user_id' => $notification->claim ? $notification->claim->user_id : null,
            'date_of_mmi' => $notification->claim ? $notification->claim->date_of_mmi : null,
        ]);

    }else{
        $claim->update([
            'incident_type_id' => $notification->incident_type_id,
            'monthly_earning' => $notification->monthly_earning ? $notification->monthly_earning : 0,
            'contrib_month' => $notification->contrib_month,
            'pd' => $notification->claim ? $notification->claim->pd : 0.00,
            'user_id' => $notification->claim ? $notification->claim->user_id : null,
            'date_of_mmi' => $notification->claim ? $notification->claim->date_of_mmi : null,
        ]);
    }
    
}

public function getCurrentFinYearId(){
    $fin_year_name = $this->currentFinancialYear();
    $fin = DB::table('main.fin_years')->select('id')
    ->where('name',$fin_year_name)->first();
    if($fin){
        return $fin->id;
    }else{
        return null;
    }


}


public function verifyDocumentForAccrual($notification){

    $return = false;

    $query = DB::table('main.document_notification_report')
    ->where('notification_report_id',$notification->id)->pluck('document_id')->toArray();

    switch ($notification->incident_type_id) {
        case 1:
        $conditions1 = [18,21];
        $conditions2 = [19,21];
        $first = $this->isDocExists($conditions1, $query);
        $second = $this->isDocExists($conditions2, $query);

        if ($first == true Or $second == true) {
            $return = true;
        }

        break;
        case 2:
        $conditions = [8,6];
        $first = $this->isDocExists($conditions, $query);

        if ($first == true) {
            $return = true;
        }

        break;
        case 3:
        $conditions1 = [15,12,6];
        $conditions2 = [46,12,6];

        $first = $this->isDocExists($conditions1, $query);
        $second = $this->isDocExists($conditions2, $query);

        if ($first == true Or $second == true) {

            $return = true;
        }

        break;

        default:

        break;

    }

    return $return;
}

public function isDocExists($check,$array){
    $return = true;

    for ($i=0; $i < count($check); $i++) { 
      if (!in_array($check[$i], $array)) {
        $return = false;
    }
}
return $return;
}


public function notificationsToAccrualInFinYear($from_date,$to_date){
    $notifications = NotificationReport::whereBetween('incident_date',[$from_date,$to_date])
    ->where('progressive_stage',4)
    // ->where('id',4701)
    ->where(function ($query) {
        $query->where('contrib_before', '=', 1)->orWhere('contrib_on', '=' ,1);
    })->get();
        // dump($notifications);
    return $notifications;
}

public function currentFinancialYear(){

    $this_year = date('Y');
    $next_year = date('Y') + 1;
    $last_year = date('Y') - 1;

    $current_financial_year = '';
    global $current_financial_year;

    if (date('m') > 6) {

        $current_financial_year = $this_year.'/'.$next_year;

    } else {
        $current_financial_year = $last_year.'/'.$this_year;
    }

    return $current_financial_year;

}
public function findAccrualIncidentDate($id)
{
    $notification_report = NotificationReport::findOrFail($id);

    $incident_type_id = $notification_report->incident_type_id;
    if ($incident_type_id == 1) {
            //accident
        $incident_date = $notification_report->accident->accident_date;
    } elseif ($incident_type_id == 2) {
            //disease
        $incident_date = $notification_report->disease->diagnosis_date;
    } elseif ($incident_type_id == 3) {
            //death
        $incident_date = $notification_report->death->death_date;
    }
        // convert date into first day of month
//        $incident_date = Carbon::create(Carbon::parse($incident_date)->format('Y'), Carbon::parse($incident_date)->format('m'), 1);
    return $incident_date;
}
     /**
     * @param Model $incident
     * @return mixed
     */
     public function getAccrualIncidentDate(Model $incident)
     {
        $incident_type_id = $incident->incident_type_id;
        switch ($incident_type_id) {
            case 1:
                //accident
            $incident_date = $incident->accident->accident_date;
            break;
            case 2:
                //disease
            $incident_date = $incident->disease->diagnosis_date;
            break;
            case 3:
                //death
            $incident_date = $incident->death->death_date;
            break;
        }
        // convert date into first day of month
//        $incident_date = Carbon::create(Carbon::parse($incident_date)->format('Y'), Carbon::parse($incident_date)->format('m'), 1);
        return $incident_date;
    }
    public function checkAllMandatoryDocumentsAccrualProgressive(Model $incident, $need_conditional = true)
    {
        $uploaded_documents = $this->getUploadedDocuments($incident);
        $pending_documents = (new DocumentRepository())
        ->query()
            //->where('ismandatory', 1)
        ->whereIn("id", $this->getProgressiveDocumentList($incident, [], NULL, $need_conditional))
        ->whereNotIn('id', $uploaded_documents)->orderBy('document_group_id', 'asc')
        ->get();
        return $pending_documents;
    }
    public function getUploadedDocuments(Model $incident)
    {
        $uploaded_documents = $incident->documents()->whereNotNull('document_notification_report.name')->get()->pluck('id')->all();
        return $uploaded_documents;
    }

    public function getProgressiveDocumentList(Model $incident, $benefits = [], $member_type_id = 2, $need_conditional = true)
    {
        $incident_type_id = $incident->incident_type_id;
        $codeValue = new CodeValueRepository();
        if ($incident_type_id == 3) {
            //Death Incident ...
            $death_cause_id = $incident->death->death_cause_id;
            $incident_type_id = $death_cause_id;
        }
        $arrList = [];
        if (count($benefits)) {
            foreach ($benefits as $benefit) {
                switch ($benefit) {
                    case 2:
                        //Funeral Grants
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 3:
                        //Permanent Partial Disablement
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 4:
                        //Permanent Total Disablement
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 5:
                        //Constant Attendance Care Grant
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 7:
                        //Temporary Partial Disablement
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 8:
                        //Temporary Total Disablement
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 9:
                        //Rehabilitation
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 10:
                        //Medical Aid Expenses
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 11:
                        //Survival Benefits
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 14:
                        //Temporary Partial Disablement Refund
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 15:
                        //Temporary Total Disablement Refund
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                    case 16:
                        //Medical Aid Expenses Refund
                    $array = (new DocumentBenefitRepository())->query()->where(['benefit_type_id' => $benefit, 'member_type_id' => $member_type_id, 'incident_type_id' => $incident_type_id])->pluck("document_id")->all();
                    $arrList += $array;
                    break;
                }
            }
        } else {

            //Check if is not incident rejected by system [Out of time | Before Statutory Period]
            switch ($incident->notification_staging_cv_id) {
                case $codeValue->NSNREJSYBS():
                case $codeValue->NSNREJSYOT():
                case $codeValue->NSONNREJSYW():
                case $codeValue->NSCSYSREJ():
                    //Notification Rejection by System (Before the WCF Statutory Period)
                    //Notification Rejection by System (Out of Time)
                    //On Notification Rejection by System Workflow
                    //Closed Rejected Incident By System
                    $array = [27]; //Must be requiring WCN-1 document
                    $arrList += $array;
                    break;
                    default:
                    //MAE Approval (Default Required for Notification Approval)
                    $benefit_type = (new BenefitTypeRepository())->find(10);
                    //$array = [];
                    $array = $benefit_type->documents()->where("documents.ismandatory", 1)->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                    $arrList += $array;
                    //Add documents for death
                    switch ($incident->incident_type_id) {
                        case 3:
                            //Death Incident
                            $arrList[] = 12; //Burial Permit
                            $arrList[] = 11; //Death Certificate
                            break;
                        }
                        if ($need_conditional) {
                        //Check for additional document depending on the incident nature...
                        //check if employer is foreigner
                            if ($incident->employee_id) {
                                $employee = $incident->employee;
                                if (!is_null($employee->country_id) And $employee->country_id != 1) {
                                $array = [26]; //Must be requiring [Work Permit] Document
                                $arrList[] = 26;
                            }
                        }
                        //Check if is an Accident
                        if ($incident->incident_type_id == 1 Or $incident->incident_type_id == 3) {
                            $codeValue = new CodeValueRepository();
                            switch ($incident->incident_type_id) {
                                case 3:
                                    //Death
                                $accident_cause_cv_id = $incident->death->accident_cause_cv_id;
                                break;
                                default:
                                    //Accident
                                $accident_cause_cv_id = $incident->accident->accident_cause_cv_id;
                                break;
                            }
                            //This is an accident
                            //check if cause of accident is MTA
                            if ($codeValue->MTA($accident_cause_cv_id)) {
                                $array = [56]; //Must be requiring [PF90] Document
                                $arrList[] = 56;
                            }
                            //if cause of accident is Criminal, Thugs & Robbery
                            if ($accident_cause_cv_id == $codeValue->ACTHGSROBBRY()) {
                                $array = [10]; //Must be requiring [General Police Report] Document
                                $arrList[] = 10;
                            }
                            //If cause of Accident is Marine : requires Captain Report, document_id 55 TODO: add in the cause of accident list

                            //If cause of Accident is Air Traffic Accident : required Air Marshal Report, document_id 54 TODO: add in the cause of accident list
                        }
                    }
                    break;
                }

                $benefits = $incident->eligibleBenefits;
                foreach ($benefits as $benefit) {
                    $member_type_id = $benefit->pivot->member_type_id;
                    $benefit_types = $benefit->benefitTypes;
                    foreach ($benefit_types as $benefit_type) {
                        switch ($benefit_type->id) {
                            case 2:
                            //Funeral Grants
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 3:
                            //Permanent Partial Disablement
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 4:
                            //Permanent Total Disablement
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 5:
                            //Constant Attendance Care Grant
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 7:
                            //Temporary Partial Disablement
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 8:
                            //Temporary Total Disablement
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 9:
                            //Rehabilitation
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 10:
                            //Medical Aid Expenses
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 11:
                            //Survival Benefits
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 14:
                            //Temporary Partial Disablement Refund
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 15:
                            //Temporary Total Disablement Refund
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                            case 16:
                            //Medical Aid Expenses Refund
                            $array = $benefit_type->documents()->wherePivot("member_type_id", $member_type_id)->wherePivot("incident_type_id", $incident_type_id)->pluck("documents.id")->all();
                            $arrList += $array;
                            break;
                        }
                    }
                }
            }
        //$documents = (new DocumentRepository())->query()->select(['name', 'id'])->whereIn("id", $arrList)->get();
        //return $documents;
            return $arrList;
        }

        public function isAccrualDate(){

         $today = Carbon::now()->format('m-d');
         $return = false;

         switch ($today) {
             case '07-01':
                // first quarter
             $return = true;
             break;
             case '10-01':
                // sec Quarter
             $return = true;
             break;
             case '01-01':
                // third quarter
             $return = true;
             break;
             case '04-01':
                // fourth quarter
             $return = true;
             break;

             default:
                   # code...
             break;
         }

         return $return;
     }


     public function getWfStatus($status)
     {
        $return = "";
        switch ($status) {
            case 1:
                //if administration
            $return = "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='Administration level'><i class='icon fa fa-wheelchair' ></i> Admin</span>";
            break;
            case 2:
                //if assessment
            $return = "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='Assessment level'><i class='icon fa fa-user-md' ></i> Assessment</span>";
            break;
            case 3:
                //if finance
           $return = "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='Fiannce level'><i class='icon fa fa-money' ></i> Finance</span>";
            break;
        }
        return $return;
    }

 }