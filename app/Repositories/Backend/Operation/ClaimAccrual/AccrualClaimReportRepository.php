<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\AccidentType;
use App\Models\Operation\Claim\Claim;
use App\Models\Operation\Claim\DeathCause;
use App\Models\Operation\ClaimAccrual\AccrualClaim;
use App\Repositories\Backend\Operation\Claim\Pd\PdAgeAdjustmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdAgeRangeRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdFecRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdInjuryRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdOccupationAdjustmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdOccupationCharacteristicRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Repositories\Backend\Operation\Claim\NotificationDisabilityStateAssessmentRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualClaimRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualClaimReportRepository;

class AccrualClaimReportRepository extends  BaseRepository
{

    const MODEL = AccrualClaim::class;

    protected  $notification_disability_state_assessments;
    protected  $accrual_notification_disability_state_assessments;

    public function __construct()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->notification_disability_state_assessments = new NotificationDisabilityStateAssessmentRepository();
        $this->accrual_notification_disability_state_assessments = new AccrualNotificationDisabilityStateAssessmentRepository();
        // $this->claim_accrual = new NotificationsRepository();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $claim = $this->query()->find($id);

        if (!is_null($claim)) {
            return $claim;
        }
        throw new GeneralException(trans('exceptions.backend.claim.claim_not_found'));
    }

    


    public function claimAssessmentProgressive(Model $incident, Model $eligible, array $input)
    {
        return DB::transaction(function () use ($incident, $eligible, $input) {

            $claim = $incident->claim;



        });

    }
    /**
     * Get claims which need Constant care assistant and not yet added
     */
    public function getClaimsPendingCcaForDataTable()
    {
        $benefit_type_claim_id = 5; //For PD
        return $this->query()->where('need_cca', 1)->where('cca_registered', 0)->whereHas('notificationReport', function ($query) use($benefit_type_claim_id){
            $query->whereHas('benefits', function ($query)use($benefit_type_claim_id){
                $query->where('benefit_type_claim_id', $benefit_type_claim_id)->where('processed',1);
            });
        });
    }



    public function getClaimAccrualReportForDatatable()
    {
        $claim_accrual = $this->getClaimAccrualReportQueryFilter()
        ->select($this->getClaimAccrualReportSelectQuery());

        return $claim_accrual;
    }


    public function getClaimAccrualReportQueryFilter()
    {
    // dd(request()->all);
        $codeValue = new CodeValueRepository();
        $claim_accrual = $this->getClaimAccrualReportQuery();
        // $accrual = $this->AccrualClaimRepository();
     //    $input = $request->all();
     //    $search_flag = array_key_exists('search_flag', $input) ? $input['search_flag'] : 0;

     //    if(($search_flag == 1)) {
     //     $date_range = $this->date_ranges->dateRange($request->all());
     //     $date_range = ([
     //         'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
     //         'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null, 'input' => $date_range]);
     // }
        return $claim_accrual;
        $incident = (int) request()->input("incident");
        if ($incident) {
            $claim_accrual->where("notification_reports.incident_type_id", "=", $incident);
        }
        $employee = request()->input("employee");
        if ($employee) {
            $claim_accrual->where("notification_reports.employee_id", "=", $employee);
        }
        $employer = request()->input("employer");
        if ($employer) {
            $claim_accrual->where("notification_reports.employer_id", "=", $employer);
        }
        $region = request()->input("region");
        if ($region) {
            $claim_accrual->where("regions.id", "=", $region);
        }
        $district = request()->input("district");
        if ($district) {
            $claim_accrual->where("districts.id", "=", $district);
        }
        $status = request()->input('status');
        $accrued_das = request()->input('accrued_das');

        if ($accrued_das) {
            $allocated_case_ids = $this->AccrualClaimRepository()->allocatedFilesIds();
            $claim_accrual = ($accrued_das==1) ? $claim_accrual->whereIn("notification_reports.id", $allocated_case_ids) : $claim_accrual->whereNotIn("notification_reports.id", $allocated_case_ids);
        }

        $accrued_dfpi = request()->input('accrued_dfpi');
        if ($accrued_dfpi) {
            $report_case_ids = $plan_notification_repo->returnAllNotificationIdsWithReportUploaded();
            $claim_accrual = ($report==1) ? $claim_accrual->whereIn("notification_reports.id",$report_case_ids) : $claim_accrual->whereNotIn("notification_reports.id", $report_case_ids);
        }

        $claim_paid = request()->input('claim_paid');
        if ($claim_paid) {
            $investigated_ids = $plan_notification_repo->returnAllInvestigatedNotificationIds();
            if ($claim_paid == 1) {
               $claim_accrual =  $claim_accrual->whereIn("notification_reports.id",$investigated_ids);
           } else {
            $claim_accrual = $claim_accrual->whereNotIn("notification_reports.id",$investigated_ids);
        }
    }


        //check claim_accrual status
    switch ($status) {
        case 1:
                //Allocated
        // $claim_accrual->whereIn("notification_reports.id", $this->allocatedFilesIds());
        break;
        case 2:
            //Unallocated
        // $claim_accrual->whereNotIn("notification_reports.id", $this->allocatedFilesIds());
        break;
        case 3:
                //Allocated to User
        $user_id = request()->input('user_id');
        // $claim_accrual->whereIn("notification_reports.id", $this->allocatedFilesIds());
        if($user_id){
            // $claim_accrual->whereIn("investigation_plan_notification_reports.assigned_to", [$user_id]);
        }

        break;
    }
    return $claim_accrual;
}


public function getClaimAccrualReportSelectQuery()
{
    $codeValue = new CodeValueRepository();
    $onClaimAccrual = "({$codeValue->NSPCRFI()}, {$codeValue->NSOPRFLI()})";
    return [
        "*",
        DB::raw("accrual_claims.day_hours as age"),
        DB::raw("notification_reports.id as id"),
        DB::raw("accrual_claims.id as id"),
        DB::raw("notification_reports.filename"),
        DB::raw("incident_types.name as incident_type"),
        DB::raw("case when notification_reports.employee_id is null then notification_reports.employee_name else concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) end as employee"),
        DB::raw("case when notification_reports.employer_id is null then notification_reports.employer_name else employers.name end as employer"),
        // DB::raw("notification_reports.incident_date"),
        // DB::raw("notification_reports.receipt_date as notification_date"),
        // DB::raw("notification_reports.created_at as registration_date"),
        // DB::raw("accidents.job_title_id as occupation"),
        // DB::raw("districts.name as district"),
        // DB::raw("accrual_pensioners.dob as date_of_birth"),
        // DB::raw("accidents.activity_performed"),
        // DB::raw("code_values.name as accident_cause"),
        // DB::raw("accident_types.name as nature"),
        // DB::raw("accidents.accident_place"),
        // DB::raw("concat_ws(' ', coalesce(accrual_dependents.firstname, ''), coalesce(accrual_dependents.middlename, ''),coalesce(accrual_dependents.lastname, '')) as dependants"),
        // DB::raw("accrual_notification_disability_states.days"),
        // DB::raw("accrual_notification_disability_states.percent_of_ed_ld"),
        // DB::raw("accrual_notification_benefits.pd_percent"),
        // DB::raw("accrual_dependent_employee.funeral_grant"),
        // DB::raw("accrual_notification_disability_states.days"),
        // DB::raw("accrual_claim_compensation.dob as nature"),
    ];
}


public function getClaimAccrualReportQuery()
{
    $closedIncident = (new CodeValueRepository())->NSCINC();
    $claim_accrual = $this->query()
    ->leftJoin("notification_reports","accrual_claims.notification_report_id", "=", "accrual_claims.id")
    ->leftJoin("incident_types", "accrual_claims.incident_type_id", "=", "incident_types.id")
    ->leftJoin("employees", "notification_reports.employee_id", "=", "employees.id")
    ->leftJoin("employers", "notification_reports.employer_id", "=", "employers.id")
    ->leftJoin("accrual_medical_expenses","accrual_medical_expenses.notification_report_id", "=", "notification_reports.id")
    // ->leftJoin("accrual_dependent_employee","accrual_dependent_employee.employee_id","=", "employees.id")
    // ->leftJoin("accrual_dependents","accrual_dependent_employee.accrual_dependent_id","=", "accrual_dependents.id")
    // ->leftJoin("districts", "notification_reports.district_id", "=", "districts.id")
    // ->leftJoin("regions", "districts.region_id", "=", "regions.id")
    // ->join("code_values", "notification_reports.notification_staging_cv_id", "=", "code_values.id")
    // ->join("accidents", "accidents.accident_cause_cv_id", "=", "code_values.id")
    // ->leftJoin("accident_types", "accidents.accident_type_id", "=", "accident_types.id")
    // ->leftJoin("accrual_pensioners","accrual_pensioners.notification_report_id", "=", "notification_reports.id")
    // ->leftJoin("accrual_notification_disability_states","accrual_notification_disability_states.notification_report_id", "=", "notification_reports.id")
    //  ->leftJoin("accrual_notification_reports","accrual_notification_reports.notification_report_id", "=", "notification_reports.id")
    // ->leftJoin("accrual_notification_benefits","accrual_notification_benefits.accrual_notification_report_id", "=", "accrual_notification_reports.id")
    // ->leftJoin("accrual_dependent_employee","accrual_dependent_employee.accrual_dependent_id", "=", "accrual_dependents.id")
    // ->where("notification_staging_cv_id", "<>", $closedIncident);
    // ->leftJoin("claim_medical_expenses","accrual_claims.incident_type_id", "=", "incident_types.id")
    ;
    return $claim_accrual;
}





}