<?php

namespace App\Repositories\Backend\Operation\ClaimAccrual;

use App\Exceptions\GeneralException;
use App\Models\Operation\ClaimAccrual\AccrualNotificationBenefit;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class AccrualNotificationBenefitRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class AccrualNotificationBenefitRepository extends  BaseRepository
{

    const MODEL = AccrualNotificationBenefit::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $accrual = $this->query()->find($id);

        if (!is_null($accrual)) {
            return $accrual;
        }
        throw new GeneralException(trans('exceptions.backend.claim.accident_not_found'));
    }

}