<?php

namespace App\Repositories\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\Accident;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\ClaimFollowup\ClaimFollowup;
use App\Models\Operation\Claim\NotificationReport; 
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Operation\Claim\ClaimFollowupUpdateRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Auth\User;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Claim\ClaimFollowupCalculation;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;


/**
 * Class AccidentRepository
 * @package App\Repositories\Backend\Operation\Claim
 */
class ClaimFollowupRepository extends  BaseRepository
{

    const MODEL = ClaimFollowup::class;

    public function __construct()
    {
        $this->notification_report = new NotificationReportRepository();
        $this->claim_followup_update = new ClaimFollowupUpdateRepository();

    }

    public function findOrThrowException($id)
    {
        $followup = $this->query()->find($id);
        if (!is_null($followup)) {
            return $followup;
        }
        throw new GeneralException('The followup does not exist');
    }

    public function syncFolloups(){
        // $this->saveClaimFilesFollowups();
        $this->followUpCalculation();
    }

    public function followUpCalculation($input = null){

        $collection = array();
        $current_month = Carbon::now()->format('Y-m-d');

        $staff_claims = $this->query()->select('claim_followups.user_id','notification_report_id',DB::raw("COUNT(claim_followups.notification_report_id) as total_notification"))->groupBy('claim_followups.user_id','claim_followups.notification_report_id')->where('current_month', $current_month)->get();

        $staff_claims = $this->query()->pluck("user_id as user_name");
        $array = $staff_claims->toArray();

        if (count($array) < 1) {
            return $collection;
        }



        $values = array_count_values($array);

        $claim_array = array();

        $i = 0;
        $sn = 1;
        foreach ($values as $key => $value) {


            $data['user_id'] = $key;
            $data[$this->getTotalFiles($key)['week_name']] = $this->getTotalFiles($key)['total_files'];
            $data[$this->getTotalFiles($key)['current_week_name']] = $this->getTotalFiles($key)['week_division'];

            $data[$this->getTotalFiles($key)['attended_files_name']] = $this->getTotalFiles($key)['attended_files'];
            $data[$this->getTotalFiles($key)['missed_files_name']] = $this->getTotalFiles($key)['missed_files'];
            $data[$this->getTotalFiles($key)['overall_files_name']] = $this->getTotalFiles($key)['overall_files'];
            $data['current_month'] = $current_month;

            $i++;
            $sn++;

            /** save calculation */
            DB::table('claim_followup_calculations')->updateOrInsert(
                ['user_id' => $data['user_id'] , 'current_month' => $current_month] , $data );

        }
    }


    public function saveClaimFilesFollowups(){

        $cr_in_followups_array = $this->CRfilesInFollowUps();

        $notification_reports = $this->notificationReportArrayFromFileNames($cr_in_followups_array);

        foreach ($notification_reports as $n) {
            dump('start saving normals');
            $this->saveFollowupClaims($n);
        }

        $cr_intermidiate_followups_array = $this->CRfilesIntermidiateFollowUps();

        $notification_reports = $this->notificationReportArrayFromFileNames($cr_intermidiate_followups_array);

        foreach ($notification_reports as $n) {
         $elgb = new AccrualNotificationReportRepository();
         $is_eligible = $elgb->hasWorkFlowLevel1Accepted($n->id);
         if ($is_eligible > 0) {
            dump('start saving abnormals');
            $this->saveFollowupClaims($n);
        }
    }

}

public function notificationReportArrayFromFileNames($cr_in_followups_array){
    $notification_reports = NotificationReport::whereIn('filename',$cr_in_followups_array)->get();
    return $notification_reports;
}

public function CRfilesNotInFollowUps(){

    $paid_claims = DB::table('main.cr2')->pluck('filename')->toArray();
    $approved_rejected_claims = DB::table('main.cr3')->pluck('filename')->toArray();
    $pending_rejected_claims = DB::table('main.cr24')->pluck('filename')->toArray();
    $dfpi_archived = DB::table('main.cr26')->pluck('filename')->toArray();
    $dfpi = DB::table('main.cr4')->pluck('filename')->toArray();
    $dmas = DB::table('main.cr6')->pluck('filename')->toArray();
    $dmas_archived = DB::table('main.cr28')->pluck('filename')->toArray();
    $void_notification = DB::table('main.cr21')->pluck('filename')->toArray();

    // dg
    // $incomplete_documents = DB::table('main.cr10')->pluck('filename')->toArray();

    $result = array_merge($paid_claims, $approved_rejected_claims, $pending_rejected_claims, $dfpi_archived, $dfpi, $dmas, $dmas_archived, $void_notification );

    return $result;
}

public function CRfilesIntermidiateFollowUps(){

    $do = DB::table('main.cr5')->pluck('filename')->toArray();
    $do_archived = DB::table('main.cr27')->pluck('filename')->toArray();

    $result = array_merge($do, $do_archived);

    return $result;


}

public function CRfilesInFollowUps(){

    $validated_files = DB::table('main.cr8')->pluck('filename')->toArray();
    $unvalidated_files = DB::table('main.cr22')->pluck('filename')->toArray();
    $validated_files2 = DB::table('main.cr9')->pluck('filename')->toArray();
    $validated_files3 = DB::table('main.cr10')->pluck('filename')->toArray();
    $unvalidated_files2 = DB::table('main.cr11')->pluck('filename')->toArray();
    $unvalidated_files3 = DB::table('main.cr29')->pluck('filename')->toArray();
    $unvalidated_files4 = DB::table('main.cr23')->pluck('filename')->toArray();
    $unvalidated_files5 = DB::table('main.cr12')->pluck('filename')->toArray();
    $unvalidated_files6 = DB::table('main.cr13')->pluck('filename')->toArray();
    $unvalidated_files7 = DB::table('main.cr14')->pluck('filename')->toArray();

    $result = array_merge($validated_files, $unvalidated_files, $validated_files2, $validated_files3, $unvalidated_files2, $unvalidated_files3, $unvalidated_files4, $unvalidated_files5, $unvalidated_files6, $unvalidated_files7 );

    return $result;
}

public function followupEligibility($benefits){
    $eligible = 0;

    foreach ($benefits as $b) {
        $lvl = new AccrualNotificationReportRepository();
        $level = $lvl->checkWorkFlowLevel($b->notification_workflow_id);
        dump('level '.$level);

        if ($level == 1) {
            $eligible += 1;
            dump('**********************************************************************************************************');
        }
    }

    return $eligible;

}

public function saveFollowupClaims($notification_report){
   dump('in saving');
   $claim_followup = $this->query()->where('notification_report_id',$notification_report->id)->first();
   $pending_documents = $this->notification_report->checkAllMandatoryDocumentsProgressive($notification_report);
   dump($pending_documents->count());
   $data = [
    'user_id' => $notification_report->allocated,
    'notification_report_id' => $notification_report->id,
    'start_date' => Carbon::now(),
    'current_date' => null,
    'current_month' => Carbon::now()->format('Y-m-d'),
    'missing_documents' => $pending_documents->count(),
];

$data2 = [
    'user_id' => $notification_report->allocated,
    'current_date' => Carbon::now(),
    'current_month' => Carbon::now()->format('Y-m-d'),
    'missing_documents' => $pending_documents->count(),
];

if (is_null($claim_followup)) {
   dump('start create');
   $this->query()->create($data);

}else{
   dump('start update');
   $this->query()->where('notification_report_id',$notification_report->id)->update($data2);
}
dump('saved');
}

public function storeFollowUp($input){
    // dd($input);
    $followup = $this->query()->where('notification_report_id', $input['notification_report_id'])->first();
        // dd($followup);
    $this->query()->where('notification_report_id', $input['notification_report_id'])->increment('followup_count');
    $followup_cv = (new CodeValueRepository())->findByReference($input['follow_up_type_cv_id']);
    $input['claim_followup_id'] = $followup->id;
    $input['follow_up_type_cv_id'] = $followup_cv->id;

    unset($input['reminder_date']);
    // $input = $this->goUnset($input);

    if($input['district'] == null){
        // if ($input['district'] == null) {
        unset($input['district']);
        // }
    }else{
        unset($input['district']);
    } 

    if(isset($input['ward'])){
        if ($input['ward'] != null) {
            $postcode = DB::table('postcodes')->select('id')->where('postcode', $input['ward'])->first();
            if (!is_null($postcode)) {
                $input['postcode_id'] = $postcode->id;
            }
        }

        unset($input['ward']);
    }

    unset($input['action_type']);

        // dd($input);
    $this->claim_followup_update->storeFollowupUpdate($input);



}

public function getClaimFollowupDt($incident_id){

    $follow_up = $this->query()->where('notification_report_id', $incident_id)->first();

    if (is_null($follow_up)) {
        return [];
    }else{
        return $follow_up->ClaimFollowupUpdate()->select('*','claim_followup_updates.id as followup_id')->get();
    }

}

public function getMyFollowUps($user_id){
    return $this->query()->where('user_id', $user_id)->get();

}

public function mainMyFollowUps($input = null){

    $collection = array();
    if (isset($input['follow_up_date'])) {
        $max_date = $this->checkIfMaxDate($input);
        if (!$max_date) {
            return $collection;
        }
    }

    $current_month = Carbon::now()->format('Y-m-d');

    $staff_claims = $this->query()->select('claim_followups.user_id','notification_report_id',DB::raw("COUNT(claim_followups.notification_report_id) as total_notification"))->groupBy('claim_followups.user_id','claim_followups.notification_report_id')->where('current_month', $current_month)->get();

    if ($input['staff_id'] == -1) {
        $staff_claims = $this->query()->pluck("user_id as user_name");
        $array = $staff_claims->toArray();
    } else {
        $staff_claims = $this->query()->where('user_id', $input['staff_id'])->pluck("user_id as user_name");
        $array = $staff_claims->toArray();
    }

    if (count($array) < 1) {
        return $collection;
    }
    
    

    $values = array_count_values($array);
    $claim_followups = ClaimFollowupCalculation::where('current_month', $current_month)->get();

    $i = 0;
    $sn = 1;
    foreach ($values as $key => $value) {

        $collection[$i]['sn'] = $sn;
        $collection[$i]['user_name'] = $key;
        $collection[$i]['total_files'] = $this->getTotalFiles($key, $value)['total_files'];
        $collection[$i]['total_files_per_week'] = $this->getTotalFiles($key, $value)['week_division'];
        $collection[$i]['1st_week_updated'] = $this->getWeeklyCount($input, $key)['1st_week_updated'];
        $collection[$i]['1st_week_missed'] = $this->getWeeklyCount($input, $key)['1st_week_missed'];
        $collection[$i]['1st_week'] = $this->getPercentage($this->getWeeklyCount($input, $key)['1st_week'], $this->getWeeklyCount($input, $key)['1st_week_updated']);

        $collection[$i]['2nd_week_updated'] = $this->getWeeklyCount($input, $key)['2nd_week_updated'];
        $collection[$i]['2nd_week_missed'] = $this->getWeeklyCount($input, $key)['2nd_week_missed'];
        $collection[$i]['2nd_week'] = $this->getPercentage($this->getWeeklyCount($input, $key)['2nd_week'], $this->getWeeklyCount($input, $key)['2nd_week_updated']);

        $collection[$i]['3rd_week_updated'] = $this->getWeeklyCount($input, $key)['3rd_week_updated'];
        $collection[$i]['3rd_week_missed'] = $this->getWeeklyCount($input, $key)['3rd_week_missed'];
        $collection[$i]['3rd_week'] = $this->getPercentage($this->getWeeklyCount($input, $key)['3rd_week'], $this->getWeeklyCount($input, $key)['3rd_week_updated']);

        $collection[$i]['4th_week_updated'] = $this->getWeeklyCount($input, $key)['4th_week_updated'];
        $collection[$i]['4th_week_missed'] = $this->getWeeklyCount($input, $key)['4th_week_missed'];
        $collection[$i]['4th_week'] = $this->getPercentage($this->getWeeklyCount($input, $key)['4th_week'], $this->getWeeklyCount($input, $key)['4th_week_updated']);


        $collection[$i]['overall'] = ($collection[$i]['1st_week'] + $collection[$i]['2nd_week'] + $collection[$i]['3rd_week'] + $collection[$i]['4th_week']) / 4 ;
        $collection[$i]['overall'] = round($collection[$i]['overall'], 2);

        $i++;
        $sn++;
    }

    return $collection;

}

public function getTotalFiles($user_id){
    // if start of month take all files

    $files = array();
    // $total_files = 0;
    $current_month = Carbon::now()->format('Y-m-d');
    $week_no = $this->weekNo($current_month);
    $week_no = 4;

    $total = $this->query()->select(DB::raw("COUNT(claim_followups.notification_report_id) as first_week_total_files"),'review_status')->groupBy('claim_followups.user_id','review_status')->where('user_id', $user_id)->where(function ($query) {
        $query->where('review_status', '=', NULL)->orWhere('review_status', '=' ,0)->orWhere('review_status', '=' ,2)->orWhere('review_status', '=' ,3);
    })->get();

    // dump($total);

    $lqt = ClaimFollowupCalculation::where('user_id', $user_id)->where('current_month', $current_month)->first();

    $total_files = 0;

    foreach ($total as $t) {
        $total_files = $t->first_week_total_files;
        break;
    }

    switch ($week_no) {
        case 1:

        $files['week_division'] = $this->weekDivision($week_no, $total_files);
        $files['total_files'] = $total_files;
        $files['week_name'] = 'first_week_total_files';
        $files['current_week_name'] = 'first_week_current_files';
        $files['attended_files'] = !is_null($lqt) ? $lqt->first_week_attended_files : 0;
        $files['attended_files_name'] = 'first_week_attended_files';
        $files['missed_files'] = !is_null($lqt) ? $lqt->first_week_missed_files : 0;
        $files['missed_files_name'] = 'first_week_missed_files';
        $files['overall_files'] = !is_null($lqt) ? $lqt->first_week_overall_files : 0;
        $files['overall_files_name'] = 'first_week_overall_files';
        
        $data = [
            'last_quotient' => $files['week_division'],
        ];
        $this->query()->where('current_month',$current_month)->where('user_id', $user_id)->update($data);

        break;
        case 2:
// dump($total_files);
// dump($lqt->first_week_current_files);
        $total_files = !is_null($lqt) ? $total_files - $lqt->first_week_current_files : 0;
        // dump($total_files);
        // die;
        $files['total_files'] = $total_files;
        $files['week_name'] = 'second_week_total_files';
        $files['current_week_name'] = 'second_week_current_files';
        $files['attended_files'] = !is_null($lqt) ? $lqt->second_week_attended_files : 0;
        $files['attended_files_name'] = 'second_week_attended_files';
        $files['missed_files'] = !is_null($lqt) ? $lqt->second_week_missed_files : 0;
        $files['missed_files_name'] = 'second_week_missed_files';
        $files['overall_files'] = !is_null($lqt) ? $lqt->second_week_overall_files : 0;
        $files['overall_files_name'] = 'second_week_overall_files';

        $files['week_division'] = $this->weekDivision($week_no, $total_files);
        $data = [
            'last_quotient' => $files['week_division'],
        ];
        $this->query()->where('current_month',$current_month)->where('user_id', $user_id)->update($data);

        break;
        case 3:
        $diff = !is_null($lqt) ? $lqt->first_week_current_files + $lqt->second_week_current_files : 0;
        $total_files = $total_files - $diff;
        $files['week_name'] = 'third_week_total_files';
        $files['current_week_name'] = 'third_week_current_files';
        $files['attended_files'] = !is_null($lqt) ? $lqt->third_week_attended_files: 0;
        $files['attended_files_name'] = 'third_week_attended_files';
        $files['missed_files'] = !is_null($lqt) ? $lqt->third_week_missed_files : 0;
        $files['missed_files_name'] = 'third_week_missed_files';
        $files['overall_files'] = !is_null($lqt) ? $lqt->third_week_overall_files : 0;
        $files['overall_files_name'] = 'third_week_overall_files';

        $files['total_files'] = $total_files;
        $files['week_division'] = $this->weekDivision($week_no, $total_files);
        $data = [
            'last_quotient' => $files['week_division'],
        ];
        $this->query()->where('current_month',$current_month)->where('user_id', $user_id)->update($data);
        break;
        case 4:
        $diff = !is_null($lqt) ? $lqt->first_week_current_files + $lqt->second_week_current_files + $lqt->third_week_current_files : 0;
        $total_files = $total_files - $diff;

        $files['week_name'] = 'fourth_week_total_files';
        $files['current_week_name'] = 'fourth_week_current_files';

        $files['attended_files'] = !is_null($lqt) ? $lqt->fourth_week_attended_files : 0;
        $files['attended_files_name'] = 'fourth_week_attended_files';
        $files['missed_files'] = !is_null($lqt) ? $lqt->fourth_week_missed_files : 0;
        $files['missed_files_name'] = 'fourth_week_missed_files';
        $files['overall_files'] = !is_null($lqt) ? $lqt->fourth_week_overall_files : 0;
        $files['overall_files_name'] = 'fourth_week_overall_files';
        $files['total_files'] = $total_files;

        $files['week_division'] = $this->weekDivision($week_no, $total_files);
        $data = [
            'last_quotient' => $files['week_division'],
        ];
        $this->query()->where('current_month',$current_month)->where('user_id', $user_id)->update($data);
        break;

        default:
                        # code...
        break;
    }

    return $files;

}

public function getTotalFiles1($user_id, $total_files){
    // if start of month take all files

    $files = array();
    // $total_files = 0;
    $current_month = Carbon::now()->format('Y-m-d');
    $week_no = $this->weekNo($current_month);
    $week_no = 1;

    switch ($week_no) {
        case 1:
        
        $files['week_division'] = $this->weekDivision($week_no, $total_files);
        $files['total_files'] = $total_files;
        $data = [
            'last_quotient' => $files['week_division']['w1'],
        ];
        $this->query()->where('current_month',$current_month)->where('user_id', $user_id)->update($data);

        break;
        case 2:

        $lqt = $this->query()->select('last_quotient')->where('user_id', $user_id)->where('current_month', $current_month)->first();
        $total_files = $total_files - $lqt->last_quotient;
        $files['total_files'] = $total_files;

        $files['week_division'] = $this->weekDivision($week_no, $total_files);
        $data = [
            'last_quotient' => $files['week_division']['w1'],
        ];
        $this->query()->where('current_month',$current_month)->where('user_id', $user_id)->update($data);

        break;
        case 3:
        $lqt = $this->query()->select('last_quotient')->where('user_id', $user_id)->where('current_month', $current_month)->first();
        $total_files = $total_files - $lqt->last_quotient;

        $files['total_files'] = $total_files;
        $files['week_division'] = $this->weekDivision($week_no, $total_files);
        $data = [
            'last_quotient' => $files['week_division']['w1'],
        ];
        $this->query()->where('current_month',$current_month)->where('user_id', $user_id)->update($data);
        break;
        case 4:
        $lqt = $this->query()->select('last_quotient')->where('user_id', $user_id)->where('current_month', $current_month)->first();
        $total_files = $total_files - $lqt->last_quotient;
        $files['total_files'] = $total_files;

        $files['week_division'] = $this->weekDivision($week_no, $total_files);
        $data = [
            'last_quotient' => $files['week_division']['w1'],
        ];
        $this->query()->where('current_month',$current_month)->where('user_id', $user_id)->update($data);
        break;

        default:
                        # code...
        break;
    }

    return $files;

}

public function weekDivision($current_week, $total_files){
    $week_division = 0;

    $week_division = $total_files > 0 ? $total_files / (5 - $current_week) : 0;
    $week_devision = $this->is_decimal($week_division) ? round($week_division,2, PHP_ROUND_HALF_UP) : $week_division;
    if ($total_files == 1) {
        $week_division = 1;
    }
    return round($week_division,0);

}

function is_decimal( $val )
{
    return is_numeric( $val ) && floor( $val ) != $val;
}

public function checkIfMaxDate($input = null){
    $return = false;
    $date = Carbon::parse($input['follow_up_date']);
    $claim_date = $this->query()->select(DB::raw("MIN(claim_followups.start_date) as min_start_date"), DB::raw("MAX(claim_followups.start_date) as max_start_date"))->first();
    $max_date = Carbon::parse($claim_date->max_start_date)->endOfMonth();
    $min_date = Carbon::parse($claim_date->min_start_date)->startOfMonth();

    if($date->greaterThanOrEqualTo($min_date) and $date->lessThanOrEqualTo($max_date)){
        $return = true;
    }

    return $return;
}

public function getPercentage($total_files, $updated_files){
    $percent = 0;
    if($updated_files > 0){
        $percent = ($updated_files / $total_files) * 100;
        $percent = round($percent, 2);
    }

    return $percent;

}

public function getTotalCurrentFiles($input = null, $user_id){
    $total_files = 0;

    $days = $this->getFirstLastDayOfWeek($input);
    $FirstDay = $days[0];
    $LastDay = $days[1];

    if (isset($input['follow_up_date'])) {

        $days = $this->getCurrentDateWeek($input, $days);
        $FirstDay = $days[0];
        $LastDay = $days[1];
    }

    $claims = $this->query()->where('user_id',$user_id)->get();

    foreach ($claims as $c) {
        if(Carbon::parse($c->start_date)->format('Y-m-d') > Carbon::parse($FirstDay)->format('Y-m-d') and Carbon::parse($c->start_date)->format('Y-m-d') < Carbon::parse($LastDay)->format('Y-m-d')){
                // file in a current week
            $total_files += 1;
        }
    }

    return $total_files;


}

public function getCurrentDateWeek($input, $days){
    $array = explode('-', $input['follow_up_date']);
    $month = $array[1];

    $array1 = explode('-', $days[0]);
    $array1[1] = $month;
    $FirstDay = implode('-', $array1);

    $array2 = explode('-', $days[1]);
    $array2[1] = $month;
    $LastDay = implode('-', $array2);

    $days[0] = $FirstDay;
    $days[1] = $LastDay;

    return $days;
}

public function getWeeklyCount($input = null, $user_id){
    $total_files = array();
    $total_files['1st_week'] = 0;
    $total_files['2nd_week'] = 0;
    $total_files['3rd_week'] = 0;
    $total_files['4th_week'] = 0;

    $total_files['1st_week_updated'] = 0;
    $total_files['2nd_week_updated'] = 0;
    $total_files['3rd_week_updated'] = 0;
    $total_files['4th_week_updated'] = 0;

    $total_files['1st_week_missed'] = 0;
    $total_files['2nd_week_missed'] = 0;
    $total_files['3rd_week_missed'] = 0;
    $total_files['4th_week_missed'] = 0;



    $claims = $this->query()->where('user_id',$user_id)->get();

    $total_files = $this->weeklyCount($input, $total_files, $claims);

    return $total_files;


}

public function weeklyCount($input = null, $total_files, $claims){
   foreach ($claims as $c) {
    $date = $c->start_date;
    if (!isset($input['follow_up_date'])) {
      $is_current = $this->isCurrentMonth($date);
      if ($is_current) {
        $week_no = $this->weekNo($date);

        switch ($week_no) {
            case 1:
            $total_files['1st_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['1st_week_updated'] += 1;
            }else{
                $total_files['1st_week_missed'] += 1;
            }

            break;
            case 2:
            $total_files['2nd_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['2nd_week_updated'] += 1;
            }else{
                $total_files['2nd_week_missed'] += 1;
            }
            break;
            case 3:
            $total_files['3rd_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['3rd_week_updated'] += 1;
            }else{
                $total_files['3rd_week_missed'] += 1;
            }
            break;
            case 4:
            $total_files['4th_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['4th_week_updated'] += 1;
            }else{
                $total_files['4th_week_missed'] += 1;
            }
            break;

            default:
                        # code...
            break;
        }

    }
}else{

    $days = $this->getFirstLastDayOfWeek($input);
    $days = $this->getCurrentDateWeek($input, $days);
    $FirstDay = $days[0];
    $LastDay = $days[1];

    if(Carbon::parse($date)->format('Y-m-d') > Carbon::parse($FirstDay)->format('Y-m-d') and Carbon::parse($date)->format('Y-m-d') < Carbon::parse($LastDay)->format('Y-m-d')){
        $week_no = $this->weekNo($date);

        switch ($week_no) {
            case 1:
            $total_files['1st_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['1st_week_updated'] += 1;
            }

            break;
            case 2:
            $total_files['2nd_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['2nd_week_updated'] += 1;
            }
            break;
            case 3:
            $total_files['3rd_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['3rd_week_updated'] += 1;
            }
            break;
            case 4:
            $total_files['4th_week'] += 1;
            if ($c->followup_count > 0) {
                $total_files['4th_week_updated'] += 1;
            }
            break;

            default:
                            # code...
            break;
        }
    }

}

}

return $total_files;

}

public function weekNo($date){
    $timestamp = Carbon::parse($date)->format('Y-m-d H:i:s');
    $date = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, 'Africa/Dar_es_Salaam');
    $date->setTimezone('UTC');
    $week_no = $date->weekOfMonth;
    return $week_no;
}

public function getFirstLastDayOfWeek($input = null){

    //     $timestamp    = strtotime($input['follow_up_date']);
    //     $FirstDay = date('Y-m-01', $timestamp);
    //     $LastDay  = date('Y-m-t', $timestamp); 
    //     $dayofweek = date('w', strtotime($date));

    $FirstDay = date("Y-m-d", strtotime('sunday last week'));  
    $LastDay = date("Y-m-d", strtotime('sunday this week'));

    $days = array();

    $days[0] = $FirstDay;
    $days[1] = $LastDay;

    return $days;
}


public function isCurrentMonth($date){
    $return = false;

    $date = Carbon::parse($date)->format('m');
    $today = Carbon::now()->format('m');

    if ($date == $today) {
        $return = true;
    }

    return $return;
}

public function userFollowUps($user_id){

    return $this->query()->where('user_id', $user_id)->get();


}

public function exportToExcel($input)
{
    $date = isset($input) ? $input['follow_up_date'] : Carbon::now()->format('Y-m');
    $collection = $this->mainMyFollowUps($input);


    $results = array();
    foreach ($collection as $result) {
        $user = User::find($result['user_name']);
        $result['user_name'] = $user->name;
        $results[] = $result;
    }

    return Excel::create('claim_follow_up:'. $date, function($excel) use ($results) {
        $excel->sheet('mySheet', function($sheet) use ($results)
        {
            $sheet->fromArray((array)$results);
        });
    })->download('csv');

}

public function ifFollowUpDateExceeded($notification){
    $in_followup = $this->isInFollowUp($notification);
    $is_date_exceeded = false;
    if ($in_followup) {
        $followup = $this->query()->where('notification_report_id', $notification->id)->first();
        $is_date_exceeded = $this->isDateExceeded($followup->start_date);
    }
    return $is_date_exceeded;
}

public function isInFollowUp($notification){
    $return = false;
    $followup = $this->query()->where('notification_report_id', $notification->id)->first();
    if (!is_null($followup)) {
     $return = true;
 }
 return $return;
}

public function isDateExceeded($date){
    $exceeded = false;
    $week_no = $this->weekNo($date);
    $end_month = Carbon::now()->endOfMonth();
    $date = Carbon::parse($date);
    $exceeded = $end_month->greaterThanOrEqualTo(Carbon::parse($date)) ? false : true;

    return $exceeded;
}

public function updateReviewStatus($notification_report_id, $status){
    $this->query()->where('notification_report_id', (int)$notification_report_id)->update(['review_status' => (int)$status]);
}


}