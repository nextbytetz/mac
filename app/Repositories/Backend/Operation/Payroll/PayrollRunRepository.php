<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Jobs\RunPayroll\PayrollReconciliations;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\Pensioner;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollReconciliationRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PayrollRunRepository extends  BaseRepository
{

    const MODEL = PayrollRun::class;



    public function __construct()
    {

    }

//find or throwException for job title
    public function findOrThrowException($id)
    {
        $payroll_run = $this->query()->find($id);

        if (!is_null($payroll_run)) {
            return $payroll_run;
        }
        throw new GeneralException(trans('exceptions.backend.payroll.payroll_run_not_found'));
    }


    /*
     * create new
     */

    public function create($input) {
        $payroll_run = $this->query()->create($input);
        return $payroll_run;
    }


    /*
     * update
     */
    public function update($id,$input) {

    }

    /**
     * Undo Payroll runs all by approval id
     */
    public function undoAllRunsByApprovalId($payroll_run_approval_id)
    {
        $this->query()->where('payroll_run_approval_id', $payroll_run_approval_id)->delete();
    }

    /**
     * @param $start_date  i.e. Date of MMI (ACCIDENT, DISEASE)  or Death Date (DEATH)
     * @param $payroll_run_date (Payroll Payment date)
     * Months for accumulative payment for first pay beneficiary i.e. Accident / Disease => Date of MMI , Death => death date from Payroll run date
     */

    public function getFirstPayMonthDiff( $start_pay_date, $payroll_run_date )
    {
        $months_diff = $this->calculatePayMonthDiff($start_pay_date, $payroll_run_date);
        return $months_diff;
    }

    /**
     * Get Start Pay date for payroll - for System notification files
     */
    public function getStartPayDate(Model $notificationReport)
    {
        // Death
        if (in_array($notificationReport->incident_type_id , [3,4,5])) {
            $start_date = $notificationReport->incident_date;
        }else{
            // Disease and Accident
            $check_if_is_system_manual = $notificationReport->manualSystemFile()->count();
            if($check_if_is_system_manual == 0){
                /*Processed completely on the system*/
                $start_date = $notificationReport->claim->date_of_mmi;
            }else{
                /*Processed initial from the system but finished on manually*/
                $start_date = $notificationReport->manualSystemFile->date_of_mmi;
            }
        }
        return $start_date;
    }


    /**
     * Get Start Pay date for payroll - for manual files
     */
    public function getStartPayDateManualFile(Model $manualNotificationReport)
    {
        // Death
        if (in_array($manualNotificationReport->incident_type_id , [3,4,5])) {
            $start_date = $manualNotificationReport->incident_date;
        }else{
            // Disease and Accident
            $start_date = $manualNotificationReport->date_of_mmi;
        }
        return $start_date;
    }

    /**
     * Get Exit Paydate for payroll
     * Get last day of the month exited from payroll
     */
    public function getExitPayDate($member_type_id,$resource_id )
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        /*exit date if deactivated get exit date / if not deactivated get most recent payroll approved*/
        $exit_date =  ($resource->exit_date) ? Carbon::parse($resource->exit_date) : $this->getMostRecentApprovedRunDate();
        $exit_date_last_day = $exit_date;
        return $exit_date_last_day;
    }


    /**
     * @param $start_date
     * @param $end_date
     * Calculate Pay month diff using start date and end date for payroll
     */
    public function calculatePayMonthDiffOld($start_date, $end_date)
    {
        $start_date = Carbon::parse($start_date);
        $end_date = Carbon::parse($end_date);
        $days_diff = $start_date->diffInDays($end_date);
        $no_days_in_month = sysdefs()->data()->no_of_days_for_a_month_in_assessment;
        $months_diff = $days_diff / $no_days_in_month;
        return round($months_diff,2,1);
    }

    /**
     * @param $start_date
     * @param $end_date
     * @return float|int|string
     * Get first pay months for arrears (NEW Formula)
     * Only first months will be calculated for payable days.
     */
    public function calculatePayMonthDiff($start_date, $end_date)
    {
        $no_days_in_month = sysdefs()->data()->no_of_days_for_a_month_in_assessment;//30.3333
        $start_date_original = $start_date;// start date - keep the old format/value
        $start_date_original = Carbon::parse($start_date_original);
        $start_date = Carbon::parse($start_date);
        $start_date_last_day = $start_date->lastOfMonth();//Last day/date of start date i.e. MMI, death date
        $first_month_payable_days = $start_date_original->diffInDays($start_date_last_day) / $no_days_in_month;//Get payable days on first month
        $first_month_payable_days = round($first_month_payable_days,2,1);//payable days into month
        $end_date = Carbon::parse($end_date);
        $months_diff = months_diff($start_date_original, $end_date);
        $total_months = $first_month_payable_days + $months_diff;

        return $total_months;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $payroll_run_id
     * check if beneficiary has payroll run for specified payroll
     */
    public function checkIfBeneficiaryHasPayrollRun($member_type_id, $resource_id,$employee_id, $payroll_run_approval_id)
    {
        $payroll_run_count = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('payroll_run_approval_id', $payroll_run_approval_id)->where('employee_id',$employee_id)->count();
        return ($payroll_run_count > 0) ? true : false;
    }

    /*check if already paid atleast once*/
    public function checkIfBeneficiaryIsPaidAtLeastOnce($member_type_id, $resource_id,$employee_id)
    {
        $payroll_run_count = $this->getMpForDataTable()->where('payroll_run_approvals.wf_done', 1)->where('payroll_runs.member_type_id', $member_type_id)->where('payroll_runs.resource_id', $resource_id)->where('payroll_runs.employee_id',$employee_id)->count();
        return ($payroll_run_count > 0) ? true : false;
    }



    /**
     * @param $payroll_approval_run_id
     * Get total amounts for specified payroll i.e. net amount, arrears, deductions
     */
    public function getTotalAmountsForRunApproval($payroll_approval_run_id)
    {
        $query = $this->query()->where('payroll_run_approval_id', $payroll_approval_run_id);
        $total_net_amount = $query->sum('amount');
        $total_arrears_amount = $query->sum('arrears_amount');
        $total_deductions_amount = $query->sum('deductions_amount');

        return ['total_net_amount' => $total_net_amount, 'total_arrears_amount' => $total_arrears_amount, 'total_deductions_amount' => $total_deductions_amount];
    }


    /**
     * General retrieve monthly payroll for DataTable
     */
    public function getMpForDataTable()
    {
        $query = $this->query()->select([
            DB::raw("concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname) as dependent_name"),
            DB::raw("concat_ws(' ', pensioners.firstname, coalesce(pensioners.middlename, ''), pensioners.lastname) as pensioner_name"),
            DB::raw("payroll_runs.amount as amount"),
            DB::raw("payroll_runs.months_paid"),
            DB::raw("payroll_runs.monthly_pension"),
            DB::raw("payroll_runs.payroll_run_approval_id as payroll_run_approval_id"),
            DB::raw("payroll_run_approvals.wf_done as wf_done"),
            DB::raw("payroll_runs.arrears_amount as arrears_amount"),
            DB::raw("payroll_runs.deductions_amount as deductions_amount"),
            DB::raw("payroll_runs.unclaimed_amount as unclaimed_amount"),
            DB::raw("payroll_runs.employee_id as employee_id"),
            DB::raw("member_types.name as member_type_name"),
            DB::raw("payroll_procs.run_date as run_date"),
            DB::raw("bank_branches.name as bank_branch_name"),
            DB::raw("banks.name as bank_name"),
            DB::raw("payroll_runs.accountno as run_accountno"),
            DB::raw("payroll_runs.bank_branch_id as run_bank_branch_id"),
            DB::raw("payroll_runs.bank_id as run_bank_id"),
            DB::raw("payroll_runs.resource_id as resource_id"),
            DB::raw("payroll_runs.member_type_id as member_type_id"),
            DB::raw("payroll_runs.new_payee_flag as new_payee_flag"),
        ])
            ->leftjoin("pensioners", function($join){
                $join->on('payroll_runs.resource_id', 'pensioners.id')
                    ->where('payroll_runs.member_type_id', 5);
            })
            ->leftjoin("dependents", function($join){
                $join->on('payroll_runs.resource_id', 'dependents.id')
                    ->where('payroll_runs.member_type_id', 4);
            })
            ->join("payroll_run_approvals","payroll_runs.payroll_run_approval_id", "payroll_run_approvals.id")
            ->join("payroll_procs","payroll_procs.id", "payroll_run_approvals.payroll_proc_id")
            ->join("member_types","payroll_runs.member_type_id", "member_types.id")
            ->leftjoin('bank_branches', 'payroll_runs.bank_branch_id', 'bank_branches.id')
            ->leftjoin('banks', 'banks.id', 'payroll_runs.bank_id')
            ->whereNull('payroll_run_approvals.deleted_at');

        return $query;
    }

    /**
     * Get monthly pensions for all beneficiaries
     */
    public function getMpForDataTableWithFilename(){
        $runs =   DB::table('payroll_runs_view')->select(
            'resource_id',
            'employee_id',
            'member_type_name',
            'member_type_id',
            'filename',
            'run_date',
            'member_name',
            'employee_name',
            'memberno',
            'bank_name',
            'bank_branch_name',
            'run_accountno',
            'arrears_amount',
            'unclaimed_amount',
            'deductions_amount',
            'months_paid',
            'monthly_pension',
            'amount',
            'isconstantcare',
            'wf_done',
            'gender',
            'relationship'
        );

        return $runs;
    }


    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * Get new payees in payroll for datatable
     */
    public function getNewPayeesInPayrollForDataTable($member_type_id, $payroll_run_approval_id, $constant_care_flag = 0)
    {
        $payroll_runs = $this->getMpForDataTable()->where('payroll_runs.member_type_id', $member_type_id)->where('payroll_run_approval_id', $payroll_run_approval_id)->where('new_payee_flag', 1)->where('payroll_runs.isconstantcare', $constant_care_flag);
        return $payroll_runs;
    }

    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * Get general verification checklist for payroll approval
     */
    public function getQueryGeneralForPayrollChecklistByApproval($payroll_run_approval_id)
    {
        return $this->query()->select(
            DB::raw(" case WHEN payroll_runs.isconstantcare = 1 THEN 'Constant Care' ELSE m.name END as member_type_name"),
            'b.member_name',
            'b.filename',
            'b.relationship',
            'payroll_runs.arrears_amount',
            'payroll_runs.deductions_amount',
            'payroll_runs.unclaimed_amount',
            'payroll_runs.monthly_pension',
            'payroll_runs.amount',
            'payroll_runs.months_paid',
            'payroll_runs.member_type_id',
            'payroll_runs.resource_id',
            'payroll_runs.employee_id',
            'prv.bank_name',
            'prv.run_accountno'
        )
            ->join('member_types as m', 'm.id', 'payroll_runs.member_type_id')
            ->join('payroll_runs_view as prv', 'prv.run_id', 'payroll_runs.id')
            ->join('payroll_beneficiaries_view as b', function($join){
                $join->on('payroll_runs.resource_id', 'b.resource_id')->whereRaw("payroll_runs.member_type_id = b.member_type_id")->whereRaw("payroll_runs.employee_id = b.employee_id");
            })->where('payroll_runs.payroll_run_approval_id', $payroll_run_approval_id);
    }


    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * Get get Payee with arrears
     */
    public function getPayeeWithArrearsForDt($payroll_run_approval_id)
    {
        return $this->getQueryGeneralForPayrollChecklistByApproval($payroll_run_approval_id)->where('payroll_runs.arrears_amount' , '>', 0)->where('payroll_runs.new_payee_flag', 0);
    }

    /**
     * @param $payroll_run_approval_id
     * @return mixed
     * Get child overage with no extension
     */
    public function getChildOverageWithNoExtensionForDt($payroll_run_approval_id)
    {
        $payroll_run_approval = (new PayrollRunApprovalRepository())->find($payroll_run_approval_id);
        $run_date =$payroll_run_approval->payroll_month;
        $limit_date = Carbon::parse($run_date)->subYears(sysdefs()->data()->payroll_child_limit_age)->startOfMonth();
        return $this->getQueryGeneralForPayrollChecklistByApproval($payroll_run_approval_id)
            ->join('dependents', function($join){
                $join->on('payroll_runs.resource_id', 'dependents.id')->whereRaw("payroll_runs.member_type_id = 4");
            })
            ->where('b.dependent_type_id', 3)->where('b.dob' , '<', standard_date_format($limit_date))->where('dependents.isdisabled', 0)->where('dependents.iseducation', 0);
    }

    /**
     * @param $payroll_run_approval_id
     * @return mixed
     * Get other dependents finished cycles but ineligible
     */
    public function getOtherDependentsFinishedTheirCyclesStillOnPayrollDt($payroll_run_approval_id)
    {
        return $this->getQueryGeneralForPayrollChecklistByApproval($payroll_run_approval_id)
            ->join('dependent_employee', function($join){
                $join->on('payroll_runs.resource_id', 'dependent_employee.dependent_id')->whereRaw("payroll_runs.member_type_id = 4")->whereNotNull('dependent_employee.other_dependency_type');
            })->whereRaw("(select sum(pv.months_paid) from payroll_runs pv  where pv.deleted_at is null and pv.member_type_id = payroll_runs.member_type_id and pv.resource_id = payroll_runs.resource_id  and pv.employee_id = payroll_runs.employee_id) > ?" , [sysdefs()->data()->max_payable_months_otherdep_full]);
    }



    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * @return mixed
     * Terminated beneficiary before this payroll - but beneficiary was paid on previous payroll
     */
    public function getBeneficiaryDeactivatedBeforeThisPayroll($member_type_id, $payroll_run_approval_id, $constant_care_flag = 0){
        $payroll_run_approvals = new PayrollRunApprovalRepository();
        $pensioners = new PensionerRepository();
        $dependents = new DependentRepository();
        $previous_payroll_approval = $payroll_run_approvals->getPreviousPayrollApproval($payroll_run_approval_id);
        $previous_payroll_approval_id =  ($previous_payroll_approval)?  $previous_payroll_approval->id : null;
        if($member_type_id == 5){
            /*pensioners*/
            $terminated_beneficiaries = $pensioners->getForDataTable()->where('isactive', 2)->whereExists(function ($query) use($previous_payroll_approval_id, $member_type_id, $constant_care_flag) {
                $query->select(DB::raw(1))
                    ->from('payroll_runs')
                    ->where('payroll_runs.payroll_run_approval_id' ,'=', $previous_payroll_approval_id)->where('payroll_runs.member_type_id', $member_type_id)->whereRaw('payroll_runs.resource_id = pensioners.id')->where('payroll_runs.isconstantcare', $constant_care_flag)->whereNull('deleted_at');
            })->whereNotExists(function ($query) use($payroll_run_approval_id,$member_type_id, $constant_care_flag) {
                $query->select(DB::raw(1))
                    ->from('payroll_runs')
                    ->where('payroll_runs.payroll_run_approval_id' ,'=', $payroll_run_approval_id)->where('payroll_runs.member_type_id', $member_type_id)->whereRaw('payroll_runs.resource_id = pensioners.id')->where('payroll_runs.isconstantcare', $constant_care_flag)->whereNull('deleted_at');
            });
        }else{
            /*dependents*/
            $terminated_beneficiaries = $dependents->dependentAllWithPivot()->where('isactive', 2)->whereExists(function ($query) use($previous_payroll_approval_id, $member_type_id, $constant_care_flag) {
                $query->select(DB::raw(1))
                    ->from('payroll_runs')
                    ->where('payroll_runs.payroll_run_approval_id' ,'=', $previous_payroll_approval_id)->where('payroll_runs.member_type_id', $member_type_id)->whereRaw('payroll_runs.resource_id = dependents.id')->where('payroll_runs.isconstantcare', $constant_care_flag)->whereNull('deleted_at');
            })->whereNotExists(function ($query) use($payroll_run_approval_id,$member_type_id, $constant_care_flag) {
                $query->select(DB::raw(1))
                    ->from('payroll_runs')
                    ->where('payroll_runs.payroll_run_approval_id' ,'=', $payroll_run_approval_id)->where('payroll_runs.member_type_id', $member_type_id)->whereRaw('payroll_runs.resource_id = dependents.id')->where('payroll_runs.isconstantcare', $constant_care_flag)->whereNull('deleted_at');
            });
        }

        return $terminated_beneficiaries;
    }



    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * @return mixed
     * Suspended beneficiary before this payroll - but beneficiary was paid/active on previous payroll
     */
    public function getBeneficiarySuspendedBeforeThisPayroll($member_type_id, $payroll_run_approval_id, $constant_care_flag = 0){
        $payroll_run_approvals = new PayrollRunApprovalRepository();
        $pensioners = new PensionerRepository();
        $dependents = new DependentRepository();
        $previous_payroll_approval = $payroll_run_approvals->getPreviousPayrollApproval($payroll_run_approval_id);
        $previous_payroll_approval_id =  ($previous_payroll_approval) ?  $previous_payroll_approval->id : null;
        if($member_type_id == 5){
            /*pensioners*/
            $suspended_beneficiaries = $pensioners->getForDataTable()->whereExists(function ($query) use($payroll_run_approval_id,$member_type_id, $constant_care_flag) {
                $query->select(DB::raw(1))
                    ->from('payroll_suspended_runs')
                    ->where('payroll_suspended_runs.payroll_run_approval_id' ,'=', $payroll_run_approval_id)->where('payroll_suspended_runs.member_type_id', $member_type_id)->whereRaw('payroll_suspended_runs.resource_id = pensioners.id')->where('payroll_suspended_runs.isconstantcare', $constant_care_flag)->whereNull('deleted_at');
            })->whereExists(function ($query) use($previous_payroll_approval_id, $member_type_id,$constant_care_flag) {
                $query->select(DB::raw(1))
                    ->from('payroll_runs')
                    ->where('payroll_runs.payroll_run_approval_id' ,'=', $previous_payroll_approval_id)->where('payroll_runs.member_type_id', $member_type_id)->whereRaw('payroll_runs.resource_id = pensioners.id')->where('payroll_runs.isconstantcare', $constant_care_flag)->whereNull('deleted_at');
            });
        }else{
            /*dependents*/
            $suspended_beneficiaries = $dependents->dependentAllWithPivot()->whereExists(function ($query) use($payroll_run_approval_id, $member_type_id, $constant_care_flag) {
                $query->select(DB::raw(1))
                    ->from('payroll_suspended_runs')
                    ->where('payroll_suspended_runs.payroll_run_approval_id' ,'=', $payroll_run_approval_id)->where('payroll_suspended_runs.member_type_id', $member_type_id)->whereRaw('payroll_suspended_runs.resource_id = dependents.id')->where('payroll_suspended_runs.isconstantcare', $constant_care_flag)->whereNull('deleted_at');
            })->whereExists(function ($query) use($previous_payroll_approval_id, $member_type_id, $constant_care_flag) {
                $query->select(DB::raw(1))
                    ->from('payroll_runs')
                    ->where('payroll_runs.payroll_run_approval_id' ,'=', $previous_payroll_approval_id)->where('payroll_runs.member_type_id', $member_type_id)->whereRaw('payroll_runs.resource_id = dependents.id')->where('payroll_runs.isconstantcare', $constant_care_flag)->whereNull('deleted_at');
            });
        }

        return $suspended_beneficiaries;
    }

    /*Get beneficiaries with changed Bank details on this payroll*/
    public function getBeneficiariesWithChangedBankDetailsThisPayroll($payroll_run_approval_id){
        $payroll_run_approvals = new PayrollRunApprovalRepository();
        $previous_payroll_approval = $payroll_run_approvals->getPreviousPayrollApproval($payroll_run_approval_id);
        $previous_payroll_approval_id =  ($previous_payroll_approval)?  $previous_payroll_approval->id : null;

        $beneficiaries = DB::table('payroll_runs_view as pc')->select(
            'pc.resource_id',
            'pc.member_type_id',
            'pc.member_type_name',
            'pc.member_name',
            DB::raw("concat_ws(' - ',pc.bank_name, pc.run_accountno) as bank_details_current"),
            DB::raw("concat_ws(' - ',pbank.name, pp.accountno) as bank_details_previous"),
            'pc.relationship',
            'pc.employee_id'
        )->join('payroll_runs as pp', function($join)use($previous_payroll_approval_id){
            $join->on('pp.resource_id', 'pc.resource_id')->whereRaw("pp.member_type_id = pc.member_type_id");
        })->leftJoin('banks as pbank', 'pbank.id', 'pp.bank_id')

            ->where('pp.payroll_run_approval_id',$previous_payroll_approval_id)->where('pc.payroll_run_approval_id',$payroll_run_approval_id)
            ->whereRaw("(pc.run_bank_id <> pp.bank_id  or pc.run_accountno <> pp.accountno )");

        return $beneficiaries;
    }

    /**
     * Get data summary for payroll run approval
     */
    public function getPayrollRunApprovalDataSummary($payroll_run_approval_id)
    {
        $new_pensioners = $this->getNewPayeesInPayrollForDataTable(5, $payroll_run_approval_id)->count();
        $new_dependents = $this->getNewPayeesInPayrollForDataTable(4, $payroll_run_approval_id)->count();
        $new_constant_cares = $this->getNewPayeesInPayrollForDataTable(4, $payroll_run_approval_id,1)->count();
        $suspended_pensioners = $this->getBeneficiarySuspendedBeforeThisPayroll(5, $payroll_run_approval_id)->count();
        $suspended_dependents = $this->getBeneficiarySuspendedBeforeThisPayroll(4, $payroll_run_approval_id)->count();
        $suspended_constant_cares = $this->getBeneficiarySuspendedBeforeThisPayroll(4, $payroll_run_approval_id,1)->count();
        $terminated_pensioners = $this->getBeneficiaryDeactivatedBeforeThisPayroll(5, $payroll_run_approval_id)->count();
        $terminated_dependents = $this->getBeneficiaryDeactivatedBeforeThisPayroll(4, $payroll_run_approval_id)->count();
        $terminated_constant_cares = $this->getBeneficiaryDeactivatedBeforeThisPayroll(4, $payroll_run_approval_id,1)->count();
        $members_for_erp_supplier_id = $this->getBeneficiariesWithNoErpSupplierIdForDataTable($payroll_run_approval_id);
        $member_with_change_bank_details = $this->getBeneficiariesWithChangedBankDetailsThisPayroll($payroll_run_approval_id)->count();
        $member_with_arrears = $this->getPayeeWithArrearsForDt($payroll_run_approval_id)->count();
        $children_overage_no_extension = 0 ?? $this->getChildOverageWithNoExtensionForDt($payroll_run_approval_id)->count();
//        $otherdependents_not_eligible = $this->getOtherDependentsFinishedTheirCyclesStillOnPayrollDt($payroll_run_approval_id)->count();
        return [
            'new_pensioners' => $new_pensioners,
            'new_dependents'=> $new_dependents,
            'suspended_pensioners' => $suspended_pensioners,
            'suspended_dependents'=> $suspended_dependents,
            'terminated_pensioners' => $terminated_pensioners,
            'terminated_dependents'=> $terminated_dependents,
            'new_constant_cares'=> $new_constant_cares,
            'suspended_constant_cares' => $suspended_constant_cares,
            'terminated_constant_cares'=> $terminated_constant_cares,
            'members_for_erp_supplier_id' => $members_for_erp_supplier_id,
            'member_with_change_bank_details' => $member_with_change_bank_details,
            'member_with_arrears' => $member_with_arrears,
            'children_overage_no_extension' => $children_overage_no_extension
//            'otherdependents_not_eligible' => $otherdependents_not_eligible
        ];
    }

    /**
     * @param $id
     * @param $bank_id
     * @return mixed
     * Get payee by bank per payroll approval
     */
    public function getPayeesByBankInPayrollRunForDt($payroll_run_approval_id, $bank_id){
        $payees = $this->getQueryGeneralForPayrollChecklistByApproval($payroll_run_approval_id)->where('payroll_runs.bank_id', $bank_id);
        return $payees;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get first payroll for member per notification report
     */
    public function getFirstPayrollForMember($member_type_id, $resource_id, $employee_id)
    {
        $run_approvals = new PayrollRunApprovalRepository();
        $payroll_suspended_runs = new PayrollSuspendedRunRepository();
        $first_payroll_run = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id)->whereHas('payrollRunApproval', function ($query){
            $query->where('wf_done',1);
        })->orderBy('id', 'asc')->first();
        $first_payroll_suspended_run = $payroll_suspended_runs->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id)->whereHas('payrollRunApproval', function ($query){
            $query->where('wf_done',1);
        })->orderBy('id', 'asc')->first();

        /*Has both run and suspended*/
        if($first_payroll_run && $first_payroll_suspended_run){

            $first_payroll_run_date = Carbon::parse($first_payroll_run->created_at);
            $first_payroll_suspended_run_date = Carbon::parse($first_payroll_suspended_run->created_at);
            $first_run_payroll = ($first_payroll_run_date < $first_payroll_suspended_run_date) ?
                $first_payroll_run : $first_payroll_suspended_run;
        }elseif($first_payroll_run && !$first_payroll_suspended_run){
            /*Has only run*/
            $first_run_payroll = $first_payroll_run;
        }elseif(!$first_payroll_run && $first_payroll_suspended_run){
            /*has only suspended*/
            $first_run_payroll = $first_payroll_suspended_run;
        }

        return $first_run_payroll;
    }

    /**
     * Get the most recent payroll run date (payroll month) which is approved
     */
    public function getMostRecentApprovedRunDate()
    {
        $run_approvals = new PayrollRunApprovalRepository();
        $recent_run_approval = $run_approvals->query()->where('wf_done', 1)->orderBy('id', 'desc')->first();
        if($recent_run_approval){
            $recent_payproc = $recent_run_approval->payrollProc;
            $run_date = ($recent_payproc->run_date) ? $recent_payproc->run_date : Carbon::now();
        }else{
            $run_date = Carbon::now();
        }
        return $run_date;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * MP Balance for member per notification report
     */
    public function getMpBalanceForMember($member_type_id, $resource_id,$employee_id )
    {
        $end_date = $this->getExitPayDate($member_type_id, $resource_id);
        $total_eligible_payable = $this->findTotalEligiblePensionForMember($member_type_id, $resource_id,$employee_id) ;
        $total_pension_paid = $this->getTotalPensionPaidForMember($member_type_id, $resource_id, $employee_id);
        $pending_net_recovery = $this->getNetPendingRecoveryAmountForMember($member_type_id, $resource_id,$employee_id);
        $balance = ($total_pension_paid + $pending_net_recovery) - $total_eligible_payable ;
        return $balance;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get total mp amount already run paid and processed per notification report
     */
    public function getTotalApprovedPaidMpAmountForMember($member_type_id, $resource_id,$employee_id)
    {
        /*Total paid and processed*/
        $total_run_amount = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id)->whereHas('payrollRunApproval', function($query){
            $query->where('wf_done', 1);
        })->sum('amount');

        return $total_run_amount;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get Total Pension paid to beneficiary per notification report including processed and suspended amount
     */
    public function getTotalPensionPaidForMember($member_type_id, $resource_id,$employee_id)
    {
        $payroll_suspended_runs = new PayrollSuspendedRunRepository();
        /*Total paid and processed*/
        $total_run_amount = $this->getTotalApprovedPaidMpAmountForMember($member_type_id, $resource_id,$employee_id);
        /*total suspended processed*/
        $total_suspended_amount = $payroll_suspended_runs->getTotalSuspendedAmountForMember($member_type_id, $resource_id, $employee_id);
        $total_amount = $total_run_amount + $total_suspended_amount;
        return $total_amount;
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get total eligible payable pension amount for beneficiary per notification report
     */
    public function findTotalEligiblePensionForMember($member_type_id, $resource_id,$employee_id, $exit_date = null)
    {
        $dependents = new DependentRepository();
        $payrolls = new PayrollRepository();
        $notification_reports = new NotificationReportRepository();
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        $notification_report_id = ($member_type_id == 5) ? $resource->notification_report_id : $resource->getDependentEmployee($employee_id)->notification_report_id;

        $notification_report = $notification_reports->find($notification_report_id);
        $mp = 0;
        if($member_type_id == 4){
            $mp = $dependents->getMonthlyPensionPerNotification($resource_id, $employee_id);
        }elseif($member_type_id == 5){
            $mp = $resource->monthly_pension_amount;
        }
        /*Get months eligible  start--*/
        $start_date = $this->getStartPayDate($notification_report);
        $end_date = ($exit_date == null) ? $this->getExitPayDate($member_type_id, $resource_id) : (Carbon::parse($exit_date)->lastOfMonth());
        $end_date = Carbon::parse($end_date)->lastOfMonth();
        $first_payroll_run = $this->getFirstPayrollForMember($member_type_id, $resource_id,$employee_id);
        $first_payroll_run_date = $first_payroll_run->payrollRunApproval->payrollProc->run_date;
        $first_pay_months = $this->calculatePayMonthDiff($start_date, $first_payroll_run_date); //  months from active date to first payroll run date
        $months_after_first_Payroll = months_diff($first_payroll_run_date, $end_date);// total months after the first payroll to exit date
        $total_months = $first_pay_months + $months_after_first_Payroll;
        $total_months = round($total_months,2,1);
        /*end get months eligible*/
        $total_mp_eligible = $total_months * $mp;
        return  round($total_mp_eligible, 2, 1);

    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get net pending recovery amount for member per notification report
     */
    public function getNetPendingRecoveryAmountForMember($member_type_id, $resource_id,$employee_id)
    {
        $payroll_arrears = new PayrollArrearRepository();
        $payroll_deductions = new PayrollDeductionRepository();
        $payroll_unclaims = new PayrollUnclaimRepository();

        $pending_arrears_amount = $payroll_arrears->getNetPendingArrearsAmountForMember($member_type_id, $resource_id,$employee_id) ;
        $pending_deduction_amount = $payroll_deductions->getNetPendingDeductionAmountForMember($member_type_id, $resource_id,$employee_id) ;
//        $payroll_unclaims_amount = $payroll_unclaims->getNetPendingUnclaimsAmountForMember($member_type_id, $resource_id,$employee_id) ;

        $net_pending_recovery = ($pending_arrears_amount)  - $pending_deduction_amount;
        return $net_pending_recovery;
    }





    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get Monthly pension payments summary for member per notification report.
     */
    public function getMpPaymentSummaryForMember($member_type_id, $resource_id,$employee_id)
    {
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        $payroll_suspended_runs = new PayrollSuspendedRunRepository();
        $mp_processed_paid = $this->getTotalApprovedPaidMpAmountForMember($member_type_id, $resource_id,$employee_id);
        $suspended_mp = $payroll_suspended_runs->getTotalSuspendedAmountForMember($member_type_id, $resource_id,$employee_id);
//        $most_recent_run_date = $this->getMostRecentApprovedRunDate();
        $pending_net_recovery = $this->getNetPendingRecoveryAmountForMember($member_type_id, $resource_id,$employee_id);
        $balance = $this->getMpBalanceForMember($member_type_id, $resource_id,$employee_id);
        $total_mp_eligible = $this->findTotalEligiblePensionForMember($member_type_id, $resource_id,$employee_id);
        $mp_paid = $this->getTotalPensionPaidForMember($member_type_id, $resource_id,$employee_id);
        return ['total_mp_approved_paid' => $mp_processed_paid, 'total_suspended_amount' => $suspended_mp, 'total_mp_paid' => $mp_paid,  'mp_balance' => $balance, 'total_mp_eligible' => $total_mp_eligible , 'total_pending_recovery_amount' => $pending_net_recovery];
    }


    /**
     * @param $payroll_run_approval_id
     * @return bool
     * Check if payroll runs of certain payroll is exported to erp.
     */
    public function checkIfAllPensionsAreExportedToErp($payroll_run_approval_id){
        $payroll_runs = $this->query()->where('payroll_run_approval_id', $payroll_run_approval_id)->where('isexported', 0)->count();
        if($payroll_runs > 0){
            return false;
        }else{
            return true;
        }
    }


    /**
     * @param $id
     *Get beneficiaries with no supplier id for erp
     */
    public function getBeneficiariesWithNoErpSupplierIdForDataTable($payroll_run_approval_id)
    {
        $beneficiaries = $this->query()->select('member_name',
            DB::raw(" CASE WHEN payroll_runs.member_type_id = 5 THEN 'Pensioners' ELSE 'Dependent' END as member_type")
        )
            ->join('payroll_beneficiaries_view', function($join){
                $join->on('payroll_beneficiaries_view.member_type_id', 'payroll_runs.member_type_id')
                    ->on('payroll_beneficiaries_view.resource_id', 'payroll_runs.resource_id');
            })
            ->where('payroll_run_approval_id', $payroll_run_approval_id)
            ->where(function ($query){
                $query->whereNotIn('payroll_runs.bank_id', [3,4])->orWhereNull('payroll_runs.bank_id');
            })->whereRaw("(select count(1) from erp_suppliers e1 where e1.supplier_id  = payroll_beneficiaries_view.memberno) = 0  ");

        return $beneficiaries;
    }


    /*Get Payees with Arrears*/
    public function getPayeesWithArrearsByMemberType($payroll_run_approval_id, $member_type_id)
    {
        return $this->query()->where('payroll_run_approval_id', $payroll_run_approval_id)->where('member_type_id', $member_type_id)->whereRaw("(arrears_amount > 0 or months_paid > 0)")->get();
    }

    /**
     * Get most recent active payroll run
     */
    public function getMostRecentPayrollRunByMember($member_type_id, $resource_id, $employee_id)
    {
        return $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id)->orderByDesc('payroll_run_approval_id')->first();
    }


    /**
     * @return mixed
     * Get Payroll run total summary
     */
    public function getQueryPayrollRunTotalSummary()
    {
        return   DB::table('payroll_runs_view')->select(
            DB::raw("sum(amount) as net_amount"),
            DB::raw("sum(arrears_amount) as arrears_amount"),
            DB::raw("sum(deductions_amount) as deductions_amount"),
            DB::raw("sum(unclaimed_amount) as unclaimed_amount"),
            DB::raw("count(distinct resource_id) as no_of_payees")
        );
    }
}