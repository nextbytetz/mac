<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Jobs\RunPayroll\PostPayrollAlertTaskChecker;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollRetireeSuspensionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollVerificationRepository extends  BaseRepository
{

    const MODEL = PayrollVerification::class;

    protected $payroll_status_changes;

    public function __construct()
    {
        $this->payroll_status_changes = new PayrollStatusChangeRepository();

    }

    /*Get no of days for verification to be eligible for suspension*/
    public function getDaysForVerification()
    {
        return    sysdefs()->data()->pension_verification_limit_days;
    }

    /*Get no of days for verification alerts for internal use on alert monitor*/
    public function getDaysForVerificationAlert()
    {
        return 182;
    }
    /*
     * create new
     * @param $resource = benecifiary
     */

    public function create(array $input, Model $resource)
    {
        return DB::transaction(function () use ($input, $resource) {
            $payroll_verification = $this->query()->create([
                'member_type_id' => $input['member_type_id'],
                'resource_id' => $input['resource_id'],
                'iseducation' => isset($input['iseducation']) ? $input['iseducation'] : 0,
                'isdisabled' => isset($input['isdisabled']) ? $input['isdisabled'] : 0,
                'folionumber' => $input['folionumber'],
                'user_id' => access()->id(),
                'verification_date' => $input['verification_date'],
                'region_id' => $input['region_id'],
                'district_id' => $input['district_id'],
                'phone' => $input['phone'],
                'next_kin_phone' => $input['next_kin_phone'],
                'payroll_status_change_id' => isset($input['payroll_status_change_id']) ? $input['payroll_status_change_id'] : null,
            ]);

            /*Status change - Close payroll alert checker <payroll_inbox_task>*/
            $stage_cv_ref ='PALEPEDVER';
            dispatch(new PostPayrollAlertTaskChecker($stage_cv_ref, $input['member_type_id'], $input['resource_id'],1));


            return $payroll_verification;
        });
    }

    /*
 * update
 */
    public function update($payroll_verification,$input) {
        return DB::transaction(function () use ($payroll_verification, $input) {

            $payroll_verification->update([
                'iseducation' => isset($input['iseducation']) ? $input['iseducation'] : 0,
                'folionumber' => $input['folionumber'],
                'verification_date' => $input['verification_date'],
                'region_id' => $input['region_id'],
                'district_id' => $input['district_id'],
                'phone' => $input['phone'],
                'next_kin_phone' => $input['next_kin_phone'],
            ]);

            /*Update status change remark*/
            $payroll_verification->payrollStatusChange->update([
                'remark' => isset( $input['remark']) ?  $input['remark'] : 'Verification'
            ]);
            return $payroll_verification;
        });
    }


    /*Verify Beneficiary*/
    public function reinstateSuspendedBeneficiary($resource)
    {
        DB::transaction(function () use ($resource) {

            /*Check if beneficiary is suspended*/
            if($resource->suspense_flag == 1){
                /*If beneficiary is suspended - Reinstate*/
                $this->payroll_status_changes->reinstate($resource);
            }

        });
    }


    /*Verify Beneficiary on Approval*/
    public function verifyBeneficiaryOnApproval($resource, $verification)
    {
        DB::transaction(function () use ($resource, $verification) {
            $status_change = $verification->payrollStatusChange;
            $member_type_id = $status_change->member_type_id;
            $resource_id = $status_change->resource_id;
            /*Verify*/
            $this->payroll_status_changes->verify($resource, $verification->verification_date);

            /*General info for all member types*/
            $resource->update([
                'phone' => $verification->phone,
                'next_kin_phone' => $verification->next_kin_phone,
                'region_id' => $verification->region_id,
                'district_id' => $verification->district_id,
            ]);

            $check_if_can_reinstate = true;
            /*Info depend on member type*/
            switch($verification->member_type_id)
            {
                case 4:
                    /*dependent*/
                    $resource->update([
//                        'iseducation' => $verification->iseducation,
                    ]);

                    /*check if can be reinstate*/
                    $check_if_can_reinstate =$resource->getCheckIfAgeIsEligible($status_change->employee_id);

                    break;

                case 5:
                    /*For pensioner - sync info with employee*/
                    $resource->employee->update([
                        'phone' => $verification->phone,
                        'next_of_kin_phone' => $verification->next_kin_phone,
                        'region_id' => $verification->region_id,
                        'district_id' => $verification->district_id,
                    ]);
                    /*Update suspension alerts if any*/
                    (new PayrollRetireeSuspensionRepository())->updateStatus($resource->id);
                    break;
            }

            /*If can be reinstate - Reinstate if suspended*/
            if($check_if_can_reinstate){
//                $this->reinstateSuspendedBeneficiary($resource);
            }

            /*update document status*/
            $document_type = 63;
            $this->updateDocumentStatus($member_type_id, $resource_id, $document_type);
        });

    }





    /**
     * @param $input
     * Update child flags i.e. iseducation
     */
    public function updateChildFlags(Model $dependent_child, $input)
    {
        $iseducation = $input['iseducation'];
        $dependent_child->update(['iseducation' => $iseducation]);
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $document_type
     * Update beneficiary document statsus
     */
    public function updateDocumentStatus($member_type_id, $resource_id, $document_type)
    {
        /*update pending*/
        $payrolls = new PayrollRepository();
        $payrolls->updateBeneficiaryDocumentPendingStatusPerType($member_type_id, $resource_id, $document_type);
        /*update isused status*/
        $payrolls->updateBeneficiaryDocumentUsedPerType($member_type_id, $resource_id, $document_type);
    }





    /**
     *Check if beneficiary verification can be initiated
     */
    public function checkIfBeneficiaryVerificationCanBeInitiated($member_type_id, $employee_id,Model $resource)
    {
        $payroll_status_change_repo = new PayrollStatusChangeRepository();
        $payrolls = new PayrollRepository();
        /*For suspended beneficiary*/
//        if($resource->suspense_flag == 1){
//            /*Check if reinstated can be done*/
//            $payroll_status_change_repo->checkIfCanInitiateNewEntry($member_type_id, $resource->id, $employee_id, 'PSCREIN');
//        }

        /*check if there pending verification*/
        $document_type_id = 63;
        $payrolls->checkIfThereIsDocumentForAction($member_type_id, $resource->id, $document_type_id);
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * @return mixed
     * Get verification history of beneficiary
     */
    public function getVerificationsByBeneficiaryForDataTable($member_type_id, $resource_id){
        return $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id);
    }


    /**
     * Reset documents for verification
     */
    public function resetDocumentStatusForAllInitiatedVerification()
    {
        $status_change_cv_id = (new CodeValueRepository())->findIdByReference('PSCVERIF');
        $verification_status_change_on_progress = (new PayrollStatusChangeRepository())->query()->where('wf_done', 0)->where('status_change_cv_id', $status_change_cv_id)->get();
        foreach ($verification_status_change_on_progress as $status_change){
            $member_type_id = $status_change->member_type_id;
            $resource_id = $status_change->resource_id;
            $document_type = 63;

            (new PayrollRepository())->updateBeneficiaryDocumentPendingStatusPerType($member_type_id, $resource_id, $document_type);
        }
    }


    /**
     * @return mixed
     * Get verifications distinct for all members
     */
    public function getQueryVerificationAllDistinct()
    {
      return  DB::table('payroll_beneficiaries_view as b')->select(
            DB::raw("max(b.filename) as filename"),
            DB::raw("max(b.member_name) as member_name"),
            DB::raw("max(b.employer_name) as employer_name"),
            DB::raw("max(b.employee_name) as employee_name"),
            DB::raw("max(districts.name) as district"),
            DB::raw("max(regions.name) as region"),
            DB::raw("max(b.member_phone) as phone"),
            DB::raw("max(b.monthly_pension_amount) as monthly_pension_amount "),
            DB::raw("max(banks.name) as bank_name "),
            DB::raw("max(b.accountno) as accountno "),
            DB::raw("max(b.lastresponse) as lastresponse "),
            DB::raw("max((b.lastresponse + INTERVAL '6 month')::date) as next_verification_date "),
//            DB::raw("max((age((now()::timestamp), (b.lastresponse + INTERVAL '6 month')::timestamp))) as days_left "),
            DB::raw("max(DATE_PART('day', (b.lastresponse + INTERVAL '6 month')::timestamp - now()::date)) as days_left "),// TODO 6 Month coded here
            DB::raw("max(b.relationship) as relationship "),
            'b.member_type_id',
            'b.resource_id',
            DB::raw("max(b.pivot_id) as pivot_id "),
            DB::raw("max(b.employee_id) as employee_id "),
            DB::raw("max(b.dob) as dob"),
            DB::raw("max(v.iseducation) as iseducation")
        )
            ->leftJoin('banks', 'banks.id', 'b.bank_id')
            ->leftJoin('bank_branches', 'bank_branches.id', 'b.bank_branch_id')
            ->leftJoin('regions', 'regions.id', 'b.member_region_id')
            ->leftJoin('districts', 'districts.id', 'b.member_district_id')
            ->leftJoin('payroll_verifications as v', function($join){
                $join->on('v.resource_id', 'b.resource_id')->on('v.member_type_id', 'b.member_type_id');
            })->groupBy('b.resource_id', 'b.member_type_id')
            ->whereRaw(" b.isactive  in (1)");
//            ->whereRaw(" b.isactive not in (0)");
//            ->whereRaw(" b.isactive not in (0,2,3)");
    }

}