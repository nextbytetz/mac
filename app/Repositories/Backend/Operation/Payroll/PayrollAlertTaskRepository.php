<?php

/**
 * Implementation for new task alert
 */

//1. Add staging on cv and then include on initializeAlertTaskForAll and Flag input
//2. On The blade and div for that table
//3. Gor to the retrieving query and check if isinbox_task to select only for that user
//4. Go and append saveNewTask and closeTask on respective functions on that activity

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\PayrollAlertTask;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Models\Sysdef\Code;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollChildSuspensionRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollRetireeSuspensionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollAlertTaskRepository extends  BaseRepository
{

    const MODEL = PayrollAlertTask::class;

    /*Initialize checker for all*/
    public function initializeAlertTaskForAll()
    {
        $cv_repo = new CodeValueRepository();
        $pensioner_repo = new PensionerRepository();
        $dependent_repo = new DependentRepository();
        $payroll_repo = new PayrollRepository();
        $staging_cvs = $cv_repo->query()->where('is_active', 1)->where('code_id', 64)->get();
        $member_type_id = null;
        $member_ids = [];
        //TODO need to reset temporary
//        $this->resetHardPendingCheckers();
        foreach ($staging_cvs as $staging){
            $stage_cv_ref = $staging->reference;
            $stage_cv_id = $staging->id;
            $member_ids = [];
            $already_saved = 0 ;

            $flag_input = [
                'notify' => 0,
                'priority' => 0,
                'shouldcount' => 1,
                'resource_pivot_id' => null,
            ];
            /*Staging*/
            switch($stage_cv_ref){

//                case 'PALEPENAC': //Pensioners for activation
//                    $member_type_id = 5;
//                    $member_ids = $pensioner_repo->getReadyForPayrollForDataTable()->select('pensioners.id as id')->get();
//
//                    break;
//
//                case 'PALEDEPAC': //Dependents for activation
//                    $member_type_id = 4;
//                    $member_ids = $dependent_repo->getReadyForPayrollForDataTable()->select('dependents.id as id')->get();
//
//                    break;

//                case 'PALECHSALR': //child suspension alerts
//                    $member_type_id = 4;
//                    $flag_input['shouldcount'] = 0;
//                    $member_ids =(new PayrollChildSuspensionRepository())->getSuspensionPendingAction()->select('dependents.id as id')->get();
//
//                    break;
//
//                case 'PALECHA18': //child Approaching 18yrs
//                    $member_type_id = 4;
//                    $flag_input['shouldcount'] = 0;
//                    $member_ids =(new PayrollChildSuspensionRepository())->getChildLimitAgeAlertsForDataTable()->select('dependents.id as id')->get();
//
//                    break;

//                case 'PALERETSU': //Retiree Suspensions
//                    $member_type_id = 5;
//                    $member_ids =(new PayrollRetireeSuspensionRepository())->getPendingSuspensionForAction()->select('pensioners.id as id')->get();
//
//                    break;

                case 'PALEPEDVER': //Pending documents verification
                    /*For Dependents*/
                    $member_type_id = 4;
                    $member_ids =$payroll_repo->getPendingDocsForBeneficiaryUpdateDataTable(63)->where('document_payroll_beneficiary.member_type_id', 4)->select('document_payroll_beneficiary.resource_id as id', 'document_payroll_beneficiary.id as pivot_id')->get();


                    $this->savePayrollAlertTaskForByMemberType($stage_cv_ref, $member_type_id, $member_ids);

                    /*For pensioners*/
                    $member_type_id = 5;
                    $member_ids =$payroll_repo->getPendingDocsForBeneficiaryUpdateDataTable(63)->where('document_payroll_beneficiary.member_type_id', 5)->select('document_payroll_beneficiary.resource_id as id', 'document_payroll_beneficiary.id as pivot_id')->get();
                    $this->savePayrollAlertTaskForByMemberType($stage_cv_ref, $member_type_id, $member_ids);

                    $already_saved = 1;
                    break;

                case 'PALEPEDBAN': //Pending documents bank
                    /*For Dependents*/
                    $member_type_id = 4;
                    $member_ids =$payroll_repo->getPendingDocsForBeneficiaryUpdateDataTable(64)->where('document_payroll_beneficiary.member_type_id', 4)->select('document_payroll_beneficiary.resource_id as id', 'document_payroll_beneficiary.id as pivot_id')->get();
                    $this->savePayrollAlertTaskForByMemberType($stage_cv_ref, $member_type_id, $member_ids);

                    /*For pensioners*/
                    $member_type_id = 5;
                    $member_ids =$payroll_repo->getPendingDocsForBeneficiaryUpdateDataTable(64)->where('document_payroll_beneficiary.member_type_id', 5)->select('document_payroll_beneficiary.resource_id as id', 'document_payroll_beneficiary.id as pivot_id')->get();
                    $this->savePayrollAlertTaskForByMemberType($stage_cv_ref, $member_type_id, $member_ids) ;

                    $already_saved = 1;
                    break;
                case 'PALEPEDDET': //Pending documents details
                    /*For Dependents*/
                    $member_type_id = 4;
                    $member_ids =$payroll_repo->getPendingDocsForBeneficiaryUpdateDataTable(65)->where('document_payroll_beneficiary.member_type_id', 4)->select('document_payroll_beneficiary.resource_id as id', 'document_payroll_beneficiary.id as pivot_id')->get();
                    $this->savePayrollAlertTaskForByMemberType($stage_cv_ref, $member_type_id, $member_ids);

                    /*For pensioners*/
                    $member_type_id = 5;
                    $member_ids =$payroll_repo->getPendingDocsForBeneficiaryUpdateDataTable(65)->where('document_payroll_beneficiary.member_type_id', 5)->select('document_payroll_beneficiary.resource_id as id', 'document_payroll_beneficiary.id as pivot_id')->get();
                    $this->savePayrollAlertTaskForByMemberType($stage_cv_ref, $member_type_id, $member_ids);

                    $already_saved = 1;
                    break;
            }

            /*If not already saved*/
            if($already_saved == 0){

                $this->savePayrollAlertTaskForByMemberType($stage_cv_ref, $member_type_id, $member_ids);
            }

        }
    }

    /*Get flag input by staging cv ref*/
    public function getFlagInputByStaging($staging_cv_ref)
    {

        $flag_input = [
            'notify' => 0,
            'priority' => 0,
            'shouldcount' => 1,
        ];
        /*Staging*/
        switch($staging_cv_ref){

            case 'PALEPENAC': //Pensioners for activation

                break;

            case 'PALEDEPAC': //Dependents for activation

                break;

            case 'PALECHSALR': //child suspension alerts
                $flag_input['shouldcount'] = 0;

                break;

            case 'PALECHA18': //child Approaching 18yrs
                $flag_input['shouldcount'] = 0;
                break;

            case 'PALERETSU': //Retiree Suspensions

                break;

            case 'PALEPEDVER': //Pending documents verification
                $flag_input['notify'] = 1;
                $flag_input['priority'] = 1;

                break;

            case 'PALEPEDBAN': //Pending documents bank
                $flag_input['notify'] = 1;
                $flag_input['priority'] = 1;

                break;
            case 'PALEPEDDET': //Pending documents details
                $flag_input['notify'] = 1;
                $flag_input['priority'] = 1;
                break;
        }

        return $flag_input;

    }

    /*Save payroll alert by member type*/
    public function savePayrollAlertTaskForByMemberType($stage_cv_ref,$member_type_id, $member_ids, array $opt_input = [])
    {
        foreach($member_ids as $key => $member){
            $opt_input =[
                'resource_pivot_id' =>   isset($member->pivot_id) ? $member->pivot_id : (isset($opt_input['resource_pivot_id']) ? $opt_input['resource_pivot_id'] : null),
            ];
            $this->savePayrollAlertTaskIndividual($stage_cv_ref,$member_type_id, $member->id,  $opt_input);
        }
    }

    /*Save new alert - individual*/
    public function savePayrollAlertTaskIndividual($stage_cv_ref,$member_type_id, $member_id, array $opt_input = [])
    {
        $cv_repo = new CodeValueRepository();
        $stage_cv_id = $cv_repo->findIdByReference($stage_cv_ref);
        /*user id to assign*/
        $flag_input = $this->getFlagInputByStaging($stage_cv_ref);
        $user_id = $this->retrieveAllocatedUserForCheckerAllStages();
        $user_id = ($user_id) ? $user_id : 56;
        $input = [
            'member_type_id' => $member_type_id,
            'resource_id' =>  $member_id,
            'resource_pivot_id' => (isset($opt_input['resource_pivot_id']) ? $opt_input['resource_pivot_id'] : null),
            'staging_cv_id' => $stage_cv_id,
            'status' => 0,
            'user_id' => $user_id,
            'notify' => isset($flag_input['notify']) ? $flag_input['notify'] : 0,
            'priority' => isset($flag_input['priority']) ? $flag_input['priority'] : 0,
            'shouldcount' => isset($flag_input['shouldcount']) ? $flag_input['shouldcount'] : 0,
        ];
        $this->saveTask($input);
    }


    /*Save new*/
    public function saveTask(array $input){
        $payroll_alert = $this->query()->where('member_type_id', $input['member_type_id'])->where('resource_id', $input['resource_id'])->where('staging_cv_id', $input['staging_cv_id'])->orderByDesc('id')->first();

        if(!$payroll_alert){
            /*Create new*/
            $payroll_alert =   $this->query()->create($input);
        }else{
            $payroll_alert->update([
                'status' => 0
            ]);
        }

        /*Initialize checker*/
        $this->initializeCheckerByTask($payroll_alert);

    }


    /*Close Task*/
    public function closePayrollAlertCheckerTask($member_type_id, $resource_id, $staging_cv_ref){
        $cv_repo = new CodeValueRepository();
        $staging_cv_id = $cv_repo->findIdByReference($staging_cv_ref);
        $input = [
            'member_type_id' => $member_type_id,
            'staging_cv_id' => $staging_cv_id,
            'resource_id' => $resource_id
        ];
        $this->closeRecently($input);
    }

    /*Close recently*/
    public function closeRecently(array $input)
    {
        DB::transaction(function () use ( $input) {
            $check_cat_cv_id = (new CodeValueRepository())->findIdByReference('CHCPAYALERT');
            /*close payroll alert task*/
            $payroll_alert = $this->query()->where('member_type_id', $input['member_type_id'])->where('resource_id', $input['resource_id'])->where('staging_cv_id', $input['staging_cv_id'])->first();
            $payroll_alert->update([
                'status' => 2,
            ]);

            /*close checker*/
            $checker_input = [
                'resource_id' => $payroll_alert->id,
                'checker_category_cv_id' => $check_cat_cv_id,
            ];
            (new CheckerRepository())->closeRecently($checker_input);
        });
    }



    /*Initialize checker by task*/
    public function initializeCheckerByTask(Model $payrollTaskAlert)
    {
        $cv_repo = new CodeValueRepository();
        $checker_repo = new CheckerRepository();
        $user_repo = new UserRepository();
        $check_cat_cv_id = $cv_repo->findIdByReference('CHCPAYALERT');
        $staging_cv_id = $payrollTaskAlert->staging_cv_id;
        $staging_cv = $cv_repo->find($staging_cv_id);
        $staging_cv_ref = $staging_cv->reference;
        $user_id = $payrollTaskAlert->user_id;
        $department = $user_repo->getUserDepartment($user_id);
        $status = $payrollTaskAlert->status;
        $notify = $payrollTaskAlert->notify;
        $comments = '{ "submittedDate":"' . now_date() . '", "currentStageCvId":' . $staging_cv_id . ', "department":"' . $department . '" }';
        $priority = $payrollTaskAlert->priority;
        $shouldcount = $payrollTaskAlert->shouldcount;
        $checker_exist = $payrollTaskAlert->checkers()->where('checkers.status', 0)->first();

        if($checker_exist){
            /*if exists*/
            if($status != 0){
                $input = [
                    'resource_id' => $payrollTaskAlert->id,
                    'checker_category_cv_id' => $check_cat_cv_id,
                ];
                $checker_repo->closeRecently($input);
            }

        }else{

            $input = [
                'comments' => $comments,
                'user_id' => $user_id,
                'checker_category_cv_id' => $check_cat_cv_id,
                'priority' => $priority,
                'notify' => $notify,
                'shouldcount' => $shouldcount,
            ];
            $checker_repo->create($payrollTaskAlert, $input);
        }

    }









    /**
     * @return null
     * Retrieve user with less count for next assignment
     */
    public function retrieveAllocatedUserForCheckerAllStages(){
        $cv_repo = new CodeValueRepository();
        $user_cv = $cv_repo->findByReference('PAYDALEUS');
        $users = $user_cv->users()->orderBy('users.id')->pluck('users.id');
        /*Get Next User with less tasks*/
        $task_counts_arr = [];
        $user_id = null;

        foreach ($users as $user){
            $task_count =$this->query()->where('status', 0)->where('user_id', $user)->count();
            $array = [$task_count];
            $task_counts_arr = array_merge_recursive($task_counts_arr, $array);
        }

        /*get next user with min count*/
        if(count($task_counts_arr)){
            $user_id_index_min = array_keys($task_counts_arr, min($task_counts_arr))[0];
            $user_id = $users[$user_id_index_min];
        }


        return $user_id;
    }


    /**
     * Reset hard pending checkers
     */
    public function resetHardPendingCheckers()
    {
        DB::table('checkers')->where('resource_type', 'App\Models\Operation\Payroll\PayrollAlertTask')->update([
            'status' => 2
        ]);

        $this->query()->update([
            'status' => 2
        ]);
    }
}