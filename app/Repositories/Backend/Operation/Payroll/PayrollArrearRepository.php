<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollArrearRepository extends  BaseRepository
{

    const MODEL = PayrollArrear::class;



    protected $pensioners;
    protected $dependents;
    protected $code_values;

    public function __construct()
    {
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->code_values = new CodeValueRepository();
    }


    /*Find payroll arrears by recovery id*/
    public function findByRecoveryId($payroll_recovery_id)
    {
        $arrears = $this->query()->where('payroll_recovery_id', $payroll_recovery_id)->first();
        return $arrears;
    }

    /*
     * create new
     */

    public function create(Model $payroll_recovery)
    {
        return DB::transaction(function () use ($payroll_recovery) {
            $arrears = $this->query()->create([
                'payroll_recovery_id' => $payroll_recovery->id,
                'amount' => $this->findAmountPerCycle($payroll_recovery),
                'recycles' => $payroll_recovery->cycles,
            ]);

            return $arrears;
        });
    }

    /*Find amount per cycle/month to be deducted*/
    public function findAmountPerCycle(Model $payroll_recovery)
    {
        $total_amount = $payroll_recovery->total_amount;
        $cycles = $payroll_recovery->cycles;
        $amount_per_cycle = $total_amount / $cycles;

        return round($amount_per_cycle,2);
    }


    /*
     * update
     */
    public function update($id,$input) {

    }

    /*Query for getting active arrears */
    public function queryForActiveArrears()
    {
        return $this->query()->where('isactive', 1)->where('recycles', '>', 0);
    }


    /*Get active /pending arrears for specified beneficiary*/
    public function getActiveArrearsCount($member_type_id, $resource_id)
    {
        $code_values = new CodeValueRepository();
        $arrears_cv_refs_array = $this->getArrearsTypeReferencesArray();
        $arrears_cv_ids_array = $this->code_values->findIdsByReferences($arrears_cv_refs_array);
        return $this->queryForActiveArrears()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $arrears_cv_ids_array){
            $query->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->whereIn('recovery_type_cv_id', $arrears_cv_ids_array);
        })->count();
    }

    /*Get array of cv reference of all arrears payroll recovery*/
    public function getArrearsTypeReferencesArray()
    {
        return ['PRTUNDP', 'PRAROCH'];
    }

    /*Update Recycles*/
    public function updateRecycle($payroll_recovery_id)
    {
        $arrear = $this->findByRecoveryId($payroll_recovery_id);
        $isactive = ($arrear->recycles > 1) ? 1 : 0;
        $arrear->update([
            'recycles' =>$arrear->recycles - 1,
            'isactive' => $isactive,
        ]);
    }

    /*Get unpaid arrears for a member*/
    public function getNetPendingArrearsAmountForMember($member_type_id, $resource_id,$employee_id)
    {
        $code_values = new CodeValueRepository();
        $arrears_cv_refs_array = $this->getArrearsTypeReferencesArray();
        $arrears_cv_ids_array = $this->code_values->findIdsByReferences($arrears_cv_refs_array);
        $pending_arrears = $this->queryForActiveArrears()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $arrears_cv_ids_array,$employee_id){
            $query->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->whereIn('recovery_type_cv_id', $arrears_cv_ids_array)->where('employee_id', $employee_id);
        })->get();
        $total_balance = 0;
        foreach ($pending_arrears as $pending_arrear){
            $total_balance = $total_balance + ($this->calculateBalanceAmount($pending_arrear->id));
        }
        return $total_balance;
    }

    /**
     * @param $id
     * Calculate pending amount to be recovered
     */
    public function calculateBalanceAmount($id)
    {
        $payroll_recovery_trans = new PayrollRecoveryTransactionRepository();
        $payroll_arrear = $this->find($id);
        $total_amount = $payroll_arrear->payrollRecovery->total_amount;
        $recovered_amount = $payroll_recovery_trans->getTotalAmountRecoveredForRecovery($payroll_arrear->payroll_recovery_id);
        $balance = $total_amount - $recovered_amount;
        return $balance;
    }
}