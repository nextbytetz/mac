<?php

namespace App\Repositories\Backend\Operation\Payroll\Retiree;

use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeFollowup;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollRetireeFollowupRepository extends  BaseRepository
{

    const MODEL = PayrollRetireeFollowup::class;


    public function __construct()
    {

    }


    /*Insert/create PayrollRetireeFollowup*/
    public function store(array $input)
    {
        return DB::transaction(function() use($input){
            $payrollRetireeFollowup = $this->query()->create([
                'member_type_id' => $input['member_type_id'],
                'resource_id' => $input['resource_id'],
                'employee_id' => $input['employee_id'],
                'date_of_follow_up' => $input['date_of_follow_up'],
                'retiree_feedback_cv_id' => $input['retiree_feedback_cv_id'],
                'date_of_next_follow_up' =>isset( $input['date_of_next_follow_up']) ?  $input['date_of_next_follow_up'] : $input['date_of_next_follow_up_esc'],
                'follow_up_type_cv_id' => $input['follow_up_type_cv_id'],
                'contact' => isset($input['contact']) ? $input['contact'] : null,
                'contact_person' =>isset( $input['contact_person']) ?  $input['contact_person'] : null,
                'user_id' => access()->id(),
            ]);

            /*update for no further action i.e. still working*/
            $this->updateForFeedbackOnResource($payrollRetireeFollowup);

            return $payrollRetireeFollowup;
        });
    }

    /*Modify PayrollRetireeFollowup*/
    public function update(Model $payrollRetireeFollowup, array $input)
    {
        return DB::transaction(function() use($payrollRetireeFollowup,$input){
            $payrollRetireeFollowup->update([
                'date_of_follow_up' => $input['date_of_follow_up'],
                'retiree_feedback_cv_id' => $input['retiree_feedback_cv_id'],
                'date_of_next_follow_up' =>isset( $input['date_of_next_follow_up']) ?  $input['date_of_next_follow_up'] : $payrollRetireeFollowup->date_of_next_follow_up,
                'follow_up_type_cv_id' => $input['follow_up_type_cv_id'],
                'contact' => isset($input['contact']) ? $input['contact'] : null,
                'contact_person' =>isset( $input['contact_person']) ?  $input['contact_person'] : null,
            ]);

            /*update for no further action i.e. still working*/
            $this->updateForFeedbackOnResource($payrollRetireeFollowup);

            return $payrollRetireeFollowup;
        });
    }


    /**
     * @param Model $payrollRetireeFollowup
     * Update  for feedback no need for mp udpate e.g. still working
     */
    public function updateForFeedbackOnResource(Model $payrollRetireeFollowup)
    {
        $member_type_id = $payrollRetireeFollowup->member_type_id;
        $resource_id = $payrollRetireeFollowup->resource_id;
        $employee_id = $payrollRetireeFollowup->employee_id;
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        $feed_back_cv_ref = $payrollRetireeFollowup->retireeFeedback->reference;
        $resource->update([
            'date_next_retiree_follow_up' => $payrollRetireeFollowup->date_of_next_follow_up,
            'retiree_feedback_cv_id' => $payrollRetireeFollowup->retiree_feedback_cv_id,
        ]);


    }



    /*Destroy / remove PayrollRetireeFollowup*/
    public function delete(Model $payrollRetireeFollowup)
    {
        DB::transaction(function() use($payrollRetireeFollowup) {
            /*Reset followup details on resource*/
            $this->resetResourceFollowupDetails($payrollRetireeFollowup);
            /*remove*/
            $payrollRetireeFollowup->delete();
        });
    }


    /*Reset resource followup details*/
    public function resetResourceFollowupDetails(Model $payrollRetireeFollowup)
    {
        $member_type_id = $payrollRetireeFollowup->member_type_id;
        $resource_id = $payrollRetireeFollowup->resource_id;
        $employee_id = $payrollRetireeFollowup->employee_id;
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        $feed_back_cv_ref = $payrollRetireeFollowup->retireeFeedback->reference;
        $previous_followup = $this->getQueryRetireeFollowupsByMember($member_type_id, $resource_id, $employee_id)->where('date_of_follow_up', '<',$payrollRetireeFollowup->date_of_follow_up )->orderByDesc('date_of_follow_up')->first();
        $resource->update([
            'date_next_retiree_follow_up' =>  ($previous_followup) ?   $previous_followup->date_of_next_follow_up : null,
            'retiree_feedback_cv_id' => ($previous_followup) ? $previous_followup->retiree_feedback_cv_id : null,
        ]);

    }


    /*Get all for Datatable PayrollRetireeFollowup*/
    public function getAllForDt()
    {
        $query = $this->query();
        return $query;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * Retiree followups by member
     */
    public function getQueryRetireeFollowupsByMember($member_type_id, $resource_id, $employee_id)
    {

        $base_query = $this->query()->where('payroll_retiree_followups.member_type_id', $member_type_id)->where('payroll_retiree_followups.resource_id', $resource_id)->where('payroll_retiree_followups.employee_id',$employee_id);
        return $base_query;
    }


    /**
     * Payroll retiree alerts who have reached age for retirement voluntary or mandatory
     */
    public function getPayrollRetireeAlertsForDt()
    {
        $limit_voluntary_age = sysdefs()->data()->payroll_voluntary_retire_age;//55
        $months_to_alert = 2;
        $today = Carbon::now();
        $cv_repo = new CodeValueRepository();
        $still_working_cv_id = $cv_repo->findIdByReference('PRFFSTLL');
        return DB::table('payroll_beneficiaries_view as b')
            ->select(
                'b.filename',
                'b.member_name',
                'b.resource_id',
                'b.member_type_id',
                'b.employee_id',
                'b.dob',
                DB::raw("coalesce(b.date_next_retiree_follow_up, (b.dob::date + (cast(55 as varchar) || ' year')::interval)) as next_followup_date"),// TODO 55 age coded here
                DB::raw("DATE_PART('day', coalesce(b.date_next_retiree_follow_up, b.dob::date + INTERVAL '55 year') - now()::date + INTERVAL '2 month') as days_left") // TODO 55 age & 2months coded here
            )
            ->whereRaw("coalesce(b.date_next_retiree_follow_up, b.dob::date + (cast(? as varchar) || ' year')::interval) <= now()::date + (cast(? as varchar) || ' mons')::interval", [$limit_voluntary_age,$months_to_alert ])
            ->where('b.isactive', 1)->whereRaw("coalesce(retiree_feedback_cv_id, ?) = ?", [$still_working_cv_id,$still_working_cv_id]);
    }


    /*Get alert retiree by member type*/
    public function getPayrollRetireeAlertsByMemberTypeForDt($member_type_id)
    {
        return $this->getPayrollRetireeAlertsForDt()->where('member_type_id',$member_type_id);
    }


}