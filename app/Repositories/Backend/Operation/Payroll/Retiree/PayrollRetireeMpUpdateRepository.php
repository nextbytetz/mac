<?php

namespace App\Repositories\Backend\Operation\Payroll\Retiree;

use App\Events\NewWorkflow;
use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeFollowup;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeMpUpdate;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollArrearRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollDeductionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollMpChangeTrackRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRecoveryRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\_caseless_remove;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollRetireeMpUpdateRepository extends  BaseRepository
{

    const MODEL = PayrollRetireeMpUpdate::class;


    public function __construct()
    {

    }

    public function getWfModuleGroupId()
    {
        return 34;
    }

    /*Insert/create PayrollRetireeMpupdate*/
    public function store(array $input)
    {
        return DB::transaction(function() use($input){
            $new_mp = $input['current_mp'] - $input['social_security_mp'];
            $deduction_details =  $this->calculateDeduction($input['member_type_id'], $input['resource_id'], $input['employee_id'], $input);
            $input['deduction_amount'] =$deduction_details['deduction_amount'] ?? 0;
            $input['deduction_summary'] =$deduction_details['deduction_summary'];
            $input['deduction_per_cycle'] =$deduction_details['deduction_per_cycle'] ?? 0;
            $input['cycles_actual'] =$deduction_details['cycles_actual'];
            $payrollRetireeMpupdate = $this->query()->create([
                'payroll_retiree_followup_id' => $input['payroll_retiree_followup_id'],
                'member_type_id' => $input['member_type_id'],
                'resource_id' => $input['resource_id'],
                'employee_id' => $input['employee_id'],
                'current_mp' => $input['current_mp'],
                'social_security_mp' => $input['social_security_mp'],
                'new_mp' => ($new_mp > 0) ? $new_mp : 0,
                'retirement_date' => $input['retirement_date'],
                'remark' => $input['remark'],
                'deduction_amount' => isset($input['deduction_amount']) ? $input['deduction_amount'] : 0,
                'deduction_summary' => isset($input['deduction_summary']) ? $input['deduction_summary'] : null,
                'deduction_per_cycle' => isset($input['deduction_per_cycle']) ? $input['deduction_per_cycle'] : 0,
                'cycles_actual' => isset($input['cycles_actual']) ? $input['cycles_actual'] : 0,
                'doc_evidence' => isset($input['doc_evidence']) ? $input['doc_evidence'] : null,
                'user_id' => access()->id(),
            ]);

            /*initiate wf*/
            $this->initiateWfOnCreate($payrollRetireeMpupdate, $input);




            return $payrollRetireeMpupdate;
        });
    }

    /*Modify PayrollRetireeMpupdate*/
    public function update(Model $payrollRetireeMpupdate, array $input)
    {
        return DB::transaction(function() use($payrollRetireeMpupdate,$input){

            $new_mp = $input['current_mp'] - $input['social_security_mp'];
            $deduction_details =  $this->calculateDeduction($input['member_type_id'], $input['resource_id'], $input['employee_id'], $input);
            $input['deduction_amount'] =$deduction_details['deduction_amount'];
            $input['deduction_summary'] =$deduction_details['deduction_summary'];
            $input['deduction_per_cycle'] =$deduction_details['deduction_per_cycle'];
            $input['cycles_actual'] =$deduction_details['cycles_actual'];

            $payrollRetireeMpupdate->update([
                'social_security_mp' => $input['social_security_mp'],
                'new_mp' => ($new_mp > 0) ? $new_mp : 0,
                'retirement_date' => $input['retirement_date'],
                'remark' => $input['remark'],
                'deduction_amount' => isset($input['deduction_amount']) ? $input['deduction_amount'] : 0,
                'deduction_summary' => isset($input['deduction_summary']) ? $input['deduction_summary'] : null,
                'deduction_per_cycle' => isset($input['deduction_per_cycle']) ? $input['deduction_per_cycle'] : null,
                'cycles_actual' => isset($input['cycles_actual']) ? $input['cycles_actual'] : null,
                'doc_evidence' => isset($input['doc_evidence']) ? $input['doc_evidence'] : null,
            ]);


            /*Post Mp change track*/
            $this->postMpChangeTrack($payrollRetireeMpupdate);//todo need to remove this after test
            return $payrollRetireeMpupdate;
        });
    }


    /**
     * @param array $input
     * @throws GeneralException
     * Initiate workflow on create/store
     */
    public function initiateWfOnCreate(Model $payrollRetireeMpupdate, array $input)
    {

        $wf_module_group_id = $this->getWfModuleGroupId();
        return  DB::transaction(function () use ($wf_module_group_id, $input, $payrollRetireeMpupdate) {

            $wf_type = $this->getWfType($payrollRetireeMpupdate);

            /*workflow start*/
            access()->hasWorkflowModuleDefinition($wf_module_group_id,$wf_type, 1);
//            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $status_change->id], [], ['comments' => $input['remark']] ));
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $payrollRetireeMpupdate->id, 'type' => $wf_type], [], ['comments' => $input['remark']]));
            /*end workflow*/
            return $payrollRetireeMpupdate;
        });
    }


    /**
     * @param $retirement_date
     * @param $current_mp
     * @param $social_security_mp
     * Calculate Deduction
     */
    public function calculateDeduction($member_type_id, $resource_id, $employee_id, array $mp_data)
    {
        $deduction_amount = 0;
        $arrears_amount = 0;
        $current_mp = $mp_data['current_mp'];
        $social_security_mp = $mp_data['social_security_mp'];
        $new_mp = $current_mp - $social_security_mp;
        $new_mp = ($new_mp > 0) ? $new_mp : 0;
        $retirement_date = $mp_data['retirement_date'];
        $retire_date_formatted = format_date_salary_eligible($retirement_date);
        $day_retired = Carbon::parse($retire_date_formatted)->format('j');
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        $payroll_runs_check = ($resource->payrollRuns()->count() > 0) ? true : false;
        $recent_payroll_run_approval = (new PayrollRunApprovalRepository())->getLastPayroll();
        $recent_payroll_run_approval_member = (new PayrollRunApprovalRepository())->getLastPayrollByMember($member_type_id, $resource_id, $employee_id);
        $payroll_month =($payroll_runs_check) ? $recent_payroll_run_approval->payroll_month : $retirement_date;
        $payroll_month_member =($recent_payroll_run_approval_member) ? $recent_payroll_run_approval_member->payroll_month : $retirement_date;
        /*TODO uncomment this if suspended payment to be void*/
        //        $recent_payroll_run = (new PayrollRunRepository())->getMostRecentPayrollRunByMember($member_type_id, $resource_id, $employee_id);
//        $payroll_run_approval = $recent_payroll_run->payrollRunApproval;
//        $payroll_month = $payroll_run_approval->payroll_month;

        if($new_mp != $current_mp) {
            $months_diff = months_diff($retirement_date, $payroll_month_member);

            if ($day_retired == 1) {
                $months_diff = $months_diff + 1;
            }
            $overpaid_mp = $current_mp - $new_mp;

            $deduction_amount = $months_diff * $overpaid_mp;


            /*Calculate arrears amount*/
            if ($deduction_amount == 0) {
$next_month_after_last_payroll_member = Carbon::parse($payroll_month_member)->addMonthsNoOverflow(1)->startOfMonth();
            $months_diff_arrears = months_diff($next_month_after_last_payroll_member, $payroll_month) + 1;
//            if ($day_retired == 1) {
//                $months_diff_arrears = $months_diff_arrears + 1;
//            }
            if ($months_diff_arrears > 0) {
                $arrears_amount = $new_mp * $months_diff_arrears;
            }
        }
            $deduction_amount = $deduction_amount - $arrears_amount;
        }
        $deduction_amount =  ($deduction_amount <> 0) ? $deduction_amount: 0;
        /*summary*/
        $summary = null;
        if($deduction_amount > 0){
            $deduction_cycle_details = $this->getDeductionCyclesForRecovery($new_mp, $deduction_amount);

            $summary = 'Upon approval this amount TZS ' . '<b>' . number_2_format($deduction_amount)  .  '</b>' .  ' will be deducted from beneficiary pension on next payroll(s). Deduction amount is derived from amount paid after retirement date <b>' . short_date_format($retirement_date) . '</b> to the last processed payroll <b>' .short_date_format($payroll_month). '</b>. Beneficiary will be deducted for the next <b>' .number_2_format( $deduction_cycle_details['cycles_actual']) .'</b> months, where every month this amount will be deducted, TZS <b>' . number_2_format($deduction_cycle_details['deduction_per_cycle']) . '</b>';
        }elseif ($deduction_amount < 0){
            /*arrears*/
            $deduction_cycle_details = $this->getDeductionCyclesForRecovery($new_mp, $deduction_amount);
            $summary = 'Upon approval this arrears of amount TZS ' . '<b>' . number_2_format(abs($deduction_amount))  .  '</b>' .  ' will be paid to beneficiary pension on next payroll(s). Arrears amount is derived from amount not paid after retirement date <b>' . short_date_format($retirement_date) . '</b> to the last processed payroll <b>' .short_date_format($payroll_month);

        }



        return ['deduction_amount' => $deduction_amount, 'deduction_summary' => $summary, 'deduction_per_cycle' => $deduction_cycle_details['deduction_per_cycle'], 'last_cycle_amount' => $deduction_cycle_details['last_cycle_amount'],'cycles_actual' => $deduction_cycle_details['cycles_actual'], ];
    }

    public function calculateDeductionOld($member_type_id, $resource_id, $employee_id, array $mp_data)
    {
        $deduction_amount = 0;
        $current_mp = $mp_data['current_mp'];
        $social_security_mp = $mp_data['social_security_mp'];
        $new_mp = $current_mp - $social_security_mp;
        $new_mp = ($new_mp > 0) ? $new_mp : 0;
        $retirement_date = $mp_data['retirement_date'];
        $retire_date_formatted = format_date_salary_eligible($retirement_date);
        $day_retired = Carbon::parse($retire_date_formatted)->format('j');
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        $payroll_runs_check = ($resource->payrollRuns()->count() > 0) ? true : false;
        $recent_payroll_run_approval = (new PayrollRunApprovalRepository())->getLastPayroll();
        $payroll_month =($payroll_runs_check) ? $recent_payroll_run_approval->payroll_month : $retirement_date;
        /*TODO uncomment this if suspended payment to be void*/
        //        $recent_payroll_run = (new PayrollRunRepository())->getMostRecentPayrollRunByMember($member_type_id, $resource_id, $employee_id);
//        $payroll_run_approval = $recent_payroll_run->payrollRunApproval;
//        $payroll_month = $payroll_run_approval->payroll_month;

        if($new_mp != $current_mp){
            $months_diff = months_diff($retirement_date,$payroll_month);

            if($day_retired == 1){
                $months_diff = $months_diff + 1;
            }
            $overpaid_mp = $current_mp - $new_mp;

            $deduction_amount = $months_diff * $overpaid_mp;
        }
        $deduction_amount =  ($deduction_amount > 0) ? $deduction_amount: 0;
        /*summary*/
        $summary = null;
        if($deduction_amount > 0){
            $deduction_cycle_details = $this->getDeductionCyclesForRecovery($new_mp, $deduction_amount);

            $summary = 'Upon approval this amount TZS ' . '<b>' . number_2_format($deduction_amount)  .  '</b>' .  ' will be deducted from beneficiary pension on next payroll(s). Deduction amount is derived from amount paid after retirement date <b>' . short_date_format($retirement_date) . '</b> to the last processed payroll <b>' .short_date_format($payroll_month). '</b>. Beneficiary will be deducted for the next <b>' .number_2_format( $deduction_cycle_details['cycles_actual']) .'</b> months, where every month this amount will be deducted, TZS <b>' . number_2_format($deduction_cycle_details['deduction_per_cycle']) . '</b>';
        }



        return ['deduction_amount' => $deduction_amount, 'deduction_summary' => $summary, 'deduction_per_cycle' => $deduction_cycle_details['deduction_per_cycle'], 'last_cycle_amount' => $deduction_cycle_details['last_cycle_amount'],'cycles_actual' => $deduction_cycle_details['cycles_actual'], ];
    }

    /**
     * @param $new_mp
     * @param $total_deduction
     * Get deduction cycles for recovery
     */
    public function getDeductionCyclesForRecovery($new_mp, $total_deduction)
    {

        $deduction_per_cycle = (0.3) * $new_mp; // 30% deduction
        $deduction_per_cycle = ($deduction_per_cycle > 0 ) ? $deduction_per_cycle : $total_deduction;
        $cycles = $total_deduction / $deduction_per_cycle;

        $cycles_floor = floor($cycles);
        $cycles_ceil = ceil($cycles);
        $offset_cycle = $cycles - $cycles_floor;
        $last_cycle_deduction =  $offset_cycle * $deduction_per_cycle;
        $last_cycle_deduction =  $deduction_per_cycle;

        if($total_deduction > 0){
            return [
                'deduction_per_cycle' => $deduction_per_cycle,
                'last_cycle_amount' => $last_cycle_deduction,
                'cycles' => $cycles_ceil,
                'cycles_actual' => $cycles
            ];
        }else{
            /*Arrears*/
            return [
                'deduction_per_cycle' => $total_deduction,
                'last_cycle_amount' => $total_deduction,
                'cycles' => 1,
                'cycles_actual' => 1
            ];
        }

    }

    /**
     * @param $input
     * Check if action can be initiated
     */
    public function checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id)
    {
        $change_pending_count = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id',
            $employee_id)->where('wf_done', 0)->count();
        if ($change_pending_count > 0) {
            throw new GeneralException('There is pending approval for this beneficiary! Complete all pending to proceed with new entry');
        }
    }

    /*Destroy / remove PayrollRetireeMpupdate*/
    public function delete(Model $payrollRetireeMpUpdate)
    {
        DB::transaction(function() use($payrollRetireeMpUpdate) {
            $payrollRetireeMpUpdate->wfTracks()->delete();
            $payrollRetireeMpUpdate->delete();

        });
    }

    /*Get all for Datatable PayrollRetireeMpupdate*/
    public function getAllForDt()
    {
        $query = $this->query();
        return $query;
    }

    /*Get mp updates by member*/
    public function getMpUpdatesByMemberForDt($member_type_id, $resource_id, $employee_id)
    {
        return $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id',
            $employee_id);
    }


    /**
     * WORKFLOW
     */

    /*Process actions at levels before on approval before wf complete*/
    public function processWfActionAtLevels($current_wf_track, array $input = null)
    {
        DB::transaction(function () use ($current_wf_track, $input) {
            $wf_module_repo = new WfModuleRepository();
            $resource_id = $current_wf_track->resource_id;
            $wf_module_id = $current_wf_track->wfDefinition->wf_module_id;
            $wf_definition = $current_wf_track->wfDefinition;
            $is_approval_level = $wf_definition->is_approval;
            $level = $wf_definition->level;
            $payroll_retiree_mp_update = $this->find($resource_id);
            $is_approved = ($current_wf_track->status == 1) ? 1 : 0;

            switch ($wf_module_id) {
                case 75:

                    break;
            }


//            /*Complete wf - Update on approval level*/
            if($is_approval_level == 1 && $is_approved == 1)
            {
                /*wf complete on approved  at Approval level*/
                $this->updateOnWfComplete($resource_id);
            }


        });

    }

    /**
     * @param $payroll_retiree_mp_update_id
     * Update on wf complete
     */
    public function updateOnWfComplete($payroll_retiree_mp_update_id)
    {
        DB::transaction(function () use ($payroll_retiree_mp_update_id) {
            $payroll_retiree_mp_update = $this->find($payroll_retiree_mp_update_id);
            /*update monthly pension*/
            $this->updateMonthlyPension($payroll_retiree_mp_update);

            /*Post deduction into recovery*/
            if($payroll_retiree_mp_update->deduction_amount > 0){
                $this->postDeductionIntoRecovery($payroll_retiree_mp_update);
            }elseif($payroll_retiree_mp_update->deduction_amount < 0) {
                /*Post Arrears*/
                $this->postArrearsIntoRecovery($payroll_retiree_mp_update);
            }

            /*Post Mp change track*/
            $this->postMpChangeTrack($payroll_retiree_mp_update);

        });
    }

    /**
     * @param Model $payroll_retiree_mp_update
     * Update month pension
     */
    public function updateMonthlyPension(Model $payroll_retiree_mp_update)
    {
        $member_type_id = $payroll_retiree_mp_update->member_type_id;
        $resource_id = $payroll_retiree_mp_update->resource_id;
        $employee_id = $payroll_retiree_mp_update->employee_id;
        $current_mp = $payroll_retiree_mp_update->current_mp;
        $new_mp = $payroll_retiree_mp_update->new_mp;
        if($current_mp != $new_mp){
            /*update only if there is change in mp*/
            if($member_type_id == 5)
            {
                /*pensioners*/
                (new PensionerRepository())->updateReassessedMp($resource_id, $current_mp, $new_mp);
            }elseif($member_type_id == 4){
                /*Dependents*/
                (new DependentRepository())->updateReassessedMp($resource_id, $employee_id, $current_mp, $new_mp);
            }

        }

    }



    /**
     * @param Model $payroll_retiree_mp_update
     * Post deduction into
     */
    public function postDeductionIntoRecovery(Model $payroll_retiree_mp_update)
    {
        DB::transaction(function () use ($payroll_retiree_mp_update) {
            $total_deduction = $payroll_retiree_mp_update->deduction_amount;
            if($total_deduction > 0){
                /*post only if deduction is > 0*/
                $deduction_per_cycle = $payroll_retiree_mp_update->deduction_per_cycle;
                $cycles_actual = $payroll_retiree_mp_update->cycles_actual;
                $cycles_ceil = ceil($cycles_actual);
                $cv_repo = (new CodeValueRepository());
                $recovery_type_cv_id = $cv_repo->findIdByReference('PRTOVEP');
                $recovery_data = [
                    'resource_id' => $payroll_retiree_mp_update->resource_id,
                    'member_type_id' => $payroll_retiree_mp_update->member_type_id,
                    'employee_id' => $payroll_retiree_mp_update->employee_id,
                    'recovery_type_cv_id' => $recovery_type_cv_id,
                    'remark' => 'Payroll Retiree MP update: ' . $payroll_retiree_mp_update->deduction_summary,
                    'total_amount' => $total_deduction,
                    'cycles' => $cycles_actual,
                    'user_id' => $payroll_retiree_mp_update->user_id,
                    'payroll_reconciliation_id' => null,
                    'ismanual' => 0,
                    'auto_calculation' => 0,
                    'entity_resource_id' => $payroll_retiree_mp_update->id,
                    'entity_resource_type' => 'App\Models\Operation\Payroll\Retiree\PayrollRetireeMpUpdate'
                ];
                /*Create recovery - only if not yet created*/
                if($payroll_retiree_mp_update->payrollRecovery()->count() == 0) {
                    $this->createMassAssignDbBuilder('payroll_recoveries', $recovery_data);

                    /*Auto wf complete*/
                    /*wf Complete*/
                    $payroll_recovery = $payroll_retiree_mp_update->payrollRecovery;
                    $payroll_recovery->update([
                        'wf_done' => 1,
                        'wf_done_date' => standard_date_format(Carbon::now())
                    ]);

                    /*Create deduction*/
                    $deduction_input = [
                        'amount' => $deduction_per_cycle,
                        'cycles' => $cycles_actual,
                        'recycles' => $cycles_actual,
                        'last_cycle_amount' => $deduction_per_cycle
//                        'last_cycle_amount' => ($cycles_actual - floor($cycles_actual)) * $deduction_per_cycle
                    ];
                    if ($payroll_recovery->payrollDeduction()->count() == 0) {
                        (new PayrollDeductionRepository())->create($payroll_recovery, $deduction_input);
                    }
                }

            }

        });
    }

    /**
     * @param Model $payroll_retiree_mp_update
     * Post arrears into recovery
     */
    public function postArrearsIntoRecovery(Model $payroll_retiree_mp_update)
    {
        DB::transaction(function () use ($payroll_retiree_mp_update) {
            $total_arrears = $payroll_retiree_mp_update->deduction_arrear_abs;
            if($total_arrears > 0){
                /*post only if arrears is > 0*/

                $cv_repo = (new CodeValueRepository());
                $recovery_type_cv_id = $cv_repo->findIdByReference('PRTUNDP');
                $recovery_data = [
                    'resource_id' => $payroll_retiree_mp_update->resource_id,
                    'member_type_id' => $payroll_retiree_mp_update->member_type_id,
                    'employee_id' => $payroll_retiree_mp_update->employee_id,
                    'recovery_type_cv_id' => $recovery_type_cv_id,
                    'remark' => 'Payroll Retiree MP update: ' . $payroll_retiree_mp_update->deduction_summary,
                    'total_amount' => $total_arrears,
                    'cycles' => 1,
                    'user_id' => $payroll_retiree_mp_update->user_id,
                    'payroll_reconciliation_id' => null,
                    'ismanual' => 0,
                    'auto_calculation' => 0,
                    'entity_resource_id' => $payroll_retiree_mp_update->id,
                    'entity_resource_type' => 'App\Models\Operation\Payroll\Retiree\PayrollRetireeMpUpdate'
                ];
                /*Create recovery - only if not yet created*/
                if($payroll_retiree_mp_update->payrollRecovery()->count() == 0) {
                    $this->createMassAssignDbBuilder('payroll_recoveries', $recovery_data);

                    /*Auto wf complete*/
                    /*wf Complete*/
                    $payroll_recovery = $payroll_retiree_mp_update->payrollRecovery;
                    $payroll_recovery->update([
                        'wf_done' => 1,
                        'wf_done_date' => standard_date_format(Carbon::now())
                    ]);

                    /*Create arrears*/
                    $arrears_input = [
                        'amount' => $total_arrears,
                        'cycles' => 1,
                        'recycles' => 1,
//                        'last_cycle_amount' => ($cycles_actual - floor($cycles_actual)) * $deduction_per_cycle
                    ];
                    if ($payroll_recovery->payrollArrear()->count() == 0) {
                        (new PayrollArrearRepository())->create($payroll_recovery, $arrears_input);
                    }
                }

            }

        });
    }
    /*Payroll mp change track*/
    public function postMpChangeTrack(Model $payroll_retiree_mp_update)
    {
        $payroll_mp_change_repo = new PayrollMpChangeTrackRepository();
        $current_mp = $payroll_retiree_mp_update->current_mp;
        $new_mp = $payroll_retiree_mp_update->new_mp;
        $member_type_id = $payroll_retiree_mp_update->member_type_id;
        $resource_id = $payroll_retiree_mp_update->resource_id;
        $employee_id = $payroll_retiree_mp_update->employee_id;
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        $start_date = $resource->getStartPayDateAttribute($employee_id);
        /*get end date*/
        $retirement_date = $payroll_retiree_mp_update->retirement_date;
        $retirement_date_salary_eligible = format_date_salary_eligible($retirement_date);
        $retirement_date_salary_eligible_parsed = Carbon::parse($retirement_date_salary_eligible);
        $day_retired = $retirement_date_salary_eligible_parsed->format('j');

        if($day_retired == 1) {
            $end_date = $retirement_date_salary_eligible_parsed->subMonthsNoOverflow(1)->endOfMonth();
        }else{
            $end_date = $retirement_date_salary_eligible_parsed->endOfMonth();
        }

        $input =[
            'member_type_id' => $member_type_id,
            'resource_id' => $resource_id,
            'employee_id' => $employee_id,
            'mp' => $current_mp,
            'start_date' => standard_date_format($start_date),
            'end_date' => standard_date_format($end_date),
            'trans_resource_id' => $payroll_retiree_mp_update->id,
            'trans_resource_type' => 'App\Models\Operation\Payroll\Retiree\PayrollRetireeMpUpdate',
            'user_id' => $payroll_retiree_mp_update->user_id
        ];

        $payroll_mp_change_repo->store($input);
    }

    /**
     * @param Model $payroll_retiree_mp_update
     * @return int
     * Get wf type
     */
    public function getWfType(Model $payroll_retiree_mp_update)
    {
        $type = 1;

        return $type;
    }

}