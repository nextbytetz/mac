<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollRecoveryRepository extends  BaseRepository
{

    const MODEL = PayrollRecovery::class;


    protected $pensioners;
    protected $dependents;
    protected $code_values;

    public function __construct()
    {
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->code_values = new CodeValueRepository();
    }



    /*
     * create new
     */

    public function create($input)
    {

        return DB::transaction(function () use ($input) {
            $recovery_type = $this->code_values->find($input['recovery_type_id']);
            $recovery = $this->query()->create([
                'resource_id' => $input['resource_id'],
                'member_type_id' => $input['member_type_id'],
                'employee_id' => $input['employee_id'],
                'recovery_type_cv_id' => $input['recovery_type_id'],
                'remark' => $input['remark'],
                'total_amount' =>  (isset($input['total_amount']) And ($input['total_amount']))  ? str_replace(",", "", $input['total_amount']) : null,
                'cycles' => $input['cycles'],
                'user_id' => isset($input['user_id']) ? $input['user_id'] : access()->id(),
                'payroll_reconciliation_id' => array_key_exists('payroll_reconciliation_id', $input) ? $input['payroll_reconciliation_id']  : null,
                'ismanual' => isset($input['ismanual']) ? $input['ismanual'] : 0,
                'auto_calculation' => isset($input['auto_calculation']) ? $input['auto_calculation'] : 1,

            ]);
            /*Update cycles  for deduction*/
            if($recovery_type->reference == 'PRTOVEP')
            {
                $recovery->update(['cycles' => $this->findCyclesForDeductions($recovery)]);
            }

            /*update manual payroll member*/
            if(isset($input['manual_payroll_member_id']) ){
                if($input['ismanual'] == 1){
                    (new ManualPayrollMemberRepository())->updateArrearsStatus($input['manual_payroll_member_id']);
                }
            }


            return $recovery;
        });
    }




    /*
     * update
     */
    public function update(PayrollRecovery $payroll_recovery,$input)
    {
        return DB::transaction(function () use ($input, $payroll_recovery) {
            $payroll_recovery->update([
                'remark' => $input['remark'],
                'total_amount' =>  (isset($input['total_amount']) And ($input['total_amount']))  ? str_replace(",", "", $input['total_amount']) : null,
                'cycles' => $input['cycles'],
                'auto_calculation' => isset($input['auto_calculation']) ? $input['auto_calculation'] : 1,

            ]);
            /*Update cycles  for deduction*/
            $recovery_type = $this->code_values->find($payroll_recovery->recovery_type_cv_id);
            if($recovery_type->reference == 'PRTOVEP')
            {
                $payroll_recovery->update(['cycles' => $this->findCyclesForDeductions($payroll_recovery)]);
            }

            return $payroll_recovery;
        });
    }




    /**
     * @param Model $status_change
     * Undo Payroll recovery entry
     */
    public function undo(Model $payroll_recovery)
    {

        DB::transaction(function () use ( $payroll_recovery) {
            $wf_modules = new WfModuleRepository();
            $wf_module_group_id = 13;
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payroll_recovery->id]);
            /*delete payroll recovery*/
            $payroll_recovery->delete();
            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();
        });
    }


    /**
     * @param $id
     * Action when workflow is completed
     */
    public function wfDoneComplete($id)
    {
        $payroll_recovery = $this->find($id);
        $recovery_type_ref = $payroll_recovery->recoveryType->reference;
        switch ($recovery_type_ref){
            case 'PRTUNDP':
            case 'PRAROCH':
                $payroll_arrears = new PayrollArrearRepository();
                $payroll_arrears->create($payroll_recovery);
                break;
            case 'PRTOVEP':
                $payroll_deductions = new PayrollDeductionRepository();
                $payroll_deductions->create($payroll_recovery);
                break;
            case 'PRTUNCLP':
                $payroll_unclaims = new PayrollUnclaimRepository();
                $payroll_unclaims->create($payroll_recovery);
                break;
        }
    }




    /*Get Monthly pension*/
    public function getMonthlyPension(Model $payroll_recovery)
    {
        $member_type_id = $payroll_recovery->member_type_id;
        $resource_id = $payroll_recovery->resource_id;
        $employee_id = $payroll_recovery->employee_id;
        if ($member_type_id == 5){
            /*pensioners*/
            $mp = $this->pensioners->getMonthlyPensionPerNotification($resource_id, $employee_id);
        }elseif($member_type_id == 4){
            /*dependent*/
            $mp = $this->dependents->getMonthlyPensionPerNotification($resource_id,$employee_id);
        }
        return $mp;
    }


    /*Find amount per cycle/month to be deducted*/
    public function findAmountPerCycleForDeduction(Model $payroll_recovery)
    {
        $auto_calculation = $payroll_recovery->auto_calculation;
        $mp = $this->getMonthlyPension($payroll_recovery);
        $total_amount = $payroll_recovery->total_amount;
        $cycles = $payroll_recovery->cycles;

        if ($total_amount < $mp && $auto_calculation == 1) {
            $amount_per_cycle = $total_amount;
        } else {

            if ($auto_calculation == 1) {
                /*auto calculate*/
                $amount_per_cycle = $mp;
            } else {
                /*manual*/
                $amount_per_cycle = $total_amount / $cycles;
            }

        }

        return round($amount_per_cycle,2);
    }


    /**
     * @param Model $payroll_recovery
     * @return float
     * Find last cycle amount to be deducted during payroll
     */
    public function findLastCycleAmountForDeduction(Model $payroll_recovery)
    {
        $auto_calculation = $payroll_recovery->auto_calculation;
        $mp = $this->getMonthlyPension($payroll_recovery);
        $total_amount = $payroll_recovery->total_amount;
        $remained_amount = $total_amount % $mp;
        $cycles = $payroll_recovery->cycles;

        if ($remained_amount > 0 && $cycles > 1 && $auto_calculation == 1){
            $last_cycle_amount = $remained_amount;
        }else{
            $last_cycle_amount = $this->findAmountPerCycleForDeduction($payroll_recovery);
        }

        return round($last_cycle_amount,2);
    }



    /**
     * @param Model $payroll_reconciliation
     * Find no of cycles for deductions
     */
    public function findCyclesForDeductions(Model $payroll_recovery)
    {
        $mp = $this->getMonthlyPension($payroll_recovery);
        $total_amount = $payroll_recovery->total_amount;
        $auto_calculation = $payroll_recovery->auto_calculation;
        if($auto_calculation == 1){
            /*auto*/
            $cycles = $total_amount / $mp;
            $cycles_ceil = ceil($cycles);
        }else{
            /*Manual*/
            $cycles_ceil =$payroll_recovery->cycles;
        }

        return $cycles_ceil;
    }






    /**
     * @param $member_type_id
     * @param $resource_id
     * Get Pending payroll recoveries by beneficiary.
     */
    public function getAllPendingsForDataTable()
    {
        $query = $this->query()->where('wf_done', 0);
        return $query;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * Get Pending payroll recoveries by beneficiary.
     */
    public function getPendingByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)
    {
        $query = $this->getByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)->where('wf_done', 0);
        return $query;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * @return mixed
     * Get approved recoveries by beneficiary per recovery type
     */
    public function getApprovedByBeneficiaryForDataTable($member_type_id, $resource_id,$employee_id)
    {
//        $recovery_cv_type_id = $this->code_values->findByReference($recovery_type_ref)->id;
        $query = $this->getByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)->where('wf_done', 1);

        return $query;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get By beneficiary for datatable
     */
    public function getByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)
    {
        return $this->query()->whereRaw("(iscancelled = 0 or cancelled_but_paid = 1)")->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
    }



    /**
     * @param $input
     * Check if can initiate new entry
     */
    public function checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id, $recovery_type_id)
    {
        $pending_count = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id',
            $employee_id)->where('recovery_type_cv_id', $recovery_type_id)->where('wf_done', 0)->count();
        if($pending_count > 0){
            throw new GeneralException('There is pending approval for this beneficiary! Complete all pendings to proceed with new entry of this recovery');
        }
    }


    /**
     * @param $id
     * @return bool
     * Check if there is pending payment for this approved recovery.
     */
    public function checkIfIsPendingForPayment($id){
        $recovery = $this->find($id);
        $recovery_type = (new CodeValueRepository())->find($recovery->recovery_type_cv_id);
        $check_active = 0; // check recovery which are approved but pending for payment
        if($recovery->wf_done == 1){
            switch ($recovery_type->reference){
                case 'PRTUNDP':
                    $arrear = $recovery->payrollArrear;
                    $check_active = ($arrear->isactive == 1) ? 1 : 0;
                    break;
                case 'PRTOVEP':
                    $deduction = $recovery->payrollDeduction;
                    $check_active = ($deduction->isactive == 1) ? 1 : 0;
                    break;
                case 'PRTUNCLP':
                    $unclaimed = $recovery->payrollUnclaim;
                    $check_active = ($unclaimed->isactive == 1) ? 1 : 0;
                    break;
            }

        }
        $return = ($check_active == 1) ? true : false;
        return $return;

    }


    /**
     * @param $id
     * @param array $input
     * Cancel Approved recovery
     */
    public function cancelApprovedRecovery(Model $payroll_recovery, array $input)
    {
        DB::transaction(function () use ($payroll_recovery, $input) {
            $recovery = $payroll_recovery;
            $recovery_type = (new CodeValueRepository())->find($recovery->recovery_type_cv_id);
            if ($recovery->wf_done == 1) {
                switch ($recovery_type->reference) {
                    case 'PRTUNDP':
                        $payment = $recovery->payrollArrear;
                        break;
                    case 'PRTOVEP':
                        $payment = $recovery->payrollDeduction;
                        break;
                    case 'PRTUNCLP':
                        $payment = $recovery->payrollUnclaim;

                        break;
                }

                /*Deactivate pending payment recovery*/
                if (isset($payment)) {
                    $payment->update([
                        'isactive' => 0,
                        'recycles' => 0,
                    ]);
                }

                /*Cancel recovery*/
                $recovered_amount = $payroll_recovery->recovered_amount;
                $recovery->update([
                    'iscancelled' => 1,
                    'cancel_reason' => $input['cancel_reason'],
                    'cancelled_but_paid' => ($recovered_amount > 0) ? 1 : 0,
                    'cancelled_by' => access()->id(),
                ]);
            }
        });
    }



    /**
     * Auto create Payroll Recovery for over age child who is eligible
     */


    public function storeRecoveryForOverageChild($dependent_id, $employee_id, $initiator_user_id)
    {
        DB::transaction(function () use ($dependent_id, $employee_id, $initiator_user_id) {
            /*Payroll recovery*/
            $input = [];
            $input['member_type_id'] = 4;
            $child_overage_eligible_data = (new PayrollRepository())->getOverageChildEligibleData(4, $dependent_id, $employee_id);
            $recovery_type = $this->code_values->findByReference('PRAROCH');
            $input['recovery_type_id'] = $recovery_type->id;
            $input['resource_id'] = $dependent_id;
            $input['remark'] = $recovery_type->name . ' : ' . 'Arrears for period of eligibility of '. $child_overage_eligible_data['first_pay_months'] . ' months from death date of employee, ' .short_date_format( $child_overage_eligible_data['death_date'])  . ' to last month of payroll eligibility, ' .  short_date_format( $child_overage_eligible_data['child_deadline_eligible']) . '.' ;
            $input['cycles']  = 1;
            $input['total_amount']  = $child_overage_eligible_data['arrears'];
            $input['user_id']  = $initiator_user_id;
            $input['employee_id']  = $employee_id;
            $input['ismanual']  = 0;
            $input['auto_calculation']  = 1;

            /*save recovery*/
            $recovery = $this->create($input);

            /*wf Complete*/
            $recovery->update([
                'wf_done' => 1,
                'wf_done_date' => standard_date_format(Carbon::now())
            ]);


            /*Create Allowance*/
            $this->wfDoneComplete($recovery->id);

        });
    }

}