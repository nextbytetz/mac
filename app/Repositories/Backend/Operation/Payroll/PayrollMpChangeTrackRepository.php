<?php

namespace App\Repositories\Backend\Operation\Payroll;


use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Operation\Compliance\Member\EmployerParticularChange;
use App\Models\Operation\Payroll\PayrollMpChangeTrack;
use App\Models\Operation\Payroll\PayrollMpUpdate;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Api\ClosedBusinessRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollMpChangeTrackRepository extends BaseRepository
{



    const MODEL = PayrollMpChangeTrack::class;


    /**
     * @param array $input
     * @return mixed
     * Store Mp change track
     */
    public function store(array $input)
    {
        return DB::transaction(function() use($input){
            $latest_mp_change_track = $this->getLatestMpTrack($input['member_type_id'], $input['resource_id'], $input['employee_id']);
            $end_date_last_change = ($latest_mp_change_track) ? $latest_mp_change_track->end_date : null;
            $start_date = ($end_date_last_change) ? Carbon::parse($end_date_last_change)->addMonthNoOverflow(1)->startOfMonth() : $input['start_date'];
            if($this->checkIfRangeAlreadyExists($input) == false) {
                $this->query()->create([
                    'member_type_id' => $input['member_type_id'],
                    'resource_id' => $input['resource_id'],
                    'employee_id' => $input['employee_id'],
                    'mp' => $input['mp'],
                    'start_date' => standard_date_format($start_date),
                    'end_date' => isset($input['end_date']) ? standard_date_format($input['end_date']) : null,
                    'trans_resource_id' => isset($input['trans_resource_id']) ? $input['trans_resource_id'] : null,
                    'trans_resource_type' => isset($input['trans_resource_type']) ? $input['trans_resource_type'] : null,
                    'user_id' => isset($input['user_id']) ? $input['user_id'] : access()->id()
                ]);
            };
        });
    }

    /*Check if Range already exists to avoid overlap*/
    public function checkIfRangeAlreadyExists($input)
    {
        $check = $this->queryMpChangeByMember($input['member_type_id'], $input['resource_id'], $input['employee_id'])->where('start_date', '<=', standard_date_format($input['end_date']))->where('end_date', '>=', standard_date_format($input['end_date']))->count();
        if($check > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * @return mixed
     * Query Mp Change by member
     */
    public function queryMpChangeByMember($member_type_id, $resource_id, $employee_id)
    {
        $query = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
        return $query;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * @return mixed
     * Latest mp track
     */
    public function getLatestMpTrack($member_type_id, $resource_id, $employee_id)
    {
        $latest_mp_track = $this->queryMpChangeByMember($member_type_id, $resource_id, $employee_id)->orderByDesc('end_date')->first();
        return $latest_mp_track;
    }



}