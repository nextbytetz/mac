<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Events\NewWorkflow;
use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Http\Controllers\Backend\Operation\Payroll\PayrollStatusChangeController;
use App\Jobs\RunPayroll\PostPayrollAlertTaskChecker;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollRetireeSuspensionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PayrollStatusChangeRepository extends  BaseRepository
{

    const MODEL = PayrollStatusChange::class;


    protected $code_values;
    protected $pensioners;
    protected $dependents;
    protected $notification_reports;

    public function __construct()
    {
        $this->code_values = new CodeValueRepository();
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->notification_reports = new NotificationReportRepository();
    }


    /*Main wf module group id*/
    public function wfModuleGroupId()
    {
        return 12;
    }

    /*
     * create new
     */

    public function create($input)
    {
        return DB::transaction(function () use ($input) {
            $resource = (new PayrollRepository())->getResource($input['member_type_id'], $input['resource_id']);
            $status_change_cv = $this->code_values->find( $input['status_change_type_id']);
            $suspense_flag = $resource->suspense_flag;
            $status_change = $this->query()->create([
                'resource_id' => $input['resource_id'],
                'member_type_id' => $input['member_type_id'],
                'employee_id' => $input['employee_id'],
                'status_change_cv_id' =>$status_change_cv->id,
                'remark' => $input['remark'],
                'exit_date' => array_key_exists('exit_date', $input) ? standard_date_format( $input['exit_date']) : null,
                'old_suspense_flag' => $suspense_flag,
                'arrears_summary' => isset($input['arrears_summary']) ? $input['arrears_summary'] : null,
                'firstpay_flag' => isset($input['firstpay_flag']) ? $input['firstpay_flag'] : null,
                'deadline_date' => isset($input['deadline_date']) ? $input['deadline_date'] : null,
                'hasarrears' => isset($input['hasarrears']) ? $input['hasarrears'] : 0,
                'pending_pay_months' => isset($input['pending_pay_months']) ? $input['pending_pay_months'] : 0,
                'doc_evidence' => isset($input['doc_evidence']) ? $input['doc_evidence'] : null,
                'user_id' => access()->id(),
            ]);

            /*suspend beneficiary when deactivating si initiated*/
            if($suspense_flag != 1){
                $this->suspendWhenInitiateApproval($input['member_type_id'], $input['resource_id'], $input['status_change_type_id']);
            }


            $status_change_cv_ref = $status_change_cv->reference;

            /*Child extension - save*/
            if($status_change_cv_ref == 'PSCCHDREI'){
                /*child extension*/
                (new PayrollChildExtensionRepository())->create($status_change->id, $input);
            }

            /*Status change - Close payroll alert checker <payroll_inbox_task>*/
//            if($status_change_cv_ref == 'PSCACT'){
//                $stage_cv_ref = ($input['member_type_id'] == 4) ? 'PALEDEPAC' : 'PALEPENAC';
//                dispatch(new PostPayrollAlertTaskChecker($stage_cv_ref, $input['member_type_id'], $input['resource_id'],1));
//            }


            return $status_change;
        });
    }


    /*
     * update
     */
    public function update(Model $status_change,$input) {
        return DB::transaction(function () use ($input, $status_change) {

            $status_change->update([
                'remark' => $input['remark'],
                'exit_date' => array_key_exists('exit_date', $input) ? standard_date_format( $input['exit_date']) : null,
                'firstpay_flag' => isset($input['firstpay_flag']) ? $input['firstpay_flag'] : null,
                'arrears_summary' => isset($input['arrears_summary']) ? $input['arrears_summary'] : null,
                'deadline_date' => isset($input['deadline_date']) ? $input['deadline_date'] : null,
                'hasarrears' => isset($input['hasarrears']) ? $input['hasarrears'] : $status_change->hasarrears,
                'pending_pay_months' => isset($input['arrears_apending_pay_monthsmount']) ? $input['pending_pay_months'] : $status_change->pending_pay_months,
                'doc_evidence' => isset($input['doc_evidence']) ? $input['doc_evidence'] : null,
            ]);
            $status_change_cv_ref= $status_change->statusChangeType->reference;
            /*Child extension - save*/
            if($status_change_cv_ref == 'PSCCHDREI'){
                /*child extension*/
                (new PayrollChildExtensionRepository())->update($status_change, $input);
            }

            return $status_change;
        });
    }


    /**
     * @param array $input
     * @throws GeneralException
     * Extensive function for initiate WF on create
     */
    public function initiateWfOnCreate(array $input)
    {
        $status_change_type = $this->code_values->find($input['status_change_type_id']);
        $this->checkIfCanInitiateNewEntry($input['member_type_id'], $input['resource_id'], $input['employee_id'], $status_change_type->reference);
        $wf_module_group_id = 12;
        return  DB::transaction(function () use ($wf_module_group_id, $input) {
            /*create status change entry*/
            $member_type_id = $input['member_type_id'];
            $resource_id = $input['resource_id'];
            $status_change = $this->create($input);
            $wf_type = $this->getWfType($status_change);

            /*workflow start*/
            access()->hasWorkflowModuleDefinition($wf_module_group_id,$wf_type, 1);
//            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $status_change->id], [], ['comments' => $input['remark']] ));
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $status_change->id, 'type' => $wf_type], [], ['comments' => $input['remark']]));
            /*end workflow*/
            return $status_change;
        });
    }

    /*Get wf Type*/
    public function getWfType(Model $payroll_status_change)
    {
        $status_change_cv_ref = $payroll_status_change->statusChangeType->reference;
        $type = 1;
        switch($status_change_cv_ref){
            case 'PSCACT'://Activation
            case 'PSCDEACT'://Deactivation
            case 'PSCREIN'://Reinstate
            case 'PSCMASU'://Manual Suspension
            case 'PSCVERIF'://Verification
                $type = 1;
                break;

            case 'PSCCHDREI'://Child Reinstate (Continuation)
                $child_extension = $payroll_status_change->payrollChildExtension;
                $child_continuation_cv_ref = $child_extension->childContinuationReason->reference;

                switch ($child_continuation_cv_ref){
                    case 'PAYCHCOSCH'://School Continuation
                        $type = 2;
                        break;

                    case 'PAYCHCDIS'://Disability
                        $type = 3;
                        break;
                }

                break;
        }
        return $type;

    }


    /*Get level For child extension: disability assessment*/
    public function getLevelForDisabilityAssessment(PayrollStatusChange $statusChange)
    {
        $wf_module_repo = new WfModuleRepository();
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->getWfType($statusChange);
        $wf_module = (new WfModuleRepository())->query()->where('type', $type)->where('wf_module_group_id', $wf_module_group_id)->first();
        $wf_module_id = $wf_module->id;
        switch($wf_module_id){
            case 70:
                return 7;
                break;
        }
    }

    /*Get level For letter preparation*/
    public function getLevelForLetterPreparation(PayrollStatusChange $statusChange)
    {
        $wf_module_repo = new WfModuleRepository();
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->getWfType($statusChange);
        $wf_module = (new WfModuleRepository())->query()->where('type', $type)->where('wf_module_group_id', $wf_module_group_id)->first();
        $wf_module_id = $wf_module->id;
        switch($wf_module_id){
            case 69:
                return 6;
                break;
            case 70:
                return 12;
                break;
        }
    }

    /*Check if can initiate disability assess*/
    public function checkIfCanInitiateActionByLevel($current_track, $level)
    {
        $return = false;
        if(isset($current_track)){
            $wf_definition = $current_track->wfDefinition;
            $current_level = $wf_definition->level;
            $wf_module_id = $wf_definition->wf_module_id;
            $status_change = $this->find($current_track->resource_id);
            $wf_module_group_id = $this->wfModuleGroupId();
            $type = $this->getWfType($status_change);

            $access = access()->user()->hasWorkflowModuleDefinition($wf_module_group_id, $type, $level);

            if($current_level == $level && $access == true && $status_change->wf_done == 0)
//            if($current_level == $level_for_letter && $access == true && $arrears == 0)
            {
                //Check if was declined on approval level
                $return = true;

            }
        }
        return $return;
    }


    /*Process actions at levels before on approval before wf complete*/
    public function processWfActionAtLevels($current_wf_track, array $input = null)
    {
        DB::transaction(function () use ($current_wf_track, $input) {
            $wf_module_repo = new WfModuleRepository();
            $payroll_child_ext_repo = new PayrollChildExtensionRepository();
            $resource_id = $current_wf_track->resource_id;
            $wf_module_id = $current_wf_track->wfDefinition->wf_module_id;
            $wf_definition = $current_wf_track->wfDefinition;
            $is_approval_level = $wf_definition->is_approval;
            $level = $wf_definition->level;
            $status_change = $this->find($resource_id);
            $is_approved = ($current_wf_track->status == 1) ? 1 : 0;

            switch ($wf_module_id) {
                case 70://disable
                    switch ($level) {

                        case 7://disability assessment
                            /*child disability assessment*/
                            $payroll_child_ext_repo->checkIfDisabilityAssessed($status_change);
                            break;

                        case 12://letter prepare
                            $payroll_child_ext_repo->checkIfLetterIsPrepared($status_change);
                            break;
                    }
                    break;


                case 69://school
                    switch ($level) {

                        case 6://letter prepare
                            $payroll_child_ext_repo->checkIfLetterIsPrepared($status_change);
                            break;
                    }
                    break;

                case 26://default module
//                    switch ($level) {
//
//
//                    }
                    break;

            }


            /*Complete wf - Update on approval level*/
            if($is_approval_level == 1 && $is_approved == 1)
            {
                /*wf complete on approved  at Approval level*/
                $this->wfDoneComplete($resource_id);
            }


        });

    }





    /**
     * @param Model $status_change
     * Undo status change
     */
    public function undo(Model $status_change)
    {
        DB::transaction(function () use ( $status_change) {
            $wf_modules = new WfModuleRepository();
            $wf_module_group_id = 12;
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $status_change->id]);
            /*delete status change*/
            $status_change->delete();
            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();

            /*Reinstate user if is deactivation*/
            $this->reinstateAfterApprovalByChangeType($status_change);
        });
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * Suspend beneficiary when initiate deactivation from payroll
     */
    public function suspendWhenInitiateApproval($member_type_id, $resource_id, $status_change_cv_id)
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        if($resource->suspense_flag != 1){
            $code_value = $this->code_values->find($status_change_cv_id);
            if($code_value->reference == 'PSCDEACT'){
                $suspend_reason = 'Suspension for Deactivaion Process';
                $this->suspend($resource, $suspend_reason);
            }
        }

    }

    public function reinstateAfterApprovalByChangeType($status_change)
    {
        $member_type_id = $status_change->member_type_id;
        $resource_id = $status_change->resource_id;
        $status_change_cv_id = $status_change->status_change_cv_id;
        $old_suspense_flag = $status_change->old_suspense_flag;
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        $code_value = $this->code_values->find($status_change_cv_id);
        /*Only reinstate if was active*/
        if($old_suspense_flag != 1){
            if($code_value->reference == 'PSCDEACT'){

                $this->reinstate($resource);
            }
        }


    }

    /**
     * @param $input
     * Check if action can be initiated
     */
    public function checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id, $status_change_type_ref)
    {
        $change_pending_count = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id',
            $employee_id)->where('wf_done', 0)->count();
        if($change_pending_count > 0){
            throw new GeneralException('There is pending approval for this beneficiary! Complete all pendings to proceed with new status change');
        }

        /*Check per status type change*/
        $return = false;
        switch ($status_change_type_ref){
            /*Activate*/
            case('PSCACT'):

                /*Check if child is eligible - age limit*/
//                //todo Remove this if statement when go live
//                if(env('TESTING_MODE') == 0){
//                    $child_check = ($member_type_id == 4) ? $this->checkIfChildUnderAgeLimits($resource_id) : '';
//                }

                /*check if there is bank details update is pending*/
                $bank_updates = new PayrollBankUpdateRepository();
                $bank_updates->checkIfThereIsPendingApproval($member_type_id, $resource_id);

                /*Check if required details they are filled*/
                $this->checkIfRequiredDetailsAreFilled($member_type_id, $resource_id);

                break;
            /*Deactivate*/


            /*Reinstate*/
            case('PSCREIN'):
                /*Check if child is eligible - age limit*/
                $child_check = ($member_type_id == 4) ? $this->checkIfChildUnderAgeLimits($resource_id) : '';

                /*check if there is bank details update is pending*/
                $bank_updates = new PayrollBankUpdateRepository();
                $bank_updates->checkIfThereIsPendingApproval($member_type_id, $resource_id);
                break;
            /*Deactivate*/
            case('PSCDEACT'):
                /*check if there is bank details update is pending*/
                $bank_updates = new PayrollBankUpdateRepository();
                $bank_updates->checkIfThereIsPendingApproval($member_type_id, $resource_id);
                break;

            case('PSCVERIF'):
                /*check if there is bank details update is pending*/
                $bank_updates = new PayrollBankUpdateRepository();
                $bank_updates->checkIfThereIsPendingApproval($member_type_id, $resource_id);
                break;
        }
    }


    /*Check if there is pending Status change*/
    public function checkIfThereIsPendingStatusChange($member_type_id,$resource_id, $status_change_ref){
        $code_value = $this->code_values->findByReference($status_change_ref);
        $check_pending = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('status_change_cv_id', $code_value->id)->where('wf_done', 0)->count();
        if($check_pending > 0){
            throw new GeneralException('There is pending '. $code_value->name . ' approval, can not initiate this! Please check!');
        }
    }

    /*Check if child is still eligible for payroll*/
    public function checkIfChildUnderAgeLimits($resource_id)
    {
        /*check if dependent is child*/
        $child_check = DB::table('dependent_employee')->where('dependent_id', $resource_id)->where('dependent_type_id',3 )->count();

        if($child_check > 0){

            if($this->dependents->checkIfChildUnderAgeLimits($resource_id) == false){
                throw new GeneralException('Child is already over age! Please check!');
            }

        }
    }

    /**
     * @param $id
     * Check if bank details of beneficiary are filled when activating
     */
    public function checkIfBankDetailsAreFilled($member_type_id,$resource_id)
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        if($resource->bank_branch_id == 156 || $resource->accountno == null || $resource->accountno = '')
        {
            throw new GeneralException('Bank details for this beneficiary are not complete! Please check!');
        }
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @throws WorkflowException
     * Check if beneficiary contact required details are filled i.e phone, dob
     */
    public function checkIfRequiredDetailsAreFilled($member_type_id,$resource_id)
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        $error_message = null;
        if($resource->phone == null || $resource->phone = '')
        {
            $error_message = $error_message . 'Phone number not filled!';
//            throw new GeneralException('Phone number not filled! Please check!');
        }
        if($resource->dob == null || $resource->dob = '')
        {
            $error_message =  $error_message . ', ' . 'Date of birth (DOB) not filled!';

        }

//        if($resource->bank_branch_id == 156 || $resource->accountno == null || $resource->accountno = '' || $resource->bank_id == 5 || $resource->bank_id == 6)
//        {
//            $error_message =  $error_message . ', ' . 'Bank details for this beneficiary are not complete!';
//
//        }
        if(isset($error_message)){
            throw new GeneralException($error_message . '. Please Check!');
        }

    }


    /**
     * @param Model $resource
     * Verify beneficiary
     */
    public function verify(Model $resource, $verification_date = null)
    {

        $resource->update([
            'lastresponse' =>  (isset($verification_date)) ? Carbon::parse($verification_date)->format('Y-m-d H:i:s') : Carbon::now(),
//            'suspense_flag' => ($resource->suspense_flag == 1) ? 3 : 0,
        ]);
    }


    /**
     * @param Model $resource
     * Reinstate suspended beneficiary
     */
    public function reinstate(Model $resource)
    {
        $resource->update([
            'suspense_flag' => 3
        ]);

    }

    /**
     * @param Model $resource
     * Reinstate removed/deactivated
     */
    public function reinstateRemoved($member_type_id, $resource_id, $employee_id )
    {
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        /*Reinstate*/
        $this->reinstate($resource);
        /*Reactivate*/
        switch ($member_type_id){
            case 4://dependent
                $this->activateDependent($resource, $employee_id,1);
                break;

            case 5://pensioner
                $this->activatePensioner($resource, 1);
                break;
        }

    }

    /**
     * @param Model $resource
     * Suspend beneficiary
     */
    public function suspend(Model $resource, $suspend_reason = null)
    {
        $resource->update([
            'suspense_flag' => 1,
            'suspended_date' => standard_date_format(Carbon::now()),
            'suspension_reason' => isset($suspend_reason) ? $suspend_reason : $resource->suspension_reason
        ]);
    }


    /*---------Start -----ACTIVATION-----------------------*/
    /**
     * @param Model $resource
     * Activate beneficiary - Pensioner
     */
    public function activatePensioner(Model $resource,  $isreacivate = 0)
    {
        $resource->update([
            'isactive' => 1,
            'isresponded' => 1,
            'lastresponse' => ($isreacivate == 0) ? Carbon::now() : $resource->lastresponse,
        ]);
    }

    /**
     * @param Model $resource
     * Activate beneficiary - Dependent
     */
    public function activateDependent(Model $resource, $employee_id, $isreacivate = 0)
    {
        DB::transaction(function () use ( $resource,$employee_id, $isreacivate ) {
            /*update pivot table - dependent_employee*/
            $dependent_employee = $resource->employees()->newPivotStatement()
                ->where('employee_id',$employee_id)
                ->where('dependent_id', $resource->id)
                ->update([
                    'isactive' => 1,
                    'isresponded' => 1,
                ]);
            /*update dependent lastresponse*/
            if($isreacivate == 0){
                $resource->update([
                    'lastresponse' => Carbon::now(),
                ]);
            }

        });
    }
    /*---------End ACTIVATION -----------------------------------------*/

    /**
     * @param Model $resource
     * Activate beneficiary - Dependent
     */
    public function activateOverageChildEligibleDependent(Model $resource, $employee_id, $initiator_user_id)
    {
        //Deactivate overage child - only pay arrears one time
        $this->deactivateDependent($resource, $employee_id);
        //Update overage eligible flag
        $this->updateIsOverageEligibleDependent($resource, $employee_id);
        //create payroll recovery - one time
        (new PayrollRecoveryRepository())->storeRecoveryForOverageChild($resource->id, $employee_id, $initiator_user_id);
    }


    /*Update first pay status on wf done*/
    public function updateFirstPayStatusOnWfDone(Model $status_change)
    {
        DB::transaction(function () use ( $status_change ) {
            $firstpay_flag = $status_change->firstpay_flag;
            if (isset($status_change->firstpay_flag)) {
                $member_type_id = $status_change->member_type_id;
                $resource_id = $status_change->resource_id;
                $employee_id = $status_change->employee_id;
                $resource = $this->getResource($member_type_id, $resource_id);
                /*Update first pay status*/
                switch ($member_type_id) {
                    case 4:
                        DB::table('dependent_employee')->where('employee_id', $employee_id)->where('dependent_id', $resource_id)->update([
                            'firstpay_flag' => $firstpay_flag
                        ]);
                        break;
                    case 5:
                        $resource->update([
                            'firstpay_flag' => $firstpay_flag
                        ]);
                        break;
                }

                /*Deactivate  all pending suspended payments */
                $suspend_input = [
                    'member_type_id' => $member_type_id,
                    'resource_id' => $resource_id,
                    'employee_id' => $employee_id,
                    'deleted_by' => $status_change->user_id,
                    'delete_reason' => 'First Pay Status reset, reference payroll status change - ' . $status_change->id,
                ];
                (new PayrollSuspendedRunRepository())->removeSuspendedPayments($suspend_input);

            }
        });
    }

    /*---------End ACTIVATION -----------------------------------------*/


    /*---------Start -----DEACTIVATION-----------------------*/
    /**
     * @param Model $resource
     * Activate beneficiary - Pensioner
     */
    public function deactivatePensioner(Model $resource)
    {
        DB::transaction(function () use ( $resource ) {
            $resource->update([
                'isactive' => 2,
            ]);
            /*Deactivate assistant if any*/
            $this->deactivateCareTakerAssistant($resource);
        });
    }

    /**
     * @param $pensioner
     * Deactivate care taker assistant if PTD 100
     */
    public function deactivateCareTakerAssistant($pensioner)
    {
        $employee = $pensioner->employee;
        $assistant_query = $employee->dependents()->where('dependent_type_id',7)->where('dependent_employee.isactive', 1);
        if($assistant_query->count() > 0){
            $payrolls = new PayrollRepository();
            $dependent_employee = $assistant_query->first()->pivot;
            $assistant = $payrolls->getResource(5,$dependent_employee->dependent_id );
            $this->deactivateDependent($assistant, $pensioner->employee_id);

        }

    }

    /**
     * @param Model $resource
     * Deactivate beneficiary - Dependent
     */
    public function deactivateDependent(Model $resource, $employee_id)
    {
        DB::transaction(function () use ( $resource,$employee_id ) {
            /*update pivot table - dependent_employee*/
            $dependent_employee = $resource->employees()->newPivotStatement()
                ->where('employee_id',$employee_id)
                ->where('dependent_id', $resource->id)
                ->update([
                    'isactive' => 2,
                ]);
            /*update dependent lastresponse*/

        });
    }
    /*---------End DEACTIVATION -----------------------------------------*/

    /**
     * @param Model $resource
     * Update is overage eligible - for child dependent
     */
    public function updateIsOverageEligibleDependent(Model $resource, $employee_id)
    {
        DB::transaction(function () use ( $resource,$employee_id ) {
            /*update pivot table - dependent_employee*/
            $dependent_employee = $resource->employees()->newPivotStatement()
                ->where('employee_id',$employee_id)
                ->where('dependent_id', $resource->id)
                ->update([
                    'is_overage_eligible' => 1,
                ]);


        });
    }




    /**
     * Updates when workflow is updated
     */
    public function wfDoneComplete($id)
    {
        DB::transaction(function () use ( $id) {
            $status_change = $this->find($id);
            $member_type_id = $status_change->member_type_id;
            $resource_id = $status_change->resource_id;
            $employee_id = $status_change->employee_id;
            $resource = $this->getResource($member_type_id, $resource_id );
            $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
            $firstpay_flag = ($member_type_id == 4) ? $dependent_employee->firstpay_flag : $resource->firstpay_flag;
            $status_change_firstpay_flag =$status_change->firstpay_flag;
            $status_change_type = $this->code_values->find($status_change->status_change_cv_id);
            $status_change_type_ref = $status_change_type->reference;
            $payroll_repo = new PayrollRepository();
            $update_input = [];
            switch ($status_change_type_ref) {
                case 'PSCACT':

                    /*Check if child is eligible - age limit*/
//                    $child_check = ($member_type_id == 4) ? $this->checkIfChildUnderAgeLimits($resource_id) : '';
                    if($member_type_id == 5){
                        /*pensioner*/
                        $this->activatePensioner($resource);
                    }elseif($member_type_id == 4){
                        /*Dependent*/
                        //check if child
                        $dependent_employee = $resource->getDependentEmployee($employee_id);
                        $check_if_child_eligible = ($dependent_employee->dependent_type_id == 3) ? (new DependentRepository())->checkIfChildUnderAgeLimits($resource_id) : true;
                        if($dependent_employee->dependent_type_id = 3 && $check_if_child_eligible == false){
                            //activate - overage child dependent
                            $this->activateOverageChildEligibleDependent($resource, $employee_id, $status_change->user_id);
                        }else{

                            //Normal dependents eligible - Activate
                            $this->activateDependent($resource, $employee_id);

                            /*check if is other dependents and has completed remained cycles (Payable months) (Originate from manual)*/
                            if($dependent_employee->isotherdep == 1 && $dependent_employee->recycles_pay_period == 0){
                                /*Suspend for further action i.e. deactivation*/

                                $suspend_reason = 'Have already completed cycles for payable months';
                                $this->suspend($resource, $suspend_reason);
                            }

                        }

                    }

                    /*Update firstpay status*/
                    if($firstpay_flag == 0)
                    {
                        $this->updateFirstPayStatusOnWfDone($status_change);
                    }

                    /*Update Status change*/
                    $update_input = [
                        'arrears_summary' => ($status_change->arrears_summary) ? $payroll_repo->getArrearsSummaryOnActivation($member_type_id,$resource_id, $employee_id)['summary'] : null,
                    ];
                    $this->updateOnWfComplete($status_change, $update_input);

                    /*Send to e-office on successfully activation*/
                    $payroll_repo->postBeneficiaryToDms($member_type_id,$resource_id, $employee_id);
                    break;

                case 'PSCDEACT':
                    if($member_type_id == 5){
                        /*pensioner*/
                        $this->deactivatePensioner($resource);
                    }elseif($member_type_id == 4){
                        /*Dependent*/
                        $this->deactivateDependent($resource, $employee_id);
                    }
                    /*reinstate*/
                    $this->reinstateAfterApprovalByChangeType($status_change);
                    /*Update retiree suspensions alert*/
                    if($member_type_id == 5){
                        (new PayrollRetireeSuspensionRepository())->updateStatus($resource->id);
                    }

                    break;

                case 'PSCREIN':
                    /*Check if child is eligible - age limit*/
                    $child_check = ($member_type_id == 4) ? $this->checkIfChildUnderAgeLimits($resource_id) : '';
                    $this->reinstate($resource);

                    /*Update firstpay status*/
                    if($firstpay_flag == 0)
                    {
                        $this->updateFirstPayStatusOnWfDone($status_change);
                    }

                    /*Update retiree suspensions alert*/
                    if($member_type_id== 5){
                        (new PayrollRetireeSuspensionRepository())->updateStatus($resource->id);
                    }

                    break;

                case 'PSCMASU':
                    $suspend_reason = $status_change->remark;
                    $this->suspend($resource,$suspend_reason);
                    break;

                case 'PSCREINR':


                    break;

                case 'PSCVERIF':
                    /*Payroll verification*/
                    (new PayrollVerificationRepository())->verifyBeneficiaryOnApproval($resource, $status_change->payrollVerification);
                    break;

                case 'PSCCHDREI':
                    /*Payroll child reinstate - (Continuation)*/
                    $this->updateChildContinuationOnWfComplete($status_change);
                    break;
                default:

            }

        });
    }
    /*Update on WF Complete*/
    public function updateOnWfComplete(Model $status_change, array $input)
    {
        $status_change->update($input);
    }


    /*WF complete but declined*/
    public function wfDoneCompleteDeclined($id)
    {
        DB::transaction(function () use ( $id) {
            $status_change = $this->find($id);
            $member_type_id = $status_change->member_type_id;
            $resource_id = $status_change->resource_id;
            $notification_report_id = $status_change->notification_report_id;
            $resource = $this->getResource($member_type_id, $resource_id );
            $status_change_type = $this->code_values->find($status_change->status_change_cv_id);
            $status_change_type_ref = $status_change_type->reference;

            switch ($status_change_type_ref) {
                case 'PSCACT':
                    break;

                case 'PSCDEACT':
                    /*reinstate*/
                    $this->reinstate($resource);
                    break;

                case 'PSCREIN':

                    break;

                case 'PSCMASU':

                    break;

                case 'PSCREINR':


                    break;
                default:

            }


        });
    }


    /*Update child continuation on WF complete*/
    public function updateChildContinuationOnWfComplete(Model $status_change)
    {
        $child_extension = $status_change->payrollChildExtension;
        $child_cont_reason_ref =$child_extension->childContinuationReason->reference;
        $member_type_id = $status_change->member_type_id;
        $resource_id = $status_change->resource_id;
        $employee_id =$status_change->employee_id;
        $deadline_date = $status_change->deadline_date;
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        switch($child_cont_reason_ref){

            case 'PAYCHCOSCH'://School continuation
                $input = ['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'employee_id' => $employee_id, 'deleted_by' => $status_change->user_id, 'delete_reason' => 'Child reinstate for school continuation'];
                /*Remove suspended payments*/
//                (new PayrollSuspendedRunRepository())->removeSuspendedPayments($input);

                /*Update deadline*/
                (new DependentRepository())->updateDeadlineDate($resource_id, $employee_id, $deadline_date);

                /*Update education status*/
                (new DependentRepository())->updateEducationStatus($resource_id, 1);
                break;


            case 'PAYCHCDIS'://Disability
                (new DependentRepository())->updateIsDisabledStatus($resource_id,1);
                /*deadline*/
                if($child_extension->deadline_date){
                    (new DependentRepository())->updateDeadlineDate($resource_id, $employee_id, $deadline_date);
                }

                break;
        }
        /*Reinstate removed*/
        $this->reinstateRemoved($member_type_id, $resource_id, $employee_id);
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * Get resource based on the member type
     */
    public function getResource($member_type_id, $resource_id)
    {
        $payroll_repo = new PayrollRepository();
        $resource = $payroll_repo->getResource($member_type_id, $resource_id);
        return $resource;
    }








    /**
     * @param $member_type_id
     * @param $resource_id
     *
     * Get By beneficiary for datatable
     */
    public function getByBeneficiaryForDataTable($member_type_id, $resource_id)
    {
        return $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id);
    }

    /**
     * Get activation and deactivation per beneficiary per notification report
     */
    public function getActivationsForDataTable($member_type_id, $resource_id, $employee_id)
    {
        $status_types []  = $this->code_values->findByReference('PSCACT')->id;//activation
        $status_types []= $this->code_values->findByReference('PSCDEACT')->id;//deactivation

        return $this->getByBeneficiaryForDataTable($member_type_id, $resource_id)->whereIn('status_change_cv_id', $status_types)->where('employee_id', $employee_id);
    }

    /**
     * Get suspensions and reinstates per beneficiary per notification report
     */
    public function getSuspensionsReinstatesForDataTable($member_type_id, $resource_id, $employee_id)
    {
        $status_types []  = $this->code_values->findByReference('PSCREIN')->id;//reinstate
        $status_types []= $this->code_values->findByReference('PSCMASU')->id;//suspend
        $status_types []= $this->code_values->findByReference('PSCCHDREI')->id;//reinstate child continuation

        return $this->getByBeneficiaryForDataTable($member_type_id, $resource_id)->whereIn('status_change_cv_id', $status_types)->where('employee_id', $employee_id);
    }



}