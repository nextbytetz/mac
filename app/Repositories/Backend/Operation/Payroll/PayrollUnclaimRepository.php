<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollUnclaimRepository extends  BaseRepository
{

    const MODEL = PayrollUnclaim::class;




    /*Find payroll unclaim  by recovery id*/
    public function findByRecoveryId($payroll_recovery_id)
    {
        $arrear = $this->query()->where('payroll_recovery_id', $payroll_recovery_id)->first();
        return $arrear;
    }

    /*
     * create new
     */

    public function create(Model $payroll_recovery)
    {
        return DB::transaction(function () use ($payroll_recovery) {
            $deduction = $this->query()->create([
                'payroll_recovery_id' => $payroll_recovery->id,
                'amount' => $this->findAmountPerCycle($payroll_recovery),
                'recycles' => $payroll_recovery->cycles,
            ]);

            return $deduction;
        });
    }

    /*Find amount per cycle/month to be deducted*/
    public function findAmountPerCycle(Model $payroll_recovery)
    {
        $total_amount = $payroll_recovery->total_amount;
        $cycles = $payroll_recovery->cycles;
        $amount_per_cycle = $total_amount / $cycles;

        return round($amount_per_cycle,2);
    }


    /*
     * update
     */
    public function update($id,$input) {

    }


    /*Query for getting active unclaims */
    public function queryForActiveUnclaims()
    {
        return $this->query()->where('isactive', 1)->where('recycles', '>', 0);
    }


    /*Get active /pending unclaims for specified beneficiary*/
    public function getActiveUnclaimsCount($member_type_id, $resource_id)
    {
        $code_values = new CodeValueRepository();
        $recovery_type_cv_id = $code_values->findByReference('PRTUNCLP')->id;
        return $this->queryForActiveUnclaims()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $recovery_type_cv_id){
            $query->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->where('recovery_type_cv_id', $recovery_type_cv_id);
        })->count();
    }



    /*Update Recycles*/
    public function updateRecycle($payroll_recovery_id)
    {
        $unclaim = $this->findByRecoveryId($payroll_recovery_id);
        $isactive = ($unclaim->recycles > 1) ? 1 : 0;
        $unclaim->update([
            'recycles' =>$unclaim->recycles - 1,
            'isactive' => $isactive,
        ]);
    }




    /*Get un-recovered un-claims for a member*/
    public function getNetPendingUnclaimsAmountForMember($member_type_id, $resource_id, $employee_id)
    {
        $code_values = new CodeValueRepository();
        $recovery_type_cv_id = $code_values->findByReference('PRTUNCLP')->id;
        $pending_unclaims = $this->queryForActiveUnclaims()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $recovery_type_cv_id, $employee_id){
            $query->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->where('recovery_type_cv_id', $recovery_type_cv_id)->where('employee_id', $employee_id);
        })->get();
        $total_balance = 0;
        foreach ($pending_unclaims as $pending_unclaim){
            $total_balance = $total_balance + ($this->calculateBalanceAmount($pending_unclaim->id));
        }
        return $total_balance;
    }

    /**
     * @param $id
     * Calculate pending amount to be recovered
     */
    public function calculateBalanceAmount($id)
    {
        $payroll_recovery_trans = new PayrollRecoveryTransactionRepository();
        $payroll_unclaim = $this->find($id);
        $total_amount = $payroll_unclaim->payrollRecovery->total_amount;
        $recovered_amount = $payroll_recovery_trans->getTotalAmountRecoveredForRecovery($payroll_unclaim->payroll_recovery_id);
        $balance = $total_amount - $recovered_amount;
        return $balance;
    }



}