<?php

namespace App\Repositories\Backend\Operation\Payroll\Suspension;

use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\Suspension\PayrollManualSuspension;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Operation\Payroll\Suspension\PayrollSystemSuspension;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Repositories\Backend\Operation\Payroll\PayrollVerificationRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollManualSuspensionRepository extends  BaseRepository
{

    const MODEL = PayrollManualSuspension::class;



    public function __construct()
    {

    }


    /*
     * create new
     */
    public function create(array $input)
    {
        return DB::transaction(function () use ($input) {
            $manual_suspension = $this->query()->create([
                'user_id' => access()->id(),
                'run_date' => ($input['run_date']),
                'remark' => ($input['remark']),
            ]);
            return $manual_suspension;
        });
    }




    /*Get all for datatable*/
    public function getForDataTable()
    {
        return $this->query();
    }




    /*Suspend dependents and pensioners who are unverified*/
    public function suspendUnverifiedBeneficiaries($id)
    {
        DB::transaction(function () use ($id) {
            $suspension = $this->find($id);
            $verification_days = (new PayrollVerificationRepository())->getDaysForVerification();
            $limit_date = Carbon::now()->subDays($verification_days);
            $limit_date = '\'' . standard_date_format($limit_date) .'\'';
            $suspend_reason = '\'Suspension - Unverified\'';

            /*Dependents*/
            $this->suspendUnverifiedDependents($id, $limit_date, $suspend_reason);
            /*Pensioners*/
            $this->suspendUnverifiedPensioners($id, $limit_date, $suspend_reason);

        });
    }


    /*Suspend Unverified dependents bulk*/
    public function suspendUnverifiedDependents($id, $limit_date, $suspend_reason)
    {
        $member_type_id = 4;
        $sql = <<<SQL
        
update dependents set suspense_flag = 1 ,suspension_reason = {$suspend_reason}, suspended_date = now()   where lastresponse::date < {$limit_date} and id in (select dependent_id from dependent_employee where isactive = 1 and survivor_pension_amount > 0 ) ;

insert into payroll_system_suspensions (resource_id, member_type_id, created_at, payroll_manual_suspension_id)
select id, {$member_type_id},now(), {$id} from dependents where  lastresponse::date < {$limit_date} and id in (select dependent_id from dependent_employee where isactive = 1 and survivor_pension_amount > 0 );
SQL;

        DB::unprepared($sql);
    }

    /*Suspend Unverified pensioners bulk*/
    public function suspendUnverifiedPensioners($id, $limit_date, $suspend_reason)
    {
        $member_type_id = 5;
        $sql = <<<SQL
        
        update pensioners set suspense_flag = 1 ,suspension_reason = {$suspend_reason} , suspended_date = now()  where lastresponse::date < {$limit_date} and isactive = 1;

insert into payroll_system_suspensions (resource_id, member_type_id, created_at, payroll_manual_suspension_id)
select id, {$member_type_id},now(), {$id} from pensioners where  lastresponse::date < {$limit_date} and isactive = 1
SQL;

        DB::unprepared($sql);
    }


}