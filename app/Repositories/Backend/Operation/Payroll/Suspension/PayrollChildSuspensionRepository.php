<?php

namespace App\Repositories\Backend\Operation\Payroll\Suspension;

use App\Exceptions\GeneralException;
use App\Jobs\RunPayroll\Suspension\ProcessChildSuspension;
use App\Models\Operation\Payroll\Suspension\PayrollChildSuspension;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PayrollChildSuspensionRepository extends  BaseRepository
{

    const MODEL = PayrollChildSuspension::class;

    protected $payroll_status_changes;
    protected $dependents;

    public function __construct()
    {
        $this->payroll_status_changes = new PayrollStatusChangeRepository();
        $this->dependents = new DependentRepository();
    }

    /*
     * create new
     */

    public function create(array $input)
    {
        if($this->checkDoesNotIfExist($input)){
            return DB::transaction(function () use ($input) {

                /*Delete exists*/
                $this->deleteExists($input);

                /*create new entry*/
                $child_suspension = $this->query()->create([
                    'dependent_id' => $input['dependent_id'],
                    'iseducation' => $input['iseducation'],
                    'reason' => $input['reason'],
                ]);

                return $child_suspension;

            });
        }

        /*Suspend*/
        $this->suspend(4,$input['dependent_id'],  $input['reason']);
    }




    /*Check if dependent does not exist*/
    public function checkDoesNotIfExist( array $input)
    {
        $suspension = $this->query()->where('dependent_id', $input['dependent_id'])->where('reason', $input['reason'])->first();
        if($suspension){
            return false;
        }else{
            return true;
        }
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * Suspend beneficiary
     */
    public function suspend($member_type_id, $resource_id, $reason = null)
    {
        /*suspend resource*/
        $resource = $this->payroll_status_changes->getResource($member_type_id, $resource_id);
        $this->payroll_status_changes->suspend($resource,$reason);
        /*end suspend*/
    }


    /*Delete exists*/
    public function deleteExists(array $input)
    {
        $this->query()->where('dependent_id', $input['dependent_id'])->update([
            'deleted_at' => Carbon::now(),
        ]);
    }

    /**
     * Process job for auto child suspension who reach 18years old
     */
    public function processChildLimitAgeSuspensionJob()
    {
        /*Suspend child dependents - 18yrs*/
        $limit_age = sysdefs()->data()->payroll_child_limit_age;//18
        $today = Carbon::now();
        $limit_age_dob =  Carbon::now()->subYears($limit_age)->startOfMonth();
        $child_dependents = $this->dependents->getChildDependentsForSuspension()->where('dependents.isdisabled', 0)->where('dependents.iseducation', 0)->whereDate('dependents.dob', '<', $limit_age_dob);
        $child_dependents->select(['dependents.id'])->orderBy('dependents.id')->chunk(100, function ($child_dependents) {
            if (isset($child_dependents)) {
                dispatch(new ProcessChildSuspension($child_dependents, 0, '18yr Limit'));
            }
        });


        /*Suspend all passed deadline  for education extension*/
        $child_dependents_with_extension = $this->dependents->getChildDependentsForSuspension()->where('dependents.iseducation', 1)->whereNotNull('dependent_employee.deadline_date')->where('dependent_employee.deadline_date', '<=', standard_date_format(getTodayDate()));
        $child_dependents_with_extension->select(['dependents.id'])->orderBy('dependents.id')->chunk(100, function ($child_dependents) {
            if (isset($child_dependents)) {
                dispatch(new ProcessChildSuspension($child_dependents, 1, 'Education extension passed deadline'));
            }
        });

        /*Suspend all passed deadline  for disability*/
        $child_dependents_with_extension = $this->dependents->getChildDependentsForSuspension()->where('dependents.isdisabled', 1)->whereNotNull('dependent_employee.deadline_date')->where('dependent_employee.deadline_date', '<=', standard_date_format(getTodayDate()));
        $child_dependents_with_extension->select(['dependents.id'])->orderBy('dependents.id')->chunk(100, function ($child_dependents) {
            if (isset($child_dependents)) {
                dispatch(new ProcessChildSuspension($child_dependents, 0, 'Disability extension passed deadline'));
            }
        });


        /*Suspend all granted education on 21yrs old*/
//        $limit_education_age = sysdefs()->data()->payroll_child_education_limit_age;//18
//        $limit_education_age_dob =  Carbon::now()->subYears($limit_education_age);
//        $child_education_dependents = $this->dependents->getChildDependentsForSuspension()->where('iseducation', 1)->whereNotNull('dependent_employee.deadline_date')->whereDate('dependents.dob', '<', $limit_education_age_dob);
//        $child_education_dependents->select(['dependents.id'])->orderBy('dependents.id')->chunk(100, function ($child_dependents) {
//            if (isset($child_dependents)) {
//                dispatch(new ProcessChildSuspension($child_dependents, 1, '21yr Limit'));
//            }
//        });
    }


    /*Get all for dataTable*/
    public function getForDataTable()
    {
        return $this->query()->select([
            DB::raw("INITCAP(concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname)) as fullname"),
            DB::raw("dependents.firstname"),
            DB::raw("dependents.middlename"),
            DB::raw("dependents.lastname"),
            DB::raw("dependents.dob as dob"),
            DB::raw("dependents.isdisabled as isdisabled"),
            DB::raw("dependents.iseducation as iseducation"),
            DB::raw("dependents.suspense_flag as suspense_flag"),
            DB::raw("dependent_employee.isactive as isactive"),
            DB::raw("payroll_child_suspensions.created_at as suspended_date"),
            DB::raw("payroll_child_suspensions.id as id"),
            DB::raw("payroll_child_suspensions.dependent_id as dependent_id"),
            DB::raw("payroll_child_suspensions.iseducation as iseducation"),
            DB::raw("dependent_employee.id as pivot_id"),
            DB::raw("payroll_child_suspensions.created_at as suspended_date"),
            DB::raw("payroll_child_suspensions.reason as reason"),
            DB::raw("b.filename as filename"),
        ])
            ->join("dependents", "payroll_child_suspensions.dependent_id", "dependents.id")
            ->join("dependent_employee", "payroll_child_suspensions.dependent_id", "dependent_employee.dependent_id")
            ->join('payroll_beneficiaries_view as b', function($join){
                $join->on('b.resource_id', 'dependents.id')->where('b.member_type_id', 4)->whereRaw("b.employee_id = dependent_employee.employee_id");
            });
    }


    /**
     * @return mixed
     * Get Child suspension which await action i.e. verified / disabled
     */
    public function getSuspensionPendingAction()
    {
        return $this->getForDataTable()->where('dependent_employee.isactive', 1)->where('dependents.suspense_flag', 1);
    }


    /*Get child dependents alerts (Approaching 18yrs) who need to be verified in next two months*/
    public function getChildLimitAgeAlertsForDataTable()
    {
        $limit_age = sysdefs()->data()->payroll_child_limit_age;//18
        $today = Carbon::now();
        $limit_age_dob = Carbon::now()->subYears($limit_age)->startOfMonth();
        $alert_months = sysdefs()->data()->payroll_suspension_alert_months;
        $limit_age_dob_alert = $limit_age_dob->addMonth($alert_months);
        $child_dependents = $this->dependents->getChildDependentsForSuspension()->whereDate('dependents.dob', '<', $limit_age_dob_alert)->where('dependents.iseducation', 0)->select([
            DB::raw("INITCAP(concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname)) as fullname"),
            DB::raw("dependent_employee.id as pivot_id"),
            DB::raw("dependent_employee.dependent_id as dependent_id"),
            DB::raw("dependents.dob as dob"),
            DB::raw("b.filename as filename"),
        ]);
        return $child_dependents;
    }


    /*Get child with granted extension*/
    public function getChildWithGrantedExtensionForDt($child_continuation_cv_ref)
    {
        switch($child_continuation_cv_ref)
        {
            case 'PAYCHCOSCH':
                return $this->getChildWithEducationExtensionDeadlineAlertsForDataTable();
                break;

            case 'PAYCHCDIS':
                return $this->getChildWithDisabilityExtensionDeadlineAlertsForDataTable();
                break;
        }
    }

    /*Get child dependents alerts (Approaching Education extensions) who need to be verified in next two months*/
    public function getChildWithEducationExtensionDeadlineAlertsForDataTable()
    {
        $alert_months = sysdefs()->data()->payroll_suspension_alert_months;
        $limit_date_alert = Carbon::now()->addMonth($alert_months);
        $child_dependents = $this->dependents->getChildDependentsForSuspension()->whereNotNull('dependent_employee.deadline_date')->whereDate('dependent_employee.deadline_date', '<=', $limit_date_alert)->where('dependents.iseducation', 1)->select([
            DB::raw("INITCAP(concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname)) as fullname"),
            DB::raw("dependent_employee.id as pivot_id"),
            DB::raw("dependent_employee.dependent_id as dependent_id"),
            DB::raw("dependents.dob as dob"),
            DB::raw("dependent_employee.deadline_date as deadline_date"),
        ]);
        return $child_dependents;
    }

    /*Get child dependents alerts (Approaching Education extensions) who need to be verified in next two months*/
    public function getChildWithDisabilityExtensionDeadlineAlertsForDataTable()
    {
        $alert_months = sysdefs()->data()->payroll_suspension_alert_months;
        $limit_date_alert = Carbon::now()->addMonth($alert_months);
        $child_dependents = $this->dependents->getChildDependentsForSuspension()->whereNotNull('dependent_employee.deadline_date')->whereDate('dependent_employee.deadline_date', '<=', $limit_date_alert)->where('dependents.isdisabled', 1)->select([
            DB::raw("INITCAP(concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname)) as fullname"),
            DB::raw("dependent_employee.id as pivot_id"),
            DB::raw("dependent_employee.dependent_id as dependent_id"),
            DB::raw("dependents.dob as dob"),
            DB::raw("dependent_employee.deadline_date as deadline_date"),
        ]);
        return $child_dependents;
    }
}