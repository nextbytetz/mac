<?php

namespace App\Repositories\Backend\Operation\Payroll\Suspension;

use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\Suspension\PayrollSystemSuspension;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollSystemSuspensionRepository extends  BaseRepository
{

    const MODEL = PayrollSystemSuspension::class;


    protected $payroll_status_changes;

    public function __construct()
    {
        $this->payroll_status_changes = new PayrollStatusChangeRepository();

    }


    /*
     * create new
     */

    public function create($resource_id, $member_type_id, $payroll_manual_suspension_id = null)
    {
        return DB::transaction(function () use ($resource_id, $member_type_id,$payroll_manual_suspension_id) {
            /*Save system suspension*/
            $system_suspension = $this->query()->create([
                'member_type_id' => $member_type_id,
                'resource_id' => $resource_id,
                'payroll_manual_suspension_id' => $payroll_manual_suspension_id,

            ]);

            /*Suspend*/
            $this->suspend($member_type_id,$resource_id);

//            return $system_suspension;
        });
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * Suspend beneficiary
     */
    public function suspend($member_type_id, $resource_id)
    {
        /*suspend resource*/
        $resource = $this->payroll_status_changes->getResource($member_type_id, $resource_id);
        $this->payroll_status_changes->suspend($resource);
        /*end suspend*/
    }


    /*Get All suspensions by manual suspensions for datatable*/
    public function getAllByManualSuspensionForDt($manual_suspension_id)
    {
return $this->query()->select(
    'b.resource_id',
    'b.member_type_id',
    'b.employee_id',
    'b.member_name',
    'b.lastresponse'
)
    ->join('payroll_beneficiaries_view as b', function($join){
        $join->on('b.resource_id', 'payroll_system_suspensions.resource_id')->on('b.member_type_id', 'payroll_system_suspensions.member_type_id');
    })->where('payroll_system_suspensions.payroll_manual_suspension_id', $manual_suspension_id);
    }


}