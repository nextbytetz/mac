<?php

namespace App\Repositories\Backend\Operation\Payroll\Suspension;

use App\Exceptions\GeneralException;
use App\Jobs\ProcessChildSuspension;
use App\Jobs\RunPayroll\Suspension\ProcessRetireeSuspension;
use App\Models\Operation\Payroll\Suspension\PayrollChildSuspension;
use App\Models\Operation\Payroll\Suspension\PayrollManualSuspension;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Operation\Payroll\Suspension\PayrollRetireeSuspension;
use App\Models\Operation\Payroll\Suspension\PayrollSystemSuspension;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollRetireeSuspensionRepository extends  BaseRepository
{

    const MODEL = PayrollRetireeSuspension::class;

    protected $payroll_status_changes;
    protected $pensioners;

    public function __construct()
    {
        $this->payroll_status_changes = new PayrollStatusChangeRepository();
        $this->pensioners = new PensionerRepository();
    }

    /*
     * create new
     */

    public function create(array $input)
    {
        if($this->checkDoesNotIfExist($input['pensioner_id'], $input['isvoluntary'])) {
            return DB::transaction(function () use ($input) {
                $retiree_suspension = $this->query()->create([
                    'pensioner_id' => $input['pensioner_id'],
                    'isvoluntary' => $input['isvoluntary']
                ]);
                /*Suspend*/
                $this->suspend(5, $retiree_suspension->pensioner_id);
                return $retiree_suspension;
            });
        }
    }

    /*Check if pensioner does not exist for the same case i.e. voluntary/compulsory*/
    public function checkDoesNotIfExist($pensioner_id, $isvoluntary)
    {
        $suspension = $this->query()->where('pensioner_id', $pensioner_id)->where('isvoluntary', $isvoluntary)->first();
        if($suspension){
            return false;
        }else{
            return true;
        }
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * Suspend beneficiary
     */
    public function suspend($member_type_id, $resource_id)
    {
        /*suspend resource*/
        $resource = $this->payroll_status_changes->getResource($member_type_id, $resource_id);
        $this->payroll_status_changes->suspend($resource);
        /*end suspend*/
    }


    /**
     * Process job for auto retiree suspension who reach 55/60years old
     */
    public function processRetireeSuspensionJob()
    {
        $limit_voluntary_age = sysdefs()->data()->payroll_voluntary_retire_age;//55
        $today = Carbon::now();
        $limit_voluntary_age_dob =  Carbon::now()->subYears($limit_voluntary_age);
        $voluntary_pensioners = $this->pensioners->queryEligibleForPayroll()->whereDate('dob', '<', $limit_voluntary_age_dob);
        /*Suspend voluntary retiree*/
        $voluntary_pensioners->select(['id'])->chunk(100, function ($pensioners) {
            if (isset($pensioners)) {
                        dispatch(new ProcessRetireeSuspension($pensioners, 1));
            }
        });
        //Suspend compulsory retiree
        $limit_compulsory_age = sysdefs()->data()->payroll_compulsory_retire_age;//60
        $limit_compulsory_age_dob =  Carbon::now()->subYears($limit_compulsory_age);
        $compulsory_pensioners = $this->pensioners->queryEligibleForPayroll()->whereDate('dob', '<', $limit_compulsory_age_dob);
        $compulsory_pensioners->select(['id'])->chunk(100, function ($pensioners) {
            if (isset($pensioners)) {
                dispatch(new ProcessRetireeSuspension($pensioners, 0));
            }
        });
    }


    /*Get all for datatable*/
    public function getForDataTable()
    {
        return $this->query()->select([
            DB::raw("INITCAP(concat_ws(' ', pensioners.firstname, coalesce(pensioners.middlename, ''), pensioners.lastname)) as fullname"),
            DB::raw("pensioners.firstname"),
            DB::raw("pensioners.middlename"),
            DB::raw("pensioners.lastname"),
            DB::raw("pensioners.dob as dob"),
            DB::raw("pensioners.isactive as isactive"),
            DB::raw("pensioners.suspense_flag as suspense_flag"),
            DB::raw("payroll_retiree_suspensions.created_at as suspended_date"),
            DB::raw("payroll_retiree_suspensions.id as id"),
            DB::raw("payroll_retiree_suspensions.pensioner_id as pensioner_id"),

        ])
            ->join("pensioners", "payroll_retiree_suspensions.pensioner_id", "pensioners.id");



    }


    /*Get retiree suspensions pending for further action i.e. */
    public function getPendingSuspensionForAction()
    {
        $retiree_suspensions = $this->getForDataTable()->where('payroll_retiree_suspensions.isactive', 1);
        return $retiree_suspensions;
    }


/*update status*/
    public function updateStatus($pensioner_id)
    {
        $suspension = $this->query()->where('pensioner_id', $pensioner_id)->where('isactive', 1)->update([
            'isactive' => 0,
        ]);
    }

}