<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Payroll\ManualPayrollRun;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ManualPayrollRunRepository extends  BaseRepository
{
    use FileHandler, AttachmentHandler;

    const MODEL = ManualPayrollRun::class;

    protected $document_name = 'manual_payroll_runs.xlsx';

    public function __construct()
    {

    }

    public function uploadedFileDir()
    {
        return manual_files_dir() . DIRECTORY_SEPARATOR . $this->document_name;
    }


    public function uploadedFileUrl()
    {
        return manual_files_url() . DIRECTORY_SEPARATOR . $this->document_name;
    }

    /*
     * create new
     */

    public function create(array $input)
    {
        return DB::transaction(function () use ($input) {

        });
    }



    /*
     * update
     */
    public function update($id,$input) {

        return DB::transaction(function () use ($id,$input) {

        });
    }


    /**
     * @throws WorkflowException
     * Sync manual payroll runs with member
     */
    public function syncManualPayrollRunsWithMember($input)
    {
        $sync_category = $input['category'];

        switch($sync_category){

            case 1://Specified member
                $this->checkSyncFormValidation($input);

                if(isset($input['unsync_flag'])){
                    /*Unsync selected member*/
                    $this->unSyncSelectedMemberToManualPension($input);

                }else{
                    /*Sync selected member to manual pensions picked*/
                    $this->syncSelectedMemberToManualPension($input);
                }

                break;

            case 2://All pensioners by case no
                $this->syncAllPensionersByCaseNo();
                break;
        }
    }

    /*Check sync form validation*/
    public function checkSyncFormValidation($input)
    {
        if(!isset($input['resource_id']))
        {
            throw new WorkflowException("Member not selected! Please check!");
        }

        if(!isset($input['id']))
        {
            throw new WorkflowException("Select at least one manual payroll pension for synchronization! Please check!");
        }

    }

    /*Sync selected member to manual pension*/
    public function syncSelectedMemberToManualPension($input)
    {

        foreach ($input['id'] as $manual_payroll_run_id) {
            $manual_payroll_run = $this->find($manual_payroll_run_id);
            $manual_payroll_run->update([
                'resource_id' => $input['resource_id'],
                'employee_id' => $input['employee_id'],
            ]);
        }
    }


    /*Sync selected member to manual pension*/
    public function unSyncSelectedMemberToManualPension($input)
    {

        foreach ($input['id'] as $manual_payroll_run_id) {
            $manual_payroll_run = $this->find($manual_payroll_run_id);
            $manual_payroll_run->update([
                'resource_id' => null,
                'employee_id' => null,
            ]);
        }
    }
    /**
     * Sync all pensioners by case no
     */
    public function syncAllPensionersByCaseNo()
    {

        /*Update manual payroll runs -> resource*/
        DB::statement("UPDATE manual_payroll_runs r
SET resource_id = m.resource_id, employee_id = p.employee_id
FROM manual_payroll_members m
join pensioners p on m.resource_id = p.id and m.member_type_id = 5
WHERE (cast( r.case_no as varchar(30))) = (cast( m.case_no as varchar(30)))  and r.incident_type_id = m.incident_type_id  and r.resource_id is null and r.employee_id is null");
    }

    /*Get Manual Payroll runs by beneficiary dt*/
    public function getManualPayrollRunsByBeneficiaryDt($member_type_id, $resource_id, $employee_id)
    {
        return $this->getForDataTable()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
    }

    /*Get for datatable*/
    public function getForDataTable()
    {
        return $this->query();
    }

    /*Get for payroll beneficiaries from manual for dt*/
    public function getPayrollBeneficiariesFromManualDt()
    {
        return DB::table('payroll_beneficiaries_view as b')->whereRaw("(b.firstpay_manual = 1 or (b.firstpay_manual = 0 and b.isactive > 0 and b.monthly_pension_amount > 0))");
    }


    /*Get maual payroll runs by request*/
    public function getManualPayrollRunsByRequestForDt(array $input)
    {
        $base_query = $this->getForDataTable()->whereNull('manual_payroll_runs.upload_error');

        if(isset($input['search_member_name']))
        {
            $base_query = $base_query->whereRaw(' manual_payroll_runs.payee_name ~* ?', [$input['search_member_name']]);
        }
        if(isset($input['member_type_id']))
        {
            $base_query = $base_query->whereRaw(' manual_payroll_runs.member_type_id = ?', [$input['member_type_id']]);
        }
        if(isset($input['case_no']))
        {
            $base_query = $base_query->whereRaw(' manual_payroll_runs.case_no = ?', [$input['case_no']]);
        }

        if(isset($input['incident_type_id']))
        {
            $base_query = $base_query->whereRaw(' manual_payroll_runs.incident_type_id = ?', [$input['incident_type_id']]);
        }

        if(isset($input['other_criteria']))
        {
            $other_criteria = $input['other_criteria'];
            if($other_criteria == 1){
                /*Not Synced*/
                $base_query = $base_query->whereNull('manual_payroll_runs.resource_id');
            }elseif($other_criteria == 2){
                /*synced*/
                $base_query = $base_query->whereNotNull('manual_payroll_runs.resource_id')->whereRaw("manual_payroll_runs.payee_name ~* ? or  (manual_payroll_runs.resource_id = ? ) ",[$input['search_member_name'], $input['resource_id']]);
            }elseif($other_criteria == 3){
                /*sync all pensioners*/
                $base_query = $base_query->where('manual_payroll_runs.member_type_id', 5)->whereNull('manual_payroll_runs.resource_id');
            }
        }

        return $base_query;
    }


    /**
     * @return array
     * Member sync member summary details
     */
    public function getSyncMemberSummaryDetails()
    {
        $synced = DB::table('payroll_beneficiaries_view as b')->select('b.member_type_id', 'b.resource_id', 'b.employee_id')
            ->join('manual_payroll_runs as r', function($join){
                $join->on('r.resource_id', 'b.resource_id')->whereRaw(" b.member_type_id = r.member_type_id")->whereRaw(" b.employee_id = r.employee_id");
            })->where('b.firstpay_manual', 1)->groupBy('b.member_type_id', 'b.resource_id', 'b.employee_id')->get()->count();
        $not_synced = DB::table('payroll_beneficiaries_view as b')->select('b.member_type_id', 'b.resource_id', 'b.employee_id')
            ->leftJoin('manual_payroll_runs as r', function($join){
                $join->on('r.resource_id', 'b.resource_id')->whereRaw(" b.member_type_id = r.member_type_id")->whereRaw(" b.employee_id = r.employee_id");
            })->whereNull('r.resource_id')->where('b.firstpay_manual', 1)->groupBy('b.member_type_id', 'b.resource_id', 'b.employee_id')->get()->count();

        return [
            'member_synced' => $synced,
            'member_not_synced' => $not_synced,
        ];
    }

    /**\
     * @param array $input
     * Upload bulk files from excel
     */
    public function saveUploadedDocumentFromExcel(array $input)
    {
        DB::transaction(function () use ($input) {
            $base = manual_files_dir();
            $file = $this->uploadedFileDir();
            $request_file_name = 'document_file';
            /*check if exist*/
            if (request()->hasFile($request_file_name)) {
                /* If has file: remove and replace */
                if (file_exists($file)) {
                    unlink($file);
                }

                /*Save*/
//                $document_name = '';
                $this->saveDocumentGeneral($request_file_name, $this->document_name, $base);
                /*Upload manual files into table*/
                $this->uploadBulkManualFiles($input);
            }
            /* end replace if exist*/

        });
    }

    /**
     * @param array $input
     * Upload files from save excel document
     */
    public function uploadBulkManualFiles(array $input)
    {
//        try {
        $member_type_id = $input['member_type_id'];
        $user_id = access()->id();
        $uploaded_file = $this->uploadedFileDir();

        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($uploaded_file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();

        if($member_type_id == 5){
            /*Pensioners*/
            $verifyArr = ['case_no','incident_type','employee_name', 'employer_name', 'bank_name','accountno','payroll_month', 'monthly_pension', 'remark'];
        }else{
            $verifyArr = [ 'survivor_name', 'employee_name','employer_name','bank_name', 'accountno', 'payroll_month','monthly_pension', 'remark'];
        }

        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new GeneralException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }

        }
        /** end : Check if all excel headers are present */

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($uploaded_file)
            ->chunk(250, function($result) use($user_id, $uploaded_file, $member_type_id)  {
                $rows = $result->toArray();
                try {

                    //let's do more processing (change values in cells) here as needed
                    //$counter = 0;
                    foreach ($rows as $row) {
                        /* Changing date from excel format to unix date format ... */
//                        $row['payroll_month'] = PHPExcel_Style_NumberFormat::toFormattedString($row['payroll_month'], 'YYYY-MM-DD');
                        $row['payroll_month'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['payroll_month'], 'YYYY-MM-DD');
                        /* start: Validating row entries */
                        $error_report = null;
                        $error = 0;
                        foreach ($row as $key => $value) {
                            if (trim($key) == 'payroll_month') {
                                if (check_date_format($value) == 0 && isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            } elseif (in_array(trim($key), ['monthly_pension'], true)) {
                                if (!is_numeric($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            }
                        }
                        $incident_id = null;
                        $incident = isset($row['incident_type']) ? strtolower(trim($row['incident_type'])) : null;
                        if ($incident == 'occupational accident' || $incident == 'occupation accident') {
                            $incident_id = 1;
                        } elseif ($incident == 'occupational disease' || $incident == 'occupation disease') {
                            $incident_id = 2;
                        } elseif ($incident == 'occupational death' || $incident == 'occupation death') {
                            $incident_id = 3;
                        }
                        $payee = ($member_type_id == 5) ? $row['employee_name'] : $row['survivor_name'];
                        $incident_id = ($member_type_id == 4) ? 3 : $incident_id;
                        $monthly_pension = remove_thousands_separator($row['monthly_pension']);
                        /* end: Validating row entries */
                        /* insert into db */
                        $data = ['employer_name' => trim($row['employer_name']), 'employee_name' => trim($row['employee_name']), 'payee_name' => trim($payee)   , 'case_no' => $row['case_no'] ?? null, 'incident_type' => isset($row['incident_type']) ? trim($row['incident_type']) : null, 'incident_type_id' => $incident_id, 'bank_name' => $row['bank_name'], 'accountno' => $row['accountno'], 'payroll_month' => ($row['payroll_month']), 'monthly_pension' => $monthly_pension ,'net_amount' => $monthly_pension, 'user_id' => $user_id, 'upload_error' => $error_report, 'member_type_id' => $member_type_id, 'remark' => $row['remark']  ];
                        /*Create notification report*/
                        $find_exist =$this->findManualPayrollRunExist(trim($payee), ($row['payroll_month']));

                        if (!$find_exist) {
                            $this->query()->create($data);
                        }else{
                            if(!$find_exist->resource_id){
                                /*update if not synced*/
                                $find_exist->update($data);
                            }

                        };
                    }
                } catch (\Exception $e) {
                    // an error occurred
                    $error_message = $e->getMessage();
//                    $this->saveExceptionMessage($employer, $error_message);
                    log_info(print_r($error_message,true));
                }


            }, true);

    }


    /**
     * @param $payee
     * @param $payroll_month
     * @return bool
     * cHECK IF MANUAL payroll run does not exists
     */
    public function findManualPayrollRunExist($payee, $payroll_month)
    {
//        $check = $this->query()->where('payee_name', $payee)->where('payroll_month', $payroll_month)->first();
        $payroll_month_parsed = Carbon::parse($payroll_month);
        $check = $this->query()->where('payee_name', $payee)->whereMonth('payroll_month','=', $payroll_month_parsed->format('m'))->whereYear('payroll_month','=', $payroll_month_parsed->format('Y'))->first();
        return $check;
    }

}