<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Jobs\RunPayroll\PostPayrollAlertTaskChecker;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollBankUpdateRepository extends  BaseRepository
{

    const MODEL = PayrollBankInfoUpdate::class;

    protected $pensioners;
    protected $dependents;

    public function __construct()
    {
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
    }



    /*
     * create new
     */

    public function create($resource_id, array $input)
    {
        return DB::transaction(function () use ($input, $resource_id) {
            $bank_update = $this->query()->create([
                'resource_id' => $resource_id,
                'member_type_id' => $input['member_type_id'],
                'old_bank_id' => $input['old_bank_id'],
                'old_bank_branch_id' => isset($input['old_bank_branch_id']) ? $input['old_bank_branch_id'] : null,
                'old_accountno' => $input['old_accountno'],
                'new_bank_id' => $input['new_bank'],
                'new_bank_branch_id' => isset($input['new_branch']) ? $input['new_branch'] : null,
                'new_accountno' => $input['new_accountno'],
                'folionumber' => $input['folionumber'],
                'user_id' => access()->id(),
                'employee_id' => $input['employee_id'],
            ]);

            /* - Close payroll alert checker <payroll_inbox_task>*/
            $stage_cv_ref ='PALEPEDBAN';
            dispatch(new PostPayrollAlertTaskChecker($stage_cv_ref, $input['member_type_id'], $resource_id,1));
            /*Suspend beneficiary until approval complete*/
            $this->suspendOnCreate($bank_update->member_type_id, $bank_update->resource_id);

            return $bank_update;
        });
    }


    /**
     * @param array $input
     * Update bank details when beneficiary not in payroll yet - This no need for workflow
     */
    public function updateWhenBeneficiaryIsNotActive(array $input, $resource_id)
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($input['member_type_id'], $resource_id);
        /*update to resource table*/
        $resource->update([
            'bank_id'=> $input['new_bank'],
            'bank_branch_id'=> isset($input['new_branch']) ? $input['new_branch'] : null,
            'accountno'=> $input['new_accountno'],
        ]);

        return $resource;
    }


    /*
     * update
     */
    public function update(Model $bank_update,$input)
    {
        return DB::transaction(function () use ($input, $bank_update) {
            $bank_update->update([
                'new_bank_id' => $input['new_bank'],
                'new_bank_branch_id' =>isset($input['new_branch']) ? $input['new_branch'] : null,
                'new_accountno' => $input['new_accountno'],
                'folionumber' => $input['folionumber'],
            ]);
            return $bank_update;
        });
    }


    /**
     * @param Model $bank_update
     * undo bank update
     */
    public function undo(Model $bank_update)
    {

        DB::transaction(function () use ( $bank_update) {
            $wf_modules = new WfModuleRepository();
            $wf_module_group_id = 11;
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $bank_update->id]);
            /*delete bank update*/
            $bank_update->delete();
            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();

            /*Reinstate on undoing*/
            $this->reinstate($bank_update->member_type_id, $bank_update->resource_id);
        });
    }
    /*Suspend when creating new bank update entry*/
    public function suspendOnCreate($member_type_id, $resource_id){
        $payrolls = new PayrollRepository();
        $status_changes = new PayrollStatusChangeRepository();
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        $suspend_reason = 'Suspension for Bank Update';
        $status_changes->suspend($resource, $suspend_reason);
    }

    /*Reinstate when undoing bank update*/
    public function reinstate($member_type_id, $resource_id){
        $payrolls = new PayrollRepository();
        $status_changes = new PayrollStatusChangeRepository();
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        $status_changes->reinstate($resource);
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $document_type
     * Update document after being used to initiate approval for updating beneficiary details
     */
    public function updateBeneficiaryDocumentUsedPerType($member_type_id, $resource_id, $document_type)
    {
        $payroll_repo = new PayrollRepository();
        $payroll_repo->updateBeneficiaryDocumentUsedPerType($member_type_id, $resource_id, $document_type);
    }




    /**
     * @param $resource_id
     * On workflow done update resource i.e. pensioner / dependent their bank details
     * Update bank details
     */
    public function wfDoneComplete($id)
    {

        DB::transaction(function () use ( $id) {
            $status_changes = new PayrollStatusChangeRepository();
            $bank_update = $this->find($id);
            $resource = $this->getResource($bank_update->member_type_id, $bank_update->resource_id);
            $resource->update([
                'bank_id' => $bank_update->new_bank_id,
                'bank_branch_id' => $bank_update->new_bank_branch_id,
                'accountno' => $bank_update->new_accountno
            ]);
            /*Reinstate on complete approval*/
            $status_changes->reinstate($resource);

            /*update document isused status*/
            /*update document used - folio*/
            $document_type = 64;
            $this->updateBeneficiaryDocumentUsedPerType($bank_update->member_type_id, $bank_update->resource_id, $document_type);

            /*For pensioner should update employee table*/
            if($bank_update->member_type_id == 5){
                $input = ['bank_id' => $bank_update->new_bank_id, 'bank_branch_id' => $bank_update->new_bank_branch_id, 'accountno' => $bank_update->new_accountno];
                (new EmployeeRepository())->UpdateDetailsFromPayrollApprovals($resource->employee_id, $input);
            }
        });
    }

    public function wfDoneCompleteDeclined($id)
    {

        DB::transaction(function () use ( $id) {
            $status_changes = new PayrollStatusChangeRepository();
            $bank_update = $this->find($id);
            $resource = $this->getResource($bank_update->member_type_id, $bank_update->resource_id);

            /*Reinstate on complete*/
            $status_changes->reinstate($resource);

        });
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * Get resource based on the member type
     */
    public function getResource($member_type_id, $resource_id)
    {
        $payroll_repo = new PayrollRepository();
        $resource = $payroll_repo->getResource($member_type_id, $resource_id);
        return $resource;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get By beneficiary for datatable
     */
    public function getByBeneficiaryForDataTable($member_type_id, $resource_id)
    {
        return $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id);
    }

    /*Check if can initiate new entry*/
    public function checkIfCanInitiateNewEntry($member_type_id, $resource_id)
    {
        $resource = $this->getResource($member_type_id, $resource_id);
        $this->checkIfThereIsPendingApproval($member_type_id, $resource_id);
        /*check if there is pending suspension*/
        $status_changes = new PayrollStatusChangeRepository();
        $status_changes->checkIfThereIsPendingStatusChange($member_type_id,$resource_id, 'PSCMASU');
        /*Check if is suspended*/
        $this->checkIfBeneficiaryIsSuspended($resource);
    }


    /**
     * Check if Pending approval
     */
    public function checkIfThereIsPendingApproval($member_type_id, $resource_id)
    {
        $pending = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('wf_done', 0)->count();
        if ($pending > 0){
            throw new GeneralException(trans('Can not initiate new entry, there is pending bank update approval! Please check!'));
        }
    }


    /**
     * @param Model $resource
     * Check if beneficiary is suspended - Bank details cn be modified to only active beneficiaries
     */
    public function checkIfBeneficiaryIsSuspended(Model $resource)
    {
        if($resource->suspense_flag == 1){
            throw new GeneralException(trans('Beneficiary is suspended! Reinstate first to proceed with bank details modification! Please check!'));
        }
    }

    /*Erase current bank details for beneficiary not active yet*/
    public function eraseCurrentBankDetails($member_type_id, $resource_id, $employee_id)
    {
        DB::transaction(function () use ( $member_type_id, $resource_id, $employee_id) {

            $resource = $this->getResource($member_type_id, $resource_id);
            switch($member_type_id)
            {
                case 4:
                    /*dependent*/
                    $resource->update([
                        'bank_id' => null,
                        'bank_branch_id' => null,
                        'accountno' => null
                    ]);
                    break;


                case 5:
                    /*pensioner*/
                    $resource->update([
                        'bank_id' => null,
                        'bank_branch_id' => null,
                        'accountno' => null
                    ]);
                    $input = ['bank_id' => null, 'bank_branch_id' => null ,'accountno' => null];
                    (new EmployeeRepository())->UpdateDetailsFromPayrollApprovals($resource->employee_id, $input);


                    break;
            }

        });
    }

}