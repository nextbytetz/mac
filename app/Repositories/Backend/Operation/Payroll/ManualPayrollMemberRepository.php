<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Operation\Payroll\ManualPayrollMember;
use App\Models\Operation\Payroll\ManualPayrollRun;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentTypeRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ManualPayrollMemberRepository extends  BaseRepository
{
    use FileHandler, AttachmentHandler;

    const MODEL = ManualPayrollMember::class;

    protected $document_name = 'manual_payroll_members.xlsx';

    public function __construct()
    {

    }
    public function uploadedFileDir()
    {
        return manual_files_dir() . DIRECTORY_SEPARATOR . $this->document_name;
    }


    public function uploadedFileUrl()
    {
        return manual_files_url() . DIRECTORY_SEPARATOR . $this->document_name;
    }

    /*
     * create new
     */

    public function create(array $input)
    {
        return DB::transaction(function () use ($input) {

        });
    }



    /*
     * update
     */
    public function update($id,$input) {

        return DB::transaction(function () use ($id,$input) {

        });
    }

    /*Get for datatable*/
    public function getForDataTable()
    {
        return $this->query();
    }

    public function getForAddingByMemberTypeDataTable($member_type)
    {
        return $this->getForDataTable()->where('manual_payroll_members.member_type_id',$member_type)
            ->join('manual_notification_reports as nr', function($join){
                $join->on('nr.case_no', 'manual_payroll_members.case_no')->whereRaw(" nr.incident_type_id = manual_payroll_members.incident_type_id ");
            })->where('nr.has_payroll',1);
    }
    /**\
     * @param array $input
     * Upload bulk files from excel
     */
    public function saveUploadedDocumentFromExcel(array $input)
    {
        DB::transaction(function () use ($input) {
            $base = manual_files_dir();
            $file = $this->uploadedFileDir();
            $request_file_name = 'document_file';
            /*check if exist*/
            if (request()->hasFile($request_file_name)) {
                /* If has file: remove and replace */
                if (file_exists($file)) {
                    unlink($file);
                }
                /*Save*/
                $document_name = '';
                $this->saveDocumentGeneral($request_file_name, $this->document_name, $base);
                /*Upload manual files into table*/
                $this->uploadBulkManualFiles($input['member_type_id']);
            }
            /* end replace if exist*/




        });
    }

    /**
     * @param $employee_id
     * @param array $input
     * @return mixed
     * Save survivor manual processed
     */
    public function createManualDependent($employee_id, array $input)
    {
        $dependent_repo = new DependentRepository();
        return DB::transaction(function () use ($employee_id,$input, $dependent_repo) {
            $ispaid = isset($input['ispaid']) ? $input['ispaid'] : 0;
            $manual_notification = $this->findManualNotificationReport($input['manual_dependent_id']);
            $input['survivor_pension_receiver'] = 1;
            $dependent = $dependent_repo->create($employee_id, $input);
            /*Other dep*/
            $payable_months_data = $this->getPayableMonthsData($input);
            $recycles_pay_period = $payable_months_data['recycles_pay_period'];
            $isotherdep = isset($input['isotherdep']) ? 1 : 0;
//            $isactive = ($isotherdep == 1 && $recycles_pay_period == 0) ? 2 : 0;
            /*end other dep*/
            $dependent->employees()->syncWithoutDetaching([$employee_id => [
                'survivor_pension_amount' =>isset($input['mp'])  ? str_replace(",", "", $input['mp']) : null,
                'manual_notification_report_id' => $manual_notification->id,
                'firstpay_flag' => $ispaid,
                'funeral_grant_pay_flag' => 1,
                'recycles_pay_period' =>($isotherdep == 1)  ? $recycles_pay_period : null,
            ]]);
            /*activate dependent*/
//            //TODO After improvement - comment this <activateForPayroll>
//            if(env('TESTING_MODE') == 0) {
//                $dependent_repo->activateForPayroll($dependent->id, $employee_id);
//            }
            /*update sync status*/
            $this->updateSyncStatus($input['manual_dependent_id'], $dependent->id, $input);

            /*update firsy_manual status*/
            $dependent->update(['firstpay_manual' => $ispaid]);
            return $dependent;
        });
    }

    /*Payable Months Data*/
    public function getPayableMonthsData(array $input)
    {
        $months_payable = sysdefs()->data()->max_payable_months_otherdep_full;
        $pending_pay_months = isset($input['pending_pay_months']) ? $input['pending_pay_months'] : 0;
        $other_dep_months_paid = isset($input['other_dep_months_paid']) ? $input['other_dep_months_paid'] : 0;
        $recycles_remained =$months_payable - ($other_dep_months_paid + $pending_pay_months);
        return ['recycles_pay_period' => $recycles_remained];
    }

    /*update sync status and information of synced dependent*/
    public function updateSyncStatus($manual_dependent_id, $dependent_id, array $input)
    {
        $member = $this->find($manual_dependent_id);
        $member->update([
            'issynced' =>  1,
            'resource_id' => $dependent_id,
            'ispaid' => isset($input['ispaid']) ? $input['ispaid'] : null,
            'hasarrears' => isset($input['hasarrears']) ? $input['hasarrears'] : 0,
            'pending_pay_months' => isset($input['pending_pay_months']) ? $input['pending_pay_months'] : 0,
            'remark' => isset($input['remark']) ? $input['remark'] : null,
        ]);
    }

    /**
     * @param $id
     * Update arrears status from payroll recovery when is recovered
     */
    public function updateArrearsStatus($id)
    {
        $member = $this->find($id);
        $member->update(['hasarrears' => 2]);
    }


    public function findManualNotificationReport($id){
        $member = $this->find($id);
        $manual_notification = $member->manualNotificationReport($member->incident_type_id)->first();
        return $manual_notification;
    }


    /**
     * @param array $input
     * Upload files from save excel document
     */
    public function uploadBulkManualFiles($member_type_id)
    {
//        try {

        $bank_repo = new BankRepository();
        $user_id = access()->id();
        $uploaded_file = $this->uploadedFileDir();
        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($uploaded_file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();

        $verifyArr = ['case_no','name','dob', 'bank', 'account_no','mp', 'phone', 'incident'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new GeneralException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }

        }
        /** end : Check if all excel headers are present */

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($uploaded_file)
            ->chunk(250, function($result) use($user_id, $uploaded_file, $bank_repo,$member_type_id)  {
                $rows = $result->toArray();
                try {

                    //let's do more processing (change values in cells) here as needed
                    //$counter = 0;
                    foreach ($rows as $row) {
                        /* Changing date from excel format to unix date format ... */
                        $row['dob'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['dob'], 'YYYY-MM-DD');


                        /* start: Validating row entries */
                        $error_report = null;
                        $error = 0;
                        foreach ($row as $key => $value) {
                            if (trim($key) == 'dob') {
                                if (check_date_format($value) == 0 && isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }

                            } elseif (in_array(trim($key), ['case_no'], true)) {
                                if (!is_numeric($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }

                            } elseif (trim($key) == 'name') {
                                if (!isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                                }
                            } elseif (trim($key) == 'mp') {
                                if (!isset($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                                }
                            }
                        }

                        $bank_id = null;
                        $bank_name = strtoupper(trim($row['bank']));
                        $bank  = $bank_repo->query()->where('name', $bank_name)->first();
                        if (isset($bank)) {
                            $bank_id = $bank->id;
                        }
                        $mp =  isset($row['mp'])  ? str_replace(",", "", $row['mp']) : null;
                        $incident_id = null;
                        $incident = strtolower(trim($row['incident']));
                        if ($incident == 'occupational accident' || $incident == 'occupation accident') {
                            $incident_id = 1;
                            $member_type_id = 5;
                                            } elseif ($incident == 'occupational disease' || $incident == 'occupation disease') {
                            $incident_id = 2;
                            $member_type_id = 5;
                        } elseif ($incident == 'occupational death' || $incident == 'occupation death') {
                            $incident_id = 3;
                            $member_type_id = 4;
                        }
                        /* end: Validating row entries */
                        /* insert into db */
                        $data = ['case_no' => trim($row['case_no']), 'name' => trim($row['name']), 'member_type_id' => $member_type_id, 'monthly_pension' => $mp, 'bank_name' => $row['bank'], 'dob' => $row['dob'], 'bank_id' => $bank_id, 'accountno' => $row['account_no'], 'user_id' => $user_id, 'upload_error'=> $error_report, 'phone' => $row['phone'], 'incident_type_id' => $incident_id];

                        /*Create notification report*/
                        if ($this->checkIfMemberDoesNotExist($data) && $this->checkIfIncidentTypeMatchMemberType($data)) {
                            $this->query()->create($data);
                        }else{
//
//                            $this->query()->where('case_no', trim($row['case_no']))->where('incident_type_id', $incident_id)->where('issynced',0)->where('name',trim($row['name']))->update($data);
                        };
                    }
                } catch (\Exception $e) {
                    // an error occurred
                    $error_message = $e->getMessage();
//                    $this->saveExceptionMessage($employer, $error_message);
                    Log::info(print_r($error_message,true));
                }


            }, true);

    }

    /*Check if member does no exist*/
    public function checkIfMemberDoesNotExist($input)
    {
          $check = $this->query()->where('case_no', $input['case_no'])->where('incident_type_id', $input['incident_type_id'])->where('name', $input['name'])->count();

        if($check > 0){
            return false;
        }else{
            return true;
        }

    }

    /*Check if incident type match member type*/
    public function checkIfIncidentTypeMatchMemberType($input)
    {
        $manual_notification_repo = new ManualNotificationReportRepository();
        $manual_notification = $manual_notification_repo->query()->where('case_no', $input['case_no'])->where('incident_type_id', $input['incident_type_id'])->first();
        $incident_type = (isset($manual_notification)) ? $manual_notification->incident_type_id : null;
        if ($input['member_type_id'] == 5 && $incident_type != 3){
            return true;
        }elseif($input['member_type_id'] == 4 && $incident_type == 3){
            return true;
        }else{
            return false;
        }
    }

    /*Get Pending summary*/
    public function getPendingSummary(){
        $pending_survivor_sync = $this->getForAddingByMemberTypeDataTable(4)->where('manual_payroll_members.issynced',0)->count();
        $pending_pensioner_sync = $this->query()->where('manual_payroll_members.member_type_id', 5)->where('manual_payroll_members.issynced',0)->count();

        return ['pending_survivor_sync' => $pending_survivor_sync, 'pending_pensioner_sync' => $pending_pensioner_sync];
    }


    /*Member with pending arrears*/
    public function getManualMembersWithPendingArrearsForDataTable()
    {
        return $this->query()->where('hasarrears', 1);
    }

}