<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\PayrollDeduction;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollDeductionRepository extends  BaseRepository
{

    const MODEL = PayrollDeduction::class;

    protected $payroll_recoveries;


    public function __construct()
    {
        $this->payroll_recoveries = new PayrollRecoveryRepository();
    }

    /*Find payroll deduction by recovery id*/
    public function findByRecoveryId($payroll_recovery_id)
    {
        $arrear = $this->query()->where('payroll_recovery_id', $payroll_recovery_id)->first();
        return $arrear;
    }


    /*
     * create new
     */

    public function create(Model $payroll_recovery, array $input = [])
    {
        return DB::transaction(function () use ($payroll_recovery,$input) {
            $isqueued = ($this->checkIfThereNoIsPendingDeduction($payroll_recovery->member_type_id, $payroll_recovery->resource_id, $payroll_recovery->employee_id)) ? 0 : 1;
$recycles =  $input['cycles'] ?? $payroll_recovery->cycles;
            $save_input = [
                'payroll_recovery_id' => $payroll_recovery->id,
                'amount' => $input['amount'] ?? $this->payroll_recoveries->findAmountPerCycleForDeduction($payroll_recovery),
                'recycles' => ceil($recycles),
                'actual_recycles' =>  $recycles,
                'last_cycle_amount' =>$input['last_cycle_amount'] ?? $this->payroll_recoveries->findLastCycleAmountForDeduction($payroll_recovery),
                'isqueued' => $isqueued
            ];

            $deduction = $this->query()->create($save_input);
            return $deduction;
        });
    }


    /*
     * update
     */
    public function update(Model $deduction,$input)
    {

    }

    /*Query for getting active deductions */
    public function queryForActiveDeductions()
    {
        return $this->query()->where('isactive', 1)->where('recycles', '>', 0)->where('isqueued', 0);
    }


    /*Get active /pending deductions for specified beneficiary*/
    public function getActiveDeductionsCount($member_type_id, $resource_id)
    {
        $code_values = new CodeValueRepository();
        $recovery_type_cv_id = $code_values->findByReference('PRTOVEP')->id;
        return $this->queryForActiveDeductions()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $recovery_type_cv_id){
            $query->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('recovery_type_cv_id', $recovery_type_cv_id);
        })->count();
    }


    /*Update Recycles*/
    public function updateRecycle($payroll_recovery_id)
    {
        $deduction = $this->findByRecoveryId($payroll_recovery_id);
        $isactive = ($deduction->recycles > 1) ? 1 : 0;
        $deduction->update([
            'recycles' =>$deduction->recycles - 1,
            'isactive' => $isactive,
        ]);

        /*update queued deductions if any*/
        if($isactive == 0){
            $this->updateQueuedDeduction();
        }
    }

    /**
     * When active deduction complete need to update Queued Deduction
     *
     */
    public function updateQueuedDeduction()
    {
        $queued_deduction = $this->getQueuedDeductions()->orderBy('id', 'asc')->first();
        if($queued_deduction){
            $queued_deduction->update(['isqueued' =>  0]);
        }
    }

    /**
     * @return mixed
     * Get queued deductions
     */
    public function getQueuedDeductions()
    {
        return $this->query()->where('isactive', 1)->where('recycles', '>', 0)->where('isqueued', 1);
    }

    /**
     * @return bool
     * Check if the active pending deduction which is still recovered
     */
    public function checkIfThereNoIsPendingDeduction($member_type_id, $resource_id, $employee_id)
    {
        $deduction = $this->queryForActiveDeductions()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $employee_id){
            $query->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
        })->first();
        if($deduction)
        {
            return false;
        }else{
            return true;
        }
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @return int
     * Get net pending deductions for member per notification report
     */
    public function getNetPendingDeductionAmountForMember($member_type_id, $resource_id, $employee_id)
    {
        $code_values = new CodeValueRepository();
        $recovery_type_cv_id = $code_values->findByReference('PRTOVEP')->id;
        $pending_deductions = $this->queryForActiveDeductions()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $recovery_type_cv_id, $employee_id){
            $query->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->where('recovery_type_cv_id', $recovery_type_cv_id)->where('employee_id', $employee_id);
        })->get();
        $total_balance = 0;
        foreach ($pending_deductions as $pending_deduction){
            $total_balance = $total_balance + ($this->calculateBalanceAmount($pending_deduction->id));
        }
        return $total_balance;
    }

    /**
     * @param $id
     * Calculate pending amount to be recovered
     */
    public function calculateBalanceAmount($id)
    {
        $payroll_recovery_trans = new PayrollRecoveryTransactionRepository();
        $payroll_deduction = $this->find($id);
        $total_amount = $payroll_deduction->payrollRecovery->total_amount;
        $recovered_amount = $payroll_recovery_trans->getTotalAmountRecoveredForRecovery($payroll_deduction->payroll_recovery_id);
        $balance = $total_amount - $recovered_amount;
        return $balance;
    }

}