<?php

namespace App\Repositories\Backend\Operation\Payroll;


use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Operation\Compliance\Member\EmployerParticularChange;
use App\Models\Operation\Payroll\PayrollMpUpdate;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Api\ClosedBusinessRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollMpUpdateRepository extends BaseRepository
{
    use AttachmentHandler, FileHandler;


    const MODEL = PayrollMpUpdate::class;

    protected $payroll_repo;


    public function __construct()
    {
        parent::__construct();
        $this->payroll_repo = new PayrollRepository();

    }


    /*Store*/
    public function store(array $input)
    {
        return DB::transaction(function () use ($input) {
            $mp_update = $this->query()->create([
                'resource_id' => $input['resource_id'],
                'member_type_id' => $input['member_type_id'],
                'employee_id' => $input['employee_id'],
                'old_mp' => $input['old_mp'],
                'new_mp' => $input['new_mp'],
                'remark' => $input['remark'],
                'folionumber' => isset($input['folionumber']) ? $input['folionumber'] : null,
                'user_id' => access()->id(),
            ]);
            return $mp_update;
        });
    }


    /*Update*/
    public function update(Model $payroll_mp_update,   array $input)
    {
        return DB::transaction(function () use ($input, $payroll_mp_update) {
            $payroll_mp_update->update([
                'old_mp' => $input['old_mp'],
                'new_mp' => $input['new_mp'],
                'remark' => $input['remark'],
                'folionumber' => isset($input['folionumber']) ? $input['folionumber'] : null,
            ]);

            return $payroll_mp_update;
        });
    }



    /**
     * @param Model Mp update
     * Undo Payroll mp update
     */
    public function undo(Model $payroll_mp_update)
    {

        DB::transaction(function () use ( $payroll_mp_update) {
            $wf_modules = new WfModuleRepository();
            $wf_module_group_id = 24;
            $type = $this->getWfModuleType();
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payroll_mp_update->id, 'type' => $type]);
            /*delete payroll mp update*/
            $payroll_mp_update->delete();
            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();
        });
    }




    /**
     * @param $id
     * Action when workflow is completed
     */
    public function updateOnWfDoneComplete($id)
    {
        $payroll_mp_update = $this->find($id);
        DB::transaction(function () use ( $payroll_mp_update) {
            $resource = $this->payroll_repo->getResource($payroll_mp_update->member_type_id, $payroll_mp_update->resource_id);
            $member_type_id = $payroll_mp_update->member_type_id;
            $old_mp = $payroll_mp_update->old_mp;
            $new_mp = $payroll_mp_update->new_mp;
            $employee_id = $payroll_mp_update->employee_id;
            if($member_type_id == 5)
            {
                /*pensioners*/
                (new PensionerRepository())->updateReassessedMp($resource->id, $old_mp, $new_mp);
            }elseif($member_type_id == 4){
                /*Dependents*/
                (new DependentRepository())->updateReassessedMp($resource->id, $employee_id, $old_mp, $new_mp);
            }
        });
    }




    /**
     * @param $input
     * Check if can initiate new entry
     */
    public function checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id)
    {
        $pending_count = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id',
            $employee_id)->where('wf_done', 0)->count();
        if($pending_count > 0){
            throw new GeneralException('There is pending approval for this beneficiary! Complete all pending to proceed with new entry');
        }
    }




    /**
     * @param $member_type_id
     * @param $resource_id
     * Get Mp updates per Beneficiary for DataTable
     */
    public function getByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)
    {
        return $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
    }




    /*Module type */
    public function getWfModuleType()
    {
        $type = 1;
        return $type;
    }



}