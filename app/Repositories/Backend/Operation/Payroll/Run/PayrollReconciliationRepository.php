<?php

namespace App\Repositories\Backend\Operation\Payroll\Run;

use App\Exceptions\GeneralException;

use App\Jobs\RunPayroll\PayrollReconciliations;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Payroll\Run\PayrollReconciliation;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRecoveryRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollReconciliationRepository extends  BaseRepository
{

    const MODEL = PayrollReconciliation::class;





    public function __construct()
    {

    }


    public function create(array $input)
    {
        return DB::transaction(function () use ($input) {
            $amount = ($input['amount'] < 0) ? (-1 * $input['amount']) : $input['amount'];
            $reconciliation = $this->query()->create([
                'resource_id' => $input['resource_id'],
                'member_type_id' => $input['member_type_id'],
                'notification_report_id' => $input['notification_report_id'],
                'remark' =>$input['remark'],
                'amount' => $amount,
                'type' => ($input['amount'] > 0) ? 2 : 1

            ]);

            return $reconciliation;
        });
    }

    public function undo(Model $payroll_reconciliation)
    {
        $payroll_reconciliation->update(['status'=> 2, 'user_id' => access()->id()]);
              $payroll_reconciliation->delete();
    }


    /**
     * @param Model $payroll_reconciliation
     * Initiate reconciliation approval
     */
    public function initiateReconciliation(Model $payroll_reconciliation)
    {
        $payroll_reconciliation->update([
            'status' => 1,
            'user_id' => access()->id(),
        ]);
    }






    public function updateOnWfComplete($id)
    {
        return DB::transaction(function () use ($id) {
            $code_values = new CodeValueRepository();
            $payroll_recoveries = new PayrollRecoveryRepository();
            $payroll_reconciliation = $this->find($id);
            /*update*/
            $type = $payroll_reconciliation->type;
            $general_input = [
                'member_type_id' => $payroll_reconciliation->member_type_id,
                'resource_id' => $payroll_reconciliation->resource_id,
                'notification_report_id' => $payroll_reconciliation->notification_report_id,
                'total_amount' => $payroll_reconciliation->amount,
                'remark' => $payroll_reconciliation->remark,
                'user_id' => $payroll_reconciliation->user_id,
                'payroll_reconciliation_id' => $payroll_reconciliation->id,
            ];
            switch ($type){
                case 1:
                    /*underpaid - arrears*/
                    $recovery_type_id = $code_values->findByReference('PRTUNDP')->id;
                    $arrear_input = ['cycles' => 1, 'recovery_type_id' =>$recovery_type_id  ];
                    $input = array_merge($arrear_input, $general_input);

                    break;
                case 2:
                    /*overpaid - deductions*/
                    $recovery_type_id = $code_values->findByReference('PRTOVEP')->id;
                    $arrear_input = ['cycles' => 1, 'recovery_type_id' =>$recovery_type_id  ];
                    $input = array_merge($arrear_input, $general_input);

                    break;
            }
            /*Save recovery*/
            $recovery =  $payroll_recoveries->create($input);
            /*Auto complete wf for this recovery*/
            $recovery->update([ 'wf_done' => 1, 'wf_done_date' => Carbon::now()]);
            $payroll_recoveries->wfDoneComplete($recovery->id);
        });
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Check if this beneficiary has any pending reconciliation per notification report
     */
    public function checkIfHasPendingReconciliation($member_type_id, $resource_id, $notification_report_id)
    {
        $pending_reconciliation = $this->query()->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->where('notification_report_id', $notification_report_id)->where('wf_done',0)->count();
        if($pending_reconciliation > 0)
        {
            /*there is pending*/
            return true;
        }else{
            /*no pending*/
            return false;
        }
    }







    /*Get all reconciliations alerts for datatable*/
    public function getAllForDataTable()
    {
        $query = $this->query()->select([
            DB::raw("concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname) as dependent_name"),
            DB::raw("concat_ws(' ', pensioners.firstname, coalesce(pensioners.middlename, ''), pensioners.lastname) as pensioner_name"),
            DB::raw("payroll_reconciliations.amount as amount"),
            DB::raw("payroll_reconciliations.status as status"),
            DB::raw("payroll_reconciliations.wf_done as wf_done"),
            DB::raw("payroll_reconciliations.notification_report_id as notification_report_id"),
            DB::raw("member_types.name as member_type_name"),
            DB::raw("payroll_reconciliations.resource_id as resource_id"),
            DB::raw("payroll_reconciliations.member_type_id as member_type_id"),
            DB::raw("payroll_reconciliations.type as type"),
            DB::raw("notification_reports.filename as filename"),
            DB::raw("payroll_reconciliations.remark as remark"),
            DB::raw("payroll_reconciliations.id as id"),
        ])
            ->leftjoin("pensioners", function($join){
                $join->on('payroll_reconciliations.resource_id', 'pensioners.id')
                    ->where('payroll_reconciliations.member_type_id', 5);
            })
            ->leftjoin("dependents", function($join){
                $join->on('payroll_reconciliations.resource_id', 'dependents.id')
                    ->where('payroll_reconciliations.member_type_id', 4);
            })
            ->join("member_types","payroll_reconciliations.member_type_id", "member_types.id")
            ->join("notification_reports","payroll_reconciliations.notification_report_id", "notification_reports.id");

        return $query;
    }




    /**
     * Payroll system reconciliations for all eligible beneficiaries - chunk for Job processing
     * Eligibility for reconciliation - firstpay_flag = 1, active and does not have any pending recovery to be approved
     */
    public function payrollSystemReconciliationJobChunk()
    {
        $pensioners = new PensionerRepository();
        $dependents = new DependentRepository();

        /*pensioners start--*/
        $pensioners_to_reconcile = $pensioners->getEligibleForReconciliation();

        $pensioners_to_reconcile->select(['id', 'notification_report_id'])->chunk(100, function ($pensioners)  {
            if (isset($pensioners)) {
                dispatch(new PayrollReconciliations($pensioners, 5));
            }
        });
        /*end pensioners--*/

        /*dependents start --*/
        $dependents_to_reconcile = $dependents->getEligibleForReconciliation();
        $dependents_to_reconcile->select(['dependent_id', 'notification_report_id'])->orderBy('dependent_id')->chunk(100, function ($dependents)  {
            if (isset($dependents)) {

                dispatch(new PayrollReconciliations($dependents, 4));
            }
        });
        /*end dependents--*/
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notication_report_id
     * Process reconciliation for each beneficiary from the job
     */
    public function processPayrollReconciliation($member_type_id, $resource_id, $notification_report_id)
    {
        DB::transaction(function () use ($member_type_id, $resource_id, $notification_report_id) {
            $payroll_runs = new PayrollRunRepository();
            $notification_report = (new NotificationReportRepository())->find($notification_report_id);
            $employee_id = $notification_report->employee_id;
            $mp_balance = $payroll_runs->getMpBalanceForMember($member_type_id, $resource_id, $employee_id);
            if($mp_balance != 0)
            {

                /*Insert if there is no pending reconciliation*/
                if($this->checkIfHasPendingReconciliation($member_type_id, $resource_id, $notification_report_id) == false){
                    /*Enter new reconciliation for approval*/
                    $input = ['member_type_id' => $member_type_id, 'resource_id' => $resource_id, 'notification_report_id' => $notification_report_id, 'amount' => $mp_balance, 'remark' => 'System Reconciliation'];

                    /*no pending create new entry*/
                    $this->create($input);
                }

            }


        });

    }






}