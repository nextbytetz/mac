<?php

namespace App\Repositories\Backend\Operation\Payroll\Run;

use App\Exceptions\GeneralException;

use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Models\Operation\Payroll\Run\PayrollRunVoucher;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollRunVoucherRepository extends  BaseRepository
{

    const MODEL = PayrollRunVoucher::class;



    public function __construct()
    {

    }

    /*Create batch for bank i.e nmb and crdb*/
    public function create(array $input)
    {
        DB::transaction(function () use ($input) {

            if($this->checkIfExist($input) == false){
                $this->query()->create([
                    'amount' => $input['amount'],
                    'bank_id' => $input['bank_id'],
                    'payroll_run_approval_id' => $input['payroll_run_approval_id'],
                    'benefit_type_id' => $input['benefit_type_id'],
                    'user_id' => access()->id(),
                ]);
            }

        });
    }

/*Create for individuals*/
    public function createIndividual(array $input)
    {
        DB::transaction(function () use ($input) {
            if($this->checkIfExistIndividual($input) == false){
                $this->query()->create([
                    'amount' => $input['amount'],
                    'payroll_run_id' => $input['payroll_run_id'],
                    'payroll_run_approval_id' => $input['payroll_run_approval_id'],
                    'benefit_type_id' => $input['benefit_type_id'],
                    'user_id' => access()->id(),
                    'isbatch' => 0,
                ]);
            }

        });
    }

    /*Check if already exist*/
    public function checkIfExist(array $input)
    {
        $check = $this->query()->where('bank_id', $input['bank_id'])->where('benefit_type_id', $input['benefit_type_id'])->where('payroll_run_approval_id', $input['payroll_run_approval_id'])->count();
        if($check > 0){
            return true;
        }else{
            return false;
        }
    }


    public function checkIfExistIndividual(array $input)
    {
        $check = $this->query()->where('payroll_run_id', $input['payroll_run_id'])->count();
        if($check > 0){
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param $id
     * New function to get last two finance officers from wf track for pv printout
     */
    public function getFinanceWfTrackForPvPerBenefit($payroll_run_approval_id)
    {
        $wf_module_group_id = 10;
        $unit_finance = 12;
        $wf_track_repo = new WfTrackRepository();
        $last_two_finance_wf_tracks = $wf_track_repo->query()->where('resource_id', $payroll_run_approval_id)->whereHas('wfDefinition', function($query) use( $wf_module_group_id, $unit_finance){
            $query->where('unit_id', $unit_finance)->whereHas('wfModule', function($query)use($wf_module_group_id){
                $query->where('wf_module_group_id',$wf_module_group_id );
            });
        })->orderBy('id', 'desc')->limit(2)->get();
//        foreach($last_two_finance_wf_tracks as $wf_track)
        $officer = $last_two_finance_wf_tracks[0]->user;
        $senior_officer = $last_two_finance_wf_tracks[1]->user;
               return ['officer' => $officer, 'senior_officer' => $senior_officer, 'officer_wf_date' => $last_two_finance_wf_tracks[0]->forward_date ];
    }



    /**
     * @param $payroll_run_approval_id
     * @return bool
     * Check if payroll runs vouchers of certain payroll is exported to erp.
     */
    public function checkIfAllPensionsVouchersAreExportedToErp($payroll_run_approval_id){
        $payroll_run_vouchers = $this->query()->where('payroll_run_approval_id', $payroll_run_approval_id)->where('is_exported', 0)->count();
        if($payroll_run_vouchers > 0){
            return false;
        }else{
            return true;
        }
    }


    /**
     * @param $payroll_run_id
     * Update export flag
     */
    public function updateExportFlag($id){
        $payroll_run_voucher = $this->find($id);
       $payroll_run_approval_id = $payroll_run_voucher->payroll_run_approval_id;
        $payroll_run_voucher->update([
            'is_exported' => 1,
        ]);
        /*Update payroll run approval payment process status*/
        /*update pay process status*/
        $check_if_all_exported = $this->checkIfAllPensionsVouchersAreExportedToErp($payroll_run_approval_id);
        if($check_if_all_exported == true) {
            (new PayrollRunApprovalRepository())->updatePayStatus($payroll_run_approval_id);
        }
    }


}