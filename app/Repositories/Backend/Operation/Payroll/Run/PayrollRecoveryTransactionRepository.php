<?php

namespace App\Repositories\Backend\Operation\Payroll\Run;


use App\Models\Operation\Payroll\Run\PayrollRecoveryTransaction;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollRecoveryTransactionRepository extends  BaseRepository
{

    const MODEL = PayrollRecoveryTransaction::class;

    public function __construct()
    {

    }

    /*Create new transaction*/
    public function create(array $input)
    {
        return DB::transaction(function () use ($input) {

            $tran = $this->query()->create([
                'payroll_run_approval_id' => $input['payroll_run_approval_id'],
                'payroll_recovery_id' => $input['payroll_recovery_id'],
                'amount' => $input['amount']
            ]);

            return $tran;
        });

    }


    /**
     * Undo Payroll runs all by approval id
     */
    public function undoAllTransactionsByApprovalId($payroll_run_approval_id)
    {
        $this->query()->where('payroll_run_approval_id', $payroll_run_approval_id)->delete();
    }


    /*Update paid flag when Wf complete for payroll approval*/
    public function updatePaidFlag($payroll_run_approval_id)
    {
        $this->query()->where('payroll_run_approval_id', $payroll_run_approval_id)->update(['ispaid' => 1]);
    }

    /**
     * Get pending payroll arrears for update after payment wf_done
     */
    public function getPendingArrearsForUpdate($payroll_run_approval_id)
    {
        return $this->query()->has('payrollArrear')->where('ispaid', 0)->where('payroll_run_approval_id', $payroll_run_approval_id)->select(['id','payroll_recovery_id'])->get();
    }

    /**
     * Get pending payroll deductions for update after payment wf_done
     */
    public function getPendingDeductionsForUpdate($payroll_run_approval_id)
    {
        return $this->query()->has('payrollDeduction')->where('ispaid', 0)->where('payroll_run_approval_id', $payroll_run_approval_id)->select(['id', 'payroll_recovery_id'])->get();
    }

    /**
     * Get pending payroll unclaims for update after payment wf_done
     */
    public function getPendingUnclaimsForUpdate($payroll_run_approval_id)
    {
        return $this->query()->has('payrollUnclaim')->where('ispaid', 0)->where('payroll_run_approval_id', $payroll_run_approval_id)->select(['id', 'payroll_recovery_id'])->get();
    }

    /**
     * @param $payroll_recovery_id
     * Get total amount recovered per recovery type
     */
    public function getTotalAmountRecoveredForRecovery($payroll_recovery_id)
    {
        $amount = $this->query()->where('payroll_recovery_id', $payroll_recovery_id)->where('ispaid',1)->sum('amount');
        return $amount;
    }

}