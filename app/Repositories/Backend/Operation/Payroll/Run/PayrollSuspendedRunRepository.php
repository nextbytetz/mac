<?php

namespace App\Repositories\Backend\Operation\Payroll\Run;


use App\Models\Operation\Payroll\Run\PayrollSuspendedRun;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollSuspendedRunRepository extends  BaseRepository
{

    const MODEL = PayrollSuspendedRun::class;



    public function __construct()
    {

    }


    /*
     * create new
     */

    public function create($input) {
        $payroll_suspended_run = $this->query()->create([
            'payroll_run_approval_id' => $input['payroll_run_approval_id'],
            'member_type_id' => $input['member_type_id'],
            'resource_id' => $input['resource_id'],
            'amount' => $input['amount'],
            'months_paid' => $input['months_paid'],
            'monthly_pension' => $input['monthly_pension'],
            'employee_id' => $input['employee_id'],
            'arrears_amount' => $input['arrears_amount'],
            'deductions_amount' => $input['deductions_amount'],
            'unclaimed_amount' =>  $input['unclaimed_amount'],
            'isconstantcare' =>  $input['isconstantcare']
        ]);
        return $payroll_suspended_run;
    }


    /**
     * Undo Payroll runs all by approval id
     */
    public function undoAllSuspendedRunsByApprovalId($payroll_run_approval_id)
    {
        $this->query()->where('payroll_run_approval_id', $payroll_run_approval_id)->delete();
    }


    /**
     * Get Pending Suspended payments by beneficiary
     * @param $ismanual = Flag to imply if notification was processed manually or from system  ( 1=> Manual notifiction, 0 => System notification report)
     */
    public function pendingSuspendedPaymentsByBeneficiary($member_type_id, $resource_id, $employee_id)
    {
        return $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id' ,$employee_id)->where('ispaid', 0);
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $payroll_run_approval_id
     * Update after wf approved -- update flag ispaid to 1
     */
    public function updatePaidStatusAfterWfDone($member_type_id, $resource_id, $payroll_run_approval_id)
    {
        $suspended_run = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('ispaid', 0)->update([
            'ispaid' => 1,
            'paid_payroll_run_approval_id' => $payroll_run_approval_id
        ]);
    }


    /**
     * @param $payroll_approval_run_id
     * Get total amounts for specified payroll
     */
    public function getTotalAmountsForRunApproval($payroll_approval_run_id)
    {
        $query = $this->query()->where('payroll_run_approval_id', $payroll_approval_run_id);
        $total_net_amount = $query->sum('amount');
        $total_arrears_amount = $query->sum('arrears_amount');
        $total_deductions_amount = $query->sum('deductions_amount');

        return ['total_suspended_amount' => $total_net_amount, 'total_suspended_arrears_amount' => $total_arrears_amount, 'total_suspended_deductions_amount' => $total_deductions_amount];
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get Total suspended amount paid but not processed to beneficiary per notification report
     */
    public function getTotalSuspendedAmountForMember($member_type_id, $resource_id,$employee_id)
    {
        $total_amount = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id)->whereHas('payrollRunApproval', function($query){
            $query->where('wf_done', 1);
        })->where('ispaid', 0)->sum('amount');
        return $total_amount;
    }



    /**
     * General retrieve suspended monthly payroll for DataTable
     */
    public function getSuspendedMpForDataTable()
    {
        $query = $this->query()->select([
            DB::raw("concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname) as dependent_name"),
            DB::raw("concat_ws(' ', pensioners.firstname, coalesce(pensioners.middlename, ''), pensioners.lastname) as pensioner_name"),
            DB::raw("payroll_suspended_runs.amount as amount"),
            DB::raw("payroll_suspended_runs.months_paid"),
            DB::raw("payroll_suspended_runs.monthly_pension"),
            DB::raw("payroll_suspended_runs.payroll_run_approval_id as payroll_run_approval_id"),
            DB::raw("payroll_suspended_runs.paid_payroll_run_approval_id as paid_payroll_run_approval_id"),
            DB::raw("payroll_run_approvals.wf_done as wf_done"),
            DB::raw("payroll_suspended_runs.arrears_amount as arrears_amount"),
            DB::raw("payroll_suspended_runs.deductions_amount as deductions_amount"),
            DB::raw("payroll_suspended_runs.unclaimed_amount as unclaimed_amount"),
            DB::raw("payroll_suspended_runs.employee_id as employee_id"),
            DB::raw("member_types.name as member_type_name"),
            DB::raw("payroll_procs.run_date as run_date"),
            DB::raw("payroll_suspended_runs.resource_id as resource_id"),
            DB::raw("payroll_suspended_runs.ispaid as ispaid"),
            DB::raw("payroll_suspended_runs.member_type_id as member_type_id"),

        ])
            ->leftjoin("pensioners", function($join){
                $join->on('payroll_suspended_runs.resource_id', 'pensioners.id')
                    ->where('payroll_suspended_runs.member_type_id', 5);
            })
            ->leftjoin("dependents", function($join){
                $join->on('payroll_suspended_runs.resource_id', 'dependents.id')
                    ->where('payroll_suspended_runs.member_type_id', 4);
            })
            ->join("payroll_run_approvals","payroll_suspended_runs.payroll_run_approval_id", "payroll_run_approvals.id")
            ->join("payroll_procs","payroll_procs.id", "payroll_run_approvals.payroll_proc_id")
            ->join("member_types","payroll_suspended_runs.member_type_id", "member_types.id");


        return $query;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $payroll_run_id
     * check if beneficiary has payroll run for specified payroll
     */
    public function checkIfBeneficiaryHasPayrollRun($member_type_id, $resource_id,$employee_id, $payroll_run_approval_id)
    {
        $payroll_run_count = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('payroll_run_approval_id', $payroll_run_approval_id)->where('employee_id',$employee_id)->count();
        return ($payroll_run_count > 0) ? true : false;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * Remove suspended payments
     */
    public function removeSuspendedPayments(array $input)
    {
        $this->query()->where('employee_id', $input['employee_id'])->where('resource_id',  $input['resource_id'])->where('member_type_id',  $input['member_type_id'])->where('ispaid', 0)->update([
            'deleted_at' => Carbon::now(),
            'delete_reason' => $input['delete_reason'],
            'deleted_by' => isset($input['deleted_by']) ? $input['deleted_by'] : null
        ]);
    }


    /*Get Arrears summary for suspended payments*/
    public function getArrearsSummaryForSuspendedPayments($member_type_id, $resource_id,$employee_id)
    {
        $pending_amount = $this->pendingSuspendedPaymentsByBeneficiary($member_type_id, $resource_id, $employee_id)->sum('payroll_suspended_runs.amount');
        $summary = 'This beneficiary will be paid arrears of total Tshs ' . '<b class=underline>'.    number_2_format($pending_amount) . '</b>' . ' on next payroll. These arrears are derived from suspended payments for the period this beneficiary was suspended.';
        return ['summary' => $summary];
    }



    /**
     * @return mixed
     * Get Payroll run total summary
     */
    public function getQueryPayrollSuspendedRunTotalSummary()
    {
        return   DB::table('payroll_suspended_runs_view')->select(
            DB::raw("sum(amount) as net_amount"),
            DB::raw("sum(arrears_amount) as arrears_amount"),
            DB::raw("sum(deductions_amount) as deductions_amount"),
            DB::raw("sum(unclaimed_amount) as unclaimed_amount"),
            DB::raw("count(distinct resource_id) as no_of_payees")
        );
    }


}