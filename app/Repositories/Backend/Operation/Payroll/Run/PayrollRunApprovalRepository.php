<?php

namespace App\Repositories\Backend\Operation\Payroll\Run;

use App\Exceptions\GeneralException;

use App\Exceptions\WorkflowException;
use App\Jobs\RunPayroll\Payment\ProcessPayrollVouchers;
use App\Models\MacErp\ClaimsPayable;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Models\Operation\Payroll\Run\PayrollRunVoucher;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\MacErp\ClaimsErpApiRepositroy;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class PayrollRunApprovalRepository extends  BaseRepository
{

    const MODEL = PayrollRunApproval::class;

    protected $pensioners;
    protected $dependents;
    protected $payroll_runs;
    protected $payroll_suspended_runs;



    public function __construct()
    {
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->payroll_runs = new PayrollRunRepository();
        $this->payroll_suspended_runs = new PayrollSuspendedRunRepository();
    }


    public function create($payroll_proc_id)
    {
        return DB::transaction(function () use ($payroll_proc_id) {
            $payroll_procs = new PayrollProcRepository();
            $payroll_proc = $payroll_procs->find($payroll_proc_id);
            $run_date = $payroll_proc->run_date;
            $approval = $this->query()->create([
                'no_of_pensioners' => $this->getEligiblePensionersCount($run_date),
                'no_of_dependents' => $this->getEligibleSurvivorsCount(),
                'no_of_constant_cares' => $this->getEligibleConstantCareCount(),
                'no_of_suspended_pensioners' => $this->getEligibleSuspendedPensionersCount($run_date),
                'no_of_suspended_dependents' => $this->getEligibleSuspendedSurvivorsCount(),
                'no_of_suspended_constant_cares' => $this->getEligibleSuspendedConstantCareCount(),
                'payroll_proc_id' => $payroll_proc_id,
            ]);

            /*Update file name*/
            $this->updatePayrollFilename($payroll_proc->run_date, $approval);

            return $approval;
        });
    }

    /*Update payroll filename*/
    public function updatePayrollFilename($run_date, Model $payroll_run_approval)
    {
        $run_date_parse = Carbon::parse($run_date);
        $filename =  'MP/'. $run_date_parse->format('m'). $run_date_parse->format('Y'). '/'. $payroll_run_approval->id;
        $payroll_run_approval->update(['filename' => $filename]);
        return $payroll_run_approval;
    }




    /**
     * @param $id
     * Initiate payroll run approval
     */
    public function initiateRunApproval($id)
    {
        $payroll_run_approval = $this->find($id);
        $payroll_run_approval->update([
            'status' => 1
        ]);
        return $payroll_run_approval;
    }



    /**
     * @param $id
     * Undo payroll run approval
     */
    public function undoRunApproval($id)
    {
        $payroll_run_approval = $this->find($id);
        $payroll_proc = $payroll_run_approval->payrollProc;
        DB::transaction(function () use ($payroll_run_approval, $payroll_proc, $id) {
            $payroll_run_approval_id = $payroll_run_approval->id;
            /*Undo payroll run*/
            $this->payroll_runs->undoAllRunsByApprovalId($payroll_run_approval_id);
            /*undo payroll suspended payroll run*/
            $this->payroll_suspended_runs->undoAllSuspendedRunsByApprovalId($payroll_run_approval_id);
            /*undo payroll recovery transactions*/
            $recovery_trans = new PayrollRecoveryTransactionRepository();
            $recovery_trans->undoAllTransactionsByApprovalId($payroll_run_approval_id);
            /*undo payroll run approval*/
            $payroll_run_approval->delete();
            /*undo payroll procs*/
            $payroll_procs = new PayrollProcRepository();
            $payroll_procs->undo($payroll_proc->id);
            /*Deactivate workflow tracks*/
            $wf_module_group_id = 10;
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $id]);
            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();
            /*end workflow track*/
        });
    }


    /*Get Last Approved Payroll*/
    public function getLastPayroll($wf_done = null)
    {
        if($wf_done){
            return $this->query()->where('wf_done',$wf_done)->orderBy('id', 'desc')->first();
        }else{
            return $this->query()->orderBy('id', 'desc')->first();
        }

    }


    /*Get Last Approved Payroll by member */
    public function getLastPayrollByMember($member_type_id, $resource_id, $employee_id, $wf_done = null)
    {
        if($wf_done){
            $approval_run = $this->query()->where('wf_done',$wf_done)->whereHas('payrollRuns', function($q) use($member_type_id, $resource_id, $employee_id){
                $q->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
            })->orderBy('id', 'desc')->first();
            $approval_run_suspended = $this->query()->where('wf_done',$wf_done)->whereHas('payrollSuspendedRuns', function($q) use($member_type_id, $resource_id, $employee_id){
                $q->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
            })->orderBy('id', 'desc')->first();

            $last_payroll = (($approval_run->id ?? 0) >= $approval_run_suspended->id ?? 0) ? $approval_run : $approval_run_suspended;
        }else{
            $approval_run = $this->query()->whereHas('payrollRuns', function($q) use($member_type_id, $resource_id, $employee_id){
                $q->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
            })->orderBy('id', 'desc')->first();
            $approval_run_suspended = $this->query()->whereHas('payrollSuspendedRuns', function($q) use($member_type_id, $resource_id, $employee_id){
                $q->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('employee_id', $employee_id);
            })->orderBy('id', 'desc')->first();

            $last_payroll = (($approval_run->id ?? 0) >= $approval_run_suspended->id ?? 0) ? $approval_run : $approval_run_suspended;
        }

        return $last_payroll;

    }

    /**
     * @param $id
     * Check if payroll is first on the system
     */
    public function checkIfIsTheFirstPayroll()
    {
        $check = $this->query()->count();
        if($check == 1){
            return true;
        }elseif($check > 1){
            return false;
        }
    }

    /**
     * @param $id
     * @throws GeneralException
     * Check validation during initiation of approval
     */
    public function checkIfCanInitiateRunApproval($id)
    {
        $payroll_run_approval = $this->find($id);
        /*check complete run status*/
        if($payroll_run_approval->run_status == 0){
            throw new GeneralException('Can not initiate approval before payroll process has not completed');
        }

    }



    /*Get count of eligible pensioners for payroll*/
    public function getEligiblePensionersCount($run_date)
    {
        return $this->pensioners->getEligibleForPayroll($run_date)->count();

    }

    /*Get count of eligible pensioners for payroll*/
    public function getEligibleSuspendedPensionersCount($run_date)
    {
        return $this->pensioners->getEligibleSuspendedForPayroll($run_date)->count();

    }

    /*Get count of eligible survivors for payroll*/
    public function getEligibleSurvivorsCount($run_date = null)
    {
        $eligible_survivors = $this->dependents->getEligibleSurvivorsForPayroll($run_date)->count();
        $eligible_child_overage = $this->dependents->getEligibleChildrenOverageForPayroll()->count();
        return $eligible_survivors + $eligible_child_overage;

    }
    /*Get count of eligible suspended dependents for payroll*/
    public function getEligibleSuspendedSurvivorsCount($run_date = null)
    {
        return $this->dependents->getEligibleSuspendedSurvivorsForPayroll($run_date)->count();
    }


    /*Get count of eligible constant care for payroll*/
    public function getEligibleConstantCareCount($run_date = null)
    {
        return $this->dependents->getEligibleConstantCareForPayroll($run_date)->count();
    }

    /*Get count of eligible suspended constant care for payroll*/
    public function getEligibleSuspendedConstantCareCount($run_date = null)
    {
        return $this->dependents->getEligibleSuspendedConstantCareForPayroll($run_date)->count();
    }


    /**
     * @param $id
     * Process payment for payroll - Generate voucher for each bank.
     */
    public function processPayment($id)
    {
        DB::transaction(function () use ($id) {
            $payroll_run_voucher_repo = new PayrollRunVoucherRepository();
            $payroll_run_approval = $this->find($id);

            /*Process batch*/
            $this->processBatchVoucher($id);

            /*Process individuals*/
            $this->processIndividualVouchers($id);

        });

        /*Post to API*/
        //TODO enable post to erp
        dispatch(new ProcessPayrollVouchers($id));
    }

    /*Process batch bank voucher*/
    public function processBatchVoucher($id)
    {
        $payroll_run_voucher_repo = new PayrollRunVoucherRepository();
        /*only CRDB AND BANK*/
        $bank_repo = new BankRepository();
        $bank_ids = $bank_repo->getBankIdsForPayrollBatchPayment();
        $banks = (new BankRepository())->query()->whereIn('id', $bank_ids)->get();
        /*Bank*/
        $payroll_run_approval = $this->find($id);
        $benefit_types = (new BenefitTypeRepository())->query()->whereIn('id',[5,18,6])->get();
        foreach($banks as $bank) {
            $bank_id = $bank->id;
            /*Loop on all benefit types*/
            foreach($benefit_types as $benefit_type){
                $benefit_type_id = $benefit_type->id;
                $amount = $payroll_run_approval->getAmountByBankInPayrollRunByBenefitType($id, $bank_id, $benefit_type_id);
                $input = ['amount' => $amount, 'bank_id' => $bank_id, 'payroll_run_approval_id'=> $id, 'benefit_type_id' => $benefit_type_id];
                if($amount > 0){
                    $payroll_run_voucher_repo->create($input);
                }

            }
        }
    }


    /*Process Individual with no bank from crdb and Nmb*/
    public function processIndividualVouchers($id){
        $payroll_run_voucher_repo = new PayrollRunVoucherRepository();
        $bank_repo = new BankRepository();
        $bank_ids = $bank_repo->getBankIdsForPayrollBatchPayment();
        $payroll_runs = $this->payroll_runs->query()->where('payroll_run_approval_id', $id)->where(function ($query) use($bank_ids){
            $query->whereNotIn('bank_id', $bank_ids)->orWhereNull('bank_id');
        })->get();
        foreach($payroll_runs as $run){
            $amount = $run->amount;
            $benefit_type_id = $run->benefit_type_id;
            $input = ['amount' => $amount, 'payroll_run_id' => $run->id, 'payroll_run_approval_id'=> $id, 'benefit_type_id' => $benefit_type_id];
            if($amount > 0){
                $payroll_run_voucher_repo->createIndividual($input);
            }
        }
    }

    /**
     * @param $id
     * Update after payroll complete running --------- start--------
     */
    public function updateAfterCompletion($id)
    {
        $this->updateTotalAmountsForThisPayroll($id);
        $this->updateRunStatus($id);
    }

    /*Get total Amounts for this payroll*/
    public function updateTotalAmountsForThisPayroll($id)
    {
        $run_approval = $this->find($id);
        $total_amounts = $this->payroll_runs->getTotalAmountsForRunApproval($id); //Active
        $total_suspended_amounts = $this->payroll_suspended_runs->getTotalAmountsForRunApproval($id); //Suspended amounts
        $run_approval->update([
            'total_net_amount' => $total_amounts['total_net_amount'],
            'total_arrears_amount' => $total_amounts['total_arrears_amount'],
            'total_deductions_amount' => $total_amounts['total_deductions_amount'],
            'total_suspended_amount' =>$total_suspended_amounts['total_suspended_amount'],
        ]);

    }

    /*Update run status when payroll complete*/
    public function updateRunStatus($id)
    {
        $run_approval = $this->find($id);
        $run_approval->update([
            'run_status' => 1,
        ]);
    }

    /*Update payroll pay processed flag when payroll complete*/
    public function updatePayStatus($id)
    {
//        $check_if_all_exported = $this->payroll_runs->checkIfAllPensionsAreExportedToErp($id);
//        if($check_if_all_exported){
//            /*when all exported update payment status*/
        $run_approval = $this->find($id);
        $run_approval->update([
            'ispayprocessed' => 1,
        ]);
//        }

    }

    /*end -=----------update payroll after complete----------*/


    /**
     * Get Payroll approvals for datatable
     */
    public function getPayrollRunApprovalsForDataTable()
    {
        return $this->query();
    }


    /**
     * @param $id
     * Get previous payroll approval from this
     */
    public function getPreviousPayrollApproval($id)
    {
        $previous_payroll_approval = $this->query()->where('id', '<', $id)->orderBy('id', 'desc')->first();
        return $previous_payroll_approval;
    }

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @return mixed
     */
    public function checkIfPaymentIsProcessed($level, $resource_id)
    {
        $workflow = new Workflow(['wf_module_group_id' => 10, 'resource_id' => $resource_id]);
        $finance_level = $workflow->payrollFinancePaymentLevel();
        switch ($level) {
            case $finance_level:
                $payroll_run_approval = $this->find($resource_id);
                //TODO: Remove this code to bypass process payment for may 2019 - until resolved implementation of erp for pension payroll
                /*By pass payroll run approval for may*/
                if($payroll_run_approval->id != 8){
                    // check if payment is processed
                    if ($payroll_run_approval->ispayprocessed == 0){
                        throw new WorkflowException('Payment has not been processed! Please check!');
                    }
                    if ($payroll_run_approval->ispaid == 0){
                        throw new WorkflowException('Payment has not been paid on ERP! Please finish payments for all beneficiaries to proceed!');
                    }
                    break;
                }

        }
    }





    /**
     * @param $id
     * @return mixed
     * Get payroll runs ready for export to erp.
     */
    public function getPayrollRunsForExportToErp($id)
    {
        $payroll_runs = $this->payroll_runs->query()->where('payroll_run_approval_id',$id)->where('isexported', 0)->get();
        return $payroll_runs;
    }



    /*Export Mp by member type for payroll run approval*/
    public function exportMpByMemberType($beneficiary_type,$payroll_run_approval_id){

        $is_constantcare = 0;
        if($beneficiary_type == 1){
            /*dependents*/
            $member_type_id = 4;
        }elseif($beneficiary_type == 2){
            /*pensioners*/
            $member_type_id = 5;
        }else{
            /*constant care*/
            $member_type_id = 4;
            $is_constantcare = 1;
        }

        $beneficiaries = DB::table('payroll_runs_view')->select(
            'resource_id',
            'member_type_name',
            'gender',
            'relationship',
            'filename',
            'run_date as payroll_month',
            'member_name',
            'memberno',
            'employee_name',
            'bank_name',
            'bank_branch_name',
            'run_accountno',
            'arrears_amount',
            'deductions_amount',
            'unclaimed_amount',
            'months_paid',
            'monthly_pension',
            'amount'


        )->where('member_type_id', $member_type_id)
            ->where('isconstantcare', $is_constantcare)
            ->where('payroll_run_approval_id', $payroll_run_approval_id);
        $payroll_runs = $beneficiaries->get()->toArray();
        $results = array();
        foreach ($payroll_runs as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }
        return Excel::create(('monthlypensionpayroll'. time()), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');
    }



    /**
     * @param $id
     * @return mixed
     * Get payroll run vouchers ready for export to erp.
     */
    public function getPayrollRunVouchersForExportToErp($id)
    {
        $payroll_run_vouchers = DB::table('payroll_run_vouchers')->where('payroll_run_approval_id',$id)->where('is_exported', 0)->get();
        return $payroll_run_vouchers;
    }

    /**
     * @param Model $payroll_run
     * Get api fields for export to erp
     */
    public function getApiFieldsForPendingExports($payroll_run_voucher_id, Model $payroll_run_approval)
    {
        $member_types = new MemberTypeRepository();
        $bank_repo = new BankRepository();
//        $member_type = $member_types->find(1);
        $payroll_run_voucher = (new PayrollRunVoucherRepository())->find($payroll_run_voucher_id);
        $filename = $payroll_run_approval->filename;
        $bank = $bank_repo->find($payroll_run_voucher->bank_id);
        $payee = null;
        /*check if is batch or individual*/
        if($payroll_run_voucher->isbatch == 1){
            /*need update*/
            $employer = (new EmployerRepository())->find($bank->employer_id);
            $memberno =  (new PaymentVoucherTransactionRepository())->getMemberNoForErpApi($employer, 1, null);
            $member_type = 'Bank';
            $payee = $employer->name;
            /*need change*/
            /*bank details*/
            $bank_name = $bank->name;
            $accountno = isset($employer->accountno) ? $employer->accountno : 0;
            $swift_code = ($bank->id == 4) ? 'CORUTZTZ' : 'NMIBTZTZ';
            $bank_branch_id = 0;
        }else{
            /*Individual*/
            $payroll_run = $this->payroll_runs->find($payroll_run_voucher->payroll_run_id);
            $member_type = $member_types->find($payroll_run->member_type_id)->name;
            $member_type_id = $payroll_run->member_type_id;
            $member = (new PayrollRepository())->getResource($member_type_id, $payroll_run->resource_id);
            $memberno =  (new PaymentVoucherTransactionRepository())->getMemberNoForErpApi($member, $member_type_id, $payroll_run->employee_id);
            $payee = $member->name;
            $bank_name = 'CRDB';
            $accountno = 0;
            $swift_code = ($bank_name == 'CRDB') ? 'CORUTZTZ' : 'NMIBTZTZ';
            $bank_branch_id = 0;
        }

        $benefit_type =(new BenefitTypeRepository())->find($payroll_run_voucher->benefit_type_id);
        $gfs_code = $benefit_type->finCode->gfs_code;
        return [
            'filename' => $filename,
            'payee' => $payee ,
            'memberno' =>$memberno,
            'member_type' =>$member_type,
            'benefit_type' => $benefit_type->name,
            'amount' => $payroll_run_voucher->amount ,
//            'debit_account_expense' => '01.001.AB.000000.27110104.000.000',
            'debit_account_expense' => '01.001.AB.000000.'. $gfs_code .'.000.000',
            'credit_account_receivable' => '01.001.AB.000000.43181104.000.000',
            'accountno' => $accountno,
            'swift_code' => $swift_code,
            'bank_branch_id' =>$bank_branch_id,
            'bank_name' =>$bank_name,
            'bank_address' =>'Dar es Salaam' ,
            'country_name_bank' => 'Tanzania',
            'city_of_bank' => 'Dar es Salaam' ,
            'state_or_region' => 'Dar es Salaam',
            'is_paid' => 0,
        ];
    }


    /**
     * @param $transaction_id
     * @param array $input
     * Update payment reference from erp
     */
    public function updateRunVoucherPaymentInfoFromErp($transaction_id, array $input)
    {
        DB::transaction(function () use ($input, $transaction_id) {
            $claim_payable = ClaimsPayable::query()->find($transaction_id);
            $resource_id = $claim_payable->payment_resource_id;
            $run_voucher = PayrollRunVoucher::query()->find($resource_id);
            $date = isset($input['payment_date']) ? standard_date_format($input['payment_date']) : null;
            $run_voucher->update([
                'payment_date' => $date,
                'payreference' => $input['payment_reference'],
                'paymethod' => $input['payment_method'],
                'ispaid' => 1
            ]);

            /*update pay status*/
            $this->updateRunApprovalPaymentStatus($run_voucher->payroll_run_approval_id);
        });
    }

    /**
     * @param $run_approval_id
     * Update run approval payment status when all run vouchers are paid
     */
    public function updateRunApprovalPaymentStatus($run_approval_id)
    {
        $run_approval = $this->find($run_approval_id);
        $check = $run_approval->payrollRunVouchers()->where('ispaid', 0)->count();
        if($check > 0){
            /*still pending*/
        }else{
            $run_approval->update([
                'ispaid' => 1
            ]);
        }
    }







    /*Get workflow type*/
    public function getWfType(Model $payrollRunApproval)
    {
        $run_date = $payrollRunApproval->payrollProc->run_date;
        $start_date_for_9L = '2020-06-17';//Start date for new workflow - 9l
        if(comparable_date_format($run_date) < comparable_date_format($start_date_for_9L))
        {
            return 1;
        }

        return 2;
    }
}