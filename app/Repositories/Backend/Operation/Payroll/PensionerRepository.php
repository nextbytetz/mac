<?php

namespace App\Repositories\Backend\Operation\Payroll;

use Adldap\Models\ModelDoesNotExistException;
use App\Exceptions\GeneralException;
use App\Models\Operation\Payroll\Pensioner;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PensionerRepository extends  BaseRepository
{

    const MODEL = Pensioner::class;
    protected $employees;


    public function __construct()
    {
        $this->employees = new EmployeeRepository();
    }

//find or throwException
    public function findOrThrowException($id)
    {
        $pensioner = $this->query()->find($id);

        if (!is_null($pensioner)) {
            return $pensioner;
        }
        throw new GeneralException(trans('exceptions.backend.claim.pensioner_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {
        return DB::transaction(function () use ($input) {
            $pensioner = null;
            if($this->checkIfDoesNotExist($input)){
                $employee_details = $this->employeeDetails($input['resource_id']);
                $pensioner = $this->query()->create([
                    'employee_id' => $input['resource_id'],
                    'benefit_type_id' => $input['benefit_type_id'],
                    'compensation_payment_type_id' => $input['compensation_payment_type_id'],
                    'monthly_pension_amount' => $input['amount'],
                    'recycl' => (isset($input['recycl'])) ? $input['recycl'] :  null,
                    'pay_period' => (isset($input['pay_period'])) ? $input['pay_period'] :  null,
                    'lastresponse' => Carbon::parse('now')->format('Y-m-d'),
                    'notification_report_id' => $input['notification_report_id'],
                    'firstname' => $employee_details['firstname'],
                    'middlename' => $employee_details['middlename'],
                    'lastname' => $employee_details['lastname'],
                    'accountno' => $employee_details['accountno'],
                    'bank_branch_id' => $employee_details['bank_branch_id'],
                    'notification_eligible_benefit_id' => (isset($input['notification_eligible_benefit_id']) ? $input['notification_eligible_benefit_id'] : NULL),
                    'firstpay_manual' => 0,
                ]);
            }else{
//                /*Update - MP if claim compensation computed again*/
                $pensioner = $this->getPensionerExistByInput($input);
                $pensioner->update([
                    'monthly_pension_amount' => $input['amount'],
                ]);

            }
            /*Send pensioner into e-office*/
//            (new PayrollRepository())->postBeneficiaryToDms(5,$pensioner->id, $pensioner->employee_id);

            return $pensioner;
        });
    }


    /**
     * @param $input
     * @return mixed
     * Create pensioner from manual processing
     */
    public function createManualPensioner($input) {
        return DB::transaction(function () use ($input) {
            $pensioner = null;
            if($this->checkIfDoesNotExist($input)){
                $employee_details = $this->employeeDetails($input['resource_id']);
                $ispaid = $input['ispaid'];
                $pensioner = $this->query()->create([
                    'employee_id' => $input['resource_id'],
                    'benefit_type_id' => $input['benefit_type_id'],
                    'compensation_payment_type_id' => $input['compensation_payment_type_id'],
                    'monthly_pension_amount' => $input['monthly_pension_amount'],
                    'lastresponse' => Carbon::parse('now')->format('Y-m-d'),
                    'firstname' => $employee_details['firstname'],
                    'middlename' => $employee_details['middlename'],
                    'lastname' => $employee_details['lastname'],
                    'accountno' => $input['accountno'],
                    'bank_branch_id' => $input['bank_branch_id'],
                    'bank_id' => $input['bank_id'],
                    'manual_notification_report_id' => isset($input['manual_notification_report_id']) ? $input['manual_notification_report_id'] : null,
                    'notification_report_id' => isset($input['notification_report_id']) ? $input['notification_report_id'] : null,
                    'firstpay_manual' => $ispaid,
                    'dob' =>  isset($input['dob']) ? standard_date_format($input['dob']) : null,
                    'phone' =>  isset($input['phone']) ? $input['phone'] : null,
                ]);
            }

            if($pensioner){
                /*Enroll pensioner*/
                //TODO After improvement - comment this <activateForPayroll>
//                if(env('TESTING_MODE') == 0) {
//                    $this->activateForPayroll($pensioner);
//                }
                /*update first pay flag*/
                $status = $ispaid;
                $this->updateFirstPayFlag($pensioner, $status);
            }

            return $pensioner;
        });
    }

    /**
     * @param $id
     * @param $manual
     * Merge pensioner info who exist on the system but processed payroll manually
     * And auto enroll pensioner into payroll
     */
    public function mergeManualPensioner($id, array $input)
    {
        return DB::transaction(function () use ($id,$input) {
            $pensioner = $this->find($id);
            $pensioner->update([
                'monthly_pension_amount' => isset($input['mp'])  ? str_replace(",", "", $input['mp']) : null,
                'dob' => standard_date_format($input['dob']),
                'firstname' => $input['firstname'],
                'middlename' => $input['middlename'],
                'lastname' => $input['lastname'],
                'accountno' => $input['accountno'],
                'bank_branch_id' =>isset( $input['bank_branch']) ?  $input['bank_branch'] : null,
                'bank_id' => $input['bank'],
                'phone' => $input['phone'],
                'email' => $input['email'],
                'firstpay_manual' => 1,
            ]);
            //TODO After improvement - comment this <activateForPayroll>
            /*Enroll pensioner*/
//            if(env('TESTING_MODE') == 0) {
//                $this->activateForPayroll($pensioner);
//            }
            /*update firstpay flag*/
            $this->updateFirstPayFlag($pensioner);
            return $pensioner;
        });

    }

    /**
     * @param $id
     * @param $employee_id
     * @param $status
     * @return mixed
     * Update manual pay status when merging manual payroll and system payroll
     */
    public function updateManualPayStatus($id, $employee_id, $status){
        return DB::transaction(function () use ($id, $employee_id, $status) {
            $pensioner = $this->find($id);
            switch ($status){
                case 0:
                    /*Not yet paid*/
                    $pensioner->update(['firstpay_manual' => 0]);
                    break;
                case 1:
                    /*Already paid*/
                    break;
                case 2:
                    /*Already paid and terminated already*/
                    $pensioner->update(['firstpay_manual' => 1, 'isactive' => 2, 'firstpay_flag' => 1]);
                    break;

                case 3:
                    /*Reset status*/
                    $pensioner->update(['firstpay_manual' => 2, ]);
                    break;
            }
        });

    }

    /**
     * @param $input
     * Check if pensioner does not exist  when adding new
     */
    public function checkIfDoesNotExist($input)
    {
//        $notification_eligible_benefit_id = (isset($input['notification_eligible_benefit_id']) ? $input['notification_eligible_benefit_id'] : NULL);
        $check = $this->getPensionerExistByInput($input);

        return ($check) ? false : true;
    }

    /*find pensioner exists by input*/
    public function getPensionerExistByInput(array $input){
        $notification_eligible_benefit_id = (isset($input['notification_eligible_benefit_id']) ? $input['notification_eligible_benefit_id'] : NULL);
        $pensioner = $this->query()->where('employee_id', $input['resource_id'])->where('benefit_type_id', $input['benefit_type_id'])->where('notification_eligible_benefit_id', $notification_eligible_benefit_id)->first();
        return $pensioner;
    }

    /**
     * @param $employee_id
     * Get employee details to save into pensioner
     */
    public function employeeDetails($employee_id)
    {
        $employee = $this->employees->find($employee_id);
        return ['firstname' => $employee->firstname, 'middlename' => $employee->middlename, 'lastname' => $employee->lastname, 'bank_branch_id' => $employee->bank_branch_id, 'accountno' => $employee->accountno , 'email' => $employee->email];
    }

    /*
     * update
     */
    public function update($id, $input) {

    }

    /**
     * @param $pensioner
     * Activate for Monthly Payroll
     * Need update when developing Payroll administration
     */
    public function updateFirstPayFlag($pensioner, $status = 1){
        $pensioner->update(['firstpay_flag'=> $status,]);
    }




    /**
     * @param $pensioner
     * Activate for Monthly Payroll
     * Need update when developing Payroll administration
     */
    public function activateForPayroll($pensioner){
        $pensioner->update(['isactive'=> 1, 'isresponded'=> 1]);
    }


    /**
     * @param $pensioner
     * Deactivate pensioner
     */
    public function deactivate($pensioner){
        $pensioner->update(['isactive'=> 0]);
    }


    /*
     * Update Suspense flag after payroll is approved for reinstated pensioners only
     *
     */
    public function updateSuspenseFlagAfterReinstatedPayroll(Model $pensioner)
    {
        $pensioner->update([
            'suspense_flag' => 0,
        ]);
    }


    /*Query for flags for eligible for payroll*/
    public function queryEligibleForPayroll()
    {
        return $this->query()->where('isactive',1)->where('isresponded',1);
    }

    /**
     * Get pensioners eligible for Monthly Payroll
     */
    public function getEligibleForPayroll($run_date = null){
        $run_date = ($run_date == null) ? Carbon::now()->endOfMonth() : $run_date;
        $run_date = standard_date_format($run_date);

        /*new to accommodate manually processed*/
        $pensioners = $this->queryEligibleForPayroll()->where('suspense_flag','<>', 1);
        return $pensioners;
    }

    /**
     * Get pensioners eligible but suspended for Monthly Payroll
     */
    public function getEligibleSuspendedForPayroll($run_date = null){

        /*new to accommodate manually processed*/
        $pensioners = $this->queryEligibleForPayroll()->where('suspense_flag', 1);
        return $pensioners;
    }


    /**
     * Get Reinstated Pensioners for payroll
     */
    public function getReinstatedForPayroll(){
        $pensioners = $this->queryEligibleForPayroll()->where('suspense_flag', 3);
        return $pensioners;
    }


    /**
     * @return mixed
     * Get all eligible pensioners for system payroll reconciliations
     */
    public function getEligibleForReconciliation()
    {
        $pensioners_to_reconcile = $this->query()->where('isactive',1)->where('firstpay_flag', 1)->where('firstpay_manual', 0)->whereDoesntHave('payrollRecoveries', function($query){
            $query->where('member_type_id', 5)->where('wf_done',0 );
        });
        return $pensioners_to_reconcile;
    }



    /**
     * @param Model $pensioner
     * @param $notification_report_id
     * Get Monthly pension
     */
    public function getMonthlyPensionPerNotification($id, $employee_id)
    {
        $mp = $this->query()->where('id', $id)->where('employee_id', $employee_id)->first()->monthly_pension_amount;
        return $mp;
    }



    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * getRegistered dependents
     */
    public function getRegisteredPensioners($q)
    {
        $name = $q;
        $data['items'] = $this->query()->select(['id', DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as pensioner")])->whereRaw("firstname like :firstname or middlename like :middlename or lastname like :lastname or concat_ws(' ',firstname, coalesce(middlename, ''), lastname)  like :fullname ", ['firstname' => "%$name%", 'middlename' => "%$name%", 'lastname' => "%$name%", 'fullname' => "%$name%"])->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }


    /**
     * Get pensioners for Datatable
     */
    public function getForDataTable()
    {
        return  $this->query()->select([
            'pensioners.employee_id',
            DB::raw("pensioners.id as id"),
            DB::raw("INITCAP(concat_ws(' ', pensioners.firstname, coalesce(pensioners.middlename, ''), pensioners.lastname)) as fullname"),
            DB::raw("benefit_types.name as benefit_type"),
            DB::raw("pensioners.monthly_pension_amount as mp"),
            DB::raw("pensioners.isactive as isactive"),
            DB::raw("pensioners.suspense_flag as suspense_flag"),
            DB::raw("pensioners.suspense_flag"),
            DB::raw("compensation_payment_types.name as payment_type"),
            DB::raw("notification_reports.filename as filename"),
            DB::raw("manual_notification_reports.case_no as case_no"),
            DB::raw("pensioners.firstname"),
            DB::raw("pensioners.middlename"),
            DB::raw("pensioners.lastname"),
            DB::raw("pensioners.dob as dob"),
            DB::raw("pensioners.lastresponse as lastresponse"),
            DB::raw("pensioners.firstpay_manual as firstpay_manual"),
            DB::raw("notification_eligible_benefits.processed as processed"),
        ])
            ->join("employees", "employees.id", "=", "pensioners.employee_id")
            ->join("benefit_types", "benefit_types.id", "=", "pensioners.benefit_type_id")
            ->join("compensation_payment_types", "compensation_payment_types.id", "=", "pensioners.compensation_payment_type_id")
            ->leftjoin("notification_reports", "notification_reports.id", "=", "pensioners.notification_report_id")
            ->leftjoin("manual_notification_reports", "manual_notification_reports.id", "=", "pensioners.manual_notification_report_id")
            ->leftjoin("notification_eligible_benefits", "notification_eligible_benefits.id", "=", "pensioners.notification_eligible_benefit_id");
    }


    /**
     * Get all enrolled pensioners for DataTable - Active in payroll
     */
    public function getEnrolledPensionersForDataTable()
    {
        return $this->getForDataTable()->where('pensioners.isactive','<>', 0);
    }



    /**
     * Get all active pensioners for DataTable - Active in payroll
     */
    public function getActiveForDataTable()
    {
        return $this->getForDataTable()->where('pensioners.isactive', 1);
    }

    /**
     * Get pensioners who are ready to be enrolled into payroll
     */
    public function getReadyForPayrollForDataTable()
    {

        $status_active_cv_id = (new CodeValueRepository())->query()->where('reference', 'PSCACT')->first()->id;
        $pensioner_query =  $this->getForDataTable()->where('isactive', 0)
            ->leftJoin('payroll_status_changes as ps',function($join) use($status_active_cv_id) {
                $join->on('ps.resource_id', 'pensioners.id')->where('ps.member_type_id', 5)->where('ps.status_change_cv_id',$status_active_cv_id)->whereNull('ps.deleted_at');
            })->whereNull('ps.id');


        /*retrieve only assigned on inbox task*/
        $input = request()->all();
        if(isset($input['isinbox_task'])){
            $pensioner_query = $pensioner_query->join('payroll_alert_tasks',function($join){
                $join->on('payroll_alert_tasks.resource_id','pensioners.id')->where('payroll_alert_tasks.member_type_id', 5);
            })->where('payroll_alert_tasks.user_id', access()->allUsers());
        }
        return $pensioner_query;

    }


    /**
     * Get pensioners who are ready to be enrolled into payroll for merging manual and system
     */
    public function getReadyForPayrollForManualMergeForDataTable()
    {
        return $this->getForDataTable()->where('isactive', 0)->where('firstpay_manual', 2);
    }

    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * Get Pensioners for select2
     */
    public function getPensionersForSelect($q, $page)
    {
        $name = strtolower($q);
        $data['items'] = $this->query()
            ->select([
                'id',
                DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as pensioner"),
            ])
            ->whereRaw("cast(concat_ws(' ', pensioners.firstname, coalesce(pensioners.middlename, ''), pensioners.lastname) as text)  ~* ? ", [$name])
            ->limit($page)
            ->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }


    /*Get Pending summary - pensioners to be merged with manual processing of payroll*/
    public function getPendingManualMergeSummary(){
        $pending_pensioner_sync = $this->getReadyForPayrollForManualMergeForDataTable()->count();

        return [ 'pending_system_pensioner_sync' => $pending_pensioner_sync];
    }



    /*Get pensioners with missing details important i.e. dob, phone and bank*/
    public function getPensionersWithMissingDetails()
    {
        return $this->query()
            ->join('payroll_beneficiaries_view as b', function($join){
                $join->on('b.resource_id', 'pensioners.id')->where('b.member_type_id', 5)->whereRaw("b.employee_id = pensioners.employee_id");
            })
            ->where(function($query){
                $query->whereNull('pensioners.accountno')->orWhereIn('pensioners.bank_id',[5,6])->orWhereNull('pensioners.bank_id')->orWhereNull('pensioners.phone')->orWhereNull('pensioners.dob');
            })->where('pensioners.isactive','<>', 0)->where('pensioners.suspense_flag', '<>',1);
    }


    /**
     * @param $id
     * Find notification report i.e. system notification report or manual notification
     */
    public function findNotificationReport($id)
    {
        $pensioner = $this->find($id);
        $notification_report = null;
        if(isset($pensioner->notification_report_id)){
            $notification_report = (new NotificationReportRepository())->find($pensioner->notification_report_id);
        }elseif(isset($pensioner->manual_notification_report_id)){
            $notification_report = (new ManualNotificationReportRepository())->find($pensioner->manual_notification_report_id);
        }
        return $notification_report;
    }


    /*Get pensioners who are close to be verified within 2 next months or if already passed*/
    public function getVerificationAlertsForDt()
    {
        $alert_months = payroll_verification_alert_months();
        $verification_limit_days = (new PayrollVerificationRepository())->getDaysForVerificationAlert();
        $cut_off_date = Carbon::now()->addMonthNoOverflow($alert_months);
//        $pensioners = $this->getForDataTable()->distinct('pensioners.id')
//            ->leftJoin('payroll_verifications as v', function($join){
//                $join->on('v.resource_id', 'pensioners.id')->where('v.member_type_id', 5);
//            })->leftJoin('payroll_status_changes as s', function($join){
//                $join->on('s.resource_id', 'pensioners.id')->where('s.member_type_id', 5)->where('s.wf_done', 0);
//            })
//                        ->whereRaw("(pensioners.lastresponse::date +  (cast(? as varchar) || ' days')::interval) < ?", [$verification_limit_days, $cut_off_date ])->where('suspense_flag', 0)
//            ->whereRaw(" coalesce(s.wf_done, 1) = 1")->whereRaw(" pensioners.isactive not in (0,2)");

        $pensioners = (new PayrollVerificationRepository())->getQueryVerificationAllDistinct()
            ->leftJoin('payroll_status_changes as s', function($join){
                $join->on('s.resource_id', 'b.resource_id')->where('s.member_type_id', 5)->where('s.wf_done', 0);
            })->where('b.member_type_id', 5)->where('b.suspense_flag','<>', 1)->whereRaw(" coalesce(s.wf_done, 1) = 1")
            ->whereRaw("(b.lastresponse::date +  (cast(? as varchar) || ' days')::interval) < ?", [$verification_limit_days, $cut_off_date ]);

//            ->whereRaw("(v.verification_date::date +  (cast(? as varchar) || ' mons')::interval) < ?", [$verification_limit_months, $cut_off_date ])->where('suspense_flag', 0);
        return $pensioners;
    }



    /*Update reassessed Monthly pension */
    public function updateReassessedMp($id, $old_mp, $new_mp)
    {
        $pensioner = $this->find($id);
        $pensioner->update([
            'monthly_pension_amount' => $new_mp,
            'old_mp' => $old_mp
        ]);
    }


    /**
     * @param $pensioner_id
     * Get constant care assistants by pensioner for datatable
     */
    public function getConstantCaresByPensionerForDt($pensioner_id){

        $pensioner = $this->find($pensioner_id);
        return DB::table('payroll_beneficiaries_view as b')->where('b.employee_id', $pensioner->employee_id)->where('b.dependent_type_id', 7);
    }

}