<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Jobs\RunPayroll\PostPayrollAlertTaskChecker;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollBeneficiaryUpdate;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollBeneficiaryUpdateRepository extends  BaseRepository
{

    const MODEL = PayrollBeneficiaryUpdate::class;



    public function __construct()
    {

    }


    /*initiate new beneficiary update*/
    public function create(array $input)
    {
        return DB::transaction(function () use ($input) {
            $beneficiary_update = $this->query()->create([
                'resource_id' => $input['resource_id'],
                'member_type_id' => $input['member_type_id'],
                'old_values' => $this->serializeOldValues($input),
                'new_values' => $this->serializeNewValues($input),
                'user_id' => access()->id(),
                'remark' => (array_key_exists('remark', $input) ? $input['remark'] : null),
                'folionumber' => $input['folionumber'],
                'employee_id' => $input['employee_id'],

            ]);

            /* - Close payroll alert checker <payroll_inbox_task>*/
            $stage_cv_ref ='PALEPEDDET';
            dispatch(new PostPayrollAlertTaskChecker($stage_cv_ref, $input['member_type_id'], $input['resource_id'],1));

            return $beneficiary_update;
        });
    }


    /*update beneficiary update*/
    public function update(Model $beneficiary_update, array $input)
    {
        return DB::transaction(function () use ($beneficiary_update, $input) {
            $beneficiary_update->update([
                'new_values' =>$this->serializeNewValues($input),
                'user_id' => access()->id(),
                'remark' => (array_key_exists('remark', $input) ? $input['remark'] : null),
                'folionumber' => $input['folionumber'],
            ]);
            return $beneficiary_update;
        });
    }

    /**
     * @param array $input
     * Serialize old values
     */
    public function serializeOldValues(array $input)
    {
        $payrolls = new PayrollRepository();
        $resource= $payrolls->getResource($input['member_type_id'], $input['resource_id']);
        $old_values = ['firstname' => $resource->firstname, 'middlename' => $resource->middlename, 'lastname' => $resource->lastname, 'dob' => $resource->dob,
            'phone' => $resource->phone,'email' => $resource->email, 'region_id' => $resource->region_id, 'district_id' => $resource->district_id];

        return serialize($old_values);
    }

    /*Serialize new values*/
    public function serializeNewValues(array $input)
    {
        $payrolls = new PayrollRepository();

        $new_values = ['firstname' => $input['firstname'], 'middlename' => $input['middlename'], 'lastname' => $input['lastname'], 'dob' => $input['dob'],
            'phone' => $input['phone'],'email' => $input['email'] ,'region_id' => ($input['region_id'] ?? null), 'district_id' => ($input['district_id'] ?? null)];
        return serialize($new_values);
    }



    /**
     * @param $input
     * Check if can initiate new entry
     */
    public function checkIfCanInitiateNewEntry($member_type_id, $resource_id)
    {
        $pending_count = $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('wf_done', 0)->count();
        if($pending_count > 0){
            throw new GeneralException('There is pending approval for this beneficiary! Complete all pendings to proceed with new entry');
        }
    }


    /**
     * @param Model $beneficiary_update
     * @throws \Exception
     * Undo Beneficiary update
     */
    public function undo(Model $beneficiary_update)
    {
        DB::transaction(function () use ($beneficiary_update) {
            $id = $beneficiary_update->id;
            $beneficiary_update->delete();
            /*workflow delete*/
            $wf_module_group_id = 15;
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $id]);
            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();
        });
        }



    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get By beneficiary for datatable
     */
    public function getByBeneficiaryForDataTable($member_type_id, $resource_id)
    {
        return $this->query()->where('member_type_id', $member_type_id)->where('resource_id', $resource_id);
    }


    /**
     * @param array $input
     * Update beneficiary details when npt in payroll yet - This no need for workflow
     */
    public function updateWhenBeneficiaryIsNotActive(array $input)
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($input['member_type_id'], $input['resource_id']);
        /*update to resource table*/
        $resource->update([
            'firstname'=> $input['firstname'],
            'middlename'=> $input['middlename'],
            'lastname'=> $input['lastname'],
            'phone'=> $input['phone'],
            'dob'=> $input['dob'],
            'email'=> $input['email'],
//            'folionumber' => $input['folionumber'],
        ]);

        return $resource;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $document_type
     * Update document after being used to initiate approval for updating beneficiary details
     */
    public function updateBeneficiaryDocumentUsedPerType($member_type_id, $resource_id, $document_type)
    {
        $payroll_repo = new PayrollRepository();
        $payroll_repo->updateBeneficiaryDocumentUsedPerType($member_type_id, $resource_id, $document_type);
    }


    /**
     * @param Model $beneficiary_update
     *
     */
    public function updateOnWfComplete(Model $beneficiary_update)
    {
        $payrolls = new PayrollRepository();
        $resource = $payrolls->getResource($beneficiary_update->member_type_id, $beneficiary_update->resource_id);
        $unserialized_new_values = $beneficiary_update->unSerializedNewValues();
        /*update to resource table*/
        $resource->update([
            'firstname'=> $unserialized_new_values['firstname'],
            'middlename'=> $unserialized_new_values['middlename'] ?? $resource->middlename,
            'lastname'=> $unserialized_new_values['lastname'],
            'phone'=> $unserialized_new_values['phone'],
            'dob'=> $unserialized_new_values['dob'],
            'email'=> $unserialized_new_values['email'] ?? $resource->email,
            'region_id'=> $unserialized_new_values['region_id'] ?? $resource->regions_id,
            'district_id'=> $unserialized_new_values['district_id'] ?? $resource->district_id,
        ]);

        /*update document used - folio*/
        $document_type = 65;
        $this->updateBeneficiaryDocumentUsedPerType($beneficiary_update->member_type_id, $beneficiary_update->resource_id, $document_type);

        /*For pensioner should update employee table*/
        if($beneficiary_update->member_type_id == 5){
            $input = ['firstname' => $unserialized_new_values['firstname'], 'middlename' => $unserialized_new_values['middlename'], 'lastname' => $unserialized_new_values['lastname'], 'phone' => $unserialized_new_values['phone'], 'dob' => $unserialized_new_values['dob'], 'email' => $unserialized_new_values['email']];
            (new EmployeeRepository())->UpdateDetailsFromPayrollApprovals($resource->employee_id, $input);
        }

    }


}