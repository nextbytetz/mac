<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Jobs\RunPayroll\PostEoffice\PostBeneficiaryToDms;
use App\Jobs\RunPayroll\PostEoffice\PostBulkBeneficiariesToDms;
use App\Jobs\RunPayroll\PostPayrollAlertTaskChecker;
use App\Jobs\RunPayroll\Suspension\SystemSuspensions;

use App\Models\Operation\Claim\Document;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Compliance\Member\Dependent;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\Pensioner;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunVoucherRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\DocBlock\StandardTagFactory;

class PayrollRepository extends  BaseRepository
{


    protected $pensioners;
    protected $dependents;
    protected $payroll_procs;
    protected $payroll_run_approvals;

    public function __construct()
    {
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->payroll_procs = new PayrollProcRepository();
        $this->payroll_run_approvals = new PayrollRunApprovalRepository();


    }


    /**
     * @param array $input
     * Process Monthly Payroll
     */
    public function processPayroll(array $input)
    {
        return DB::transaction(function () use ($input) {
            $user_id = access()->id();
            $run_date = $input['run_date'];
            /*Check run date if is valid*/
            $this->payroll_procs->checkValidationOnRunDateForPayroll(2, $run_date);
            //CHECK For pension payroll == 2
            $run_date = Carbon::parse($input['run_date']);
            $run_date = $run_date->lastOfMonth();
            $run_month = Carbon::parse($run_date)->format("M y");
            $create_input = ['description' => 'MONTHLY PENSION', 'user_id' => $user_id, 'bquarter' => 'MONTHLY PENSION' . ' - ' . $run_month, 'payroll_proc_type_id' => 2, 'run_date' => $run_date];
            /*create payroll_proc*/
            $payroll_proc = $this->payroll_procs->create($create_input);
            /*create payroll run approval entry*/
            $payroll_run_approval = $this->payroll_run_approvals->create($payroll_proc->id);

            return $payroll_run_approval;
        });
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * Post beneficiary file into e-office for both i.e. pensioners/employees and survivors
     */
    public function postBeneficiaryToDms($member_type_id, $resource_id, $employee_id)
    {
        $resource = $this->getResource($member_type_id, $resource_id);
        if($resource->isdmsposted == 0){
            dispatch(new PostBeneficiaryToDms($resource, $member_type_id, $employee_id));
        }
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * Re-Post beneficiary file into e-office for both i.e. pensioners/employees and survivors
     */
    public function repostBeneficiaryToDms($member_type_id, $resource_id, $employee_id)
    {
        $resource = $this->getResource($member_type_id, $resource_id);
//        if($resource->isdmsposted == 0 ){
        dispatch(new PostBeneficiaryToDms($resource, $member_type_id, $employee_id));
//        }
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * Post bulk pending payroll beneficiaries
     */
    public function postBulkBeneficiariesToDms()
    {
        /*Post pensioners*/
        $pending_pensioners = $this->pensioners->query()->where('isactive', '<>', 0)->where('isdmsposted', 0)->get();
        dispatch(new PostBulkBeneficiariesToDms($pending_pensioners, 5));
        /*end posting pensioners*/

        /*Post dependents*/
        $pending_dependent_ids = $this->dependents->query()->where('isdmsposted', 0)->get()->pluck('id');
        $pending_dependent_employees = DB::table('dependent_employee')->whereIn('dependent_id', $pending_dependent_ids)->where('isactive', '<>', 0)->get();
        dispatch(new PostBulkBeneficiariesToDms($pending_dependent_employees, 4));
        /*end posting dependents*/
    }



    /**
     *
     * Auto suspend pension beneficiary i.e. dependents and pensioners
     */
    public function pensionSuspension()
    {
        $verification_limit_days = sysdefs()->data()->pension_verification_limit_days;
        $today = Carbon::now();
        $last_response_limit = $today->subDays($verification_limit_days);
        /*Suspend pensioners*/
        $this->pensioners->query()->where('isactive',1)->where('suspense_flag', 0)->whereDate("lastresponse", '<', $last_response_limit)->select(['id'])->chunk(100, function ($pensioners) {
            if (isset($pensioners)){
                dispatch(new SystemSuspensions($pensioners, 5));
//                $this->systemSuspend($pensioners);
            }
        });
        /*Suspend dependents*/
        $this->dependents->query()->where('suspense_flag', 0)->whereDate("lastresponse", '<', $last_response_limit)->whereHas('employees', function ($query){
            $query->where('dependent_employee.isactive',1)->where('survivor_pension_amount', '>', 0);
        })->select(['id'])->chunk(100, function
        ($dependents) {
            if (isset($dependents)){
                dispatch(new SystemSuspensions($dependents, 4));
            }
        });

    }

    /**
     * @param array $input
     * Process buld suspension after verification process quartely
     */
    public function bulkSuspensionAfterVerification(Model $payroll_manual_suspension)
    {
        return DB::transaction(function () use ( $payroll_manual_suspension) {
            $payroll_manual_suspension_id = $payroll_manual_suspension->id;
            $start_date = $payroll_manual_suspension->start_date;
            /*Suspend pensioners*/
            $this->pensioners->query()->where('isactive', 1)->where('suspense_flag', 0)->whereDate("lastresponse", '<', $start_date)->select(['id'])->chunk(100, function ($pensioners) use ($payroll_manual_suspension_id) {
                if (isset($pensioners)) {
                    dispatch(new SystemSuspensions($pensioners, 5, $payroll_manual_suspension_id));
//                $this->systemSuspend($pensioners);
                }
            });
            /*Suspend dependents*/
            $this->dependents->query()->where('suspense_flag', 0)->whereDate("lastresponse", '<', $start_date)->whereHas('employees', function ($query) {
                $query->where('dependent_employee.isactive', 1)->where('survivor_pension_amount', '>', 0);
            })->select(['id'])->chunk(100, function ($dependents) use ($payroll_manual_suspension_id) {
                if (isset($dependents)) {
                    dispatch(new SystemSuspensions($dependents, 4, $payroll_manual_suspension_id));
                }
            });
        });
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * Get Beneficiary / resource based on the member type
     */
    public function getResource($member_type_id, $resource_id)
    {
        if($member_type_id == 5){
            /*Pensioner*/
            $resource = $this->pensioners->find($resource_id);
        }elseif($member_type_id == 4){
            /*Dependents*/
            $resource = $this->dependents->find($resource_id);
        }
        return $resource;
    }




    /*Return resource profile page url*/
    public function returnResourceProfileUrl($member_type_id, $resource_id,$employee_id)
    {
        if($member_type_id == 5){
            /*Pensioner*/
            $url = 'payroll/pensioner/profile/'.$resource_id;
        }elseif($member_type_id == 4){
            /*dependent*/
            $resource = $this->getResource($member_type_id, $resource_id);
            $dependent_employee = $resource->getDependentEmployee($employee_id);
            $url = 'compliance/dependent/profile/'.$dependent_employee->id;
        }
        return $url;
    }
    /*Return resource profile page url*/
    public function returnResourceProfileRoute($member_type_id, $resource_id,$employee_id)
    {
        if($member_type_id == 5){
            /*Pensioner*/
            $route =          'backend.payroll.pensioner.profile';
            $parameter = $resource_id;
        }elseif($member_type_id == 4){
            /*dependent*/
            $resource = $this->getResource($member_type_id, $resource_id);
            $dependent_employee = $resource->getDependentEmployee($employee_id);
            $route = 'backend.compliance.dependent.profile';
            $parameter =  $dependent_employee->id;
        }
//
        return ['route' => $route, 'parameter' => $parameter];
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * Check if beneficiary is active for atleast one payroll
     */
    public function checkIfResourceAlreadyEnrolled($member_type_id, $resource_id)
    {
        $return = false;
        $resource = $this->getResource($member_type_id, $resource_id);
        if($member_type_id == 5){
            /*pensioners*/
            $return = ($resource->isactive != 0) ? true : false;
        }else{
            /*dependents*/
            $active_dependent_employee = DB::table('dependent_employee')->where('dependent_id',$resource_id)->where('isactive','<>', 0)->count();
            $return = ($active_dependent_employee > 0) ? true : false;
        }
        return $return;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * Get MP - Monthly pension of beneficiry per notification
     */
    public function getMp($member_type_id, $resource_id, $employee_id)
    {
        if($member_type_id == 5){
            /*Pensioner*/
            $resource = $this->pensioners->find($resource_id);
            $mp = $resource->monthly_pension_amount;
        }elseif($member_type_id == 4){
            /*Dependents*/
            $mp = $this->dependents->getMonthlyPensionPerNotification($resource_id,$employee_id);

        }
        return $mp;
    }

    public function updateMp($member_type_id, $resource_id, $employee_id)
    {
        if($member_type_id == 5){
            /*Pensioner*/
            $resource = $this->pensioners->find($resource_id);
            $mp = $resource->monthly_pension_amount;
        }elseif($member_type_id == 4){
            /*Dependents*/
            $mp = $this->dependents->getMonthlyPensionPerNotification($resource_id,$employee_id);

        }
        return $mp;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Check if beneficiary if being paid for the firsttime
     */
    public function checkIfIsTheFirstPayee($member_type_id, $resource_id, $employee_id)
    {
        $return =  false;
        if($member_type_id == 5){
            /*Pensioner*/
            $resource = $this->pensioners->find($resource_id);
            $return = ($resource->firstpay_flag == 1 ) ? false : true;
        }elseif($member_type_id == 4){
            /*Dependents*/
            $resource = $this->dependents->find($resource_id);
            $return = $resource->isFirstPaid($employee_id) ? false : true;
        }
        return $return;
    }


    /**
     * Update beneficiary document
     */
    public function updateBeneficiaryDocument($file_number, $document_id,array $input)
    {
        return DB::transaction(function () use ($file_number,$document_id, $input) {
            $member_type_id = null;
            if($input['fileType'] == 6){
                /*pensioners*/
                $member_type_id = 5;
            }elseif($input['fileType']== 7){
                /*dependents*/
                $member_type_id = 4;
            }

            $count = DB::table('document_payroll_beneficiary')->where(['member_type_id'=> $member_type_id, 'resource_id' => $file_number, 'document_id' => $document_id, 'eoffice_document_id' => $input['documentId']])->count();

            $dateOnDocument = isset($input['dateOnDocument']) ? $input['dateOnDocument'] : NULL;
            $receivedDate = isset($input['receivedDate']) ? $input['receivedDate'] : NULL;
            $createdAt = isset($input['createdAt']) ? $input['createdAt'] : NULL;
            $folioNumber = isset($input['folioNumber']) ? $input['folioNumber'] : NULL;

            if (!$count) {
                //create
                DB::table('document_payroll_beneficiary')->insert(['member_type_id'=> $member_type_id, 'resource_id' => $file_number, 'document_id' => $document_id, 'eoffice_document_id' => $input['documentId'],'name' => 'e-office',  'description' => $input['subject'], 'created_at' => Carbon::now(), 'folio' => $folioNumber, 'doc_date' => $dateOnDocument, 'doc_receive_date' => $receivedDate, 'doc_create_date' => $createdAt]);
            } else {
                //update
                DB::table('document_payroll_beneficiary')->where(['member_type_id'=> $member_type_id, 'resource_id' => $file_number, 'eoffice_document_id' => $input['documentId']])->update([ 'name' => 'e-office','description' => $input['subject'], 'created_at' => Carbon::now(), 'folio' => $folioNumber, 'doc_date' => $dateOnDocument, 'doc_receive_date' => $receivedDate, 'doc_create_date' => $createdAt]);
            }


            /*Post payroll alert task - checker*/
            $doc_saved = DB::table('document_payroll_beneficiary')->where(['member_type_id'=> $member_type_id, 'resource_id' => $file_number, 'document_id' => $document_id, 'eoffice_document_id' => $input['documentId']])->first();
            $stage_cv_ref = null;
            switch($document_id){
                case 63://verification
                    $stage_cv_ref = 'PALEPEDVER';
                    break;
                case 64://bank details
                    $stage_cv_ref = 'PALEPEDBAN';
                    break;
                case 65://details
                    $stage_cv_ref = 'PALEPEDDET';
                    break;
            }

            if($stage_cv_ref){
                dispatch(new PostPayrollAlertTaskChecker($stage_cv_ref, $member_type_id, $file_number,0, ['resource_pivot_id' => $doc_saved->id]));
            }

        });


    }


    /**
     * Update Beneficiary doc use status
     */
    public function updateBeneficiaryDocUseStatus($doc_id, $status)
    {
        $doc = DB::table('document_payroll_beneficiary')->where('id', $doc_id)->update([
            'isused' => $status,
            'ispending' => 0
        ]);
    }


    public function updateBeneficiaryDocUsePendingStatus($doc_id, $status, $pending_status)
    {
        $doc = DB::table('document_payroll_beneficiary')->where('id', $doc_id)->update([
            'isused' => $status,
            'ispending' => $pending_status
        ]);
    }

    /**
     * Update payroll document
     */
    public function updatePayrollDocument($file_number, $document_id, array $input)
    {
        return DB::transaction(function () use ($file_number,$document_id, $input) {

            $count = DB::table('document_payroll')->where(['payroll_run_approval_id' => $file_number, 'document_id' => $document_id, 'eoffice_document_id' => $input['documentId']])->count();

            $dateOnDocument = isset($input['dateOnDocument']) ? $input['dateOnDocument'] : NULL;
            $receivedDate = isset($input['receivedDate']) ? $input['receivedDate'] : NULL;
            $createdAt = isset($input['createdAt']) ? $input['createdAt'] : NULL;
            $folioNumber = isset($input['folioNumber']) ? $input['folioNumber'] : NULL;

            if (!$count) {
                //create
                DB::table('document_payroll')->insert(['payroll_run_approval_id' => $file_number, 'document_id' => $document_id, 'eoffice_document_id' => $input['documentId'],'name' => 'e-office',  'description' => $input['subject'], 'created_at' => Carbon::now(), 'folio' => $folioNumber, 'doc_date' => $dateOnDocument, 'doc_receive_date' => $receivedDate, 'doc_create_date' => $createdAt]);
            } else {
                //update
                DB::table('document_payroll')->where(['payroll_run_approval_id'=> $file_number, 'eoffice_document_id' => $input['documentId']])->update([ 'name' => 'e-office','description' => $input['subject'], 'created_at' => Carbon::now(), 'folio' => $folioNumber, 'doc_date' => $dateOnDocument, 'doc_receive_date' => $receivedDate, 'doc_create_date' => $createdAt]);
            }

        });


    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $document_type
     * Get document pending to be used for validation of beneficiary information
     */
    public function getBeneficiaryDocumentsPendingValidation($member_type_id, $resource_id, $document_type)
    {
        $beneficiary = $this->getResource($member_type_id, $resource_id);
        $pending_documents = $beneficiary->documents()->where('document_id', $document_type)->where('isused', 0)->orderBy('doc_receive_date', 'desc')->get();
        return $pending_documents;
    }

    /*Get document already used for validation */
    public function getBeneficiaryDocumentValidatedPerType($member_type_id, $resource_id, $document_type)
    {
        $beneficiary = $this->getResource($member_type_id, $resource_id);
        $documents = $beneficiary->documents()->where('document_id', $document_type)->where('isused', 1)->orderBy('doc_receive_date', 'desc')->get();
        return $documents;
    }

    /*Get All documents by document*/
    public function getBeneficiaryDocumentsPerType($member_type_id, $resource_id, $document_type)
    {
        $beneficiary = $this->getResource($member_type_id, $resource_id);
        $documents = $beneficiary->documents()->where('document_id', $document_type)->orderBy('doc_receive_date', 'desc')->get();
        return $documents;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $document_type
     * Update beneficiary document already used for verification of beneficiary information change
     */
    public function updateBeneficiaryDocumentUsedPerType($member_type_id, $resource_id, $document_type)
    {

        $beneficiary = $this->getResource($member_type_id, $resource_id);
        DB::table('document_payroll_beneficiary')->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('document_id', $document_type)->where('isused', 0)->update([
            'isused' => 1
        ]);
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $document_type
     * Update beneficiary document already worked on for verification of beneficiary information change
     */
    public function updateBeneficiaryDocumentPendingStatusPerType($member_type_id, $resource_id, $document_type)
    {
        $beneficiary = $this->getResource($member_type_id, $resource_id);
        DB::table('document_payroll_beneficiary')->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->where('document_id', $document_type)->where('ispending', 1)->update([
            'ispending' => 0
        ]);
    }

    /**
     * @param $document_type
     * Get all documents of payroll beneficiaries for dataTable
     */
    public function getDocumentBeneficiariesForDataTable()
    {
        $query = DB::table('document_payroll_beneficiary')->select([
//            DB::raw("concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname) as dependent_name"),
//            DB::raw("concat_ws(' ', pensioners.firstname, coalesce(pensioners.middlename, ''), pensioners.lastname) as pensioner_name"),
            DB::raw("b.member_name as member_name"),
            DB::raw("b.filename as filename"),
            DB::raw("document_payroll_beneficiary.member_type_id as member_type_id"),
            DB::raw("member_types.name as member_type_name"),
            DB::raw("document_payroll_beneficiary.resource_id as resource_id"),
            DB::raw("document_payroll_beneficiary.description as description"),
            DB::raw("document_payroll_beneficiary.document_id as document_id"),
            DB::raw("documents.name as document_type_name"),
            DB::raw("document_payroll_beneficiary.isused as isused"),
            DB::raw("document_payroll_beneficiary.doc_date as doc_date"),
            DB::raw("document_payroll_beneficiary.doc_receive_date as doc_receive_date"),
            DB::raw("document_payroll_beneficiary.doc_create_date as doc_create_date"),
            DB::raw("document_payroll_beneficiary.folio as folio"),
            DB::raw("document_payroll_beneficiary.ispending as ispending"),

        ])
//            ->leftjoin("pensioners", function($join){
//                $join->on('document_payroll_beneficiary.resource_id', 'pensioners.id')
//                    ->where('document_payroll_beneficiary.member_type_id', 5);
//            })
//            ->leftjoin("dependents", function($join){
//                $join->on('document_payroll_beneficiary.resource_id', 'dependents.id')
//                    ->where('document_payroll_beneficiary.member_type_id', 4);
//            })
            ->join('payroll_beneficiaries_view as b', function($join){
                $join->on('b.resource_id', 'document_payroll_beneficiary.resource_id')->whereRaw('b.member_type_id = document_payroll_beneficiary.member_type_id');
            })
            ->join("member_types","document_payroll_beneficiary.member_type_id", "member_types.id")
            ->join("documents","document_payroll_beneficiary.document_id", "documents.id");

        /*retrieve only assigned on inbox task*/
        $input = request()->all();
        if(isset($input['isinbox_task'])){
            $query = $query->join('payroll_alert_tasks',function($join){
                $join->on('payroll_alert_tasks.resource_pivot_id','document_payroll_beneficiary.id');
            })->whereIn('payroll_alert_tasks.user_id', access()->allUsers());
        }
        return $query;
    }



    /**
     * @param $document_type
     * Get pending documents from e-office which trigger beneficiary information updates
     */
    public function getPendingDocsForBeneficiaryUpdateDataTable($document_type)
    {
        $documents = $this->getDocumentBeneficiariesForDataTable()->where('document_payroll_beneficiary.ispending', 1)->where('document_payroll_beneficiary.isused',0)->where('document_payroll_beneficiary.document_id', $document_type);
        return $documents;

    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $document_type_id
     * Resolve redirect route for action need for pending document of beneficiaries.
     */
    public function getPendingDocsForBeneficiaryAction($member_type_id, $resource_id, $document_type_id)
    {
        $resource = $this->getResource($member_type_id, $resource_id);
        if($member_type_id == 5)
        {
            /*pensioner*/
            $employee_id = $resource->employee_id;
        }else{
            /*dependent*/
            $employee_id = $resource->employees()->first()->pivot->employee_id;
        }


        switch($document_type_id){
            case 63:
                /*verification*/
                return redirect()->route('backend.payroll.verification.create',['member_type' => $member_type_id, 'resource' => $resource->id, 'employee_id' => $employee_id]);
                break;
            case 64:
                /*bank update*/
                return redirect()->route('backend.payroll.create_new_bank',['member_type' => $member_type_id, 'resource' => $resource->id, 'employee_id' => $employee_id]);
                break;
            case 65:
                /*beneficiary update*/
                return redirect()->route('backend.payroll.beneficiary_update.create',['member_type' => $member_type_id, 'resource' => $resource->id, 'employee_id' => $employee_id]);
                break;
        }
    }

    /*Get pending documents by beneficiary and document type*/
    public function getPendingDocsByBeneficiaryDataTable($member_type_id, $resource_id, $document_type)
    {
        $documents = $this->getDocumentBeneficiariesForDataTable()->where('document_payroll_beneficiary.ispending', 1)->where('document_payroll_beneficiary.document_id', $document_type)->where('document_payroll_beneficiary.member_type_id', $member_type_id)->where('document_payroll_beneficiary.resource_id', $resource_id);
        return $documents;

    }


    /*Check if document for action uploaded on e-office*/
    public function checkIfThereIsDocumentForAction($member_type_id, $resource_id,$document_type_id)
    {
        $payrolls = new PayrollRepository();
        $document = Document::query()->find($document_type_id);
        $pending_docs = $payrolls->getPendingDocsByBeneficiaryDataTable($member_type_id, $resource_id, $document_type_id)->count();

        if($pending_docs == 0){
            /*if there is no pending doc*/
            throw new GeneralException( $document->name . ' not uploaded for this beneficiary! Please upload form on e-office to proceed.');
        }
    }


    /**
     * @param Model $payroll_run
     * Get api fields for export to erp for each beneficiary
     */
//    public function getApiFieldsForPendingExports(Model $payroll_run)
//    {
//        $member_types = new MemberTypeRepository();
//        $member_type_id = $payroll_run->member_type_id;
//        $resource_id = $payroll_run->resource_id;
//        $employee_id = $payroll_run->employee_id;
//        $member_type = $member_types->find($member_type_id);
//        $notification_report =  $this->getNotificationReportByMember($member_type_id, $resource_id, $employee_id);
//        $member = $member_types->getMember($member_type_id, $resource_id);
//
//        if(isset($payroll_run->bank_branch_id)){
//            $bank = DB::table('main.bank_branches')
//                ->join('main.banks', 'bank_branches.bank_id', '=', 'banks.id')
//                ->where('bank_branches.id',$payroll_run->bank_branch_id)
//                ->first();
//            $bank_branch_id = $payroll_run->bank_branch_id;
//            $bank_name = 'CRDB';
//            $accountno = $payroll_run->accountno;
//            $swift_code = ($bank_name == 'CRDB') ? 'CORUTZTZ' : 'NMIBTZTZ';
//        }else{
//            $bank_branch_id = 0;
//            $bank_name = 'CRDB';
//            $accountno = '00';
//            $swift_code = 'CORUTZTZ';
//        }
//        $benefit_type = $this->getBenefitTypeByMember($member_type_id, $resource_id,$employee_id);
//        $gfs_code = $benefit_type->finCode->gfs_code;
//        $memberno =  (new PaymentVoucherTransactionRepository())->getMemberNoForErpApi($member, $member_type_id, $employee_id);
//        $member_type = ($benefit_type->id == 5) ?  'Constant Care Assistant' : $member_type->name;
//        return [
//            'filename' => $notification_report->filename,
//            'payee' => $member->name ,
//            'memberno' =>$memberno,
//            'member_type' =>$member_type,
//            'benefit_type' => $benefit_type->name,
//            'amount' => $payroll_run->amount ,
////            'debit_account_expense' => '01.001.AB.000000.27110104.000.000',
//            'debit_account_expense' => '01.001.AB.000000.'. $gfs_code .'.000.000',
//            'credit_account_receivable' => '01.001.AB.000000.43181104.000.000',
//            'accountno' => $accountno,
//            'swift_code' => $swift_code,
//            'bank_branch_id' =>$bank_branch_id,
//            'bank_name' =>$bank_name,
//            'bank_address' =>'Dar es Salaam' ,
//            'country_name_bank' => 'Tanzania',
//            'city_of_bank' => 'Dar es Salaam' ,
//            'state_or_region' => 'Dar es Salaam',
//            'is_paid' => 0,
//        ];
//    }




    /**
     * @param $payroll_run_id
     * Update export flag
     */
    public function updateExportFlag($payroll_run_id){
        $payroll_run = (new PayrollRunVoucherRepository())->find($payroll_run_id);
        $payroll_run->update([
            'isexported' => 1,
        ]);
        /*Update payroll run approval payment process status*/
        /*update pay process status*/
        (new PayrollRunApprovalRepository())->updatePayStatus($payroll_run->payroll_run_approval_id);
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * @throws GeneralException
     * Get notification report either system or manual file
     */
    public function getNotificationReportByMember($member_type_id, $resource_id, $employee_id)
    {
        $notification_report = null;
        if($member_type_id == 4){
            /*dependent*/
            $notification_report = $this->dependents->findNotificationReportEnrolledDependent($resource_id, $employee_id);
        }elseif($member_type_id == 5){
            /*pensioners*/
            $notification_report = $this->pensioners->findNotificationReport($resource_id);
        }

        return $notification_report;
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * @return array
     * @throws GeneralException
     * Get Claim details by member
     */
    public function getClaimDetailsByMember($member_type_id, $resource_id, $employee_id)
    {
        $notification_report = $this->getNotificationReportByMember($member_type_id, $resource_id, $employee_id);
        $resource = $this->getResource($member_type_id, $resource_id);
        $pd = null;
        $gme = null;
        $return = [];
        if($resource->firstpay_manual == 1){
            /*manual*/
            $claim = $notification_report->claim;
            $gme =  null;
            $pd = $notification_report->man_pd ?? null;
        }else{
            $claim = $notification_report->claim;
            $gme =  $claim->monthly_earning ?? null;
            $pd =  $claim->pd ?? null;
        }

        /*Pd for manual system file*/
        if(isset($notification_report->manualSystemFile))
        {
            $pd = $notification_report->manualSystemFile->pd;
        }



        $return = [
            'monthly_earning' => $gme,
            'pd' => $pd,
        ];
        return $return;
    }
    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * Get benefit type by member i.e. Pension benefit
     */
    public function getBenefitTypeByMember($member_type_id, $resource_id, $employee_id){
        $benefit_type_repo = new BenefitTypeRepository();
        $benefit_type = null;
        $beneficiary = $this->getResource($member_type_id, $resource_id);

        if($member_type_id == 4){
            /*dependents*/
            $dependent_employee = $beneficiary->getDependentEmployee($employee_id);
            $dependent_type = $dependent_employee->dependent_type_id;
            if($dependent_type == 7){
                /*constant grant*/
                $benefit_type = $benefit_type_repo->find(5);
            }else{
                /*dependent mp*/
                $benefit_type =  $benefit_type_repo->find(6);
            }
        }elseif($member_type_id == 5){
            /*pensioners*/
            $benefit_type =  $benefit_type_repo->find(18);
        }
        return $benefit_type;
    }


    /**
     * @return mixed
     * Get Payroll pending workflow for datatable
     */
    public function getPayrollPendingWorkflowsForDataTable(){
        $pending_wfs = DB::table('payroll_pending_workflows_view');
        return $pending_wfs;
    }


    /**
     * @param $notification_report_id
     * @param int $isapproved
     * Get first payroll payment by employee - Monthly pension payroll
     */
    public function getFirstPayrollPaymentByEmployee($notification_report_id, $is_approved = 0)
    {
        $notification = NotificationReport::find($notification_report_id);
        $employee_id = $notification->employee_id;
        if($is_approved == 0) {
            $payroll_run = PayrollRun::query()->where('employee_id', $employee_id)->where('new_payee_flag', 1)->has('payrollRunApproval')->first();
        }else{
            $payroll_run = PayrollRun::query()->where('employee_id', $employee_id)->where('new_payee_flag',1)->whereHas('payrollRunApproval', function($query) use($is_approved){
                $query->where('wf_done',$is_approved )->first();
            });
        }
        if ($payroll_run) {
            $total_amount = $payroll_run->amount;
            $total_arrears = $payroll_run->arrears_amount;
            $arrears_month = $payroll_run->months_paid - 1;
        } else {
            $total_amount = 0;
            $total_arrears = 0;
            $arrears_month = 0;
        }

        return ['total_amount' => $total_amount, 'total_arrears' => $total_arrears, 'arrears_month' => $arrears_month];
    }



    /**
     * @param $notification_report_id
     * @param int $isapproved
     * Get first payroll amount estimated to be paid on the first payroll based on start pay date and payroll run date
     */
    public function getCurrentPayrollInfoByEmployee($notification_report_id, $run_date = null)
    {
        $pay_run_repo = new PayrollRunRepository;
        $notification = NotificationReport::find($notification_report_id);
        if(!isset($run_date)){
            $run_date =  standard_date_format(Carbon::now()->endOfMonth());
        }else{
            $run_date = standard_date_format(Carbon::parse($run_date)->endOfMonth());
        }
        $start_date = $pay_run_repo->getStartPayDate($notification);
        $total_months = $pay_run_repo->getFirstPayMonthDiff($start_date, $run_date);
        $arrears_month = $total_months - 1;
        //$total_arrears = $mp * $arrears_month;
        //$total_amount = $total_months * $mp;
        return ['arrears_month' => $arrears_month];
    }


    /*Check beneficiary has atleast on document from eoffice*/
    public function checkIfBeneficiaryHasDocument($member_type_id, $resource_id)
    {
        $check = DB::table('document_payroll_beneficiary')->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->whereNotNull('eoffice_document_id')->count();
        if($check > 0)
        {
            return true;
        }else{
            return false;
        }
    }


    /**
     * Get overage child eligible data when activating beneficiary
     */
    public function getOverageChildEligibleData($member_type_id, $resource_id, $employee_id)
    {
        $is_overage_eligible = 0;
        $first_pay_months = 0;
        $arrears = 0;
        $death_date = null;
        $child_deadline_eligible = null;
        $dob = null;
        $resource = $this->getResource($member_type_id, $resource_id);
        $dependent_employee = $resource->getDependentEmployee($employee_id);
        $mp = 0;
        if($member_type_id == 4 && $dependent_employee->dependent_type_id == 3 && $dependent_employee->isdisabled == 0 && $dependent_employee->iseducation == 0 ){
            //overage as of now() - today
            $check_if_underage = (new DependentRepository())->checkIfChildUnderAgeLimits($resource_id);
            if($check_if_underage == false){
                $dob = $resource->dob;
                $death_date = $resource->getStartPayDateAttribute($employee_id);
                $child_age_limit = sysdefs()->data()->payroll_child_limit_age; //18yrs
                $child_deadline_eligible =  Carbon::parse($resource->dob)->addYears($child_age_limit)->endOfMonth();
                if(comparable_date_format($child_deadline_eligible) > comparable_date_format($death_date)){
                    $first_pay_months = (new PayrollRunRepository())->getFirstPayMonthDiff(standard_date_format($death_date),standard_date_format( $child_deadline_eligible));//inclusive
                }else{
                    $first_pay_months = 0;
                }

                $mp = $dependent_employee->survivor_pension_amount;
                $arrears = $mp * $first_pay_months;
                $is_overage_eligible = ($first_pay_months > 0) ? 1 : 0;
            }
        }
        $summary = 'This child is already overage as of today, but was eligible for payroll at time of death of employee, hence dependent will be paid only once to recover all arrears of this amount Tshs ' . '<b class=underline>'.    number_2_format($arrears) . '</b>' .  ' for eligible period of '  . '<b class=underline>'.    number_2_format($first_pay_months) . '</b>' . ' months' . ' from death date, '  . '<b class=underline>'.  short_date_format($death_date) . '</b>'  . ' to ' . 'last month of eligibility, ' . '<b class=underline>'.  short_date_format($child_deadline_eligible) . '</b>' . '. Dependent once paid will automatically be deactivated from payroll. Date of birth of dependent is ' . '<b class=underline>'.    short_date_format($dob) . '.'.'</b>' . '</b>' . ' and monthly pension is Tshs ' . '<b class=underline>'.    number_2_format($mp) . '.'.'</b>';

        return ['is_overage_eligible' => $is_overage_eligible,
            'first_pay_months' => $first_pay_months,
            'arrears' => $arrears,
            'dob' => $dob,
            'death_date' => $death_date,
            'child_deadline_eligible' => $child_deadline_eligible,
            'summary'=>$summary
        ];
    }




    /*Get Arrears summary on Activation of beneficiary*/
    public function getArrearsSummaryOnActivation($member_type_id, $resource_id, $employee_id)
    {
        $enroll_date_descr = 'Enrollment date';
        $enrollment_date = null;
        $arrears = 0;
        $first_pay_months = 0;
        $mp = 0;
        $pending_manual_months = 0;
        $remained_payable_months = 0;
        $resource = $this->getResource($member_type_id, $resource_id);
//        $notification_report = $this->getNotificationReportByMember($member_type_id, $resource_id, $employee_id);
        $last_approved_payroll = (new PayrollRunApprovalRepository())->getLastPayroll();
        $last_run_date = (new PayrollProcRepository())->getPayrollRunDate($last_approved_payroll->id);
        $is_eligible_now = 1;
        $isotherdep_manual = 0;
        $summary = null;
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
        $first_pay_flag = ($member_type_id == 4) ?  $dependent_employee->firstpay_flag : $resource->firstpay_flag;
        $first_pay_flag_system = $resource->getFirstPayFlagSystemAttribute($employee_id);
        $firstpay_manual = isset($resource->firstpay_manual) ? $resource->firstpay_manual : 0;
        if($first_pay_flag == 0)
        {
            switch($member_type_id){
                case 4:
                    /*dependent*/
                    $enroll_date_descr = 'Death date of employee on ';
                    $enrollment_date = $resource->getStartPayDateAttribute($employee_id);
                    $dependent_employee = $resource->getDependentEmployee($employee_id);
                    $first_pay_flag = $dependent_employee->firstpay_flag;
                    $mp = $dependent_employee->survivor_pension_amount;
                    if($first_pay_flag == 0){

                        if($dependent_employee->dependent_type_id == 3) {
                            /*child*/
                            $check_if_underage = (new DependentRepository())->checkIfChildUnderAgeLimits($resource_id);
                            if ($check_if_underage == false) {
                                /*Overage by now*/
                                $overage_arrears_data = $this->getOverageChildEligibleData($member_type_id, $resource_id, $employee_id);
                                $arrears = $overage_arrears_data['arrears'];
                                $first_pay_months = $overage_arrears_data['first_pay_months'];
                                $summary = $overage_arrears_data['summary'];
                                $is_eligible_now = 0;
                            } else {
                                /*Child still underage */
                                $first_pay_months= (new PayrollRunRepository())->getFirstPayMonthDiff(standard_date_format($enrollment_date), standard_date_format($last_run_date));//inclusive

                            }

                        }else{
                            /*Normal eligible survivor*/

                            $first_pay_months = (new PayrollRunRepository())->getFirstPayMonthDiff(standard_date_format($enrollment_date),standard_date_format($last_run_date));//inclusive
                            $first_pay_months = $this->getFirstPayMonthsForDependents($dependent_employee, $first_pay_months);
                        }


                        /*Other dependents with full dependency -  Should always be on last if statement (Overwrite Normal Arrears summary from above*/
                        if($dependent_employee->isotherdep == 1){

                            if($resource->firstpay_manual == 1){
                                /*Other dependent but already paid manually*/
                                $first_pay_months = (new PayrollRunRepository())->getFirstPayMonthDiff(standard_date_format($enrollment_date),standard_date_format($last_run_date));//inclusive
                                $first_pay_months = $this->getFirstPayMonthsForDependents($dependent_employee, $first_pay_months);

                            }else{
                                /*Other dependent not yet paid manual*/
                                $first_pay_months = (new PayrollRunRepository())->getFirstPayMonthDiff(standard_date_format($enrollment_date),standard_date_format($last_run_date));//inclusive
                                $first_pay_months = $this->getFirstPayMonthsForDependents($dependent_employee, $first_pay_months);

                            }

                        }


                    }
                    break;

                case 5:
                    /*Pensioner*/
                    $enroll_date_descr = 'date of MMI ';
                    $enrollment_date = $resource->start_pay_date;
                    $first_pay_months = (new PayrollRunRepository())->getFirstPayMonthDiff(standard_date_format($enrollment_date),standard_date_format($last_run_date));
                    $mp = $resource->monthly_pension_amount;
                    break;

            }
        }


        /*Check if beneficiary is still eligible as by now*/
        if($is_eligible_now == 1)
        {
            $arrears = $mp * $first_pay_months;
            if($firstpay_manual == 0){
                /*From system*/
                $summary = 'This beneficiary will be paid arrears of total Tshs ' . '<b class=underline>'.    number_2_format($arrears) . '</b>' .  ' on his/her first payroll, Arrears is generated from eligible period of '  . '<b class=underline>'.    number_2_format($first_pay_months) . '</b>' . ' months' . ' from ' . $enroll_date_descr   . '<b class=underline>'.  short_date_format($enrollment_date) . '</b>'  . ' to ' . 'last payroll processed, ' . '<b class=underline>'.  short_date_format($last_run_date) . '</b>' .  '. Monthly pension eligible for this beneficiary is '  . '<b class=underline>'.    number_2_format($mp) . '</b>.';

            }else{
                /*From manual (Pensioner and Dependents*/
                switch($member_type_id){
                    case 4:
                        /*dependents*/
                        $summary = $this-> getArrearsSummaryForDependentantFromManual($resource, $dependent_employee)['summary'];
                        break;

                    case 5:
                        /*pensioners
                        */
                        $summary = $this-> getArrearsSummaryForPensionerFromManual($resource)['summary'];;
                        break;
                }
            }
        }


        return [
            'arrears' => $arrears,
            'first_pay_months' => $first_pay_months,
            'summary' => $summary
        ];


    }

    /*Arrears summary for other dependents from manual*/
    public function getArrearsSummaryForDependentantFromManual($dependent, $dependent_employee)
    {
        $isotherdep = $dependent_employee->isotherdep;
        $mp = $dependent_employee->survivor_pension_amount;
        $arrears = (new PayrollRecoveryRepository())->getByBeneficiaryForDataTable(4, $dependent->id, $dependent_employee->employee_id)->sum('total_amount');
        $arrears_month = $arrears / $mp ;

        if($isotherdep == 1){
            /*Other dependents*/
            $eligible_payable_months = $dependent_employee->pay_period_months;
            $recycles_pay_period = $dependent_employee->recycles_pay_period;
//            dd($arrears_month);
            $paid_months_manual = $eligible_payable_months - ($arrears_month + $recycles_pay_period);
            $paid_amount_manual = $paid_months_manual * $mp;
            $summary = 'This beneficiary will be paid arrears of total Tshs '  . '<b class=underline>'.    number_2_format($arrears) . '</b>' . ' on his/her first payroll, Arrears is generated from eligible period of '  . '<b class=underline>'.    number_2_format($arrears_month) . '</b>' . ' months which were hold on manual before system. Beneficiary already paid total Tshs ' . '<b class=underline>'.    number_2_format($paid_amount_manual)  . '</b>' . ' for eligible months of '  . '<b class=underline>'.    number_2_format($paid_months_manual) . '</b>'    . ' months through manual process. Remained payable ' . '<b class=underline>'.    number_2_format($recycles_pay_period) . '</b>' . ' months will be paid on monthly basis.'. 'Monthly pension eligible for this beneficiary is '  . '<b class=underline>'.    number_2_format($mp) . '</b>. and total payable months is ' .  '<b class=underline>'.    number_2_format($eligible_payable_months) . '</b>.' ;

        }else{
            /*Normal dependents i.e spouse/child*/
            $summary = 'This beneficiary will be paid arrears of total Tshs '  . '<b class=underline>'.    number_2_format($arrears) . '</b>' . ' on his/her first payroll, Arrears is generated from eligible period of '  . '<b class=underline>'.    number_2_format($arrears_month) . '</b>' . ' months which were hold on manual before system. Monthly pension eligible for this beneficiary is '  . '<b class=underline>'.    number_2_format($mp) . '</b>.';
        }
        return ['summary' => $summary];
    }



    /*Arrears summary for other dependents from manual*/

    public function getArrearsSummaryForPensionerFromManual($pensioner)
    {
        $mp = $pensioner->monthly_pension_amount;
        $arrears = (new PayrollRecoveryRepository())->getByBeneficiaryForDataTable(5, $pensioner->id, $pensioner->employee_id)->sum('total_amount');
        $arrears_month = $arrears / $mp ;
        /*Normal dependents i.e spouse/child*/
        $summary = 'This beneficiary will be paid arrears of total Tshs '  . '<b class=underline>'.    number_2_format($arrears) . '</b>' . ' on his/her first payroll, Arrears is generated from eligible period of '  . '<b class=underline>'.    number_2_format($arrears_month) . '</b>' . ' months which were hold on manual before system. Monthly pension eligible for this beneficiary is '  . '<b class=underline>'.    number_2_format($mp) . '</b>.';

        return ['summary' => $summary];
    }

    /*Get first pay months for dependents*/
    public function getFirstPayMonthsForDependents($dependent_employee, $first_pay_months)
    {
        $payable_months = $dependent_employee->pay_period_months;
        if($dependent_employee->isotherdep == 1){
            /*Other dependents - eligible*/
            if($payable_months < $first_pay_months){
                /*Limit to only payable months*/
                $first_pay_months = $payable_months;
            }
        }
        return $first_pay_months;
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * @param array $input
     * HOLD BENEFICIARY---
     */

    /*Update Hold Status*/
    public function holdBeneficiary($member_type_id, $resource_id, $employee_id, array $input = [])
    {
        DB::transaction(function () use ($member_type_id, $resource_id, $employee_id, $input) {
            $table_name = '';
            if($member_type_id == 4){
                $table_name = 'dependent_employee';
                $where_input = ['dependent_id' => $resource_id, 'employee_id' => $employee_id];
            }elseif($member_type_id == 5){
                $table_name = 'pensioners';
                $where_input = ['id' => $resource_id];
            }
            $update_input = ['hold_reason' => $input['hold_reason'], 'hold_date' => standard_date_format(getTodayDate()), 'isactive' => 3];

            $this->generalDbBuilderUpdateQuery($table_name, $where_input, $update_input);
        });

    }

    /*Release hold on beneficiary*/
    public function releaseHoldBeneficiary($member_type_id, $resource_id, $employee_id)
    {
        DB::transaction(function () use ($member_type_id, $resource_id, $employee_id) {
            $table_name = '';
            if($member_type_id == 4){
                $table_name = 'dependent_employee';
                $where_input = ['dependent_id' => $resource_id, 'employee_id' => $employee_id];
            }elseif($member_type_id == 5){
                $table_name = 'pensioners';
                $where_input = ['id' => $resource_id];
            }
            $update_input = [ 'isactive' => 0];

            $this->generalDbBuilderUpdateQuery($table_name, $where_input, $update_input);
        });

    }

    /*Get held beneficiaries for DataTable*/
    public function getHeldBeneficiariesForDt()
    {
        return DB::table('payroll_beneficiaries_view as b')->select(
            'b.member_name',
            't.name as member_type_name',
            'b.resource_id',
            'b.member_type_id',
            'b.employee_id',
            'b.hold_date'
        )->where('isactive',3 )
            ->join('member_types as t', 't.id', 'b.member_type_id');
    }

    /*eND: Hold beneficiary*/


    /*Get all suspended beneficiaries for Dt*/
    public function getAllSuspendedBeneficiariesForDt()
    {
        return DB::table('payroll_beneficiaries_view as b')->select(
            'b.filename',
            'b.member_name',
            't.name as member_type_name',
            'b.resource_id',
            'b.member_type_id',
            'b.employee_id',
            'b.lastresponse',
            'b.suspended_date'
        )->where('isactive',1 )->where('suspense_flag', 1)
            ->join('member_types as t', 't.id', 'b.member_type_id');
    }




    /*Quick fix for bulk upload payroll beneficiary to eoffice */
    public function postBulkBeneficiaryToEofficeQuickFix()
    {
        /*Post pensioners*/
        $pending_pensioners = $this->pensioners->query()->where('isactive', '<>', 0)->where('isdmsposted', 1)->get();
        dispatch(new PostBulkBeneficiariesToDms($pending_pensioners, 5));
        /*end posting pensioners*/

        /*Post dependents*/
        $pending_dependent_ids = $this->dependents->query()->where('isdmsposted', 1)->get()->pluck('id');
        $pending_dependent_employees = DB::table('dependent_employee')->whereIn('dependent_id', $pending_dependent_ids)->where('isactive', '<>', 0)->get();
        dispatch(new PostBulkBeneficiariesToDms($pending_dependent_employees, 4));
    }
}
