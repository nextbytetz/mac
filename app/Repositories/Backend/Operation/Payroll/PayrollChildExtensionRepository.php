<?php

namespace App\Repositories\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Jobs\RunPayroll\PostPayrollAlertTaskChecker;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\PayrollBeneficiaryUpdate;
use App\Models\Operation\Payroll\PayrollChildExtension;
use App\Models\Operation\Payroll\PayrollRun;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Operation\Payroll\PayrollUnclaim;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PayrollChildExtensionRepository extends  BaseRepository
{

    const MODEL = PayrollChildExtension::class;



    public function __construct()
    {

    }

    /**
     * @param $payroll_status_change_id
     * @param array $input
     * @return mixed
     * Create
     */
    public function create($payroll_status_change_id, array $input)
    {
        return DB::transaction(function () use ($input, $payroll_status_change_id) {
            $child_extension = $this->query()->create([
                'payroll_status_change_id' =>$payroll_status_change_id,
                'child_continuation_reason_cv_id' => $input['child_continuation_reason_cv_id'],
                'period_provision_type' => ($input['child_continuation_reason_cv_ref'] == 'PAYCHCOSCH') ? 1 : null,
                'deadline_date' => isset($input['deadline_date']) ? $input['deadline_date'] : null,
                'school_name' => isset($input['school_name']) ? $input['school_name'] : null,
                'education_level_cv_id' => isset($input['education_level_cv_id']) ? $input['education_level_cv_id'] : null,
                'school_grade' => isset($input['school_grade']) ? $input['school_grade'] : null,
                'user_id' => access()->id(),
            ]);
            return $child_extension;
        });
    }



    /**
     * @param $payroll_status_change_id
     * @param array $input
     * @return mixed
     *
     */
    public function update(Model $payroll_status_change, array $input)
    {
        return DB::transaction(function () use ($input, $payroll_status_change) {
            $child_extension = $payroll_status_change->payrollChildExtension;
            $child_extension->update([
                'child_continuation_reason_cv_id' => $input['child_continuation_reason_cv_id'],
                'period_provision_type' => ($input['child_continuation_reason_cv_ref'] == 'PAYCHCOSCH') ? 1 : null,
                'deadline_date' => isset($input['deadline_date']) ? $input['deadline_date'] : null,
                'school_name' => isset($input['school_name']) ? $input['school_name'] : null,
                'education_level_cv_id' => isset($input['education_level_cv_id']) ? $input['education_level_cv_id'] : null,
                'school_grade' => isset($input['school_grade']) ? $input['school_grade'] : null,
            ]);

            return $child_extension;
        });
    }

    /*Store disability assessment*/
    public function storeDisabilityAssessment(PayrollStatusChange $statusChange, array $input)
    {
        return DB::transaction(function () use ($input, $statusChange) {
            $child_extension = $statusChange->payrollChildExtension;
            /*child extension*/
            $child_extension->update([
                'period_provision_type' => $input['period_provision_type'],
                'deadline_date' => isset($input['deadline_date']) ? $input['deadline_date'] : null,
            ]);

            /*status change*/
            $statusChange->update([
                'deadline_date' => isset($input['deadline_date']) ? $input['deadline_date'] : null,
            ]);

        });

    }

    /**
     * @param PayrollStatusChange $statusChange
     * Update status letter prepared
     */
    public function updateLetterPreparedStatus(PayrollStatusChange $statusChange)
    {
        $child_extension = $statusChange->payrollChildExtension;
        $child_extension->update([
            'isletter_prepared' => 1
        ]);
    }


    /*check if disability assessed*/
    public function checkIfDisabilityAssessed(PayrollStatusChange $statusChange)
    {
        $child_extension = $statusChange->payrollChildExtension;
        if($child_extension->period_provision_type == null){
            throw  new WorkflowException('Child disability assessment not complete');
        }
    }

    /*Check if letter is prepared*/
    public function checkIfLetterIsPrepared(PayrollStatusChange $statusChange)
    {
        $child_extension = $statusChange->payrollChildExtension;
        if($child_extension->isletter_prepared == 0){
            throw  new WorkflowException('Letter is not yet prepared! Please check!');
        }
    }
}