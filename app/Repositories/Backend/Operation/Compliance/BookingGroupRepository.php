<?php

namespace App\Repositories\Backend\Operation\Compliance;

use App\Models\Finance\Receivable\BookingGroup;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class BookingGroupRepository extends BaseRepository
{
    const MODEL = BookingGroup::class;

    public function getForDataTable()
    {
        $list = $this->query()->select([
            'booking_groups.id',
            'employers.name',
            'employers.reg_no',
            'employer_id'
        ])
            ->join("employers", "booking_groups.employer_id", "=", "employers.id");;
        return $list;
    }

    public function store(array $input)
    {
        $this->query()->updateOrCreate(['employer_id' => $input['employer']]);
        return true;
    }

    public function destroy(array $input)
    {
        return DB::transaction(function () use ($input) {
            if (isset($input['id']) And count($input['id'])) {
                //soft deleting the closed businesses
                $business_groups = $this->query()->find($input['id']);
                foreach ($business_groups as $business_group) {
                    $business_group->delete();
                }
                return true;
            } else {
                return false;
            }
        });
    }

}