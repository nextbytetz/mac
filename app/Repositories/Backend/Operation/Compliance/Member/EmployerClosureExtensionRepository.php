<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureExtension;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Operation\Compliance\Member\EmployerClosureReopen;
use App\Repositories\Backend\Api\ClosedBusinessRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmployerClosureExtensionRepository extends BaseRepository
{
    use AttachmentHandler, FileHandler;


    const MODEL = EmployerClosureExtension::class;

    /*Get Wf Module Group*/
    public function getWfModuleGroupId()
    {
        return 30;
    }

    /*Insert/create EmployerClosureExtension*/
    public function store(array $input)
    {
        return DB::transaction(function() use($input){
            $closure = EmployerClosure::query()->find($input['employer_closure_id']);

            $employerClosureExtension = $this->query()->create([
                'employer_closure_id' => $input['employer_closure_id'],
                'application_date' => $input['application_date'],
                'new_reopen_date' => isset($input['new_reopen_date']) ? $input['new_reopen_date'] : null,
                'remark' => $input['remark'],
                'user_id' => access()->id(),
            ]);

            return $employerClosureExtension;
        });
    }

    /*Modify EmployerClosureExtension*/
    public function update($employerClosureExtension, array $input)
    {
        return DB::transaction(function() use($employerClosureExtension,$input){
            $employerClosureExtension->update([
                'application_date' => $input['application_date'],
                'new_reopen_date' => isset($input['new_reopen_date']) ? $input['new_reopen_date'] : null,
                'remark' => $input['remark'],
            ]);
            return $employerClosureExtension;
        });
    }

    /*Destroy / remove EmployerClosureExtension*/
    public function delete(Model $employerClosureExtension)
    {
        DB::transaction(function() use($employerClosureExtension) {
            $employerClosureExtension->wfTracks()->delete;

            $employerClosureExtension->delete();
        });
    }


    /**
     * @param $id
     * Updates on initiate workflow
     */
    public function initiateApproval($id)
    {
        $extension = $this->find($id);
        $this->checkIfCanInitiateExtension($extension);

        $extension->update([
            'wf_status' => 1
        ]);
        return $extension;
    }

    /*Check if can initiate*/
    public function checkIfCanInitiateExtension($extension)
    {
        $pending_tasks = $this->getPendingTasksBeforeInitiate($extension);
        if(count($pending_tasks))
        {
            throw new GeneralException('Complete all pending tasks to proceed! Please check');
        }
    }

    /**
     * @param $employer_closure_id
     * Get doc attached for this closure
     */
    public function getDocsAttached($extension_id)
    {
        $extension = $this->find($extension_id);
        $employer_closure = $extension->employerClosure;
        $employer = $employer_closure->closedEmployer;
        $docs_all = $this->queryDocumentsForAttachment()->pluck('id')->toArray();
        $docs_attached = $employer->documents()->where('external_id', $extension_id)->whereIn('document_id',$docs_all)->get();
        return $docs_attached;
    }

    /**
     * @param int $closure_type
     * @return array
     * Get mandatory docs ids by closure type
     */
    public function queryDocumentsForAttachment()
    {
        return (new DocumentRepository())->query()->where('document_group_id', 27);
    }



    /*Get pending tasks before initiate*/
    public function getPendingTasksBeforeInitiate($extension)
    {
        $pending_tasks = [];
        $doc_attached = $this->getDocsAttached($extension->id);
        if($doc_attached->count() == 0){
            $pending_tasks[] = 'Supporting document not attached';
        }
        return $pending_tasks;
    }


    /**
     * @param $employer_closure
     * @return int
     * Get module type for workflow when initiating approval workflow
     */
    public function getWfModuleType($extension)
    {
        $type = 0;

        return $type;
    }


    /*Update wf done*/
    public function updateOnWorkflowComplete($id)
    {
        return DB::transaction(function () use ($id) {
            $employer_closure_repo = new EmployerClosureRepository();
            $extension = $this->find($id);
            $closure = $extension->employerClosure;
            $input['specified_reopen_date'] = $extension->new_reopen_date;
            $formatted_reopen_date_internal = (new EmployerClosureRepository())->formatDatesForInternal($input);
            $specified_reopen_date_internal = isset($extension->new_reopen_date) ? $formatted_reopen_date_internal['specified_reopen_date_internal'] : null;
            $closure->update([
                'extension_reopen_date' => $specified_reopen_date_internal,
                'followup_ref_date' => isset($specified_reopen_date_internal) ? $employer_closure_repo->getFollowupRefDateFormatted($specified_reopen_date_internal) : $extension->application_date,
            ]);

            /*InitiateChecker*/
            (new EmployerClosureRepository())->initializeChecker($closure, 'EMPDERREOP');

            return $extension;
        });
    }

    /**
     *
     * Start: Letter Methods
     */

    /*Check if can initiate wf letter*/
    public function checkIfCanInitiateLetterWf($current_track)
    {
        $return = false;
        if(isset($current_track)){
            $wf_definition = $current_track->wfDefinition;
            $current_level = $wf_definition->level;
            $wf_module_id = $wf_definition->wf_module_id;
            $extension = $this->find($current_track->resource_id);
            $wf_module_group_id = $this->getWfModuleGroupId();
            $type = $this->getWfModuleType($extension);
            $level_for_letter = $this->getLevelForLetter($extension);
            $access = access()->user()->hasWorkflowModuleDefinition($wf_module_group_id, $type, $level_for_letter);

            if($current_level == $level_for_letter && $access == true && $extension->wf_done == 0)
//            if($current_level == $level_for_letter && $access == true && $arrears == 0)
            {
                //Check if was declined on approval level
                $check_if_declined = (new WfTrackRepository())->checkIfModuleDeclined($current_track->resource_id, $wf_module_id);
                if($check_if_declined == false){
                    //only if approved
                    $return = true;
                }
            }
        }
        return $return;
    }




    /*Get level For Letter*/
    public function getLevelForLetter($extension)
    {
        $wf_module_repo = new WfModuleRepository();
        $wf_module_group_id = $this->getWfModuleGroupId();
        $type = $this->getWfModuleType($extension);
        $wf_module = (new WfModuleRepository())->query()->where('type', $type)->where('wf_module_group_id', $wf_module_group_id)->first();
        $wf_module_id = $wf_module->id;
        switch($wf_module_id){
            case 66:
                return 5;
                break;


        }
    }

    /*Check if wf letter is initiated*/
    public function checkIfWfLetterIsInitiated($extension, $current_wf_track)
    {
        $check_if_need_response = $this->checkIfNeedResponseLetter($extension);
        if($extension->closureExtensionLetter()->where('isinitiated','<>', 0)->count() == 0 && $this->checkIfCanInitiateLetterWf($current_wf_track) == true && $check_if_need_response == true)
        {
            throw new WorkflowException('Workflow for letter not initiated! Initiate to proceed!');
        }
        return true;
    }



    /*Check if need response letter*/
    public function checkIfNeedResponseLetter(Model $extension){
        $return = true;
        return $return;
    }

    /*Process actions at levels before on approval before wf complete*/
    public function processWfActionAtLevels($current_wf_track, array $input = null)
    {
        DB::transaction(function () use ($current_wf_track, $input) {
            $wf_module_repo = new WfModuleRepository();
            $resource_id = $current_wf_track->resource_id;
            $wf_module_id = $current_wf_track->wfDefinition->wf_module_id;
            $wf_definition = $current_wf_track->wfDefinition;
            $level = $wf_definition->level;
            $extension = $this->find($resource_id);
            $is_approved = ($current_wf_track->status == 1) ? 1 : 0;

            switch ($wf_module_id) {
                case 66:
                    switch ($level) {

                        case 5:
                            /*level for letter*/

                            $this->checkIfWfLetterIsInitiated($extension, $current_wf_track);

                            break;
                    }

                    break;

            }
        });

    }



    /**
     * @param $letter_cv_reference
     * Get Previous letter
     */
    public function getPreviousLetter($letter_cv_reference)
    {
        $cv_repo = new CodeValueRepository();
        $cv_id = $cv_repo->findIdByReference($letter_cv_reference);
        $prev_letter = (new LetterRepository())->queryLettersByReference($cv_id)->orderBy('letters.id', 'desc')->first();
        return $prev_letter;
    }

    /*Get Letter reference No. attribute*/
    public function getLetterReferenceNo($letter_cv_reference)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference);
        $prev_reference = (isset($prev_letter)) ? $prev_letter->reference : null;
        $next_folio_no = $this->getLetterFolioNo($letter_cv_reference);
        $reference_no = '';
        $default = 'AC.454/468/';//TODO need to update <deploy_extension>
        $reference_no = $this->getFullReferenceNo($prev_reference, $default, $next_folio_no)  ;

        return $reference_no;
    }


    /*Prefix of letter reference no*/
    public function getFullReferenceNo($prev_reference, $default_ref, $next_folio_no)
    {
        if(isset($prev_reference)){
            $ref_no =  remove_after_last_this_character('/',$prev_reference) . '/';
        }else{
            $ref_no = $default_ref;
        }
        return $ref_no  . $next_folio_no;
    }

    /*Letter folio no*/
    public function getLetterFolioNo($letter_cv_reference)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference);
        if(isset($prev_letter)){
            $resources =  json_decode($prev_letter->resources, true);
            $next_folio_no =  $resources[0]['value'] + 1;
        }else{
            $next_folio_no = 1;
        }
        return $next_folio_no;
    }



    /*Get all for DataTable EmployerClosureExtension*/
    public function getAllForDt()
    {
        $query = $this->query();
        return $query;
    }



}