<?php
namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\ContributionInterestRate;
use App\Repositories\BaseRepository;
use DB;
use Carbon\Carbon;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\GeneralException;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Services\Workflow\Workflow;
use App\Models\Workflow\WfTrack;
use App\Events\NewWorkflow;
use App\Exceptions\WorkflowException;
use Log;

class ContributionInterestRateRepository extends BaseRepository
{
	use FileHandler, AttachmentHandler;
	const MODEL = ContributionInterestRate::class;

	public function __construct()
	{
		parent::__construct();
		$this->wf_module_group_id = 38;

	}

	public function findOrThrowException($contrib_rate_id)
	{
		$contrib_rate = $this->query()
		->find($contrib_rate_id);
		if (!is_null($contrib_rate))
		{
			return $contrib_rate;
		}
		throw new GeneralException('Request Failed! Contribution / Interest rate profile not found');
	}


	public function getRatesForDataTable()
	{
		$return = $this->query();

		if (!empty(request()->input('fin_code_id'))) {
			$return = $return->where('fin_code_id',(int)request()->input('fin_code_id'));
		}

		if (!empty(request()->input('employer_category_cv_id'))) {
			$return = $return->where('employer_category_cv_id',(int)request()->input('employer_category_cv_id'));
		}

		if (!empty(request()->input('status'))) {
			$status = request()->input('status');
			switch ((int)request()->input('status')) {
				case 2:
				$return = $return->where('wf_status',2)->where('wf_done',true);	
				break;
				case 1:
				$return = $return->whereIn('wf_status',[0,1,3])->where('wf_done',false);	
				break;
				case 3:
				$return = $return->whereIn('wf_status',[4,3])->where('wf_done',true);	
				break;	
				default:
				break;
			}
		}		
		return $return->orderBy('fin_code_id','desc')->orderBy('employer_category_cv_id','asc')
		->orderBy('start_date','desc')->orderBy('fin_code_id','desc');
	}

	public function getCurrentRate($fin_code_id, $employer_cat)
	{
		return $this->query()->where('fin_code_id',$fin_code_id)
		->where('employer_category_cv_id',$employer_cat)
		->whereNull('end_date')->where('wf_status',2)->first();
	}

	public function checkPendingRateRequest($fin_code_id, $employer_cat)
	{
		return $this->query()->where('fin_code_id',$fin_code_id)
		->where('employer_category_cv_id',$employer_cat)->whereNull('wf_done')->first();
	}


	public function saveNewRate(array $input, $request)
	{
		$current_rate = $this->getCurrentRate($input['rate_type'], $input['employer_category']);
		$request_attachment = $request->hasFile('request_attachment') ? 'request_attachment.' . $request
		->file('request_attachment')
		->getClientOriginalExtension() : null;

		$history_query = [
			1 => [36,37],
			2 => [36,37]
		];
		$array_history = [];
		foreach ($history_query as $key => $value) {
			foreach ($value as $employer_cat) {
				$history_item = $this->getCurrentRate($key,$employer_cat);
				if (!empty($history_item)) {
					$histroy_result = $this->createRateHistory($history_item);
					foreach ($histroy_result as $index => $history) {
						$array_history[$index]=$history;
					}
				}
			}
		}

		$result = $this->query()->create([
			'fin_code_id' => $input['rate_type'],
			'employer_category_cv_id' => $input['employer_category'],
			'rate' => $input['new_rate'],
			'start_date' =>  Carbon::parse(Carbon::parse($input['start_date'])->startOfMonth())->format('Y-m-d'),
			'entered_date' => $input['start_date'],
			'user_id' => access()->user()->id,
			'description' => $input['description'],
			'parent_id' => !empty($current_rate->id) ? $current_rate->id : null,
			'request_attachment' => $request_attachment,
			'history' => !empty($array_history) ? json_encode($array_history) : null
		]);
		$this->saveRateAttachment($request,$result->id);
		return $result;
	}


	public function saveRateAttachment($request, $rate_request_id)
	{
		if ($request->file('request_attachment'))
		{
			$file_name_and_ext = $request->file('request_attachment')
			->getClientOriginalName();
			$filename = pathinfo($file_name_and_ext, PATHINFO_FILENAME);
			$file_ext = $request->file('request_attachment')
			->getClientOriginalExtension();
			$store_file_name = 'request_attachment.' . $file_ext;
			$path = $request->file('request_attachment')
			->storeAs('public/member/rates/' . $rate_request_id . '/', $store_file_name);
		}
	}

	public function getRateAttachmentPath($rate_request_id)
	{
		$rate = $this->findOrThrowException($rate_request_id);

		if (test_uri())
		{
			$file_path = asset('storage/member/rates' . DIRECTORY_SEPARATOR . $rate->id);
			$request_path = !empty($rate->request_attachment) ? $file_path . DIRECTORY_SEPARATOR . $rate->request_attachment : null;

		}
		else
		{
			$public_path = asset('public/storage/member/rates' . DIRECTORY_SEPARATOR . $rate->id);
			$request_path = !empty($rate->request_attachment) ? $public_path . DIRECTORY_SEPARATOR . $rate->request_attachment : null;
		}
		return ['request_path' => $request_path];
	}


	public function submitRateForReviewAndApproval($rate)
	{
		$return = DB::transaction(function () use ($rate)
		{
			$check = workflow([['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $rate->id, 'type' => 0]])
			->checkIfHasWorkflow();
			if (!$check)
			{
				$this->query()
				->where('id', $rate->id)
				->update(['wf_status' => 1, 'updated_at' => Carbon::now() ]);
				event(new NewWorkflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $rate->id, 'type' => 0],[],["comments"=>$rate->description]));
			}
			return true;
		});
		return $return;
	}



	public function updateRateOnUpdateWorkflow($resource_id, $current_level, $next_level, $status = null)
	{
		$current_level = (int)$current_level;
		$next_level = (int)$next_level;

		if (($current_level < $next_level) || ($current_level == 5 && $next_level == 0))
		{
			//forward
			if ($current_level == 1)
			{
				$this->query()
				->where('id', $resource_id)->update(['wf_status' => 1]);
			}

			if ($current_level == 5)
			{
                //status == 1 decline (wf_status = 3 ) || status == 0 approve (wf_status = 2)
				$wf_status = $status == 1 ? 3 : 2; 
				$rate = $this->findOrThrowException($resource_id);
				if ($wf_status == 2)
				{
					//update parent_id == end date 
					$end_date = Carbon::parse(Carbon::parse($rate->start_date)->startOfMonth())->format('Y-m-d');
					$this->query()
					->where('id', $rate->parent_id)->update(['end_date' => $end_date]);
				}
				$this->query()
				->where('id', $resource_id)->update(['wf_status' => $wf_status, 'wf_done' => 1, 'wf_done_date' => Carbon::now()]);
			}

		}else{
			//reverse
			if ($next_level == 1)
			{
				$this->query()
				->where('id', $resource_id)->update(['wf_status' => 3, 'wf_done' => 0, 'wf_done_date' => null]);
			}
		}
	}


	public function saveEditeRateRequest(array $input, $request, $rate)
	{
		$current_rate = $this->getCurrentRate($input['rate_type'], $input['employer_category']);
		$request_attachment = $request->hasFile('request_attachment') ? 'request_attachment.' . $request
		->file('request_attachment')
		->getClientOriginalExtension() : null;

		$result = $this->query()->where('id',$rate->id)->update([
			'fin_code_id' => $input['rate_type'],
			'employer_category_cv_id' => $input['employer_category'],
			'rate' => $input['new_rate'],
			'entered_date' => $input['start_date'],
			'start_date' =>  Carbon::parse(Carbon::parse($input['start_date'])->startOfMonth())->format('Y-m-d'),
			'user_id' => access()->user()->id,
			'description' => $input['description'],
			'parent_id' => !empty($current_rate->id) ? $current_rate->id : null,
			'request_attachment' => !empty($request_attachment) ? $request_attachment : $rate->request_attachment
		]);
		$this->saveRateAttachment($request,$rate->id);
		return $rate;
	}


	public function createRateHistory($history_item)
	{

		$new_history_array = [];
		if (!empty($history_item->rate)) {
			$parent = [];
			if ($history_item->parent_id) {
				$parent[$history_item->parent->id] = [
					'fin_code_id' => $history_item->parent->fin_code_id,
					'employer_category_cv_id' =>  $history_item->parent->employer_category_cv_id,
					'rate' =>  $history_item->parent->rate,
					'start_date' =>  $history_item->parent->start_date,
				];
			}

			$new_history_array[$history_item->id] = [
				'fin_code_id' => $history_item->fin_code_id,
				'employer_category_cv_id' => $history_item->employer_category_cv_id,
				'rate' => $history_item->rate,
				'start_date' => $history_item->start_date,
				'parent' => $parent
			];
		}
		return $new_history_array;
	}

}

