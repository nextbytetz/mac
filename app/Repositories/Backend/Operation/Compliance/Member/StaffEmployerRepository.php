<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;


use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Jobs\Compliance\StaffEmployerAllocation\StaffEmployerAllocationJob;
use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use App\Models\Sysdef\CodeValue;
use App\Notifications\Backend\Employer\EmployerFollowUpEmailReminder;
use App\Notifications\Backend\Employer\StaffEmployerCollectionPerformance;
use App\Notifications\Backend\Employer\StaffEmployerFollowUpReminder;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\Traits\StaffEmployerComplimentTrait;
use App\Repositories\Backend\Operation\Compliance\Member\Traits\StaffEmployerFollowUpRepoTrait;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use App\Services\Compliance\StaffEmployerAllocation\AllocateBonusEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateLargeEmployers;

use App\Services\Compliance\StaffEmployerAllocation\AllocateNonLargeEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateTreasuryEmployers;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\DocBlock;

class StaffEmployerRepository extends  BaseRepository
{

    use AttachmentHandler, FileHandler, StaffEmployerFollowUpRepoTrait, StaffEmployerComplimentTrait;

    public function __construct()
    {

    }



    public function staffEmployerConfig(){
        return  DB::transaction(function (){
            $config = DB::table('staff_employer_configs')->first();
            if (isset($config)) {
                return $config;
            } else {
                /*create default*/
                $config_id = DB::table('staff_employer_configs')->insertGetId([
                    'allocation_percent' => 70,
                    'non_large_employers_per_staff' => 200,
                    'created_at' => Carbon::now()
                ]);

                $config = DB::table('staff_employer_configs')->first();
            }

            return $config;
        });
    }



    /*update allocation configuration parameters*/
    public function updateStaffEmployerConfig(array $input){
        return  DB::transaction(function () use($input){
            /*get staff ids*/
            $staff = 0;
            $selected_staff = [];
            $compliance_officers = [];
            $intern_staff = [];
            foreach ($input as $key => $value) {
                switch ($key) {
                    case 'staff_for_large_contributors':
                        $staff = sizeof($value);
                        $selected_staff = $value;
                        break;

                    case 'compliance_officers':
                        $staff = sizeof($value);
                        $compliance_officers = $value;
                        break;

                    case 'intern_staff':
                        $staff = sizeof($value);
                        $intern_staff = $value;
                        break;
                }
            }
            /*update*/
            DB::table('staff_employer_configs')->where('id', $input['staff_employer_config_id'])->update([
                'allocation_percent' => isset($input['allocation_percent']) ?  $input['allocation_percent'] : 0,
                'non_large_employers_per_staff' =>  isset($input['non_large_employers_per_staff']) ?  $input['non_large_employers_per_staff'] : 0,
                'large_employers_per_staff' =>isset($input['large_employers_per_staff']) ?  $input['large_employers_per_staff'] : 0,
                'staff_for_large_contributors' => serialize($selected_staff),
                'compliance_officers' => serialize($compliance_officers),
                'intern_staff' => serialize($intern_staff),
                'followup_weekly_target' => $input['followup_weekly_target'],
                'followup_weekly_target_intern' => $input['followup_weekly_target_intern'],
                'followup_reminder_next_date' =>isset($input['followup_reminder_next_date']) ?  standard_date_format($input['followup_reminder_next_date']) : null,
                'updated_at' => Carbon::now()
            ]);


            /*save staff for contributors*/
//            $this->saveStaffForLargeContributors($input);
            $this->updateIsInternForStaffEmployers();
        });
    }

    /*Update is inter for staff employers*/
    public function updateIsInternForStaffEmployers()
    {
        $config = $this->staffEmployerConfig();
        $intern_users = unserialize($config->intern_staff);
        /*Interns*/
        DB::table('staff_employer')->whereIn('user_id', $intern_users)->update([
            'isintern' => 1
        ]);

        /*Officers*/
        DB::table('staff_employer')->whereNotIn('user_id', $intern_users)->update([
            'isintern' => 0
        ]);

    }

    /*Get Users query base in debt managment*/
    public function getQueryUsersInDebtMgt()
    {
        $debt_config = (new StaffEmployerRepository())->staffEmployerConfig();
        $all_compliance_officer_ids = array_merge(unserialize($debt_config->compliance_officers), unserialize($debt_config->intern_staff));
        $users = User::query()->where(function($q) use($all_compliance_officer_ids){
            $q->where('unit_id', 15)->orWhereIn('id',$all_compliance_officer_ids);
        });

        return $users;
    }

    /*Save staff for large contributors*/
    public function saveStaffForLargeContributors(array $input)
    {
        $existing_staff_array = DB::table('staff_for_large_contributors')->pluck('user_id')->toArray();

        foreach ($input as $key => $value) {
            switch ($key)  {
                case 'staff_for_large_contributors':
                    $new_staff_array = $value;

                    if(count($existing_staff_array) && count($new_staff_array)){
                        $diff_array_to_add = array_diff($new_staff_array, $existing_staff_array);
                        /*add new*/
                        foreach($diff_array_to_add as $staff_id){
                            DB::table('staff_for_large_contributors')->insert([
                                'user_id' => $staff_id,
                                'created_at' => Carbon::now(),
                            ]);
                        }
                        $diff_array_to_remove = array_diff($existing_staff_array,$new_staff_array);

                        /*remove not in new list*/
                        foreach($diff_array_to_remove as $staff_id){
                            DB::table('staff_for_large_contributors')->where('user_id', $staff_id)->delete();
                        }
                    }elseif(count($new_staff_array)){
                        /*update for the first time*/
                        foreach($new_staff_array as $staff_id){
                            DB::table('staff_for_large_contributors')->insert([
                                'user_id' => $staff_id,
                                'created_at' => Carbon::now(),
                            ]);
                        }
                    }

                    break;
            }
        }
    }



    /*Save staff employer allocation - STore*/

    public function saveAllocation(array $input){
        return  DB::transaction(function () use($input){
            $staff = 0;
            $selected_staff = [];
            foreach ($input as $key => $value) {
                switch ($key) {
                    case 'staff':
                        $staff = sizeof($value);
                        $selected_staff = $value;
                        break;
                }
            }
            $config = $this->staffEmployerConfig();

            $allocation_id =   DB::table('staff_employer_allocations')->insertGetId([
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'no_of_employers' => $input['no_of_employers'],
                'no_of_staff' => $staff,
                'islarge' => $input['islarge'],
                'user_id' => access()->id(),
                'selected_staff' => serialize($selected_staff),
                'category' => $input['category'],
                'created_at' => Carbon::now(),
                'isbonus' => $input['isbonus'],
                'isintern' => isset( $input['isintern']) ?  $input['isintern'] : 0
            ]);


            /*Dispatch job to allocate staff*/
            dispatch(new StaffEmployerAllocationJob($allocation_id,$input['action_type']));

            return $allocation_id;
        });
    }



    /**
     *
     * Store staff allocation for non large employers
     */
    public function allocateForNonLargeEmployers(array $input){
        $config = $this->staffEmployerConfig();
        $input['islarge'] = 0;
        $input['category'] = 2;
        $input['isbonus'] = 0;
        $input['action_type'] = 1;
        $input['no_of_employers'] = $config->non_large_employers_per_staff * sizeof($input['staff']);
        $allocation_id =  $this->saveAllocation($input);
        return $allocation_id;
    }


    /**
     *
     * Store staff allocation for large employers
     */
    public function allocateForLargeEmployers(array $input){
        $service = new AllocateLargeEmployers();
        $input['islarge'] = 1;
        $input['category'] = 1;
        $input['isbonus'] = 0;
        $input['action_type'] = 2;
        $input['no_of_employers'] = $service->queryForLargeContributors()->count();
        $allocation_id =  $this->saveAllocation($input);
        return $allocation_id;
    }


    /**
     *
     * Store staff allocation for Bonus employers
     */
    public function allocateForBonusEmployers(array $input){
        $allocate_bonus_serv = new AllocateBonusEmployers();

        $input['islarge'] = 0;
        $input['category'] = 3;
        $input['isbonus'] = 1;
        $input['action_type'] = 3;
        $input['no_of_employers'] =  $allocate_bonus_serv->dormantEmployersQueryCount();
        $allocation_id =  $this->saveAllocation($input);
        return $allocation_id;
    }


    /**
     *
     * Store staff allocation for treasury
     */
    public function allocateTreasuryEmployers(array $input){
        $service = new AllocateTreasuryEmployers();
        $input['islarge'] = 0;
        $input['category'] = 4;
        $input['isbonus'] = 2;
        $input['action_type'] = 5;
        $input['no_of_employers'] =  $service->queryForTreasuryEmployers()->count();
        $allocation_id =  $this->saveAllocation($input);
        return $allocation_id;
    }

    /**
     *
     * Store staff allocation for intern
     */
    public function allocateForIntern(array $input){
//        $service = new AlocateEmployersForIntern();
        $input['islarge'] = 0;
        $input['category'] = 5;
        $input['isbonus'] = 0;
        $input['action_type'] = 8;//Job action type
        $input['no_of_employers'] =  0;
        $allocation_id =  $this->saveAllocation($input);
        return $allocation_id;
    }




    /*Allocate new staff - after when allocation is on progress - Applicable for Non large allocations */
    public function allocateNewStaff($staff_employer_allocation_id, array $input)
    {
        DB::transaction(function () use($staff_employer_allocation_id, $input) {
            $allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);

            switch($allocation->category){

                case 2:
                    /*Allocate for new staff - Non large allocations*/
                    $this->allocateNewStaffForNonLarge($staff_employer_allocation_id, $input);
                    break;

                case 3:
                    /*Allocate for new staff for bonus allocations*/
                    $service_allocation = new AllocateBonusEmployers();
                    $service_allocation->redistributeBonusForNewStaff($input, $staff_employer_allocation_id);
                    break;
            }
        });
    }

    /**
     * @param $staff_employer_allocation_id
     * @param array $input
     * Allocate New staff for non large
     */
    public function allocateNewStaffForNonLarge($staff_employer_allocation_id, array $input)
    {

        DB::transaction(function () use($staff_employer_allocation_id, $input) {
            $allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
            $staff_allocated = unserialize($allocation->selected_staff);
            $service_allocation = new AllocateNonLargeEmployers();
            foreach ($input as $key => $value) {
                switch ($key) {
                    case 'staff':
                        $selected_staff = $value;
                        foreach ($selected_staff as $staff) {
                            /*Allocate for each staff selected - NEW*/
                            $new_staff=[$staff];
                            $staff_allocated = array_merge($staff_allocated, $new_staff);
                            $service_allocation->allocateForNewStaff($staff_employer_allocation_id, $staff);
                        }
                        break;
                }
            }

            /*Update selected staff on allocation table*/
            $allocation->update([
                'selected_staff' => serialize($staff_allocated),
                'no_of_employers' => $allocation->no_of_employers_allocated,
                'no_of_staff' => sizeof($staff_allocated),
            ]);

        });
    }


    /**
     * @param $staff_employer_allocation
     * Approve allocation
     */
    public function approveAllocation($staff_employer_allocation)
    {
        DB::transaction(function () use($staff_employer_allocation) {
            $staff_employer_allocation->update([
                'isapproved' => 1,
                'isactive' => 1,
                'approved_date' => standard_date_format(Carbon::now())
            ]);

            $last_allocation = $this->getRecentByCategory( $staff_employer_allocation->category, $staff_employer_allocation->id);
            if (isset($last_allocation->id)) {
                /*deactivate relation*/
                $this->deactivateStaffEmployerRelation($last_allocation->id);

                /*deactivate previous allocation*/
                $last_allocation->update([
                    'isactive' => 0,
                ]);
            }

        });
    }

    /*Revoke allocation*/
    public function revokeAllocation($staffEmployerAllocation)
    {
        DB::transaction(function () use($staffEmployerAllocation) {
            $staff_employer_allocation_id = $staffEmployerAllocation->id;
            /*Remove follow ups*/
            StaffEmployerFollowUp::query()
                ->join('staff_employer', 'staff_employer.id', 'staff_employer_follow_ups.staff_employer_id')
                ->where('staff_employer.staff_employer_allocation_id', $staff_employer_allocation_id)
                ->delete();

            /*Undo allocation*/
            $this->undoAllocation($staffEmployerAllocation);
        });
    }

    /**
     * @param Model $staff_employer_allocation
     * @param array $input
     * Reset stat details on allocation table when there is change on allocation
     */
    public function resetStatDetailsOnAllocation(Model $staff_employer_allocation, array $input)
    {
        /*Update selected staff on allocation table*/
        $staff_allocated =isset( $input['staff_allocated']) ?  $input['staff_allocated'] : null;
        $staff_employer_allocation->update([
            'selected_staff' => ($staff_allocated) ? serialize($staff_allocated)  : $staff_employer_allocation->selected_staff,
            'no_of_employers' => $staff_employer_allocation->no_of_employers_allocated,
            'no_of_staff' =>($staff_allocated) ? sizeof($staff_allocated) : $staff_employer_allocation->no_of_staff,
        ]);
    }

    /*deactivate Relationship Allocation*/
    public function deactivateStaffEmployerRelation($staff_employer_allocation_id)
    {
        DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation_id)->update([
            'isactive' => 0
        ]);
    }

    /*deactivate user with employer Relationship*/
    public function deactivateUserAllocation($user_id,$employer_id,$staff_employer_allocation_id)
    {
        DB::table('staff_employer')->where('user_id', $user_id)->where('employer_id', $employer_id)->where('staff_employer_allocation_id', $staff_employer_allocation_id)->update([
            'isactive' => 0
        ]);
    }


    /**
     * @param $staff_employer_allocation
     * Undo allocation
     */
    public function undoAllocation($staff_employer_allocation)
    {
        /*Delete from staff employers*/
        DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation->id)->delete();

        /*delete allocation instance*/
        $staff_employer_allocation->delete();

    }



    /*Update allocation*/
    public function updateAllocation($staff_employer_allocation, array $input)
    {

        DB::transaction(function () use($staff_employer_allocation, $input) {
            $staff_employer_allocation->update([
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date']
            ]);

            $this->updateDatesOnStaffEmployer($staff_employer_allocation);
        });
    }

    /*Update dates on staff employer table*/
    public function updateDatesOnStaffEmployer($staff_employer_allocation)
    {
        DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation->id)->update([
            'start_date' => $staff_employer_allocation->start_date,
            'end_date' => $staff_employer_allocation->end_date,
        ]);
    }

    /**
     * @param $user_id
     * @param $staff_employer_allocation_id
     * Remove staff from allocation
     */
    public function removeStaffFromAllocation($user_id, $staff_employer_allocation_id)
    {
        DB::transaction(function () use($user_id, $staff_employer_allocation_id) {
            $staff_employer_allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
            $staff_allocated = unserialize($staff_employer_allocation->selected_staff);
            $user_to_remove = [$user_id];
            $staff_allocated = array_diff($staff_allocated, $user_to_remove);
            /*remove allocations*/
            DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation_id)
                ->where('user_id', $user_id)->update([
                    'end_date' => standard_date_format(getTodayDate()),
                    'isactive' => 0
                ]);

            /*reset stat on allocation*/
            $update_input = [
                'staff_allocated' => $staff_allocated,
            ];
            $this->resetStatDetailsOnAllocation($staff_employer_allocation, $update_input);
        });
    }


    /*Update run status*/
    public function updateRunStatus($staff_employer_allocation_id)
    {
        $staff_employer_allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $target_employers = $staff_employer_allocation->no_of_employers;
        $allocated_employers = $staff_employer_allocation->no_of_employers_allocated;
        if($target_employers == $allocated_employers)
        {
            $staff_employer_allocation->update([
                'run_status' => 1
            ]);
        }
    }


    /*Complete Allocation*/
    public function completeAllocation(StaffEmployerAllocation $staffEmployerAllocation)
    {
        DB::transaction(function () use($staffEmployerAllocation) {
            /*Update status*/
            $staffEmployerAllocation->update([
                'status' => 2,
                'isactive' => 0
            ]);

            /*Update status on staff_employer*/
            DB::table('staff_employer')->where('staff_employer_allocation_id', $staffEmployerAllocation->id)->update([
                'isactive' => 0
            ]);

        });
    }


    /**
     * @param $staff_employer_allocation_id
     * @param $employer_status
     * Get no of employers allocated by status for all employers for each staff
     *
     */
    public function getNoOfAllocatedEmployersForStaffByStatus($staff_employer_allocation_id,$user_id, $employer_status)
    {
        $employer_status = ($employer_status != 3) ? [$employer_status] : [3,4];
        $employers = DB::table('staff_employer')
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->where('staff_employer_allocation_id', $staff_employer_allocation_id)
            ->whereIn('employers.employer_status', $employer_status)
//            ->where('employers.duplicate_id', $is_duplicate)
            ->where('staff_employer.user_id', $user_id)
            ->where('staff_employer.isactive',1)->count();

        return $employers;

    }

    /*Get no of employers allocated but turned duplicate on progress*/
    public function getNoOfAllocatedEmployersForStaffDuplicate($staff_employer_allocation_id,$user_id)
    {
        $employers = DB::table('staff_employer')
            ->where('staff_employer_allocation_id', $staff_employer_allocation_id)
            ->where('staff_employer.user_id', $user_id)
            ->where('staff_employer.isduplicate',1)
            ->count();

        return $employers;

    }



    /*Get staff Employer allocations for datatable*/
    public function getStaffEmployerAllocationsForDataTable()
    {
        return StaffEmployerAllocation::query();
    }


    /*Get recent allocation by type  - Last allocation before this*/
    public function getRecentAllocationByType($islarge, $isbonus,$current_allocation_id = null)
    {
        if(isset($current_allocation_id)){
            $allocation = StaffEmployerAllocation::query()->where('id','<>', $current_allocation_id)->where('islarge', $islarge)->where('isbonus', $isbonus)->orderBy('end_date', 'desc')->first();
        }else{
            $allocation = StaffEmployerAllocation::query()->where('islarge', $islarge)->where('isbonus', $isbonus)->orderBy('end_date', 'desc')->first();
        }
        return $allocation;
    }


    /*Get recent allocation by category - Last allocation before this*/
    public function getRecentByCategory($category,$current_allocation_id = null)
    {
        if(isset($current_allocation_id)){
            $allocation = StaffEmployerAllocation::query()->where('id','<>', $current_allocation_id)->where('category', $category)->orderBy('end_date', 'desc')->first();
        }else{
            $allocation = StaffEmployerAllocation::query()->where('category', $category)->orderBy('end_date', 'desc')->first();
        }
        return $allocation;
    }


    /*Staff allocated by allocation for DataTable*/
    public function getStaffAllocatedByAllocationForDataTable($staff_employer_allocation_id)
    {
        $staff_employer_allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff = $staff_employer_allocation->selected_staff;
        $selected_staff_array = unserialize($selected_staff);
        $users = (new UserRepository())->query()->whereIn('id', $selected_staff_array);
        return $users;
    }

    /**
     * REALLOCATION
     */

    public function getStaffAllocationsForReallocateForDt()
    {
        $input = request()->all();
        $unallocated_category_id = isset($input['unallocated_contrib_category_id']) ? $input['unallocated_contrib_category_id'] : 0;
        if($unallocated_category_id == 0){
            /*Normal reallocation on allocated employers*/
            $query = StaffEmployerAllocation::query()->select(
                'staff_employer.id as staff_employer_id',
                DB::raw("concat_ws(' ', users.firstname, users.lastname) as staff"),
                'employers.name as employer',
                'staff_employer.start_date as allocation_start_date',
                'staff_employer.end_date as allocation_end_date'
            )
                ->join('staff_employer', 'staff_employer_allocations.id', 'staff_employer.staff_employer_allocation_id')
                ->join('employers', 'employers.id', 'staff_employer.employer_id')
                ->join('users', 'users.id', 'staff_employer.user_id')
                ->where('staff_employer.isactive',1);

            $query = $this->queryWithFilterAllocationForReallocate($query, $input);
        }else{
            /*Search for unallocatedForReallocation*/
            $query = $this->queryForUnallocatedForReallocation($input);
            $query = $this->queryWithFilterAllocationForReallocate($query, $input);
        }


        return $query;

    }

    public function queryWithFilterAllocationForReallocate($parent_query,array $input)
    {
        /*category i.e. 1 => large, 2 => non large, 3 => bonus*/
        if(isset($input['category']))
        {
            $category = $input['category'];
            $parent_query = $parent_query->where('staff_employer_allocations.category', $category);
        }

        /*employer*/
        if(isset($input['employer']))
        {
            $parent_query = $parent_query->where('staff_employer.employer_id', $input['employer']);
        }

        /*assigned to - user*/
        if(isset($input['user_id']))
        {
            $parent_query = $parent_query->where('staff_employer.user_id', $input['user_id']);
        }
        if(isset($input['region_id']))
        {
            $parent_query = $parent_query->where('employers.region_id', $input['region_id']);
        }
        return $parent_query;
    }

    /*Query for unallocated for re-allocation*/
    public function queryForUnallocatedForReallocation(array $input){
        $unallocated_category_id = $input['unallocated_contrib_category_id'];

        return  DB::table('contributing_category_employer')->select(
            'employers.id as staff_employer_id',
            DB::raw("concat_ws(' ', users.firstname, users.lastname) as staff"),
            'employers.name as employer',
            DB::raw("'' as allocation_start_date "),
            DB::raw("'' as allocation_end_date ")
        )
            ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
            ->leftJoin('staff_employer',function($join){
                $join->on('staff_employer.employer_id', 'employers.id')
                    ->where('staff_employer.isactive', 1);
            })->leftJoin('users', 'users.id', 'staff_employer.user_id')
            ->whereNull('staff_employer.id')->where('contributing_category_employer.contributing_category_id',$unallocated_category_id )
            ->whereNull('employers.duplicate_id')->whereNull('employers.deleted_at')
            ->where('employers.employer_status', 1);

    }

    /*Reallocate Staff*/
    public function reallocateStaff(array $input){
        DB::transaction(function () use($input) {

            $unallocated_contrib_category_id = isset($input['unallocated_contrib_category_id']) ? $input['unallocated_contrib_category_id'] : 0;
            $assigned_user = $input['assigned_user'];
            $check_if_intern = $this->checkIfStaffIsIntern($assigned_user);
            if($unallocated_contrib_category_id == 0){

                /*update staff_employer*/
                foreach ($input['id'] as $staff_employer_id) {
                    /*Update end date*/
                    $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
                    $end_date = $staff_employer->end_date;
                    if ($staff_employer->user_id != $assigned_user && $staff_employer->isactive == 1) {
                        /*Users should different*/
                        DB::table('staff_employer')->where('id', $staff_employer_id)->update([
                            'end_date' => standard_date_format(Carbon::now()),
                            'isactive' => 0,
                        ]);
                        /*staff allocation id*/
                        $staff_employer_allocation_id = $staff_employer->staff_employer_allocation_id;
                        $staff_employer_allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
                        $collection_categories = $this->getCategoriesForCollectionAllocationArray();
                        $check_if_collection_category = in_array($staff_employer_allocation->category, $collection_categories);
                        if($check_if_collection_category){
                            $assigner_user_allocation = $this->getMatchingStaffEmployerAllocationCollectionByStaff($assigned_user);
                        }else{
                            $assigner_user_allocation = null;
                        }
                        /*end staff allocation id*/


                        /*Attach new user to this employer*/
                        DB::table('staff_employer')->insert([
                            'user_id' => $assigned_user,
                            'employer_id' => $staff_employer->employer_id,
                            'start_date' => standard_date_format(Carbon::now()),
                            'end_date' => $end_date,
                            'unit_id' => 5,
                            'created_at' => Carbon::now(),
                            'staff_employer_allocation_id' => ($assigner_user_allocation) ? $assigner_user_allocation->allocation_id :  $staff_employer->staff_employer_allocation_id,
                            'isbonus' => $staff_employer->isbonus,
                            'isintern' => ($check_if_intern) ? 1 : 0
                        ]);
                    }
                }
                $this->updateSelectedStaffWhenReallocate($staff_employer->staff_employer_allocation_id);


            }else{
                /*Reallocate for unallocated*/
                $this->checkReallocateFormValidation($input);
                $this->reallocateStaffForUnallocated($input);
            }

        });
    }

    /*Reallocate staff for unallocated*/
    public function reallocateStaffForUnallocated(array $input)
    {
        $assigned_user = $input['assigned_user'];
        /*update staff_employer*/
        foreach ($input['id'] as $employer_id) {

            /*Update end date*/
            $staff_employer_allocation = $this->getStaffEmployerAllocationForUnallocated($input);
            $staff_employer_allocation_id = $staff_employer_allocation->allocation_id;
            $end_date = $staff_employer_allocation->end_date;
            $category= $staff_employer_allocation->category;
            /*Attach new user to this employer*/
            DB::table('staff_employer')->insert([
                'user_id' => $assigned_user,
                'employer_id' => $employer_id,
                'start_date' => standard_date_format(Carbon::now()),
                'end_date' => $end_date,
                'unit_id' => 5,
                'created_at' => Carbon::now(),
                'staff_employer_allocation_id' => $staff_employer_allocation_id,
                'isbonus' => 0,
                'isintern' => ($category == 5) ? 1 : 0,
            ]);
        }
        $this->updateSelectedStaffWhenReallocate($staff_employer_allocation_id);
    }


    /*Check reallocate form validation*/
    public function checkReallocateFormValidation($input)
    {
        if(!isset($input['assigned_user']))
        {
            throw new WorkflowException("The user to assign the employer(s) has not been selected! Please check!");
        }

        if(!isset($input['id']))
        {
            throw new WorkflowException("Select at least one employer for reallocation! Please check!");
        }

        /*Validation*/
        $check_if_allocation_exist = $this->getStaffEmployerAllocationForUnallocated($input);
        if(!$check_if_allocation_exist)
        {
            throw new WorkflowException("There is no staff employer allocation for this staff and category selected! Please check!");
        }
    }

    /*Get destination staff employer allocation when reallocate for unallocated*/
    public function getStaffEmployerAllocationForUnallocated(array $input)
    {
        $unallocated_contrib_category_id = isset($input['unallocated_contrib_category_id']) ? $input['unallocated_contrib_category_id'] : 0;
        $allocation = null;

        if($unallocated_contrib_category_id != 0)
        {
            $assigned_user = $input['assigned_user'];
            $allocation = $this->getMatchingStaffEmployerAllocationCollectionByStaff($assigned_user);

        }

        return $allocation;
    }

    /**
     * @param $user_id
     * Get Matching staff employer allocation for collection by staff
     */
    public function getMatchingStaffEmployerAllocationCollectionByStaff($user_id)
    {
        $config = $this->staffEmployerConfig();
        $assigned_user = $user_id;
        $check_staff_intern = $this->checkIfStaffIsIntern($assigned_user);
        $large_staff_ids = isset($config->large_employers_per_staff) ? unserialize($config->staff_for_large_contributors) : [];
        $check_large = in_array($assigned_user, $large_staff_ids);
        $perm_category = ($check_large == true) ? 1 : 2;//default category for permanent officer
//            $check_staff_intern = ($check_staff_intern > 0) ? true : false;

        /*allocation*/
        if($check_staff_intern){
            $allocation = StaffEmployerAllocation::query()->select(
                'staff_employer_allocations.id as allocation_id',
                'staff_employer_allocations.start_date as start_date',
                'staff_employer_allocations.end_date as end_date',
                'staff_employer_allocations.category'
            )->where('category', 5)->where('isactive', 1)->first();
        }else{
            $allocation = StaffEmployerAllocation::query()->select(
                'staff_employer_allocations.id as allocation_id',
                'staff_employer_allocations.start_date as start_date',
                'staff_employer_allocations.end_date as end_date',
                'staff_employer_allocations.category'
            )
                ->join('staff_employer', function($q) use($assigned_user){
                    $q->on('staff_employer.staff_employer_allocation_id', 'staff_employer_allocations.id')->where('staff_employer.isactive', 1)->where('staff_employer.user_id', $assigned_user);
                })
                ->whereIn('staff_employer_allocations.category', [$perm_category])->where('staff_employer_allocations.isactive', 1)->first();
        }

        /*For new staff*/
        if(!isset($allocation))
        {

            $large_staff_ids = isset($config->large_employers_per_staff) ? unserialize($config->staff_for_large_contributors) : [];
            $check_large = in_array($assigned_user, $large_staff_ids);
            $category = ($check_large == true) ? 1 : 2;
            $category = ($check_staff_intern == true) ? 5 : $category;
            /*Get collection allocation*/
            $allocation = StaffEmployerAllocation::query()->select(
                'staff_employer_allocations.id as allocation_id',
                'staff_employer_allocations.start_date as start_date',
                'staff_employer_allocations.end_date as end_date',
                'staff_employer_allocations.category'
            )
                ->where('category', $category)->where('staff_employer_allocations.isactive', 1)->first();
        }
        return $allocation;
    }


    /*updates selected staff for this allocation after reallocation*/
    public function updateSelectedStaffWhenReallocate($staff_employer_allocation_id)
    {
        $staff_ids = DB::table('staff_employer')->select('user_id')->distinct('user_id')->where('staff_employer.isactive', 1)->where('staff_employer_allocation_id', $staff_employer_allocation_id)->pluck('user_id')->toArray();
        /*Staff employer allocation*/
        StaffEmployerAllocation::query()->where('id', $staff_employer_allocation_id)->update(['selected_staff'  => serialize($staff_ids), 'no_of_staff' => sizeof($staff_ids)]);
    }

    /**
     *
     * MY EMPLOYER METHODS
     */

    /*Update attendance status of employer*/
    public function updateAttendanceStatus($staff_employer_id, $status)
    {
        DB::table('staff_employer')->where('id', $staff_employer_id)->update(
            [
                'isattended' => $status
            ]
        );
    }




    /*Get employers allocated for staff for active allocations*/
    public function getEmployersAllocatedForStaffActiveAllocationForDt($user_id,$isbonus, $with_contact = 0)
    {
        if($isbonus == 2)
        {
            if($with_contact == 0){
                /*Get all for active allocations*/
                $allocations = DB::table('main.staff_employer')->select(
                    'main.staff_employer.user_id as user_id',
                    'main.staff_employer.id as staff_employer_id',
                    'main.staff_employer.employer_id as employer_id',
                    DB::raw("concat_ws(' ', main.users.firstname, main.users.lastname) as staff"),
                    'main.employers.name as employer',
                    'main.staff_employer.start_date as allocation_start_date',
                    'main.staff_employer.end_date as allocation_end_date',
                    'main.staff_employer.isactive as relation_status',
                    'main.staff_employer.isattended as attendance_status',
                    'main.staff_employer.no_of_follow_ups as no_of_follow_ups',
                    DB::raw("' Treasury' as contributing_category"),
                    'main.employers.isonline as isonline',
                    'main.employers.employer_status as employer_status',
                    'main.regions.name as region_name',
                    DB::raw("(select count(1) from main.bookings_view b
join booking_grace_period_view grace on grace.booking_id = b.booking_id
where grace.grace_period_end < now()::date and b.employer_id = employers.id and b.ispaid = 0) as missing_months")
                )
                    ->join('main.staff_employer_allocations', 'staff_employer_allocations.id', 'staff_employer.staff_employer_allocation_id')
                    ->join('main.employers', 'employers.id', 'staff_employer.employer_id')
//                ->join('contributing_employers', 'employers.id', 'staff_employer.employer_id')
                    ->join('main.users', 'users.id', 'staff_employer.user_id')
                    ->leftJoin('main.regions', 'regions.id', 'employers.region_id')
                    ->where('staff_employer_allocations.isactive', 1)->where('staff_employer.isactive', 1)->where('staff_employer.user_id',$user_id)->where('staff_employer_allocations.isbonus', 2)->orderBy('staff_employer.isbonus')->orderBy('staff_employer.isattended', 'asc');
            }else{

                /*With contact*/
                $allocations = DB::table('main.staff_employer')->select(
                    'main.staff_employer.user_id as user_id',
                    'main.staff_employer.id as staff_employer_id',
                    'main.staff_employer.employer_id as employer_id',
                    DB::raw("concat_ws(' ', main.users.firstname, main.users.lastname) as staff"),
                    'main.employers.name as employer',
                    'main.staff_employer.start_date as allocation_start_date',
                    'main.staff_employer.end_date as allocation_end_date',
                    'main.staff_employer.isactive as relation_status',
                    'main.staff_employer.isattended as attendance_status',
                    'main.staff_employer.no_of_follow_ups as no_of_follow_ups',
                    DB::raw("' Treasury' as contributing_category"),
                    'main.employers.isonline as isonline',
                    'main.employers.employer_status as employer_status',
                    'main.regions.name as region_name',
                    DB::raw("(select count(1) from main.bookings_view b
join booking_grace_period_view grace on grace.booking_id = b.booking_id
where grace.grace_period_end < now()::date and b.employer_id = employers.id and b.ispaid = 0) as missing_months"),

                    DB::raw('portal.users.name as contact_person'),
                    DB::raw('coalesce(portal.users.phone, main.employers.phone) as contact_phone'),
                    DB::raw('coalesce(portal.users.email, main.employers.email) as contact_email')
                )
                    ->join('main.staff_employer_allocations', 'main.staff_employer_allocations.id', 'main.staff_employer.staff_employer_allocation_id')
                    ->join('main.employers', 'employers.id', 'main.staff_employer.employer_id')
                    ->join('main.users', 'main.users.id', 'main.staff_employer.user_id')
                    ->leftJoin('main.regions', 'regions.id', 'employers.region_id')
                    ->leftJoin('portal.employer_user', function($join) {
                        $join->on('portal.employer_user.employer_id', 'main.employers.id');
                    })
                    ->leftJoin('portal.users','portal.employer_user.user_id', 'portal.users.id' )
                    ->where('staff_employer_allocations.isactive', 1)->where('staff_employer.isactive', 1)->where('portal.users.isactive',1 )->where('main.staff_employer.user_id',$user_id)->where('main.staff_employer_allocations.isbonus', $isbonus)->orderBy('main.staff_employer.isbonus')->orderBy('main.staff_employer.isattended', 'asc');
            }

        }else{
            /*Get based on category specified*/

            if($with_contact == 0){

                $allocations = DB::table('main.staff_employer')->select(
                    'main.staff_employer.user_id as user_id',
                    'main.staff_employer.id as staff_employer_id',
                    'main.staff_employer.employer_id as employer_id',
                    DB::raw("concat_ws(' ', main.users.firstname, main.users.lastname) as staff"),
                    'main.employers.name as employer',
                    'main.staff_employer.start_date as allocation_start_date',
                    'main.staff_employer.end_date as allocation_end_date',
                    'main.staff_employer.isactive as relation_status',
                    'main.staff_employer.isattended as attendance_status',
                    'main.staff_employer.no_of_follow_ups as no_of_follow_ups',
                    'main.employer_contributing_categories.name as contributing_category',
                    'main.employers.isonline as isonline',
                    'main.employers.employer_status as employer_status',
                    'main.regions.name as region_name',
                    DB::raw("(select count(1) from main.bookings_view b
join booking_grace_period_view grace on grace.booking_id = b.booking_id
where grace.grace_period_end < now()::date and b.employer_id = employers.id and b.ispaid = 0) as missing_months")
                )
                    ->join('main.staff_employer_allocations', 'staff_employer_allocations.id', 'staff_employer.staff_employer_allocation_id')
                    ->join('main.employers', 'employers.id', 'staff_employer.employer_id')
                    ->join('main.users', 'main.users.id', 'staff_employer.user_id')
                    ->leftJoin('main.regions', 'regions.id', 'employers.region_id')
                    ->leftJoin('main.contributing_category_employer', 'employers.id', 'contributing_category_employer.employer_id')
                    ->leftJoin('main.employer_contributing_categories', 'employer_contributing_categories.id', 'contributing_category_employer.contributing_category_id')
                    ->where('staff_employer_allocations.isactive', 1)->where('staff_employer.isactive', 1)->where('staff_employer.user_id',$user_id)->where('staff_employer_allocations.isbonus', $isbonus)->orderBy('staff_employer.isbonus')->orderBy('employer_contributing_categories.id', 'desc')->orderBy('staff_employer.isattended', 'asc');
            }else{
                /*With contact*/
                $allocations = DB::table('main.staff_employer')->select(
                    'main.staff_employer.user_id as user_id',
                    'main.staff_employer.id as staff_employer_id',
                    'main.staff_employer.employer_id as employer_id',
                    DB::raw("concat_ws(' ', main.users.firstname, main.users.lastname) as staff"),
                    'main.employers.name as employer',
                    'main.staff_employer.start_date as allocation_start_date',
                    'main.staff_employer.end_date as allocation_end_date',
                    'main.staff_employer.isactive as relation_status',
                    'main.staff_employer.isattended as attendance_status',
                    'main.staff_employer.no_of_follow_ups as no_of_follow_ups',
                    'main.employer_contributing_categories.name as contributing_category',
                    'main.employers.isonline as isonline',
                    'main.employers.employer_status as employer_status',
                    'main.regions.name as region_name',
                    DB::raw("(select count(1) from main.bookings_view b
join booking_grace_period_view grace on grace.booking_id = b.booking_id
where grace.grace_period_end < now()::date and b.employer_id = employers.id and b.ispaid = 0) as missing_months"),

                    DB::raw('portal.users.name as contact_person'),
                    DB::raw('coalesce(portal.users.phone, main.employers.phone) as contact_phone'),
                    DB::raw('coalesce(portal.users.email, main.employers.email) as contact_email')
                )
                    ->join('main.staff_employer_allocations', 'main.staff_employer_allocations.id', 'main.staff_employer.staff_employer_allocation_id')
                    ->join('main.employers', 'employers.id', 'main.staff_employer.employer_id')
                    ->join('main.users', 'main.users.id', 'main.staff_employer.user_id')
                    ->leftJoin('main.regions', 'regions.id', 'employers.region_id')
                    ->leftJoin('main.contributing_category_employer', 'main.employers.id', 'main.contributing_category_employer.employer_id')
                    ->leftJoin('main.employer_contributing_categories', 'main.employer_contributing_categories.id', 'main.contributing_category_employer.contributing_category_id')
                    ->leftJoin('portal.employer_user', function($join) {
                        $join->on('portal.employer_user.employer_id', 'main.employers.id');
                    })
                    ->leftJoin('portal.users','portal.employer_user.user_id', 'portal.users.id' )
                    ->where('staff_employer_allocations.isactive', 1)->where('staff_employer.isactive', 1)->where('portal.users.isactive',1 )->where('main.staff_employer.user_id',$user_id)->where('main.staff_employer_allocations.isbonus', $isbonus)->orderBy('main.staff_employer.isbonus')->orderBy('main.employer_contributing_categories.id', 'desc')->orderBy('main.staff_employer.isattended', 'asc');
            }
        }

        return $allocations;
    }

    /*Get employers allocated for general search from my emplyers page*/
    public function getEmployersAllocatedForGeneralForDt($user_id,$db_script_id )
    {
        $query = $this->getEmployersAllocatedForStaffActiveAllocationForDt($user_id, 0, 1);
        switch($db_script_id){
            case 2:
                /*Not contributed current month*/
                $current_month = Carbon::now()->subMonthsNoOverflow(1);
                $current_month_parsed = Carbon::parse($current_month);
                $collection_allocation_ids = StaffEmployerAllocation::query()->where('isactive', 1)->whereIn('category', [1,2,5])->pluck('id');
                $query = $query->leftJoin('receipt_codes as codes' ,function($join) use($current_month_parsed){
                    $join->on('codes.employer_id', 'staff_employer.employer_id')->whereMonth('codes.contrib_month','=',$current_month_parsed->format('m') )->whereYear('codes.contrib_month','=',$current_month_parsed->format('Y') )->whereNull('codes.deleted_at');
                })->whereNull('codes.id');

                return $query->where('staff_employer.user_id', $user_id)->whereIn('staff_employer.staff_employer_allocation_id', $collection_allocation_ids);
                break;
        }
    }

    /*Get all employers allocated for active allocations*/
    public function getAllEmployersAllocatedForDt()
    {
        $allocations = DB::table('staff_employer')->select(
            'staff_employer.user_id as user_id',
            'staff_employer.id as staff_employer_id',
            'staff_employer.employer_id as employer_id',
            DB::raw("concat_ws(' ', users.firstname, users.lastname) as staff"),
            'employers.name as employer',
            'staff_employer.start_date as allocation_start_date',
            'staff_employer.end_date as allocation_end_date',
            'staff_employer.isactive as relation_status',
            'staff_employer.isattended as attendance_status',
            'staff_employer.no_of_follow_ups as no_of_follow_ups',
//                DB::raw("' Treasury' as contributing_category"),
            'employer_contributing_categories.name as contributing_category',
            'employers.isonline as isonline',
            'employers.employer_status as employer_status',
            'staff_employer.isbonus as isbonus',
            DB::raw("(select count(1) from bookings_view b where b.employer_id = employers.id and b.ispaid = 0) as missing_months")
        )
            ->join('staff_employer_allocations', 'staff_employer_allocations.id', 'staff_employer.staff_employer_allocation_id')
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->join('users', 'users.id', 'staff_employer.user_id')
            ->leftJoin('main.contributing_category_employer', 'main.employers.id', 'main.contributing_category_employer.employer_id')
            ->leftJoin('main.employer_contributing_categories', 'main.employer_contributing_categories.id', 'main.contributing_category_employer.contributing_category_id')
            ->where('staff_employer_allocations.isactive', 1)
            ->where('staff_employer.isactive',1)
            ->orderBy('staff_employer.isbonus')->orderBy('staff_employer.isattended', 'asc');
        return $allocations;
    }

    /*Get collection summary*/
    public function getCollectionSummary($staff_employer_id)
    {
        $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
        $employer_id = $staff_employer->employer_id;
        $start_date = $staff_employer->start_date;
        $end_date = $staff_employer->end_date;
        $contribution = DB::table('employer_contributions')->where('employer_id', $employer_id)
            ->where('receipt_created_at', '>=', $start_date)->where('receipt_created_at', '<=', $end_date)->sum('contrib_amount');
        $interest = DB::table('employer_interest_payments')->where('employer_id', $employer_id)
            ->where('receipt_created_at', '>=', $start_date)->where('receipt_created_at', '<=', $end_date)->sum('interest_amount');
        return ['contribution_collection' => $contribution, 'interest_collection' => $interest];
    }

    /*Get contrib interest summary*/
    public function getGeneralContribSummary($staff_employer_id)
    {
        $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
        $employer_id = $staff_employer->employer_id;
        $contribution = DB::table('employer_contributions')->where('employer_id', $employer_id)
            ->sum('contrib_amount');
        $receivable = DB::table('bookings_mview')->where('employer_id', $employer_id)->where('ispaid',0)
            ->sum('booked_amount');
        $missing_months = $this->getEmployerMissingMonthsCount($employer_id);
        return ['contribution' => $contribution, 'receivable' => $receivable, 'missing_months' => $missing_months];
    }


//    /*Get contribution - receivable summary for employers not in large/non large allocations*/
//    public function queryForEmployersNotAssignedForCollection()
//    {
//$employers
//    }

    public function getPerformanceSummaryForOtherEmployers()
    {

        $performance_other_employers = DB::table('staff_other_employers_performance')->first();

        return $performance_other_employers;


    }



    /*Get no of missing months*/
    public function getEmployerMissingMonthsCount($employer_id)
    {
        $employer_repo = new EmployerRepository();
        return $employer_repo->getBookingsForMissingMonthsForDataTable($employer_id)->count();
    }



    /*eND OF mY EMPLOYER METHODS*/






    /*Update / deactivate duplicate employers in the staff employer*/
    public function deactivateDuplicateEmployers()
    {
        DB::table('staff_employer')
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->whereNotNull('employers.duplicate_id')
            ->where('staff_employer.isduplicate', 0)->update([
                'isduplicate' => 1,
                'isactive' => 0,
                'end_date' => standard_date_format(Carbon::now())
            ]);
        ;
    }


    /*Export employers for staff allocated employers */
    public function exportToExcelMyEmployers($user_id, $isbonus, $with_contact, $dt_script_id = 1)
    {
        switch($dt_script_id){
            case 1:
                $employers = $this->getEmployersAllocatedForStaffActiveAllocationForDt($user_id, $isbonus, $with_contact)->get()->toArray();
                break;

            default:
                $employers = $this->getEmployersAllocatedForGeneralForDt($user_id,$dt_script_id)->get()->toArray();
                break;
        }

        $results = array();
        foreach ($employers as $result) {
            $results[] = (array)$result;
        }

        return Excel::create('staffemployersallocated'. time(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

    }






    /*Allocation of employers to staff for top up monthly*/
    public function allocateBonusEmployersTopUp()
    {
        $recent_allocation = StaffEmployerAllocation::query()->where('isbonus', 1)->where('isactive',1)->where('category', 3)->orderBy('id', 'desc')->first();
        if(isset($recent_allocation->id)){
            dispatch(new StaffEmployerAllocationJob($recent_allocation->id, 4));
        }

    }


    /*Get my employers title page*/
    public function getMyEmployerPageTitle($isbonus)
    {
        switch ($isbonus)
        {
            case 0:
                return 'My Contributing employers';
                break;
            case 1:
                return 'My Dormant employers';
                break;
            case 2:
                return 'My Treasury employers';
                break;
            default:
                return 'My employers';
                break;
        }
    }






    /*Get Doc extension*/
    public function getDocExtension($file_name_input)
    {
        $file = request()->file($file_name_input);
        if($file->isValid()) {
            return $file->getClientOriginalExtension();
        }else{
            return '';
        }
    }

    /**
     * @param array $input
     * @param $closure_id
     * Save documents for follow ups
     */
    public function saveDocuments(array $input, $employer_id, $external_id)
    {
        return DB::transaction(function () use ($employer_id, $input, $external_id) {
            $document_id = $input['document_id'];
//            $staff_employer = DB::table('staff_employer')->where('id', $employer_id)->first();
            $employer = Employer::query()->find($employer_id);

            $check_if_recurring = (new DocumentRepository())->checkIfDocumentIsRecurring($document_id);
            $ext = $this->getDocExtension('document_file');

            if($check_if_recurring == false)
            {
                /*for non recurring document*/
                $check_if_exist = $this->checkIfDocumentExist($employer_id, $document_id, $external_id);
                if($check_if_exist == false){
                    /*if not exists - Attach new*/
                    $this->savePivotDocument($employer_id, $input,$external_id);
                }else{
                    $uploaded_doc = $employer->documents()->where('external_id', $external_id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
                    $this->updatePivotDocument($employer_id, $input,$uploaded_doc->pivot->id , $external_id);
                }

                /*Attach to storage*/
                $this->attachDocFileToStorage($employer_id, $document_id, $ext, $external_id);
            }else{
                /*for recurring documents*/
                $check_if_exist = isset($input['doc_pivot_id']) ? true : false;

                if($check_if_exist){
                    /*Exist - update*/
                    $this->updatePivotDocument($employer_id, $input, $input['doc_pivot_id'], $external_id);
                }else {
                    /*Does not exist - Add new*/
                    $this->savePivotDocument($employer_id, $input, $external_id);
                }
                /*Attach to storage*/
                $this->attachDocFileToStorage($employer_id, $document_id, $ext, $external_id);
            }
        });
    }


    /*Save into pivot table of document*/
    public function savePivotDocument($employer_id, array $input, $external_id)
    {
        $document_id = $input['document_id'];
        $employer = Employer::query()->find($employer_id);
        $check_if_exist = $this->checkIfDocumentExist($employer_id, $document_id,$external_id);
        $file = request()->file('document_file');
        if($file->isValid()) {
            $employer->documents()->attach([$document_id => [
                'name' => $file->getClientOriginalName(),
                'description' => $file->getClientOriginalName(),
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'external_id' => $external_id
            ]]);
        }
    }

    /*Update pivot table of document*/
    public function updatePivotDocument($employer_id, array $input, $doc_pivot_id, $external_id)
    {
        $employer = Employer::query()->find($employer_id);
        $file = request()->file('document_file');
        if($file->isValid()) {
            DB::table('document_employer')->where('id', $doc_pivot_id)->update([
                'name' => $file->getClientOriginalName(),
                'description' => $file->getClientOriginalName(),
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'external_id' => $external_id
            ]);
        }
    }


    /*Attach doc to server storage*/
    public function attachDocFileToStorage($employer_id, $document_id, $ext, $external_id)
    {
        /*Attach document to server*/
        $employer = Employer::query()->find($employer_id);
        $uploaded_doc = $employer->documents()->where('external_id', $external_id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
        $path = employer_debt_collection_dir() . DIRECTORY_SEPARATOR . $employer->id . DIRECTORY_SEPARATOR . $document_id . DIRECTORY_SEPARATOR;
        $filename = $uploaded_doc->pivot->id . '.' . $ext;
        $this->makeDirectory($path);
        $this->saveDocumentBasic('document_file',$filename, $path );
    }


    /*Delete document*/
    public function deleteDocument($doc_pivot_id)
    {
        $uploaded_doc =DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $path = employer_debt_collection_dir() . DIRECTORY_SEPARATOR . $uploaded_doc->employer_id . DIRECTORY_SEPARATOR . $uploaded_doc->document_id . DIRECTORY_SEPARATOR;
        $file_path = $path . $doc_pivot_id . '.' . $uploaded_doc->ext;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        /*delete*/
        DB::table('document_employer')->where('id', $doc_pivot_id)->delete();
    }


    /* Check if document exists for closure */
    public function checkIfDocumentExist($employer_id, $document_id, $external_id)
    {

        $employer = Employer::query()->find($employer_id);
        $check = $employer->documents()->where('external_id', $external_id)->where('document_id', $document_id)->count();
        if($check > 0)
        {
            return true;
        }else{
            return false;
        }
    }

    /*Get uploaded doc per staff employer by document id*/
    public function getUploadedDoc($external_id, $document_id)
    {
        $uploaded_doc =DB::table('document_employer')->where('external_id', $external_id)->where('document_id', $document_id)->first();
        return $uploaded_doc;
    }



    /*Get document doc path file url*/
    public function getDocPathFileUrl($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $file_path = employer_debt_collection_path() . DIRECTORY_SEPARATOR. $uploaded_doc->employer_id . DIRECTORY_SEPARATOR .  $uploaded_doc->document_id . DIRECTORY_SEPARATOR . $doc_pivot_id . '.' .$uploaded_doc->ext; //closure.
        return $file_path;

    }






    /*STAFF EMPLOYER DASHBOARD  - PERFORMANCE*/

    /*Start of contribution performance */
    public function queryForStaffEmployerPerformance(){

        $query =  DB::table('staff_employer_contrib_performance')->select(
            'staff_employer_contrib_performance.user_id',
            'staff_employer_contrib_performance.fullname',
            'staff_employer_contrib_performance.no_of_employers',
            'staff_employer_commitment_followups.perfomance as percent_commitment',
            DB::raw(" arrears_before + receivable_current as target_receivable"),
            DB::raw(" arrears_contribution + contribution_current as contribution"),
            'online_employers',
            'arrears_before',
            'arrears_contribution',
            'receivable_current',
            'contribution_current',
            'attended_commitments',
            'no_of_commitments',
            DB::raw("((arrears_contribution + contribution_current) / (arrears_before + receivable_current)) * 100 as performance"),
            DB::raw(" ((contribution_current) / (receivable_current)) * 100  as current_performance")
        )->leftJoin('staff_employer_commitment_followups','staff_employer_commitment_followups.user_id', '=', 'staff_employer_contrib_performance.user_id');

        return $query;
    }


    /*Collection performance query - Intern (Mview)*/
    public function queryForStaffEmployerPerformanceIntern(){

        $query =  DB::table('staff_employer_contrib_performance_intern')->select(
            'user_id',
            'fullname',
            'no_of_employers',
            DB::raw(" arrears_before + receivable_current as target_receivable"),
            DB::raw(" arrears_contribution + contribution_current as contribution"),
            'online_employers',
            'arrears_before',
            'arrears_contribution',
            'receivable_current',
            'contribution_current',
            DB::raw(" ( (arrears_contribution + contribution_current) / (arrears_before + receivable_current)) * 100  as performance"),
            DB::raw(" ( (contribution_current) / (receivable_current)) * 100  as current_performance")
        );

        return $query;
    }


    /*Staff employer performance from staff_employer_collection_performances table*/
    public function queryForStaffEmployerCollectionPerformanceTable(){

        $query =  DB::table('staff_employer_collection_performances')->select(
            'staff_employer_collection_performances.id as performance_id',
            'user_id',
            DB::raw("concat_ws(' ', u.firstname, u.middlename, u.lastname)as fullname "),
            'no_of_employers',
            DB::raw(" arrears_before + receivable_current as target_receivable"),
            DB::raw(" arrears_collection + collection_current as contribution"),
            'percent_online_employers',
            'arrears_before',
            'arrears_collection',
            'receivable_current',
            'collection_current',
            'collection_month',
            DB::raw(" ( (arrears_collection + collection_current) / (arrears_before + receivable_current)) * 100  as performance"),
            DB::raw(" ( (collection_current) / (receivable_current)) * 100  as current_performance")
        )
            ->join('users as u', 'u.id', 'staff_employer_collection_performances.user_id');

        return $query;
    }

    /*Get staff employer performance*/
    public function getStaffEmployerPerformance()
    {
        $staffs = $this->queryForStaffEmployerPerformance()->orderBy('performance', 'desc')->get();
        return $staffs;
    }


    /*Get Staff employer performance Intern*/
    public function getStaffEmployerPerformanceIntern()
    {
        $staffs = $this->queryForStaffEmployerPerformanceIntern()->orderBy('performance', 'desc')->get();
        return $staffs;
    }


    /*Get total summary of total target and total actual*/
    public function getStaffEmployerPerformanceTotalSummary()
    {
        $summary = DB::table('staff_employer_contrib_performance')->select(
            DB::raw("sum(no_of_employers) as no_of_employers"),
            DB::raw("sum(arrears_before + receivable_current) as total_receivable"),
            DB::raw("sum(arrears_contribution + contribution_current) as total_contribution"),
            DB::raw("sum(online_employers) as total_online_employers"),
            DB::raw("sum(arrears_before) as total_arrears_before"),
            DB::raw("sum(arrears_contribution) as total_arrears_contribution"),
            DB::raw("sum(receivable_current) as total_receivable_current"),
            DB::raw("sum(contribution_current) as total_contribution_current")
        )->first();
        return $summary;
    }

    /*Staff employer performance totoal summary for intern*/
    public function getStaffEmployerPerformanceTotalSummaryIntern()
    {
        $summary = DB::table('staff_employer_contrib_performance_intern')->select(
            DB::raw("sum(no_of_employers) as no_of_employers"),
            DB::raw("sum(arrears_before + receivable_current) as total_receivable"),
            DB::raw("sum(arrears_contribution + contribution_current) as total_contribution"),
            DB::raw("sum(online_employers) as total_online_employers"),
            DB::raw("sum(arrears_before) as total_arrears_before"),
            DB::raw("sum(arrears_contribution) as total_arrears_contribution"),
            DB::raw("sum(receivable_current) as total_receivable_current"),
            DB::raw("sum(contribution_current) as total_contribution_current")
        )->first();
        return $summary;
    }

    /*emd-- of staff contribution performance*/



    /*Start of dormant employers performance*/

    public function queryForStaffEmployerDormantPerformance(){

        $query =  DB::table('staff_employers_dormant_performance')->select(
            'user_id',
            'fullname',
            'allocated_employers',
            'activated_employers',
            DB::raw(" (cast(activated_employers as decimal) * 100 ) / cast(allocated_employers as decimal) as performance")
        );

        return $query;
    }

    public function getStaffEmployerDormantPerformance()
    {
        $staffs = $this->queryForStaffEmployerDormantPerformance()->orderBy('performance', 'desc')->get();
        return $staffs;
    }

    /*Get total summary of total target and total actual - Dormant performance*/
    public function getStaffEmployerDormantPerformanceTotalSummary()
    {
        $summary = DB::table('staff_employers_dormant_performance')->select(
            DB::raw("sum(allocated_employers) as total_allocated_employers"),
            DB::raw("sum(activated_employers) as total_activated_employers")
        )->first();
        return $summary;
    }

    /*End of dormant employers performance*/



    /*Start of dormant employers performance*/

    public function queryForStaffEmployerTreasuryPerformance(){

        $query =  DB::table('staff_employers_treasury_performance')->select(
            'user_id',
            'fullname',
            'allocated_employers',
            'online_employers',
            DB::raw(" (cast(online_employers as decimal) * 100 ) / cast(allocated_employers as decimal)  as performance")
        );

        return $query;
    }

    public function getStaffEmployerTreasuryPerformance()
    {
        $staffs = $this->queryForStaffEmployerTreasuryPerformance()->orderBy('performance', 'desc')->get();
        return $staffs;
    }

    /*Get total summary of total target and total actual - Treasury performance*/
    public function getStaffEmployerTreasuryPerformanceTotalSummary()
    {
        $summary = DB::table('staff_employers_treasury_performance')->select(
            DB::raw("sum(allocated_employers) as total_allocated_employers"),
            DB::raw("sum(online_employers) as total_online_employers")
        )->first();
        return $summary;
    }


    /*End of dormant employers performance*/


    /*Check if user accessing this method has right or is the owner*/
    public function checkIfUserIsOwner($owner_user_id){
        $access_user = access()->user();
        $owner_user = User::query()->find($owner_user_id);
        $substitute = $owner_user->substitutingUser;
        $substitute_id = isset($substitute) ? $substitute->id : null;
        if($owner_user_id == $substitute_id || $access_user->id == $owner_user_id){
            /*of - owner or delegate*/
        }else{
            /*no owner*/
            throw  new GeneralException('You do not have access right to perform this action! Please check allocated user');
        }
    }



    /**
     *
     * ALLOCATE STAFF REGION - responsible i.e. area of specification
     */

    public function storeAllocationStaffRegion(array $input)
    {
        foreach ($input as $key => $value) {
            if (strpos($key, 'region') !== false) {
                $user_id = substr($key, 6);
                $region_id = $input['region'. $user_id];
                $check = DB::table('staff_region')->where('user_id', $user_id)->where('unit_id', 5)->count();
                if($check == 0)
                {
                    /*create new*/
                    DB::table('staff_region')->insert([
                        'user_id' => $user_id,
                        'region_id' => $region_id,
                        'unit_id' => 5,
                        'created_at' => Carbon::now()
                    ]);
                }else{
                    /*update existing*/
                    DB::table('staff_region')->where('user_id', $user_id)->where('unit_id', 5)->update([
                        'region_id' => $region_id,
                        'updated_at' => Carbon::now()
                    ]);
                }
            }
        }

    }






    /**
     * REPLACE employers with Medium active employers
     */
    /*Replace Employers*/
    public function replaceEmployers(array $input){
        DB::transaction(function () use($input) {
            $this->checkReplacementFormValidation($input);
            $replacement_type = $input['replacement_type'];
            /*update staff_employer*/
            foreach ($input['id'] as $staff_employer_id) {
                /*Update end date*/
                $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
                $new_medium = $this->getUnallocatedActiveMediumEmployers(1)->first();
                $this->replaceEmployer($staff_employer_id,$new_medium->employer_id,$replacement_type);
//                if($replacement_type == 3){
//                    /*Replace closed employer*/
//                    $this->replaceEmployer($staff_employer_id,$new_medium->employer_id,$input['replacement_type']);
//                }else{
//                    /*Replace others small, dormant, duplicate*/
//                    $this->replaceEmployer($staff_employer->employer_id, $new_medium->employer_id, $staff_employer_id,$input['replacement_type']);
//                }

            }

        });
    }

    /*Get unallocated active medium employers*/
    public function getUnallocatedActiveMediumEmployers($limit)
    {
        $service = new AllocateNonLargeEmployers();
        $employers = $service->getUnallocatedAllMediumsEmployers($limit);
        return $employers;
    }

//    /*Replace employers*/
//    public function replaceEmployer($old_employer_id, $new_employer_id, $staff_employer_id, $replacement_type){
//
//        DB::table('staff_employer')->where('id', $staff_employer_id)
//            ->update([
//                'employer_id' => $new_employer_id,
//                'old_employer_id' => $old_employer_id,
//                'isactive' =>1,
//                'isduplicate' =>0,
//                'replacement_type' => $replacement_type,
//                'updated_at' => Carbon::now(),
//            ]);
//    }

    /*Replace Employers*/
    public function replaceEmployer($staff_employer_id, $new_employer_id, $replacement_type)
    {
        DB::transaction(function () use($staff_employer_id, $new_employer_id, $replacement_type) {
            $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
            $check_if_intern = $this->checkIfStaffIsIntern($staff_employer->user_id);
            /*Users should different*/
            DB::table('staff_employer')->where('id', $staff_employer_id)->update([
                'end_date' => standard_date_format(Carbon::now()),
                'isactive' => 0,
                'replacement_type' => $replacement_type,
                'updated_at' => Carbon::now(),
            ]);
            /*Attach new employer to this allocation*/
            DB::table('staff_employer')->insert([
                'user_id' => $staff_employer->user_id,
                'employer_id' => $new_employer_id,
                'start_date' => $staff_employer->start_date,//TODO need to be current in near future
                'end_date' => $staff_employer->end_date,
                'unit_id' => 5,
                'created_at' => Carbon::now(),
                'staff_employer_allocation_id' => $staff_employer->staff_employer_allocation_id,
                'old_employer_id' => $staff_employer->employer_id,
                'isbonus' => $staff_employer->isbonus,
                'replacement_type' => null,
                'isintern' => ($check_if_intern) ? 1 : 0
            ]);
        });
    }


    /*Remove Employers*/
    public function removeEmployers(array $input){
        DB::transaction(function () use($input) {
            $this->checkReplacementFormValidation($input);
            $replacement_type = $input['replacement_type'];
            /*update staff_employer*/
            foreach ($input['id'] as $staff_employer_id) {
                /*Update end date*/
                $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
                DB::table('staff_employer')->where('id', $staff_employer_id)->update([
                    'isactive' => 0,
                    'end_date' => standard_date_format(Carbon::now()),
                    'replacement_type' => $replacement_type,
                    'updated_at' => Carbon::now(),
                ]);

                /*Update no of employers*/
                $this->updateNoOfEmployersToAllocated($staff_employer->staff_employer_allocation_id);
            }

        });
    }


    /*Get allocations for replacement list */
    public function getStaffAllocationsForReplacementForDt()
    {
        $input = request()->all();
        $filter_category = $input['category'] ?? null;
        $bonus = ($filter_category == 6) ? [0,1] : [0];//bonus
        $query = StaffEmployerAllocation::query()->select(
            'staff_employer.id as staff_employer_id',
            DB::raw("concat_ws(' ', users.firstname, users.lastname) as staff"),
            'employers.name as employer',
            'staff_employer.start_date as allocation_start_date',
            'staff_employer.end_date as allocation_end_date',
            'employer_contributing_categories.name as contributing_category',
            'staff_employer.no_of_follow_ups',
            'employers.employer_status',
            'staff_employer.isduplicate'
        )
            ->join('staff_employer', 'staff_employer_allocations.id', 'staff_employer.staff_employer_allocation_id')
            ->leftJoin('contributing_category_employer', 'contributing_category_employer.employer_id', 'staff_employer.employer_id')
            ->leftJoin('employer_contributing_categories','employer_contributing_categories.id', 'contributing_category_employer.contributing_category_id')
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->join('users', 'users.id', 'staff_employer.user_id')
            ->whereIn('staff_employer.isbonus',$bonus)->where('staff_employer.isactive',1)->whereNull('staff_employer.replacement_type');

        $query = $this->queryWithFilterAllocationForReplacement($query, $input);

        return $query;

    }


    /*Query with filter allocation for replacement*/
    public function queryWithFilterAllocationForReplacement($parent_query,array $input)
    {
        /*Category*/
        if(isset($input['category']))
        {
            $category = $input['category'];
            if ($category == 1){
                /*Small*/
                $parent_query = $parent_query->where('contributing_category_employer.contributing_category_id', 4)->where('staff_employer.isactive',1);
            }elseif ($category == 2){
                /*Duplicate*/
                $parent_query = $parent_query->where('staff_employer.isduplicate', 1);
            }elseif($category == 3){
                /*closed business*/
                $parent_query = $parent_query->where('employers.employer_status', 3)->where('staff_employer.isactive',1);
            }elseif($category == 4){
                /*dormant*/
                $parent_query = $parent_query->where('employers.employer_status', 2)->where('staff_employer.isactive',1);
            }elseif($category == 5){
                /*Active*/
                $parent_query = $parent_query->where('employers.employer_status', 1)->where('staff_employer.isactive',1);
            }elseif($category == 6){
                /*Bonus allocation*/
                $parent_query = $parent_query->where('staff_employer_allocations.category',3);
            }


        }else{
//            $parent_query = $parent_query->whereRaw("( ((contributing_category_employer.contributing_category_id = 4 or employers.employer_status = 2 or employers.employer_status = 3) and staff_employer.isactive = 1 ) or staff_employer.isduplicate = 1)  ");
        }


        /*assigned to - user*/
        if(isset($input['user_id']))
        {
            $parent_query = $parent_query->where('staff_employer.user_id', $input['user_id']);
        }

        return $parent_query;
    }


    /*Check replacement form validation*/
    public function checkReplacementFormValidation($input)
    {

        if(!isset($input['id']))
        {
            throw new WorkflowException("Select at least one employer for replacement! Please check!");
        }

    }

//TODO : rectify already replaced employers - Temporary
    public function rectifyAlreadyReplacedEmployer()
    {
        DB::table('staff_employer')->where('staff_employer.id', 11)->where('employer_id', 6962)->update([
            'replacement_type' => 2
        ]);
    }







    /**
     * Save Staff  monthly collection performances
     */
    public function saveStaffMonthlyCollectionPerformances($isintern = 0)
    {
        if($isintern == 0){
            $table_object = 'staff_employer_contrib_performance';

        }else{
            /*Is intern*/
            $table_object = 'staff_employer_contrib_performance_intern';

        }

        $contrib_performances = DB::table($table_object)->get();
        /*Start collection month*/
        $today = Carbon::now();
        $collection_month = $today->subMonthNoOverflow(2)->endOfMonth();
        /*end collection month*/
        foreach($contrib_performances as $performance)
        {
            $check_if_exists = DB::table('staff_employer_collection_performances')->whereMonth('collection_month','=', $collection_month->format('n'))->whereYear('collection_month','=', $collection_month->format('Y'))->where('user_id', $performance->user_id)->where('isintern', $isintern)->count();
            $percent_online_employers = ($performance->online_employers * 100) / $performance->no_of_employers;
            /*check if exists for user*/
            if($check_if_exists == 0){
                DB::table('staff_employer_collection_performances')->insert([
                    'user_id' => $performance->user_id,
                    'no_of_employers' => $performance->no_of_employers,
                    'percent_online_employers' => $percent_online_employers,
                    'arrears_before' =>( $performance->arrears_before) ?  $performance->arrears_before : 0,
                    'arrears_collection' => ($performance->arrears_contribution) ? $performance->arrears_contribution : 0,
                    'receivable_current' => ($performance->receivable_current) ? $performance->receivable_current : 0,
                    'collection_current' => ($performance->contribution_current) ? $performance->contribution_current : 0,
                    'collection_month' => standard_date_format($collection_month),
                    'created_at' => standard_date_format(Carbon::now()),
                    'isintern' => $isintern,
                ]);
            }
        }

    }



    /*Check if all performance are already generated*/
    public function checkIfAllPerformancesExists($isintern = 0){

        /*Start collection month*/
        $today = Carbon::now();
        $collection_month = $today->subMonthNoOverflow(2)->endOfMonth();
        $count_saved = DB::table('staff_employer_collection_performances')->whereMonth('collection_month','=', $collection_month->format('n'))->whereYear('collection_month','=', $collection_month->format('Y'))->where('isintern', $isintern)->count();
        $categories = ($isintern == 0) ? [1,2] : [5];
        $no_of_staff = StaffEmployerAllocation::query()->where('isbonus', 0)->whereIn('category', $categories)->sum('no_of_staff');
        if($count_saved < $no_of_staff){
            /*few missing*/
            return false;
        }else{
            /*All exists*/
            return true;
        }


    }

    /*Save staff employer*/
    public function saveStaffEmployer($user_id, $employer_ids, $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
//        $start_date = ($allocation->isapproved == 0) ?  $allocation->start_date : standard_date_format(Carbon::now());// if not approved is in the beginning user allocation start_date, after approval need to take todays date
        $start_date = $allocation->start_date;
        $end_date = $allocation->end_date;
        $unit_id = 5;
        $created_at = Carbon::now();

        foreach($employer_ids as $employer){
            $check_if_allocated = $this->checkIfEmployerAlreadyAllocated($employer->employer_id, $staff_employer_allocation_id);

            if($check_if_allocated == false)
            {
                DB::table('staff_employer')->insert([
                    'user_id' => $user_id,
                    'employer_id' => $employer->employer_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'unit_id' => $unit_id,
                    'created_at' => $created_at,
                    'staff_employer_allocation_id' => $staff_employer_allocation_id,
                    'isintern' => ($allocation->category == 5) ? 1 : 0,
                ]);
            }

        }


    }


    /*Check if employer is already allocated*/
    public function checkIfEmployerAlreadyAllocated($employer_id, $staff_employer_allocation_id)
    {
        $check = DB::table('staff_employer')->where('isactive', 1)->where('employer_id', $employer_id)->count();

        if($check >= 1)
        {
            return true;

        }else{
            return false;
        }
    }


    /*check if staff already allocated*/
    public function checkIfStaffAlreadyAllocated($user_id, $staff_employer_allocation_id)
    {
        $check = DB::table('staff_employer')->where('user_id', $user_id)->where('staff_employer_allocation_id', $staff_employer_allocation_id)->where('isactive',1)->count();
        if($check > 0)
        {
            return true;

        }else{
            return false;
        }
    }

    /**
     * @return bool
     * Save document bulk assignment
     */
    public function saveBulkAssignmentFileAttachment()
    {
        if (request()->hasFile('document_file')) {
            $file = request()->file('document_file');
            if ($file->isValid()) {
                //Check the excel file
                $file->move(temporary_dir() . DIRECTORY_SEPARATOR . 'staff_employer' . DIRECTORY_SEPARATOR , "file");
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*Update No of employers to allocated */
    public function updateNoOfEmployersToAllocated($staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
        $count = DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation_id)->count();

        $allocation->update([
            'no_of_employers' => $count
        ]);

    }

    /*Check if staff is intern*/
    public function checkIfStaffIsIntern($user_id)
    {
        $config = $this->staffEmployerConfig();
        $intern_staff_arr = unserialize($config->intern_staff);
        if(in_array($user_id, $intern_staff_arr))
        {
            return true;
        }else{
            return false;
        }
    }


    /*Get staff employers assigned by user*/
    public function getStaffEmployersByUserForSelect($q, $page)
    {
        //$name = trim(preg_replace('/\s+/', '', $q));
        $user_id = access()->id();
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = strtolower($q);
        $data['items'] = DB::table('staff_employer as se')
            ->select([
                'se.id as staff_employer_id',
                'e.name',
            ])
            ->join("employers as e", "e.id", "=", "se.employer_id")
            ->whereRaw("(e.name  ~* ? or cast(e.id as varchar) ~* ?)", [$name, $name])
            ->whereNull('se.deleted_at')->where('se.isactive',1)->where('se.user_id',$user_id)
            ->limit($resultCount)
            ->offset($offset)
            ->get()->toArray();

        return response()->json($data);

    }


    /*Get Categories for collection Allocation Array*/
    public function getCategoriesForCollectionAllocationArray()
    {
        return [1,2,5];
    }

    /*Query Staff Employer General*/
    public function queryStaffEmployerGeneral()
    {
        return DB::table('staff_employer as se')->select(
            'e.id as employer_id',
            'e.reg_no',
            'e.name as employer_name',
            'r.name as region_name',
            'se.start_date',
            'se.end_date',
            'u.username'
        )
            ->join('employers as e', 'e.id', 'se.employer_id')
            ->join('users as u', 'u.id', 'se.user_id')
            ->join('staff_employer_allocations as sa', 'sa.id', 'se.staff_employer_allocation_id')
            ->leftJoin('regions as r', 'r.id', 'e.region_id')
            ->whereNull('se.deleted_at');
    }

    /*General function for exporting staff employer*/
    public function exportStaffEmployerGeneral($result_list,  $filename = 'staffEmployer')
    {
        $result_list = $result_list->get()->toArray();
        $results = array();
        foreach ($result_list as $result) {
            $results[] = (array)$result;
        }

        return Excel::create($filename. time(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

    }


/* commitment status */
    public function commitmentStatus($status){
      $return = '';

      switch ($status) {
        case 0:
        $return = '<span class="btn btn-warning btn-sm"> Pending </span>';
        break;
        case 1:
        $return = '<span class="btn btn-success btn-sm"> Attended </span>';
        break;
        case 2:
        $return = '<span class="btn btn-danger btn-sm"> Cancelled </span>';
        break;
        case 3:
        $return = '<span class="btn btn-danger btn-sm"> Rejected </span>';
        break;

        default:
      # code...
        break;
    }

    return $return;
}
}