<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\LegacyEmployeeContribution;
use App\Repositories\BaseRepository;

class LegacyEmployeeContributionRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = LegacyEmployeeContribution::class;

}