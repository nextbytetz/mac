<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;


use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerParticularChange;
use App\Models\Operation\Compliance\Member\EmployerParticularChangeType;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use App\Services\Compliance\MergeDeMergeEmployers;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Mail\NotifyChangeParticularOnline;
use Mail;

class ContributionModificationsRepository extends BaseRepository
{
   use AttachmentHandler, FileHandler;


   const MODEL = EmployerParticularChange::class;

   protected $employer_repo;


   public function __construct()
   {
    parent::__construct();
    $this->employer_repo = new EmployerRepository();

}

public function replaceMonthPaid(array $input, $request, $payroll_id)
{
    $return_month_paid = DB::transaction(function() use ($input, $request, $payroll_id)
    {
        $wfTrack = new wfTrackRepository();
        DB::table('portal.change_contribution_months')->insert([

            'employer_id' => $input['employer_id'],
            'old_contrib_month' => $input['old_contrib_month'],
            'new_contrib_month' => $input['new_contrib_month'],
            'old_receipt_attachment' => $input['old_receipt_attachment'],
            'reason_change' => $input['reason_change'],
            'document_attachment' => $input['document_attachment'],
            'contribution_id' => $input['contribution_id'],
            'description' => $input['descritpion'],
            'user_id' => $input['user_id'],
            'payroll_id'=> $input['payroll_id'],
            'user_type' => 'App\Models\Auth\PortalUser',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

    });
    return $return_month_paid;

}

public function changeMultipleMonthPaid(array $input, $request, $payroll_id)
{
 
        // dd($request->all());
  $return = DB::transaction(function () use ($input, $request,$payroll_id) {


    $wfTrack = new WfTrackRepository();            
    /*Start: save into main contribution multiple paid table*/
    $change__employer_contribution = new ContributionModificationsRepository();


    /*Start: save into portal  contribution multiple table table*/        
    DB::table('portal.contribution_multiple_paid')->insert([
        'employer_id' => $input['employer_id'],
        'old_contrib_month' => $input['old_contrib_month'],
        'old_employee_count' => $input['old_employee_count'],
        'old_total_grosspay' => $input['old_total_grosspay'],
        'old_receipt_code' => $input['old_receipt_code'],
        'new_contrib_month' =>  $input['new_contrib_month'],
        'new_employee_count' =>  $input['new_employee_count'],
        'new_total_grosspay' =>  $input['new_total_grosspay'],
        'reason_change' => $input['reason_change'],
        'document_attachment' => $input['document_attachment'],
        'description' => $input['description'],
        'user_id' => $input['user_id'],
        'payroll_id' => $input['payroll_id'],
        'user_type' => 'App\Models\Auth\PortalUser',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ]);

});

  return $return;
}

/* to update storage*/


public function update(array $input, $request, $payroll_id)
{

    $return = DB::transaction(function () use ($input, $request, $payroll_id) {

        $wfTrack = new WfTrackRepository();

        $change_contribution = $this->query()->where('id',$request->request_id)->update($input);

        /*Start: save into main employer particular changes table*/
        $change_contribution_month = new ContributionModification();
        $change_contribution_month = $change_contribution_month->query()->where('employee_uploads_id',$request->request_id)->first();

        $emp_change_particular = (new ContributionModification())->query()->where('employee_uploads_id',$request->request_id)->update([
            'employer_id' => $input['employer_id'],
            'remark' => $input['description'],
            'user_id' => $input['user_id'],
            'updated_at' => Carbon::now(),
        ]);
        /*end*/

        /*Start: save into main employer particular change types table*/

        DB::table('main.change_contribution_months')->where('change_contribution_month_id',$change_contribution_month->id)->update([
            'name_change_type_cv_id' => $change_types['name_change_type_cv_id'],
            'old_values' => $input['old_values'],
            'new_values' => $input['new_values'],
            'description' => $input['description'],
            'payroll_id' => $input['payroll_id'],
            'updated_at' => Carbon::now(),
            'particular_change_type_cv_id' =>$change_types['change_type_cv_id'],

        ]);
        if (!empty($request->reason_change)) {
            $this->saveDocuments($input,  $change_contribution_month->id);
        }

        /* start : Reinitialize Workflow */
        $this->resubmitForWfApprovalAfterCorrection($request->request_id);


        return $change_contribution;
    });
    return $return;
}


}