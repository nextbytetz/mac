<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\EmployerAdvancePayment;
use App\Repositories\BaseRepository;

class EmployerAdvancePaymentRepository extends BaseRepository
{
    const MODEL = EmployerAdvancePayment::class;

    public function getAllForDatatable($id)
    {
        return $this->query()->where("employer_id", $id);
    }

}