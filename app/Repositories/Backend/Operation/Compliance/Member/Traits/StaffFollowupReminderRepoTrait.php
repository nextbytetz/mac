<?php

namespace App\Repositories\Backend\Operation\Compliance\Member\Traits;

use App\Jobs\Compliance\StaffEmployerAllocation\PostStaffEmployerFollowupReminderIndividualJob;
use App\Jobs\Compliance\StaffEmployerAllocation\PostStaffEmployerFollowupReminderJob;
use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use App\Models\Operation\Compliance\Member\StaffFollowupReminder;
use App\Notifications\Backend\Employer\StaffMissingFollowupReminder;
use Carbon\Carbon;
use function Complex\theta;
use Illuminate\Support\Facades\DB;
trait StaffFollowupReminderRepoTrait {


    protected $followup_reminder_next_date;

    /*Save followup reminder*/
    public function saveFollowupReminder(array $input)
    {
        return DB::transaction(function() use($input){
            $user_id = $input['user_id'];
            $start_date = $input['start_date'];
            $end_date  = $input['end_date'];
            $isnew = isset($input['isnew']) ? 1 : 0;
            $start_end_date_range = $this->getStartEndDatesActiveRemindersByUser($user_id);
//            $compensation_followups = $this->getCompensationBetweenDatesByUser($user_id, $start_end_date_range['start_date'], $end_date);
            $compensation_followups = 0;
            $target_missing = $this->getFollowupMissingTargetOnDateRange($user_id, $start_date, $end_date);
            $weekly_target = $this->getActualWeeklyTargetByUser($user_id, $start_date, $end_date);
//            $prev_missing_target = $this->getPreviousTotalMissingFollowupTarget($user_id, $start_date);
//            $total_missing_target = $target_missing + $prev_missing_target - $compensation_followups ;
            $total_missing_target = $this->getFollowupMissingTargetOnDateRange($user_id, $start_end_date_range['start_date'] ?? $start_date, $end_date);
            $total_missing_target = ($total_missing_target > 0) ? $total_missing_target : 0;
            $previous_reminder = $this->getPreviousFollowupReminder($user_id, $start_date);
            $previous_reminder = null;
            $parent_reminder_id = ($previous_reminder) ? $previous_reminder->id : null;
            $elevation_level_trigger = ($target_missing > 0) ? 1 : 0;
            $elevation_level = $this->getElevationLevelByUser($user_id, $start_date) + $elevation_level_trigger;
            $isactive = ($total_missing_target == 0) ? 0 : 1;
            $completed_date= ($isactive == 0) ? standard_date_format(getTodayDate()) : null;
            $days_worked = $this->getNoOfDaysWorkedForReminder($user_id, $start_date, $end_date);

            $save_data = [
                'user_id' => $user_id,
                'week_start_date' => standard_date_format($start_date),
                'week_end_date' => standard_date_format($end_date),
                'weekly_missing_target' => $target_missing,
                'weekly_target' => $weekly_target,
                'parent_reminder_id' => $parent_reminder_id,
                'elevation_level' => $elevation_level,
                'prev_missing_target' => 0,
                'total_missing_target' => $total_missing_target,
                'isactive' => $isactive,
                'completed_date' => $completed_date,
                'compensation_followups' => $compensation_followups,
                'days_worked' => $days_worked,
                'oldest_start_date' => isset($start_end_date_range['start_date']) ? standard_date_format($start_end_date_range['start_date']) : $start_date
            ];


            /*save data*/
            $reminder = $this->updateOrCreateFollowupReminder($save_data);
            /*Post email*/
            if(($isnew == 1 || $reminder->isnotified == 0) && $total_missing_target > 0 && $target_missing > 0){
                $this->postEmailNotificationForReminder($reminder->id);
            }else{
                /*Deactivate all*/
                $this->deactivateAllFollowupRemindersByUser($user_id);
            }
        });
    }

    /*Update or create follow up reminder*/
    public function updateOrCreateFollowupReminder(array $input)
    {
        return DB::transaction(function() use($input){
            $reminder = StaffFollowupReminder::updateOrCreate(['user_id' => $input['user_id'], 'week_start_date' => $input['week_start_date']],
                $input);
            return $reminder;
        });
    }

    /*Deactivate all followup reminder by user*/
    public function deactivateAllFollowupRemindersByUser($user_id)
    {
        StaffFollowupReminder::query()->where('user_id', $user_id)->where('isactive', 1)->update([
            'isactive' => 0
        ]);
    }

    /*Get weekly follow up target based on staff worked days*/
    public function getWeeklyFollowupTarget($user_id)
    {
        $config = DB::table('staff_employer_configs')->first();
        $check_if_intern = $this->checkIfStaffIsIntern($user_id);
        return ($check_if_intern) ?  $config->followup_weekly_target_intern :  $config->followup_weekly_target;
    }

    /*GetActual weekly target by user*/
    public function getActualWeeklyTargetByUser($user_id, $start_date, $end_date)
    {
        $default_weekly_target = $this->getWeeklyFollowupTarget($user_id);
        $daily_target = $default_weekly_target / 5;
        $no_days_worked = $this->getNoOfDaysWorkedForReminder($user_id, $start_date, $end_date);
        return $no_days_worked * $daily_target;
    }

    /*No of days worked*/
    public function getNoOfDaysWorkedForReminder($user_id, $start_date, $end_date)
    {

        $days_worked =  DB::table('audits')->select('user_id',
            DB::raw("created_at::date")
        )->where('user_id', $user_id)->whereRaw("created_at::date >= ? and created_at::date <= ? ", [standard_date_format($start_date), standard_date_format($end_date)])
            ->whereRaw("EXTRACT(ISODOW FROM created_at::date) NOT IN (6, 7)")
            ->groupBy('user_id', DB::raw("created_at::date"))->get()->count();

//        $days_worked =  DB::table('audits')->select('user_id',
//            DB::raw("created_at_test::date")
//        )->where('user_id', $user_id)->whereRaw(" created_at_test::date >= ? and created_at_test::date <= ? ", [standard_date_format($start_date), standard_date_format($end_date)])
//            ->whereRaw("EXTRACT(ISODOW FROM created_at_test::date) NOT IN (6, 7)")
//            ->groupBy('user_id', DB::raw("created_at_test::date"))->get()->count();
        return $days_worked;
    }

    /*Get staff ids in array for follow up reminders*/
    public function getStaffForReminderArray()
    {
        $config = DB::table('staff_employer_configs')->first();
        $users = unserialize($config->compliance_officers);
        $users_intern = unserialize($config->intern_staff);
        return array_merge($users, $users_intern);
    }

    /*Get no of unique weekly follow ups made - search by staff and date range*/
    public function getNoOfFollowUpsMadeOnDateRange($user_id, $start_date, $end_date)
    {

        /*unique follow ups*/
        $no_of_followups = StaffEmployerFollowUp::query()->select('staff_employer_id')->where('user_id', $user_id)->whereDate('date_of_follow_up', '>=', standard_date_format($start_date))->whereDate('date_of_follow_up', '<=', standard_date_format($end_date))->groupBy('staff_employer_id')->get()->count();
        return $no_of_followups;
    }

    /*Get missing target on date range*/
    public function getFollowupMissingTargetOnDateRange($user_id, $start_date, $end_date)
    {
        $weekly_target = $this->getActualWeeklyTargetByUser($user_id, $start_date, $end_date);
        $followup_made = $this->getNoOfFollowUpsMadeOnDateRange($user_id, $start_date, $end_date);
        return ($followup_made < $weekly_target) ? ($weekly_target - $followup_made)  : 0;
    }


    /*Get previous total missing follow ups target*/
    public function getPreviousTotalMissingFollowupTarget($user_id, $start_date)
    {
        $total = 0;
        $previous_reminders = StaffFollowupReminder::query()->where('isactive',1)->where('user_id', $user_id)->whereDate('week_end_date', '<', standard_date_format($start_date))->sum('weekly_missing_target');
//        foreach ($previous_reminders as $reminder)
//        {
//            $total = $reminder->weekly_missing_target + $total;
//        }
        return $previous_reminders;
    }


    /*Get previous followups reminder*/
    public function getPreviousFollowupReminder($user_id, $start_date)
    {
        $previous_reminder = StaffFollowupReminder::query()->where('isactive',1)->where('user_id', $user_id)->whereDate('week_end_date', '<', standard_date_format($start_date))->orderBy('week_end_date', 'desc')->first();
        return $previous_reminder;
    }

    /*Elevation level*/
    public function getElevationLevelByUser($user_id, $start_date)
    {
        $count_active_reminders_before = StaffFollowupReminder::query()->where('user_id', $user_id)->where('isactive', 1)->whereDate('week_end_date', '<', standard_date_format($start_date))->count();
        return $count_active_reminders_before;
    }

    /*Query for getting all followup reminders */
    public function queryNextFollowupReminders($user_id, $end_date)
    {
        $next_reminders = StaffFollowupReminder::query()->where('user_id', $user_id)->where('week_start_date', '>', standard_date_format($end_date))->orderBy('week_start_date');
        return $next_reminders;
    }

    /*Get compensation follow ups from future weeks*/
    public function getCompensationFollowUpsFromFutureWeeks($user_id, $end_date)
    {
        /*on existing reminders*/
        $extra_followups_next_reminders = 0;
        $extra_followups_current_reminders = 0;
        $default_weekly_followups = $this->getWeeklyFollowupTarget($user_id);
        $next_reminder = StaffFollowupReminder::query()->where('user_id', $user_id)->where('week_start_date', '>', standard_date_format($end_date))->orderBy('week_start_date')->first();

        if($next_reminder){
            $week_start_date = $next_reminder->week_start_date;
            $week_end_date = $next_reminder->week_end_date;
            $no_of_followups_made = $this->getNoOfFollowUpsMadeOnDateRange($user_id, $week_start_date, $week_end_date);
            $actual_followups_required = $this->getActualWeeklyTargetByUser($user_id, $week_start_date, $week_end_date);
            $extra_followups_next_reminders = $extra_followups_next_reminders + ($actual_followups_required < $no_of_followups_made) ? ($no_of_followups_made - $actual_followups_required) : 0;
        }

        /*Current / on going week if there is no next pending reminder*/
        if(!$next_reminder){
            $config = $this->staffEmployerConfig();
            $next_run_date =$this->followup_reminder_next_date ? $this->followup_reminder_next_date : (($config->followup_reminder_next_date) ? ($config->followup_reminder_next_date) : getTodayDate());// next run date after week complete
            $next_start_date_end_of_week = Carbon::parse($end_date)->addWeek()->endOfWeek();//start_date of future complete weeks
            $next_end_date_end_of_week = Carbon::parse($next_run_date)->endOfWeek();// end date of last complete week - cut off
            $no_of_followups_made_next_week = 0;
            $actual_followup_target = 0;
            /*Loop for each week to get extras*/
            while(comparable_date_format($next_start_date_end_of_week) <= comparable_date_format($next_end_date_end_of_week)) {
                $date_ranges = $this->getOnGoingWeekForReminder($next_start_date_end_of_week);
                $actual_followup_target = $actual_followup_target +  $this->getActualWeeklyTargetByUser($user_id, $date_ranges['week_start_date'], $date_ranges['week_end_date']);
                $no_of_followups_made_next_week = $no_of_followups_made_next_week +  $this->getNoOfFollowUpsMadeOnDateRange($user_id, $date_ranges['week_start_date'], $date_ranges['week_end_date']);
                $next_start_date_end_of_week = Carbon::parse($next_start_date_end_of_week)->addWeek()->endOfWeek();

            }

            $extra_followups_current_reminders = ($no_of_followups_made_next_week > $actual_followup_target) ? ($no_of_followups_made_next_week - $actual_followup_target) : 0;
        }

        return $extra_followups_current_reminders + $extra_followups_next_reminders;
    }

    /**
     * @param $user_id
     * @param $start_date
     * @param $end_date
     * @return float|int
     * Get compensation
     */
    public function getCompensationBetweenDatesByUser($user_id, $start_date, $end_date)
    {
        $no_of_followups_made = $this->getNoOfFollowUpsMadeOnDateRange($user_id, $start_date, $end_date);
        $actual_followups_required = $this->getActualWeeklyTargetByUser($user_id, $start_date, $end_date);
        $compensation = ($no_of_followups_made -  $actual_followups_required);
        return ($compensation > 0) ? $compensation : 0;
    }


    /*Ongoing week*/
    public function getOnGoingWeekForReminder($target_date)
    {
        $week_day = Carbon::parse(standard_date_format($target_date))->dayOfWeek;
        if ($week_day == Carbon::SATURDAY && $week_day == Carbon::SUNDAY)
        {
            $weekday_start = Carbon::parse($target_date)->addDays(2)->startOfWeek();
            $weekday_end = Carbon::parse($target_date)->addDays(2)->endOfWeek()->subDays(2);
        }else{
            $weekday_start = Carbon::parse($target_date)->startOfWeek();
            $weekday_end = Carbon::parse($target_date)->endOfWeek()->subDays(2);
        }
        return [
            'week_start_date' => $weekday_start,
            'week_end_date' => $weekday_end
        ];
    }

    /*Get no of followups done on the on going / current week*/
    public function getNoOfFollowupsDoneOnCurrentWeekByUser($user_id)
    {
        $current_week_dates = $this->getOnGoingWeekForReminder(getTodayDate());
        $start_date = $current_week_dates['week_start_date'];
        $end_date = $current_week_dates['week_end_date'];
        $no_of_followups_made = $this->getNoOfFollowUpsMadeOnDateRange($user_id, $start_date, $end_date);
        return $no_of_followups_made;
    }

    /*Get Target follow up summary data by user*/
    public function getTargetFollowupSummaryDataForUser($user_id)
    {
        $latest = StaffFollowupReminder::query()->where('user_id', $user_id)->where('isactive',1)->orderBy('week_end_date', 'desc')->first();
        $prev_missing = ($latest) ? $latest->total_missing_target : 0;
        $current_dates = $this->getOnGoingWeekForReminder(getTodayDate());
        $start_date = $current_dates['week_start_date'];
        $end_date = $current_dates['week_end_date'];
        $current_week_followup = $this->getNoOfFollowUpsMadeOnDateRange($user_id, $start_date, $end_date);
        $actual_follow_ups_target = $this->getActualWeeklyTargetByUser($user_id,$start_date, $end_date);
        return [
            'prev_missing_target' => $prev_missing,
            'current_week_followups' => $current_week_followup,
            'actual_follow_ups_target' => $actual_follow_ups_target,
            'weekly_target' => $this->getWeeklyFollowupTarget($user_id)
        ];
    }


    /*Update main reminders i.e. active and inactive*/
    public function updateMainReminders()
    {
        $this->updateAllInActiveRemindersWithCutoffDate();
        $this->updateAllActiveReminders();
    }

    /*Update/refresh all active reminders*/
    public function updateAllActiveReminders($user_id = null)
    {
        $input = [];
        if($user_id){
            $active_reminders = StaffFollowupReminder::query()->where('isactive', 1)->where('user_id', $user_id)->orderBy('week_start_date')->get();
        }else{
            $active_reminders = StaffFollowupReminder::query()->where('isactive', 1)->orderBy('week_start_date')->get();
        }

        foreach ($active_reminders as $active_reminder) {
            $input['user_id'] = $active_reminder->user_id;
            $input['start_date'] = $active_reminder->week_start_date;
            $input['end_date'] = $active_reminder->week_end_date;
            $this->saveFollowupReminder($input);
        }
    }


    /*Update All inactive reminders with cutoff date - to check if there is any change on previous data*/
    public function updateAllInActiveRemindersWithCutoffDate()
    {
        $limit_months = 1;
        $cut_off_date = Carbon::now()->subMonthsNoOverflow($limit_months);
        $inactive_reminders = StaffFollowupReminder::query()->where('isactive', 0)->where('completed_date','<=', standard_date_format($cut_off_date))->orderBy('week_start_date')->get();
        foreach ($inactive_reminders as $inactive_reminder) {
            $input['user_id'] = $inactive_reminder->user_id;
            $input['start_date'] = $inactive_reminder->week_start_date;
            $input['end_date'] = $inactive_reminder->week_end_date;
            $this->saveFollowupReminder($input);
        }
    }

    /*Update active status parent reminders*/
    public function updateActiveStatusParentReminders($user_id = null,$status = 0)
    {
        if($user_id){
            $parent_reminders = StaffFollowupReminder::query()->select('staff_followup_reminders.parent_reminder_id')
                ->join('staff_followup_reminders as parent', 'staff_followup_reminders.parent_reminder_id', 'parent.id')
                ->where('staff_followup_reminders.isactive',0)->where('parent.isactive', 1)->where('parent.user_id', $user_id)->get();
        }else{
            $parent_reminders = StaffFollowupReminder::query()->select('staff_followup_reminders.parent_reminder_id')
                ->join('staff_followup_reminders as parent', 'staff_followup_reminders.parent_reminder_id', 'parent.id')
                ->where('staff_followup_reminders.isactive',0)->where('parent.isactive', 1)->get();
        }


        foreach ($parent_reminders as $parent)
        {
            $this->changeStatusFollowupReminder($parent->parent_reminder_id, $status);
        }
    }

    /*Change status*/
    public function changeStatusFollowupReminder($staff_followup_reminder_id, $status)
    {
        $reminder =    StaffFollowupReminder::query()->where('id', $staff_followup_reminder_id)->first();
        $reminder->update([
            'isactive' => $status,
            'completed_date' => ($status == 0) ? standard_date_format(getTodayDate()) : $reminder->completed_date
        ]);
    }
    /**
     * POSTING WEEKLY MISSING TARGET FOLLOWUPS - JOBS----------
     */

    /*Post weekly missing target followups*/
    public function postWeeklyMissingTargetFollowupReminders($user_id = null)
    {
        DB::transaction(function() use($user_id){
            $users_id_arrays =  $this->getStaffForReminderArray();
            $users_id_arrays = ($user_id) ? [$user_id] : $users_id_arrays;
            $config = $this->staffEmployerConfig();
            $next_date = $this->followup_reminder_next_date ? $this->followup_reminder_next_date :  (($config->followup_reminder_next_date) ? ($config->followup_reminder_next_date) : getTodayDate());
            $current_week_dates = $this->getOnGoingWeekForReminder($next_date);
            $start_date = $current_week_dates['week_start_date'];
            $end_date = $current_week_dates['week_end_date'];

            $save_data = [
                'start_date' => $start_date,
                'end_date' => $end_date
            ];

            $day_nextdate = Carbon::parse($next_date)->dayOfWeek;
            /*check if is sunday or next date is less than today*/
            if ($day_nextdate == Carbon::SUNDAY || comparable_date_format($next_date) <= comparable_date_format(getTodayDate())) {

                foreach ($users_id_arrays as $user_id){
//                    $weekly_target = $this->getWeeklyFollowupTarget($user_id);
                    $days_worked = $this->getNoOfDaysWorkedForReminder($user_id, $start_date, $end_date);
//                    $followups_made = $this->getNoOfFollowUpsMadeOnDateRange($user_id, $start_date, $end_date);
//                    dd($followups_made);
//                    if($followups_made < $weekly_target && $days_worked > 0){
                    if($days_worked > 0){
                        $save_data['user_id'] = $user_id;
                        $save_data['isnew'] = 1;
                        $this->saveFollowupReminder($save_data);
                    }
                }

                /*Update next date*///todo need to comment this
//                $this->updateNextDateForPostingMissingTarget($end_date);
            }
        });
    }



    /*Main posting and updating weekly missing target*/
    public function mainPostAndUpdateWeeklyMissingTargetFollowups($user_id = null, $next_run_date = null)
    {
        $config = $this->staffEmployerConfig();
        /*find next run date*/
        if($next_run_date){
            $next_date = $next_run_date;
        }else{
            $next_date = ($config->followup_reminder_next_date) ? ($config->followup_reminder_next_date) :Carbon::today()->endOfWeek();

        }
        $this->followup_reminder_next_date = $next_date;
        $new_start_date = '2021-04-25';
        $next_date = (comparable_date_format($next_date) > comparable_date_format($new_start_date)) ? $next_date : $new_start_date;
        $this->followup_reminder_next_date = $next_date;
        if( (comparable_date_format($next_date) < comparable_date_format($new_start_date))){

            DB::table('staff_employer_configs')->update(['followup_reminder_next_date' => standard_date_format($new_start_date)]);
        }

        if ( comparable_date_format($next_date) <= comparable_date_format(getTodayDate())) {
            /*Update In Active reminders last 1 month*/
//        $this->updateAllInActiveRemindersWithCutoffDate();

            /*update all active reminders*/
//            $this->updateAllActiveReminders($user_id);

            /*Update parent reminders active with child inactive*/
//            $this->updateActiveStatusParentReminders($user_id);
//
//            /*Post new target missing*/

            $this->postWeeklyMissingTargetFollowupReminders($user_id);
//
//            /*Reconcile all followup reminders for each user*/
//            $this->reconcileMissingTargetUpToDate($user_id);
        }

    }

    /*update next week start of week for running posting missig target*/
    public function updateNextDateForPostingMissingTarget($end_date)
    {
        $next_start_of_week = Carbon::parse($end_date)->endOfWeek()->addWeek();
        $config = $this->staffEmployerConfig();
        if($config) {
            DB::table('staff_employer_configs')->where('id', $config->id)->update([
                'followup_reminder_next_date' => standard_date_format($next_start_of_week)
            ]);
        }
    }


    /*Update is notifiec after posting email*/
    public function updateIsNotifiedAfterPostingEmail($followup_reminder_id)
    {
        $reminder = StaffFollowupReminder::query()->find($followup_reminder_id);
        $reminder->update(['isnotified' => 1]);
    }
    /*End of weekly target missing follow ups------*/



    /**
     *
     * EMAIL NOTIFICATION---- start-----
     */

    /*Start and end dates for all active reminders*/
    public function getStartEndDatesActiveRemindersByUser($user_id)
    {
        $latest = StaffFollowupReminder::query()->where('user_id', $user_id)->where('isactive',1)->orderBy('week_end_date', 'desc')->first();
        $oldest = StaffFollowupReminder::query()->where('user_id', $user_id)->where('isactive',1)->orderBy('week_start_date', 'asc')->first();

        $end_date = ($latest) ? $latest->week_end_date : null;
        $start_date = ($oldest) ? $oldest->week_start_date : null;

        return [
            'start_date' => $start_date,
            'end_date' => $end_date
        ];

    }

    /*Email notification data for reminder*/
    public function postEmailNotificationForReminder($followup_reminder_id)
    {
        $latest = StaffFollowupReminder::query()->where('id', $followup_reminder_id)->first();
        $elevation_level = $latest->elevation_level;
        $elevation_level_in_word = $this->numberToWordForReminder($elevation_level);
        $user_id = $latest->user_id;
        $user = User::query()->find($user_id);
        $dates_range = $this->getStartEndDatesActiveRemindersByUser($user_id);
        $start_date = $dates_range['start_date'];
        $end_date = $dates_range['end_date'];
        $subject = 'Missing Weekly Follow up Target Alert - ' .$elevation_level_in_word . ' Reminder (Sent with high Important)';
        $email_data = [
            'subject' => $subject,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'elevation_level' => $elevation_level,
            'staff_name' => $user->name,

        ];

        if(env('TESTING_MODE') == 0){
            $user->notify(new StaffMissingFollowupReminder($email_data));

        }

        /*update isnotified*///
        $this->updateIsNotifiedAfterPostingEmail($followup_reminder_id);
    }

    /*number to word th*/
    public function numberToWordForReminder($elevation_level)
    {
        $word = '';
        $array = ['1' => 'First','2' => 'Second', '3' => 'Third', '4' => 'Fourth', '5' =>'Fifth', '6' => 'Sixth', '7' => 'Seventh', '8' => 'Eighth', '9' => 'Nineth', '10' => 'Tenth'];
        if(isset($array[$elevation_level])){
            $word =   $array[$elevation_level];
        }else{
            $word = $elevation_level;
        }
        return $word;
    }

    /*---end date ---*/



    /**
     * Get for DataTable
     */
    /*Get user missing target reminders*/
    public function getUserMissingTargetRemindersForDt()
    {
        return StaffFollowupReminder::query()->select(
            'staff_followup_reminders.user_id as user_id',
            DB::raw("concat_ws(' ', u.firstname, u.lastname) as staff_name "),
            DB::raw("sum(staff_followup_reminders.weekly_missing_target - staff_followup_reminders.compensation_followups) as total_missing_target"),
            DB::raw("min(staff_followup_reminders.week_start_date) as week_start_date "),
            DB::raw("max(staff_followup_reminders.week_end_date) as week_end_date"),
            DB::raw("max(staff_followup_reminders.elevation_level) as no_of_weeks"),
            DB::raw(" sum(staff_followup_reminders.days_worked) as total_days_worked "),
            DB::raw(" max(staff_followup_reminders.elevation_level) as elevation_level ")
        )
            ->join('users as u', 'u.id', 'staff_followup_reminders.user_id')
            ->where('staff_followup_reminders.isactive', 1)
            ->groupBy('staff_followup_reminders.user_id', DB::raw("concat_ws(' ', u.firstname, u.lastname) "));

    }


    //TODO - Pending compensation for two weeks of compensation
    /*
     * Get no of weeks and no of worked cap 10 on those two weeks , get actual follow ups target and and get no of follow ups made in those two weeks these if does not have next reminder
     * */


    /*Reconcile to last reminders for each user*/
    public function reconcileMissingTargetUpToDate($user_id = null)
    {
        $config = $this->staffEmployerConfig();
        $next_target_date = ($this->followup_reminder_next_date) ? $this->followup_reminder_next_date : $config->followup_reminder_next_date;
        $today = getTodayDate();
        $last_end_date = $next_target_date;
        if(comparable_date_format($today) < comparable_date_format($next_target_date))
        {
            $last_end_date = Carbon::parse($next_target_date)->subDays(7);
        }

        if($user_id){
            $user_with_reminders = $this->getUserMissingTargetRemindersForDt()->where('user_id', $user_id);
        }else{
            $user_with_reminders = $this->getUserMissingTargetRemindersForDt();
        }

        foreach($user_with_reminders->get() as $user){
            $user_id = $user->user_id;
            $user_oldest_reminder = StaffFollowupReminder::query()->where('user_id', $user_id)->orderBy('elevation_level', 'asc')->first();
            $week_start_date = $user_oldest_reminder->week_start_date;
            $week_start_date_end_week = Carbon::parse($week_start_date)->endOfWeek();
            $last_end_date_end_week = Carbon::parse($last_end_date)->endOfWeek();
//            $missing_target = $this->getFollowupMissingTargetOnDateRange($user_id, $week_start_date, $last_end_date);
            $actual_followup_target = 0;$no_of_followups_made= 0;

            /*Loop for each week to get extras*/
            while(comparable_date_format($week_start_date_end_week) <= comparable_date_format($last_end_date_end_week)) {
                $date_ranges = $this->getOnGoingWeekForReminder($week_start_date_end_week);
                $actual_followup_target = $actual_followup_target +  $this->getActualWeeklyTargetByUser($user_id, $date_ranges['week_start_date'], $date_ranges['week_end_date']);
                $no_of_followups_made = $no_of_followups_made +  $this->getNoOfFollowUpsMadeOnDateRange($user_id, $date_ranges['week_start_date'], $date_ranges['week_end_date']);
                $week_start_date_end_week = Carbon::parse($week_start_date_end_week)->addWeek()->endOfWeek();
            }

            $missing_target = $actual_followup_target  - $no_of_followups_made;
            $missing_target = ($missing_target > 0) ? $missing_target : 0;
            $user_oldest_reminder->update([
                'total_missing_target' => $missing_target
            ]);
        }
    }


    /*Dispatch Followup Reminder For staff*/
    public function dispatchFollowupReminderForStaffJob()
    {
        /*tempo rectification*/
        $this->rectifyFollowupReminderDate();
        $config = $this->staffEmployerConfig();
        $next_date = $config->followup_reminder_next_date;
        if (comparable_date_format($next_date) <= comparable_date_format(getTodayDate())) {
            $users_id_arrays =  $this->getStaffForReminderArray();
            User::query()->whereIn('id', $users_id_arrays)->select(['id'])->chunk(3, function ($users) use($next_date){
                if (isset($users)) {
                    dispatch(new PostStaffEmployerFollowupReminderJob($users, $next_date));
                }
            });
            /*update new date*/
            $config = $this->staffEmployerConfig();
            $next_date = $config->followup_reminder_next_date;

            $current_week_dates = $this->getOnGoingWeekForReminder($next_date);
            $end_date = $current_week_dates['week_end_date'];
            $this->updateNextDateForPostingMissingTarget($end_date);
        }
    }


    /*Rectify followup eminder date*/
    public function rectifyFollowupReminderDate()
    {
        $new_start_date = '2021-05-02';
        if(comparable_date_format($new_start_date) > comparable_date_format(getTodayDate())){
            DB::table('staff_employer_configs')->update([
                'followup_reminder_next_date' =>  $new_start_date
            ]);
        }

    }


}
