<?php

namespace App\Repositories\Backend\Operation\Compliance\Member\Traits;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use App\Models\Operation\Compliance\Member\StaffFollowupReminder;
use App\Notifications\Backend\Employer\StaffEmployer\StaffEmployerBonusPerformance;
use App\Notifications\Backend\Employer\StaffEmployer\StaffEmployerVerificationPerformance;
use App\Notifications\Backend\Employer\StaffEmployerCollectionPerformance;
use App\Notifications\Backend\Employer\StaffMissingFollowupReminder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
trait StaffEmployerComplimentTrait {



    /*Notification for top staff collection performance - email*/
    public function staffCollectionPerformanceCongratEmail($isintern = 0)
    {
        $today = Carbon::now();
        $month = $today->subMonthNoOverflow(2);// get prev month since job will be running on 1st of new month
        $month_formatted = Carbon::parse($month)->format('M Y');
        $check_if_there_pending = $this->getQueryToGetStaffForCongratulationNote($month, $isintern)->whereRaw("(((collection_current) / (receivable_current)) * 100) >= 90")->count();
        if($check_if_there_pending > 0) {
            $first_position = $this->getQueryToGetStaffForCongratulationNote($month, $isintern)->whereRaw("(((collection_current) / (receivable_current)) * 100) >= 90")->orderBy('current_performance', 'desc')->first();
            /*notify first position*/
            if (isset($first_position->user_id)) {
                $first_user = User::query()->find($first_position->user_id);

                $first_user->notify(new StaffEmployerCollectionPerformance($first_user->name, $month_formatted, $first_position->current_performance, 1));
                /*update flag*/
                $this->updateCongratulationFlag($first_position->performance_id);
                /*Above 90s*/
                $above_90s = $this->getQueryToGetStaffForCongratulationNote($month, $isintern)->whereRaw("(((collection_current) / (receivable_current)) * 100) >= 90")->where('user_id', '<>', $first_position->user_id)->get();
                foreach ($above_90s as $staff) {
                    $user = User::query()->find($staff->user_id);
                    $user->notify(new StaffEmployerCollectionPerformance($user->name, $month_formatted, $staff->current_performance, 2));
                    /*update flag*/
                    $this->updateCongratulationFlag($staff->performance_id);
                }

            }
        }

    }



    /*Get staff for congratulation*/
    public function getQueryToGetStaffForCongratulationNote($month, $isintern = 0){
        $month_parsed = Carbon::parse($month);
        return  $this->queryForStaffEmployerCollectionPerformanceTable()->where('iscongratulated', 0)->where('staff_employer_collection_performances.isintern', $isintern)->whereYear('collection_month','=', $month_parsed->format('Y'))->whereMonth('collection_month', '=',$month_parsed->format('m'))->where('staff_employer_collection_performances.receivable_current', '>', 0);

    }


    /*Update congratulation flag for staff performance*/
    public function updateCongratulationFlag($performance_id)
    {
        DB::table('staff_employer_collection_performances')->where('id', $performance_id)->update([
            'iscongratulated' => 1
        ]);
    }





    /**
     *
     * COMPLIMENT NOTIFICATION FOR DORMANT/BONUS 100% ACHIEVEMENT
     */


    /*Notification for top staff in 100% online verification - email*/
    public function staffVerificationCongratEmail()
    {
        $today = Carbon::now();
        $month = $today->subMonthNoOverflow(1);// get prev month since job will be running on 1st of new month

        $achieved_staffs = $this->getQueryToGetStaffForVerificationCongratsNote($month)->get();
        foreach ($achieved_staffs as $staff) {
            $user = User::query()->find($staff->user_id);
            $prev_month = Carbon::now()->subMonthNoOverflow(2);
            $check_if_new_achiever =$this->checkIfPreviousWasUnderAchieverVerification($user->id, $prev_month);
            if($check_if_new_achiever == true){
                $user->notify(new StaffEmployerVerificationPerformance($user->name));
                /*update flag*/
                $this->updateVerificationCongratulationFlag($staff->performance_id);

            }

        }
    }



    /*Get staff for verification 100% congrats email*/
    public function getQueryToGetStaffForVerificationCongratsNote($month){
        $month_parsed = Carbon::parse($month);
        return  $this->queryForStaffEmployerCollectionPerformanceTable()->where('staff_employer_collection_performances.isverification_congrated', 0)->whereYear('collection_month','=', $month_parsed->format('Y'))->whereMonth('collection_month','=', $month_parsed->format('m'))->where('staff_employer_collection_performances.percent_online_employers', 100);

    }

    /*Update congratulation flag for staff performance*/
    public function updateVerificationCongratulationFlag($performance_id)
    {
        DB::table('staff_employer_collection_performances')->where('id', $performance_id)->update([
            'isverification_congrated' => 1
        ]);
    }

    /*Check if previous was under achiever for verification*/
    public function checkIfPreviousWasUnderAchieverVerification($user_id, $prev_month)
    {
        $month_parsed = Carbon::parse($prev_month);
        $check = DB::table('staff_employer_collection_performances')->where('staff_employer_collection_performances.user_id', $user_id)->whereYear('collection_month','=',$month_parsed->format('Y'))->whereMonth('collection_month','=', $month_parsed->format('m'))->where('staff_employer_collection_performances.isverification_congrated', 1)->where('staff_employer_collection_performances.percent_online_employers', 100)->count();
        if($check > 0){
            return false;
        }else{
            return true;
        }
    }

    /*End of verification 100%*/


    /**
     *
     * BONUS/DORMANT NOTIFICATION ACHIEVING
     */


    /*Notification for top staff in 100% dormant/bonus - email*/
    public function staffBonusCongratEmail()
    {

        $achieved_staffs = $this->getQueryToGetStaffForBonusCongratsNote()->get();
        foreach ($achieved_staffs as $staff) {
            $user = User::query()->find($staff->user_id);
            $user->notify(new StaffEmployerBonusPerformance($user->name));
            /*update flag*/
            $this->updateBonusAchiever();



        }
    }


    /*Get staff for Bonus 100% congrats email*/
    public function getQueryToGetStaffForBonusCongratsNote(){
        $bonus_allocation = StaffEmployerAllocation::query()->where('category', 3)->first();
        $users_already_achieved = ($bonus_allocation) ? json_decode($bonus_allocation->bonus_achiever) : [];
        $users_already_achieved = ($users_already_achieved) ? $users_already_achieved : [];

        return  DB::table('staff_employers_dormant_performance')->whereRaw("allocated_employers = activated_employers")->whereNotIn('user_id',$users_already_achieved);

    }

    /*Update congratulation flag for staff bonus performance*/
    public function updateBonusAchiever()
    {
        $bonus_allocation = StaffEmployerAllocation::query()->where('category', 3)->first();
        if($bonus_allocation){
            $users_achieved = $this->getQueryToGetStaffForBonusCongratsNote()->get()->pluck('user_id')->toArray();
                 $bonus_allocation->update([
                'bonus_achiever' => json_encode($users_achieved),
            ]);
        }
    }



    /*End of verification 100%*/



}
