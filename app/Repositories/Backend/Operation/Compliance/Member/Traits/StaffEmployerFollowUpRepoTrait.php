<?php

namespace App\Repositories\Backend\Operation\Compliance\Member\Traits;

use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Jobs\Compliance\StaffEmployerAllocation\StaffEmployerAllocationJob;
use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use App\Models\Sysdef\CodeValue;
use App\Notifications\Backend\Employer\EmployerFollowUpEmailReminder;
use App\Notifications\Backend\Employer\StaffEmployerCollectionPerformance;
use App\Notifications\Backend\Employer\StaffEmployerFollowUpReminder;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use App\Services\Compliance\StaffEmployerAllocation\AllocateBonusEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateLargeEmployers;

use App\Services\Compliance\StaffEmployerAllocation\AllocateNonLargeEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateTreasuryEmployers;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

trait StaffEmployerFollowUpRepoTrait {

    use StaffFollowupReminderRepoTrait;
    /*FOLLOW UPS*/

    /*create follow up*/
    public function storeFollowUp(array $input)
    {
        // dd($input);
        DB::transaction(function () use($input) {
            $staff_employer_id = $input['staff_employer_id'];
            $is_commitment = false;
            if (isset($input['commitment_attend'])) {
                if ($input['commitment_attend'] != 0) {
                    $is_commitment = true;
                    /* check if attended */
                    $attended = $this->attendedCommitment($input['commitment_attend']);
                    if ($attended) {
                        throw new GeneralException('Please attend the commitment first!');
                    }
                }
            }
//            $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
//            $isbonus = $staff_employer->isbonus;
            $feedback_cv = (new CodeValueRepository())->findByReference($input['feedback_cv_id']);
            $followup_cv = (new CodeValueRepository())->findByReference($input['follow_up_type_cv_id']);
            $assigned_reviewer =  $this->visitationReviewerRoundRobin();
            $follow_up_id =   DB::table('staff_employer_follow_ups')->insertGetId([
                'remark' => $input['remark'],
                'feedback_cv_id' => $feedback_cv->id,
                'follow_up_type_cv_id' => $followup_cv->id,
                'contact' => $input['contact'],
                'contact_person' => $input['contact_person'],
                'user_id' => access()->id(),
                'staff_employer_id' => $input['staff_employer_id'],
                'date_of_follow_up' => $input['date_of_follow_up'],
                'date_of_reminder' => $input['reminder_date'],
                'needs_review' => ($followup_cv->reference == 'FUVSIT' ) ? 1 : 0,
                'reminder_email' => isset($input['reminder_email']) ? $input['reminder_email'] : null,
                'created_at' => Carbon::now(),
                'assigned_reviewer'=> $assigned_reviewer,
                'employer_id' => $input['employer_id'],
                'is_commitment' => $is_commitment,
                'commitment_request_id' => $input['commitment_attend']
            ]);


            /*Save the document */
            $document_id = $this->getDocumentIdByFollowUpType($followup_cv);
            if(isset($document_id)){
                $input['document_id'] = $document_id;
                $this->saveDocuments($input, $input['employer_id'], $follow_up_id);
            }


            /*Update for staff employer*/
            if(isset($staff_employer_id)){

                /*update as attanded commmitment*/
                if (isset($input['commitment_attend'])) {
                    if ($input['commitment_attend'] != -1) {
                        $this->updateEmployerAsAttendedFollowUpCommitment($input['staff_employer_id'], $input['commitment_attend']);
                    }
                }else{
                    /*update as attanded*/
                    $this->updateEmployerAsAttendedFollowUp($input['staff_employer_id']);
                }
                /*update no of follow ups*/
                $this->updateNoOfFollowUps($staff_employer_id);
                // die;

            }

        });
    }

    /*update follow up*/
    public function updateFollowUp( array $input)
    {

        DB::transaction(function () use( $input) {
            $feedback_cv = (new CodeValueRepository())->findByReference($input['feedback_cv_id']);
            $followup_cv = (new CodeValueRepository())->findByReference($input['follow_up_type_cv_id']);
            $is_commitment = false;
            if (isset($input['commitment_attend'])) {
                $is_commitment = true;
                /* check if attended */
                $attended = $this->attendedCommitment($input['commitment_attend']);
                if ($attended) {
                    throw new GeneralException('Please attend the commitment first!');
                }
            }

            DB::table('staff_employer_follow_ups')->where('id', $input['follow_up_id'])->update([
                'remark' => $input['remark'],
                'feedback_cv_id' => $feedback_cv->id,
                'follow_up_type_cv_id' => $followup_cv->id,
                'contact' => $input['contact'],
                'contact_person' => $input['contact_person'],
                'date_of_follow_up' => $input['date_of_follow_up'],
                'date_of_reminder' => $input['reminder_date'],
                'needs_review' => ($followup_cv->reference == 'FUVSIT' ) ? 1 : 0,
                'reminder_email' => isset($input['reminder_email']) ? $input['reminder_email'] : null,
                'updated_at' => Carbon::now(),
                'needs_correction' => (($input['needs_correction']) == 1) ? 2 : $input['needs_correction'],
                'is_commitment' => $is_commitment,
                'commitment_request_id' => $input['commitment_attend']
            ]);


            /*Save the document - visit form*/
            if(isset($input['document_file'])){
                $document_id = $this->getDocumentIdByFollowUpType($followup_cv);
                if(isset($document_id)){
                    $input['document_id'] = $document_id;
                    $this->saveDocuments($input, $input['employer_id'], $input['follow_up_id']);
                }

            }

        });
    }

    public function getFollowupTypesReferencesByCategory($category)
    {
        switch($category){
            case 5:
            return ['FUPHONC'];
            break;
        }
    }


    /*Get document id by follow type*/
    public function getDocumentIdByFollowUpType($follow_type)
    {
        $document_id = null;
        switch ($follow_type->reference)
        {
            case 'FUVSIT':
            $document_id = 73;
            break;

            case 'FULETT':
            $document_id = 82;
            break;
        }

        return $document_id;
    }


    /* Review follow ups for bonus employers*/
    public function reviewFollowUp($follow_up_id, array $input)
    {
        DB::transaction(function () use( $follow_up_id, $input) {
            if($input['review_action']  == 1){
                /*verified*/
                DB::table('staff_employer_follow_ups')->where('id', $follow_up_id)->update([
                    'reviewed_by' => access()->id(),
                    'isreviewed' => 1,
                    'reviewed_date' => Carbon::now(),
                    'review' => $input['comment'],
                ]);
            }elseif($input['review_action']  == 2){
                /*Reverse for correction*/
                DB::table('staff_employer_follow_ups')->where('id', $follow_up_id)->update([
                    'reverse_reason' => $input['comment'],
                    'needs_correction' => 1
                ]);
            }

        });
    }



    public function deleteFollowUp($follow_up_id)
    {
        DB::transaction(function () use( $follow_up_id) {
            $follow_up = StaffEmployerFollowUp::query()->find($follow_up_id);
            $staff_employer_id = $follow_up->staff_employer_id;
            StaffEmployerFollowUp::query()->where('id', $follow_up_id)->delete();

            /*For Staff employer relationship*/
            if(isset($staff_employer_id)){
                /*update employer as attended*/
                $this->updateEmployerAsAttendedFollowUp($staff_employer_id);

                /*update no of follow ups*/
                $this->updateNoOfFollowUps($staff_employer_id);
            }

        });
    }

    /*Update no of followps on staff employer when create / delete follow up*/
    public function updateNoOfFollowUps($staff_employer_id)
    {
        $no_of_follow_ups = StaffEmployerFollowUp::query()->where('staff_employer_id', $staff_employer_id)->count();
        DB::table('staff_employer')->where('id', $staff_employer_id)->update([
            'no_of_follow_ups' => $no_of_follow_ups
        ]);
    }


    /*Update employers as attended when adding or deleting followup*/
    public function updateEmployerAsAttendedFollowUp($staff_employer_id)
    {
        $no_of_follow_ups = $this->getNoOfFollowUps($staff_employer_id);
        if($no_of_follow_ups > 0){
            /*attended*/
            $this->updateAttendanceStatus($staff_employer_id, 1);
        }else{
            /*un attended*/
            $this->updateAttendanceStatus($staff_employer_id, 0);
        }
    }


    public function updateEmployerAsAttendedFollowUpCommitment($staff_employer_id, $commitment_request_id){
     $no_of_follow_ups = $this->getNoOfFollowUps($staff_employer_id);
     $no_of_follow_up_commitments = $this->getNoOfFollowUpCommitments($staff_employer_id, $commitment_request_id);
     if($no_of_follow_ups > 0){
        /*attended*/
        $this->updateAttendanceStatus($staff_employer_id, 1);
    }else{
        /*un attended*/
        $this->updateAttendanceStatus($staff_employer_id, 0);
    }

    if ($no_of_follow_up_commitments < 2) {
        $this->updateStaffEmployerCommitmentFollowup($staff_employer_id, $commitment_request_id);
    }
}


/*Get staff employer follow up for datatable*/
public function getStaffEmployerFollowupsForDt($staff_employer_id)
{
    return StaffEmployerFollowUp::query()->where('staff_employer_id', $staff_employer_id)->orderBy('date_of_follow_up', 'desc');
}

/*Get staff employer follow ups for DT*/
public function getStaffEmployerFollowupsByEmployerForDt($employer_id)
{
    return $this->queryStaffEmployerFollowUps()->where('staff_employer_follow_ups.employer_id', $employer_id)->orderBy('date_of_follow_up', 'desc');
}

/*Query general for staff employer follow ups*/
public function queryStaffEmployerFollowUps()
{
    return StaffEmployerFollowUp::query()->select(
        'staff_employer_follow_ups.id as follow_up_id',
        'employers.name as employer_name',
        'staff_employer_follow_ups.remark as remark',
        'feedbacks.name as feedback',
        'follow_up_types.name as follow_up_type',
        'staff_employer_follow_ups.contact as contact',
        'staff_employer_follow_ups.contact_person as contact_person',
        'staff_employer_follow_ups.date_of_follow_up as date_of_follow_up',
        'staff_employer_follow_ups.reverse_reason as reverse_reason',
        DB::raw(" concat_ws(' ', users.firstname, users.middlename, users.lastname) as staff"),
        DB::raw(" concat_ws(' ', reviewers.firstname, users.middlename, reviewers.lastname) as assigned_reviewer")
    )
    ->join('staff_employer','staff_employer.id', 'staff_employer_follow_ups.staff_employer_id')
    ->join('employers', 'employers.id', 'staff_employer.employer_id')
    ->join('users','staff_employer_follow_ups.user_id', 'users.id')
    ->leftJoin('users as reviewers','staff_employer_follow_ups.assigned_reviewer', 'reviewers.id')
    ->join('code_values as feedbacks', 'feedbacks.id', 'staff_employer_follow_ups.feedback_cv_id')
    ->join('code_values as follow_up_types', 'follow_up_types.id', 'staff_employer_follow_ups.follow_up_type_cv_id');

}

/*Get pending follow ups for review for datatable*/
public function getPendingFollowUpsForReviewForDt()
{
    return StaffEmployerFollowUp::query()->select(
        'staff_employer_follow_ups.id as follow_up_id',
        'employers.name as employer_name',
        'staff_employer_follow_ups.remark as remark',
        'feedbacks.name as feedback',
        'follow_up_types.name as follow_up_type',
        'staff_employer_follow_ups.contact as contact',
        'staff_employer_follow_ups.contact_person as contact_person',
        'staff_employer_follow_ups.date_of_follow_up as date_of_follow_up',
        'staff_employer_follow_ups.reverse_reason as reverse_reason',
        DB::raw(" concat_ws(' ', users.firstname, users.middlename, users.lastname) as staff"),
        DB::raw(" concat_ws(' ', reviewers.firstname, users.middlename, reviewers.lastname) as assigned_reviewer")
    )
    ->join('staff_employer','staff_employer.id', 'staff_employer_follow_ups.staff_employer_id')
    ->join('employers', 'employers.id', 'staff_employer.employer_id')
    ->join('users','staff_employer_follow_ups.user_id', 'users.id')
    ->leftJoin('users as reviewers','staff_employer_follow_ups.assigned_reviewer', 'reviewers.id')
    ->join('code_values as feedbacks', 'feedbacks.id', 'staff_employer_follow_ups.feedback_cv_id')
    ->join('code_values as follow_up_types', 'follow_up_types.id', 'staff_employer_follow_ups.follow_up_type_cv_id')
    ->where('staff_employer.isactive', 1)->where('staff_employer_follow_ups.isreviewed', 0)->where('staff_employer_follow_ups.needs_review',1)
    ->orderBy('staff_employer_follow_ups.date_of_follow_up');
}

/*end follow ups*/


/*Get no of follow ups*/
public function getNoOfFollowUps($staff_employer_id)
{
    $no_of_follow_ups = DB::table('staff_employer_follow_ups')->where('staff_employer_id', $staff_employer_id)->count();
    return $no_of_follow_ups;
}

public function getNoOfFollowUpCommitments($staff_employer_id, $commitment_request_id){
    $no_of_follow_ups = DB::table('staff_employer_follow_ups')->where('commitment_request_id', $commitment_request_id)->where('staff_employer_id', $staff_employer_id)->where('is_commitment', true)->count();
    return $no_of_follow_ups;
}


/*Get no of follow ups for allocated employers - for active and current employers allocation*/
public function getNoOfFollowUpsForAllocatedEmployers($user_id,$isbonus)
{
    $no_of_follow_ups = StaffEmployerFollowUp::query()
    ->join('staff_employer','staff_employer.id', 'staff_employer_follow_ups.staff_employer_id')
    ->where('isactive', 1)->where('staff_employer.user_id', $user_id)->where('isbonus', $isbonus)->count();

    return $no_of_follow_ups;
}


/*Send Reminders*/
public function sendFollowUpReminders()
{
    $follow_up_ids =  StaffEmployerFollowUp::query()->where('isreminded', 0)->whereDate('date_of_reminder','<=', standard_date_format(Carbon::now()))->whereNotNull('date_of_reminder')->whereDate('date_of_reminder','>=', '2020-01-01')->select('id')->get();

    foreach ($follow_up_ids as $follow_up_id)
    {
        $staff_employer_follow_up = StaffEmployerFollowUp::query()->find($follow_up_id->id);
        $feedback = (new CodeValueRepository())->find($staff_employer_follow_up->feedback_cv_id);
        $staff_employer = DB::table('staff_employer')->select('employers.name as employer_name', 'staff_employer.user_id as user_id', 'staff_employer.employer_id')
        ->join('employers', 'employers.id', 'staff_employer.employer_id')
        ->where('staff_employer.id', $staff_employer_follow_up->staff_employer_id)->first();
        $user_id = $staff_employer->user_id;
        $user = (new UserRepository())->find($user_id);

        $user->notify(new StaffEmployerFollowUpReminder($user->name, $staff_employer->employer_name, $feedback->name, $staff_employer_follow_up->date_of_follow_up, $user->name));

        /*Notify Substitutes*/
//            if($user->substitutingUser()->count() > 0) {
//                            $substitute = $user->substitutingUser;
//
//                $user->notify(new StaffEmployerFollowUpReminder($substitute->name, $staff_employer->employer_name, $feedback->name, $staff_employer_follow_up->date_of_follow_up, $user->name));
//            }


        /*Notify companies*/
        $employer = Employer::query()->find($staff_employer->employer_id);
        if(isset($employer->email) || isset($staff_employer_follow_up->reminder_email) ){
            $employer->notify(new EmployerFollowUpEmailReminder($staff_employer->employer_name, $feedback->name, $staff_employer_follow_up->date_of_follow_up, $user->name, $staff_employer_follow_up->reminder_email));
        }



        /*update flag*/
        $staff_employer_follow_up->update(['isreminded' => 1]);
    }

}


/*Export employer follow ups for staff employer*/
public function exportEmployerFollowUps($staff_employer_id)
{
    $follow_ups = $this->getStaffEmployerFollowupsForDt($staff_employer_id)->get()->toArray();
    $results = array();
    foreach ($follow_ups as $result) {
        $results[] = (array)$result;
    }

    return Excel::create('staffemployerfollowups'. time(), function($excel) use ($results) {
        $excel->sheet('mySheet', function($sheet) use ($results)
        {
            $sheet->fromArray((array)$results);
        });
    })->download('csv');

}


/*Export staff missing follow ups reminders*/
public function exportStaffMissingFollowUpsReminders()
{
    $follow_ups = $this->getUserMissingTargetRemindersForDt()->get()->toArray();
    $results = array();
    foreach ($follow_ups as $result) {
        $results[] = (array)$result;
    }

    return Excel::create('staffemployerfollowupsReminders'. time(), function($excel) use ($results) {
        $excel->sheet('mySheet', function($sheet) use ($results)
        {
            $sheet->fromArray((array)$results);
        });
    })->download('csv');

}





    /**
     * ROUND ROBIN FOR NEXT USER TO REVIEW VISITATION
     *
     */

    public function visitationReviewerRoundRobin()
    {
        $user_id = null;
        $config = DB::table('staff_employer_configs')->first();

        $current_position_pointer = $config->visitation_reviewer_pointer;

        $users = User::whereHas('permissions', function($query){
            $query->where('permissions.name', 'review_staff_employer_followup');
        })->orderBy('id', 'asc');

        $users_count = $users->count();

        /*round robin starts here*/
        if ($current_position_pointer % $users_count  == 0 || $current_position_pointer > $users_count) {
            $current_position_pointer = 0;
        }

        if ($users_count > 0) {
            /*update pivot current_position_pointer*/
            DB::table('staff_employer_configs')->update([
                'visitation_reviewer_pointer' => $current_position_pointer + 1
            ]);

            /*first user to be assigned*/
            $user = $users->offset($current_position_pointer)->first();
            $user_id = $user->id;
        }

        /*round robin end here*/
        return $user_id;
    }


    /*Get Follow up By User Date*/
    public function getFollowupsByUserDateRange($user_id, $start_date, $end_date)
    {
        return StaffEmployerFollowUp::query()->where('user_id', $user_id)->whereRaw(" date_of_follow_up >= ? and date_of_follow_up <= ?", [ $start_date, $end_date]);
    }

    public function updateStaffEmployerCommitmentFollowup($staff_employer_id){
        $data = array();
        $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
        $data['user_id'] = $staff_employer->user_id;
        $user = User::query()->find($staff_employer->user_id)->first();
        $data['fullname'] = $user->firstname.' '.$user->middlename.' '.$user->lastname;
        $data['no_of_commitments'] = DB::table("staff_employer_commitment_totals")->where('user_id',$staff_employer->user_id)->count();
        $followup = $this->returnCommitmentFollowups($data);

        if (is_null($followup)) {
            $this->updateStaffCommitments($data);
            $followup = $this->returnCommitmentFollowups($data);
            $this->updateStaffCommitments($this->calculateCommitmentFollowup($data,$followup));

        } else {
            $this->updateStaffCommitments($this->calculateCommitmentFollowup($data, $followup));
        }
    }

    public function updateStaffCommitments($data){
       DB::table('staff_employer_commitment_followups')->updateOrInsert(
        ['user_id' => $data['user_id']] , $data );
   }

   public function returnCommitmentFollowups($data){
    $followup = DB::table('staff_employer_commitment_followups')->where('user_id', $data['user_id'])->first();
    return $followup;
}

public function calculateCommitmentFollowup($data, $followup){
    $data['attended_commitments'] = $followup->attended_commitments + 1;
    $data['missed_commitments'] = $data['no_of_commitments'] - $data['attended_commitments'];
    $data['perfomance'] = round(($data['attended_commitments'] / $data['no_of_commitments']) * 100, 2);

    return $data;
}

public function attendedCommitment($commitment_request_id){
    $return = true;
    $commitment = DB::table('portal.commitment_requests')
    ->where('id', $commitment_request_id)
    ->where('status', '!=', 0)
    ->first();

    if (!is_null($commitment)) {
        $return = false;
    }

    return $return;
}
}
