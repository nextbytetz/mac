<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Member\UnregisteredFollowupInspection;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UnregisteredFollowupInspectionRepository extends BaseRepository
{
    const MODEL = UnregisteredFollowupInspection::class;


    //find or throw exception for employer
    public function findOrThrowException($id)
    {
        $unregistered_followup_inspection = $this->query()->find($id);

        if (!is_null($unregistered_followup_inspection)) {
            return $unregistered_followup_inspection;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.unregistered_followup_inspection_not_found'));
    }



    /**
     * @param $columns
     * @param $current determine whether inspection to be retrieved are current active (1) or all inspections (0)
     * @return mixed
     */
    public function getForDataTable($current)
    {
        switch ($current) {
            case 1:
                $return = $this->query()->where("end_date", ">=", Carbon::now())->where('iscancelled', 0)->where('complete_status', 0)->orderBy("start_date", "asc")->select();
                break;
            case 0:
                $return = $this->query()->orderBy("start_date", "asc")->select();
                break;
            default:
                $return = $this->query()->orderBy("start_date", "asc")->select();
        }
        return $return;
    }



    /**
     * @param $input
     * @return mixed
     * Create New
     */
    public function create(array $input){
        return DB::transaction(function () use ($input) {
            $followup_inspection = $this->query()->create([
                'comments' => $input['comments'],
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'user_id' => access()->id(),
            ]);
            $this->syncStaff($followup_inspection, $input);
            return $followup_inspection;
        });
    }



    /**
     * @param $id
     * @param $input
     * @return mixed
     * Update Existing
     */
    public function update($id,$input)
    {
        return DB::transaction(function () use ($id,$input) {
        $followup_inspection =$this->findOrThrowException($id);
         $followup_inspection->update([
            'comments' => $input['comments'],
            'start_date' => $input['start_date'],
            'end_date' => $input['end_date'],
            'user_id' => access()->id(),
        ]);
            $this->syncStaff($followup_inspection, $input);
        return $followup_inspection;
        });
    }


    /**
     * @param Model $inspection
     * @return mixed
     * Complete
     */
    public function complete($id)
    {
        $followup_inspection =$this->findOrThrowException($id);
        return DB::transaction(function () use ($followup_inspection) {
            $this->checkIfIsComplete($followup_inspection->id);
            $input = [
                'complete_status' => 1,
                'complete_user' => access()->id(),
                'complete_date' => Carbon::now(),
            ];
            $followup_inspection->update($input);
        });
    }


    /**
     * @param Model $inspection
     * @return mixed
     * Cancel inspection
     */
    public function cancel($id)
    {
        $followup_inspection =$this->findOrThrowException($id);
        return DB::transaction(function () use ($followup_inspection) {
            $this->checkIfHasActiveTask($followup_inspection->id);
            $input = [
                'iscancelled' => 1,
                'cancel_user' => access()->id(),
                'cancel_date' => Carbon::now(),
            ];
            $followup_inspection->update($input);
        });
    }



    /**
     * @param Model $followup_inspection
     * @param $input
     * Sync Staff assigned to this inspection
     */
    public function syncStaff(Model $followup_inspection, $input){
        $staffs = [];
        $current = $input["staffs"];
        foreach ($current as $perm) {
            if (is_numeric($perm)) {
                array_push($staffs, $perm);
            }
        }
        $followup_inspection->users()->sync($staffs);
    }


    /**
     * @param $id
     * Check if task are assigned when cancelling the inspection
     */
    public function checkIfHasActiveTask($id)
    {
        $inspection = $this->findOrThrowException($id);
        if (count($inspection->unregisteredFollowupInspectionTasks))
        {
            throw new GeneralException(trans('exceptions.backend.compliance.inspection_has_active_task'));
        }
    }

    /**
     * @param $id
     * @throws GeneralException
     * Check if Inspection is complete
     */
    public function checkIfIsComplete($id)
    {
        $inspection = $this->findOrThrowException($id);
        if ($inspection->percentage_completed < 100)
        {
            throw new GeneralException(trans('exceptions.backend.compliance.inspection_not_yet_completed'));
        }
    }


}