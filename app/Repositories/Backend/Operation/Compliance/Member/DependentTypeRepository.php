<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\DependentType;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

class DependentTypeRepository extends  BaseRepository
{

    const MODEL = DependentType::class;

    public function __construct()
    {

    }

//find or throwException for job title
    public function findOrThrowException($id)
    {
        $dependent_type = $this->query()->find($id);

        if (!is_null($dependent_type)) {
            return $dependent_type;
        }
        throw new GeneralException(trans('exceptions.backend.claim.dependent_type_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }


    /*Dependent types which should not duplicate for single employee*/
    public function uniqueDependentTypeIds()
    {
        return [1,4,5];
    }




}