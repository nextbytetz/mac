<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\RegisteredUnregisteredEmployer;
use App\Repositories\BaseRepository;

class RegisteredUnregisteredEmployerRepository extends BaseRepository
{
    const MODEL = RegisteredUnregisteredEmployer::class;

    public function getAllStoredEmployers($q)
    {
        $data['items'] = $this->regexColumnSearch(['name' => $q, 'tin' => $q])->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

}