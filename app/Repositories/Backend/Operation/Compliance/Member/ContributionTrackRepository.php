<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\ContributionTrack;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

class ContributionTrackRepository extends  BaseRepository
{

    const MODEL = ContributionTrack::class;

    public function __construct()
    {

    }

//find or throwexception for employer
    public function findOrThrowException($id)
    {
        $contribution_track = $this->query()->find($id);

        if (!is_null($contribution_track)) {
            return $contribution_track;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.contribution_track_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {
        $this->query()->create(['receipt_code_id'=>$input['id'],'user_id'=>access()->user()->id, 'comments' => $input['comments'],'todo' => $input['todo']]);
    }
    /*
     * update
     */
    public function update($id,$input) {
        $contribution_track = $this->findOrThrowException($id);
        $contribution_track->update([ 'comments' => $input['comments'],'todo' => $input['todo'], 'status' =>
            $input['status']]);
        return $contribution_track;
    }


    /*
     * when updating check ifbelong to user created it
     */
    public function checkIfBelongToUser($id) {
        $contribution_track = $this->findOrThrowException($id);
        if ($contribution_track->user_id == access()->user()->id){
//            it belongs proceed
            return '';
        }
        throw new GeneralException(trans('exceptions.backend.compliance.contribution_track_dont_belong'));
    }






}