<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Backend\Access\UserController;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Member\UnregisteredFollowupInspectionTask;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UnregisteredFollowupInspectionTaskRepository extends BaseRepository
{
    const MODEL = UnregisteredFollowupInspectionTask::class;

    public function findOrThrowException($id)
    {
        $unregistered_followup_inspection_task = $this->query()->find($id);

        if (!is_null($unregistered_followup_inspection_task)) {
            return $unregistered_followup_inspection_task;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.unregistered_followup_inspection_task_not_found'));
    }


    /**
     * @param $input
     * @return mixed
     * Create New
     */
    public function store($input)
    {
        /**/
        return DB::transaction(function () use ($input) {
            $task = $this->query()->create([
                'unregistered_followup_inspection_id' => $input['inspection_id'],
                'name' => $input['name'],
                'deliverable' => $input['deliverable'],
                'unregistered_inspection_profile_id' => ($input['target_employer'] == 1) ? $input['inspection_profile_id'] : null,
                'target_employer' => $input['target_employer'],
            ]);
            /* Link to unreg employers*/
            $this->checkIfProfileHasRequiredNo($task->id,$input);
            $this->linkToUnregisteredEmployers($task->id,$input);
            /* Sync User */
            $this->syncStaff($task, $input);
            return $task;
        });
    }


    /**
     * @param $id
     * @param $input
     * @return mixed
     * Update
     */
    public function update($id,$input)
    {
        /**/
        return DB::transaction(function () use ($id, $input) {
            $task = $this->findOrThrowException($id);
            $task->update([
                'name' => $input['name'],
                'deliverable' => $input['deliverable'],
                'unregistered_inspection_profile_id' => ($input['target_employer'] == 1) ? $input['inspection_profile_id'] : null,
                'target_employer' => $input['target_employer'],
            ]);
            /* Link to unreg employers*/
            $this->linkToUnregisteredEmployers($task->id,$input);
            /* Sync User */
            $this->syncStaff($task, $input);
            return $task;
        });
    }

    /**
     * @param $id
     * delete
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $task = $this->findOrThrowException($id);
            $this->unassignAllStaff($id);
            $this->detachUnregisteredEmployers($task->id);

            $task->delete();

        });
    }

    /**
     * Assigned unregistered employers with assigned staff
     */

    public function syncStaff(Model $task, $input){
        $staffs = [];
        $current = $input["staffs"];
        foreach ($current as $perm) {
            if (is_numeric($perm)) {
                array_push($staffs, $perm);
            }
        }
        $task->users()->sync($staffs);
    }


    public function linkToUnregisteredEmployers($id,$input)
    {
        switch ($input['target_employer'])  {
            case 0: //individual employers
                $this->linkToIndividualUnregisteredEmployers($id,$input);
                break;
            case 1: //profile employers

                $this->linkToUnregisteredEmployersUsingProfile($id,$input);
                break;
        }
    }


    public function linkToIndividualUnregisteredEmployers($id,$input)
    {
        /*detach*/
        $this->detachUnregisteredEmployers($id);
        /*Re sync*/
        foreach ($input as $key => $value) {
            switch ($key)  {
                case 'employer':
                    $unregistered_employers = new UnregisteredEmployerRepository();
                    $unregistered_employers->query()->whereIn('id', $value)->update(['unregistered_followup_inspection_task_id'=> $id]);
                    break;
            }
        }
    }


    public function linkToUnregisteredEmployersUsingProfile($id, $input)
    {
        $profiles = new UnregisteredInspectionProfileRepository();
        $unregistered_employers = new UnregisteredEmployerRepository();
        /*detach*/
        $this->detachUnregisteredEmployers($id);
        /*Re sync*/
        $employer_assigned_count = $unregistered_employers->query()->where('assign_to', '>', 0)->where('unregistered_followup_inspection_task_id', $id)->count();
        $employer_count_limit = $input['employer_count'] - $employer_assigned_count;

        $task = $this->findOrThrowException($id);
        $profile = $task->unregisteredInspectionProfile;
        $query = $profiles->dbToArrayQuery($profile->query);

        if ($employer_count_limit >= 0){
            $unregistered_employers_to_update =       $query->where('assign_to', 0)->limit($employer_count_limit)->get()->pluck('id');
            $unregistered_employers->query()->whereIn('id', $unregistered_employers_to_update)->update(['unregistered_followup_inspection_task_id' => $id]);
        }else{
            /*when number (employer count) is decreased*/
            $unregistered_employers_to_update =       $query->where('is_registered', 0)->limit($employer_count_limit * -1)->get()->pluck('id');
            $unregistered_employers->query()->whereIn('id', $unregistered_employers_to_update)->update(['unregistered_followup_inspection_task_id' => null, 'assign_to' => 0]);
        }

    }



    public function detachUnregisteredEmployers($id)
    {
        /*Detach*/
        $unregistered_employers = new UnregisteredEmployerRepository();
        $unregistered_employer_linked = $unregistered_employers->query()->where('unregistered_followup_inspection_task_id', $id)->where('assign_to',0)->where('is_registered',0)->get();

        if (count($unregistered_employer_linked) > 0)
        {
            $unregistered_employers->query()->where('is_registered',0)->where('unregistered_followup_inspection_task_id', $id)->update(['unregistered_followup_inspection_task_id' => null]);
        }
    }


    /**
     * @param $id
     * Unassign all staff when deleting the task
     */
    public function unassignAllStaff($id)
    {
        $unregistered_employers = new UnregisteredEmployerRepository();
        $unregistered_employers->query()->where('is_registered', 0)->where('unregistered_followup_inspection_task_id', $id)->update(['assign_to'=> 0]);
    }


    /**
     * @param Model $task
     * @return mixed
     * Assign Task
     */
    public function assign($id)
    {
        $output = DB::transaction(function () use ($id) {
            $task = $this->findOrThrowException($id);
            $region = new RegionRepository();
            $unregisteredEmployer = new UnregisteredEmployerRepository();
//            $employerInspectionTask = new EmployerInspectionTaskRepository();
            $regions = $region->query()->select(['id'])->whereHas("unregisteredEmployers", function ($query) use ($id) {
                $query->whereHas("unregisteredFollowupInspectionTask", function ($subQuery) use ($id) {
                    $subQuery->where("unregistered_followup_inspection_task_id", $id)->where('is_registered',0);
                });
            });
            $region_count = count($regions->get()->toArray());
            $user_count = $task->users()->count();
            if ($region_count > $user_count) {
                throw new GeneralException(trans("exceptions.backend.inspection.task.many_regions", ['region' => $region_count, 'staff' => $user_count]));
            }
            $region_weight = [];
            foreach ($regions->get() as $region) {
                $count = $unregisteredEmployer->query()->whereHas("unregisteredFollowupInspectionTask", function ($subQuery) use ($task) {
                    $subQuery->where("unregistered_followup_inspection_task_id", $task->id)->where('is_registered',0);
                })->where("region_id", $region->id)->count();
                $region_weight[$region->id] = $count;
            }
            arsort($region_weight);
            $av = (int) floor($user_count /  $region_count);
            $av_mod = $user_count % $region_count;
            $users = $task->users->pluck("id")->all();
            $user_pointer = 0;
            $global_offset = 0;
            $counter = 0; /* Special loop counter */
            $region_values = array_values($region_weight);
            foreach ($region_weight as $key => $value) {
                if ($av_mod > 0) {
                    $percentDecrease = (floor(($region_values[$counter] - $region_values[$counter + 1]) / $region_values[$counter]) * 100);
                    switch (true) {
                        case $percentDecrease > 0 And $percentDecrease <= 50:
                            $av_loop = $av + 1;
                            $av_mod--;
                            break;
                        default:
                            $av_loop = $av + $av_mod;
                            $av_mod = 0;
                    }
                } else {
                    $av_loop = $av;
                }
                $av_region = (int) floor($value / $av_loop);
                $av_mod_region = $value % $av_loop;
                $offset = $global_offset;
                $loop_user_pointer = $user_pointer;
                for ($x = 1; $x <= $av_loop; $x++) {
                    $unregistered_employers = $unregisteredEmployer->query()->whereHas("unregisteredFollowupInspectionTask", function ($subQuery) use ($task) {
                        $subQuery->where("unregistered_followup_inspection_task_id", $task->id)->where('is_registered',0);
                    })->where("region_id", $key)->orderBy("id")->limit($av_region)->offset($offset)->get()->pluck("id")->all();
                    $unregisteredEmployer->query()->whereIn("id", $unregistered_employers)->where("unregistered_followup_inspection_task_id", $task->id)->update(["assign_to" => $users[$loop_user_pointer], 'date_assigned' => Carbon::parse('now')->format('Y-n-j')]);

                    $loop_user_pointer++;
                    $offset = $offset + $av_region;
                }
                $loop_user_pointer_mod = $user_pointer;
                for ($x = 0; $x < $av_mod_region; $x++) {
                    $unregistered_employers = $unregisteredEmployer->query()->whereHas("unregisteredFollowupInspectionTask", function ($subQuery) use ($task) {
                        $subQuery->where("unregistered_followup_inspection_task_id", $task->id);
                    })->where("region_id", $key)->orderBy("id")->limit(1)->offset($offset)->get()->pluck("id")->all();
                    $unregisteredEmployer->query()->whereIn("id", $unregistered_employers)->where("unregistered_followup_inspection_task_id", $task->id)->where('is_registered',0)->update(["assign_to" => $users[$loop_user_pointer_mod], 'date_assigned' => Carbon::parse('now')->format('Y-n-j')]);
                    $loop_user_pointer_mod++;
                    $offset++;
                }
                //$global_offset = $offset;
                $user_pointer = $loop_user_pointer;
                $counter++;
            }
            return true;
        });
        return $output;
    }

    public function getForDataTable($unregistered_followup_inspection_id)
    {
        return $this->query()
            ->where("unregistered_followup_inspection_id", $unregistered_followup_inspection_id)
            ->select();
    }




    public function getStaffs($q, $inspection_id)
    {
        $name = $q;
        $user = new UserRepository();
        $data['items'] = $user->query()->select(['id', DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(middlename, ''), coalesce(lastname, '')) as staff")])->where(function ($query) use ($name) {
            $query->where("firstname", "like", "%$name%")->orWhere("middlename", "like", "%$name%")->orWhere("lastname", "like", "%$name%")->orWhere("username", "like", "%$name%");
        })->whereHas("unregisteredFollowupInspections", function ($query) use ($inspection_id) {
            $query->where("unregistered_followup_inspections.id", $inspection_id);
        })->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }



    public function checkIfProfileHasRequiredNo($id, $input)
    {
        if ($input['target_employer'] == 1) {
            $task = $this->findOrThrowException($id);
            $profile = $task->unregisteredInspectionProfile;
            if ($profile->employers_count < $input['employer_count']) {
                throw new GeneralException(trans('exceptions.backend.compliance.profile_employer_count_validation'));
            }
        }
    }



    public function getEmployerListForDatatable($id)
    {

        return  $this->query()->find($id)->unregisteredEmployers;
    }



    public function downloadEmployers($id)
    {
        $task = $this->findOrThrowException($id);
        $unregisteredEmployer = new UnregisteredEmployerRepository();
        $user = new UserRepository();
        $user_id = access()->id();
        $space = ['name' => '', 'tin' => ''];
        if ($task->users()->where('user_id', $user_id)->first()) {
            $assignee = [
                ["name" => "Staff : " . $user->find($user_id)->name],
                ["name" => ""],
            ];
            $list = $unregisteredEmployer->query()->select([DB::raw("tin as tin"), 'unregistered_employers.name', DB::raw("regions.name as region")])->where("unregistered_followup_inspection_task_id", $task->id)->where("assign_to", $user_id)->join('regions', 'unregistered_employers.region_id', '=', 'regions.id')->get()->toArray();
            if (count($list)) {
                $heading = ['name' => trans('labels.backend.compliance_menu.inspection.unregistered'), 'tin' => ''];
                # add headers for each column in the CSV download
                array_unshift($list, $heading);
            }

        } else {
            $assignee = [
                ["name" => "Staff : All"],
                ["name" => ""],
            ];
            $list = $unregisteredEmployer->query()->select([DB::raw("tin as tin"), 'unregistered_employers.name', DB::raw("regions.name as region")])->where("unregistered_followup_inspection_task_id", $task->id)->join('regions', 'unregistered_employers.region_id', '=', 'regions.id')->get()->toArray();
            if (count($list)) {
                $heading = ['name' => trans('labels.backend.compliance_menu.inspection.unregistered'), 'tin' => ''];
                # add headers for each column in the CSV download
                array_unshift($list, $heading);
            }

        }
        return $list;
    }




}