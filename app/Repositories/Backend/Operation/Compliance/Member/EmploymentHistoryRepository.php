<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Member\EmploymentHistory;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class EmploymentHistoryRepository extends  BaseRepository
{

    const MODEL = EmploymentHistory::class;
    protected  $employer_id;
    protected  $other_employer;
    protected  $insurance_id;
    protected  $other_insurance;


    public function __construct()
    {

    }

//find or throwexception for job title
    public function findOrThrowException($id)
    {
        $employment_history = $this->query()->find($id);

        if (!is_null($employment_history)) {
            return $employment_history;
        }
        throw new GeneralException(trans('exceptions.backend.claim.employment_history_not_found'));
    }


    /**
     * @param $employee_id
     * @param $input
     * create
     */

    public function create($employee_id,$input) {
        $this->getEmployerFilled($input);
        $this->getInsuranceFilled($input);
        $employment_history = $this->query()->create(['employee_id' => $employee_id, 'job_title_id' => $input['job_title_id'], 'department' => $input['department_name'], 'activity' => $input['activity'], 'from_date' => $input['min_date'], 'to_date' => $input['max_date'], 'employer_id' => $this->employer_id ,'other_employer' => $this->other_employer ,'insurance_id' => $this->insurance_id, 'other_insurance' => $this->other_insurance ]);
    }

    /**
     * @param $id
     * @param $input
     * update
     */
    public function update($id,$input) {

        $this->getEmployerFilled($input);
        $this->getInsuranceFilled($input);
        $employment_history = $this->findOrThrowException($id);
        $employment_history->update([ 'job_title_id' => $input['job_title_id'], 'department' => $input['department'], 'activity' => $input['activity'], 'from_date' => $input['min_date'], 'to_date' => $input['max_date'], 'employer_id' => $this->employer_id ,'other_employer' => $this->other_employer ,'insurance_id' => $this->insurance_id, 'other_insurance' => $this->other_insurance ]);
        return $employment_history;
    }




    /*
     * get if employer which is filled
     */
    public function getEmployerFilled($input) {
        if ($input['other_employer_checkbox'] == 1 ){
            $this->other_employer =  $input['other_employer'];
            $this->employer_id = null;
        }else{
            $this->other_employer =  null;
            $this->employer_id = $input['employer_id'];
        }


    }

    /*
      * get if insurance which is filled
      */
    public function getInsuranceFilled($input) {
        if ($input['other_insurance_checkbox'] == 1 ){
            $this->other_insurance =  $input['other_insurance'];
            $this->insurance_id = null;
        }else{
            $this->other_insurance =  null;
            $this->insurance_id = $input['insurance_id'];
        }

    }



}