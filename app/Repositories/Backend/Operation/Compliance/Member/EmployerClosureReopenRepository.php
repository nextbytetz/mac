<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use Adldap\Models\Model;
use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Operation\Compliance\Member\EmployerClosureReopen;
use App\Repositories\Backend\Api\ClosedBusinessRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EmployerClosureReopenRepository extends BaseRepository
{
    use AttachmentHandler, FileHandler;


    const MODEL = EmployerClosureReopen::class;

    protected $employer;

    protected $closed;

    public function __construct()
    {
        parent::__construct();
        $this->employer = new EmployerRepository();
        $this->closed = new ClosedBusinessRepository();

    }



    /*post open business - Store new*/
    public function postOpenBusiness(array $input)
    {
        return DB::transaction(function () use ($input) {
            $employer = $this->employer->query()->find($input['employer_id']);
            $closure = $employer->getRecentEmployerClosure();
            $closure_open = $this->query()->create([
                'employer_closure_id' =>  ($closure)  ? $closure->id : null,
                'employer_id' =>  $employer->id,
                'open_date' => standard_date_format($input['open_date']),
                'reason' => $input['open_reason'],
                'user_id' => access()->id(),
            ]);


            return $closure_open;
        });
    }



    /**
     * @param array $input
     * @return mixed
     * update closure - new function with workflow
     */
    public function update($employer_closure_open,  array $input)
    {
        return DB::transaction(function () use ($input, $employer_closure_open) {

            $employer_closure_open->update([
                'open_date' => $input['open_date'],
                'reason' => $input['open_reason'],
            ]);

            return $employer_closure_open;
        });
    }


    /**
     * @param $employer_closure
     * Undo closure
     */
    public function undo($employer_closure_open)
    {

        DB::transaction(function () use ($employer_closure_open) {
            $employer_closure_open->delete();

            /*Deactivate workflow tracks*/
            $type = $this->getWfModuleType($employer_closure_open);
            $wf_module_group_id = 22;
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $employer_closure_open->id, "type" => $type]);
            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();
            /*end workflow track*/
        });
    }


    /**
     * @param $id
     * Updates on initiate workflow
     */
    public function initiateApproval($id)
    {
        $closure_open = $this->find($id);
        $this->checkIfCanInitiateReopen($closure_open);

        $closure_open->update([
            'status' => 1
        ]);
        return $closure_open;
    }


    /*check if can initiate*/
    public function checkIfCanInitiateApproval($closure_open_id)
    {
        /*check if doc are submitted -- Mandatory*/
        $doc = $this->getDocumentsNotSubmitted($closure_open_id);
        if($doc->isNotEmpty())
        {
            throw new WorkflowException('Please attach all required document to proceed!');
        }
    }

    /*Check if can create new opening process*/
    public function checkIfCanCreateNew($closure_id)
    {
        $closure = (new EmployerClosureRepository())->find($closure_id);
        $closure_open = $this->query()->where('employer_closure_id', $closure_id)->where('wf_done', 0)->count();
        if($closure_open > 0 || $closure->open()->count() > 0 )
        {
            throw new GeneralException('There is pending business open process! Please check!');
        }
    }

    /*Check if can initiate*/
    public function checkIfCanInitiateReopen($employer_closure_open)
    {
        $pending_tasks = $this->getPendingTasksBeforeInitiate($employer_closure_open);
        if(count($pending_tasks))
        {
            throw new GeneralException('Complete all pending tasks to proceed! Please check');
        }
    }


    /*Update wf done*/
    public function updateOnWorkflowComplete($id)
    {
        return DB::transaction(function () use ($id) {
            $closure_open = $this->find($id);
            $closure = $closure_open->employerClosure;
            $employer = ($closure) ? $closure->closedEmployer : $closure_open->employer;
            $dormant_sub_type_cv_id = (new CodeValueRepository())->findIdByReference('ETCLDORMANT');
            /*Check if closure exist*/
            if ($closure){
                $closure->open_date = $closure_open->open_date;
                $closure->open_date_internal = Carbon::parse($closure_open->open_date)->startOfMonth();
                $closure->open_user_id =  $closure_open->user_id;
                $closure->open_reason =  $closure_open->reason;
                $closure->isbookingskip = ($closure->sub_type_cv_id == $dormant_sub_type_cv_id) ? 0 : $closure->isbookingskip;
                $closure->save();

            }

            /*Update employer status*/
            $employer->employer_status = 1;
            $employer->employer_closure_id = NULL;
            $employer->save();


            /*InitiateChecker*/
            (new EmployerClosureRepository())->initializeChecker($closure, 'EMPDERREOP');

            return $closure_open;
        });
    }







    /**
     * @param int $closure_type
     * @return array
     * Get mandatory docs ids by closure type
     */
    public function queryDocumentsForAttachment($reopen_id)
    {
        return (new DocumentRepository())->query()->where('document_group_id', 26);
    }



    /**
     * @param $employer_closure_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * Get documents pending to be attached
     */
    public function getDocumentsNotSubmitted($employer_closure_id)
    {
        $closure = $this->find($employer_closure_id);
        $employer = $closure->closedEmployer;
        $docs_id_required = $this->getMandatoryDocsIdsByClosureType($closure->closure_type, $closure->isdomestic);
        $docs_attached = $employer->documents()->where('external_id', $employer_closure_id)->whereIn('document_id',$docs_id_required)->pluck('document_id')->toArray();
        $doc_id_not_attached = array_diff($docs_id_required, $docs_attached);
        $docs_pending =  Document::query()->whereIn('id', $doc_id_not_attached)->get();
        return $docs_pending;

    }


    /**
     * @param $employer_closure_id
     * Get doc attached for this closure
     */
    public function getDocsAttached($reopen_id)
    {
        $reopen = $this->find($reopen_id);
        $employer_closure = $reopen->employerClosure;
        $employer = ($employer_closure) ? $employer_closure->closedEmployer : (new EmployerRepository())->find($reopen->employer_id);
        $docs_all = $this->queryDocumentsForAttachment($reopen_id)->pluck('id')->toArray();
        $docs_attached = $employer->documents()->where('external_id', $reopen_id)->whereIn('document_id',$docs_all)->get();
        return $docs_attached;
    }


    /**
     * @param $employer_closure
     * @return int
     * Get module type for workflow when initiating approval workflow
     */
    public function getWfModuleType($employer_closure_open)
    {
        $type = 1;

        return $type;
    }


    /*Get pending tasks before initiate*/
    public function getPendingTasksBeforeInitiate($employer_closure_open)
    {
        $pending_tasks = [];
        $employer_id = ($employer_closure_open->employer_id) ? $employer_closure_open->employer_id : $employer_closure_open->employerClosure->employer_id;
        $doc_attached = $this->getDocsAttached($employer_closure_open->id);
        if($doc_attached->count() == 0){
            $pending_tasks[] = 'Supporting document not attached';
        }
        return $pending_tasks;
    }





    /*Get closures for dataTable per employer history*/
    public function getClosureByEmployerForDataTable($employer_id)
    {
        return $this->query()->where('employer_id', $employer_id);
    }






    /**
     * --Function For AUTO Register Employers with dormant closure and employer status 4
     */

    public function autoUpdateStatusForDormantDeregistered()
    {
        DB::transaction(function ()  {
            $dormant_sub_type_cv_id = (new CodeValueRepository())->findIdByReference('ETCLDORMANT');
            $dormant_contributed_employers = Employer::query()->select('employers.id as employer_id')->distinct('employers.id')
                ->join('employer_contributions as c', 'c.employer_id', 'employers.id')
                ->join('employer_closures as cl', 'cl.employer_id', 'employers.id')
                ->where('employers.employer_status', 4)
                ->where('cl.sub_type_cv_id', $dormant_sub_type_cv_id)
                ->whereRaw("c.contrib_month >= cl.close_date_internal")
                ->whereRaw("c.rct_date >= cl.close_date")
                ->get();

            foreach($dormant_contributed_employers as $dormant_employer)
            {
                /*Update Employer status*/
                $employer = Employer::query()->find($dormant_employer->employer_id);
                $employer_closure_id = $employer->employer_closure_id;
                $employer->update([
                    'employer_status' => 1,
                    'employer_closure_id' => null
                ]);

                /*Update dormant closure*/
                if(isset($employer_closure_id)){
                    $employer_closure = EmployerClosure::query()->where('id',$employer_closure_id)->where('sub_type_cv_id',$dormant_sub_type_cv_id)->first();
                    if(isset($employer_closure)){
                        $employer_closure->update([
                            'open_date' => standard_date_format(getTodayDate()),
                            'open_date_internal' => standard_date_format(Carbon::now()->startOfMonth()),
                            'isbookingskip' => 0
                        ]);
                    }

                }
            }


        });
    }


}