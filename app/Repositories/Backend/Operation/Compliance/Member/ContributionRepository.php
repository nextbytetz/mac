<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Contribution;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Services\Scopes\IsactiveScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContributionRepository extends  BaseRepository
{

    const MODEL = Contribution::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $contribution = $this->query()->find($id);

        if (!is_null($contribution)) {
            return $contribution;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.contribution_not_found'));
    }


    /**
     * @param $receipt_id
     * Deactivate employeee contribution when dishonour or canceling receipt
     */
    public function deactivateContributions($receipt_id) {
        $employee_contributions = $this->query()->whereHas('receiptCode', function($query) use($receipt_id) {
            $query->where('receipt_id', '=',$receipt_id);
        });
        $employee_contributions->update(['isactive'=> 0]);
    }


    /**
     * @param $receipt_id
     * Activate employeee contribution when replacing the cheque
     */
    public function activateContributions($receipt_id) {
        $employee_contributions = $this->query()->withoutGlobalScopes
        ([IsactiveScope::class])->whereHas('receiptCode', function($query) use
        ($receipt_id) {
            $query->where('receipt_id', '=',$receipt_id);
        });
        $employee_contributions->update(['isactive'=> 1]);
    }



    /*  Legacy activating / Deactivating */
    /**
     * @param $receipt_id
     * Deactivate employee contribution when dishonour or canceling receipt
     */
    public function deactivateLegacyContributions($legacy_receipt_id) {
        $employee_contributions = $this->query()->whereHas('legacyReceiptCode', function($query) use($legacy_receipt_id) {
            $query->where('legacy_receipt_id', '=',$legacy_receipt_id);
        });
        $employee_contributions->update(['isactive'=> 0]);
    }


    /**
     * @param $receipt_id
     * Activate employee contribution when replacing the cheque
     */
    public function activateLegacyContributions($legacy_receipt_id) {
        $employee_contributions = $this->query()->withoutGlobalScopes
        ([IsactiveScope::class])->whereHas('legacyReceiptCode', function($query) use
        ($legacy_receipt_id) {
            $query->where('legacy_receipt_id', '=',$legacy_receipt_id);
        });
        $employee_contributions->update(['isactive'=> 1]);
    }


    /**
     * @param $receipt_code_id
     * @return mixed
     */
    public function getMemberContributions($receipt_code_id)
    {
        return $this->query()
            ->select([
                DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as employee"),
                "contributions.employee_amount",
                "contributions.employer_amount",
                "contributions.grosspay",
                "contributions.salary",
                "employees.firstname",
                "employees.middlename",
                "employees.lastname",
                "employee_id",
            ])
            ->join("employees", "employees.id", "=", "contributions.employee_id")
            ->where("receipt_code_id", $receipt_code_id);
    }


    /**
     * @param $legacy_receipt_code_id
     * @return mixed
     */
    public function getMemberLegacyContributions($legacy_receipt_code_id)
    {
        return $this->query()->where("legacy_receipt_code_id", $legacy_receipt_code_id);
    }

    /**
     * @param $employee_id
     * @param $employer_id
     * @param $contrib_month
     * @throws GeneralException
     */
    public function checkIfContributionExistByEmployee($employee_id, $employer_id, $contrib_month)
    {
        //$contrib_month = Carbon::parse($contrib_month);
        //$count = $this->query()->withoutGlobalScope(\App\Services\Scopes\IsactiveScope::class)->whereRaw("date_part('month', contrib_month) = :month and date_part('year', contrib_month) = :year and employer_id = :employer_id and employee_id = :employee_id and isactive = 1 ", [ 'month' => $contrib_month->format('n'), 'year' => $contrib_month->format('Y'), 'employer_id' => $employer_id, 'employee_id' => $employee_id ])->count();
        $contribution = $this->getContribution($employee_id, $employer_id, $contrib_month);
        $count = $contribution->count();
        if ($count) {
            throw new GeneralException(trans('exceptions.backend.compliance.contribution_already_exist'));
        }
    }

    /**
     * @param $employee_id
     * @param $employer_id
     * @param $contrib_month
     * @return mixed
     */
    public function doesEmployeeHasContribution($employee_id, $employer_id, $contrib_month)
    {
        $contribution = $this->getContribution($employee_id, $employer_id, $contrib_month);
        $count = $contribution->count();
        return $count;
    }

    /**
     * @param $employee_id
     * @param $employer_id
     * @param $contrib_month
     * @return mixed
     */
    public function getContribution($employee_id, $employer_id, $contrib_month)
    {
        $contrib_month = Carbon::parse($contrib_month);
        $contribution = $this->query()->withoutGlobalScope(\App\Services\Scopes\IsactiveScope::class)->whereRaw("date_part('month', contrib_month) = :month and date_part('year', contrib_month) = :year and employer_id = :employer_id and employee_id = :employee_id and isactive = 1 ", [ 'month' => $contrib_month->format('n'), 'year' => $contrib_month->format('Y'), 'employer_id' => $employer_id, 'employee_id' => $employee_id ])->orderBy("grosspay", "desc");
        return $contribution;
    }

    /**
     * @param $employee_id
     * @param $employer_id
     * @param $contrib_month
     * @return mixed
     */
    public function findIfContributionExistByEmployee($employee_id, $employer_id, $contrib_month)
    {
        $contrib_month = Carbon::parse($contrib_month);
        $contribution = $this->query()->withoutGlobalScope(\App\Services\Scopes\IsactiveScope::class)->whereRaw("date_part('month', contrib_month) = :month and date_part('year', contrib_month) = :year and employer_id = :employer_id and employee_id = :employee_id and isactive = 1 ", [ 'month' => $contrib_month->format('n'), 'year' => $contrib_month->format('Y'), 'employer_id' => $employer_id, 'employee_id' => $employee_id ])->first();
        return $contribution;
    }


}