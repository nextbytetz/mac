<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\UnregisteredEmployer;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Propaganistas\LaravelPhone\PhoneNumber;

class UnregisteredEmployerRepository extends  BaseRepository
{

    const MODEL = UnregisteredEmployer::class;

    public function __construct()
    {

    }

//find or throwexception for employer
    public function findOrThrowException($id)
    {
        $unregistered_employer = $this->query()->find($id);

        if (!is_null($unregistered_employer)) {
            return $unregistered_employer;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.unregistered_employer_not_found'));
    }


    /**
     * @param $input
     * Create new
     */
    public function create($input){
        return DB::transaction(function () use ($input) {
//            if (count($input['tin'])) {
//                $this->checkIfTinExistWhenInsert($input['tin']);
//            }
            $unregistered_employer = $this->query()->create([
                'name' => $input['name'],
                'doc' => (isset($input['doc']) And ($input['doc'])) ? $input['doc'] : null,
                'tin' => (isset($input['tin']) And ($input['tin'])) ? preg_replace('/[^A-Za-z0-9]/', '', $input['tin']) : null,
                'district_id' =>  array_key_exists('district_id', $input) ? (($input['district_id']) ? $input['district_id'] : null ) : null,
                'region_id' =>  array_key_exists('region_id', $input) ? (($input['region_id']) ? $input['region_id'] : null ) : null,
                'address' => $input['box_no'],
                'location_type_id' =>  array_key_exists('location_type_id', $input) ? (($input['location_type_id']) ? $input['location_type_id'] : null ) : null,
                'unsurveyed_area' => $input['unsurveyed_area'],
                'street' => $input['street'],
                'road' => $input['road'],
                'plot_number' => $input['plot_no'],
                'block_no' => $input['block_no'],
                'surveyed_extra' => $input['surveyed_extra'],
                'phone' => (isset($input['phone']) And ($input['phone'])) ?  phone_255($input['phone'])  : null,
                'telephone' => (isset($input['telephone']) And ($input['telephone'])) ?  phone_255($input['telephone'])  : null,
                'email' => $input['email'],
                'fax' => $input['fax'],
                'business_activity' => $input['business_activity'],
                'sector' =>  array_key_exists('sector', $input) ? (($input['sector']) ? $input['sector'] : null ) : null,
                'no_of_employees' => $input['no_of_employees'],
            ]);

            return $unregistered_employer;
        });


    }


    /**
     * @param $input
     * Update
     */
    public function update($id,$input){
        return DB::transaction(function () use ($id,$input) {
            $unregistered_employer = $this->findOrThrowException($id);
            $unregistered_employer->update([
                'name' => $input['name'],
                'doc' => (isset($input['doc']) And ($input['doc'])) ? $input['doc'] : null,
                'tin' => (isset($input['tin']) And ($input['tin'])) ? preg_replace('/[^A-Za-z0-9]/', '', $input['tin']) : null,
                'district_id' =>  array_key_exists('district_id', $input) ? (($input['district_id']) ? $input['district_id'] : null ) : null,
                'region_id' =>  array_key_exists('region_id', $input) ? (($input['region_id']) ? $input['region_id'] : null ) : null,
                'address' => $input['box_no'],
                'location_type_id' =>  array_key_exists('location_type_id', $input) ? (($input['location_type_id']) ? $input['location_type_id'] : null ) : null,
                'unsurveyed_area' => $input['unsurveyed_area'],
                'street' => $input['street'],
                'road' => $input['road'],
                'plot_number' => $input['plot_no'],
                'block_no' => $input['block_no'],
                'surveyed_extra' => $input['surveyed_extra'],
                'phone' => (isset($input['phone']) And ($input['phone'])) ?  phone_255($input['phone'])  : null,
                'telephone' => (isset($input['telephone']) And ($input['telephone'])) ?  phone_255($input['telephone'])  : null,
                'email' => $input['email'],
                'fax' => $input['fax'],
                'business_activity' => $input['business_activity'],
                'sector' =>  array_key_exists('sector', $input) ? (($input['sector']) ? $input['sector'] : null ) : null,
                'no_of_employees' => $input['no_of_employees'],
            ]);
//            if (count($input['tin'])) {
//                $this->checkIfTinExistWhenUpdate($input['tin']);
//            }

            return $unregistered_employer;
        });


    }



    /**
     * @param $id
     * delete
     */

    public function delete($id){
        $unregistered_employer = $this->findOrThrowException($id);
        $unregistered_employer->delete();
    }


    /**
     * @param $id
     * Register into
     */
    public function register($id){
        return DB::transaction(function () use ($id) {
            $this->checkIfTinIsFilled($id);
            $this->checkTinNameValidationOnRegister($id);
            $unregistered_employer = $this->findOrThrowException($id);
            $employer_registrations = new Employer();
            $employer_registration = $employer_registrations->query()->create([
                'name' => $unregistered_employer->name,
                'tin' => $unregistered_employer->tin,
                'doc' => $unregistered_employer->doc,
                'district_id' =>  $unregistered_employer->district_id,
                'box_no' => $unregistered_employer->address,
                'location_type_id' => $unregistered_employer->location_type_id,
                'unsurveyed_area' => $unregistered_employer->unsurveyed_area,
                'street' => $unregistered_employer->street,
                'road' => $unregistered_employer->road,
                'plot_no' => $unregistered_employer->plot_number,
                'block_no' => $unregistered_employer->block_no,
                'surveyed_extra' => $unregistered_employer->surveyed_extra,
                'phone' => $unregistered_employer->phone,
                'telephone' => $unregistered_employer->telephone,
                'email' => $unregistered_employer->email,
                'fax' => $unregistered_employer->fax,
                'business_activity' => $unregistered_employer->business_activity,
                'created_by' => access()->user()->id
            ]);
//      Update flag is_registered
            $unregistered_employer->update(['is_registered'=> 1, 'date_registered' =>  Carbon::parse('now')->format('Y-n-j')]);
            return $employer_registration;
        });
    }

    /**
     * @param $id
     * @return mixed
     * Mark Registered if already registered
     */
    public function markRegistered($id)
    {
        return DB::transaction(function () use ($id) {
            $unregistered_employer = $this->findOrThrowException($id);
//      Update flag is_registered
            $unregistered_employer->update(['is_registered'=> 1, 'date_registered' =>  Carbon::parse('now')->format('Y-n-j')]);
            return $unregistered_employer;
        });
    }



    /**
     * @param $id
     * staff Assignment to employer
     */
    public function assignStaff($id, $user_id){
        $unregistered_employer = $this->findOrThrowException($id);
        $unregistered_employer->update([
            'assign_to' => $user_id,
            'date_assigned' => Carbon::parse('now')->format('Y-n-j')
        ]);
    }


    /**
     * @paraffm $id
     * Changed assigned staff
     *
     */
    public function changeAssignedStaff($id,$input){
        $unregistered_employer = $this->findOrThrowException($id);
        $unregistered_employer->update([
            'assign_to' => $input['assigned_to'],
            'date_assigned' => Carbon::parse('now')->format('Y-n-j')
        ]);
    }



    /**
     * @param $id
     * @return mixed
     * getAssigned staff
     */
    public function getAssignedStaff($id){
        $unregistered_employer = $this->findOrThrowException($id);
        $assigned_staff = $unregistered_employer->assign_to;
        return $assigned_staff;
    }

    /**
     * @param $id
     * Check if user assigned
     */
    public function checkIfUserAssigned($id){
        $assigned_staff = $this->getAssignedStaff($id);

        if ($assigned_staff) {

            if ($assigned_staff <> access()->user()->id) {
                throw new GeneralException(trans('exceptions.backend.compliance.user_not_assigned_to_this_employer'));
            }
        }else{
            throw new GeneralException(trans('exceptions.backend.compliance.unregistered_employer_not_assigned'));
        }

    }

    /**
     * @param $id
     * @throws GeneralException
     * Check if TIN number is filled
     */
    public function checkIfTinIsFilled($id)
    {
        $unregistered_employer = $this->findOrThrowException($id);

        if (!$unregistered_employer->tin) {

            throw new GeneralException(trans('exceptions.backend.compliance.tin_not_filled'));
        };
    }

    /**
     * @param $id
     * @throws GeneralException
     * Check if TIN number exist
     */
    public function checkIfTinExistWhenInsert($tin)
    {
        $employers = new EmployerRepository();
        if (($employers->checkIfTinAlreadyExist($tin)['tin_unregistered_employer'])->isNotEmpty() || ($employers->checkIfTinAlreadyExist($tin)['tin_employer'])->isNotEmpty()) {
            throw new GeneralException(trans('exceptions.backend.compliance.employer_with_tin_exist'));
        };
    }




    /**
     * @param $id
     * @throws GeneralException
     * Check if TIN number exist when update
     */
    public function checkIfTinExistWhenUpdate($tin)
    {
        $employers = new EmployerRepository();
        if (($employers->checkIfTinAlreadyExist($tin)['tin_unregistered_employer'])->count() > 1 || ($employers->checkIfTinAlreadyExist($tin)['tin_employer'])->isNotEmpty()) {
            throw new GeneralException(trans('exceptions.backend.compliance.employer_with_tin_exist'));
        };
    }


    /**
     * @param $id
     * CHeck if employer is already registered
     */
    public function checkIfEmployerIsRegistered($id){
        $unregistered_employer = $this->findOrThrowException($id);
        if ($unregistered_employer->is_registered == 1) {

            throw new GeneralException(trans('exceptions.backend.compliance.unregistered_employer_already_registered'));
        };
    }

    /**
     * @param $tin
     * @throws GeneralException
     * Find if Tin Exist In employer table
     */
    public function findIfTinExistInEmployer($tin)
    {
        $employers = new EmployerRepository();
        if (($employers->checkIfTinAlreadyExist($tin)['tin_employer'])->isNotEmpty()) {
            return 1;
        }else {
            return 0;
        }
    }


    /**
     * @param $id
     * Check validation when registering unregistered
     */
    public function checkTinNameValidationOnRegister($id)
    {
        $employers = new EmployerRepository();
        $unregistered_employer = $this->findOrThrowException($id);
        $input = ['tin' => $unregistered_employer->tin, 'name' => $unregistered_employer->name];
        $employers->checkValidationEmployerName($input, 1);
        $employers->checkIfTinEmployerExistWhenInsert($input);
    }





    /**
     * @param $linked_file
     * @throws GeneralException
     * Check if column header are all included
     */
    public function checkColumnOnUploadedBulk()
    {
        $file_extension = request()->file('linked_file')->getClientOriginalExtension();
        $linked_file = unregistered_employer_bulk_dir() . DIRECTORY_SEPARATOR . Carbon::parse('now')->format('M') . '_' . Carbon::parse('now')->format('Y') . '.' . $file_extension;
             $headings = Excel::selectSheetsByIndex(0)
            ->load($linked_file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['name','tin','doc','business_activity','sector', 'no_of_employees','phone','telephone','email','p_o_box','region', 'postal_city', 'region', 'street', 'plot_no', 'block_no'];

        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new GeneralException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));

            }
        }
    }




    /**
     * @param $id
     * @return mixed
     * Get all follow ups of this unregistered employer
     */
    public function getFollowUps($id)
    {
        return $this->query()->find($id)->unregisteredEmployerFollowUps;
    }


    public function getUnRegisteredUnassignedEmployers($q)
    {
        //$name = strtolower(trim(preg_replace('/\s+/', '', $q)));
        $name = strtolower($q);
        $data['items'] =  DB::select('select * from unregistered_employers where (LOWER(name) like :name) and deleted_at is null and assign_to = :id', ['name' => "%$name%", 'id' => 0 ]);
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }


}