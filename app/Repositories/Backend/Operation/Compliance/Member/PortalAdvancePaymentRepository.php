<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\PortalAdvancePayment;
use App\Repositories\BaseRepository;

class PortalAdvancePaymentRepository extends BaseRepository
{
    const MODEL = PortalAdvancePayment::class;

}