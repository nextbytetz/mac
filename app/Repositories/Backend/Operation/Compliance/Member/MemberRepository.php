<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\Member;
use App\Repositories\BaseRepository;

class MemberRepository extends BaseRepository
{
    const MODEL = Member::class;

}