<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Member\UnregisteredInspectionProfile;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\DataFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UnregisteredInspectionProfileRepository extends BaseRepository
{
    const MODEL = UnregisteredInspectionProfile::class;

    public function findOrThrowException($id)
    {
        $unregistered_inspection_profile = $this->query()->find($id);

        if (!is_null($unregistered_inspection_profile)) {
            return $unregistered_inspection_profile;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.unregistered_inspection_profile_not_found'));
    }

    public function getForDataTable()
    {
        return $this->query()->select();
    }


    /**
     * @param $input
     * Create
     */
    public function create($input)
    {
        return DB::transaction(function () use ($input) {
            $profile = $this->query()->create([
                'name' => $input['name'],
                'query' => $this->generateQuery($input),
                'user_id' => access()->id(),
]);
            return $profile;
    });
    }

    /**
     * @param $id
     * @param $input
     * Update
     */
    public function update($id, $input)
    {
        return DB::transaction(function () use ($id,$input) {
            $profile = $this->findOrThrowException($id);
            $profile->update([
                'name' => $input['name'],
                'query' => $this->generateQuery($input),
              ]);
            return $profile;
        });
    }



    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $profile = $this->findOrThrowException($id);
            $profile->delete();
            return true;
        });
    }



    public function generateQuery($input)
    {
        $data = [];

        if (isset($input["sector"])) {
            $sector = [];
            $current = $input["sector"];
            foreach ($current as $perm) {
//                if (is_numeric($perm)) {
                    array_push($sector, $perm);
//                }
            }
            $data['sector'] = $sector;
        }
        if (isset($input["region"])) {
            $region = [];
            $current = $input["region"];
            foreach ($current as $perm) {
                              if (is_numeric($perm)) {
                    array_push($region, $perm);
                }
            }
            $data['region'] = $region;
        }


        if (isset($input["no_of_employees_min"])) {

            $data['no_of_employees_min'] = $input["no_of_employees_min"];
        }

        if (isset($input["no_of_employees_max"])) {
            $data['no_of_employees_max'] = $input["no_of_employees_max"];
        }

      $query = $this->arrayToDb($data);
        return $query;
    }




    public function getEmployerForDataTable(Model $profile)
    {
        return $this->dbToArrayQuery($profile->query);
    }



    /**
     * @param array $input
     * @return string
     */
    public function arrayToDb(array $input)
    {
        $serialized = base64_encode(serialize($input));
        return $serialized;
    }

    /**
     * @param $input
     * @return mixed
     */
    public function dbToArray($input)
    {
        $unserialized = unserialize(base64_decode($input));
        return $unserialized;
    }

    /**
     * @param $input
     * @return string
     */
    public function dbArrayInfo($input)
    {
        $return = "";
        $array = $this->dbToArray($input);

        foreach ($array as $key => $value) {
            switch ($key) {
                case 'sector':
                    $return .= "Business : ";
                    $return .= implode(", ", $value);
                    $return .= "<br/>";
                    break;
                case 'region':
                    $region = new RegionRepository();
                    $list = $region->find($value)->pluck('name')->all();
                    $return .= "Region : ";
                    $return .= implode(", ", $list);
                    $return .= "<br/>";
                    break;
                case 'no_of_employees_min':
                    $return .= "No. of Employees (Min) : ";
                    $return .= ( $value);
                    $return .= "<br/>";
                    break;
                case 'no_of_employees_max':
                    $return .= "No. of Employees (Max) : ";
                    $return .=  ( $value);
                    $return .= "<br/>";
                    break;
            }


        }
        return $return;
    }

    /**
     * @param $input
     * @return mixed
     */
    public function dbToArrayQuery($input)
    {
        $employer = new UnregisteredEmployerRepository();
        $query = $employer->query();
        $array = $this->dbToArray($input);
        foreach ($array as $key => $value) {
            switch ($key) {
                case 'sector':
                    $query = $query->whereIn("sector", $value);
                    break;
                case 'region':
                    $region = new RegionRepository();
                    $list = $region->find($value)->pluck('id')->all();
//                    $list = array_map('strtolower' ,$list);
//                    $list = preg_replace('/[^A-Za-z0-9]/', '', $list);
//                    $query = $query->where(function ($query) use ($list){
////                        $query->whereIn("location", $list)->orWhereIn('postal_city', $list)
//                        $query->whereIn(DB::raw("LOWER(regexp_replace(" . DB::raw('location') . " , '[^a-zA-Z0-9]', '', 'g'))"), $list) ;
//            });
                    $query = $query->whereIn('region_id', $list );
                    break;
                case 'no_of_employees_min':
//                    $region = new RegionRepository();
//                    $list = $region->find($value)->pluck('id')->all();
                    $query = $query->where(DB::raw("COALESCE(no_of_employees,0)"),'>=', $value);
                    break;
                case 'no_of_employees_max':
//                    $region = new RegionRepository();
//                    $list = $region->find($value)->pluck('id')->all();
                    $query = $query->where(DB::raw("COALESCE(no_of_employees,0)"),'<=', $value);
                    break;
            }
        }
//        /* Pick only which are no are assigned yet*/
//        $query->where('assign_to', 0);
        return $query;
    }

    /**
     * @param $input
     * @return array
     */
    public function dbToArrayData($input)
    {
        $output = [];
        $array = $this->dbToArray($input);
        foreach ($array as $key => $value) {
            switch ($key) {
                case 'sector':
//                    $business = new BusinessRepository();
//                    $list = $business->find($value)->pluck('id')->all();
                    $output['sector'] = $value;
                    break;
                case 'region':
                    $region = new RegionRepository();
                    $list = $region->find($value)->pluck('id')->all();
                    $output['region'] = $list;
                    break;
                case 'no_of_employees_min':
//                    $region = new RegionRepository();
//                    $list = $region->find($value)->pluck('id')->all();
                    $output['no_of_employees_min'] = $value;
                    break;
                case 'no_of_employees_max':
//                    $region = new RegionRepository();
//                    $list = $region->find($value)->pluck('id')->all();
                    $output['no_of_employees_max'] = $value;
                    break;
            }
        }
        return $output;
    }


}