<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\OnlineEmployerVerification;
use App\Repositories\BaseRepository;

class OnlineEmployerVerificationRepository extends BaseRepository
{
	const MODEL = OnlineEmployerVerification::class;


	public function getAllForDatatable($id)
	{
		return $this->query()->where("employer_id", $id);
	}

	public function getAllApprovedForDatatable($id)
	{
		return $this->query()->where("employer_id", $id)->where('status',1);
	}

}