<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Events\EmployeeUpdated;
use App\Models\Operation\Compliance\Member\Employee;
use App\Exceptions\GeneralException;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Claim\DeathRepository;
use App\Repositories\Backend\Operation\Claim\EmployeeOldCompensationRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmployeeRepository extends  BaseRepository
{

    use AttachmentHandler;

    const MODEL = Employee::class;

    protected $contributions;
    protected $deaths;
    protected $employment_histories;
    protected $employee_old_compensations;

    /**
     * EmployeeRepository constructor.
     */
    public function __construct( )
    {
        $this->contributions = new ContributionRepository();
        $this->deaths = new DeathRepository();
        $this->employment_histories = new EmploymentHistoryRepository();
        $this->employee_old_compensations = new EmployeeOldCompensationRepository();
    }

    /*
     * EMPLOYEE METHODS =====================================
     */

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $employee = $this->query()->find($id);

        if (!is_null($employee)) {
            return $employee;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.employee_not_found'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findNotificationReportDeathIncident($id)
    {
        $employee = $this->findOrThrowException($id);
        $notification_report = $employee->notificationReports()->whereIn('incident_type_id', [3,4,5])->first();

        return $notification_report;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function store(array $input){
        return DB::transaction(function () use ($input) {
            $this->checkIfAlreadyExist($input,1);
            $basicpay = (isset($input['basicpay']) And ($input['basicpay'])) ? str_replace(",", "", $input['basicpay']) : 0;
            $allowance = (isset($input['allowance']) And ($input['allowance'])) ? str_replace(",", "", $input['allowance']) : 0;
            $data = [
                'firstname' =>  trim(preg_replace('/\s+/', '', $input['firstname']))  ,
                'middlename' =>  trim(preg_replace('/\s+/', '', $input['middlename']))  ,
                'lastname' => trim(preg_replace('/\s+/', '', $input['lastname']))  ,
                'dob' => $input['dob'],
                'gender_id' => $input['gender_id'],
                'marital_status_cv_id' => array_key_exists('marital_status_cv_id', $input) ? $input['marital_status_cv_id'] : null,
                'employee_category_cv_id' => $input['employee_category_cv_id'],
                'job_title_id' => array_key_exists('job_title_id', $input) ? $input['job_title_id'] : null,
                'identity_id' => array_key_exists('identity_id', $input) ? $input['identity_id'] : null,
                'id_no' => $input['id_no'],
                'nid' => $input['nid'],
                'basicpay' => $basicpay,
                'grosspay' => $this->findGrosspay($basicpay,$allowance),
                'department' => $input['department'],
                'email' => $input['email'],
                'phone' =>(isset($input['phone']) And ($input['phone'])) ? phone_255($input['phone'])  : null,
                'fax' => $input['fax'],
                'country_id' => $input['country_id'],
                'location_type_id' =>array_key_exists('location_type_id', $input) ? $input['location_type_id'] : null,
                'street' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['street'] : null) : null,
                'road' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['road'] : null) : null,
                'plot_no' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['plot_no'] : null) : null,
                'block_no' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['block_no'] : null) : null,
                'unsurveyed_area' =>  array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 2) ? $input['unsurveyed_area'] : null) : null,
                'surveyed_extra' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['surveyed_extra'] : null) : null,
                'memberno' => 0,
                'user_id' =>  access()->id(),
            ];
            if ($input['country_id'] != 1) {
                $data['residence_permit_no'] = $input['residence_permit_no'];
                $data['work_permit_no'] = $input['work_permit_no'];
                $data['passport_no'] = $input['passport_no'];
            }
            $employee = $this->query()->create($data);
            /* save member no */
            $employee_id = $employee->id;
            $employee->memberno = checksum($employee_id, sysdefs()->data()->employee_number_length);
            $employee->save();
            /*attach employers */
            $this->attachEmployers($employee, $input);
            return $employee;
        });

    }

    public function findGrosspay($basicpay, $allowance)
    {
        $grosspay = $basicpay + $allowance;
        return $grosspay;
    }

    /**
     * @param array $input
     * @return mixed
     * Update
     */
    public function update($id, array $input){
        return DB::transaction(function () use ($id, $input) {
            $employee = $this->findOrThrowException($id);
            $basicpay = (isset($input['basicpay']) And ($input['basicpay'])) ? str_replace(",", "", $input['basicpay']) : 0;
            $allowance = (isset($input['allowance']) And ($input['allowance'])) ? str_replace(",", "", $input['allowance']) : 0;
            $data = [
                'firstname' =>  trim(preg_replace('/\s+/', '', $input['firstname']))  ,
                'middlename' =>  trim(preg_replace('/\s+/', '', $input['middlename']))  ,
                'lastname' => trim(preg_replace('/\s+/', '', $input['lastname']))  ,
                'dob' => $input['dob'],
                'gender_id' => $input['gender_id'],
                'marital_status_cv_id' => array_key_exists('marital_status_cv_id', $input) ? $input['marital_status_cv_id'] : null,
                'employee_category_cv_id' => $input['employee_category_cv_id'],
                'job_title_id' => array_key_exists('job_title_id', $input) ? $input['job_title_id'] : null,
                'identity_id' => array_key_exists('identity_id', $input) ? $input['identity_id'] : null,
                'id_no' => $input['id_no'],
                'nid' => $input['nid'],
                'basicpay' => $basicpay,
                'grosspay' => $this->findGrosspay($basicpay,$allowance),
                'department' => $input['department'],
                'email' => $input['email'],
                'phone' =>(isset($input['phone']) And ($input['phone'])) ? phone_255($input['phone'])  : null,
                'fax' => $input['fax'],
                'country_id' => $input['country_id'],
                'location_type_id' =>array_key_exists('location_type_id', $input) ? $input['location_type_id'] : null,
                'street' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['street'] : null) : null,
                'road' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['road'] : null) : null,
                'plot_no' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['plot_no'] : null) : null,
                'block_no' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['block_no'] : null) : null,
                'unsurveyed_area' =>  array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 2) ? $input['unsurveyed_area'] : null) : null,
                'surveyed_extra' => array_key_exists('location_type_id', $input) ? (($input['location_type_id'] == 1) ? $input['surveyed_extra'] : null) : null,

            ];
            if ($input['country_id'] != 1) {
                $data['residence_permit_no'] = $input['residence_permit_no'];
                $data['work_permit_no'] = $input['work_permit_no'];
                $data['passport_no'] = $input['passport_no'];
            } else {
                $data['residence_permit_no'] = NULL;
                $data['work_permit_no'] = NULL;
                $data['passport_no'] = NULL;
            }
            /* check if exist */
            $this->checkIfAlreadyExist($input,2);
            $employee->update($data);
            DB::table('portal.employee_uploads')->where('memberno',$employee->memberno)
            ->update([
                'firstname' =>strtoupper($data['firstname']),
                'middlename' => strtoupper($data['middlename']),
                'lastname' =>  strtoupper($data['lastname']),
                'dob' => $data['dob'],
            ]);
            /*attach employers */
            if (empty($input['return_url'])) {
                $this->attachEmployers($employee, $input);
            }
            //dd("Hi");
            //Update employee information in e-office
            event(new EmployeeUpdated($id));

            return $employee;
        });

    }

    /**
     * @param $id
     * @param $employer_id
     * @param $date_hired
     * @throws GeneralException
     */
    public function storeDateHired($id, $employer_id, $date_hired)
    {
        $employee = $this->findOrThrowException($id);
        $employee->employers()->syncWithoutDetaching([$employer_id => ['datehired' => $date_hired]]);
    }

    /**
     * @param $employee_id
     * @param $employer_id
     * @return null
     * @throws GeneralException
     */
    public function getDateHired($employee_id, $employer_id)
    {
        $date_hired = NULL;
        $employee = $this->findOrThrowException($employee_id);
        $pivot_employer = $employee->employers()->where('employer_id', $employer_id);
        if ($pivot_employer->count()){
            $date_hired = $pivot_employer->first()->pivot->datehired;
        }
        return $date_hired;
    }

    public function rectifyMemberno($employer_id)
    {
        $empty_member_nos =$this->query()->where('memberno', 0)->whereHas('employers', function ($query) use($employer_id){
            $query->where('employer_id', $employer_id);
        })->get();
        foreach ($empty_member_nos as $empty_member_no){
            $empty_member_no->memberno = checksum($empty_member_no->id, sysdefs()->data()->employee_number_length);
            $empty_member_no->save();
        }

    }

    /**
     * @param $input
     * Attach employee to employers
     */
    public function attachEmployers(Model $employee, array $input){
        /*Re sync*/
        $employer_array = [];
        foreach ($input as $key => $value) {
            switch ($key)  {
                case 'employer':

                $employer_array = $value;
                break;

            }
        }
        $employee->employers()->sync($employer_array);
    }

    /**
     * @param array $input
     * @throws GeneralException
     * Check if already exist
     * Action type; 1 => create, 2 => update
     */
    public function checkIfAlreadyExist(array $input, $action_type){

        switch ($action_type)  {
            case 1: //create
            $employee = $this->query()->whereRaw("levenshtein(lower(firstname), :firstname) between 0 and 1 and levenshtein(lower(lastname), :lastname) between 0 and 1 and dob = :dob", ['firstname' => strtolower($input['firstname']), 'lastname' => strtolower($input['lastname']), 'dob' => $input['dob']]);
            if ($employee->count()) {
                throw new GeneralException(trans('exceptions.backend.compliance.employee_already_exist'));
            }

            break;
            case 2: //update
            $employee = $this->query()->whereRaw("levenshtein(lower(firstname), :firstname) between 0 and 1 and levenshtein(lower(lastname), :lastname) between 0 and 1 and dob = :dob and id <> :id", ['firstname' => strtolower($input['firstname']), 'lastname' => strtolower($input['lastname']), 'dob' => $input['dob'], 'id' => $input['employee_id']]);
            if ($employee->count()) {
                    //TODO: to be enabled later, temporary disabled to allow processing of claims
                    //throw new GeneralException(trans('exceptions.backend.compliance.employee_already_exist'));
            }

            break;
        }

    }

    //Get for employees dataTable
    public function getForDataTable() {
//        return $this->query();
        $employees = $this->query()->select([
            'memberno',
            DB::raw("employees.id as id"),
            DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as fullname"),
            DB::raw("firstname"),
            DB::raw("middlename"),
            DB::raw("lastname"),
            DB::raw("dob"),
            DB::raw("coalesce(code_values.name, employees.emp_cate) as category"),
            DB::raw("employers.name as employer_name"),
        ])
        ->join("employee_employer", "employees.id", "=", "employee_employer.employee_id")
        ->join("employers", "employers.id", "=", "employee_employer.employer_id")
        ->leftJoin("code_values", "employee_employer.employee_category_cv_id", "=", "code_values.id");

        return $employees;
    }

    //====End Employee Methods-------------------------

    /*
     * CONTRIBUTION METHODS=====================================
     */
    public function getAllContributions($id) {
//        return   $this->query()->find($id)->contributions;
        $contribution = new ContributionRepository();
        return   $contribution->query()->has('receiptCode')->where("employee_id", $id);
    }

    public function getAllLegacyContributions($id) {
        $legacyContribution = new ContributionRepository();
        return   $legacyContribution->query()->doesntHave('receiptCode')->where("employee_id", $id);
    }

    /**
     * @param $id | employee_id
     * @param array $input
     * @return mixed
     */
    public function updateContribution($id, array $input)
    {
        return DB::transaction(function () use ($id, $input) {

            $contributionRepo = new ContributionRepository();

            $legacyReceipt = new LegacyReceiptRepository();
            $receiptRepo = new ReceiptCodeRepository();
            $legacy_receipt =  $legacyReceipt->findIfRctnoExist($input['rctno'], $input['employer_id'], $input['contrib_month']);
            $receipt = $receiptRepo->findIfRctnoExist($input['rctno'], $input['employer_id'], $input['contrib_month']);
            $this->checkIfRctnoExist($legacy_receipt, $receipt);

            $contribution = $contributionRepo->getContribution($id, $input['employer_id'], $input['contrib_month'])->first();

            $basicpay = str_replace(",", "", $input['basic_pay']);
            $grosspay = str_replace(",", "", $input['gross_pay']);
            if ($grosspay < $basicpay) {
                throw new GeneralException("Gross Pay can not be less than Basic Pay, Please Check...!!");
            }
            $data = [
                'salary' => $basicpay,
                'grosspay' => $grosspay,
                'employee_amount' => $this->findEmployeeAmount($input['employer_id'], $grosspay),
                'user_id' => access()->id(),
            ];
            $contribution->update($data);
            //dd($employee_contribution);
            return $contribution;
        });
    }

    /**
     * @param $id
     * @param array $input
     * @param int $source | 1 = from Employee Profile, 0 from notification missing contribution workflow
     * @return mixed
     */
    public function storeContribution($id, array $input, $source = 1)
    {
        return DB::transaction(function () use ($id, $input, $source) {
            $contribution = new ContributionRepository();
            $legacyReceipt = new LegacyReceiptRepository();
            $receiptRepo = new ReceiptCodeRepository();
            $legacy_receipt =  $legacyReceipt->findIfRctnoExist($input['rctno'], $input['employer_id'], $input['contrib_month']);
            $receipt = $receiptRepo->findIfRctnoExist($input['rctno'], $input['employer_id'], $input['contrib_month']);
            $this->checkIfRctnoExist($legacy_receipt, $receipt);
            //$employer_id =  count($legacy_receipt) ?  $legacy_receipt->employer_id : null;
            if ($source) {
                $contribution->checkIfContributionExistByEmployee($id, $input['employer_id'], $input['contrib_month']);
            }
            //$employee = $this->findOrThrowException($id);
            /* store contribution */
            $basicpay = str_replace(",", "", $input['basic_pay']);
            $grosspay = str_replace(",", "", $input['gross_pay']);
            if ($grosspay < $basicpay) {
                throw new GeneralException("Gross Pay can not be less than Basic Pay, Please Check...!!");
            }
            $data = [
                'employer_id' =>  $input['employer_id'],
                'employee_id' => $id,
                'salary' => $basicpay,
                'grosspay' => $grosspay,
                'employee_amount' => $this->findEmployeeAmount($input['employer_id'], $grosspay),
                'user_id' => access()->id(),
                'contrib_month' => $input['contrib_month'],
            ];
            if ($receipt) {
                $data['receipt_code_id'] = $receipt->id;
            }
            $employee_contribution = $contribution->query()->create($data);
            return $employee_contribution;
        });
    }

    /**
     * @param $legacy_receipt
     * @param $receipt
     * @return mixed
     * @throws GeneralException
     */
    public function checkIfRctnoExist($legacy_receipt, $receipt)
    {
        if (!$legacy_receipt And !$receipt)  {
            //if (!env('TESTING_MODE', 0)) {
            throw new GeneralException(trans('exceptions.backend.finance.receipts.not_found'));
           // }
        } else {
            if ($legacy_receipt) {
                $return = $legacy_receipt;
            } else {
                $return = $receipt;
            }
            return $return;
        }
    }

    /**
     * @param $employer_id
     * @param $grosspay
     * @return float|int
     * @throws GeneralException
     */
    public function findEmployeeAmount($employer_id, $grosspay)
    {
        //$employee = $this->findOrThrowException($id);
        $employerRepo = new EmployerRepository();
        $employer = $employerRepo->query()->find($employer_id);
        switch ($employer->employerCategory->reference){
            case 'ECPUB':
            $employee_amount = $grosspay *  (sysdefs()->data()->public_contribution_percent / 100);
            break;
            case 'ECPRI':
            $employee_amount = $grosspay *  (sysdefs()->data()->private_contribution_percent / 100);
            break;
        }
        return $employee_amount;
    }

// end contribution -------------------------

    /*
     * --CLAIM METHODS =================================================
     */
    /**
     * @param $id
     * @return mixed
     */
    public function getAccidentNotifications($id) {
        return   $this->query()->find($id)->accidents();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDiseaseNotifications($id) {
        return   $this->query()->find($id)->diseases();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDeathNotification($id) {
        return   $this->deaths->query()->whereHas("notificationReport", function ($query) use ($id)  {
            $query->where(['employee_id' => $id]);
        });
    }

    /*Get manual notifications by employee for Datatable*/
    public function getManualNotificationsForDt($id) {
        $employee =  $this->query()->find($id);
        return   $employee->manualNotificationReports;
    }


        /**
     * @param $id
     * @return mixed
     */
        public function getEmploymentHistories($id) {
            return   $this->query()->find($id)->employmentHistories;
        }

    /**
     * @param $employment_history_id
     * @return mixed
     * @throws GeneralException
     * Employment history with selected id
     */
    public function getEmploymentHistory($employment_history_id) {
        return   $this->employment_histories->findOrThrowException($employment_history_id);
    }


    //store employment histories
    public function storeEmploymentHistory($id, $input) {
        $employment_history= $this->employment_histories->create($id, $input);
    }

    //update employment histories
    public function updateEmploymentHistory($employment_history_id, $input) {
        $employment_history= $this->employment_histories->update($employment_history_id, $input);
        return $employment_history;
    }


    //old compensation
    public function getOldCompensations($id) {
        return   $this->query()->find($id)->employeeOldCompensations;
    }

    public function storeOldCompensation($id, $input) {
        $employment_history= $this->employee_old_compensations->create($id, $input);
    }

    /**
     * @param $employee_old_compensation_id
     * @return mixed
     * @throws GeneralException
     */
    public function getOldCompensation($employee_old_compensation_id) {
        return   $this->employee_old_compensations->findOrThrowException($employee_old_compensation_id);
    }

    /**
     * @param $employee_old_compensation_id
     * @param $input
     * @return mixed
     */
    public function updateOldCompensation($employee_old_compensation_id, $input) {
        $employee_old_compensation = $this->employee_old_compensations->update($employee_old_compensation_id, $input);
        return $employee_old_compensation;
    }

    /**
     * @param $id
     * @return mixed
     * dependents
     */
    public function getDependents($id) {
        return   $this->query()->find($id)->dependents();
    }

    public function getAccrualDependents($id) {
        return   $this->query()->find($id)->accrualDependents();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * dob -> date of birth
     */
    public function getDob($id) {
        return    $this->findOrThrowException($id)->dob;
    }

    /**
     * @param $employee_id
     * @param $notification_report_id
     * @return array
     */
    public function getMonthlyEarningBeforeIncident($employee_id, $notification_report_id)
    {
        // convert date into first day of month
        $id = $employee_id;
        $notificationReport = new NotificationReportRepository();
        $notification_report = $notificationReport->query()->where('id', $notification_report_id)->first();
        $incident_date = $notification_report->incident_date;
        $date_hired = $notification_report->datehired;

        $required_contrib_month = Carbon::parse($incident_date)->subMonthNoOverflow();

        if (!is_null($date_hired)) {
            //$date_hired = Carbon::now()->toDateString();
            /*check if incident date is same date hired*/
            if(months_diff($date_hired, $incident_date) == 0){
                $required_contrib_month = Carbon::parse($incident_date);
            }
        }
        //return $required_contrib_month;

        /* find monthly earning (gross pay) from contributions */
        $contrib_month = $required_contrib_month->format("n");
        $contrib_year = $required_contrib_month->format("Y");

        /* Check if required contribution if before or after electronic receipt */
        if ($required_contrib_month >= getElectronicReceiptStartDate())
        {
            /* after electronic receipt */
            $monthly_earning = $this->contributions->query()->where('employee_id', $id)->whereHas('receiptCode', function($query) use ($contrib_month, $contrib_year) {
                $query->whereMonth('contrib_month','=', $contrib_month)->whereYear('contrib_month','=', $contrib_year);
            })->sum('grosspay');
        } else {
            /* before electronic receipt */
            $monthly_earning = $this->contributions->query()->where('employee_id', $id)->whereMonth('contrib_month','=', $contrib_month)->whereYear('contrib_month','=', $contrib_year)->sum('grosspay');
        }

        $monthly_earning = ($monthly_earning > 0) ?  $monthly_earning :  $notification_report->monthly_earning;

        return ['monthly_earning' => $monthly_earning, 'contrib_month' => $required_contrib_month] ;
    }

    /* the below function will be in use
       if the same above function will 
       proceed to through an error
    */

       public function getMonthlyEarningBeforeIncidentAccrual($employee_id, $notification_report_id)
       {
        // convert date into first day of month
        $id = $employee_id;
        $notificationReport = new NotificationReportRepository();
        $notification_report = $notificationReport->query()->where('id', $notification_report_id)->first();
        $incident_date = $notification_report->incident_date;
        $date_hired = $notification_report->datehired;

        $required_contrib_month = Carbon::parse($incident_date)->subMonthNoOverflow();

        if (!is_null($date_hired)) {
            //$date_hired = Carbon::now()->toDateString();
            /*check if incident date is same date hired*/
            if(months_diff($date_hired, $incident_date) == 0){
                $required_contrib_month = Carbon::parse($incident_date);
            }
        }

        /* find monthly earning (gross pay) from contributions */
        $contrib_month = $required_contrib_month->format("n");
        $contrib_year = $required_contrib_month->format("Y");
        $contribution_month = Carbon::parse(''.$contrib_year.'-'.$contrib_month.'')->format('Y-m');

        /* Check if required contribution if before or after electronic receipt */
        if ($required_contrib_month >= getElectronicReceiptStartDate())
        {
            /* after electronic receipt */
            $monthly_earning = $this->contributions->query()->where('employee_id', $id)
            ->whereHas('receiptCode', function($query) use ($contribution_month) {
                $query->whereRaw("to_char(contrib_month, 'YYYY-MM') = '".$contribution_month."'");
            })->sum('grosspay');
        } else {
            /* before electronic receipt */
            $monthly_earning = $this->contributions->query()->where('employee_id', $id)->whereRaw("to_char(contrib_month, 'YYYY-MM') = '".$contribution_month."'")->sum('grosspay');
        }

        $monthly_earning = ($monthly_earning > 0) ?  $monthly_earning :  $notification_report->monthly_earning;

        return ['monthly_earning' => $monthly_earning, 'contrib_month' => $required_contrib_month] ;
    }

    /**
     * @param $id
     * @param $incident_date
     * @return array
     * @throws GeneralException
     * Get gross pay 1 month before or month of incident depending on datehired (dec 04 2017)
     */
    public function getMonthlyEarningBeforeIncident_old_dec2017($id,$incident_date) {
        // convert date into first day of month
        $notificationReport = new NotificationReportRepository();
        $notification_report = $notificationReport->query()->where('employee_id', $id)->where('incident_date', $incident_date)->first();
        $employer_id = $notification_report->employer_id;
        $employee = $this->findOrThrowException($id);
        $pivot = $employee->employers()->where('employer_id', $employer_id)->first()->pivot;
        $date_hired = $pivot->datehired;
        $required_contrib_month = Carbon::parse($incident_date)->subMonth(1);

        /* If Has Date hired*/
        if ($date_hired){
            /*check if incident date is same date hired*/
            if(months_diff($date_hired,$incident_date) == 0){
                $required_contrib_month = Carbon::parse($incident_date);
            }

        }
        /* find monthly earning (gross pay)*/
        $paydate_month = $required_contrib_month->format("n");
        $paydate_year = $required_contrib_month->format("Y");
        $monthly_earning = $this->contributions->query()->where('employee_id', $id)->where(function ($query) use ($paydate_month, $paydate_year) {
            $query->whereMonth('contrib_month','=', $paydate_month)->whereYear('contrib_month','=',$paydate_year);
        })->sum('grosspay');

        return ['monthly_earning' => $monthly_earning, 'contrib_month' => $required_contrib_month] ;
    }

    /*
     * Get monthly earning recent salary before the incident======
     * CONTRIBUTION AFTER JUL 2017 Mac Improved
     *  Backup -> get gross pay of most recent before incident (sep 2017)
     */
    public function getMonthlyEarningBeforeIncident_old_sep_2017($id,$incident_date) {
        // convert date into first day of month
        $incident_date = Carbon::create(Carbon::parse($incident_date)->format('Y'), Carbon::parse($incident_date)->format('m'), 1);

        $contribution = $this->contributions->query()->where('employee_id', $id)->where('contrib_month', '<',$incident_date)->first();

        if ($contribution) {
            $paydate = $contribution->contrib_month;
            $paydate = Carbon::parse($paydate);
            $paydate_month = $paydate->format("n");
            $paydate_year = $paydate->format("Y");
            $monthly_earning = $this->contributions->query()->where('employee_id', $id)->where(function ($query) use ($paydate_month, $paydate_year) {
                $query->whereMonth('contrib_month','=', $paydate_month)->whereYear('contrib_month','=', $paydate_year);
            })->sum('grosspay');
            return ['monthly_earning' => !is_null($monthly_earning) ? $monthly_earning : 0, 'contrib_month' => !is_null($paydate)  ? $paydate : null] ;
        }
    }

    /*
    * Get monthly earning recent salary before the incident======
     * CONTRIBUTION BEFORE JUL 2017
     *
    */
    public function getMonthlyEarningBeforeIncidentContributionBefore2017($id,$incident_date) {
        // convert date into first day of month
        $contributions = new ContributionRepository();
        $incident_date = Carbon::create(Carbon::parse($incident_date)->format('Y'), Carbon::parse($incident_date)->format('m'), 1);
        $contribution = $contributions->query()->where('employee_id', $id)->where('contrib_month', '<',$incident_date)->first();
        if ($contribution) {
            $paydate = $contribution->contribution_month;

            $monthly_earning_contribution = $contributions->query()->where('employee_id', $id)->whereMonth('contrib_month', '=', Carbon::parse($paydate)->format('m'))->whereYear('contrib_month', '=', Carbon::parse($paydate)->format('Y'))->first();

            $monthly_earning = $monthly_earning_contribution->salary  +  $monthly_earning_contribution->allowance;
            return ['monthly_earning' => !is_null($monthly_earning) ? $monthly_earning : 0, 'contrib_month' => $paydate ];
        }else{
            return ['monthly_earning' => 0, 'contrib_month' => null ];
        }
    }

    /**
     * @param $input
     * @throws GeneralException
     */
    public function updateBankDetails($input){
        $employee= $this->findOrThrowException($input['employee_id']);
        $employee->update(['bank_branch_id'=> (isset($input['employee_bank_branch_id'])) ? $input['employee_bank_branch_id'] : null ,'accountno'=> (isset($input['employee_accountno'])) ? $input['employee_accountno'] : null, 'bank_id'=> (isset($input['employee_bank_id'])) ? $input['employee_bank_id'] : null  ]);
    }

    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * getRegistered employees
     */
    public function getRegisterEmployees($q, $page)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = trim(preg_replace('/\s+/', '', $q));
        //$name = $q;
        $data['items'] = $this->query()
        ->select([
            'id',
            DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as employee"),
        ])
        ->whereRaw("regexp_replace(cast(concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as text), '\s+', '', 'g')  ~* ? ", [$name])
            //->whereRaw("cast(concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as text)  ~* ? ", [$name])
        ->limit($resultCount)
        ->offset($offset)
        ->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * @description get the list of registered employees for notification report registration.
     */
    public function  getRegisterEmployeesForNotification($q, $page)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = trim(preg_replace('/\s+/', '', $q));
        //$name = strtolower($q);
        $data['items'] = $this->query()
        ->select([
            DB::raw("employees.id as id"),
            DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) || ' - ( ' || employers.name || ' )' as employee"),
        ])
        ->join("employee_employer", "employees.id", "=", "employee_employer.employee_id")
        ->join("employers", "employers.id", "=", "employee_employer.employer_id")
        ->whereRaw("regexp_replace(cast(concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as text), '\s+', '', 'g')  ~* ? ", [$name])
            //->whereRaw("cast(concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as text)  ~* ?", [$name])
        ->limit($resultCount)
        ->offset($offset)
        ->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    /**
     * @param $q
     * @param $page
     * @param $employer_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRegisterEmployeesForNotificationEmployer($q, $page, $employer_id)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = trim(preg_replace('/\s+/', '', $q));
        //$name = strtolower($q);
        $data['items'] = [];
        $data['total_count'] = count($data['items']);
        if (!empty($employer_id)) {
            $data['items'] = $this->query()
            ->select([
                DB::raw("employees.id as id"),
                DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) || ' - ( ' || employers.name || ' )' as employee"),
            ])
            ->join("employee_employer", "employees.id", "=", "employee_employer.employee_id")
            ->join("employers", "employers.id", "=", "employee_employer.employer_id")
            ->whereRaw("regexp_replace(cast(concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as text), '\s+', '', 'g')  ~* ? ", [$name])
                //->whereRaw("cast(concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as text)  ~* ?", [$name])
            ->where("employee_employer.employer_id", $employer_id)
            ->limit($resultCount)
            ->offset($offset)
            ->get()->toArray();
            $data['total_count'] = count($data['items']);
        }
        return response()->json($data);
    }

    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * Get Beneficiary Employees with approved claims (workflow completed)
     */
    public function getBeneficiaryEmployees($q)
    {
        $name = strtolower($q);
        $data['items'] = $this->query()->select(['employees.id', DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as employee")])->whereRaw("(LOWER(firstname) like :firstname or LOWER(middlename) like :middlename or LOWER(lastname) like :lastname or concat_ws(' ',firstname, coalesce(middlename, ''), lastname)  like :fullname)", ['firstname' => "%$name%", 'middlename' => "%$name%", 'lastname' => "%$name%", 'fullname' => "%$name%"])->join('notification_reports', 'notification_reports.employee_id',
            'employees.id')->whereRaw
        ("(notification_reports.wf_done = 1)")->get()->toArray();

        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function getEmployers($id)
    {
        $employee = $this->findOrThrowException($id);
        $employers = $employee->employers;
        return $employers;
    }

    public function getEmployeeByEmployer($employer_id)
    {
        $employees = $this->query()->select([
            'memberno',
            DB::raw("employees.id as id"),
            DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as fullname"),
            DB::raw("firstname"),
            DB::raw("middlename"),
            DB::raw("lastname"),
            DB::raw("dob"),
            DB::raw("coalesce(code_values.name, employees.emp_cate) as category"),
        ])
        ->join("employee_employer", "employees.id", "=", "employee_employer.employee_id")
        ->leftJoin("code_values", "employee_employer.employee_category_cv_id", "=", "code_values.id")
        ->where("employee_employer.employer_id", $employer_id);
        return $employees;
    }

// ---END employee -------------------------------------

    /**  Dashboard Summary */
    /**
     * GRAPH SUMMARY
     */
    /* Get Summary of monthly registered employers */
    public function getTotalMonthlyRegisteredEmployees()
    {
        $start = Carbon::parse($this->getStartSummaryPeriod());
        $data = [];
        /* Get Monthly Target*/
        $fiscalYear = new FiscalYearRepository();
        $fiscal_year = $fiscalYear->findByFiscalYear(financial_year());
        $monthly_target =  ($fiscal_year) ? $fiscal_year->monthlyTarget('ATTEMPL') : 0;

        /* end monthly target */
        /* Get Total before new Financial Year - Open Total Amount*/
        $prev_fin_year_end = $this->getPrevEndSummaryPeriod();
        $data_table = [];
        $count_open = $this->query()->select(['id'])->has('employers')
        ->whereDate("created_at", "<=",  $prev_fin_year_end)
        ->count();

        $total_open = $count_open;
        $period_open = Carbon::parse($prev_fin_year_end)->format("M y");
        $data_table[] = [$period_open, (int) $total_open, 0, 0];
        /* end open */

        for ($x = 1; $x <= $this->getSummaryPeriodDiffs(); $x++) {
            $month = $start->format("n");
            $year = $start->format("Y");
            $count = $this->query()->select(['id'])->has('employers')
            ->whereMonth("created_at", "=",  $month)
            ->whereYear("created_at", "=", $year)
            ->count();
            $period = $start->format("M y");

            $data[] = [$period, (int) $count]; //number_format( $sum , 2 , '.' , ',' )
            $data_table[] = [$period, (int) $count, $monthly_target,
                $this->targetVariancePercentage($count,$monthly_target)];
                $start->addMonth();
            }
            $total = $this->query()->select(['id'])->has('employers')->where('created_at', '>=', $this->getStartSummaryPeriod() . ' 00:00:00')->where('created_at', '<=', $this->getEndSummaryPeriod() . ' 23:59:59')->count();

            $overall_total = $total_open + $total;
            $return['graph_employee_registered'] = $data;
            $return['table_employee_registered'] = $data_table;
            $return['total_employees_registered'] =  $overall_total;
            return $return;
        }



        /* Employee statuses summary */
        public function getEmployeeStatusSummary()
        {
            $total_active = $this->query()->select(['id'])->where('employee_status', 1)->has('employers')->count();
            $active_public = $this->query()->select(['id'])->where('employee_status', 1)->whereHas('employers', function($query){
                $query->whereHas('employerCategory', function ($sub_query) {
                    $sub_query->where('reference', 'ECPUB');});
            })->count();
            $active_private = $total_active - $active_public;
//        $active_private = $this->query()->select(['id'])->where('employee_status', 1)->whereHas('employers', function($query){
//            $query->whereHas('employerCategory', function ($sub_query) {
//                $sub_query->where('reference', 'ECPRI');});
//        })->count();
            $dormant_public = $this->query()->select(['id'])->where('employee_status', 2)->whereHas('employers', function($query){
                $query->whereHas('employerCategory', function ($sub_query) {
                    $sub_query->where('reference', 'ECPUB');});
            })->count();
            $dormant_private = $this->query()->select(['id'])->where('employee_status', 2)->whereHas('employers', function($query){
                $query->whereHas('employerCategory', function ($sub_query) {
                    $sub_query->where('reference', 'ECPRI');});
            })->count();
            $closed_public = $this->query()->select(['id'])->where('employee_status', 3)->whereHas('employers', function($query){
                $query->whereHas('employerCategory', function ($sub_query) {
                    $sub_query->where('reference', 'ECPUB');});
            })->count();
            $closed_private = $this->query()->select(['id'])->where('employee_status', 3)->whereHas('employers', function($query){
                $query->whereHas('employerCategory', function ($sub_query) {
                    $sub_query->where('reference', 'ECPRI');});
            })->count();

            $total_active_employee = $active_public + $active_private;
            $total_dormant_employee = $dormant_private + $dormant_public;
            $total_closed_employee = $closed_private + $closed_public;

            /*get percent*/
            $total = $total_active_employee + $total_dormant_employee + $total_closed_employee;
            $active_employee_percent = $this->getPercent($total_active_employee, $total);
            $dormant_employee_percent = $this->getPercent($total_dormant_employee, $total);
            $closed_employee_percent =$this->getPercent($total_closed_employee, $total);


            return ['active_public_employee' =>   $active_public  , 'active_private_employee' =>   $active_private , 'dormant_public_employee' =>   $dormant_public , 'dormant_private_employee' =>  $dormant_private , 'closed_public_employee' =>  $closed_public  , 'closed_private_employee' =>   $closed_private ,
            'total_active_employee' =>     $total_active_employee  , 'total_dormant_employee' =>     $total_dormant_employee  , 'total_closed_employee' =>    $total_closed_employee ,
            'active_employee_percent' => $active_employee_percent,
            'dormant_employee_percent' => $dormant_employee_percent,
            'closed_employee_percent' => $closed_employee_percent,];

        }

        /* End of Dashboard Summary  */


        /*Update employee details from payroll workflow update approval*/
        public function UpdateDetailsFromPayrollApprovals($employee_id, array $input)
        {
            $employee = $this->find($employee_id);
            $employee->update([
                'phone' => isset($input['phone']) ? $input['phone'] : $employee->phone,
                'email' => isset($input['email']) ? $input['email'] : $employee->email,
                'firstname' => isset($input['firstname']) ? $input['firstname'] : $employee->firstname,
                'middlename' => isset($input['middlename']) ? $input['middlename'] : $employee->middlename,
                'lastname' => isset($input['lastname']) ? $input['lastname'] : $employee->lastname,
                'dob' => isset($input['dob']) ? $input['dob'] : $employee->dob,
                'bank_id' => isset($input['bank_id']) ? $input['bank_id'] : $employee->bank_id,
                'bank_branch_id' => isset($input['bank_branch_id']) ? $input['bank_branch_id'] : $employee->bank_branch_id,
                'accountno' => isset($input['accountno']) ? $input['accountno'] : $employee->accountno,
            ]);
        }

    // get employee with its occupations (tasco)
        public function employeeForOshModule($id ,$notification_report_id) {
            $tasco = $this->query()->select([
                DB::raw("job_titles.name as occupation"),
                DB::raw("incident_date"),
                DB::raw("employee_employer.datehired"),
                DB::raw("workplaces.workplace_name"),
                DB::raw("workplaces.workplace_regno"),
            ])
            ->join("employee_employer","employee_employer.employee_id","=","employees.id")
            ->leftJoin("notification_reports", "notification_reports.employee_id", "=", "employees.id")
            ->leftJoin("workplaces", "workplaces.id", "=", "notification_reports.workplace_id")
            ->leftJoin("incident_assessments", "incident_assessments.notification_eligible_benefit_id", "=", "notification_reports.id")
            ->leftJoin("occupations", "occupations.id", "=", "incident_assessments.occupation_id")
            ->leftJoin("job_titles", "job_titles.id", "=", "employees.job_title_id")
            ->where("notification_reports.id",$notification_report_id)
            ->first();

            return $tasco;
        }

        public function getAllRemovals($id) {
            return $this->query()->select([
                DB::raw("employers.name as employer"),
                DB::raw("employee_removal_history.created_at as removed_on"),
                DB::raw("employee_removal_history.remove_attachment"),
                DB::raw("employee_removal_history.remove_reason"),
                DB::raw("employers.reg_no"),
            ])
            ->join("portal.employee_removal_history","employee_removal_history.employee_id","=","employees.id")
            ->join("employers", "employers.id", "=", "employee_removal_history.employer_id")->where('employees.id',$id)
            ->where('employee_removal_history.is_removed',true);
            
        }
    }