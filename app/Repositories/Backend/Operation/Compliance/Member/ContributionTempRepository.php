<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Repositories\BaseRepository;
use App\Models\Operation\Compliance\Member\ContributionTemp;

class ContributionTempRepository extends BaseRepository
{

    const MODEL = ContributionTemp::class;

}