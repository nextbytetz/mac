<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Models\MacErp\ErpSupplier;
use App\Models\Operation\Compliance\Member\Dependent;
use App\Models\Operation\Compliance\Member\DependentType;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Payroll\ManualPayrollMemberRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollVerificationRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DependentRepository extends  BaseRepository
{

    const MODEL = Dependent::class;
    protected $employees;



    public function __construct()
    {
        $this->employees = new EmployeeRepository();
    }

//find or throwException for dependent
    public function findOrThrowException($id)
    {
        $dependent = $this->query()->find($id);

        if (!is_null($dependent)) {
            return $dependent;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.dependent_not_found'));
    }


    public function checkGender($input){

    }


    /*
     * create new
     */

    public function create($employee_id, $input) {
        return DB::transaction(function () use ($employee_id,$input) {
            /*Dependent type exists check unique*/
            $this->checkIfUniqueDependentTypeExists($employee_id,$input);

            $dependent = $this->query()->create([
                'firstname' => trim(preg_replace('/\s+/', '', $input['first_name']))  ,
                'middlename' =>trim(preg_replace('/\s+/', '', $input['middle_name']))  ,
                'lastname' => trim(preg_replace('/\s+/', '', $input['last_name'])) ,
                'gender_id' => $input['gender_type'],
                'dob' => standard_date_format($input['dependent_dob']),
                'identity_id' => $input['identity'],
                'id_no' => ($input['identity'] <> 5) ? $input['id_no']  : 0,
                'country_id' => $input['country'],
                'address' => isset($input['address_1']) ? $input['address_1'] : null,
                'location_type_id' => $input['location_type'],
                'unsurveyed_area' =>isset($input['unsurveyedarea']) ? $input['unsurveyedarea'] : null,
                'street' => isset($input['street_name']) ? $input['street_name'] : null,
                'road' => isset($input['road_name']) ? $input['road_name'] : null,
                'plot_no' =>  isset($input['plotno']) ? $input['plotno'] : null,
                'block_no' => isset($input['blockno']) ? $input['blockno'] : null,
                'surveyed_extra' => isset($input['surveyedextra']) ? $input['surveyedextra'] : null,
                'phone' => isset($input['phone_1']) ? $input['phone_1'] : null,
                'telephone' =>isset($input['tele_phone']) ? $input['tele_phone'] : null,
                'email' => isset($input['email_address']) ? $input['email_address'] : null,
                'fax' => isset($input['fax_no']) ? $input['fax_no'] : null,
                'bank_branch_id' => array_key_exists('bank_branch', $input) ? (($input['bank_branch']) ? $input['bank_branch'] : null ) : null,
                'bank_id' => array_key_exists('bank', $input) ? (($input['bank']) ? $input['bank'] : null ) : null,
                'accountno' => isset($input['accountno_dependent']) ? $input['accountno_dependent'] : null,
                'lastresponse' => Carbon::parse('now')->format('Y-m-d'),
                'isdisabled' => isset($input['isdisabled'] ) ? $input['isdisabled'] : 0,
                'iseducation' => isset($input['iseducation'] ) ? $input['iseducation'] : 0,
                'birth_certificate_no' => array_key_exists('birth_certificate_no', $input) ? $input['birth_certificate_no'] : 0,
                'firstpay_manual' => 0,
            ]);

            $this->syncWithEmployee($dependent,$employee_id,$input);
            $this->processPensionPercentDistribution($employee_id);
            $this->processFuneralGrantsDistribution($employee_id);
            $this->checkIfPercentagesExceed100($employee_id);
            /*Update if cca is registered*/
            $this->updateCcaRegistered($dependent, $employee_id, $input['dependent_type']);
            return $dependent;
        });
    }

    /**
     * @param Model $incident
     * @return array
     */
    public function checkHasMandatoryDocument(Model $incident)
    {
        $return = ['success' => true, 'message' => ''];
        $countSpouseQuery = $this->query()->whereHas('employees', function ($query) use ($incident) {
            $query->where([
                'employee_id' => $incident->employee_id,
                //'notification_report_id' => $incident->id,
            ])->whereIn('dependent_type_id', [1,2]);
        }); //1 => Husband, 2 => Wife ...
        //logger($countSpouseQuery->toSql());
        $countSpouse = $countSpouseQuery->count();
        //logger("Spouse : {$countSpouse}");
        if ($countSpouse) {
            //count Wife or Husband, check marriage certificates ...
            $countDocs = $incident->documents()->where('document_notification_report.document_id', 44)->count();
            //logger("SpouseDocs : {$countDocs}");
            if ($countDocs < $countSpouse) {
                $return = ['success' => false, 'message' => "Please check and ensure that {$countSpouse} marriage certificate(s) document has been uploaded"];
            }
        }
        if (!$countSpouse) {
            $countChild = $this->query()->whereHas('employees', function ($query) use ($incident) {
                $query->where([
                    'employee_id' => $incident->employee_id,
                    //'notification_report_id' => $incident->id,
                ])->whereIn('dependent_type_id', [3]);
            })->count(); //3 => Child ...
            //logger("Children : {$countChild}");
            if ($countChild) {
                //count Child , count birth certificates ...
                $countDocs = $incident->documents()->where('document_notification_report.document_id', 25)->count();
                //logger("ChildrenDocs : {$countDocs}");
                if ($countDocs < $countChild) {
                    $return = ['success' => false, 'message' => "Please check and ensure that {$countChild} birth certificate(s) document has been uploaded"];
                }
            }
        }
        return $return;
    }

    /**
     * Sync with employee on PIVOT Table
     */
    public function syncWithEmployee(Model $dependent,$employee_id, $input)
    {

        $dependent->employees()->syncWithoutDetaching([$employee_id => [
            'dependent_type_id' => $input['dependent_type'],
            'parent_id' => array_key_exists('parent_id', $input) ? (($input['parent_id']) ? $input['parent_id'] : null ) : null,
            'survivor_pension_percent' => array_key_exists('survivor_pension_percent', $input) ? (($input['survivor_pension_percent']) ? $input['survivor_pension_percent'] : 0 ) : 0,
            'survivor_pension_flag' => array_key_exists('survivor_pension_receiver', $input) ? (($input['survivor_pension_receiver']) ? $input['survivor_pension_receiver'] : (isset($input['other_dependency_type']) ? 1 : 0)) : (isset($input['other_dependency_type']) ? 1 : 0),
            'survivor_gratuity_flag' => array_key_exists('survivor_gratuity_receiver', $input) ? (($input['survivor_gratuity_receiver']) ? $input['survivor_gratuity_receiver'] : 0 ) : 0,
//            'funeral_grant_percent' => array_key_exists('funeral_grant_percent', $input) ? (count($input['funeral_grant_percent']) ? $input['funeral_grant_percent'] : 0 ) : 0,
//            'funeral_grant' => array_key_exists('funeral_grant_percent', $input) ?  (count($input['funeral_grant_percent']) ? $input['funeral_grant_percent'] : 0) * (sysdefs()->data()->funeral_grant_amount) * 0.01 : 0,
            'dependency_percentage' => array_key_exists('dependency_percentage', $input) ? (($input['dependency_percentage']) ? $input['dependency_percentage'] : null ) : null,
            'isotherdep' => isset($input['other_dependency_type']) ? 1 : 0,
            'other_dependency_type' => isset($input['other_dependency_type']) ? $input['other_dependency_type'] : null,
            'pay_period_months' => isset($input['other_dependency_type']) ?   $this->getPayablePeriodMonthsByOtherDependencyType($input['other_dependency_type'])  : null,
            'recycles_pay_period' => isset($input['other_dependency_type']) ?   $this->getPayablePeriodMonthsByOtherDependencyType($input['other_dependency_type'])  : null,
        ]]);
    }

    /**
     * @param $other_dep_type
     * Get payable months period depending on other dependency type
     */
    public function getPayablePeriodMonthsByOtherDependencyType($other_dep_type)
    {
        switch($other_dep_type){
            /*Full*/
            case 1:
                return sysdefs()->data()->max_payable_months_otherdep_full;
                break;
            /*Partial*/
            case 2:
                return sysdefs()->data()->max_payable_months_otherdep_partial;
                break;

            default:
                return  null;
                break;
        }
    }

    /**
     * Update Constant care assistant registered and claim table flag to specify if cca already registered
     */
    public function updateCcaRegistered(Model $dependent, $employee_id, $dependent_type)
    {
        $pensioner = (new PensionerRepository())->query()->where('employee_id', $employee_id)->orderBy('id', 'desc')->first();
        if($dependent_type == 7 && isset($pensioner) )
        {
            /*When Constant care is registered*/
            $dependent_pivot = $dependent->employees()->where('employee_id', $employee_id)->first()->pivot;
            $assistant_percent = sysdefs()->data()->assistant_percent;
            $pensioner_pension = $pensioner->monthly_pension_amount;
            $assistant_mp  = $pensioner_pension * (0.01 * $assistant_percent) ;

            /*Update dependent pivot table - Payment  pension details*/
            $dependent->employees()->syncWithoutDetaching([$employee_id => [
                'survivor_pension_amount' =>$assistant_mp,
                'survivor_pension_percent' =>$assistant_percent,
                'notification_report_id' => $pensioner->notification_report_id
            ]]);

            /*update claim flag -- cca registered*/
            $claim = $pensioner->claim;
            if($claim){
                //update if system file
                $claim->update(['cca_registered' => 1]);
            }

        }

    }



    /*
     * update
     */
    public function update($id, $input) {
        return DB::transaction(function () use ($id,$input) {

            $dependent = $this->editDependent($id, $input);
            /*Synch with pivot table*/
            $employee_id = $input['employee_id'];
            $this->syncWithEmployee($dependent,$employee_id,$input);
            $this->processPensionPercentDistribution($employee_id);
            $this->processFuneralGrantsDistribution($employee_id);
            $this->checkIfPercentagesExceed100($employee_id);
            /*Update if cca is registered*/
            $this->updateCcaRegistered($dependent, $employee_id, $input['dependent_type']);
            return $dependent;
        });
    }

    /*Edit dependent details*/
    public function editDependent($id, array $input){
        return DB::transaction(function () use ($id,$input) {
            /*Dependent type exists check unique*/
            $this->checkIfUniqueDependentTypeExists($input['employee_id'],$input);

            $dependent = $this->findOrThrowException($id);
            $dependent->update([
                'firstname' => trim(preg_replace('/\s+/', '', $input['first_name']))  ,
                'middlename' =>trim(preg_replace('/\s+/', '', $input['middle_name']))  ,
                'lastname' => trim(preg_replace('/\s+/', '', $input['last_name'])) ,
                'gender_id' => $input['gender_type'],
                'dob' => standard_date_format($input['dependent_dob']),
                'identity_id' => $input['identity'],
                'id_no' => ($input['identity'] <> 5) ? $input['id_no']  : 0,
                'country_id' => $input['country'],
                'address' => $input['address_1'],
                'location_type_id' => $input['location_type'],
                'unsurveyed_area' => $input['unsurveyedarea'],
                'street' => $input['street_name'],
                'road' => $input['road_name'],
                'plot_no' => $input['plotno'],
                'block_no' => $input['blockno'],
                'surveyed_extra' => $input['surveyedextra'],
                'phone' => $input['phone_1'],
                'telephone' => $input['tele_phone'],
                'email' => $input['email_address'],
                'fax' => $input['fax_no'],
                'bank_id' => array_key_exists('bank', $input) ? (($input['bank']) ? $input['bank'] : null ) : null,
                'bank_branch_id' => array_key_exists('bank_branch', $input) ? (($input['bank_branch']) ? $input['bank_branch'] : null ) : null,
                'accountno' => $input['accountno_dependent'],
                'lastresponse' => Carbon::parse('now')->format('Y-m-d'),
                'isdisabled' => isset($input['isdisabled'] ) ? $input['isdisabled'] : 0,
                'iseducation' => isset($input['iseducation'] ) ? $input['iseducation'] : 0,
                'birth_certificate_no' => array_key_exists('birth_certificate_no', $input) ? $input['birth_certificate_no'] : null,
            ]);


            return $dependent;
        });
    }

    /*Check if can edit dependent exception*/
    public function checkIfCanEditDependentException($dependent_id, $employee_id)
    {
        $dependent = $this->find($dependent_id);
        $dependent_employee_pivot = $dependent->employees()->where('employee_id', $employee_id)->first()->pivot;
        if($dependent_employee_pivot->isactive > 0){
            throw new GeneralException('This dependent is already enrolled on payroll can not be edited from this menu. Please check!');
        }

    }

    /**
     * @param $category
     * Get array of dependent types y category
     */
    public function getDependentTypesArrayByCategory($category)
    {
        switch($category){

            case 'spouse':
                return [1,2];
                break;
            case 'child':
                return [3];
                break;
            case 'parent':
                return [4,5];
                break;
            case 'other':
                return [8,9,10,11,12,13];
                break;
            case 'estate':
                return [7];
                break;

        }
    }

    /**
     * @param $employee_id
     * Merge dependents who exist on the system but paid through manual process
     */
    public function mergeManualDependent($employee_id, array $input)
    {
        return DB::transaction(function () use ($employee_id,$input) {
            $dependent = $this->find($input['dependent_id']);
//            $this->editDependent($dependent->id, $input);
            $this->update($dependent->id, $input);
            $ispaid = isset($input['ispaid']) ? $input['ispaid'] : 0;
            $notification_report = $this->findNotificationReport($input['dependent_id'], $employee_id);
            /*Other dep*/
            $payable_months_data = (new ManualPayrollMemberRepository())->getPayableMonthsData($input);
            $recycles_pay_period = $payable_months_data['recycles_pay_period'];
            $isotherdep = isset($input['isotherdep']) ? 1 : 0;
//            $isactive = ($isotherdep == 1 && $recycles_pay_period == 0) ? 2 : 0;
            /*end other dep*/
            $dependent->employees()->syncWithoutDetaching([$employee_id => [
                'survivor_pension_amount' =>isset($input['mp'])  ? str_replace(",", "", $input['mp']) : null,
                'firstpay_flag' => $ispaid,
                'isactive' => 0,
                'survivor_pension_flag' => 1,
                'notification_report_id' => $notification_report->id,
                'isresponded' => 0,
                'other_dependency_type' => ($isotherdep == 1) ? 1 : null,
                'isotherdep' => $isotherdep,
                'pay_period_months' =>sysdefs()->data()->max_payable_months_otherdep_full,
                'recycles_pay_period' =>($isotherdep == 1)  ? $recycles_pay_period : null,
            ]]);
            /*Update firstpay manual*/
            $dependent->update(['firstpay_manual' => $ispaid]);
            return $dependent;
        });
    }

    /*Check if unique dependent type exists*/
    public function checkIfUniqueDependentTypeExists($employee_id,array $input)
    {
        $dependent_type_id = $input['dependent_type'];
        $unique_dependent_types_arr = (new DependentTypeRepository())->uniqueDependentTypeIds();
        /*check if dependent_type in unique dependent type array*/
        if(in_array($dependent_type_id, $unique_dependent_types_arr)){

            $dependent_type = DependentType::query()->find($dependent_type_id);
            if(isset($input['dependent_id'])){
                /*On edit*/
                $dependent = $this->find($input['dependent_id']);
                $dependent_employee = $dependent->getDependentEmployee($employee_id);
                if($dependent_employee->dependent_type_id == $dependent_type_id && $dependent_employee->dependent_id != $dependent->id){
                    throw new GeneralException($dependent_type->name . ' already exists for this employee! Please check!');
                }
            }else{
                /*on add new*/
                $check = DB::table('dependent_employee')->where('dependent_type_id', $dependent_type_id)->where('employee_id', $employee_id)->count();

                if($check > 0){
                    throw new GeneralException($dependent_type->name . ' already exists for this employee! Please check!');
                }
            }

        }

    }

    public function updateManualPayStatus($id, $employee_id, $status){
        return DB::transaction(function () use ($id, $employee_id, $status) {
            $dependent = $this->find($id);
            switch ($status){
                case 0:
                    /*Not yet paid*/
                    $dependent->update(['firstpay_manual' => 0]);
                    break;
                case 1:
                    /*Already paid*/
                    break;
                case 2:
                    /*Already paid and terminated already*/
                    $dependent->update(['firstpay_manual' => 1]);
                    $dependent->employees()->syncWithoutDetaching([$employee_id => [
                        'isactive' => 2,
                        'firstpay_flag' => 1
                    ]]);
                    break;

                case 3:
                    /*Reset status*/
                    $dependent->update(['firstpay_manual' => 2]);
                    break;
            }
        });

    }




    /**
     * @param Model $pensioner
     * @param $notification_report_id
     * Get Monthly pension per notification specified
     */
    public function getMonthlyPensionPerNotification($id, $employee_id)
    {
        $dependent = $this->find($id);
        $mp = $dependent->employees()->where('employee_id', $employee_id)->first()->pivot->survivor_pension_amount;
        return $mp;
    }

    /**
     * @param Model $pensioner
     * @param $notification_report_id
     * Get Monthly pension from all active notification reports
     */
    public function getTotalMonthlyPension($id)
    {
        $dependent = $this->find($id);
        $dependent_employees = $dependent->employees()->where('isactive', 1)->get();
        $mp = 0;
        foreach ($dependent_employees as $dependent_employee){
            $mp = $mp + $dependent_employee->pivot->survivor_pension_amount;
        }
        return $mp;
    }





    /*Get dependents belong to employee*/
    public function dependentsWithPivot($employee_id)
    {
        $dependent_employee = $this->dependentAllWithPivot()->where("dependent_employee.employee_id", $employee_id);

        return $dependent_employee;
    }


    /*Get all dependents with pivot */
    public function dependentAllWithPivot()
    {
        $dependent_employee = $this->employees->query()->select([
            DB::raw("INITCAP(concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname)) as fullname"),
            DB::raw("INITCAP(concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname)) as employee_name"),
            DB::raw("dependent_types.name as type_name"),
            DB::raw("dependent_type_id"),
            DB::raw("survivor_pension_flag"),
            DB::raw("survivor_pension_percent"),
            DB::raw("survivor_pension_amount"),
            DB::raw("survivor_gratuity_flag"),
            DB::raw("survivor_gratuity_pay_flag"),
            DB::raw("survivor_gratuity_amount"),
            DB::raw("survivor_gratuity_percent"),
            DB::raw("funeral_grant_percent"),
            DB::raw("funeral_grant"),
            DB::raw("funeral_grant_pay_flag"),
            DB::raw("dependency_percentage"),
            DB::raw("suspense_flag"),
            DB::raw("isactive"),
            DB::raw("dependents.isdisabled"),
            DB::raw("dependents.iseducation"),
            DB::raw("isresponded"),
            DB::raw("dependent_employee.firstpay_flag as firstpay_flag"),
            DB::raw("dependent_employee.parent_id as parent_id"),
            DB::raw("dependent_employee.dependent_id as dependent_id"),
            DB::raw("dependent_employee.employee_id as employee_id"),
            DB::raw("dependent_employee.notification_report_id as notification_report_id"),
            DB::raw("dependent_employee.manual_notification_report_id as manual_notification_report_id"),
            DB::raw("notification_reports.filename as filename"),
            DB::raw("notification_reports.incident_date as incident_date"),
            DB::raw("notification_reports.incident_type_id as incident_type_id"),
            DB::raw("dependents.firstpay_manual as firstpay_manual"),
            DB::raw("manual_notification_reports.case_no as case_no"),
            DB::raw("dependents.firstname"),
            DB::raw("dependents.middlename"),
            DB::raw("dependents.lastname"),
            DB::raw("dependents.dob as dob"),
            DB::raw("dependents.lastresponse as lastresponse"),
            DB::raw("employees.firstname as emp_firstname"),
            DB::raw("employees.middlename as emp_middlename"),
            DB::raw("employees.lastname as emp_lastname"),
            DB::raw("dependent_employee.id as pivot_id"),
            DB::raw("dependents.bank_branch_id as bank_branch_id"),
            DB::raw("dependents.bank_id as bank_id"),
            DB::raw("dependents.accountno as accountno"),
            DB::raw("notification_eligible_benefits.processed as processed"),
            DB::raw("claims.date_of_mmi as date_of_mmi")
        ]) ->join("dependent_employee", "dependent_employee.employee_id", "employees.id")
            ->join("dependents", "dependent_employee.dependent_id", "dependents.id")
            ->join("dependent_types", "dependent_types.id", "dependent_employee.dependent_type_id")
            ->leftjoin('notification_reports', 'notification_reports.id', 'dependent_employee.notification_report_id')
            ->leftjoin('manual_notification_reports', 'manual_notification_reports.id', 'dependent_employee.manual_notification_report_id')
            ->leftjoin('claims', 'claims.notification_report_id', 'notification_reports.id')
            ->leftjoin('notification_eligible_benefits', 'notification_eligible_benefits.id', 'dependent_employee.notification_eligible_benefit_id');

        return $dependent_employee;
    }

    /**
     * Get all pensionable dependents who are active and enrolled in payroll
     */
    public function getPensionableDependentsForDataTable(){
        return $this->dependentAllWithPivot()->where('dependent_employee.isactive','<>' ,0)->where('dependent_employee.survivor_pension_percent', '>', 0);
    }



    /**
     * Get all dependents who are active in payroll
     */
    public function getActiveDependentsForDataTable(){
        return $this->dependentAllWithPivot()->where('dependent_employee.isactive', 1);
    }


    /**
     * Get pensioners who are ready to be enrolled into payroll
     */
    public function getReadyForPayrollForDataTable()
    {

        $status_active_cv_id = (new CodeValueRepository())->query()->where('reference', 'PSCACT')->first()->id;
        return $this->dependentAllWithPivot()->where('dependent_employee.isactive', 0)->where('survivor_pension_amount','>', 0)->where(function($query){
            $query->whereNotNull('dependent_employee.notification_report_id')->orWhereNotNull('dependent_employee.manual_notification_report_id');
        })  ->leftJoin('payroll_status_changes as ps',function($join) use($status_active_cv_id) {
            $join->on('ps.resource_id', 'dependents.id')->where('ps.member_type_id', 4)->where('ps.status_change_cv_id',$status_active_cv_id)->whereNull('ps.deleted_at');
        })->whereNull('ps.id');

        /*retrieve only assigned on inbox task*/
        $input = request()->all();
        if(isset($input['isinbox_task'])){
            $dependents = $dependents->join('payroll_alert_tasks',function($join){
                $join->on('payroll_alert_tasks.resource_id','dependent_employee.dependent_id')->where('payroll_alert_tasks.member_type_id', 4);
            })->where('payroll_alert_tasks.user_id', access()->allUsers());
        }
        /*end payroll alert tasks*/
        return $dependents;

    }
    /**
     * Get pensioners who are ready to be enrolled into payroll - merge of manual files to system
     */
    public function getReadyForPayrollForManualMergeForDataTable()
    {
        return $this->dependentAllWithPivot()->where('dependent_employee.isactive', 0)->whereNotNull('dependent_employee.manual_notification_report_id')->where('survivor_pension_amount','>', 0)->where('dependents.firstpay_manual', 2);

    }


    /**
     * Check if in dependents list there is spouse(s)
     */
    public function checkIfHasSpouses($employee_id)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        $spouse = $employee->dependents()->where('employee_id', $employee_id)->where(function($query){
            $query->where('dependent_type_id', 1)->orWhere('dependent_type_id', 2);
        })->get();

        return $spouse;
    }






    //Rectify survivor percent when is pension flag is removed when updating
    public function rectifySurvivorPercent($id){
        $dependent  = $this->findOrThrowException($id);
        if($dependent->survivor_pension_flag == 0){
            $dependent->update(['survivor_pension_percent' => 0,'survivor_gratuity_percent' => 0]);
        }
    }


    //Revert Other dependents Survivor pension / gratuity amount distribution
    public function revertOtherDependentsDistributionOld($employee_id){
        $employee = $this->employees->findOrThrowException($employee_id);
        $other_dependents = $employee->dependents()->where('dependency_percentage','>', 0)->get();
        foreach($other_dependents as $other_dependent){

            $employee->dependents()->syncWithoutDetaching([$other_dependent->id => ['survivor_pension_percent' => 0,'survivor_gratuity_percent' => 0, 'survivor_pension_amount' => 0,'survivor_gratuity_amount' => 0,'survivor_pension_flag' => 0]]);
        }
    }

    /*Revert other dependent pension and Gratuity*/
    public function revertOtherDependentPensionAndGratuity($dependent_id, $employee_id)
    {
        $dependent_employee = ($this->find($dependent_id))->getDependentEmployee($employee_id);
        if($dependent_employee->other_dependency_type == 1){
            /*remove gratuity allocation - FULL (Receive pension)*/
            DB::table('dependent_employee')->where('id', $dependent_employee->id)->update([
                'survivor_gratuity_flag' => 0,
                'survivor_gratuity_percent' => 0,
                'survivor_gratuity_amount' => 0,
            ]);
        }else{
            /*Partial (receive gratuity) - remove pension*/
            DB::table('dependent_employee')->where('id', $dependent_employee->id)->update([
                'survivor_pension_flag' => 0,
                'survivor_pension_percent' => 0,
                'survivor_pension_amount' => 0
            ]);
        }
    }


    /*Revert other dependents pension/ gratuity distribution*/
    public function revertOtherDependentsDistribution($employee_id){
        $employee = $this->employees->findOrThrowException($employee_id);
        $other_dependents = $employee->dependents()->where('isotherdep', 1)->get();
        foreach($other_dependents as $other_dependent){

            $employee->dependents()->syncWithoutDetaching([$other_dependent->id => ['survivor_pension_percent' => 0,'survivor_gratuity_percent' => 0, 'survivor_pension_amount' => 0,'survivor_gratuity_amount' => 0,'survivor_pension_flag' => 0]]);
        }
    }

    /*Revert funeral grants distribution when adding or editing dependent*/
    public function revertFuneralGrantsDistribution($employee_id){
        DB::table('dependent_employee')->where('employee_id', $employee_id)->update(['funeral_grant_percent' => 0, 'funeral_grant' => 0]);
    }


    /**
     * @param $id
     * @param $input
     * Update Gratuity
     */
    public function updateGratuity($id,$amount, $employee_id, $notification_report_id, $eligible_id = null) {
        $dependent = $this->findOrThrowException($id);
        $dependent->employees()->syncWithoutDetaching([$employee_id =>['survivor_gratuity_amount' => $amount,
            'notification_report_id' => $notification_report_id, 'notification_eligible_benefit_id' => $eligible_id]]);
    }

    /**
     * @param $id
     * @param $amount
     * @param $employee_id
     * Update gratuity flag
     */
    public function updateGratuityPayFlag($id,$flag, $employee_id) {
        $dependent = $this->findOrThrowException($id);
        $dependent->employees()->syncWithoutDetaching([$employee_id =>['survivor_gratuity_pay_flag' => $flag]]);
    }


    /**
     * @param $id
     * @param $flag
     * @param $employee_id
     * Update Funeral grant flag
     */
    public function updateFuneralPayFlag($id,$flag, $employee_id) {
        $dependent = $this->findOrThrowException($id);
        $dependent->employees()->syncWithoutDetaching([$employee_id =>['funeral_grant_pay_flag' => $flag]]);
    }


    /**
     * @param $id
     * @param $input
     * Update Monthly Pension
     */
    public function updateMP($id,$amount, $employee_id, $notification_report_id, $eligible_id = null) {
        $dependent = $this->findOrThrowException($id);
        $dependent->update(['firstpay_manual' => 0]);
        $dependent->employees()->syncWithoutDetaching([$employee_id =>['survivor_pension_amount' => $amount,
            'notification_report_id' => $notification_report_id, 'notification_eligible_benefit_id' => $eligible_id ]]);
    }


    /**
     * @param $id
     * @param $amount
     * @param $employee_id
     * @param $notification_report_id
     * Update FirstPayFlag
     */
    public function updateFirstPayFlag($id, $employee_id, $flag) {
        $dependent = $this->findOrThrowException($id);
        $dependent->employees()->syncWithoutDetaching([$employee_id => ['firstpay_flag' => $flag,
        ]]);
    }




    /**
     * @param $dependent
     * Activate for Monthly Payroll
     * Need update when developing Payroll administration
     */
    public function activateForPayroll($id,$employee_id){
        $dependent = $this->findOrThrowException($id);
        $dependent->employees()->syncWithoutDetaching([$employee_id =>['isactive'=> 1, 'isresponded'=> 1]]);
    }


    /*
 * Update Suspense flag after payroll is approved for reinstated dependents only
 *
 */
    public function updateSuspenseFlagAfterReinstatedPayroll(Model $dependent)
    {
        $dependent->update([
            'suspense_flag' => 0,
        ]);
    }


    /**
     * @param $id
     * @param $input
     * Update Survivor Pension Percent
     */
    public function updateSurvivorPensionPercent($dependent,$percent,$employee_id) {

        $dependent->employees()->syncWithoutDetaching([$employee_id =>['survivor_pension_percent' => $percent, 'survivor_pension_flag' => ($percent > 0 ? 1 : 0)]]);
    }


    /**
     * @param $id
     * @param $input
     * Update Survivor Gratuity Percent
     */
    public function updateSurvivorGratuityPercent($dependent,$percent, $employee_id) {
        $dependent->employees()->syncWithoutDetaching([$employee_id =>['survivor_gratuity_percent' => $percent, 'survivor_gratuity_flag' => ($percent > 0 ? 1 : 0)]]);
    }


    /**
     * @param $id
     * @param $input
     * Update Funeral Percent
     */
    public function updateFuneralGrantPercent($dependent,$percent,$employee_id) {
        $dependent->employees()->syncWithoutDetaching([$employee_id =>['funeral_grant_percent' => $percent, 'funeral_grant' =>($percent * 0.01 * (sysdefs()->data()->funeral_grant_amount)),],
        ]);

    }



    /*
* Find Notification Report for dependents and Assistant
*/
    public function findNotificationReport($id, $employee_id){
        $employee = $this->employees->findOrThrowException($employee_id);
        $dependent = $this->findOrThrowException($id);
        $pivot = $dependent->employees()->where('employee_id', $employee_id)->first()->pivot;
        //if assistant for PTD

        if ($pivot->dependent_type_id == 7){
            $notification_report = $employee->notificationReports()->whereHas('claim', function($query){
                $query->where('pd',100);
            })->first();

        }else{
            $notification_report = $employee->notificationReports()->whereIn('incident_type_id',[3,4,5])->where('status', '<>', 2) ->first();
        }


        return $notification_report;
    }


    /**
     * @param $id
     * @param $employee_id
     * @return mixed
     * @throws GeneralException
     * Get notification report for enrolled dependent i.e. Get manual notification or System notification
     */
    public function findNotificationReportEnrolledDependent($id, $employee_id)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        $dependent = $this->findOrThrowException($id);
        $pivot = $dependent->employees()->where('employee_id', $employee_id)->first()->pivot;
        $notification_report = null;
        if (isset($pivot->notification_report_id)) {
            /*system notification*/
            $notification_report = (new NotificationReportRepository())->find($pivot->notification_report_id);
        }elseif(isset($pivot->manual_notification_report_id)){
            /*manual notification*/
            $notification_report = (new ManualNotificationReportRepository())->find($pivot->manual_notification_report_id);
        }
        return $notification_report;
    }
    /**
     * @param $id
     * @param $input
     * Find active assistant care dependent
     */
    public function findActiveAssistant($employee_id) {
        $dependent = $this->dependentAllWithPivot()->where('dependent_employee.employee_id', $employee_id)->where('dependent_employee.survivor_pension_flag', 1)->where('dependent_type_id', 7)->first();
        return $dependent;
    }


    /**
     * Get Employees eligible for Monthly Payroll who have dependents. i.e Death Incident(Survivor grant) or Pd = 100.
     */
    public function getEligibleForPayroll($run_date){
        $employees = $this->employees->query()
            ->whereHas('notificationReports', function($query) use($run_date){
                $query->where('incident_date','<', $run_date)->where(function($subQuery){
                    $subQuery->whereIn('incident_type_id', [3,4,5])->orWhereHas('benefits', function($query){
                        $query->where('processed',1)->where('benefit_type_claim_id', 7);
                    })->orWhereHas('claim', function($subQuery){
                        $subQuery->where('pd', 100);
                    });
                });
            });

        return $employees;
    }



    /**
     * Get Employees /deceased eligible for Monthly Payroll who have dependents. i.e Death Incident or Pd = 100.
     */
    public function getEmployeesWithActiveDependentsForPayroll($run_date){
        $run_date = standard_date_format($run_date);

        /*<New> to accommodate manual processed*/
        $employees = $this->employees->query()->whereHas('dependents', function ($query){
            $query->where('isactive', 1)->where('survivor_pension_amount', '>', 0)->where('suspense_flag', '<>', 1);
        });

        return $employees;
    }

    public function getEmployeesWithSuspendedDependentsForPayroll($run_date){
        $run_date = standard_date_format($run_date);

        /*<New> to accommodate manual processed*/
        $employees = $this->employees->query()->whereHas('dependents', function ($query){
            $query->where('isactive', 1)->where('survivor_pension_amount', '>', 0)->where('suspense_flag', 1);
        });
        return $employees;
    }



    /*Query for flags for eligible for payroll*/
    public function queryEligibleForPayroll()
    {

        return $this->dependentAllWithPivot()->where('dependent_employee.isactive',1);

    }


    /* Get query string of eligible dependents for payroll*/
    public function getEligibleDependentsForPayroll($run_date = null)
    {
        $run_date = ($run_date == null) ? Carbon::now()->endOfMonth() : $run_date;
        $run_date = standard_date_format($run_date);
        return $this->queryEligibleForPayroll()->where('suspense_flag','<>', 1)->where('survivor_pension_amount', '>', 0)->whereRaw("coalesce(dependent_employee.deadline_date, ?) >= ?", [$run_date, $run_date])->whereRaw("(coalesce(dependent_employee.recycles_pay_period, 1) > 0)");
    }

    /* Get query string of eligible suspended dependents for payroll*/
    public function getEligibleSuspendedDependentsForPayroll($run_date = null)
    {
        $run_date = ($run_date == null) ? Carbon::now()->endOfMonth() : $run_date;
        $run_date = standard_date_format($run_date);
        return $this->queryEligibleForPayroll()->where('suspense_flag', 1)->where('survivor_pension_amount', '>', 0)->whereRaw("coalesce(dependent_employee.deadline_date, ?) >= ?", [$run_date, $run_date])->whereRaw("(coalesce(dependent_employee.recycles_pay_period, 1) > 0)");
    }


    /* Get query string of eligible survivors for payroll*/
    public function getEligibleSurvivorsForPayroll($run_date = null)
    {
        return $this->getEligibleDependentsForPayroll($run_date)->where('dependent_type_id', '<>', 7);
    }
    /* Get query string of eligible suspended survivors for payroll*/
    public function getEligibleSuspendedSurvivorsForPayroll($run_date = null)
    {
        return $this->getEligibleSuspendedDependentsForPayroll($run_date)->where('dependent_type_id', '<>', 7);
    }

    /* Get query string of eligible constant care assistant for payroll*/
    public function getEligibleConstantCareForPayroll($run_date = null)
    {
        return $this->getEligibleDependentsForPayroll($run_date)->where('dependent_type_id', '=', 7);
    }
    /* Get query string of eligible suspended constant care assistant for payroll*/
    public function getEligibleSuspendedConstantCareForPayroll($run_date = null)
    {
        return $this->getEligibleSuspendedDependentsForPayroll($run_date)->where('dependent_type_id', '=', 7);
    }

    /* Get query string of eligible constant care assistant for payroll*/
    public function getEligibleChildrenOverageForPayroll()
    {
        return $this->dependentAllWithPivot()->where('firstpay_flag', 0)->where('is_overage_eligible', 1);
    }

    /**
     * Get Reinstated Dependents for payroll
     */
    public function getReinstatedForPayroll(){
        $dependents = $this->queryEligibleForPayroll()->where('suspense_flag', 3);
        return $dependents;
    }


    /**
     * @return mixed
     * Get all eligible pensioners for system payroll reconciliations
     */
    public function getEligibleForReconciliation()
    {

        $dependent_ids_with_not_pending_recovery = $this->query()->where('firstpay_manual', 0)->whereHas('employees', function($query){
            $query->where('isactive',1)->where('firstpay_flag', 1);
        })->whereDoesntHave('payrollRecoveries', function($query){
            $query->where('member_type_id', 4)->where('wf_done',0 );
        })->get()->pluck('id');
        $dependents_to_reconcile = DB::table('dependent_employee')->whereIn('dependent_id',$dependent_ids_with_not_pending_recovery);
        return $dependents_to_reconcile;
    }

    /**
     * Get eligible child dependents for age limit suspension
     */
    public function getChildDependentsForSuspension()
    {
//        return  $this->query()->whereHas('employees', function($query){
//            $query->where('dependent_type_id', 3)->where('isactive', 1);
//        })->where('isdisabled', 0)->where('suspense_flag',0);
        $dep_query = DB::table('dependent_employee')
            ->join('dependents', 'dependent_employee.dependent_id', 'dependents.id')
            ->join('payroll_beneficiaries_view as b', function($join){
                $join->on('b.resource_id', 'dependents.id')->where('b.member_type_id', 4)->whereRaw("b.employee_id = dependent_employee.employee_id");
            });
        return  $dep_query->where('dependent_employee.dependent_type_id', 3)->where('dependent_employee.isactive', 1)->where('dependents.suspense_flag', '<>', 1);
//        return  $this->dependentAllWithPivot()->where('dependent_type_id', 3)->where('dependent_employee.isactive', 1)->where('dependents.isdisabled', 0)->where('dependents.suspense_flag',0);
    }



    /**
     * @param $employee_id
     * @param $input
     * @return string
     * @throws GeneralException
     * check if gratuity percentage will exceed 100%
     */

    public function checkIfGratuityPercentageExceed100(Model $employee) {
        $total_percentage = $employee->dependents()->sum('survivor_gratuity_percent');
//        $total_percentage = $this->query()->where('employee_id','=', $employee_id)->sum('survivor_gratuity_percent');
        if ( $total_percentage > 100)
            throw new GeneralException(trans('exceptions.general.gratuity_percentage_exceed_100'));
        return '';
    }


    /**
     * @param $employee_id
     * @param $input
     * @return string
     * @throws GeneralException
     *
     */

    public function checkIfPensionPercentageExceed100(Model $employee) {

        $total_percentage = $employee->dependents()->sum('survivor_pension_percent');
//        $total_percentage = $this->query()->where('employee_id','=', $employee_id)->sum('survivor_pension_percent');
        if ( $total_percentage > 100)
            throw new GeneralException(trans('exceptions.general.pension_percentage_exceed_100'));
        return '';
    }


    /**
     * @param $employee_id
     * @param $input
     * @return string
     * @throws GeneralException
     * Check if Funeral grants distribution exceed 100 percentage
     */
    public function checkIfFuneralGrantPercentageExceed100(Model $employee) {
//        $employee = $this->employees->findOrThrowException($employee_id);
        $total_percentage = $employee->dependents()->sum('funeral_grant_percent');
//        $total_percentage = $this->query()->where('employee_id','=', $employee_id)->sum('funeral_grant_percent');
        if ( $total_percentage > 100)
            throw new GeneralException(trans('exceptions.general.funeral_grant_percentage_exceed_100'));
        return '';
    }


    /**
     * @param $employee_id
     * @param $input
     * @return string
     * @throws GeneralException
     * Check if funeral grants distribution is less 100
     */
    public function checkIfFuneralGrantPercentageIs100(Model $employee) {
//        $employee = $this->employees->findOrThrowException($employee_id);
        $total_percentage = $employee->dependents()->sum('funeral_grant_percent');
//        $total_percentage = $this->query()->where('employee_id','=', $employee_id)->sum('funeral_grant_percent');
        if ( $total_percentage < 100)
            throw new GeneralException(trans('exceptions.general.funeral_grant_percentage_dont_reach_100'));
        return '';
    }

    /**
     * @param $employee_id
     * @param $input
     * Check percentage when creating new
     */

    public function checkIfPercentagesExceed100($employee_id) {

        $employee = $this->employees->findOrThrowException($employee_id);
        $this->checkIfFuneralGrantPercentageExceed100($employee);
        $this->checkIfGratuityPercentageExceed100($employee);
        $this->checkIfPensionPercentageExceed100($employee);
    }




    /*Check if can be reinstated based on age limit 18 and 21 yrs*/
    public function checkIfChildUnderAgeLimits($id){
        /*Check if 18 yrs old and not (disabled or education benefit)*/
        $return = false;
        $child = $this->find($id);
        $today = Carbon::now();
        $dob = $child->dob;
        $day_dob = Carbon::parse($dob)->format('j');
//        $dob = ($day_dob >= 15) ? standard_date_format(Carbon::parse($dob)->endOfMonth()) : $dob;
        $dob = standard_date_format(Carbon::parse($dob)->endOfMonth());
        $child_age_limit = sysdefs()->data()->payroll_child_limit_age; //18yrs
        $child_age_limit_dob =  Carbon::now()->subYears($child_age_limit)->startOfMonth();

        $child_education_age_limit = sysdefs()->data()->payroll_child_education_limit_age; //21yrs
        $child_education_age_limit_dob =  Carbon::now()->subYears($child_education_age_limit);

//        if($child->iseducation == 0 && $child->isdisabled == 0){
//            /*if is not granted education and not disabled*/
//            $return = ($dob < $child_age_limit_dob) ? false : true;
//
//        }elseif($child->iseducation == 1){
//            /*if granted education*/
//            $return = ($dob < $child_education_age_limit_dob) ? false : true;
//        }else{
//            $return = true;
//        }
        $return = (comparable_date_format($dob) < comparable_date_format($child_age_limit_dob)) ? false : true;
        return $return;
    }

    /**
     * Process pension percent distribution
     */
    public function processPensionPercentDistribution($employee_id){

        //Get Dependents eligible for survivor pension
        $employee = $this->employees->findOrThrowException($employee_id);

        // Wife or Spouse
        $this-> processWifePensionPercentDistribution($employee);

        // Children
        $this->processChildrenPensionPercentDistribution($employee);

        //Assistant

        $this->processAssistantPensionPercentDistribution($employee);

        // Other dependents
        $this->processOtherDependentsDistribution($employee);


//        check if exceed 100
        $this->checkIfPensionPercentageExceed100($employee);

        /*Allocate unallocated percent (< 1%)*/

    }


    /**
     * Process pension percent distribution for WIFE and Spouse
     */
    public function processWifePensionPercentDistribution(Model $employee){
        $total_percent_eligible = 0;
        // Wife or Spouse
        $dependents = $employee->dependents()->where(function ($query){
            $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
        });
        $spouses = $dependents->where(function ($query){
            $query->where('dependent_type_id',1)->orWhere('dependent_type_id', 2);
        })->get();
        $spouses_count = $dependents->where(function ($query){
            $query->where('dependent_type_id',1)->orWhere('dependent_type_id', 2);
        })->count();
        // When there are spouse
        if ($spouses_count > 0) {
            //Divide 40% equally
            $total_percent_eligible = sysdefs()->data()->wife_spouse_percent;
            $percent = $total_percent_eligible / $spouses_count;
            foreach ($spouses as $spouse) {
                $this->updateSurvivorPensionPercent($spouse, $percent,$employee->id);
            }

        }

        if($total_percent_eligible > 0){
            $this->allocateUnallocatedPercentToYoungestBeneficiary($employee->id, [1,2], $total_percent_eligible, 1);
        }

//        elseif(count($spouses) == 1){
//            //  40%
//            $percent = sysdefs()->data()->wife_spouse_percent;
//            $this->updateSurvivorPensionPercent($spouses->first(), $percent);
//        }

    }



    /**
     * Process pension percent distribution for CHILDREN
     */
    public function processChildrenPensionPercentDistribution(Model $employee)
    {
        $total_percent_eligible = 0;
        $dependents = $employee->dependents()->where(function ($query){
            $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
        });
        //Wife or Spouse
        $spouses = $dependents->where(function ($query) {
            $query->where('dependent_type_id', 1)->orWhere('dependent_type_id', 2);
        })->get();
        $spouses_count = $dependents->where(function ($query) {
            $query->where('dependent_type_id', 1)->orWhere('dependent_type_id', 2);
        })->count();
        $dependents = $employee->dependents()->where('survivor_pension_flag',1);
        $children = $dependents->where('dependent_type_id', 3)->get();
        $children_count =  $dependents->where('dependent_type_id', 3)->count();
        // When there are spouse and children
        if (($spouses_count > 0)) {
            //Divide 60% equally
            if (($children_count > 3)) {
                $total_percent_eligible =  (100 - sysdefs()->data()->wife_spouse_percent);
                $percent =$total_percent_eligible / $children_count;
                foreach ($children as $child) {
                    $this->updateSurvivorPensionPercent($child, $percent, $employee->id);
                }
            } elseif (($children_count > 0) && ($children_count <= 3)) {
                //  20%
                $total_percent_eligible =  sysdefs()->data()->child_percent_with_spouse;
                $percent = $total_percent_eligible;

                foreach($children as $child) {

                    $this->updateSurvivorPensionPercent($child, $percent, $employee->id);
                }

            }
        }
        //when there is no spouse
        if (($spouses_count == 0)) {
            //Divide 100% equally
            if (($children_count > 2)) {
                $total_percent_eligible = 100;
                $percent = $total_percent_eligible / $children_count;
                foreach ($children as $child) {
                    $this->updateSurvivorPensionPercent($child, $percent, $employee->id);
                }
            } elseif (($children_count > 0) && ($children_count <= 2)) {
                //  40%
                $total_percent_eligible = sysdefs()->data()->child_percent_without_spouse;
                $percent = $total_percent_eligible;
                foreach ($children as $child) {
                    $this->updateSurvivorPensionPercent($child, $percent, $employee->id);
                }
            }
        }

        /*Allocate unallocated offset percent to youngest*/
        if($total_percent_eligible > 0) {
            $this->allocateUnallocatedPercentToYoungestBeneficiary($employee->id, [3], $total_percent_eligible, 1);
        }
    }



    /**
     * Process pension percent distribution for Assistant
     */
    public function processAssistantPensionPercentDistribution(Model $employee){
        $dependents = $employee->dependents()->where('survivor_pension_flag',1);
        $assistants = $dependents->where('dependent_type_id',7);
        $assistants_count = $dependents->where('dependent_type_id',7)->count();
        if ($assistants_count > 0){
            if (($assistants_count == 1)) {
                $percent = sysdefs()->data()->assistant_percent;
                $this->updateSurvivorPensionPercent($assistants->first(), $percent, $employee->id);
            } else {
                //Not allowed to have two pensionable assistants
                throw new GeneralException(trans('exceptions.backend.claim.assistant_not_allowed_more_than_two'));
            }
        }
    }


    /**
     * Process pension percent distribution for WIFE and Spouse
     */
    public function processOtherDependentsDistribution(Model $employee){
        $total_percent_eligible = 0;
        // Other dependents
        $dependents = $employee->dependents()->where(function ($query){
            $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
        });

        $eligible_dependents_count =  $dependents->where(function ($query){
            $query->whereIn('dependent_type_id',[1,2,3]);
        })->count();
        $other_dependents = $employee->dependents()->where('isotherdep',1)->get();
        $other_dependents_count = $employee->dependents()->where('isotherdep',1)->count();
        $reallocation_action_type = 1;
        // When there eligible dependents
        if (($eligible_dependents_count > 0)) {
            $this->revertOtherDependentsDistribution($employee->id);
        } else {

            if (($other_dependents_count > 0 && $eligible_dependents_count < 1)) {

                $total_percent_eligible_full = sysdefs()->data()->otherdep_full_pension_percent;;
                $total_percent_eligible_partial= 100;

                foreach ($other_dependents as $other_dependent) {
                    //TODO need to ask if partial and full can co-exists
                    /*Revert pension/gratuity*/
                    $this->revertOtherDependentPensionAndGratuity($other_dependent->id, $employee->id);
                    if($other_dependent->pivot->other_dependency_type == 1){
                        /*full*/
                        $total_percent_eligible = $total_percent_eligible_full;
                        $percent = $total_percent_eligible / $other_dependents_count;
                        $this->updateSurvivorPensionPercent($other_dependent, $percent, $employee->id);
                        $total_percent_eligible =$total_percent_eligible_full;
                    }else{
                        /*partial*/
                        $total_percent_eligible = $total_percent_eligible_partial;
                        $percent = $total_percent_eligible / $other_dependents_count;
                        $this->updateSurvivorGratuityPercent($other_dependent, $percent, $employee->id);
                        $reallocation_action_type = 2;
                    }

                }
            }
        }

        /*Allocate unallocated offset percent to youngest - for other dependents*/

        if($total_percent_eligible > 0) {
            //TODO need to ask if partial and full can co-exists
            $this->allocateUnallocatedPercentToYoungestBeneficiaryOtherDep($employee->id, $total_percent_eligible,$reallocation_action_type);
        }
    }




    /**
     * Process pension percent distribution for WIFE and Spouse - Old Approach
     */
    public function processOtherDependentsDistributionOld(Model $employee){
        // Wife or Spouse
        $dependents = $employee->dependents()->where(function ($query){
            $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
        });
        $eligible_dependents = $dependents->where(function ($query){
            $query->where('dependent_type_id',1)->orWhere('dependent_type_id', 2)->orWhere('dependent_type_id', 3);
        })->get();
        $eligible_dependents_count =  $dependents->where(function ($query){
            $query->where('dependent_type_id',1)->orWhere('dependent_type_id', 2)->orWhere('dependent_type_id', 3);
        })->count();
        $other_dependents = $employee->dependents()->where('dependency_percentage', '>', 0)->where(function ($query){
            $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
        })->get();
        $other_dependents_count = $employee->dependents()->where('dependency_percentage', '>', 0)->where(function ($query){
            $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
        })->count();
        // When there eligible dependents
        if (($eligible_dependents_count > 0)) {
            $this->revertOtherDependentsDistribution($employee->id);
        }  else {

            if (($other_dependents_count > 0 && $eligible_dependents_count < 1)) {
                $dependency_percentage = $employee->dependents()->where('dependency_percentage', '>', 0)->where(function ($query){
                    $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
                })->max('dependency_percentage');

                if ($dependency_percentage == 100) {
                    //Divide 40% equally == Monthly Pension
                    $percent = sysdefs()->data()->wife_spouse_percent / $other_dependents_count;
                    foreach ($other_dependents as $other_dependent) {
                        $this->updateSurvivorPensionPercent($other_dependent, $percent, $employee->id);
                    }
                }else{
                    //Gratuity / Lump sum
                    $percent = $dependency_percentage / $other_dependents_count;

                    foreach ($other_dependents as $other_dependent) {
                        $this->updateSurvivorGratuityPercent($other_dependent, $percent, $employee->id);
                    }
                }
            }
        }

    }


    /**
     * @param Model $employee
     * Process funeral grants percentage distributions
     * Spouse -> eligible children -> Parents -> Estate Admin -> Child not eligible
     */
    public function processFuneralGrantsDistribution($employee_id)
    {
        $employee = $this->employees->findOrThrowException($employee_id);
        //Wife or Spouse
        $spouses_query = $employee->dependents()->where(function ($query){
            $query->where('survivor_pension_flag',1)->orWhere('survivor_gratuity_flag',1);
        })->where(function ($query) {
            $query->where('dependent_type_id', 1)->orWhere('dependent_type_id', 2);
        });
        $children_query =  $employee->dependents()->where('survivor_pension_flag',1)->where('dependent_type_id', 3);
        $parents_query =   $employee->dependents()->where(function ($query) {
            $query->where('dependent_type_id', 4)->orWhere('dependent_type_id', 5);
        });
        $estate_admin_query = $employee->dependents()->where('dependent_type_id', 6);
        $children_not_eligible_query = $employee->dependents()->where('dependent_type_id', 3);//children over 18yrs old
        /*Other dep*/
        $other_dep_query =   $employee->dependents()->where(function ($query) {
            $query->whereNotNull('other_dependency_type');
        });


        /*revert distribution*/
        $this->revertFuneralGrantsDistribution($employee_id);
        /*If there is spouse*/
        if($spouses_query->count() > 0){
            //Divide equally
            $percent = 100 / $spouses_query->count() ;
            foreach ($spouses_query->get() as $spouse) {
                $this->updateFuneralGrantPercent($spouse, $percent,$employee->id);
            }

        }elseif($children_query->count() > 0){
            /*eligible children*/
            $percent = 100 / $children_query->count() ;
            foreach ($children_query->get() as $child) {
                $this->updateFuneralGrantPercent($child, $percent,$employee->id);
            }
        }elseif($other_dep_query->count() > 0){
            /*eligible parents/other dependents*/
            $percent = 100 / $other_dep_query->count() ;
            foreach ($other_dep_query->get() as $other_dep) {
                $this->updateFuneralGrantPercent($other_dep, $percent,$employee->id);
            }
        }elseif($estate_admin_query->count() > 0){
            /*estate*/
            $percent = 100 / $estate_admin_query->count() ;
            foreach ($estate_admin_query->get() as $estate_admin) {
                $this->updateFuneralGrantPercent($estate_admin, $percent,$employee->id);
            }
        }elseif($children_not_eligible_query->count() > 0){
            /*children over 18yrs*/
            $percent = 100 / $children_not_eligible_query->count() ;
            foreach ($children_not_eligible_query->get() as $child) {
                $this->updateFuneralGrantPercent($child, $percent,$employee->id);
            }
        }

        /*Allocate unallocated percent*/
        /*Allocate unallocated offset percent to youngest*/

        $this->allocateUnallocatedPercentToYoungestBeneficiary($employee->id, [1,2,3,4,5,6,7], 100, 2);

    }


    /**
     * @param $employee_id
     * @param $action_type i.e. 1 => pension , 2 => funeral grants
     * Allocate unallocated percent to youngest beneficiary when (remained % is less than 1%)
     */
    /*Allocate Unallocated Percent to youngest beneficiary*/
    public function allocateUnallocatedPercentToYoungestBeneficiary($employee_id, array $dependent_type_ids, $total_percent, $action_type)
    {
        $employee = $this->employees->find($employee_id);
        switch($action_type) {
            case 1:
                /*Pension*/
                $total_pension_percentage_allocated = $employee->dependents()->whereIn('dependent_employee.dependent_type_id', $dependent_type_ids)->sum('survivor_pension_percent');

                if (($total_percent - $total_pension_percentage_allocated) < 1 && $total_percent > $total_pension_percentage_allocated) {
                    $youngest_dependent = $this->query()->whereHas('employees', function ($q) use ($employee_id, $dependent_type_ids) {
                        $q->where('dependent_employee.survivor_pension_percent', '>', 0)->whereIn('dependent_employee.dependent_type_id', $dependent_type_ids)->where('dependent_employee.employee_id', $employee_id);
                    })->orderBy('dob', 'desc')->first();
                    $dependent_employee = $youngest_dependent->getDependentEmployee($employee_id);
                    $unallocated_percent = $total_percent - $total_pension_percentage_allocated;
                    $unallocated_percent = round($unallocated_percent, 2);
                    $new_percent = $unallocated_percent + $dependent_employee->survivor_pension_percent;
                    $this->updateSurvivorPensionPercent($youngest_dependent, $new_percent, $employee_id);
                }
                break;

            case 2:

                /*Funeral Grants*/
                $total_fg_percentage_allocated = $employee->dependents()->sum('funeral_grant_percent');
                if (($total_percent - $total_fg_percentage_allocated) < 1 && $total_percent > $total_fg_percentage_allocated) {
                    $youngest_dependent = $this->query()->whereHas('employees', function ($q) use ($employee_id) {
                        $q->where('dependent_employee.funeral_grant_percent', '>', 0)->where('dependent_employee.employee_id', $employee_id);
                    })->orderBy('dob', 'desc')->first();
                    $dependent_employee = $youngest_dependent->getDependentEmployee($employee_id);
                    $unallocated_percent = $total_percent - $total_fg_percentage_allocated;
                    $unallocated_percent = round($unallocated_percent, 2);
                    $new_percent = $unallocated_percent + $dependent_employee->funeral_grant_percent;
                    $this->updateFuneralGrantPercent($youngest_dependent, $new_percent, $employee_id);
                    break;

                }


        }

    }

    /*Allocate Unallocated percent to youngest Beneficiary for other dependents*/
    public function allocateUnallocatedPercentToYoungestBeneficiaryOtherDep($employee_id,$total_percent ,$action_type = 1)
    {

        $employee = $this->employees->find($employee_id);
        switch($action_type) {
            /*Pension*/
            case 1:
                $total_pension_percentage_allocated = $employee->dependents()->where('isotherdep',1)->sum('survivor_pension_percent');
                if (($total_percent - $total_pension_percentage_allocated) < 1 && $total_percent > $total_pension_percentage_allocated) {
                    $youngest_dependent = $this->query()->whereHas('employees', function($q) use($employee_id){
                        $q->where('dependent_employee.isotherdep',1)->where('dependent_employee.employee_id', $employee_id);
                    })->orderBy('dob', 'desc')->first();

                    $dependent_employee = $youngest_dependent->getDependentEmployee($employee_id);
                    $unallocated_percent = $total_percent - $total_pension_percentage_allocated;
                    $unallocated_percent = round($unallocated_percent, 2);
                    $new_percent = $unallocated_percent + $dependent_employee->funeral_grant_percent;
                    $this->updateFuneralGrantPercent($youngest_dependent, $new_percent, $employee_id);
                }
                break;
            case 2:
                /*Gratuity*/
                $total_gratuity_percentage_allocated = $employee->dependents()->sum('survivor_gratuity_percent');
                if (($total_percent - $total_gratuity_percentage_allocated) < 1 && $total_percent > $total_gratuity_percentage_allocated) {
                    $youngest_dependent = $this->query()->whereHas('employees', function ($q) use ($employee_id) {
                        $q->where('dependent_employee.survivor_gratuity_percent', '>', 0)->where('dependent_employee.employee_id', $employee_id);
                    })->orderBy('dob', 'desc')->first();
                    $dependent_employee = $youngest_dependent->getDependentEmployee($employee_id);
                    $unallocated_percent = $total_percent - $total_gratuity_percentage_allocated;
                    $unallocated_percent = round($unallocated_percent, 2);
                    $new_percent = $unallocated_percent + $dependent_employee->survivor_gratuity_percent;
                    $this->updateSurvivorGratuityPercent($youngest_dependent, $new_percent, $employee_id);
                    break;

                }
        }
    }



    /**
     * @param $id
     * @param $input
     * Update bank details from Notification Report profile
     */
    public function updateMultipleDependentsBankDetails($input){
        foreach ($input as $key => $value) {
            if (strpos($key, 'dependent_bank_id') !== false) {
                $dependent_id = substr($key, 17);
                $dependent = $this->findOrThrowException($dependent_id);
                $dependent->update(['bank_branch_id'=> (isset($input['dependent_bank_branch_id'.$dependent_id]) ? $input['dependent_bank_branch_id'.$dependent_id] : null),'accountno'=> (isset($input['dependent_accountno'.$dependent_id]) ? $input['dependent_accountno'.$dependent_id] : null)]);
//                    }
            }
        }

    }

    /**
     * funeral grants pending (NOT YET PAID)
     * Get all dependents id with pending funeral grants
     */
    public function pendingFuneralGrantDependents(){
        $dependents = $this->dependentAllWithPivot()->where('wf_done',1)->where('funeral_grant_pay_flag',0)->where('funeral_grant','>',0)
            ->pluck('dependent_id');
        $return = $this->query()->whereIn('id', $dependents);
        return $return;
    }

    /**
     * @return mixed
     * Get all pending funeral grants
     *  Get for DataTable
     *
     */
    public function pendingFuneralGrants(){
        $dependents = $this->dependentAllWithPivot()->where('notification_reports.incident_type_id', 3)->where('notification_reports.wf_done',1)->where('dependent_employee.funeral_grant_pay_flag',0)->where('dependent_employee.funeral_grant','>',0)->whereRaw("(select count(1) from claim_compensations where member_type_id = 4 and resource_id = dependents.id and benefit_type_id = 3) = 0");
        return $dependents;
    }


    /**
     * @return mixed
     * Pending survivor gratuity
     * Get all dependents with pending survivor gratuity
     */
    public function pendingSurvivorGratuityDependents(){
//        $dependents =  $this->dependentAllWithPivot()->where('notification_reports.incident_type_id', 3)->where('wf_done',1)->where('survivor_gratuity_pay_flag',0)->where('survivor_gratuity_flag',1)->where('survivor_gratuity_amount','>',0)->pluck('dependent_id');
        $dependents = $this->pendingSurvivorGratuity()->pluck('dependent_id');
        $return = $this->query()->whereIn('id', $dependents);
        return $return;
    }


    /**
     *Pending Survivor Gratuity (NOT YET PAID)
     * Get for DataTable
     *
     */
    public function pendingSurvivorGratuity(){
        $dependents =  $this->dependentAllWithPivot()->where('notification_reports.incident_type_id', 3)->where('wf_done',1)->where('survivor_gratuity_pay_flag',0)->where('survivor_gratuity_flag',1)->where('survivor_gratuity_amount','>',0)->whereRaw("(select count(1) from claim_compensations where member_type_id = 4 and  resource_id = dependents.id and benefit_type_id = 11) = 0");
        return $dependents;
    }

    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * getRegistered dependents
     */
    public function getRegisteredDependents($q)
    {
        $name = strtolower($q);
        $data['items'] = $this->query()->select(['id', DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as dependent")])->whereRaw("LOWER(firstname) like :firstname or LOWER(middlename) like :middlename or LOWER(lastname) like :lastname or concat_ws(' ',LOWER(firstname), coalesce(LOWER(middlename), ''), LOWER(lastname))  like :fullname ", ['firstname' => "%$name%", 'middlename' => "%$name%", 'lastname' => "%$name%", 'fullname' => "%$name%"])->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }



    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * Get dependents eligible to receive gratuity
     */
    public function getGratuityDependents($q)
    {
        $name = strtolower($q);
        $data['items'] = $this->query()->select(['id', DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as dependent")])->whereRaw("(LOWER(firstname) like :firstname or LOWER(middlename) like :middlename or LOWER(lastname) like :lastname or concat_ws(' ',LOWER(firstname), coalesce(LOWER(middlename), ''), LOWER(lastname))  like :fullname ) ", ['firstname' => "%$name%", 'middlename' => "%$name%", 'lastname' => "%$name%", 'fullname' => "%$name%"])
            ->get()
            ->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     * Get dependents for select2
     */
    public function getDependentsForSelect($q, $page)
    {
        $name = strtolower($q);
        $data['items'] = $this->query()
            ->select([
                'id',
                DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as dependent"),
            ])
            ->whereRaw("cast(concat_ws(' ', dependents.firstname, coalesce(dependents.middlename, ''), dependents.lastname) as text)  ~* ? ", [$name])
            ->limit($page)
            ->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }




    /*Get Pending summary - dependents to be merged with manual processing of payroll  - depenent who exist on the system but paid manually*/
    public function getPendingManualMergeSummary(){
        $pending_sync = $this->getReadyForPayrollForManualMergeForDataTable()->count();

        return [ 'pending_system_survivor_sync' => $pending_sync];
    }


    /*Get pensioners with missing details important i.e. dob, phone and bank for alert*/
    public function getDependentsWithMissingDetails()
    {
        return $this->query()
            ->join('payroll_beneficiaries_view as b', function($join){
                $join->on('b.resource_id', 'dependents.id')->where('b.member_type_id', 4);
            })
            ->where(function($query){
                $query->whereNull('dependents.accountno')->orWhereNull('dependents.bank_id')->orWhereIn('dependents.bank_id',[5,6])->orWhereNull('dependents.phone')->orWhereNull('dependents.dob');
            })->where('b.isactive','<>', 0)->where('b.suspense_flag', '<>',1);

    }



    /**
     * @param $id
     * Get Dependent for exporting to erp as supplier.
     */
    public function getDependentReadyToBeExportedToErpForDataTable()
    {
        $dependent_for_benefits  = $this->dependentAllWithPivot()
            ->join('claim_compensations', 'claim_compensations.resource_id', 'dependents.id')
            ->where('claim_compensations.member_type_id', 4)
            ->where('dependent_employee.iserpsupplier', 0);

        $dependent_for_pension = $this->dependentAllWithPivot()
            ->join('payroll_runs', function($join){
                $join->on('payroll_runs.resource_id', 'dependents.id')
                    ->where('payroll_runs.member_type_id', 4);
            })
            ->where(function ($query){
                $query->whereNotIn('payroll_runs.bank_id', [3,4])->orWhereNull('payroll_runs.bank_id');
            })
            ->where('dependent_employee.iserpsupplier', 0);

        $dependents = $dependent_for_benefits->union($dependent_for_pension);
        return $dependents;
    }

    /**
     * @param array $input
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Store supplier to erp
     */
    public function storeErpSupplier(array $input)
    {
        $request = $input;
        $supplier_exist=DB::table('main.erp_suppliers')->where('supplier_id',$request['supplier_id'])->first();
        if($supplier_exist){
            return view("backend/finance/receipt/employer_supplier");
        }else{
            $new_supplier=new ErpSupplier();
            $new_supplier->trx_id=mt_rand(1000, 1999).$request['supplier_id_no_delimiter'];
            $new_supplier->supplier_name=$request['supplier_name'];
            $new_supplier->supplier_id=$request['supplier_id'];
            $new_supplier->vendor_site_code=$request['vendor_site_code'];
            $new_supplier->address_line1=$request['address_line1'];
            $new_supplier->address_line2=$request['address_line2'];
            $new_supplier->city=$request['city'];
            $new_supplier->liability_account=$request['liability_account'];
            $new_supplier->status_code=200;
            $new_supplier->pay_group='BENEFICIARIES';
            $new_supplier->user_id=access()->user()->id;
            $new_supplier->save();
            $this->postSupplierErpApi();
        }

        /*update export status*/
        $this->updateErpExportStatus($input['dependent_id'], $input['employee_id']);
    }


    /*Post erp supplier to erp*/
    public function postSupplierErpApi()
    {
        $url ='http://192.168.1.9/mac_erp/public/api/post_supplier_to_q/';
        Log::info($url);
        try {
            $client = new Client();
            $response =  $client->request('GET', $url);
            $status = $response->getStatusCode();
        }
        catch (ClientError $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status:'.$status.' ClientError');
        }
        catch (ServerError $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status: '.$status.' ServerError');
        }
        catch (BadResponse $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status: '.$status,' BadResponse');

        }
        catch(\Exception $e){
            $status = $e->getCode();
            Log::info(' Status: '.$e->getMessage().' Exception');

        }

    }


    /**
     * @param $id
     * @param $employee_id
     * Update dependent status after is exported to erp as supplier
     */
    public function updateErpExportStatus($id,$employee_id){
        DB::table('dependent_employee')->where('dependent_id', $id)->where('employee_id', $employee_id)->update([
            'iserpsupplier' => 1,
        ]);

    }


    /**
     * Update ERP export status for already exported dependents
     */
    public function updateErpExportStatusAlreadyExported()
    {
        $dependents = $this->getDependentReadyToBeExportedToErpForDataTable()->get();
        foreach($dependents as $dependent_employee){
            $dependent = $this->find($dependent_employee->dependent_id);
            $supplier_id = $dependent->getMemberNoForErpApi($dependent_employee->employee_id);
            $check = DB::table('erp_suppliers')->where('supplier_id', $supplier_id)->count();
            if($check > 0){
                $this->updateErpExportStatus($dependent_employee->dependent_id, $dependent_employee->employee_id);
            }
        }

    }


    /*Get dependent who are close to be verified within 2 next months or if already passed*/
    public function getVerificationAlertsForDt()
    {
        $alert_months = payroll_verification_alert_months();
        $verification_limit_days = (new PayrollVerificationRepository())->getDaysForVerificationAlert();
        $cut_off_date = Carbon::now()->addMonthNoOverflow($alert_months);
//        $dependents = $this->dependentAllWithPivot()->distinct('dependents.id')
//             ->whereRaw(" dependent_employee.isactive not in (0,2)")
//            ->leftJoin('payroll_verifications as v', function($join){
//                $join->on('v.resource_id', 'dependents.id')->where('v.member_type_id', 4);
//            })->leftJoin('payroll_status_changes as s', function($join){
//                $join->on('s.resource_id', 'dependents.id')->where('s.member_type_id', 4)->where('s.wf_done', 0);
//            })
//            ->whereRaw("(dependents.lastresponse::date +  (cast(? as varchar) || ' days')::interval) < ?", [$verification_limit_days, $cut_off_date ])->where('dependents.suspense_flag', 0)
//        ->whereRaw(" coalesce(s.wf_done, 1) = 1");


        $dependents = (new PayrollVerificationRepository())->getQueryVerificationAllDistinct()
            ->leftJoin('payroll_status_changes as s', function($join){
                $join->on('s.resource_id', 'b.resource_id')->where('s.member_type_id', 4)->where('s.wf_done', 0);
            })->where('b.member_type_id', 4)->where('b.suspense_flag','<>', 1)->whereRaw(" coalesce(s.wf_done, 1) = 1")
            ->whereRaw("(b.lastresponse::date +  (cast(? as varchar) || ' days')::interval) < ?", [$verification_limit_days, $cut_off_date ]);
        return $dependents;
    }


    /*Update reassessed Monthly pension */
    public function updateReassessedMp($id,$employee_id, $old_mp, $new_mp)
    {
        $dependent = $this->find($id);
        $dependent_employee = $dependent->getDependentEmployee($employee_id);
        DB::table('dependent_employee')->where('id', $dependent_employee->id)->update([
            'survivor_pension_amount' => $new_mp,
            'old_mp' => $old_mp
        ]);

    }



    /*Update notification report dependent employee//todo: temporary solution - comment after used*/
    public function updateNotificationReportPivot($dependent_employee_id, $dependent_id, $employee_id)
    {
        $notification_report = $this->findNotificationReport($dependent_id, $employee_id);
        DB::table('dependent_employee')->where('id', $dependent_employee_id)->update([
            'notification_report_id' => $notification_report->id
        ]);
    }

    /*Rectify mp amount for dependents added after*/
    public function rectifyMp($id, $employee_id)
    {
        $dependent = $this->find($id);
        $dependent_employee = $dependent->getDependentEmployee($employee_id);
        $notification_report = (new EmployeeRepository())->findNotificationReportDeathIncident($employee_id);
        $ppd_lumpsum = (new ClaimCompensationRepository())->findPpdLumpsum($notification_report);
        $monthly_pension_payable =  (new BenefitTypeRepository())->getTotalDisablementPayableAmount($ppd_lumpsum,1);
        $dependent_pension = (0.01 * $dependent_employee->survivor_pension_percent) * $monthly_pension_payable;
        /*Update*/
        DB::table('dependent_employee')->where('dependent_id', $id)->where('employee_id', $employee_id)->update([
            'survivor_pension_amount' =>$dependent_pension,
            'notification_report_id' => $notification_report->id,

        ]);
        return $dependent_employee;
    }



    /*Redistribute Mp Allocation for Manual System File*/
    public function redistributeMpAllocationForManualSystemFile(Model $incident)
    {
        $monthly_pension_payable = (new ClaimCompensationRepository())->findPayableMpForDeathIncident($incident);
        $employee_id = $incident->employee_id;
        $dependents = $this->dependentsWithPivot($employee_id)->where('survivor_pension_flag',1)->get();
        $active_dependents = $this->dependentsWithPivot($employee_id)->where('survivor_pension_flag',1)->where('dependent_employee.isactive', '<>', 0)->count();
        if($active_dependents == 0){
            $this->processPensionPercentDistribution($employee_id);
            foreach ($dependents as $dependent) {
                $survivor_pension_percent = $dependent->survivor_pension_percent;
                $survivor_pension = ($survivor_pension_percent * 0.01) * $monthly_pension_payable;
                $this->updateMP($dependent->dependent_id, $survivor_pension,$employee_id, $incident->id, null);

            }

        }else{
            throw new GeneralException('There is active dependent(s) can not recalculate/modify pension details for already enrolled beneficiary! Please contact system administrator');
        }

    }

    /*Update deadline date*/
    public function updateDeadlineDate($dependent_id, $employee_id, $deadline_date)
    {
        DB::table('dependent_employee')->where('dependent_id', $dependent_id)->where('employee_id', $employee_id)->update([
            'deadline_date' => standard_date_format($deadline_date)
        ]);
    }


    /*Update Isdisabled status*/
    public function updateIsDisabledStatus($dependent_id, $status){
        $dependent = $this->find($dependent_id);
        $dependent->update([
            'isdisabled' => $status
        ]);
    }

    /*Update iseducation status*/
    public function updateEducationStatus($dependent_id, $status){
        $dependent = $this->find($dependent_id);
        $dependent->update([
            'iseducation' => $status
        ]);
    }

}