<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\LegacyContributionTemp;
use App\Repositories\BaseRepository;

class LegacyContributionTempRepository extends BaseRepository
{

    const MODEL = LegacyContributionTemp::class;

}