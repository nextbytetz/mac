<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Member\UnregisteredEmployerFollowUp;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Doctrine\DBAL\Driver\IBMDB2\DB2Connection;
use Illuminate\Support\Facades\DB;
use Propaganistas\LaravelPhone\PhoneNumber;

class UnregisteredEmployerFollowUpRepository extends  BaseRepository
{

    const MODEL = UnregisteredEmployerFollowUp::class;

    public function __construct()
    {

    }

//find or throwexception for follow up
    public function findOrThrowException($id)
    {
        $unregistered_employer_follow_up = $this->query()->find($id);

        if (!is_null($unregistered_employer_follow_up)) {
            return $unregistered_employer_follow_up;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.unregistered_employer_follow_up_not_found'));
    }


    /**
     * @param $id
     * @param $request
     * Create
     */
    public function create($unregistered_employer_id,$input){
        return DB::transaction(function () use ($unregistered_employer_id,$input) {
            $unregistered_employer_follow_up =$this->query()->create([
                'description'=> $input['description'],
                'follow_up_type_cv_id'=> $input['follow_up_type_cv_id'],
                'contact'=>  ($input['follow_up_type_cv_id'] == 91) ? (($input['contact']) ? phone_255($input['contact']) : null ) : $input['contact'],
                'contact_person'=> $input['contact_person'],
                'date_of_follow_up'=> $input['date_of_follow_up'],
                'feedback'=> $input['feedback'],
                'unregistered_employer_id'=> $unregistered_employer_id,
                'user_id' => access()->user()->id
            ]);
            return $unregistered_employer_follow_up;
        });
    }




    /**
     * @param $id
     * @param $request
     * Update
     */
    public function update($id,$input){
        return DB::transaction(function () use ($id,$input) {
            $unregistered_employer_follow_up = $this->findOrThrowException($id);
$code_values = new CodeValueRepository();
            $unregistered_employer_follow_up->update([
                'description'=> $input['description'],
                'follow_up_type_cv_id'=> $input['follow_up_type_cv_id'],
                'contact'=>  ($input['follow_up_type_cv_id'] == $code_values->getFollowupTypePhoneCall()) ? (($input['contact']) ? phone_255($input['contact']) : null ) : $input['contact'],
                'contact_person'=> $input['contact_person'],
                'date_of_follow_up'=> $input['date_of_follow_up'],
                'feedback'=> $input['feedback'],
            ]);
            return $unregistered_employer_follow_up;
        });
    }


    /**
     * @param $id
     * @param $id
     * @return mixed
     *
     */
    public function delete($id) {
        return DB::transaction(function () use ($id) {
            $unregistered_employer_follow_up = $this->findOrThrowException($id);
            $unregistered_employer_follow_up->delete();
        });
    }


}