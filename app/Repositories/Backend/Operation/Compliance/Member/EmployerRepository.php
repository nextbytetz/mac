<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;



use App\Jobs\Compliance\CategorizeEmployerContributors;
use App\Jobs\Compliance\PostBookingForSkippedMonths;
use App\Jobs\Compliance\PostBookingForSkippedMonthsLegacy;
use App\Jobs\Compliance\PostBookingsForNonContributors;
use App\Jobs\Compliance\PostBookingsNonBooked;
use App\Jobs\Compliance\UpdateBookingsNotContributed;
use App\Jobs\Compliance\UpdateEmployersStatusJob;
use App\Jobs\Employer\UndoLoadedEmployees;
use App\Employer\Jobs\Upload;
use App\Exceptions\JobException;
use App\Jobs\LoadEmployeeList;
use App\Jobs\PostInterestWithBoardAdjust;
use App\Jobs\RegisterEmployeeList;
use App\Models\Finance\Receivable\NonBookedBooking;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Compliance\Member\Employer;
use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Member\EmployerCertificateIssue;
use App\Models\Operation\Compliance\Member\EmployerVerTwo;
use App\Models\Operation\Compliance\Member\LegacyMergedContribution;
use App\Models\Operation\Compliance\Member\RegisteredUnregisteredEmployer;
use App\Repositories\Backend\Finance\DishonouredChequeRepository;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use App\Services\Compliance\PostBookingsForESkippedMonths;
use App\Services\Compliance\PostBookingsForESkippedMonthsLegacy;
use App\Services\Scopes\IsApprovedScope;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\InterestWriteOffRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Operation\Compliance\Member\EmployeeUpload;
use Log;
use App\Jobs\CalculateBooking;

class EmployerRepository extends  BaseRepository
{


    use AttachmentHandler, FileHandler;

    const MODEL = Employer::class;
    protected $booking_interests;
    protected $interest_write_offs;
    protected $receipt_codes;
    protected $receipts;
    protected $unregistered_employers;
    protected $error_message;

    /* start : Handling Documents */
    protected $base = null;
//    protected $base = null;
    /* end : Handling Documents */

    public function __construct()
    {
        parent::__construct();
        $this->booking_interests = new BookingInterestRepository();
        $this->interest_write_offs = new InterestWriteOffRepository();
        $this->receipt_codes = new ReceiptCodeRepository();
        $this->receipts = new ReceiptRepository();
        $this->unregistered_employers = new UnregisteredEmployerRepository();

        /* start : Initialise document store directory */
        $this->base = $this->real(employer_registration_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }
//        if (!$this->base_path) {
//            throw new GeneralException('Base path does not exist');
//        }
        /* end : Initialise document store directory */

    }


    /*
* GENERAL METHODS=====================================
*/

    /*
     * check if max date is greater than min date
     */
    public function checkIfMaxDateIsGreater($from_date, $end_date)
    {
        if ($end_date >= $from_date) {
            return '';
        }
        throw new GeneralException(trans('exceptions.backend.compliance.max_date_greater'));
    }


    /*
 * check if dates exceed todays date
 */
    public function checkIfDatesExceedThisMonth($from_date, $end_date)
    {
        if ((months_diff($end_date, Carbon::parse('now')) < 0) || (months_diff($from_date, Carbon::parse('now')) <
                0)) {

            throw new GeneralException(trans('exceptions.backend.compliance.dates_exceed_this_month'));
        }
        return '';
    }

    //===End General Methods ---------------------------------


    /*
     * EMPLOYER METHODS =====================================
     */


//find or throwOrexception for employer
    public function findOrThrowException($id)
    {
        $employer = $this->query()->find($id);

        if (!is_null($employer)) {
            return $employer;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.employer_not_found'));
    }


    /*  find or throw exception for employer -> Without Scope */
    public function findWithoutScopeOrThrowException($id)
    {
        $employer = $this->query()->withTrashed()->withoutGlobalScopes([IsApprovedScope::class])->find($id);
        if (!is_null($employer)) {
            return $employer;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.employee_not_found'));
    }

    //Need update from Finance it depend on Finance code provided
    public function getFinCodeId($id)
    {
        return 2;
    }

//    Get Member count
    public function getMemberCount($id)
    {
        $employer = $this->query()->find($id);
        return $employer->employees()->select(['id'])->count();
    }

    //Get for employers datatable
    public function getForDataTable()
    {
        return $this->query()->select(['id', 'reg_no', 'doc', 'name', 'district_id', 'region_id'])->orderBy("id", "desc");
    }

    //====End Employer Methods-------------------------

    /*
     * CONTRIBUTION METHODS=====================================
     */
    public function getAllContributions($id)
    {
        return $this->receipt_codes->getAllContributions($id);
    }

    public function getAllLegacyContributions($id)
    {
        $legacyEmployerContribution = new LegacyReceiptRepository();
        return $legacyEmployerContribution->getAllContributions($id);
    }


    public function getAllLegacyMergedContributions($id)
    {
        $legacyEmployerContribution = new LegacyMergedContribution();
        return $legacyEmployerContribution->query()->where('employer_id', $id)->where('iscancelled', 0)->where('isdishonoured', 0)->whereNotNull('rctno')->where('rctno', '<>', '')->whereHas('legacyReceipt', function ($query) {
            $query->where('isverified', 1);
        });
    }


    public function getInactiveReceipts($id)
    {
        $receipts = $this->receipts->query()->withTrashed()->where('employer_id', $id)->where(function ($query) {
            $query->where('iscancelled', 1)->orWhere('isdishonoured', 1);
        })->get();
        return $receipts;
    }

    /*Get dishonoured cheques by employer for dataTable*/
    public function getDishonouredChequesByEmployerForDt($id)
    {
        $dishonoured_cheques = new DishonouredChequeRepository();
        return $dishonoured_cheques->getQuery()->whereHas('receiptWithTrashed', function ($query) use ($id) {
            $query->where('employer_id', $id);
        });
    }



    //===END Contribution methods -------------------------------

    /*
     * BOOKING AND BOOKING INTERESTS METHODS FOR THIS EMPLOYER ============================
     */


    // Get all bookings for this employer
    public function getBookings($id)
    {
        return $this->query()->find($id)->bookings;
    }


    /*
     * Get all interest for this employer
     */
    public function getBookingInterests($id)
    {
        return $this->query()->find($id)->bookingInterests;
    }


    /*Get all missing months not contributed*/
    public function getBookingsForMissingMonthsForDataTable($id)
    {
        return DB::table('bookings_mview')
            ->join('booking_grace_period_view', 'booking_grace_period_view.booking_id', 'bookings_mview.booking_id')->where('employer_id', $id)->where('ispaid', 0)->where('grace_period_end', '<', standard_date_format(Carbon::now()));
    }

    /*
     * Get Total Interest Paid for booking_id
     */

    public function getTotalInterestPaidForBookingId($booking_id)
    {
        return $this->receipt_codes->getTotalInterestPaidForBookingId($booking_id);
    }


    /**
     * @param $id
     * Get Interest summary for employer on its Due amount, Paid amount and Remaining Amount
     */
    public function interestPaymentSummary($id)
    {
        $interest_payment_details = $this->interestPaymentDetails($id);
        $total_interest =$interest_payment_details['total_interest'];
        $total_paid = $interest_payment_details['total_paid'];
        $total_remain = $interest_payment_details['total_remain'];
        return ['total_interest' => number_format($total_interest, 2, '.', ','), 'total_paid' => number_format($total_paid, 2, '.', ','), 'total_remain' => number_format($total_remain, 2, '.', ',')];
    }

    /*Get interest payment details*/
    public function interestPaymentDetails($id)
    {
        $total_interest = $this->getBookingInterests($id)->sum('amount');
        $total_paid = $this->receipt_codes->getTotalInterestPaidForEmployer($id);
        $total_remain = $total_interest - $total_paid;
        return ['total_interest' => $total_interest,'total_paid' => ($total_paid), 'total_remain' => $total_remain];
    }

    /*
     * Get Total Interest Paid for booking_id
     */

    public function getTotalContributionPaidForBookingId($booking_id)
    {
        return $this->receipt_codes->getTotalContributionPaidForBookingId($booking_id);
    }

    /**
     * @param $id
     * Get booking payment summary for employer on its Due amount, Paid amount and Remaining Amount
     */
    public function bookingPaymentSummary($id)
    {
        $total_booking = $this->getBookings($id)->sum('amount');
        $total_paid = $this->receipt_codes->getTotalContributionPaidForEmployer($id);
        $total_remain = $total_booking - $total_paid;
        return ['total_booking' => number_format($total_booking, 2, '.', ','), 'total_paid' => number_format($total_paid, 2, '.', ','), 'total_remain' => number_format($total_remain, 2, '.', ',')];
    }

    public function getContribReceivableSummary($employer_id)
    {
        $contribution = DB::table('employer_contributions')->where('employer_id', $employer_id)
            ->sum('contrib_amount');
        $receivable = DB::table('bookings_mview')->where('employer_id', $employer_id)->where('ispaid', 0)
            ->sum('booked_amount');
        $missing_months = $this->getBookingsForMissingMonthsForDataTable($employer_id)
            ->count();
        return ['contribution' => $contribution, 'receivable' => $receivable, 'missing_months' => $missing_months];
    }



    //==End Booking and Interest ---------------------------------------------


    /*
     * INTEREST ADJUSTMENT METHODS for this employer==================================
     *
     */

    /*
 * get all interests adjusted belong to this employer
 */
    public function getInterestAdjusted($id)
    {
        return $this->booking_interests->getInterestAdjusted($id);
    }

    public function interestAdjustment($booking_id, $input)
    {
        $this->booking_interests->interestAdjustment($booking_id, $input);
//       initiate workflow
    }


    /*
*
* Get status of interest adjustment
*/
    public function getInterestAdjustLabel($booking_id)
    {
        $this->booking_interests->getInterestAdjusted($booking_id);
    }


    /**
     * Get employer with interest overpayment on Interest Adjustment of Sep 2017
     */
    public function getEmployerInterestOverpayment()
    {
        $fin_code_groups = new FinCodeGroupRepository();
        $employers = $this->query()->select(['id', 'name', 'reg_no'])->whereHas('receipts', function ($query) use ($fin_code_groups) {
            $query->where('iscancelled', 0)->where('isdishonoured', 0)->whereHas('receiptCodes', function ($query) use ($fin_code_groups) {
                $query->where('fin_code_id', $fin_code_groups->getInterestinCodeGroupId());
            });
        });
        return $employers;
    }



    //End interest adjustment --------------------------------------------

    /*
     * WRITE OFF INTEREST FOR THIS EMPLOYER METHODS =============================
     */
    public function writeOffInterest($id, $input)
    {
        return DB::transaction(function () use ($id, $input) {
            $this->checkIfDateRangeEligibleToBeWrittenOff($id, $input['min_date'], $input['max_date']);
            $interest_written_off = $this->interest_write_offs->query()->create((['user_id' => access()->user()->id, 'description' => $input['comments']]));
            $this->booking_interests->updateWriteOffId($id, $interest_written_off->id, $input['min_date'], $input['max_date']);
//       initiate workflow
            return $interest_written_off;
        });
    }

    /*
     * get all interest write offs belong to this employer
     */
    public function getInterestWriteOffs($id)
    {
        return $this->interest_write_offs->getInterestWriteOffs($id);
    }


    /**
     * @param $interest_write_off_id
     * @return mixed
     */
    public function getWriteOffEmployer($interest_write_off_id)
    {
        $employer = $this->query()->whereHas("bookingInterests", function ($query) use ($interest_write_off_id) {
            $query->withTrashed()->where(['interest_write_off_id' => $interest_write_off_id]);
        })->first();
        return $employer;
    }


    /*
     *
     * Check status of interest write off
     */
    public function getInterestWriteOffIsCompleteLabel($interest_write_off_id)
    {
        if (($this->interest_write_offs->checkIfIsWrittenOff($interest_write_off_id)) == 1)
            return "<span class='tag tag-success success_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.complete') . "'>" . '.' . "</span>";
        return "<span class='tag tag-warning warning_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.backend.finance.receipt.incomplete') . "'>" . '.' . "</span>";

    }

    /*
 * check if there if Interest in the date range provided are all eligible
 */
    public function checkIfDateRangeEligibleToBeWrittenOff($id, $from_date, $end_date)
    {
        $this->booking_interests->checkIfThereIsEligibleInterestInDateRange($id, $from_date, $end_date);
        $this->booking_interests->checkIfThereIsInEligibleInterestInDateRange($id, $from_date, $end_date);
    }



//===End Write Off Interest ---------------------------------------

    /**
     * BOOKING INTEREST METHODS FOR THIS EMPLOYER=========================
     *
     */


    public function getBookingInterestEmployer($booking_interest_id)
    {
        $employer = $this->query()->whereHas("bookingInterests", function ($query) use ($booking_interest_id) {
            $query->where(['booking_interests.id' => $booking_interest_id]);
        })->first();
        return $employer;
    }


//-------------End Booking Interest -------------------------------


    //...........Check if employer is online.......................//

    public function checkIfIsOnline($id)
    {
        $isonline = $this->query()->where('id', $id)->where('isonline', 1)->first();
        if ($isonline) {

            return true;
        } else {
            return false;
        }


    }

    public function returnOnlineEmployerUser($id)
    {
        $query = DB::table('portal.employer_user')
            ->join('portal.users', 'users.id', '=', 'employer_user.user_id')
            ->where('ismanage', true)
            ->where('employer_id', $id)->get();

        $employer_user_array = [];
        foreach ($query as $q) {
            $payroll_id = $q->payroll_id;
            $employer_user_array[$payroll_id] = $q->name;
        }
        return $employer_user_array;
    }

//End of checking online registration status---//


    public function getRegisterEmployers($q, $page)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $data['items'] = $this->regexColumnSearch(['name' => $q, 'reg_no' => $q])->limit($resultCount)->offset($offset)->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    public function getNotificationRegisterEmployers($q, $employee_id, $page)
    {
        $data['items'] = [];
        $data['total_count'] = count($data['items']);
        if (!empty($employee_id)) {
            $employee = (new EmployeeRepository())->find($employee_id);
            $employers = $employee->employers()->pluck("employers.id")->all();
            $data['items'] = $this->regexColumnSearch(['name' => $q, 'reg_no' => $q])->whereIn("id", $employers)->get()->toArray();
            $data['total_count'] = count($data['items']);
        }
        return response()->json($data);
    }

    public function getRegisterEmployersTin($q)
    {
        $data['items'] = $this->regexColumnSearch(['name' => $q, 'tin' => $q])->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }


    public function getAllEmployers($q)
    {
        //$name = strtolower(trim(preg_replace('/\s+/', '', $q)));
        $data['items'] = $this->regexColumnSearch(['name' => $q, 'reg_no' => $q])->withoutGlobalScopes()->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    /* Get all employers with approved claim (completed workflow)*/
    public function getBeneficiaryEmployers($q)
    {

        $data['items'] = $this->regexColumnSearch(['name' => $q, 'reg_no' => $q])->whereHas('notificationReports', function ($query) {
            $query->where('wf_done', 1);
        })->get()
            ->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }


    public function getRegisteredEmployeesForDataTable($id)
    {
//        $employer= $this->findOrThrowException($id);
//        return $employer->employees()->select(['memberno', 'firstname', 'lastname', 'middlename', 'dob', 'employee_category_cv_id', 'emp_cate']);
        $employee = new EmployeeRepository();
        return $employee->getEmployeeByEmployer($id);
    }


    /**
     * @param $id
     * @param $input
     * Update bank details
     */
    public function updateBankDetails($employer_id, $bank_branch, $accountno)
    {
        $employer = $this->findOrThrowException($employer_id);
        $employer->update(['bank_branch_id' => $bank_branch, 'accountno' => $accountno]);

    }


    /**
     * @param $id
     * @throws GeneralException
     * Check if Tin number already exist in employers and employer registration.
     */
    public function checkIfTinAlreadyExist($tin)
    {

        $unregistered_employers = new UnregisteredEmployerRepository();

        //  table of employers
        $tin_employer = $this->query()->withoutGlobalScopes([IsApprovedScope::class])->select(DB::raw("tin"))
            ->where(DB::raw("regexp_replace(" . DB::raw('tin') . " , '[^a-zA-Z0-9]', '', 'g')"), '=',
                $string = preg_replace('/[^A-Za-z0-9]/', '', $tin))
            ->get();
        /*Check from only registered employers */
        $tin_registered_employer = $this->query()->select(DB::raw("tin"))
            ->where(DB::raw("regexp_replace(" . DB::raw('tin') . " , '[^a-zA-Z0-9]', '', 'g')"), '=',
                $string = preg_replace('/[^A-Za-z0-9]/', '', $tin))
            ->get();


        //  table of unregistered employers
        $tin_unregistered_employer = $unregistered_employers->query()->select(DB::raw("tin"))
            ->where(DB::raw("regexp_replace(" . DB::raw('tin') . " , '[^a-zA-Z0-9]', '', 'g')"), '=',
                $string = preg_replace('/[^A-Za-z0-9]/', '', $tin))
            ->get();

        return ['tin_employer' => $tin_employer, 'tin_registered_employer' => $tin_registered_employer, 'tin_unregistered_employer' =>
            $tin_unregistered_employer];
    }


    /**
     * @param $id
     * @throws GeneralException
     * Check if TIN number exist
     */
    public function checkIfTinEmployerExistWhenInsert($input)
    {
        if (($this->checkIfTinAlreadyExist($input['tin'])['tin_employer'])->isNotEmpty() && ($this->checkIfEmployerNameExist($input)->isNotEmpty())) {
            throw new GeneralException(trans('exceptions.backend.compliance.employer_with_tin_exist'));
        };
    }


    /**
     * @param $id
     * @throws GeneralException
     * Check if TIN number exist when update
     */
    public function checkIfTinEmployerExistWhenUpdate($input)
    {
        if (($this->checkIfTinAlreadyExist($input['tin'])['tin_employer'])->count() > 1 && ($this->checkIfEmployerNameExist($input)->count()) > 1) {
            throw new GeneralException(trans('exceptions.backend.compliance.employer_with_tin_exist'));
        };
    }


    /**
     * @param $vote
     * Check if Has Tin
     */
    public function checkIfHasTin($id)
    {
        $employer = $this->query()->where('id', $id)->where(function ($query) {
            $query->whereNull('tin')->orWhere('tin', '');
        })->first();
        if ($employer) {
            throw new GeneralException(trans('exceptions.backend.compliance.employer_need_to_finish_registration'));

        }

    }


    /**
     * @param $input
     * Check if Tin and employer name already exist
     */
    public function checkIfEmployerNameExist($input)
    {

//        $employers = $this->query()->withoutGlobalScopes([IsApprovedScope::class])->whereRaw("levenshtein(lower(name), :name) between 0 and 1", ['name' => strtolower($input['name'])])->get();
        $employers = $this->query()->withoutGlobalScopes([IsApprovedScope::class])->whereRaw("lower(name) = :name", ['name' => strtolower($input['name'])])->get();
        return $employers;
    }


    public function getNameClearance($name)
    {
        $employers = $this->query()->withoutGlobalScopes([IsApprovedScope::class])->whereRaw("levenshtein(lower(name), :name) between 0 and 1 and lower(name) <> :name", ['name' => strtolower($name)])->get();
        return $employers;
    }


    /**
     * @param $vote
     * @throws GeneralException
     * Check if Employer name already exist; 1 => insert, 2=> update
     */
    public function checkValidationEmployerName($input, $action_type)
    {
        if ($action_type == 1) { // insert
            if (($this->checkIfEmployerNameExist($input))->isNotEmpty()) {
                throw new GeneralException(trans('exceptions.backend.compliance.employer_with_name_exist'));
            }
        } elseif ($action_type == 2) { //update
            if (($this->checkIfEmployerNameExist($input)->count()) > 1) {
                throw new GeneralException(trans('exceptions.backend.compliance.employer_with_name_exist'));
            }
        }
    }





    /**
     *
     * REGISTRATION ====================================
     */

    /**
     * Get all merged employers (unregistered + registered)
     */
    public function getAllUnregRegEmployers()
    {
        $employers = new  RegisteredUnregisteredEmployer();
        $all_employers = $employers->query();

        return $all_employers;
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function create($input)
    {
        $wfModuleRepo = new WfModuleRepository();
        $group = $wfModuleRepo->employerRegistrationType()['group'];
        $type = $wfModuleRepo->employerRegistrationType()['type'];
        $module = $wfModuleRepo->getModule(['wf_module_group_id' => $group, 'type' => $type]);
        return DB::transaction(function () use ($input, $module) {
            $this->checkIfTinEmployerExistWhenInsert($input);
            $this->checkValidationEmployerName($input, 1);
            $employer = $this->query()->create([
                'name' => $input['name'],
                'doc' => $input['doc'],
                'tin' => (isset($input['tin']) And ($input['tin'])) ? preg_replace('/[^A-Za-z0-9]/', '', $input['tin']) : null,
                'annual_earning' => str_replace(",", "", $input['annual_earning']),
                'vote' => $input['vote'],
                'employer_category_cv_id' => $input['employer_category_cv_id'],
                'phone' => (isset($input['phone']) And ($input['phone'])) ? phone_255($input['phone']) : null,
                'telephone' => (isset($input['telephone']) And ($input['telephone'])) ? phone_255($input['telephone']) : null,
                'email' => $input['email'],
                'fax' => $input['fax'],
                'box_no' => $input['box_no'],
                'district_id' => $input['district_id'],
                'region_id' => $input['region_id'],
                'location_type_id' => $input['location_type_id'],
                'street' => $input['street'],
                'road' => $input['road'],
                'plot_no' => $input['plot_no'],
                'block_no' => $input['block_no'],
                'business_activity' => $input['business_activity'],
                'unsurveyed_area' => $input['unsurveyed_area'],
                'surveyed_extra' => $input['surveyed_extra'],
                'parent_id' => array_key_exists('parent_employer_id', $input) ? $input['parent_employer_id'] : null,
                'has_tin_verified' => ($this->checkIfTinAlreadyExist($input['tin'])['tin_unregistered_employer'])->isNotEmpty() ? 1 : 0,
                'has_tin_existing' => (($this->checkIfTinAlreadyExist($input['tin'])['tin_employer'])->isNotEmpty()) ? 1 : 0,
                'created_by' => access()->user()->id,
                'wf_module_id' => $module,
            ]);

            return $employer;

        });
    }


    /**
     * @param $input
     * @return mixed
     * Update Employer Registration
     */

    public function update($id, $input)
    {
        return DB::transaction(function () use ($id, $input) {
            $employer = $this->findWithoutScopeOrThrowException($id);
            $employer->update([
                'name' => $input['name'],
                'doc' => $input['doc'],
                'tin' => (isset($input['tin']) And ($input['tin'])) ? preg_replace('/[^A-Za-z0-9]/', '', $input['tin']) : null,
                'annual_earning' => str_replace(",", "", $input['annual_earning']),
                'vote' => $input['vote'],
                'employer_category_cv_id' => $input['employer_category_cv_id'],
                'phone' => (isset($input['phone']) And ($input['phone'])) ? phone_255($input['phone']) : null,
                'telephone' => (isset($input['telephone']) And ($input['telephone'])) ? phone_255($input['telephone']) : null,
                'email' => $input['email'],
                'fax' => $input['fax'],
                'district_id' => $input['district_id'],
                'region_id' => $input['region_id'],
                'location_type_id' => $input['location_type_id'],
                'street' => $input['street'],
                'road' => $input['road'],
                'box_no' => $input['box_no'],
                'plot_no' => $input['plot_no'],
                'block_no' => $input['block_no'],
                'business_activity' => $input['business_activity'],
                'unsurveyed_area' => $input['unsurveyed_area'],
                'surveyed_extra' => $input['surveyed_extra'],
                'parent_id' => array_key_exists('parent_employer_id', $input) ? $input['parent_employer_id'] : null,
                'has_tin_verified' => ($this->checkIfTinAlreadyExist($input['tin'])['tin_unregistered_employer'])->isNotEmpty() ? 1 : 0,
                'is_treasury' => isset($input['is_treasury']) ? $input['is_treasury'] : 0

            ]);
            $this->checkIfTinEmployerExistWhenUpdate($input);
            $this->checkValidationEmployerName($input, 2);
            $employer->update(['has_tin_existing' => (($this->checkIfTinAlreadyExist($input['tin'])['tin_employer'])->count() > 1) ? 1 : 0,]);
            return $employer;
        });
    }


    /**
     * @param $id
     * @param int $status
     * @return mixed
     * @throws GeneralException
     */
    public function activate($id, $status = 1)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        // Log::info(print_r($employer, true)); 
        $employer->update(['approval_id' => $status]);
        if ($status == 1) {
            $this->generateRegNo($id);
            if ($employer->isonline == 1) {
                // Log::info('------- online');
                $this->generateMemberNumber($id);
            }
        }
        return $employer;
    }

    public function reject($id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        $employer->update(['approval_id' => 3]);
        $employer->delete();
        return $employer;
    }


    /**
     * @param $id
     * @return mixed
     * create Employees number for employer added from online
     */

    public function generateMemberNumber($id)
    { //save to MAC employees & employee_employerS & PORTAL employee_uploadS -update memberno

        $uploadedemployees = Employeeupload::where('employer_id', '=', $id)->get();

        foreach ($uploadedemployees as $employeeTemp) {

            if ($employeeTemp->memberno == NULL) {
                $data = ['firstname' => $employeeTemp->firstname, 'middlename' => $employeeTemp->middlename, 'lastname' => $employeeTemp->lastname, 'dob' => $employeeTemp->dob, 'gender_id' => NULL, 'sex' => $employeeTemp->gender, 'memberno' => 0, 'emp_cate' => $employeeTemp->emp_cate, 'created_at' => Carbon::now()];
                // new employee, create first
                /* db builder*/
                $registeredEmployeeId = DB::table('employees')->insertGetId(
                    $data
                );

                // $registeredEmployee = $employee->findOrThrowException($registeredEmployeeId);
                /*end db builder*/
                /* to change member_no name index in the new mac operation */
                $memberno = checksum($registeredEmployeeId, sysdefs()->data()->employee_number_length);

                // $registeredEmployee->save();
                /*update memberno*/
                DB::table('employees')
                    ->where('id', $registeredEmployeeId)
                    ->update(['memberno' => $memberno, 'updated_at' => Carbon::now()]);


                DB::table('employee_employer')->insert([
                    'employee_id' => $registeredEmployeeId,
                    'employer_id' => $id,
                    'basicpay' => $employeeTemp->salary,
                    'grosspay' => $employeeTemp->grosspay,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),

                ]);

                $employeeTemp->memberno = $memberno;
                $employeeTemp->save();
            }


        }

    }


    /**
     * @param $id
     * Generate Reg No for this employer
     */
    public function generateRegNo($id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        $sector = $employer->sectors()->first();
        $reg_no = Carbon::parse($employer->doc)->format('Y') . '-' . $sector->id . '-' . str_pad($employer->id, 6, "0", STR_PAD_LEFT);
        //        Update reg_no
        $employer->update(['reg_no' => $reg_no]);
        return $reg_no;
    }


    /**
     * @param $input
     * @return mixed
     * Employer Registration
     */

    public function register($input)
    {
        return DB::transaction(function () use ($input) {
            $employer_counts = new EmployeeCountRepository();
//       store employer registration

            $employer_registration = $this->create($input);
            /* Attach sector */
            $this->attachSector($employer_registration->id, $input['business_sector_cv_id']);
//            store employee statistics

            $employer_counts->createEmployeeCount($employer_registration->id, $input);

            $this->uploadDocuments($employer_registration->id, $input);

            return $employer_registration;
        });
    }


    /**
     * @param $input
     * @return mixed
     * Employer Registration modify
     */

    public function modifyRegister($id, $input)
    {
        return DB::transaction(function () use ($id, $input) {
            $employer_counts = new EmployeeCountRepository();
//       update employer registration

            $employer_registration = $this->update($id, $input);
            $this->syncSector($employer_registration->id, $input['business_sector_cv_id']);
//            update employee statistics

            $employer_counts->updateEmployeeCount($employer_registration->id, $input);

            $this->uploadDocuments($employer_registration->id, $input);

            return $employer_registration;
        });
    }


    public function approveRegistration($id)
    {
        DB::transaction(function () use ($id) {
            $employer = $this->findWithoutScopeOrThrowException($id);
            $this->checkAllValidationsOnApprove($id);
            $employer->update(['approval_id' => 4]);
        });
    }


    /**
     * @param $id
     * @param $action_type
     * Keep track of certificate issues
     * Action Type; 1 => reissue, 0 => first issue
     */
    public function issueCertificate($id, $action_type)
    {
        DB::transaction(function () use ($id, $action_type) {
            $employer_certificate_issues = new EmployerCertificateIssue();
            $employer_certificate_issue = $employer_certificate_issues->query()->create([
                'user_id' => access()->id(),
                'employer_id' => $id,
                'is_reissue' => $action_type,
            ]);
        });
    }


    /**
     * @param $id
     * @param $input
     * Upload documents / Attachments
     * Method: Upload to insert new document(Attach), Update existing documents (Reattach) and Delete (Detach)
     */
    public function uploadDocuments($id, $input)
    {
        DB::transaction(function () use ($id, $input) {
            $user_id = access()->id();
            $employerRegistration = $this->findWithoutScopeOrThrowException($id);
            foreach ($input as $key => $value) {
                if (strpos($key, 'employer_document') !== false) {
                    $employer_document_id = substr($key, 17);
                    $path = $this->base . DIRECTORY_SEPARATOR . $employerRegistration->id;

                    $uploadedDocument = ($employerRegistration->documents()->where("document_id", $employer_document_id)->first()) ? $employerRegistration->documents()->where("document_id", $employer_document_id)->first()->pivot : null;

                    /* variable*/


                    if ($input['employer_document' . $employer_document_id] == 1) {
                        //  Attach document : New
                        if (!$uploadedDocument) {
                            $employerRegistration->documents()->attach($employer_document_id);
                            $uploadedDocument = $employerRegistration->documents()->where("document_id", $employer_document_id)->first()->pivot;
                            $this->saveDocument($employerRegistration, 'document_file' . $employer_document_id, $path, $uploadedDocument);

//                            $linked_file = $path . DIRECTORY_SEPARATOR . $uploadedDocument->document_id  . '.' . $uploadedDocument->ext  ;
                            $linked_file = $employerRegistration->employeeFile();
                            /*  Check validation on employee list upload  */


                            /*  upload employee list to employee temp  */
                            if ($employer_document_id == 37) {
                                $this->checkColumnOnUploadedEmployeeList($linked_file);

//                                $this->uploadEmployeeListJobs($employerRegistration->id);
//                                dispatch(new LoadEmployeeList($employerRegistration->id,$user_id));
                                $this->uploadEmployeeList($employerRegistration);
                            }
                            /* end upload  */


                        } else {

//                            $linked_file = $path .DIRECTORY_SEPARATOR . $uploadedDocument->document_id  . '.' . $uploadedDocument->ext  ;
                            $linked_file = $employerRegistration->employeeFile();

                            //  Update: If Exist (Re-Attach)
                            $document_file_name = 'document_file' . $employer_document_id;
                            /* check if new document is attached */
                            if (request()->hasFile($document_file_name)) {
                                /* If has file: remove and replace */
                                if (file_exists($linked_file)) {
                                    unlink($linked_file);
                                }


                                $this->saveDocument($employerRegistration, 'document_file' . $employer_document_id, $path, $uploadedDocument);

                                /*updated linked file*/
                                $updated_linked_file = $employerRegistration->employeeFile();

                                /*  upload employee list to employee temp  */
                                if ($employer_document_id == 37) {
                                    $this->checkColumnOnUploadedEmployeeList($updated_linked_file);

//                                    $this->uploadEmployeeListJobs($employerRegistration->id);
//                                    dispatch(new LoadEmployeeList($employerRegistration->id,$user_id));
                                    $this->uploadEmployeeList($employerRegistration);
                                }
                                /* end upload  */
                            }

                        }

                    } else {

                        //  delete document : When not applicable
                        if ($uploadedDocument) {
                            $linked_file = $path . DIRECTORY_SEPARATOR . $uploadedDocument->document_id . '.' . $uploadedDocument->ext;
                            unlink($linked_file);
                            //      Detach
                            $employerRegistration->documents()->detach($employer_document_id);
                        }

                    }
                }
            }
        });
    }


    /**
     * @param Model $employer
     * @throws JobException
     * Load employee list into Employee Temps
     */
    public function uploadEmployeeList(Model $employer)
    {

//        $linked_file = $this->linked_file;
        $employer_id = $employer->id;
        $linked_file = $employer->employeeFile();
        $user_id = access()->id();
        $employeeTemp = new EmployeeTempRepository();
        $document = new DocumentRepository();
        $employee_file_document = $document->getEmployeeFileDocument();
        /* start: Deleting any employee temps entry for this employee, ready for re-uploading */
        $employeeTemp->query()->where(['employer_id' => $employer_id])->delete();
        /*End: Deleting any employee temps entry for this employee */

        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($linked_file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();

        $verifyArr = ['firstname', 'middlename', 'lastname', 'dob', 'basicpay', 'allowance', 'gender', 'job_title', 'employment_category'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new JobException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */

        $pivot = $employer->documents()->where("document_id", $employee_file_document)->first()->pivot;

        /*update rows imported*/
        Excel::load($linked_file, function ($reader) use ($pivot) {
            $objWorksheet = $reader->getActiveSheet();
            //exclude the heading
            $pivot->total_rows = $objWorksheet->getHighestRow() - 1;
            $pivot->rows_imported = 0;
            $pivot->upload_error = NULL;
            $pivot->error = 0;
            $pivot->isuploaded = 0;
            $pivot->success = 0;
            $pivot->hasfileerror = 0;
            $pivot->hasinputerror = 0;
            $pivot->save();
        });

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($linked_file)
            ->chunk(250, function ($result) use ($employeeTemp, $employer_id, $user_id, $pivot, $employer) {
                $rows = $result->toArray();

                try {
                    //let's do more processing (change values in cells) here as needed
                    //$counter = 0;
                    foreach ($rows as $row) {
                        /* Changing date from excel format to unix date format ... */
                        $row['dob'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['dob'], 'YYYY-MM-DD');
                        /* start: Validating row entries */
                        $error_report = "";
                        $error = 0;
                        foreach ($row as $key => $value) {
                            if (trim($key) == 'dob') {
                                if (check_date_format($value) == 0) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            } elseif (in_array(trim($key), ['allowance'], true)) {
                                if (!is_numeric($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            } elseif (in_array(trim($key), ['basicpay'], true)) {
                                if (!is_numeric($value)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                            } elseif (in_array(trim($key), ['firstname', 'lastname'], true)) {
                                if (trim($value) == "" Or $value == NULL) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }

                            } elseif (trim($key) == 'gender') {
                                $male_gender = ['MALE', 'MALE  ', 'M', 'ME', 'MWANAUME', 'MVULANA', 'MME'];
                                $female_gender = ['FEMALE', 'F', 'KE', 'MWANAMKE', 'MSICHANA', 'MKE'];
                                $gender = array_merge($male_gender, $female_gender);
                                $value = trim($value);
                                $value = strtoupper($value);
                                if (!in_array($value, $gender)) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                                }
                            }

                        }

                        /* end: Validating row entries */
                        $allowance = str_replace(",", "", $row['allowance']);
                        $basicpay = str_replace(",", "", $row['basicpay']);
                        $grosspay = ($allowance + $basicpay);
                        $firstname = preg_replace('/[^A-Za-z\']/', '', $row['firstname']);
                        $middlename = preg_replace('/[^A-Za-z\']/', '', $row['middlename']);
                        $lastname = preg_replace('/[^A-Za-z\']/', '', $row['lastname']);

                        /* insert into db */

                        $data = ['firstname' => $firstname, 'middlename' => $middlename, 'lastname' => $lastname, 'basicpay' => $basicpay, 'grosspay' => $grosspay, 'dob' => $row['dob'], 'employer_id' => $employer_id, 'gender' => $row['gender'], 'job_title' => $row['job_title'], 'employment_category' => $row['employment_category'], 'error' => $error, 'error_report' => $error_report];
                        $employee_temp_id = DB::table('employee_temps')->insertGetId(
                            $data
                        );
                        /*update rows imported*/
                        $pivot->rows_imported = $employeeTemp->query()->select('id')->where('employer_id', $employer_id)->count();
                        $pivot->isuploaded = 1;
                        $pivot->save();


                    }

                } catch (\Exception $e) {
                    // an error occurred
                    $error_message = $e->getMessage();
                    $this->saveExceptionMessage($employer, $error_message);

                }


            }, true);

    }

    /* Save exception method when uploading employee list */
    public function saveExceptionMessage(Model $employer, $exception_message)
    {
        $document = new DocumentRepository();
        $employee_document_id = $document->getEmployeeFileDocument();
        $employer->documents()->syncWithoutDetaching([$employee_document_id => ['upload_error' => $exception_message, 'error' => 1,]]);
    }


    /**
     * @param Model $employer
     * Register employee list to LIVE
     */
    public function registerEmployeeList(Model $employer)
    {
        DB::table('employee_temps')->where('employer_id', $employer->id)->orderBy('id')->chunk(100, function ($employees) use ($employer) {
            // Process the records...
            dispatch(new RegisterEmployeeList($employees, $employer->id, access()->id()));
//            return false;
        });

    }


    public function undoLoadedEmployees(Model $employer)
    {
        DB::table('employee_temps')->where('employer_id', $employer->id)->orderBy('id')->chunk(100, function ($employees) use ($employer) {
            // Process the records...
            dispatch(new UndoLoadedEmployees($employees, $employer->id));
//            return false;
        });
    }


    /**
     * @param $id
     * @param $sector_id
     * Attach Sector
     */

    public function attachSector($id, $sector_id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        $employer->sectors()->attach($sector_id);
    }


    /**
     * @param $id
     * @param $sector_id
     * Detach sector
     */
    public function detachSector($id, $sector_id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        $employer->sectors()->detach($sector_id);
    }

    /**
     * @param $id
     * @param $sector_id
     * Sync Sectors : Need Updated if need to add multiple sectors to one employer.
     */

    public function syncSector($id, $sector_id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        $employer->sectors()->sync([$sector_id]);
    }


    public function getEmployeeCounts($id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        return $employer->employeeCounts;
    }


    /**
     * @param $id
     * Check if employee counts are filled
     */
    public function checkIfEmployeeCountsFilled($id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        if (!$employer->employeeCounts()->count()) {
//            If not filled
            throw new GeneralException(trans('exceptions.backend.compliance.employee_count_not_filled'));
        }
    }


    /**
     * @param $id
     * Check if Compulsory Documents are Attached
     */
    public function checkIfDocumentsAttached($id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        $compulsory_documents = $employer->documents()->where(function ($query) {
            $query->where('document_id', 31)->orWhere('document_id', 33);
        })->get();
        /*    Check if Tin is Attached */
        if (!$employer->documents()->where('document_id', 31)->first()) {
            throw new GeneralException(trans('exceptions.backend.compliance.tin_certificate_not_attached'));
        }
//        /*      Check if Brela is Attached */
//        if (!count($employer->documents()->where('document_id', 33)->first())){
//            /*  If not attached */
//            throw new GeneralException(trans('exceptions.backend.compliance.brela_certificate_not_attached'));
//        }

        /*      Check if list of employee is Attached */
//        if (!count($employer->documents()->where('document_id', 37)->first())){
//            throw new GeneralException(trans('exceptions.backend.compliance.list_of_employee_not_attached'));
//        }

//        $this->checkIfEmployeeListHasError($id);
    }


    /**
     * Check if employee list document is clean with no Errors
     */
    public function checkIfEmployeeListHasError($id)
    {
        $employer = $this->findWithoutScopeOrThrowException($id);
        $error = $employer->employeeTemps()->where('error', 1)->first();

        if ($error) {
//            If has error
            throw new GeneralException(trans('exceptions.backend.compliance.employee_list_doc_has_error'));
        }
    }


    /**
     * @param $id
     */
    public function checkAllValidationsOnApprove($id)
    {
        $this->checkIfEmployeeCountsFilled($id);
//        $this->checkIfDocumentsAttached($id);

    }


    /**
     * @param $linked_file
     * @throws GeneralException
     * Check if column header are all included
     */
    public function checkColumnOnUploadedEmployeeList($linked_file)
    {
        $headings = Excel::selectSheetsByIndex(0)
            ->load($linked_file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();

        $verifyArr = ['firstname', 'middlename', 'lastname', 'dob', 'basicpay', 'allowance', 'gender', 'job_title', 'employment_category'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new GeneralException(trans('exceptions.backend.upload.list_of_employee_error') . ':' . trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }

        }
    }

    /*----END OF REGISTRATION -----------------------*/


    /**
     * GRAPH SUMMARY
     */
    /* Get Summary of monthly registered employers */
    public function getTotalMonthlyRegisteredEmployers()
    {
        $data = [];
        /* Get Monthly Target*/
        $fiscalYear = new FiscalYearRepository();
        $fiscal_year = $fiscalYear->findByFiscalYear(financial_year());
        $monthly_target = ($fiscal_year) ? $fiscal_year->monthlyTarget('ATTEMPLYR') : 0;
        /* end monthly target */
        /* Get Total before new Financial Year - Open Total Amount*/
        $data_table = [];

        $count_open = DB::table('main.comp_yearly_employer_registration')->select(["name", "fin_year_id", "employers"])->where("fin_year_id", fin_year()->prevFinYearID())->first();

        $total_open = $count_open->employers;
        $period_open = $count_open->name;
        $data_table[] = [$period_open, (int)$total_open, 0, 0];
        /* end open */

        $employers = DB::table('main.comp_monthly_employer_registration')->select(["period", "employers"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $total_all = 0;
        foreach ($employers as $value) {
            $total = $value->employers;
            $total_all += $total;
            $period = $value->period;
            $data[] = [$period, (int)$total]; //number_format( $sum , 2 , '.' , ',' )
            $data_table[] = [$period, (int)$total, $monthly_target, $this->targetVariancePercentage($total, $monthly_target)];
        }

        $overall_total = $total_open + $total_all;
        $return['graph_employer_registered'] = $data;
        $return['table_employer_registered'] = $data_table;
        $return['total_employers_registered'] = $overall_total;
        return $return;
    }


    /* Get Summary of Employer Registered Per Regions */
    public function getTotalRegisteredEmployersPerRegion()
    {
        $data = [];

        $regions = DB::table('main.comp_employers_per_region')->select(["name", "employers"])->orderBy("name", "asc")->get()->all();

        foreach ($regions as $value) {
            $data[] = [$value->name, (int)$value->employers];
        }

        $return['graph_registered_per_region'] = $data;
        return $return;
    }


    /* Employer Business statuses */
    public function getEmployerBusinessStatus()
    {
        $employer = DB::table('main.comp_employer_business_status')->first();

        $active_public = $employer->active_public;
        $active_private = $employer->active_private;
        $dormant_public = $employer->dormant_public;
        $dormant_private = $employer->dormant_private;
        $closed_public = $employer->closed_public;
        $closed_private = $employer->closed_private;
        $online_registered = $employer->online_registered;
        $large_contributor = $employer->large_contributor;
        $all_employer = $employer->all_employers;
        $large_contributor_percent = ($large_contributor / $all_employer) * 100;


        $total_active_employer = $active_public + $active_private;
        $total_dormant_employer = $dormant_private + $dormant_public;
        $total_closed_employer = $closed_private + $closed_public;

        $online_registered_percent = ($online_registered / $total_active_employer) * 100;
        /*get percent*/
        $total = $total_active_employer + $total_dormant_employer + $total_closed_employer;
        $active_employer_percent = $this->getPercent($total_active_employer, $total);
        $dormant_employer_percent = $this->getPercent($total_dormant_employer, $total);
        $closed_employer_percent = $this->getPercent($total_closed_employer, $total);

        return ['active_public_employer' => $active_public, 'active_private_employer' => $active_private, 'dormant_public_employer' =>
            $dormant_public, 'dormant_private_employer' => $dormant_private, 'closed_public_employer' => $closed_public, 'closed_private_employer' => $closed_private, 'total_active_employer' => $total_active_employer, 'total_dormant_employer' => $total_dormant_employer, 'total_closed_employer' => $total_closed_employer,
            'active_employer_percent' => $active_employer_percent,
            'dormant_employer_percent' => $dormant_employer_percent,
            'closed_employer_percent' => $closed_employer_percent,
            'large_contributor' => $large_contributor,
            'large_contributor_percent' => $large_contributor_percent,
            'online_registered' => $online_registered,
            'online_registered_percent' => $online_registered_percent

        ];
    }


    /* Certificate Issues  */
    public function getCertificateIssues()
    {
        $data = [];
        /* Get Monthly Target*/
        $fiscalYear = new FiscalYearRepository();
        $fiscal_year = $fiscalYear->findByFiscalYear(financial_year());
        $monthly_target = ($fiscal_year) ? $fiscal_year->monthlyTarget('ATTCERT') : 0;

        $certificate = DB::table('main.comp_monthly_employer_certificate')->select(["period", "certificate"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $prev_certificate = DB::table('main.comp_yearly_employer_certificate')->select(["name", "certificate"])->where("fin_year_id", fin_year()->prevFinYearID())->first();

        /* end monthly target */
        /* Get Total before new Financial Year - Open Total Amount*/
        $data_table = [];
        $total_open = $prev_certificate->certificate;
        $period_open = $prev_certificate->name;
        $data_table[] = [$period_open, $total_open, 0, 0];
        /* end open */

        $total = 0;
        foreach ($certificate as $value) {
            $period = $value->period;
            $total += $value->certificate;
            $data[] = [$period, (int)$value->certificate]; //number_format( $sum , 2 , '.' , ',' )
            $data_table[] = [$period, $value->certificate, $monthly_target, $this->targetVariancePercentage($value->certificate, $monthly_target)];
        }

        $overall_total = $total + $total_open;
        $return['graph_employer_certificate_issues'] = $data;
        $return['employer_certificate_issues_table'] = $data_table;
        $return['total_certificates_issued'] = $overall_total;
        return $return;
    }


    /* eND OF Graph summaries */


    /**
     *
     * Post Interest with Board Adjustment
     */
    public function postInterestWithBoardAdjustSchedule()
    {

        $this->query()->select(['id'])->chunk(100, function ($employers) {
            if (isset($employers)) {
                dispatch(new PostInterestWithBoardAdjust($employers));
            }
        });
    }

    public function calculateBookingSchedule($rcv_date = null)
    {
        $fin_code_groups = new FinCodeGroupRepository();
        $this->query()->whereNull("employer_closure_id")->whereHas('receiptCodes', function ($query) use ($fin_code_groups) {
            $query->whereHas('finCode', function ($query) use ($fin_code_groups) {
                $query->where('fin_code_group_id', '=', $fin_code_groups->getContributionFinCodeGroupId());
            })->whereHas('receipt', function ($query) {
                $query->where('iscancelled', 0)->where('isdishonoured', 0);
            });
        })->select(['id'])->chunk(100, function ($employers) use ($rcv_date) {
            if (isset($employers)) {
                dispatch(new CalculateBooking($employers, $rcv_date));
            }

        });
    }

    /*retrieve all agents from WCFePG*/
    public function getAgents($id)
    {
        $agents = DB::table('portal.users')->select('users.name as agent_name', 'users.phone', 'users.email', 'employers.name as employer_name', 'employers.tin', 'employers.reg_no', 'regions.name as region_name', 'employers.email as employer_email', 'employers.phone as employers_phone')
            ->join('portal.employer_user', 'users.id', '=', 'employer_user.user_id')
            ->join('main.employers', 'employer_user.employer_id', '=', 'employers.id')
            ->join('main.regions', 'employers.region_id', '=', 'regions.id')
            ->where('employer_user.user_id', '=', $id)
            ->where('employers.reg_no', '!=', NULL)
            ->get();
        return $agents;
    }

    /**
     * Categorize employers contributuors
     */
    public function categorizeEmployerContributors()
    {

        /*delete all duplicate entries*/
        //TODO: remove after it is rectified
        $service = new \App\Services\Compliance\CategorizeEmployerContributors();
        $check = $service->checkIfThereIsDuplicateEmployer();
        if ($check) {
            $service->resetEmployersIdForDuplicateEmployers();
        }

        /*dispatch categorizing employers job*/
        DB::table('contributing_employers')
            ->join('employers', 'employers.id', 'contributing_employers.employer_id')
//            ->where('employer_status','<>',3)
            ->where('is_treasury', false)
            ->whereNull('duplicate_id')
            ->select(['employers.id'])->orderBy('employers.id')->chunk(100, function ($employers) {
                if (isset($employers)) {
                    dispatch(new CategorizeEmployerContributors($employers));
                }

            });
    }


    /**
     * Update employers status i.e. Active and dormant
     */
    public function updateEmployersStatus()
    {
        $active_status_for_update = [1,2];
        $this->query()->where('approval_id', 1)->whereIn('employer_status', $active_status_for_update)->whereNull('duplicate_id')->select(['id'])->chunk(100, function ($employers) {
            if (isset($employers)) {
                dispatch(new UpdateEmployersStatusJob($employers));
            }
        });
    }

    /**
     * @param Model $employer
     * Post bookings for non booked contributions for all non contributors - does not have contribution
     */
    public function postBookingForNonContributors()
    {
        $this->query()->select('id')->where('approval_id', 1)->where('is_treasury', false)->whereNull('duplicate_id')->doesntHave('bookings')->orderBy('id')->chunk(100, function ($employers) {
            // Process the records...
            dispatch(new PostBookingsForNonContributors($employers));
//            return false;
        });
    }


    /**
     * Get employers who has contributed at least once and has bookings which are not yet contributed
     */
    public function updateBookingsNotContributed()
    {
        $this->query()->select('id')->where('is_treasury', false)->whereHas('bookings', function ($query) {
            $query->whereDoesntHave('receiptCodes', function ($query) {
                $query->has('activeReceipt');
            });
        })->orderBy('id')->chunk(100, function ($employers) {
            // Process the records...
            dispatch(new UpdateBookingsNotContributed($employers));
//            return false;
        });
    }


    /**
     * Get employers who has contributed at least once and has bookings which are not yet contributed
     */
    public function postBookingForSkippedMonths()
    {
//        $this->query()->select('id')->whereHas('bookings', function($query){
//            $query->whereHas('receiptCodes', function($query){
//                $query->whereHas('receipt',function($query){
//                    $query->where('iscancelled',0);
//                });
//            });
//        })->orderBy('id')->chunk(100, function
//        ($employers)  {
//            // Process the records...
//            dispatch(new PostBookingForSkippedMonths($employers));
////            return false;
//        });


//        $this->query()->where('is_treasury', false)->select('id')->has('bookings')->whereDate('created_at', '>', standard_date_format('2018-06-30'))->orderBy('id')->chunk(100, function
//        ($employers)  {
//            // Process the records...
//            dispatch(new PostBookingForSkippedMonths($employers));
////            return false;
//        });


        $this->query()->where('is_treasury', false)->where('employer_status', '<>', 3)->select('id')->has('bookings')->orderBy('id')->chunk(100, function ($employers) {
            // Process the records...
            dispatch(new PostBookingForSkippedMonths($employers));
//            return false;
        });

    }

    /**
     * post booking for employers in legacy receipts only
     */
    public function postBookingForEmployerInLegacyReceiptOnly()
    {

//        $this->query()->has('legacyReceipts')->doesntHave('bookings')->where('is_treasury', false)->orderBy('id')->chunk(100, function ($employers) {
        $this->query()->has('legacyReceipts')->where('is_treasury', false)->orderBy('id')->chunk(100, function ($employers) {
            // Process the records...
            dispatch(new PostBookingForSkippedMonthsLegacy($employers));
//            return false;
        });
    }


    /**
     * @param $employer_id
     * Get all incidents/notification reports with pay status for employer for datatable
     */
    public function getIncidentsWithPayStatusForDt($employer_id)
    {
        $incidents = DB::table('employers_incidents_view as i')->select(
            'i.notification_report_id as notification_report_id',
            'n.incident_date as incident_date',
            't.name as incident_type_name',
            DB::raw(" concat_ws(' ', ee.firstname, ee.middlename, ee.lastname) as employee_name "),
            'e.name',
            'n.incident_date',
            'i.pay_status'
        )
            ->join('incident_types as t', 't.id', 'i.incident_type_id')
            ->join('employers as e', 'e.id', 'i.employer_id')
            ->join('employees as ee', 'ee.id', 'i.employee_id')
            ->join('notification_reports as n', 'n.id', 'i.notification_report_id')
            ->where('n.employer_id', $employer_id);

        return $incidents;
    }


    /**
     * @param Model $employer
     * @param array $input
     * Update general info
     */
    public function updateGeneralInfo(Model $employer, array $input)
    {
        return DB::transaction(function () use ($employer, $input) {
            $employer->update([
                'parent_id' => isset($input['parent_id']) ? $input['parent_id'] : null,
//                'annual_earning' => $input['annual_earning'],
                'business_activity' => $input['business_activity'],
                'employer_category_cv_id' => $input['employer_category_cv_id'],
                'is_treasury' => isset($input['is_treasury']) ? $input['is_treasury'] : 0,
            ]);
            /*sync businesses*/
            $employer->businesses()->sync($input['business_sector_cv_id']);
            return $employer;
        });
    }

    //get employers claims
    public function getEmployerClaimDataTable()
    {
        $employers = $this->query()->select([
            DB::raw("employers.id"),
            DB::raw("employers.name as name"),
            DB::raw("regions.name as region"),
            DB::raw("districts.name as district"),
            DB::raw("reg_no"),
            DB::raw("doc"),
            DB::raw("counts"),
            DB::raw("bsns.name as bussines_sector"),
            DB::raw("emplyer.name as employer_category"),
            DB::raw("notif.counts as counts"),
        ])
            ->join("code_values as emplyer", "emplyer.id", "=", "employers.employer_category_cv_id")
            ->leftJoin("employer_sectors", "employer_sectors.employer_id", "=", "employers.id")
            ->leftJoin("code_values as bsns", "bsns.id", "=", "employer_sectors.business_sector_cv_id")
            ->leftJoin("districts", "districts.id", "=", "employers.district_id")
            ->leftJoin("regions", "regions.id", "=", "employers.region_id")
            ->join(DB::raw("(SELECT notification_reports.employer_id, COUNT(*) as counts from notification_reports  group by notification_reports.employer_id) as notif"), function ($join) {
                $join->on ( 'notif.employer_id', '=', 'employers.id' );
            })->distinct("employer_sectors.employer_id");

        $employer = request()->input("employer");
        if ($employer) {
            $employers->where("employers.id", "=", $employer);
        }
        $region = request()->input("region");
        if ($region) {
            $employers->where("regions.id", "=", $region);
        }
        $district = request()->input("district");
        if ($district) {
            $employers->where("districts.id", "=", $district);
        }
        $employer_category = request()->input("employer_category");
        if ($employer_category) {
            $employers->where("emplyer.id", "=", $employer_category);
        }
        $bussines_sector = request()->input("bussines_sector");
        if ($bussines_sector) {
            $employers->where("bsns.id", "=", $bussines_sector);
        }

        return $employers;


    }

    //get workplaces of employer
    public function workplaces($employer_id){
        $workplaces = DB::table('workplaces')
            ->select('*')
            ->where('employer_id',$employer_id)
            ->get();

        return $workplaces;
    }

    /**
     * @param $employer_id
     * @return mixed
     * Get compliance staff assigned employer
     */
    public function getComplianceStaffAssigned($employer_id)
    {
        $employer = $this->find($employer_id);
        if($employer) {
            $staff = $employer->complianceOfficers()->where('isactive', 1)->first();

            if (isset($staff)) {
                return $staff->pivot->user_id;
            }
        }
        return null;
    }


    /*Get all portal contact person for this employer - Only Active*/
    public function getPortalContactPerson($employer_id)
    {
        $portal_users = $this->query()->select(
            'pcv.name as user_type',
            'pu.name as user_name',
            'pu.email as user_email',
            'pu.phone as user_phone'
        )
            ->join('portal.employer_user as peu','peu.employer_id', 'main.employers.id')
            ->join('portal.users as pu', function($join){
                $join->on('peu.user_id','pu.id')->where('pu.isactive', 1);
            })
            ->join('portal.code_values as pcv', 'pcv.id', 'pu.user_type_cv_id')
            ->where('employers.id', $employer_id)
            ->get();

        return $portal_users;
    }

    /**
     * @param $employer_id
     * @return bool
     * Check if employer has multiple payrolls on portal
     */
    public function checkIfEmployerHasMultiplePayrolls($employer_id)
    {
        $return = false;
        $check = DB::table('portal.employer_user as eu')->select('eu.employer_id',
            DB::raw("min(eu.payroll_id) as min_payroll_id"),
            DB::raw("max(eu.payroll_id) as max_payroll_id")
        )->where('eu.employer_id', $employer_id)->whereNull('eu.mac_deactivated_at')
            ->groupBy('eu.employer_id')->first();

        if($check){
            if($check->min_payroll_id != $check->max_payroll_id){
                $return = true;
            }
        }

        return $return;
    }

    /*get Payrolls of employer from portal*/
    public function getEmployerPayrolls($employer_id)
    {

        $payrolls = DB::table('portal.employer_user as eu')->select('eu.employer_id',
         'eu.payroll_id',
            DB::raw(" concat_ws(' ', 'Payroll ', eu.payroll_id) as payroll_name")
        )->where('eu.employer_id', $employer_id)->whereNull('eu.mac_deactivated_at')
            ->groupBy('eu.employer_id', 'eu.payroll_id')->get();
        return $payrolls;
    }


    /*Update receivable schedule*/
    public function updateRcvableSchedule($schedule, $start_date, $end_date)
    {
        DB::statement("update employers set rcvable_schedule = 0");
//switch($schedule){
//    case 6:
////        DB::statement("UPDATE employers e
////SET rcvable_schedule = 6
////FROM bookings_mview b
////WHERE e.id = b.employer_id and b.ispaid = 1   and b.rcv_date >= '{$start_date}' and b.rcv_date <= '{$end_date}' and b.receipt_date <= '{$end_date}'");
////
////        DB::statement("update employers set rcvable_schedule = 6 where approval_id = 1 and  id not in (select employer_id FROM bookings_mview b
////WHERE  b.ispaid = 1   and b.rcv_date >= '{$start_date}' and b.rcv_date <= '{$end_date}' and b.receipt_date <= '{$end_date}')");
////        break;
        switch($schedule){
            case 2:
                DB::statement("UPDATE employers e
SET rcvable_schedule = 2
FROM bookings_mview b
WHERE e.id = b.employer_id and b.ispaid = 1   and b.rcv_date >= '{$start_date}' and b.rcv_date <= '{$end_date}' and b.receipt_date <= '{$end_date}'");;
                break;
            case 6:
                DB::statement("update employers set rcvable_schedule = 6 where approval_id = 1 and  id not in (select employer_id FROM bookings_mview b
WHERE  b.ispaid = 1   and b.rcv_date >= '{$start_date}' and b.rcv_date <= '{$end_date}' and b.receipt_date <= '{$end_date}')");
                break;
        }
    }
}