<?php
namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Models\Operation\Compliance\Member\ContributionModification\ContribModification;
use App\Repositories\BaseRepository;
use DB;
use Carbon\Carbon;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\GeneralException;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Services\Workflow\Workflow;
use App\Models\Workflow\WfTrack;
use App\Events\NewWorkflow;
use App\Exceptions\WorkflowException;
use Log;

class ContributionModificationRepository extends BaseRepository
{
	use FileHandler, AttachmentHandler;
	const MODEL = ContribModification::class;

	public function __construct()
	{
		parent::__construct();
		$this->wf_module_group_id = 37;

	}

	public function findOrThrowException($modification_request_id)
	{
		$modification_request = $this->query()
		->find($modification_request_id);
		if (!is_null($modification_request))
		{
			return $modification_request;
		}
		throw new GeneralException('Request Failed! Contribution modification request not found');
	}

	public function saveNew(array $input, $request)
	{
		$receipt = (int)$request->receipt_type == 2 ? (new LegacyReceiptRepository())
		->find($input['receipt_id']) : (new ReceiptRepository())->find($input['receipt_id']);
		switch ($input['request_type'])
		{
			case '1':
			//save modify months
			$return = $this->saveChangeMonths($input, $receipt, $request);
			break;
			case '2':
			// save amount redistribution
			$return = $this->saveAmountRedistribution($input, $receipt, $request);
			break;
			default:

			break;
		}

		return $return ? $return : false;
	}

	public function getRequestsForDataTable($employer_id)
	{
		return $this->query()
		->where('employer_id', $employer_id);
	}

	public function saveChangeMonths($input, $receipt, $request)
	{
		return DB::transaction(function () use ($input, $receipt, $request)
		{
			$arr_receipt = [];
			$arr_change = [];
			foreach ($input as $key => $value)
			{
				if (strpos($key, 'month_new') !== false && !empty($value))
				{
					$code_id = (int)str_replace("month_new_", "", $key);
					$rct_code = (int)$request->receipt_type == 2 ? DB::table('main.legacy_receipt_codes')
					->find($code_id) : DB::table('main.receipt_codes')->find($code_id);
					if (!empty($rct_code->contrib_month))
					{
						$old_month = Carbon::parse($rct_code->contrib_month)
						->format('Y-m-28');
						$new_month = Carbon::parse($value)->format('Y-m-28');
						if ($old_month !== $new_month)
						{
							$arr_change[$code_id] = ['new' => $new_month, 'old' => $old_month];
						}
					}
				}
			}
			
			if (!empty($arr_change))
			{
				$receipt_codes = [];
				//save receipt codes zote history keeping
				foreach ($receipt->receiptCodes as $rct_code)
				{
					$receipt_codes[$rct_code->id] = ['id' => $rct_code->id, 'fin_code_id' => $rct_code->fin_code_id, 'contrib_month' => $rct_code->contrib_month, 'amount' => $rct_code->amount, 'employee_count' => $rct_code->member_count];
				}

				$request_attachment = request()->hasFile('request_attachment') ? 'request_attachment.' . request()
				->file('request_attachment')
				->getClientOriginalExtension() : null;
				$receipt_attachment = request()->hasFile('receipt_attachment') ? 'receipt_attachment.' . request()
				->file('receipt_attachment')
				->getClientOriginalExtension() : null;

				//save request
				$modification_request = $this->query()
				->create(['employer_id' => $input['employer_id'], 'receipt_id' => $input['receipt_id'], 'request_type' => $input['request_type'], 'user_id' => access()->user()->id, 'rctno' => $receipt->rctno, 'amount' => $receipt->amount, 'status' => 0, 'old_rct_description' => $receipt->description, 'date_received' => Carbon::parse($receipt->request_date)
					->format('Y-m-d') , 'reason' => 'Employer Request', 'payroll_id' => !empty($receipt->payroll_id) ? $receipt->payroll_id : 1, 'receipt_codes' => !empty($receipt_codes) ? serialize($receipt_codes) : null, 'request_attachment' => $request_attachment, 'receipt_attachment' => $receipt_attachment, 'reason' => $input['reason'], 'receipt_type' => (int)$request->receipt_type]);

				//save each month changed
				foreach ($arr_change as $index => $change)
				{
					DB::table('main.contrib_modification_months')->updateOrInsert(['contrib_modification_id' => $modification_request->id, 'receipt_code_id' => $index, 'receipt_code_type' => (int)$request->receipt_type], ['old_month' => $change['old'], 'new_month' => $change['new'], 'created_at' => Carbon::now() , 'updated_at' => Carbon::now(), 'fin_code_id' => (int)$input["payment_type_".$index] ]);
				}
				//save attachment
				$this->saveAttachment($request, $modification_request->id);
				return $modification_request;
			}
			
		});
	}

	public function saveAmountRedistribution($input, $receipt, $request)
	{
		return DB::transaction(function () use ($input, $receipt, $request)
		{
			$new = [];
			foreach ($input as $key => $value)
			{
				if (strpos($key, 'contrib_month') !== false)
				{
					$key_id = str_replace("contrib_month_", "", $key);
					$new[$key_id] = [
						'contrib_month' => Carbon::parse($value)->format('Y-m-28'),
						'amount' => $input["contrib_amount_".$key_id],
						'member_count' => !empty($input["employee_count_".$key_id]) ? $input["employee_count_".$key_id] : 0,
						'fin_code_id' => $input["payment_type_".$key_id],
					];
				}
			}

			$receipt_codes = [];

			$receipts_history =  (int)$request->receipt_type == 2 ? $receipt->legacyReceiptCodes : $receipt->receiptCodes;
			foreach ($receipts_history as $rct_code)
			{
				//keep history of receipts codes
				$receipt_codes[$rct_code->id] = ['id' => $rct_code->id, 'fin_code_id' => $rct_code->fin_code_id, 'contrib_month' => $rct_code->contrib_month, 'amount' => $rct_code->amount, 'employee_count' => $rct_code->member_count];
			}
			$request_attachment = request()->hasFile('request_attachment') ? 'request_attachment.' . request()
			->file('request_attachment')
			->getClientOriginalExtension() : null;
			$receipt_attachment = request()->hasFile('receipt_attachment') ? 'receipt_attachment.' . request()
			->file('receipt_attachment')
			->getClientOriginalExtension() : null;

			$modification_request = $this->query()
			->create(['employer_id' => $input['employer_id'], 'receipt_id' => $input['receipt_id'], 'request_type' => $input['request_type'], 'user_id' => access()->user()->id, 'rctno' => $receipt->rctno, 'amount' => $receipt->amount, 'status' => 0, 'old_rct_description' => $receipt->description, 'date_received' => Carbon::parse($receipt->request_date)
				->format('Y-m-d') , 'reason' => 'Employer Request', 'payroll_id' => !empty($receipt->payroll_id) ? $receipt->payroll_id : 1, 'receipt_codes' => !empty($receipt_codes) ? serialize($receipt_codes) : null, 'request_attachment' => $request_attachment, 'receipt_attachment' => $receipt_attachment, 'reason' => $input['reason'], 'receipt_type' => (int)$request->receipt_type]);

			foreach ($new as $index => $change)
			{
				$change['contrib_modification_id'] = $modification_request->id;
				$change['created_at'] = Carbon::now();
				$change['updated_at'] = Carbon::now();
				DB::table('main.contrib_modification_redistributions')->insert($change);
			}
			$this->saveAttachment($request, $modification_request->id);
			return $modification_request;
		});
	}

	public function checkReceiptHasPendingRequest($receipt_id, $receipt_type, $modification_request = null)
	{
		$rct_type = !empty($receipt_type) ? (int)$receipt_type : 1;
		
		if(!empty($modification_request)){
			if ($modification_request->receipt_id != $receipt_id) {
				$pending = $this->query()
				->where('receipt_id', $receipt_id)->where('wf_done', 0)
				->whereNull('deleted_at')
				->first();
			}
		}else{
			$pending = $this->query()
			->where('receipt_id', $receipt_id)->where('wf_done', 0)
			->whereNull('deleted_at')
			->first();
		}
		return count($pending) ? true : false;
	}

	public function saveAttachment($request, $modification_request_id)
	{
		if ($request->file('request_attachment'))
		{
			$file_name_and_ext = $request->file('request_attachment')
			->getClientOriginalName();
			$filename = pathinfo($file_name_and_ext, PATHINFO_FILENAME);
			$file_ext = $request->file('request_attachment')
			->getClientOriginalExtension();
			$store_file_name = 'request_attachment.' . $file_ext;
			$path = $request->file('request_attachment')
			->storeAs('public'.DIRECTORY_SEPARATOR.'contrib_modification'.DIRECTORY_SEPARATOR. $request->employer_id . DIRECTORY_SEPARATOR . $modification_request_id . DIRECTORY_SEPARATOR, $store_file_name);
		}

		if ($request->file('receipt_attachment'))
		{
			$receipt_name_and_ext = $request->file('receipt_attachment')
			->getClientOriginalName();
			$receipt_name = pathinfo($receipt_name_and_ext, PATHINFO_FILENAME);
			$receipt_ext = $request->file('receipt_attachment')
			->getClientOriginalExtension();
			$store_receipt_name = 'receipt_attachment.' . $receipt_ext;
			$path = $request->file('receipt_attachment')
			->storeAs('public'.DIRECTORY_SEPARATOR.'contrib_modification'.DIRECTORY_SEPARATOR. $request->employer_id .DIRECTORY_SEPARATOR. $modification_request_id.DIRECTORY_SEPARATOR, $store_receipt_name);
		}

	}

	public function getAttachmentPath($modification_request_id)
	{
		$modification_request = $this->findOrThrowException($modification_request_id);

		if (test_uri())
		{
			$file_path = asset('storage' . DIRECTORY_SEPARATOR . 'contrib_modification' . DIRECTORY_SEPARATOR . $modification_request->employer_id . DIRECTORY_SEPARATOR . $modification_request->id);
			$request_path = !empty($modification_request->request_attachment) ? $file_path . DIRECTORY_SEPARATOR . $modification_request->request_attachment : null;
			$receipt_path = !empty($modification_request->receipt_attachment) ? $file_path . DIRECTORY_SEPARATOR . $modification_request->receipt_attachment : null;

		}
		else
		{
			$public_path = asset('public/storage' . DIRECTORY_SEPARATOR . 'contrib_modification' . DIRECTORY_SEPARATOR . $modification_request->employer_id . DIRECTORY_SEPARATOR . $modification_request->id);
			$request_path = !empty($modification_request->request_attachment) ? $public_path . DIRECTORY_SEPARATOR . $modification_request->request_attachment : null;
			$receipt_path = !empty($modification_request->receipt_attachment) ? $public_path . DIRECTORY_SEPARATOR . $modification_request->receipt_attachment : null;
		}
		return ['request_path' => $request_path, 'receipt_path' => $receipt_path];
	}

	public function submitForReviewAndApproval($modification_request)
	{
		$return = DB::transaction(function () use ($modification_request)
		{
			$check = workflow([['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $modification_request->id, 'type' => 0]])
			->checkIfHasWorkflow();
			if (!$check)
			{
				$this->query()
				->where('id', $modification_request->id)
				->update(['status' => 1, 'updated_at' => Carbon::now() ]);
				event(new NewWorkflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $modification_request->id, 'type' => 0],[],["comments"=>$modification_request->reason]));
			}
			return true;
		});
		return $return;
	}

	public function updateWorkflowStatus($resource_id, $current_level, $next_level, $status = null)
	{
		$current_level = (int)$current_level;
		$next_level = (int)$next_level;

		if (($current_level < $next_level))
		{
            // forward
			if ($current_level == 1)
			{
				$this->query()
				->where('id', $resource_id)->update(['status' => 1]);
			}

			if ($current_level == 4)
			{
                //status == 1 decline || status == 0 approve
				$approved_status = $status == 1 ? 3 : 2;
				$modification_request = $this->findOrThrowException($resource_id);

				if ($approved_status == 2)
				{
					$result = false;
					switch ($modification_request->request_type)
					{
						case 1:
                            //change months
						$result = $this->updateMonthsAfterApproval($modification_request);
						break;
						case 2:
                            //redistribute amount
						$result = $this->redistributeAmountAfterApproval($modification_request);
						break;
						default:
						break;
						if (!$result)
						{
							throw new WorkflowException('Approval Failed! Something went wrong. Kindly try again or contact system administrator.');
						}
					}
				}

				$this->query()
				->where('id', $resource_id)->update(['status' => $approved_status, 'wf_done' => 1, 'new_rct_description' => $approved_status == 2 ? $modification_request
					->receipt->description : null, 'wf_done_date' => Carbon::now() ]);
			}
		}
		elseif ($current_level == 5 && $next_level == 0)
		{
            //level 5 akiforward
			$modification_request = $this->findOrThrowException($resource_id);
			if ($modification_request->letters[0]->isinitiated != 1)
			{
				throw new WorkflowException('Response letter not initiated! Kindly Initiate to proceed!');
			}
			else
			{
				$this->query()
				->where('id', $resource_id)->update(['wf_ended' => true]);
			}
		}
		else
		{
            //reverse
			if ($current_level == 5)
			{
				// $this->query()->where('id', $resource_id)->update(['wf_done' => 0, 'wf_done_date'=>null,'status'=>1]);
				throw new WorkflowException('Request Failed! This workflow can\'t be reversed at this level');
			}
			if ($next_level == 1 && $next_level != 5)
			{
				$this->query()
				->where('id', $resource_id)->update(['status' => 3]);
			}
		}
	}

	public function updateChangeMonths($input, $receipt, $request_id)
	{
		return DB::transaction(function () use ($input, $receipt, $request)
		{
			$arr_receipt = [];
			$arr_change = [];
			foreach ($input as $key => $value)
			{
				if (strpos($key, 'month_new') !== false && !empty($value))
				{
					$arr_receipt[substr($key, 10) ] = $value;
				}
			}
			foreach ($arr_receipt as $key => $value)
			{
				$rct_code = (int)$request->receipt_type == 2 ? DB::table('main.legacy_receipt_codes')
				->find($key) : DB::table('main.receipt_codes')->find($key);
				if (!empty($rct_code->contrib_month))
				{
					$old_month = Carbon::parse($rct_code->contrib_month)
					->format('Y-m-28');
					$new_month = Carbon::parse($value)->format('Y-m-28');
					if ($old_month !== $new_month)
					{
						$arr_change[$key] = ['new' => $new_month, 'old' => $old_month];
					}
				}
			}
			if (count($arr_change))
			{
				$receipt_codes = [];
				foreach ($receipt->receiptCodes as $rct_code)
				{
					$receipt_codes[$rct_code->id] = ['id' => $rct_code->id, 'fin_code_id' => $rct_code->fin_code_id, 'contrib_month' => $rct_code->contrib_month, 'amount' => $rct_code->amount, 'employee_count' => $rct_code->member_count];
				}

				$request_attachment = request()->hasFile('request_attachment') ? 'request_attachment.' . request()
				->file('request_attachment')
				->getClientOriginalExtension() : null;
				$receipt_attachment = request()->hasFile('receipt_attachment') ? 'receipt_attachment.' . request()
				->file('receipt_attachment')
				->getClientOriginalExtension() : null;

				$modification_request = $this->query()
				->create(['employer_id' => $input['employer_id'], 'receipt_id' => $input['receipt_id'], 'request_type' => $input['request_type'], 'user_id' => access()->user()->id, 'rctno' => $receipt->rctno, 'amount' => $receipt->amount, 'status' => 0, 'old_rct_description' => $receipt->description, 'date_received' => Carbon::parse($receipt->request_date)
					->format('Y-m-d') , 'reason' => 'Employer Request', 'payroll_id' => !empty($receipt->payroll_id) ? $receipt->payroll_id : 1, 'receipt_codes' => !empty($receipt_codes) ? serialize($receipt_codes) : null, 'request_attachment' => $request_attachment, 'receipt_attachment' => $receipt_attachment, 'reason' => $input['reason'], 'receipt_type' => (int)$request->receipt_type]);

				foreach ($arr_change as $index => $change)
				{
					DB::table('main.contrib_modification_months')->updateOrInsert(['contrib_modification_id' => $modification_request->id, 'receipt_code_id' => $index, 'receipt_code_type' => (int)$request->receipt_type], ['old_month' => $change['old'], 'new_month' => $change['new'], 'created_at' => Carbon::now() , 'updated_at' => Carbon::now() ]);
				}
				$this->saveAttachment($request, $modification_request->id);
			}
			return $modification_request;
		});
	}

	public function updateMonthsAfterApproval($modification_request)
	{
		$return = DB::transaction(function () use ($modification_request)
		{
			switch ($modification_request->receipt_type)
			{
				case 2:
				return $this->updateMonthsLegacy($modification_request);
				break;
				case 1:
				return $this->updateMonthsReceipt($modification_request);
				break;
				default:
				return false;
				break;
			}
		});

		return $return;
	}

	public function updateMonthsReceipt($modification_request)
	{
		$receipt_repo = new ReceiptRepository();
		$receipt = $receipt_repo->find($modification_request->receipt_id);

		$modify_months = ["employer_id" => $modification_request->employer_id, ];

		foreach ($modification_request->months as $month)
		{
			$receipt_code = DB::table('main.receipt_codes')->where('id', $month->receipt_code_id)
			->first();

			if ($receipt_code->fin_code_id == 2) {
				$modify_months = array_merge($modify_months, ["contribution_month" . $receipt_code->id => Carbon::parse($month->new_month)
					->format('m') , "contribution_year" . $receipt_code->id => Carbon::parse($month->new_month)
					->format('Y') , "contribution_amount" . $receipt_code->id => $receipt_code->amount, "member_count" . $receipt_code->id => $receipt_code->member_count]);
			} else {
				$modify_months = array_merge($modify_months, ["contribution_month" . $receipt_code->id => Carbon::parse($month->new_month)
					->format('m') , "contribution_year" . $receipt_code->id => Carbon::parse($month->new_month)
					->format('Y') , "interest_amount" . $receipt_code->id => $receipt_code->amount, "member_count" . $receipt_code->id => $receipt_code->member_count]);
			}		
		}

		if (count($modification_request->months) > 0)
		{
			$receipt_repo->updateModificationMonths($receipt, $modify_months);
			return true;
		}
		else
		{
			Log::info('Contrib modification ' . $modification_request->id . ' has Failed! Nothing to update');
			return false;
		}
	}

	public function updateMonthsLegacy($modification_request)
	{
		$receipt_repo = new LegacyReceiptRepository();
		$receipt = $receipt_repo->find($modification_request->receipt_id);

		$modify_months = ["employer_id" => $modification_request->employer_id, ];

		foreach ($modification_request->months as $month)
		{
			$receipt_code = DB::table('main.legacy_receipt_codes')->where('id', $month->receipt_code_id)
			->first();

			if ($receipt_code->fin_code_id == 2) {
				$modify_months = array_merge($modify_months, ["contribution_month" . $receipt_code->id => Carbon::parse($month->new_month)
					->format('m') , "contribution_year" . $receipt_code->id => Carbon::parse($month->new_month)
					->format('Y') , "contribution_amount" . $receipt_code->id => $receipt_code->amount, "member_count" . $receipt_code->id => $receipt_code->member_count]);
			} else {
				$modify_months = array_merge($modify_months, ["contribution_month" . $receipt_code->id => Carbon::parse($month->new_month)
					->format('m') , "contribution_year" . $receipt_code->id => Carbon::parse($month->new_month)
					->format('Y') , "interest_amount" . $receipt_code->id => $receipt_code->amount, "member_count" . $receipt_code->id => $receipt_code->member_count]);
			}
		}

		if (count($modification_request->months) > 0)
		{
			$receipt_repo->updateModificationMonths($receipt, $modify_months);
			return true;
		}
		else
		{
			Log::info('Contrib modification ' . $modification_request->id . ' has Failed! Nothing to update');
			return false;
		}
	}

	public function redistributeAmountAfterApproval($modification_request)
	{
		$return = DB::transaction(function () use ($modification_request)
		{
			switch ($modification_request->receipt_type)
			{
				case 2:
				return $this->destributeLegacy($modification_request);
				break;
				case 1:
				return $this->distributeReceipt($modification_request);
				break;
				default:
				return false;
				break;
			}
		});

		return $return;
	}

	public function destributeLegacy($modification_request)
	{
		$check_months = [];
		$receipt_repo = new LegacyReceiptRepository();
		$receipt = $receipt_repo->find($modification_request->receipt_id);
		foreach ($modification_request->redistributions as $redistribution)
		{

			DB::table('main.legacy_receipt_codes')->updateOrInsert(['legacy_receipt_id' => $modification_request->receipt_id, 'contrib_month' => Carbon::parse($redistribution->contrib_month)
				->format('Y-m-28') , 'deleted_at' => null, 'fin_code_id' => $redistribution->fin_code_id], ['amount' => $redistribution->amount, 'member_count' => $redistribution->member_count, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ]);

			$rct_za_kuchek = DB::table('main.legacy_receipt_codes')->where(['legacy_receipt_id' => $modification_request->receipt_id, 'contrib_month' => Carbon::parse($redistribution->contrib_month)->format('Y-m-28') , 'deleted_at' => null, 'fin_code_id' => $redistribution->fin_code_id])->first();
			array_push($check_months, $rct_za_kuchek->id);
		}

		if (!empty($check_months))
		{
			DB::table('main.legacy_receipt_codes')->where('legacy_receipt_id', $modification_request->receipt_id)
			->whereNotIn('id', $check_months)->whereNull('deleted_at')
			->update(['deleted_at' => Carbon::now() ]);

			$receipt_codes = DB::table('main.legacy_receipt_codes')->where('legacy_receipt_id', $modification_request->receipt_id)
			->whereIn('id', $check_months)->whereNull('deleted_at')
			->get();

			$redistribute_months = ["employer_id" => $modification_request->employer_id];

			foreach ($receipt_codes as $destributed)
			{
				if ($receipt_code->fin_code_id == 2) {
					$redistribute_months = array_merge($redistribute_months, ["contribution_month" . $destributed->id => Carbon::parse($destributed->contrib_month)
						->format('m') , "contribution_year" . $destributed->id => Carbon::parse($destributed->contrib_month)
						->format('Y') , "contribution_amount" . $destributed->id => $destributed->amount, "member_count" . $destributed->id => $destributed->member_count]);;
				} else {
					$redistribute_months = array_merge($redistribute_months, ["contribution_month" . $destributed->id => Carbon::parse($destributed->contrib_month)
						->format('m') , "contribution_year" . $destributed->id => Carbon::parse($destributed->contrib_month)
						->format('Y') , "interest_amount" . $destributed->id => $destributed->amount, "member_count" . $destributed->id => $destributed->member_count]);;
				}
			}
			$receipt_repo->updateModificationMonths($receipt, $redistribute_months);
			return true;
		}
		else
		{
			Log::info('Contrib modification ' . $modification_request->id . ' has Failed! Nothing to update');
			return false;
		}
	}

	public function distributeReceipt($modification_request)
	{
		$check_months = [];
		$receipt_repo = new ReceiptRepository();
		$receipt = $receipt_repo->find($modification_request->receipt_id);
		foreach ($modification_request->redistributions as $redistribution)
		{
			$booking = DB::table('main.bookings')->whereNull('deleted_at')
			->where('employer_id', $modification_request->employer_id)
			->where('rcv_date', Carbon::parse($redistribution->contrib_month)
				->format('Y-m-28'))
			->first();

			DB::table('main.receipt_codes')
			->updateOrInsert(['receipt_id' => $modification_request->receipt_id, 'contrib_month' => Carbon::parse($redistribution->contrib_month)
				->format('Y-m-28') , 'deleted_at' => null, 'fin_code_id' => $redistribution->fin_code_id], ['amount' => $redistribution->amount, 'member_count' => $redistribution->member_count, 'employer_id' => $modification_request->employer_id, 'booking_id' => $booking->id, 'created_at' => Carbon::now() , 'updated_at' => Carbon::now() ]);

			$rct_za_kuchek = DB::table('main.receipt_codes')->where(['receipt_id' => $modification_request->receipt_id, 'contrib_month' => Carbon::parse($redistribution->contrib_month)->format('Y-m-28') , 'deleted_at' => null, 'fin_code_id' => $redistribution->fin_code_id])->first();
			array_push($check_months, $rct_za_kuchek->id);
		}

		if (!empty($check_months))
		{
			DB::table('main.receipt_codes')->where('receipt_id', $modification_request->receipt_id)
			->whereNotIn('id', $check_months)->whereNull('deleted_at')
			->update(['deleted_at' => Carbon::now() ]);

			$receipt_codes = DB::table('main.receipt_codes')->where('receipt_id', $modification_request->receipt_id)
			->whereIn('id', $check_months)
			->get();

			$redistribute_months = ["employer_id" => $modification_request->employer_id, ];

			foreach ($receipt_codes as $destributed)
			{
				if ($destributed->fin_code_id == 2) {
					$redistribute_months = array_merge($redistribute_months, ["contribution_month" . $destributed->id => Carbon::parse($destributed->contrib_month)
						->format('m') , "contribution_year" . $destributed->id => Carbon::parse($destributed->contrib_month)
						->format('Y') , "contribution_amount" . $destributed->id => $destributed->amount, "member_count" . $destributed->id => $destributed->member_count]);;
				} else {
					$redistribute_months = array_merge($redistribute_months, ["contribution_month" . $destributed->id => Carbon::parse($destributed->contrib_month)
						->format('m') , "contribution_year" . $destributed->id => Carbon::parse($destributed->contrib_month)
						->format('Y') , "interest_amount" . $destributed->id => $destributed->amount, "member_count" . $destributed->id => !empty($destributed->member_count)? $destributed->member_count : null]);;
				}		
			}
			$receipt_repo->updateModificationMonths($receipt,$redistribute_months);
			return true;
		}
		else
		{
			Log::info('Contrib modification ' . $modification_request->id . ' has Failed! Nothing to update');
			return false;
		}
	}


	// ==================== update request on edit
	public function updateEditedRequest($modification_request, array $input, $request)
	{
		$receipt = $modification_request->receipt;
		switch ($modification_request->request_type)
		{
			case 1:
			$return = $this->updateEditedChangeMonths($modification_request, $input, $receipt, $request);
			break;
			case 2:
			$return = $this->updateEditedAmountRedistribution($modification_request, $input, $receipt, $request);
			break;
			default:
			$return = false;
			break;
		}
		return $return;
	}


	public function updateEditedChangeMonths($modification_request, $input, $receipt, $request)
	{
		return DB::transaction(function () use ($modification_request, $input, $receipt, $request)
		{
			$request_attachment = request()->hasFile('request_attachment') ? 'request_attachment.' . request()
			->file('request_attachment')
			->getClientOriginalExtension() : null;
			$receipt_attachment = request()->hasFile('receipt_attachment') ? 'receipt_attachment.' . request()
			->file('receipt_attachment')
			->getClientOriginalExtension() : null;

			$arr_receipt = [];
			$arr_change = [];
			foreach ($input as $key => $value)
			{
				if (strpos($key, 'month_new') !== false && !empty($value))
				{
					$code_id = (int)str_replace("month_new_", "", $key);
					$arr_receipt[substr($key, 10) ] = $value;

					$rct_code = $modification_request->receipt_type == 2 ? DB::table('main.legacy_receipt_codes')
					->find($code_id) : DB::table('main.receipt_codes')->find($code_id);
					if (!empty($rct_code->contrib_month))
					{
						$old_month = Carbon::parse($rct_code->contrib_month)
						->format('Y-m-28');
						$new_month = Carbon::parse($value)->format('Y-m-28');
						if ($old_month !== $new_month)
						{
							$arr_change[$code_id] = ['new' => $new_month, 'old' => $old_month];
						}
					}
				}
			}
			
			
			if (count($arr_change))
			{
				$receipt_codes = [];
				foreach ($receipt->receiptCodes as $rct_code)
				{
					$receipt_codes[$rct_code->id] = ['id' => $rct_code->id, 'fin_code_id' => $rct_code->fin_code_id, 'contrib_month' => $rct_code->contrib_month, 'amount' => $rct_code->amount, 'employee_count' => $rct_code->member_count];
				}

				$this->query()->where('id',$modification_request->id)
				->update([
					'date_received' => Carbon::parse($input['request_date'])->format('Y-m-d'), 
					'reason' => $input['reason'],
					'request_attachment' => !empty($request_attachment) ? $request_attachment : $modification_request->request_attachment, 
					'receipt_attachment' => !empty($receipt_attachment) ? $receipt_attachment : $modification_request->receipt_attachment,
					'rctno' => $receipt->rctno, 'amount' => $receipt->amount, 'old_rct_description' => $receipt->description,
					'payroll_id' => !empty($receipt->payroll_id) ? $receipt->payroll_id : 1, 'receipt_codes' => !empty($receipt_codes) ? serialize($receipt_codes) : null,
				]);

				DB::table('main.contrib_modification_months')->where('contrib_modification_id',$modification_request->id)->update(['deleted_at'=>Carbon::now()]);

				foreach ($arr_change as $index => $change)
				{
					DB::table('main.contrib_modification_months')->updateOrInsert(['contrib_modification_id' => $modification_request->id,'receipt_code_id'=>$index],['old_month' => $change['old'], 'new_month' => $change['new'], 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),'deleted_at'=>null, 'receipt_code_type' => (int)$request->receipt_type, 'fin_code_id'=>(int)$input["payment_type_".$index]]);
				}
				$this->saveAttachment($request, $modification_request->id);
			}
			return $modification_request;
		});
	}

	public function updateEditedAmountRedistribution($modification_request, $input, $receipt, $request)
	{
		return DB::transaction(function () use ($modification_request, $input, $receipt, $request)
		{

			// dd($input);
			$new = [];
			foreach ($input as $key => $value)
			{
				if (strpos($key, 'contrib_month') !== false)
				{
					$key_id = str_replace("contrib_month_", "", $key);
					$new[$key_id] =[
						'contrib_month' => Carbon::parse($value)->format('Y-m-28'),
						'contrib_amount' => $input["contrib_amount_".$key_id],
						'member_count' => !empty($input["employee_count_".$key_id]) ? $input["employee_count_".$key_id] : 0,
						'fin_code_id' => $input["payment_type_".$key_id],
					];
				}
			}
			// dd($new);
			$receipt_codes = [];
			foreach ($receipt->receiptCodes as $rct_code)
			{
				$receipt_codes[$rct_code->id] = ['id' => $rct_code->id, 'fin_code_id' => $rct_code->fin_code_id, 'contrib_month' => $rct_code->contrib_month, 'amount' => $rct_code->amount, 'employee_count' => $rct_code->member_count];
			}
			$request_attachment = request()->hasFile('request_attachment') ? 'request_attachment.' . request()
			->file('request_attachment')
			->getClientOriginalExtension() : null;
			$receipt_attachment = request()->hasFile('receipt_attachment') ? 'receipt_attachment.' . request()
			->file('receipt_attachment')
			->getClientOriginalExtension() : null;

			$this->query()->where('id',$modification_request->id)
			->update([
				'date_received' => Carbon::parse($input['request_date'])->format('Y-m-d'), 
				'reason' => $input['reason'],
				'request_attachment' => !empty($request_attachment) ? $request_attachment : $modification_request->request_attachment, 
				'receipt_attachment' => !empty($receipt_attachment) ? $receipt_attachment : $modification_request->receipt_attachment,
				'rctno' => $receipt->rctno, 'amount' => $receipt->amount, 'old_rct_description' => $receipt->description,
				'payroll_id' => !empty($receipt->payroll_id) ? $receipt->payroll_id : 1, 'receipt_codes' => !empty($receipt_codes) ? serialize($receipt_codes) : null,
			]);

			DB::table('main.contrib_modification_redistributions')->where('contrib_modification_id',$modification_request->id)->update(['deleted_at'=>Carbon::now()]);

			foreach ($new as $index => $change)
			{
				DB::table('main.contrib_modification_redistributions')->updateOrInsert(['contrib_modification_id' => $modification_request->id, 'contrib_month' => $change['contrib_month'], 'fin_code_id' => $change['fin_code_id']], ['amount' => $change['contrib_amount'], 'member_count' => $change['member_count'], 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'deleted_at'=>null]);
			}
			$this->saveAttachment($request, $modification_request->id);
			return $modification_request;
		});
	}

	public function cancelModificationRequest(array $input)
	{
		$return = DB::transaction(function () use ($input) { 
			$this->query()->where('id',$input['modification_request_id'])->update([
				'status' => 4,
				'iscancelled' => true,
				'deleted_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'wf_done' => 1,
				'cancel_reason' => $input['cancel_reason'],
				'wf_ended' => true,
			]);

			/*Deactivate workflow*/
			$workflow = new Workflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $input['modification_request_id']]);
			$workflow->wfTracksDeactivate();
			return true;
		});

		return  $return;
	}

	public function findWithTrashed($request_id)
	{
		$result =  $this->query()->where('id',$request_id)->withTrashed()->first();
		if (count($result))
		{
			return $result;
		}
		throw new GeneralException('Request Failed! Contribution modification request not found');
	}

}

