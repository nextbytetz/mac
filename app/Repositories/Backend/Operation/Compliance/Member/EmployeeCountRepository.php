<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Member\EmployeeCount;
use App\Models\Operation\Compliance\Member\UnregisteredEmployer;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Propaganistas\LaravelPhone\PhoneNumber;

class EmployeeCountRepository extends  BaseRepository
{

    const MODEL = EmployeeCount::class;

    public function __construct()
    {

    }

//find or throwexception for employer
    public function findOrThrowException($id)
    {
        $employeeCount = $this->query()->find($id);

        if (!is_null($employeeCount)) {
            return $employeeCount;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.employee_count_not_found'));
    }




    public function create($input){
        if  ($input['count'] > 0){
            return $this->query()->create($input);
        }
    }




    public function update($input){
        $employee_count = $this->query()->where('employer_id', $input['employer_id'])->where('employee_category_cv_id', $input['employee_category_cv_id'])->where('gender_id',$input['gender_id'])->first();

        if ($employee_count){
            $employee_count->update($input);

//            Delete if not applicable
            $this->deleteIfApplicableIsZero($employee_count->id);

        }else{
            $this->create($input);
        }
    }




    public function deleteIfApplicableIsZero($id)
    {
        $employee_count = $this->findOrThrowException($id);
        if (($employee_count->count == 0) || (!$employee_count->count)){
            $employee_count->delete();
        }
    }




    /**
     * @param $id
     * @param $input
     * create Employee counts
     */

    public function createEmployeeCount($id,$input){
        DB::transaction(function () use ($id, $input) {
                     foreach ($input as $key => $value) {
                if (strpos($key, 'employee_category') !== false) {
                    $employee_category_id = substr($key, 17);
                    if ($input['employee_category' . $employee_category_id] == 1) {
//
//                       male count

    $employee_count_input = ['employer_id' => $id,
        'employee_category_cv_id' => $employee_category_id,
        'gender_id' => 1,
        'count' => $input['male' . $employee_category_id]];
    $employee_count = $this->create($employee_count_input);



//                        female count

                            $employee_count_input = ['employer_id' => $id,
                                'employee_category_cv_id' => $employee_category_id,
                                'gender_id' => 2,
                                'count' => $input['female' . $employee_category_id]];
                            $employee_count = $this->create($employee_count_input);
                                            }
                }
            }
        });
    }

    /**
     * @param $id
     * @param $input
     * Update Employee counts
     */

    public function updateEmployeeCount($id,$input){
        DB::transaction(function () use ($id, $input) {
            foreach ($input as $key => $value) {

                if (strpos($key, 'employee_category') !== false) {

                    $employee_category_id = substr($key, 17);

                    //                       male count

                    if( array_key_exists('male'.$employee_category_id, $input)) {

                        $employee_count_input = ['employer_id' => $id,
                            'employee_category_cv_id' => $employee_category_id,
                            'gender_id' => 1,
                            'count' => $input['male' . $employee_category_id]];
                        $employee_count = $this->update($employee_count_input);
                    }else{
//                        if not applicable assign = 0
                        $employee_count_input = ['employer_id' => $id,
                            'employee_category_cv_id' => $employee_category_id,
                            'gender_id' => 1,
                            'count' => 0];
                        $employee_count = $this->update($employee_count_input);
                    }

//                        female count
                    if( array_key_exists('female'.$employee_category_id, $input)) {
                        $employee_count_input = ['employer_id' => $id,
                            'employee_category_cv_id' => $employee_category_id,
                            'gender_id' => 2,
                            'count' => $input['female' . $employee_category_id]];
                        $employee_count = $this->update($employee_count_input);
                    }else{
                        //                        if not applicable assign = 0
                        $employee_count_input = ['employer_id' => $id,
                            'employee_category_cv_id' => $employee_category_id,
                            'gender_id' => 2,
                            'count' => 0];
                        $employee_count = $this->update($employee_count_input);
                    }

                }
            }
        });
    }




}