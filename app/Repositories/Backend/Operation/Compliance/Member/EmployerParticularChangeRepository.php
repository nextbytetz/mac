<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;


use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerParticularChange;
use App\Models\Operation\Compliance\Member\EmployerParticularChangeType;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\BaseRepository;
use App\Services\Compliance\MergeDeMergeEmployers;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Mail\NotifyChangeParticularOnline;
use Mail;
use Carbon\Carbon;
use App\Services\Compliance\Traits\EmployeesExcel;

class EmployerParticularChangeRepository extends BaseRepository
{
    use AttachmentHandler, FileHandler, EmployeesExcel;


    const MODEL = EmployerParticularChange::class;

    protected $employer_repo;


    public function __construct()
    {
        parent::__construct();
        $this->employer_repo = new EmployerRepository();

    }


    /*Get employer current values*/
    public function getEmployerCurrentValues($employer_id)
    {
        $employer = Employer::query()->find($employer_id);
        $values = [
            'name' => $employer->name, 'tin' => $employer->tin, 'doc' => isset($employer->doc) ? standard_date_format($employer->doc) : null, 'phone' => $employer->phone, 'telephone' => $employer->telephone, 'vote' => $employer->vote,
            'email' => $employer->email, 'box_no' => $employer->box_no, 'fax' => $employer->fax,'region_id' => $employer->region_id, 'district_id' => $employer->district_id, 'location_type_id' => $employer->location_type_id, 'unsurveyed_area' => $employer->unsurveyed_area, 'street' => $employer->street,  'road' => $employer->road, 'plot_no' => $employer->plot_no,'block_no' => $employer->block_no,   'surveyed_extra' => $employer->surveyed_extra, 'business_sector_cv_id' => ($employer->sectors()->count()) ? $employer->sectors->first()->id : null,
        ];

        return $values;
    }


    /*Store*/
    public function store(array $input)
    {
        return DB::transaction(function () use ($input) {
            $change_values = $this->getChangedValues($input['employer_id'], $input);
//            $this->checkIfThereIsNoChange($change_values);
            $employer_change = $this->query()->create([
                'employer_id' => $input['employer_id'],
                'date_received' => $input['date_received'],
                'remark' => $input['remark'],
                'user_id' => access()->id(),

            ]);

            /*Store change types*/
            $this->storeChangeTypes($employer_change, $input);

            return $employer_change;
        });
    }



    /*Update */
    public function update(Model $employer_change, array $input)
    {
        return DB::transaction(function () use ($input, $employer_change) {
//            $this->checkIfThereIsNoChange($change_values);
            $employer_change->update([
                'date_received' => $input['date_received'],
                'remark' => $input['remark'],
            ]);

            /*Store change types*/
            $this->storeChangeTypes($employer_change, $input);

            return $employer_change;
        });
    }



    /*store change types*/
    public function storeChangeTypes(Model $employer_change, array $input)
    {
        /*name change*/
        $this->storeNameChangeType($employer_change, $input);
//        /*address change*/
//        $this->storeAddressChangeType($employer_change, $input);
        /*General change*/
        $this->storeGeneralChangeType($employer_change, $input);
        /*registration authority*/
        $this->storeRegistrationAuthorityChange($employer_change, $input);
    }


    /*Store name change*/
    public function storeNameChangeType(Model $employer_change,  array $input)
    {
        $change_type_cv_id = (new CodeValueRepository())->findByReference('EMTCNAME')->id;
        $type = $this->findParticularChangeType($employer_change, $change_type_cv_id);

        if(isset($input['name_change_type_cv_id'])){
            $name_change_type_cv_id = $input['name_change_type_cv_id'];

            if($name_change_type_cv_id == 'EMNCTME' ){
                /*merger*/
                $input['new_values'] =[ $input['merge_employer_id']];
            }elseif($name_change_type_cv_id == 'EMNCTNA' ){
                /*name change*/
                $input['old_values'] =[ $employer_change->employer->name ];
                $input['new_values'] =[ $input['new_name']];
            }elseif($name_change_type_cv_id == 'EMNCTSPL' ) {
                /*merger*/
                $input['new_values'] = [$input['split_employer_id']];
            }

            /*if isset store*/
            if(isset($type)){
                $this->updateChangeType($type, $input);
            }else{
                $this->storeChangeType($employer_change->id, $change_type_cv_id, $input);
            }
        }else{
            /*if change restored- delete entry*/
            if(isset($type))
            {
                $type->delete();
            }
        }

    }


    /*Store General change*/
    public function storeGeneralChangeType(Model $employer_change,  array $input)
    {
        $change_type_cv_id = (new CodeValueRepository())->findByReference('EMTCGEN')->id;
        $change_values = $this->getChangedValues($input['employer_id'], $input);
        $input['old_values'] = $change_values['old_values'];
        $input['new_values'] = $change_values['new_values'];
        $type = $this->findParticularChangeType($employer_change, $change_type_cv_id);

        if(sizeof($change_values['new_values']) > 0){
            if(isset($type)){
                $this->updateChangeType($type, $input);
                if(!isset( $input['new_values'])){
                    /*if changes restored - delete entry*/
                    $type->delete();
                }
            }else{
                $this->storeChangeType($employer_change->id, $change_type_cv_id, $input);
            }
        }

        /*update general change flags*/
        $this->updateChangeFlags($employer_change, $change_values['new_values']);

    }

    /*Update change flags*/
    public function updateChangeFlags($employer_change, array $changed_values)
    {
        $is_tin_changed = 0;
        $is_doc_changed  = 0;
        /*Tin*/
        if(array_key_exists('tin', $changed_values)){
            $is_tin_changed = 1;
        }
        /*Doc*/
        if(array_key_exists('doc', $changed_values)){
            $is_doc_changed = 1;
        }

        /*Update employer changes flag*/
        $employer_change->update([
            'is_tin_changed' => $is_tin_changed,
            'is_doc_changed' => $is_doc_changed,
        ]);
    }


    /*Get all new values by employer change*/
    public function getAllNewValues($employer_change)
    {
        $changed_values = [];
        foreach ($employer_change->changeTypes as $changeType) {
            $new_values = unserialize($changeType->new_values);
            $changed_values = array_merge($new_values, $changed_values);
        }
        return $changed_values;

    }

    /*Store Address change  i.e. moved to general changes*/
    public function storeAddressChangeType(Model $employer_change,  array $input)
    {
        $change_type_cv_id = (new CodeValueRepository())->findByReference('EMTCADDR')->id;
        $change_values = $this->getChangedValues($input['employer_id'], $input);
        $input['old_values'] = $change_values['old_values'];
        $input['new_values'] = $change_values['new_values'];
        $type = $this->findParticularChangeType($employer_change, $change_type_cv_id);

        if(sizeof($change_values['new_values']) > 0){
            if(isset($type)){
                $this->updateChangeType($type, $input);
                if(!isset( $input['new_values'])){
                    /*if changes restored - delete entry*/
                    $type->delete();
                }
            }else{
                $this->storeChangeType($employer_change->id, $change_type_cv_id, $input);
            }
        }


    }

    /*Store registration authority change*/
    public function storeRegistrationAuthorityChange(Model $employer_change,  array $input)
    {
        $change_type_cv_id = (new CodeValueRepository())->findByReference('EMTCREGA')->id;
        $input['new_values'] = [ $input['registration_authority_cv_id']];
        $type = $this->findParticularChangeType($employer_change, $change_type_cv_id);

        if(isset( $input['registration_authority_cv_id'])){
            /*if isset store*/
            if(isset($type)){
                $this->updateChangeType($type, $input);
            }else{
                $this->storeChangeType($employer_change->id, $change_type_cv_id, $input);
            }
        }else{
            /*if change restored- delete entry*/
            if(isset($type))
            {
                $type->delete();
            }
        }

    }


    /*Store change type*/
    public function storeChangeType($employer_change_id, $change_type_cv_id, array $input)
    {
        $change_type_cv = (new CodeValueRepository())->find($change_type_cv_id);
        $change_type_ref = $change_type_cv->reference;
        $name_change_type = (new CodeValueRepository())->findByReference($input['name_change_type_cv_id']);
        EmployerParticularChangeType::query()->create([
            'employer_particular_change_id' => $employer_change_id,
            'name_change_type_cv_id' => (isset($name_change_type)  && $change_type_ref == 'EMTCNAME') ? $name_change_type->id : null,
            'particular_change_type_cv_id' => $change_type_cv_id,
            'old_values' => isset($input['old_values']) ? serialize($input['old_values']) : null,
            'new_values' => isset($input['new_values']) ? serialize($input['new_values']) : null,
            'description' => $input['description'] ?? null,
        ]);
    }

    /*Update change type*/
    public function updateChangeType($change_type, array $input)
    {
        $change_type_cv = (new CodeValueRepository())->find($change_type->particular_change_type_cv_id);
        $change_type_ref = $change_type_cv->reference;
        $name_change_type = (new CodeValueRepository())->findByReference($input['name_change_type_cv_id']);
        $change_type->update([
            'name_change_type_cv_id' => (isset($name_change_type)  && $change_type_ref == 'EMTCNAME') ? $name_change_type->id : null,
            'old_values' => isset($input['old_values']) ? serialize($input['old_values']) : null,
            'new_values' => isset($input['new_values']) ? serialize($input['new_values']) : null,
            'description' => isset($input['description']) ? ($input['description']) : null,
        ]);
    }


    /*Find particular change type*/
    public function findParticularChangeType(Model $employer_change, $change_type_cv_id)
    {
        $type = $employer_change->changeTypes()->where('particular_change_type_cv_id', $change_type_cv_id)->first();
        return $type;
    }

    /*Get changed values*/
    public function getChangedValues($employer_id, array $input){
        $current_values = $this->getEmployerCurrentValues($employer_id);
        $new_values = [];
        $old_values = [];
        foreach ($input as $key => $value) {
            if (array_key_exists($key, $current_values)) {
                if ($input[$key] <> $current_values[$key]) {
                    /*If changes*/
                    $new_values[$key] = $input[$key];
                    $old_values[$key] = $current_values[$key];
                }
            }
        }
        return ['old_values' => $old_values, 'new_values' => $new_values];
    }


    /*Check if there is no change*/
    public function checkIfThereIsNoChange($change_values){
        $new_values = $change_values['new_values'];
        if(sizeof($new_values) == 0){
            throw new GeneralException('No value has been updated! Please check!');
        }
    }




    /*Undo*/
    public function undo(Model $employer_change){
        DB::transaction(function () use ( $employer_change) {
            $wf_modules = new WfModuleRepository();
            $wf_module_group_id = 23;
            $type = $this->getWfModuleType($employer_change);

            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $employer_change->id, 'type' => $type]);
            /*delete employer change*/
            $this->deleteChangeTypes($employer_change);

            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();

            $employer_change->delete();

        });
    }

    /*Delete change types*/
    public function deleteChangeTypes(Model $employer_change)
    {
        EmployerParticularChangeType::query()->where('employer_particular_change_id', $employer_change->id)->delete();
    }

    /*check if can initiate*/
    public function checkIfCanInitiateApproval($employer_change_id)
    {
        /*check if doc are submitted -- Mandatory*/
        $doc = $this->getPendingDocuments($employer_change_id);
        if(sizeof($doc) > 0)
        {
            throw new WorkflowException('Please attach all required document to proceed!');
        }
    }

    /*Get unserialized new values by change type*/
    public function getNewValuesByChangeType(Model $employer_change, $change_type_cv_ref)
    {
        $return = null;
        $change_type_cv = (new CodeValueRepository())->findByReference($change_type_cv_ref);
        $change_type = $employer_change->changeTypes()->where('particular_change_type_cv_id', $change_type_cv->id)->first();
        if(isset($change_type)){
            /*Change type*/
            $new_values = (isset($change_type->new_values)) ? unserialize($change_type->new_values) : null;
            $return  = $new_values;
        }
        return $return;
    }

    /**
     * @param $id
     * Action when workflow is completed
     */
    public function updateOnWfDoneComplete($id)
    {
        $cv_repo = new CodeValueRepository();
        $employer_change = $this->find($id);

        /*update for Name changing*/
        $change_type_cv = $cv_repo->findByReference('EMTCNAME');
        $name_change_type = $this->findParticularChangeType($employer_change, $change_type_cv->id);
        if(isset($name_change_type)){

            $this->updateNameChangeOnWfDone($id);
        }

        /*Update for general changing*/
        $change_type_cv = $cv_repo->findByReference('EMTCGEN');
        $general_change_type = $this->findParticularChangeType($employer_change, $change_type_cv->id);
        if(isset($general_change_type)){
            $this->updateGeneralChangeOnWfDone($id);
        }

        /*Update registration change*/
        $change_type_cv = $cv_repo->findByReference('EMTCREGA');
        $registration_change_type = $this->findParticularChangeType($employer_change, $change_type_cv->id);
        if(isset($registration_change_type)){

        }

        /*For portal request ONLY*/
        if($employer_change->source == 2){
            $this->updatePortalOnWfComplete($id);
        }

    }

    /*Update On change of particulars on wf complete*/
    public function updatePortalOnWfComplete($id){
        $employer_change = $this->find($id);
        $wf_done = $employer_change->wf_done;
        $status = ($wf_done == 1) ? 2 : 3;

        $this->updateStatusOnPortal($employer_change, $status);
        $this->sendChangeParticularEmail($employer_change->portal_particular_change_id,$status);
        /*Update flag on the table*/
        $new_values = $this->getAllNewValues($employer_change);
        $this->updateChangeFlags($employer_change, $new_values);

    }




    public function sendChangeParticularEmail($online_change_id,$status)
    {
        $online_change = DB::table('portal.change_of_particulars')
        ->select(['users.email','users.name as user_name','employers.name as employer_name','change_of_particulars.*'])
        ->join("portal.users", "users.id", "=", "change_of_particulars.user_id")
        ->join("main.employers", "employers.id", "=", "change_of_particulars.employer_id")
        ->where('change_of_particulars.id',$online_change_id)->first();

        $description = trim(substr($online_change->description, strpos($online_change->description, ':')+1));
        $description = $this->returnDescriptionForEmail($description);

        switch ($status) {
            case 2:
            $data = [
                'employer_id' => $online_change->employer_id,
                'to_email' => $online_change->email,
                'name'  => $online_change->user_name,
                'employer_name'  => $online_change->employer_name,
                'message' => 'This is to inform your request to change '.$description.' for '.$online_change->employer_name.' has been approved and changes have been effected as requested.',
            ];
            break;
            default:
                //rejection
            $data = [
                'employer_id' => $online_change->employer_id,
                'to_email' => $online_change->email,
                'name'  => $online_change->user_name,
                'employer_name'  => $online_change->employer_name,
                'message' => 'This is to inform your request to change '.$description.' for '.$online_change->employer_name.' has been rejected. <br> <br> Reason for rejection : <blockquote>'.$online_change->reject_reason.'</blockquote>'
            ];
            break;
        }
        if(env('TESTING_MODE') == 0){
            /*Send only on live*/
            Mail::to($data['to_email'])->send(new NotifyChangeParticularOnline($data));
        }


    }




    /*Update Name change on Wf Done*/
    public function updateNameChangeOnWfDone($id)
    {
        $cv_repo = new CodeValueRepository();
        $employer_change = $this->find($id);
        $employer = $employer_change->employer;
        $change_type_cv = $cv_repo->findByReference('EMTCNAME');
        $change_type = $this->findParticularChangeType($employer_change, $change_type_cv->id);
        $name_change_type_cv = $cv_repo->find($change_type->name_change_type_cv_id);
        $new_values = unserialize($change_type->new_values);
        $merge_demerge_serv = new MergeDeMergeEmployers();
        $user_id = access()->id();
        /*name change type*/
        switch($name_change_type_cv->reference){
            case 'EMNCTME':
            /*Merger*/
            $replaced_by = $employer_change->employer_id;
            $old_regno = $new_values[0];
            $merge_demerge_serv->mergeEmployer($old_regno, $replaced_by, $user_id);
            break;

            case 'EMNCTSPL':
            /*splitting*/
            $replaced_by = $employer_change->employer_id;
            $old_regno = $new_values[0];
            $merge_demerge_serv->demergeEmployers($old_regno, $replaced_by);
            break;

            case 'EMNCTNA':
            /*Name change*/
            foreach($new_values as $new_value){
                $employer->update([
                    'name' => $new_value
                ]);
            }

            break;

        }
    }


    /*Update general change wf done*/
    public function updateGeneralChangeOnWfDone($id)
    {
        $cv_repo = new CodeValueRepository();
        $employer_change = $this->find($id);
        $employer = $employer_change->employer;
        $change_type_cv = $cv_repo->findByReference('EMTCGEN');

        $change_type = $this->findParticularChangeType($employer_change, $change_type_cv->id);
        $new_values = unserialize($change_type->new_values);
        foreach($new_values as $key => $value) {
            /*from portal do not have _id*/
            if(in_array($key, ['district', 'region'])){
                $key = $key . '_id';
            }

            if ($key == 'doc') {
             if (!empty($employer_change->payroll_id)) {
                $least_doc = $this->getLeastDocForParticularChange($employer->id,$employer_change->payroll_id);
                $update = ($least_doc <= $value) ? $least_doc : $value;
                if ($value <= $least_doc) {
                    $this->checkEmployerReceiptVsDoc($employer->id, $value);
                }else{
                    $this->checkEmployerReceiptVsDoc($employer->id, $value, $employer_change->payroll_id);
                }
                $employer->update([$key=> $update]);
                $this->updateDateOfCommencementPerPayroll($employer_change, $value);
            }else{
             $this->checkEmployerReceiptVsDoc($employer->id, $value);
             $employer->update([$key=> $value]);}
         }else{
            $employer->update([$key=> $value]);}
        }
    }

    /**
     * @param Model $employer_change
     * @param $status
     * Start wf for Portal
     */

    /*update*/
    public function updateStatusOnPortal(Model $employer_change, $status)
    {
        DB::table('portal.change_of_particulars')->where('id',$employer_change->portal_particular_change_id)->update([
            'approval_status' => $status
        ]);
    }

    /*Update on portal general*/
    public function updateOnPortalGeneral(Model $employer_change,array $data)
    {
        DB::table('portal.change_of_particulars')->where('id',$employer_change->portal_particular_change_id)->update($data);
    }


    /*reject to employer level*/
    public function wfRejectToLevel1Portal($current_wf_track)
    {
        $employer_change = $this->find($current_wf_track->resource_id);
        $reject_reason = $current_wf_track->comments;
        $data = [
            'approval_status' => 3,
            'reject_reason' => $reject_reason
        ];
        $this->updateOnPortalGeneral($employer_change, $data);
        $this->sendChangeParticularEmail($employer_change->portal_particular_change_id,3);
        /*Send Notification*/
    }

    /*End: wf for portal*/

    /**
     * @param $employer_particular_change
     * Get doc attached for this particular change
     */
    public function getDocsAttached($employer_particular_change_id)
    {
        $employer_change = $this->find($employer_particular_change_id);
        $employer = $employer_change->employer;
        $docs_all = $this->getDocTypesIdsForParticularChange();
        $docs_attached = $employer->documents()->where('external_id', $employer_particular_change_id)->whereIn('document_id',$docs_all)->get();
        return $docs_attached;
    }


    /*Get documents attached by doc type id*/
    public function getQueryDocsAttachedByDocType($employer_particular_change_id, $document_id)
    {
        $employer_change = $this->find($employer_particular_change_id);
        $employer = $employer_change->employer;
        $docs_attached = $employer->documents()->where('external_id', $employer_particular_change_id)->where('document_id',$document_id);
        return $docs_attached;
    }

    /*Pending documents*/
    public function getPendingDocuments($employer_particular_change_id)
    {
        $document_repo = new DocumentRepository();
        $cv_repo = new CodeValueRepository();
        $employer_change = $this->find($employer_particular_change_id);
        $pending_docs = [];
        /*Check if COM is attached*/
//        $document_id = 85;
//        $check = $this->getQueryDocsAttachedByDocType($employer_particular_change_id, $document_id)->count();
//        if($check == 0){
//            $pending_docs[] = $document_repo->find($document_id)->name;
//        }

        /*Check other documents*/
        $document_id = 86;
        $needed_other_docs = 0;
        /*Name change / General change*/
        $name_change_cv_id = $cv_repo->findByReference('EMTCNAME')->id;
        $general_change_cv_id = $cv_repo->findByReference('EMTCGEN')->id;
        $check_other_doc_count = $employer_change->changeTypes()->whereIn('particular_change_type_cv_id', [$name_change_cv_id, $general_change_cv_id])->count();//check name change
        $needed_other_docs = ($check_other_doc_count > 0) ? ($needed_other_docs + 1) : $needed_other_docs;
        /*Registration change*/
        $reg_change_cv_id = $cv_repo->findByReference('EMTCREGA')->id;
        $check_other_doc_count = $employer_change->changeTypes()->where('particular_change_type_cv_id', $reg_change_cv_id)->count();//check reg change
        $needed_other_docs = ($check_other_doc_count > 0) ? ($needed_other_docs + 1) : $needed_other_docs;

//        /*General change*/
//        $check_other_doc_count = $employer_change->changeTypes()->where('particular_change_type_cv_id', 447)->count();//check general change
//        $needed_other_docs = ($check_other_doc_count > 0) ? ($needed_other_docs + 1) : $needed_other_docs;

        $attached_other_docs = $this->getQueryDocsAttachedByDocType($employer_particular_change_id, $document_id)->count();
        $needed_other_docs = $needed_other_docs - $attached_other_docs;
        /*check if there still pending other doc not attached*/
        if($needed_other_docs > 0){
            $pending_docs[] = $document_repo->find($document_id)->name . ' - (' . $needed_other_docs . ')';
        }

        return $pending_docs;
    }


    /**
     * @param array $input
     * @param $employer_particular_change_id
     * Save documents
     */
    public function saveDocuments(array $input, $employer_particular_change_id)
    {
        return DB::transaction(function () use ($employer_particular_change_id, $input) {
            $document_id = $input['document_id'];
            $employer_particular_change = $this->find($employer_particular_change_id);
            $employer = $employer_particular_change->employer;
            $check_if_recurring = (new DocumentRepository())->checkIfDocumentIsRecurring($document_id);
            $ext = $this->getDocExtension('document_file');

            if($check_if_recurring == false)
            {
                /*for non recurring document*/
                $check_if_exist = $this->checkIfDocumentExistForChangeRequest($employer_particular_change_id, $document_id);
                if($check_if_exist == false){
                    /*if not exists - Attach new*/
                    $this->savePivotDocument($employer_particular_change_id, $input);
                }else{
                    $uploaded_doc = $employer->documents()->where('external_id', $employer_particular_change_id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
                    $this->updatePivotDocument($employer_particular_change_id, $input,$uploaded_doc->pivot->id );
                }
                /*Attach to storage*/
                $this->attachDocFileToStorage($employer_particular_change_id, $document_id, $ext);
            }else{
                /*for recurring documents*/
                $check_if_exist = isset($input['doc_pivot_id']) ? true : false;

                if($check_if_exist){
                    /*Exist - update*/
                    $this->updatePivotDocument($employer_particular_change_id, $input, $input['doc_pivot_id']);
                }else {
                    /*Does not exist - Add new*/
                    $this->savePivotDocument($employer_particular_change_id, $input);
                }
                /*Attach to storage*/
                $this->attachDocFileToStorage($employer_particular_change_id, $document_id, $ext);
            }
        });
    }

    /*Save pivot document*/
    public function savePivotDocument($employer_particular_change_id, array $input)
    {
        $document_id = $input['document_id'];
        $employer_particular_change = $this->find($employer_particular_change_id);
        $employer = $employer_particular_change->employer;
        $file = request()->file('document_file');
        if($file->isValid()) {
            $employer->documents()->attach([$document_id => [
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'external_id' => $employer_particular_change_id
            ]]);
        }
    }


    /*Update pivot table of document*/
    public function updatePivotDocument($external_id, array $input, $doc_pivot_id)
    {
//        $closure = $this->find($closure_id);
        $file = request()->file('document_file');
        if($file->isValid()) {
            DB::table('document_employer')->where('id', $doc_pivot_id)->update([
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'external_id' => $external_id
            ]);
        }
    }


    /*Attach doc to server storage*/
    public function attachDocFileToStorage($employer_particular_change_id, $document_id, $ext)
    {
        /*Attach document to server*/
        $employer_particular_change = $this->find($employer_particular_change_id);
        $employer = $employer_particular_change->employer;
        $uploaded_doc = $employer->documents()->where('external_id', $employer_particular_change_id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
        $path = employer_particular_change_dir() . DIRECTORY_SEPARATOR . $employer->id . DIRECTORY_SEPARATOR . $employer_particular_change_id . DIRECTORY_SEPARATOR;
        $filename = $uploaded_doc->pivot->id . '.' . $ext;
        $this->makeDirectory($path);
        $this->saveDocumentBasic('document_file',$filename, $path );
    }


    /*Delete document*/
    public function deleteDocument($doc_pivot_id)
    {
        $uploaded_doc =DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $path = employer_particular_change_dir() . DIRECTORY_SEPARATOR . $uploaded_doc->employer_id . DIRECTORY_SEPARATOR . $uploaded_doc->external_id . DIRECTORY_SEPARATOR;
        $file_path = $path . $doc_pivot_id . '.' . $uploaded_doc->ext;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        /*delete*/
        DB::table('document_employer')->where('id', $doc_pivot_id)->delete();
    }


    /* Check if document exists for this change request */
    public function checkIfDocumentExistForChangeRequest($external_id, $document_id)
    {
        $employer_change = $this->find($external_id);
        $employer = $employer_change->employer;
        $check = $employer->documents()->where('external_id', $external_id)->where('document_id', $document_id)->count();
        if($check > 0)
        {
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param int $closure_type
     * @return array
     * Get mandatory docs ids by closure type
     */
    public function getDocTypesIdsForParticularChange()
    {
        return [86];
    }


    /**
     * @param $input
     * Check if can initiate new entry
     */
    public function checkIfCanInitiateNewEntry($employer_id)
    {
        $pending_count = $this->query()->where('employer_id',
            $employer_id)->where('wf_done', 0)->count();
        if($pending_count > 0){
            throw new GeneralException('There is pending approval for this employer! Complete all pending to proceed with new entry');
        }
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * Get changes by employer for datatable
     */
    public function getByEmployerForDataTable($employer_id)
    {
        return $this->query()->where('employer_id', $employer_id);
    }




    /*Module type */
    public function getWfModuleType(Model $particular_change = null)
    {
        $source = ($particular_change) ? $particular_change->source : 1;
        if($source == 1){
            /*internal*/
            $type = 1;
        }elseif($source == 2){
            /*portal*/
            $type = 2;
        }

        return $type;
    }


    /*Check if change type exists*/
    public function checkIfNameChangeTypeExists($particular_change_id, $name_change_type_cv_ref)
    {

        $particular_change = $this->find($particular_change_id);
        $cv_repo = new CodeValueRepository();
        $change_type_for_name = $cv_repo->findByReference('EMTCNAME');
        $name_change_type =$cv_repo->findByReference($name_change_type_cv_ref);
        $check = $particular_change->changeTypes()->where('particular_change_type_cv_id', $change_type_for_name->id)->where('name_change_type_cv_id', $name_change_type->id)->count();
        if($check > 0)
        {
            return true;
        }else{
            return false;
        }
    }


    public function getChangeType($particular_change_id, $change_type_cv_ref, $name_change_type_cv_ref = null)
    {
        $particular_change = $this->find($particular_change_id);
        $cv_repo = new CodeValueRepository();
        $change_type_cv = $cv_repo->findByReference($change_type_cv_ref);
        $name_change_type =$cv_repo->findByReference($name_change_type_cv_ref);
        $query = $particular_change->changeTypes()->where('particular_change_type_cv_id', $change_type_cv->id);

        if(isset($name_change_type_cv_ref)){
            $query = $query->where('name_change_type_cv_id', $name_change_type->id);
        }

        return $query;

    }


    /**
     * @param $employer_id
     * @param $new_doc_date
     * Check new doc changed if has contribution on previous month
     */
    public function checkWhenDocChangedNoContributionBeforeNewDate($employer_id, $new_doc_date)
    {
        $check = DB::table('employer_contributions')->where('employer_id', $employer_id)->whereDate('contrib_month', '<=', standard_date_format($new_doc_date))->count();
        if($check > 0 && isset($new_doc_date)){
            return false;
        }else{
            return true;
        }
    }

    public function returnDescriptionForEmail($description)
    {
        $description =  str_replace('location_type_id', 'location type', $description);
        $description =  str_replace('employer_category_cv_id', 'employer category', $description);
        $description =  str_replace('doc', 'date of recruiting first employee', $description);
        $description = ucwords(strtolower(str_replace('_', ' ', $description)));

        return preg_replace("((.*),)", "$1 and", $description);
    }


    public function updateDateOfCommencementPerPayroll($employer_change,$new_doc)
    {
        DB::table('portal.employer_user')->where('employer_id',$employer_change->employer_id)->where('payroll_id',$employer_change->payroll_id)->update([
            'doc' => Carbon::parse($new_doc)->format('Y-m-d')
        ]);

        $this->saveArrearCommencement([
            'reg_no' => $employer_change->employer_id,
            'doc' => $new_doc,
            'payroll_id' => $employer_change->payroll_id,
        ]);

        DB::table('portal.contribution_arrears')
        ->where('employer_id', $employer_change->employer_id)
        ->where('status',0)->where('payroll_id',$employer_change->payroll_id)
        ->where('contrib_month','<',(Carbon::parse(Carbon::parse($new_doc))->format('Y-m-28')))
        ->update([
            'status' => 1,
            'mac_updated_by' => access()->user()->id,
            'updated_at' => Carbon::now(),
            'mac_updated_at' => Carbon::now(),
            'mac_clear_reason' => 'Change Particular Approved : Date of Commencement'
        ]);
    }

}