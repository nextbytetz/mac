<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;

use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Member\EmployeeTemp;
use App\Repositories\BaseRepository;

class EmployeeTempRepository extends BaseRepository
{

    const MODEL = EmployeeTemp::class;


    //find or throwException for employee
    public function findOrThrowException($id)
    {
        $employee = $this->query()->find($id);

        if (!is_null($employee)) {
            return $employee;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.employee_not_found'));
    }



    public function destroy($id)
    {
        $employeeTemp = $this->findOrThrowException($id);
        $employeeTemp->delete();
    }





}