<?php

namespace App\Repositories\Backend\Operation\Compliance\Member\Online;

use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\Compliance\Member\Online\EmployerPayrollMergeReceipt;
use App\Models\Operation\Compliance\Member\Online\EmployerPayrollMerge;
use App\Repositories\BaseRepository;
use App\Services\Compliance\Traits\EmployeesExcel;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Exceptions\GeneralException;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Services\Workflow\Workflow;
use App\Models\Workflow\WfTrack;
use App\Events\NewWorkflow;
use App\Exceptions\WorkflowException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MergeDeMergeEmployerPayrollRepository extends BaseRepository
{
	use  FileHandler, AttachmentHandler, EmployeesExcel;

	const MODEL = EmployerPayrollMerge::class;

	public function __construct()
	{
		parent::__construct();
		$this->wf_module_group_id = 35;

	}

	public function findOrThrowException($merge_id)
	{
		$merge_request = $this->query()->find($merge_id);
		if (!is_null($merge_request))
		{
			return $merge_request;
		}
		throw new GeneralException('Request Failed! Merge request not existing');
	}

	public function getEmployerRequestsForDatatable($employer_id)
	{
		return $this->query()->where('employer_id',$employer_id)->orderBy('created_at','desc');
	}

	public function saveNewRequest($request)
	{
		// dump($request->all());
		$user_id = access()->id();
		$old_values = [];
		$new_values = [];
		$old_users = [];
		$new_user = [];
		$employer_id = $request->employer_id;

		$keep_user = DB::table('portal.employer_user')
		->where('user_id', $request->merge_type == 1 ? $request->new_payroll : $request->new_payroll_user);
		$keep_user = $request->merge_type == 1 ? $keep_user->where('employer_id',$employer_id)->first() : $keep_user->first();

		$merge_user = DB::table('portal.employer_user')->select('payroll_id')
		->whereIn('user_id', $request->old_payroll)
		->where('employer_id',$employer_id)
		->distinct('payroll_id')->get();

		$all_request_payrolls = [];
		if ($request->merge_type == 1) {
			foreach ($merge_user as  $key => $merge) {
				if ($merge->payroll_id != $keep_user->payroll_id) {
					$old_values[$key] = $merge->payroll_id;
					$new_values[$key] = $keep_user->payroll_id;
				}
			}

			$contribution_check = $this->checkHasSameContribution($keep_user->payroll_id,$old_values, $employer_id);
			if(count($contribution_check) > 0) return (['errors' => ['exceptions_found' => $contribution_check, 'keep_payroll'=>$keep_user->payroll_id]]);
			
		}else{
			$combined_values = $this->combinedPayrolls($merge_user, $old_values, $request);
			if(count($combined_values['arrear_payrolls']) > 0) return (['arrear_payrolls' => 'Request Failed! Payroll '.implode(', ' , $combined_values['arrear_payrolls']).' has arrears!']);
			$old_values = $combined_values['old_values'];
			$new_values[0] = $combined_values['new_value'];
		}
		
		foreach ($request->old_payroll as $key => $merge_user) {
			$old_users[$key] = $merge_user;
			$new_user[$key] = $request->merge_type == 1 ? $request->new_payroll : $request->new_payroll_user;
		}

		if (count($old_values) > 0) { 
			$return = DB::transaction(function () use ($new_values, $old_values, $user_id, $employer_id, $request,$old_users,$new_user, $keep_user) {

				$new_user_combine = [ 0 => $new_user[0]];

				$payroll_for_request = array_unique(array_merge($old_values,$new_values));
				$payroll_merge = $this->query()->updateOrCreate([
					'employer_id' => $employer_id,
					'old_values' => serialize($old_values),
					'new_values' => serialize($new_values),
					'merge_type' => (int)$request->merge_type,
				],[
					'user_id' => $user_id,
					'old_users' => serialize($old_users),
					'new_user' => serialize($request->merge_type == 1 ? $new_user : $new_user_combine),
					'merge_reason' => $request->merge_reason,
					'letter_date' => Carbon::parse($request->letter_date)->format('Y-m-d'),
					'doc' => Carbon::parse($request->doc)->format('Y-m-d'),
					'letter_reference' => $request->letter_reference,
					'status' => 1,
				]);

				$this->saveMergeEmployerPayrollFileAttachment($payroll_merge->id);
				
				$payroll_details = $this->returnPayrollDetails($payroll_for_request,$employer_id);
				
				$this->saveMergePayrollDetails($payroll_details, $payroll_merge->id);
				
				$this->query()->where('id',$payroll_merge->id)->update(['attachment' => $payroll_merge->id.'.'.request()->file('document_file50')->getClientOriginalExtension()]);
				
				DB::table('main.employer_payroll_merge_details')
				->where('payroll_merge_id',$payroll_merge->id)
				->where('payroll_id',$request->merge_type == 1 ? $keep_user->payroll_id : $new_values[0])
				->update($this->returnAfterMergingDetails($payroll_merge->id, $employer_id));


				$check = workflow([['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $payroll_merge->id, 'type' => 0]])->checkIfHasWorkflow();
				if (!$check)
				{
					event(new NewWorkflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $payroll_merge->id, 'type' => 0],[],["comments"=>$payroll_merge->merge_reason]));
				}

				return $payroll_merge;
			});
		}
		return $return;
	}

	

	public function updateOnWfApproval($resource_id, $current_level, $next_level, $status = null)
	{
		$current_level = (int)$current_level;
		$next_level = (int)$next_level;

		if (($current_level < $next_level) || ($current_level == 5 && $next_level == 0))
		{
			//forward

			if ($current_level == 1)
			{
				// level 1 kwenda mbele 2
				$this->query()->where('id', $resource_id)->update(['status' => 1]);
			}

			if ($current_level == 4)
			{
				//level 4 (DO approve or decline)
                //status == 1 decline (wf_status = 3 )       ||   status == 0 approve (wf_status = 2)
				$wf_status = ($status == 1) ? 3 : 2; 				
				if ($wf_status == 2)
				{
					//approved
					$this->mergeEmployerOnlinePayroll($resource_id);
				}else{
					//declined
					$this->query()->where('id',$resource_id)->update([
						'status' => 3,
						'wf_done' => 1,
						'wf_done_date' => Carbon::now()
					]);
				}
			}

			if ($current_level == 5)
			{
				//level 5 check barua
				$merge_request = $this->findOrThrowException($resource_id);
				if($merge_request->letters[0]->isinitiated != 1)
				{
					throw new WorkflowException('Response letter not initiated! Kindly Initiate to proceed');
				}
			}
		}else{
			//reverse
			if ($next_level == 1)
			{
				$this->query()
				->where('id', $resource_id)->update(['status' => 3, 'wf_done' => 0, 'wf_done_date' => null]);
			}
		}

	}

	public function mergeEmployerOnlinePayroll($merge_request_id)
	{
		$user_id = access()->id();
		$merge_request = $this->findOrThrowException($merge_request_id);

		$merge_details = DB::table('main.employer_payroll_merge_details')
		->where('payroll_merge_id', $merge_request->id)->get();

		$merging_payrolls = [];
		foreach ($merge_details as $merge_detail) {
			array_push($merging_payrolls, $merge_detail->payroll_id);
		}
		$merging_payrolls = array_unique($merging_payrolls);

		$return = DB::transaction(function () use ($merge_request,$merging_payrolls) { 
			$new_values = unserialize($merge_request->new_values);
			// dd($new_values[0]);
			foreach (unserialize($merge_request->old_values) as $key => $value) {
				$this->query()->where('id',$merge_request->id)->update([
					'status' => 2,
					'wf_done' => 1,
					'wf_done_date' => Carbon::now()
				]);

				if ($merge_request->merge_type == 2) {
					//create new payroll
					$parent_id = $this->createUpdateNewPayroll($merge_request);
				}else{
					// merge receipts
					DB::table('main.receipts')
					->where('payroll_id', $value)
					->where('employer_id', $merge_request->employer_id)
					->update([
						'payroll_id' => $new_values[0],
						'is_payroll_merged'=> 1,
						'updated_at' => Carbon::now()
					]);
				}

				//deactivate employer users 
				DB::table('portal.employer_user')
				->where('payroll_id', $value)
				->where('employer_id', $merge_request->employer_id)
				->where('ismanage',true)
				->update([
					'ismanage' => 0, 
					'deactivate_reason' => $merge_request->merge_type == 1 ? 'Payroll merged' : 'Payroll combined',
					'parent_id' => $merge_request->merge_type == 1 ? null : $parent_id,
					'mac_deactivated_by' => $merge_request->user_id,
					'is_payroll_merged'=> 1,
					'mac_deactivated_at' => Carbon::now()     
				]);

				// update details
				$update_details = $this->returnPayrollDetails($merging_payrolls,$merge_request->employer_id);
				$this->saveMergePayrollDetails($update_details, $merge_request->id);
				DB::table('main.employer_payroll_merge_details')
				->where('payroll_merge_id',$merge_request->id)
				->where('payroll_id',$new_values[0])
				->update($this->returnAfterMergingDetails($merge_request->id, $merge_request->employer_id));

				//clear arrears
				$this->clearArrearClosedPayroll($merge_request->employer_id, $value);
			}

			$this->mergeEmployees($merging_payrolls,$merge_request->employer_id,$new_values[0]);

			$this->addPayrollArrears($merge_request);
			$this->clearArrearsPaid($merge_request->employer_id, $new_values[0]);
			return true;
		});

		if ($return) {
			return $return;
		} else {
			throw new WorkflowException('Action Failed! Please try again');
		}
	}

	public function clearArrearsPaid($employer_id, $payroll_id)
	{
		$arrears = DB::table('portal.contribution_arrears')->where('employer_id', $employer_id)->where('status',0)->where('payroll_id',$payroll_id)->get();
		foreach ($arrears as $clear_arrear) {
			$legacy_exist = $this->isContributionMonthInLegacy($clear_arrear->employer_id, Carbon::parse($clear_arrear->contrib_month)->format('Y-m-28'));
			$receipt_exist = $this->isContributionMonthInReceipt($clear_arrear->employer_id, Carbon::parse($clear_arrear->contrib_month)->format('Y-m-28'), $clear_arrear->payroll_id);
			$legacy_code_exist = $this->isContributionMonthInLegacyCodes($clear_arrear->employer_id, Carbon::parse($clear_arrear->contrib_month)->format('Y-m-28'));

			if ($legacy_exist || $receipt_exist || $legacy_code_exist) {
				DB::table('portal.contribution_arrears')->where('id', $clear_arrear->id)->update([
					'status' => 1,
					'mac_updated_by' => access()->user()->id,
					'updated_at' => Carbon::now(),
					'mac_updated_at' => Carbon::now(),
					'mac_clear_reason' => 'Payroll merged. Already paid']);
			}
		}
	}

	public 
	function clearArrearClosedPayroll($employer_id, $payroll_id)
	{
		DB::table('portal.contribution_arrears')
		->where('employer_id',$employer_id)
		->where('payroll_id',$payroll_id)
		->update([
			'status' => 1,
			'mac_updated_by' => access()->user()->id,
			'updated_at' => Carbon::now(),
			'mac_updated_at' => Carbon::now(),
			'mac_clear_reason' => 'Payroll merged',
		]);
	}

	public function getAttachmentPathFileUrl($merge_request_id)
	{
		$merge_request = $this->findOrThrowException($merge_request_id);

		if (test_uri()) {
			$file_path = asset('storage'.DIRECTORY_SEPARATOR.'employer_registration'.DIRECTORY_SEPARATOR.'payroll_merging'.DIRECTORY_SEPARATOR.$merge_request->employer_id.DIRECTORY_SEPARATOR.$merge_request->id.DIRECTORY_SEPARATOR.$merge_request->attachment);
		} else {
			$file_path = asset('public/storage'.DIRECTORY_SEPARATOR.'employer_registration'.DIRECTORY_SEPARATOR.'payroll_merging'.DIRECTORY_SEPARATOR.$merge_request->employer_id.DIRECTORY_SEPARATOR.$merge_request->id.DIRECTORY_SEPARATOR.$merge_request->attachment);
		}
		return $file_path;
	}


	public function getAllPayrollByEmployer($employer_id)
	{
		$all_requests = $this->query()->where('employer_id',$employer_id)->get();
		$merged_payroll = [];
		foreach ($all_requests as $employer_merge) {
			switch ($employer_merge->status) {
				case 3:
				//reversed
				//rejected
				$exclude = ($employer_merge->wf_done == 0) ? true : false;
				break;
				case 0:
				case 1:
				case 2:
				$exclude = true;
				break;
				default:
				$exclude = false;
				break;
			}

			if ($exclude) {
				foreach (unserialize($employer_merge->old_values) as $payroll_merged) {
					array_push($merged_payroll, $payroll_merged);
				}
			}
		}
		return array_unique($merged_payroll);
	}


	public function getUsersForMerging($employer_id)
	{
		$exclude_payroll = $this->getAllPayrollByEmployer($employer_id);
		return DB::table('portal.employer_user')->select(['users.id as id', 'users.name', 'employer_user.payroll_id','employer_user.ismanage'])
		->join('portal.users', 'employer_user.user_id', '=', 'users.id')
		->where('employer_id', $employer_id)
		->where('ismanage', true)
		->whereNotIn('payroll_id',$exclude_payroll)
		->get();
	}


	public function returnPayrollDetails($payrolls_array,$employer_id)
	{
		$result = [];
		foreach ($payrolls_array as $value) {
			$employee_uploads = DB::table('portal.employee_uploads')->where('employer_id',$employer_id)
			->where('payroll_id',$value)->whereNull('deleted_at')->get();
			$employees = [];
			foreach ($employee_uploads as $employee) {
				array_push($employees, $employee->memberno);
			}

			$contrib_arrears = DB::table('portal.contribution_arrears')->where('employer_id',$employer_id)
			->where('payroll_id',$value)->where('status',0)->get();
			$arrears = [];
			foreach ($contrib_arrears as $arrear) {
				array_push($arrears, $arrear->id);
			}

			$receipts_contributions = DB::table('main.receipts')->where("employer_id", $employer_id)
			->whereNull('deleted_at')
			->whereNull('cancel_date')
			->where('payroll_id', $value)->get();
			$receipts = [];
			foreach ($receipts_contributions as $rct) {
				array_push($receipts, $rct->id);
			}

			$result [$value] = [
				'number_of_employees' => count($employee_uploads),
				'employees' => serialize($employees),
				'number_of_contributions' => count($receipts_contributions),
				'receipts' => serialize($receipts),
				'number_of_arrears' => count($contrib_arrears),
				'arrears' => serialize($arrears)
			];
		}

		return $result;
	}


	public function saveMergePayrollDetails($payroll_details, $payroll_merge_id)
	{
		foreach ($payroll_details as $key => $value) {
			DB::table('main.employer_payroll_merge_details')->updateOrInsert([
				'payroll_merge_id' => $payroll_merge_id,
				'payroll_id' => $key],
				[
					'number_of_employees' => $value['number_of_employees'],
					'employees' => $value['employees'],
					'number_of_contributions' => $value['number_of_contributions'],
					'receipts' => $value['receipts'],
					'number_of_arrears' => $value['number_of_arrears'],
					'arrears' => $value['arrears'],
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				]);
		}
	}

	public function getPayrollDetails($payroll_merge_id, $payroll_id)
	{
		return DB::table('main.employer_payroll_merge_details')->where("payroll_merge_id", $payroll_merge_id)
		->where('payroll_id', $payroll_id)->first();
	}

	public function addPayrollArrears($merge_request)
	{
		$employer_user = DB::table('portal.employer_user')
		->where('employer_id',$merge_request->employer_id)->where('ismanage',true)
		->where('payroll_id',$merge_request->getNewUserPayrollAttribute())->first();
		if (count($employer_user)) {
			$employer = (new \App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository())->findOrThrowException($merge_request->employer_id);
			$online_verification = DB::table('main.online_employer_verifications')
			->where('employer_id',$merge_request->employer_id)
			->where('status',1)->where('payroll_id',$merge_request->getNewUserPayrollAttribute())->first();

			if (count($online_verification)) {
				$this->saveArrearCommencement([
					'reg_no' => $merge_request->employer_id,
					'commencement_source' => $employer_user->commencement_source,
					'doc' => $online_verification->doc,
					'start_date' => $online_verification->project_start_date,
					'payroll_id' => $online_verification->payroll_id,
				]);
			}else{
				if ($employer->source == 2) {
					$this->saveArrearCommencement([
						'reg_no' => $employer->id,
						'commencement_source' => $employer_user->commencement_source,
						'doc' => $employer->doc,
						'start_date' => $employer->doc,
						'payroll_id' => $employer_user->payroll_id,
					]);
				}
			}
		}
	}


	public function returnAfterMergingDetails($payroll_merge_id,$employer_id)
	{
		$result = [];

		$before_merge = DB::table('main.employer_payroll_merge_details')
		->where('payroll_merge_id', $payroll_merge_id)->get();

		$b4_arrears = [];
		$b4_employees = [];
		$b4_receipts = [];
		$payrolls = [];
		foreach ($before_merge as $before) {
			array_push($payrolls,$before->payroll_id);
			foreach (unserialize($before->employees) as $memberno) {
				array_push($b4_employees,$memberno);
			}
			foreach (unserialize($before->arrears) as $arrear_id) {
				array_push($b4_arrears,$arrear_id);
			}
			foreach (unserialize($before->receipts) as $receipt_id) {
				array_push($b4_receipts,$receipt_id);
			}
		}

		$new_receipts = array_unique($b4_receipts);
		$new_employees = array_unique($b4_employees);
		$new_arrears = $this->afterMergeArrearEffect(array_unique($b4_arrears),array_unique($payrolls),$employer_id);

		return [
			'employees_after' => count($new_employees),
			'remain_employees' => serialize($new_employees),
			'contributions_after' => count($new_receipts),
			'remain_receipts' => serialize($new_receipts),
			'arrears_after' => count($new_arrears),
			'remain_arrears' => serialize(array_keys($new_arrears)),
			'updated_at' => Carbon::now(),
		];
	}


	public function afterMergeArrearEffect(array $arrears, array $payrolls, $employer_id)
	{
		$new_arrears = [];
		foreach ($arrears as $arrear_id) {
			$arrear = DB::table('portal.contribution_arrears')->where('id',$arrear_id)->where('status',0)->first();
			if (count($arrear)) {
				$exist_in_legacy = DB::table('main.legacy_receipts')
				->whereNull('deleted_at')
				->where('iscancelled',false)
				->where('employer_id', $employer_id)->where('contrib_month', $arrear->contrib_month)->first();
				$exist_in_receipt = DB::table('main.receipt_codes')
				->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
				->join('main.employers', 'receipts.employer_id', '=', 'employers.id')
				->where('iscancelled', 0)
				->where('receipt_codes.employer_id', $employer_id)
				->whereIn('receipts.payroll_id', $payrolls)
				->where('receipt_codes.contrib_month', $arrear->contrib_month)
				->first();

				$exist_in_legacy_codes = DB::table('main.legacy_receipts')
				->join('main.legacy_receipt_codes', 'legacy_receipt_codes.legacy_receipt_id', '=', 'legacy_receipts.id')
				->where('legacy_receipts.employer_id', $employer_id)
				->whereNull('legacy_receipts.deleted_at')
				->whereNull('legacy_receipt_codes.deleted_at')
				->where('legacy_receipt_codes.contrib_month', $arrear->contrib_month)->first();

				if (count($exist_in_receipt) < 1 && count($exist_in_legacy) < 1 && count($exist_in_legacy_codes) < 1 ) {
					$new_arrears[$arrear_id] = $arrear->contrib_month;
				}
			}
		}
		return array_unique($new_arrears);
	}


	public function mergeEmployees($payrolls,$employer_id,$payroll_id)
	{
		DB::table('portal.employee_uploads')
		->where('employer_id', $employer_id)->whereIn('payroll_id',$payrolls)->update([
			'payroll_id' => $payroll_id
		]);

		DB::table('main.employee_employer')
		->where('employer_id', $employer_id)->whereIn('payroll_id',$payrolls)->update([
			'payroll_id' => $payroll_id
		]);
	}

	public function combinedPayrolls($merge_user, $old_values, $request){
		$arrear_payrolls = [];
		foreach ($merge_user as  $key => $merge) {
			$payroll_id = $this->hasArrears($merge, $request->employer_id);
			if($payroll_id) $arrear_payrolls[] = $payroll_id;

			$old_values[$key] = $merge->payroll_id;
		}

		$max = max(json_decode(json_encode($merge_user)));

		return [
			'new_value' => $max->payroll_id + 1,
			'old_values' => $old_values,
			'arrear_payrolls' => $arrear_payrolls
		];
	}

	public function createUpdateNewPayroll($merge_request){
		DB::table('portal.employer_user')->updateOrInsert([
			'payroll_id' => unserialize($merge_request->new_values)[0],
			'user_id' => unserialize($merge_request->new_user)[0],
			'employer_id' => $merge_request->employer_id
		],
		[
			'ismanage' => true,
			'is_combined' => true,
			'doc' => $merge_request->doc,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		]);

		$employer_user = DB::table('portal.employer_user')->where([
			'payroll_id' => unserialize($merge_request->new_values)[0],
			'user_id' => unserialize($merge_request->new_user)[0],
			'employer_id' => $merge_request->employer_id,
			'ismanage' =>true
		])->first();

		return $employer_user->id;
		// return DB::getPdo()->lastInsertId();
	}

	public function hasArrears($merge, $employer_id){
		$return = false;
		$arrear = DB::table('portal.contribution_arrears')
		->where('employer_id', $employer_id)
		->where('payroll_id', $merge->payroll_id)
		->where('status', 0)->get();

		return count($arrear) > 0 ? $merge->payroll_id : $return;

	}

	public function mergeType($merge_request){
		return $merge_request->merge_type == 1 ? true : false;
	}

	public function checkHasSameContribution($keep_payroll_id, array $merge_payrolls, $employer_id)
	{
		$contrib_keep = [];
		$miezi_imejirudia = [];
		$contribs = DB::table('main.receipt_codes')->select('receipt_codes.*','receipts.payroll_id')
		->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
		->where('receipt_codes.employer_id', $employer_id)
		->whereNull('receipts.cancel_date')
		->where('receipts.iscancelled',false)
		->whereNull('receipts.deleted_at')
		->where('payroll_id', $keep_payroll_id)->where('receipt_codes.fin_code_id',2)->whereNull('receipt_codes.deleted_at')->get();

		foreach ($contribs as $contrib) {
			array_push($contrib_keep, Carbon::parse($contrib->contrib_month)->format('Y-m-28'));
		}
		foreach ($merge_payrolls as $payroll_za_merge) {
			$contrib_merge= DB::table('main.receipt_codes')->select('receipt_codes.*','receipts.payroll_id')
			->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
			->where('receipt_codes.employer_id', $employer_id)
			->where('payroll_id', $payroll_za_merge)->where('receipt_codes.fin_code_id',2)->whereNull('receipt_codes.deleted_at')->get();
			$mwezi_double = [];

			foreach ($contrib_merge as $merge) {
				if (in_array(Carbon::parse($merge->contrib_month)->format('Y-m-28'), array_unique($contrib_keep))) {
					array_push($mwezi_double, Carbon::parse($merge->contrib_month)->format('M, Y'));
				}
			}

			if (!empty($mwezi_double)) {
				$miezi_imejirudia[$payroll_za_merge] = array_unique($mwezi_double);
			}
		}
		return $miezi_imejirudia;
	}

	public function returnPayrollEmployeesDetails($request_id, $payroll_id, $type=1)
	{
		$data = $this->getPayrollDetails($request_id, $payroll_id);
		switch ($type) {
			case 2:
			$employees = !empty($data->remain_employees) ?  unserialize($data->remain_employees) : null;
			break;
			default:
			$employees = !empty($data->employees) ?  unserialize($data->employees) : null;
			break;
		}

		if (!empty($employees)) {
			$html = "<thead><tr><th>Member No#</th><th>Name</th></tr></thead><tbody>";
			foreach ($employees as $memberno) {
				$employee = DB::table('employees')->where('memberno',$memberno)->first();
				if (!empty($employee->firstname)) {
					$name = trim($employee->firstname.' '.$employee->middlename.' '.$employee->lastname);
					$html .= "<tr><td>".$memberno."</td><td>".strtoupper($name)."</td></tr>";
				}
			}
			return	$html .= "</tbody>";
		}
		return '';
	}



	public function returnPayrollArrearsDetails($request_id, $payroll_id, $type=1)
	{
		$data = $this->getPayrollDetails($request_id, $payroll_id);
		switch ($type) {
			case 2:
			$arrears = !empty($data->remain_arrears) ?  unserialize($data->remain_arrears) : null;
			break;
			default:
			$arrears = !empty($data->arrears) ?  unserialize($data->arrears) : null;
			break;
		}

		if (!empty($arrears)) {
			$html = "<thead><tr><th>No#</th><th>Month</th></tr></thead><tbody>";
			$number = 1;
			$loop_arrears = DB::table('portal.contribution_arrears')->whereIn('id',$arrears)->orderBy('contrib_month','asc')->get();
			foreach ($loop_arrears as $arrear) {
				$html .= "<tr><td>".$number."</td><td>".Carbon::parse($arrear->contrib_month)->format('M Y')."</td></tr>";
				$number++;			
			}
			return	$html .= "</tbody>";
		}
		return '';
	}

	public function returnPayrollContributionsDetails($request_id, $payroll_id, $type=1)
	{
		$data = $this->getPayrollDetails($request_id, $payroll_id);
		switch ($type) {
			case 2:
			$receipts = !empty($data->remain_receipts) ?  unserialize($data->remain_receipts) : null;
			break;
			default:
			$receipts = !empty($data->receipts) ?  unserialize($data->receipts) : null;
			break;
		}
		// dd($receipts);
		if (!empty($receipts)) {
			$html = "<thead><tr><th>Receipt No#</th><th>Amount</th><th>Month</th></tr></thead><tbody>";
			$number = 1;
			$loop_receipts = DB::table('main.receipt_codes')->select('receipt_codes.*','receipts.rctno')
			->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
			->whereIn('receipt_codes.id',$receipts)->orderBy('contrib_month','asc')->get();
			foreach ($loop_receipts as $receipt) {
				$html .= "<tr><td>".$receipt->rctno."</td><td>".number_format($receipt->amount,2)."</td><td>".Carbon::parse($receipt->contrib_month)->format('M Y')."</td></tr>";
				$number++;			
			}
			return	$html .= "</tbody>";
		}
		return '';
	}

}