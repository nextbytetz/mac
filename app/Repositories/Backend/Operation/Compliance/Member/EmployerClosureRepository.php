<?php

namespace App\Repositories\Backend\Operation\Compliance\Member;


use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Jobs\Compliance\UpdateBookingsNotContributed;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Api\ClosedBusinessRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EmployerClosureRepository extends BaseRepository
{
    use AttachmentHandler, FileHandler;


    const MODEL = EmployerClosure::class;

    protected $employer;

    protected $closed;

    public function __construct()
    {
        parent::__construct();
        $this->employer = new EmployerRepository();
        $this->closed = new ClosedBusinessRepository();

    }


    /*Get temporary closure period months - after closure to remind officers to followup*/
    public function getTemporaryClosurePeriodMonths()
    {
        return 6;
    }


    public function postCloseBusiness(array $input)
    {
        return DB::transaction(function () use ($input) {
            $input['close_date'] = $input['close_year'] . '-' . str_pad($input['close_month'], 2, "0", STR_PAD_LEFT) . '-' . str_pad($input['close_day'], 2, "0", STR_PAD_LEFT);
            $closure = $this->query()->create([
                'close_date' => $input['close_date'],
                'employer_id' => $input['employer_id'],
                'user_id' => access()->id(),
                'reason' => $input['reason'],
                'closure_type' => $input['closure_type'],
            ]);
            $this->employer->query()->where(['id' => $input['employer_id']])->update(['employer_closure_id' => $closure->id, 'employer_status' => 3]);
            //Check if there is any associated business from closed list from TRA.
            $query = $this->closed->query()->where(['isassociated' => 0, 'employer_id' => $input['employer_id']])->orderBy("id", "desc")->first();
            if ($query) {
                $query->isassociated = 1;
                $query->save();
            }
            $this->saveEmployerClosureDocument($closure->id);
            return $closure;
        });
    }

    /**
     * @param array $input
     * @return mixed
     * Store closure - new function with workflow
     */
    public function store(array $input)
    {
        return DB::transaction(function () use ($input) {
            $sub_type = (new CodeValueRepository())->findByReference($input['sub_type_cv_id']);
            $dormant_source_cv_id = isset($input['dormant_source_cv_id']) ? (new CodeValueRepository())->findIdByReference($input['dormant_source_cv_id']) : null;
            $formatted_internal_dates = $this->formatDatesForInternal($input);
            $closure = $this->query()->create([
                'close_date' => $input['close_date'],
                'application_date' => $input['application_date'],
                'employer_id' => $input['employer_id'],
                'user_id' => access()->id(),
                'reason' => $input['reason'],
                'closure_type' => ($sub_type->reference == 'ETCLPERMSETTL') ? 1  : $input['closure_type'],
                'is_legacy' => 0,
                'isbookingskip' => 0,
                'isdomestic' => (isset($input['isdomestic'])) ? 1 : 0,
                'specified_reopen_date' => $input['specified_reopen_date'],
                'sub_type_cv_id' => isset($input['sub_type_cv_id']) ? $sub_type->id : null,
                'dormant_source_cv_id' => isset($input['dormant_source_cv_id']) ? $dormant_source_cv_id : null,
                'other_dormant_source' => isset($input['other_dormant_source']) ? $input['other_dormant_source'] : null,
                'last_contribution' => isset($input['last_contribution']) ? $input['last_contribution'] : null,
                'close_date_internal' => $formatted_internal_dates['close_date_internal'],
                'specified_reopen_date_internal' => $formatted_internal_dates['specified_reopen_date_internal'],
                'followup_ref_date' => isset($input['specified_reopen_date']) ? $this->getFollowupRefDateFormatted($formatted_internal_dates['specified_reopen_date_internal']) :$formatted_internal_dates['close_date_internal'],
            ]);
            /*Update arrears data*/
            $this->updateArrearsDetails($closure->id);
            return $closure;
        });
    }



    /**
     * @param array $input
     * @return mixed
     * update closure - new function with workflow
     */
    public function update($employer_closure,  array $input)
    {
        return DB::transaction(function () use ($input, $employer_closure) {

            $sub_type = (new CodeValueRepository())->findByReference($input['sub_type_cv_id']);
            $dormant_source_cv_id = isset($input['dormant_source_cv_id']) ? (new CodeValueRepository())->findIdByReference($input['dormant_source_cv_id']) : null;
            $formatted_internal_dates = $this->formatDatesForInternal($input);
            $employer_closure->update([
                'close_date' => $input['close_date'],
                'application_date' => $input['application_date'],
                'reason' => $input['reason'],
                'closure_type' => ($sub_type->reference == 'ETCLPERMSETTL') ? 1  : $input['closure_type'],
                'isdomestic' => (isset($input['isdomestic'])) ? 1 : 0,
                'specified_reopen_date' => $input['specified_reopen_date'],
                'sub_type_cv_id' => isset($input['sub_type_cv_id']) ? $sub_type->id : null,
                'dormant_source_cv_id' => isset($input['dormant_source_cv_id']) ? $dormant_source_cv_id : null,
                'other_dormant_source' => isset($input['other_dormant_source']) ? $input['other_dormant_source'] : null,
                'last_contribution' => isset($input['last_contribution']) ? $input['last_contribution'] : null,
                'close_date_internal' => $formatted_internal_dates['close_date_internal'],
                'specified_reopen_date_internal' => $formatted_internal_dates['specified_reopen_date_internal'],
                'followup_ref_date' => isset($input['specified_reopen_date']) ? $this->getFollowupRefDateFormatted($formatted_internal_dates['specified_reopen_date_internal']) :$formatted_internal_dates['close_date_internal'],
            ]);

            /*Update arrears data*/
            $this->updateArrearsDetails($employer_closure->id);
            return $employer_closure;
        });
    }


    /*Format date for internal use- */
    public function formatDatesForInternal(array $input)
    {
        //Close date
        $close_date = null;
        if(isset($input['close_date'])){
            $close_date_parse = Carbon::parse($input['close_date']);
            $close_date_day = Carbon::parse($input['close_date'])->format('j');
            if($close_date_day >= 1 && $close_date_day <= 15){
                $close_date = $close_date_parse->startOfMonth();
            }else{
                $close_date = $close_date_parse->endOfMonth();
            }
        }
        //Reopen date
        $reopen_date = null;
        if(isset($input['specified_reopen_date'])) {
            $reopen_date =  Carbon::parse($input['specified_reopen_date'])->startOfMonth();
        }

        return ['close_date_internal' => $close_date, 'specified_reopen_date_internal' => $reopen_date];
    }

    /*Update Arrears Details*/
    public function updateArrearsDetails($employer_closure_id)
    {
        $employer_repo = new EmployerRepository();
        $employer_closure = $this->find($employer_closure_id);
        $employer_id = $employer_closure->employer_id;
        $close_date = $employer_closure->close_date;
        $isdomestic = $employer_closure->isdomestic;
        $receivable_details = $this->getReceivableDetails($employer_id, $close_date);
        $refund_details = $this->getRefundDetails($employer_id);
        $interest_payment_data = $employer_repo->interestPaymentDetails($employer_id);
        $interest_due_to_close_date =$employer_repo->getBookingInterests($employer_id)->where('miss_month','<=', standard_date_format($employer_closure->close_date))->sum('amount');
        $interest_remain = $interest_due_to_close_date - $interest_payment_data['total_paid'];
        $employer_closure->update([
            'arrears' => ($isdomestic == 1)  ? ($refund_details['contribution']) : $receivable_details['receivable'] ,
            'missing_months' => ($isdomestic == 1) ? null : $receivable_details['missing_months'],
            'interest_amount' =>  ($isdomestic == 1) ? null : $interest_remain,

        ]);

    }

    /*Get receivables at the time of closure*/
    public function getReceivableDetails($employer_id, $close_date)
    {
        $receivables = DB::table('bookings_view')->where('employer_id', $employer_id)->where('rcv_date', '<=', standard_date_format($close_date))->where('ispaid',0)
            ->sum('booked_amount');
        $missing_months = DB::table('bookings_view')->where('employer_id', $employer_id)->where('rcv_date', '<=', standard_date_format($close_date))->where('ispaid',0)
            ->count();

        return ['receivable' => $receivables, 'missing_months' => $missing_months];
    }

    /*Get refund details for domestic employers*/
    public function getRefundDetails($employer_id)
    {
        $contribution = (new EmployerRepository())->getContribReceivableSummary($employer_id)['contribution'];
        return ['contribution' => $contribution];
    }

    /**
     * @param $employer_id
     * Min CLose Date by employer
     */
    public function getFirstClosureByEmployer($employer_id)
    {
        $closure = $this->query()->where('employer_id', $employer_id)->orderBy('close_date')->first();
        return $closure;
    }

    /**
     * @param $employer_closure
     * Undo closure
     */
    public function undo($employer_closure)
    {
        $closure_id = $employer_closure->id;
        DB::transaction(function () use ($closure_id, $employer_closure) {
            $employer_closure->delete();

            /*Deactivate workflow tracks*/
            $type = $this->getWfModuleType($employer_closure);
            $wf_module_group_id = 18;
            $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $closure_id, "type" => $type]);
            /*deactivate wf track*/
            $workflow->wfTracksDeactivate();
            /*end workflow track*/
        });
    }




    public function postOpenBusiness(array $input)
    {
        return DB::transaction(function () use ($input) {
            $employer = $this->employer->query()->find($input['employer_id']);
            $closure = $employer->closures()->orderBy("id", "desc")->first();
            /*Check if closure exist*/
            if ($closure){
                $closure->open_date = $input['open_date'];
                $closure->open_user_id = access()->id();
                $closure->open_reason = $input['open_reason'];
                $closure->save();
            }
            $employer->employer_status = 1;
            $employer->employer_closure_id = NULL;
            $employer->save();

        });
    }


    /**
     * @param $id
     * Updates on initiate workflow
     */
    public function initiateApproval($id)
    {
//        $this->checkIfCanInitiateApproval($id);
        return  DB::transaction(function () use ($id) {
            $closure = $this->find($id);
            $closure->update([
                'wf_status' => 1
            ]);

            /*Update arrears*/
            $this->updateArrearsDetails($id);

            return $closure;
        });

    }


    /*check if can initiate*/
    public function checkIfCanInitiateApproval($closure_id)
    {
        /*check if doc are submitted -- Mandatory*/
        $doc = $this->getDocumentsNotSubmitted($closure_id);
        if($doc->isNotEmpty())
        {
            throw new WorkflowException('Please attach all required document to proceed!');
        }
    }

    /*Update wf done*/
    public function updateOnWorkflowComplete($id)
    {
        return DB::transaction(function () use ($id) {
            $closure = $this->find($id);
            /*Update employer status*/
            $closure = $this->updateEmployerStatusAfterApproval($id);

            /*Update closure arrears*/
            $this->updateArrearsDetails($id);

            /*U[date portal contrib arrears status*/
            $this->updatePortalContribArrearsStatus($closure);

        });
    }


    /*Update wf done for closure*/
    public function updateWfDone($closure, $wf_done = 1, $isapproved = 1)
    {
        $wf_done_date = $closure->wf_done_date;
        $isbookingskip = ($isapproved == 1) ? 1 : 0;
        if(isset($wf_done_date)){
            $closure->update(['wf_done' => $wf_done, 'isbookingskip' => $isbookingskip]);
        }else{
            $closure->update(['wf_done' => $wf_done, 'wf_done_date' => Carbon::now(), 'isbookingskip' => $isbookingskip]);
        }

    }




    /*Update closure status for employer after approved by DO/DG*/
    public function updateEmployerStatusAfterApproval($id)
    {
        $sub_type_cv_id = (new CodeValueRepository())->findIdByReference('ETCLDORMANT');
        $closure = $this->find($id);
        $new_employer_status = ($closure->sub_type_cv_id == $sub_type_cv_id) ? 4 : 3;
        $employer_id = $closure->employer_id;
        $this->employer->query()->where(['id' => $employer_id])->update(['employer_closure_id' => $closure->id, 'employer_status' => $new_employer_status]);

        //Check if there is any associated business from closed list from TRA.
        $query = $this->closed->query()->where(['isassociated' => 0, 'employer_id' => $employer_id])->orderBy("id", "desc")->first();
        if ($query) {
            $query->isassociated = 1;
            $query->save();
        }
        return $closure;
    }


    /**
     * @param Model $closure
     * Get first contribution month after closure
     */
    public function getFirstContributionMonthAfterClosure(Model $closure)
    {
        $close_date = $closure->close_date_internal ?? $closure->close_date;
        $payroll_ids =isset($closure->payroll_ids) ? json_decode($closure->payroll_ids) : [];
        $contrib_after_close_date = null;
        if(count($payroll_ids)) {
            $contrib_after_close_date = DB::table('employer_contributions as e')
                ->select('e.*')
                ->join('receipts as r', function($join){
                    $join->on('r.employer_id', 'e.employer_id');
                })
                ->where('r.employer_id', $closure->employer_id)
                ->whereIn('r.payroll_id', $payroll_ids)
                ->where('e.contrib_month', '>', $close_date)->orderBy('contrib_month')->first();
        }

        return $contrib_after_close_date->contrib_month ?? null;
    }


    /**
     * @param $closure
     * Update Portal contrib arrears status ---------Start Contrib Arrears--------------
     */
    public function updatePortalContribArrearsStatus($closure)
    {
        $employer_repo = new EmployerRepository();
        $employer_id = $closure->employer_id;
        $check_if_has_multiple_payrolls = $employer_repo->checkIfEmployerHasMultiplePayrolls($employer_id);
        if($check_if_has_multiple_payrolls == false){
            /*Update payroll fields*/
            $payroll_ids = $employer_repo->getEmployerPayrolls($employer_id)->pluck('payroll_id');

            $data = [
                'payroll_ids' => json_encode($payroll_ids),
                'ispayroll_confirmed' =>1
            ];
            $closure = $this->confirmPayrollsForThisClosure($closure->id, $data);
        }
        /*Portal contrib arrears - remove for this employer*/
        if(isset($closure->payroll_ids)){
            $closure = $closure->refresh();
            $contrib_month_after_close_date = $this->getFirstContributionMonthAfterClosure($closure);
            (new \App\Services\Compliance\UpdateBookingsNotContributed())->updateStatusForContribArrearsWithBusinessClosureWithPayroll($employer_id,$closure->close_date_internal, json_decode($closure->payroll_ids), $contrib_month_after_close_date);
        }



    }

    /**
     * @param Model $closure
     * check if payroll to close has been confirmed
     */
    public function checkIfPayrollIsConfirmed(Model $closure)
    {
        $employer_repo = new EmployerRepository();
        $employer_id = $closure->employer_id;
        $check_if_has_multiple_payrolls = $employer_repo->checkIfEmployerHasMultiplePayrolls($employer_id);
        if($check_if_has_multiple_payrolls == false) {
            /*Update payroll fields*/
            $payroll_ids = $employer_repo->getEmployerPayrolls($employer_id)->pluck('payroll_id');
            $data = [
                'payroll_ids' => json_encode($payroll_ids),
                'ispayroll_confirmed' =>1
            ];
            $closure =  $this->confirmPayrollsForThisClosure($closure->id, $data);

        }

        /*check flag*/
        if ($closure->ispayroll_confirmed == 0){
            throw new WorkflowException('Payroll not confirmed! Please check before proceeding!');
        }

    }



    /*Confirm Payroll for this closure*/
    public function confirmPayrollsForThisClosure($closure_id, array $input)
    {
        $closure = $this->find($closure_id);
        $closure->update([
            'payroll_ids' => $input['payroll_ids'],
            'ispayroll_confirmed' => $input['ispayroll_confirmed']
        ]);

        return $closure;
    }


    /*--------End Portal contrib arrears ------------*/

    /*Process actions at levels before on approval before wf complete*/
    public function processWfActionAtLevels($current_wf_track, array $input = null)
    {
        DB::transaction(function () use ($current_wf_track, $input) {
            $wf_module_repo = new WfModuleRepository();
            $resource_id = $current_wf_track->resource_id;
            $wf_module_id = $current_wf_track->wfDefinition->wf_module_id;
            $wf_definition = $current_wf_track->wfDefinition;
            $level = $wf_definition->level;
            $closure = $this->find($resource_id);
            $is_approved = ($current_wf_track->status == 1) ? 1 : 0;

            switch ($wf_module_id) {
                case $wf_module_repo->employerBusinessClosureModules()[0]:
                    /*Temporary closure - 37*/
                    switch ($level) {
                        case 4:

                            //update closure wf_done
                            $this->updateWfDone($closure, 0, $is_approved);

                            if ($is_approved == 1) {

                                /*wf complete on approved  at Approval level*/
                                $this->updateOnWorkflowComplete($resource_id);
                            }
                            break;


                        case 5:
                            /*level for letter*/

                            $this->checkIfWfLetterIsInitiated($closure, $current_wf_track);

                            /*payroll confirmation*/
                            $this->checkIfPayrollIsConfirmed($closure);

                            /*Update contrib arrears portals*/
                            $this->updatePortalContribArrearsStatus($closure);
                            break;
                    }

                    break;

                case $wf_module_repo->employerBusinessClosureModules()[1]:
                    /*Permanent closure - 38*/
                    switch ($level) {
                        case 5:

                            //update closure wf_done
                            $this->updateWfDone($closure, 0);

                            if ($is_approved == 1) {
                                /*wf complete on approved  at Approval level*/
                                $this->updateOnWorkflowComplete($resource_id);
                            }
                            break;


                        case 6:
                            /*level for letter*/
                            $this->checkIfWfLetterIsInitiated($closure, $current_wf_track);
                            /*payroll confirmation*/
                            $this->checkIfPayrollIsConfirmed($closure);

                            /*Update contrib arrears portals*/
                            $this->updatePortalContribArrearsStatus($closure);
                            break;
                    }

                    break;

            }
        });

    }


    /*Check if need response letter*/
    public function checkIfNeedResponseLetter(Model $closure){
        $return = true;
        if(isset($closure->sub_type_cv_id)){
            $sub_type_ref = $closure->subType->reference;
            if($sub_type_ref == 'ETCLDORMANT'){
                $return = false;
            }


        }

        return $return;
    }


    /*Get level For Letter*/
    public function getLevelForLetter($closure)
    {
        $wf_module_repo = new WfModuleRepository();
        $wf_module_group_id = 18;
        $type = $this->getWfModuleType($closure);
        $wf_module = (new WfModuleRepository())->query()->where('type', $type)->where('wf_module_group_id', $wf_module_group_id)->first();
        $wf_module_id = $wf_module->id;
        switch($wf_module_id){
            case $wf_module_repo->employerBusinessClosureModules()[0]://37
                return 5;
                break;

            case $wf_module_repo->employerBusinessClosureModules()[1]://38
                return 6;
                break;
        }
    }

    /*Check if can initiate wf letter*/
    public function checkIfCanInitiateLetterWf($current_track)
    {
        $return = false;
        if(isset($current_track)){
            $wf_definition = $current_track->wfDefinition;
            $current_level = $wf_definition->level;
            $wf_module_id = $wf_definition->wf_module_id;
            $closure = $this->find($current_track->resource_id);
            $wf_module_group_id = 18;
            $type = $this->getWfModuleType($closure);
            $level_for_letter = $this->getLevelForLetter($closure);
            $access = access()->user()->hasWorkflowModuleDefinition($wf_module_group_id, $type, $level_for_letter);
            $arrears = $closure->arrears;
            if($current_level == $level_for_letter && $access == true && $closure->wf_done == 0)
//            if($current_level == $level_for_letter && $access == true && $arrears == 0)
            {
                //Check if was declined on approval level
                $check_if_declined = (new WfTrackRepository())->checkIfModuleDeclined($current_track->resource_id, $wf_module_id);
                if($check_if_declined == false){
                    //only if approved
                    $return = true;
                }
            }
        }
        return $return;
    }


    /*Check if wf letter is initiated*/
    public function checkIfWfLetterIsInitiated($closure, $current_wf_track)
    {
        $check_if_need_response = $this->checkIfNeedResponseLetter($closure);
        if($closure->closureLetter()->where('isinitiated','<>', 0)->count() == 0 && $this->checkIfCanInitiateLetterWf($current_wf_track) == true && $check_if_need_response == true)
        {
            throw new WorkflowException('Workflow for letter not initiated! Initiate to proceed!');
        }
        return true;
    }

//    public function getDocExtension($file_name_input)
//    {
//        $file = request()->file($file_name_input);
//        if($file->isValid()) {
//            return $file->getClientOriginalExtension();
//        }else{
//            return '';
//        }
//    }


    /**
     * @param array $input
     * @param $closure_id
     * Save documents for closure
     */
    public function saveClosureDocuments(array $input, $closure_id)
    {
        return DB::transaction(function () use ($closure_id, $input) {
            $document_id = $input['document_id'];
            $closure = $this->find($closure_id);
            $employer = $closure->closedEmployer;
            $check_if_recurring = (new DocumentRepository())->checkIfDocumentIsRecurring($document_id);
            $ext = $this->getDocExtension('document_file');

            if($check_if_recurring == false)
            {
                /*for non recurring document*/
                $check_if_exist = $this->checkIfDocumentExistForClosure($closure_id, $document_id);
                if($check_if_exist == false){
                    /*if not exists - Attach new*/
                    $this->savePivotDocument($closure_id, $input);
                }else{
                    $uploaded_doc = $employer->documents()->where('external_id', $closure_id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
                    $this->updatePivotDocument($closure_id, $input,$uploaded_doc->pivot->id );
                }

                /*Attach to storage*/
                $this->attachDocFileToStorage($closure_id, $document_id, $ext);
            }else{
                /*for recurring documents*/
                $check_if_exist = isset($input['doc_pivot_id']) ? true : false;

                if($check_if_exist){
                    /*Exist - update*/
                    $this->updatePivotDocument($closure_id, $input, $input['doc_pivot_id']);
                }else {
                    /*Does not exist - Add new*/
                    $this->savePivotDocument($closure_id, $input);
                }

                /*Attach to storage*/
                $this->attachDocFileToStorage($closure_id, $document_id, $ext);
            }
        });
    }


    /*Save into pivot table of document*/
    public function savePivotDocument($closure_id, array $input)
    {
        $document_id = $input['document_id'];
        $closure = $this->find($closure_id);
        $employer = $closure->closedEmployer;
        $check_if_exist = $this->checkIfDocumentExistForClosure($closure_id, $document_id);
        $file = request()->file('document_file');
        if($file->isValid()) {
            $employer->documents()->attach([$document_id => [
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'external_id' => $closure_id
            ]]);
        }
    }

    /*Update pivot table of document*/
    public function updatePivotDocument($external_id, array $input, $doc_pivot_id)
    {
//        $closure = $this->find($closure_id);
        $file = request()->file('document_file');
        if($file->isValid()) {
            DB::table('document_employer')->where('id', $doc_pivot_id)->update([
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'external_id' => $external_id
            ]);
        }
    }


    /*Attach doc to server storage*/
    public function attachDocFileToStorage($closure_id, $document_id, $ext)
    {
        /*Attach document to server*/
        $closure = $this->find($closure_id);
        $employer = $closure->closedEmployer;
        $uploaded_doc = $employer->documents()->where('external_id', $closure_id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
        $path = employer_closure_dir() . DIRECTORY_SEPARATOR . $employer->id . DIRECTORY_SEPARATOR . $closure_id . DIRECTORY_SEPARATOR;
        $filename = $uploaded_doc->pivot->id . '.' . $ext;
        $this->makeDirectory($path);
        $this->saveDocumentBasic('document_file',$filename, $path );
    }


    /*Delete document*/
    public function deleteDocument($doc_pivot_id)
    {
        $uploaded_doc =DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $path = employer_closure_dir() . DIRECTORY_SEPARATOR . $uploaded_doc->employer_id . DIRECTORY_SEPARATOR . $uploaded_doc->external_id . DIRECTORY_SEPARATOR;
        $file_path = $path . $doc_pivot_id . '.' . $uploaded_doc->ext;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        /*delete*/
        DB::table('document_employer')->where('id', $doc_pivot_id)->delete();
    }

    /* Check if document exists for closure */
    public function checkIfDocumentExistForClosure($external_id, $document_id)
    {
        $closure = $this->find($external_id);
        $employer = $closure->closedEmployer;
        $check = $employer->documents()->where('external_id', $external_id)->where('document_id', $document_id)->count();
        if($check > 0)
        {
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param int $closure_type
     * @return array
     * Get mandatory docs ids by closure type
     */
    public function getMandatoryDocsIdsByClosureType($closure)
    {
        switch ($closure->closure_type)
        {
            case 1:
                /*temporary*/
                $return =  [48];
                if(isset($closure->sub_type_cv_id)){
                    if(isset($closure->dormant_source_cv_id)){
                        $dormant_source_ref = $closure->dormantSource->reference;
                        if($dormant_source_ref == 'AUSDREGWCF'){
                            //Dormant - From WCF
                            $return = [97, 98];
                        }else{
                            //Dormant - Other authorities
                            $return = [95,96, 97];
                        }
                    }

                }

                return $return;
                break;

            case 2:
                /*permanent*/
                if($closure->isdomestic == 0){
                    return [48];
                }else{
                    return [48];
                }

                break;

            default:
                /*all docs for closure of business*/
                $return =[48,69,70,71,80,81];

                return $return;
                break;
        }
    }


    /**
     * @param $closure
     * @return array
     * Get documents for select when attaching document
     */
    public function getDocsIdsForAttachmentByClosureType($closure)
    {
        switch ($closure->closure_type)
        {
            case 1:
                /*temporary*/
                /*all docs for closure of business*/
                $return =[48,69,70,71,80,81];
                if(isset($closure->sub_type_cv_id)){
                    if(isset($closure->dormant_source_cv_id)){
                        $dormant_source_ref = $closure->dormantSource->reference;
                        if($dormant_source_ref == 'AUSDREGWCF'){
                            //Dormant - From WCF
                            $return = [70, 97, 98];
                        }else{
                            //Dormant - Other authorities
                            $return = [70,95,96, 97];
                        }
                    }

                }
                return $return;
                break;

            case 2:
                /*permanent*/
                $return =[48,69,70,71,80,81];
                return $return;
                break;

            default:

                break;
        }
    }


    /**
     * @param $employer_closure_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * Get documents pending to be attached
     */
    public function getDocumentsNotSubmitted($employer_closure_id)
    {
        $closure = $this->find($employer_closure_id);
        $employer = $closure->closedEmployer;
        $docs_id_required = $this->getMandatoryDocsIdsByClosureType($closure);
        $docs_attached = $employer->documents()->where('external_id', $employer_closure_id)->whereIn('document_id',$docs_id_required)->pluck('document_id')->toArray();

        $doc_id_not_attached = array_diff($docs_id_required, $docs_attached);

        $docs_pending =  Document::query()->whereIn('id', $doc_id_not_attached)->get();
        return $docs_pending;
    }



    /*Get IDs of all docs which are non recurring for all closure types*/
    public function getDocsIdsNonRecurringForAllClosureTypes(){
        return  [48,69,70,71,80,81,95,96, 97, 98];
    }

    /**
     * @param $employer_closure_id
     * Get doc attached for this closure
     */
    public function getDocsAttachedNonRecurring($employer_closure_id)
    {
        $closure = $this->find($employer_closure_id);
        $employer = $closure->closedEmployer;
        $docs_all =$this->getDocsIdsNonRecurringForAllClosureTypes();
        $docs_attached = $employer->documents()->where('external_id', $employer_closure_id)->whereIn('document_id',$docs_all)->get();
        return $docs_attached;
    }



    /**
     * @param $employer_closure_id
     * Get doc attached for this closure
     */
    public function getDocsAttachedRecurring($employer_closure_id)
    {
        $closure = $this->find($employer_closure_id);
        $employer = $closure->closedEmployer;
        $docs_all = [72];
        $docs_attached = $employer->documents()->where('external_id', $employer_closure_id)->whereIn('document_id',$docs_all)->get();
        return $docs_attached;
    }


    /*Years for wcp docs attached*/
    public function getYearsForWcpDocsAttached($employer_closure_id)
    {
        $wcp_doc_id = 72;
        $docs = DB::table('document_employer')->select(DB::raw(" distinct(date_part('year' :: text, date_reference))"))->where('external_id', $employer_closure_id)->where('document_id',$wcp_doc_id)->get();
        return $docs;
    }


    /*Months for wcp1 to be submitted*/
    public function getMonthsForWcp1Document($employer_closure_id)
    {
        $closure = $this->find($employer_closure_id);
        $close_date = standard_date_format($closure->close_date);
        $close_date_parsed = Carbon::parse($close_date)->startOfMonth();
        $months_array = [];
        while(comparable_date_format($close_date_parsed) < comparable_date_format(Carbon::now()))
        {
            $months_array[] = $close_date_parsed->format('Y'). '-' . $close_date_parsed->format('m');
            $close_date_parsed->addMonthNoOverflow(1);
        }
        return $months_array;
    }

    /*Get  close date limit to close in advance*/
    public function getClosureDateCutOff()
    {
        return standard_date_format(\Carbon\Carbon::now()->addMonthNoOverflow(1));
    }


    /*Next followup ref date*/
    public function getFollowupRefDateFormatted($expected_reopen_date)
    {
        $expected_reopen_date = isset($expected_reopen_date) ? standard_date_format($expected_reopen_date) : standard_date_format(getTodayDate());
        $closure_period = $this->getTemporaryClosurePeriodMonths();
        $follow_up_ref_date = Carbon::parse($expected_reopen_date)->subMonthsNoOverflow($closure_period);
        return standard_date_format($follow_up_ref_date);
    }


    /**
     * @param $employer_closure
     * @return int
     * Get module type for workflow when initiating approval workflow
     */
    public function getWfModuleType($employer_closure)
    {
        $type = 0;
        switch ($employer_closure->closure_type)
        {
            case 1:
                /*Temporary*/
                $type = 1;
                break;
            case 2:
                /*Permanent*/
                $type = 2;
                break;
        }
        return $type;
    }


    /*Get closures for dataTable per employer history*/
    public function getClosureByEmployerForDataTable($employer_id)
    {
        return $this->query()->where('employer_id', $employer_id);
    }


    /*General query for all closures*/
    public function queryClosures()
    {
        return $this->query()->select(
            'employer_closures.id as closure_id',
            'e.reg_no as employer_regno',
            'e.name as employer_name',
            'r.name as region_name',
            'employer_closures.close_date as close_date',
            'employer_closures.application_date as application_date',
            'employer_closures.wf_done_date as approved_date',
            'employer_closures.followup_ref_date as followup_ref_date',
            'employer_closures.closure_type as closure_type',
            DB::raw("CASE WHEN employer_closures.closure_type = 1 THEN 'Temporary Closure' ELSE 'Permanent Closure' END as closure_type_name")
        )
            ->join('employers as e', 'e.id', 'employer_closures.employer_id')
            ->leftjoin('regions as r', 'r.id', 'e.region_id');
    }


    /**
     * Get temporary closed employers for follow up to check status of their business (After 6 months)
     */
    public function getTemporaryClosedForFollowUpStatusForDt()
    {
        $period_months = $this->getTemporaryClosurePeriodMonths();
        $today = Carbon::parse(getTodayDate());
        $limit_date = $today->subMonthsNoOverflow($period_months);
        $limit_date = standard_date_format($limit_date);
        return $this->queryClosures()->where('employer_closures.closure_type', 1)->where('employer_closures.is_legacy',0)->where('employer_closures.wf_done', 1)->whereNull('employer_closures.open_date')->where('e.employer_status', 3)->whereRaw("(coalesce(employer_closures.followup_ref_date,employer_closures.close_date_internal) < ?)" , [$limit_date]);
    }

    /*Store status follow up of temporary closed after 6months*/
    public function storeStatusFollowUp($employer_closure, array $input)
    {
        DB::transaction(function () use ($employer_closure, $input) {

            $employer_closure->update([
                'followup_ref_date' => $input['followup_date'],
                'followup_outcome' => $input['followup_outcome'],
                'followup_by' => access()->id(),
            ]);

            /*InitiateChecker*/
            $this->initializeChecker($employer_closure, 'EMPDERREOP');
        });
    }

    /*Get Status follow up outcome*/
    public function getStatusFollowupOutcome($followup_outcome_id)
    {
        switch($followup_outcome_id){
            case 1:
                return 'Still Closed';
                break;
            case 2:
                return 'Reopen/Resume Business';
                break;

            default:
                return '';
                break;

        }
    }

    /**
     *
     *FOLLOW UP METHODS
     */

    public function getFollowUpsByClosureForDataTable($employer_closure_id)
    {
        return EmployerClosureFollowUp::query()->where('employer_closure_id', $employer_closure_id);
    }


    /*Store FOLLOW UP */
    public function storeFollowUp(array $input)
    {
        return DB::transaction(function () use ($input) {
            $followup_cv = CodeValue::query()->find($input['follow_up_type_cv_id']);
            $employer_closure_follow_up = EmployerClosureFollowUp::query()->create([
                'description' => $input['description'],
                'feedback' => isset( $input['feedback']) ?  $input['feedback'] : $this->getStatusFollowupOutcome($input['followup_outcome']),
                'follow_up_type_cv_id' => $input['follow_up_type_cv_id'],
                'contact' => isset( $input['contact']) ?  $input['contact'] : null,
                'contact_person' => $input['contact_person'],
                'date_of_follow_up' => $input['date_of_follow_up'],
                'user_id' => access()->id(),
                'employer_closure_id' => $input['employer_closure_id'],
                'isstatus_followup' => $input['isstatus_followup']
            ]);

            /*save status follow up*/
            if($employer_closure_follow_up->isstatus_followup ==1){
                $data = [
                    'followup_date' => $input['date_of_follow_up'],
                    'followup_outcome' => $input['followup_outcome'],
                ];

                $this->storeStatusFollowUp($employer_closure_follow_up->employerClosure, $data);
            }

            /*Save the document */
            $document_id = $this->getDocumentIdByFollowUpType($followup_cv);
            if(isset($document_id)){
                $input['document_id'] = $document_id;
                $this->saveClosureFollowUpDocuments($input, $employer_closure_follow_up->id);
            }

            return $employer_closure_follow_up;
        });
    }



    /*Update FOLLOW UP */
    public function updateFollowUp($follow_up_id, array $input)
    {
        return DB::transaction(function () use ($input, $follow_up_id) {
            $follow_up  = EmployerClosureFollowUp::query()->where('id', $follow_up_id)->first();
            $follow_up->update([
                'description' => $input['description'],
                'feedback' => isset( $input['feedback']) ?  $input['feedback'] : $this->getStatusFollowupOutcome($input['followup_outcome']),
                'follow_up_type_cv_id' => $input['follow_up_type_cv_id'],
                'contact' => isset( $input['contact']) ?  $input['contact'] : null,
                'contact_person' => $input['contact_person'],
                'date_of_follow_up' => $input['date_of_follow_up'],
                'isstatus_followup' => $input['isstatus_followup']
            ]);


            /*save status follow up*/
            if($follow_up->isstatus_followup == 1){
                $data = [
                    'followup_date' => $input['date_of_follow_up'],
                    'followup_outcome' => $input['followup_outcome'],
                ];

                $this->storeStatusFollowUp($follow_up->employerClosure, $data);
            }

            /*Save the document */
            if(isset($input['document_file'])) {
                $followup_cv = CodeValue::query()->find($input['follow_up_type_cv_id']);
                $document_id = $this->getDocumentIdByFollowUpType($followup_cv);
                if (isset($document_id)) {
                    $input['document_id'] = $document_id;
                    $this->saveClosureFollowUpDocuments($input, $follow_up->id);
                }
            }
            return $follow_up;
        });
    }

    /*Get document id by follow type*/
    public function getDocumentIdByFollowUpType($follow_type)
    {
        $document_id = null;
        switch ($follow_type->reference)
        {
            case 'FUVSIT':
                $document_id = 83;
                break;

            case 'FULETT':
                $document_id = 84;
                break;
        }

        return $document_id;
    }


    /**
     * @param $follow_up_id
     */
    public function deleteFollowUp($follow_up_id)
    {
        $follow_up  = EmployerClosureFollowUp::query()->where('id', $follow_up_id)->first();
        $follow_up->delete();
    }


    /**
     * E-OFFICE DOCUMENT
     */

    /*get pending closures*/
    public function getPendingClosuresFromEofficeForDataTable()
    {
        return DB::table('eoffice_employer_closures')->where('ispending', 1)->whereNull('deleted_at')->where('document_id', 48);
    }


    public function syncClosureFromEoffice($eoffice_employer_closure_id,array $input)
    {
        return DB::transaction(function () use ($input, $eoffice_employer_closure_id) {
            /*update on alert employer_id*/
            $eoffice_employer_closure = DB::table('eoffice_employer_closures')->where('id',$eoffice_employer_closure_id)->first();
            DB::table('eoffice_employer_closures')->where('id', $eoffice_employer_closure_id)->update([
                'employer_id' => $input['employer_id'],
                'ispending' => 0,
            ]);


            /*create closure on MAC*/
            $employer_closure =    $this->store($input);


            /*insert into document employer*/
            $document_id = 48;
            $input['document_id'] = $document_id;
            $this->saveDocumentEmployerFromEoffice($input, $eoffice_employer_closure, $employer_closure->id);


            return $employer_closure;
        });
    }

    public function saveDocumentEmployerFromEoffice(array $input, $eoffice_employer_closure,$employer_closure_id)
    {
        ; //closure letter
        DB::table('document_employer')->insert([
            'document_id' => $input['document_id'],
            'employer_id' => $input['employer_id'],
            'eoffice_external_id' =>$eoffice_employer_closure->eoffice_closure_id,
            'eoffice_document_id' =>$eoffice_employer_closure->eoffice_document_id,
            'external_id' =>$employer_closure_id,
            'doc_date' => $eoffice_employer_closure->doc_date,
            'doc_receive_date' =>  $eoffice_employer_closure->doc_receive_date,
            'doc_create_date' =>  $eoffice_employer_closure->doc_create_date,
            'folio' => $eoffice_employer_closure->folio,
            'description' => $eoffice_employer_closure->subject,
            'name' =>'E-office',
            'created_at' => Carbon::now(),
        ]);
    }


    /*Save closures alert from e-office*/
    public function saveClosureDocsFromEoffice(array $input)
    {
        DB::table('eoffice_employer_closures')->insert([
            'eoffice_document_id' => $input['documentId'],
            'eoffice_closure_id' => $input['closureId'],
            'doc_date' => isset($input['documentDate']) ? $input['documentDate'] : null ,
            'doc_receive_date' =>  isset($input['documentReceiveDate']) ? $input['documentReceiveDate'] : null ,
            'doc_create_date' =>  isset($input['documentCreateDate']) ? $input['documentCreateDate'] : null ,
            'folio' => isset($input['folionumber']) ? $input['folionumber'] : null,
            'subject' => isset($input['subject']) ? $input['subject'] : null,
            'from' => isset($input['from']) ? $input['from'] : null,
            'document_id' => isset($input['document_id']) ? $input['document_id'] : null,
            'ismainfile' => isset($input['document_id']) ? (($input['document_id'] == 48) ? 1 : 0 ): 0 ,
            'created_at' => Carbon::now(),
        ]);
    }

    /*Check if employer has pending closure*/
    public function checkIfEmployerHasPendingClosure($employer_id)
    {
        $check = EmployerClosure::query()->where('employer_id', $employer_id)->where('is_legacy', 0)->where('wf_done',0)->count();
        if($check > 0)
        {
            /*There is pending*/
            return true;
        }else{
            /*There is no pending*/
            return false;
        }
    }

    /*Check if closure exist*/
    public function checkIfClosureExistForEmployer($employer_id, $close_date)
    {
        /*when create*/
        $check = EmployerClosure::query()->where('employer_id', $employer_id)->whereDate('close_date', standard_date_format($close_date))->count();

        if($check > 0)
        {
            /*There is closure - exists*/
            return true;
        }else{
            /*There is no closure - does not exists*/
            return false;
        }
    }




    /*ATTACH FOLLOW UP DOCUMENT*/
    /**
     * @param array $input
     * @param $closure_id
     * Save documents for closure
     */
    public function saveClosureFollowUpDocuments(array $input, $follow_up_id)
    {
        return DB::transaction(function () use ($follow_up_id, $input) {
            $document_id = $input['document_id'];
            $follow_up = EmployerClosureFollowUp::query()->find($follow_up_id);
            $employer = $follow_up->employerClosure->closedEmployer;
            $check_if_recurring = (new DocumentRepository())->checkIfDocumentIsRecurring($document_id);
            $ext = $this->getDocExtension('document_file');

            if($check_if_recurring == false)
            {
                /*for non recurring document*/
                $check_if_exist = $this->checkIfDocumentExistForFollowUp($follow_up->id, $document_id);
                if($check_if_exist == false){
                    /*if not exists - Attach new*/
                    $this->savePivotFollowUpDocument($follow_up_id, $input);
                }else{
                    $uploaded_doc = $employer->documents()->where('external_id', $follow_up_id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
                    $this->updatePivotDocument($follow_up_id, $input,$uploaded_doc->pivot->id );
                }

                /*Attach to storage*/
                $this->attachFollowUpDocFileToStorage($follow_up_id, $document_id, $ext);
            }else{
                /*for recurring documents*/
                $this->savePivotFollowUpDocument($follow_up_id, $input);
                /*Attach to storage*/
                $this->attachFollowUpDocFileToStorage($follow_up_id, $document_id, $ext);
            }
        });
    }


    /* Check if document exists for follow up */
    public function checkIfDocumentExistForFollowUp($external_id, $document_id)
    {
        $follow_up =EmployerClosureFollowUp::query()->find($external_id);
        $employer = $follow_up->employerClosure->closedEmployer;
        $check = $employer->documents()->where('external_id', $external_id)->where('document_id', $document_id)->count();
        if($check > 0)
        {
            return true;
        }else{
            return false;
        }
    }


    /*Save into pivot table of document*/
    public function savePivotFollowUpDocument($follow_up_id, array $input)
    {
        $document_id = $input['document_id'];
        $follow_up =EmployerClosureFollowUp::query()->find($follow_up_id);
        $employer = $follow_up->employerClosure->closedEmployer;
        $check_if_exist = $this->checkIfDocumentExistForFollowUp($follow_up_id, $document_id);
        $file = request()->file('document_file');
        if($file->isValid()) {
            $employer->documents()->attach([$document_id => [
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
                'external_id' => $follow_up_id
            ]]);
        }
    }

    /*Attach doc to server storage*/
    public function attachFollowUpDocFileToStorage($follow_up_id, $document_id, $ext)
    {
        /*Attach document to server*/
        $follow_up =EmployerClosureFollowUp::query()->find($follow_up_id);
        $employer = $follow_up->employerClosure->closedEmployer;
        $uploaded_doc = $employer->documents()->where('external_id', $follow_up_id)->where('document_id', $document_id)->orderBy('id', 'desc')->first();
        $path = employer_closure_dir() . DIRECTORY_SEPARATOR . $employer->id . DIRECTORY_SEPARATOR . $follow_up->employer_closure_id . DIRECTORY_SEPARATOR . 'follow_ups' . DIRECTORY_SEPARATOR;
        $filename = $uploaded_doc->pivot->id . '.' . $ext;
        $this->makeDirectory($path);
        $this->saveDocumentBasic('document_file',$filename, $path );
    }

    /*Get document doc path file url*/
    public function getFollowUpDocPathFileUrl($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $follow_up =EmployerClosureFollowUp::query()->find($uploaded_doc->external_id);
        $file_path = employer_closure_path() . DIRECTORY_SEPARATOR. $uploaded_doc->employer_id . DIRECTORY_SEPARATOR .  $follow_up->employer_closure_id . DIRECTORY_SEPARATOR . 'follow_ups' . DIRECTORY_SEPARATOR . $doc_pivot_id . '.' .$uploaded_doc->ext; //closure.
        return $file_path;

    }


    /*Get uploaded doc per staff employer by document id*/
    public function getUploadedDoc($external_id, $document_id)
    {
        $uploaded_doc =DB::table('document_employer')->where('external_id', $external_id)->where('document_id', $document_id)->first();
        return $uploaded_doc;
    }




    /**
     *
     * Start: Letter Methods
     */

    /**
     * @param $letter_cv_reference
     * Get Previous letter
     */
    public function getPreviousLetter($letter_cv_reference, $closure_type_id)
    {
        $cv_repo = new CodeValueRepository();
        $cv_id = $cv_repo->findIdByReference($letter_cv_reference);
        $prev_letter = $this->getQueryLetters($letter_cv_reference, $closure_type_id)->orderBy('letters.id', 'desc')->first();
        return $prev_letter;
    }


    public function getQueryLetters($letter_cv_reference, $closure_type_id)
    {
        $cv_repo = new CodeValueRepository();
        $cv_id = $cv_repo->findIdByReference($letter_cv_reference);
        return (new LetterRepository())->queryLettersByReference($cv_id)
            ->join('employer_closures as c', 'c.id', 'letters.resource_id')
            ->where('c.closure_type', $closure_type_id);

    }


    /*Get Letter reference No. attribute*/
    public function getLetterReferenceNo($letter_cv_reference, $closure_type_id)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference,$closure_type_id);
        $prev_reference = (isset($prev_letter)) ? $prev_letter->reference : null;
        $next_folio_no = $this->getLetterFolioNo($letter_cv_reference, $closure_type_id);
        $reference_no = '';
        switch($letter_cv_reference){
            case 'CLIEMPCLOSURE':
                switch($closure_type_id){
                    case 1:
                        /*Temporary*/
                        $default = 'AC.454/468/';
                        $reference_no = $this->getFullReferenceNo($prev_reference, $default, $next_folio_no)  ;
                        break;
                    case 2:
                        /*Permanent*/
                        $default = 'AC.504/531/';
                        $reference_no = $this->getFullReferenceNo($prev_reference, $default, $next_folio_no) ;
                        break;
                }

                break;

            //TODO need to ask on reference no for settlements
            case 'CLIEMPCLOSSETT':
                switch($closure_type_id){
                    case 1:
                        /*Temporary*/
                        $default = 'AC.454/468/';
                        $reference_no = $this->getFullReferenceNo($prev_reference, $default, $next_folio_no) ;
                        break;
                    case 2:
                        /*Permanent*/
                        $default = 'AC.504/531/';
                        $reference_no = $this->getFullReferenceNo($prev_reference, $default, $next_folio_no);
                        break;
                }

                break;
        }

        return $reference_no;
    }


    /*Prefix of letter reference no*/
    public function getFullReferenceNo($prev_reference, $default_ref, $next_folio_no)
    {

        if(isset($prev_reference)){
            $ref_no =  remove_after_last_this_character('/',$prev_reference) . '/';
        }else{
            $ref_no = $default_ref;
        }
        return $ref_no  . $next_folio_no;
    }

    /*Letter folio no*/
    public function getLetterFolioNo($letter_cv_reference, $closure_type_id)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference,$closure_type_id);
        if(isset($prev_letter)){
            $resources =  json_decode($prev_letter->resources, true);
            $next_folio_no =  $resources[0]['value'] + 1;
        }else{
            $next_folio_no = 1;
        }
        return$next_folio_no;
    }



    /**
     * Checker Tasks
     */

    /*Initialize Checker for all*/
    public function initializeCheckerForAll($stage_cv_ref){
        $closures = $this->queryClosures()->get();
        switch($stage_cv_ref){
            case 'EMPDERREOP'://Temp de-registered reopen alert
                $limit_date = standard_date_format(Carbon::now()->subYears(3));
                $closures = $this->queryClosures()->where('employer_closures.closure_type', 1)->where('employer_closures.wf_done', 1)->where('employer_closures.close_date', '>', $limit_date)->get();
                break;

        }

        /*Closures*/
        foreach($closures as $closure){
            $employer_closure = $this->find($closure->closure_id);
            $this->initializeChecker($employer_closure, $stage_cv_ref);
        }
    }

    /*Initialize Checker*/
    public function initializeChecker(Model $employerClosure, $stage_cv_ref = null)
    {

        $employerClosure->refresh();
        $codeValue = new CodeValueRepository();
        $stage =($stage_cv_ref) ? $codeValue->findIdByReference($stage_cv_ref) : null;
        $stage =($stage) ? $stage : $employerClosure->staging_cv_id;

        if($stage){
            $stage_cv = $codeValue->find($stage);
            $stage_cv_ref = $stage_cv->reference;
            $checkerRepo = new CheckerRepository();
            $userRepo = new UserRepository();

            //--> Allocate Employer Inspection Task User
            $user = null;
            $user = $this->retrieveAllocatedUserForChecker($employerClosure, $stage_cv_ref);
            $department = $userRepo->getUserDepartment($user);

            $notify = 0;
            $comments = '{"submittedDate":"' . now_date() . '", "currentStage":"Track Temporary De-registered for Re-Open", "currentStageCvId":' . $stage . ', "department":"' . $department . '"}';
            $closeChecker = 0;
            $priority = 0;
            $process_checker = 0;

            /*Per this stage check if need to close or create*/
            switch($stage_cv_ref){
                case 'EMPDERREOP'://Temp reopen alert
                    $check_if_on_reopen_alert_pending = $this->getTemporaryClosedForFollowUpStatusForDt()->where('employer_closures.id', $employerClosure->id)->count();
                    $closeChecker = ($check_if_on_reopen_alert_pending > 0) ? 0 : 1;
                    $process_checker = 1;
                    break;

                default:

                    break;
            }

            /*check this closure need to process checker*/
            if($process_checker){
                if ($closeChecker) {
                    //Close recent checker entry
                    $input = [
                        'resource_id' => $employerClosure->id,
                        'checker_category_cv_id' => $codeValue->CHCEMPDEREG(),
                    ];
                    $checkerRepo->closeRecently($input);
                } else {
                    //Create checker entry
                    $input = [
                        'comments' => $comments,
                        'user_id' => $user,
                        'checker_category_cv_id' => $codeValue->CHCEMPDEREG(),
                        'priority' => $priority,
                        'notify' => $notify,
                    ];
                    $checkerRepo->create($employerClosure, $input);
                }
            }
        }
        return true;
    }


    public function retrieveAllocatedUserForChecker(Model $employerClosure, $stage_cv_ref){
        $checker_repo = new CheckerRepository();
        $code_value_repo = new CodeValueRepository();
        $checker_category_cv_id = $code_value_repo->findIdByReference('CHCEMPDEREG');
        switch($stage_cv_ref){
            case 'EMPDERREOP'://Temp reopen alert
                $user_cv = $code_value_repo->findByReference('CDAEMPREOPUS');
                $users = $user_cv->users()->orderBy('users.id')->pluck('users.id');
                $users = (isset($users[0])) ? $users : [102];
                break;


            default:
                $users = [$employerClosure->user_id];
                break;
        }

        /*Get Next User with less tasks*/
        $task_counts_arr = [];
        foreach ($users as $user){
            $task_count =$checker_repo->query()->where('status', 0)->where('checker_category_cv_id',$checker_category_cv_id)->count();
            $array = [$task_count];
            $task_counts_arr = array_merge_recursive($task_counts_arr, $array);
        }

        /*get next user with min count*/
        if(count($task_counts_arr)){

            $user_id_index_min = array_keys($task_counts_arr, min($task_counts_arr))[0];
            $user_id = $users[$user_id_index_min];
        }


        return $user_id;


    }

    /**
     * @return null
     * Retrieve user with less count for next assignment
     */
    public function retrieveAllocatedUserForCheckerAllStages(){
        $cv_repo = new CodeValueRepository();
        $user_cv = $cv_repo->findByReference('PAYDALEUS');
        $users = $user_cv->users()->orderBy('users.id')->pluck('users.id');
        /*Get Next User with less tasks*/
        $task_counts_arr = [];
        $user_id = null;

        foreach ($users as $user){
            $task_count =$this->query()->where('status', 0)->where('user_id', $user)->count();
            $array = [$task_count];
            $task_counts_arr = array_merge_recursive($task_counts_arr, $array);
        }

        /*get next user with min count*/
        if(count($task_counts_arr)){

            $user_id_index_min = array_keys($task_counts_arr, min($task_counts_arr))[0];
            $user_id = $users[$user_id_index_min];
        }


        return $user_id;
    }

    /*end checker*/
}