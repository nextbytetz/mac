<?php

namespace App\Repositories\Backend\Operation\Compliance;

use App\Exceptions\WorkflowException;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Services\Storage\DataFormat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\InspectionTaskRepository;
use App\DataTables\Report\Compliance\EmployerInspectionProfilesDataTable;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\VarDumper\Cloner\Data;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class InspectionTaskRepository
 * @author Erick Chrysostom
 * @package App\Repositories\Backend\Operation\Compliance\Inspection
 */
class EmployerInspectionProfilesReportRepository extends BaseRepository
{
    const MODEL = InspectionTask::class;

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */


    public function __construct()
    {
     $this->inspection = new EmployerInspectionTaskRepository();
 }

 public function findOrThrowException($id)
 {
    $inspection_task = $this->query()->find($id);

    if (!is_null($inspection_task)) {
        return $inspection_task;
    }
    throw new GeneralException(trans('exceptions.backend.compliance.inspection_profile_report_not_found'));
}

public function returnInspectionDetails()
{

    // DB::table("inspection_assessments")
   // $report_inspected = $this->query()
   $employer = !empty(request()->input("employer")) ? "where t.employer_id =  '".request()->input("employer")."'" : '';
   $allocated_staff = !empty(request()->input("allocated_staff")) ? "where t.allocated =  '".request()->input("allocated_staff")."'" : '';
   $region = !empty(request()->input("region")) ? "where e.region_id =  '".request()->input("region")."'" : '';
   $stage =  !empty(request()->input("stage")) ? "where a.stage  = '".request()->input("stage")."'" : '';     
   

   $report_inspected = DB::select(DB::raw("select a.task_type,a.employer,a.allocated_staff,a.attended,a.stage,a.region,a.visit_date,a.review_start_date,a.review_end_date,outstanding_contribution, total_outstanding_amount, outstanding_interest,overpaid_contrib, paid_after as outstanding_contribution_paid,outstanding_interest_paid
    from (select v.name as task_type,e.name as employer, u.firstname as allocated_staff,
    (case WHEN t.attended = 1 then 'Yes'  ELSE 'No' END) as attended, s.name as stage, r.name as region, t.visit_date, a.contrib_month, 
    t.review_start_date, t.review_end_date, x.x_outstanding_contribution as outstanding_contribution,
    x.x_outstanding_interest as outstanding_interest,a.contrib_amount contrib_by_employer, 
    x.x_overpaid_contribution  as overpaid_contrib, 
    x.x_total_outstanding_amount as total_outstanding_amount,
    ir.ir_paid_after as paid_after,
    (ir_interest_before + ir_interest_after) as outstanding_interest_paid
    from employer_inspection_task t
    join employers e on e.id = t.employer_id
    join (select employer_id,sum(underpaid) as x_outstanding_contribution,sum(interest_on_unpaid) as x_outstanding_interest,sum(overpaid) as x_overpaid_contribution,sum(accumulative_to_be_paid) as x_total_outstanding_amount from main.inspection_assessment_template group by employer_id) as x on e.id = x.employer_id
    join (select employer_id,sum(paid_after) as ir_paid_after,sum(interest_before) as ir_interest_before,sum(interest_after) as ir_interest_after  from main.inspection_remittance group by employer_id) as ir on e.id = ir.employer_id
    left join regions r on r.id = e.region_id
    join inspection_tasks t2 on t.inspection_task_id = t2.id
    left join inspection_task_user tu on t.id = tu.inspection_task_id
    left join users u on u.id = t.allocated
    left join inspection_assessments a on t.id = a.employer_inspection_task_id
    left join code_values v on t2.inspection_type_cv_id = v.id
    left join code_values s on t.staging_cv_id = s.id  ".$employer." ".$region." ".$allocated_staff." ".$stage.") a

    group by a.task_type,a.employer,a.allocated_staff,a.attended,a.stage,a.region,a.visit_date,a.review_start_date,a.review_end_date,a.overpaid_contrib,outstanding_contribution,total_outstanding_amount,outstanding_interest,paid_after,outstanding_interest_paid"));
   

   return $report_inspected;
}

public function allocatedStaffDetail($q, $page)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = trim(preg_replace('/\s+/', '', $q));
        //$name = $q;
        // $this->query()
        $data['items'] =  DB::table('main.users')
        ->select([
            'id',
             DB::raw("concat_ws(' ', users.firstname, coalesce(users.middlename, ''), users.lastname) as allocated_staff"),
        ])
        ->whereRaw("regexp_replace(cast(concat_ws(' ', users.firstname, coalesce(users.middlename, ''), users.lastname) as text), '\s+', '', 'g')  ~* ? ", [$name])
        ->limit($resultCount)
        ->offset($offset)
        ->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    public function inspectionStage($b, $page)
    {
        $resultCount = 15;
        $offset = ($page - 1) * $resultCount;
        $name = trim(preg_replace('/\s+/', '', $b));
        $data['items'] =  DB::table('main.inspection_assessments')
        ->select([
            'inspection_assessments.id',
             DB::raw("s.name as stage")
        ])
        ->leftJoin(DB::raw("employer_inspection_task t"), 't.id', '=', 'inspection_assessments.employer_inspection_task_id')
        ->leftJoin(DB::raw("code_values s"), 't.staging_cv_id', '=', 's.id') 
        ->whereRaw("regexp_replace('cast(code_values.name as text)', '\s+', '', 'g')  ~* ? ", [$name])
        ->limit($resultCount)
        ->offset($offset)
        ->get()->toArray();
        // dd($data);
        $data['total_count'] = count($data['items']);
        return response()->json($data);

    }

public function getInspectionReportSelectQuery()
{
    $codeValue = new CodeValueRepository();
    $report_inspection = "({$codeValue->NSPCRFI()}, {$codeValue->NSOPRFLI()})";
    return [
        DB::raw("employer_inspection_task.id"),
        DB::raw("a.name as inspection_type"),
        DB::raw("employers.name as employer_name"),
        DB::raw("concat_ws(' ', e.firstname, e.lastname) inspector"),
        DB::raw("CASE employer_inspection_task.attended WHEN '1' THEN 'Yes' ELSE 'No' END AS attended"),
        DB::raw("b.name stage"),
        DB::raw("regions.name as regions"),
        DB::raw("districts.name as districts"),
        DB::raw("employer_inspection_task.visit_date"),
        DB::raw("employer_inspection_task.review_start_date"),
        DB::raw("employer_inspection_task.review_end_date"),

    ];
}

public function getInspectionReportQuery()
{

    $inspection = (new InspectionTaskRepository())->query()
    ->leftJoin("employer_inspection_task", "employer_inspection_task.inspection_task_id", "=", "inspection_tasks.id")
    ->leftJoin("employers", "employer_inspection_task.employer_id", "=", "employers.id")
    ->leftJoin(DB::raw('code_values a'), "inspection_tasks.inspection_type_cv_id", "=", "a.id")
    ->leftJoin(DB::raw('code_values b'), "employer_inspection_task.staging_cv_id", "=", "b.id")
    ->leftJoin(DB::raw('users as e'), "e.id", "=", "employer_inspection_task.allocated")
    ->leftJoin("regions", "employers.region_id", "=", "regions.id")
    ->leftJoin("districts", "employers.district_id", "=", "districts.id");
    return $inspection;
}
public function returnAllInspectionType()
{
  $inspection_type = DB::table('main.inspection_task')
  ->whereNotNull('name')->pluck('inspection_task_id')->all();
  return array_unique($inspection_type);
}
public function returnAllStaffAlloacated()
{
    $inspector = DB::table('main.inspection_task')
    ->whereNotNull('name')->pluck('inspection_task_id')->all();
    return array_unique($inspector); 
}

}