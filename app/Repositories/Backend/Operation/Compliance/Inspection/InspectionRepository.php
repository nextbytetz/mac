<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Events\NewWorkflow;
use App\Exceptions\WorkflowException;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\Compliance\Inspection\Inspection;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionTaskRepository;

/**
 * Class InspectionRepository
 * @package App\Repositories\Backend\Operation\Compliance\Inspection
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class InspectionRepository extends BaseRepository
{
    const MODEL = Inspection::class;

    public $inspection_task;

    public $employer_inspection_task;

    public function __construct()
    {
        parent::__construct();
        $inspection_task = new InspectionTaskRepository();
        $employer_inspection_task = new EmployerInspectionTaskRepository();
        $this->inspection_task = $inspection_task;
        $this->employer_inspection_task = $employer_inspection_task;
    }

    public function retrieveInspectionAllocatedUser(Model $inspection)
    {
        if (!$inspection->allocated) {
            $user = (new UserRepository())->getAllocatedForInspection($inspection);
            $inspection->allocated = $user;
            $inspection->save();
        } else {
            $user = $inspection->allocated;
        }
        return $user;
    }

    /**
     * @param Model $incident
     */
    public function updateInspectionStageId(Model $inspection)
    {
        $inspection_stage_id = $inspection->stages()->orderByDesc("inspection_stages.id")->limit(1)->first()->pivot->id;
        $inspection->inspection_stage_id = $inspection_stage_id;
        $inspection->save();
    }

    public function retrievePreviousStage(Model $inspection)
    {
        $return = [];
        $query = $inspection->stages()->orderBy("inspection_stages.id", "desc");
        if ($query->count()) {
            $return = json_decode($query->first()->pivot->comments);
        }
        return $return;
    }

    /**
     * @param Model $inspection
     * @param int $caller , if method called by System 0, 1 called by user.
     */
    public function initialize(Model $inspection, $caller = 0)
    {
        $inspection->refresh();
        $stage = $inspection->staging_cv_id;
        $codeValue = new CodeValueRepository();
        $checkerRepo = new CheckerRepository();
        $userRepo = new UserRepository();

        //--> Allocate Inspection User
        $user = $this->retrieveInspectionAllocatedUser($inspection);
        $prevStage = $this->retrievePreviousStage($inspection);
        $prevComment = (($prevStage) ? $prevStage->{'currentStage'} : "Inspection Registration");
        $comments = NULL;
        $process = 1;
        $closeChecker = 0;
        $priority = 0;
        $notify = 0;


        switch ($stage) {
            case $codeValue->IOPISPCRTD():
                $priority = 1;
                //Inspection Created
                if ($user) {
                    //--> Checker Inbox & Tasks
                    $comments = '{"previousStage":"Inspection Registration", "submittedDate":"' . now_date() . '", "currentStage":"Inspection Created", "currentStageCvId":' . $stage . ', "nextStage":"Initiate for Inspection Plan Workflow", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "inspectionReview":""}';
                }
                break;
            case $codeValue->IOPOISPPAW():
                //On Inspection Plan Approval Workflow
                $closeChecker = 1;
                if ($user) {
                    //--> Checker Inbox & Tasks
                    $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Inspection Plan Approval Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Approved or Declined Inspection Plan", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "inspectionReview":""}';
                }
                break;
            case $codeValue->IOPAPPRVDIPLN():
                //Approved Inspection Plan, Planning Visit
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Progress, Inspection Plan Approved", "currentStageCvId":' . $stage . ', "nextStage":"Plan Visit, Issuing Inspection Notice Letter", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->IOPPLNVIDNLTR():
                //Issuing Inspection Response Letter
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Progress, Planning Visit, Issuing Demand Notice Letter", "currentStageCvId":' . $stage . ', "nextStage":"Completing Inspection", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->IRJDCLNDIPLN():
                //Declined Inspection Plan
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Declined Inspection Plan", "currentStageCvId":' . $stage . ', "nextStage":"Closed Inspection", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->IOPPNDGIASCDL():
                //Pending Inspection Assessment Schedule
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Pending Inspection Assessment Schedule", "currentStageCvId":' . $stage . ', "nextStage":"Updating Inspection Assessment Schedule For Each Employer", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->IOPCOMPLTDIPCN():
                //Completed Inspection
                if ($user) {
                    $closeChecker = 1;
                    //--> Checker Inbox & Tasks
                    $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Completed Inspection", "currentStageCvId":' . $codeValue->IOPCOMPLTDIPCN() . ', "nextStage":"Closed Inspection", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "inspectionReview":""}';
                }
                break;
            case $codeValue->ICNCANCLDISPPLN():
                //Cancelled Inspection Plan
                if ($user) {
                    $closeChecker = 1;
                    //--> Checker Inbox & Tasks
                    $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Cancelled Inspection Plan", "currentStageCvId":' . $stage . ', "nextStage":"Cancelled Inspection", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "inspectionReview":""}';
                }
                break;
        }
        if ($process And $user) {
            if (!empty($comments)) {
                //Save comment on inspection stages
                $inspection->stages()->attach($stage,
                    [
                        'comments' => $comments
                    ]);
                $this->updateInspectionStageId($inspection);
            }
            if ($closeChecker) {
                //Close recent checker entry
                $input = [
                    'resource_id' => $inspection->id,
                    'checker_category_cv_id' => $codeValue->CCAEMPLYINSPCTN(),
                ];
                $checkerRepo->closeRecently($input);
            } else {
                //Create checker entry
                $input = [
                    'comments' => $comments,
                    'user_id' => $user,
                    'checker_category_cv_id' => $codeValue->CCAEMPLYINSPCTN(),
                    'priority' => $priority,
                    'notify' => $notify,
                ];
                $checkerRepo->create($inspection, $input);
            }
        }
    }

    //find or throw exception for inspection
    public function findOrThrowException($id)
    {
        $inspection = $this->query()->find($id);

        if (!is_null($inspection)) {
            return $inspection;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.inspection_not_found'));
    }

    public function listQuery()
    {
        $attributes = [
            DB::raw("a.name inspection_type"),
            DB::raw("b.name inspection_trigger"),
            DB::raw("c.name inspection_trigger_category"),
            DB::raw("d.name stage"),
            DB::raw("(EXTRACT(days FROM (coalesce(end_date, now()) - start_date)) / 7)::int duration"),
            DB::raw("to_char(start_date, 'Day,DD Mon YYYY') start_date_formatted"),
            DB::raw("iscancelled"),
            DB::raw("inspections.id"),
        ];
        $query = $this->query()
            ->select($attributes)
            ->join(DB::raw('code_values a'), "a.id", "=", "inspections.inspection_type_cv_id")
            ->join(DB::raw('code_values b'), "b.id", "=", "inspections.inspection_trigger_cv_id")
            ->leftJoin(DB::raw('code_values c'), "c.id", "=", "inspections.trigger_category_cv_id")
            ->join(DB::raw('code_values d'), "d.id", "=", "inspections.staging_cv_id");
        return $query;
    }

    /**
     * @param $columns
     * @param $current determine whether inspection to be retrieved are current active (1) or all inspections (0)
     * @return mixed
     */
    public function getInspectionForDataTable($current = 0)
    {
        switch ($current) {
            case 1:
                //Ongoing Inspection
                $return = $this->listQuery()->where("complete_status", 1);
                break;
            case 0:
                //All Inspection
                $return = $this->listQuery();
                break;
            default:
                $return = $this->listQuery();
        }
        return $return->orderBy("start_date", "asc");
    }

    /**
     * @param array $input
     * @param int $caller if called by system 0, called by user 1.
     * @return mixed
     */
    public function store(array $input, $caller = 1)
    {
        return DB::transaction(function () use ($input, $caller) {
            $user = access()->id();
            $codeValueRepo = new CodeValueRepository();
            $wfModuleRepo = new WfModuleRepository();
            $codeValue = $codeValueRepo->query()->where("reference", $input['inspection_type'])->first();
            switch ($input['inspection_type']) {
                case "ITROUTINE":
                    //Routine...
                    $sequence = $this->getNextInsprNoSysdef();
                    $filename = "INSP/R/" . financial_year() . "/" . $sequence;
                    $input['inspection_trigger_cv_id'] = $codeValueRepo->ITRGPLNDINSPCTN(); //Planned Inspection...
                    break;
                case "ITSPECIAL":
                    //Special...
                    $sequence = $this->getNextInspsNoSysdef();
                    $filename = "INSP/S/" . financial_year() . "/" . $sequence;
                    $input['inspection_trigger_cv_id'] = $codeValueRepo->ITRGPLNDINSPCTN(); //Planned Inspection...
                    break;
                default:
                    //Blitz
                    $sequence = $this->getNextInspbNoSysdef();
                    $filename = "INSP/B/" . financial_year() . "/" . $sequence;
                    $input['inspection_trigger_cv_id'] = $codeValueRepo->ITRGSPCEVNT(); //Specific Event...
                    break;
            }
            $group = $wfModuleRepo->employerInspectionPlanType()['group'];
            $type = $wfModuleRepo->employerInspectionPlanType()['type'];
            $module = $wfModuleRepo->getModule(['wf_module_group_id' => $group, 'type' => $type]);
            $data = [
                'inspection_type_cv_id' => $codeValue->id,
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'comments' => $input['comments'],
                'user_id' => $user,
                'filename' => $filename,
                'trigger_category_cv_id' => (isset($input['trigger_category'])) ? $input['trigger_category'] : NULL,
                'inspection_trigger_cv_id' => $input['inspection_trigger_cv_id'],
                'staging_cv_id' => $codeValueRepo->IOPISPCRTD(), //Inspection Created...
                'wf_module_id' => $module,
            ];
            if ($caller) {
                $data['allocated'] = $user;
            }
            $inspection = $this->query()->create($data);
            $this->initialize($inspection);
            return $inspection;
        });
    }

    public function update(Model $inspection, array $input)
    {
        return DB::transaction(function () use ($input, $inspection) {
            $user = access()->id();
            $codeValueRepo = new CodeValueRepository();
            $wfModuleRepo = new WfModuleRepository();
            $codeValue = $codeValueRepo->query()->where("reference", $input['inspection_type'])->first();
            switch ($codeValue->id) {
                case $codeValueRepo->ITROUTINE():
                case $codeValueRepo->ITSPECIAL():
                    //Routine...
                    //Special...
                    $input['inspection_trigger_cv_id'] = $codeValueRepo->ITRGPLNDINSPCTN(); //Planned Inspection...
                    break;
                default:
                    $input['inspection_trigger_cv_id'] = $codeValueRepo->ITRGSPCEVNT(); //Specific Event...
                    break;
            }
            $data = [
                'inspection_type_cv_id' => $codeValue->id,
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'comments' => $input['comments'],
                'user_id' => $user,
                'trigger_category_cv_id' => (isset($input['trigger_category'])) ? $input['trigger_category'] : NULL,
                'inspection_trigger_cv_id' => $input['inspection_trigger_cv_id'],
            ];
            $inspection->update($data);
            /*$staffs = [];
            $current = $input["staffs"];
            foreach ($current as $perm) {
                if (is_numeric($perm)) {
                    array_push($staffs, $perm);
                }
            }
            $inspection->users()->sync([]);
            $inspection->attachUsers($staffs);*/
            return true;
        });
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function initiateWorkflow(array $input)
    {
        $return = DB::transaction(function () use ($input) {
            //Check if has access to initiate this workflow at level 1
            access()->hasWorkflowModuleDefinition($input['group'], $input['type'], 1);

            $wfModule = new WfModuleRepository();
            $moduleRepo = new WfModuleRepository();

            $group = $input['group'];
            $resource = $input['resource'];
            $type = $input['type'];
            $comments = $input['comments'];

            $inspection = $this->find($resource);

            $module = $wfModule->getModule(['wf_module_group_id' => $group, 'type' => $type]);

            switch ($module) {
                case $moduleRepo->employerInspectionPlanModule()[0]:
                case $moduleRepo->employerInspectionPlanModule()[1]:
                    //check if initiated by inspection master
                    access()->assignedForInspection();
                    //check if inspection tasks has been created...
                    if (!$inspection->inspectionTasks()->count()) {
                        throw new GeneralException("Action cancelled, Can't initialize Workflow, No task(s) defined!");
                    }
                    break;
            }

            //Initialize Workflow
            event(new NewWorkflow(['wf_module_group_id' => $group, 'resource_id' => $inspection->id, 'type' => $type], [], ['comments' => $comments]));

            //Update Notification Stage
            $this->updateWorkflowStage($inspection, $group, $type);

            return $input;
        });
        return $return;
    }

    /**
     * @param Model $inspection
     * @param $group
     * @param $type
     */
    public function updateWorkflowStage(Model $inspection, $group, $type)
    {
        $wfModule = new WfModuleRepository();
        $codeValue = new CodeValueRepository();
        $updated = false;
        switch (true) {
            case ($group == $wfModule->employerInspectionPlanType()['group'] And $type == $wfModule->employerInspectionPlanType()['type']):
                //Inspection Plan Approval
                $inspection->staging_cv_id = $codeValue->IOPOISPPAW(); //On Inspection Plan Approval Workflow
                $inspection->isinitiated = 1;
                $inspection->save();
                $updated = true;
                break;
        }
        if ($updated) {
            //Initialize Inspection
            $this->initialize($inspection);
        }
    }

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @return mixed
     */
    public function completeInspectionApproval($level, $resource_id, $module)
    {
        $return = DB::transaction(function () use ($level, $resource_id, $module) {
            $codeValueRepo = new CodeValueRepository();
            $inspection = $this->find($resource_id);

            //check whether declined or approve and update the stage respectively.
            $check = (new WfTrackRepository())->checkIfModuleDeclined($inspection->id, $module);

            if ($check) {
                //Declined
                //update stage for inspection
                $inspection->staging_cv_id = $codeValueRepo->IRJDCLNDIPLN(); //Declined Inspection Plan
                $inspection->progressive_stage = 3; //Declined Inspection Plan
                //update stage in all employer inspection tasks
                $inspection->employerTasks()->update([
                    "staging_cv_id" => $codeValueRepo->EITDCLNEMPLYITS(), //Declined Employer Inspection Task
                ]);
            } else {
                //Approved
                //update stage in all employer inspection tasks
                $inspection->employerTasks()->update([
                    "progressive_stage" => 2, //Tasks Assigned to Staff
                    "staging_cv_id" => $codeValueRepo->EITPPLVDAT(), //Planing Visit Date
                ]);
                //initialize all employer inspection tasks
                $return = (new EmployerInspectionTaskRepository())->initializeAll($inspection);
                $inspection->wf_done = 1;
                $inspection->wf_done_date = Carbon::now();
                $inspection->progressive_stage = 2;

                //update stage for inspection
                $inspection->staging_cv_id = $codeValueRepo->IOPAPPRVDIPLN(); //Approved Inspection Plan, Planning Visit

            }

            $inspection->save();
            $this->initialize($inspection);

            return true;
        });

        return $return;
    }

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @throws WorkflowException
     */
    public function checkIfInspectorsHasBeenAssigned($level, $resource_id, $module)
    {
        $moduleRepo = new WfModuleRepository();
        if ((int)$level == $moduleRepo->employerInspectionPlanAssignInspectorLevel($module)) {
            $inspection = $this->find($resource_id);
            //check if inspection has inspectors
            $count = $inspection->users()->count();
            if (!$count) {
                throw new WorkflowException("Action cancelled, Please assign inspectors for this inspection.");
            }
        }
    }

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @throws WorkflowException
     */
    public function checkIfAllTasksAllocated($level, $resource_id, $module)
    {
        $moduleRepo = new WfModuleRepository();
        if ((int)$level == $moduleRepo->employerInspectionPlanAssignInspectorLevel($module)) {
            //$inspection = $this->find($resource_id);
            $count = $this->query()->whereHas("inspectionTasks", function ($query) {
                $query->whereHas("employers", function ($query) {
                    $query->whereNull("allocated");
                });
            })->where("inspections.id", $resource_id)->count();
            if ($count) {
                throw new WorkflowException("Action cancelled, Please confirm allocation of all employer tasks");
            }
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getResourceAllocationQuery($id)
    {
        $allocation = $this->query()
            ->leftJoin("inspection_tasks", "inspection_tasks.inspection_id", "=", "inspections.id")
            ->leftJoin("employer_inspection_task", "employer_inspection_task.inspection_task_id", "=", "inspection_tasks.id")
            ->leftJoin("employers", "employer_inspection_task.employer_id", "=", "employers.id")
            ->leftJoin(DB::raw('code_values a'), "inspection_tasks.inspection_type_cv_id", "=", "a.id")
            ->leftJoin(DB::raw('code_values b'), "employer_inspection_task.staging_cv_id", "=", "b.id")
            ->leftJoin(DB::raw('users'), "users.id", "=", "employer_inspection_task.allocated")
            ->leftJoin("districts", "employers.district_id", "=", "districts.id")
            ->leftJoin("regions", "employers.region_id", "=", "regions.id")
/*            ->join("incident_types", "notification_reports.incident_type_id", "=", "incident_types.id")
            ->leftJoin("districts", "employers.district_id", "=", "districts.id")
            ->leftJoin("regions", "employers.region_id", "=", "regions.id")
            ->leftJoin("notification_investigators", function ($join) {
                $join->on("notification_reports.id", "=", "notification_investigators.notification_report_id");
                $join->where("notification_investigators.iscomplete", "=", "0");
            })
            ->leftJoin(DB::raw('users b'), "notification_reports.allocated", "=", "b.id")
            ->join("code_values", "notification_reports.notification_staging_cv_id", "=", "code_values.id")*/
            ->where("inspections.id", $id);
        return $allocation;
    }

    /**
     * @return array
     */
    public function getResourceAllocationSelectQuery()
    {
        return [
            DB::raw("employer_inspection_task.id as id"),
            DB::raw("a.name as task_type"),
            DB::raw("employers.name employer"),
            DB::raw("regions.name region"),
            DB::raw("districts.name district"),
            DB::raw("concat_ws(' ', users.firstname, users.lastname) allocated_user"),
            DB::raw("case when employer_inspection_task.attended = 1 then 'Yes' else 'No' end user_attended"),
            DB::raw("b.name as stage"),
            DB::raw("employer_inspection_task.created_at"),
        ];
    }

    /**
     * @return array
     */
    public function getResourceAllocationSelectInspectionTypeGroupQuery()
    {
        return [
            DB::raw("inspection_tasks.inspection_type_cv_id"),
            DB::raw("a.name"),
            DB::raw('COUNT(employer_inspection_task.id) as employer_tasks'),
        ];
    }

    /**
     * @param $category
     * @return array
     */
    public function getResourceAllocationSelectStaffGroupQuery()
    {
        return [
            DB::raw("employer_inspection_task.allocated"),
            DB::raw("concat_ws(' ', users.firstname, users.lastname) staff"),
            DB::raw('COUNT(employer_inspection_task.id) as employer_tasks'),
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getResourceAllocationQueryFilter($id)
    {
        $codeValue = new CodeValueRepository();
        $allocation = $this->getResourceAllocationQuery($id);
        $employer = request()->input("employer");
        if ($employer) {
            $allocation->where("employer_inspection_task.employer_id", "=", $employer);
        }
        $region = request()->input("region");
        if ($region) {
            $allocation->where("regions.id", "=", $region);
        }
        $district = request()->input("district");
        if ($district) {
            $allocation->where("districts.id", "=", $district);
        }
        $stage = request()->input("stage");
        if ($stage) {
            $allocation->where("employer_inspection_task.staging_cv_id", "=", $stage);
        }
        $status = request()->input('status');
        //check allocation status
        switch ($status) {
            case 1:
                //Allocated
                $allocation->whereNotNull("users.id");
                break;
            case 2:
                //Unallocated
                $allocation->whereNull("users.id");
                break;
            case 3:
                //Allocated to User
                $user_id = request()->input('user_id');
                $allocation->where("users.id", $user_id);
                break;
            case 4:
                //Attended (Allocated)
                $allocation->whereNotNull("users.id")->where("employer_inspection_task.attended", 1);
                break;
            case 5:
                //Unattended (Allocated)
                $allocation->whereNotNull("users.id")->where("employer_inspection_task.attended", 0);
                break;
        }
        return $allocation;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getResourceAllocationDatatable($id)
    {
        $allocation = $this->getResourceAllocationQueryFilter($id)->select($this->getResourceAllocationSelectQuery());
        $datatables = app('datatables')
            ->of($allocation);
        return $datatables;
    }

    /**
     * @return mixed
     */
    public function getResourceAllocationInspectionTypeGroup($id)
    {
        $allocation = $this->getResourceAllocationQueryFilter($id)
            ->select($this->getResourceAllocationSelectInspectionTypeGroupQuery())
            ->groupBy('inspection_tasks.inspection_type_cv_id', 'a.name')
            ->orderBy(DB::raw('COUNT(employer_inspection_task.id)'), "desc");
        return $allocation;
    }

    public function getResourceAllocationStaffGroup($id)
    {
        $allocation = $this->getResourceAllocationQueryFilter($id)
                        ->select($this->getResourceAllocationSelectStaffGroupQuery())
                        ->groupBy("employer_inspection_task.allocated", DB::raw("concat_ws(' ', users.firstname, users.lastname)"))->orderBy(DB::raw('COUNT(employer_inspection_task.id)'), "desc");
        return $allocation;
    }

    /**
     * @return array
     */
    public function getResourceStatus($id)
    {
        //suffix _eit means employer inspection task
        $output = [];
        $allocation = $this->getResourceAllocationQuery($id);
        $output['available_eit'] = with(clone $allocation)->count();
        $output['allocated_eit'] = with(clone $allocation)->whereNotNull("users.id")->count();
        $output['unallocated_eit'] = with(clone $allocation)->whereNull("users.id")->count();
        $output['attended_eit'] = with(clone $allocation)->whereNotNull("users.id")->where("employer_inspection_task.attended", 1)->count();
        $output['unattended_eit'] = with(clone $allocation)->whereNotNull("users.id")->where("employer_inspection_task.attended", 0)->count();
        return $output;
    }

    public function assignAllocation(array $input)
    {

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getInspectorForDatatable($id)
    {
        return $this->query()->select([
            DB::raw("concat_ws(' ', users.firstname, users.lastname) fullname"),
            "users.id",
            DB::raw("users.thirdparty_id useridentity"),
        ])
            ->join("inspection_user", "inspection_user.inspection_id", "=", "inspections.id")
            ->join("users", "inspection_user.user_id", "=", "users.id")
            ->where("inspections.id", $id);
    }

    /**
     * @param Model $inspection
     * @param array $input
     * @return mixed
     */
    public function postInspectors(Model $inspection, array $input)
    {
        return DB::transaction(function () use ($inspection, $input) {
            if (isset($input['inspectors'])) {
                $inspectors = $input['inspectors'];
                $arr = [];
                foreach ($inspectors as $inspector) {
                    if (is_numeric($inspector)) {
                        $arr[] = $inspector;
                    }
                }
                //$inspection->attachUsers($arr);
                $inspection->users()->syncWithoutDetaching($arr);
            }
            return true;
        });
    }

    /**
     * @param Model $inspection
     * @param array $input
     * @return mixed
     * @throws WorkflowException
     */
    public function removeInspectors(Model $inspection, array $input)
    {
        //check if has any assigned tasks
        $count = $this->query()->whereHas("inspectionTasks", function ($query) use ($input, $inspection) {
            $query->whereHas("allocations", function ($query) use ($input, $inspection) {
                $query->where("allocated", $input['id']);
            })->where("inspection_id", $inspection->id);
        })->count();
        if ($count) {
            throw new WorkflowException("Action Cancelled, User has allocated Employer Tasks.");
        }
        return DB::transaction(function () use ($inspection, $input) {
            if (isset($input['id'])) {
                $inspectors = $input['id'];
                $arr = [];
                foreach ($inspectors as $inspector) {
                    if (is_numeric($inspector)) {
                        $arr[] = $inspector;
                    }
                }
                $inspection->users()->detach($arr);
            }
            return true;
        });
    }

/*    private function updateInsprNo(Model $inspection)
    {
        $next_insprno = $this->getNextInsprNoSysdef();
        $inspection->filename = $next_insprno;
        $inspection->save();
    }

    private function updateInspsNo(Model $inspection)
    {
        $next_inspsno = $this->getNextInspsNoSysdef();
        $inspection->filename = $next_inspsno;
        $inspection->save();
    }

    private function updateInspbNo(Model $inspection)
    {
        $next_inspbno = $this->getNextInspbNoSysdef();
        $inspection->filename = $next_inspbno;
        $inspection->save();
    }*/

    public function getNextInsprNoSysdef()
    {
        $sysdef = sysdefs()->data();
        $nextInsprNo = $sysdef->insprno;
        $sysdef->insprno = $nextInsprNo + 1;
        $sysdef->save();
        return $nextInsprNo;
    }

    public function getNextInspsNoSysdef()
    {
        $sysdef = sysdefs()->data();
        $nextInspsNo = $sysdef->inspsno;
        $sysdef->inspsno = $nextInspsNo + 1;
        $sysdef->save();
        return $nextInspsNo;
    }

    public function getNextInspbNoSysdef()
    {
        $sysdef = sysdefs()->data();
        $nextInspbNo = $sysdef->inspbno;
        $sysdef->inspbno = $nextInspbNo + 1;
        $sysdef->save();
        return $nextInspbNo;
    }

    /**
     * @param Model $inspection
     * @return mixed
     */
    public function complete(Model $inspection)
    {
        return DB::transaction(function () use ($inspection) {
            $input = [
                'complete_status' => 1,
                'complete_user' => access()->id(),
                'complete_date' => Carbon::now(),
            ];
            $inspection->update($input);
        });
    }

    /**
     * @param Model $inspection
     * @return mixed
     */
    public function cancel(Model $inspection)
    {
        return DB::transaction(function () use ($inspection) {
            $input = [
                'iscancelled' => 1,
                'cancel_user' => access()->id(),
                'cancel_date' => Carbon::now(),
            ];
            $inspection->update($input);
        });
    }


    /**
     * @param $employer_id
     * @return mixed
     * METHOD FOR FOLLOW UP TYPE INSPECTION ============================>
     */

    /**
     * @param Model $inspection
     * @return mixed
     * Complete
     */
    public function completeFollowup($id)
    {
        $followup_inspection =$this->findOrThrowException($id);
        return DB::transaction(function () use ($followup_inspection) {
            $this->checkIfIsComplete($followup_inspection->id);
            $input = [
                'complete_status' => 1,
                'complete_user' => access()->id(),
                'complete_date' => Carbon::now(),
            ];
            $followup_inspection->update($input);
        });
    }


    /**
     * @param Model $inspection
     * @return mixed
     * Cancel inspection
     */
    public function cancelFollowup($id)
    {
        $followup_inspection =$this->findOrThrowException($id);
        return DB::transaction(function () use ($followup_inspection) {
            $this->checkIfHasActiveTask($followup_inspection->id);
            $input = [
                'iscancelled' => 1,
                'cancel_user' => access()->id(),
                'cancel_date' => Carbon::now(),
            ];
            $followup_inspection->update($input);
        });
    }
    /* End of Follow up type --------------------------- */
    /**
     * @param $employer_id
     * @return mixed
     * @deprecated
     */
    public function getEmployerListForDataTable($employer_id)
    {
        $employer = new EmployerInspectionTaskRepository();
        //$query = $employer->query()->where("employer_id", $employer_id);
        $query = $this->query()
            ->leftJoin("inspection_tasks", "inspection_tasks.inspection_id", "=", "inspections.id")
            ->leftJoin("employer_inspection_task", "employer_inspection_task.inspection_task_id", "=", "inspection_tasks.id")
            ->leftJoin("employers", "employer_inspection_task.employer_id", "=", "employers.id")
            ->leftJoin(DB::raw('code_values a'), "inspection_tasks.inspection_type_cv_id", "=", "a.id")
            ->leftJoin(DB::raw('code_values b'), "employer_inspection_task.staging_cv_id", "=", "b.id")
            ->leftJoin(DB::raw('users'), "users.id", "=", "employer_inspection_task.allocated")
            ->leftJoin("districts", "employers.district_id", "=", "districts.id")
            ->leftJoin("regions", "employers.region_id", "=", "regions.id")
            ->where("employer_inspection_task.employer_id", $employer_id);
        return $query;
    }

    /* start: Reports based on time Period, providing details on a monthly bases */

    public function getTotalMonthlyEmergence()
    {
        $data = [];

        $inspection = DB::table('main.comp_monthly_emergence_inspection')->select(["period", "total"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $total = 0;
        foreach ($inspection as $value) {
            $sub_total = (isset($value->total) ? $value->total : 0);
            $total += $sub_total;
            $period = $value->period;
            $data = array_merge($data, [ (int) $sub_total]);
        }

        $return['emergence_inspections'] = implode("," , $data);
        $return['emergence_inspections_total'] =  $total ;
        return $return;
    }

    public function getTotalMonthlyInspectedEmployers()
    {
        $data = [];

        $inspection = DB::table('main.comp_monthly_inspected_employers')->select(["period", "employers"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $total = 0;
        foreach ($inspection as $value) {
            $sub_total = (isset($value->employers) ? $value->employers : 0);
            $total += $sub_total;
            $period = $value->period;
            $data = array_merge($data, [ (int) $sub_total]); //$period . " : " . $sum
        }

        $return['inspected_employers'] = implode("," , $data);
        $return['inspected_employers_total'] =  $total;
        return $return;
    }


    public function getTotalMonthlyInspectedEmployersTable()
    {
        $data = [];
        $data_table = [];
         /* Get Monthly Target*/
        $fiscalYear = new FiscalYearRepository();
        $fiscal_year = $fiscalYear->findByFiscalYear(financial_year());
        $monthly_target =  ($fiscal_year) ? $fiscal_year->monthlyTarget('ATTINSP') : 0;
        /* end monthly target */

        $inspection = DB::table('main.comp_monthly_inspected_employers')->select(["period", "employers"])->where("fin_year_id", fin_year()->finYearID())->orderBy("year", "asc")->orderBy("month", "asc")->get()->all();

        $total = 0;
        foreach ($inspection as $value) {
            $sub_total = (isset($value->employers) ? $value->employers : 0);
            $total += $sub_total;
            $period = $value->period;
            $data[] = [$period, (int) $sub_total];
            $data_table[] = [$period, (int) $sub_total, $monthly_target, $this->targetVariancePercentage($sub_total, $monthly_target) ];
        }

        $return['inspected_employer_graph'] = $data;
        $return['inspected_employer_table'] = $data_table;
        $return['inspected_employers_overall_total'] =  $total;
        return $return;
    }

    /* end: Reports based on time Period, providing details on a monthly bases */

}