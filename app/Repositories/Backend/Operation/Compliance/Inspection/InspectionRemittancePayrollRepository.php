<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\InspectionRemittancePayroll;
use App\Repositories\BaseRepository;

class InspectionRemittancePayrollRepository extends BaseRepository
{
    public const MODEL = InspectionRemittancePayroll::class;

}