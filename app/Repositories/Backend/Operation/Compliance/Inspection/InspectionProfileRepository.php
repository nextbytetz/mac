<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Repositories\BaseRepository;
use App\Models\Operation\Compliance\Inspection\InspectionProfile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Services\Storage\DataFormat;

class InspectionProfileRepository extends BaseRepository
{
    const MODEL = InspectionProfile::class;

    public function destroy(Model $profile)
    {
        return DB::transaction(function () use ($profile) {
            $profile->delete();
            return true;
        });
    }

    public function update(Model $profile, array $input)
    {
        return DB::transaction(function() use ($profile, $input){
            $data = [];
            $dataFormat = new DataFormat();
            if (isset($input["business"])) {
                $business = [];
                $current = $input["business"];
                foreach ($current as $perm) {
                    if (is_numeric($perm)) {
                        array_push($business, $perm);
                    }
                }
                $data['business_id'] = $business;
            }
            if (isset($input["region"])) {
                $region = [];
                $current = $input["region"];
                foreach ($current as $perm) {
                    if (is_numeric($perm)) {
                        array_push($region, $perm);
                    }
                }
                $data['region_id'] = $region;
            }
            $input['query'] = $dataFormat->arrayToDb($data);
            $profile->update($input);
            return true;
        });
    }

    public function store(array $input)
    {
        return DB::transaction(function() use ($input){
            $data = [];
            $dataFormat = new DataFormat();
            if (isset($input["business"])) {
                $business = [];
                $current = $input["business"];
                foreach ($current as $perm) {
                    if (is_numeric($perm)) {
                        array_push($business, $perm);
                    }
                }
                $data['business_id'] = $business;
            }
            if (isset($input["region"])) {
                $region = [];
                $current = $input["region"];
                foreach ($current as $perm) {
                    if (is_numeric($perm)) {
                        array_push($region, $perm);
                    }
                }
                $data['region_id'] = $region;
            }
            $input['query'] = $dataFormat->arrayToDb($data);
            $profile = $this->query()->create($input);
            return $profile;
        });
    }

    public function getInspectionProfileForDataTable()
    {
        return $this->query()->select();
    }

    public function getEmployerForDataTable(Model $profile)
    {
        $dataFormat = new DataFormat();
        return $dataFormat->dbToArrayQuery($profile->query);
    }


    public function getUnregisteredEmployerForDataTable(Model $profile)
    {
        $dataFormat = new DataFormat();
        return $dataFormat->dbToArrayQueryFollowup($profile->query);
    }




}