<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Events\NewWorkflow;
use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Compliance\Inspection\EmployerInspectionTask;
use App\Notifications\Backend\Operation\Compliance\InspectionNotices;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Auth\PortalUser;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Finance\Receivable\Portal\ContributionArrear;

/**
 * Class EmployerInspectionTaskRepository
 * @package App\Repositories\Backend\Operation\Compliance\Inspection
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class EmployerInspectionTaskRepository extends BaseRepository
{

    use FileHandler, AttachmentHandler;

    const MODEL = EmployerInspectionTask::class;

    /**
     * @param Model $employerTask
     * @param array $input
     * @return bool
     * @throws GeneralException
     */
    public function postUpdateVisitation(Model $employerTask, array $input)
    {
        $preVisitDate = $employerTask->visit_date;

        $codeValueRepo = new CodeValueRepository();

        if ($employerTask->progressive_stage >= 5) {
            ///restrict update visitation if the stage has gone past [Approved Inspection Assessment Schedule]
            return false;
        }


        ///TODO: Check if review_start_date is less than the most recent review_end_date of the previous inspection

        //Check if review_start_date is less than review_end_date
        $review_end_date = $this->query()->where("employer_id", $employerTask->employer_id)->where("id", "<>", $employerTask->id)->whereNotNull("review_end_date")->max("review_end_date");
        if ($review_end_date) {
            $last = Carbon::parse($review_end_date);
            $current = Carbon::parse();
            if ($current->lessThan($last)) {
                throw new GeneralException("Action cancelled, last review end date can not be greater than the current review start date. Please check ...");
            }
        }

        return DB::transaction(function () use ($employerTask, $input, $preVisitDate, $codeValueRepo) {
            $preReviewStartDate = $employerTask->review_start_date;
            $preReviewEndDate = $employerTask->review_end_date;

            $employerTask->update([
                'visit_date' => $input['visit_date'],
                'exit_date' => $input['exit_date'],
                'doc' => $input['doc'],
                'review_start_date' => $input['review_start_date'],
                'findings' => $input['findings'],
                'last_inspection_date' => $input['last_inspection_date'],
                'review_end_date' => $input['review_end_date'],
                'resolutions' => $input['resolutions'],
            ]);
            //dd($input);

            switch ($employerTask->staging_cv_id) {
                case $codeValueRepo->EITPPLVDAT():
                    //Planing Visit Date
                    $employerTask->staging_cv_id = $codeValueRepo->EITPISINNTLT(); //Issuing Inspection Notice Letter
                    $employerTask->progressive_stage = 3; //Task on Progress
                    $employerTask->save();
                    $this->initialize($employerTask);
                    break;
                case $codeValueRepo->EITPWTIASCHD():
                    //Waiting Inspection Assessment Schedule
                    //==> Check if visit date has changed
                    $prevDateInstance = Carbon::parse($preVisitDate);
                    $newDateInstance = Carbon::parse($input['visit_date']);
                    if ($prevDateInstance->diffInDays($newDateInstance)) {
                        //Visit Date has been Changed ...
                        $employerTask->staging_cv_id = $codeValueRepo->EITPVDPWTIASCHD(); //Visit Date Postponed, Waiting Inspection Assessment Schedule
                        $employerTask->save();
                        $this->initialize($employerTask);
                    }
                    break;
            }

            $payrolls = $employerTask->payrolls;

            foreach ($payrolls as $payroll) {
                if (!$payroll->review_start_date) {
                    $payroll->review_start_date = $employerTask->review_start_date;
                    $payroll->save();
                }
                if (!$payroll->review_end_date) {
                    $payroll->review_end_date = $employerTask->review_end_date;
                    $payroll->save();
                }
            }

            if ($employerTask->review_start_date And $employerTask->review_end_date) {
                $generate = 0;
                if (!$preReviewStartDate And !$preReviewEndDate) {
                    //Generate Inspection Assessments
                    $generate = 1;
                }
                if ($preReviewStartDate) {
                    $preReviewStartDateInstance = Carbon::parse($preReviewStartDate);
                    $newDateInstance = Carbon::parse($employerTask->review_start_date);
                    if ($preReviewStartDateInstance->diffInDays($newDateInstance)) {
                        $generate = 1;
                    }
                }
                if ($preReviewEndDate) {
                    $preReviewEndDateInstance = Carbon::parse($preReviewEndDate);
                    $newDateInstance = Carbon::parse($employerTask->review_end_date);
                    if ($preReviewEndDateInstance->diffInDays($newDateInstance)) {
                        $generate = 1;
                    }
                }
                if ($generate) {
                    $this->generateAssessmentSchedule($employerTask);
                }
            }

           return true;
        });
    }

    /**
     * @param Model $employerTask
     * @return mixed
     */
    public function generateAssessmentSchedule(Model $employerTask)
    {
        $return = DB::transaction(function() use ($employerTask) {
            $employer_inspection_task_id = $employerTask->id;
            //$inspectionTaskPayroll = $employerTask->payrolls()->orderByDesc("inspection_task_payrolls.id")->first();
            $payrolls = $employerTask->payrolls;
            $employer_id = $employerTask->employer_id;

            $reference = $employerTask->employer->employerCategory->reference;
            switch ($reference) {
                case "ECPUB":
                    //Public Institution
                    $percent = sysdefs()->data()->public_contribution_percent;
                    break;
                default:
                    //Private Institution
                    $percent = sysdefs()->data()->private_contribution_percent;
                    break;
            }
            $percent = $percent / 100;

            /*$insertQuery = "select {$employer_inspection_task_id}, bb.contrib_month, aa.contrib_amount, aa.last_pay_date, aa.member_count, {$percent}, aa.first_pay_date from (select a.employer_id, b.contrib_month, sum(b.amount) contrib_amount, min(a.rct_date) first_pay_date, sum(b.member_count) member_count, max(a.rct_date) last_pay_date  from receipts a join receipt_codes b on a.id = b.receipt_id where b.grouppaystatus = 0 and b.contrib_month between '{$start_date}' and '{$end_date}' and a.iscancelled = 0 and a.isdishonoured = 0 and a.employer_id = {$employer_id} and b.fin_code_id = 2 group by a.employer_id, b.contrib_month) aa right join (SELECT (('{$start_date}' :: date + ('1 mon' :: interval month * (generate_series(0, months_diff('{$start_date}', '{$end_date}')::smallint)) :: double precision))) :: date AS contrib_month) bb on date_part('month', aa.contrib_month) = date_part('month', bb.contrib_month) and date_part('year', aa.contrib_month) = date_part('year', bb.contrib_month)";*/

            //dd($insertQuery);

            //Log::error($percent);

            //delete all assessments
            //$employerTask->assessments()->delete();
            //insert all assessments
            $sql = <<<SQL
delete from inspection_assessments where employer_inspection_task_id = {$employer_inspection_task_id};
SQL;
            DB::unprepared($sql);

            foreach ($payrolls as $payroll) {

                $reviewStartDate = Carbon::parse($payroll->review_start_date)->startOfMonth();
                $reviewEndDate = Carbon::parse($payroll->review_end_date)->endOfMonth();
                $start_date = $reviewStartDate->format("Y-m-d");
                $end_date = $reviewEndDate->format("Y-m-d");

                $payrollFilter = ' and a.payroll_id = 1 ';
                if ($payroll->payroll_id) {
                    $payrollFilter = " and a.payroll_id = {$payroll->payroll_id} ";
                }
                $insertQuery = "select {$payroll->id}, {$employer_inspection_task_id}, bb.contrib_month, coalesce(aa.contrib_amount, cc.contrib_amount) contrib_amount, coalesce(aa.last_pay_date, cc.last_pay_date) last_pay_date, coalesce(aa.member_count, cc.member_count) member_count, {$percent}, coalesce(aa.first_pay_date, cc.first_pay_date) first_pay_date, coalesce(dd.contrib_amount, 0) interest_amount  from (select a.employer_id, b.contrib_month, sum(coalesce(b.amount)) contrib_amount, min(a.rct_date) first_pay_date, sum(b.member_count) member_count, max(a.rct_date) last_pay_date  from receipts a join receipt_codes b on a.id = b.receipt_id where b.grouppaystatus = 0 and b.contrib_month between '{$start_date}' and '{$end_date}' and a.iscancelled = 0 and a.isdishonoured = 0 and a.employer_id = {$employer_id} and b.fin_code_id = 2 {$payrollFilter} group by a.employer_id, b.contrib_month) aa right join (SELECT (('{$start_date}' :: date + ('1 mon' :: interval month * (generate_series(0, months_diff('{$start_date}', '{$end_date}')::smallint)) :: double precision))) :: date AS contrib_month) bb on date_part('month', aa.contrib_month) = date_part('month', bb.contrib_month) and date_part('year', aa.contrib_month) = date_part('year', bb.contrib_month) left join (select employer_id, contrib_month, max(amount) contrib_amount, min(rct_date) first_pay_date, max(member_count) member_count, max(rct_date) last_pay_date from legacy_merged_contributions where iscancelled = 0 and isdishonoured = 0 and employer_id = {$employer_id} and fin_code_id = 2 group by employer_id, contrib_month) cc on date_part('month', cc.contrib_month) = date_part('month', bb.contrib_month) and date_part('year', cc.contrib_month) = date_part('year', bb.contrib_month) left join (select a.employer_id, b.contrib_month, sum(coalesce(b.amount)) contrib_amount, min(a.rct_date) first_pay_date, max(a.rct_date) last_pay_date  from receipts a join receipt_codes b on a.id = b.receipt_id where b.grouppaystatus = 0 and b.contrib_month between '{$start_date}' and '{$end_date}' and a.iscancelled = 0 and a.isdishonoured = 0 and a.employer_id = {$employer_id} and b.fin_code_id = 1 {$payrollFilter} group by a.employer_id, b.contrib_month) dd on date_part('month', dd.contrib_month) = date_part('month', bb.contrib_month) and date_part('year', dd.contrib_month) = date_part('year', bb.contrib_month);";

                $sql = <<<SQL
insert into inspection_assessments (inspection_task_payroll_id, employer_inspection_task_id, contrib_month, sys_contrib_amount, last_pay_date, member_count, percent, first_pay_date, interest_amount) $insertQuery;
SQL;
                DB::unprepared($sql);
            }

            $sql = <<<SQL
REFRESH MATERIALIZED VIEW inspection_assessment_template;
SQL;
            DB::unprepared($sql);

            /*
               $assessments = [];
               while ($reviewStartDate->lessThanOrEqualTo($reviewEndDate)) {
                $assessments[] = [
                    'employer_inspection_task_id' => $employer_inspection_task_id,
                    'contrib_month' => $reviewStartDate->format("Y-m-28"),
                ];
                $reviewStartDate->addMonth();
            }
            $employerTask->assessments()->createMany($assessments);*/
            return true;
        });
        return $return;
    }

    public function updatePreRemittance(Model $employerTask)
    {
        $taskId = $employerTask->id;
        $sql = <<<SQL
/*1. when first_count has some value and second_count is null, all have some values of payroll_id (same, varying, unique for each)
2. when first_count - second_count is 1, everything is okay
3. when first_count is equal to second_count, all have payroll_id which is null
4. when first_count - second_count is greater than 1, all have some values of payroll_id, (same, varying, unique for each)
  Resolutions:
  Case 3 -- update one entry with 1
  Case 1,4 -- manipulate to ensure each entry has unique payroll_id
  */
  -- Clean payroll id for inspection_task_payrolls, avoid duplicates ...
DO $$
    DECLARE r record;
    declare t record;
BEGIN
    FOR r IN select a.employer_inspection_task_id, a.payroll_id, a.count first_count, b.count second_count from (select employer_inspection_task_id, count(id) count, min(payroll_id)  payroll_id from inspection_task_payrolls where employer_inspection_task_id = {$taskId} group by employer_inspection_task_id having count(id) > 1) a left join (select employer_inspection_task_id, count(id) count from inspection_task_payrolls where payroll_id is null and employer_inspection_task_id = {$taskId} group by employer_inspection_task_id having count(id) > 1) b on a.employer_inspection_task_id = b.employer_inspection_task_id
    LOOP
        if (r.first_count = r.second_count) then
            -- update one entry with payroll_id 1
            update inspection_task_payrolls set payroll_id = 1 where employer_inspection_task_id = r.employer_inspection_task_id and id = (select min(id) from inspection_task_payrolls where employer_inspection_task_id = r.employer_inspection_task_id);
        end if;

        if (r.first_count - coalesce(r.second_count, 0) > 1) then
            -- manipulate to ensure each entry has unique payroll_id
            for t in select employer_inspection_task_id, payroll_id, array_agg(id order by id) ids from inspection_task_payrolls where employer_inspection_task_id = r.employer_inspection_task_id group by employer_inspection_task_id, payroll_id having count(payroll_id) > 1
            loop
                update inspection_task_payrolls set payroll_id = t.payroll_id where id = t.ids[1];
                update inspection_task_payrolls set payroll_id = NULL where id in (select unnest(t.ids[2:]));
            end loop;
        end if;

    END LOOP;
    -- Update for default Payroll for employer inspection task with only one payroll
    update inspection_task_payrolls set payroll_id = 1 from (select employer_inspection_task_id , count(id) from inspection_task_payrolls where employer_inspection_task_id = {$taskId} group by employer_inspection_task_id having count(id) = 1) a where a.employer_inspection_task_id = inspection_task_payrolls.employer_inspection_task_id;
END$$;

update inspection_assessments set sys_contrib_amount = tt.contrib_amount, interest_amount = tt.interest_amount from (select a.id, a.employer_inspection_task_id, d.contrib_amount interest_amount, coalesce(e.contrib_amount, f.contrib_amount) contrib_amount from inspection_assessments a join inspection_task_payrolls b on a.inspection_task_payroll_id = b.id join employer_inspection_task c on b.employer_inspection_task_id = c.id left join (select a.employer_id, b.contrib_month, a.payroll_id, sum(coalesce(b.amount)) contrib_amount, min(a.rct_date) first_pay_date, max(a.rct_date) last_pay_date  from receipts a join receipt_codes b on a.id = b.receipt_id where b.grouppaystatus = 0 and a.iscancelled = 0 and a.isdishonoured = 0 and b.fin_code_id = 1 group by a.employer_id, b.contrib_month, a.payroll_id) d on d.employer_id = c.employer_id and  date_part('month', d.contrib_month) = date_part('month', a.contrib_month) and date_part('year', d.contrib_month) = date_part('year', a.contrib_month) and d.payroll_id = b.payroll_id and d.first_pay_date < c.last_inspection_date left join (select a.employer_id, b.contrib_month, a.payroll_id, sum(coalesce(b.amount)) contrib_amount, min(a.rct_date) first_pay_date, max(a.rct_date) last_pay_date  from receipts a join receipt_codes b on a.id = b.receipt_id where b.grouppaystatus = 0 and a.iscancelled = 0 and a.isdishonoured = 0 and b.fin_code_id = 2 group by a.employer_id, b.contrib_month, a.payroll_id) e on e.employer_id = c.employer_id and date_part('month', e.contrib_month) = date_part('month', a.contrib_month) and date_part('year', e.contrib_month) = date_part('year', a.contrib_month) and e.payroll_id = b.payroll_id and e.first_pay_date < c.last_inspection_date left join (select employer_id, contrib_month, 1 payroll_id, max(amount) contrib_amount, min(rct_date) first_pay_date, max(member_count) member_count, max(rct_date) last_pay_date from legacy_merged_contributions where iscancelled = 0 and isdishonoured = 0 and fin_code_id = 2 group by employer_id, contrib_month, 1) f on f.employer_id = c.employer_id and date_part('month', f.contrib_month) = date_part('month', a.contrib_month) and date_part('year', f.contrib_month) = date_part('year', a.contrib_month) and f.payroll_id = b.payroll_id and f.first_pay_date < c.last_inspection_date) tt where tt.id = inspection_assessments.id and tt.employer_inspection_task_id = {$taskId};

SQL;
        DB::unprepared($sql);
    }

    /**
     * @param Model $employerTask
     * @param array $input
     * @return bool
     */
    public function postPayroll(Model $employerTask, array $input)
    {
        $return = DB::transaction(function() use ($employerTask, $input) {
            $update = 0;
            if ($employerTask->progressive_stage < 5) {
                $update = 1;
            }

            $inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
            $special = [];
            $ids = [];
            /* start : collect all special keys */
            foreach ($input as $key => $value) {
                if ( strpos( $key, 'payroll_name' ) !== false ) {
                    $this_special = substr($key, 12);
                    if (!in_array($this_special, $special, true)) {
                        $special[] = $this_special;
                    }
                }
            }

            foreach ($special as $value) {
                if (is_numeric($value)) {
                    //update Existing...
                    $payroll = $inspectionTaskPayrollRepo->find($value);
                    if ($update) {
                        $payroll->review_start_date = $input['review_start_date' . $value];
                        $payroll->review_end_date = $input['review_end_date' . $value];
                    }
                    $payroll->name = $input['payroll_name' . $value];
                    $payroll->payroll_id = $input['payroll_id' . $value];
                    $payroll->save();
                    $ids[] = $payroll->id;
                } else {
                    $payroll = $inspectionTaskPayrollRepo->query()->create([
                        'name' => $input['payroll_name' . $value],
                        'payroll_id' => $input['payroll_id' . $value],
                        'review_start_date' => $input['review_start_date' . $value],
                        'review_end_date' => $input['review_end_date' . $value],
                        'employer_inspection_task_id' => $employerTask->id,
                    ]);
                    $ids[] = $payroll->id;
                }
            }
            //re-generate assessment schedule
            if ($update) {
                $inspectionTaskPayrollRepo->query()->whereNotIn('id', $ids)->where('employer_inspection_task_id', $employerTask->id)->delete();
                $this->generateAssessmentSchedule($employerTask);
            } else {
                $this->updatePreRemittance($employerTask);
            }

            return true;
        });
        return $return;
    }

    public function skipStage(Model $employerTask)
    {
        $return = DB::transaction(function() use ($employerTask) {
            $codeValueRepo = new CodeValueRepository();
            switch ($employerTask->staging_cv_id) {
                case $codeValueRepo->EITPWAISPLAP():
                    //Planing Visit Date ...
                    $employerTask->progressive_stage = 3;
                    $employerTask->staging_cv_id = $codeValueRepo->EITPWTIASCHD(); //Waiting Inspection Assessment Schedule
                    $employerTask->save();
                    $this->initialize($employerTask);
                    break;
                case $codeValueRepo->EITPISINNTLT():
                    //Issuing Inspection Notice Letter ...
                    $employerTask->staging_cv_id = $codeValueRepo->EITPWTIASCHD(); //Waiting Inspection Assessment Schedule
                    $employerTask->save();
                    $this->initialize($employerTask);
                    break;
                default:
                    throw new GeneralException("This stage can not be skipped.");
                    break;
            }
        });
        return $return;
    }

    /**
     * @param Model $employerTask
     * @return mixed|null
     */
    public function retrieveInspectionTaskAllocatedUser(Model $employerTask)
    {
        $user = NULL;
        if (!$employerTask->allocated) {
            /*$user = (new UserRepository())->getAllocatedForInspection($employerTask);
            $employerTask->allocated = $user;
            $employerTask->save();*/
        } else {
            $user = $employerTask->allocated;
        }
        return $user;
    }

    /**
     * @param Model $incident
     */
    public function updateInspectionStageId(Model $employerTask)
    {
        $employer_inspection_task_stage_id = $employerTask->stages()->orderByDesc("employer_inspection_task_stages.id")->limit(1)->first()->pivot->id;
        $employerTask->employer_inspection_task_stage_id = $employer_inspection_task_stage_id;
        $employerTask->save();
    }

    /**
     * @param Model $employerTask
     * @return array|mixed
     */
    public function retrievePreviousStage(Model $employerTask)
    {
        $return = [];
        $query = $employerTask->stages()->orderBy("employer_inspection_task_stages.id", "desc");
        if ($query->count()) {
            $return = json_decode($query->first()->pivot->comments);
        }
        return $return;
    }

    public function initializeAll(Model $inspection)
    {
        //get all employer inspection tasks
        $employerTasks = $inspection->employerTasks;
        foreach ($employerTasks as $employerTask) {
            $return = $this->initialize($employerTask);
        }
        return true;
    }

    /**
     * @param Model $employerTask
     * @param int $caller
     * @return bool
     */
    public function initialize(Model $employerTask, $caller = 0)
    {
        //logger("I have passed!");
        if ($employerTask->progressive_stage == 1) {
            //does not need to initialize, task not yet assigned to staff. SKIP
            return false;
        }
        //logger("I have passed!");
        $employerTask->refresh();
        $stage = $employerTask->staging_cv_id;
        $codeValue = new CodeValueRepository();
        $checkerRepo = new CheckerRepository();
        $userRepo = new UserRepository();

        //--> Allocate Employer Inspection Task User
        $user = $this->retrieveInspectionTaskAllocatedUser($employerTask);
        $prevStage = $this->retrievePreviousStage($employerTask);
        $prevComment = (($prevStage) ? $prevStage->{'currentStage'} : "Approved Inspection Plan");
        $department = $userRepo->getUserDepartment($user);
        $notify = 0;
        $comments = NULL;
        $process = 1;
        $closeChecker = 0;
        $priority = 0;


        switch ($stage) {
            case $codeValue->EITPWAISPLAP():
                //Waiting Inspection Plan Approval
                $process = 0;
                break;
            case $codeValue->EITPPLVDAT():
                //Planing Visit Date
                //--> Checker Inbox & Tasks
                $priority = 1;
                $comments = '{"previousStage":"Approved Inspection Plan", "submittedDate":"' . now_date() . '", "currentStage":"Planing Visit Date", "currentStageCvId":' . $stage . ', "nextStage":"Issuing Inspection Notice Letter", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPISINNTLT():
                //Issuing Inspection Notice Letter
                $priority = 1;
                $prevComment = (($prevStage) ? $prevStage->{'currentStage'} : "Approved Inspection Plan");
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Issuing Inspection Notice Letter", "currentStageCvId":' . $stage . ', "nextStage":"Created Inspection Notice Letter", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPINNTLTCRTD():
                //Created Inspection Notice Letter
                $prevComment = (($prevStage) ? $prevStage->{'currentStage'} : "Approved Inspection Plan");
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Inspection Notice Letter Created", "currentStageCvId":' . $stage . ', "nextStage":"Waiting Inspection Assessment Schedule", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPWTIASCHD():
                //Inspection Notice Letter Sent
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Inspection Notice Letter Sent", "currentStageCvId":' . $stage . ', "nextStage":"Upload Inspection Assessment Schedule", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPVDPWTIASCHD():
                //Visit Date Postponed, Waiting Inspection Assessment Schedule
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Visit Date Postponed, Waiting Inspection Assessment Schedule", "currentStageCvId":' . $stage . ', "nextStage":"Upload Inspection Assessment Schedule", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPUPIASSC():
                //Uploaded Inspection Assessment Schedule
                $priority = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Uploaded Inspection Assessment Schedule", "currentStageCvId":' . $stage . ', "nextStage":"Initiate Inspection Assessment Schedule Review Workflow", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPOIASCHRVW():
                //On Inspection Assessment Schedule Review Workflow
                $closeChecker = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Inspection Assessment Schedule Review Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Completed Inspection Assessment Schedule Review", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPCMIASCRVW():
                $notify = 1;
                //Completed Inspection Assessment Schedule Review
                //Come to this stage when assessment result to outstanding balance or not debt. Else Go to With Overpayment Stage.
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Completed Inspection Assessment Schedule Review", "currentStageCvId":' . $stage . ', "nextStage":"Issuing Inspection Response Letter", "department":"' . $department . '", "comment":"Issue Demand Notice Letter or Letter of Compliance", "inspectionReview":"Demand Notice Letter in case if an employer has an outstanding balance and Letter of Compliance in case if employer has no outstanding balance."}';
                break;
            case $codeValue->EITPISSIRESLTR():
                //Issuing Inspection Response Letter
                $priority = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Issuing Inspection Response Letter", "currentStageCvId":' . $stage . ', "nextStage":"Issuing Reminder Letter or Closing Employer Inspection Task", "department":"' . $department . '", "comment":"Possibly issuing reminder letter if there is outstanding amount not paid, or closing employer inspection", "inspectionReview":""}';
                break;
            case $codeValue->EITPIRESLTRCRTD():
                //Created Inspection Response Letter
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Created Inspection Response Letter", "currentStageCvId":' . $stage . ', "nextStage":"Dispatching Inspection Response Letter", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPIRESLTRPRTLCRTD():
                //Partially Created Inspection Response Letter
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Partially Created Inspection Response Letter", "currentStageCvId":' . $stage . ', "nextStage":"Dispatching Inspection Response Letter", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPISUEDIRSLR():
                //Inspection Response Letter Sent
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Issued Inspection Response Letter", "currentStageCvId":' . $stage . ', "nextStage":"Issuing Reminder Letter or Closing Employer Inspection Task", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPISSREMLTR():
                //Issuing Reminder Letter
                $priority = 1;
                $notify = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Issuing Reminder Letter", "currentStageCvId":' . $stage . ', "nextStage":"Employer Paid Outstanding/Closing Employer Inspection Task", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPREMLTRCRTD():
                //Reminder Letter Created
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Reminder Letter Created", "currentStageCvId":' . $stage . ', "nextStage":"Reminder Letter to be dispatched", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPREMLTRSNT():
                //Reminder Letter Sent
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Reminder Letter Sent", "currentStageCvId":' . $stage . ', "nextStage":"Waiting for Employer Remmitance", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPEMPPDOUTSTNDN():
                //Employer Paid Outstanding
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Employer Paid Outstanding", "currentStageCvId":' . $stage . ', "nextStage":"Closing Employer Inspection Task", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPOINSTPRWFW():
                //On Instalment Payment Request Workflow
                $closeChecker = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Instalment Payment Request Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Employer Paying By Installment", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPEPBYINSTLMNT():
                //Employer Paying By Installment
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Employer Paying By Installment", "currentStageCvId":' . $stage . ', "nextStage":"Employer Paid Outstanding", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPDFLTDEMPLY():
                //Defaulted Employer
                $priority = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Defaulted Employer", "currentStageCvId":' . $stage . ', "nextStage":"On Defaulted Employer Workflow", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPODFTDEMWFW():
                //On Defaulted Employer Workflow
                $closeChecker = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Defaulted Employer Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Closing Employer Inspection", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPWTHOVRPYMNT():
                //With Overpayment
                $priority = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"With Overpayment", "currentStageCvId":' . $stage . ', "nextStage":"On Overpayment Refunding Workflow", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITOROVRWKFLW():
                //On Overpayment Refunding Workflow
                $closeChecker = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Overpayment Refunding Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Employer Overpayment Refunded", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITEMPLYREFOVP():
                //Employer Overpayment Refunded
                $priority = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Employer Overpayment Refunded", "currentStageCvId":' . $stage . ', "nextStage":"Closing Employer Inspection", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITPCLSDEMPINSPCTN():
                //Closed Employer Inspection
                $closeChecker = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Closed Employer Inspection", "currentStageCvId":' . $stage . ', "nextStage":"", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITOCNEMPLYITSW():
                //On Cancelled Employer Inspection Task Workflow
                $closeChecker = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"On Cancelled Employer Inspection Task Workflow", "currentStageCvId":' . $stage . ', "nextStage":"Cancelled Employer Inspection Task", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITCANEMPLYITS():
                //Cancelled Employer Inspection Task
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Cancelled Employer Inspection Task", "currentStageCvId":' . $stage . ', "nextStage":"Closing Employer Inspection", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
            case $codeValue->EITDCLNEMPLYITS():
                //Declined Employer Inspection Task
                $closeChecker = 1;
                $comments = '{"previousStage":"' . $prevComment . '", "submittedDate":"' . now_date() . '", "currentStage":"Declined Employer Inspection Task", "currentStageCvId":' . $stage . ', "nextStage":"Closing Employer Inspection", "department":"' . $department . '", "comment":"", "inspectionReview":""}';
                break;
        }

        if ($process And $user) {
            if (!empty($comments)) {
                //Save comment on employer inspection task stages
                $employerTask->stages()->attach($stage,
                    [
                        'comments' => $comments
                    ]);
                $this->updateInspectionStageId($employerTask);
            }
            if ($closeChecker) {
                //Close recent checker entry
                $input = [
                    'resource_id' => $employerTask->id,
                    'checker_category_cv_id' => $codeValue->CCAEMPINSPCTNTSK(),
                ];
                $checkerRepo->closeRecently($input);
            } else {
                //Create checker entry
                $input = [
                    'comments' => $comments,
                    'user_id' => $user,
                    'checker_category_cv_id' => $codeValue->CCAEMPINSPCTNTSK(),
                    'priority' => $priority,
                    'notify' => $notify,
                ];
                $checkerRepo->create($employerTask, $input);
            }
        }
        return true;
    }

    public function updateWorkflowStage(Model $employerTask, $group, $type)
    {
        $wfModule = new WfModuleRepository();
        $codeValue = new CodeValueRepository();
        $updated = false;
        $staging_cv_id = 0;
        switch (true) {
            case ($group == $wfModule->employerInspectionAssessmentReviewType()['group'] And $type == $wfModule->employerInspectionAssessmentReviewType()['type']):
                //Inspection Assessment Review
                $staging_cv_id = $codeValue->EITPOIASCHRVW(); //On Inspection Assessment Schedule Review Workflow
                $updated = true;
                break;
            case ($group == $wfModule->employerInspectionDefaulterType()['group'] And $type == $wfModule->employerInspectionDefaulterType()['type']):
                //Defaulters (Employer)
                $staging_cv_id = $codeValue->EITPODFTDEMWFW(); //On Defaulted Employer Workflow
                $updated = true;
                break;
            case ($group == $wfModule->employerInspectionInstalmentPaymentType()['group'] And $type == $wfModule->employerInspectionInstalmentPaymentType()['type']):
                //Instalment Payment Request (Employer)
                $staging_cv_id = $codeValue->EITPOINSTPRWFW(); //On Instalment Payment Request Workflow
                $updated = true;
                break;
            case ($group == $wfModule->employerInspectionCancellationType()['group'] And $type == $wfModule->employerInspectionCancellationType()['type']):
                //Employer Inspection Task Cancellation
                $staging_cv_id = $codeValue->EITOCNEMPLYITSW(); //On Cancelled Employer Inspection Task Workflow
                $updated = true;
                break;
            case ($group == $wfModule->employerInspectionRefundOverPaymentType()['group'] And $type == $wfModule->employerInspectionRefundOverPaymentType()['type']):
                //Refunding Employer Overpayment
                $staging_cv_id = $codeValue->EITOROVRWKFLW(); //On Overpayment Refunding Workflow
                $updated = true;
                break;
        }
        if ($updated) {
            //Initialize Inspection
            $employerTask->staging_cv_id = $staging_cv_id;
            $employerTask->save();
            $this->initialize($employerTask);
        }
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function initiateWorkflow(array $input)
    {
        $return = DB::transaction(function () use ($input) {
            //Check if has access to initiate this workflow at level 1
            access()->hasWorkflowModuleDefinition($input['group'], $input['type'], 1);

            $wfModule = new WfModuleRepository();
            $moduleRepo = new WfModuleRepository();
            $payrollRepo = new InspectionTaskPayrollRepository();

            $group = $input['group'];
            $resource = $input['resource'];
            $type = $input['type'];
            $comments = $input['comments'];

            $employerTask = $this->find($resource);

            $module = $wfModule->getModule(['wf_module_group_id' => $group, 'type' => $type]);

            //check if initiated by allocated user
            access()->assignedForEmployerInspectionTask($employerTask);
            //Check if there is any active employer insection task workflow, restrict
            $count = $employerTask->workflows()->where("wf_done", 0)->count();
            if ($count) {
                throw new GeneralException("Can't initiate workflow, there is another active workflow pending for this inspection");
            }

            switch ($module) {
                case $moduleRepo->employerInspectionAssessmentReviewModule()[0]:
                    //check if there is not upload error and all computations has been done successful
                    if ($employerTask->file_error) {
                        throw new GeneralException("Can't initiate workflow, there were errors on uploading the assessment");
                    }
                    //check if all payrolls has uploaded assessments
                    $count = $payrollRepo->query()->where(['employer_inspection_task_id' => $employerTask->id, 'hasassessment' => 0])->count();
                    if ($count) {
                        throw new GeneralException("Can't initiate workflow, check that each payroll has assessment successfully uploaded ...");
                    }
                    break;
                default:

                    break;
            }

            //Insert into Employer Inspection Task Workflow
            $workflow = $employerTask->workflows()->create([
                'wf_module_id' => $module,
            ]);

            //Initialize Workflow
            event(new NewWorkflow(['wf_module_group_id' => $group, 'resource_id' => $workflow->id, 'type' => $type], [], ['comments' => $comments]));

            //Update Notification Stage
            $this->updateWorkflowStage($employerTask, $group, $type);

            return $input;
        });
        return $return;
    }

    public function remittanceSummary(Model $employerTask, $inspection_task_payroll_id = 0)
    {
        $return = [];
        $remittance = (new InspectionRemittanceRepository())->query()->where('employer_inspection_task_id', $employerTask->id)->orderBy('contrib_month', 'asc');
        $contribution = $remittance->sum('paid_after');
        $interest_after = $remittance->sum('interest_after');
        $interest_before = $remittance->sum('interest_before');
        $interest = $interest_after + $interest_before;
        $total_paid = $contribution + $interest;
        $contribution_balance = $remittance->sum('paid_balance');
        $interest_balance = $remittance->sum('interest_balance_actual');
        $total_balance = $contribution_balance + $interest_balance;
        $contribmonths = $remittance->select(['contrib_month_formatted', 'contrib_month', 'paid_before', 'interest_before', 'paid_after', 'interest_after'])->get();
//        if ($employerTask->iscompliant == 0) {
        if (true) {
            $return = [
                'contribution' => number_2_format($contribution),
                'contribution_balance' => number_2_format($contribution_balance),
                'interest' => number_2_format($interest),
                'interest_balance' => number_2_format($interest_balance),
                'total_balance' => number_2_format($total_balance),
                'total_paid' => number_2_format($total_paid),
                'contribmonths' => $contribmonths,

            ];
        }
        //dd($return);
        return $return;
    }

    /**
     * @param Model $employerTask
     * @param int $inspection_task_payroll_id
     * @return array
     */
    public function assessmentSummary(Model $employerTask, $inspection_task_payroll_id = 0)
    {
        $inspectionAssessmentTemplateRepo = new InspectionAssessmentTemplateRepository();
        $inspectionRemittancePayrollRepo = new InspectionRemittancePayrollRepository();
        $inspectionRemittanceRepo = new InspectionRemittanceRepository();
        $inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
        if ($inspection_task_payroll_id) {
            $assessment = $inspectionAssessmentTemplateRepo->query()->where("employer_inspection_task_id", $employerTask->id)->where("inspection_task_payroll_id", $inspection_task_payroll_id);
            $payroll = $inspectionTaskPayrollRepo->find($inspection_task_payroll_id);
            $remittance = $inspectionRemittancePayrollRepo->query()->where('employer_inspection_task_id', $employerTask->id)->where('payroll_id', $payroll->payroll_id);
        } else {
            $assessment = $inspectionAssessmentTemplateRepo->query()->where('employer_inspection_task_id', $employerTask->id);
            $remittance = $inspectionRemittanceRepo->query()->where('employer_inspection_task_id', $employerTask->id);
        }

        $underpaid = $assessment->sum("underpaid");
        $interest_on_unpaid = $assessment->sum("interest_on_unpaid");
        $interest_on_paid = $assessment->sum("interest_on_paid");
        $accumulative_to_be_paid = $assessment->sum("accumulative_to_be_paid");
        $overpaid = $assessment->sum("overpaid");
        $total_interest = $interest_on_unpaid + $interest_on_paid;
        $outstandingbalance = $remittance->sum('paid_balance');
        $interestbalance = $remittance->sum('interest_balance');
        //$accumulativebalance = $outstandingbalance + $interestbalance;
        //$accumulativebalance = $accumulative_to_be_paid;
        $accumulativebalance = $accumulative_to_be_paid - $outstandingbalance;

        if ($assessment->count()) {
            $return = [
                'underpaid' => number_2_format($underpaid),
                'interest_on_unpaid' => number_2_format($interest_on_unpaid),
                'interest_on_paid' => number_2_format($interest_on_paid),
                'total_interest' => number_2_format($total_interest),
                'accumulative_to_be_paid' => number_2_format($accumulative_to_be_paid),
                'overpaid' => number_2_format($overpaid),
                'hasoutstanding' => ($underpaid > 5 && $outstandingbalance > 5) ? 1 : 0,
                'hasinterest' => ($total_interest > 5 && $interestbalance > 5) ? 1 : 0,
                'outstandingbalance' => number_2_format($outstandingbalance),
                'interestbalance' => number_2_format($interestbalance),
                'accumulativebalance' => number_2_format($accumulativebalance),
            ];
        } else {
            $return = [
                'underpaid' => 0,
                'interest_on_unpaid' => 0,
                'interest_on_paid' => 0,
                'total_interest' => 0,
                'accumulative_to_be_paid' => 0,
                'overpaid' => 0,
                'hasoutstanding' => 0,
                'hasinterest' => 0,
                'outstandingbalance' => 0,
                'interestbalance' => 0,
                'accumulativebalance' => 0,
            ];
        }
        return $return;
    }

    /**
     * @param Model $employerTask
     * @return string
     * @throws \Throwable
     */
    public function getMinuteNoteForAssessmentApproval(Model $employerTask, array $assessment_summary)
    {
        $note = 'Sample Note';

        $employer = $employerTask->employer;
        $employername = ucwords(strtolower($employer->name)) . " of " . $employer->region_label;
        $assessment_start = Carbon::parse($employerTask->review_start_date)->format("F Y");
        $assessment_end = Carbon::parse($employerTask->review_end_date)->format("F Y");
        $inspection_date = Carbon::parse($employerTask->last_inspection_date)->format("d<\s\u\p>S</\s\u\p> F Y");
        $owed_amount = 0;
        switch (true) {
            case ($employerTask->iscompliant And $employerTask->hasoverpay):
                //complntoverpaid
                $note = getLanguageBlock("backend.lang.inspection.note.complntoverpaid", ['employer_task' => $employerTask, 'employername' => $employername, 'assessment_start' => $assessment_start, 'assessment_end' => $assessment_end, 'inspection_date' => $inspection_date, 'assessment_summary' => $assessment_summary])->render();
                break;
            case ($employerTask->iscompliant And !$employerTask->hasoverpay):
                //complnt
                $note = getLanguageBlock("backend.lang.inspection.note.complnt", ['employer_task' => $employerTask, 'employername' => $employername, 'assessment_start' => $assessment_start, 'assessment_end' => $assessment_end, 'inspection_date' => $inspection_date])->render();
                break;
            case (!$employerTask->iscompliant And $employerTask->hasoverpay):
                //noncomplntoverpaid
                //$note = getLanguageBlock("backend.lang.inspection.note.noncomplntoverpaid", ['employer_task' => $employerTask, 'employername' => $employername, 'assessment_start' => $assessment_start, 'assessment_end' => $assessment_end, 'inspection_date' => $inspection_date, 'assessment_summary' => $assessment_summary])->render();
                $note = getLanguageBlock("backend.lang.inspection.note.noncomplntoverpaid", ['employer_task' => $employerTask, 'employername' => $employername, 'assessment_start' => $assessment_start, 'assessment_end' => $assessment_end, 'inspection_date' => $inspection_date, 'assessment_summary' => $assessment_summary])->render();
                break;
            case (!$employerTask->iscompliant And !$employerTask->hasoverpay):
                //noncomplnt
                $note = getLanguageBlock("backend.lang.inspection.note.noncomplnt", ['employer_task' => $employerTask, 'employername' => $employername, 'assessment_start' => $assessment_start, 'assessment_end' => $assessment_end, 'inspection_date' => $inspection_date, 'assessment_summary' => $assessment_summary])->render();
                break;
        }
        return $note;
    }

    public function completeAssessmentReview($id, $module_id)
    {
        return DB::transaction(function () use ($id, $module_id) {
            $workflow = (new EmployerInspectionTaskWorkflowRepository())->find($id);
            $employerTask = $workflow->employerTask;

            //update wf_done on employer inspection task workflow
            $workflow->wf_done_date = Carbon::now();

            //check whether declined or approve and update the stage respectively.
            $check = (new WfTrackRepository())->checkIfModuleDeclined($id, $module_id);

            if ($check) {
                //Declined
                $employerTask->staging_cv_id = (new CodeValueRepository())->EITPWTIASCHD(); //Waiting Inspection Assessment Schedule
                $employerTask->progressive_stage = 3; //Task on Progressive
                $employerTask->save();
                $workflow->wf_done = 2;
            } else {
                //Approved
                $employerTask->staging_cv_id = (new CodeValueRepository())->EITPCMIASCRVW(); //Completed Inspection Assessment Schedule Review
                $employerTask->progressive_stage = 5; //Task on Progressive
                $employerTask->save();
                $workflow->wf_done = 1;
                //update contribution status
                $this->updateContributionStatus($employerTask);
                //update inspection payrolls
                $this->updatePreRemittance($employerTask);
            }
            //update workflow model
            $workflow->save();

            //initialize notification
            $this->initialize($employerTask);

            return true;
        });
    }

    /**
     * @param Model $employer_task
     * @throws GeneralException
     */
    public function checkUploadAssessmentTemplate(Model $employer_task)
    {
        //confirm visit date, review start date & review end date
        $msg = "";
        if (!$employer_task->visit_date)
            $msg .= " <span class='tag tag-success'>Visit Date</span> ";
        if (!$employer_task->review_start_date)
            $msg .= " <span class='tag tag-success'>Assessment Start Date</span> ";
        if (!$employer_task->review_end_date)
            $msg .= " <span class='tag tag-success'>Assessment End Date</span> ";
        if (!$employer_task->last_inspection_date)
            $msg .= " <span class='tag tag-success'>Last Inspection Date/Exit Meeting Date</span> ";
        //dd($msg);
        if (!empty($msg))
            throw new GeneralException("Please Update {$msg}");

    }

    /**
     * @param Model $employerTask
     * @param array $input
     * @return mixed
     */
    public function processAssessmentTemplate(Model $employerTask, array $input)
    {
        $payroll_id = $input['payroll_id'];
        $return = DB::transaction(function() use ($employerTask, $payroll_id) {

            $id = $employerTask->id;
            $file = inspection_contribution_analysis_dir() . DIRECTORY_SEPARATOR . $id;
            $return = ['success' => true, "id" => $employerTask->id];

            try {
                $headings = Excel::selectSheetsByIndex(0)
                    ->load($file)
                    ->takeRows(1)
                    ->first()
                    ->keys()
                    ->toArray();
                $verifyArr = ['month','no_employees_verified','basic_salary','allowances', 'amount_contributed', 'payment_date'];
                $list = NULL;
                foreach ($verifyArr as $key) {
                    if (!in_array($key, $headings, true)) {
                        $list .= "<span class='tag tag-success'>{$key}</span>&nbsp;";
                    }
                }
                if (!is_null($list)) {
                    throw new WorkflowException(trans('exceptions.backend.upload.column_missing', ['column' => $list]));
                }


                /** start : Uploading excel data into the database */
                Excel::selectSheetsByIndex(0)
                    ->load($file)
                    ->chunk(500, function($result) use ($employerTask, $payroll_id) {
                        $rows = $result->toArray();
                        DB::beginTransaction();
                        //let's do more processing (change values in cells) here as needed
                        //$counter = 0;
                        foreach ($rows as $row) {
                            //Log::info($row);
                            /* Changing date from excel format to unix date format ... */
                            //Log::info($row['payment_date']);
                            if ($row['payment_date'] != "") {
                                $row['payment_date'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['payment_date'], 'YYYY-MM-DD');
                            }

                            $row['month'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['month'], 'YYYY-MM-DD');
                            Log::info($row['payment_date']);
                            /* start: Validating row entries */
                            $error_report = NULL;
                            $file_error = 0;
                            foreach ($row as $key => $value) {
                                if (trim($key) === 'payment_date') {
                                    if (trim($value) != "" And check_date_format($value) == 0) {
                                        $file_error = 1;
                                        $error_report .= trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                        $row[$key] = NULL;
                                    }
                                } elseif (in_array(trim($key), ['basic_salary', 'no_employees_verified'], true)) { //, 'no_employees_declared'
                                    if (trim($value) === "" Or $value === NULL Or !is_numeric($value)) { //$value == 0 Or
                                        logger($value);
                                        $file_error = 1;
                                        $error_report .= trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                        $row[$key] = NULL;
                                    }
                                } elseif (in_array(trim($key), ['allowances', 'non_fixed_allowances'], true)) {
                                    if (trim($value) != "" And (!is_numeric($value))) {
                                        $file_error = 1;
                                        $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                        $row[$key] = NULL;
                                    }
                                } elseif (in_array(trim($key), ['amount_contributed'], true)) {
                                    if (trim($value) != "") {
                                        if (!is_numeric($value)) {
                                            $file_error = 1;
                                            $error_report .= trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                            $row[$key] = NULL;
                                        }
                                        if ($row['amount_contributed'] > 0 And $row['payment_date'] == "") {
                                            $file_error = 1;
                                            $error_report .= trans('exceptions.backend.upload.entries', ['column' => "payment_date", 'entry' => $row['payment_date']]) . ", \r\n";
                                        }
                                    }
                                }
                            }
                            $contrib_amount = str_replace(",", "", $row['amount_contributed']);
                            $salary = str_replace(",", "", $row['basic_salary']);
                            $fixed_allowance = str_replace(",", "", $row['allowances']);

                            $employerTask->assessments()->where(['contrib_month' => $row['month'], "inspection_task_payroll_id" => $payroll_id])->update([
                                'contrib_amount' => $contrib_amount ?: NULL, //to be changed ...
                                'member_count_verified' => $row['no_employees_verified'],
                                'salary' => $salary ?: NULL,
                                'fixed_allowance' => $fixed_allowance ?: NULL,
                                //'non_fixed_allowance' => $row['non_fixed_allowances'],
                                'first_pay_date' => $row['payment_date'],
                                'file_error' => $file_error,
                                'error_report' => $error_report,
                            ]);

                        }

                        DB::commit();
                    }, false);

                DB::beginTransaction();

                $payroll = (new InspectionTaskPayrollRepository())->query()->where("id", $payroll_id)->first();

                $file_error = 0;
                $assessments = $employerTask->assessments()->where("inspection_task_payroll_id", $payroll_id);
                if ($assessments->where("file_error", 1)->count()) {
                    $employerTask->file_error = 2;
                    $employerTask->upload_error = "Uploaded file error";
                    $file_error = 2;
                    $payroll->hasassessment = 0;
                } else {
                    $employerTask->file_error = 0;
                    $employerTask->upload_error = NULL;
                    $payroll->hasassessment = 1;
                }
                $employerTask->save();
                $payroll->save();

                DB::commit();

                $sql = <<<SQL
REFRESH MATERIALIZED VIEW inspection_assessment_template;
SQL;
                DB::unprepared($sql);

                /// Refresh inspection assessment materialized view ...
                if (!$file_error) {

                    $this->updateContributionStatus($employerTask, $payroll_id);

                    ///=> update stage & progressive stage  EITPUPIASSC
                    $progressive_stage = $employerTask->progressive_stage;

                    if ($progressive_stage == 3) {
                        //Log::error($progressive_stage);
                        $employerTask->staging_cv_id = (new CodeValueRepository())->EITPUPIASSC(); //Uploaded Inspection Assessment Schedule
                        $employerTask->progressive_stage = 4; //Uploaded Assessment Schedule
                        $employerTask->save();
                        //$payroll->save();
                        //initialize employer inspection task
                        //Log::error("I am in");
                        $this->initialize($employerTask);
                    }
                }


                /** end : Uploading excel data into the database */
            } catch (\Exception $e) {
                $employerTask->file_error = 1;
                $employerTask->upload_error = $e->getMessage();
                $employerTask->save();
                $return = ['success' => false, "message" => $e->getMessage()];
            } catch (\Throwable $e) {
                $employerTask->file_error = 1;
                $employerTask->upload_error = $e->getMessage();
                $employerTask->save();
                $return = ['success' => false, "message" => $e->getMessage()];
            }

            return $return;
        });

        return $return;

    }

    /**
     * @param Model $employerTask
     * @param int $payroll_id
     */
    public function updateContributionStatus(Model $employerTask, $payroll_id = 0)
    {
        $inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
        if ($payroll_id) {
            $payrolls = $inspectionTaskPayrollRepo->query()->where("employer_inspection_task_id", $employerTask->id)->get();
        } else {
            $payrolls = $inspectionTaskPayrollRepo->query()->where("employer_inspection_task_id", $employerTask->id)->where("id", $payroll_id)->get();
        }
        foreach ($payrolls as $payroll) {
            ///=> check if compliant
            $assessmentsTemplate = (new InspectionAssessmentTemplateRepository())->query()->where("employer_inspection_task_id", $employerTask->id)->where("inspection_task_payroll_id", $payroll->id);
            $underpaid = $assessmentsTemplate->sum("accumulative_to_be_paid");
            $iscompliant = 1;
            if ($underpaid > 5) {
                $iscompliant = 0;
            }

            ///=> check if has overpayment
            $overpaid = $assessmentsTemplate->sum("overpaid");
            $hasoverpay = 0;
            if ($overpaid > 5) {
                $hasoverpay = 1;
            }

            $payroll->hasoverpay = $hasoverpay;
            $payroll->iscompliant = $iscompliant;
            $payroll->save();
        }

        //if (!$payroll_id) {
            ///=> check if compliant
        $assessmentsTemplate = (new InspectionAssessmentTemplateRepository())->query()->where("employer_inspection_task_id", $employerTask->id);
        $underpaid = $assessmentsTemplate->sum("accumulative_to_be_paid");
        $iscompliant = 1;
        if ($underpaid > 5) {
            $iscompliant = 0;
        }

        ///=> check if has overpayment
        $overpaid = $assessmentsTemplate->sum("overpaid");
        $hasoverpay = 0;
        if ($overpaid > 5) {
            $hasoverpay = 1;
        }

        $employerTask->hasoverpay = $hasoverpay;
        $employerTask->iscompliant = $iscompliant;
        $employerTask->save();
       // }

    }

    /**
     * @param Model $employer_task
     * @return mixed
     * @throws GeneralException
     */
    public function assessmentTemplate(Model $employer_task, $query = "", $inspection_task_payroll = 0)
    {
        //confirm visit date, review start date & review end date
        $msg = "";
        if (!$employer_task->visit_date)
            $msg .= " <span class='tag tag-success'>Visit Date</span> ";
        if (!$employer_task->review_start_date)
            $msg .= " <span class='tag tag-success'>Assessment Start Date</span> ";
        if (!$employer_task->review_end_date)
            $msg .= " <span class='tag tag-success'>Assessment End Date</span> ";
        //dd($msg);
        if (!empty($msg))
            throw new GeneralException("Please Update {$msg}");

        $employer_inspection_task_id = $employer_task->id;
        $employer = $employer_task->employer->name;
        $excelname = $employer . " (" . $employer_task->inspectionTask->inspection->filename . ")";
        if ($inspection_task_payroll) {
            $payroll = $employer_task->payrolls()->where("inspection_task_payrolls.id", $inspection_task_payroll)->first();
            if ($payroll) {
                $excelname .= " " .  preg_replace('/\s+/', '_', strtolower($payroll->name));
            }
        }
        if (empty($query)) {
            $query = "select contrib_month as month, member_count_verified no_employees_verified, salary basic_salary, fixed_allowance allowances, sys_contrib_amount amount_contributed_mac, contrib_amount amount_contributed, payment_date  from inspection_assessment_template where employer_inspection_task_id = {$employer_inspection_task_id} and  inspection_task_payroll_id = {$inspection_task_payroll} order by contrib_month asc;";
            //member_count no_employees_declared, non_fixed_allowance non_fixed_allowances,
        }
        $queryResult = DB::select(DB::raw($query));
        //dd($queryResult);

        $results = json_decode(json_encode($queryResult), true);
        return Excel::create($excelname, function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array) $results);
            });
        })->download('xlsx');
    }

    /**
     * @param Model $employer_task
     * @return mixed
     * @throws GeneralException
     */
    public function downloadAssessmentSchedule(Model $employer_task, $inspection_task_payroll)
    {
        $employer_inspection_task_id = $employer_task->id;
        $query = "select contrib_month_formatted as month, member_count no_employees_declared, member_count_verified no_employees_verified, member_count_difference member_difference, salary::text basic_salary, fixed_allowance allowances, non_fixed_allowance non_fixed_allowances, payroll_declared gross_declared, payroll_verified gross_verified, contrib_amount amount_contributed, contrib_amount_verified amount_to_be_contributed, underpaid, overpaid, payment_date, months_delayed_paid months_delayed_on_paid, months_delayed_unpaid months_delayed_on_unpaid, interest_on_unpaid, interest_on_paid, accumulative_to_be_paid  from inspection_assessment_template where employer_inspection_task_id = {$employer_inspection_task_id} and inspection_task_payroll_id = {$inspection_task_payroll} order by contrib_month asc;";
        return $this->assessmentTemplate($employer_task, $query);
    }

    /**
     * @param Model $employer_task
     * @return mixed
     * @throws GeneralException
     */
    public function downloadAssessmentError(Model $employer_task)
    {
        $employer_inspection_task_id = $employer_task->id;
        //dd($employer_inspection_task_id);
        $query = "select error_report, contrib_month as month, member_count no_employees_declared, member_count_verified no_employees_verified, salary basic_salary, fixed_allowance fixed_allowances, non_fixed_allowance non_fixed_allowances, contrib_amount amount_contributed, payment_date from inspection_assessment_template where employer_inspection_task_id = {$employer_inspection_task_id} and file_error = 1;";
        return $this->assessmentTemplate($employer_task, $query);
    }

    /**
     * @param Model $employer_task
     * @return mixed
     */
    public function getDocsAttachedNonRecurring(Model $employer_task)
    {
        $documentRepo = new DocumentRepository();
        $docs_all = $documentRepo->query()->where("document_group_id", 20)->pluck("id")->all();
        $docs_attached = $employer_task->documents()->where('employer_inspection_task_id', $employer_task->id)->whereIn('document_id',$docs_all)->get();
        return $docs_attached;
    }

    /**
     * @param $employerTask
     * @param $document_id
     * @return bool
     */
    public function checkIfDocumentExistFoEmployerTask($employerTask, $document_id)
    {
        $check = $employerTask->documents()->where('document_id', $document_id)->count();
        if($check)
        {
            $return = true;
        } else {
            $return = false;
        }
        return $return;
    }


    /*Save into pivot table of document*/
    public function savePivotDocument($employerTask, array $input)
    {
        $document_id = $input['document_id'];
        $file = request()->file('document_file');
        if($file->isValid()) {
            $employerTask->documents()->attach([$document_id => [
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
            ]]);
        }
    }

    /**
     * @param $employerTask
     * @param $document_id
     * @param $ext
     * @throws GeneralException
     */
    public function attachDocFileToStorage($employerTask, $document_id, $ext)
    {
        /*Attach document to server*/
        $uploaded_doc = $employerTask->documents()->where(['document_id' => $document_id, "employer_inspection_task_id" => $employerTask->id])->orderBy('id', 'desc')->first();
        $path = inspection_employer_task_dir() . DIRECTORY_SEPARATOR . $employerTask->id . DIRECTORY_SEPARATOR;
        $filename = $uploaded_doc->pivot->id . '.' . $ext;
        $this->makeDirectory($path);
        $this->saveDocumentBasic('document_file', $filename, $path );
    }

    public function getDocExtension($file_name_input)
    {
        $file = request()->file($file_name_input);
        if($file->isValid()) {
            return $file->getClientOriginalExtension();
        }else{
            return '';
        }
    }

    public function updatePivotDocument(array $input, $doc_pivot_id)
    {
        $file = request()->file('document_file');
        if($file->isValid()) {
            DB::table('document_employer_inspection_task')->where('id', $doc_pivot_id)->update([
                'name' => $file->getClientOriginalName(),
                'description' =>  isset($input['document_title']) ? $input['document_title'] : null,
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
                'ext' => $file->getClientOriginalExtension(),
                'date_reference' => isset($input['date_reference']) ? $input['date_reference'] : null,
            ]);
        }
    }

    /**
     * @param $doc_pivot_id
     */
    public function deleteDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer_inspection_task')->where('id', $doc_pivot_id)->first();
        $path = inspection_employer_task_dir() . DIRECTORY_SEPARATOR . $uploaded_doc->employer_inspection_task_id . DIRECTORY_SEPARATOR;
        $file_path = $path . $doc_pivot_id . '.' . $uploaded_doc->ext;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        /*delete*/
        DB::table('document_employer_inspection_task')->where('id', $doc_pivot_id)->delete();
    }

    /**
     * @param Model $employer_task
     * @param array $input
     * @return mixed
     */
    public function saveEmployerTaskDocuments(Model $employer_task, array $input)
    {
        return DB::transaction(function () use ($employer_task, $input) {
            $document_id = $input['document_id'];
            $check_if_recurring = (new DocumentRepository())->checkIfDocumentIsRecurring($document_id);
            $ext = $this->getDocExtension('document_file');

            if ($check_if_recurring) {
                //It is a recurring document
                /* Save for Recurring Documents */
                $this->savePivotDocument($employer_task, $input);
            } else {
                //It is not recurring document
                $check_if_exist = $this->checkIfDocumentExistFoEmployerTask($employer_task, $document_id);
                if ($check_if_exist) {
                    $uploaded_doc = $employer_task->documents()->where('document_id', $document_id)->orderBy('id', 'desc')->first();
                    $this->updatePivotDocument($input, $uploaded_doc->pivot->id );
                } else {
                    /*if not exists - Attach new*/
                    $this->savePivotDocument($employer_task, $input);
                }
            }
            /*Attach to storage*/
            $this->attachDocFileToStorage($employer_task, $document_id, $ext);
            return true;
        });
    }

    public function getEmployerListForDataTable($employer_id)
    {
        //$employer = new EmployerInspectionTaskRepository();
        //$query = $employer->query()->where("employer_id", $employer_id);
        $query = $this->query()
            ->select([
                DB::raw("employer_inspection_task.id"),
                DB::raw("a.name as inspection_type"),
                DB::raw("employer_inspection_task.visit_date"),
                DB::raw("b.name stage"),
                DB::raw("concat_ws(' ', users.firstname, users.lastname) inspector"),
            ])
            ->leftJoin("inspection_tasks", "inspection_tasks.id", "=", "employer_inspection_task.inspection_task_id")
/*            ->leftJoin("employer_inspection_task", "employer_inspection_task.inspection_task_id", "=", "inspection_tasks.id")*/
            ->leftJoin("employers", "employer_inspection_task.employer_id", "=", "employers.id")
            ->leftJoin(DB::raw('code_values a'), "inspection_tasks.inspection_type_cv_id", "=", "a.id")
            ->leftJoin(DB::raw('code_values b'), "employer_inspection_task.staging_cv_id", "=", "b.id")
            ->leftJoin(DB::raw('users'), "users.id", "=", "employer_inspection_task.allocated")
            ->leftJoin("districts", "employers.district_id", "=", "districts.id")
            ->leftJoin("regions", "employers.region_id", "=", "regions.id")
            ->where("employer_inspection_task.employer_id", $employer_id);
        return $query;
    }

    public function updateAttendedStatus(Model $employer_task)
    {
        return DB::transaction(function() use ($employer_task) {
            $employer_task->attended = 1;
            $employer_task->save();
            return true;
        });
    }

    public function sendInspectionNoticeToPortal($resource_id, $type){
        try {
            $inspect = $this->getEmployerPayrol($resource_id, $type);
            if (!is_null($inspect)) {

               $notice_date = $inspect->letter_date ?: $inspect->wf_done_date ?: $inspect->updated_at;
               $last_inspection_date = $inspect->last_inspection_date;
               $reference = $inspect->reference;

               $header = '';
               $description = '';

               $employer = Employer::find($inspect->employer_id);
               $user_ids = DB::table('portal.employer_user')->where('employer_id', $employer->id)->where('payroll_id', $inspect->payroll_id)->pluck('user_id');
               $portal_users = PortalUser::whereIn('id',$user_ids)->get();
               $url = portal_inspection_url_index($employer->id);


               switch ($type) {
                case 1:
                $header = 'Inspection Notice';
                $description = $this->inspectionNoticeEmailDescription($inspect->visit_date);
                break;
                case 2:
                $header = 'Demand Notice';
                $description = $this->demandNoticeEmailDescription($notice_date, $last_inspection_date, $reference);
                $this->saveArrears($resource_id);
                break;
                
                default:
                    # code...
                break;
            }

            foreach ($portal_users as $user) {
                $name = $user->firstname.' '.$user->lastname;
                $this->sendEmail($user, $employer, $description, $name, $url, $header);

            }
            $name = $employer->name;
            $this->sendEmail($employer, $employer, $description, $name, $url, $header);
        }
           

        } catch (\Exception $e) {
            logger($e->getMessage());
        }
    }

    public function saveArrears($resource_id){
        $assessment_detail = $this->getAssessmentTemplates($resource_id);
        $data = [];

        foreach ($assessment_detail as $assess) {
            $data = [
                'employer_id' => $assess->employer_id, 
                'payroll_id' => $assess->payroll_id ? $assess->payroll_id : 1,
                'contrib_month' => $assess->contrib_month,
                'inspection_template_id' => $assess->id,
            ];

            $inspection = ContributionArrear::where('inspection_template_id', $assess->id)->first();
            if (is_null($inspection)) {
                ContributionArrear::create($data);
            } else {
                $inspection->update($data);
            }
            
        }
    }

    public function getAssessmentTemplates($resource_id){
        $assessment_detail = $this->getEmployerPayrol($resource_id, 2);
        $assessment_template = DB::table('main.inspection_assessment_template')
        ->where('employer_id',$assessment_detail->employer_id)
        ->where('inspection_task_payroll_id', $assessment_detail->inspection_task_payroll_id)
        ->where('employer_inspection_task_id', $assessment_detail->employer_inspection_task_id)
        ->where('underpaid','>', 5)
        ->get();

        return $assessment_template;

    }

    public function sendEmail($user, $employer, $description, $name, $url, $header){
        \Notification::send($user, new InspectionNotices($user, $employer, $description, $name, $url, $header));
    }

    public function getEmployerPayrol($resource_id, $type){
        $resource = $this->returnResourceReference($resource_id);
        $inspect = null;
        if ($resource->resource_type == 'App\Models\Operation\Compliance\Inspection\EmployerInspectionTask' || $resource->reference == 'CLINNOLTR') {
            $inspect = DB::table('letters')->select('employer_inspection_task.employer_id','inspection_task_payrolls.payroll_id','employer_inspection_task.visit_date','letters.letter_date','letters.wf_done_date','letters.updated_at','employer_inspection_task.last_inspection_date','letters.reference','inspection_task_payrolls.id as inspection_task_payroll_id','inspection_task_payrolls.employer_inspection_task_id')
            ->join('employer_inspection_task','employer_inspection_task.id','=','letters.resource_id')
            ->join('inspection_task_payrolls','employer_inspection_task.id','=','inspection_task_payrolls.employer_inspection_task_id')
            // ->where('letters.resource_type', $resource->resource_type)
            ->where('letters.id', $resource_id)->first();

        } elseif($resource->resource_type == 'App\Models\Operation\Compliance\Inspection\InspectionTaskPayroll' || $resource->reference == 'CLDNDNOLTR') {
            $inspect = DB::table('letters')->select('employer_inspection_task.employer_id','inspection_task_payrolls.payroll_id','employer_inspection_task.visit_date','letters.letter_date','letters.wf_done_date','letters.updated_at','employer_inspection_task.last_inspection_date','letters.reference','inspection_task_payrolls.id as inspection_task_payroll_id','inspection_task_payrolls.employer_inspection_task_id')
            ->join('inspection_task_payrolls','letters.resource_id','=','inspection_task_payrolls.id')
            ->join('employer_inspection_task','employer_inspection_task.id','=','inspection_task_payrolls.employer_inspection_task_id')
            // ->where('letters.resource_type', $resource->resource_type)
            ->where('letters.id', $resource_id)->first();
        }
        
        return $inspect;
    }

    public function inspectionNoticeEmailDescription($date){

        $email = 'We would like to inform you that on '.Carbon::parse($date)->format('jS F Y').', the Fund shall conduct inspection through its dully Authorized Officers, at your office premises.';
        return $email;
    }

    public function demandNoticeEmailDescription($notice_date, $last_inspection_date, $reference){

        $email = 'The assessment of our inspection notice with reference number '.$reference.' dated '.Carbon::parse($notice_date)->format('jS F Y').' and the inspection exercise that was carried out on '.Carbon::parse($last_inspection_date)->format('jS F Y').' is completed.';

        return $email;
    }

    public function returnResourceReference($resource_id){

        $resources = DB::table('letters')->select('code_values.reference','letters.resource_type')
        ->join('code_values','code_values.id','=','letters.cv_id')
        ->where('letters.id', $resource_id)->first();

        return $resources;
    }

}