<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Repositories\BaseRepository;
use App\Models\Operation\Compliance\Inspection\InspectionType;

class InspectionTypeRepository extends BaseRepository
{
    const MODEL = InspectionType::class;

}