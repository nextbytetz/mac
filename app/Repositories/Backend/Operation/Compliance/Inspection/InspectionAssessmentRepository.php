<?php
/**
 * Created by PhpStorm.
 * User: gwanchi
 * Date: 11/23/19
 * Time: 11:55 AM
 */

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\InspectionAssessment;
use App\Repositories\BaseRepository;

class InspectionAssessmentRepository extends BaseRepository
{
    const MODEL = InspectionAssessment::class;



}