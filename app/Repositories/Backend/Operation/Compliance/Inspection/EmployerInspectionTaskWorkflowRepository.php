<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\EmployerInspectionTaskWorkflow;
use App\Repositories\BaseRepository;

class EmployerInspectionTaskWorkflowRepository extends BaseRepository
{
    const MODEL = EmployerInspectionTaskWorkflow::class;



}