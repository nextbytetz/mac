<?php
/**
 * Created by PhpStorm.
 * User: gwanchi
 * Date: 11/23/19
 * Time: 11:56 AM
 */

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\InspectionAssessmentTemplate;
use App\Repositories\BaseRepository;

class InspectionAssessmentTemplateRepository extends BaseRepository
{
    const MODEL = InspectionAssessmentTemplate::class;



}