<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\InspectionRemittance;
use App\Repositories\BaseRepository;

class InspectionRemittanceRepository extends BaseRepository
{
    const MODEL = InspectionRemittance::class;

}