<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Exceptions\WorkflowException;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Services\Storage\DataFormat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\VarDumper\Cloner\Data;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class InspectionTaskRepository
 * @author Erick Chrysostom
 * @package App\Repositories\Backend\Operation\Compliance\Inspection
 */
class InspectionTaskRepository extends BaseRepository
{
    const MODEL = InspectionTask::class;

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $inspection_task = $this->query()->find($id);

        if (!is_null($inspection_task)) {
            return $inspection_task;
        }
        throw new GeneralException(trans('exceptions.backend.compliance.inspection_task_not_found'));
    }

    public function getForDataTable($inspection_id)
    {
        return $this->query()->select([
            DB::raw("inspection_tasks.id"),
            DB::raw("inspection_tasks.name"),
            DB::raw("inspection_tasks.deliverable"),
            DB::raw("code_values.name inspection_type"),
        ])
            ->join("code_values", "code_values.id", "=", "inspection_tasks.inspection_type_cv_id")
            ->where("inspection_id", $inspection_id);
    }

    public function getEmployersForDatatable($task, array $input)
    {
        $return = $this->query()->select([
            DB::raw("employers.name employer"),
            DB::raw("employer_inspection_task.visit_date"),
            DB::raw("case when users.id is not null then concat_ws(' ', users.firstname, users.lastname) else '-' end staff"),
            DB::raw("employer_inspection_task.id"),
        ])
            ->join("employer_inspection_task", "employer_inspection_task.inspection_task_id", "=", "inspection_tasks.id")
            ->join("employers", "employer_inspection_task.employer_id", "=", "employers.id")
            ->leftJoin("users", "employer_inspection_task.allocated", "=", "users.id")
            ->where("inspection_tasks.id", $task);
        if ($input['user_id']) {
            $return->where("employer_inspection_task.allocated", $input['user_id']);
        }
        return $return;
    }

    public function store(array $input)
    {
        $return = DB::transaction(function () use ($input) {
                $codeValueRepo = new CodeValueRepository();

                $data = [
                    'name' => $input['name'],
                    'deliverable' => $input['deliverable'],
                    'inspection_id' => $input['inspection_id'],
                    'inspection_profile_id' => isset($input['inspection_profile_id']) ? $input['inspection_profile_id'] : NULL,
                    'employer_count' => isset($input['employer_count']) ? $input['employer_count'] : NULL,
                    'last_id' => $input['last_id'],
                    'target_employer' => $input['target_employer'],
                    'inspection_type_cv_id' => $input['inspection_type'],
                    'filename' => $input['filename'],
                ];
                $inspectionTask = $this->query()->create($data);
                $employers = [];

                if (isset($input["target_employer"])) {
                    switch ($input["target_employer"]) {
                        case 0:
                            //Selected Individual Employers
                            $employers_input = $input['employer'];
                            //extract
                            $inspectionTask->employers()->sync([]);
                            if (count($employers_input)) {
                                $inspectionTask->attachEmployers($employers_input);
                            }
                            break;
                        case 1:
                            //Selected Inspection Profile
                            $this->allocateFromProfile($inspectionTask, $input["inspection_profile_id"], $input["employer_count"]);
                            break;
                        case 2:
                            //Imported from Excel
                            $inspectionTask->employers()->sync([]);
                            try {
                                $this->allocatedFromExcel($inspectionTask);
                            } catch (\Exception $e) {
                                $this->query()->where("id", $inspectionTask->id)->delete();
                                return ['success' => false, "message" => $e->getMessage()];
                            } catch (\Throwable $e) {
                                $this->query()->where("id", $inspectionTask->id)->delete();
                                return ['success' => false, "message" => $e->getMessage()];
                            }
                            break;
                    }
                }
                $this->updateInitialStage($inspectionTask);
                $this->updatePayrolls($inspectionTask);
                $duplicateCount = $this->removeEmployerIfExist($inspectionTask);
                $inspection = $inspectionTask->inspection;
                if ($inspection->progressive_stage == 2) {
                    $inspection->employerTasks()->where("inspection_tasks.id", $inspectionTask->id)->update([
                        "progressive_stage" => 2, //Tasks Assigned to Staff
                        "staging_cv_id" => $codeValueRepo->EITPPLVDAT(), //Planing Visit Date
                    ]);
                }
                $return = ['success' => true, "id" => $inspectionTask->id, 'return_url' => route('backend.compliance.inspection.show', $inspectionTask->inspection_id)];

            return $return;
        });
        return $return;
    }

    /**
     * @param Model $task
     * @return bool
     * @throws WorkflowException
     */
    public function allocatedFromExcel(Model $task)
    {
        $file = inspection_task_dir() . DIRECTORY_SEPARATOR . $task->filename;
        //logger($file);
        $headings = Excel::selectSheetsByIndex(0)
            ->load($file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['regno'];
        $list = NULL;
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                $list .= "<span class='tag tag-success'>{$key}</span>&nbsp;";
            }
        }
        if (!is_null($list)) {
            throw new WorkflowException(trans('exceptions.backend.upload.column_missing', ['column' => $list]));
        }
        /** start : Uploading excel data into the database */
        Excel::selectSheetsByIndex(0)
            ->load($file, function($result) use ($task) {
                $rows = $result->get();
                //logger($rows);
                $ids = [];
                foreach ($rows as $row) {
                    $regno = trim($row['regno']);
                    $count = (new EmployerRepository())->query()->where("id", $regno)->count();
                    //logger($regno);
                    if (($regno) And ($count)) {
                        $ids[] = $row['regno'];
                    }
                }
                if (count($ids)) {
                    //Log::info($ids);
                    $task->attachEmployers($ids);
                }
            }, true);
        //DB::commit();
        return true;
    }

    public function updatePayrolls(Model $task)
    {
        $tasks = $task->employerTasks;
        //dd($tasks);
        foreach ($tasks as $task) {
            //dd($task);
            $payroll = $task->payrolls()->create([
                'name' => 'Default',
                'isactive' => 1,
                'employer_inspection_task_id' => $task->id,
                'isdefault' => 1, //default payroll for every employer ...
            ]);
        }
    }

    /**
     * @param Model $inspectionTask
     * @param array $input
     * @return mixed
     */
    public function update(Model $inspectionTask, array $input)
    {
        $return = DB::transaction(function () use ($input, $inspectionTask) {
            $data = [
                'name' => $input['name'],
                'deliverable' => $input['deliverable'],
                'inspection_id' => $input['inspection_id'],
                'inspection_profile_id' => isset($input['inspection_profile_id']) ? $input['inspection_profile_id'] : NULL,
                'employer_count' => isset($input['employer_count']) ? $input['employer_count'] : NULL,
                'target_employer' => $input['target_employer'],
                'inspection_type_cv_id' => $input['inspection_type'],
                'filename' => $input['filename'],
            ];
            $inspectionTask->update($data);
            $employers = [];
            if (isset($input["target_employer"])) {
                switch ($input["target_employer"]) {
                    case 0:
                        //Selected Individual Employers
                        $employers_input = $input['employer'];
                        $inspectionTask->employers()->sync([]);
                        if (count($employers_input)) {
                            $inspectionTask->attachEmployers($employers_input);
                        }
                        break;
                    case 1:
                        //Selected Inspection Profile....
                        $this->allocateFromProfile($inspectionTask, $input["inspection_profile_id"], $input["employer_count"]);
                        break;
                    case 2:
                        //Imported from Excel
                        if ($input['hasfile']) {
                            $inspectionTask->employers()->sync([]);
                            try {
                                $this->allocatedFromExcel($inspectionTask);
                            } catch (\Exception $e) {
                                $this->query()->where("id", $inspectionTask->id)->delete();
                                return ['success' => false, "message" => $e->getMessage()];
                            } catch (\Throwable $e) {
                                $this->query()->where("id", $inspectionTask->id)->delete();
                                return ['success' => false, "message" => $e->getMessage()];
                            }
                        }

                        break;
                    default:
                        if ($inspectionTask->employers()->count()) {
                            $inspectionTask->employers()->sync([]);
                        }
                        break;
                }
            } else {
                if ($inspectionTask->employers()->count()) {
                    $inspectionTask->employers()->sync([]);
                }
            }
            $this->updateInitialStage($inspectionTask);
            $this->updatePayrolls($inspectionTask);
            $duplicateCount = $this->removeEmployerIfExist($inspectionTask);
            $return = ['success' => true, "id" => $inspectionTask->id, 'return_url' => route('backend.compliance.inspection.task.show', $inspectionTask->id)];
            return $return;
        });
        return $return;
    }

    /**
     * @param Model $inspectionTask
     * @param $profileId
     * @param $count
     * @return bool
     * @throws GeneralException
     */
    public function allocateFromProfile(Model $inspectionTask, $profileId, $count)
    {
        $profile = new InspectionProfileRepository();
        $profileM = $profile->find($profileId);
        $dataFormat = new DataFormat();
        $query = $dataFormat->dbToArrayQuery($profileM->query);
        if (!$query->count()) {
            throw new GeneralException("exceptions.backend.inspection.profile.no_employer");
        }
        $inspectionTask->employers()->sync([]);
        $lastTask = $this->query()->where("inspection_profile_id", $profileId)->orderBy("id", "desc")->offset(1)->first();
        if ($lastTask) {
            $employers = $query->where("id", ">", $lastTask->last_id)->orderBy("id", "asc")->limit($count)->get()->pluck("id")->all();
            rsort($employers);
            if (count($employers) == $count) {
                $inspectionTask->last_id = $employers[0];
                $inspectionTask->save();
            } else {
                $remain = $count - count($employers);
                if (count($employers)) {
                    $inspectionTask->attachEmployers($employers);
                }
                $query = $dataFormat->dbToArrayQuery($profileM->query);
                $employers = $query->orderBy("id", "asc")->limit($remain)->get()->pluck("id")->all();
                rsort($employers);
                $inspectionTask->last_id = $employers[0];
                $inspectionTask->save();
            }
        } else {
            $employers = $query->orderBy("id", "asc")->limit($count)->get()->pluck("id")->all();
            rsort($employers);
            $inspectionTask->last_id = $employers[0];
            $inspectionTask->save();
        }
        $inspectionTask->attachEmployers($employers);
        return true;
    }

    public function destroy(Model $inspectionTask)
    {
        $inspectionTask->delete();
        return true;
    }

    public function getStaffs($q, $inspection_id)
    {
        $name = ($q) ;
        $user = new UserRepository();
        $data['items'] = $user->query()->select(['id', DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(middlename, ''), coalesce(lastname, '')) as staff")])->where(function ($query) use ($name) {
            $query->where("firstname", "like", "%$name%")->orWhere("middlename", "like", "%$name%")->orWhere("lastname", "like", "%$name%")->orWhere("username", "like", "%$name%");
        })->whereHas("inspections", function ($query) use ($inspection_id) {
            $query->where("inspections.id", $inspection_id);
        })->get()->toArray();


        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    public function updateEmployer(Model $employer_inspection_task, array $input)
    {
        return DB::transaction(function () use ($input, $employer_inspection_task) {
            $input['visit_date'] = fix_form_date($input['visit_date']);
            $employer_inspection_task->update($input);
            return $employer_inspection_task;
        });
    }

    /**
     * @param Model $inspectionTask
     * @return bool
     * @description Remove employer in this task if already present in other task of the same inspection
     * @throws WorkflowException
     */
    public function removeEmployerIfExist(Model $inspectionTask): bool
    {
        $employerInspectionTaskRepo = new EmployerInspectionTaskRepository();
        $inspection = $inspectionTask->inspection;
        $checkQuery = $this->query()
                    ->select(["employer_inspection_task.progressive_stage", "employer_inspection_task.id", "employer_inspection_task.employer_id"])
                    ->join("employer_inspection_task", "employer_inspection_task.inspection_task_id", "=", "inspection_tasks.id")
                    ->where("inspection_tasks.inspection_id", $inspection->id)
                    ->where("inspection_tasks.id", "<>", $inspectionTask->id)
                    ->whereIn("employer_inspection_task.employer_id", function ($query) use ($inspectionTask) {
                        $query->select(["employer_id"])->from("employer_inspection_task")->where("inspection_task_id", $inspectionTask->id);
                    });
        //$duplicateEmployerCount = $checkQuery->count();
        //$taskEmployerCount = $inspectionTask->employers()->count();
        //Log::info($duplicateEmployerCount);
        //Log::info($taskEmployerCount);
        //check for each employer_inspection_task is it has gone past approval stage
        /*if ($duplicateEmployerCount == $taskEmployerCount) {
            throw new WorkflowException('Action cancelled!, same list of employers already specified in other task(s) of this inspection.');
        }*/
        /*if ($duplicateEmployerCount) {
            $employers = $checkQuery->pluck('employer_id')->all();
            //delete for the current inspection task
            (new EmployerInspectionTaskRepository())->query()->where('inspection_task_id', $inspectionTask->id)->whereIn('employer_id', $employers)->delete();
        }*/
        $results = $checkQuery->get();
        foreach ($results as $result) {
            if ($result->progressive_stage >= 5) {
                //set parent_id
                $employerInspectionTaskRepo->query()->where('inspection_task_id', $inspectionTask->id)->where('employer_id', $result->employer_id)->update([
                    'parent_id' => $result->id,
                ]);
            } else {
                //delete current created inspection
                $employerInspectionTaskRepo->query()->where('inspection_task_id', $inspectionTask->id)->where('employer_id', $result->employer_id)->delete();
            }

        }
        return true;
    }

    /**
     * @param Model $inspectionTask
     */
    public function updateInitialStage(Model $inspectionTask)
    {
        (new EmployerInspectionTaskRepository())->query()->where("inspection_task_id", $inspectionTask->id)->update([
            'staging_cv_id' => (new CodeValueRepository())->EITPWAISPLAP(), //Waiting Inspection Plan Approval
        ]);
        //Update Last Review End Date
        $employerTasks = (new EmployerInspectionTaskRepository())->query()->where("inspection_task_id", $inspectionTask->id)->get();
        foreach ($employerTasks as $employerTask) {
            //get last employer_inspection_task if exists
            $recentInstance = (new EmployerInspectionTaskRepository())->query()->where("inspection_task_id", "<>", $inspectionTask->id)->where("employer_id", $employerTask->employer_id)->orderByDesc("id")->limit("1");
            if ($recentInstance->count()) {
                //Has recent inspection...
                $data = $recentInstance->first();
                $employerTask->last_review_end_date = $data->review_end_date;
            } else {
                //Has manual inspection...
                $recentInstance2 = (new EmployerInspectionLegacyRepository())->query()->where("employer_id", $inspectionTask->employer_id);
                if ($recentInstance2->count()) {
                    $data = $recentInstance2->first();
                    $employerTask->last_review_end_date = $data->review_end_date;
                } else {
                    //take from date of commencement...
                    $employerTask->last_review_end_date = $employerTask->employer->doc;
                }
            }
            $employerTask->save();
        }
    }

    public function initializeAllTasks(Model $inspection)
    {
        $tasks = $inspection->inspectionTasks;
        $employerInspectionTaskRepo = new EmployerInspectionTaskRepository();
        foreach ($tasks as $task) {
            $taskEmployers = $task->employers;
            foreach ($taskEmployers as $taskEmployer) {
                $employerInspectionTaskRepo->initialize($taskEmployer);
            }
        }
        return true;
    }

    public function assignAllocation(Model $inspection, array $input)
    {
        return DB::transaction(function () use ($inspection, $input) {
            if (isset($input['id']) And count($input['id'])) {
                $resources = (new EmployerInspectionTaskRepository())->query()->find($input['id']);
                if ($input['assign_status'] == 1) {
                    if (!$input['assigned_user']) {
                        throw new WorkflowException("The user to assign the incident has not been selected.");
                    }
                    //Assign to user
                    $codeValue = new CodeValueRepository();
                    foreach ($resources as $resource) {
                        if ($resource->attended) {
                            //Temporary Disable Checking whether the resource has been attended ...
                            //throw new WorkflowException("This employer inspection is already attended.");
                        }
                        if (!is_null($resource->allocated)) {
                            //Have been allocated before
                            logger("Have been allocated before");
                            $user = $resource->allocated;
                            $resource->allocated = $input['assigned_user'];
                            $resource->allocate_status = 0;
                            $resource->save();
                            //Update the respective checker
                            (new CheckerRepository())->update([
                                'resource_id' => $resource->id,
                                'checker_category_cv_id' => $codeValue->CCAEMPINSPCTNTSK(), //Employer Inspection Task
                                'user_id' => $user,
                                'replacer' => $input['assigned_user'],
                                'staging_cv_id' => $resource->staging_cv_id,
                            ]);
                        } else {
                            logger("Have never been allocated before");
                            //Have never been allocated before
                            $resource->allocated = $input['assigned_user'];
                            $resource->allocate_status = 0;
                            $resource->save();
                            (new EmployerInspectionTaskRepository())->initialize($resource, 1);
                        }
                    }
                }  elseif ($input['assign_status'] == 2) {
                    ////Re-allocate Evenly
                    if ($inspection->progressive_stage == 2) {
                        throw new WorkflowException("Unable to allocate evenly, inspection has already been approved");
                    }
                    $return = $this->reAllocateEvenly($inspection, $input['id']);
                }
            } else {
                throw new WorkflowException("Please select at least one employer inspection.");
            }
            return true;
        });
    }

    public function reAllocateEvenly(Model $inspection, array $employerTasks)
    {
        $output = DB::transaction(function () use ($inspection, $employerTasks) {
            $countUnallocated = (new EmployerInspectionTaskRepository())->query()->whereHas("inspectionTask", function ($query) use ($inspection) {
                $query->where("inspection_id", $inspection->id);
            })->whereNull("employer_inspection_task.allocated")->count();
            $countAllTasks = (new EmployerInspectionTaskRepository())->query()->whereHas("inspectionTask", function ($query) use ($inspection) {
                $query->where("inspection_id", $inspection->id);
            })->count();
            if ($countAllTasks == $countUnallocated) {
                $return = $this->assign($inspection);
                //Log::error($countUnallocated);
                //Log::error($inspection->id);
                //Log::error("Yes");
            } else {
                $return = $this->assign($inspection, $employerTasks);
                //Log::error("Yes");
            }
            return true;
        });
        return $output;
    }

    /**
     * Assign staffs who where selected for a specific inspection
     * to inspection task.
     * @author Erick Chrysostom <e.chrysostom@nextbyte.co.tz>
     * @version    Release: 1.0.0  (15/07/2017)
     * @version    Release: 1.2.0  (05/11/2017)
     * @since      Class available since Release 1.0.0
     * @param Model $task
     * @return mixed
     */
    public function assign(Model $inspection, array $employerTasks = [])
    {
        $output = DB::transaction(function () use ($inspection, $employerTasks) {
            $region = new RegionRepository();
            $employer = new EmployerRepository();
            $employerInspectionTask = new EmployerInspectionTaskRepository();

            if (count($employerTasks)) {
                $search_column = "employer_inspection_task.id";
                $taskArray = $employerTasks;
                $regionsQuery = $region->query()->select(['id'])->whereHas("employers", function ($query) use ($employerTasks, $search_column) {
                    $query->whereHas("inspectionTasks", function ($subQuery) use ($employerTasks, $search_column) {
                        $subQuery->whereIn($search_column, $employerTasks);
                    });
                });
                //get users who does not have pending tasks
                $usersQuery = $inspection->users()->select(["users.id"])->where(function ($query) use ($regionsQuery, $inspection) {
                    $query->whereDoesntHave("inspectionTasks", function ($query) use ($inspection) {
                        $query->where("progressive_stage", "<=", 3)->where("inspection_id", "<>", $inspection->id);
                    })->whereHas("inspectionTasks", function ($query) use ($regionsQuery) {
                        $query->whereHas("employers", function ($query) use ($regionsQuery) {
                            $query->whereIn("region_id", $regionsQuery->pluck("id")->all());
                        });
                    });
                })->orWhereDoesntHave('inspectionTasks', function ($query) use ($inspection) {
                    $query->where("inspection_id", $inspection->id);
                });
                //Log::info($usersQuery->pluck("users.id")->all());
                //return true;
            } else {
                //Log::error("I am at right place.");
                $search_column = "inspection_task_id";
                $taskArray = $inspection->inspectionTasks()->pluck("id")->all();
                //Log::error($taskArray);
                $regionsQuery = $region->query()->select(['id'])->whereHas("employers", function ($query) use ($taskArray, $search_column) {
                    $query->whereHas("inspectionTasks", function ($subQuery) use ($taskArray, $search_column) {
                        $subQuery->whereIn($search_column, $taskArray);
                    });
                });
                //get users who does not have pending tasks
                $usersQuery = $inspection->users()->select(["users.id"])->whereDoesntHave("inspectionTasks", function ($query) use ($inspection) {
                    $query->where("progressive_stage", "<=", 3)->where("inspection_tasks.inspection_id", "<>", $inspection->id);
                });
            }
            //dd($inspection);
            $region_count = $regionsQuery->count();
            $user_count = $usersQuery->count();
            //Log::info($region_count);
            //Log::info($user_count);
            //dd($usersQuery->toSql());
            //Log::info("I have passed here ...");
            try {
                if ($user_count) {
                    //Log::error("We have Users");
                    $users = $usersQuery->get()->pluck("id")->all();
                    //Log::error($users);
                    if ($region_count > $user_count) {
                        //throw new GeneralException(trans("exceptions.backend.inspection.task.many_regions", ['region' => $region_count, 'staff' => $user_count]));
                        $regions = $regionsQuery->limit($user_count)->get();
                    } else {
                        $regions = $regionsQuery->get();
                    }
                    $region_weight = [];
                    foreach ($regions as $region) {
                        $count = $employer->query()->whereHas("inspectionTasks", function ($subQuery) use ($taskArray, $search_column) {
                            $subQuery->whereIn($search_column, $taskArray);
                        })->where("region_id", $region->id)->count();
                        $region_weight[$region->id] = $count;
                    }
                    arsort($region_weight);
                    $av = (int) floor($user_count /  $region_count);
                    $av_mod = $user_count % $region_count;
                    $user_pointer = 0;
                    $global_offset = 0;
                    $counter = 0; /* Special loop counter */
                    $region_values = array_values($region_weight);
                    foreach ($region_weight as $key => $value) {
                        if ($av_mod > 0) {
                            $percentDecrease = (floor(($region_values[$counter] - $region_values[$counter + 1]) / $region_values[$counter]) * 100);
                            switch (true) {
                                case $percentDecrease >= 0 And $percentDecrease <= 50:
                                    $av_loop = $av + 1;
                                    $av_mod--;
                                    break;
                                default:
                                    $av_loop = $av + $av_mod;
                                    $av_mod = 0;
                            }
                        } else {
                            $av_loop = $av;
                        }
                        //Log::info($av_loop);
                        if ($av_loop) {
                            $av_region = (int) floor($value / $av_loop);
                            $av_mod_region = $value % $av_loop;
                            $offset = $global_offset;
                            $loop_user_pointer = $user_pointer;
                            for ($x = 1; $x <= $av_loop; $x++) {
                                $employers = $employer->query()->whereHas("inspectionTasks", function ($subQuery) use ($taskArray, $search_column) {
                                    $subQuery->whereIn($search_column, $taskArray);
                                })->where("region_id", $key)->orderBy("id")->limit($av_region)->offset($offset)->get()->pluck("id")->all();
                                $employerInspectionTask->query()->whereIn("employer_id", $employers)->whereIn($search_column, $taskArray)->update(["allocated" => $users[$loop_user_pointer]]);
                                $loop_user_pointer++;
                                $offset = $offset + $av_region;
                            }
                            $loop_user_pointer_mod = $user_pointer;
                            for ($x = 0; $x < $av_mod_region; $x++) {
                                $employers = $employer->query()->whereHas("inspectionTasks", function ($subQuery) use ($taskArray, $search_column) {
                                    $subQuery->whereIn($search_column, $taskArray);
                                })->where("region_id", $key)->orderBy("id")->limit(1)->offset($offset)->get()->pluck("id")->all();
                                $employerInspectionTask->query()->whereIn("employer_id", $employers)->whereIn($search_column, $taskArray)->update(["allocated" => $users[$loop_user_pointer_mod]]);
                                $loop_user_pointer_mod++;
                                $offset++;
                            }
                            //$global_offset = $offset;
                            $user_pointer = $loop_user_pointer;
                            $counter++;
                        }
                    }
                }
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            } catch (\Throwable $e) {
                Log::error($e->getMessage());
            }
            return true;
        });
        return $output;
    }

    /**
     * @param $task
     * @return array
     * @throws GeneralException
     */
    public function downloadEmployers($task)
    {
        $employer = new EmployerRepository();
        $user = new UserRepository();
        $user_id = request()->input("user_id");
        $space = ['name' => '', 'registration' => ''];
        $name = "All";
        $list = $employer->query()->select([DB::raw("reg_no as registration"), 'employers.name', DB::raw("regions.name as region")])->whereHas("inspectionTasks", function ($subQuery) use ($task, $user_id) {
            $subQuery->where("inspection_task_id", $task)->whereNotNull("employer_id");
            if ($user_id) { $subQuery->where("allocated", $user_id); }
        })->join('regions', 'employers.region_id', '=', 'regions.id')->get()->toArray();
        if ($user_id) {
            $name = $user->find($user_id)->name;
        }
        if (count($list)) {
            $heading = ['name' => trans('labels.backend.compliance_menu.inspection.registered'), 'registration' => ''];
            # add headers for each column in the CSV download
            array_unshift($list, $heading);
        }
        $assignee = [
            ["name" => "Staff : {$name}"],
            ["name" => ""],
        ];
        $list = array_merge($assignee, $list);
        return $list;
    }




    /**
     * ACTION WHEN CASE IS FOLLOW UP INSPECTION TYPE ==========================
     *
     */



    /**
     * @param $input
     * @return mixed
     * Create New
     */
    public function storeFollowup($input)
    {
        /**/
        return DB::transaction(function () use ($input) {
            $inspectionTask = $this->query()->create($input);
            /* Link to unreg employers*/
            $this->checkIfProfileHasRequiredNo($inspectionTask->id,$input);
            $this->linkToUnregisteredEmployers($inspectionTask->id,$input);
            /* Sync User */
            $this->syncStaff($inspectionTask, $input);
            return $inspectionTask;
        });
    }


    /**
     * @param $id
     * @param $input
     * @return mixed
     * Update
     */
    public function updateFollowup($id,$input)
    {
        /**/
        return DB::transaction(function () use ($id, $input) {
            $task = $this->findOrThrowException($id);
            if (isset($input["target_employer"]) And $input["target_employer"] != 1) {
                $input['inspection_profile_id'] = NULL;
                $input['employer_count'] = NULL;
                $input['last_id'] = NULL;
            }
            $task->update($input);

            /* Link to unreg employers*/
            $this->linkToUnregisteredEmployers($task->id,$input);
            /* Sync User */
            $this->syncStaff($task, $input);
            return $task;
        });
    }

    /**
     * @param $id
     * delete
     */
    public function destroyFollowup($id)
    {
        return DB::transaction(function () use ($id) {
            $task = $this->findOrThrowException($id);
            $this->unassignAllStaff($id);
            $this->detachUnregisteredEmployers($task->id);

            $task->delete();

        });
    }

    /**
     * Assigned unregistered employers with assigned staff
     */

    public function syncStaff(Model $task, $input){
        $staffs = [];
        $current = $input["staffs"];
        foreach ($current as $perm) {
            if (is_numeric($perm)) {
                array_push($staffs, $perm);
            }
        }
        $task->users()->sync($staffs);
    }


    public function linkToUnregisteredEmployers($id,$input)
    {
        switch ($input['target_employer'])  {
            case 0: //individual employers
                $this->linkToIndividualUnregisteredEmployers($id,$input);
                break;
            case 1: //profile employers

                $this->linkToUnregisteredEmployersUsingProfile($id,$input);
                break;
        }
    }


    public function linkToIndividualUnregisteredEmployers($id,$input)
    {
        /*detach*/
        $this->detachUnregisteredEmployers($id);
        /*Re sync*/
        foreach ($input as $key => $value) {
            switch ($key)  {
                case 'employer':
                    $unregistered_employers = new UnregisteredEmployerRepository();
                    $unregistered_employers->query()->whereIn('id', $value)->update(['inspection_task_id'=> $id]);
                    break;
            }
        }
    }


    public function linkToUnregisteredEmployersUsingProfile($id, $input)
    {
        $dataformat = new DataFormat();
        $unregistered_employers = new UnregisteredEmployerRepository();
        /*detach*/
        $this->detachUnregisteredEmployers($id);
        /*Re sync*/
        $employer_assigned_count = $unregistered_employers->query()->where('assign_to', '>', 0)->where('inspection_task_id', $id)->count();
        $employer_count_limit = $input['employer_count'] - $employer_assigned_count;

        $task = $this->findOrThrowException($id);
        $profile = $task->inspectionProfile;
        $query = $dataformat->dbToArrayQueryFollowup($profile->query);
        if ($employer_count_limit >= 0){
            $unregistered_employers_to_update =       $query->where('assign_to', 0)->limit($employer_count_limit)->get()->pluck('id');
            $unregistered_employers->query()->whereIn('id', $unregistered_employers_to_update)->update(['inspection_task_id' => $id]);
        }else{
            /*when number (employer count) is decreased*/
            $unregistered_employers_to_update =       $query->where('is_registered', 0)->limit($employer_count_limit * -1)->get()->pluck('id');
            $unregistered_employers->query()->whereIn('id', $unregistered_employers_to_update)->update(['inspection_task_id' => null, 'assign_to' => 0]);
        }

    }



    public function detachUnregisteredEmployers($id)
    {
        /*Detach*/
        $unregistered_employers = new UnregisteredEmployerRepository();
        $unregistered_employer_linked = $unregistered_employers->query()->where('inspection_task_id', $id)->where('assign_to',0)->where('is_registered',0)->get();

        if (count($unregistered_employer_linked) > 0)
        {
            $unregistered_employers->query()->where('is_registered',0)->where('inspection_task_id', $id)->update(['inspection_task_id' => null]);
        }
    }


    /**
     * @param $id
     * Unassign all staff when deleting the task
     */
    public function unassignAllStaff($id)
    {
        $unregistered_employers = new UnregisteredEmployerRepository();
        $unregistered_employers->query()->where('is_registered', 0)->where('inspection_task_id', $id)->update(['assign_to'=> 0]);
    }


    /**
     * @param Model $task
     * @return mixed
     * Assign Task
     */
    public function assignFollowup($id)
    {
        $output = DB::transaction(function () use ($id) {
            $task = $this->findOrThrowException($id);
            $region = new RegionRepository();
            $unregisteredEmployer = new UnregisteredEmployerRepository();
//            $employerInspectionTask = new EmployerInspectionTaskRepository();
            $regions = $region->query()->select(['id'])->whereHas("unregisteredEmployers", function ($query) use ($id) {
                $query->where("inspection_task_id", $id)->where('is_registered',0);
            });
            $region_count = count($regions->get()->toArray());
            $user_count = $task->users()->count();
            if ($region_count > $user_count) {
                throw new GeneralException(trans("exceptions.backend.inspection.task.many_regions", ['region' => $region_count, 'staff' => $user_count]));
            }
            $region_weight = [];
            foreach ($regions->get() as $region) {
                $count = $unregisteredEmployer->query()->where('is_registered',0)->where("inspection_task_id", $id)->where('is_registered',0)->where("region_id", $region->id)->count();
                $region_weight[$region->id] = $count;
            }
            arsort($region_weight);
            $av = (int) floor($user_count /  $region_count);
            $av_mod = $user_count % $region_count;
            $users = $task->users->pluck("id")->all();

            $user_pointer = 0;
            $global_offset = 0;
            $counter = 0; /* Special loop counter */
            $region_values = array_values($region_weight);

            foreach ($region_weight as $key => $value) {
                if ($av_mod > 0) {
                    $percentDecrease = (floor(($region_values[$counter] - $region_values[$counter + 1]) / $region_values[$counter]) * 100);
                    switch (true) {
                        case $percentDecrease > 0 And $percentDecrease <= 50:
                            $av_loop = $av + 1;
                            $av_mod--;
                            break;
                        default:
                            $av_loop = $av + $av_mod;
                            $av_mod = 0;
                    }
                } else {
                    $av_loop = $av;
                }
                $av_region = (int) floor($value / $av_loop);
                $av_mod_region = $value % $av_loop;
                $offset = $global_offset;
                $loop_user_pointer = $user_pointer;

                for ($x = 1; $x <= $av_loop; $x++) {
                    $unregistered_employers = $unregisteredEmployer->query()->where("inspection_task_id", $id)->where('is_registered',0)->where("region_id", $key)->orderBy("id")->limit($av_region)->offset($offset)->get()->pluck("id")->all();

                    $unregisteredEmployer->query()->whereIn("id", $unregistered_employers)->where("inspection_task_id", $task->id)->update(["assign_to" => $users[$loop_user_pointer], 'date_assigned' => Carbon::parse('now')->format('Y-n-j')]);

                    $loop_user_pointer++;
                    $offset = $offset + $av_region;
                }
                $loop_user_pointer_mod = $user_pointer;
                for ($x = 0; $x < $av_mod_region; $x++) {
                    $unregistered_employers = $unregisteredEmployer->query()->where("inspection_task_id", $id)->where('is_registered',0)->where("region_id", $key)->orderBy("id")->limit(1)->offset($offset)->get()->pluck("id")->all();
                    $unregisteredEmployer->query()->whereIn("id", $unregistered_employers)->where("inspection_task_id", $task->id)->where('is_registered',0)->update(["assign_to" => $users[$loop_user_pointer_mod], 'date_assigned' => Carbon::parse('now')->format('Y-n-j')]);
                    $loop_user_pointer_mod++;
                    $offset++;
                }
                //$global_offset = $offset;
                $user_pointer = $loop_user_pointer;
                $counter++;
            }
            return true;
        });
        return $output;
    }




//    public function getStaffs($q, $inspection_id)
//    {
//        $name = $q;
//        $user = new UserRepository();
//        $data['items'] = $user->query()->select(['id', DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(middlename, ''), coalesce(lastname, '')) as staff")])->where(function ($query) use ($name) {
//            $query->where("firstname", "like", "%$name%")->orWhere("middlename", "like", "%$name%")->orWhere("lastname", "like", "%$name%")->orWhere("username", "like", "%$name%");
//        })->whereHas("unregisteredFollowupInspections", function ($query) use ($inspection_id) {
//            $query->where("unregistered_followup_inspections.id", $inspection_id);
//        })->get()->toArray();
//        $data['total_count'] = count($data['items']);
//        return response()->json($data);
//    }



    public function checkIfProfileHasRequiredNo($id, $input)
    {
        if ($input['target_employer'] == 1) {
            $task = $this->findOrThrowException($id);
            $profile = $task->inspectionProfile;
            if ($task->inspection->inspection_type_id == 3){
                $employer_count = $profile->name_with_unregistered_count;
            }else{
                $employer_count = $profile->name_with_employer_count;
            }
            if ($profile->employers_count < $input['employer_count']) {
                throw new GeneralException(trans('exceptions.backend.compliance.profile_employer_count_validation'));
            }
        }
    }



    public function getEmployerListForDatatable($id)
    {

        return  $this->query()->find($id)->unregisteredEmployers;
    }



    public function downloadEmployersFollowup($id)
    {
        $task = $this->findOrThrowException($id);
        $unregisteredEmployer = new UnregisteredEmployerRepository();
        $user = new UserRepository();
        $user_id = access()->id();
        $space = ['name' => '', 'tin' => ''];
        if ($task->users()->where('user_id', $user_id)->first()) {
            $assignee = [
                ["name" => "Staff : " . $user->find($user_id)->name],
                ["name" => ""],
            ];
            $list = $unregisteredEmployer->query()->select([DB::raw("tin as tin"), 'unregistered_employers.name', DB::raw("regions.name as region")])->where("inspection_task_id", $task->id)->where("assign_to", $user_id)->join('regions', 'unregistered_employers.region_id', '=', 'regions.id')->get()->toArray();
            if (count($list)) {
                $heading = ['name' => trans('labels.backend.compliance_menu.inspection.unregistered'), 'tin' => ''];
                # add headers for each column in the CSV download
                array_unshift($list, $heading);
            }

        } else {
            $assignee = [
                ["name" => "Staff : All"],
                ["name" => ""],
            ];
            $list = $unregisteredEmployer->query()->select([DB::raw("tin as tin"), 'unregistered_employers.name', DB::raw("regions.name as region")])->where("inspection_task_id", $task->id)->join('regions', 'unregistered_employers.region_id', '=', 'regions.id')->get()->toArray();
            if (count($list)) {
                $heading = ['name' => trans('labels.backend.compliance_menu.inspection.unregistered'), 'tin' => ''];
                # add headers for each column in the CSV download
                array_unshift($list, $heading);
            }

        }
        return $list;
    }










    /* End of Follow up type -------------------*/












}