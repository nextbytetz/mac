<?php

namespace App\Repositories\Backend\Operation\Compliance\Inspection;

use App\Models\Operation\Compliance\Inspection\EmployerInspectionLegacy;
use App\Repositories\BaseRepository;

class EmployerInspectionLegacyRepository extends BaseRepository
{
    const MODEL = EmployerInspectionLegacy::class;



}