<?php

namespace App\Repositories\Backend\Investiment;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\DataTables\WorkflowTrackDataTable;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Investiment\FixedDepositUpdate;
use App\Models\Sysdef\DateRange;
use App\Models\Investiment\InvestimentFixedDeposit;
use App\Models\Investiment\InvestimentTreasuryBill;
use App\Models\Investiment\TreasuryBillsUpdate;
use App\Models\Investiment\InvestimentTreasuryBond;
use App\Models\Investiment\InvestimentCorporateBond;
use App\Models\Investment\InvestmentLoan;
use App\Models\Investment\InvestmentBudgetAllocation;
use App\Models\Investiment\BondIncrementDate;
use App\Models\Investiment\BillIncrementDate;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Investment\InvestmentEquity;
use App\Models\Investment\InvestmentCallAccount;
// use App\Price\PHPExcel_Calculation_Financial;


class InvestimentTypeRepository extends BaseRepository
{

	public function fixedDepositIndex(){
		$fin_years = DB::table('fin_years')->get();
		$currencies = DB::table('main.currencies')->get();
		$coupon_tenure = DB::table('main.coupon_tenure')
		->select('years','days','coupon_rate','id')->get();
		return view('backend.investment.investiment_types.fixed_deposit',compact('fin_years','currencies','coupon_tenure'));
	}

	public function treasuryBillIndex(){
		$fin_years = DB::table('fin_years')->get();
		$coupon_tenure = DB::table('main.coupon_tenure')
		->select('years','days','coupon_rate','id')->get();
		return view('backend.investment.investiment_types.treasury_bills',compact('fin_years','coupon_tenure'));
	}

	public function corporateBondIndex(){
		$fin_years = DB::table('fin_years')->get();
		$types = DB::table('investment_code_types')
		->where('code_value_reference','INTCBONDS')
		->pluck('name','id');
		$coupon_tenure = DB::table('main.coupon_tenure')
		->select('years','days','coupon_rate','id')->get();

		return view('backend.investment.investiment_types.corporate_bonds',compact('fin_years','types','coupon_tenure'));
	}

	public function treasuryBondIndex(){
		$fin_years = DB::table('fin_years')->get();
		$coupon_tenure = DB::table('main.coupon_tenure')
		->select('years','days','coupon_rate','id')->get();
		return view('backend.investment.investiment_types.treasury_bonds',compact('fin_years','coupon_tenure'));
	}
	public function callAccountIndex(){
		$fin_years = DB::table('fin_years')->get();
		return view('backend.investment.investiment_types.call_account.index',compact('fin_years'));
	}

	public function exchangeRate($amount_invested, $exchange_rate){
		return $amount_invested = round(($amount_invested * $exchange_rate), 4);

	}

	public function fixedDeposit($request){
		$budget = DB::table('main.investment_budgets')
		->where('fin_year_id',$request->fin_year_id)
		->first();

		if (count($budget) < 1) {
			return response()->json(array('error' =>'The chosen financial year in not active'));
		}else{
			$allocation = DB::table('main.investment_budget_allocation')
			->where('investment_budget_id',$budget->id)
			->where('code_value_reference','INTFDR')
			->first();
			if (count($allocation) < 1) {
				return response()->json(array('error' =>'The chosen Investment type is not allocated'));
				
			} else {
				$data = $request->except(['_token','certificate_copy','fin_year_id']);
				$data['opening_date'] = ($data['opening_date'] == null ? null : Carbon::parse($data['opening_date'])->format('Y-m-d'));
				$data['maturity_date'] = ($data['maturity_date'] == null ? null : Carbon::parse($data['maturity_date'])->format('Y-m-d')); 
				$data['payment_date_received'] = ($data['payment_date_received'] == null ? null : Carbon::parse($data['payment_date_received'])->format('Y-m-d'));
				//check rate
				if ($data['currency_id'] == 2) {
					$amount_invested = $this->exchangeRate($data['amount_invested'],$data['exchange_rate']);
					$data['amount_invested'] = $amount_invested;
				}

				$institution = Employer::find($request->employer_id);
				$institution_name = $institution->name;
				
			    //interest income
				$days = explode('-', $data['tenure']);
				$diff = $days[0];

				$interest_income = $this->interestIncome($data['tax_rate'],$data['interest_rate'],$data['amount_invested'],$data['opening_date']);
				$interest_receivable = $this->interestReceivable($data['tax_rate'],$data['interest_rate'],$data['amount_invested'],$diff);
				$outstanding_maturity = $this->outstandingAtMaturity($data['tax_rate'],$data['amount_invested'],$data['interest_rate'],$diff);

				$outstanding_received = $this->outstandingReceived((float)$request->principal_redeemed,(float)$request->interest_received);
				$outstanding = $outstanding_maturity - $outstanding_received;
				$penalty = $this->fixedDepositPenalty($data['maturity_date'],$data['penalty_rate'],$outstanding,$data['interest_rate'],$data['tax_rate']);
				$p = $this->checkPenalties();
				// dump($p);
				// die;
				$penalty_income = $penalty['penalty_income'];
				$penalty_receivable = $penalty['penalty_receivable'];

				// save weighted_average_return to entrance
				$this->fixedWar();

				$data['interest_income'] = $interest_income;
				$data['interest_receivable'] = $interest_receivable;
				$data['penalty_income'] = $penalty_income;
				$data['penalty_receivable'] = $penalty_receivable;
				$data['overpaid_interest'] = $outstanding;
				$data['institution_name'] = $institution_name;

				$data = array_merge($data,['investment_budget_id' => $budget->id]);
				$data = array_merge($data,['investment_budget_allocation_id' => $allocation->id]);
				$create = InvestimentFixedDeposit::create($data);
				$fixed_deposit_id = DB::getPdo()->lastInsertId();
				return response()->json(array('success' => $this->returnRedirectResponse($fixed_deposit_id,$request->fin_year_id,"INTFDR")));;
			}

		}
		

	}

	public function fixedDepositStoreUpdate($request){
		$budget = DB::table('main.investment_budgets')
		->where('fin_year_id',$request->fin_year_id)
		->first();

		if (count($budget) < 1) {
			return response()->json(array('error' =>'The chosen financial year in not active'));
		}else{
			$allocation = DB::table('main.investment_budget_allocation')
			->where('investment_budget_id',$budget->id)
			->where('code_value_reference','INTFDR')
			->first();
			if (count($allocation) < 1) {
				return response()->json(array('error' =>'The chosen Investment type is not allocated'));

			} else {
				$data = $request->except(['_token','certificate_copy','fin_year_id']);
				$data['opening_date'] = ($data['opening_date'] == null ? null : Carbon::parse($data['opening_date'])->format('Y-m-d'));
				$data['maturity_date'] = ($data['maturity_date'] == null ? null : Carbon::parse($data['maturity_date'])->format('Y-m-d')); 
				$data['payment_date_received'] = ($data['payment_date_received'] == null ? null : Carbon::parse($data['payment_date_received'])->format('Y-m-d'));
				$institution = Employer::find($request->employer_id);
				$institution_name = $institution->name;
				$data['institution_name'] = $institution_name;

				$fixed_deposit = InvestimentFixedDeposit::find($request->id);
				$fixed_deposit->update($data);
				return response()->json(array('success' => $this->returnRedirectResponse($fixed_deposit->id,$request->fin_year_id,"INTFDR")));
			}			

		}
		

	}

	public function penaltyReceivable($penalty_rate,$amount_invested,$interest_rate,$tax_rate,$diff){
		$penalty_rate = ($penalty_rate / 100);
		$outstanding_maturity = $this->outstandingAtMaturity($tax_rate,$amount_invested,$interest_rate,$diff);
		$outstanding_maturity = $this->outstandingAtMaturityPenalty($tax_rate,$outstanding_maturity,$interest_rate,$diff);

		$penalty_receivable = round(($penalty_rate * $outstanding_maturity * $diff) / 365 ,2);
		return $penalty_receivable;

	}

	public function penaltyIncome($penalty_rate,$amount_invested,$interest_rate,$tax_rate,$diff){
		$penalty_rate = ($penalty_rate / 100);
		$outstanding_maturity = $this->outstandingAtMaturity($tax_rate,$amount_invested,$interest_rate,$diff);
		$outstanding_maturity = $this->outstandingAtMaturityPenalty($tax_rate,$outstanding_maturity,$interest_rate,$diff);

		$penalty_income = round(($penalty_rate * $outstanding_maturity * $diff) / 365 ,2);
		return $penalty_income;

	}

	public function interestReceivable($tax,$interest_rate,$principle_amount,$diff){
		$tax = ($tax / 100);
		$interest_rate = ($interest_rate / 100);

		$interest_receivable = ($interest_rate * $principle_amount * $diff) / 365;
		$withholding_tax = ($interest_receivable * $tax);

		$interest_receivable = $interest_receivable - $withholding_tax;
		return round($interest_receivable, 4);

	}

	public function interestIncome($tax,$interest_rate,$principle_amount,$start_date){
		$tax = ($tax / 100);
		$interest_rate = ($interest_rate / 100);
		$now = Carbon::now();
		$start_date = Carbon::parse($start_date);
		$diff = $now->diffInDays($start_date);

		$interest_income = ($interest_rate * $principle_amount * $diff) / 365;

		$withholding_tax = ($interest_income * $tax);

		$interest_income = $interest_income - $withholding_tax;

		return round($interest_income, 4);


	}

	public function getFixedDeposit($fin_year_id){
		$fixed_deposits = InvestimentFixedDeposit::join('investment_budgets','investment_budgets.id','=','investiment_fixed_deposits.investment_budget_id')->where('fin_year_id',$fin_year_id)->get();
		return json_decode(json_encode($fixed_deposits));

	}

	public function war($interest_rate,$principle_amount,$total_principle_amount){

		$interest_rate = round(($interest_rate / 100), 2);
		return $war = round((($interest_rate * $principle_amount)/$total_principle_amount),2);
	}

	public function updateFixedDeposit($request){
		if($request->interest_received == null and $request->principal_redeemed == null and $request->penalty_received == null and $request->payment_date_received == null){

			$fixed = InvestimentFixedDeposit::where('id',$request->investment_fixed_deposit_id)
			->update(['certificate_number' => $request->certificate_number]);
			return response()->json(array('success' =>'success'));
		}
		if ($data['currency_id'] == 2) {
			
			if ($data['interest_received'] != null) {
				$interest_received = $this->exchangeRate($data['interest_received'],$data['exchange_rate']);
				$data['interest_received'] = $interest_received;
			} elseif($data['principal_redeemed'] != null) {
				$principal_redeemed = $this->exchangeRate($data['principal_redeemed'],$data['exchange_rate']);
				$data['principal_redeemed'] = $principal_redeemed;
			}elseif($data['penalty_received'] != null){
				$penalty_received = $this->exchangeRate($data['penalty_received'],$data['exchange_rate']);
				$data['penalty_received'] = $penalty_received;
			}
			
		}

		$fixed = InvestimentFixedDeposit::where('id',$request->investment_fixed_deposit_id)
		->update(['certificate_number' => $request->certificate_number]);

		$fixed = InvestimentFixedDeposit::find($request->investment_fixed_deposit_id);

		$days = explode('-', $fixed->tenure);
		$diff = $days[0];
		$interest_income = $this->interestIncome($fixed->tax_rate,$fixed->interest_rate,$fixed->amount_invested,$fixed->opening_date);
		$interest_receivable = $this->interestReceivable($fixed->tax_rate,$fixed->interest_rate,$fixed->amount_invested,$diff);
		$total = InvestimentFixedDeposit::select(DB::raw('sum(amount_invested) as total_principle_amount'))
		->where('is_paid',null)->first();

		$war = $this->war($fixed->interest_rate,$fixed->amount_invested,$total->total_principle_amount);

		$outstanding_maturity = $this->outstandingAtMaturity($fixed->tax_rate,$fixed->amount_invested,$fixed->interest_rate,$diff);
		// dump($outstanding_maturity);

		$outstanding_received = $this->outstandingReceived($request->principal_redeemed,$request->interest_received);
        // dump($outstanding_received);
		$principal_redeemed = $request->principal_redeemed;
		$interest_received = $fixed->interest_received + $request->interest_received;
		// dump($principal_redeemed);
		// dump($interest_received);

		$total_outstanding_received = $this->outstandingReceived($principal_redeemed,$interest_received);
		// save weighted_average_return to entrance
		$this->fixedWar();

		$outstanding = $outstanding_maturity - $total_outstanding_received;

		$penalty = $this->fixedDepositPenalty($fixed->maturity_date,$fixed->penalty_rate,$outstanding,$fixed->interest_rate,$fixed->tax_rate);

		$penalty_receivable = $penalty['penalty_receivable'];
		$penalty_income = $penalty['penalty_income'];

		//penalty received
		$penalty_received = $fixed->penalty_received + $data['penalty_received'];
		if ($penalty_received == $penalty_receivable) {
			$penalty_receivable = 0;
			$penalty_income = 0;
		}
		
		$fixed->interest_received = $interest_received;
		$fixed->principal_redeemed = $principal_redeemed;
		$fixed->interest_income = $interest_income;
		$fixed->interest_receivable = $interest_receivable;
		$fixed->weighted_average_return = $war;
		$fixed->penalty_income = $penalty_income;
		$fixed->penalty_receivable = $penalty_receivable;
		$fixed->penalty_received = $penalty_received;
		$fixed->overpaid_interest = $outstanding;
		$is_paid = ($outstanding < 0 ) ? 1 : null;
		$fixed->is_paid = $is_paid;
		$fixed->save();

		$data = $request->except(['_token','certificate_number']);
		$data['interest_received'] = $interest_received;
		$data['principal_redeemed'] = $principal_redeemed;
		$data['interest_income'] = $interest_income;
		$data['interest_receivable'] = $interest_receivable;
		$data['weighted_average_return'] = $war;
		$data['penalty_income'] = $penalty_income;
		$data['penalty_receivable'] = $penalty_receivable;
		$data['penalty_received'] = $data['penalty_received'];
		$data['overpaid_interest'] = $outstanding;

		$create = FixedDepositUpdate::create($data);
		return response()->json(array('success' =>'success'));

	}

	//check not paid up to maturity to penalty them 
	public function checkPenalties(){

		$fixed_deposits = InvestimentFixedDeposit::where('is_paid',null)->get();

		foreach ($fixed_deposits as $f) {
			$a = explode('-',$f->tenure);
			$diff = $a[0];

			$outstanding_maturity = $this->outstandingAtMaturity($f->tax_rate,$f->amount_invested,$f->interest_rate,(int)$diff);
			$outstanding_received = $this->outstandingReceived($f->principal_redeemed,$f->interest_received);
			$outstanding = $outstanding_maturity - $outstanding_received;
			$penalty = $this->fixedDepositPenalty($f->maturity_date,$f->penalty_rate,$outstanding,$f->interest_rate,$f->tax_rate);

		// saving penalties
			$f->penalty_receivable = $penalty['penalty_receivable'];
			$f->penalty_income = $penalty['penalty_income'];
			$f->save();

		}

		// return 'Success';
	}

	public function fixedDepositPenalty($maturity_date,$penalty_rate,$outstanding,$interest_rate,$tax_rate){
		$penalt_start = Carbon::parse($maturity_date)->addDays(3)->format('Y-m-d');
		if ($penalt_start <= Carbon::now()->format('Y-m-d')) {
			$now = Carbon::now();
			$maturity_date = Carbon::parse($maturity_date);
			$diff = $now->diffInDays($maturity_date);
			$penalty_receivable = $this->penaltyReceivable($penalty_rate,$outstanding,$interest_rate,$tax_rate,$diff);
			$now = Carbon::now();
			$maturity_date = Carbon::parse($maturity_date);
			$diff = $now->diffInDays($maturity_date);
			$penalty_income = $this->penaltyIncome($penalty_rate,$outstanding,$interest_rate,$tax_rate,$diff);
		}else{
			$penalty_receivable = 0;
			$penalty_income = 0;
		}
		$penalty = [];
		$penalty = [
			'penalty_receivable' => $penalty_receivable,
			'penalty_income' => $penalty_income
		];

		return $penalty;
	}

	public function treasuryBondsUpdate($request){
		if ($request->holding_number != null) {
			$tbond = InvestimentTreasuryBond::find($request->investment_id);
			$tbond->holding_number = $request->holding_number;
			$tbond->save();

		}
		$bond = BondIncrementDate::where([
			'treasury_bond_id'=>$request->investment_id,
			'interest_date'=>$request->interest_date
		])->first();

		if (count($bond) > 0) {
			//save
			$interest_earned = (float)$bond->interest_earned + $request->interest_earned;
			$bond->payment_date = $request->payment_date;
			$bond->interest_earned = $interest_earned;
			$bond->save();
			return response()->json(array('success' =>'success'));
		} else {
			return response()->json(array('error' =>'Please Enter Valid Interest Date')); 

		}

	}

	public function treasuryBillsUpdate($request){
		$tbill = InvestimentTreasuryBill::find($request->investment_id);
		$interest_earned = (float)$tbill->receivable + $request->interest_earned;
		$tbill->holding_number = $request->holding_number;
		$tbill->receivable = $interest_earned;
		$tbill->save();

		$create = [];
		$create['treasury_bill_id'] = $tbill->id;
		$create['interest_earned'] = $request->interest_earned;
		$create['payment_date'] = Carbon::parse($request->payment_date)->format('Y-m-d');

		TreasuryBillsUpdate::create($create);

		return response()->json(array('success' =>'success'));

	}

	public function corporateBondsUpdate($request){
		// dd($request);
		$bond = BondIncrementDate::where([
			'corporate_bond_id'=>$request->investment_id,
			'interest_date'=>$request->interest_date
		])->first();

		if (count($bond) > 0) {
			//save
			if ($bond->interest_amount == $request->interest_earned) {

				$bond->payment_date = $request->payment_date;
				$bond->interest_earned = $request->interest_earned;
				$bond->save();
				return response()->json(array('success' =>'success'));

			} else {
				return response()->json(array('error' =>'The Interest Received must be equal to the required interest amount'));
			}
			
		} else {
			return response()->json(array('error' =>'Please Enter Valid Interest Date')); 

		}

	}

	public function fixedWar(){
		$total = InvestimentFixedDeposit::select(DB::raw('sum(amount_invested) as total_principle_amount'))
		->where('is_paid',null)->first();
		$fixed_deposits = InvestimentFixedDeposit::all();

		foreach ($fixed_deposits as $f) {
			$war = $this->war($f->interest_rate,$f->amount_invested,$total->total_principle_amount);
			$f->weighted_average_return = $war;
			$f->save();
		}

		
	}

	public function outstandingAtMaturityPenalty($tax_rate,$outstanding_maturity,$interest_rate,$n){
		$diff = ($n/365);

		$net_interest = $this->interestReceivable($tax_rate,$interest_rate,$amount_invested,$diff);
		$new_outstanding_maturity = $outstanding_maturity + $net_interest;

		return round($new_outstanding_maturity,2);

	}

	public function outstandingAtMaturity($tax_rate,$amount_invested,$interest_rate,$diff){

		$net_interest = $this->interestReceivable($tax_rate,$interest_rate,$amount_invested,$diff);
		$outstanding_maturity = $amount_invested + $net_interest;

		return round($outstanding_maturity,4);

	}

	public function outstandingReceived($principal_redeemed,$interest_received){
		$outstanding_received = $principal_redeemed + $interest_received;

		return round($outstanding_received,2);

	}

	public function treasuryBills($request){
		$budget = DB::table('main.investment_budgets')
		->where('fin_year_id',$request->fin_year_id)
		->first();

		if (count($budget) < 1) {
			return response()->json(array('error' =>'The chosen financial year in not active'));
		}else{
			$first_coupon_date = Carbon::parse($request->first_coupon_date);
			$next_coupon_date = Carbon::parse($request->next_coupon_date);
			$diff_in_days = $first_coupon_date->diffInDays($next_coupon_date);

			$allocation = DB::table('main.investment_budget_allocation')
			->where('investment_budget_id',$budget->id)
			->where('code_value_reference','INTTBILLS')
			->first();
			if (count($allocation) < 1) {
				return response()->json(array('error' =>'The chosen Investment type is not allocated'));
				
			// }elseif($diff_in_days > 365){
			// 	return response()->json(array('error' =>'The difference between first coupon date and next coupon date must be less than 365 days'));

			}else {
				$data = $request->except(['_token','certificate_copy','fin_year_id']);
				$data['settlement_date'] = ($data['settlement_date'] == null ? null : Carbon::parse($data['settlement_date'])->format('Y-m-d'));
				$data['maturity_date'] = ($data['maturity_date'] == null ? null : Carbon::parse($data['maturity_date'])->format('Y-m-d')); 
				$data['auction_date'] = ($data['auction_date'] == null ? null : Carbon::parse($data['auction_date'])->format('Y-m-d'));
				$days = explode('-', $data['tenure']);
				$diff = $days[0];

				$cost = $this->cost($data['amount_invested'],$data['price']);
				// dump((float)$data['tenure']);
				// dump($cost);
				$interest_income = $this->tbIncome($data['amount_invested'], $cost);
				$interest_receivable = $this->tbReceivable($data['amount_invested'], $cost);
				$interest_amount = $data['amount_invested'] - $cost;
				// dump($interest_amount);

				$yield = $this->tbYield($cost,(float)$data['tenure'],$interest_amount);
				// $yield = $data['yield'];
				// dump($yield);
				// die;

				$total = InvestimentTreasuryBill::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();

				$war = $this->tbWar($yield, $cost, $total->total_principle_amount);

				$data['interest_income'] = $interest_income;
				$data['interest_receivable'] = $interest_receivable;
				$data['cost'] = $cost;
				$data['yield'] = $yield;
				$data['weighted_average_return'] = $war;				

				$data = array_merge($data,['investment_budget_id' => $budget->id]);
				$data = array_merge($data,['investment_budget_allocation_id' => $allocation->id]);
				$create = InvestimentTreasuryBill::create($data);
				$treasury_bill_id = DB::getPdo()->lastInsertId();

				return response()->json(array('success' => $this->returnRedirectResponse($treasury_bill_id,$request->fin_year_id,"INTTBILLS")));
			}

		}


	}

	public function treasuryBillsStoreUpdate($request){
		$budget = DB::table('main.investment_budgets')
		->where('fin_year_id',$request->fin_year_id)
		->first();

		if (count($budget) < 1) {
			return response()->json(array('error' =>'The chosen financial year in not active'));
		}else{
			$first_coupon_date = Carbon::parse($request->first_coupon_date);
			$next_coupon_date = Carbon::parse($request->next_coupon_date);
			$diff_in_days = $first_coupon_date->diffInDays($next_coupon_date);

			$allocation = DB::table('main.investment_budget_allocation')
			->where('investment_budget_id',$budget->id)
			->where('code_value_reference','INTTBILLS')
			->first();
			if (count($allocation) < 1) {
				return response()->json(array('error' =>'The chosen Investment type is not allocated'));
				
			// }elseif($diff_in_days > 365){
			// 	return response()->json(array('error' =>'The difference between first coupon date and next coupon date must be less than 365 days'));

			}
			else {
				$data = $request->except(['_token','certificate_copy','fin_year_id']);
				$data['settlement_date'] = ($data['settlement_date'] == null ? null : Carbon::parse($data['settlement_date'])->format('Y-m-d'));
				$data['maturity_date'] = ($data['maturity_date'] == null ? null : Carbon::parse($data['maturity_date'])->format('Y-m-d')); 
				$data['auction_date'] = ($data['auction_date'] == null ? null : Carbon::parse($data['auction_date'])->format('Y-m-d'));
				$days = explode('-', $data['tenure']);
				$diff = $days[0];

				$cost = $this->cost($data['amount_invested'],$data['price']);
				$interest_income = $this->tbIncome($data['amount_invested'], $cost);
				$interest_receivable = $this->tbReceivable($data['amount_invested'], $cost);
				$interest_amount = $data['amount_invested'] - $cost;
				$yield = $this->tbYield($cost,(float)$data['tenure'],$interest_amount);
				$total = InvestimentTreasuryBill::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();

				$war = $this->tbWar($yield, $cost, $total->total_principle_amount);

				$data['interest_income'] = $interest_income;
				$data['interest_receivable'] = $interest_receivable;
				$data['cost'] = $cost;
				$data['yield'] = $yield;
				$data['weighted_average_return'] = $war;				

				$data = array_merge($data,['investment_budget_id' => $budget->id]);
				$data = array_merge($data,['investment_budget_allocation_id' => $allocation->id]);

				$treasury_bill = InvestimentTreasuryBill::find($request->id);
				$update = $treasury_bill->update($data);

				return response()->json(array('success' => $this->returnRedirectResponse($request->id,$request->fin_year_id,"INTTBILLS")));
			}

		}
	}

	public function tbWar($yield, $cost, $total_cost){
		$tbWar = (($yield * $cost) / $total_cost);
		return round($tbWar, 4);
	}

	public function tbYield($cost,$tenure,$interest_amount){
		$a = ($interest_amount / $cost);
		$b = (364 / $tenure);
		$yield = ($a * $b * 100);
		return round($yield, 4);

	}


	public function tbYield2($price){
		return round((100 - $price) ,4);
       // return round(((100 - $price) / 100) ,4);
       // return round(($interest_income / 182) ,4);
	}

	public function tbDocYield($interest_income){
		return round(($interest_income / 365) ,4);
       // return round(($interest_income / 182) ,4);
	}

	public function tbIncome($face_value,$cost){
		return round(($face_value - $cost),4);
	}

	public function tbReceivable($face_value,$cost){
		return round(($face_value - $cost),4);
	}

	public function cost($face_value, $price){
		return $cost = round((($face_value * $price) / 100),2);
	}

	public function getTreasuryBills($fin_year_id){
		$fixed_deposits = InvestimentTreasuryBill::join('investment_budgets','investment_budgets.id','=','investiment_treasury_bills.investment_budget_id')->where('fin_year_id',$fin_year_id)->get();
		return json_decode(json_encode($fixed_deposits));

	}

	public function price($settlement, $maturity, $rate, $yield, $redemption, $frequency, $basis){
		$price = new \PHPExcel_Calculation_Financial();
		$price = $price->PRICE($settlement, $maturity, $rate, $yield, $redemption, $frequency, $basis);
		// $price = ($price - (float)'0.0181‬') ;
		return (round($price,4));
	}

	public function accruedInterestPriceTwo($coupon_rate,$settlement_date){
		$coupon_rate = ($coupon_rate / 100);
		$now = Carbon::now();
		$settlement_date = Carbon::parse($settlement_date);
		$diff_now = $settlement_date->diffInDays($now);

		$accrued_interest_price = ((($coupon_rate * $diff_now) / 365) * 100);
		return round($accrued_interest_price, 2);

	}

	public function accruedInterestPrice($coupon_rate,$tenure){
		$coupon_rate = ($coupon_rate / 100);
		$accrued_interest_price = ((($coupon_rate * $tenure) / 365) * 100);
		return round($accrued_interest_price, 2);

	}

	public function checkAuctionNumberDates($auction_number, $maturity_date, $table){
		$array = array();
		$auction = DB::table(''.$table.'')
		->select('maturity_date')
		->where('auction_number',$auction_number)
		->first();

		if ($auction) {
			$array['exists'] = true;
			if ($maturity_date == $auction->maturity_date) {
				$array['proceed'] = true;
			}else{
				$array['proceed'] = false;
				$array['maturity_date'] = $auction->maturity_date;
			}
			# code...
		}else{
			$array['exists'] = false;
		}

		return $array;

	}

	public function treasuryBonds($request){
		$budget = DB::table('main.investment_budgets')
		->where('fin_year_id',$request->fin_year_id)
		->first();

		if (count($budget) < 1) {
			return response()->json(array('error' =>'The chosen financial year in not active'));
		}else{
			$first_coupon_date = Carbon::parse($request->first_coupon_date);
			$next_coupon_date = Carbon::parse($request->next_coupon_date);
			$diff_in_days = $first_coupon_date->diffInDays($next_coupon_date);

			$allocation = DB::table('main.investment_budget_allocation')
			->where('investment_budget_id',$budget->id)
			->where('code_value_reference','INTTBONDS')
			->first();
			if (count($allocation) < 1) {
				return response()->json(array('error' =>'The chosen Investment type is not allocated'));
				
			// }elseif($diff_in_days > 365){
			// 	return response()->json(array('error' =>'The difference between first coupon date and next coupon date must be less than 365 days'));

			}else {
				$data = $request->except(['_token','certificate_copy','fin_year_id']);
				$data['settlement_date'] = ($data['settlement_date'] == null ? null : Carbon::parse($data['settlement_date'])->format('Y-m-d'));
				$data['maturity_date'] = ($data['maturity_date'] == null ? null : Carbon::parse($data['maturity_date'])->format('Y-m-d')); 
				$data['auction_date'] = ($data['auction_date'] == null ? null : Carbon::parse($data['auction_date'])->format('Y-m-d'));
				if (isset($data['first_coupon_date'])) {
					$data['first_coupon_date'] = ($data['first_coupon_date'] == null ? null : Carbon::parse($data['first_coupon_date'])->format('Y-m-d'));
					$first_coupon_date = $data['first_coupon_date'];
				}
				if (isset($data['last_coupon_date'])) {
					$data['last_coupon_date'] = ($data['last_coupon_date'] == null ? null : Carbon::parse($data['last_coupon_date'])->format('Y-m-d'));
					$first_coupon_date = $data['last_coupon_date'];
					$data['first_coupon_date'] = $data['last_coupon_date'];
					unset($data['last_coupon_date']);

				}
				$data['next_coupon_date'] = ($data['next_coupon_date'] == null ? null : Carbon::parse($data['next_coupon_date'])->format('Y-m-d'));
				$days = explode('-', $data['tenure']);
				$diff = $days[0];

				if ($data['source'] == 2) {
					$settlement = Carbon::parse($data['settlement_date'])->format('d-m-Y');
					$maturity= Carbon::parse($data['maturity_date'])->format('d-m-Y');
					$rate = ($data['coupon_rate'] / 100);
					$yield = ($data['yield'] / 100);
					$redemption = 100;
					$frequency = 2;
					$basis = 3;
					// $clean_price = $this->price($settlement, $maturity, $rate, $yield, $redemption, $frequency, $basis);
					$clean_price = $data['price'];

					$cost = $this->cost($data['amount_invested'],$clean_price);

					$accrued_interest_price = $this->accruedInterestPrice($data['coupon_rate'],$data['tenure']);

					$dirty_price = $accrued_interest_price + $clean_price;

				}else{
					$clean_price = null;
					$dirty_price = null;
					$cost = $this->cost($data['amount_invested'],$data['price']);
					// $yield = $this->yield($data['coupon_rate'],$data['amount_invested'],$cost,$data['tenure']);
					$yield = $data['yield'];
				}

				$amortized = $this->armotizedValue($data['amount_invested'],$cost,$diff,$data['settlement_date']);
				$carrying_amount = round(($cost + $amortized), 2);
				
				$discount_premium = round(($data['amount_invested'] - $cost), 2);
				$fair_value = round(($cost - $amortized), 2);

				$amortization = round(($discount_premium / $diff), 2);
				$total = InvestimentTreasuryBond::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();

				$war = $this->war($data['coupon_rate'],$data['amount_invested'],$total->total_principle_amount);

				$interest_income = $this->interestIncome($data['tax_rate'],$data['coupon_rate'],$data['amount_invested'],$data['settlement_date']);
				$interest_receivable = $this->interestReceivable($data['tax_rate'],$data['coupon_rate'],$data['amount_invested'],$data['tenure']);
				$balance_sheet_value = $cost + $amortized + $interest_receivable;

				$data['cost'] = $cost;
				if ($data['source'] == 1) {
					$data['price'] = $data['price'];
					$data['dirty_price'] = null;
					$data['yield'] = $yield;
				} else {
					$data['price'] = $clean_price;
					$data['dirty_price'] = $dirty_price;
					$data['yield'] = $data['yield'];
				}

				$data['discount'] = $discount_premium;
				$data['fair_value'] = $fair_value;
				$data['amortization'] = $amortization;
				$data['interest_income'] = $interest_income;
				$data['interest_receivable'] = $interest_receivable;
				$data['expected_income'] = $expected_income;
				$data['balance_sheet_value'] = $balance_sheet_value;
				$data['carrying_amount'] = $carrying_amount;
				$data['weighted_average_return'] = $war;				

				$data = array_merge($data,['investment_budget_id' => $budget->id]);
				$data = array_merge($data,['investment_budget_allocation_id' => $allocation->id]);
				$create = InvestimentTreasuryBond::create($data);

				$treasury_bond_id = DB::getPdo()->lastInsertId();
				$flag = 1;
				$this->incrementDates($first_coupon_date,$data['next_coupon_date'],$data['settlement_date'],$data['maturity_date'],$data['amount_invested'],$data['coupon_rate'],$treasury_bond_id,$flag,$data['source']);

				return response()->json(array('success' => $this->returnRedirectResponse($treasury_bond_id,$request->fin_year_id,"INTTBONDS")));
			}

		}

	}


	public function treasuryBondsStoreUpdate($request){
		$budget = DB::table('main.investment_budgets')
		->where('fin_year_id',$request->fin_year_id)
		->first();

		if (count($budget) < 1) {
			return response()->json(array('error' =>'The chosen financial year in not active'));
		}else{
			$first_coupon_date = Carbon::parse($request->first_coupon_date);
			$next_coupon_date = Carbon::parse($request->next_coupon_date);
			$diff_in_days = $first_coupon_date->diffInDays($next_coupon_date);

			$allocation = DB::table('main.investment_budget_allocation')
			->where('investment_budget_id',$budget->id)
			->where('code_value_reference','INTTBONDS')
			->first();
			if (count($allocation) < 1) {
				return response()->json(array('error' =>'The chosen Investment type is not allocated'));
				
			// }elseif($diff_in_days > 365){
			// 	return response()->json(array('error' =>'The difference between first coupon date and next coupon date must be less than 365 days'));

			}else {
				$data = $request->except(['_token','certificate_copy','fin_year_id']);
				$data['settlement_date'] = ($data['settlement_date'] == null ? null : Carbon::parse($data['settlement_date'])->format('Y-m-d'));
				$data['maturity_date'] = ($data['maturity_date'] == null ? null : Carbon::parse($data['maturity_date'])->format('Y-m-d')); 
				$data['auction_date'] = ($data['auction_date'] == null ? null : Carbon::parse($data['auction_date'])->format('Y-m-d'));
				if (isset($data['first_coupon_date'])) {
					$data['first_coupon_date'] = ($data['first_coupon_date'] == null ? null : Carbon::parse($data['first_coupon_date'])->format('Y-m-d'));
					$first_coupon_date = $data['first_coupon_date'];
				}
				if (isset($data['last_coupon_date'])) {
					$data['last_coupon_date'] = ($data['last_coupon_date'] == null ? null : Carbon::parse($data['last_coupon_date'])->format('Y-m-d'));
					$first_coupon_date = $data['last_coupon_date'];
					$data['first_coupon_date'] = $data['last_coupon_date'];
					unset($data['last_coupon_date']);

				}
				$data['next_coupon_date'] = ($data['next_coupon_date'] == null ? null : Carbon::parse($data['next_coupon_date'])->format('Y-m-d'));
				$days = explode('-', $data['tenure']);
				$diff = $days[0];

				if ($data['source'] == 2) {
					$settlement = Carbon::parse($data['settlement_date'])->format('d-m-Y');
					$maturity= Carbon::parse($data['maturity_date'])->format('d-m-Y');
					$rate = ($data['coupon_rate'] / 100);
					$yield = ($data['yield'] / 100);
					$redemption = 100;
					$frequency = 2;
					$basis = 3;
					// $clean_price = $this->price($settlement, $maturity, $rate, $yield, $redemption, $frequency, $basis);
					$clean_price = $data['price'];

					$cost = $this->cost($data['amount_invested'],$clean_price);

					$accrued_interest_price = $this->accruedInterestPrice($data['coupon_rate'],$data['tenure']);

					$dirty_price = $accrued_interest_price + $clean_price;

				}else{
					$clean_price = null;
					$dirty_price = null;
					$cost = $this->cost($data['amount_invested'],$data['price']);
					// $yield = $this->yield($data['coupon_rate'],$data['amount_invested'],$cost,$data['tenure']);
					$yield = $data['yield'];
				}

				$amortized = $this->armotizedValue($data['amount_invested'],$cost,$diff,$data['settlement_date']);
				$carrying_amount = round(($cost + $amortized), 2);
				
				$discount_premium = round(($data['amount_invested'] - $cost), 2);
				$fair_value = round(($cost - $amortized), 2);

				$amortization = round(($discount_premium / $diff), 2);
				$total = InvestimentTreasuryBond::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();

				$war = $this->war($data['coupon_rate'],$data['amount_invested'],$total->total_principle_amount);

				$interest_income = $this->interestIncome($data['tax_rate'],$data['coupon_rate'],$data['amount_invested'],$data['settlement_date']);
				$interest_receivable = $this->interestReceivable($data['tax_rate'],$data['coupon_rate'],$data['amount_invested'],$data['tenure']);
				$balance_sheet_value = $cost + $amortized + $interest_receivable;

				$data['cost'] = $cost;
				if ($data['source'] == 1) {
					$data['price'] = $data['price'];
					$data['dirty_price'] = null;
					$data['yield'] = $yield;
				} else {
					$data['price'] = $clean_price;
					$data['dirty_price'] = $dirty_price;
					$data['yield'] = $data['yield'];
				}

				$data['discount'] = $discount_premium;
				$data['fair_value'] = $fair_value;
				$data['amortization'] = $amortization;
				$data['interest_income'] = $interest_income;
				$data['interest_receivable'] = $interest_receivable;
				$data['expected_income'] = $expected_income;
				$data['balance_sheet_value'] = $balance_sheet_value;
				$data['carrying_amount'] = $carrying_amount;
				$data['weighted_average_return'] = $war;				

				$data = array_merge($data,['investment_budget_id' => $budget->id]);
				$data = array_merge($data,['investment_budget_allocation_id' => $allocation->id]);

				$treasury_bonds = InvestimentTreasuryBond::find($request->id);
				$update = $treasury_bonds->update($data);

				$flag = 1;
				$delete = BondIncrementDate::where('treasury_bond_id', $request->id)->delete();
				$this->incrementDates($first_coupon_date,$data['next_coupon_date'],$data['settlement_date'],$data['maturity_date'],$data['amount_invested'],$data['coupon_rate'],$request->id,$flag,$data['source']);

				return response()->json(array('success' => $this->returnRedirectResponse($request->id,$request->fin_year_id,"INTTBONDS")));
			}

		}
	}

	public function incrementDates($first_coupon_date,$next_coupon_date,$settlement_date,$maturity_date,$amount_invested,$coupon_rate,$treasury_bond_id,$flag,$source){
		$coupon_rate = ($coupon_rate / 100);
		$interest_amount = (($amount_invested * $coupon_rate) / 2);
		$start = Carbon::parse($settlement_date);
		$end = Carbon::parse($maturity_date);
		$diff_in_years = $start->diffInYears($end);
		
		$fstyear = explode('-', $first_coupon_date);
		$sndyear = explode('-', $next_coupon_date);
		$count = 1;
		if ($fstyear[0] != $sndyear[0]) {
			$first = Carbon::parse($fstyear[0].'-'.$fstyear[1].'-'.$fstyear[2]);
			if ($source == 1) {
				$this->saveIncrements($first,$interest_amount,$treasury_bond_id,$flag);
			}
			
			for ($i = $diff_in_years; $i > 0; $i--) { 

				$first = Carbon::parse($fstyear[0].'-'.$fstyear[1].'-'.$fstyear[2]);
				$sec = Carbon::parse($sndyear[0].'-'.$sndyear[1].'-'.$sndyear[2]);
				
				while ($count == 1) {
					$fstyear[0]++;
					$first = Carbon::parse($fstyear[0].'-'.$fstyear[1].'-'.$fstyear[2]);
					$count--;
				}
				
				$this->saveIncrements($sec,$interest_amount,$treasury_bond_id,$flag);
				$this->saveIncrements($first,$interest_amount,$treasury_bond_id,$flag);
				$fstyear[0]++;
				$sndyear[0]++;
				
				
			}
		} else {
			for ($i = $diff_in_years; $i > 0; $i--) { 

				$first = Carbon::parse($fstyear[0].'-'.$fstyear[1].'-'.$fstyear[2]);
				$sec = Carbon::parse($sndyear[0].'-'.$sndyear[1].'-'.$sndyear[2]);

				if ($source == 1) {
					$this->saveIncrements($first,$interest_amount,$treasury_bond_id,$flag);
					$this->saveIncrements($sec,$interest_amount,$treasury_bond_id,$flag);
				}elseif($source == 2){
					while ($count == 1) {
						$fstyear[0]++;
						$first = Carbon::parse($fstyear[0].'-'.$fstyear[1].'-'.$fstyear[2]);
						$count--;
					}
					$this->saveIncrements($sec,$interest_amount,$treasury_bond_id,$flag);
					$this->saveIncrements($first,$interest_amount,$treasury_bond_id,$flag);

				}

				$fstyear[0]++;
				$sndyear[0]++;
			}
		}

	}

	public function saveIncrements($coupon_date,$interest_amount,$bond_id,$flag){
		if ($flag == 1) {
			$increment = new BondIncrementDate();
			$increment->interest_date = $coupon_date;
			$increment->interest_amount = $interest_amount;
			$increment->treasury_bond_id = $bond_id;
			$increment->save();
		} elseif($flag == 2) {
			
			$increment = new BondIncrementDate();
			$increment->interest_date = $coupon_date;
			$increment->interest_amount = $interest_amount;
			$increment->corporate_bond_id = $bond_id;
			$increment->save();
		}elseif($flag == 3){
			$increment = new BillIncrementDate();
			$increment->interest_date = $coupon_date;
			$increment->interest_amount = $interest_amount;
			$increment->treasury_bill_id = $bond_id;
			$increment->save();

		}
		
		
	}



	public function yield($coupon_rate,$face_value,$cost,$tenure){
		$coupon_rate = ($coupon_rate /100);
		$tenure = ($tenure / 365);

		$a = (($face_value - $cost) / $tenure);
		$b = ($coupon_rate * $face_value); 
		$yield = ((($b + $a) / $cost) * 100);

		return round($yield, 2);
	}

	public function interestIncomeTreasuryBond($interest_rate, $face_value, $diff){
		return $interest_income = round(((($interest_rate * $face_value) / 100) * ($diff / 365)), 2);

	}

	public function armotizedValue($face_value, $cost, $diff, $settlement_date){
		$a = (($face_value - $cost) / $diff);
		$now = Carbon::now();
		$settlement_date = Carbon::parse($settlement_date);
		$diff_now = $settlement_date->diffInDays($now);

		$b = $diff_now;
		return $amortized = round(($a * $b), 2); 

	}



	public function getTreasuryBonds($fin_year_id){
		$fixed_deposits = InvestimentTreasuryBond::join('investment_budgets','investment_budgets.id','=','investiment_treasury_bonds.investment_budget_id')->where('fin_year_id',$fin_year_id)->get();
		return json_decode(json_encode($fixed_deposits));

	}

	public function corporateBonds($request){

		$budget = DB::table('main.investment_budgets')
		->where('fin_year_id',$request->fin_year_id)
		->first();

		if (count($budget) < 1) {
			return response()->json(array('error' =>'The chosen financial year in not active'));
		}else{
			$allocation = DB::table('main.investment_budget_allocation')
			->where('investment_budget_id',$budget->id)
			->where('investment_code_type_id',$request->type)
			->first();
			if (count($allocation) < 1) {
				return response()->json(array('error' =>'The chosen Investment type is not allocated'));
				
			} else {
				$data = $request->except(['_token','certificate_copy','fin_year_id']);
				$data['settlement_date'] = ($data['settlement_date'] == null ? null : Carbon::parse($data['settlement_date'])->format('Y-m-d'));
				$data['maturity_date'] = ($data['maturity_date'] == null ? null : Carbon::parse($data['maturity_date'])->format('Y-m-d')); 
				$data['auction_date'] = ($data['auction_date'] == null ? null : Carbon::parse($data['auction_date'])->format('Y-m-d'));
				if (isset($data['first_coupon_date'])) {
					$data['first_coupon_date'] = ($data['first_coupon_date'] == null ? null : Carbon::parse($data['first_coupon_date'])->format('Y-m-d'));
					$first_coupon_date = $data['first_coupon_date'];
				}
				if (isset($data['last_coupon_date'])) {
					$data['last_coupon_date'] = ($data['last_coupon_date'] == null ? null : Carbon::parse($data['last_coupon_date'])->format('Y-m-d'));
					$first_coupon_date = $data['last_coupon_date'];
					$data['first_coupon_date'] = $data['last_coupon_date'];
					unset($data['last_coupon_date']);

				}

				$data['ipo_date'] = ($data['ipo_date'] == null ? null : Carbon::parse($data['ipo_date'])->format('Y-m-d'));
				$data['next_coupon_date'] = ($data['next_coupon_date'] == null ? null : Carbon::parse($data['next_coupon_date'])->format('Y-m-d'));
				$days = explode('-', $data['tenure']);
				$diff = $days[0];

				if ($data['source'] == 2) {
					$settlement = Carbon::parse($data['settlement_date'])->format('d-m-Y');
					$maturity= Carbon::parse($data['maturity_date'])->format('d-m-Y');
					$rate = ($data['coupon_rate'] / 100);
					$yield = ($data['yield'] / 100);
					$redemption = 100;
					$frequency = 2;
					$basis = 3;
					// $clean_price = $this->price($settlement, $maturity, $rate, $yield, $redemption, $frequency, $basis);
					$clean_price = $data['price'];

					$cost = $this->cost($data['amount_invested'],$clean_price);

					$accrued_interest_price = $this->accruedInterestPrice($data['coupon_rate'],$data['tenure']);

					$dirty_price = $accrued_interest_price + $clean_price;

				}else{
					$clean_price = null;
					$dirty_price = null;
					$cost = $this->cost($data['amount_invested'],$data['price']);
					// $yield = $this->yield($data['coupon_rate'],$data['amount_invested'],$cost,$data['tenure']);
					$yield = $data['yield'];
				}

				$amortized = $this->armotizedValue($data['amount_invested'],$cost,$diff,$data['settlement_date']);
				$carrying_amount = round(($cost + $amortized), 2);
				
				$discount_premium = round(($data['amount_invested'] - $cost), 2);
				$fair_value = round(($cost - $amortized), 2);

				$amortization = round(($discount_premium / $diff), 2);
				$total = InvestimentCorporateBond::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();

				$war = $this->war($data['coupon_rate'],$data['amount_invested'],$total->total_principle_amount);

				$interest_income = $this->interestIncome($data['tax_rate'],$data['coupon_rate'],$data['amount_invested'],$data['settlement_date']);
				$interest_receivable = $this->interestReceivable($data['tax_rate'],$data['coupon_rate'],$data['amount_invested'],$data['tenure']);
				$balance_sheet_value = $cost + $amortized + $interest_receivable;

				$data['cost'] = $cost;
				if ($data['source'] == 1) {
					$data['price'] = $data['price'];
					$data['dirty_price'] = null;
					$data['yield'] = $yield;
				} else {
					$data['price'] = $clean_price;
					$data['dirty_price'] = $dirty_price;
					$data['yield'] = $data['yield'];
				}

				$data['discount'] = $discount_premium;
				$data['fair_value'] = $fair_value;
				$data['amortization'] = $amortization;
				$data['interest_income'] = $interest_income;
				$data['interest_receivable'] = $interest_receivable;
				$data['expected_income'] = $expected_income;
				$data['balance_sheet_value'] = $balance_sheet_value;
				$data['carrying_amount'] = $carrying_amount;
				$data['weighted_average_return'] = $war;				

				$data = array_merge($data,['investment_budget_id' => $budget->id]);
				$data = array_merge($data,['investment_budget_allocation_id' => $allocation->id]);
				$create = InvestimentCorporateBond::create($data);

				$corporate_bond_id = DB::getPdo()->lastInsertId();
				$flag = 2;
				$this->incrementDates($data['first_coupon_date'],$data['next_coupon_date'],$data['settlement_date'],$data['maturity_date'],$data['amount_invested'],$data['coupon_rate'],$corporate_bond_id,$flag,$data['source']);

				return response()->json(array('success' => $this->returnRedirectResponse($corporate_bond_id,$request->fin_year_id,"INTCBONDS")));
			}

		}

	}

	public function corporateBondsStoreUpdate($request){
		$budget = DB::table('main.investment_budgets')
		->where('fin_year_id',$request->fin_year_id)
		->first();

		if (count($budget) < 1) {
			return response()->json(array('error' =>'The chosen financial year in not active'));
		}else{
			$allocation = DB::table('main.investment_budget_allocation')
			->where('investment_budget_id',$budget->id)
			->where('investment_code_type_id',$request->type)
			->first();
			if (count($allocation) < 1) {
				return response()->json(array('error' =>'The chosen Investment type is not allocated'));
				
			} else {
				$data = $request->except(['_token','certificate_copy','fin_year_id']);
				$data['settlement_date'] = ($data['settlement_date'] == null ? null : Carbon::parse($data['settlement_date'])->format('Y-m-d'));
				$data['maturity_date'] = ($data['maturity_date'] == null ? null : Carbon::parse($data['maturity_date'])->format('Y-m-d')); 
				$data['auction_date'] = ($data['auction_date'] == null ? null : Carbon::parse($data['auction_date'])->format('Y-m-d'));
				if (isset($data['first_coupon_date'])) {
					$data['first_coupon_date'] = ($data['first_coupon_date'] == null ? null : Carbon::parse($data['first_coupon_date'])->format('Y-m-d'));
					$first_coupon_date = $data['first_coupon_date'];
				}
				if (isset($data['last_coupon_date'])) {
					$data['last_coupon_date'] = ($data['last_coupon_date'] == null ? null : Carbon::parse($data['last_coupon_date'])->format('Y-m-d'));
					$first_coupon_date = $data['last_coupon_date'];
					$data['first_coupon_date'] = $data['last_coupon_date'];
					unset($data['last_coupon_date']);

				}

				$data['ipo_date'] = ($data['ipo_date'] == null ? null : Carbon::parse($data['ipo_date'])->format('Y-m-d'));
				$data['next_coupon_date'] = ($data['next_coupon_date'] == null ? null : Carbon::parse($data['next_coupon_date'])->format('Y-m-d'));
				$days = explode('-', $data['tenure']);
				$diff = $days[0];

				if ($data['source'] == 2) {
					$settlement = Carbon::parse($data['settlement_date'])->format('d-m-Y');
					$maturity= Carbon::parse($data['maturity_date'])->format('d-m-Y');
					$rate = ($data['coupon_rate'] / 100);
					$yield = ($data['yield'] / 100);
					$redemption = 100;
					$frequency = 2;
					$basis = 3;
					// $clean_price = $this->price($settlement, $maturity, $rate, $yield, $redemption, $frequency, $basis);
					$clean_price = $data['price'];

					$cost = $this->cost($data['amount_invested'],$clean_price);

					$accrued_interest_price = $this->accruedInterestPrice($data['coupon_rate'],$data['tenure']);

					$dirty_price = $accrued_interest_price + $clean_price;

				}else{
					$clean_price = null;
					$dirty_price = null;
					$cost = $this->cost($data['amount_invested'],$data['price']);
					// $yield = $this->yield($data['coupon_rate'],$data['amount_invested'],$cost,$data['tenure']);
					$yield = $data['yield'];
				}

				$amortized = $this->armotizedValue($data['amount_invested'],$cost,$diff,$data['settlement_date']);
				$carrying_amount = round(($cost + $amortized), 2);
				
				$discount_premium = round(($data['amount_invested'] - $cost), 2);
				$fair_value = round(($cost - $amortized), 2);

				$amortization = round(($discount_premium / $diff), 2);
				$total = InvestimentCorporateBond::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();

				$war = $this->war($data['coupon_rate'],$data['amount_invested'],$total->total_principle_amount);

				$interest_income = $this->interestIncome($data['tax_rate'],$data['coupon_rate'],$data['amount_invested'],$data['settlement_date']);
				$interest_receivable = $this->interestReceivable($data['tax_rate'],$data['coupon_rate'],$data['amount_invested'],$data['tenure']);
				$balance_sheet_value = $cost + $amortized + $interest_receivable;

				$data['cost'] = $cost;
				if ($data['source'] == 1) {
					$data['price'] = $data['price'];
					$data['dirty_price'] = null;
					$data['yield'] = $yield;
				} else {
					$data['price'] = $clean_price;
					$data['dirty_price'] = $dirty_price;
					$data['yield'] = $data['yield'];
				}

				$data['discount'] = $discount_premium;
				$data['fair_value'] = $fair_value;
				$data['amortization'] = $amortization;
				$data['interest_income'] = $interest_income;
				$data['interest_receivable'] = $interest_receivable;
				$data['expected_income'] = $expected_income;
				$data['balance_sheet_value'] = $balance_sheet_value;
				$data['carrying_amount'] = $carrying_amount;
				$data['weighted_average_return'] = $war;				

				$data = array_merge($data,['investment_budget_id' => $budget->id]);
				$data = array_merge($data,['investment_budget_allocation_id' => $allocation->id]);
				$corporate_bond = InvestimentCorporateBond::find($request->id);
				$update = $corporate_bond->update($data);

				$flag = 2;
				$delete = BondIncrementDate::where('corporate_bond_id', $request->id)->delete();
				$this->incrementDates($data['first_coupon_date'],$data['next_coupon_date'],$data['settlement_date'],$data['maturity_date'],$data['amount_invested'],$data['coupon_rate'],$request->id,$flag,$data['source']);

				return response()->json(array('success' => $this->returnRedirectResponse($corporate_bond_id,$request->fin_year_id,"INTCBONDS")));
			}

		}
	}

	public function returnRedirectResponse($id,$fin_year_id,$reference){
		switch ($reference) {
			case 'INTFDR':
			$investment = InvestimentFixedDeposit::find($id);
			$allocation_id = $investment->investment_budget_allocation_id;
			break;
			case 'INTCBONDS':
			$investment = InvestimentCorporateBond::find($id);
			$allocation_id = $investment->investment_budget_allocation_id;
			break;
			case 'INTTBONDS':
			$investment = InvestimentTreasuryBond::find($id);
			$allocation_id = $investment->investment_budget_allocation_id;
			break;
			default:
			$investment = InvestimentTreasuryBill::find($id);
			$allocation_id = $investment->investment_budget_allocation_id;
			break;
		}
		$array = array();
		$array = [
			'fin_year_id' => $fin_year_id,
			'reference' => $reference,
			'allocation_id' => $allocation_id,
		];

		return $array;
	}

	public function getCorporateBonds($fin_year_id){
		$fixed_deposits = InvestimentCorporateBond::join('investment_budgets','investment_budgets.id','=','investiment_corporate_bonds.investment_budget_id')->where('fin_year_id',$fin_year_id)->get();
		return json_decode(json_encode($fixed_deposits));

	}


	//loans
	public function saveNewLoan($request)
	{

		$dsb_date = Carbon::parse($request->disbursement_date);
		$mt_date = Carbon::parse($request->maturity_date);
		$diff_in_months = $mt_date->diffInMonths($dsb_date);

		$allocation_budget  = InvestmentBudgetAllocation::where('fin_year_id',$request->fin_year)->where('code_value_id',$request->code_value_id)->first();
		InvestmentLoan::insert([
			'budget_allocation_id' => isset($allocation_budget->id) ? $allocation_budget->id : '',
			'loan_type' => $request->loan_type,
			'loan_type_name' => $this->getLoanTypeName($request->loan_type),
			'loanee_name' => $request->loanee,
			'loan_amount' => number_format((float)$request->loan_amount, 2, '.', ''),
			'disbursement_date' =>  $request->disbursement_date,
			'maturity_date' => $request->maturity_date,
			'tenure' => $diff_in_months,
			'interest_rate' => $request->interest_rate,
			'penalty_rate' => $request->penalty_rate,
			'tax_rate' => $request->tax_rate,
			'fin_year_id' => $request->fin_year, 
		]);
	}

	public function loansForDatatable($budget_allocation_id){
		return InvestmentLoan::where('budget_allocation_id',$budget_allocation_id);
	}

	public function loanRepaymentForDatatable($loan_id){
		return DB::table('main.investment_loan_repayments')->where('investment_loan_id',$loan_id);
	}

	

	public function getLoanTypeName($loan_type)
	{
		switch ($loan_type) {
			case 1:
			$return = 'Government';
			break;
			case 2:
			$return = 'Cooperate';
			break;
			case 3:
			$return = 'Cooperative Union';
			break;
			default:
			$return =  '';
			break;
		}

		return $return;
	}

	public function getEmployers(){
		$input = request()->all();
		$employers = [];
		$search = strtolower(trim($input['q']));

		return $employers = Employer::where('name', 'ilike', '%'.$search.'%')
		->get();

	}

	public function getCodeTypeByReference($reference)
	{
		return DB::table('main.investment_code_types')
		->where('code_value_reference',$reference)
		->get();
	}
	public function callCreate($request)
	{
      // dd($request->all());
		// $balance_date = Carbon::parse($request->balance_date);

		$calls  = InvestmentCallAccount::insert([
			'balance_amount' => $request->balance_amount,
			'interest_rate' => $this->getLoanTypeName($request->interest_rate),
			'tax_rate' => $request->tax_rate,
			'received_interest' => $request->received_interest,
			'interest_income' => $request->interest_income,
			'interest_rate' => $request->interest_rate,
			'interest_receivable' => $request->interest_receivable,
			'balance_date' => $request->balance_date,
		]);
		// return response()->json(array('success' =>'success'));
		return redirect()->route('backend.investment.call_account.index');
	}

	public function getSpecificInvestimentType(){

	}

}
