<?php

namespace App\Repositories\Backend\Investiment;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;


use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Investment\Equity\InvestmentEquityLot;
use App\Models\Investment\Equity\InvestmentEquityIssuer;
use App\Models\Investment\Equity\InvestmentEquityUpdate;

class InvestmentEquityRepository extends BaseRepository
{

	const MODEL = InvestmentEquityIssuer::class;

	public function __construct()
	{
		parent::__construct();
	}

	public function findOrThrowException($id)
	{
		$issuer = $this->query()->find($id);
		if (count($issuer)) {
			return $issuer;
		}
		throw new GeneralException(trans('The Issuer not found'));
	}


	public function findByEmployerId($employer_id)
	{
		$return = $this->query()->where('issuer_id',$employer_id)->first();
		return $return;
	}


	public function findByNameAndReference($name,$reference)
	{
		$return = $this->query()->whereRaw("lower(issuer_name) LIKE ?", ['%'.strtolower(trim($name)).'%'])
		->whereRaw("upper(issuer_reference) LIKE ?", ['%'.strtoupper(trim($reference)).'%'])->first();
		return $return;
	}

	

	public function getSummary()
	{
		return [
			'issuer_count' => $this->query()->count(),
			// 'shares_count' => $this->query()->count('scheme_name'),
			'shares_count' => DB::table('main.investment_equity_lots')->sum('remaining_share'),
		];
	}


	public function saveIssuer($request)
	{
		try {
			if ($request['issuer_source'] == 'MAC') {
				$employer = Employer::find($request['issuer']);
				if (count($employer)) {
					$issuer_name = $employer->name;
				}
			} else {
				$issuer_name = $request['issuer_name'];
			}
			$this->query()->create([
				'issuer_id'=> !empty($request['issuer']) ? $request['issuer'] : null,
				'issuer_name'=>$issuer_name,
				'issuer_reference'=>strtoupper(trim($request['issuer_reference'])),
			]);	
			return true;
		} catch (\Exception $e) {
			logger($e);	
			return false;
		}
	}


	public function getEquityForDatatable()
	{
		return $this->query()->select('*');
	}

	public function getUpdatesForDatatable($equity_id)
	{
		return InvestmentEquityUpdate::where('equity_issuer_id',$equity_id)->where('is_update',true)->orderBy('value_date','desc');
	}

	public function getLotsForDatatable($equity_id)
	{
		return InvestmentEquityLot::where('equity_issuer_id',$equity_id)->orderBy('purchase_date','desc');
	}

	public function getSalesForDatatable($equity_id)
	{
		return InvestmentEquityUpdate::where('equity_issuer_id',$equity_id)->where('is_sell',true)->orderBy('value_date','desc');
	}

	public function getDividendsForDatatable($equity_id)
	{
		return InvestmentEquityUpdate::where('equity_issuer_id',$equity_id)->where('is_dividend',true)->orderBy('dividend_payment_date','desc');
	}

	public function getIssuers()
	{
		return $this->query()->select('*')->groupBy('id')->get();
	}

	
	public function saveNewEquity($data,$equity_category, $budget_allocation)
	{
		$return = DB::transaction(function () use ($data,$equity_category,$budget_allocation) { 
			$issuer = $this->query()->find((int)$data['issuer']);
			$broker = Employer::find($data['broker']);
			$price_per_share = number_format((float)$data['price_per_share'], 4, '.', '');
			$commission = !empty($data['commission']) ? number_format((float)$data['commission'], 4, '.', '') : 0;

			$save = [
				'equity_issuer_id' => $data['issuer'],
				'purchase_date' => Carbon::parse($data['purchase_date'])->format('Y-m-d'),
				'budget_allocation_id' => $budget_allocation['budget_allocation_id'],
				'fin_year_id' => $budget_allocation['fin_year_id'],
				'category_id' => $equity_category->id,
				'category_name' => $equity_category->name,
				'lot_number' => $this->returnLotNumberForSave($data['issuer']),
				'number_of_share' => $data['number_of_shares'],
				'total_issued_share' => $data['total_issued_shares'],
				'price_per_share' => $price_per_share,
				'total_price' => $price_per_share * $data['number_of_shares'],
				'remaining_share' => $data['number_of_shares'],
				'disbursment_date' => Carbon::parse($data['purchase_date'])->format('Y-m-d'),
				'broker_id' => count($broker) ? $broker->id : null,
				'broker_name' =>  count($broker) ? $broker->name : null,
				'subscription_date' => !empty($data['subscription_date']) ? Carbon::parse($data['subscription_date'])->format('Y-m-d') : null,
				'allotment_date' => !empty($data['allotment_date']) ? Carbon::parse($data['allotment_date'])->format('Y-m-d') : null,
				'commission' => !empty($commission) ? $commission : null,
				'total_cost' => $commission + ( $price_per_share * $data['number_of_shares']),
			];

			$equity_lot = InvestmentEquityLot::where([
				'equity_issuer_id' => $data['issuer'],
				'purchase_date' => Carbon::parse($data['purchase_date'])->format('Y-m-d')
			])->first();

			if (!count($equity_lot)) {
				$new_lot = InvestmentEquityLot::create($save);
				if (count($new_lot)) {
					InvestmentEquityUpdate::create([
						'equity_issuer_id' => $new_lot->equity_issuer_id,
						'equity_lot_id' => $new_lot->id,
						'lot_number' => $new_lot->lot_number,
						'number_of_share' => $new_lot->remaining_share,
						'share_value' => $new_lot->price_per_share,
						'value_date' =>  $new_lot->purchase_date,
						'total_share_value' => $new_lot->total_price,
						'is_update'=>true,
						'total_share_available' => $new_lot->remaining_share,
					]);
				}
			}

			return true;
		});

		return $return;
	}



	public function updateLotPrice($request)
	{
		$issuer = $this->query()->find($request['equity_issuer_id']);
		if (count($issuer)) {
			$share_value = number_format((float)$request['price_per_share'], 4, '.', '');
			$shares =  number_format($issuer->total_shares, 0, '.', '');
			$total_price = $share_value*$shares;
			
			InvestmentEquityUpdate::updateOrCreate(
				[
					'equity_issuer_id' => $request['equity_issuer_id'],
					'value_date'=> Carbon::parse($request['price_date'])->format('Y-m-d')
				],
				[
					'number_of_share' => $shares,
					'share_value' => $share_value,
					'total_share_value'=>  $total_price,
					'is_update'=>true
				]
			);
		}
		return true;
	}

	public function returnBudgetAllocationDetails($purchase_date,$category_id)
	{

		$tdy_mnth = Carbon::parse($purchase_date)->format('m');
		$tdy_yr = Carbon::parse($purchase_date)->format('Y');
		if($tdy_mnth < 10){
			$fy = ($tdy_yr-1).'/'.$tdy_yr;
		}else{
			$fy = ($tdy_yr).'/'.($tdy_yr+1);
		}
		$financial_year = DB::table('main.fin_years')->where('name', $fy)->first();	
		$investment_budget_allocation = DB::table('main.investment_budget_allocation')
		->where('code_value_reference','INTEQUITIES')
		->where('investment_code_type_id',$category_id)
		->where('fin_year_id',$financial_year->id)->first();

		if (count($investment_budget_allocation)) {
			$id = [
				'budget_allocation_id'=>$investment_budget_allocation->id,
				'fin_year_id'=>$financial_year->id,
			];
		} else {
			$id = null;
		}
		return $id;
	}


	public function returnLotNumberForSave($issuer_id) {
		$issuer = $this->query()->find($issuer_id);

		if (count($issuer)) {
			$lot = InvestmentEquityLot::select('id','lot_number')->where('equity_issuer_id',$issuer_id)->orderBy('id', 'desc')->first();
			if (count($lot)) {
				$last = abs((int) filter_var($lot->lot_number, FILTER_SANITIZE_NUMBER_INT));
				$new = $last+1;
				return $issuer->issuer_reference.' - '.$new;
			} else {
				return $issuer->issuer_reference.' - 1';
			}
		}else{
			return null;
		}
	}

	public function sellShares($request)
	{
		$return = DB::transaction(function () use ($request) { 
			$equity_lot = InvestmentEquityLot::find($request['sell_lot']);
			$broker = Employer::find($request['broker']);
			if (count($equity_lot)) {
				$number_of_shares = number_format((float)$request['number_of_shares'], 0, '.', '');
				$sell_price = number_format((float)$request['price_per_share'], 4, '.', '');
				$selling_date = Carbon::parse($request['selling_date'])->format('Y-m-d');
				$commission = number_format((float)$request['commission'], 4, '.', '');
				
				InvestmentEquityUpdate::create(
					[
						'equity_issuer_id' => $equity_lot->equity_issuer_id,
						'equity_lot_id'=> $request['sell_lot'],
						'lot_number' => $equity_lot->lot_number,
						'number_of_share' => $number_of_shares,
						'share_value' => $sell_price,
						'selling_date'=> $selling_date,
						'is_sell' => true,
						'total_share_available' =>$equity_lot->number_of_share,
						'total_share_value' => $number_of_shares * $sell_price,
						'selling_broker' => count($broker) ? $broker->name : $request['broker'],
						'selling_broker_regno' => $request['broker'],
						'commission'=> $commission
					]);

				$equity_lot->remaining_share -= $number_of_shares;
				$equity_lot->save();
			}
			return true;
		});
		return $return;
	}


	public function saveDividend($request)
	{
		$return = DB::transaction(function () use ($request) { 
			$issuer = $this->query()->find($request['id']);
			if (count($issuer)) {
				$number_of_share = number_format((float)$issuer->total_shares, 0, '.', '');
				$dividend_per_share = number_format((float)$request['dividend_per_share'], 4, '.', '');
				
				$declaration_date = Carbon::parse($request['declaration_date'])->format('Y-m-d');
				$dividend_payment_date = Carbon::parse($request['dividend_payment_date'])->format('Y-m-d');
				
				$dividend_exist = InvestmentEquityUpdate::where('equity_issuer_id',$request['id'])->where('dividend_declaration_date',$declaration_date)->first();
				if (!count($dividend_exist)) {
					InvestmentEquityUpdate::create(
						[
							'equity_issuer_id' => $request['id'],
							'number_of_share' => $number_of_share,
							'total_share_available' => $number_of_share,
							'total_share_value' => $dividend_per_share*$number_of_share,
							'share_value' => $dividend_per_share,
							'dividend_receivable' => $dividend_per_share*$number_of_share,
							'dividend_declaration_date'=>$declaration_date,
							'dividend_payment_date'=>$declaration_date,
							'is_dividend' => true,
						]
					);
				}
			}
			return true;
		});
		return $return;
	}




}
