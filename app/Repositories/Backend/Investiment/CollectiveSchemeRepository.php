<?php

namespace App\Repositories\Backend\Investiment;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;


use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Investment\CollectiveScheme\InvestmentCollectiveLot;
use App\Models\Investment\CollectiveScheme\InvestmentCollectiveScheme;
use App\Models\Investment\CollectiveScheme\InvestmentCollectiveUpdate;

class CollectiveSchemeRepository extends BaseRepository
{

	const MODEL = InvestmentCollectiveScheme::class;

	public function __construct()
	{
		parent::__construct();
	}

	public function findOrThrowException($id)
	{
		$scheme = $this->query()->find($id);
		if (count($scheme)) {
			return $scheme;
		}
		throw new GeneralException(trans('The Investment Scheme not found'));
	}

	public function findSchemeByNameOrReference($manager_id, $scheme_name=null,$scheme_reference=null)
	{
		
		if (!empty($scheme_name) || !empty($scheme_reference)) {
			$inv_scheme = InvestmentCollectiveScheme::where('manager_id',$manager_id);
			if (!empty($scheme_name)) {
				$scheme_name = ucwords(strtolower(trim($scheme_name)));
				$inv_scheme = $inv_scheme->where('scheme_name',$scheme_name);
			} 

			if (!empty($scheme_reference)) {
				$scheme_reference = strtoupper(trim($scheme_reference));
				$inv_scheme = $inv_scheme->where('scheme_reference',$scheme_reference);
			}
			return $inv_scheme->first();
		} else {
			return null;
		}
	}



	public function getSummary()
	{
		return [
			'manager_count' => $this->query()->distinct('manager_id')->count('manager_id'),
			'scheme_count' => $this->query()->count('scheme_name'),
			'units_count' => DB::table('main.investment_collective_lots')->sum('remaining_unit'),
		];
	}

	public function getManagers()
	{
		return $this->query()->select('employers.name', 'employers.id')
		->join('main.employers','employers.id','=','investment_collective_schemes.manager_id')
		->groupBy('employers.id')->get();

	}

	public function getForDatatable()
	{
		$return = $this->query()->select('*');
		return $return;
	}

	public function returnLotNumberForSave($scheme_id) {
		$scheme = $this->query()->find($scheme_id);

		if (count($scheme)) {
			$lot = InvestmentCollectiveLot::select('id','lot_number')->where('scheme_id',$scheme_id)->orderBy('id', 'desc')->first();
			if (count($lot)) {
				$last = abs((int) filter_var($lot->lot_number, FILTER_SANITIZE_NUMBER_INT));
				$new = $last+1;
				return $scheme->scheme_reference.' - '.$new;
			} else {
				return $scheme->scheme_reference.' - 1';
			}
		}else{
			return null;
		}
	}



	public function saveScheme($request)
	{
		InvestmentCollectiveScheme::updateOrCreate(
			[
				'manager_id' => $request['manager'],
				'scheme_name'=> ucwords(strtolower(trim($request['scheme_name'])))
			],
			[
				'scheme_reference' =>  strtoupper(trim($request['scheme_reference']))
			]
		);
		return true;
	}

	public function getUpdatesForDatatable($scheme_id)
	{
		return InvestmentCollectiveUpdate::where('scheme_id',$scheme_id)->where('is_update',true)->orderBy('value_date','desc');
	}

	public function getLotsForDatatable($scheme_id)
	{
		return InvestmentCollectiveLot::where('scheme_id',$scheme_id)->orderBy('purchase_date','desc');
	}

	public function getSalesForDatatable($scheme_id)
	{
		return InvestmentCollectiveUpdate::where('scheme_id',$scheme_id)->where('is_sell',true)->orderBy('selling_date','desc');
	}

	public function getDividendsForDatatable($scheme_id)
	{
		return InvestmentCollectiveUpdate::where('scheme_id',$scheme_id)->where('is_dividend',true)->orderBy('dividend_payment_date','desc');
	}


	public function saveSchemeUnits($request,$allocation)
	{

		$return = DB::transaction(function () use ($request,$allocation) { 
			
			$purchase_nav = number_format((float)$request['unit_value'], 4, '.', '');
			$scheme_lot_exist = InvestmentCollectiveLot::where('scheme_id',$request['scheme'])
			->where('purchase_date',Carbon::parse($request['purchase_date'])->format('Y-m-d'))
			->where('category_id',$request['category'])->first();
			if (!count($scheme_lot_exist)) {
				$scheme_lot = InvestmentCollectiveLot::create([
					'budget_allocation_id' => $allocation['budget_allocation_id'],
					'fin_year_id' => $allocation['fin_year_id'],
					'scheme_id' => $request['scheme'],
					'category_id' => $request['category'],
					'lot_number' => $this->returnLotNumberForSave($request['scheme']),
					'purchased_unit'=> $request['total_units'],
					'purchase_nav'=> $purchase_nav,
					'purchase_date'=> Carbon::parse($request['purchase_date'])->format('Y-m-d'),
					'purchase_amount'=> $purchase_nav * $request['total_units'],
					'remaining_unit'=> $request['total_units'],
					'disbursment_date'=> Carbon::parse($request['disbursment_date'])->format('Y-m-d'),
				]);
				if (count($scheme_lot)) {
					$scheme = $this->query()->where('id',$scheme_lot->scheme_id)->first();
					if (count($scheme)) {
						InvestmentCollectiveUpdate::updateOrCreate(
							[
								'scheme_id' => $scheme_lot->scheme_id,
								'value_date'=>$scheme_lot->purchase_date
							],
							[
								'unit_value' => $scheme_lot->purchase_nav,
								'total_unit_amount'=>$scheme->total_units * $scheme_lot->purchase_nav,
								'number_of_unit'=>$scheme->total_units,
								'total_unit'=>$scheme->total_units,
								'is_update'=>true
							]
						);
					}
				}
			} 
			return true;
		});

		return $return;
	}

	public function updateLotPrice($request)
	{
		$scheme = $this->query()->find($request['scheme_id']);
		if (count($scheme)) {
			$unit_value = number_format((float)$request['unit_value'], 4, '.', '');
			$units =  number_format($scheme->total_units, 0, '.', '');
			$total_amount = $unit_value*$units;
			InvestmentCollectiveUpdate::updateOrCreate(
				[
					'scheme_id' => $scheme->id,
					'value_date'=> Carbon::parse($request['value_date'])->format('Y-m-d')
				],
				[
					'unit_value' => $unit_value,
					'total_unit_amount'=>  number_format($total_amount, 4, '.', ''),
					'number_of_unit'=>$units,
					'total_unit'=>$units,
					'is_update'=>true
				]
			);
		}
		return true;

	}

	public function sellShares($request)
	{
		$return = DB::transaction(function () use ($request) { 
			$investment_lot = InvestmentCollectiveLot::find($request['sell_lot']);
			
			if (count($investment_lot)) {
				$number_of_units = number_format((float)$request['sell_units'], 0, '.', '');
				$sell_price = number_format((float)$request['sell_price'], 4, '.', '');
				$selling_date = Carbon::parse($request['selling_date'])->format('Y-m-d');

				InvestmentCollectiveUpdate::create(
					[
						'scheme_id' => $investment_lot->scheme_id,
						'lot_id'=> $request['sell_lot'],
						'lot_number' => $investment_lot->lot_number,
						'number_of_unit' => $number_of_units,
						'selling_price' => $sell_price,
						'selling_date'=> $selling_date,
						'is_sell' => true,
						'total_unit' => $investment_lot->remaining_unit,
						'total_unit_amount' => $number_of_units * $sell_price
					]);
				$investment_lot->remaining_unit -= $number_of_units;
				$investment_lot->save();
			}
			return true;
		});
		return $return;
	}


	public function saveDividend($request)
	{

		$return = DB::transaction(function () use ($request) { 
			$scheme = $this->query()->find($request['scheme_id']);
			if (count($scheme)) {
				$number_of_unit = number_format((float)$scheme->remain_units, 0, '.', '');
				$dividend_per_unit = number_format((float)$request['dividend_per_unit'], 4, '.', '');
				
				$declaration_date = Carbon::parse($request['declaration_date'])->format('Y-m-d');
				$payment_date = Carbon::parse($request['payment_date'])->format('Y-m-d');
				
				$dividend_income = $number_of_unit * $dividend_per_unit;

				$dividend_exist = InvestmentCollectiveUpdate::where('scheme_id',$request['scheme_id'])->where('dividend_declaration_date',$declaration_date)->first();

				if (!count($dividend_exist)) {
					InvestmentCollectiveUpdate::create(
						[
							'scheme_id' => $request['scheme_id'],
							'number_of_unit' => $number_of_unit,
							'total_unit' => $number_of_unit,
							'total_unit_amount' => $dividend_income,
							'dividend_nav' => $dividend_per_unit,
							'dividend_income' => $dividend_income,
							'dividend_receivable' => $dividend_income,
							// 'dividend_income' => $dividend_amount - (($dividend_amount * 10)/100),
							'dividend_declaration_date'=>$declaration_date,
							'dividend_payment_date'=>$declaration_date,
							'is_dividend' => true,
						]
					);
				}
			}
			return true;
		});
		return $return;
	}


	public function returnBudgetAllocationDetails($purchase_date)
	{

		$tdy_mnth = Carbon::parse($purchase_date)->format('m');
		$tdy_yr = Carbon::parse($purchase_date)->format('Y');
		if($tdy_mnth < 7){
			$fy = ($tdy_yr-1).'/'.$tdy_yr;
		}else{
			$fy = ($tdy_yr).'/'.($tdy_yr+1);
		}
		
		$financial_year = DB::table('main.fin_years')->where('name', $fy)->first();	
		$investment_budget_allocation = DB::table('main.investment_budget_allocation')
		->where('code_value_reference','INTCIS')
		->where('fin_year_id',$financial_year->id)->first();

		if (count($investment_budget_allocation)) {
			$id = [
				'budget_allocation_id'=>$investment_budget_allocation->id,
				'fin_year_id'=>$financial_year->id,
			];
		} else {
			$id = null;
		}
		return $id;
	}



}
