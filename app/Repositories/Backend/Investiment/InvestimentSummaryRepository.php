<?php

namespace App\Repositories\Backend\Investiment;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\DataTables\WorkflowTrackDataTable;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Investiment\InvestimentFixedDeposit;
use App\Models\Investiment\InvestimentTreasuryBill;
use App\Models\Investiment\InvestimentTreasuryBond;
use App\Models\Investiment\InvestimentCorporateBond;
use Yajra\Datatables\Datatables;
use App\DataTables\App\DataTables\Investment\InvestmentType\FixedDepositDatatable;
use App\Models\Investiment\FixedDepositUpdate;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;

class InvestimentSummaryRepository extends BaseRepository
{
	public function getFixedDepositSummary($request){
		$investment_type = InvestimentFixedDeposit::select('maturity_date','institution_name','amount_invested as face_value','principal_redeemed','tax_rate','interest_receivable','interest_received as interest_paid','interest_income','interest_receivable as interest_maturity','interest_rate','opening_date','tenure');
		return $investment_type;
	}

   public function summaryDatatable($fin_id){

   	    $fixed = DB::table('main.investment_budgets')
   	    ->select(DB::raw('count(investiment_fixed_deposits.id) as total_investment,investment_budgets.fin_year_id,
            investment_budget_allocation_id, name,investment_budget_allocation.percent ,investment_budget_allocation.amount,code_value_reference as cid'))
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->join('code_values', 'code_values.reference', '=', 'investment_budget_allocation.code_value_reference')
		->join('investiment_fixed_deposits', 'investiment_fixed_deposits.investment_budget_allocation_id', '=', 'investment_budget_allocation.id')
		->where('investment_budget_allocation.fin_year_id',$fin_id)
		->groupBy('investment_budget_allocation_id','investment_budgets.fin_year_id', 'name','investment_budget_allocation.percent','investment_budget_allocation.amount','code_value_reference');

	    $t_bills = DB::table('main.investment_budgets')
   	    ->select(DB::raw('count(investiment_treasury_bills.id) as total_investment,investment_budgets.fin_year_id,
            investment_budget_allocation_id, name,investment_budget_allocation.percent ,investment_budget_allocation.amount,code_value_reference as cid'))
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->join('code_values', 'code_values.reference', '=', 'investment_budget_allocation.code_value_reference')
		->join('investiment_treasury_bills', 'investiment_treasury_bills.investment_budget_allocation_id', '=', 'investment_budget_allocation.id')
		->where('investment_budget_allocation.fin_year_id',$fin_id)
		->union($fixed)
		->groupBy('investment_budget_allocation_id','investment_budgets.fin_year_id', 'name','investment_budget_allocation.percent','investment_budget_allocation.amount','code_value_reference');

		$t_bonds = DB::table('main.investment_budgets')
   	    ->select(DB::raw('count(investiment_treasury_bonds.id) as total_investment,investment_budgets.fin_year_id,
            investment_budget_allocation_id, name,investment_budget_allocation.percent ,investment_budget_allocation.amount,code_value_reference as cid'))
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->join('code_values', 'code_values.reference', '=', 'investment_budget_allocation.code_value_reference')
		->join('investiment_treasury_bonds', 'investiment_treasury_bonds.investment_budget_allocation_id', '=', 'investment_budget_allocation.id')
		->where('investment_budget_allocation.fin_year_id',$fin_id)
		->union($t_bills)
		->groupBy('investment_budget_allocation_id','investment_budgets.fin_year_id', 'name','investment_budget_allocation.percent','investment_budget_allocation.amount','code_value_reference');

		$c_bonds = DB::table('main.investment_budgets')
   	    ->select(DB::raw('count(investiment_corporate_bonds.id) as total_investment,investment_budgets.fin_year_id,
            investment_budget_allocation_id, name,investment_budget_allocation.percent ,investment_budget_allocation.amount,code_value_reference as cid'))
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->join('code_values', 'code_values.reference', '=', 'investment_budget_allocation.code_value_reference')
		->join('investiment_corporate_bonds', 'investiment_corporate_bonds.investment_budget_allocation_id', '=', 'investment_budget_allocation.id')
		->where('investment_budget_allocation.fin_year_id',$fin_id)
		->union($t_bonds)
		->groupBy('investment_budget_allocation_id','investment_budgets.fin_year_id', 'name','investment_budget_allocation.percent','investment_budget_allocation.amount','code_value_reference');

		$loans = DB::table('main.investment_budgets')
   	    ->select(DB::raw('count(investment_loans.id) as total_investment,investment_budgets.fin_year_id,
            budget_allocation_id as investment_budget_allocation_id, name,investment_budget_allocation.percent ,investment_budget_allocation.amount,code_value_reference as cid'))
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->join('code_values', 'code_values.reference', '=', 'investment_budget_allocation.code_value_reference')
		->join('investment_loans', 'investment_loans.budget_allocation_id', '=', 'investment_budget_allocation.id')
		->where('investment_budget_allocation.fin_year_id',$fin_id)
		->union($c_bonds)
		->groupBy('budget_allocation_id','investment_budgets.fin_year_id', 'name','investment_budget_allocation.percent','investment_budget_allocation.amount','code_value_reference');

		$equity = DB::table('main.investment_budgets')
   	    ->select(DB::raw('sum(number_of_share) as total_investment,investment_budgets.fin_year_id,
            budget_allocation_id as investment_budget_allocation_id, name,investment_budget_allocation.percent ,investment_budget_allocation.amount,investment_budget_allocation.code_value_reference as cid'))
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->join('code_values', 'code_values.reference', '=', 'investment_budget_allocation.code_value_reference')
		->join('investment_equity_lots', 'investment_equity_lots.budget_allocation_id', '=', 'investment_budget_allocation.id')
		->where('investment_budget_allocation.fin_year_id',$fin_id)
		->union($loans)
		->groupBy('budget_allocation_id','investment_budgets.fin_year_id', 'name','investment_budget_allocation.percent','investment_budget_allocation.amount','investment_budget_allocation.code_value_reference');

		$collective = DB::table('main.investment_budgets')
   	    ->select(DB::raw('sum(purchased_unit) as total_investment,investment_budgets.fin_year_id,
            budget_allocation_id as investment_budget_allocation_id, name,investment_budget_allocation.percent ,investment_budget_allocation.amount,investment_budget_allocation.code_value_reference as cid'))
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->join('code_values', 'code_values.reference', '=', 'investment_budget_allocation.code_value_reference')
		->join('investment_collective_lots', 'investment_collective_lots.budget_allocation_id', '=', 'investment_budget_allocation.id')
		->where('investment_budget_allocation.fin_year_id',$fin_id)
		->union($equity)
		->groupBy('budget_allocation_id','investment_budgets.fin_year_id', 'name','investment_budget_allocation.percent','investment_budget_allocation.amount','investment_budget_allocation.code_value_reference');

		$summary  = $collective;

		return DataTables::of($summary)
		->editColumn("amount", function($summary){
			return number_format($summary->amount,2) ;
		})
		->make(true);
	return $summary;
   }

   public function investmentTypeDatatable($fin_id, $budget_alocation_id, $reference){
        $investment_type = '';
		switch ($reference) {
			case 'INTFDR':
			$investment_type = InvestimentFixedDeposit::select('*','opening_date as start_date','institution_name as issuer','investiment_fixed_deposits.id as inv_id')
			->join('investment_budget_allocation','investment_budget_allocation.id','=','investiment_fixed_deposits.investment_budget_allocation_id')
			->where('investment_budget_allocation_id',$budget_alocation_id)
			->where('investment_budget_allocation.fin_year_id',$fin_id);
			break;
			case 'INTTBILLS':
			$investment_type = InvestimentTreasuryBill::select('*','settlement_date as start_date','investiment_treasury_bills.id as inv_id')
			->join('investment_budget_allocation','investment_budget_allocation.id','=','investiment_treasury_bills.investment_budget_allocation_id')
			->where('investment_budget_allocation_id',$budget_alocation_id)
			->where('investment_budget_allocation.fin_year_id',$fin_id);
			break;
			case 'INTTBONDS':
			$investment_type = InvestimentTreasuryBond::select('*','settlement_date as start_date','investiment_treasury_bonds.id as inv_id')
			->join('investment_budget_allocation','investment_budget_allocation.id','=','investiment_treasury_bonds.investment_budget_allocation_id')
			->where('investment_budget_allocation_id',$budget_alocation_id)
			->where('investment_budget_allocation.fin_year_id',$fin_id);
			break;
			case 'INTCBONDS':
			$investment_type = InvestimentCorporateBond::select('*','settlement_date as start_date','investiment_corporate_bonds.id as inv_id')
			->join('investment_budget_allocation','investment_budget_allocation.id','=','investiment_corporate_bonds.investment_budget_allocation_id')
			->where('investment_budget_allocation_id',$budget_alocation_id)
			->where('investment_budget_allocation.fin_year_id',$fin_id);
			break;
			default:
			return redirect()->back();
			break;
		}

		return DataTables::of($investment_type)
		->editColumn("amount_invested", function($investment_type){
			return number_format($investment_type->amount_invested,2) ;
		})
		->editColumn("issuer", function($investment_type) use($reference){
			if ($reference == 'INTTBILLS' || $reference == 'INTTBONDS') {
				return 'BOT';
			} else {
				return $investment_type->issuer;
			}
		})
		->make(true);
	     return $investment_type;

   }

   public function showFixedDepositDatatable($investment_id){
   	$investment = FixedDepositUpdate::where('investment_fixed_deposit_id',$investment_id)->orderBy('created_at','desc')->get();
   	if (count($investment) > 0) {
   		return DataTables::of($investment)
		->editColumn("interest_income", function($investment_type) use ($investment_id) {
			
			$income = new InvestimentTypeRepository();
			$fixed = InvestimentFixedDeposit::find($investment_id);
			$interest_income = $income->interestIncome($fixed->tax_rate,$fixed->interest_rate,$fixed->amount_invested,$fixed->opening_date);

			return number_format($interest_income,4) ;
		})
		->editColumn("weighted_average_return", function($investment_type) use($investment_id){
			$war = new InvestimentTypeRepository();
			$total = InvestimentFixedDeposit::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();
			$fixed = InvestimentFixedDeposit::find($investment_id);
		    $war = $war->war($fixed->interest_rate,$fixed->amount_invested,$total->total_principle_amount);

			return number_format($war,2) ;
		})
		->editColumn("interest_receivable", function($investment_type) use ($reference) {
			return number_format($investment_type->interest_receivable,4) ;
		})
		->make(true);
   	} else {
   		$reference = 'INTFDR';
   		$investment = $this->findInvestmentDatatable($investment_id,$reference);
   	}
   	
		return $investment;

   }

   public function showTBondsDatatable($investment_id){
   	$investment = InvestimentTreasuryBond::where('investiment_treasury_bonds.id',$investment_id)
   	->leftJoin('bond_increment_dates','bond_increment_dates.treasury_bond_id','=','investiment_treasury_bonds.id')
   	->orderBy('bond_increment_dates.id','asc');
   	return DataTables::of($investment)
		->editColumn("amount_invested", function($investment_type){
			return number_format($investment_type->amount_invested,2) ;
		})
		->addColumn('interest_receivable', function($data){
            $interest_receivable = $data->interest_earned == $data->interest_amount ? null : $data->interest_receivable;
            return $interest_receivable;
        })
		->make(true);

		return $investment;

   }

    public function showTBillsDatatable($investment_id){
   	$investment = InvestimentTreasuryBill::where('investiment_treasury_bills.id',$investment_id);
   	return DataTables::of($investment)
		->editColumn("amount_invested", function($investment_type){
			return number_format($investment_type->amount_invested,2) ;
		})
		->addColumn('interest_receivable', function($data){
            $interest_receivable = $data->interest_receivable == $data->receivable ? null : $data->interest_receivable;
            return number_format($interest_receivable,4);
        })
        ->editColumn('with_holding_tax', function($data){
            // $with_holding_tax = $this->withHoldingTax($data->interest_receivable, $data->tax_rate);
            return number_format($data->with_holding_tax,4);
        })
        ->addColumn('outstanding', function($data){
            $outstanding = $this->tbOutstanding($data->interest_receivable, $data->receivable);
            return number_format($outstanding, 4);
        })
        ->addColumn('interest_income', function($data){
            return number_format($data->interest_income, 4);
        })
        ->addColumn('cost', function($data){
            return number_format($data->cost, 4);
        })
        ->editColumn('yield', function($data){
            return round($data->yield, 2).'%';
        })
		->make(true);

		return $investment;

   }
   public function tbOutstanding($interest_receivable, $receivable){
       return round(($interest_receivable - $receivable), 4);
   }

   public function withHoldingTax($receivable, $tax_rate){
         $tax = round(($tax_rate / 100), 4);
         return round(($receivable * $tax), 4);
   }

    public function showCBondsDatatable($investment_id){
   	$investment = InvestimentCorporateBond::where('investiment_corporate_bonds.id',$investment_id)
   	->leftJoin('bond_increment_dates','bond_increment_dates.corporate_bond_id','=','investiment_corporate_bonds.id')
   	->orderBy('bond_increment_dates.id','asc');
   	return DataTables::of($investment)
		->editColumn("amount_invested", function($investment_type){
			return number_format($investment_type->amount_invested,2) ;
		})
		->addColumn('interest_receivable', function($data){
            $interest_receivable = $data->interest_earned == $data->interest_amount ? null : $data->interest_receivable;
            return $interest_receivable;
        })
		->make(true);

		return $investment;

   }

   public function findInvestmentDatatable($investment_id, $reference){
   	    $investment = '';
		switch ($reference) {
			case 'INTFDR':
			$investment = InvestimentFixedDeposit::where('id',$investment_id);
			break;
			case 'INTTBILLS':
			$investment = InvestimentTreasuryBill::where('id',$investment_id);
			break;
			case 'INTTBONDS':
			$investment = InvestimentTreasuryBond::where('id',$investment_id);
			break;
			case 'INTCBONDS':
			$investment = InvestimentCorporateBond::select('*','investment_code_types.name as bond_type')
			->join('investment_code_types','investment_code_types.id','=','investiment_corporate_bonds.type')
			->where('investiment_corporate_bonds.id',$investment_id);
			break;
			default:
			return redirect()->back();
			break;
		}

		return DataTables::of($investment)
		->editColumn("amount_invested", function($investment_type){
			return number_format($investment_type->amount_invested,2) ;
		})
		->editColumn("interest_income", function($investment_type) use ($reference) {
			$income = new InvestimentTypeRepository();
			$interest_income = $income->interestIncome($investment_type->tax_rate,$investment_type->interest_rate,$investment_type->amount_invested,$investment_type->opening_date);

			return number_format($interest_income,4) ;
		})
		->editColumn("interest_receivable", function($investment_type) use ($reference) {
			return number_format($investment_type->interest_receivable,4) ;
		})
		->editColumn("weighted_average_return", function($investment_type) use($reference) {
			$war = new InvestimentTypeRepository();
			switch ($reference) {
			case 'INTFDR':
			$total = InvestimentFixedDeposit::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();
			break;
			case 'INTTBILLS':
			$total = InvestimentTreasuryBill::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();
			break;
			case 'INTTBONDS':
			$total = InvestimentTreasuryBond::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();
			break;
			case 'INTCBONDS':
			$total = InvestimentCorporateBond::select(DB::raw('sum(amount_invested) as total_principle_amount'))->first();
			break;
			default:
			return 0;
			break;
		    }
		    $war = $war->war($investment_type->interest_rate,$investment_type->amount_invested,$total->total_principle_amount);

			return number_format($war,2) ;
		})
		->make(true);

		return $investment;


   }

     public function findInvestment($investment_id, $reference){
   	    $investment = '';
		switch ($reference) {
			case 'INTFDR':
			$investment = InvestimentFixedDeposit::find($investment_id);
			break;
			case 'INTTBILLS':
			$investment = InvestimentTreasuryBill::find($investment_id);
			break;
			case 'INTTBONDS':
			$investment = InvestimentTreasuryBond::find($investment_id);
			break;
			case 'INTCBONDS':
			$investment = InvestimentCorporateBond::find($investment_id);
			break;
			default:
			return redirect()->back();
			break;
		}
		return $investment;


   }

   public function budget($investment_id, $reference){

		$budget = '';
		switch ($reference) {
			case 'INTFDR':
			$budget = InvestimentFixedDeposit::select('investment_budget_allocation.amount as amount_alocated','investment_budgets.amount as total_amount_invested','investment_budget_allocation.id as bgtId')
			->join('investment_budget_allocation','investment_budget_allocation.id','=','investiment_fixed_deposits.investment_budget_allocation_id')
			->join('investment_budgets','investment_budgets.id','=','investment_budget_allocation.investment_budget_id')
			->where('investiment_fixed_deposits.id',$investment_id)
			->first();
			break;
			case 'INTTBILLS':
			$budget = InvestimentTreasuryBill::select('investment_budget_allocation.amount as amount_alocated','investment_budgets.amount as total_amount_invested','investment_budget_allocation.id as bgtId')
			->join('investment_budget_allocation','investment_budget_allocation.id','=','investiment_treasury_bills.investment_budget_allocation_id')
			->join('investment_budgets','investment_budgets.id','=','investment_budget_allocation.investment_budget_id')
			->where('investiment_treasury_bills.id',$investment_id)
			->first();
			break;
			case 'INTTBONDS':
			$budget = InvestimentTreasuryBond::select('investment_budget_allocation.amount as amount_alocated','investment_budgets.amount as total_amount_invested','investment_budget_allocation.id as bgtId')
			->join('investment_budget_allocation','investment_budget_allocation.id','=','investiment_treasury_bonds.investment_budget_allocation_id')
			->join('investment_budgets','investment_budgets.id','=','investment_budget_allocation.investment_budget_id')
			->where('investiment_treasury_bonds.id',$investment_id)
			->first();
			break;
			case 'INTCBONDS':
			$budget = InvestimentCorporateBond::select('investment_budget_allocation.amount as amount_alocated','investment_budgets.amount as total_amount_invested','investment_budget_allocation.id as bgtId')
			->join('investment_budget_allocation','investment_budget_allocation.id','=','investiment_corporate_bonds.investment_budget_allocation_id')
			->join('investment_budgets','investment_budgets.id','=','investment_budget_allocation.investment_budget_id')
			->where('investiment_corporate_bonds.id',$investment_id)
			->first();
			break;
			default:
			return redirect()->back();
			break;
		}

		return $budget;
   }

   public function calculate($request){
      $totalAarray = [];
		switch ($request->reference) {
			case 'INTFDR':

			$budget = DB::table('main.investment_budget_allocation')
			->select('investment_budget_allocation.amount as amount_alocated')
			->where('investment_budget_allocation.id',$request->budget_id)
			->first();

			$total = InvestimentFixedDeposit::select(DB::raw('sum(amount_invested) as total_amount'))->first();
			$remained = $budget->amount_alocated - $total->total_amount;
			$totalAarray = [
               'used_budget' => number_format($total->total_amount, 4),
               'remained_budget' => number_format($remained, 4),
			];

			break;
			case 'INTTBILLS':
			$budget = DB::table('main.investment_budget_allocation')
			->select('investment_budget_allocation.amount as amount_alocated')
			->where('investment_budget_allocation.id',$request->budget_id)
			->first();

			$total = InvestimentTreasuryBill::select(DB::raw('sum(amount_invested) as total_amount'))->first();
			$remained = $budget->amount_alocated - $total->total_amount;
			$totalAarray = [
               'used_budget' => number_format($total->total_amount, 4),
               'remained_budget' => number_format($remained, 4),
			];

			break;
			case 'INTTBONDS':
			$budget = DB::table('main.investment_budget_allocation')
			->select('investment_budget_allocation.amount as amount_alocated')
			->where('investment_budget_allocation.id',$request->budget_id)
			->first();

			$total = InvestimentTreasuryBond::select(DB::raw('sum(amount_invested) as total_amount'))->first();
			$remained = $budget->amount_alocated - $total->total_amount;
			$totalAarray = [
               'used_budget' => number_format($total->total_amount, 4),
               'remained_budget' => number_format($remained, 4),
			];

			break;
			case 'INTCBONDS':
			
			$budget = DB::table('main.investment_budget_allocation')
			->select('investment_budget_allocation.amount as amount_alocated')
			->where('investment_budget_allocation.id',$request->budget_id)
			->first();

			$total = InvestimentCorporateBond::select(DB::raw('sum(amount_invested) as total_amount'))->first();
			$remained = $budget->amount_alocated - $total->total_amount;
			$totalAarray = [
               'used_budget' => number_format($total->total_amount, 4),
               'remained_budget' => number_format($remained, 4),
			];

			break;
			default:
			return redirect()->back();
			break;
		}
		return $totalAarray;
   }
}