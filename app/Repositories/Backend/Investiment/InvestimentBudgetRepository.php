<?php

namespace App\Repositories\Backend\Investiment;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Finance\FinYear;
use App\Models\Investment\InvestmentBudget;
use App\Models\Investment\InvestmentBudgetAllocation;

use App\Events\NewWorkflow;
use App\Services\Workflow\Workflow;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\DataTables\WorkflowTrackDataTable;
use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;

use Excel;
use Exception;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Shared_Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use DateTime;
use DateTimeZone;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class InvestimentBudgetRepository extends BaseRepository
{

	const MODEL = InvestmentBudget::class;

	function __construct($foo = null)
	{
		parent::__construct();	
		$this->wf_module_group_id = 29;
	}



	public function find($id)
	{
		$investment_budget = $this->query()->find($id);
		return $investment_budget;
	}


	public function findOrThrowException($id)
	{
		$investment_budget = $this->query()->find($id);
		if (!is_null($investment_budget)) {
			return $investment_budget;
		}
		throw new GeneralException(trans('Investment budget not found'));
	}


	public function findByFinYearAndId($id,$fin_year_id)
	{
		$investment_budget = $this->query()->where('id',$id)
		->where('fin_year_id',$fin_year_id)->first();
		return $investment_budget;
	}

	public function findByFinYear($fin_year_id)
	{
		$investment_budget = $this->query()->where('fin_year_id',$fin_year_id)->first();
		return $investment_budget;
	}


	public function findAllocationOrThrowException($allocation_id)
	{
		$investment_budget_allocation = InvestmentBudgetAllocation::find($allocation_id);
		if (!is_null($investment_budget_allocation)) {
			return $investment_budget_allocation;
		}
		throw new GeneralException(trans('Investment budget allocation not found'));
	}

	public function findAllocation($allocation_id)
	{
		$investment_budget_allocation = InvestmentBudgetAllocation::find($allocation_id);
		return $investment_budget_allocation;
	}

	public function findAllocationByBudgetId($investment_budget_id,$code_value_id)
	{
		$investment_budget_allocation = InvestmentBudgetAllocation::
		where('investment_budget_id',$investment_budget_id)->where('code_value_id',$code_value_id)->first();
		return $investment_budget_allocation;
	}


	public function update($id,$data)
	{
		$this->query()->find($id)->update($data);
	}


	public function updateAllocation($allocation_id, $data)
	{
		InvestmentBudgetAllocation::find($allocation_id)->update($data);
	}

	public function updateAllocationByBudgetId($investment_budget_id,$code_value_id, $data)
	{
		$inv_allocation = InvestmentBudgetAllocation::where('investment_budget_id',$investment_budget_id)
		->where('code_value_id',$code_value_id)->first();
		if (count($inv_allocation)) {
			$inv_allocation->update($data);
		}
	}

	public function getForDatatable()
	{
		return $this->query()->select('*','investment_budgets.id as id')
		->join('fin_years', 'investment_budgets.fin_year_id', '=', 'fin_years.id');
	}

	public function recommendAndinitiateWf($id)
	{

		$budget = $this->find($id);
		if (count($budget)) {
			$return = DB::transaction(function () use ($id) {
				//update status wf
				$this->update($id,[
					'user_id' => access()->id,
					'status' => 1,
					'updated_at' => Carbon::now()
				]);
				//initiate wf
				$check = workflow([['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $id]])->checkIfHasWorkflow();
				if (!$check) {
					event(new NewWorkflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $id]));
				}
				return true;
			});
			return $return;
		} else {
			return false;
		}
		
	}


	public function setNewBudget($request)
	{

		DB::transaction(function () use ($request) { 
			$amount = str_replace( ',', '',$request->total_budget);
			$amount = !empty($amount) ? number_format((float)$amount, 2, '.', '') : number_format((float)0, 2, '.', '');

			$budget = $this->query()->create([
				'fin_year_id' => $request->financial_year,
				'amount' => $amount,
			]);
			foreach ($request->investment_type as $key => $value) {
				$amount = str_replace( ',', '',$value);
				$amount = !empty($amount) ? number_format((float)$amount, 2, '.', '') : number_format((float)0, 2, '.', '');
				$percent = str_replace( '%', '',str_replace( ',', '',$request->edit_percent[$key]));
				$percent = !empty($percent) ? number_format((float)$percent, 2, '.', '') : number_format((float)0, 2, '.', '');
				$investment_allocation = InvestmentBudgetAllocation::create([
					'investment_budget_id' => $budget->id,
					'fin_year_id' => $request->financial_year,
					'code_value_id' => $key,
					'amount' => $amount,
					'percent' => $percent,
					'updated_at' => Carbon::now(),
					'created_at' => Carbon::now()
				]);
			}
		});
	}


	public function updateBudget($request)
	{
		DB::transaction(function () use ($request) { 
			$amount = str_replace( ',', '',$request->edit_total_budget);
			$total_amount =  !empty($amount) ? number_format((float)$amount, 2, '.', '') : number_format((float)0, 2, '.', '');
			$this->update($request->edit_budget_id,[
				'amount' => $total_amount,
			]);

			foreach ($request->edit_investment_type as $key => $value) {
				$amount = str_replace( ',', '',$value);
				$save_amount = !empty($amount) ? number_format((float)$amount, 2, '.', '') : number_format((float)0, 2, '.', '');
				$percent = str_replace( '%', '',str_replace( ',', '',$request->edit_percent[$key]));
				$save_percent = !empty($percent) ? number_format((float)$percent, 2, '.', '') : number_format((float)0, 2, '.', '');

				$this->updateAllocationByBudgetId($request->edit_budget_id,$key,[
					'amount' => $save_amount,
					'percent' => $save_percent,
				]);
			}
		});
	}

	public function recommendBudget($id,$level)
	{
		if ((int)$level == 1) {
			$this->query()->where('id',$id)->update(['status' => 1]);
		}
	}


	public function rejectBudget($id) {
		$this->query()->where('id',$id)->update(['status' => 3]);
	}

	public function approveBudget($id, $level) {
		if ((int)$level == 2) {
			$this->query()->where('id',$id)->update([
				'wf_done' => 1,
				'wf_done_date' => Carbon::now(),
				'status' => 2
			]);
		}
	}

	public function returnBudgetFinYearForSelect()
	{
		$budgtes = $this->query()->get();
		$fin_years = [];
		foreach ($budgtes as $b) {
			if (!empty($b->fin_year)) {
				$fin_years[$b->fin_year->id] = $b->fin_year->name;
			}
		}
		return $fin_years;
	}


	public function returnLoanAllocatedBudget($fin_year_id,$code_value_id)
	{
		$budget_allocation = InvestmentBudgetAllocation::where('fin_year_id',$fin_year_id)->where('code_value_id',$code_value_id)->first();

		$loan_budget = [];
		if (count($budget_allocation)) {
			$inv_loan = DB::table('main.investment_loans')->sum('loan_amount');
			$total = number_format((float)$budget_allocation->amount, 2, '.', '');
			$loaned = number_format((float)$inv_loan, 2, '.', '');
			$remain = $total - $loaned;
			$loan_budget = [
				'total' => number_format((float)$budget_allocation->amount, 2, '.', ''),
				'loaned' => number_format((float)$inv_loan, 2, '.', ''),
				'remain' => number_format((float)$remain, 2, '.', ''),
			];
		} else {
			$zero = number_format((float)0, 2, '.', '');
			$loan_budget = [
				'total' => $zero,
				'loaned' => $zero,
				'remain' => $zero,
			];
		}
		return $loan_budget;
		
	}

	public function uploadBudget($request)
	{

		$path = $request->file('budget_file')->getRealPath();
		$data = $this->returnExcelArray($path);
		foreach ($data as $key => $value) {
			if(!empty(trim($key))){
				$type_query = DB::table('main.investment_budget_categories')
				->whereRaw("lower(name) = '".strtolower($key)."'")->first();

				if (count($type_query)) {
					$this->saveInvestmentMonths($type_query->id, $request->fin_year_id, $value);
				}	
			}
		}

		$query_months = DB::table('main.investment_budget_months')
		->where('investment_budget_category_id',1)
		->where('fin_year_id',$request->fin_year_id)->get();

		
		if (count($query_months)>0) {
			$total_budget = 0;
			foreach ($query_months as $q) {
				$total_budget += $q->total;
			}

			$find_budget = $this->query()->where('fin_year_id',$request->fin_year_id)->first();
			if (count($find_budget)) {
				$find_budget->amount =  number_format((float)$total_budget, 2, '.', '');
				$find_budget->save();
			} else {
				$find_budget = $this->query()->create([
					'fin_year_id' => $request->fin_year_id,
					'amount' => number_format((float)$total_budget, 2, '.', ''),
				]);
			}
			
			if (count($find_budget)) {
				foreach ($query_months as $qm) {
					$percent = (!empty($find_budget->amount) && !empty($qm->total)) ? ($qm->total/$find_budget->amount)*100 : 0.00;
					InvestmentBudgetAllocation::updateOrCreate([
						'investment_budget_id' => $find_budget->id,
						'code_value_reference' => $qm->code_value_reference,
						'investment_code_type_id'=> $qm->investment_code_type_id,
					],
					[
						'code_value_id' => $qm->code_value_id,
						'fin_year_id' => $request->fin_year_id,
						'amount' => $qm->total,
						'percent' => $percent,
						'updated_at' => Carbon::now(),
						'created_at' => Carbon::now()
					]);
				}
			}
		}



	}


	public function saveInvestmentMonths($type_id,$fin_year_id, $data)
	{
		foreach ($data as $key => $value) {
			foreach ($value as $k => $e) {	

				$budget_array = [];
				foreach ($e as $month => $budget) {
					if ($month != 'total') {
						$budget_array[$month] = !empty($budget) ? number_format((float)$budget, 2, '.', '') :  number_format((float)0, 2, '.', '');
					}
				}

				$total_b = 0;
				foreach ($budget_array as $b) {
					$total_b += $b;
				}
				$budget_array['total'] = $total_b;

				$code_type = DB::table('main.investment_code_types')
				->whereRaw("lower(name) = '".strtolower($k)."'")->first();
				if (count($code_type)) {
					$code_v = DB::table('main.code_values')->where('reference',$code_type->code_value_reference)->first();
				} else {
					$code_v = DB::table('main.code_values')->whereRaw("lower(name) = '".strtolower($k)."'")->first();
				}
				if (count($code_v) && count($budget_array)>0) {

					$budget_array['investment_code_type_id'] =  count($code_type) ? $code_type->id : null;
					$budget_array['created_at'] =  Carbon::now();
					$budget_array['updated_at'] =  Carbon::now();
					DB::table('main.investment_budget_months')->updateOrInsert(
						[
							'investment_budget_category_id' => $type_id,
							'fin_year_id' => $fin_year_id,
							'code_value_reference' => $code_v->reference,
							'investment_type_name' => ucwords(strtolower($k)),
							'code_value_id' => $code_v->id,
						],
						$budget_array
					);
				}
			}
		}
	}


	public function returnExcelArray($file)
	{
		
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
		$referenceRow= array();

		$row_num = $spreadsheet->getSheet(0)->getHighestDataRow();
		$value=[];
		$array_p = [];
		for ( $row = 2; $row <= $row_num ; $row++ ){
			
			$row_array = array();
			$my_array = [];
			$col_array = [];
			for ( $col = 1; $col <= 15; $col++ ){
				if (!$spreadsheet->getActiveSheet()->getCellByColumnAndRow( $col, $row )->isInMergeRange() || $spreadsheet->getActiveSheet()->getCellByColumnAndRow( $col, $row )->isMergeRangeValueCell()) {
					$data[$row][$col] = $spreadsheet->getActiveSheet()->getCellByColumnAndRow( $col, $row )->getCalculatedValue();
					$referenceRow[$col]=$data[$row][$col];  
				} else {
					$data[$row][$col] = $referenceRow[$col]; 
				}

				switch ($col) {
					case 1:
					$row_array['category'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][$col])] = [];
					break;
					case 2:
					$row_array['investment_type'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][$col])] = [];
					break;
					case 3:
					$row_array['july'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['july'] = trim($data[$row][$col]);
					break;
					case 4:
					$row_array['august'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['august'] = trim($data[$row][$col]);
					break;
					case 5:
					$row_array['september'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['september'] = trim($data[$row][$col]);
					break;
					case 6:
					$row_array['october'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['october'] = trim($data[$row][$col]);
					break;
					case 7:
					$row_array['november'] = trim($data[$row][$col]);					
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['november'] = trim($data[$row][$col]);
					break;
					case 8:
					$row_array['december'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['december'] = trim($data[$row][$col]);
					break;
					case 9:
					$row_array['january'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['january'] = trim($data[$row][$col]);
					break;
					case 10:
					$row_array['february'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['february'] = trim($data[$row][$col]);
					break;
					case 11:
					$row_array['march'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['march'] = trim($data[$row][$col]);
					break;
					case 12:
					$row_array['april'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['april'] = trim($data[$row][$col]);
					break;
					case 13:
					$row_array['may'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['may'] = trim($data[$row][$col]);
					break;
					case 14:
					$row_array['june'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['june'] = trim($data[$row][$col]);
					break;
					case 15:
					$row_array['total'] = trim($data[$row][$col]);
					$my_array[trim($data[$row][1])][trim($data[$row][2])]['total'] = trim($data[$row][$col]);
					break;
					default:
					break;
				}
				
			}
			
			if(!empty(trim($row_array['category']))){
				array_push($array_p, $my_array);
			}
		}


		$type_array = [];
		foreach ($array_p as $key => $val_arr) {
			foreach ($val_arr as $key => $v) {
				if (array_key_exists($key,$type_array)) {
				}else{
					$type_array[$key] = [];
				}
				array_push($type_array[$key], $v);
			}
		}

		return $type_array;
	}

	public function returnInvestmentCodeTypeName($code_type_id)
	{
		$inv_code_type = DB::table('main.investment_code_types')->find($code_type_id);
		if (count($inv_code_type)) {
			return $inv_code_type->name;
		}else{
			return '';	
		}
	}



}
