<?php

namespace App\Repositories\Backend\WorkPlaceRiskAssesment;
/*Juma*/
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\DataTables\WorkflowTrackDataTable;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Models\WorkPlaceRiskAssesment\OshAuditFeedback;
use App\Models\WorkPlaceRiskAssesment\OshAuditQuestion;
use App\Models\WorkPlaceRiskAssesment\OshAuditPeriodEmployer;
use App\Models\WorkPlaceRiskAssesment\OshAuditPeriod;
use App\Models\WorkPlaceRiskAssesment\OshAuditRating;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OshAuditEmployerRepository extends BaseRepository
{

	const MODEL = OshAuditPeriodEmployer::class;

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll()
	{
		return $this->query()->get();
	}


	public function findPeriodProfile($id)
	{

		$osh_audit = OshAuditPeriod::find($id);
		if (!empty($osh_audit->status)) {
			return $osh_audit;
		}
		throw new GeneralException('Employer profile not existing');
	}


	public function findOrThrowException($osh_audit_employer_id)
	{

		$osh_audit_employer = $this->query()
		->select('osh_audit_period_employers.*','osh_audit_period.*', 'osh_audit_period_employers.id as id','employers.name as name','employers.reg_no', 'districts.name as district','regions.name as region','osh_audit_period_employers.user_id as user_id')
		->join('main.employers','employers.id','=','osh_audit_period_employers.employer_id')
		->join('main.osh_audit_period','osh_audit_period.id','=','osh_audit_period_employers.osh_audit_period_id')
		->leftJoin('main.districts','districts.id','=','osh_audit_period_employers.district_id')
		->leftJoin('main.regions','regions.id','=','districts.region_id')
		->where('osh_audit_period_employers.id',$osh_audit_employer_id)->first();

		if (!is_null($osh_audit_employer)) {
			return $osh_audit_employer;
		}
		throw new GeneralException('Employer Audit profile not existing');
	}

	public function returnFeedbacks($osh_audit_employer_id)
	{
		return OshAuditFeedback::where('osh_audit_period_employer_id',$osh_audit_employer_id)->get();
	}


	public function returnOshAuditCodeValues()
	{
		$arr = [
			'OACOHSA','OACADORR','OACT','OACERG','OACHW','OACPPE','OACMUM','OACCHEM','OACEPRE'
		];
		return DB::table('main.code_values')->whereIn('reference',$arr)->orderBy('sort','asc')->get();
	}

	public function assignAuditToUser(array $input)
	{
		
		DB::transaction(function () use ($input) {
			foreach ($input['selected_employers'] as $id) {
				$this->query()->where('id',$id)->update([
					'user_id' => $input['assigned_user'],
				]);
			}

		});

	}

	public function auditEmployerProfile($osh_audit_period_id, $employer_id)
	{

		$osh_audit_employer = $this->query()
		->select('osh_audit_period_employers.*','osh_audit_period.*', 'osh_audit_period_employers.id as id','employers.name as name','districts.name as district','employers.reg_no','regions.name as region','osh_audit_period_employers.user_id as user_id',DB::raw("(SELECT count(*) from main.employee_employer where employer_id= ".$employer_id." and deleted_at is null) employee_count"))
		->join('main.employers','employers.id','=','osh_audit_period_employers.employer_id')
		->join('main.osh_audit_period','osh_audit_period.id','=','osh_audit_period_employers.osh_audit_period_id')
		->leftJoin('main.districts','districts.id','=','osh_audit_period_employers.district_id')
		->leftJoin('main.regions','regions.id','=','districts.region_id')
		->where('employer_id',$employer_id)->where('osh_audit_period_id',$osh_audit_period_id)->first();

		if (!is_null($osh_audit_employer)) {
			return $osh_audit_employer;
		}
		throw new GeneralException('Employer audit profile not created yet');
	}



	public function updateFeedback(array $input)
	{
		
		DB::transaction(function () use ($input) {
			foreach ($input['answers'] as $key => $value) {
				OshAuditFeedback::where('id', $key)->update([
					'feedback' => $value,
					'comment' => $input['comments'][$key]
				]);
			}
		});
	}


	public function returnProfileNotificationsForDatatable($osh_audit_employer_id, $type)
	{
		$query = DB::table('main.osh_audit_employer_notifications')
		->select(DB::raw("CONCAT(UPPER(employees.firstname),' ',UPPER(employees.middlename),' ',UPPER(employees.lastname)) as name"),
			'notification_reports.filename','incident_types.name as incident','osh_audit_employer_notifications.notification_report_id','notification_reports.reporting_date')
		->join('main.notification_reports', 'osh_audit_employer_notifications.notification_report_id', '=', 'notification_reports.id')
		->join('main.incident_types', 'notification_reports.incident_type_id', '=', 'incident_types.id')
		->join('main.employees', 'notification_reports.employee_id', '=', 'employees.id')
		->where('osh_audit_employer_id',$osh_audit_employer_id);

		switch ($type) {
			case 'fatal':
			$query->where('is_fatal',true);
			break;
			case 'days_lost':
			$query->where('is_lost_days',true);
			break;
			default:
			case 'non_conveyance':
			$query->where('not_conveyance_accident',true);
			break;
		}

		return $query;
	}


	public function returnFeedbackScore($osh_audit_employer_id)
	{
		$code_values = $this->returnOshAuditCodeValues();

		$feedback_score = [];
		foreach ($code_values as $code_value) {
			$score_array = $this->returnScore($osh_audit_employer_id, $code_value->id);
			$feedback_score[$code_value->id] = $score_array;
			$feedback_score[$code_value->id]['reference'] = $code_value->reference;
		}

		$overall = $feedback_score;

		$feedback_score['overall']['actual_score'] = 0;
		$feedback_score['overall']['total_possible'] = 0;
		foreach ($overall as $value) {
			$feedback_score['overall']['actual_score'] += $value['actual_score'];
			$feedback_score['overall']['total_possible'] += $value['total_possible'];
		}
		$overall_percent =  ($feedback_score['overall']['actual_score']/$feedback_score['overall']['total_possible'])*100;
		$feedback_score['overall']['percent'] = number_format($overall_percent,2);

		$feedback_score['rating'] = $this->employerRating($feedback_score['overall']['percent']);

		// dd($feedback_score);
		return $feedback_score;
	}


	public function returnScore($osh_audit_employer_id, $code_value_id)
	{
		$actual_score = OshAuditFeedback::select('feedback')
		->join('main.osh_audit_questions', 'osh_audit_questions.id', '=', 'osh_audit_feedback.osh_audit_question_id')
		->where('osh_audit_period_employer_id',$osh_audit_employer_id)->where('code_value_id',$code_value_id)->get();

		$array = [];
		$array['actual_score'] = 0;
		foreach ($actual_score as $actual) {
			if ($actual->feedback == 'Yes') {
				$array['actual_score']+=1;
			}
		}

		$array['total_possible'] = count($actual_score);
		$array['percent'] = ($array['actual_score'] > 0) ? number_format(($array['actual_score'] / $array['total_possible'])*100,2) : number_format(0,2);
		return $array;

	}

	public function employerRating($overall_percent)
	{

		if ($overall_percent <= 49) {
			$rating['score'] = 'LOW';
			$rating['tag'] = 'tag-danger';
		} elseif(($overall_percent >= 50) && ($overall_percent <= 69)) {
			$rating['score'] = 'MEDIUM';
			$rating['tag'] = 'tag-warning';
		}else{
			$rating['score'] = 'HIGH';
			$rating['tag'] = 'tag-success';
		}
		return $rating;
	}


	public function returnQuestions($osh_audit_employer_id, $code_value_id)
	{
		$actual_score = OshAuditFeedback::select('osh_audit_feedback.id as id','feedback','question','comment','osh_audit_questions.id as question_id')
		->join('main.osh_audit_questions', 'osh_audit_questions.id', '=', 'osh_audit_feedback.osh_audit_question_id')
		->where('osh_audit_period_employer_id',$osh_audit_employer_id)->where('code_value_id',$code_value_id)->get();

		return $actual_score;

	}


	public function updateFeedbackGeneralInfo(array $input)
	{
		DB::transaction(function () use ($input) {

			$score_array = $this->returnFeedbackScore($input['osh_audit_employer_id']);
			$osh_audit_employer = $this->findOrThrowException($input['osh_audit_employer_id']);

			$input['osh_audit_period_id'] = $osh_audit_employer->osh_audit_period_id;
			$input['employer_id'] = (int)substr($osh_audit_employer->reg_no, -6);
			$input['period'] = $osh_audit_employer->period;
			$input['fin_year'] = $osh_audit_employer->fin_year;
			$this->updateRatings($input, $score_array);

			$this->query()->where('id', $input['osh_audit_employer_id'])->update([
				'general_comments' => $input['general_comment'],
				'workplace_name' => ucfirst(strtolower($input['workplace_name'])),
				'workplace_address' => ucfirst(strtolower($input['workplace_address'])),
				'workplace_location' => ucfirst(strtolower($input['workplace_location'])),
				'district_id' => $input['workplace_district'],
				'contact_person' => ucfirst(strtolower($input['contact_person'])),
				'contact_person_phone' => $input['phone'],
				'is_submitted' => true,
			]);
		});
	}


	public function updateRatings(array $data, array $score_array)
	{

		// dump($score_array);
		$osh_audit_rating = OshAuditRating::firstOrNew([
			'osh_audit_period_employer_id'=> $data['osh_audit_employer_id'],
		]);
		$osh_audit_rating->osh_audit_period_employer_id = $data['osh_audit_employer_id'];
		$osh_audit_rating->osh_audit_period_id = $data['osh_audit_period_id'];
		$osh_audit_rating->employer_id = $data['employer_id'];
		$osh_audit_rating->period = $data['period'];
		$osh_audit_rating->fin_year = $data['fin_year'];
		foreach ($score_array as $key => $value) {
			if(isset($value['reference'])){
				switch ($value['reference']) {
					case 'OACOHSA':
					$osh_audit_rating->ohsa = number_format($value['actual_score'],2);
					break;
					case 'OACADORR':
					$osh_audit_rating->adorr = number_format($value['actual_score'],2);
					break;
					case 'OACT':
					$osh_audit_rating->training = number_format($value['actual_score'],2);
					break;
					case 'OACERG':
					$osh_audit_rating->ergonomics = number_format($value['actual_score'],2);
					break;
					case 'OACHW':
					$osh_audit_rating->health_welfare = number_format($value['actual_score'],2);
					break;
					case 'OACPPE':
					$osh_audit_rating->personal_protective_equipment = number_format($value['actual_score'],2);
					break;
					case 'OACMUM':
					$osh_audit_rating->machine_use_maintainance = number_format($value['actual_score'],2);
					break;
					case 'OACCHEM':
					$osh_audit_rating->chemicals = number_format($value['actual_score'],2);
					break;
					case 'OACEPRE':
					$osh_audit_rating->emergency_preparedness = number_format($value['actual_score'],2);
					break;
					default:
					break;
				}
			}
		}
		$osh_audit_rating->rate = number_format($score_array['overall']['percent'],2);
		$osh_audit_rating->total_score = number_format($score_array['overall']['actual_score'],2);
		$osh_audit_rating->average_score = number_format($score_array['overall']['actual_score']/9,2);
		$osh_audit_rating->possible_score = number_format($score_array['overall']['total_possible'],2);
		$osh_audit_rating->save();
	}


	public function isCodeValueFilled($osh_audit_employer_id)
	{
		$code_values = $this->returnOshAuditCodeValues();

		$is_codevalue_filled = [];

		foreach ($code_values as $cd) {
			$osh_audit_feedback = OshAuditFeedback::select('feedback')
			->join('main.osh_audit_questions', 'osh_audit_questions.id', '=', 'osh_audit_feedback.osh_audit_question_id')
			->where('osh_audit_period_employer_id',$osh_audit_employer_id)->where('code_value_id',$cd->id)->whereNotNull('feedback')->first();

			if (count($osh_audit_feedback)) {
				$is_codevalue_filled[$cd->id] = true;
			}else{
				$is_codevalue_filled[$cd->id] = false;
			}
			
		}

		return $is_codevalue_filled;
		
	}


	public function isFeedbackSubmitted($osh_audit_employer_id)
	{
		
	}

	public function getMyPendingOshAuditCount()
	{
		return $this->query()
		->where("user_id", access()->user()->id)
		->where('is_submitted',false)
		->count();
	}

}
