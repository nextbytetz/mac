<?php

namespace App\Repositories\Backend\WorkPlaceRiskAssesment;

use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Exceptions\GeneralException;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\BaseRepository;
use App\Services\System\Database\CheckForeignReference;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Compliance\Member\Employer;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Carbon\Carbon;

class OshDataManagementRepository extends  BaseRepository
{
    const MODEL = NotificationReport::class;
    protected $employees;

    /**
     * OshData constructor.
     */
    public function __construct( )
    {
        $this->employees = new EmployeeRepository();
    }

    public function getEmployeeData(){
        return $this->employees->getForDataTable();
    }

    public function getEmployee($id){
        return $this->employees->findOrThrowException($id);
    }

    public function tascoOccupation($id,$not_id){
        return $this->employees->employeeForOshModule($id,$not_id);
    }

    public function employmentCategories(){
        return $employment_categories = DB::table('code_values')
                ->where('code_id',7)
                ->pluck('name','id');
    }

    public function employerCategories(){
        return $employer_categories = DB::table('code_values')
                ->where('code_id',4)
                ->pluck('name','id');
    }

    public function bussinesSectors(){
        return $bussines_sectors = DB::table('code_values')
                ->where('code_id',5)
                ->pluck('name','id');
    }
    public function accidentTypes(){
        return $accident_types = DB::table('accident_types')
                ->pluck('name','id');
    }

    public function occupations(){
        return $occupations = DB::table('occupations')->pluck('tasco','id');
    }

    public function jobTitles(){
        return $occupations = DB::table('main.job_titles')->pluck('name','id');
    }

    public function allworkplaces(){
        $workplaces = DB::table('workplaces')->select(DB::raw("CONCAT(workplace_regno ,'-', workplace_name) as workplace_no"),'id')->get()->pluck('workplace_no','id');
        return $workplaces;
    }

    public function finYears(){
        $fin_years = DB::table('fin_years')
        ->pluck('name','name');
        return $fin_years;
    }

    public function getForDataTable()
    {
        // dd(request()->all());
           $incidents = $this->query()->select([
            DB::raw("notification_reports.id as case_no"),
            DB::raw("employees.id as employee_id"),
            DB::raw("incident_types.name as incident_type"),
            DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as fullname"),
            DB::raw("employees.firstname as firstname"),
            DB::raw("employees.middlename as middlename"),
            DB::raw("employees.lastname as lastname"),
            DB::raw("genders.name as gender"),
            DB::raw("bsns.name as bussines_sector"),
            DB::raw("emplyee.name as employment_category"),
            DB::raw("emplyer.name as employer_category"),
            DB::raw("employers.name as employer"),
             DB::raw("employees.gender_id"),
            DB::raw("notification_reports.incident_date as incident_date"),
            DB::raw("notification_reports.receipt_date as receipt_date"),
            DB::raw("notification_reports.status as status"),
            DB::raw("notification_reports.filename as filename"),
            DB::raw("coalesce(regions.name, '') as region"),
            DB::raw("notification_staging_cv_id"),
            DB::raw("employers.employer_category_cv_id"),
            DB::raw("notification_staging_cv_id"),
            DB::raw("notification_staging_cv_id"),
            DB::raw("job_titles.name as occupation"),
            DB::raw("CONCAT(workplace_regno ,'-', workplace_name) as workplace_no"),
            DB::raw("workplace_name"),
            DB::raw("workplace_regno"),
            DB::raw("EXTRACT(year FROM age(incident_date,employees.dob)) :: int as age")
        ])
            ->join("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
            ->join("employers", "employers.id", "=", "notification_reports.employer_id")
            ->Join("employees", "employees.id", "=", "notification_reports.employee_id")
            ->leftJoin("job_titles", "job_titles.id", "=", "employees.job_title_id")
            ->leftJoin("genders", "genders.id", "=", "employees.gender_id")
            ->leftJoin("code_values as emplyee", "emplyee.id", "=", "employees.employee_category_cv_id")
            ->leftJoin("code_values as emplyer", "emplyer.id", "=", "employers.employer_category_cv_id")
            ->leftJoin("employer_sectors", "employer_sectors.employer_id", "=", "employers.id")
            ->leftJoin("code_values as bsns", "bsns.id", "=", "employer_sectors.business_sector_cv_id")
            ->leftJoin("incident_assessments", "incident_assessments.notification_eligible_benefit_id", "=", "notification_reports.id")
            ->leftJoin("occupations", "occupations.id", "=", "incident_assessments.occupation_id")
            ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
            ->leftJoin("regions", "regions.id", "=", "districts.region_id")
            ->leftJoin("workplaces", "workplaces.id", "=", "notification_reports.workplace_id");

        $incident = (int) request()->input("incident");
        if ($incident) {
            $incidents->where("notification_reports.incident_type_id", "=", $incident);
        }
        $workplaces = request()->input("workplaces");
        if ($workplaces) {
            $incidents->where("notification_reports.workplace_id", "=", $workplaces);
        }
        $employee = request()->input("employee");
        if ($employee) {
            $incidents->where("notification_reports.employee_id", "=", $employee);
        }
        $employer = request()->input("employer");
        if ($employer) {
            $incidents->where("notification_reports.employer_id", "=", $employer);
        }
        $region = request()->input("region");
        if ($region) {
            $incidents->where("regions.id", "=", $region);
        }
        $district = request()->input("district");
        if ($district) {
            $incidents->where("districts.id", "=", $district);
        }
        $stage = request()->input("stage");
        if ($stage) {
            $incidents->where("notification_reports.notification_staging_cv_id", "=", $stage);
        }
        $gender = request()->input("gender");
        if ($gender) {
              $incidents->where("genders.id", "=", $gender);            
        }
        if ($gender == '0') {
            $incidents->where("employees.gender_id", "=", null); 
        }
        $employment_category = request()->input("employment_category");
        if ($employment_category) {
            $incidents->where("emplyee.id", "=", $employment_category);
        }
        $employer_category = request()->input("employer_category");
        if ($employer_category) {
            $incidents->where("emplyer.id", "=", $employer_category);
        }
        $bussines_sector = request()->input("bussines_sector");
        if ($bussines_sector) {
            $incidents->where("bsns.id", "=", $bussines_sector);
        }
        $occupation = request()->input("occupation");
        if ($occupation) {
            $incidents->where("job_titles.id", "=", $occupation);
        }
        $start_age = request()->input("start_age");
        $end_age = request()->input("end_age");
        if($start_age && $end_age){
            if ($start_age < $end_age) {
                $incidents->whereBetween(DB::raw("EXTRACT(year FROM age(incident_date,employees.dob)) :: int"), array($start_age, $end_age));
            }
        }

        return $incidents; 
        
        
    }

    public function getForClaimsDataTable($request)
    {
        // dd($request);
           $incidents = $this->query()->select([
            DB::raw("distinct notification_reports.id as case_no"),
            DB::raw("employees.id as employee_id"),
            DB::raw("incident_types.name as incident_type"),
            DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as fullname"),
            DB::raw("employees.firstname as firstname"),
            DB::raw("employees.middlename as middlename"),
            DB::raw("employees.lastname as lastname"),
            DB::raw("genders.name as gender"),
            DB::raw("employers.name as employer"),
            DB::raw("notification_reports.incident_date as incident_date"),
            DB::raw("notification_reports.receipt_date as receipt_date"),
            DB::raw("notification_reports.reporting_date as reporting_date"),
            DB::raw("notification_reports.filename as filename"),
            DB::raw("accident_types.name as accident_location"),
            DB::raw("coalesce(regions.name, '') as region"),
            DB::raw("accidents.accident_time as accident_time"),
            DB::raw("accidents.accident_place as accident_place"),
            DB::raw("body_injury.name as body_injury"),
            DB::raw("nature_injury.name as nature_injury"),
            DB::raw("diseases.name as disease"),
            DB::raw("diseases.diagnosis_date as diagnosis_date"),
            DB::raw("disease_typ"),
            DB::raw("accident_cause_by_type.name as accident_cause_type"),
            DB::raw("accident_cause_by_agent.name as accident_cause"),
            DB::raw("disease_agent_type"),
            DB::raw("disease_organ_type"),
            DB::raw("disease_agent"),
            DB::raw("disease_organ"),
            DB::raw("benefit_amount as amount"),
            DB::raw("benefit_name as benefit_type"),
            DB::raw("mae"),
            DB::raw("lightduty"),
            DB::raw("perc_disability"),
            DB::raw("notification_reports.status, case when (status=0) then 'Pending'
                when (status=1) then 'Approved'
                when (status=2) then 'Rejected'
                else 'On progress' end as progressive_status"),
            DB::raw("reasn.rejection_comments as reasons"),
            DB::raw("notification_reports.incident_type_id,
            (case when(notification_reports.incident_type_id in (1,2)) 
            then 'non fatal' else 'fatal' end) injury_outcome"),
            ])
            ->join("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
            ->join("employers", "employers.id", "=", "notification_reports.employer_id")
            ->Join("employees", "employees.id", "=", "notification_reports.employee_id")
            ->leftJoin("genders", "genders.id", "=", "employees.gender_id")
             ->leftJoin("code_values as emplyee", "emplyee.id", "=", "employees.employee_category_cv_id")
            ->leftJoin("code_values as emplyer", "emplyer.id", "=", "employers.employer_category_cv_id")
            ->leftJoin("employer_sectors", "employer_sectors.employer_id", "=", "employers.id")
            ->leftJoin("code_values as nature_injury", "nature_injury.id", "=", "notification_reports.nature_of_incident_cv_id")
            ->leftJoin("code_values as body_injury", "body_injury.id", "=", "notification_reports.body_part_injury_cv_id")
            ->leftJoin("code_values as accident_cause_by_type", "accident_cause_by_type.id", "=", "notification_reports.accident_cause_type_cv_id")
            ->leftJoin("code_values as accident_cause_by_agent", "accident_cause_by_agent.id", "=", "notification_reports.accident_cause_agency_cv_id")
            ->leftJoin("incident_assessments", "incident_assessments.notification_eligible_benefit_id", "=", "notification_reports.id")
            ->leftJoin("occupations", "occupations.id", "=", "incident_assessments.occupation_id")
            ->leftJoin("districts", "districts.id", "=", "notification_reports.district_id")
            ->leftJoin("regions", "regions.id", "=", "districts.region_id")            
            ->leftJoin("diseases","diseases.notification_report_id", "=", "notification_reports.id");
            // disease type name
                $incidents->leftJoin(DB::raw('(SELECT di_type.name as disease_typ,di_type.id as di_id, inv_feedback.notification_report_id, inv_feedback.code_value_id, inv_feedback.investigation_question_id as qns32 from investigation_feedbacks as inv_feedback join code_values as di_type on di_type.id = inv_feedback.code_value_id where inv_feedback.investigation_question_id = 32 ) as feed'), function ($join) {
                    $join->on ( 'feed.notification_report_id', '=', 'notification_reports.id' );
                });
                //disease agent type
                $incidents->leftJoin(DB::raw('(SELECT di_type.name as disease_agent_type,di_type.id as di_id, inv_feedback.notification_report_id, inv_feedback.code_value_id, inv_feedback.investigation_question_id as qns33 from investigation_feedbacks as inv_feedback join code_values as di_type on di_type.id = inv_feedback.code_value_id where inv_feedback.investigation_question_id = 33 ) as agent_feed'), function ($join) {
                    $join->on ( 'agent_feed.notification_report_id', '=', 'notification_reports.id' );
                });
                //disease agent
                $incidents->leftJoin(DB::raw('(SELECT di_agent.name as disease_agent,di_agent.id as di_id, inv_feedback.notification_report_id, inv_feedback.code_value_id, inv_feedback.investigation_question_id as qns33 from investigation_feedbacks as inv_feedback join disease_exposure_agents as di_agent on di_agent.id = inv_feedback.disease_exposure_agent_id where inv_feedback.investigation_question_id = 33 ) as agent_child_feed'), function ($join) {
                    $join->on ( 'agent_child_feed.notification_report_id', '=', 'notification_reports.id' );
                });
                // disease organ type
                $incidents->leftJoin(DB::raw('(SELECT di_type.name as disease_organ_type,di_type.id as di_id, inv_feedback.notification_report_id, inv_feedback.code_value_id, inv_feedback.investigation_question_id as qns34 from investigation_feedbacks as inv_feedback join code_values as di_type on di_type.id = inv_feedback.code_value_id where inv_feedback.investigation_question_id = 34 ) as organ_feed'), function ($join) {
                    $join->on ( 'organ_feed.notification_report_id', '=', 'notification_reports.id' );
                });
                //disease organ
                $incidents->leftJoin(DB::raw('(SELECT di_organ.name as disease_organ,di_organ.id as di_id, inv_feedback.notification_report_id, inv_feedback.code_value_id, inv_feedback.investigation_question_id as qns34 from investigation_feedbacks as inv_feedback join disease_exposure_agents as di_organ on di_organ.id = inv_feedback.disease_exposure_agent_id where inv_feedback.investigation_question_id = 34 ) as organ_child_feed'), function ($join) {
                    $join->on ( 'organ_child_feed.notification_report_id', '=', 'notification_reports.id' );
                });
                // $incidents->leftJoin(DB::raw('(SELECT acc_cause_type.name as accident_cause_type, fback.notification_report_id,fback.code_id,acc_cause_type.id as acc_id,fback.investigation_question_id as qns31 from investigation_feedbacks as fback join codes as acc_cause_type on acc_cause_type.id = fback.code_id where fback.investigation_question_id = 31 ) as accident_cause_type_feed'), function ($join) {
                //     $join->on ( 'accident_cause_type_feed.notification_report_id', '=', 'notification_reports.id' );
                // });
                // $incidents->leftJoin(DB::raw('(SELECT acc_cause.name as accident_cause, fback.notification_report_id,fback.code_value_id,acc_cause.id as acc_id,fback.investigation_question_id as qns31 from investigation_feedbacks as fback join code_values as acc_cause on acc_cause.id = fback.code_value_id where fback.investigation_question_id = 31 ) as accident_cause_feed'), function ($join) {
                //     $join->on ( 'accident_cause_feed.notification_report_id', '=', 'notification_reports.id' );
                // });
                $incidents->leftJoin(DB::raw('(select k.claim_id,k.benefit_type_id,k.benefit_name,k.benefit_claim_name,k.benefit_type_claim_id,k.benefit_amount,k.notification_report_id,k.notification_eligible_benefit_id,
                case when (k.benefit_type_claim_id = 5)
                then a.rating else null end  as perc_disability from
                (select j.claim_id,j.benefit_type_id,j.benefit_name,j.benefit_type_claim_id,j.benefit_amount,j.benefit_claim_name,j.notification_report_id,ne.id as notification_eligible_benefit_id from
                (select g.claim_id,g.benefit_type_id,g.benefit_type_claim_id,g.benefit_name,g.benefit_amount, g.benefit_claim_name,c.notification_report_id from
                (select d.claim_id,d.benefit_type_id,d.benefit_type_claim_id,d.benefit_name,d.benefit_amount, bc.name as benefit_claim_name from
                (select b.claim_id,b.benefit_type_id,bt.benefit_type_claim_id,bt.name as benefit_name,b.benefit_amount from
                (select claim_id,benefit_type_id,
                case when (benefit_type_id=2) then sum(amount) else amount end as benefit_amount
                from main.claim_compensations group by claim_id,benefit_type_id,amount) b
                join main.benefit_types bt on bt.id=b.benefit_type_id) d
                left join main.benefit_type_claims bc on bc.id=d.benefit_type_claim_id) g
                join main.claims c on c.id=g.claim_id) j 
                left join main.notification_eligible_benefits ne on j.notification_report_id=ne.notification_report_id) k
                left join main.incident_assessments a on k.notification_eligible_benefit_id=a.notification_eligible_benefit_id) as claim_group'), function ($join) {
                $join->on ('claim_group.notification_report_id', '=', 'notification_reports.id' );
                });

                // $incidents->leftJoin(DB::raw('(select d.claim_id,d.benefit_type_id,d.benefit_name,d.benefit_amount,c.notification_report_id from
                // (select b.claim_id,b.benefit_type_id,bt.name as benefit_name,b.benefit_amount from
                // (select claim_id,benefit_type_id,
                // case when (benefit_type_id=2) then sum(amount) else amount end as benefit_amount
                // from main.claim_compensations group by claim_id,benefit_type_id,amount) b
                // join main.benefit_types bt on bt.id=b.benefit_type_id) d
                // join main.claims c on c.id=d.claim_id) as claim_group'), function ($join) {
                //         $join->on ('claim_group.notification_report_id', '=', 'notification_reports.id' );
                // });
                $incidents->leftJoin(DB::raw('(SELECT d.notification_eligible_benefit_id,d.mae,d.percentage,d.lightduty,n.notification_report_id from
                (SELECT notification_eligible_benefit_id, 
                SUM(CASE WHEN (disability_state_checklist_id in (1,2)) THEN days END) mae,
                ROUND(AVG(CASE WHEN (disability_state_checklist_id in (1,2)) THEN percent_of_ed_ld END),2) percentage,
                SUM(CASE WHEN (disability_state_checklist_id = 3) THEN days END) lightduty
                from main.notification_disability_states
                GROUP BY notification_eligible_benefit_id) d
                join main.notification_eligible_benefits as n
                on d.notification_eligible_benefit_id=n.id) as benefits'), function ($join) {
                        $join->on ('benefits.notification_report_id', '=', 'notification_reports.id' );
                });
                $incidents->leftJoin(DB::raw("(select n.id,n.status as st,w.comments,w.resource_id, 
                case when (n.status=2)
                then w.comments else null end as rejection_comments
                from (select * from (
                select
                resource_id,comments,
                row_number() over (partition by resource_id order by id asc) as row_number
                from main.wf_tracks where resource_type='App\Models\Operation\Claim\NotificationReport' group by resource_id,comments,receive_date,id
                ) wr where row_number = 1) w
                join main.notification_reports as n on n.id=w.resource_id) as reasn"),function($join){
                      $join->on ('reasn.resource_id', '=', 'notification_reports.id' );
                });

        if (isset($request['from_day']) && isset($request['from_month']) && isset($request['from_year']) && isset($request['to_day']) && isset($request['to_month']) && isset($request['to_year'])) {
            $from_date = $request['from_day'].'-'.$request['from_month'].'-'.$request['from_year'];
            $to_date = $request['to_day'].'-'.$request['to_month'].'-'.$request['to_year'];
            $carbon_from_date = Carbon::parse($from_date)->format('Y-m-d');
            $carbon_to_date = Carbon::parse($to_date)->format('Y-m-d');

            if ($carbon_from_date < $carbon_to_date) {
                $incidents->whereBetween("notification_reports.incident_date",array($carbon_from_date,$carbon_to_date));
            }

        }

        if (isset($request['benefit_type'])) {
            $benefit_type = (int) $request['benefit_type'];
            $incidents->where("claim_group.benefit_type_id", "=", $benefit_type);
        }
            
        if (isset($request['disease_organ'])) {
            $incidents->where("organ_child_feed.di_id", "=", $request['disease_organ']);
            $incidents->where("organ_child_feed.qns34",34);
        }

        if (isset($request['disease_organ_type'])) {
            $incidents->where("organ_feed.di_id", "=", $request['disease_organ_type']);
            $incidents->where("organ_feed.qns34",34);
        }
        if (isset($request['disease_agent'])) {
            $incidents->where("agent_child_feed.di_id", "=", $request['disease_agent']);
            $incidents->where("agent_child_feed.qns33",33);
        }
        if (isset($request['disease_agent_type'])) {
            $incidents->where("agent_feed.di_id", "=", $request['disease_agent_type']);
            $incidents->where("agent_feed.qns33",33);
        }
        if (isset($request['occupational_disease_type'])) {
            $incidents->where("feed.di_id", "=", $request['occupational_disease_type']);
            $incidents->where("feed.qns32",32);
        }
        if (isset($request['accident_cause_type'])) {
            $incidents->where("accident_cause_by_type.id", "=", $request['accident_cause_type']);

            // $incidents->where("accident_cause_type_feed.acc_id", "=", $request['accident_cause_type']);
            // $incidents->where("accident_cause_type_feed.qns31",31);
        }
        if (isset($request['accident_cause'])) {
            $incidents->where("accident_cause_by_agent.id", "=", $request['accident_cause']);
            
            // $incidents->where("accident_cause_feed.acc_id", "=", $request['accident_cause']);
            // $incidents->where("accident_cause_feed.qns31",31);
        }
        if (isset($request['status'])) {
            if ($request['status'] != -1) {
                $incidents->where("notification_reports.status", "=", $request['status']);
            }
            
        }
        if (isset($request['incident'])) {
            $incidents->where("notification_reports.incident_type_id", "=", $request['incident']);
        }
        if (isset($request['employee'])) {
            $incidents->where("notification_reports.employee_id", "=", $request['employee']);
        }
        if (isset($request['location_accident'])) {
            $incidents->leftJoin("accidents","accidents.notification_report_id", "=", "notification_reports.id");
            $incidents->join("accident_types","accidents.accident_type_id", "=", "accident_types.id");
            $incidents->where("accident_types.id", "=", $request['location_accident']);
        }else{
            $incidents->leftJoin("accidents","accidents.notification_report_id", "=", "notification_reports.id");
            $incidents->leftJoin("accident_types","accidents.accident_type_id", "=", "accident_types.id");
        }
        if (isset($request['employer'])) {
            $incidents->where("notification_reports.employer_id", "=", $request['employer']);
        }
        if (isset($request['region'])) {
            $incidents->where("regions.id", "=", $request['region']);
        }
        if (isset($request['district'])) {
            $incidents->where("districts.id", "=", $request['district']);
        }
        if (isset($request['gender'])) {
            if ($request['gender'] == '0') {
             $incidents->where("employees.gender_id", "=", null); 
            }else{
             $incidents->where("genders.id", "=", $request['gender']);
            }
            
        }


        return $incidents; 
        
        
    }

    
    public function employerIncidentsDatatable($id)
    {
           $incidents = $this->query()->select([
            DB::raw("notification_reports.id as case_no"),
            DB::raw("employees.id as employee_id"),
            DB::raw("incident_types.name as incident_type"),
            DB::raw("concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) as fullname"),
            DB::raw("employees.firstname as firstname"),
            DB::raw("employees.middlename as middlename"),
            DB::raw("employees.lastname as lastname"),
            DB::raw("genders.name as gender"),
            DB::raw("genders.name as gender"),
            DB::raw("bsns.name as bussines_sector"),
            DB::raw("emplyee.name as employment_category"),
            DB::raw("emplyer.name as employer_category"),
            DB::raw("employers.name as employer"),
            DB::raw("notification_reports.incident_date as incident_date"),
            DB::raw("notification_reports.receipt_date as receipt_date"),
            DB::raw("notification_reports.status as status"),
            DB::raw("notification_reports.filename as filename"),
            DB::raw("coalesce(regions.name, '') as region"),
            DB::raw("notification_staging_cv_id"),
            DB::raw("employers.employer_category_cv_id"),
            DB::raw("notification_staging_cv_id"),
            DB::raw("job_titles.name as occupation"),
            DB::raw("EXTRACT(year FROM age(incident_date,employees.dob)) :: int as age")
        ])
            ->join("incident_types", "incident_types.id", "=", "notification_reports.incident_type_id")
            ->join("employers", "employers.id", "=", "notification_reports.employer_id")
            ->Join("employees", "employees.id", "=", "notification_reports.employee_id")
            ->leftJoin("job_titles", "job_titles.id", "=", "employees.job_title_id")
            ->leftJoin("genders", "genders.id", "=", "employees.gender_id")
             ->leftJoin("code_values as emplyee", "emplyee.id", "=", "employees.employee_category_cv_id")
            ->leftJoin("code_values as emplyer", "emplyer.id", "=", "employers.employer_category_cv_id")
            ->leftJoin("employer_sectors", "employer_sectors.employer_id", "=", "employers.id")
            ->leftJoin("code_values as bsns", "bsns.id", "=", "employer_sectors.business_sector_cv_id")
            ->leftJoin("incident_assessments", "incident_assessments.notification_eligible_benefit_id", "=", "notification_reports.id")
            ->leftJoin("occupations", "occupations.id", "=", "incident_assessments.occupation_id")
            ->join("districts", "districts.id", "=", "notification_reports.district_id")
            ->join("regions", "regions.id", "=", "districts.region_id")
            ->distinct("employer_sectors.employer_id");
        if ($id) {
            $incidents->where("notification_reports.employer_id", $id);
        }

        return $incidents;
    }

}