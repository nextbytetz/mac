<?php

namespace App\Repositories\Backend\WorkPlaceRiskAssesment;
/*Juma*/
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

use App\Models\WorkPlaceRiskAssesment\OshAuditPeriod;
use App\Models\WorkPlaceRiskAssesment\OshAuditPeriodEmployer;
use App\Models\WorkPlaceRiskAssesment\OshAuditEmployerNotification;
use App\Models\WorkPlaceRiskAssesment\OshAuditQuestion;
use App\Models\WorkPlaceRiskAssesment\OshAuditFeedback;

use App\Events\NewWorkflow;
use Carbon\Carbon;
use App\Services\Workflow\Workflow;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\DataTables\WorkflowTrackDataTable;
use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;

class OshAuditPeriodRepository extends BaseRepository
{

	const MODEL = OshAuditPeriod::class;

	public function __construct()
	{
		parent::__construct();
		$this->wf_module_group_id = 28;

	}

	public function getAll()
	{
		return $this->query()->get();
	}


	public function findOrThrowException($id)
	{
		$osh_audit = $this->query()->find($id);

		if (!is_null($osh_audit)) {
			return $osh_audit;
		}
		throw new GeneralException('Period not existing');
	}



	public function initiateWf(array $input)
	{

		$return = DB::transaction(function () use ($input) {

			$osh_audit_period = OshAuditPeriod::firstOrNew(['id'=>$input['osh_audit_period_id']]);
			$osh_audit_period->updated_at = Carbon::now();
			$osh_audit_period->user_id = access()->id();
			$osh_audit_period->fatal = $input['fatal'];
			$osh_audit_period->lost_time_injury = $input['lti'];
			$osh_audit_period->number_of_employers = $this->returnPeriodEmployersCount($input['osh_audit_period_id'],$input['fatal'], $input['lti'], $input['non_conveyance']);
			$osh_audit_period->non_conveyance = $input['non_conveyance'];
			$osh_audit_period->status = 1;
			$osh_audit_period->save();

			//save employers and notifications
			$save_employers = $this->addOrUpdateProfileEmployers($osh_audit_period, $input['fatal'], $input['lti'], $input['non_conveyance']);

			$check = workflow([['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $input['osh_audit_period_id']]])->checkIfHasWorkflow();
			if (!$check) {
				event(new NewWorkflow(['wf_module_group_id' => $this->wf_module_group_id, 'resource_id' => $input['osh_audit_period_id']]));
			}
			
		});

		return $return;
	}

	public function approveOshAuditList($osh_audit_period_id, $level) {
		// dd($level);
		if ((int)$level == 3) {
			$osh_audit_period = $this->query()->where('id', $osh_audit_period_id)->update([
				'wf_done' => 1,
				'wf_done_date' => Carbon::now(),
				'status' => 2
			]);
		}
	}


	public function rejectOshAuditList($osh_audit_period_id) {
		$osh_audit_period = $this->query()->where('id', $osh_audit_period_id)->update([
			'status' => 3
		]);
	}



	public function getStatusLabelOshAuditPeriod($status, $period, $fin_year)
	{
		$label = '';
		
		$fn_year_end = substr($fin_year, -4,4);
		$fn_year_start = substr($fin_year, 0,4);
		$year = Carbon::parse()->format('Y');

		$now_month = Carbon::parse()->format('m');
		if (($year == $fn_year_start) || ($year == $fn_year_end)) {

			if (($period == 1) && (7 < $now_month) && ($now_month < 12) ) {
				$label = $this->labelByStatus($status, 'active');
			} elseif (($period == 2) && (7 > $now_month) && ($now_month > 1)) {
				$label = $this->labelByStatus($status, 'active');
			}else{
				$label = $this->labelByStatus($status, 'inactive');
			}		
		}else{
			$label = $this->labelByStatus($status, 'inactive');
		}
		
		return $label;		
	}


	public function getWfStatusLabelOshAuditPeriod($status, $period, $fin_year)
	{
		$label = '';
		if ($this->isPeriodActive($period, $fin_year)) {
			$label = $this->labelByStatus($status, 'active');	
		}else{
			$label = $this->labelByStatus($status, 'inactive');
		}
		
		return $label;		
	}


	protected function labelByStatus($status, $type)
	{
		switch ($status) {
			case 1:
			$label = '<div style="width:15%;"><span class="btn btn-sm btn-info  btn-block">Initiated</span></div>';
			break;
			case 2:
			$label = '<div  style="width:15%;"><span class="btn btn-sm btn-success  btn-block">Approved</span></div>';
			break;
			case 3:
			$label = '<div  style="width:15%;"><span class="btn btn-sm btn-danger  btn-block">Rejected</span></div>';
			break;
			default:
			if ($type == 'active') {
				$label = '<div  style="width:15%;"><span class="btn btn-sm btn-warning  btn-block">Pending</span></div>';
			} else {
				// $label = '<div  style="width:15%;"><span class="btn btn-sm btn-danger  btn-block">Not Due</span></div>';
				$label = '';
			}
			
			break;
		}

		return $label;
	}


	public function isPeriodActive($period, $fin_year)
	{
		$active = false;
		$osh_audit_period = OshAuditPeriod::where('fin_year',$fin_year)->where('period',$period)->first();
		if ($osh_audit_period->is_active) {
			$active = true;
		}
		return $active;		
	}

	public function isPeriodActiveOld($period, $fin_year)
	{
		$active = false;
		
		$fn_year_end = substr($fin_year, -4,4);
		$fn_year_start = substr($fin_year, 0,4);
		$year = Carbon::parse()->format('Y');

		$now_month = Carbon::parse()->format('m');

		if (($year == $fn_year_start)) {
			if (($period == 1) && (7 < $now_month) && ($now_month < 12) ) {
				$active = true;
			} 
		}elseif (($year == $fn_year_end)) {
			if (($period == 2) && (1 < $now_month) && ($now_month < 7)) {
				$active = true;
			}	
		}
		return $active;		
	}

	public function canWfInitiated($period, $fin_year, $status)
	{
		$return = false;
		$is_period_active = $this->isPeriodActive($period, $fin_year);
		if (empty($status) && $is_period_active) {
			$return = true;
		}
		return $return;
		
	}



	public function updateWf($input)
	{
		
	}


	public function addOrUpdateProfileEmployers($osh_audit_period, $request_fatal, $request_lti, $request_non_conveyance)
	{
		
		DB::transaction(function () use ($osh_audit_period,$request_fatal, $request_lti, $request_non_conveyance) {
			$osh_audit_employers = DB::table('osh_audit_checklist_employers_summary')->where('fin_year',$osh_audit_period->fin_year)->get();
			
			foreach ($osh_audit_employers as $audit_employer) {

				$employer_id = (int)substr($audit_employer->regno, -6);

				$osh_audit_notifications = DB::table('osh_audit_checklist_employers')->where('employer_id',$employer_id)->where('fin_year',$osh_audit_period->fin_year)->get();

				$employees ['fatal'] = [];
				$employees ['lost_days'] = [];
				$employees ['exclude_conveyance_accident'] = [];
				$lost_days = 0;
				foreach ($osh_audit_notifications as $notification) {
					$month_in_period = $this->isMonthInPeriod($osh_audit_period, $notification->reporting_date);

					if($month_in_period){
						$notification_report = DB::table('main.notification_reports')->where('filename',$notification->filename)->first();

						if(count($notification_report)){

							if ($notification->conveyance_death == 224) {
								array_push($employees['fatal'], $notification_report->id);
							}

							if (!is_null($notification->lost_days)) {
								
								$employees ['lost_days'][$notification_report->id] = $notification->lost_days;
								$lost_days += $notification->lost_days;
							}

							if ($notification->conveyance_accident != 2) {
								array_push($employees ['exclude_conveyance_accident'], $notification_report->id);
							}

						}
					}
				}
				
				$fatal = count($employees['fatal']);
				$exclude_conveyance_accident = count($employees['exclude_conveyance_accident']);
				$lti = !empty($request_lti) ?  (int)$request_lti : 0;
				$non_conveyance = !empty($request_non_conveyance) ? (int)$request_non_conveyance : 0;

				if ( ($fatal > $request_fatal) || $lost_days > $lti || $exclude_conveyance_accident > $non_conveyance) {
					if ($fatal >  $request_fatal) {
						foreach ($employees['fatal'] as $fatal) {
							$data['id'] = $fatal;
							$this->saveProfileAndNotification($employer_id, $osh_audit_period, $data, 'fatal');
						}
					}

					if ($lost_days >  $lti) {
						foreach ($employees['lost_days'] as $key => $value) {
							$data['id'] = $key;
							$data['lost_days'] = $value;
							$this->saveProfileAndNotification($employer_id, $osh_audit_period, $data, 'lost_days');
						}
					}

					if ($exclude_conveyance_accident > $non_conveyance) {
						foreach ($employees ['exclude_conveyance_accident'] as $emp_non) {
							$data['id'] = $emp_non;
							$this->saveProfileAndNotification($employer_id, $osh_audit_period, $data, 'non_conveyance');
						}
					}
				}

			}
		});
	}

	public function isMonthInPeriod($osh_audit_period, $compare_date)
	{
		$now_month = Carbon::parse($compare_date)->format('m');
		$now_year = Carbon::parse($compare_date)->format('Y');

		$fn_year_end = substr($osh_audit_period->fin_year, -4,4);
		$fn_year_start = substr($osh_audit_period->fin_year, 0,4);
		$active = false;

		if (($now_year == $fn_year_start)) {
			if (($osh_audit_period->period == 1) && (7 < $now_month) && ($now_month < 12) ) {
				$active = true;
			} 
		}elseif (($now_year == $fn_year_end)) {
			if (($osh_audit_period->period == 2) && (1 < $now_month) && ($now_month < 7)) {
				$active = true;
			}	
		}
		return $active;
	}


	public function isMonthInQuater($osh_audit_period, $compare_date, $quater=null)
	{
		$now_month = Carbon::parse($compare_date)->format('m');
		$now_year = Carbon::parse($compare_date)->format('Y');

		$fn_year_end = substr($osh_audit_period->fin_year, -4,4);
		$fn_year_start = substr($osh_audit_period->fin_year, 0,4);
		$active = false;

		if (($now_year == $fn_year_start)) {
			if (is_null($quater)) {
				if (($osh_audit_period->period == 1) && ((7 <= $now_month) && ($now_month <= 12))) {
					$active = true;
				}
			} else {
				if (($osh_audit_period->period == 1) && ((7 <= $now_month) && ($now_month <= 9)) && $quater == 1) {
					$active = true;
				}
				if(($osh_audit_period->period == 1) && ((10 <= $now_month) && ($now_month <= 12)) && $quater == 2){
					$active = true;
				}
			}
			
		}elseif (($now_year == $fn_year_end)) {
			if (is_null($quater)) {
				if (($osh_audit_period->period == 2) && ((1 <= $now_month) && ($now_month <= 6))) {
					$active = true;
				}
			} else {
				if (($osh_audit_period->period == 2) && ((1 <= $now_month) && ($now_month <= 3)) && $quater==3) {
					$active = true;
				}
				if (($osh_audit_period->period == 2) && ((4 <= $now_month) && ($now_month <= 6)) && $quater==4) {
					$active = true;
				}	
			}

		}
		return $active;
	}


	public function addEmployerInFeedback($employer_id, $osh_audit_period_id)
	{
		$osh_audit_questions = OshAuditQuestion::get();
		foreach ($osh_audit_questions as $osh_audit_question) {
			$osh_audit_feedback = OshAuditFeedback::firstOrNew([
				'osh_audit_period_employer_id'=>$osh_audit_period_id,
				'osh_audit_question_id'=>$osh_audit_question->id
			]);
			$osh_audit_feedback->save();
		}		

	}


	public function saveProfileAndNotification($employer_id, $osh_audit_period, $data, $type)
	{
		$osh_audit_period_employer =  OshAuditPeriodEmployer::firstOrCreate([
			'osh_audit_period_id'=>$osh_audit_period->id,
			'employer_id'=>$employer_id
		]);

		$notification_for_quater = DB::table('main.notification_reports')->find($data['id']);

		switch ($type) {
			case 'fatal':
			$number_of_fatal = is_null($osh_audit_period_employer->number_of_fatal) ? 1 : $osh_audit_period_employer->number_of_fatal+1;
			$osh_audit_period_employer->number_of_fatal =  $number_of_fatal;

			$osh_audit_notification =  OshAuditEmployerNotification::firstOrCreate([
				'osh_audit_employer_id'=>$osh_audit_period_employer->id,
				'notification_report_id'=>$data['id'],
				'quater' => $this->returnQuaterId($notification_for_quater->reporting_date),
			]);
			$osh_audit_notification->is_fatal = true;
			$osh_audit_notification->save();
			break;
			case 'lost_days':
			$number_of_days_lost = is_null($osh_audit_period_employer->number_of_days_lost) ? $data['lost_days'] : $osh_audit_period_employer->number_of_days_lost+$data['lost_days'];
			$osh_audit_period_employer->number_of_days_lost =  $number_of_days_lost;

			$osh_audit_notification =  OshAuditEmployerNotification::firstOrCreate([
				'osh_audit_employer_id'=>$osh_audit_period_employer->id,
				'notification_report_id'=>$data['id'],
				'quater' => $this->returnQuaterId($notification_for_quater->reporting_date),

			]);
			$osh_audit_notification->lost_days = $data['lost_days'];
			$osh_audit_notification->is_lost_days = true;
			$osh_audit_notification->save();
			break;
			case 'non_conveyance':
			$non_conveyance_claims = is_null($osh_audit_period_employer->not_conveyance_accident) ? 1 : $osh_audit_period_employer->not_conveyance_accident+1;
			$osh_audit_period_employer->not_conveyance_accident =  $non_conveyance_claims;

			$osh_audit_notification =  OshAuditEmployerNotification::firstOrCreate([
				'osh_audit_employer_id'=>$osh_audit_period_employer->id,
				'notification_report_id'=>$data['id'],
				'quater' => $this->returnQuaterId($notification_for_quater->reporting_date),

			]);
			$osh_audit_notification->not_conveyance_accident = true;
			$osh_audit_notification->save();
			break;
			
			default:
			break;
		}

		$osh_audit_period_employer->save();
		$this->addEmployerInFeedback($employer_id,$osh_audit_period_employer->id);

	}


	public function returnFinYearsInitiated()
	{
		return $this->query()->select('fin_year')->where('status','>',0)->distinct('fin_year')->get();

	}

	public function getWfModuleType()
	{
		$type = 1;
		return $type;
	}


	
	public function activatePeriod($osh_audit_period)
	{
		$this->query()->update(['is_active'=> false]);
		$this->query()->where('id',$osh_audit_period)->update(['is_active'=> true]);
	}



	public function returnPeriodEmployersCount($osh_audit_period_id, $request_fatal,  $request_lti, $request_non_conveyance)
	{
		return count($this->returnPeriodEmployersIds($osh_audit_period_id, $request_fatal, $request_lti, $request_non_conveyance));
	}


	public function returnPeriodEmployersIds($osh_audit_period_id, $request_fatal, $request_lti, $request_non_conveyance)
	{
		$osh_audit_period = $this->query()->where('id',$osh_audit_period_id)->first();

		$ids = [];

		$osh_audit_employers = DB::table('osh_audit_checklist_employers_summary')->where('fin_year',$osh_audit_period->fin_year)->get();

		foreach ($osh_audit_employers as $audit_employer) {


			$employer_id = (int)substr($audit_employer->regno, -6);

			$osh_audit_notifications = DB::table('osh_audit_checklist_employers')->where('employer_id',$employer_id)->where('fin_year',$osh_audit_period->fin_year)->get();

			$employees ['fatal'] = [];
			$employees ['lost_days'] = [];
			$employees ['exclude_conveyance_accident'] = [];
			$lost_days = 0;
			foreach ($osh_audit_notifications as $notification) {
				$month_in_period = $this->isMonthInQuater($osh_audit_period, $notification->reporting_date);
				// $month_in_period = $this->isMonthInPeriod($osh_audit_period, $notification->reporting_date);

				if($month_in_period){
					$notification_report = DB::table('main.notification_reports')->where('filename',$notification->filename)->first();

					if(count($notification_report)){

						if ($notification->conveyance_death == 224) {
							array_push($employees['fatal'], $notification_report->id);
						}

						if (!is_null($notification->lost_days)) {

							$employees ['lost_days'][$notification_report->id] = $notification->lost_days;
							$lost_days += $notification->lost_days;
						}

						if ($notification->conveyance_accident != 2) {
							array_push($employees ['exclude_conveyance_accident'], $notification_report->id);
						}

					}
				}
			}
			$fatal = count($employees['fatal']);
			$exclude_conveyance_accident = count($employees['exclude_conveyance_accident']);

			$lti = !empty($request_lti) ?  (int)$request_lti : 0;
			$non_conveyance = !empty($request_non_conveyance) ? (int)$request_non_conveyance : 0;

			if (($fatal > $request_fatal) || $lost_days > $lti  || $exclude_conveyance_accident > $non_conveyance) {
				array_push($ids,  $employer_id);
			}
		}
		return $ids;
	}



	public function returnPeriodIds($fin_year_id, $request_fatal, $request_lti, $request_non_conveyance)
	{
		$osh_audit_periods = $this->query()->where('fin_year_id', $fin_year_id)->get();

		$ids = [];

		foreach ($osh_audit_periods as $osh_audit_period) {
			$osh_audit_employers = DB::table('osh_audit_checklist_employers_summary')->where('fin_year',$osh_audit_period->fin_year)->get();

			foreach ($osh_audit_employers as $audit_employer) {

				$employer_id = (int)substr($audit_employer->regno, -6);

				$osh_audit_notifications = DB::table('osh_audit_checklist_employers')->where('employer_id',$employer_id)->where('fin_year',$osh_audit_period->fin_year)->get();

				$employees ['fatal'] = [];
				$employees ['lost_days'] = [];
				$employees ['exclude_conveyance_accident'] = [];
				$lost_days = 0;
				foreach ($osh_audit_notifications as $notification) {
					$month_in_period = $this->isMonthInPeriod($osh_audit_period, $notification->reporting_date);

					if($month_in_period){
						$notification_report = DB::table('main.notification_reports')->where('filename',$notification->filename)->first();

						if(count($notification_report)){

							if ($notification->conveyance_death == 224) {
								array_push($employees['fatal'], $notification_report->id);
							}

							if (!is_null($notification->lost_days)) {

								$employees ['lost_days'][$notification_report->id] = $notification->lost_days;
								$lost_days += $notification->lost_days;
							}

							if ($notification->conveyance_accident != 2) {
								array_push($employees ['exclude_conveyance_accident'], $notification_report->id);
							}

						}
					}
				}
				$fatal = count($employees['fatal']);
				$exclude_conveyance_accident = count($employees['exclude_conveyance_accident']);

				$lti = !empty($request_lti) ?  (int)$request_lti : 0;
				$non_conveyance = !empty($request_non_conveyance) ? (int)$request_non_conveyance : 0;

				if (($fatal > $request_fatal) || $lost_days > $lti  || $exclude_conveyance_accident > $non_conveyance) {
					array_push($ids, $osh_audit_period->id);
				}

			}	

		}

		return count($ids) ? array_unique($ids) : $ids;
	}

	public function returnQuaters($period)
	{
		if ($period == 1) {
			$quaters = [1,2];
		}else{
			$quaters = [3,4];
		}

		return $quaters;
	}


	//quater things
	public function returnQuaterAuditListForDatatable($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater)
	{

		$osh_audit_period = $this->query()->where('id',$osh_audit_period_id)->first();
		$ids = $this->returnQuaterEmployersIds($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater);
		return DB::table('osh_audit_checklist_employers_summary')->select('osh_audit_checklist_employers_summary.*','employers.id as employer_id','osh_audit_period.id as osh_audit_period_id')
		->join('main.employers', 'employers.reg_no', '=', 'osh_audit_checklist_employers_summary.regno')
		->join('main.osh_audit_period', 'osh_audit_checklist_employers_summary.fin_year', '=', 'osh_audit_period.fin_year')
		->whereIn('regno',$ids)->where('osh_audit_checklist_employers_summary.fin_year',$osh_audit_period->fin_year)->where('osh_audit_period.id',$osh_audit_period_id);
	}


	public function returnQuaterEmployersIds($osh_audit_period_id, $request_fatal, $request_lti, $request_non_conveyance, $quater)
	{
		$osh_audit_period = $this->query()->where('id',$osh_audit_period_id)->first();

		$ids = [];

		$osh_audit_employers = DB::table('osh_audit_checklist_employers_summary')->where('fin_year',$osh_audit_period->fin_year)->get();

		foreach ($osh_audit_employers as $audit_employer) {


			$employer_id = (int)substr($audit_employer->regno, -6);

			$osh_audit_notifications = DB::table('osh_audit_checklist_employers')->where('employer_id',$employer_id)->where('fin_year',$osh_audit_period->fin_year)->get();

			$employees ['fatal'] = [];
			$employees ['lost_days'] = [];
			$employees ['exclude_conveyance_accident'] = [];
			$lost_days = 0;
			foreach ($osh_audit_notifications as $notification) {
				$month_in_period = $this->isMonthInQuater($osh_audit_period, $notification->reporting_date,$quater);
				// $month_in_period = $this->isMonthInPeriod($osh_audit_period, $notification->reporting_date);

				if($month_in_period){
					$notification_report = DB::table('main.notification_reports')->where('filename',$notification->filename)->first();

					if(count($notification_report)){

						if ($notification->conveyance_death == 224) {
							array_push($employees['fatal'], $notification_report->id);
						}

						if (!is_null($notification->lost_days)) {

							$employees ['lost_days'][$notification_report->id] = $notification->lost_days;
							$lost_days += $notification->lost_days;
						}

						if ($notification->conveyance_accident != 2) {
							array_push($employees ['exclude_conveyance_accident'], $notification_report->id);
						}

					}
				}
			}
			$fatal = count($employees['fatal']);
			$exclude_conveyance_accident = count($employees['exclude_conveyance_accident']);

			$lti = !empty($request_lti) ?  (int)$request_lti : 0;
			$non_conveyance = !empty($request_non_conveyance) ? (int)$request_non_conveyance : 0;

			if (($fatal > $request_fatal) || $lost_days > $lti  || $exclude_conveyance_accident > $non_conveyance) {
				array_push($ids,  $audit_employer->regno);
			}
		}
		return $ids;
	}



	public function returnQuaterNotificationIds($osh_audit_period_id, $request_fatal, $request_lti, $request_non_conveyance, $quater, $employer_reg_no)
	{
		$osh_audit_period = $this->query()->where('id',$osh_audit_period_id)->first();

		$ids = [];

		$audit_employer = DB::table('osh_audit_checklist_employers_summary')->where('fin_year',$osh_audit_period->fin_year)->where('regno',$employer_reg_no)->first();

		$employer_id = (int)substr($audit_employer->regno, -6);

		$osh_audit_notifications = DB::table('osh_audit_checklist_employers')->where('employer_id',$employer_id)->where('fin_year',$osh_audit_period->fin_year)->get();

		$employees ['fatal'] = [];
		$employees ['lost_days'] = [];
		$employees ['exclude_conveyance_accident'] = [];
		$lost_days = 0;
		foreach ($osh_audit_notifications as $notification) {
			$month_in_period = $this->isMonthInQuater($osh_audit_period, $notification->reporting_date, $quater);

			if($month_in_period){
				$notification_report = DB::table('main.notification_reports')->where('filename',$notification->filename)->first();

				if(count($notification_report)){

					if ($notification->conveyance_death == 224) {
						array_push($employees['fatal'], $notification_report->id);
					}

					if (!is_null($notification->lost_days)) {

						// $employees ['lost_days'][$notification_report->id] = $notification->lost_days;
						// $lost_days += $notification->lost_days;
						array_push($employees['lost_days'], $notification_report->id);
					}

					if ($notification->conveyance_accident != 2) {
						array_push($employees ['exclude_conveyance_accident'], $notification_report->id);
					}

				}
			}
		}
		$fatal = count($employees['fatal']);
		$exclude_conveyance_accident = count($employees['exclude_conveyance_accident']);
		$lost_days = count($employees['lost_days']);

		$ids['fatal'] = $fatal;
		$ids['lti'] = $lost_days;
		$ids['non_conveyance'] = $exclude_conveyance_accident;
		
		return $ids;
	}


	public function returnQuaterId($date)
	{
		$now_month = Carbon::parse($date)->format('m');
		
		if ((7 <= $now_month) && ($now_month <= 9)) {
			$quater = 1;
		} elseif((10 <= $now_month) && ($now_month <= 12)) {
			$quater = 2;
		}elseif((1 <= $now_month) && ($now_month <= 3)) {
			$quater = 3;
		}else{
			$quater = 4;
		}
		
		return $quater;

	}


	public function returnSubmittedQuaterAuditListForDatatable($osh_audit_period_id, $quater)
	{
		$ids = $this->returnSubmittedEmployerIdsByQuater($osh_audit_period_id, $quater);

		return DB::table('osh_audit_period_employers')->select('osh_audit_period_employers.*','employers.name as employer','employers.reg_no as regno')
		->join('main.employers', 'employers.id', '=', 'osh_audit_period_employers.employer_id')
		->whereIn('osh_audit_period_employers.id',$ids);

	}


	public function returnSubmittedEmployerIdsByQuater($osh_audit_period_id, $quater)
	{
		$data = DB::table('osh_audit_period_employers')->select('osh_audit_period_employers.id')
		->join('main.employers', 'employers.id', '=', 'osh_audit_period_employers.employer_id')
		->join('main.osh_audit_employer_notifications', 'osh_audit_employer_notifications.osh_audit_employer_id', '=', 'osh_audit_period_employers.id')
		->where('osh_audit_period_employers.osh_audit_period_id',$osh_audit_period_id)
		->where('quater',$quater)->get();

		$ids = [];

		foreach ($data as $v) {
			array_push($ids, $v->id);
		}
		return count($ids) ? array_unique($ids) : $ids;
	}


	public function returnSubmittedQuaterNotificationIds($osh_audit_employer_id, $quater)
	{
		
		$ids = [];

		$osh_audit_notifications = DB::table('osh_audit_employer_notifications')
		->where('osh_audit_employer_id',$osh_audit_employer_id)->where('quater',$quater)->get();

		$employees ['fatal'] = [];
		$employees ['lost_days'] = [];
		$employees ['exclude_conveyance_accident'] = [];
		
		foreach ($osh_audit_notifications as $notification) {
			if(count($notification)){
				if ($notification->is_fatal) {
					array_push($employees['fatal'], $notification->id);
				}
				if ($notification->is_lost_days) {
					array_push($employees['lost_days'], $notification->id);
				}
				if ($notification->not_conveyance_accident) {
					array_push($employees['exclude_conveyance_accident'], $notification->id);
				}
			}
		}

		$ids['fatal'] = $employees['fatal'];
		$ids['lti'] = $employees['lost_days'];
		$ids['non_conveyance'] = $employees['exclude_conveyance_accident'];
		return $ids;

	}


	public function checkIfAssignedAll($osh_audit_period_id, $level)
	{
		
		if ((int)$level == 2) {
			$un_assigned = OshAuditPeriodEmployer::where('osh_audit_period_id',$osh_audit_period_id)->whereNull('user_id')->first();
			$return = count($un_assigned) > 0 ? false : true;	
		} else {
			$return = true;
		}

		if(!$return){
			throw new WorkflowException("Request Failed! Kindly assign staff to all employers in this period first");
		}

		return $return;
		
	}



}
