<?php

namespace App\Repositories\Backend\Location;

use App\Exceptions\GeneralException;
use App\Models\Location\LocationType;
use App\Repositories\BaseRepository;

class LocationTypeRepository extends  BaseRepository
{

    const MODEL = LocationType::class;

    public function __construct()
    {

    }

//find or throwException for job title
    public function findOrThrowException($id)
    {
        $location_type= $this->query()->find($id);

        if (!is_null($location_type)) {
            return $location_type;
        }
        throw new GeneralException(trans('exceptions.backend.sysdef.location_type_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}