<?php

namespace App\Repositories\Backend\Location;

use App\Exceptions\GeneralException;
use App\Models\Location\Country;
use App\Repositories\BaseRepository;

class CountryRepository extends  BaseRepository
{

    const MODEL = Country::class;

    public function __construct()
    {

    }

//find or throwException for job title
    public function findOrThrowException($id)
    {
        $country= $this->query()->find($id);

        if (!is_null($country)) {
            return $country;
        }
        throw new GeneralException(trans('exceptions.backend.sysdef.country_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}