<?php

namespace App\Repositories\Backend\Location;

use App\Exceptions\GeneralException;
use App\Models\Location\District;
use App\Repositories\BaseRepository;

class DistrictRepository extends  BaseRepository
{

    const MODEL = District::class;

    /**
     * DistrictRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $district = $this->query()->find($id);

        if (!is_null($district)) {
            return $district;
        }
        throw new GeneralException(trans('exceptions.backend.sysdef.district_not_found'));
    }

    /**
     * @param $input
     */
    public function create($input) {

    }

    /**
     * @param $id
     * @param $input
     */
    public function update($id, $input) {

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAllByRegionPluck($id)
    {
        return $this->query()->where("region_id", $id)->pluck("name", "id")->all();
    }

    public function getHqDistrictArray()
    {
        $region = sysdefs()->data()->hq_region;
        $district = $this->query()->select(["id"])->where("region_id", $region)->pluck("id")->all();
        return $district;
    }

}