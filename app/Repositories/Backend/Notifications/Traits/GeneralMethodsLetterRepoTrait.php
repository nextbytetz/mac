<?php

namespace App\Repositories\Backend\Notifications\Traits;

use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

trait GeneralMethodsLetterRepoTrait {



    /**
     * @param $letter_cv_reference
     * Get Previous letter
     */
    public function getPreviousLetter($letter_cv_reference)
    {
        $cv_repo = new CodeValueRepository();
        $cv_id = $cv_repo->findIdByReference($letter_cv_reference);
        $prev_letter = (new LetterRepository())->queryLettersByReference($cv_id)->orderBy('letters.id', 'desc')->first();
        return $prev_letter;
    }



    /*Get Letter reference No. attribute*/
    public function getLetterReferenceNo($letter_cv_reference, $default)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference);
        $prev_reference = (isset($prev_letter)) ? $prev_letter->reference : null;
        $next_folio_no = $this->getLetterFolioNo($letter_cv_reference);
        $reference_no = '';
        $reference_no = $this->getFullReferenceNo($prev_reference, $default, $next_folio_no)  ;

        return $reference_no;
    }


    /*Prefix of letter reference no*/
    public function getFullReferenceNo($prev_reference, $default_ref, $next_folio_no)
    {
        if(isset($prev_reference)){
            $ref_no =  remove_after_last_this_character('/',$prev_reference) . '/';
        }else{
            $ref_no = $default_ref;
        }
        return $ref_no  . $next_folio_no;
    }

    /*Letter folio no*/
    public function getLetterFolioNo($letter_cv_reference)
    {
        $prev_letter = $this->getPreviousLetter($letter_cv_reference);
        if(isset($prev_letter)){
            $resources =  json_decode($prev_letter->resources, true);
            $next_folio_no =  $resources[0]['value'] + 1;
        }else{
            $next_folio_no = 1;
        }
        return $next_folio_no;
    }

}
