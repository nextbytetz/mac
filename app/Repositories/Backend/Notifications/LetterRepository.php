<?php

namespace App\Repositories\Backend\Notifications;

use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Http\Controllers\Backend\System\LetterController;
use App\Models\Notifications\Letter;
use App\Models\Operation\Compliance\Inspection\EmployerInspectionTask;
use App\Models\Operation\Compliance\Inspection\InspectionTaskPayroll;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\InterestAdjustmentRepository;
use App\Repositories\Backend\Notifications\Traits\GeneralMethodsLetterRepoTrait;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Operation\Claim\NotificationLetterRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\Online\MergeDeMergeEmployerPayrollRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionTaskPayrollRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureExtensionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionModificationRepository;

use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\OfficeRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\BaseRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDF;
use Storage;
use PDFSnappy;

/**
 * Class LetterRepository
 * @package App\Repositories\Backend\Notifications
 * @description Letter metadata
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 *
 * folio ==> letter number
 * profile_url ==> url to the dashboard of the referred page
 * name ==> file name of the referred letter
 * resource ==> all other associated data with referred letter e.g {'employee' : '', 'employer' : '', 'benefit' : ''}
 *
 */
class LetterRepository extends BaseRepository
{

    const MODEL = Letter::class;

    use GeneralMethodsLetterRepoTrait;

    /**
     * @param $resource
     * @param $reference
     * @return mixed
     */
    public function checkIfExists($resource, $reference)
    {
        $query = $this->query()->where("resource_id", $resource)->whereHas("type", function ($query) use ($reference) {
            $query->where("reference", $reference);
        });
        return $query;
    }

    /**
     * @param $resource
     * @param $reference
     * @return mixed
     */
    public function getPrevLetters($resource, $reference)
    {
        return $this->query()->where("resource_id", $resource)->whereHas("type", function ($query) use ($reference) {
            $query->where("reference", $reference);
        })->get();
    }

    /**
     * @param $unit_id
     * @return mixed
     */
    public function getTotalLettersByUnit($unit_id, $resource_id)
    {
        return $this->query()->where("unit_id", $unit_id)->where("resource_id", $resource_id)->count();
    }

    public function getLetterByReference($cv_id, $resource_id)
    {
        return $this->query()->where(["cv_id" => $cv_id, "resource_id" => $resource_id])->first();
    }

    public function getLetterByArrReference(array $cvids, $resource_id)
    {
        return $this->query()->where(["resource_id" => $resource_id])->whereIn("cv_id", $cvids)->orderByDesc("id")->first();
    }

    /*Query to get letters by code value id of letter reference*/
    public function queryLettersByReference($cv_id)
    {
        return $this->query()->where(["cv_id" => $cv_id]);
    }
    /**
     * @param Model $letter
     * @return mixed|null
     */
    public function retrieveLetterAllocatedUser(Model $letter)
    {
        $user = NULL;
        if (!$letter->allocated) {
            /*$user = (new UserRepository())->getAllocatedForInspection($employerTask);
            $employerTask->allocated = $user;
            $employerTask->save();*/
        } else {
            $user = $letter->allocated;
        }
        return $user;
    }

    /**
     * @param Model $letter
     * @return array|mixed
     */
    public function retrievePreviousStage(Model $letter)
    {
        $return = [];
        $query = $letter->stages()->orderBy("letter_stages.id", "desc");
        if ($query->count()) {
            $return = json_decode($query->first()->pivot->comments);
        }
        return $return;
    }

    /**
     * @param Model $letter
     */
    public function updateLetterStageId(Model $letter)
    {
        $letter_stage_id = $letter->stages()->orderByDesc("letter_stages.id")->limit(1)->first()->pivot->id;
        $letter->letter_stage_id = $letter_stage_id;
        $letter->save();
    }

    /**
     * @param Model $letter
     * @param int $caller
     * @return bool
     */
    public function initialize(Model $letter, $caller = 0)
    {
        $letter->refresh();
        $stage = $letter->staging_cv_id;
        $codeValue = new CodeValueRepository();
        $checkerRepo = new CheckerRepository();
        $userRepo = new UserRepository();

        //--> Allocate Letter User
        $user = $this->retrieveLetterAllocatedUser($letter);
        $prevStage = $this->retrievePreviousStage($letter);
        $prevComment = (($prevStage) ? $prevStage->{'currentStage'} : "Letter Created");
        $comments = NULL;
        $process = 1;
        $closeChecker = 0;
        $priority = 0;

        switch ($stage) {
            case $codeValue->OLPLTTRCTRTD():
                //Letter Created
            $closeChecker= 1;
            $priority = $codeValue->CHKPHIGH();
            $comments = '{"previousStage":"Creating Letter", "submittedDate":"' . now_date() . '", "currentStage":"Letter Created", "currentStageCvId":' . $stage . ', "nextStage":"Pending Dispatch Information Update", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "letterReview":""}';
            break;
            case $codeValue->OLPPDINFUPDT():
                //Pending Dispatch Information Update
            $priority = $codeValue->CHKPMEDIUM();
            $comments = '{"previousStage":"Letter Created", "submittedDate":"' . now_date() . '", "currentStage":"Pending Dispatch Information Update", "currentStageCvId":' . $stage . ', "nextStage":"Updated Dispatch Information", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "letterReview":""}';
            break;
            case $codeValue->OLPUPDTDDPINFRM():
                //Updated Dispatch Information
            $closeChecker= 1;
            $priority = $codeValue->CHKPLOW();
            $comments = '{"previousStage":"Pending Dispatch Information Update", "submittedDate":"' . now_date() . '", "currentStage":"Updated Dispatch Information", "currentStageCvId":' . $stage . ', "nextStage":"", "department":"' . $userRepo->getUserDepartment($user) . '", "comment":"", "letterReview":""}';
            break;
        }
        if ($process And $user) {
            if (!empty($comments)) {
                //Save comment on letter stages
                $letter->stages()->attach($stage,
                    [
                        'comments' => $comments
                    ]);
                $this->updateLetterStageId($letter);
            }
            if ($closeChecker) {
                //Close recent checker entry
                $input = [
                    'resource_id' => $letter->id,
                    'checker_category_cv_id' => $codeValue->CCALETTER(), //Letter
                ];
                $checkerRepo->closeRecently($input);
            } else {
                //Create checker entry
                $input = [
                    'comments' => $comments,
                    'user_id' => $user,
                    'checker_category_cv_id' => $codeValue->CCALETTER(), //Letter
                    'priority' => $priority,
                ];
                $checkerRepo->create($letter, $input);
            }
        }
        return true;

    }

    /**
     * @param Model $letter
     * @param array $input
     * @return mixed
     */
    public function update(Model $letter, array $input)
    {
        return DB::transaction(function() use ($letter, $input){
            $user_id = access()->id();
            $codeValueRepo = new CodeValueRepository();
            $codeValue = $codeValueRepo->query()->select(['reference'])->where("id", $letter->cv_id)->first();
            $resourceFunc = "resource" . $codeValue->reference;
            $letterResource = $this->$resourceFunc($letter->resource_id, $input['option']);

            /*            switch ($codeValue->reference) {
                            case $codeValueRepo->CLBNAWLTTR():
                                //Benefit Award Letter
                                $letterResource = $this->resourceCLBNAWLTTR($letter->resource_id);
                                break;
                            default:
                                //Notification Acknowledgement Letter ==> CLACKNOWLGMNT
                                $letterResource = $this->resourceCLACKNOWLGMNT($letter->resource_id);
                                break;
                            }*/

                            if (count($letterResource) And count($letterResource['extras'])) {
                                $dataextras = $letterResource['extras'];
                                $toinsert = [];
                                foreach ($dataextras as $dataextra) {
                                    $toinsert += [$dataextra['name'] => $input[$dataextra['name']]];
                                }
                                $extras = json_encode($toinsert);
                            } else {
                                $extras = NULL;
                            }

            //Update Letter
                            $letter->update([
                                'reference' => $input['reference'],
                                'salutation' => $input['salutation'],
                                'box_no' => $input['box_no'],
                                'location' => $input['location'],
                                'gender_id' => $input['gender_id'],
                                'extras' => $extras,
                                'lang'=>$input['lang']
                            ]);
            //Create Letter Log
                            $letter->logs()->create([
                                'cv_id' => $codeValueRepo->OLLLTREDTD(),
                                'user_id' => $user_id,
                            ]);
                            return $letter;
                        });
    }

    /**
     * @param $resource
     * @param $reference
     * @param array $input
     * @return mixed
     */
    public function store($resource, $reference, array $input)
    {
        return DB::transaction(function() use ($resource, $reference, $input) {
            $user_id = access()->id();
            $codeValueRepo = new CodeValueRepository();
            $codeValue = $codeValueRepo->query()->where("reference", $reference)->first();
            $wfModuleRepo = new WfModuleRepository();
            $resourceFunc = "resource" . $reference;
            $letterResource = $this->$resourceFunc($resource, $input['option']);
            $canduplicate = "f";
            $unit_id = 14;

            switch ($reference) {
                case "CLINNOLTR":
                case "CLDNDNOLTR":
                case "CLLTRFCMPLNC":
                case "CLEMPRMDLTR":
                case "CLRFNDOVRPYMNT":
                case "CLIEMPCLOSURE":
                case "CLIEMPCLREXT":
                case "CLIEMPINTEADJ":
                case "CLIEMPPAYROLLMRG":
                case "EMPCONTRIBMOD":

                    //Inspection Notice Letter
                    //Demand Notice Letter (Inspection)
                    //Letter of Compliance (Inspection)
                    //Employer Reminder Letter (Inspection)
                    //Refund Overpayment
                    //Employer closure
                    //Employer closure extension
                    //Interest Adjustment
                $wf_module_id = $wfModuleRepo->query()->select(["id"])->where(["type" => $wfModuleRepo->complianceLetterIssuanceType()['type'], "wf_module_group_id" => $wfModuleRepo->complianceLetterIssuanceType()["group"]])->first()->id;
                    $unit_id = 15; // Compliance ...
                    break;
                    case 'CLBNAWLTTR':
                    //Benefit Award Letter
                    $wf_module_id = $wfModuleRepo->query()->select(["id"])->where(["type" => $wfModuleRepo->benefitAwardLetterIssuanceType()['type'], "wf_module_group_id" => $wfModuleRepo->benefitAwardLetterIssuanceType()["group"]])->first()->id;
                    break;
                    case 'CLRMNDRLETTER':
                    //Reminder Letter
                    $wf_module_id = $wfModuleRepo->query()->select(["id"])->where(["type" => $wfModuleRepo->reminderLetterIssuanceType()['type'], "wf_module_group_id" => $wfModuleRepo->reminderLetterIssuanceType()["group"]])->first()->id;
                    break;
                    default:
                    //Notification Acknowledgement Letter ==> CLACKNOWLGMNT
                    $wf_module_id = $wfModuleRepo->query()->select(["id"])->where(["type" => $wfModuleRepo->acknowledgementLetterIssuanceType()['type'], "wf_module_group_id" => $wfModuleRepo->acknowledgementLetterIssuanceType()["group"]])->first()->id;
                    break;
                }
                $resources = json_encode($letterResource['resources']);

                if (count($letterResource) And count($letterResource['extras'])) {
                    $dataextras = $letterResource['extras'];
                    $toinsert = [];
                    foreach ($dataextras as $dataextra) {
                        $toinsert += [$dataextra['name'] => $input[$dataextra['name']]];
                    }
                    $extras = json_encode($toinsert);
                } else {
                    $extras = NULL;
                }

                $data = [
                    'cv_id' => $codeValue->id,
                    'resource_id' => $resource,
                    'reference' => $input['reference'],
                    'salutation' => $input['salutation'],
                    'box_no' => $input['box_no'],
                    'user_id' => $user_id,
                    'location' => $input['location'],
                    'gender_id' => $input['gender_id'],
                //'resource_type' => '', this is now ignored (not used at the moment) ...
                    'resources' => $resources,
                    'extras' => $extras,
                    'option' => $input['option'],
                    'lang' => $input['lang']
                ];

                $data['unit_id'] = $unit_id;
                $data['canduplicate'] = $canduplicate;
                $data['wf_module_id'] = $wf_module_id;
                $letter = $this->query()->create($data);
            //Update Resource Type
                $this->updateResourceType($letter, $input);
            //Create Letter Log
                $letter->logs()->create([
                    'cv_id' => $codeValueRepo->OLLLTRCRTD(),
                    'user_id' => $user_id,
                ]);
                return $letter;
            });
}

public function updateResourceType(Model $letter, array $input = [])
{
    $codeValueRepo = new CodeValueRepository();
    $codeValue = $codeValueRepo->query()->select(['reference'])->where("id", $letter->cv_id)->first();
    switch ($codeValue->reference) {
        case "CLINNOLTR":
        case "CLRFNDOVRPYMNT":
                //Inspection Notice Letter
                //Refund Overpayment
        $employerTask = (new EmployerInspectionTaskRepository())->find($letter->resource_id);
        $employerTask->letters()->save($letter);
        break;
        case "CLDNDNOLTR":
        case "CLLTRFCMPLNC":
        case "CLEMPRMDLTR":
                //Demand Notice Letter (Inspection)
                //Letter of Compliance (Inspection)
                //Employer Reminder Letter (Inspection)
        if (isset($input['option']) && $input['option'] === 'consolidated') {
            $task = (new EmployerInspectionTaskRepository())->find($letter->resource_id);
            $task->letters()->save($letter);
        } else {
            $payroll = (new InspectionTaskPayrollRepository())->find($letter->resource_id);
            $payroll->letters()->save($letter);
        }

        break;
        case 'CLBNAWLTTR':
                //Benefit Award Letter
        $eligible = (new NotificationEligibleBenefitRepository())->find($letter->resource_id);
        $eligible->letters()->save($letter);
        break;

        case 'CLIEMPCLOSURE':
                //Employer Closure Letter
        $eligible = (new EmployerClosureRepository())->find($letter->resource_id);
        $eligible->letters()->save($letter);
        break;
        case 'CLIEMPCLREXT':
                //Employer Extension Letter
        $eligible = (new EmployerClosureExtensionRepository())->find($letter->resource_id);
        $eligible->letters()->save($letter);
        break;
        case 'CLIEMPINTEADJ':
                //Employer interest adjust
        $eligible = (new InterestAdjustmentRepository())->find($letter->resource_id);
        $eligible->letters()->save($letter);
        break;
        case 'CLIEMPPAYROLLMRG':
                //Employer Payroll Merge Letter
        $employer_payroll_merge = (new MergeDeMergeEmployerPayrollRepository())->find($letter->resource_id);
        $employer_payroll_merge->letters()->save($letter);
        break;

        case 'EMPCONTRIBMOD':
                //Employer Contrib Modification Letter
        $contrib_modification = (new ContributionModificationRepository())->find($letter->resource_id);
        $contrib_modification->letters()->save($letter);
        break;

        default:
                //Notification Acknowledgement Letter ==> CLACKNOWLGMNT
                //Reminder Letter ==> CLRMNDRLETTER
        $incident = (new NotificationReportRepository())->find($letter->resource_id);
        $incident->letters()->save($letter);
        break;
    }
}

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @return mixed
     */
    public function checkForDispatchInfo($level, $resource_id, $module)
    {
        return DB::transaction(function () use ($level, $resource_id, $module) {
            $moduleRepo = new WfModuleRepository();
            $letter = $this->find($resource_id);
            $codeValueRepo = new CodeValueRepository();
            //check if all dispatch information has been updated ...
            if (!$letter->dispatch_number Or !$letter->delivery_date Or !$letter->received_by Or !$letter->receiver_phone_number) {
                $letter->allocated = access()->id();
                $letter->staging_cv_id = $codeValueRepo->OLPPDINFUPDT(); //Pending Dispatch Information Update
                $letter->save();
                //$this->initialize($letter); //to be reviewed later ...
            }
            if ((int)$level == $moduleRepo->complianceLetterDispatchLevel($module)) {
                //letter has been sent
                ///=> Check type of letter ...
                switch ($letter->cv_id) {
                    case $codeValueRepo->CLINNOLTR():
                        //Inspection Notice Letter
                    $employerTaskRepo = new EmployerInspectionTaskRepository();
                    $employerTask = $letter->resource;
                    if ($employerTask->progressive_stage < 4) {
                            $employerTask->staging_cv_id = $codeValueRepo->EITPWTIASCHD(); //Inspection Notice Letter Sent ...
                            $employerTask->progressive_stage = 3; //Task on Progress ...
                            $employerTask->save();
                            $employerTaskRepo->initialize($employerTask);
                        }
                        break;
                        case $codeValueRepo->CLDNDNOLTR():
                        case $codeValueRepo->CLLTRFCMPLNC():
                        //Demand Notice Letter (Inspection)
                        //Letter of Compliance (Inspection)
                        $employerTaskRepo = new EmployerInspectionTaskRepository();
                        $resource = $letter->resource;
                        if ($resource instanceof InspectionTaskPayroll) {
                            $employerTask = $employerTaskRepo->find($resource->employer_inspection_task_id);
                            $payrollIds = $employerTask->payrolls()->pluck("inspection_task_payrolls.id")->all();
                            $count = $this->query()->whereIn("cv_id", [$codeValueRepo->CLDNDNOLTR(), $codeValueRepo->CLLTRFCMPLNC()])->whereIn("resource_id", $payrollIds)->count();
                            if ($count < count($payrollIds)) {
                                //partially created inspection response letter ....
                                //$staging_cv_id = $codeValueRepo->EITPIRESLTRPRTLCRTD(); //Partially Created Inspection Response Letter //why? : Skip updating inspection response letter sent, waiting for other payrolls letter to be sent as well
                            } else {
                                $staging_cv_id = $codeValueRepo->EITPISUEDIRSLR(); //Inspection Response Letter Sent
                            }
                        }
                        if ($resource instanceof EmployerInspectionTask) {
                            $staging_cv_id = $codeValueRepo->EITPISUEDIRSLR(); //Created Inspection Response Letter
                            $employerTask = $resource;
                        }
                        $employerTask->staging_cv_id = $staging_cv_id;
                        $employerTask->save();
                        $employerTaskRepo->initialize($employerTask);
                        break;
                        case $codeValueRepo->CLEMPRMDLTR():
                        //Employer Reminder Letter (Inspection)
                        $employerTaskRepo = new EmployerInspectionTaskRepository();
                        $resource = $letter->resource;
                        $employerTask = $employerTaskRepo->find($resource->employer_inspection_task_id);
                        $employerTask->staging_cv_id = (new CodeValueRepository())->EITPREMLTRSNT(); //Reminder Letter Sent
                        $employerTask->save();
                        $employerTaskRepo->initialize($employerTask);
                        break;
                    }
                }
                return true;
            });
}

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @return mixed
     */
    public function checkIfLetterFinalized($level, $resource_id, $module)
    {
        return DB::transaction(function () use ($level, $resource_id, $module) {
            $moduleRepo = new WfModuleRepository();
            $letter = $this->find($resource_id);
            $userId = access()->id();
            //check if letter reference and dispatch data [letter date, dispatch number] has been updated and confirmed ...
            if (!$letter->letter_date && ($letter->resources)) {
                throw new WorkflowException("Letter date not updated, Please update dispatch information!");
            }
            //Check if letter is printed
            if (!$letter->isprinted) {
                throw new WorkflowException("Action Cancelled, This letter has not been printed!");
            }
            //logger($module);
            switch ($module) {
                case $moduleRepo->complianceLetterIssuanceModule()[0]:
                    //logger($module);
                $codeValueRepo = new CodeValueRepository();
                    //Detect type of letter
                switch ($letter->cv_id) {
                    case $codeValueRepo->CLINNOLTR():
                            //Inspection Notice Letter

                    break;
                    case $codeValueRepo->CLDNDNOLTR():
                    case $codeValueRepo->CLLTRFCMPLNC():
                            //Demand Notice Letter (Inspection)
                            //Letter of Compliance (Inspection)
                    $employerTaskRepo = new EmployerInspectionTaskRepository();
                    $inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
                    $resource = $letter->resource;
                    if ($resource instanceof InspectionTaskPayroll) {
                        $employerTask = $employerTaskRepo->find($resource->employer_inspection_task_id);
                    }
                    if ($resource instanceof EmployerInspectionTask) {
                        $employerTask = $resource;
                    }
                            $employerTask->staging_cv_id = (new CodeValueRepository())->EITPISUEDIRSLR(); //Issued Inspection Response Letter
                            $employerTask->progressive_stage = 6; //Issued Response Letter ...
                            $employerTask->save();
                            $employerTaskRepo->initialize($employerTask);
                            break;
                            case $codeValueRepo->CLEMPRMDLTR():
                            //Employer Reminder Letter (Inspection)

                            break;
                            case $codeValueRepo->CLRFNDOVRPYMNT():
                            //Refund Overpayment

                            break;
                        }
                        break;
                        case $moduleRepo->acknowledgementLetterIssuanceModule()[0]:
                        case $moduleRepo->reminderLetterIssuanceModule()[0]:
                        break;
                        case $moduleRepo->benefitAwardLetterIssuanceModule()[0]:
                    //logger($moduleRepo->benefitAwardLetterIssuanceModule()[0]);
                    //logger($module);
                    //==> approve benefit workflow ...
                        $eligible = $letter->resource;
                        if (!$eligible) {
                            $eligible = (new NotificationEligibleBenefitRepository())->find($letter->resource_id);
                        }
                    //check if the benefit is at level of Notify Employer
                        $workflow = new Workflow(['wf_module_id' => $eligible->workflow->wf_module_id, 'resource_id' => $eligible->notification_workflow_id]);
                        $module = $workflow->getModule();
                        $notifyLevel = $moduleRepo->claimBenefitNotifyEmployerLevel($module);
                        if ((int) $workflow->currentLevel() != (int) $notifyLevel) {
                            throw new WorkflowException("Action cancelled, the benefit approval workflow for this letter has not reached at the level of communicating to employer ...");
                        }
                        if ($workflow->checkIfIsLevelPending($notifyLevel)) {
                            $track = $workflow->currentWfTrack();
                            $letterDate = Carbon::parse($letter->letter_date)->format("d M Y");
                            $wfModule = (new WfTrackRepository())->updateWorkflow($track, [
                                'status' => 1,
                                'user_id' => $userId,
                                'assigned' => $userId,
                                'wf_definition' => 0,
                                'comments' => "Letter sent with ref no: {$letter->reference} dated {$letterDate}",
                                'forward_date' => Carbon::now(),
                            ], "approve_reject");
                        }
                        break;
                    }
                    $letter->wf_done = 1;
                    $letter->save();
            //throw new WorkflowException("Can not proceed"); //for testing purposes ...
                    return true;
                });
}

    /**
     * @param Model $letter
     * @param array $input
     * @return mixed
     */
    public function updateDispatch(Model $letter, array $input)
    {
        return DB::transaction(function () use ($letter, $input) {
            $codeValueRepo = new CodeValueRepository();

            $letter->reference = $input['reference'];
            $letter->letter_date = $input['letter_date'];
            $letter->dispatch_number = $input['dispatch_number'];
            $letter->delivery_date = $input['delivery_date'];
            $letter->received_by = $input['received_by'];
            $letter->receiver_phone_number = $input['receiver_phone_number'];
            $letter->save();
            $success = $this->generatePdf($letter, 1);

            //check if all dispatch information has been updated ...
            if ($letter->dispatch_number And $letter->delivery_date And $letter->received_by And $letter->receiver_phone_number) {
                $letter->allocated = access()->id();
                $letter->staging_cv_id = $codeValueRepo->OLPUPDTDDPINFRM(); //Updated Dispatch Information
                $letter->save();
                $this->initialize($letter);
            }
            return true;
        });
    }

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @return mixed
     */
    public function processLetter($level, $resource_id, $module)
    {
        return DB::transaction(function () use ($level, $resource_id, $module) {
            $moduleRepo = new WfModuleRepository();
            $codeValueRepo = new CodeValueRepository();
            $letter = $this->find($resource_id);
            $approve = 0;
            switch ($module) {
                case $moduleRepo->complianceLetterIssuanceModule()[0]:
                if ((int)$level == $moduleRepo->complianceLetterCreateLevel($module)) {
                        //letter has been created
                        ///=> Check type of letter ...
                    switch ($letter->cv_id) {
                        case $codeValueRepo->CLINNOLTR():
                                //Inspection Notice Letter
                        $employerTaskRepo = new EmployerInspectionTaskRepository();
                        $employerTask = $letter->resource;
                        if ($employerTask->progressive_stage < 4) {
                                    $employerTask->staging_cv_id = $codeValueRepo->EITPINNTLTCRTD(); //Created Inspection Notice Letter
                                    $employerTask->save();
                                    $employerTaskRepo->initialize($employerTask);
                                }
                                break;
                                case $codeValueRepo->CLDNDNOLTR():
                                case $codeValueRepo->CLLTRFCMPLNC():
                                //Demand Notice Letter (Inspection)
                                //Letter of Compliance (Inspection)
                                $employerTaskRepo = new EmployerInspectionTaskRepository();
                                $resource = $letter->resource;
                                if ($resource instanceof InspectionTaskPayroll) {
                                    $employerTask = $employerTaskRepo->find($resource->employer_inspection_task_id);
                                    $payrollIds = $employerTask->payrolls()->pluck("inspection_task_payrolls.id")->all();
                                    $count = $this->query()->whereIn("cv_id", [$codeValueRepo->CLDNDNOLTR(), $codeValueRepo->CLLTRFCMPLNC()])->whereIn("resource_id", $payrollIds)->count();
                                    if ($count < count($payrollIds)) {
                                        //partially created inspection response letter ....
                                        $staging_cv_id = $codeValueRepo->EITPIRESLTRPRTLCRTD(); //Partially Created Inspection Response Letter
                                    } else {
                                        $staging_cv_id = $codeValueRepo->EITPIRESLTRCRTD(); //Created Inspection Response Letter
                                    }
                                }
                                if ($resource instanceof EmployerInspectionTask) {
                                    $staging_cv_id = $codeValueRepo->EITPIRESLTRCRTD(); //Created Inspection Response Letter
                                    $employerTask = $resource;
                                }
                                $employerTask->staging_cv_id = $staging_cv_id;
                                $employerTask->save();
                                $employerTaskRepo->initialize($employerTask);
                                break;
                                case $codeValueRepo->CLEMPRMDLTR():
                                //Employer Reminder Letter (Inspection)
                                $employerTaskRepo = new EmployerInspectionTaskRepository();
                                $resource = $letter->resource;
                                $employerTask = $employerTaskRepo->find($resource->employer_inspection_task_id);
                                $employerTask->staging_cv_id = (new CodeValueRepository())->EITPREMLTRCRTD(); //Reminder Letter Created
                                $employerTask->save();
                                $employerTaskRepo->initialize($employerTask);
                                break;
                            }
                        }
                        if ((int)$level == $moduleRepo->complianceLetterIssuanceApproveLevel($module)) {
                            $approve = 1;
                        //Detect type of letter
                            switch ($letter->cv_id) {
                                case $codeValueRepo->CLINNOLTR():
                                //Inspection Notice Letter
                                //update stage
                                /*$employerTaskRepo = new EmployerInspectionTaskRepository();
                                $employerTask = $employerTaskRepo->find($letter->resource_id);
                                if ($employerTask->progressive_stage < 4) {
                                    $employerTask->staging_cv_id = (new CodeValueRepository())->EITPWTIASCHD(); //Waiting Inspection Assessment Schedule
                                    $employerTask->save();
                                    $employerTaskRepo->initialize($employerTask);
                                }*/
                                break;
                                case $codeValueRepo->CLDNDNOLTR():
                                case $codeValueRepo->CLLTRFCMPLNC():
                                //Demand Notice Letter (Inspection)
                                //Letter of Compliance (Inspection)
                                /*$employerTaskRepo = new EmployerInspectionTaskRepository();
                                $inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
                                $payroll = $inspectionTaskPayrollRepo->find($letter->resource_id);
                                $employerTask = $employerTaskRepo->find($payroll->employer_inspection_task_id);
                                $employerTask->staging_cv_id = (new CodeValueRepository())->EITPISSIRESLTR(); //Issuing Inspection Response Letter
                                //$employerTask->progressive_stage;
                                $employerTask->save();
                                $employerTaskRepo->initialize($employerTask);*/
                                break;
                                case $codeValueRepo->CLEMPRMDLTR():
                                //Employer Reminder Letter (Inspection)
                                /*$employerTaskRepo = new EmployerInspectionTaskRepository();
                                $inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
                                $payroll = $inspectionTaskPayrollRepo->find($letter->resource_id);
                                $employerTask = $employerTaskRepo->find($payroll->employer_inspection_task_id);*/
                                break;
                                case $codeValueRepo->CLRFNDOVRPYMNT():
                                //Refund Overpayment

                                break;
                            }
                        }
                        break;
                        case $moduleRepo->reminderLetterIssuanceModule()[0]:
                        if ((int) $level == $moduleRepo->reminderLetterIssuanceApproveLevel($module)) {
                            $approve = 1;
                        }
                        break;
                        case $moduleRepo->acknowledgementLetterIssuanceModule()[0]:
                        if ((int)$level == $moduleRepo->acknowledgementLetterIssuanceApproveLevel($module)) {
                            $approve = 1;
                        //update is_acknowledgement_sent
                            $incident = $letter->resource;
                            if ($incident) {
                                $is_acknowledgment_sent = true;
                        } elseif ($letter->codeValue->reference == 'CLACKNOWLGMNT') { //Acknowledgement Letters ...
                            $incident = (new NotificationReportRepository())->query()->where("id", $letter->resource_id)->first();
                            $is_acknowledgment_sent = true;
                        }
                        if ($is_acknowledgment_sent) {
                            $incident->is_acknowledgment_sent = 1;
                            $incident->save();
                        }
                    }
                    if ((int)$level == $moduleRepo->acknowledgementLetterIssuanceInitiateLevel($module)) {
                        //update is_acknowledgement_sent
                        $is_acknowledgment_initiated = false;
                        $incident = $letter->resource;
                        if ($incident) {
                            $is_acknowledgment_initiated = true;
                        } elseif ($letter->codeValue->reference == 'CLACKNOWLGMNT') { //Acknowledgement Letters ...
                            $incident = (new NotificationReportRepository())->query()->where("id", $letter->resource_id)->first();
                            $is_acknowledgment_initiated = true;
                        }
                        if ($is_acknowledgment_initiated) {
                            $incident->is_acknowledgment_initiated = 1;
                            $incident->save();
                        }
                    }
                    break;
                    case $moduleRepo->benefitAwardLetterIssuanceModule()[0]:
                    if ((int) $level == $moduleRepo->benefitAwardLetterIssuanceApproveLevel($module)) {
                        $approve = 1;
                    }
                    break;
                }
                if ($approve) {
                    $this->updateLetterApproved($letter);
                }
            //throw new GeneralException("Testing");
                return true;
            });
}

public function updateLetterApproved(Model $letter)
{
    $letter->approved = 1;
    $letter->save();
        //Generate PDF here
    $this->generatePdf($letter);
}

    /**
     * @param Model $letter
     * @param int $isrepeat
     */
    public function generatePdf(Model $letter, $isrepeat = 0) {
        $codeValueRepo = new CodeValueRepository();
        //if (!$letter->pdfready || $isrepeat) {
        if (true) {

            $savePath = letters_dir() . DIRECTORY_SEPARATOR . $letter->id . ".pdf";
            $codeValue = $codeValueRepo->query()->select(['reference'])->where("id", $letter->cv_id)->first();
            $reference = $codeValue->reference;
            $func = "preview" . $reference;
            $view = $this->$func($letter);
            try {
                if (file_exists($savePath)) {
                    logger('I have been deleted here ...');
                    unlink($savePath);
                }

                $options = [
                    //'margin-top'    => 10,
                    'margin-right'  => 16,
                    //'margin-bottom' => 10,
                    'margin-left'   => 16,
                ];
                $pdf = PDFSnappy::loadHTML($view)->setOption('footer-center', 'Page [page] of [toPage]');
                foreach ($options as $margin => $value) {
                    $pdf->setOption($margin, $value);
                }
                $pdf
                    //->setOption('minimum-font-size', '12')
                ->setOption('disable-smart-shrinking', NULL)
                ->setOption('page-size', 'A4')
                ->setOption('encoding', 'utf8')
                    ->save($savePath); //->setOption('margin-left', '4mm')->setOption('margin-right', '4mm')->setOption('minimum-font-size', 12)->setPaper('a4')->setOption('margin-bottom', 0)->setOrientation('portrait')->setOption('page-size', 'A4')->setOption('margin-right', 0)->setOption('dpi', '300')
                    $letter->pdfready = 1;
                    $letter->save();
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                } catch (\Throwable $e) {
                    Log::error($e->getMessage());
                }
            }
        }

    /**
     * @param $resource
     * @return array
     * @description Letter Metadata
     */
    public function resourceCLBNAWLTTR($resource, $option = NULL)
    {
        //Benefit Award Letter
        $eligible = (new NotificationEligibleBenefitRepository())->find($resource);
        $incident = $eligible->incident;
        $documentArray = $incident->documents()->pluck("document_notification_report.document_id")->all();
        $documentCount = (new DocumentRepository())->query()->whereIn("id", $documentArray)->count();
        //$letterCount = $this->getTotalLettersByUnit(14, $resource);
        $letterCount = $this->query()->where("unit_id", 14)->whereIn("resource_id", function ($query) use ($incident) {
            $query->select("id")->from('notification_eligible_benefits')->where('notification_report_id', $incident->id);
        })->count();
        $name = $incident->filename;
        $folioNumber = $documentCount + $letterCount + 1;
        $reference = $incident->filename . "/" . $folioNumber;
        $employer = $incident->employer;
        $employee = $incident->employee;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $benefit = $eligible->benefit;
        $profile_url = "/claim/notification_report/profile/{$incident->id}";

        $payreference = $eligible->payreference;
        $chequedate = ($eligible->payment_date) ? Carbon::parse($eligible->payment_date)->format("Y-m-d") : NULL;
        $sysdef = sysdefs()->data();
        $employer = $incident->employer;
        $gov = 0;
        if ($employer->is_treasury == "t") {
            $gov = 1;
        }
        //dd($eligible->payment_date);
        $extras = [];
        if ($employer->region_id == $sysdef->hq_region) {
            $extras_mf = [];
        } else {
            if ((new OfficeRepository())->query()->where(["isactive" => 1, "region_id" => $employer->region_id])->count()) {
                $extras_mf = [
                    ['name' => 'mf_salutation', 'title' => 'Salutation (Mfawidhi)', 'data_type' => 'text', 'value' => 'Afisa Kanda Mfawidhi'],
                    ['name' => 'mf_institution', 'title' => 'Institution (Mfawidhi)', 'data_type' => 'text', 'value' => 'Mfuko wa Fidia Kwa Mfanyakazi (WCF)'],
                ];
            } else {
                $extras_mf = [
                    ['name' => 'mf_salutation', 'title' => 'Salutation (Mfawidhi)', 'data_type' => 'text', 'value' => 'Afisa Kazi Mfawidhi'],
                    ['name' => 'mf_institution', 'title' => 'Institution (Mfawidhi)', 'data_type' => 'text', 'value' => 'Idara ya Kazi ' . ucwords($employer->region_label)],
                ];
            }
            $extras_mf[] = ['name' => 'mf_box', 'title' => 'Post Box (Mfawidhi)', 'data_type' => 'text', 'value' => ''];
            $extras_mf[] = ['name' => 'mf_location', 'title' => 'Location (Mfawidhi)', 'data_type' => 'text', 'value' => $location];
        }

        switch ($eligible->benefit_type_claim_id) {
            case 1:
                //MAE Refund (Employee/Employer)
            $extras = [
                ['name' => 'maechequeno', 'title' => 'Payment Cheque Number', 'data_type' => 'text', 'value' => $payreference],
                ['name' => 'maechequedate', 'title' => 'Payment Cheque Date', 'data_type' => 'date', 'value' => $chequedate],
            ];
            $extras = array_merge($extras, $extras_mf);
            break;
            case 3:
            case 4:
                //Temporary Disablement
                //Temporary Disablement Refund
            $claimCompensationRepo = new ClaimCompensationRepository();
            $data = $claimCompensationRepo->progressiveCompensationApprove($incident, 0, $eligible->id);
                //dd($data);

            switch (true) {
                case (isset($data['td'][0]['ttd_amount'], $data['td'][0]['tpd_amount']) And $data['td'][0]['ttd_amount'] > 0 And $data['td'][0]['tpd_amount'] > 0):
                        //ttd_tpd
                $extras = [
                    ['name' => 'ttdchequeno', 'title' => 'TTD Payment Cheque Number', 'data_type' => 'text', 'value' => $payreference],
                    ['name' => 'ttdchequedate', 'title' => 'TTD Payment Cheque Date', 'data_type' => 'date', 'value' => $chequedate],
                    ['name' => 'tpdchequeno', 'title' => 'TPD Payment Cheque Number', 'data_type' => 'text', 'value' => $payreference],
                    ['name' => 'tpdchequedate', 'title' => 'TPD Payment Cheque Date', 'data_type' => 'date', 'value' => $chequedate],
                ];
                break;
                case (isset($data['td'][0]['ttd_amount']) And $data['td'][0]['ttd_amount'] > 0) Or (isset($data['td'][0]['tpd_amount']) And $data['td'][0]['tpd_amount'] > 0):
                        //ttd or tpd
                $extras = [
                    ['name' => 'tdchequeno', 'title' => 'TD Payment Cheque Number', 'data_type' => 'text', 'value' => $payreference],
                    ['name' => 'tdchequedate', 'title' => 'TD Payment Cheque Date', 'data_type' => 'date', 'value' => $chequedate],
                ];
                break;
            }

                //if ($employer->is_treasury == "t") {
            if (true) {
                $extras = array_merge($extras, $extras_mf);
            }
            break;
            case 5:
                //Permanent Disablement
            $claim = $incident->claim;
            if ($claim->pd > (int) sysdefs()->data()->percentage_imparement_scale) {
                $extras = [
                    ['name' => 'paybank', 'title' => 'Bank Name Paid', 'data_type' => 'text', 'value' => ''],
                    ['name' => 'payaccount', 'title' => 'Bank Account Number', 'data_type' => 'text', 'value' => ''],
                ];
                $extras = array_merge($extras, $extras_mf);
            } else {
                $extras = [
                    ['name' => 'pdchequeno', 'title' => 'Payment Cheque Number', 'data_type' => 'text', 'value' => $payreference],
                    ['name' => 'pdchequedate', 'title' => 'Payment Cheque Date', 'data_type' => 'date', 'value' => $chequedate],
                ];
            }

            break;
            case 7:
                //Survivors

            break;
        }

        //if ($gov) {
        if (true) {
            $extras[] = ['name' => 'employername', 'title' => 'Employer Name', 'data_type' => 'textarea', 'value' => $employer->name];
        }

        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation("sw"),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => "/claim/notification_report/profile/{$incident->id}"],
                ["value" => $employee->name, "title" => "Employee", "url" => "/compliance/employee/profile/{$employee->id}"],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
                ["value" => $benefit->name, "title" => "Benefit", "url" => "#"],
            ],
            'extras' => $extras,
        ];
    }

    /**
     * @param Model $letter
     * @return array
     * @description Benefit Award Letter
     */
    public function dataCLBNAWLTTR(Model $letter)
    {
        $eligibleRepo = new NotificationEligibleBenefitRepository();
        $eligible = $eligibleRepo->find($letter->resource_id);
        $incident = $eligible->incident;
        $case = 0;

        $claimCompensationRepo = new ClaimCompensationRepository();
        $data = $claimCompensationRepo->progressiveCompensationApprove($incident, 0, $eligible->id);
        //dd($data);

        switch ($eligible->benefit_type_claim_id) {
            case 1:
                //MAE Refund (Employee/Employer)
            $case = 12;
            $benefits = [
                'mae' => number_2_format($data['mae'][0]['assessed_amount']),
            ];
            break;
            case 3:
            case 4:
                //Temporary Disablement
                //Temporary Disablement Refund
            $employer = $incident->employer;
            $gov = 0;
            if ($employer->is_treasury == "t") {
                $gov = 1;
            }
            $sysdef = sysdefs()->data();
            $gme = 0;
            if ($incident->monthly_earning * ($sysdef->percentage_of_earning / 100) > $sysdef->minimum_monthly_pension) {
                $gme = 1;
            }
            $td_amount = 0;
            $ttd_amount = 0;
            $tpd_amount = 0;
            switch (true) {
                case (isset($data['td'][0]['ttd_amount'], $data['td'][0]['tpd_amount']) And $data['td'][0]['ttd_amount'] > 0 And $data['td'][0]['tpd_amount'] > 0):
                        //ttd_tpd
                $ttd_amount = $data['td'][0]['ttd_amount'];
                $tpd_amount = $data['td'][0]['tpd_amount'];
                $td_amount = $ttd_amount + $tpd_amount;
                switch (true) {
                    case ($gme) And ($gov):
                    $case = 6;
                    break;
                    case ($gme) And (!$gov):
                    $case = 5;
                    break;
                    case (!$gme) And ($gov):
                    $case = 8;
                    break;
                    case (!$gme) And (!$gov):
                    $case = 7;
                    break;
                }
                break;
                case (isset($data['td'][0]['ttd_amount']) And $data['td'][0]['ttd_amount'] > 0) Or (isset($data['td'][0]['tpd_amount']) And $data['td'][0]['tpd_amount'] > 0):
                        //ttd
                $ttd_amount = $data['td'][0]['ttd_amount'];
                $tpd_amount = $data['td'][0]['tpd_amount'];
                $td_amount = $ttd_amount + $tpd_amount;
                switch (true) {
                    case ($gme) And ($gov):
                    $case = 2;
                    break;
                    case ($gme) And (!$gov):
                    $case = 1;
                    break;
                    case (!$gme) And ($gov):
                    $case = 4;
                    break;
                    case (!$gme) And (!$gov):
                    $case = 3;
                    break;
                }
                break;
                case (isset($data['td'][0]['ttd_amount']) And $data['td'][0]['ttd_amount'] == 0) Or (isset($data['td'][0]['tpd_amount']) And $data['td'][0]['tpd_amount'] == 0):
                $ttd_amount = 0;
                $tpd_amount = 0;
                $td_amount = $ttd_amount + $tpd_amount;
                        $gme = 0; //Ignore its effect in the later executions ...
                        $case = 16;
                        break;
                    }
                    $totalemployerpaid = 0;
                    $differencefromemployerpaid = 0;
                    if (!$gme) {
                        $disability_state_checklists = $eligible->notificationDisabilityStates;
                        $hospitalization = 0;
                        $ed = 0;
                        $ld = 0;
                        foreach ($disability_state_checklists as $disability_state_checklist) {
                            $disability_state_assessment = $eligible->notificationDisabilityStateAssessments->where('notification_disability_state_id', $disability_state_checklist->id)->first();
                            if ($disability_state_assessment) {
                                $days = $disability_state_assessment->days;
                                switch ($disability_state_checklist->disability_state_checklist_id) {
                                    case 1:
                                        //Hospitalization
                                    $hospitalization = $days;
                                        //dd($hospitalization);
                                    break;
                                    case 2:
                                        //Day Off
                                    $ed = $days;
                                    break;
                                    case 3:
                                        //Light Duties
                                    $ld = $days;
                                    break;
                                }
                            }
                        //dd($disability_state_checklist);

                        }
                        $compensated_amount = $incident->monthly_earning * ($sysdef->percentage_of_earning / 100);
                        $day_salary_rate = round($compensated_amount / $sysdef->no_of_days_for_a_month_in_assessment, 2);
                        $half_day_salary_rate = $day_salary_rate / 2;
                        $totalemployerpaid = (($hospitalization + $ed) * $day_salary_rate) + ($half_day_salary_rate * $ld);
                        $differencefromemployerpaid = $td_amount - $totalemployerpaid;
                    //dd($hospitalization . ' ' . $ed . ' ' . $ld);
                    }
                    $benefits = [
                        'ttd_amount' => number_2_format($ttd_amount),
                        'tpd_amount' => number_2_format($tpd_amount),
                        'td_amount' => number_2_format($td_amount),
                        'totalemployerpaid' => number_2_format($totalemployerpaid),
                        'differencefromemployerpaid' => number_2_format($differencefromemployerpaid),
                        'differencefromemployerpaidactual' => $differencefromemployerpaid,
                    ];
                //dd($benefits);
                    break;
                    case 5:
                //Permanent Disablement
                    $claim = $incident->claim;
                //dd($data);

                    $has_accumulation = 0;
                    $accumulatedperiod = 0;
                    $payrollRepo = new PayrollRepository();

                    switch (true) {
                        case (isset($data['pd']['ppd'])):
                        //ppd

                        $amount = $data['pd']['ppd'];
                        $payableamount = $amount;
                        $case = 9;

                        break;
                        case (isset($data['pd']['ppd_month'])):
                        //check the difference of date from MMI

                        $amount = $data['pd']['ppd_month'];
                        $payroll = $payrollRepo->getFirstPayrollPaymentByEmployee($incident->id);
                        $check = $claimCompensationRepo->checkMmiDiff($incident);
                        if ($check['diff']) {
                            $has_accumulation = 1;
                            if ($payroll['total_amount']) {
                                $payableamount = $payroll['total_amount'];
                                $accumulatedperiod = $payroll['arrears_month'];
                            } else {
                                $arrearsInfo = $payrollRepo->getCurrentPayrollInfoByEmployee($incident->id);
                                $accumulatedperiod = $arrearsInfo['arrears_month'];
                                $payableamount = $amount * $accumulatedperiod;
                            }
                        } else {
                            $payableamount = $amount;
                        }
                        $case = 10;
                        break;
                        case (isset($data['pd']['ptd'])):
                        //check the difference of date from MMI
                        $accumulatedperiod = 0;
                        $payroll = $payrollRepo->getFirstPayrollPaymentByEmployee($incident->id);
                        $check = $claimCompensationRepo->checkMmiDiff($incident);
                        $amount = $data['pd']['ptd'];
                        if ($check['diff']) {
                            $has_accumulation = 1;
                            if ($payroll['total_amount']) {
                                $payableamount = $payroll['total_amount'];
                                $accumulatedperiod = $payroll['arrears_month'];
                            } else {
                                $arrearsInfo = $payrollRepo->getCurrentPayrollInfoByEmployee($incident->id);
                                $accumulatedperiod = $arrearsInfo['arrears_month'];
                                $payableamount = $amount * $accumulatedperiod;
                            }
                        } else {
                            $payableamount = $amount;
                        }
                        $case = 10;
                        break;
                        default:
                        $amount = 0;
                        $payableamount = $amount;
                        $case = 16;
                        break;
                    }
                    $benefits = [
                        'pd' => (int) $claim->pd,
                        'pdamount' => number_2_format($amount),
                        'has_accumulation' => $has_accumulation,
                        'accumulatedperiod' => $accumulatedperiod,
                        'payableamount' => number_2_format($payableamount),
                    ];

                    break;
                    case 7:
                //Survivors

                    break;
                }

                if ($case) {
                    $annexure = "annexure" . $case;
                } else {
                    $annexure = "empty";
                }

                return [
                    'eligible' => $eligible,
                    'incident' => $incident,
                    'letter' => $letter,
                    'benefits' => $benefits,
                    'annexure' => $annexure,
                ];

            }

            public function previewCLBNAWLTTR(Model $letter, $print = 0)
            {
        //
                $data = $this->dataCLBNAWLTTR($letter);
        //logger($data['annexure']);
                return view("backend/letters/claim/CLBNAWLTTR/" . $data['annexure'])
            //return view("backend/letters/CLBNAWLTTR/sample")
                ->with("eligible", $data['eligible'])
                ->with("incident", $data['incident'])
                ->with("letter", $data['letter'])
                ->with("benefits", $data['benefits'])
                ->with("print", $print);
            }

    /**
     * @param $resource
     * @return array
     * @description Letter Metadata
     */
    public function resourceCLACKNOWLGMNT($resource, $option = NULL)
    {
        //Notification Acknowledgement Letter
        $incident = (new NotificationReportRepository())->find($resource);
        $documentArray = $incident->documents()->pluck("document_notification_report.document_id")->all();
        $documentCount = (new DocumentRepository())->query()->whereIn("id", $documentArray)->count();
        $letterCount = $this->getTotalLettersByUnit(14, $resource);
        $name = $incident->filename;
        $folioNumber = $documentCount + $letterCount + 1;
        $reference = $incident->filename . "/" . $folioNumber;
        $employer = $incident->employer;
        $employee = $incident->employee;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/claim/notification_report/profile/{$incident->id}";
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => "/claim/notification_report/profile/{$incident->id}"],
                ["value" => $employee->name, "title" => "Employee", "url" => "/compliance/employee/profile/{$employee->id}"],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
            ],
            "extras" => [
                ['name' => 'employername', 'title' => 'Employer Name', 'data_type' => 'textarea', 'value' => $employer->name]
            ],
        ];
    }

    /**
     * @param Model $letter
     * @return array
     * @description Acknowledgement Letter
     */
    public function dataCLACKNOWLGMNT(Model $letter)
    {
        //Preview Claim Acknowledgement Letter
        //$sample = "backend/operation/claim/notification_report/includes/letters/acknowledgement/progressive/sample";
        $notificationRepo = new NotificationReportRepository();
        $docRepo = new DocumentRepository();
        $incident = $notificationRepo->find($letter->resource_id);
        //$validationDocs = $notificationRepo->getProgressiveDocumentList($incident, [10], 2);
        $validationDocs = $notificationRepo->getProgressiveDocumentList($incident);
        //$benefitDocs = $notificationRepo->getProgressiveDocumentList($incident);
        $uploadedDocs = $incident->documents()->pluck("documents.id")->all();
        $validationDocuments = $docRepo->query()->select(["name", "id", "description"])->whereIn("id", $validationDocs)->whereNotIn("id", $uploadedDocs)
            //->where(["ismandatory" => 1, "anysource" => 1])
        ->where(["anysource" => 1])
        ->get();
        //$benefitDocuments = $docRepo->query()->select(["name", "id"])->whereIn("id", $benefitDocs)->whereNotIn("id", $uploadedDocs)->whereNotIn("id", $validationDocs)->get();
        $benefitDocuments = $docRepo->query()->select(["name", "id", "description"])->whereHas("benefits", function ($query) use ($incident) {
            $query->where("document_benefits.incident_type_id", $incident->incident_type_id)->whereHas("benefitType", function ($query) {
                $query->whereNotIn("benefit_types.benefit_type_claim_id", [1,2]);
            });
        })->whereNotIn("id", $uploadedDocs)->get();
        $has_questionaire = $incident->documents()->whereIn("documents.id", [6])->count();
        /*$validationDocuments = $docRepo->query()->select(["name", "id"])->whereHas("benefits", function ($query) use ($incident) {
            $query->where("document_benefits.incident_type_id", $incident->incident_type_id)->whereHas("benefitType", function ($query) {
                $query->whereIn("benefit_types.benefit_type_claim_id", [1]);
            });
        })->whereNotIn("id", $uploadedDocs)->get();*/
        $cases = [
            "case1" => 0,
            "case2" => 0,
            "case3" => 0,
            "case4" => 0,
            "case5" => 0,
        ];
        //dd($benefitDocuments);
        switch (true) {
            case ($incident->progressive_stage >= 4 And $incident->pd_ready):
                //INVESTIGATED&VALIDATED&ALL BENEFIT DOCUMENTS READY
            $cases['case5'] = 1;
            break;
            case ($incident->progressive_stage >= 4):
                //INVESTIGATED&VALIDATED
            $cases['case1'] = 1;
            break;
            case ($incident->approval_ready And $incident->progressive_stafe <= 2):
                //logger('Yes i am there 1 2');
                //NO INVESTIGATION BUT ALL VALIDATION DOC AVAILABLE
            if ($has_questionaire) {
                $cases['case1'] = 1;
            } else {
                $cases['case2'] = 1;
            }
                //
            break;
            case (!$incident->approval_ready And $incident->progressive_stage == 3):
                //NO VALIDATION DOC. BUT INVESTIGATED
            $cases['case3'] = 1;
            break;
            case (!$incident->approval_ready And $incident->progressive_stage <= 2):
                //logger('Yes i am there 3 4');
                //NO INVESTIGATION&NO VALIDATION DOCUMENT
            if ($has_questionaire) {
                $cases['case3'] = 1;
            } else {
                $cases['case4'] = 1;
            }
            break;
        }
        return [
            'incident' => $incident,
            'letter' => $letter,
            'cases' => $cases,
            'validation_documents' => $validationDocuments,
            'benefit_documents' => $benefitDocuments,
        ];
    }

    /**
     * @param Model $letter
     * @return mixed
     */
    public function previewCLACKNOWLGMNT(Model $letter, $print = 0)
    {
        //
        $data = $this->dataCLACKNOWLGMNT($letter);
        //dd($data);
        return view("backend/letters/claim/CLACKNOWLGMNT/template")
        ->with("incident", $data['incident'])
        ->with("letter", $data['letter'])
        ->with("cases", $data['cases'])
        ->with("validation_documents", $data['validation_documents'])
        ->with("benefit_documents", $data['benefit_documents'])
        ->with("print", $print);
    }

    /**
     * @param $resource
     * @return array
     */
    public function resourceCLINNOLTR($resource, $option = NULL)
    {
        //Inspection Notice Letter
        $employerTask = (new EmployerInspectionTaskRepository())->find($resource);
        $inspection = $employerTask->inspectionTask->inspection;
        $letterCount = $this->getTotalLettersByUnit(15, $resource);
        $folioNumber = $letterCount + 1;
        $reference = "AC.209/535/01-C" . "/" . $folioNumber;
        $name = $reference;
        $employer = $employerTask->employer;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/compliance/inspection/employer_task/{$employerTask->id}";
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => $profile_url],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
                ["value" => $inspection->filename, "title" => "Inspection Reference", "url" => "/compliance/inspection/{$inspection->id}"],
                ["value" => $employerTask->allocatedUser->name, "title" => "Inspector", "url" => "#"],
            ],
            "extras" => [

            ],
        ];
    }

    /**
     * @param Model $letter
     * @return array
     */
    public function dataCLINNOLTR(Model $letter)
    {
        //Inspection Notice Letter
        $employerInspectionTaskRepo = new EmployerInspectionTaskRepository();
        $employerTask = $employerInspectionTaskRepo->find($letter->resource_id);
        $employer = $employerTask->employer;
        return [
            'employer' => $employer,
            'letter' => $letter,
            'employer_task' => $employerTask,
        ];
    }

    public function previewCLINNOLTR(Model $letter, $print = 0)
    {
        //Inspection Notice Letter
        $data = $this->dataCLINNOLTR($letter);
        return view("backend/letters/compliance/CLINNOLTR/template")
        ->with("employer", $data['employer'])
        ->with("employer_task", $data['employer_task'])
        ->with("letter", $data['letter'])
        ->with("print", $print);
    }

    public function resourceCLDNDNOLTR($resource, $option = NULL)
    {
        //Demand Notice Letter (Inspection)
        $codeValueRepo = new CodeValueRepository();
        $inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
        $employerInspectionTaskRepo = new EmployerInspectionTaskRepository();

        if ($option == 'consolidated') {
            $employerTask = $employerInspectionTaskRepo->find($resource);
            $employer_inspection_task_id = $employerTask->id;
        } else {
            $payroll = $inspectionTaskPayrollRepo->find($resource);
            $employerTask = $employerInspectionTaskRepo->find($payroll->employer_inspection_task_id);
            $employer_inspection_task_id = $payroll->employer_inspection_task_id;
        }

        $inspection = $employerTask->inspectionTask->inspection;
        $letterCount = $this->getTotalLettersByUnit(15, $resource);
        $folioNumber = $letterCount + 1;
        $reference = "AC.209/535/01-C" . "/" . $folioNumber;
        $name = $reference;
        $employer = $employerTask->employer;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/compliance/inspection/employer_task/{$employerTask->id}";
        //Inspection Notice Letter Issued
        $refLetter = $this->getLetterByReference($codeValueRepo->CLINNOLTR(), $employer_inspection_task_id); //Inspection Notice Letter....
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "option" => $option,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => $profile_url],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
                ["value" => $inspection->filename, "title" => "Inspection Reference", "url" => "/compliance/inspection/{$inspection->id}"],
                ["value" => $employerTask->allocatedUser->name, "title" => "Inspector", "url" => "#"],
            ],
            "extras" => [
                ['name' => 'noticereference', 'title' => 'Inspection Notice Letter Reference', 'data_type' => 'text', 'value' => ($refLetter) ? $refLetter->reference : ""],
                ['name' => 'noticedate', 'title' => 'Inspection Notice Letter Date', 'data_type' => 'date', 'value' => ($refLetter) ? $refLetter->letter_date_label : ""],
                ['name' => 'employername', 'title' => 'Employer Name', 'data_type' => 'text', 'value' => $employer->name],
            ],
        ];
    }

    /**
     * @param Model $letter
     * @return array
     */
    public function dataCLDNDNOLTR(Model $letter)
    {
        //Demand Notice Letter (Inspection)
        $employerInspectionTaskRepo = new EmployerInspectionTaskRepository();
        //$inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
        //$payroll = $inspectionTaskPayrollRepo->find($letter->resource_id);
        //$employerTask = (new EmployerInspectionTaskRepository())->find($payroll->employer_inspection_task_id);
        //$employerTask = $employerInspectionTaskRepo->find($payroll->employer_inspection_task_id);
        //$assessment_summary = $employerInspectionTaskRepo->assessmentSummary($employerTask, $payroll->id);
        $resource = $letter->resource;
        if ($resource instanceof InspectionTaskPayroll) {
            $employerTask = $employerInspectionTaskRepo->find($resource->employer_inspection_task_id);
            $assessment_summary = $employerInspectionTaskRepo->assessmentSummary($employerTask, $resource->id);
        }
        if ($resource instanceof EmployerInspectionTask) {
            $employerTask = $resource;
            $assessment_summary = $employerInspectionTaskRepo->assessmentSummary($employerTask);
        }
        //dd($assessment_summary);
        $employer = $employerTask->employer;
        return [
            'employer' => $employer,
            'letter' => $letter,
            'employer_task' => $employerTask,
            'assessment_summary' => $assessment_summary,
        ];
    }

    public function previewCLDNDNOLTR(Model $letter, $print = 0)
    {
        //Demand Notice Letter (Inspection)
        $data = $this->dataCLDNDNOLTR($letter);
        return view("backend/letters/compliance/CLDNDNOLTR/template")
        ->with("employer", $data['employer'])
        ->with("employer_task", $data['employer_task'])
        ->with("letter", $data['letter'])
        ->with("assessment_summary", $data['assessment_summary'])
        ->with("print", $print);
    }

    public function resourceCLLTRFCMPLNC($resource, $option = NULL)
    {
        //Letter of Compliance (Inspection)
        $codeValueRepo = new CodeValueRepository();
        $inspectionTaskPayroll = new InspectionTaskPayrollRepository();
        $payroll = $inspectionTaskPayroll->find($resource);
        $employerTask = (new EmployerInspectionTaskRepository())->find($payroll->employer_inspection_task_id);
        $inspection = $employerTask->inspectionTask->inspection;
        $letterCount = $this->getTotalLettersByUnit(15, $resource);
        $folioNumber = $letterCount + 1;
        $reference = "AC.209/535/01-C" . "/" . $folioNumber;
        $name = $reference;
        $employer = $employerTask->employer;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/compliance/inspection/employer_task/{$employerTask->id}";
        //Inspection Notice Letter Issued
        $refLetter = $this->getLetterByReference($codeValueRepo->CLINNOLTR(), $employerTask->id); //Inspection Notice Letter....
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => $profile_url],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
                ["value" => $inspection->filename, "title" => "Inspection Reference", "url" => "/compliance/inspection/{$inspection->id}"],
                ["value" => $employerTask->allocatedUser->name, "title" => "Inspector", "url" => "#"],
            ],
            "extras" => [
                ['name' => 'noticereference', 'title' => 'Inspection Notice Letter Reference', 'data_type' => 'text', 'value' => ($refLetter) ? $refLetter->reference : ""],
                ['name' => 'noticedate', 'title' => 'Inspection Notice Letter Date', 'data_type' => 'date', 'value' =>  ($refLetter) ? $refLetter->letter_date_label : ""],
                ['name' => 'employername', 'title' => 'Employer Name', 'data_type' => 'text', 'value' => $employer->name],
            ],
        ];
    }

    public function dataCLLTRFCMPLNC(Model $letter)
    {
        //Letter of Compliance (Inspection)
        $employerInspectionTaskRepo = new EmployerInspectionTaskRepository();
        $inspectionTaskPayrollRepo = new InspectionTaskPayrollRepository();
        $payroll = $inspectionTaskPayrollRepo->find($letter->resource_id);
        //$employerTask = $employerInspectionTaskRepo->find($letter->resource_id);
        $employerTask = $employerInspectionTaskRepo->find($payroll->employer_inspection_task_id);
        $assessment_summary = $employerInspectionTaskRepo->assessmentSummary($employerTask, $payroll->id);
        $employer = $employerTask->employer;
        return [
            'employer' => $employer,
            'letter' => $letter,
            'employer_task' => $employerTask,
            'assessment_summary' => $assessment_summary,
        ];
    }

    public function previewCLLTRFCMPLNC(Model $letter, $print = 0)
    {
        //Letter of Compliance (Inspection)
        $data = $this->dataCLLTRFCMPLNC($letter);
        return view("backend/letters/compliance/CLLTRFCMPLNC/template")
        ->with("employer", $data['employer'])
        ->with("employer_task", $data['employer_task'])
        ->with("letter", $data['letter'])
        ->with("assessment_summary", $data['assessment_summary'])
        ->with("print", $print);
    }

    public function resourceCLEMPRMDLTR($resource, $option = NULL)
    {
        //Employer Reminder Letter (Inspection)

    }

    public function dataCLEMPRMDLTR(Model $letter)
    {
        //Employer Reminder Letter (Inspection)

    }

    public function previewCLEMPRMDLTR(Model $letter, $print = 0)
    {
        //Employer Reminder Letter (Inspection)
        $data = $this->dataCLEMPRMDLTR($letter);
        return view("backend/letters/compliance/CLEMPRMDLTR/template")
        ->with("employer", $data['employer'])
        ->with("print", $print);
    }

    public function resourceCLRMNDRLETTER($resource, $option = NULL)
    {
        //Notification Reminder Letter
        $codeValueRepo = new CodeValueRepository();
        $incident = (new NotificationReportRepository())->find($resource);
        $documentArray = $incident->documents()->pluck("document_notification_report.document_id")->all();
        $documentCount = (new DocumentRepository())->query()->whereIn("id", $documentArray)->count();
        $letterCount = $this->getTotalLettersByUnit(14, $resource);
        $name = $incident->filename;
        $folioNumber = $documentCount + $letterCount + 1;
        $reference = $incident->filename . "/" . $folioNumber;
        $employer = $incident->employer;
        $employee = $incident->employee;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/claim/notification_report/profile/{$incident->id}";
        //Acknowledgement Letter or Reminder Letter Issued ...
        $refLetter = $this->getLetterByArrReference([
            $codeValueRepo->CLRMNDRLETTER(), //Notification Reminder Letter
            $codeValueRepo->CLACKNOWLGMNT(), //Notification Acknowledgement Letter
        ], $resource); //Acknowledgement Letter or Reminder Letter ....
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => "/claim/notification_report/profile/{$incident->id}"],
                ["value" => $employee->name, "title" => "Employee", "url" => "/compliance/employee/profile/{$employee->id}"],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
            ],
            "extras" => [
                ['name' => 'prevreference', 'title' => 'Previous Letter Reference', 'data_type' => 'text', 'value' => ($refLetter) ? $refLetter->reference : ""],
                ['name' => 'prevdate', 'title' => 'Previous Letter Date', 'data_type' => 'date', 'value' =>  ($refLetter) ? $refLetter->letter_date_label : ""],
                ['name' => 'employername', 'title' => 'Employer Name', 'data_type' => 'textarea', 'value' => $employer->name]
            ],
        ];
    }

    public function dataCLRMNDRLETTER(Model $letter)
    {
        //Notification Reminder Letter
        //Preview Claim Acknowledgement Letter
        //$sample = "backend/operation/claim/notification_report/includes/letters/acknowledgement/progressive/sample";
        $notificationRepo = new NotificationReportRepository();
        $docRepo = new DocumentRepository();
        $incident = $notificationRepo->find($letter->resource_id);
        //$validationDocs = $notificationRepo->getProgressiveDocumentList($incident, [10], 2);
        $validationDocs = $notificationRepo->getProgressiveDocumentList($incident);
        //$benefitDocs = $notificationRepo->getProgressiveDocumentList($incident);
        $uploadedDocs = $incident->documents()->pluck("documents.id")->all();
        $validationDocuments = $docRepo->query()->select(["name", "id", "description"])->whereIn("id", $validationDocs)->whereNotIn("id", $uploadedDocs)
            //->where(["ismandatory" => 1, "anysource" => 1])
        ->where(["anysource" => 1])
        ->get();
        //$benefitDocuments = $docRepo->query()->select(["name", "id"])->whereIn("id", $benefitDocs)->whereNotIn("id", $uploadedDocs)->whereNotIn("id", $validationDocs)->get();
        $benefitDocuments = $docRepo->query()->select(["name", "id", "description"])->whereHas("benefits", function ($query) use ($incident) {
            $query->where("document_benefits.incident_type_id", $incident->incident_type_id)->whereHas("benefitType", function ($query) {
                $query->whereNotIn("benefit_types.benefit_type_claim_id", [1,2]);
            });
        })->whereNotIn("id", $uploadedDocs)->get();
        $has_questionaire = $incident->documents()->whereIn("documents.id", [6])->count();
        /*$validationDocuments = $docRepo->query()->select(["name", "id"])->whereHas("benefits", function ($query) use ($incident) {
            $query->where("document_benefits.incident_type_id", $incident->incident_type_id)->whereHas("benefitType", function ($query) {
                $query->whereIn("benefit_types.benefit_type_claim_id", [1]);
            });
        })->whereNotIn("id", $uploadedDocs)->get();*/
        $cases = [
            "case1" => 0,
            "case2" => 0,
            "case3" => 0,
            "case4" => 0,
            "case5" => 0,
        ];
        //dd($benefitDocuments);
        switch (true) {
            case ($incident->progressive_stage >= 4 And $incident->pd_ready):
                //INVESTIGATED&VALIDATED&ALL BENEFIT DOCUMENTS READY
            $cases['case5'] = 1;
            break;
            case ($incident->progressive_stage >= 4):
                //INVESTIGATED&VALIDATED
            $cases['case1'] = 1;
            break;
            case ($incident->approval_ready And $incident->progressive_stafe <= 2):
                //logger('Yes i am there 1 2');
                //NO INVESTIGATION BUT ALL VALIDATION DOC AVAILABLE
            if ($has_questionaire) {
                $cases['case1'] = 1;
            } else {
                $cases['case2'] = 1;
            }
                //
            break;
            case (!$incident->approval_ready And $incident->progressive_stage == 3):
                //NO VALIDATION DOC. BUT INVESTIGATED
            $cases['case3'] = 1;
            break;
            case (!$incident->approval_ready And $incident->progressive_stage <= 2):
                //logger('Yes i am there 3 4');
                //NO INVESTIGATION&NO VALIDATION DOCUMENT
            if ($has_questionaire) {
                $cases['case3'] = 1;
            } else {
                $cases['case4'] = 1;
            }
            break;
        }
        return [
            'incident' => $incident,
            'letter' => $letter,
            'cases' => $cases,
            'validation_documents' => $validationDocuments,
            'benefit_documents' => $benefitDocuments,
        ];
    }

    public function previewCLRMNDRLETTER(Model $letter, $print = 0)
    {
        //Notification Reminder Letter
        $data = $this->dataCLRMNDRLETTER($letter);
        return view("backend/letters/claim/CLRMNDRLETTER/template")
        ->with("incident", $data['incident'])
        ->with("letter", $data['letter'])
        ->with("cases", $data['cases'])
        ->with("validation_documents", $data['validation_documents'])
        ->with("benefit_documents", $data['benefit_documents'])
        ->with("print", $print);
    }

    public function resourceCLRFNDOVRPYMNT($resource, $option = NULL)
    {
        //Refund Overpayment

    }

    public function dataCLRFNDOVRPYMNT(Model $letter)
    {
        //Refund Overpayment

    }

    public function previewCLRFNDOVRPYMNT(Model $letter, $print = 0)
    {
        //Refund Overpayment
        $data = $this->dataCLRFNDOVRPYMNT($letter);
        return view("backend/letters/compliance/CLRFNDOVRPYMNT/template")
        ->with("employer", $data['employer'])
        ->with("print", $print);
    }


    /*Resource Client Employer closure*/
    public function resourceCLIEMPCLOSURE($resource, $option = NULL)
    {
        //Employer closure Letter (Closure)
        $codeValueRepo = new CodeValueRepository();
        $closure_repo = new EmployerClosureRepository();
        $closure = $closure_repo->find($resource);
        $folioNumber = $closure->getLetterFolioNoAttribute('CLIEMPCLOSURE');
        $reference = $closure->getLetterReferenceNoAttribute('CLIEMPCLOSURE');
        $name = $reference;
        $employer = $closure->closedEmployer;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/compliance/employer/closure/profile/{$closure->id}";
        //Inspection Notice Letter Issued
        $refLetter = $this->getLetterByReference($codeValueRepo->findIdByReference('CLIEMPCLOSURE'), $closure->id); //Inspection Notice Letter....
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => $profile_url],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
                ["value" => $closure->close_type_name, "title" => "Closure Type", "url" => $profile_url],
                ["value" => $closure->User->username, "title" => "Initiator", "url" => $profile_url],
            ],
            "extras" => [
//                ['name' => 'noticereference', 'title' => 'Inspection Notice Letter Reference', 'data_type' => 'text', 'value' => ($refLetter) ? $refLetter->reference : ""],
//                ['name' => 'noticedate', 'title' => 'Inspection Notice Letter Date', 'data_type' => 'date', 'value' =>  ($refLetter) ? $refLetter->letter_date_label : ""],
//                ['name' => 'employername', 'title' => 'Employer Name', 'data_type' => 'text', 'value' => $employer->name],
            ],
        ];
    }

    /*Data client employer closure*/
    public function dataCLIEMPCLOSURE(Model $letter)
    {
        //Employer Closure Letter (Closure)
        $closure_repo = new EmployerClosureRepository();
        $closure = $closure_repo->find($letter->resource_id);
        $employer = $closure->closedEmployer;

        $letter_ref =   $closure->getLetterReferenceAttribute($letter->lang);
        return [
            'letter_reference' => $letter_ref,
            'employer' => $employer,
            'closure' => $closure
        ];
    }

    /*Preview Client closure*/
    public function previewCLIEMPCLOSURE(Model $letter, $print = 0)
    {
        //Employer Reminder Letter (Inspection)
        $data = $this->dataCLIEMPCLOSURE($letter);
        return view("backend/letters/compliance/CLIEMPCLOSURE/template")
        ->with("employer", $data['employer'])
        ->with("closure", $data['closure'])
        ->with("letter_reference", $data['letter_reference'])
        ->with("letter", $letter)
        ->with("print", $print);
    }



    /*Resource Client Employer closure Extensions*/
    public function resourceCLIEMPCLREXT($resource, $option = NULL)
    {
        //Employer closure extension Letter (Closure)
        $codeValueRepo = new CodeValueRepository();
        $extension_repo = new EmployerClosureExtensionRepository();
        $extension = $extension_repo->find($resource);
        $folioNumber = $extension->getLetterFolioNoAttribute('CLIEMPCLREXT');
        $reference = $extension->getLetterReferenceNoAttribute('CLIEMPCLREXT');
        $name = $reference;
        $employer = $extension->employerClosure->closedEmployer;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/compliance/employer_closure/extension/profile/{$extension->id}";
        //Inspection Notice Letter Issued
        $refLetter = $this->getLetterByReference($codeValueRepo->findIdByReference('CLIEMPCLREXT'), $extension->id); //Inspection Notice Letter....
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => $profile_url],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
                ["value" => $extension->employerClosure->close_type_name, "title" => "De-Registration Extension", "url" => $profile_url],
                ["value" => $extension->user->username, "title" => "Initiator", "url" => $profile_url],
            ],
            "extras" => [
//                ['name' => 'noticereference', 'title' => 'Inspection Notice Letter Reference', 'data_type' => 'text', 'value' => ($refLetter) ? $refLetter->reference : ""],
//                ['name' => 'noticedate', 'title' => 'Inspection Notice Letter Date', 'data_type' => 'date', 'value' =>  ($refLetter) ? $refLetter->letter_date_label : ""],
//                ['name' => 'employername', 'title' => 'Employer Name', 'data_type' => 'text', 'value' => $employer->name],
            ],
        ];
    }

    /*Data client employer closure extension*/
    public function dataCLIEMPCLREXT(Model $letter)
    {
        //Employer Closure Letter (Closure)
        $extension_repo = new EmployerClosureExtensionRepository();
        $extension = $extension_repo->find($letter->resource_id);
        $employer = $extension->employerClosure->closedEmployer;

        $letter_ref =   $extension->getLetterReferenceAttribute($letter->lang);
        return [
            'letter_reference' => $letter_ref,
            'employer' => $employer,
            'extension' => $extension,
            'closure' => $extension->employerClosure
        ];
    }

    /*Preview Client closure*/
    public function previewCLIEMPCLREXT(Model $letter, $print = 0)
    {
        //Employer Reminder Letter (Inspection)
        $data = $this->dataCLIEMPCLREXT($letter);
        return view("backend/letters/compliance/CLIEMPCLREXT/template")
        ->with("employer", $data['employer'])
        ->with("closure", $data['closure'])
        ->with("extension", $data['extension'])
        ->with("letter_reference", $data['letter_reference'])
        ->with("letter", $letter)
        ->with("print", $print);
    }





    /*Resource booking interest adjustment*/
    public function resourceCLIEMPINTEADJ($resource, $option = NULL)
    {

        $codeValueRepo = new CodeValueRepository();
        $interest_adj_repo = new InterestAdjustmentRepository();
        $interest_adjustment = $interest_adj_repo->find($resource);
        $folioNumber = $interest_adj_repo->getLetterFolioNo('CLIEMPINTEADJ');
        $reference = $interest_adjustment->getLetterReferenceNoAttribute('CLIEMPINTEADJ');
        $name = $reference;
        $employer = $interest_adjustment->receipt->employer;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/compliance/interest_adjustment/profile/{$interest_adjustment->id}";
        //Inspection Notice Letter Issued
        $refLetter = $this->getLetterByReference($codeValueRepo->findIdByReference('CLIEMPINTEADJ'), $interest_adjustment->id); //Inspection Notice Letter....
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => $profile_url],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
                ["value" => ('Receipt: '. $interest_adjustment->receipt->rctno), "title" => "Interest Adjustment", "url" => $profile_url],
                ["value" => $interest_adjustment->user->username, "title" => "Initiator", "url" => $profile_url],
            ],
            "extras" => [
//                ['name' => 'noticereference', 'title' => 'Inspection Notice Letter Reference', 'data_type' => 'text', 'value' => ($refLetter) ? $refLetter->reference : ""],
//                ['name' => 'noticedate', 'title' => 'Inspection Notice Letter Date', 'data_type' => 'date', 'value' =>  ($refLetter) ? $refLetter->letter_date_label : ""],
//                ['name' => 'employername', 'title' => 'Employer Name', 'data_type' => 'text', 'value' => $employer->name],
            ],
        ];
    }

    /*Data client employer closure extension*/
    public function dataCLIEMPINTEADJ(Model $letter)
    {
        //Employer Closure Letter (Closure)
        $interest_adj_repo = new InterestAdjustmentRepository();
        $interest_adjustment = $interest_adj_repo->find($letter->resource_id);
        $employer = $interest_adjustment->receipt->employer;

        $letter_ref =   $interest_adjustment->getLetterReferenceAttribute($letter->lang);
        return [
            'letter_reference' => $letter_ref,
            'employer' => $employer,
            'interest_adjustment' => $interest_adjustment,
            'receipt' => $interest_adjustment->receipt,

        ];
    }

    /*Preview Client closure*/
    public function previewCLIEMPINTEADJ(Model $letter, $print = 0)
    {
        //Employer Reminder Letter (Inspection)
        $data = $this->dataCLIEMPINTEADJ($letter);
        return view("backend/letters/compliance/CLIEMPINTEADJ/template")
        ->with("employer", $data['employer'])
        ->with("receipt", $data['receipt'])
        ->with("interest_adjustment", $data['interest_adjustment'])
        ->with("letter_reference", $data['letter_reference'])
        ->with("letter", $letter)
        ->with("print", $print);
    }

    public function resourceCLIEMPPAYROLLMRG($resource, $option = NULL)
    {
        $codeValueRepo = new CodeValueRepository();
        $employer_payroll_merge_repo = new MergeDeMergeEmployerPayrollRepository();
        $merge_request = $employer_payroll_merge_repo->find($resource);
        $folioNumber = 'PMERGE.123/231/';
        $reference = $resource;
        $name = $reference;
        $employer = $merge_request->employer;
        $recipient = $employer->name;
        $box_no = $employer->box_no;
        $location = $employer->location_formatted;
        $profile_url = "/compliance/employer/payroll_merge/profile/{$merge_request->id}";
        $refLetter = $this->getLetterByReference($codeValueRepo->findIdByReference('CLIEMPPAYROLLMRG'), $merge_request->id);
        return [
            "name" => $name,
            "salutation" => $employer->letterSalutation(),
            "reference" => $reference,
            "recipient" => $recipient,
            "box_no" => $box_no,
            "location" => $location,
            "profile_url" => $profile_url,
            "resources" => [
                ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
                ["value" => $name, "title" => "Profile", "url" => $profile_url],
                ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
                ["value" => 'Employer Payroll Merging', "title" => "Closure Type", "url" => $profile_url],
                ["value" => $merge_request->user->name, "title" => "Initiator", "url" => $profile_url],
            ],
            "extras" => [],
        ];
    }

    /*Data Payroll Merge*/
    public function dataCLIEMPPAYROLLMRG(Model $letter)
    {
        //Employer Closure Letter (Closure)
        $employer_payroll_merge_repo = new MergeDeMergeEmployerPayrollRepository();
        $merge_request = $employer_payroll_merge_repo->find($letter->resource_id);
        $employer = $merge_request->employer;
        $merge_type = $employer_payroll_merge_repo->mergeType($merge_request);
        $letter_ref = 'MERGING MULTIPLE PAYROLLS';  
        //$merge_request->getLetterReferenceAttribute($letter->lang); fanyia kazi

        $email_array =[];
        foreach ($merge_request->merged_users_emails as $key => $value) {
          array_push($email_array, $value['email']);
      }
      
      $email_string = '';
      $last_key = end(array_keys(array_unique($email_array)));
      foreach(array_unique($email_array) as $key => $value){
        $email_string .= $value.',';
    }

    return [
        'letter_reference' => $letter_ref,
        'employer' => $employer,
        'merge_request' => $merge_request,
        'merged_emails' => $email_string.'and '.$merge_request->keep_user_email,
        'keep_email' => $merge_request->keep_user_email,
        'reference' => $merge_request->letter_reference,
        'letter_request_date' => $merge_request->letter_date,
        'merge_type' => $merge_type,
    ];
}

/*Preview Payroll Merge*/
public function previewCLIEMPPAYROLLMRG(Model $letter, $print = 0)
{
    $data = $this->dataCLIEMPPAYROLLMRG($letter);
    return view("backend/letters/compliance/CLIEMPPAYROLLMRG/template")
    ->with("employer", $data['employer'])
    ->with("merge_request", $data['merge_request'])
    ->with("letter_reference", $data['letter_reference'])
    ->with("merged_emails", $data['merged_emails'])
    ->with("keep_email", $data['keep_email'])
    ->with("reference", $data['reference'])
    ->with("letter_request_date", $data['letter_request_date'])
    ->with("letter", $letter)
    ->with("merge_type", $merge_type)
    ->with("print", $print);
}   

/*Preview Contrib Modification*/
public function resourceEMPCONTRIBMOD($resource, $option = NULL)
{
    $codeValueRepo = new CodeValueRepository();
    $contrib_modification_repo = new ContributionModificationRepository();
    $modification_request = $contrib_modification_repo->find($resource);
    $folioNumber = 'PMERGE.123/231/';
    $reference = $resource;
    $name = $reference;
    $employer = $modification_request->employer;
    $recipient = $employer->name;
    $box_no = $employer->box_no;
    $location = $employer->location_formatted;
    $profile_url = "/compliance/employer/contrib_modification/profile/{$modification_request->id}";
    $refLetter = $this->getLetterByReference($codeValueRepo->findIdByReference('EMPCONTRIBMOD'), $modification_request->id);
    return [
        "name" => $name,
        "salutation" => $employer->letterSalutation(),
        "reference" => $reference,
        "recipient" => $recipient,
        "box_no" => $box_no,
        "location" => $location,
        "profile_url" => $profile_url,
        "resources" => [
            ["value" => $folioNumber, "title" => "Folio", "url" => "#"],
            ["value" => $name, "title" => "Profile", "url" => $profile_url],
            ["value" => $employer->name, "title" => "Employer", "url" => "/compliance/employer/profile/{$employer->id}"],
            ["value" => 'Employer Contribution Modification', "title" => "Closure Type", "url" => $profile_url],
            ["value" => $modification_request->user->name, "title" => "Initiator", "url" => $profile_url],
        ],
        "extras" => [],
    ];
}

/*Data contrib modification*/
public function dataEMPCONTRIBMOD(Model $letter)
{
        //Employer Closure Letter (Closure)
    $contrib_modification_repo = new ContributionModificationRepository();
    $modification_request = $contrib_modification_repo->find($letter->resource_id);
    $employer = $modification_request->employer;
    $letter_ref = 'MODIFICATION OF CONTRIBUTION';  
        //$modification_request->getLetterReferenceAttribute($letter->lang); fanyia kazi
    return [
        'letter_reference' => $letter_ref,
        'employer' => $employer,
        'modification_request' => $modification_request,
    ];
}

/*Preview Contrib modification*/
public function previewEMPCONTRIBMOD(Model $letter, $print = 0)
{
    $data = $this->dataEMPCONTRIBMOD($letter);
    return view("backend/letters/compliance/EMPCONTRIBMOD/template")
    ->with("employer", $data['employer'])
    ->with("modification_request", $data['modification_request'])
    ->with("letter_reference", $data['letter_reference'])
    ->with("letter", $letter)
    ->with("print", $print);
}




}