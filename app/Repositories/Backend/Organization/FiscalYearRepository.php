<?php

namespace App\Repositories\Backend\Organization;

use App\Exceptions\GeneralException;
use App\Models\Organization\FiscalYear;
use App\Models\Sysdef\CodeValue;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class FiscalYearRepository extends BaseRepository
{
    const MODEL = FiscalYear::class;


    //find or throw exception for Fiscal Year
    public function findOrThrowException($id)
    {
        $fiscal_year = $this->query()->find($id);

        if (!is_null($fiscal_year)) {
            return $fiscal_year;
        }
        throw new GeneralException(trans('exceptions.backend.organization.fiscal_year_not_found'));
    }




    public function store($input)
    {
        $end_year = $input['start_year'] + 1;
        $fiscal_year = $this->query()->create([
            'start_year' => $input['start_year'],
            'end_year' => $end_year,
            'fiscal_year' => $input['start_year'] . '/' . $end_year  ,
            'user_id' => access()->id()
        ]);
        return $fiscal_year;
    }

    public function create($input)
    {
        return DB::transaction(function () use ($input) {
            $fiscal_year = $this->store($input);
            foreach ($input as $key => $value) {
                if (strpos($key, 'target_total') !== false) {
                    $target_type_id = substr($key, 12);
                    if ($input['target_total' . $target_type_id]) {
                        $total = str_replace(",", "", $input['target_total' . $target_type_id]);
                        $fiscal_year->annualTargetTypes()->syncWithoutDetaching([ $target_type_id => ['target_total'
                        => $total]]);

                    }
                }
            }
        });
    }


    public function update($id,$input)
    {
        $fiscal_year = $this->findOrThrowException($id);
        $end_year = $input['start_year'] + 1;
        $fiscal_year->update([
            'start_year' => $input['start_year'],
            'end_year' => $end_year,
            'fiscal_year' => $input['start_year'] . '/' . $end_year  ,
        ]);
        return $fiscal_year;
    }


    public function edit($id,$input)
    {
        return DB::transaction(function () use ($id, $input) {
            $fiscal_year = $this->update($id, $input);
            foreach ($input as $key => $value) {
                if (strpos($key, 'target_total') !== false) {
                    $target_type_id = substr($key, 12);
                    if ($input['target_total' . $target_type_id]) {
                        $total = str_replace(",", "", $input['target_total' . $target_type_id]);
                        $fiscal_year->annualTargetTypes()->syncWithoutDetaching([ $target_type_id => ['target_total'
                        => $total]]);
                    }
                }
            }
        });
    }


    public function findByFiscalYear($fiscal_year)
    {
        $fiscal_year = $this->query()->where('fiscal_year', $fiscal_year)->first();
        return $fiscal_year;
    }


    //Get for dataTable
    public function getForDataTable() {
        return $this->query();
    }


    /**
     * @param $date
     * @return array
     * Get which fiscal year specified date belong
     */
    public function getFiscalYearRangeByDate($date){
        $month = Carbon::parse($date)->format('m');
        $year = Carbon::parse($date)->format('Y');
        $end_date = null;
        $start_date = null;
        /*01-06*/
        if($month >= 1 && $month <= 6){
            $start_date  = ($year - 1) .'-07-01';
            $end_date  = ($year) .'-06-30';
        }elseif($month >= 7 && $month <= 12){
            $start_date  = ($year) .'-07-01';
            $end_date  = ($year + 1) .'-06-30';

        }

        return ['start_date' => $start_date, 'end_date' => $end_date];
    }



    /*Monthly target by type reference*/
    public function getMonthlyTargetByTypeReference($start_year, $target_type_ref)
    {
        $target_type = CodeValue::query()->where('reference', $target_type_ref)->first();
        $fiscal_year = $this->query()->where('start_year', $start_year)->first();
        $annual_target = $fiscal_year->annualTargetTypes()->where('annual_target_type_cv_id', $target_type->id)->first();
        $monthly_target = null;
        if($annual_target){
            $monthly_target = ($annual_target->pivot->target_total) / 12;
        }

        return $monthly_target;
    }



    /*Days target by type reference*/
    public function getDaysTargetByTypeReference($start_year, $target_type_ref, $days)
    {
        $target_type = CodeValue::query()->where('reference', $target_type_ref)->first();
        $fiscal_year = $this->query()->where('start_year', $start_year)->first();
        $fiscal_year = isset($fiscal_year) ? $this->query()->where('start_year', $start_year)->first() : $this->autoCreateFinYear($start_year);
        $annual_target = $fiscal_year->annualTargetTypes()->where('annual_target_type_cv_id', $target_type->id)->first();
        $days_target = null;
        if($annual_target){
            $days_target = ($annual_target->pivot->target_total) * $days/365;
        }

        return $days_target;
    }

    /*Auto create this fin year*/
    public function autoCreateFinYear($start_year)
    {
        $end_year = $start_year + 1;
        $check = $this->checkIfExists($start_year);
        $input = [];
        $fin_year = null;
        if($check == false)
        {
            $input['start_year'] = $start_year;
            $input['end_year'] = $end_year;
            $input['fiscal_year'] = $start_year . '/' . $end_year;

            $fin_year =  $this->store($input);

        }
        return $fin_year;
    }


    public function checkIfExists($start_year)
    {
        $check = $this->query()->where('start_year', $start_year)->count();
        if($check > 0){
            return true;
        }else{
            return false;
        }
    }

}