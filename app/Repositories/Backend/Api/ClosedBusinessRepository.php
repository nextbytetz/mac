<?php

namespace App\Repositories\Backend\Api;

use App\Models\Api\ClosedBusiness;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClosedBusinessRepository extends BaseRepository
{
    const MODEL = ClosedBusiness::class;

    public function getForDataTable() {
        $list = $this->query()->select([
            'id',
            'firstname',
            'middlename',
            'lastname',
            'taxpayername',
            'tradingname',
            'tin',
            'deregistrationdate',
        ])->where("isassociated", 0);
        return $list;
    }

    public function associateEmployer(Model $model, array $input)
    {
        DB::transaction(function () use ($input, $model) {
            $model->employer_id = $input['employer'];
            $model->user_id = access()->id();
            $model->save();
            return true;
        });
    }

    public function deleteClosedBusinessTRA(array $input)
    {
        DB::transaction(function () use ($input) {
            if (isset($input['id']) And ($input['id'])) {
                //soft deleting the closed businesses
                $closedbusinesses = $this->query()->find($input['id']);
                foreach ($closedbusinesses as $closedbusiness) {
                    $closedbusiness->user_closed = access()->id();
                    $closedbusiness->save();
                    $closedbusiness->delete();
                }
                //$this->query()->destroy($input['id']);
                return true;
            } else {
                return true;
            }
        });
    }

}