<?php

namespace App\Repositories\Backend\Api;

use App\Models\Api\NewTaxpayer;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class NewTaxpayerRepository extends BaseRepository
{
    const MODEL = NewTaxpayer::class;

    private $unregistered;

    public function __construct()
    {
        parent::__construct();
        $this->unregistered = new UnregisteredEmployerRepository();
    }

    public function getForDataTable() {
        $list = $this->query()->select([
            'id',
            'firstname',
            'middlename',
            'lastname',
            'taxpayername',
            'tradingname',
            'tin',
            'dateofregistration',
        ])->where("istaken", 0);
        return $list;
    }

    public function moveToUnregistered(array $input)
    {
        DB::transaction(function () use ($input) {
            if (isset($input['id']) And ($input['id'])) {

                $newtaxpayers = $this->query()->find($input['id']);

                foreach ($newtaxpayers as $newtaxpayer) {
                    $this->unregistered->query()->create([
                        'name' => $newtaxpayer->name,
                        'doc' => $newtaxpayer->dateofregistration,
                        'tin' => $newtaxpayer->tin,
                        'district_id' =>  NULL,
                        'region_id' =>  NULL,
                        'address' => $newtaxpayer->postaladdress,
                        'location_type_id' =>  1,
                        'unsurveyed_area' => NULL,
                        'street' => $newtaxpayer->street,
                        'road' => NULL,
                        'plot_number' => $newtaxpayer->plotnumber,
                        'block_no' => $newtaxpayer->blocknumber,
                        'surveyed_extra' => NULL,
                        'phone' => $newtaxpayer->mobile,
                        'telephone' => $newtaxpayer->telephone1,
                        'email' => $newtaxpayer->email,
                        'fax' => $newtaxpayer->fax,
                        'business_activity' => $newtaxpayer->businessactivitiesdescription,
                        'sector' =>  $newtaxpayer->businesssectordescription,
                        'no_of_employees' => $newtaxpayer->numberofemployees,
                        'user_id' => access()->id(),
                        'new_taxpayer_id' => $newtaxpayer->id,
                    ]);

                    $newtaxpayer->istaken = 1;
                    $newtaxpayer->user_id = access()->id();
                    $newtaxpayer->save();

                }
            }
        });
    }

}