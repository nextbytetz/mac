<?php

namespace App\Repositories\Backend\Api;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use function \FluidXml\fluidxml;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException as ConnectException;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;


class GotIntegrationRepository extends BaseRepository
{
    //const MODEL = NewTaxpayer::class;

  

  public function __construct()
  {
   //
  }

  public function employeesHeaderGot()
  {


    $token_array = $this->getMofToken();

    $period=$this->returnNextCheckdate();
    $pagesize=1000;
    $request = Request();
    try{
      if (!empty($token_array['access_token'])) {

       $client = new Client();
       $url = config('constants.MOF.BASE_URL').config('constants.MOF.EMPLOYEES_HEADER_URL').$period.'&pageSize='.$pagesize;
       $request = $client->request('GET', $url, [
        'headers' => [
          'token' =>$token_array['access_token'],
        ]
      ]);

       $response = $request->getBody()->getContents();
       $xml = simplexml_load_string($response);
       $json = json_decode(json_encode($xml),true);

       for ($i=0; $i < $json['TotalPages']; $i++) { 
        $this->employeesBioData($period,($i+1),$pagesize, $token_array);
      }
    } else {
      dump('No token');
    }

  }
  catch( ConnectException $e){
    Log::info('---- Connection error employees-----');
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch (ClientError $e) {
    Log::info('---------Client error employees-----');
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch (\Error $e) {
    Log::info('---------General error employees-----');
    Log::info($e);
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch( \ Exception $e){
    Log::info('---------Exception error employees -----');
    Log::info($e);
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }

}



public function employeesBioData($period,$pagenumber,$pagesize,$token_array)
{
 $request = Request();
 try{
  $client = new Client();

  $url = config('constants.MOF.BASE_URL').config('constants.MOF.EMPLOYEES_BIODATA_URL').$period.'&pageNo='.$pagenumber.'&pageSize='.$pagesize;
  $request = $client->request('GET', $url, [
    'headers' => [
      'token' =>$token_array['access_token'],
    ]
  ]);

  $response = $request->getBody()->getContents();
  $xml = simplexml_load_string($response);
  $json = json_decode(json_encode($xml),true);

      // dd($json);

  for ($i=0; $i < $pagesize; $i++) { 
   $sex = [1,2];
   $key = array_rand($sex);
   $emp_cate = [84,85,86];
   $key_cat = array_rand($emp_cate);

   $input = [
    'CheckNumber' => $json['Employee'][$i]['CheckNumber'],
    'firstname' => empty($json['Employee'][$i]['FirstName']) ?  null: strtoupper(trim($json['Employee'][$i]['FirstName'])),
    'middlename' => empty($json['Employee'][$i]['MiddleName']) ?  null: strtoupper(trim($json['Employee'][$i]['MiddleName'])),
    'lastname' => empty($json['Employee'][$i]['LastName']) ?  null: strtoupper(trim($json['Employee'][$i]['LastName'])),
    'VoteCode' => empty($json['Employee'][$i]['VoteCode']) ?  null: strtoupper(trim($json['Employee'][$i]['VoteCode'])),
    'DeptCode' => empty($json['Employee'][$i]['DeptCode']) ?  null: strtoupper(trim($json['Employee'][$i]['DeptCode'])),
    'DateHired' => Carbon::parse($json['Employee'][$i]['DateHired'])->format('Y-m-d'),
    'dob' => Carbon::parse($json['Employee'][$i]['DateHired'])->format('Y-m-d'),
    'PayGrade' => empty($json['Employee'][$i]['PayGrade']) ?  null: strtoupper(trim($json['Employee'][$i]['PayGrade'])),
    'PayStep' => empty($json['Employee'][$i]['PayStep']) ?  null: strtoupper(trim($json['Employee'][$i]['PayStep'])),
    'SalaryScale' => empty($json['Employee'][$i]['SalaryScale']) ?  null: strtoupper(trim($json['Employee'][$i]['SalaryScale'])),
    'emp_cate' => empty($json['Employee'][$i]['EmploymentStatus']) ?  null: strtoupper(trim($json['Employee'][$i]['EmploymentStatus'])),
    'job_title' => empty($json['Employee'][$i]['Designation']) ?  null: strtoupper(trim($json['Employee'][$i]['Designation'])),
    'national_id' => null,
    'basicpay' => null,
    'grosspay' => null,
    'sex' => $sex[$key],
    'employee_category_cv_id' => $emp_cate[$key_cat],
    'page_number' => $pagenumber,
  ];

  DB::transaction(function () use ($input) {
    $this->generateMemberNumber($input);
  });
}
}
catch( ConnectException $e){
  Log::info('---- Connection error employees details-----');
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}
catch (ClientError $e) {
  Log::info('---------Client error employees details -----');
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}
catch (\Error $e) {
  Log::info('---------General error employees details -----');
  Log::info($e);
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}
catch( \ Exception $e){
  Log::info('---------Exception error employees details -----');
  Log::info($e);
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}

}



public function generateMemberNumber($input){ 
       // Log::info('-------------');
       // Log::info($input);
       // Log::info('About to save');
 $employee_exist = DB::table('main.employees')->where('firstname', $input['firstname'])
 ->where('CheckNumber',  $input['CheckNumber'])->first(); 

 if(empty($employee_exist)){
  $registeredEmployeeId =  DB::table('main.employees')->insertGetId([
    'firstname' => $input['firstname'],
    'middlename' => $input['middlename'],
    'lastname' => $input['lastname'],
    'dob' => $input['dob'],
    'gender_id' =>$input['sex'] ,
    'nid' =>$input['national_id'],
    'emp_cate' => $input['emp_cate'],
    'VoteCode' => $input['VoteCode'],
    'DeptCode' => isset($input['DeptCode']) ?  $input['DeptCode'] : null,
    'PayGrade' => isset($input['PayGrade']) ?  $input['PayGrade'] : null,
    'PayStep' => isset($input['PayStep']) ?  $input['PayStep'] : null,
    'SalaryScale' => isset($input['SalaryScale']) ?  $input['SalaryScale'] : null,
    'CheckNumber' => $input['CheckNumber'],
    'job_title'=>$input['job_title'],
    'FundingSource' => $input['FundingSource'],
    'basicpay'=> $input['basicpay'],
    'grosspay'=> $input['grosspay'],
    'memberno' => 0,
    'created_at' => Carbon::now(),
  ]);

  $memberno = checksum($registeredEmployeeId, sysdefs()->data()->employee_number_length);
  DB::table('main.employees')->where('id', $registeredEmployeeId)->update([
    'memberno' => $memberno,
    'updated_at' => Carbon::now(),
  ]);

  $input['memberno'] = $memberno;
  $input['employee_id'] = $registeredEmployeeId;  
  Log::info('Saved and updated');
} else {
  $input['memberno'] = $employee_exist->memberno;
  $input['employee_id'] = $employee_exist->id;
}
$this->saveEmployeeEmployer($input);


}


public function saveEmployeeEmployer($input){
  $employer = DB::table('main.employers')->where('vote',  $input['VoteCode'])->first();

  if($employer){
    $employee_exist = DB::table('main.employee_employer')
    ->where('employer_id', $employer->id)
    ->where('employee_id',  $input['employee_id'])
    ->whereNull('deleted_at')->first();

    if (empty($employee_exist)) {
      DB::table('main.employee_employer')
      ->insert([ 
        'employee_id' =>  $input['employee_id'],
        'employer_id' =>  $employer->id,
        'basicpay' => $input['basicpay'],
        'grosspay' => $input['grosspay'], 
        'employee_category_cv_id' => $input['employee_category_cv_id'], 
        'job_title' => $input['job_title'], 
        'updated_at' => Carbon::now(),
        'created_at' => Carbon::now(),
        'page_number' => $input['page_number'],
      ]); 
    } else {
      DB::table('main.employee_employer')
      ->where('id',$employee_exist->id)
      ->limit(1)
      ->update([ 
        'basicpay' => $input['basicpay'],
        'grosspay' => $input['grosspay'], 
        'employee_category_cv_id' => $input['employee_category_cv_id'], 
        'job_title' =>  isset($input['job_title']) ? $input['job_title'] : $input['CheckNumber'], 
        'updated_at' => Carbon::now(),
        'created_at' => Carbon::now(),
        'page_number' => $input['page_number'],
      ]);
    }
  }

}

public function contributionHeader()
{


  try{
    $token_array = $this->getMofToken();
    if (!empty($token_array['access_token'])) {

         //dd($token_array);
     $period=$this->returnNextCheckdate();
     $pagesize=1000;
     $request = Request();

     $client = new Client();

     $headers = [
      'Authorization' => 'Bearer ' . $token_array['access_token'],        
      'Accept'        => 'application/json',
    ];

    $url = config('constants.MOF.BASE_URL').config('constants.MOF.CONTRIBUTION_HEADER_URL').$period.'&pageSize='.$pagesize;
    $response = $client->request('GET', $url, [
      'headers' => $headers,
    ]);

    $response = $response->getBody()->getContents();
    $json = json_decode($response,true);

    dump('save contributions per page');

    $contrib_summary_id = $this->saveOverallSummary($json, $period);
    dump($contrib_summary_id);

    for ($i=0; $i < $json['totalPages']; $i++) {
    // for ($i=463; $i <$json['totalPages']; $i++) {  
      $page = $i+1;
      dump('Page '.$page);
      $this->contributionHeaderDetails($period,$page,$pagesize,$contrib_summary_id, $token_array);
    }

  } else {
    dump('Token not included');
  }

}
catch( ConnectException $e){
  Log::info('---- Connection error header-----');
  Log::error(print_r($e->getMessage(),true));
      // $this->saveFailedPages($page, $contrib_summary_id, $e->getCode().' Connection Error', $e->getMessage());

  Log::info('-------------');
}
catch (ClientError $e) {
  Log::info('---------Client error header-----');
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}
catch (\Error $e) {
  Log::info('---------General error header-----');
  Log::info($e);
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}
catch( \ Exception $e){
  Log::info('---------Exception error header -----');
  Log::info($e);
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}

}

public function contributionHeaderDetails($period,$pagenumber,$pagesize, $overall_summary_id, $token_array)
{
 $request = Request();
 try{
  $client = new Client();

  $headers = [
    'Authorization' => 'Bearer ' . $token_array['access_token'],        
    'Accept'        => 'application/json',
  ];

  $url = config('constants.MOF.BASE_URL').config('constants.MOF.CONTRIBUTION_DETAILS_URL').$period.'&pageNo='.$pagenumber.'&pageSize='.$pagesize;

  $request = $client->request('GET', $url, [
    'headers' => $headers,
  ]);

  $response = $request->getBody()->getContents();
  $json = json_decode($response,true);
  Log::info(print_r($json,true));
  for ($i=0; $i < count($json); $i++) { 
        // Log::info('Page '.$pagenumber.' Employee '.($i+1));
    dump('Page '.$pagenumber.' Employee '.($i+1));
        // Log::info(print_r($json['Employee'][$i],true));

    $fund = empty($json[$i]['fundingSource']) ?  null: strtoupper(trim($json[$i]['fundingSource']));

    switch ($fund) {
      case 'GF':
      $funding_source = 'GOVERNMENT FUNDED';
      break;
      case 'GOVERNMENT FUNDED':
      $funding_source = 'GOVERNMENT FUNDED';
      break;
      case 'GOVERNMENT':
      $funding_source = 'GOVERNMENT FUNDED';
      break;
      case 'CF':
      $funding_source = 'COUNCIL FUNDED';
      break;
      case 'COUNCIL FUNDED':
      $funding_source = 'COUNCIL FUNDED';
      break;
      case 'COUNCIL':
      $funding_source = 'COUNCIL FUNDED';
      break;
      default:
      $funding_source = $fund;
      break;
    }


    $input = [
      'check_number' => $json[$i]['checkNumber'],
      'firstname' => empty($json[$i]['firstName']) ?  null: strtoupper(trim($json[$i]['firstName'])),
      'middlename' => empty($json[$i]['middleName'])  ?  null: strtoupper(trim($json[$i]['middleName'])),
      'lastname' => empty($json[$i]['lastName']) ?  null: strtoupper(trim($json[$i]['lastName'])),
      'vote_code' => empty($json[$i]['voteCode']) ?  null: strtoupper(trim($json[$i]['voteCode'])),
      'vote_name' => empty($json[$i]['voteName']) ?  null: strtoupper(trim($json[$i]['voteName'])),
      'grosspay' => empty($json[$i]['monthlySalary']) ?  0: strtoupper(trim($json[$i]['monthlySalary'])),
      'employee_contribution' => empty($json[$i]['employeeContribution']) ?  0: strtoupper(trim($json[$i]['employeeContribution'])),
      'employer_contribution' => empty($json[$i]['employerContribution']) ?  0: strtoupper(trim($json[$i]['employerContribution'])),
      'funding_source' => $funding_source,
      'check_date' => $period,
      'treasury_contribution_summary_id' => $overall_summary_id,
      'page_number' => $pagenumber,
    ];

    DB::transaction(function () use ($input) {
      $this->saveContributionDetails($input);
    });


  }

  $this->updateOverallSummary($period,'pages');

}
catch( ConnectException $e){
  Log::info('---- Connection error -----');
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}
catch (ClientError $e) {
  Log::info('---------Client error -----');
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}
catch (\Error $e) {
  Log::info('---------General error -----');
  Log::info($e);
  Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}
catch( \ Exception $e){
  Log::info('---------Exception error -----');
  Log::info($e);
  // Log::error(print_r($e->getMessage(),true));
  Log::info('-------------');
}

}

public function saveContributionDetails($input){ 
        // Log::info(' deails');
 $detail_exist = DB::table('main.treasury_employees_contribution')
 ->where('check_number',  $input['check_number'])
 ->where('check_date',  $input['check_date'])
 ->where('vote_code',  $input['vote_code'])
 ->first();

 if (empty($detail_exist)) {
            // Log::info('save emp details');
  DB::table('main.treasury_employees_contribution')->insert([
   'check_number' => $input['check_number'],
   'firstname' => $input['firstname'],
   'middlename' => $input['middlename'],
   'lastname' => $input['lastname'],
   'vote_code' => $input['vote_code'],
   'vote_name' => $input['vote_name'],
   'grosspay' => $input['grosspay'],
   'employee_contribution' => $input['employee_contribution'],
   'employer_contribution' => $input['employer_contribution'],
   'funding_source' => $input['funding_source'],
   'check_date' => $input['check_date'],
   'treasury_contribution_summary_id' => $input['treasury_contribution_summary_id'],
   'created_at' => Carbon::now(),
   'updated_at' => Carbon::now(),
   'page_number' => $input['page_number'],
 ]);

  $this->updateOverallSummary($input['check_date'],'employees');

} else {
  DB::table('main.treasury_employees_contribution')
  ->where('id',  $detail_exist->id)
  ->update([
   'grosspay' => $input['grosspay'],
   'employer_contribution' => $input['employer_contribution'],
   'employer_contribution' => $input['employer_contribution'],
   'funding_source' => $input['funding_source'],
   'updated_at' => Carbon::now(),
   'page_number' => $input['page_number'],
 ]);

  $this->updateOverallSummary($input['check_date'],'employees');
}

}



public function saveContributionSummary(){

 $param=DB::table('main.treasury_contribution_summary')->select('id','check_date')->orderBy('id', 'desc')->limit(1)->get();
     // dump($param[0]->check_date);
     //  dump($param[0]->id);
     // die;
 $funds=['GOVERNMENT FUNDED','COUNCIL FUNDED'];
 $period=$param[0]->check_date;
 $treasury_contribution_summary_id=$param[0]->id;

 foreach ( $funds as $fund) {
   $employees_count = DB::table('main.treasury_employees_contribution')
   ->select('check_number')
   ->where('check_date',  $period)
   ->where('funding_source', $fund)
   ->count();

   $total_grosspay = DB::table('main.treasury_employees_contribution')
   ->select('grosspay')
   ->where('funding_source', $fund)
   ->where('check_date',  $period)->sum('grosspay');

   $total_contribution = DB::table('main.treasury_employees_contribution')
   ->select('employer_contribution')
   ->where('funding_source', $fund)
   ->where('check_date',  $period)->sum('employer_contribution');

   $vote_count = DB::table('main.treasury_employees_contribution')
   ->distinct('vote_code')
   ->where('funding_source', $fund)
   ->where('check_date',  $period)->count('vote_code');

   $calculated_contrib = $total_grosspay*0.005;

   $diff = $calculated_contrib - $total_contribution;


   if($diff < 1000){

    $summary_exist = DB::table('main.treasury_funding_summary')
    ->where('check_date',  $period)
    ->where('funding_source', $fund)
    ->first();

    if(empty($summary_exist)){
     DB::table('main.treasury_funding_summary')->insert([
      'total_votes' => $vote_count,
      'total_employees' => $employees_count,
      'total_grosspay' => $total_grosspay,
      'total_contribution_amount' => $total_contribution,
      'check_date' => $period,
      'funding_source' => $fund,
      'treasury_contribution_summary_id' => $treasury_contribution_summary_id,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
   }else{
     DB::table('main.treasury_funding_summary')
     ->where('check_date',  $period)
     ->where('id',$summary_exist->id)
     ->update([
      'total_votes' => $vote_count,
      'total_employees' => $employees_count,
      'total_grosspay' => $total_grosspay,
      'total_contribution_amount' => $total_contribution,
      'check_date' => $period,
      'funding_source' => $fund,
      'updated_at' => Carbon::now(),
    ]);
   }

   dump('saved summary '.$fund);
   $this->saveVoteSummary($period);
 }else{
            //there is a difference of more than 1000
  dump('diff kubwa');
}

}




}


public function saveVoteSummary($period)
{
 $votes = DB::table('main.treasury_employees_contribution')
 ->select('vote_code')
 ->distinct('vote_code')
 ->where('check_date',  $period)->get();


 foreach ($votes as $vote) {
   $this->saveVoteFundingSummary($vote->vote_code,$period,'GOVERNMENT FUNDED');
   $this->saveVoteFundingSummary($vote->vote_code,$period,'COUNCIL FUNDED');
 }

 dump('votes summary saved');
}



public function saveVoteFundingSummary($vote,$period,$funding_source)
{

  Log::info('Vote : '.$vote.' funding_source '.$funding_source);
  dump('Vote : '.$vote.' funding_source '.$funding_source);

  $e_count = DB::table('main.treasury_employees_contribution')
  ->select('check_number')
  ->where('check_date',  $period)
  ->where('funding_source',$funding_source)
  ->where('vote_code', $vote)
  ->count();

  $t_grosspay = DB::table('main.treasury_employees_contribution')
  ->select('grosspay')
  ->where('check_date',  $period)
  ->where('funding_source',$funding_source)
  ->where('vote_code', $vote)->sum('grosspay');

  $t_contribution = DB::table('main.treasury_employees_contribution')
  ->select('employer_contribution')
  ->where('funding_source',$funding_source)
  ->where('check_date',  $period)
  ->where('vote_code', $vote)->sum('employer_contribution');

  $c_contrib = $t_grosspay*0.005;


  if ($t_contribution > 0) {
   $summary_exist = DB::table('main.treasury_funding_summary')
   ->where('check_date',  $period)
   ->where('funding_source', $funding_source)
   ->first();
   dump('now saving vote '.$vote.' empcount: '.$e_count.' grosspay '.$t_grosspay.' contribution '.$t_contribution.' vs cal '.$c_contrib);

   if(count($summary_exist) > 0){
     $vote_exits = DB::table('main.treasury_votes_summary')
     ->where('treasury_funding_summary_id', $summary_exist->id)
     ->where('vote_code', $vote)
     ->first();

     if (empty($vote_exits)) {
      DB::table('main.treasury_votes_summary')->insert([
        'vote_code' => $vote,
        'total_employees' => $e_count,
        'total_contribution_amount' => $c_contrib,
        'total_grosspay' => $t_grosspay,
        'check_date' => $period,
        'treasury_funding_summary_id' => $summary_exist->id,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
    } else {
      DB::table('main.treasury_votes_summary')
      ->where('id',$vote_exits->id)
      ->update([
       'total_employees' => $e_count,
       'total_contribution_amount' => $c_contrib,
       'total_grosspay' => $t_grosspay,
       'updated_at' => Carbon::now(),
     ]);
    }
  }
}
}
public function emailAdminError($period,$page)
{

}


public function generateTreasuryBill(Request $request)
{

// Log::info(print_r($request->all(), true));

 DB::transaction(function () use ($request) {
  $this->saveGfBill($request->contrib_month);
  $this->saveCfBill($request->contrib_month);



  $month = Carbon::parse($request->contrib_month)->format('Y-m-28');

  $treasury_bills = Bill::where('contrib_month',$month)->where('is_treasury',true)->get();

  if ($treasury_bills->count() > 0) {
   foreach ($treasury_bills as $t_bill) {
    $this->requestTreasuryControlNumber($t_bill->bill_no);
  }
}
});
 return response()->json(array('success' => true));
}


public function saveGfBill($period)
{

  $funding_source = 'GOVERNMENT FUNDED';
  $summary_gf = DB::table('main.treasury_funding_summary')
  ->where('check_date',  $period)
  ->where('funding_source', $funding_source)
  ->first();

  if ($summary_gf) {
    $bill_rand_no = mt_rand(1000, 1999);
    $latest = DB::table('portal.bills')->orderBy('id', 'desc')->limit(1)->get();
    $latest_bill = $latest[0]->id+1;
    $bill_no = $bill_rand_no.$latest_bill;
    $description = 'Contribution';
    $bill_item = 'Contribution for '.Carbon::parse($period)->format('F Y');

    $bill = new Bill;
    $bill->employer_id = 9386;
    $bill->bill_no= $bill_no;
    $bill->bill_amount = $summary_gf->total_contribution_amount;
    $bill->bill_description = $description;
    $bill->billed_item = $bill_item;
    $bill->contribution = $summary_gf->total_contribution_amount;
    $bill->member_count = $summary_gf->total_employees;
    $bill->bill_status = 1;
    $bill->contrib_month = Carbon::parse($period)->format('Y-m-28');
    if(Carbon::parse($period)->format('m') == Carbon::parse(Carbon::now())->format('m')){
     $bill->contribution_due_date = Carbon::now()->addMonth()->endOfMonth()->toDateTimeString();
     $bill->expire_date = Carbon::parse(Carbon::now())->addMonths(2)->endOfMonth()->toDateTimeString();
   }else{
     $bill->contribution_due_date = Carbon::now()->endOfMonth()->toDateTimeString();
     $bill->expire_date =  Carbon::parse(Carbon::now())->addMonth()->endOfMonth()->toDateTimeString();
   }
   $bill->user_id = access()->user()->id;
   $bill->mobile_number = '255699210954';
   $bill->bill_source ='MAC';
   $bill->is_treasury = true;
   $bill->save();

   DB::table('main.treasury_funding_summary')
   ->where('id',  $summary_gf->id)
   ->update([
     'bill_id' => $bill->bill_no,
     'updated_at' => Carbon::now(),
   ]);

   $votes_gf = DB::table('main.treasury_votes_summary')
   ->where('treasury_funding_summary_id',  $summary_gf->id)
   ->get();

   foreach ($votes_gf as $vote) {
    DB::table('main.treasury_votes_summary')
    ->where('id',  $vote->id)->update([
      'bill_id' => $bill->bill_no,
      'updated_at' => Carbon::now(), 
    ]);
  }




}



}

public function saveCfBill($period)
{

  $funding_source = 'COUNCIL FUNDED';
  $summary_cf = DB::table('main.treasury_funding_summary')
  ->where('check_date',  $period)
  ->where('funding_source', $funding_source)
  ->first();

  if ($summary_cf) {
    $votes_cf = DB::table('main.treasury_votes_summary')
    ->where('treasury_funding_summary_id',  $summary_cf->id)
    ->get();


    foreach ($votes_cf as $vote) {
     $bill_rand_no = mt_rand(1000, 1999);
     $latest = DB::table('portal.bills')->orderBy('id', 'desc')->limit(1)->get();
     $latest_bill = $latest[0]->id+1;
     $bill_no = $bill_rand_no.$latest_bill;
     $description = 'Contribution';
     $bill_item = 'Contribution for '.Carbon::parse($period)->format('F Y');

     $bill = new Bill;
                // $bill->employer_id = 9386;
     $bill->bill_no= $bill_no;
     $bill->bill_amount = $vote->total_contribution_amount;
     $bill->bill_description = $description;
     $bill->billed_item = $bill_item;
     $bill->contribution = $vote->total_contribution_amount;
     $bill->member_count = $vote->total_employees;
     $bill->bill_status = 1;
     $bill->contrib_month = Carbon::parse($period)->format('Y-m-28');
     if(Carbon::parse($period)->format('m') == Carbon::parse(Carbon::now())->format('m')){
      $bill->contribution_due_date = Carbon::now()->addMonth()->endOfMonth()->toDateTimeString();
      $bill->expire_date = Carbon::parse(Carbon::now())->addMonths(2)->endOfMonth()->toDateTimeString();
    }else{
      $bill->contribution_due_date = Carbon::now()->endOfMonth()->toDateTimeString();
      $bill->expire_date =  Carbon::parse(Carbon::now())->addMonth()->endOfMonth()->toDateTimeString();
    }
    $bill->user_id = access()->user()->id;
    $bill->mobile_number = '255699210954';
    $bill->bill_source ='MAC';
    $bill->is_treasury = true;
    $bill->vote_code = $vote->vote_code;
    $bill->save();  

    DB::table('main.treasury_votes_summary')
    ->where('id',  $vote->id)
    ->update([
      'bill_id' => $bill->bill_no,
      'updated_at' => Carbon::now(),
    ]);

  }
}

}



public function requestTreasuryControlNumber($bill_no){
 $bill = Bill::where('bill_no','=',$bill_no)->first();


 if(!empty($bill->employer_id)){
  $employer=Employer::where('id','=',$bill->employer_id)->first();
  $payer_name = $employer->name;
  $payer_id = $employer->id;
  $payer_email = $employer->email;
}else{

  $payer_name = 'COUNCIL';
  $payer_id = $bill->vote_code;
  $payer_email = 'juma.wahabu@wcf.go.tz';
}

$end = Carbon::parse($bill->expire_date);
$tdy = Carbon::now();
$days_expires_after = $end->diffInDays($tdy);


$data = [
  "payment_ref"=> $bill->bill_no,
  "sub_sp_code"=> 1001,
  "amount"=> $bill->bill_amount,
  "desc"=> $bill->bill_description,
  "gfs_code"=> $bill->gfs_code,
  "payment_type"=> 1,
  "payerid"=> $payer_id,
  "payer_name"=> $payer_name,
  "payer_cell"=>$bill->mobile_number,
            // "payer_cell"=>str_ireplace(' ','',$employer->phone),
  "payer_email"=> $payer_email,
  "days_expires_after"=> $days_expires_after,
  "generated_by"=> 'WCF',
  "approved_by"=> 'DG_WCF'
];


try {
  $client = new Client();
            // $response =  $client->request('POST', 'http://gepg.test/bills/post_bill', ['json' => $data]);
            // $response =  $client->request('POST', 'http://192.168.1.9/api/bills/post_bill', ['json' => $data]);
            // return $response->getBody();
}
catch (ClientError $e) {

  $req = $e->getRequest();
  $resp =$e->getStatusCode();
  return $resp;
}
catch (ServerError $e) {

  $req = $e->getRequest();
  $resp =$e->getStatusCode();
  return $resp;
}
catch (BadResponse $e) {

  $req = $e->getRequest();
  $resp =$e->getStatusCode();
  return $resp;
}
catch( \Error $e){
  Log::info('Error Occured!');
}
catch( \Exception $e){
  Log::info('Exception Error Occured!');
}


}

public function returnBillDatatable($bill_id,$type)
{

  $b_exist = Bill::where('bill_no',$bill_id)->first();

  if($type == 'GF'){
    $bills = DB::table('portal.bills')
    ->select('*','bills.id as id')
    ->leftjoin('portal.payments', 'bills.bill_no', '=', 'payments.bill_id')
    ->leftjoin('main.employers', 'bills.employer_id', '=', 'employers.id')
    ->where('contrib_month',$b_exist->contrib_month)->where('is_treasury',true)->where('employer_id',9386);
          // Log::info('-----------gf');
  }else{
    $bills = DB::table('portal.bills')
    ->select('*','bills.id as id')
    ->leftjoin('portal.payments', 'bills.bill_no', '=', 'payments.bill_id')
    ->leftjoin('main.employers', 'bills.employer_id', '=', 'employers.id')
    ->where('contrib_month',$b_exist->contrib_month)->where('is_treasury',true)->where('employer_id','!=',9386);
          // Log::info('--------- cf');
          // Log::info(print_r($bills,true));
  }

  return Datatables::of($bills)
  ->editColumn('bill_amount', function($bill) {
    return number_format($bill->bill_amount,2);
  })
  ->addColumn('action', function($bill) {
    if (!is_null($bill->control_no)) {
      return '<a href="'.url('/finance/nmb_transfer/'.$bill->id).'" class="btn btn-md btn-success"><i class="fa fa-download"></i> NMB Form</a> &nbsp; <a href="'.url('/finance/crdb_transfer/'.$bill->id).'" class="btn btn-md btn-success"><i class="fa fa-download"></i> CRDB Form</a> ';
    } else {
      return ' <a  class="btn btn-md btn-warning text-white btnRefreshCn"><i class="fa fa-refresh"></i> Refresh To Get Control No#</a>';
    }

  })
  ->rawColumns(['action'])
  ->make(true);
}



public function getEmployeesChanges()
{
    //personal action
  $period=$this->returnNextCheckdate();
  $pagesize=1000;
  $pagenumber = 1;
  $request = Request();
  try{

    $client = new Client();
    $request = $client->get('http://10.1.1.218/gsppapi/api/paactions?checkDate='.$period.'&pageNo='.$pagenumber.'&pageSize='.$pagesize);
    $response = $request->getBody()->getContents();
    $xml = simplexml_load_string($response);
    dump('-------------- start --- --------');

    $json = json_decode(json_encode($xml),true);


        // Log::info(print_r($json,true));

    for ($i=0; $i < $pagesize; $i++) { 
            // Log::info('--------Loop '.($i+1).' ------');
      dump('Loop '.($i+1).' starts');

      Log::info(print_r($json['Employee'][$i],true));

      $input = [
        'CheckNumber' => $json['Employee'][$i]['CheckNumber'],
        'firstname' => empty($json['Employee'][$i]['FirstName']) ?  null: strtoupper(trim($json['Employee'][$i]['FirstName'])),
        'middlename' => (!isset($json['Employee'][$i]['MiddleName'])  ?  '' : (empty($json['Employee'][$i]['MiddleName']) ? null : strtoupper(trim($json['Employee'][$i]['MiddleName'])))),
        'lastname' => empty($json['Employee'][$i]['LastName']) ?  null: strtoupper(trim($json['Employee'][$i]['LastName'])),
        'VoteCode' => empty($json['Employee'][$i]['VoteCode']) ?  null: strtoupper(trim($json['Employee'][$i]['VoteCode'])),
        'VoteName' => empty($json['Employee'][$i]['VoteName']) ?  null: strtoupper(trim($json['Employee'][$i]['VoteName'])),
        'MonthlySalary' => empty($json['Employee'][$i]['MonthlySalary']) ?  null: $json['Employee'][$i]['MonthlySalary'],
        'PreviousSalary' => empty($json['Employee'][$i]['PreviousSalary']) ?  null: $json['Employee'][$i]['PreviousSalary'],
        'SalaryChange' => empty($json['Employee'][$i]['SalaryChange']) ?  null: $json['Employee'][$i]['SalaryChange'],
        'ActionCategory' => empty($json['Employee'][$i]['ActionCategory']) ?  null: strtoupper(trim($json['Employee'][$i]['ActionCategory'])),
        'reason' => empty($json['Employee'][$i]['Reason']) ?  null: trim($json['Employee'][$i]['Reason']),
        'Gender' => empty($json['Employee'][$i]['Gender']) ?  null: strtoupper(trim($json['Employee'][$i]['Gender'])),
        'sex' =>(!isset($json['Employee'][$i]['Gender'])  ?  0 : (empty($json['Employee'][$i]['Gender']) ? 0 : strtoupper(trim($json['Employee'][$i]['Gender'] == 'M' ? 1:2)))),
        'dob' => empty($json['Employee'][$i]['Birthdate']) ?  null: Carbon::parse($json['Employee'][$i]['Birthdate'])->format('Y-m-d'),
        'DeptCode' => empty($json['Employee'][$i]['DeptCode']) ?  null: strtoupper(trim($json['Employee'][$i]['DeptCode'])),
        'DeptName' => empty($json['Employee'][$i]['DeptName']) ?  null: trim($json['Employee'][$i]['DeptName']),
        'checkDate' => $period,
        'emp_cate' => 84,
        'PayGrade' => NULL,
        'PayStep' => NULL,
        'SalaryScale' => NULL, 
        'national_id' => (!isset($json['Employee'][$i]['NationalId'])  ?  '' : (empty($json['Employee'][$i]['NationalId']) ? null : strtoupper(trim($json['Employee'][$i]['NationalId'])))), 
      ];


      $input['basicpay'] = $input['MonthlySalary'];
      $input['grosspay'] = $input['MonthlySalary'];
      $input['employee_category_cv_id'] = $input['emp_cate']; 


            // Log::info(print_r($input,true));
      switch ($input['ActionCategory']) {
        case 'NEW JOINER':
        $this->generateMemberNumber($input);
        break;
        case 'TERMINATION':
        $this->removeEmployee($input);
        break;
        case 'REINSTATED':
        Log::info('reinstated = ='.$input['ActionCategory']);
        break;
        case 'PROMOTION':
        $this->updateEmployeesJobData($input);
        break;
        default:
        break;
      }
      dump('Loop '.($i+1).' ends');
      Log::info('--------Loop '.($i+1).' ends------');
    }
    dump('Done');


  }
  catch( ConnectException $e){
    Log::info('---- Connection error -----');
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch (ClientError $e) {
    Log::info('---------Client error -----');
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch (\Error $e) {
    Log::info('---------General error -----');
    Log::info($e);
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch( \ Exception $e){
    Log::info('---------Exception error -----');
    Log::info($e);
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }

}



public function updateEmployeesBioData($input)
{
    //update bio data --> employee table
  DB::table('main.employees')
  ->where('CheckNumber',$input['CheckNumber'])
  ->update([
    'DeptCode'=> $input['DeptCode'],
    'DeptName'=> $input['DeptName'],
    'VoteCode'=> $input['VoteCode'],
    'sex'=> $input['Gender'],
    'gender_id'=> $input['sex'],
    'dob'=> $input['dob'],
    'updated_at' => Carbon::now(),
  ]);
}


public function updateEmployeesJobData($input)
{
    //update vote/salary  ---> employee employer table
  $employer = Employer::where('vote',$input['VoteCode'])->first();

  $employee = DB::table('main.employees')->where('CheckNumber',$input['CheckNumber'])->first();

  if (!empty($employee)) {
    DB::table('main.employee_employer')
    ->where('employee_id',$employee->id)
    ->where('payroll_id',null)
    ->update([
      'employer_id' => $employer->id,
      'grosspay' => $input['MonthlySalary'],
      'employee_category_cv_id' => $input['emp_cate'],
      'updated_at' => Carbon::now(),
    ]); 
  }

  $this->updateEmployeesBioData($input);


}



public function removeEmployee($input)
{
    //deleted_at and reason  --> employee_employer_table

  $employer = Employer::where('vote',$input['VoteCode'])->first();

  $employee = DB::table('main.employees')->where('CheckNumber',$input['CheckNumber'])->first();

  if (!empty($employee)) {
   if (!empty($employer)) {
    DB::table('main.employee_employer')
    ->where('employee_id',$employee->id)
    ->where('employer_id',$employer->id)
    ->update([
      'deleted_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
      'remove_reason' => $input['reason'],
    ]);
  }else{
    DB::table('main.employee_employer')
    ->where('employee_id',$employee->id)
    ->where('payroll_id',null)
    ->update([
      'deleted_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
      'remove_reason' => $input['reason'],
    ]); 
  }
} 


}

//NIDA methods starts here
public function fingerPrintVerification()
{

  $employee_json=file_get_contents('php://input');
  $employee_nin= json_decode($employee_json);
  $employee_nin= json_decode($employee_json,true);
  Log::info(print_r($employee_nin,true));

//Perform check if National ID exist
  $employee_profile= DB::table('main.employees')->where('nid',$employee_nin['NIN'])->first();
  if(!empty($employee_profile)){
   DB::table('main.employees')->where('nid',$employee_nin['NIN'])
   ->update([
    'photo' =>$employee_nin['PHOTO']
  ]);

   Log::info($employee_profile->firstname.$employee_profile->dob.$employee_profile->nid);



   return response()->json(['success' => true, 'message' =>'Successfully verified']);

 }else{
   return response()->json(['success' => false, 'message' =>'National ID does not exist or Match']);

 }


}

public function updateEmployer_id()
{
  $bills = DB::table('portal.bills')->where('is_treasury',true)->where('employer_id',null)->get();

  foreach ($bills as $b) {
    $employer = Employer::where('vote',$b->vote_code)->first();
    if(!empty($employer)){
      DB::table('portal.bills')->where('vote_code',$b->vote_code)->update([
        'employer_id' => $employer->id,
      ]);
    }
  }
  dump('done');

}



        // ================== second version


public function saveOverallSummary($summary_data,$check_date)
{
    // Log::info('--------overalll--');
    // Log::info(print_r($summary_data, true));
    // Log::info('------------');
  $chkdate = Carbon::parse($check_date)->format('Y-m-28');
  $next_check_date = Carbon::parse($chkdate)->addMonth()->endOfMonth()->format('Y-m-d');
  $return = null;
  if (!empty($summary_data)) {

    $summary_exist = DB::table('main.treasury_contribution_summary')->where('check_date',$check_date)->first();


    if (empty($summary_exist)) {
     $summary_id = DB::table('main.treasury_contribution_summary')->insertGetId([
       'total_employees'=> $summary_data['numEmp'],
       'total_pages'=> $summary_data['totalPages'],
       'check_date'=> $check_date,
       'next_check_date'=> $next_check_date,
       'loaded_employees'=> 0,
       'loaded_pages'=> 0,
       'created_at' => Carbon::now(),
       'start_date' => Carbon::now(),
       'updated_at' => Carbon::now(),
     ]);

     $return = $summary_id;
   }else{
    DB::table('main.treasury_contribution_summary')
    ->where('id',$summary_exist->id)
    ->update([
      'total_employees'=> $summary_data['numEmp'],
      'total_pages'=> $summary_data['totalPages'],
      'check_date'=> $check_date,
      'next_check_date'=> $next_check_date,
      'loaded_employees'=> 0,
      'loaded_pages'=> 0,
      'updated_at' => Carbon::now(),
    ]);

    $return = $summary_exist->id;  
  }
}

return $return;
}



public function updateOverallSummary($period,$type)
{
  $summary_exist = DB::table('main.treasury_contribution_summary')->where('check_date',$period)->first(); 

  if (!empty($summary_exist)) {
    switch ($type) {
      case 'pages':
      DB::table('main.treasury_contribution_summary')
      ->where('id',$summary_exist->id)
      ->update([
        'loaded_pages'=> ($summary_exist->loaded_pages+1),
        'updated_at' => Carbon::now(),
      ]);
      break;
      case 'employees':
      DB::table('main.treasury_contribution_summary')
      ->where('id',$summary_exist->id)
      ->update([
        'loaded_employees'=> ($summary_exist->loaded_employees+1),
        'updated_at' => Carbon::now(),
      ]);
      break;
      case 'status':
      $vote_count = DB::table('main.treasury_employees_contribution')
      ->distinct('vote_code')
      ->where('check_date',  $period)->count('vote_code');
      if (($summary_exist->total_employees == $summary_exist->loaded_employees) && ($summary_exist->total_pages == $summary_exist->loaded_pages)){
        DB::table('main.treasury_contribution_summary')
        ->where('id',$summary_exist->id)
        ->update([
          'is_complete'=> true,
          'total_votes' =>  $vote_count,
          'status'=> 'success',
          'finish_date' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);
      }else{
       DB::table('main.treasury_contribution_summary')
       ->where('id',$summary_exist->id)
       ->update([
         'is_complete'=> true,
         'total_votes' =>  $vote_count,
         'status'=> 'failed',
         'finish_date' => Carbon::now(),
         'updated_at' => Carbon::now(),
       ]);
     }
     break;
     default:
     break;
   }
 }
}


public function saveFailedPages($page, $overall_summary_id, $error_code, $reason)
{
 DB::table('main.treasury_failed_pages')->insert([
   'treasury_contribution_summary_id' => $overall_summary_id,
   'page_number' => $page,
   'error_code' => $error_code,
   'reason' => $reason,
 ]);
}

public function returnFailedSummaryDetails($contrib_month)
{
  $month = Carbon::parse($contrib_month)->endOfMonth()->format('Y-m-d');
  $summary= DB::table('main.treasury_contribution_summary')->where('check_date',$month)->first(); 

  if (isset($summary)) {
    if (!isset($summary->finish_date)) {
      $reason = 'Data is still being Loaded';
    }else{
      $reason = 'Some data failed to load';
    }
  } else {
   $reason = 'Data isn\'t Loaded yet';
 }



 $data = [
   'total_employees' => isset($summary->total_employees) ? number_format($summary->total_employees): 0,
   'loaded_employees' => isset($summary->loaded_employees) ? number_format($summary->loaded_employees): 0,
   'total_pages' => isset($summary->total_pages) ? number_format($summary->total_pages): 0,
   'loaded_pages' => isset($summary->loaded_pages) ? number_format($summary->loaded_pages): 0,
   'start_date' => isset($summary->start_date) ? Carbon::parse($summary->start_date)->format('d,F Y h:ia'): '',
   'finish_date' => isset($summary->finish_date) ? Carbon::parse($summary->finish_date)->format('d,F Y h:ia'): '',
   'reason' => $reason,
 ];


 return $data;

}


public function summaryDrillDown($check_date,$funding_source)
{

  $fund = ($funding_source == 'gf') ? 'Government Funded' : 'Council Funded';

  $month = Carbon::parse($check_date)->format('F Y');
  return view('backend.finance.govt_intergration.treasury.summary_drill_down',compact('check_date','funding_source','month','fund')); 
}


public function summaryDrillDownDatatable($check_date,$funding_source)
{

 $fund = ($funding_source == 'gf') ? 'GOVERNMENT FUNDED' : 'COUNCIL FUNDED';

 $summary_data = DB::table('main.treasury_funding_summary')->where('check_date',$check_date)->where('funding_source',$fund)->first();


 $s_id = isset($summary_data) ? $summary_data->id : null;

 $details = DB::table('main.treasury_votes_summary')->where('treasury_funding_summary_id',$s_id);

 return Datatables::of($details)

 ->addColumn('vote_name', function($details) {
   $vote = DB::table('main.treasury_employees_contribution')->select('vote_name')->where('vote_code',$details->vote_code)->first();
   return $vote->vote_name;
 })
 ->editColumn('vote_code', function($details) {
   return '<span class="vote_code" data-vote_code="'.$details->vote_code.'">'.$details->vote_code.'</span>';
 })
 ->editColumn('total_employees', function($details) {
   return number_format($details->total_employees);
 })
 ->editColumn('total_grosspay', function($details) {
  return number_format($details->total_grosspay,2);
})
 ->editColumn('total_contribution_amount', function($details) {
   return number_format($details->total_contribution_amount,2);
 })
 ->rawColumns(['vote_code'])
 ->make(true); 
}


public function votesDrillDown($vote_code,$check_date,$funding_source)
{

 $fund = ($funding_source == 'gf') ? 'Government Funded' : 'Council Funded';

 $vote = DB::table('main.treasury_employees_contribution')->select('vote_name')->where('vote_code',$vote_code)->first();
 $employer_name = $vote->vote_name;

 $month = Carbon::parse($check_date)->format('F Y');
 return view('backend.finance.govt_intergration.treasury.votes_summary_drill_down',compact('vote_code','check_date','funding_source','month','fund','employer_name')); 
}


public function votesDrillDownDatatable($vote_code,$check_date,$funding_source)
{

 $fund = ($funding_source == 'gf') ? 'GOVERNMENT FUNDED' : 'COUNCIL FUNDED';


 $details = DB::table('main.treasury_employees_contribution')
 ->where('vote_code',$vote_code)->where('check_date',$check_date)->where('funding_source',$fund);


 return Datatables::of($details)
 ->addColumn('name', function($details) {
  return ucfirst($details->firstname).' '.ucfirst($details->middlename).' '.ucfirst($details->lastname);
})
 ->editColumn('grosspay', function($details) {
  return number_format($details->grosspay,2);
})
 ->editColumn('employer_contribution', function($details) {
   return number_format($details->employer_contribution,2);
 })
 ->make(true); 
}


public function getMofToken()
{

  $request = Request();
  try{

    $url = config('constants.MOF.TOKEN_URL');
        // dd($url);
    $client = new Client();

    $response = $client->request('POST', $url, [
      'headers' => [
        'content-type' => 'application/x-www-form-urlencoded',
      ],'form_params' => [
        'client_secret' => config('constants.MOF.CLIENT_PASSWORD'),
        'client_id' => config('constants.MOF.CLIENT'),
        'grant_type' => config('constants.MOF.GRANT_TYPE')
      ],
    ]);

    $json = json_decode($response->getBody()->getContents(),true);

    return $json;


  }
  catch( ConnectException $e){
    Log::info('---- Connection error employees-----');
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch (ClientError $e) {
    Log::info('---------Client error employees-----');
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch (\Error $e) {
    Log::info('---------General error employees-----');
    Log::info($e);
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }
  catch( \ Exception $e){
    Log::info('---------Exception error employees -----');
    Log::info($e);
    Log::error(print_r($e->getMessage(),true));
    Log::info('-------------');
  }

}
public function returnNextCheckdate()
 {
   $current_period=DB::table('main.treasury_contribution_summary')->select('id','check_date','next_check_date')->where('is_complete',true)->where('status','success')->orderBy('id', 'desc')->limit(1)->get();
    
    $period=$current_period[0]->next_check_date;

   return $period;
 }
}