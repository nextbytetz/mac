<?php

namespace App\Repositories\Backend\Task;

use App\Models\Sysdef\CodeValue;
use App\Models\Task\Checker;
use App\Notifications\Backend\Operation\Claim\Portal\IncidentCreated;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Claim\BenefitTypeClaimRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\Repositories\Backend\Sysdef\CodeValueCodeRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class CheckerRepository
 * @package App\Repositories\Backend\Task
 * @description Manages all the pending tasks waiting for user action
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class CheckerRepository extends BaseRepository
{
    const MODEL = Checker::class;

    /**
     * @param array $attributes
     * @return mixed
     */
    public function queryCHCNOTN($attributes = [])
    {
        //Notification & Claim
        $codeValue = new CodeValueRepository();
        $onInvestigation = "({$codeValue->NSPCRFI()}, {$codeValue->NSOPRFLI()})";
        $attributes[] = DB::raw("notification_reports.filename");
        $attributes[] = DB::raw("incident_types.name as incident");
        $attributes[] = DB::raw("notification_reports.receipt_date");
        $attributes[] = DB::raw("notification_reports.created_at");
        $attributes[] = DB::raw("notification_reports.notification_staging_cv_id");
        $attributes[] = DB::raw("notification_reports.employer_id");
        $attributes[] = DB::raw("case when notification_reports.employee_id is null then notification_reports.employee_name else concat_ws(' ', employees.firstname, coalesce(employees.middlename, ''), employees.lastname) end as employee");
        $attributes[] = DB::raw("case when notification_reports.employer_id is null then notification_reports.employer_name else employers.name end as employer");
        $attributes[] = DB::raw("notification_reports.incident_date"); //incident date
        $attributes[] = DB::raw("regions.name as region");
        $attributes[] = DB::raw("code_values.name as stage");
        $attributes[] = DB::raw("case when notification_reports.notification_staging_cv_id in {$onInvestigation} then 'Notification Investigation' else 'Notification Checklist' end as category");

        $query = $this->query()
        ->select($attributes)
        ->join("notification_reports", "notification_reports.id", "=", "checkers.resource_id")
        ->join("incident_types", "notification_reports.incident_type_id", "=", "incident_types.id")
        ->leftJoin("employees", "notification_reports.employee_id", "=", "employees.id")
        ->leftJoin("employers", "notification_reports.employer_id", "=", "employers.id")
        ->join("districts", "notification_reports.district_id", "=", "districts.id")
        ->join("regions", "districts.region_id", "=", "regions.id")
        ->join("code_values", "notification_reports.notification_staging_cv_id", "=", "code_values.id")
        ->leftJoin("incident_mae", "incident_mae.id", "=", "notification_reports.incident_mae_id")
        ->leftJoin("incident_tds", "incident_tds.id", "=", "notification_reports.incident_td_id")
            //->where("checkers.user_id", access()->id())
        ->where("notification_reports.isprogressive", 1);
        return $query;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function queryCCAEMPLYINSPCTN($attributes = [])
    {
        //Inspection Plan
        $attributes[] = DB::raw("a.name inspection_type");
        $attributes[] = DB::raw("b.name inspection_trigger");
        $attributes[] = DB::raw("c.name inspection_trigger_category");
        $attributes[] = DB::raw("d.name stage");
        $attributes[] = DB::raw("(EXTRACT(days FROM (coalesce(end_date, now()) - start_date)) / 7)::int duration");
        $attributes[] = DB::raw("to_char(start_date, 'Day,DD Mon YYYY') start_date_formatted");
        $attributes[] = DB::raw("iscancelled");
        //$attributes[] = DB::raw("inspections.id");

        $query = $this->query()
        ->select($attributes)
        ->join("inspections", "inspections.id", "=", "checkers.resource_id")
        ->join(DB::raw('code_values a'), "a.id", "=", "inspections.inspection_type_cv_id")
        ->join(DB::raw('code_values b'), "b.id", "=", "inspections.inspection_trigger_cv_id")
        ->leftJoin(DB::raw('code_values c'), "c.id", "=", "inspections.trigger_category_cv_id")
        ->join(DB::raw('code_values d'), "d.id", "=", "inspections.staging_cv_id");
        return $query;
    }

    public function queryCCONTFCAPPCTN($attributes = [])
    {
        //Online Notification Application
        $attributes[] = DB::raw("main.incident_types.name incident_type");
        $attributes[] = DB::raw("concat_ws(' ', main.employees.firstname, coalesce(main.employees.middlename, ''), main.employees.lastname) as employee");
        $attributes[] = DB::raw("main.employees.middlename");
        $attributes[] = DB::raw("main.employees.lastname");
        $attributes[] = DB::raw("employers.name employer");
        $attributes[] = DB::raw("main.regions.name region");
        $attributes[] = DB::raw("main.districts.name district");
        $attributes[] = DB::raw("portal.incidents.incident_date");
        $attributes[] = DB::raw("portal.incidents.reporting_date");
        //$attributes[] = DB::raw("inspections.id");

        $query = $this->query()
        ->select($attributes)
        ->join(DB::raw("portal.incidents"), "portal.incidents.id", "=", "checkers.resource_id")
        ->join(DB::raw('portal.code_values a'), "a.id", "=", "portal.incidents.incident_staging_cv_id")
        ->join("main.incident_types", "portal.incidents.incident_type_id", "=", "main.incident_types.id")
        ->join("main.districts", "portal.incidents.district_id", "=", "main.districts.id")
        ->join("main.regions", "main.districts.region_id", "=", "main.regions.id")
        ->leftJoin("main.employers", "main.employers.id", "=", "portal.incidents.employer_id")
        ->leftJoin("main.notification_reports", "main.notification_reports.id", "=", "portal.incidents.notification_report_id")
        ->leftJoin("main.employees", "main.employees.id", "=", "portal.incidents.employee_id");
        return $query;
    }

    public function queryCCALETTER($attributes = [])
    {
        //Letter
        $attributes[] = DB::raw("letters.reference");
        $attributes[] = DB::raw("b.name letter_type");
        $attributes[] = DB::raw("a.name stage");
        $attributes[] = DB::raw("letters.letter_date");
        $attributes[] = DB::raw("c.name priorityall");
        //$attributes[] = DB::raw("inspections.id");

        $query = $this->query()
        ->select($attributes)
        ->join("letters", "letters.id", "=", "checkers.resource_id")
        ->join(DB::raw('code_values a'), "a.id", "=", "letters.staging_cv_id")
        ->join(DB::raw('code_values b'), "b.id", "=", "letters.cv_id")
        ->join(DB::raw('code_values c'), "c.id", "=", "checkers.priority");
        return $query;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function queryCCAEMPINSPCTNTSK($attributes = [])
    {
        //Employer Inspection Task
        $attributes[] = DB::raw("a.name task_type");
        $attributes[] = DB::raw("employers.name employer");
        $attributes[] = DB::raw("case when employer_inspection_task.attended = 1 then 'Attended' else 'Not Attended' end attend_status");
        $attributes[] = DB::raw("b.name stage");
        $attributes[] = DB::raw("regions.name region");
        $attributes[] = DB::raw("districts.name district");
        $attributes[] = DB::raw("employer_inspection_task.visit_date");
        //Employer Inspection Task
        $query = $this->query()
        ->select($attributes)
        ->join("employer_inspection_task", "employer_inspection_task.id", "=", "checkers.resource_id")
        ->join("inspection_tasks", "inspection_tasks.id", "=", "employer_inspection_task.inspection_task_id")
        ->join("inspections", "inspections.id", "=", "inspection_tasks.inspection_id")
        ->join("employers", "employers.id", "=", "employer_inspection_task.employer_id")
        ->join(DB::raw('code_values a'), "a.id", "=", "inspection_tasks.inspection_type_cv_id")
        ->join(DB::raw('code_values b'), "b.id", "=", "employer_inspection_task.staging_cv_id")
        ->leftJoin("districts", "employers.district_id", "=", "districts.id")
        ->leftJoin("regions", "employers.region_id", "=", "regions.id");
        return $query;
    }


    /**
     * @param array $attributes
     * @return mixed
     * Employer De-registration
     */
    public function queryCHCEMPDEREG($attributes = [])
    {
        //Employer Degistrations
        $attributes[] = DB::raw("employers.name as employer_name");
        $attributes[] = DB::raw("employers.reg_no as employer_regno");
        $attributes[] = DB::raw('employer_closures.close_date as close_date');
        $attributes[] = DB::raw("employer_closures.application_date as application_date");
        $attributes[] = DB::raw("employer_closures.wf_done_date as approved_date");
        $attributes[] = DB::raw("employer_closures.followup_ref_date as followup_ref_date");
        //Employer deregistration
        $query = $this->query()
            ->select($attributes)
            ->join("employer_closures", "employer_closures.id", "=", "checkers.resource_id")
            ->join("employers", "employers.id", "=", "employer_closures.employer_id");
        return $query;
    }

    /**
     * @param null $checker_category_cv_id
     * @return mixed
     */
    public function getQuery($checker_category_cv_id = NULL)
    {
        $codeValue = new CodeValueRepository();
        $attributes = [
            DB::raw("main.checkers.resource_id"),
            DB::raw("main.checkers.comments"),
            DB::raw("main.checkers.user_id"),
            DB::raw("main.checkers.status"),
            DB::raw("main.checkers.priority"),
            DB::raw("main.checkers.resource_type"),
        ];
        switch ($checker_category_cv_id) {
            case $codeValue->CHCNOTN():
                //Checker Category [Notification & Claim] (CHCNOTN)
            $query = $this->queryCHCNOTN($attributes);
            break;
            case $codeValue->CCAEMPLYINSPCTN():
                //Inspection Plan
            $query = $this->queryCCAEMPLYINSPCTN($attributes);
            break;
            case $codeValue->CCAEMPINSPCTNTSK():
                //Employer Inspection Task
            $query = $this->queryCCAEMPINSPCTNTSK($attributes);
            break;
            case $codeValue->CCALETTER():
                //Letter
            $query = $this->queryCCALETTER($attributes);
            break;
            case $codeValue->CCAEMPLYARRER():
                //Employer Arrears

                break;

            case $codeValue->CCONTFCAPPCTN():
                //Online Notification Application
                $query = $this->queryCCONTFCAPPCTN($attributes);
                break;


            case $codeValue->CHCEMPDEREG():
                //Employer De-registration
                $query = $this->queryCHCEMPDEREG($attributes);
                break;
            default:
                $query = $this->query()
                    ->select($attributes);
                break;

        }
        return $query;
    }

    /**
     * @param null $checker_category_cv_id
     * @return mixed
     */
    public function getPendingQuery($checker_category_cv_id = NULL)
    {
        $return = $this->getQuery($checker_category_cv_id)
                ->whereIn("main.checkers.user_id", access()->allUsers())
                ->where("main.checkers.status", 0);

        if ($checker_category_cv_id) {
            $return->where("main.checkers.checker_category_cv_id", $checker_category_cv_id);
        }
        return $return;
    }

    /**
     * @return mixed
     */
    public function getMyPendingCount()
    {
        return $this->getPendingQuery()
            ->whereIn("checkers.user_id", access()->allUsers())
            ->count();

    }

    public function getMyPendingNotifyCount()
    {
        return $this->getPendingQuery()
        ->whereIn("checkers.user_id", access()->allUsers())
        ->where("notify", 1)
        ->count();
    }

    public function getMyPendingShouldCount()
    {
        return $this->getPendingQuery()
        ->whereIn("checkers.user_id", access()->allUsers())
        ->where("shouldcount", 1)
        ->count();
    }

    /**
     * @param $code_value_id
     * @return mixed
     */
    public function getForDatatable($code_value_id)
    {
        $codeValue = new CodeValueRepository();
        $pendings = $this->getPendingQuery($code_value_id);

        switch ($code_value_id) {
            case $codeValue->CHCNOTN():
                //Checker Category [Notification & Claim] (CHCNOTN)
            $pendings = $this->searchDatatableForNotification($pendings);
            break;
            case $codeValue->CCAEMPLYARRER():
                //Employer Arrears

            break;
            case $codeValue->CCAEMPLYINSPCTN():
                //Inspection Plan
            $pendings = $this->searchCCAEMPLYINSPCTN($pendings);
            break;
            case $codeValue->CCAEMPINSPCTNTSK():
                //Employer Inspection Task
            $pendings = $this->searchCCAEMPINSPCTNTSK($pendings);
            break;
            case $codeValue->CCALETTER():
                //Letter
            $pendings = $this->searchCCALETTER($pendings);
            break;
            case $codeValue->CCONTFCAPPCTN():
                //Online Notification Application
            $pendings = $this->searchCCONTFCAPPCTN($pendings);
            break;
        }

        $table = app('datatables')

            ->of($pendings);
        switch ($code_value_id) {
            case $codeValue->CHCNOTN():
                //Checker Category [Notification & Claim] (CHCNOTN)
                $datatables = $table
                    ->addColumn('details_url', function($query) {
                        return route("backend.claim.notification_report.stage_comment", ["incident" => $query->resource_id]);
                    })
                    ->addColumn("priority_status", function ($query) {
                        return $query->priority_status;
                    })
                    ->rawColumns(['priority_status']);
                break;
            case $codeValue->CCAEMPLYARRER():
                //Employer Arrears

            break;
            case $codeValue->CCAEMPLYINSPCTN():
                //Inspection Plan
            $datatables = $table
            ->addColumn('details_url', function($query) {
                return route("backend.claim.notification_report.stage_comment", ["incident" => $query->resource_id]);
            })
            ->addColumn('complete_status_label', function($query) {
                return $query->resource->complete_status_label;
            })
            ->addColumn('iscancelled_label', function($query) {
                return $query->resource->iscancelled_label;
            })
            ->addColumn("priority_status", function ($query) {
                return $query->priority_status;
            })
            ->rawColumns(['iscancelled_label', 'complete_status_label', 'priority_status']);
            break;
            case $codeValue->CCAEMPINSPCTNTSK():
                //Employer Inspection Task
            $datatables = $table
            ->addColumn('details_url', function($query) {
                return route("backend.claim.notification_report.stage_comment", ["incident" => $query->resource_id]);
            })
            ->addColumn("priority_status", function ($query) {
                return $query->priority_status;
            })
            ->rawColumns(['priority_status']);
            break;
            default:
            $datatables = $table;
            break;
        }
        return $datatables;
    }

    public function searchCCAEMPLYINSPCTN($pendings)
    {
        $stage = request()->input("inspection_stage_id");
        if ($stage) {
            $pendings->where("inspections.staging_cv_id", "=", $stage);
        }
        return $pendings;
    }

    public function searchCCAEMPINSPCTNTSK($pendings)
    {
        $stage = request()->input("employer_inspection_stage");
        if ($stage) {
            $pendings->where("employer_inspection_task.staging_cv_id", "=", $stage);
        }
        return $pendings;
    }

    public function searchCCONTFCAPPCTN($pendings)
    {
        $stage = request()->input("online_notification_application_stage");
        if ($stage) {
            $pendings->where("portal.incidents.incident_staging_cv_id", "=", $stage);
        }
        return $pendings;
    }

    public function searchCCALETTER($pendings)
    {
        $stage = request()->input("letter_stage");
        if ($stage) {
            $pendings->where("letters.staging_cv_id", "=", $stage);
        }
        return $pendings;
    }

    /**
     * @param $pendings
     * @return mixed
     */
    private function searchDatatableForNotification($pendings)
    {
        $codeValue = new CodeValueRepository();
        $category = request()->input('category');
        //return $pendings;
        switch ($category) {
            case '0':
                //Notification Checklist
            $pendings->whereNotIn("notification_reports.notification_staging_cv_id", [$codeValue->NSPCRFI(), $codeValue->NSOPRFLI()]);
            break;
            case '1':
                //Notification Investigation
            $pendings->whereIn("notification_reports.notification_staging_cv_id",[$codeValue->NSPCRFI(), $codeValue->NSOPRFLI()]);
            break;
            default:
                //All Tasks
            break;
        }
        $incident = (int) request()->input("incident");
        if ($incident) {
            $pendings->where("notification_reports.incident_type_id", "=", $incident);
        }
        $employee = request()->input("employee");
        if ($employee) {
            $pendings->where("notification_reports.employee_id", "=", $employee);
        }
        $employer = request()->input("employer");
        if ($employer) {
            $pendings->where("notification_reports.employer_id", "=", $employer);
        }
        $region = request()->input("region");
        if ($region) {
            $pendings->where("regions.id", "=", $region);
        }
        $district = request()->input("district");
        if ($district) {
            $pendings->where("districts.id", "=", $district);
        }
        $stage = request()->input("stage");
        if ($stage) {
            $pendings->where("notification_reports.notification_staging_cv_id", "=", $stage);
        }
        $priority = request()->input("priority");
        switch ($priority) {
            case '0':
            case '1':
            $pendings->where("checkers.priority", "=", $priority);
            break;
            default:
            break;
        }
        $ready_to_initiate = request()->input("ready_to_initiate");
        if ($ready_to_initiate) {
            $initiated_check = NULL;
            switch ($ready_to_initiate) {
                case 1:
                    //MAE Refund (Employee/Employer)
                $column = "incident_mae.mae_refund_member_ready";
                break;
                case 3:
                    //Temporary Disablement
                $column = "incident_tds.td_ready";
                break;
                case 4:
                    //Temporary Disablement Refund
                $column = "incident_tds.td_refund_ready";
                break;
                case 5:
                    //Permanent Disablement
                $column = "notification_reports.pd_ready";
                $initiated_check = "notification_reports.pd_eligible_id";
                break;
                case 7:
                    //Survivors
                $column = "notification_reports.survivor_ready";
                $initiated_check = "notification_reports.survivor_eligible_id";

                break;
                case 50:
                $column = "notification_reports.approval_ready";
                $finishedFillingInvestigation = [$codeValue->NSPCRFNFI(), $codeValue->NSOPFRFLI()];
                $pendings->whereIn("notification_staging_cv_id", $finishedFillingInvestigation);
                break;
            }
            if (!is_null($initiated_check)) {
                $pendings->whereNull("{$initiated_check}");
            }
            switch ($ready_to_initiate) {
                case 1:
                case 3:
                case 4:
                case 5:
                    //MAE Refund (Employee/Employer)
                    //Temporary Disablement
                    //Temporary Disablement Refund
                    //Permanent Disablement
                $pendings->where("progressive_stage", ">=", 4);
                break;
            }
            $pendings->where("{$column}", "=", 1);
        }


        $type = request()->input("type");
        $overview = request()->input("overview");
        //Log::info($type);
        //Log::info($overview);
        switch ($type) {
            case 1:
                //Complete Document Not Validated
            if ($overview) {
                switch ($overview) {
                    case 1:
                            //MAE Refund (Employee/Employer)
                    $column = "incident_mae.mae_refund_member_ready";
                    break;
                    case 3:
                            //Temporary Disablement
                    $column = "incident_tds.td_ready";
                    break;
                    case 4:
                            //Temporary Disablement Refund
                    $column = "incident_tds.td_refund_ready";
                    break;
                    case 5:
                            //Permanent Disablement
                    $column = "notification_reports.pd_ready";
                    break;
                    case 7:
                            //Survivors
                    $column = "notification_reports.survivor_ready";
                    break;
                }
                $pendings->where("progressive_stage", "<", 4)->where("{$column}", "=", 1);
            }
            break;
            case 2:
                //Acknowledgement Letter Not Initiated
            $pendings->where("notification_reports.is_acknowledgment_initiated", "=", 0);
            break;
        }

        return $pendings;
    }

    /**
     * @param Model $resource
     * @param array $input resource_id | checker_category_cv_id | comments | user_id | checker_category_cv_id | priority
     * @return mixed
     */
    public function create(Model $resource, array $input)
    {
        return DB::transaction(function () use ($input, $resource) {
            //check if there is an existing resource for the same checker category, and update status = 1
            $this->closeRecently([
                'resource_id' => $resource->id,
                'checker_category_cv_id' => $input['checker_category_cv_id'],
            ]);
            $checker = $this->query()->create([
                'resource_id' => $resource->id,
                'comments' => $input['comments'],
                'user_id' => $input['user_id'],
                'checker_category_cv_id' => $input['checker_category_cv_id'],
                'priority' => (isset($input['priority']) ? $input['priority'] : 1),
                'notify' => $input['notify'] ?? 0,
                'shouldcount' => $input['shouldcount'] ?? 1,
            ]);
            //Update Resource Type
            $resource->checkers()->save($checker);
            //Send Mail to Notify about the checker entry ...
            $sendmail = $input['sendmail'] ?? 0;
            if ($sendmail) {
                $codeValueRepo = new CodeValueRepository();
                $codeValuePortalRepo = new CodeValuePortalRepository();
                $userRepo = new UserRepository();
                switch ($input['checker_category_cv_id']) {
                    case $codeValueRepo->CCONTFCAPPCTN():
                        //Online Notification Application
                    $incident = (new PortalIncidentRepository())->find($resource->id);
                    switch ($incident->incident_staging_cv_id) {
                            case $codeValuePortalRepo->ISUPSUPDOC(): //Pending Supporting Documents
                            case $codeValuePortalRepo->ISREGSTR(): //Incident Registration
                            $user = $userRepo->find($input['user_id']);
                            if ($user) {
                                $user->notify(new IncidentCreated($incident));
                            }
                            break;
                        }
                        break;
                        default:
                        break;
                    }
                }
                return true;
            });
    }

    /**
     * @param array $input resource_id | checker_category_cv_id | user_id | replacer | notification_staging_cv_id
     * @return mixed
     */
    public function update(array $input)
    {
        return DB::transaction(function () use ($input) {
            $codeValue = new CodeValueRepository();
            $query = $this->query()->where([
                'resource_id' => $input['resource_id'],
                'checker_category_cv_id' => $input['checker_category_cv_id'],
                'user_id' => $input['user_id'],
            ]);
            switch ($input['checker_category_cv_id']) {
                case $codeValue->CHCNOTN():
                    //Checker Category [Notification & Claim] (CHCNOTN)
                $query->whereRaw("comments->>'currentStageCvId' = {$input['notification_staging_cv_id']}::text");
                break;
                case $codeValue->CCAEMPINSPCTNTSK():
                    //Checker Category [Employer Inspection Task]
                $query->whereRaw("comments->>'currentStageCvId' = {$input['staging_cv_id']}::text");
                break;
                default:
                break;
            }
            if ($query->count()) {
                $checker = $query->first();
                $checker->user_id = $input['replacer'];
                if (isset($input['status'])) {
                    $checker->status = 0;
                }
                $checker->save();
            }
            return true;
        });
    }

    /**
     * @param array $input
     * @return mixed
     * @description close the most recent checker task given resource_id and checker category
     */
    public function closeRecently(array $input)
    {
        return DB::transaction(function () use ($input) {
            $query = $this->query()->where(['resource_id' => $input['resource_id'], 'checker_category_cv_id' => $input['checker_category_cv_id']])->orderByDesc("id")->limit(1);
            if ($query->count()) {
                $checker = $query->first();
                if (!$checker->status) {
                    //This checker has not been gracefully attended, and seems to be declined.
                    $checker->status = 2;
                } else {
                    $checker->status = 1;
                }
                $checker->save();
            }
            return true;
        });
    }

    /**
     * @param array $input
     * @return mixed
     * @description delete the most recent checker resource
     */
    public function deleteRecently(array $input)
    {
        return DB::transaction(function () use ($input) {
            $query = $this->query()->where(['resource_id' => $input['resource_id'], 'checker_category_cv_id' => $input['checker_category_cv_id']])->orderByDesc("id")->limit(1);
            if ($query->count()) {
                $query->delete();
            }
            return true;
        });
    }

    public function getResourceTable($reference)
    {
        switch($reference) {
            case "CCAEMPLYARRER":
                //Employer Arrears

            break;
            case "CCAEMPLYINSPCTN":
                //Inspection Plan
            $return = ['table' => 'inspections', 'column' => 'staging_cv_id'];
            break;
            case "CCAEMPINSPCTNTSK":
                //Employer Inspection Task
            $return = ['table' => 'employer_inspection_task', 'column' => 'staging_cv_id'];
            break;
            case "CCALETTER":
                //Employer Inspection Task

                $return = ['table' => 'letters', 'column' => 'staging_cv_id'];
                break;


            case "CHCEMPDEREG":
                //Employer closure - de-registration
                $return = ['table' => 'employer_closures', 'column' => 'staging_cv_id'];

                break;
            case "CHCPAYALERT":
                //Payroll Alert  tasks
                $return = ['table' => 'payroll_alert_tasks', 'column' => 'staging_cv_id'];

                break;


            case "CCONTFCAPPCTN":
                //Online Notification Application
            $return = ['table' => 'portal.incidents', 'column' => 'incident_staging_cv_id'];
            break;
            case "CHACCRCLM":
                //Accrual claim
            $return = ['table' => 'accrual_notification_reports', 'column' => 'status'];
            break;
            case "CHINVPLAN":
                //Investigation Plan 
            $return = ['table' => 'investigation_plan_notification_reports', 'column' => 'notification_report_id'];

            break;
            case "ARREARCMT":
                //Arrear Commitment 
            $return = ['table' => 'portal.commitment_requests', 'column' => 'id'];

            break;
            case "INSTMOUUPLOAD":
                //Arrear Instalment MoU Upload
            $return = ['table' => 'portal.installment_requests', 'column' => 'id'];

            break;

            default:
                //Notification & Claim
            $return = ['table' => 'notification_reports', 'column' => 'notification_staging_cv_id'];
            break;


        }
        return $return;
    }

    /**
     * @param $group
     * @param null $table
     * @param null $column
     * @return mixed
     */
    public function getAllocatedActiveUserCount($group, $reference = NULL)
    {
        $resourceTable = $this->getResourceTable($reference);
        $count = $this->query()
            ->where('checkers.status', 0)
            ->join("{$resourceTable['table']}", function ($join) use ($resourceTable) {
                $join->on(DB::raw("cast(checkers.comments->>'currentStageCvId' as BIGINT)"), '=', "{$resourceTable['table']}.{$resourceTable['column']}")->on("checkers.resource_id", "=", "{$resourceTable['table']}.id");
            })
            ->whereRaw("cast(checkers.comments->>'currentStageCvId' as bigint) = {$group}")
            ->whereIn("checkers.user_id", access()->allUsers())
            ->count();

        /*if (!$count) {
            $count = (new NotificationReportRepository())->query()->whereIn("id", function ($query) {
                $query->select(["resource_id"])->from("checkers")->where("checkers.status", 0)->where("resource_type", "App\Models\Operation\Claim\NotificationReport")->whereIn("user_id", access()->allUsers());
            })->where("notification_staging_cv_id", $group)->count();
        }*/
        return $count;
    }

    /**
     * @param null $reference
     * @return array
     */
    public function getAllocatedActiveUser($reference = NULL)
    {
        $groups = $this->queryAllocatedActiveUser($reference);
/*        switch ($reference) {
            case 'CCONTFCAPPCTN':
                //Online Notification Application
                $codeValRepo = new CodeValuePortalRepository();
                break;
            default:
                $codeValRepo = new CodeValueRepository();
                break;
            }*/
            $codeValRepo = new CodeValueRepository();
            $groupSummary = [];
            foreach ($groups as $group) {
            //$count = $this->getAllocatedActiveUserCount($group->id, $reference);
                $count = $group->value_count;
                switch (true) {
                    case ($codeValRepo->checkHasPendingCheckerNeedsNotifyStage($group->id) > 0):
                    case ($group->id == $codeValRepo->NSPPRMC()):
                    $class = 'blink';
                    break;
                    default:
                    $class = 'default';
                    break;
                }
                $groupSummary[] = ['id' => $group->id, 'name' => $group->name, 'count' => number_0_format($count), 'class' => $class,];
            }
            return $groupSummary;
        }

    /**
     * @param null $reference
     * @return mixed
     */
    public function queryAllocatedActiveUser($reference = NULL)
    {
        switch ($reference) {
            case 'CCONTFCAPPCTN':
                //Online Notification Application
            $codeValueRepo = new CodeValuePortalRepository();
            $codes = [3];
            break;
            default:
            $codeValueRepo = new CodeValueRepository();
            $codes = (new CodeValueCodeRepository())->query()->where('reference', $reference)->pluck('code_id')->all();
            break;
        }
        $resourceTable = $this->getResourceTable($reference);

        /*        $stages = $codeValueRepo->query()->select(['id', 'name'])->whereIn("id", function ($query) {
                    $query->select([DB::raw("cast(comments->>'currentStageCvId' as bigint)")])
                        ->from('checkers')
                        ->where('checkers.status', 0)
                        ->whereIn('checkers.user_id', access()->allUsers());
                })->whereIn('code_id', $codes)->orderBy('sort', 'asc')->get();*/
        $stages = $codeValueRepo->query()

                    ->select([DB::raw('code_values.id as id'), DB::raw('code_values.name as name'), DB::raw('count(ck.id) as value_count')])
                    ->join(DB::raw('main.checkers as ck'), DB::raw("cast(ck.comments->>'currentStageCvId' as BIGINT)"), '=', 'code_values.id')
                    ->join((string)($resourceTable['table']), function ($join) use ($resourceTable) {
                        $join->on(DB::raw("cast(ck.comments->>'currentStageCvId' as BIGINT)"), '=', "{$resourceTable['table']}.{$resourceTable['column']}")->on('ck.resource_id', '=', "{$resourceTable['table']}.id");
                    })
                    ->where('ck.status', 0)
                    ->whereIn('ck.user_id', access()->allUsers())
                    ->whereIn('code_id', $codes)
                    ->groupBy(['code_values.id', 'code_values.name'])
                    ->orderBy(DB::raw('count(ck.id)'), 'desc')
                    ->get();
        //logger($stages);

        return $stages;
    }


    /**
     * @param int $checkvalidated
     * @return array
     */
    public function getBenefitReadyActiveUser($checkvalidated = 1)
    {
        $benefits = (new BenefitTypeClaimRepository())->getClaimBenefits();
        $groupSummary = [];
        foreach ($benefits as $benefit) {
            $column = "";
            $class = "default";
            $query = (new NotificationReportRepository())->query()->whereHas("checkers", function ($query) {
                $query->where("checkers.status", 0)->whereIn("user_id", access()->allUsers());
            });
            $initiated_check = NULL;
            switch ($benefit->id) {
                case 1:
                    //MAE Refund (Employee/Employer)
                $column = "incident_mae.mae_refund_member_ready";
                $query->join("incident_mae", "incident_mae.id", "=", "notification_reports.incident_mae_id");
                break;
                case 3:
                    //Temporary Disablement
                $column = "incident_tds.td_ready";
                $query->join("incident_tds", "incident_tds.id", "=", "notification_reports.incident_td_id");
                break;
                case 4:
                    //Temporary Disablement Refund
                $column = "incident_tds.td_refund_ready";
                $query->join("incident_tds", "incident_tds.id", "=", "notification_reports.incident_td_id");
                break;
                case 5:
                    //Permanent Disablement
                $column = "pd_ready";
                $initiated_check = "notification_reports.pd_eligible_id";
                break;
                case 7:
                    //Survivors
                $column = "survivor_ready";
                $initiated_check = "notification_reports.survivor_eligible_id";
                break;
            }
            if (!is_null($initiated_check)) {
                $query->whereNull("{$initiated_check}");
            }
            if ($checkvalidated) {
                $count = $query->where($column, "1")->where("progressive_stage", ">=", 4)->count();
            } else {
                $count = $query->where($column, "1")->where("progressive_stage", "<", 4)->count();
            }


            if ($count) {
                $groupSummary[] = ['id' => $benefit->id, 'name' => $benefit->name, 'count' => number_format($count , 0 , '.' , ',' ), 'class' => $class];
            }
        }
        return $groupSummary;
    }

    /**
     * @return array
     */
    public function getReadyToInitiateActiveUser()
    {
        $codeValue = new CodeValueRepository();
        $groupSummary = $this->getBenefitReadyActiveUser();
        $othersToInitiate = $this->getOthersToInitiate();
        //dd($othersToInitiate);
        foreach ($othersToInitiate as $index => $value) {
            $count = 0;
            $class = "default";
            switch ($index) {
                case 50:
                    //Notification Approval
                $column = "approval_ready";
                $finishedFillingInvestigation = [$codeValue->NSPCRFNFI(), $codeValue->NSOPFRFLI()];
                $count = (new NotificationReportRepository())->query()->whereIn("notification_staging_cv_id", $finishedFillingInvestigation)->where($column, 1)->whereHas("checkers", function ($query) {
                    $query->where("checkers.status", 0)->whereIn("user_id", access()->allUsers());
                })->count();
                $class = "blink";
                break;
            }
            if ($count) {
                $groupSummary[] = ['id' => $index, 'name' => $value, 'count' => number_format($count , 0 , '.' , ',' ), 'class' => $class];
            }
        }
        return $groupSummary;
    }

    /**
     * @return array
     */
    public function getClaimChecklistOverviewActiveUser()
    {
        $groupSummary = [];
        $overviews = $this->getClaimChecklistOverview();
        foreach ($overviews as $index => $value) {
            $count = 0;
            $class = "default";
            switch ($index) {
                case 1:
                    //Complete Document Not Validated
                foreach ($this->getBenefitReadyActiveUser(0) as $data) {
                    $data['description'] = 'complete_document_not_validated';
                    $data['type'] = '1';
                    $groupSummary[] = $data;
                }
                break;
                case 2:
                    //Acknowledgement Letter Not Initiated
                $class = "blink";
                $count = (new NotificationReportRepository())->query()->whereIn("is_acknowledgment_initiated", [0])->whereHas("checkers", function ($query) {
                    $query->where("checkers.status", 0)->whereIn("user_id", access()->allUsers());
                })->count();
                if ($count) {
                    $groupSummary[] = ['id' => $index, 'name' => $value, 'count' => number_format($count , 0 , '.' , ',' ), 'class' => $class, 'description' => 'acknowledgement_letter_not_initiated', 'type' => '2'];
                }
                break;
            }
        }
        return $groupSummary;
    }

    /**
     * @return array
     */
    public function getReadyToInitiate()
    {
        $benefits = (new BenefitTypeClaimRepository())->getClaimBenefits()->pluck("name", "id")->all();
        $othersToInitiate = $this->getOthersToInitiate();
        return $benefits + $othersToInitiate;
    }

    /**
     * @return string[]
     */
    public function getClaimChecklistOverview()
    {
        //overview types ...
        return [
            1 => 'Complete Document Not Validated',
            2 => 'Acknowledgement Letter Not Initiated',
        ];
    }

    /**
     * @return array
     * @description
     */
    public function getOthersToInitiate()
    {
        $array = ["50" => "Notification Approval"];
        return $array;
    }

}