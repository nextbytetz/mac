<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureReopen;
use App\Models\Operation\Compliance\Member\EmployerParticularChange;
use App\Models\Operation\Compliance\Member\StaffEmployer;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerParticularChangeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\BaseRepository;
use App\Services\Finance\ProcessPaymentPerBenefit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class GeneralRectificationRepository extends  BaseRepository
{


    /**
     * Rectify interest says paid but receipt already cancelled or do not exist
     */
    public function rectifyInterestPaidButReceiptNotExists()
    {
        $booking_interests = BookingInterest::query()->select('booking_interests.id')
            ->join('bookings as b', function($join){
                $join->on('b.id', 'booking_interests.booking_id')->whereNull('b.deleted_at');
            })   ->join('employers as e', function($join){
                $join->on('e.id', 'b.employer_id')->whereNull('e.deleted_at');
            })->leftJoin('receipt_codes as c', function($join){
                $join->on('c.booking_id', 'booking_interests.booking_id')->where('c.fin_code_id',1);
            })->leftJoin('receipts as r', function ($join){
                $join->on('r.id', 'c.receipt_id')->where('r.iscancelled', 0)->where('isdishonoured', 0)->whereNull('r.deleted_at');
            })->where('booking_interests.ispaid', 1)->whereNull('c.id')->whereNull('booking_interests.deleted_at')->get();

        foreach($booking_interests as $booking_interest_id)
        {
            $interest = BookingInterest::query()->find($booking_interest_id->id);
            $interest->update([
                'ispaid' => 0,
                'updated_at' => Carbon::now()
            ]);
        }
    }


    /**
     * Rectify Interest which booking were paid on time
     */
    public function rectifyInterestWhichBookingsWerePaidOnTime()
    {
        $booking_interests = BookingInterest::query()->select('booking_interests.id')
            ->join('bookings_mview as b', function($join){
                $join->on('b.booking_id', 'booking_interests.booking_id');
            })   ->join('employers as e', function($join){
                $join->on('e.id', 'b.employer_id')->whereNull('e.deleted_at');
            })->join('receipt_codes as c', function($join){
                $join->on('c.booking_id', 'booking_interests.booking_id')->where('c.fin_code_id',2);
            })->join('receipts as r', function ($join){
                $join->on('r.id', 'c.receipt_id')->where('r.iscancelled', 0)->where('isdishonoured', 0)->whereNull('r.deleted_at');
            })->whereNull('booking_interests.deleted_at')
            ->where('booking_interests.amount','>', 0)
            ->whereRaw("((DATE_PART('year', b.first_payment_date::date) - DATE_PART('year', b.rcv_date::date)) * 12 +
              (DATE_PART('month', b.first_payment_date::date) - DATE_PART('month', b.rcv_date::date))) < 2")->get();


        foreach($booking_interests as $booking_interest_id)
        {
            $interest = BookingInterest::query()->find($booking_interest_id->id);
            $interest->update([
                'late_months' => 0,
                'amount' => 0,
                'updated_at' => Carbon::now()
            ]);
        }

    }



    /*Update employer is on staff employers followups for earlier data with null on employer id*/
    public function rectifyStaffEmployerEmployerId()
    {
        $follow_ups_no_employer_id = StaffEmployerFollowUp::query()->whereNull('employer_id')->get();
        foreach ($follow_ups_no_employer_id as $followup){
            $staff_employer = DB::table('staff_employer')->where('id', $followup->staff_employer_id)->first();
            $followup->update([
                'employer_id' => $staff_employer->employer_id
            ]);
        }
    }


    /*Rectify Staff Allocation Id in wrong category for staff in debt mgt*/
    public function rectifyStaffAllocationIdForStaffInDebtMgt()
    {
        $staff_employer_repo = new StaffEmployerRepository();
        $config =$staff_employer_repo->staffEmployerConfig();
        $compliance_users = unserialize($config->compliance_officers);
        $intern = unserialize($config->intern_staff);
        $users = array_merge($compliance_users, $intern);
        foreach ($users as $user)
        {
            $check_if_intern =  $staff_employer_repo->checkIfStaffIsIntern($user);
            $collection_allocation = $staff_employer_repo->getMatchingStaffEmployerAllocationCollectionByStaff($user);
            StaffEmployer::query()
                ->join('staff_employer_allocations as s', 's.id', 'staff_employer.staff_employer_allocation_id')
                ->whereIn('s.category', [1,2,5])
                ->where('s.isactive',1)
                ->where('staff_employer.isactive',1)
                ->where('staff_employer.user_id', $user)->update([
                    'staff_employer_allocation_id' => $collection_allocation->allocation_id,
                    'isintern' => ($check_if_intern) ? 1 : 0
                ]);

        }

        $allocations = StaffEmployerAllocation::query()->whereIn('category', [1,2,5])->where('isactive',1)->get();
        foreach($allocations as $allocation) {
            $assigned_users = unserialize($allocation->selected_staff);
            $dormant_users = [];
            foreach ($assigned_users as $user) {
                $employers = StaffEmployer::query()->where('user_id', $user)->where('staff_employer_allocation_id', $allocation->id)->where('staff_employer.isactive', 1)->count();
                if ($employers == 0) {
                    $dormant_users[] = $user;
                }
            }
            $assigned_users = array_diff($assigned_users, $dormant_users);



            /*update users*/
            if (count($assigned_users) == 0) {

                $assigned_users = StaffEmployer::query()->where('staff_employer_allocation_id', $allocation->id)->where('staff_employer.isactive', 1)->groupBy('user_id')->pluck('user_id')->toArray();

            }
            $allocation->update([
                'selected_staff' => serialize($assigned_users),
                'no_of_staff' => count($assigned_users),
                'no_of_employers' =>$allocation->no_of_employers_allocated
            ]);
        }

    }


    /**
     * Rectify employer closure reopens which did not update status
     */
    public function rectifyEmployerClosureReopenButNotUpdatedEmployerStatus()
    {
        $reopens = EmployerClosureReopen::query()->whereHas('employer', function($q){
            $q->where('employer_status', 3);
        })->where('employer_closure_reopens.wf_done', 1)->get();


        foreach($reopens as $reopen){
            $employer = $reopen->employer;
            $open_date = $reopen->open_date;

            $closure_after = EmployerClosure::query()->where('employer_id', $employer->id)->where('close_date', '>', $open_date)->count();

            if($closure_after > 0){
                /*Skipped if has another closure*/

            }else{
                /*reupdate wf complete of reopen*/
                (new EmployerClosureReopenRepository())->updateOnWorkflowComplete($reopen->id);
            }
        }
    }


    /**
     * undone particular change with active wf tracks
     */
    public function rectifyEmployerParticularChangeUndoneButWfTrackNotDeactivated()
    {
        $trashed_particular_change = (new EmployerParticularChangeRepository())->query()->onlyTrashed()->has('wfTracks')->get();
        foreach ($trashed_particular_change as $particular_change)
        {
            $particular_change->wfTracks()->delete();
        }
    }


    /**
     *
     * Processed for payment but not visible in claims payable i.e. Funeral grants
     * Claims compensation not pushed into claim payable funeral grants
     */
    public function rectifyClaimCompensationNotPushedIntoClaimPayableFuneralGrants()
    {
        $claim_compensations = (new ClaimCompensationRepository())->query()->whereNull('notification_eligible_benefit_id')->where('benefit_type_id', 2)->doesntHave('paymentVoucherTransaction')->get();
        foreach($claim_compensations as $claim_compensation){
            $claim = $claim_compensation->claim;
            $notification_report_id = $claim->notification_report_id;
            $eligible = (new NotificationEligibleBenefitRepository())->query()->where('notification_report_id', $notification_report_id)->where('benefit_type_claim_id', 7)->where('ispayprocessed',1)->where('ispaid',0)->first();

            if($eligible){

                $claim_compensation->update([
                    'notification_eligible_benefit_id' => $eligible->id
                ]);

                $workflow = $eligible->workflow;
                $payment = new ProcessPaymentPerBenefit($workflow->id, $claim_compensation->user_id);
                $payment->processPayment();
            }

        }

    }


    /**
     * Rectify duplicate entries on claims payable
     * Remove duplicate entries
     */
    public function rectifyDuplicateEntriesOnClaimsPayableFuneralGrants()
    {
        $claim_compensations = (new ClaimCompensationRepository())->query()->whereNotNull('notification_eligible_benefit_id')->where('benefit_type_id', 2)->whereHas('paymentVoucherTransaction', function($q){
            $q->whereHas('claimPayable', function($q){
                $q->where('status_code', '200');
            });
        })->get();

        foreach($claim_compensations as $claim_compensation){
            $pv_trans_duplicate = (new PaymentVoucherTransactionRepository())->query()->where('benefit_resource_id', $claim_compensation->claim_id)->where('member_type_id',$claim_compensation->member_type_id)->where('benefit_type_id', 2)->where('resource_id', $claim_compensation->resource_id)->has('claimPayable')->get();
            if($pv_trans_duplicate->count() > 1){
                foreach($pv_trans_duplicate as $pv_tran)
                {
                    $claim_compensation = $pv_tran->claimCompensation;
                    $pv_trans_duplicate_recheck = (new PaymentVoucherTransactionRepository())->query()->where('benefit_resource_id', $claim_compensation->claim_id)->where('member_type_id',$claim_compensation->member_type_id)->where('benefit_type_id', 2)->where('resource_id', $claim_compensation->resource_id)->has('claimPayable')->count();

                    if($claim_compensation->benefit_type_id == 2  && $pv_trans_duplicate_recheck > 1){
                        $claim_payable = $pv_tran->claimPayable;
                        if($claim_payable){

                            if($claim_payable->status_code == '200'){
                                /*claims payable*/
                                $pv_tran->claimPayable->delete();


                                /*pv tran*/
                                $pv_tran->update([
                                    'deleted_at' => Carbon::now(),
                                ]);

                                $claim_compensation->update([
                                    'deleted_at' => Carbon::now(),
                                ]);

                            }

                        }

                    }

                }
            }
        }


    }


    /**
     * Rectify funeral grants on claim payable
     */
    public function rectifyFuneralGrantsOnClaimPayable()
    {
        $this->rectifyClaimCompensationNotPushedIntoClaimPayableFuneralGrants();

        $this->rectifyDuplicateEntriesOnClaimsPayableFuneralGrants();
    }


    /**
     * Rectify wftrack which already undone but still have wf tracks - employer particular change
     */
    public function rectifyRemoveWfTrackWithDeletedResourceEmployerParticularChange()
    {
        $trashed_particular_changes = EmployerParticularChange::query()->onlyTrashed()->whereHas('wfTracks')->get();
        foreach($trashed_particular_changes as $trashed_particular_change)
        {
            (new EmployerParticularChangeRepository())->undo($trashed_particular_change);
        }
    }

}