<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\Office;
use App\Repositories\BaseRepository;

class OfficeRepository extends BaseRepository
{
    const MODEL = Office::class;
}