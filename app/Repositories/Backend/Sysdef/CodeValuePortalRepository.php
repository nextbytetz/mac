<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\CodeValuePortal;
use App\Repositories\BaseRepository;

class CodeValuePortalRepository extends BaseRepository
{
    const MODEL = CodeValuePortal::class;

    /**
     * @param null $reference
     * @return mixed
     * @deprecated
     */
    public function checkHasPendingCheckerNeedsNotify($reference = NULL)
    {
        $query =  $this->query()->whereHas('checkers', function ($query) use ($reference) {
            $query->whereIn('user_id', access()->allUsers())->where('main.checkers.status', 0)->where('main.checkers.notify', 1);
        });
        if ($reference) {
            $query->where('code_values.reference', $reference);
        }
        $count = $query->count();
        logger($reference . ' : ' . $count);
        return $count;
    }

    public function getEmployerId()
    {
        $return = $this->query()->select(['id'])->where("code_id", 1)->where("reference", "EMPLY")->first();
        return $return->id;
    }

    public function getEmployeeId()
    {
        $return = $this->query()->select(['id'])->where("code_id", 1)->where("reference", "EMPLEE")->first();
        return $return->id;
    }

    public function ISREGSTR()
    {
        //Incident Registration
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISREGSTR")->first();
        return $return->id;
    }

    public function ISUPSUPDOC()
    {
        //Pending Supporting Documents
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISUPSUPDOC")->first();
        return $return->id;
    }

    public function ISUPSUDCRVWD()
    {
        //Pending Supporting Documents (Officer Reviewed)
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISUPSUDCRVWD")->first();
        return $return->id;
    }

    public function ISINCAPPRL()
    {
        //Pending Incident Acknowledgement
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISINCAPPRL")->first();
        return $return->id;
    }

    public function ISINCAPRVD()
    {
        //Incident Approved
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISINCAPRVD")->first();
        return $return->id;
    }

    public function ISINCDCNDMBRI()
    {
        //Incident Declined & Merged with Branch Registered Incident
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISINCDCNDMBRI")->first();
        return $return->id;
    }

    public function ISINCDECLND()
    {
        //Incident Declined
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISINCDECLND")->first();
        return $return->id;
    }

    public function ISCANCLLED()
    {
        //Incident Cancelled
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISCANCLLED")->first();
        return $return->id;
    }

    public function ISPEBEDOC()
    {
        //Pending Benefit Documents
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISPEBEDOC")->first();
        return $return->id;
    }

    public function ISCANCBYUSR()
    {
        //Cancelled By User
        $return = $this->query()->select(['id'])->where("code_id", 3)->where("reference", "ISCANCBYUSR")->first();
        return $return->id;
    }

    public function getAccountDeactivateReasonsForSelect()
    {
        return $this->query()->select(['id', 'name'])->where("code_id", 4)->get()->pluck("name", "id")->all();
    }

    public function ADNPEAUTHLETTR()
    {
        //Not Proven Employer Authorization Letter
        $return = $this->query()->select(['id'])->where("code_id", 4)->where("reference", "ADNPEAUTHLETTR")->first();
        return $return->id;
    }

    public function AVACCVLDT()
    {
        //Account Validated
        $return = $this->query()->select(['id'])->where("code_id", 5)->where("reference", "AVACCVLDT")->first();
        return $return->id;
    }

    public function getCodeValuesStageForSelect(array $input)
    {
        $query = $this->query()->select(['id', 'name'])->whereIn("code_id", $input)->where("is_active", 1)->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }

}