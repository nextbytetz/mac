<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\CodeValueCode;
use App\Repositories\BaseRepository;

class CodeValueCodeRepository extends BaseRepository
{
    const MODEL = CodeValueCode::class;

}