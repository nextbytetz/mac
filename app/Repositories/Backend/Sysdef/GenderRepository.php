<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\Gender;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

class GenderRepository extends  BaseRepository
{

    const MODEL = Gender::class;

    public function __construct()
    {

    }

//find or throwException for job title
    public function findOrThrowException($id)
    {
        $gender = $this->query()->find($id);

        if (!is_null($gender)) {
            return $gender;
        }
        throw new GeneralException(trans('exceptions.backend.sysdef.gender_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}