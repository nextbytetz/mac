<?php

namespace App\Repositories\Backend\Sysdef;

use App\Repositories\BaseRepository;
use App\Models\Location\Region;

class RegionRepository extends BaseRepository
{
    const MODEL = Region::class;

    public function getForSelect()
    {
        return $this->query()->orderBy('name', 'asc')->get()->pluck('name', 'id');
    }

    /**
     * @return array
     */
    public function getWithDistrictForSelect(): array
    {
        $regions = $this->query()->with('districts')->get();
        $items = [];
        foreach ($regions as $region) {
            $districts = $region->districts;
            $items[$region->name] = $districts->pluck("name", "id")->all();
        }
        return $items;
    }

    /**
     * @return array
     */
    public function getWithDistrictForSearch(): array
    {
        $regions = $this->query()->with('districts')->get();
        $data = [];
        $items = [];
        $districtCount = 0;
        foreach ($regions as $region) {
            $districts = $region->districts;
            $items[] = [
                "text" => $region->name,
                "children" => $districts->pluck("name", "id")->all(),
            ];
            $districtCount += $districts->count();
        }
        $data['total_count'] = count($regions->count()) + $districtCount;
        $data['items'] = $items;
        return $data;
    }

    public function getHqRegion()
    {
        return 1;
    }

}