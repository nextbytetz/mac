<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\JobTitle;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

class JobTitleRepository extends  BaseRepository
{

    const MODEL = JobTitle::class;

    public function __construct()
    {

    }

//find or throwexception for job title
    public function findOrThrowException($id)
    {
        $job_title = $this->query()->find($id);

        if (!is_null($job_title)) {
            return $job_title;
        }
        throw new GeneralException(trans('exceptions.backend.sysdef.job_title_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}