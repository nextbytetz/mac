<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\DateRange;
use Carbon\Carbon;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

class DateRangeRepository extends  BaseRepository
{

    const MODEL = DateRange::class;

    public function __construct()
    {

    }

//find or throwException for job title
    public function findOrThrowException($id)
    {
        $gender = $this->query()->find($id);

        if (!is_null($gender)) {
            return $gender;
        }
        throw new GeneralException(trans('exceptions.backend.sysdef.date_range_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }

    /*
  * Update or Create new entry
  */

    public function update0rCreate($input) {

        $date_range = $this->query()->updateOrCreate(
            ['user_id' => access()->user()->id],
            ['from_date' => array_key_exists('from_date', $input) ? (!is_null($input['from_date']) ? $input['from_date'] : Carbon::now()) : Carbon::now(),
            'to_date' => array_key_exists('to_date', $input) ? (!is_null($input['to_date']) ? $input['to_date'] : Carbon::now() ) : Carbon::now(),
            'bank_id'=> array_key_exists('bank_id', $input) ? (!is_null($input['bank_id']) ? $input['bank_id'] : 0 ) : 0,
            'payment_type_id' => array_key_exists('payment_type_id', $input) ? (!is_null($input['payment_type_id']) ? $input['payment_type_id'] : 0 ) : 0,
            'employer_id' => array_key_exists('employer_id', $input) ? (!is_null($input['employer_id']) ? $input['employer_id'] : 0 ) : 0,
                'employee_id' => array_key_exists('employee_id', $input) ? (!is_null($input['employee_id']) ? $input['employee_id'] : 0 ) : 0,
                'insurance_id' => array_key_exists('insurance_id', $input) ? (!is_null($input['insurance_id']) ? $input['insurance_id'] : 0 ) : 0,
                'dependent_id' => array_key_exists('dependent_id', $input) ? (!is_null($input['dependent_id']) ? $input['dependent_id'] : 0 ) : 0,
                'pensioner_id' => array_key_exists('pensioner_id', $input) ? (!is_null($input['pensioner_id']) ? $input['pensioner_id'] : 0 ) : 0,
                'member_type_id' => array_key_exists('member_type_id', $input) ? (!is_null($input['member_type_id']) ? $input['member_type_id'] : 0 ) : 0,
                'resource_id' => array_key_exists('resource_id', $input) ? (!is_null($input['resource_id']) ? $input['resource_id'] : 0 ) : 0,
            'incident_type_id' => array_key_exists('incident_type_id', $input) ? (!is_null($input['incident_type_id']) ? $input['incident_type_id'] : 0 ) : 0,
         'payroll_proc_id' => array_key_exists('payroll_proc_id', $input) ? (!is_null($input['payroll_proc_id']) ? $input['payroll_proc_id'] : 0 ) : 0,
                'region_id' => array_key_exists('region_id', $input) ? (!is_null($input['region_id']) ? $input['region_id'] : 0 ) : 0,
                'business_id' => array_key_exists('business_id', $input) ? (!is_null($input['business_id']) ? $input['business_id'] : 0 ) : 0,
                'status_id' => array_key_exists('status_id', $input) ? (!is_null($input['status_id']) ? $input['status_id'] : -1 ) : -1,
                'type' => array_key_exists('type', $input) ? (!is_null($input['type']) ? $input['type'] : -1 ) : -1,
                'fin_code_id' => array_key_exists('fin_code_id', $input) ? (!is_null($input['fin_code_id']) ? $input['fin_code_id'] : 0 ) : 0,
                'district_id' => array_key_exists('district_id', $input) ? (!is_null($input['district_id']) ? $input['district_id'] : 0 ) : 0,
                'general_type' => array_key_exists('general_type', $input) ? (!is_null($input['general_type']) ? $input['general_type'] : -1 ) : -1,
                'date_type' => array_key_exists('date_type', $input) ? (!is_null($input['date_type']) ? $input['date_type'] : -1 ) : -1,
                'category' => array_key_exists('category', $input) ? (!is_null($input['category']) ? $input['category'] : -1 ) : -1,
                'office_zone_id' => array_key_exists('office_zone_id', $input) ? (!is_null($input['office_zone_id']) ? $input['office_zone_id'] : 0 ) : 0,
            ]);

        return $date_range;
    }


    //cache date range + associcated attributes / fields
    public function dateRange($input){

        if (array_key_exists('search_flag', $input)){

                   $date_range = $this->update0rCreate($input);
        }else{
            $date_range = ($this->getdateRange()) ? $this->getdateRange() : (['from_date' => Carbon::now(),'to_date' => Carbon::now()]);
        }
        return $date_range;
    }


// retrieve date range
    public function getdateRange(){
        return $this->query()->where('user_id',access()->user()->id)->first();
    }





}