<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\Identity;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;

class IdentityRepository extends  BaseRepository
{

    const MODEL = Identity::class;

    public function __construct()
    {

    }

//find or throwException for job title
    public function findOrThrowException($id)
    {
        $identity = $this->query()->find($id);

        if (!is_null($identity)) {
            return $identity;
        }
        throw new GeneralException(trans('exceptions.backend.sysdef.identity_type_not_found'));
    }
    /*
     * create new
     */

    public function create($input) {

    }
    /*
     * update
     */
    public function update($id,$input) {

    }








}