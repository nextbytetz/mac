<?php

namespace App\Repositories\Backend\Sysdef;

use App\Repositories\BaseRepository;
use App\Models\Sysdef\Business;

class BusinessRepository extends BaseRepository
{
    const MODEL = Business::class;

}