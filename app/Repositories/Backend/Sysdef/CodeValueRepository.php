<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\CodeValue;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\DB;

/**
 * Class CodeValueRepository
 * @package App\Repositories\Backend\Sysdef
 */
class CodeValueRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = CodeValue::class;

    public function __construct()
    {

    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $code_value= $this->query()->find($id);
        if (!is_null($code_value)) {
            return $code_value;
        }
        throw new GeneralException(trans('exceptions.backend.system.code_value_not_found'));
    }

    /*Query only active*/
    public function queryOnlyActive()
    {
        return $this->query()->where('is_active', 1);
    }

    /**
     * @param $input
     * @return mixed
     */
    public function create($input) {
        $code_value = $this->query()->create($input);
        return $code_value;
    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function update($id,$input) {
        $code_value = $this->findOrThrowException(($id));
        return $code_value->update($input);

    }

    /**
     * @param $reference
     * @return mixed
     */
    public function findByReference($reference)
    {
        $code_value = $this->query()->where('reference', $reference)->first();
        return $code_value;
    }

    /**
     * @param $reference
     * @return mixed
     */
    public function findIdByReference($reference)
    {
        $code_value = $this->findByReference($reference);
        return $code_value->id;
    }


    /**
     * @param $reference
     * @return mixed
     * Find Ids by array
     */
    public function findIdsByReferences(array $references)
    {

        $ids = [];
        foreach ($references as $reference){
            $id = $this->findIdByReference($reference);
            array_push($ids, $id);
        }

        return $ids;
    }

    /**
     * @param $reference
     * @return mixed
     * Get refrence by id
     */
    public function reference($cv_id)
    {
        $code_value = $this->find($cv_id);
        return ($code_value) ? $code_value->reference : null;
    }

    /**
     * @return mixed
     */
    public function getFollowupTypePhoneCall()
    {
        //FUPHONC
        $return = $this->query()->select(['id'])->where("code_id", 9)->where("reference", "FUPHONC")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function getFollowupTypeEmail()
    {
        //FUEMAIL
        $return = $this->query()->select(['id'])->where("code_id", 9)->where("reference", "FUEMAIL")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function getFollowupTypeVisit()
    {
        //FUVSIT
        $return = $this->query()->select(['id'])->where("code_id", 9)->where("reference", "FUVSIT")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function getIncidentOccurrenceForSelect()
    {
        $query = $this->query()->select(['name', 'id'])->where("code_id", 12)->get();
        $return = $query->pluck("name", "id");
        return $return;
    }

    public function getPendingCheckerCategories()
    {
        return $this->query()->select(['name', 'reference', 'id'])->where("code_id", 14)->whereHas("checkers", function ($query) {
            $query->whereIn("user_id", access()->allUsers())->where("checkers.status", 0);
        })->get();
    }

    /**
     * @param null $reference
     * @return mixed
     * @deprecated
     */
    public function checkHasPendingCheckerNeedsNotify($reference = NULL)
    {
        $query =  $this->query()->whereHas('checkers', function ($query) use ($reference) {
            $query->whereIn('user_id', access()->allUsers())->where('checkers.status', 0)->where('checkers.notify', 1);
        });
        if ($reference) {
            $query->where('code_values.reference', $reference);
        }

        return $query->count();
    }

    /**
     * @param $stage
     * @return mixed
     */
    public function checkHasPendingCheckerNeedsNotifyStage($stage)
    {
        return   $this->query()
        ->join(DB::raw('main.checkers as ck'), DB::raw("cast(ck.comments->>'currentStageCvId' as BIGINT)"), '=', 'code_values.id')
        ->whereIn('user_id', access()->allUsers())->where('ck.status', 0)->where('ck.notify', 1)
        ->where('code_values.id', $stage)
        ->count();
    }


    /**
     * @param $code_id
     * @return mixed
     * Get code values by code id for select
     */
    public function getCodeValuesByCodeForSelect($code_id)
    {
        $query = $this->query()->select(['id', 'name'])->where("code_id", $code_id)->where("is_active", 1)->orderBy('name')->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }


    /**
     * @param $code_id
     * @return mixed
     * Get code values by code id for select
     */
    public function getCodeValuesReferenceByCodeForSelect($code_id)
    {
        $query = $this->query()->select(['reference', 'name'])->where("code_id", $code_id)->where("is_active", 1)->get();
        $return = $query->pluck('name', 'reference');
        return $return;
    }

    public function getCodeForSelectFilteredIds($code_id, array $filter)
    {
        $query = $this->query()->select(['id', 'name'])->where('is_active', 1)->where("code_id", $code_id)->whereIn("id", $filter)->orderBy('name')->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }

    public function getCodeForSelectFilteredReferences($code_id, array $filter)
    {
        $query = $this->query()->select(['id', 'name'])->where('is_active', 1)->where("code_id", $code_id)->whereIn("reference", $filter)->orderBy('name')->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }

    public function getCvReferenceForSelectFilteredReferences($code_id, array $filter)
    {
        $query = $this->query()->select(['reference', 'name'])->where('is_active', 1)->where("code_id", $code_id)->whereIn("reference", $filter)->orderBy('name')->get();
        $return = $query->pluck('name', 'reference');
        return $return;
    }


    public function getCodeReferencesForSelectFilteredNotInReferences($code_id, array $filter)
    {
        $query = $this->query()->select(['reference', 'name'])->where('is_active', 1)->where("code_id", $code_id)->whereNotIn("reference", $filter)->orderBy('name')->get();
        $return = $query->pluck('name', 'reference');
        return $return;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function getCodeValuesByCodeArrForSelect(array $input)
    {
        $query = $this->query()->select(['id', 'name'])->whereIn("code_id", $input)->where("is_active", 1)->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }

    public function getCodeValuesNotificationStageForSelect()
    {
        $query = $this->query()->select(['id', 'name'])->whereIn("code_id", [13, 23, 24])->where("is_active", 1)->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }

    public function getCodeValuesEmployerInspectionStageForSelect()
    {
        $query = $this->query()->select(['id', 'name'])->whereIn("code_id", [38, 39])->where("is_active", 1)->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }

    public function getCodeValuesStageForSelect(array $input)
    {
        $query = $this->query()->select(['id', 'name'])->whereIn("code_id", $input)->where("is_active", 1)->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }

    public function getCodeValuesInspectionStageForSelect()
    {
        $query = $this->query()->select(['id', 'name'])->whereIn("code_id", [33, 34, 35])->where("is_active", 1)->get();
        $return = $query->pluck('name', 'id');
        return $return;
    }

    /**
     * @param $reference
     * @return mixed
     */
    public function getAllUsersByReference($reference)
    {
        $return = $this->query()->where("reference", $reference)->first();
        return $return->users()->pluck("users.id")->all();
    }

    /**
     * @param $reference
     * @return mixed
     */
    public function getAllUsersForSelectByReference($reference)
    {
        $return = $this->query()->where("reference", $reference)->first();
        return $return->users()->select([DB::raw("concat_ws(' ', users.firstname, users.lastname) fullname"), "users.id"])->pluck("fullname", "users.id")->all();
    }

    /**
     * @return mixed
     */
    public function NSNREJSYBS()
    {
        //Notification Rejection by System (Before the WCF Statutory Period)
        //System Rejected Notification
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "NSNREJSYBS")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSNREJSYOT()
    {
        //Notification Rejection by System (Out of Time)
        //System Rejected Notification
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "NSNREJSYOT")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSONNREJSYW()
    {
        //On Notification Rejection by System Workflow
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "NSONNREJSYW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSAPRDNREJSYW()
    {
        //Approved (Rejection by System) Workflow
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "NSAPRDNREJSYW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSPPRUM()
    {
        //Pending Partial Registration (Unregistered Member)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSPPRUM")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function ONSPPRUMW()
    {
        //On Pending Partial Registration (Unregistered Member) Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "ONSPPRUMW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSPPRMC()
    {
        //Pending Partial Registration (Missing Contribution)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSPPRMC")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function ONSPPRMCW()
    {
        //On Pending Partial Registration (Missing Contribution) Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "ONSPPRMCW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function ONSPPRICW()
    {
        //On (Incorrect Contribution Amount) Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "ONSPPRICW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function ONSPPRIEIFW()
    {
        //On Pending Partial Registration (Incorrect Employee Info) Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "ONSPPRIEIFW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSPCRFC()
    {
        //Pending Complete Registration (Filling Checklist)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSPCRFC")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSPCRFI()
    {
        //Pending Complete Registration (Filling Investigation)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSPCRFI")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSPCRFNFI()
    {
        //Pending Complete Registration (Finished Filling Investigation)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSPCRFNFI")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSOPNAP()
    {
        //On Progress (Notification Approval Workflow)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSOPNAP")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSOTRTDEWRK()
    {
        //On Transferring to Death Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSOTRTDEWRK")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSOPIA()
    {
        //On Progress (Incident Approved)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSOPIA")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSOPRFLI()
    {
        //On Progress (Re-Filling Investigation)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSOPRFLI")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSOPFRFLI()
    {
        //On Progress (Finished Re-Filling Investigation)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSOPFRFLI")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSRINC()
    {
        //Rejected Incident from Notification Approval
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "NSRINC")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function ONSRINCW()
    {
        //On Rejected Incident from Notification Approval Workflow
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "ONSRINCW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function APRDNSRINCW()
    {
        //Approved Rejection from Notification Approval Workflow
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "APRDNSRINCW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSOCBW()
    {
        //On Claim Benefit Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSOCBW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSOPALP()
    {
        //On Progress (At least Payed)
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSOPALP")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSCINC()
    {
        //Closed Incident
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSCINC")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSRASSM()
    {
        //On Reassessment
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSRASSM")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSCSYSREJ()
    {
        //Closed Rejected Incident By System
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "NSCSYSREJ")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSCAPPRREJ()
    {
        //Closed Rejected Incident from Notification Approval
        $return = $this->query()->select(['id'])->whereIn("code_id", [13, 23])->where("reference", "NSCAPPRREJ")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSAMAESAW()
    {
        //On Accident MAE Special Approval Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSAMAESAW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSDMAESAW()
    {
        //On Disease MAE Special Approval Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSDMAESAW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSMAEAHCHSW()
    {
        //On MAE Approval for HCP/HSP Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSMAEAHCHSW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSMAERHCHSW()
    {
        //On MAE Refund for HCP/HSP Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSMAERHCHSW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSMAERMEMW()
    {
        //On MAE Refund for Employee/Employer Workflow
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSMAERMEMW")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSATLPMAN()
    {
        //At least Paid Manually
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSATLPMAN")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSARCHIVED()
    {
        //Incident Archived
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSARCHIVED")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NSARCHVDNDROPN()
    {
        //Archived, Needs Re-open
        $return = $this->query()->select(['id'])->where("code_id", 13)->where("reference", "NSARCHVDNDROPN")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NCOVDNOTWRK()
    {
        //On Void Notification Workflow
        $return = $this->query()->select(['id'])->where("code_id", 24)->where("reference", "NCOVDNOTWRK")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NCVOIDNOTFCN()
    {
        //Void Notification
        $return = $this->query()->select(['id'])->where("code_id", 24)->where("reference", "NCVOIDNOTFCN")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function NRONUNDORJWRK()
    {
        //On Undo Rejection Workflow
        $return = $this->query()->select(['id'])->where("code_id", 23)->where("reference", "NRONUNDORJWRK")->first();
        return $return->id;
    }

    /**
     * @return mixed
     */
    public function CHCNOTN()
    {
        //Notification & Claim
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CHCNOTN")->first();
        return $return->id;
    }

    public function CCAEMPINSPCTNTSK()
    {
        //Employer Inspection Task
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CCAEMPINSPCTNTSK")->first();
        return $return->id;
    }

    public function CCAEMPLYINSPCTN()
    {
        //Inspection Plan
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CCAEMPLYINSPCTN")->first();
        return $return->id;
    }

    public function CCAEMPLYARRER()
    {
        //Employer Arrears
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CCAEMPLYARRER")->first();
        return $return->id;
    }

    public function CCALETTER()
    {
        //Letter
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CCALETTER")->first();
        return $return->id;
    }


    public function CCONTFCAPPCTN()
    {
        //Online Notification Application
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CCONTFCAPPCTN")->first();
        return $return->id;
    }


    public function CHCEMPDEREG()
    {
        //Employer De-registration
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CHCEMPDEREG")->first();
        return $return->id;
    }

    public function CHCPAYALERT()
    {
        //Payroll alert
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CHCPAYALERT")->first();
        return $return->id;
    }

    /**
     * @return mixed
     * @description Get Body Part Injury Count for Checklist Score
     */
    public function getBodyPartInjuryChecklist()
    {
        $return = $this->query()->select(['id'])->where("code_id", 2)->whereIn("reference", ["BPIHEYE", "BPIHOTHR", "BPISPIN", "BPIPTRIN"])->pluck("id")->all();
        return $return;
    }

    public function ACTHGSROBBRY()
    {
        //Thugs, Robbery
        $return = $this->query()->select(['id'])->where("code_id", 15)->where("reference", "ACTHGSROBBRY")->first();
        return $return->id;
    }

    public function MTA($id)
    {
        //
        $return = $this->query()->select(['id'])->where(["code_id" => 15, 'id' => $id])->whereIn("reference", ["ACMTAPASS", "ACMTADRIV", "ACMTAPEDSTR"])->count();
        return $return;
    }

    public function CLACKNOWLGMNT()
    {
        //Notification Acknowledgement Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 26])->whereIn("reference", ["CLACKNOWLGMNT"])->first();
        return $return->id;
    }

    public function CLBNAWLTTR()
    {
        //Benefit Award Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 26])->whereIn("reference", ["CLBNAWLTTR"])->first();
        return $return->id;
    }

    public function CLRMNDRLETTER()
    {
        //Notification Reminder Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 26])->whereIn("reference", ["CLRMNDRLETTER"])->first();
        return $return->id;
    }

    public function CLINNOLTR()
    {
        //Inspection Notice Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 27])->whereIn("reference", ["CLINNOLTR"])->first();
        return $return->id;
    }

    public function CLDNDNOLTR()
    {
        //Demand Notice Letter (Inspection)
        $return = $this->query()->select(['id'])->where(["code_id" => 27])->whereIn("reference", ["CLDNDNOLTR"])->first();
        return $return->id;
    }

    public function CLLTRFCMPLNC()
    {
        //Letter of Compliance (Inspection)
        $return = $this->query()->select(['id'])->where(["code_id" => 27])->whereIn("reference", ["CLLTRFCMPLNC"])->first();
        return $return->id;
    }

    public function CLEMPRMDLTR()
    {
        //Employer Reminder Letter (Inspection)
        $return = $this->query()->select(['id'])->where(["code_id" => 27])->whereIn("reference", ["CLEMPRMDLTR"])->first();
        return $return->id;
    }

    public function CLEMPMOUAGRMNT()
    {
        //Employer MoU Payment Agreement
        $return = $this->query()->select(['id'])->where(["code_id" => 27])->whereIn("reference", ["CLEMPMOUAGRMNT"])->first();
        return $return->id;
    }

    public function CLRFNDOVRPYMNT()
    {
        //Refund Overpayment Confirmation
        $return = $this->query()->select(['id'])->where(["code_id" => 27])->whereIn("reference", ["CLRFNDOVRPYMNT"])->first();
        return $return->id;
    }

    public function CLPYINSTCONF()
    {
        //Payment by Installment Confirmation
        $return = $this->query()->select(['id'])->where(["code_id" => 27])->whereIn("reference", ["CLPYINSTCONF"])->first();
        return $return->id;
    }

    public function OLLLTRCRTD()
    {
        //Letter Created
        $return = $this->query()->select(['id'])->where(["code_id" => 28])->whereIn("reference", ["OLLLTRCRTD"])->first();
        return $return->id;
    }

    public function OLLLTREDTD()
    {
        //Letter Edited
        $return = $this->query()->select(['id'])->where(["code_id" => 28])->whereIn("reference", ["OLLLTREDTD"])->first();
        return $return->id;
    }

    public function OLLLTRPRNTD()
    {
        //Letter Printed
        $return = $this->query()->select(['id'])->where(["code_id" => 28])->whereIn("reference", ["OLLLTRPRNTD"])->first();
        return $return->id;
    }

    public function OLLLTROPEND()
    {
        //Letter Opened
        $return = $this->query()->select(['id'])->where(["code_id" => 28])->whereIn("reference", ["OLLLTROPEND"])->first();
        return $return->id;
    }

    public function ITROUTINE()
    {
        //Routine
        $return = $this->query()->select(['id'])->where(["code_id" => 29])->whereIn("reference", ["ITROUTINE"])->first();
        return $return->id;
    }

    public function ITBLITZ()
    {
        //Blitz
        $return = $this->query()->select(['id'])->where(["code_id" => 29])->whereIn("reference", ["ITBLITZ"])->first();
        return $return->id;
    }

    public function ITSPECIAL()
    {
        //Special
        $return = $this->query()->select(['id'])->where(["code_id" => 29])->whereIn("reference", ["ITSPECIAL"])->first();
        return $return->id;
    }

    public function ITRGPLNDINSPCTN()
    {
        //Planned Inspection
        $return = $this->query()->select(['id'])->where(["code_id" => 30])->whereIn("reference", ["ITRGPLNDINSPCTN"])->first();
        return $return->id;
    }

    public function ITRGSPCEVNT()
    {
        //Specific Event
        $return = $this->query()->select(['id'])->where(["code_id" => 30])->whereIn("reference", ["ITRGSPCEVNT"])->first();
        return $return->id;
    }

    public function ITRGTRGBSYST()
    {
        //Triggered By System
        $return = $this->query()->select(['id'])->where(["code_id" => 30])->whereIn("reference", ["ITRGTRGBSYST"])->first();
        return $return->id;
    }

    public function SICUNRGRDEMPLY()
    {
        //Frequent Change on Number of Employees
        $return = $this->query()->select(['id'])->where(["code_id" => 31])->whereIn("reference", ["SICUNRGRDEMPLY"])->first();
        return $return->id;
    }

    public function SICNCONTREMPLY()
    {
        //Non Contributing Employer
        $return = $this->query()->select(['id'])->where(["code_id" => 31])->whereIn("reference", ["SICNCONTREMPLY"])->first();
        return $return->id;
    }

    public function SICERRNSCONTRBTN()
    {
        //Erroneous Contribution
        $return = $this->query()->select(['id'])->where(["code_id" => 31])->whereIn("reference", ["SICERRNSCONTRBTN"])->first();
        return $return->id;
    }

    public function RICMONTHLY()
    {
        //Inspection Period Monthly
        $return = $this->query()->select(['id'])->where(["code_id" => 32])->whereIn("reference", ["RICMONTHLY"])->first();
        return $return->id;
    }

    public function RICQUARTRLY()
    {
        //Inspection Period Quarterly
        $return = $this->query()->select(['id'])->where(["code_id" => 32])->whereIn("reference", ["RICQUARTRLY"])->first();
        return $return->id;
    }

    public function RICYEARLY()
    {
        //Inspection Period Yearly
        $return = $this->query()->select(['id'])->where(["code_id" => 32])->whereIn("reference", ["RICYEARLY"])->first();
        return $return->id;
    }

    public function IOPISPCRTD()
    {
        //Inspection Created
        $return = $this->query()->select(['id'])->where(["code_id" => 33])->whereIn("reference", ["IOPISPCRTD"])->first();
        return $return->id;
    }

    public function IOPOISPPAW()
    {
        //On Inspection Plan Approval Workflow
        $return = $this->query()->select(['id'])->where(["code_id" => 33])->whereIn("reference", ["IOPOISPPAW"])->first();
        return $return->id;
    }

    public function IOPAPPRVDIPLN()
    {
        //Approved Inspection Plan
        $return = $this->query()->select(['id'])->where(["code_id" => 33])->whereIn("reference", ["IOPAPPRVDIPLN"])->first();
        return $return->id;
    }

    public function IOPPLNVIDNLTR()
    {
        //Planning Visit, Issuing Demand Notice Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 33])->whereIn("reference", ["IOPPLNVIDNLTR"])->first();
        return $return->id;
    }

    public function IRJDCLNDIPLN()
    {
        //Declined Inspection Plan
        $return = $this->query()->select(['id'])->where(["code_id" => 34])->whereIn("reference", ["IRJDCLNDIPLN"])->first();
        return $return->id;
    }

    public function IOPPNDGIASCDL()
    {
        //Pending Inspection Assessment Schedule
        $return = $this->query()->select(['id'])->where(["code_id" => 33])->whereIn("reference", ["IOPPNDGIASCDL"])->first();
        return $return->id;
    }

    public function IOPCOMPLTDIPCN()
    {
        //Completed Inspection
        $return = $this->query()->select(['id'])->where(["code_id" => 33])->whereIn("reference", ["IOPCOMPLTDIPCN"])->first();
        return $return->id;
    }

    public function ICNCANCLDISPPLN()
    {
        //Cancelled Inspection Plan
        $return = $this->query()->select(['id'])->where(["code_id" => 35])->whereIn("reference", ["ICNCANCLDISPPLN"])->first();
        return $return->id;
    }

    public function CDEMPLYIMSR()
    {
        //Employer Inspection Master
        $return = $this->query()->select(['id'])->where(["code_id" => 36])->whereIn("reference", ["CDEMPLYIMSR"])->first();
        return $return->id;
    }

    public function CDAUTHRSDINSPCTR()
    {
        //Authorised Inspectors
        $return = $this->query()->select(['id'])->where(["code_id" => 36])->whereIn("reference", ["CDAUTHRSDINSPCTR"])->first();
        return $return->id;
    }


    public function CDAEMPREOPUS()
    {
        //Officers for Employer Temporary De-registrations Re Open Alert
        $return = $this->query()->select(['id'])->where(["code_id" => 36])->whereIn("reference", ["CDAEMPREOPUS"])->first();
        return $return->id;
    }

    public function NDNTFCTCUSR()
    {
        //Notification Checklist Users
        $return = $this->query()->select(['id'])->where(["code_id" => 37])->whereIn("reference", ["NDNTFCTCUSR"])->first();
        return $return->id;
    }

    public function EITPWAISPLAP()
    {
        //Waiting Inspection Plan Approval
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPWAISPLAP"])->first();
        return $return->id;
    }

    public function EITPPLVDAT()
    {
        //Planing Visit Date
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPPLVDAT"])->first();
        return $return->id;
    }

    public function EITPISINNTLT()
    {
        //Issuing Inspection Notice Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPISINNTLT"])->first();
        return $return->id;
    }

    public function EITPINNTLTCRTD()
    {
        //Inspection Notice Letter Created
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPINNTLTCRTD"])->first();
        return $return->id;
    }

    public function EITPWTIASCHD()
    {
        //Inspection Notice Letter Sent
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPWTIASCHD"])->first();
        return $return->id;
    }

    public function EITPVDPWTIASCHD()
    {
        //Visit Date Postponed, Waiting Inspection Assessment Schedule
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPVDPWTIASCHD"])->first();
        return $return->id;
    }

    public function EITPUPIASSC()
    {
        //Uploaded Inspection Assessment Schedule
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPUPIASSC"])->first();
        return $return->id;
    }

    public function EITPOIASCHRVW()
    {
        //On Inspection Assessment Schedule Review Workflow
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPOIASCHRVW"])->first();
        return $return->id;
    }

    public function EITPCMIASCRVW()
    {
        //Completed Inspection Assessment Schedule Review
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPCMIASCRVW"])->first();
        return $return->id;
    }

    public function EITPISSIRESLTR()
    {
        //Issuing Inspection Response Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPISSIRESLTR"])->first();
        return $return->id;
    }

    public function EITPIRESLTRCRTD()
    {
        //Created Inspection Response Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPIRESLTRCRTD"])->first();
        return $return->id;
    }

    public function EITPIRESLTRPRTLCRTD()
    {
        //Partially Created Inspection Response Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPIRESLTRPRTLCRTD"])->first();
        return $return->id;
    }

    public function EITPISUEDIRSLR()
    {
        //Inspection Response Letter Sent
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPISUEDIRSLR"])->first();
        return $return->id;
    }

    public function EITPISSREMLTR()
    {
        //Issuing Reminder Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPISSREMLTR"])->first();
        return $return->id;
    }

    public function EITPREMLTRCRTD()
    {
        //Created Reminder Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPREMLTRCRTD"])->first();
        return $return->id;
    }

    public function EITPREMLTRSNT()
    {
        //Reminder Letter Sent
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPREMLTRSNT"])->first();
        return $return->id;
    }

    public function EITPDFLTDEMPLY()
    {
        //Defaulted Employer
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPDFLTDEMPLY"])->first();
        return $return->id;
    }

    public function EITPODFTDEMWFW()
    {
        //On Defaulted Employer Workflow
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPODFTDEMWFW"])->first();
        return $return->id;
    }

    public function EITPOINSTPRWFW()
    {
        //On Instalment Payment Request Workflow
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPOINSTPRWFW"])->first();
        return $return->id;
    }

    public function EITPEPBYINSTLMNT()
    {
        //Employer Paying By Installment
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPEPBYINSTLMNT"])->first();
        return $return->id;
    }

    public function EITPEMPPDOUTSTNDN()
    {
        //Employer Paid Outstanding
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPEMPPDOUTSTNDN"])->first();
        return $return->id;
    }

    public function EITPWTHOVRPYMNT()
    {
        //With Overpayment
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPWTHOVRPYMNT"])->first();
        return $return->id;
    }

    public function EITOROVRWKFLW()
    {
        //On Overpayment Refunding Workflow
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITOROVRWKFLW"])->first();
        return $return->id;
    }

    public function EITEMPLYREFOVP()
    {
        //Employer Overpayment Refunded
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITEMPLYREFOVP"])->first();
        return $return->id;
    }

    public function EITPCLSDEMPINSPCTN()
    {
        //Closed Employer Inspection
        $return = $this->query()->select(['id'])->where(["code_id" => 38])->whereIn("reference", ["EITPCLSDEMPINSPCTN"])->first();
        return $return->id;
    }

    public function EITOCNEMPLYITSW()
    {
        //On Cancelled Employer Inspection Task Workflow
        $return = $this->query()->select(['id'])->where(["code_id" => 39])->whereIn("reference", ["EITOCNEMPLYITSW"])->first();
        return $return->id;
    }

    public function EITCANEMPLYITS()
    {
        //Cancelled Employer Inspection Task
        $return = $this->query()->select(['id'])->where(["code_id" => 39])->whereIn("reference", ["EITCANEMPLYITS"])->first();
        return $return->id;
    }

    public function EITDCLNEMPLYITS()
    {
        //Declined Employer Inspection Task
        $return = $this->query()->select(['id'])->where(["code_id" => 39])->whereIn("reference", ["EITDCLNEMPLYITS"])->first();
        return $return->id;
    }

    public function OLPLTTRCTRTD()
    {
        //Letter Created
        $return = $this->query()->select(['id'])->where(["code_id" => 40])->whereIn("reference", ["OLPLTTRCTRTD"])->first();
        return $return->id;
    }

    public function OLPPDINFUPDT()
    {
        //Pending Dispatch Information Update
        $return = $this->query()->select(['id'])->where(["code_id" => 40])->whereIn("reference", ["OLPPDINFUPDT"])->first();
        return $return->id;
    }

    public function OLPUPDTDDPINFRM()
    {
        //Updated Dispatch Information
        $return = $this->query()->select(['id'])->where(["code_id" => 40])->whereIn("reference", ["OLPUPDTDDPINFRM"])->first();
        return $return->id;
    }

    public function CHKCRITICAL()
    {
        //Critical
        $return = $this->query()->select(['id'])->where(["code_id" => 41])->whereIn("reference", ["CHKCRITICAL"])->first();
        return $return->id;
    }

    public function CHKPHIGH()
    {
        //High
        $return = $this->query()->select(['id'])->where(["code_id" => 41])->whereIn("reference", ["CHKPHIGH"])->first();
        return $return->id;
    }

    public function CHKPMEDIUM()
    {
        //Medium
        $return = $this->query()->select(['id'])->where(["code_id" => 41])->whereIn("reference", ["CHKPMEDIUM"])->first();
        return $return->id;
    }

    public function CHKPLOW()
    {
        //Low
        $return = $this->query()->select(['id'])->where(["code_id" => 41])->whereIn("reference", ["CHKPLOW"])->first();
        return $return->id;
    }

    public function DRLETTER()
    {
        //Letter
        $return = $this->query()->select(['id'])->where(["code_id" => 42])->whereIn("reference", ["DRLETTER"])->first();
        return $return->id;
    }

    public function DRNOTIFCNINCDNT()
    {
        //Notification & Claim
        $return = $this->query()->select(['id'])->where(["code_id" => 42])->whereIn("reference", ["DRNOTIFCNINCDNT"])->first();
        return $return->id;
    }

    public function DRCLMBNFT()
    {
        //Claim Benefit
        $return = $this->query()->select(['id'])->where(["code_id" => 42])->whereIn("reference", ["DRCLMBNFT"])->first();
        return $return->id;
    }

    public function IUCNOTFCTN()
    {
        //Incident User Category : Notification & Claim Checklist
        $return = $this->query()->select(['id'])->where(['code_id' => 46])->whereIn('reference', ['IUCNOTFCTN'])->first();
        return $return->id;
    }

    public function WAUARCHWRK()
    {
        //Un-Archive Workflow
        $return = $this->query()->select(['id'])->where(['code_id' => 48])->whereIn('reference', ['WAUARCHWRK'])->first();
        return $return->id;
    }

    public function ECSINSPECTN()
    {
        //Erroneous Contribution from Inspection
        $return = $this->query()->select(['id'])->where(['code_id' => 50])->whereIn('reference', ['ECSINSPECTN'])->first();
        return $return->id;
    }

    public function ECSEMREQST()
    {
        //Employer Request
        $return = $this->query()->select(['id'])->where(['code_id' => 50])->whereIn('reference', ['ECSEMREQST'])->first();
        return $return->id;
    }

    public function ECSEIDBYSTF()
    {
        //Identified by Staff
        $return = $this->query()->select(['id'])->where(['code_id' => 50])->whereIn('reference', ['ECSEIDBYSTF'])->first();
        return $return->id;
    }

    public function ECPRQSTCRTD()
    {
        //Request Created
        $return = $this->query()->select(['id'])->where(['code_id' => 51])->whereIn('reference', ['ECPRQSTCRTD'])->first();
        return $return->id;
    }

    public function ECPUPLDNFRM()
    {
        //Uploading Forms
        $return = $this->query()->select(['id'])->where(['code_id' => 51])->whereIn('reference', ['ECPUPLDNFRM'])->first();
        return $return->id;
    }

    public function ECPWTNCNTRRM()
    {
        //Waiting Contribution Remittance
        $return = $this->query()->select(['id'])->where(['code_id' => 51])->whereIn('reference', ['ECPWTNCNTRRM'])->first();
        return $return->id;
    }

    public function ECPREADFRAPVL()
    {
        //Ready for Approval
        $return = $this->query()->select(['id'])->where(['code_id' => 51])->whereIn('reference', ['ECPREADFRAPVL'])->first();
        return $return->id;
    }

    public function ECPOENCNTWRK()
    {
        //On Erroneous Contribution Workflow
        $return = $this->query()->select(['id'])->where(['code_id' => 51])->whereIn('reference', ['ECPOENCNTWRK'])->first();
        return $return->id;
    }

    public function ECPCLSDRQST()
    {
        //Closed Erroneous Contribution Request
        $return = $this->query()->select(['id'])->where(['code_id' => 51])->whereIn('reference', ['ECPCLSDRQST'])->first();
        return $return->id;
    }

    public function FLUTYP01()
    {
        //Manual Notification Register
        $return = $this->query()->select(['id'])->where(['code_id' => 56])->whereIn('reference', ['FLUTYP01'])->first();
        return $return->id;
    }

    public function CARINSFCNTINF()
    {
        //Archive Workflow Reasons; Insufficient Information
        $return = $this->query()->select(['id'])->where(['code_id' => 53])->whereIn('reference', ['CARINSFCNTINF'])->first();
        return $return->id;
    }

    public function CHACCRCLM()
    {
        //Accrual Claim
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CHACCRCLM")->first();
        return $return->id;
    }

    public function CHINVPLAN()
    {
        //Investigation Plan
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "CHINVPLAN")->first();
        return $return->id;
    }

    public function ARREARCMT()
    {
        //Investigation Plan
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "ARREARCMT")->first();
        return $return->id;
    }

    public function INSTMOUUPLOAD(){
        //instalment mou upload
        $return = $this->query()->select(['id'])->where("code_id", 14)->where("reference", "INSTMOUUPLOAD")->first();
        return $return->id;
    }

    

}