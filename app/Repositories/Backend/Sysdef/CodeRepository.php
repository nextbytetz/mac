<?php

namespace App\Repositories\Backend\Sysdef;

use App\Models\Sysdef\Code;
use App\Repositories\BaseRepository;
use App\Exceptions\GeneralException;

class CodeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Code::class;

    public function __construct()
    {

    }

//find or throwException
    public function findOrThrowException($id)
    {
        $code = $this->query()->find($id);
        if (!is_null($code)) {
            return $code;
        }
        throw new GeneralException(trans('exceptions.backend.system.code_not_found'));
    }

    /*
     * create new
     */

    public function create($input) {
        $code = $this->query()->create($input);
        return $code;
    }

    /*
     * update
     */
    public function update($id,$input) {
        $code = $this->findOrThrowException($id);
        return $code->update($input);
    }

}