<?php

namespace App\Repositories\Backend\MacErp;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use App\Models\Auth\User;
use App\Repositories\Backend\Access\UserRepository;
use App\Models\MacErp\RefundPayable;
use App\Models\MacErp\ClaimsPayable;
use App\Models\Finance\Receipt\Receipt;
use Log;


class RefundErpApiRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = ClaimsPayable::class;


    public function __construct()
    {
        //
    }




    public function saveApiTransaction($refund_id){

        Log::info('Prepare to refund');

        if (!is_null($refund_id)){
            // dump('not null');
            $refunds = DB::table('main.receipt_refunds')->where('id',$refund_id)->where('ispaid', 0)->where('is_posted_erp',false)->get();
        }else{

            $refunds = DB::table('main.receipt_refunds')->where('ispaid', 0)->where('is_posted_erp',false)->get();
            // ->where('ispaid', null)->get();
        }

        $transaction_ids = [];
        foreach ($refunds as $refund) {
            $refund = (array) $refund;

            $trx_id = $this->returnTrxId($refund['receipt_id']);
            $receipt = $this->returnReceiptNumber($refund['receipt_id']);
            $employer = $this->returnRegistrationNumber($receipt->employer_id);


            $payment_voucher = [
                'trx_id'=>$trx_id,
                'member_type'=> 'employer',
                'notification_id'=>$receipt->rctno,
                'payee_name' =>$receipt->payer,
                'supplier_id' => $employer->reg_no,
                'benefit_type'=> 'CONTRIBUTORS REFUND',
                'debit_account_expense'=> '01.001.AC.000000.22032127.000.000',
                'credit_account_receivable' => '01.001.AC.000000.32123106.000.000',
                'accountno'=>'0000',
                'bank_name'=>'CRDB',
                'swift_code' =>'CORUTZTZ',
                'bank_branch_id'=>7,
                'bank_address'=>'Dar es Salaam',
                'country_name_bank'=>'Tanzania',
                'city_of_bank'=>'Dar es Salaam',
                'state_or_region'=>'Dar es Salaam',
                'amount'=>$refund['amount'],
                'isposted'=>0,
                'pv_tran_id' => null,
                'isclaim' => 0,
            ];


            $payment_voucher['trx_id'] = $this->trxExist($trx_id);

            // dd($payment_voucher);

            $is_valid = $this->isValidTrx($payment_voucher);
            if($is_valid){
                $transaction = ClaimsPayable::create($payment_voucher);

                if(!empty($transaction)){
                    DB::table('main.receipt_refunds')->where('id', $refund['id'])->update(['is_posted_erp'=>true]);
                    array_push($transaction_ids, $transaction->id);
                }
            }

        }
        return $transaction_ids;

    }


    public function returnTrxId($claim_id){

        $trx_rand_no = mt_rand(100, 199);
        $trx_latest_id = ClaimsPayable::orderBy('id', 'desc')->limit(1)->get();

        if (count($trx_latest_id)>0) {
            $trx_latest=$trx_latest_id[0]->id+1;
            $trx_id = $trx_rand_no.$trx_latest;
        }
        else{
            $trx_id=$trx_rand_no+1;
        }


        return $trx_id;

    }




    public function updateRefundPostedToQ($id, $status){

        $claims_payable = ClaimsPayable::find($id);


        $notification_id = $claims_payable->notification_id;

        $cps = ClaimsPayable::where('notification_id', $notification_id)->get();


        if($status == 200){
            foreach ($cps as $cp) {
                $cp->status_code = config('constants.MAC_ERP.AP_POSTED_Q');
                $cp->save();
            }
        }




    }



    public function isValidTrx($data){
        // dd($data);
        $valid = ClaimsPayable::where('notification_id', $data['notification_id'])->where('amount', $data['amount'])->get();

        if(count($valid)>0){
            return false;
        }else{
            return true;
        }

    }


    public function trxExist($trx_id){
        $trx_exist = ClaimsPayable::where('trx_id', $trx_id)->get();

        if(!empty($trx_exist)){
            $trx_rand_no = mt_rand(100, 199);
            $trx_latest_id = RefundPayable::orderBy('id', 'desc')->limit(1)->get();

            if (count($trx_latest_id)>0) {
                $trx_latest=$trx_latest_id[0]->id+1;
                $trx_id = $trx_rand_no.$trx_latest;
            }
        }

        return $trx_id;

    }



    public function returnReceiptNumber($receipt_id)
    {
        $receipt = DB::table('main.receipts')->where('id', $receipt_id)->first();
        return $receipt;
    }


    public function returnRegistrationNumber($employer_id)
    {
        $employer = DB::table('main.employers')->select('reg_no')->where('id', $employer_id)->first();
        return $employer;
    }




}
