<?php

namespace App\Repositories\Backend\MacErp;

use App\Models\Auth\User;
use App\Models\MacErp\ClaimsPayable;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunVoucherRepository;
use App\Repositories\BaseRepository;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Log;

class ClaimsErpApiRepositroy extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = ClaimsPayable::class;


    public function __construct()
    {
        //
    }

    /*Claim payable - find by trx id*/
    public function findByTrxId($trx_id){
        $claim_payable = $this->query()->where('trx_id', $trx_id)->first();
        return $claim_payable;
    }


    public function saveApiTransaction(){

        $trans = new PaymentVoucherTransactionRepository();

        /*rectify duplicate entries*/
//        $trans->rectifyDuplicateEntries();

        $pv_tran =  $trans->getPendingExportPvTranForDataTable()->get();

        $transaction_ids = [];

        foreach ($pv_tran as $pv_trx) {
            $api_fields = $trans->getApiFieldsForPendingExports($pv_trx);

            // Log::info(print_r($api_fields, true));
            $trx_id = $this->returnTrxId($api_fields['memberno']);

            $payment_voucher = [
                'trx_id'=>$trx_id,
                'member_type'=> $api_fields['member_type'],
                'notification_id'=>$api_fields['filename'],
                'payee_name' =>$api_fields['payee'],
                'supplier_id' => $api_fields['memberno'],
                'benefit_type'=>$api_fields['benefit_type'],
                'debit_account_expense'=> $api_fields['debit_account_expense'],
                'credit_account_receivable' => $api_fields['credit_account_receivable'],
                'accountno'=>$api_fields['accountno'],
                'bank_name'=>$api_fields['bank_name'],
                'swift_code' =>$api_fields['swift_code'],
                'bank_branch_id'=>$api_fields['bank_branch_id'],
                'bank_address'=>$api_fields['bank_address'],
                'country_name_bank'=>$api_fields['country_name_bank'],
                'city_of_bank'=>$api_fields['city_of_bank'],
                'state_or_region'=>$api_fields['state_or_region'],
                'amount'=>$api_fields['amount'],
                'isposted'=>0,
                'pv_tran_id' => $pv_trx->id,
                'isclaim' => 1,
            ];

            $payment_voucher['trx_id'] = $this->trxExist($trx_id);

            // Log::info($payment_voucher['trx_id']);
            $is_valid = $this->isValidTrx($payment_voucher);
            if($is_valid){
                $transaction = ClaimsPayable::create($payment_voucher);
                if(!empty($transaction)){
                    array_push($transaction_ids, $transaction->id);
                }
                /*Update pv tran is exported*/
                $trans->updateExportFlag( $pv_trx->id);
            }else{
                /*Duplicate flag*///TODO:
                $trans->updateDuplicateFlag( $pv_trx->id);
            }
        }
        return $transaction_ids;

    }




    public function returnApCodes($id, $gfs_code, $payment_shortcode)
    {
        $user= User::with('unit')->find($id);

        if (!is_null($user)) {
            return [
                'trx_type' => 'Staff',
                'payee_id' => $user->external_id,
                'hr_staff_id' => $user->external_id,
                'spare_three' => $payment_shortcode.$user->external_id,
                'debitaccount_bankaccount'=>config('constants.MAC_ERP.STAFF_CR_HQ').'.'.$user->unit->shortcode.'.000000.'.$gfs_code.'.000.000',
                'creditaccount_receivableaccount' => config('constants.MAC_ERP.STAFF_CR_HQ').'.'.$user->unit->shortcode.'.000000.'.$gfs_code.'.000.000',
            ];
        }
        else{
            return [
                'trx_type' => null,
                'payee_id' => null,
                'hr_staff_id' => null,
                'spare_three' => null,
                'debitaccount_bankaccount'=> null,
                'creditaccount_receivableaccount' => null,

            ];
        }
    }




    public function returnTrxId($claim_id){

        $trx_rand_no = mt_rand(100, 199);
        $trx_latest_id =DB::table('main.claims_payable')->orderBy('id', 'desc')->limit(1)->get();
        if (count($trx_latest_id) > 0) {
            $trx_latest=$trx_latest_id[0]->id+1;
            $trx_id = $trx_rand_no.$trx_latest;
        }
        else{
            $trx_id=$trx_rand_no+1;
        }


        return $trx_id;

    }




    public function updateApPostedToQ($id, $status){

        $claims_payable = ClaimsPayable::find($id);


        $notification_id = $claims_payable->notification_id;

        $cps = ClaimsPayable::where('notification_id', $notification_id)->get();


        if($status == 200){
            foreach ($cps as $cp) {
                $cp->status_code = config('constants.MAC_ERP.AP_POSTED_Q');
                $cp->save();
            }
        }




    }


/*TODO: Need to improve this need to check if is of the same pv tran*/
    public function isValidTrx($data){
        $valid = ClaimsPayable::where('notification_id', $data['notification_id'])->where('member_type', $data['member_type'])->where('benefit_type', $data['benefit_type'])->where('amount', $data['amount'])->where('supplier_id',$data['supplier_id'] )->where('pv_tran_id', $data['pv_tran_id'])->get();

        if(count($valid)>0){
            return false;
        }else{
            return true;
        }
    }


    public function trxExist($trx_id){
        $trx_exist = ClaimsPayable::where('trx_id', $trx_id)->get();

        if(!empty($trx_exist)){
            $trx_rand_no = mt_rand(100, 199);
            $trx_latest_id = DB::table('main.claims_payable')->orderBy('id', 'desc')->limit(1)->get();

            if (count($trx_latest_id)>0) {
                $trx_latest=$trx_latest_id[0]->id+1;
                $trx_id = $trx_rand_no.$trx_latest;
            }
        }

        return $trx_id;

    }


    /**
     * @return array
     * Save Api Transaction per benefit approved <NEW>
     */
    public function saveApiTransactionPerBenefit($notfication_workflow_id)
    {
        $trans = new PaymentVoucherTransactionRepository();
        $pv_tran =  $trans->getPendingExportPvTranPerBenefitForDataTable($notfication_workflow_id)->get();

        $transaction_ids = [];
        foreach ($pv_tran as $pv_trx) {
            $api_fields = $trans->getApiFieldsForPendingExports($pv_trx);

            // Log::info(print_r($api_fields, true));
            $trx_id = $this->returnTrxId($api_fields['memberno']);

            $payment_voucher = [
                'trx_id'=>$trx_id,
                'member_type'=> $api_fields['member_type'],
                'notification_id'=>$api_fields['filename'],
                'payee_name' =>$api_fields['payee'],
                'supplier_id' => $api_fields['memberno'],
                'benefit_type'=>$api_fields['benefit_type'],
                'debit_account_expense'=> $api_fields['debit_account_expense'],
                'credit_account_receivable' => $api_fields['credit_account_receivable'],
                'accountno'=>$api_fields['accountno'],
                'bank_name'=>$api_fields['bank_name'],
                'swift_code' =>$api_fields['swift_code'],
                'bank_branch_id'=>$api_fields['bank_branch_id'],
                'bank_address'=>$api_fields['bank_address'],
                'country_name_bank'=>$api_fields['country_name_bank'],
                'city_of_bank'=>$api_fields['city_of_bank'],
                'state_or_region'=>$api_fields['state_or_region'],
                'amount'=>$api_fields['amount'],
                'isposted'=>0,
                'pv_tran_id' => $pv_trx->id,
                'isclaim' => 1,
            ];

            $payment_voucher['trx_id'] = $this->trxExist($trx_id);

            // Log::info($payment_voucher['trx_id']);
            $is_valid = $this->isValidTrx($payment_voucher);
            if($is_valid){

                $transaction = ClaimsPayable::create($payment_voucher);

                if(!empty($transaction)){

                    array_push($transaction_ids, $transaction->id);
                }

                /*Update pv tran is exported*/
                $trans->updateExportFlag( $pv_trx->id);
            }
        }

        return $transaction_ids;

    }

    /**
     * Post to AP ERP Api for posting  claim payable to ERP from MAC - Per benefit <New>
     */
    public function postApErpApi($notfication_workflow_id){
        /*Save to claim payable*/
        $transaction_ids = $this->saveApiTransactionPerBenefit($notfication_workflow_id);
        // dd($transaction_ids);
        if(env('TESTING_MODE') == 0){
            $this->exportToErp($transaction_ids);
        }

    }


    /**
     * Post to AP ERP Api for posting  claim payable to ERP from MAC - bulk exports
     */
    public function postApErpApiBulk(){
        /*Save to claim payable*/
        $transaction_ids = $this->saveApiTransaction();

        // dd($transaction_ids);
        $this->exportToErp($transaction_ids);
    }

    /**
     * Post to AP ERP Api for posting  claim payable to ERP from MAC - Pension payroll
     */
    public function postApErpApiPensionPayroll($payroll_run_approval_id){
        /*Save to claim payable*/
        $transaction_ids = $this->saveApiTransactionPensionPayroll($payroll_run_approval_id);
        // dd($transaction_ids);
        $this->exportToErp($transaction_ids);
    }


    /**
     * @param $transaction_ids
     * Export claim payable to ERP.
     */
    public function exportToErp($transaction_ids)
    {

        DB::commit();
        foreach ($transaction_ids as $transaction_id) {
            if(!is_null($transaction_id)){
                try {
                    $client = new Client();
                    $response =  $client->request('GET', config('constants.MAC_ERP.AP_TO_Q').$transaction_id);
                    $status = $response->getStatusCode();
                }
                catch (ClientError $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status:'.$status.' ClientError');

                }
                catch (ServerError $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status: '.$status.' ServerError');


                }
                catch (BadResponse $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status: '.$status,' BadResponse');

                }
                catch(\Exception $e){
                    $status = $e->getCode();
                    Log::info($transaction_id.' Status: '.$status.' Exception');

                }

                $this->updateApPostedToQ($transaction_id, $status);
            }


        }
    }


    /**
     * @param @transaction_id => transaction from claim payable
     * Save response from ERP after payment completion
     * <Applicable for old and new benefit procedures on claim>
     */
    public function updateClaimsPaymentStatus($transaction_id){
        $claim_payable = $this->query()->where('trx_id', $transaction_id)->first();
        if($claim_payable->status_code== 200){
            $claim_payable->status_code=201;
            $claim_payable->isposted=true;
            $claim_payable->save();
        }
    }




    /**
     * @param $input - Input array for
     * @transaction_id => transaction from claim payable
     * Save response from ERP after payment completion
     * <Applicable for new benefit procedures on claim>
     *
     */
    public function updateClaimPaymentInfoFromErp($transaction_id, array $input)
    {
        $claim_payable = $this->query()->where('trx_id', $transaction_id)->first();
        $pv_tran    = $claim_payable->pvTran;
        if($pv_tran){
            $claim_compensation = $pv_tran->claimCompensation;
            $notification_eligible_benefit = (isset($claim_compensation)) ? $claim_compensation->notificationEligibleBenefit : null;
            if(isset($notification_eligible_benefit)){
                /*Save payment reference*/
                $notification_eligible_benefit_repo = new NotificationEligibleBenefitRepository();
                $notification_eligible_benefit_repo->updatePaymentInfoFromErp($notification_eligible_benefit->id, $input);
            }
        }

    }







    /**
     * @param $payroll_run_approval_id
     * @return array
     * Save to api pension payroll
     */
    public function saveApiTransactionPensionPayroll($payroll_run_approval_id)
    {
        $payrolls = new PayrollRepository();
        $payroll_run_approvals = new PayrollRunApprovalRepository();
//        $payroll_runs = $payroll_run_approvals->getPayrollRunsForExportToErp($payroll_run_approval_id);
        $payroll_run_vouchers = $payroll_run_approvals->getPayrollRunVouchersForExportToErp($payroll_run_approval_id);
        $transaction_ids = [];
        $payroll_run_approval = $payroll_run_approvals->find($payroll_run_approval_id);
        foreach ($payroll_run_vouchers as $payroll_run_voucher) {
            $payroll_run_voucher_id = $payroll_run_voucher->id;
            $api_fields = $payroll_run_approvals->getApiFieldsForPendingExports($payroll_run_voucher_id, $payroll_run_approval);

            // Log::info(print_r($api_fields, true));
            $trx_id = $this->returnTrxId($api_fields['memberno']);

            $payment_voucher = [
                'trx_id' => $trx_id,
                'member_type' => $api_fields['member_type'],
                'notification_id' => $api_fields['filename'],
                'payee_name' => $api_fields['payee'],
                'supplier_id' => $api_fields['memberno'],
                'benefit_type' => $api_fields['benefit_type'],
                'debit_account_expense' => $api_fields['debit_account_expense'],
                'credit_account_receivable' => $api_fields['credit_account_receivable'],
                'accountno' => $api_fields['accountno'],
                'bank_name' => $api_fields['bank_name'],
                'swift_code' => $api_fields['swift_code'],
                'bank_branch_id' => $api_fields['bank_branch_id'],
                'bank_address' => $api_fields['bank_address'],
                'country_name_bank' => $api_fields['country_name_bank'],
                'city_of_bank' => $api_fields['city_of_bank'],
                'state_or_region' => $api_fields['state_or_region'],
                'amount' => $api_fields['amount'],
                'isposted' => 0,
                'pv_tran_id' => null,
                'isclaim' => 2,
                'payment_resource_id' => $payroll_run_voucher_id,
            ];


            $payment_voucher['trx_id'] = $this->trxExist($trx_id);
//            dd($payment_voucher);
            // Log::info($payment_voucher['trx_id']);
            $is_valid = $this->checkIfPayrollTrxExist($payment_voucher);
            if ($is_valid) {

                $transaction = ClaimsPayable::create($payment_voucher);

                if (!empty($transaction)) {

                    array_push($transaction_ids, $transaction->id);
                }
                /*Update payroll run is exported*/

                (new PayrollRunVoucherRepository())->updateExportFlag($payroll_run_voucher_id);
            }
        }

        return $transaction_ids;
    }

    /**
     * @param $payment_voucher
     * @return bool
     * Check if payroll transaction exists
     */
    public function checkIfPayrollTrxExist($payment_voucher){
        $valid = ClaimsPayable::where('notification_id', $payment_voucher['notification_id'])->where('benefit_type', $payment_voucher['benefit_type'])->where('supplier_id', $payment_voucher['supplier_id'])->where('isclaim', 2)->get();

        if(count($valid)>0){
            return false;
        }else{
            return true;
        }
    }


}
