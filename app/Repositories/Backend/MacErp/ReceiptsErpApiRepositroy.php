<?php

namespace App\Repositories\Backend\MacErp;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use App\Models\Auth\User;
use App\Repositories\Backend\Access\UserRepository;
use App\Models\MacErp\ReceiptsGl;
use App\Models\Finance\Receipt\Receipt;
use Log;
class ReceiptsErpApiRepositroy extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = ReceiptsGl::class;


    public function __construct()
    {
       //
    }


    public function saveApiTransaction($receipt_id, $employer=true){
     if(!$employer){
       return $this->saveAdminApiTransaction($receipt_id);
     }
     else{
      return  $this->saveEmployerApiTransaction($receipt_id);
    }


  }


  public function saveEmployerApiTransaction($receipt_id){

    //Get receipts details with contribution credit account default GFS_CODE
    $receipts = Receipt::select('fin_codes.*','receipts.*','receipt_codes.*','receipt_codes.amount as amount', 'receipts.employer_id as employer_id', 'receipts.id as id','receipts.fin_code_id as fin_codes_gfs')
    ->join('main.receipt_codes', 'receipts.id', '=', 'receipt_codes.receipt_id')
    ->join('main.fin_codes', 'receipt_codes.fin_code_id', '=', 'fin_codes.id')
    ->where('receipts.id',$receipt_id)->get();

    // Log::info(print_r($receipts, true));

    $transaction_ids = [];
    foreach ($receipts as $receipt) {

      $is_valid_employer_trx = $this->isValidEmployerTrx($receipt->employer_id);
      if($is_valid_employer_trx)
      { 

        $account_gfs_code=$this->checkAccountcode($receipt->id);
        $trx_id = $this->returnTrxId($receipt->id);

        $data  = [ 'trx_id' => $trx_id, 'trx_type' => 'Employer', 'rctno' =>  $receipt->rctno, 'rct_date'=> Carbon::parse($receipt->created_at)->format('Y-m-d'), 
        'payment_description'=> $receipt->description,'payment_date' => $receipt->rct_date,'employer_id' => $receipt->employer_id,'thirdparty_id' => $receipt->thirdparty_id, 'payee_name' => $receipt->payer, 
        'debitaccount_bankaccount'=> config('constants.MAC_ERP.EMPLOYER_DB_GFS').'.'.$account_gfs_code.'.'.'000.000',
        'creditaccount_receivableaccount' => config('constants.MAC_ERP.EMPLOYER_CR_GFS').'.'.$receipt->gfs_code.'.'.'000.000','control_number' => $receipt->pay_control_no,'currency' => 'TZS', 'amount' => $receipt->amount,'isposted'=> 0
      ];


      $transaction = ReceiptsGl::create($data);

      if(!empty($transaction)){

        array_push($transaction_ids, $transaction->id);
      }

    } 
  }

  return $transaction_ids;



}

public function checkAccountcode($receipt_id){
 //Get receipt details with bank account Debit GFS_CODE for each bank account selected by user
  $bankaccount_gfs=Receipt::select('fin_codes.*','receipts.*','receipts.employer_id as employer_id', 'receipts.id as id','receipts.fin_code_id as fin_codes_gfs','fin_codes.gfs_code as account_gfs')
  ->join('main.fin_codes', 'receipts.bank_code', '=', 'fin_codes.id')
  ->where('receipts.id',$receipt_id)->first();

  return $bankaccount_gfs->account_gfs;
}


public function saveAdminApiTransaction($receipt_id){
  // Log::info('moja');
  $receipts = Receipt::select('fin_codes.*','receipts.*','receipts.id as id')
  ->join('main.fin_codes', 'receipts.fin_code_id', '=', 'fin_codes.id')
  ->where('receipts.id',$receipt_id)->get();

  // Log::info('mbili');
  $transaction_ids = [];
  foreach ($receipts as $receipt) {

    $trx_id = $this->returnTrxId($receipt->id);
    $is_valid_staff_trx = $this->isValidGlStaffTrx($receipt->staff_id, $receipt->gfs_code);
    $is_valid_thirdpart_trx = $this->isValidGlThirdPartyTrx($receipt->thirdparty_id);
    $account_gfs_code=$this->checkAccountcode($receipt->id);
    if($is_valid_staff_trx)
    {

      $payee = $this->returnStaffGl($receipt->staff_id, $account_gfs_code, $receipt->gfs_code ,$receipt->dscription);

      $data = ['trx_id' => $trx_id,'trx_type' => $payee['trx_type'],'rctno' =>  $receipt->rctno,'rct_date'=>  Carbon::parse($receipt->created_at)->format('Y-m-d'),'payment_description'=> $receipt->description,'payment_date' => $receipt->rct_date,'employer_id' => $receipt->employer_id,'thirdparty_id' =>  $receipt->thirdparty_id, 'hr_staff_id' => $payee['hr_staff_id'],'payee_name' => $receipt->payer, 'debitaccount_bankaccount'=> $payee['debitaccount_bankaccount'],
      'creditaccount_receivableaccount' => $payee['creditaccount_receivableaccount'], 'control_number' => $receipt->pay_control_no,'currency' => 'TZS', 'spare_three' => $payee['spare_three'],'amount' => $receipt->amount, 'isposted'=> 0];
      $transaction = ReceiptsGl::create($data) ;
      array_push($transaction_ids, $transaction->id);
    }
    elseif($is_valid_thirdpart_trx){
      $payee = $this->returnThirdPartyGl($receipt->thirdparty_id, $account_gfs_code, $receipt->gfs_code);
      $data = ['trx_id' => $trx_id,'trx_type' => $payee['trx_type'],'rctno' =>  $receipt->rctno,'rct_date'=>  Carbon::parse($receipt->created_at)->format('Y-m-d'),'payment_description'=> $receipt->description,'payment_date' => $receipt->rct_date,'employer_id' => $receipt->employer_id,'thirdparty_id' =>  $receipt->thirdparty_id, 'hr_staff_id' => $payee['hr_staff_id'],'payee_name' => $receipt->payer, 'debitaccount_bankaccount'=> $payee['debitaccount_bankaccount'],
      'creditaccount_receivableaccount' => $payee['creditaccount_receivableaccount'], 'control_number' => $receipt->pay_control_no,'currency' => 'TZS', 'spare_three' => $payee['spare_three'],'amount' => $receipt->amount, 'isposted'=> 0];
      $transaction = ReceiptsGl::create($data) ;
      array_push($transaction_ids, $transaction->id);
    }

  }

  return $transaction_ids;




}



public function checkTrxExists()
{
    //
}


public function returnStaffGl($id, $account_gfs_code, $gfs_code, $payment_shortcode)
{
  $user= User::with('unit')->find($id);

  if (!is_null($user)) {
    $user->unit->shortcode='00';
    return [
     'trx_type' => 'Staff',
     'payee_id' => $user->external_id,
     'hr_staff_id' => $user->external_id,
     'spare_three' => $payment_shortcode.$user->external_id,
     'debitaccount_bankaccount'=>config('constants.MAC_ERP.STAFF_CR_HQ').'.'.$user->unit->shortcode.'.000000.'.$account_gfs_code.'.000.000',
     'creditaccount_receivableaccount' => config('constants.MAC_ERP.STAFF_CR_HQ').'.'.$user->unit->shortcode.'.000000.'.$gfs_code.'.000.000',
   ];
 }
 else{
  return [
   'trx_type' => null,
   'payee_id' => null,
   'hr_staff_id' => null,
   'spare_three' => null,
   'debitaccount_bankaccount'=> null,
   'creditaccount_receivableaccount' => null,

 ];
}
}


public function returnThirdPartyGl($id, $account_gfs_code, $gfs_code)
{
  if (!is_null($id)) {
    return [
     'trx_type' => 'Third Party',
     'payee_id' => $id,
     'hr_staff_id' => null,
     'spare_three' => null,
     'creditaccount_receivableaccount'=>config('constants.MAC_ERP.THIRDPARTY_CR_GFS').'.'.$gfs_code.'.000.000',
     'debitaccount_bankaccount' => config('constants.MAC_ERP.THIRDPARTY_DB_GFS').'.'.$account_gfs_code.'.000.000',

   ];
 }
 else{
   return [
     'trx_type' => null,
     'payee_id' => null,
     'spare_three' => null,
     'hr_staff_id' => null,
     'debitaccount_bankaccount'=> null,
     'creditaccount_receivableaccount' => null,

   ];
 }

}

public function returnTrxId($receipt_id){
 $trx_latest_id =ReceiptsGl::limit(1)->latest()->get();
 if (count($trx_latest_id)>0) {
   $trx_id=$trx_latest_id[0]->id.$receipt_id;
 }
 else{
  $trx_id='1'.$receipt_id;
}
return $trx_id;
}




public function updateReceiptPostedToQ($id, $status){


  if($status == 200){
   $receipt = ReceiptsGl::find($id);

   $transactions = ReceiptsGl::where('rctno', $receipt->rctno)->get();

   foreach ($transactions as $trx) {
    $trx->status_code = config('constants.MAC_ERP.GL_POSTED_Q');
    $trx->save();
  }
}

}


public function isValidGlStaffTrx($receipt_staff_id, $gfs_code){
  if(!is_null($receipt_staff_id) && ($gfs_code == config('constants.MAC_ERP.SP_GFS_CODE') || $gfs_code == config('constants.MAC_ERP.SF_GFS_CODE')  )){
    return true;
  }else{
    return false;
  }

}


public function isValidGlThirdPartyTrx($receipt_thirdparty_id){
  if(!is_null($receipt_thirdparty_id)){
    return true;
  }else{
    return false;
  }

}


public function isValidEmployerTrx($receipt_employer_id){
  if(!is_null($receipt_employer_id)){
    return true;
  }else{
    return false;
  }

}




}
