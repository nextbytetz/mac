<?php

namespace App\Repositories\Backend\Access;

use App\Models\Auth\UserAlternative;
use App\Repositories\BaseRepository;

class UserAlternativeRepository extends BaseRepository
{
    const MODEL = UserAlternative::class;

}