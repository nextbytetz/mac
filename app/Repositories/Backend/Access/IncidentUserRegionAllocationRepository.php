<?php

namespace App\Repositories\Backend\Access;

use App\Models\Access\IncidentUserRegionAllocation;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class IncidentUserRegionAllocationRepository
 * @package App\Repositories\Backend\Access
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class IncidentUserRegionAllocationRepository extends BaseRepository
{
    const MODEL = IncidentUserRegionAllocation::class;

    /**
     * @param $column
     * @param $iuc_cv_id
     * @param $value
     * @return |null
     */
    public function getUserFromPointer($column, $iuc_cv_id, $value)
    {
        $user = NULL;
        $incidentUserRegionRepo = new IncidentUserRegionRepository();
        $count = $incidentUserRegionRepo->query()->where($column, $value)->whereHas('user', function ($query) use ($iuc_cv_id) {
            $query->where('iuc_cv_id', $iuc_cv_id);
        })->count();
        //logger($count);
        if ($count) {
            $model = $this->query()->updateOrCreate(
                [$column => $value]
            );
            $pointer = $model->pointer;
            if ($pointer % $count == 0 Or $pointer > $count) {
                $pointer = 0;
            }
            //logger($pointer);
            $usersource = $incidentUserRegionRepo->query()->where($column, $value)->whereHas('user', function ($query) use ($iuc_cv_id) {
                $query->where('iuc_cv_id', $iuc_cv_id);
            })->orderBy("incident_user_regions.id", "asc")->offset($pointer)->limit(1)->first();
            //logger($usersource);
            $user = $usersource->user->user_id;
            $model->pointer = $pointer + 1;
            $model->save();
        }
        return $user;
    }

}