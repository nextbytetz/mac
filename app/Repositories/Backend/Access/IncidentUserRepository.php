<?php

namespace App\Repositories\Backend\Access;

use App\Models\Access\IncidentUser;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class IncidentUserRepository
 * @package App\Repositories\Backend\Access
 */
class IncidentUserRepository extends BaseRepository
{
    public const MODEL = IncidentUser::class;

    /**
     * @param $iuc_cv_id
     * @return string
     */
    public function getMissingRegions($iuc_cv_id)
    {
        $regionRepo = new RegionRepository();
        $regions = $regionRepo->query()->whereNotIn('id', function ($query) use ($iuc_cv_id) {
            $query->select('region_id')->from('incident_user_regions')->join('incident_users', 'incident_users.id', '=', 'incident_user_regions.incident_user_id')->where('iuc_cv_id', $iuc_cv_id)->whereNotNull('region_id');
        })
/*            ->whereNotIn('id', function ($query) use ($iuc_cv_id) {
            $query->where();
        })*/
            ->get();
        $names = $regions->pluck('name')->all();
        return implode(', ', $names);
    }

    public function getMissingDistricts($iuc_cv_id)
    {
        //logger($iuc_cv_id);
        $return = '';
        $districtRepo = new DistrictRepository();
        $districts = $districtRepo->query()->select([
            DB::raw('districts.name district'),
            DB::raw('regions.name region'),
        ])->whereNotIn('districts.id', function ($query) use ($iuc_cv_id) {
            $query->select('district_id')->from('incident_user_regions')->join('incident_users', 'incident_users.id', '=', 'incident_user_regions.incident_user_id')->where('iuc_cv_id', $iuc_cv_id)->whereNotNull('district_id');
        })->whereNotIn('districts.id', function ($query) use ($iuc_cv_id) {
            $query->select('id')->from('districts')->whereIn('region_id', function ($query) use ($iuc_cv_id) {
                $query->select('region_id')->from('incident_user_regions')->join('incident_users', 'incident_users.id', '=', 'incident_user_regions.incident_user_id')->where('iuc_cv_id', $iuc_cv_id)->whereNotNull('region_id');
            });
        })->join("regions", "regions.id", "=", "districts.region_id")->whereNull('regions.deleted_at')->get();
        //dd($districts);
        $groups = $districts->groupBy('region');
        //dd($groups);
        foreach ($groups->toArray() as $index => $values) {
            $collection = collect($values);
            $keyed = $collection->pluck('district')->all();
            $return .= implode(', ', $keyed) . " <b>({$index})</b> , ";
        }
        //dd($keyed);
        return $return;
    }

}