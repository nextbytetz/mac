<?php

namespace App\Repositories\Backend\Access;

use App\Models\Auth\UserSub;
use App\Repositories\BaseRepository;

class UserSubRepository extends BaseRepository
{

    const MODEL = UserSub::class;

}