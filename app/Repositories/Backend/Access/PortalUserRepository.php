<?php

namespace App\Repositories\Backend\Access;

use App\Models\Auth\PortalUser;
use App\Repositories\BaseRepository;

class PortalUserRepository extends BaseRepository
{
    const MODEL = PortalUser::class;
}