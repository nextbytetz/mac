<?php

namespace App\Repositories\Backend\Access;

use App\Repositories\BaseRepository;
use App\Models\Access\PermissionGroup;

/**
 * Class PermissionRepository.
 */
class PermissionGroupRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PermissionGroup::class;

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'name', $sort = 'asc')
    {
        return $this->query()
            ->orderBy($order_by, $sort)
            ->get();
    }

}
