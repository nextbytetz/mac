<?php

namespace App\Repositories\Backend\Access;

use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\PortalIncident;
use App\Notifications\Backend\Access\RevokeSubstitutedUser;
use App\Notifications\Backend\Access\SubstitutedUser;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\BaseRepository;
use App\Models\Auth\User;
use App\Repositories\Backend\Access\RoleRepository;
use App\Repositories\Backend\Access\PermissionRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use File;
use Storage;
use Response;
/**
 * Class UserRepository
 * @package App\Repositories\Backend\Access
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class UserRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = User::class;

    protected $incident_allocation_algorithm;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @var \App\Repositories\Backend\Access\PermissionRepository
     */
    protected $permission;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->role = new RoleRepository();
        $this->permission = new PermissionRepository();
        $this->incident_allocation_algorithm = 1; //==> 0 --> Round-Robin, 1 --> Location (Region and/or District)
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        $user= $this->query()->find($id);
        if (!is_null($user)) {
            return $user;
        }
        throw new GeneralException(trans('exceptions.backend.access.users.user_not_found'));
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
        ->select([
            'id',
            'firstname',
            'lastname',
            'username',
        ]);
    }

    public function revokeSubstitute(Model $user)
    {
        return DB::transaction(function () use ($user) {
            $userSubRepo = new UserSubRepository();
            $userSub = $userSubRepo->query()->find($user->user_sub_id);
            $userSub->update(['to_date' => Carbon::now(), 'user_revoke_id' => access()->id()]);
            $substitution = $user->substitution;
            $sub = $user->sub;
            $user->update(['available' => 1, 'user_sub_id' => NULL, 'sub' => NULL]);

            //Send email notification to the user who has been revoked substitution
            $substituting = $this->query()->find($sub);
            $substituting->notify(new RevokeSubstitutedUser($user, $substitution));

            //TODO: Send Socket Notification

            return true;
        });
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function postSubstitute(array $input)
    {
        return DB::transaction(function () use ($input) {
            $substituting = $this->query()->find($input['sub']);
            if (!$substituting->available) {
                throw new GeneralException("User substituting not available, action cancelled ... ");
            }
            $userSubRepo = new UserSubRepository();
            $session_user_id = access()->id();
            $user_id = $input['user_id'];
            $output = 1;
            if ($session_user_id == $user_id) {
                //User Substituting
                $output = 0;
                $user = access()->user();
            } else {
                //Another user substituting on behalf
                $user = $this->query()->find($user_id);
            }
            //Logging a substitution
            $userSub = $userSubRepo->query()->create([
                'user_id' => $user->id,
                'sub' => $input['sub'],
                'from_date' => Carbon::now(),
                'reason' => $input['reason'],
                'user_set_id' => $session_user_id,
            ]);
            $user->update(['available' => 0, 'user_sub_id' => $userSub->id, 'sub' => $input['sub']]);

            //Send email notification to the user substituted
            $substituting->notify(new SubstitutedUser($user));

            //TODO: Send Socket Notification

            return $output;
        });
    }

    /**
     * @param Model $user
     * @param array $input
     * @throws GeneralException
     */
    public function update(Model $user, array $input)
    {
        $permissions = $input['permissions'];
        $roles = $input['roles'];
        $data = $input['data'];

        /*Check if phone is unique*/
        if(isset($data['phone'])){
            $this->checkIfPhoneIsUnique(phone_255($data['phone']), 'phone', 2, $user->id);
            $phone = phone_255($data['phone']);
        }else{
            $phone = $data['phone'];
        }


        DB::transaction(function () use ($user, $permissions, $roles, $data, $phone) {

            $user->update(['email' => $data['email'], 'office_id' => $data['office_id'], 'unit_id' => $data['unit_id'], 'designation_id' => $data['designation_id'], 'external_id' => $data['external_id'], 'phone' => $phone, 'report_to' => $data['report_to'] ]);

            if (isset($data['alternative_approvers']) And count($data['alternative_approvers']) > 0) {
                $insertArr = [];
                $id = access()->id();
                foreach ($data['alternative_approvers'] as $value) {
                    $insertArr[] = [
                        'user_id' => $user->id,
                        'alternative' => $value,
                        'user_set_id' => $id,
                    ];
                }
                $user->alternatives()->delete();
                $user->alternatives()->createMany($insertArr);
            } else {
                $user->alternatives()->delete();
            }


            $this->checkUserRolesCount($roles, $permissions);
            $this->flushRoles($roles, $user);
            $this->flushPermissions($permissions, $user);

            /*
             * Put audits and logs here for editing a user here.
             */
            return true;
            //throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
        });

    }

    /**
     * @param  $roles
     * @throws GeneralException
     */
    private function checkUserRolesCount($roles, $permissions) {
        //User Updated, Update Roles
        //Validate that there's at least one role or permission chosen
        if (count($roles['assignees_roles']) == 0 And count($permissions['permission_user']) == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.permission_needed'));
        }
    }

    /**
     * @param $roles
     * @param $user
     * @throws GeneralException
     */
    private function flushRoles($roles, $user) {

        /* start: Avoid removing all admins */
        $this->checkFlushingAdmin($roles, $user, 'role');
        /* end: Avoid removing all admins */

        //Flush roles out, then add array of new ones
        $user->detachRoles($user->roles);
        if (count($roles['assignees_roles']) > 0) {
            $user->attachRoles($roles['assignees_roles']);
        }
    }

    /**
     * @param $input
     * @param $user
     * @param $case
     * @throws GeneralException
     */
    public function checkFlushingAdmin($input, $user, $case)
    {
        $permissions = [];
        switch ($case) {
            case 'role':
            if (count($input['assignees_roles'])) {
                $role_model = $this->role->query()->find($input['assignees_roles']);
                foreach ($role_model as $role) {
                    $permissions = array_merge($permissions, $role->permissions->pluck("id")->toArray());
                }
            }
            break;
            case 'permission':
            $permissions = $input['permission_user'];
            break;
        }
        $check = $user->allow("all_functions");
        if ($check And ($this->countAdmins() == 1)) {
            if (count($permissions)) {
                $admin_permission = $this->permission->query()->where("name", "=", "all_functions")->select(['id'])->first();
                if (!in_array($admin_permission->id, $permissions)) {
                    /* User attempt to remove all admins in the system */
                    throw new GeneralException(trans('exceptions.backend.access.users.admin_needed'));
                }
            }
        }
    }

    /**
     * @param $permissions
     * @param $user
     * @throws GeneralException
     */
    private function flushPermissions($permissions, $user) {

        /* start: Avoid removing all admins */
        $this->checkFlushingAdmin($permissions, $user, 'permission');
        /* end: Avoid removing all admins */

        //Flush permissions out, then add array of new ones if any
        $user->detachPermissions($user->permissions);
        if (count($permissions['permission_user']) > 0) {
            $user->attachPermissions($permissions['permission_user']);
        }
    }

    /**
     * @param Model $user
     */
    public function onUserLogin(Model $user)
    {
        $user->last_login = Carbon::now();
        $user->save();
    }

    /**
     * @return bool
     */
    public function checkAdminIfDontExists()
    {
        $check = $this->countAdmins();
        if ($check == 0) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function countAdmins()
    {
        $check = $this->query()->whereHas('permissions', function($query) {
            $query->where('name', 'all_functions');
        })->orWhereHas('roles', function($query) {
            $query->whereHas('permissions', function($query) {
                $query->where('name', 'all_functions');
            });
        })->count();
        return $check;
    }

    /**
     * @param $q
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStaffs($q)
    {
        $name = strtolower($q);
        $data['items'] = $this->query()->select(['id', DB::raw("CONCAT_WS(' ', coalesce(firstname, ''), coalesce(middlename, ''), coalesce(lastname, '')) as staff")])->whereRaw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname, username) ~* ?", [$name])->get()->toArray();
        $data['total_count'] = count($data['items']);
        return response()->json($data);
    }

    /**
     * @return mixed
     */
    public function getComplianceStaffs()
    {
        $staffs = $this->query()->where('unit_id', 15)->where('designation_id', '<>', 5)->get();
        return $staffs;
    }


    /**
     * Get number of approved / verified contribution by compliance staff
     * @param $user_id
     * @param $min_date
     * @param $max_date
     * @return mixed
     * @throws GeneralException
     */
    public function  verifiedContributions($user_id, $min_date, $max_date)
    {
        $user = $this->findOrThrowException($user_id);
        return $user->wfTracks()->where('status', 1)->where('wf_definition_id', 12)->where('forward_date', '>=', $min_date . ' 00:00:00')->where('forward_date', '<=', $max_date . ' 23:59:59')
        ->count();
    }


    /**
     * Get number of issued certificates by this compliance user
     * @param $user_id
     * @param $min_date
     * @param $max_date
     * @return mixed
     * @throws GeneralException
     */
    public function certificatesIssued($user_id, $min_date, $max_date)
    {
        $user = $this->findOrThrowException($user_id);
        return $user->employerCertificateIssues()->where('created_at', '>=', $min_date . ' 00:00:00')->where('created_at', '<=', $max_date . ' 23:59:59')->count();
    }

    /**
     * @description get the list of all users from claim administration who are available for task allocation.
     * @param null $userid
     * @return mixed
     */
    public function getCadAvailableStaffs($userid = NULL)
    {
        //Query all the claim administration officer [Officer, Senior, Principle], except call centre and manager
        //$arrDesignation = [4, 6, 7];
        if ($userid) {
            $users = $this->query()->select(['id'])->where("id", $userid);
        } else {
            $arrDesignation = [4, 7]; //Excluding Principal Designation
            $users = $this->query()->select(['id'])->where("unit_id", 14)->whereIn("designation_id", $arrDesignation)->whereIn("office_id", [1])->orderBy("id", "asc");
        }
        return $users;
    }

    public function getCasAvailableStaffs()
    {
        //Query all the claim assessment officer [Officer], except manager
        $users = $this->query()->select(['id'])->where('unit_id', 17)->whereIn('designation_id', [4])->whereIn('office_id', [1])->orderBy('id', 'asc');
        return $users;
    }

    public function getChecklistAvailableStaffs()
    {
        $iuc_cv_id = (new CodeValueRepository())->IUCNOTFCTN();
        $users = $this->query()->select(['id'])->whereIn('id', function ($query) use ($iuc_cv_id) {
            $query->select('user_id')->from('incident_users')->where('iuc_cv_id', $iuc_cv_id);
        })->orderBy('id', 'asc');
        return $users;
    }

    /**
     * @param Model $incident
     * @param int $isreallocate
     * @return int
     * @description allocated claim administration users for notification checklist and administration of an incident
     */
    public function getAllocateForNotificationChecklist(Model $incident, $isreallocate = 0)
    {
        $algorithm = 1;  //==> 0 --> Round-Robin, 1 --> Location (Region and/or District)
        $return = NULL;
        $setnew = 0;

        switch (true) {
            case ($incident instanceof NotificationReport):
                switch ($isreallocate) {
                    case 1:
                        //Set new allocation ...
                        $setnew = 1;
                        break;
                    case 0:
                        //check for previous allocated employer or set new allocation ...
                        $employer = $incident->employer_id;
                        $district = $incident->district;
                        $iuc_cv_id = (new CodeValueRepository())->IUCNOTFCTN();
                        if ($employer) {
                            $codeValueRepo = new CodeValueRepository();
                            $query  = (new NotificationReportRepository())->query()->select(["id", "allocated", "notification_staging_cv_id", "employer_id"])->whereNotNull("allocated")
                                ->where('notification_staging_cv_id', '<>', $codeValueRepo->NSCINC())
                                //->where('progressive_stage', '<', 4)
                                ->where('employer_id', $employer)->where("id", "<>", $incident->id)
                                ->whereIn('allocated', function ($query) use ($iuc_cv_id, $district) {
                                    $query->select('user_id')->from('main.incident_users')->join(DB::raw("main.incident_user_regions as iur"), "iur.incident_user_id", "=", "main.incident_users.id")->where('iuc_cv_id', $iuc_cv_id)->where(function($query) use ($district) {
                                        $query->where("iur.district_id", $district->id)->orWhere("iur.region_id", $district->region_id);
                                    });
                                })
                                ->orderByDesc("id")->limit(1)->first();
                            if ($query) {
                                $return = $query->allocated;
                            } else {
                                $setnew = 1;
                            }
                        } else {
                            $setnew = 1;
                        }
                        break;
                }
            break;
            case ($incident instanceof PortalIncident):
                $iuc_cv_id = (new CodeValueRepository())->IUCNOTFCTN();
                $employer = $incident->employer_id;
                $district = $incident->district;
                $query  = (new PortalIncidentRepository())->query()->select(["id", "allocated"])->whereNotNull("allocated")
                    ->where('employer_id', $employer)->where("id", "<>", $incident->id)
                    ->whereIn('allocated', function ($query) use ($iuc_cv_id, $district) {
                        //$query->select('user_id')->from('main.incident_users')->where('iuc_cv_id', $iuc_cv_id);
                        $query->select('user_id')->from('main.incident_users')->join(DB::raw("main.incident_user_regions as iur"), "iur.incident_user_id", "=", "main.incident_users.id")->where('iuc_cv_id', $iuc_cv_id)->where(function($query) use ($district) {
                            $query->where("iur.district_id", $district->id)->orWhere("iur.region_id", $district->region_id);
                        });
                    })
                    ->orderByDesc("id")
                    ->limit(1)
                    ->first();
                if ($query) {
                    $return = $query->allocated;
                } else {
                    $setnew = 1;
                }
                break;
        }

        if ($setnew) {
            switch ($this->incident_allocation_algorithm) {
                case 1:
                    //Location (Region and/or District)
                    $district = $incident->district;
                    if ($district) {
                        $return = $this->getNewAllocatedForNotificationChecklistLocation($district);
                    }
                    break;
                case 0:
                    //Round-Robin
                    $return = $this->getNewAllocatedForNotificationChecklistRoundRobin();
                    break;
            }
        }
        return $return;
    }

    /**
     * @param Model $incident
     * @return mixed|null
     */
    public function getAllocatedForOnlineIncident(Model $incident)
    {
        $return = NULL;
        if (!$incident->allocated) {
            $return = $this->getAllocateForNotificationChecklist($incident);
        } else {
            $return = $incident->allocated;
        }
        return $return;
    }

    public function getAllocatedForInspection(Model $inspection)
    {
        $codeValueRepo = new CodeValueRepository();
        //$codeValue = $codeValueRepo->CDEMPLYIMSR();
        $return = NULL;
        if (!$inspection->allocated) {
            $return = $this->getNewAllocatedForInspection();
        } else {
            $return = $inspection->allocated;
        }
        return $return;
    }

    public function getNewAllocatedForInspection()
    {
        $codeValueRepo = new CodeValueRepository();
        $codeValue = $codeValueRepo->query()->where("reference", "CDEMPLYIMSR")->first();
        $users = $codeValue->users();
        $count = $users->count(); //inspection_master_pointer
        $sysdef = sysdefs()->data();
        $inspection_master_pointer = $sysdef->inspection_master_pointer;
        $return = NULL;
        if ($count) {
            if ($inspection_master_pointer % $count == 0 Or $inspection_master_pointer > $count) {
                $inspection_master_pointer = 0;
            }
            $user = $users->offset($inspection_master_pointer)->first();
            $return = $user->id;
            $sysdef->inspection_master_pointer = $inspection_master_pointer + 1;
            $sysdef->save();
        }
        return $return;
    }

    /**
     * @param Model $district
     * @return |null |null
     */
    public function getNewAllocatedForNotificationChecklistLocation(Model $district)
    {
        $id = NULL;
        $codeValueReo = new CodeValueRepository();
        $incidentUserRegionRepo = new IncidentUserRegionRepository();
        $incidentUserRegionAllocationRepo = new IncidentUserRegionAllocationRepository();
        $iuc_cv_id = $codeValueReo->IUCNOTFCTN(); //Incident User Category : Notification & Claim Checklist
        $count = $incidentUserRegionRepo->query()->where('district_id', $district->id)->whereHas('user', function ($query) use ($iuc_cv_id) {
            $query->where('iuc_cv_id', $iuc_cv_id);
        })->count();
        //logger('Passed!');
        //logger($count);
        if ($count) {
            $id = $incidentUserRegionAllocationRepo->getUserFromPointer('district_id', $iuc_cv_id, $district->id);
        } else {
            $region = $district->region;
            $count = 0;
            if ($region) {
                $count = $incidentUserRegionRepo->query()->where('region_id', $region->id)->whereHas('user', function ($query) use ($iuc_cv_id) {
                    $query->where('iuc_cv_id', $iuc_cv_id);
                })->count();
            }
            //logger($count);
            if ($count) {
                //logger('I am counted ...');
                $id = $incidentUserRegionAllocationRepo->getUserFromPointer('region_id', $iuc_cv_id, $region->id);
            }
        }
        return $id;
    }

    public function getNewAllocatedForNotificationChecklistRoundRobin()
    {
        $users = $this->getChecklistAvailableStaffs();
        $count = $users->count();
        $sysdef = sysdefs()->data();
        $notification_cad_pointer = $sysdef->notification_cad_pointer;
        $return = NULL;
        if ($count) {
            if ($notification_cad_pointer % $count == 0 Or $notification_cad_pointer > $count) {
                $notification_cad_pointer = 0;
            }
            $user = $users->offset($notification_cad_pointer)->first();
            $return = $user->id;
            $sysdef->notification_cad_pointer = $notification_cad_pointer + 1;
            $sysdef->save();
        }
        return $return;
    }

    /**
     * @param $id
     * @param int $caller Shows whether a method has been called by system. 0 - System, 1 - User in some other functionality where the allocation has been done or enforced by some other actions e.g. Re-fill investigation
     * @return array | [user_id, notification_investigator_id]
     * @throws GeneralException
     * @description allocate staff for notification investigation
     */
    public function allocateForNotificationInvestigation($id, $caller = 0)
    {
        $userRepo = new UserRepository();
        $incident = (new NotificationReportRepository)->query()->select(['id', 'incident_type_id', 'checklist_score', 'district_id', 'employer_id', 'employee_id', 'need_investigation', "allocated"])->where("id", $id)->first();
        /***
         * Priority order of allocating investigation officer ...
         ** Recent Allocated Employer
         ** Recent Active Allocated Incident Region []
         ** Round Robin Fashion
         */
        //Check for incident type
        switch ($incident->incident_type_id) {
            case 1:
                //Occupational Accident
                //Responsible to Claim Administration
                ///Check the checklist score
                //if ($incident->checklist_score <= sysdefs()->data()->max_telephone_investigation_score ) {
                    //Telephone Investigation
                    //throw new GeneralException("This notification incident does not qualify for the physical investigation. See the notification checklist and the score.");
                //}
                break;
            case 2:
            case 3:
                //Occupation Disease --> //Responsible for Claim Assessment
                //Occupation Death
                //Physical Investigation
                break;

        }
        $region = $incident->district->region->id;
        $hq_region = sysdefs()->data()->hq_region;
        $user = [];
        if ($caller) {
            //This is called from notification profile page, when requested to repeat investigation
            //This method has been called by user, retrieve from already allocated investigators to repeat investigation.
            /*$query = (new NotificationReportRepository)->query()->select(["id", "employer_id", "user_id"])->whereHas("investigators", function ($query) {
                $query->whereIn("notification_investigators.attended", [0]);
            })->where("notification_reports.id", $incident->id)->orderBy("id", "asc")->limit(1);*/
            $query = $incident->investigationStaffs()->whereIn("notification_investigators.attended", [0])->orderByDesc("notification_investigators.id")->limit(1);
            if ($query->count()) {
                //$investigators = $query->first()->investigators()->select([DB::raw('users.id as user_id')])->orderByDesc("notification_investigators.id")->limit(1);
                //$pivot = $investigators->first()->pivot;
                $investigator = $query->first();
                //$recycle = $pivot->recycle + 1;
                //$user = $investigators->pluck("user_id")->all();
                $user = [$investigator->user_id];
                $investigator->iscomplete = 0;
                $investigator->attended = 0;
                $investigator->save();
                /*$incident->investigators()->syncWithoutDetaching([$user[0] => [
                    'recycle' => $recycle,
                    'iscomplete' => 0,
                    'attended' => 0,

                ]]);*/
            } else {
                //Allocate the same last user involved in doing investigation if exists ...
                $query = $incident->investigationStaffs()->orderByDesc("notification_investigators.id")->limit(1);

                if ($query->count() And $incident->progressive_stage > 2) {
                    $investigator = $query->first();
                    $user = [$investigator->user_id];
                    $this->createInvestigator($incident, $user);
                }
            }
        } else {
            //This method has been called by system, auto-allocated will be done.
            ///Check if there is recent allocated staff for the same employer in the same region and districts
            /*
            $query = (new NotificationReportRepository)->query()->select(["id", "employer_id", "user_id"])->whereHas("investigators", function ($query) {
                $query->where("notification_investigators.attended", 0);
            })->whereHas("district", function ($query) use ($region) {
                $query->whereHas("region", function ($query) use ($region) {
                    $query->where("regions.id", $region);
                });
            })->orderBy("id", "asc")->limit(1);
            */
            $query = (new NotificationReportRepository)->query()->select(["id", "employer_id", "user_id"])->whereHas("investigators", function ($query) {
                $query->where("notification_investigators.attended", 0);
            })->whereHas("employer", function ($query) use ($region) {
                $query->whereHas("region", function ($query) use ($region) {
                    $query->where("regions.id", $region);
                });
            })->orderBy("id", "asc")->limit(1);
            //Check if incident is disease
            if ($incident->incident_type_id == 2) {
                $query->where("incident_type_id", $incident->incident_type_id);
            } else {
                $query->whereIn("incident_type_id", [1,3]);
            }

            if ($query->count()) {
                ///There is allocated staff in the same region
                //Check for same region, same employer
                $emp = with(clone $query)->where("employer_id", $incident->employer_id);
                if ($emp->count()) {
                    ///There is allocated staff for the same employer in the same region
                    //$user = $emp->first()->investigators()->select([DB::raw('users.id as user_id')])->pluck("user_id")->take(1)->all();
                    $user = [$incident->allocated];
                    //$user =
                    /*$incident->investigators()->syncWithoutDetaching([$user[0] => [
                        'recycle' => 1,
                        'iscomplete' => 0,
                        'attended' => 0,

                    ]]);*/
                    $this->createInvestigator($incident, $user);
                } elseif ($region != $hq_region) {
                    //Check if the region is not the hq. (If the region is the head quarter, the allocation will go by round-robin)
                    //$user = $query->first()->investigators()->select([DB::raw('users.id as user_id')])->pluck("user_id")->take(1)->all();
                    $user = [$incident->allocated];
                    /*$incident->investigators()->syncWithoutDetaching([$user[0] => [
                        'recycle' => 1,
                        'iscomplete' => 0,
                        'attended' => 0,
                    ]]);*/
                    $this->createInvestigator($incident, $user);
                } elseif ($region == $hq_region) {
                    //Do the round-robing here
                    ///assign staff in a round robin fashion.
                    $user = $this->allocateInvestigationRoundRobin($incident, $hq_region);
                }
            } else {
                //Check if in the region there is an officer
                //Do the round-robing here
                ///assign staff in a round robin fashion.
                $user = $this->allocateInvestigationRoundRobin($incident, $hq_region);
            }
        }
        $investigators = $incident->investigators()->select(["users.id"])->orderByDesc("notification_investigators.id")->limit(1);
        if ($investigators->count()) {
            $user[] = $investigators->first()->id;
        }
        //TODO: to be removed when decided otherwise
        //$user = [];
        //$user[] = $incident->allocated;

        return $user;
    }

    /**
     * @param $incident
     * @param $hq_region
     * @return array
     * @description allocate for investigation staff in a round robin fashion
     */
    public function allocateInvestigationRoundRobin($incident, $hq_region)
    {
        $region = $incident->district->region->id;
        $sysdef = sysdefs()->data();
        switch ($incident->incident_type_id) {
            case 1:
            case 3:
                //Occupational Accident
                //Occupation Death
                //Responsible to Claim Administration
                $users = $this->getCadAvailableStaffs($incident->allocated);
                $pointer = "investigate_cad_pointer";
                $pointer_data = $sysdef->$pointer;
                break;
            case 2:
                //Occupation Disease
                //Physical Investigation
                //Responsible for Claim Assessment
                $users = $this->getCasAvailableStaffs();
                $pointer = "investigate_cas_pointer";
                $pointer_data = $sysdef->$pointer;
                break;
        }
        if ($region == $hq_region) {
            $users->whereDoesntHave("investigators", function ($query) use ($region) {
                $query->where("notification_investigators.attended", 0);
            });
        } else {
            $users->whereDoesntHave("investigators", function ($query) use ($region) {
                $query->where("notification_investigators.attended", 0)->whereNotIn("notification_reports.district_id", (new DistrictRepository())->getHqDistrictArray());
            });
        }
        $return = [];
        //$count = $users->count(); //commented to default selected checklist user
        $count = $incident->allocated ?: 0;
        if ($count) {
            if ($pointer_data % $count == 0 Or $pointer_data > $count) {
                $pointer_data = 0;
            }
            //Log::error($users->get()->toArray());
            //Log::error($pointer_data);
            //$user = $users->offset($pointer_data)->first(); //commented to default selected checklist user
            //$return[] = $user->id; //commented to default selected checklist user
            $return[] = $incident->allocated;
            //$sysdef->$pointer = $pointer_data + 1; //commented to default selected checklist user
            //$sysdef->save(); //commented to default selected checklist user
            //$incident->investigators()->attach($return[0]);
            $this->createInvestigator($incident, $return);
        }
        return $return;
    }

    /**
     * @param Model $incident
     * @param array $user
     * @return bool
     */
    public function createInvestigator(Model $incident, array $user)
    {
        $type = "";
        if ($incident->need_investigation) {
            //Physical Investigation
            $type = 2;
        } else {
            //Phone Call Investigation
            $type = 1;
        }
        $incident->investigators()->attach([$user[0] => [
            'recycle' => 1,
            'iscomplete' => 0,
            'attended' => 0,
            'type' => $type,
        ]]);
        return true;
    }

    /**
     * @param $id
     * @return string
     */
    public function getUserDepartment($id)
    {
        $department = "";
        $query = $this->query()->select(['unit_id'])->where("id", $id);
        if ($query->count() And $query->first()->unit()->count()) {
            $department = $query->first()->unit->name;
        }
        return $department;
    }

   public function exportDailyAuditLogs(){
  //export daily audit trail logs
        $daily_date=Carbon::yesterday()->format('Y-m-d');
        $file_date=$daily_date;

        $fileName=$file_date . '_daily_audit_trail.text';
         $file=$file_date . '_daily_audit_trail.text';

        $data = json_encode(\DB::table('main.audits')->select('samaccountname as username','event as action','auditable_id','auditable_type','old_values','new_values','url','ip_address','user_agent as browser_used','audits.created_at as date_accessed')
    ->join('main.users', 'audits.user_id', '=', 'users.id')
    ->where('user_id','!=',NULL)->whereDate('audits.created_at',$daily_date)->get());
         
         \Storage::put('public/audit_trail/' . $file, $data);


         return $data;
}

}