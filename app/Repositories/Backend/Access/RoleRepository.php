<?php

namespace App\Repositories\Backend\Access;

use App\Repositories\BaseRepository;
use App\Models\Access\Role;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

class RoleRepository extends BaseRepository
{

    /**
     * Associated Repository Model.
     */
    const MODEL = Role::class;

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'name', $sort = 'asc')
    {
        return $this->query()
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->with('permissions')
            ->select([
                'id',
                'name',
                'description',
                'all_functions',
            ]);
    }

    public function create(array $input)
    {

        $input['permissions'] = explode_parameter($input['permissions']);

        DB::transaction(function () use ($input) {
            $role = self::MODEL;
            $role = new $role();
            $role->name = $input['name'];
            $role->description = $input['description'];
            //check if all functions have been selected
            if (in_array(1, $input['permissions'])) {
                $role->all_functions = 1;
            }

            if ($role->save()) {
                $permissions = [];
                if (is_array($input['permissions']) And count($input['permissions'])) {
                    foreach ($input['permissions'] as $perm) {
                        if (is_numeric($perm)) {
                            array_push($permissions, $perm);
                        }
                    }
                }
                $role->attachPermissions($permissions);
                /*
                 * Put audits and logs here for creating a role
                 */
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.access.roles.create_error'));
        });
    }

    public function update(Model $role, array $input)
    {

        $input['permissions'] = explode_parameter($input['permissions']);

        $role->name = $input['name'];
        $role->description = $input['description'];
        //check if all functions have been selected
        if (in_array(1, $input['permissions'])) {
            $role->all_functions = 1;
        } else {
            $role->all_functions = 0;
        }

        DB::transaction(function () use ($role, $input) {
            if ($role->save()) {

                $role->permissions()->sync([]);

                $permissions = [];
                if (is_array($input['permissions']) And count($input['permissions'])) {
                    foreach ($input['permissions'] as $perm) {
                        if (is_numeric($perm)) {
                            array_push($permissions, $perm);
                        }
                    }
                }
                $role->attachPermissions($permissions);
                /**
                 * Put audits and logs here for updating a role
                 */
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.access.roles.update_error'));
        });

    }

    public function destroy(Model $role)
    {
        //Don't delete the role is there are users associated
        if ($role->users()->count() > 0) {
            throw new GeneralException(trans('exceptions.backend.access.roles.has_users'));
        }

        DB::transaction(function () use ($role) {
            //Detach all associated roles
            $role->permissions()->sync([]);

            if ($role->forceDelete()) {
                /*
                 * Put audits and logs here for deleting a role here
                 */

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
        });

    }

}