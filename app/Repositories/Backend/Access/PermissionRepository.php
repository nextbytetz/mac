<?php

namespace App\Repositories\Backend\Access;

use App\Repositories\BaseRepository;
use App\Models\Access\Permission;

/**
 * Class PermissionRepository.
 */
class PermissionRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Permission::class;

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'name', $sort = 'asc')
    {
        return $this->query()
            ->orderBy($order_by, $sort)
            ->get();
    }

    public function getAllPermissions($order_by = 'name', $sort = 'asc')
    {
        return Permission::with('dependencies.permission', 'backDependencies.backPermission')->orderBy($order_by, $sort)->get();
    }


    /**
     * @return int
     * Cancel receipt permission
     */
    public function getCancelReceiptPermissionId() {
        return  6;
    }

    /**
     * @return int
     * Register receipt permission
     */
    public function getRegisterReceiptPermissionId() {
        return 2;
    }

    /*
     * get load contribution permission
     */

    public function getViewComplianceMenuPermissionId() {
        return 39;
    }
}
