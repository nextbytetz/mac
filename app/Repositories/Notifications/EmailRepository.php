<?php

namespace App\Repositories\Notifications;

use App\Models\Notifications\Email;
use App\Repositories\BaseRepository;

class EmailRepository extends BaseRepository
{
    const MODEL = Email::class;

}