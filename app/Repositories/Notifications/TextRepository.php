<?php

namespace App\Repositories\Notifications;

use App\Models\Notifications\Text;
use App\Repositories\BaseRepository;

class TextRepository extends BaseRepository
{
    const MODEL = Text::class;

}