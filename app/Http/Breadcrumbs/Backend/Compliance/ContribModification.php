<?php

Breadcrumbs::register('backend.compliance.employer.contrib_modification.index', function ($breadcrumbs, $employer) {
	$breadcrumbs->parent('backend.compliance.employer.profile', $employer);
	$breadcrumbs->push('Contributon Modification', route('backend.compliance.employer.contrib_modification.index',$employer));
});

Breadcrumbs::register('backend.compliance.employer.contrib_modification.profile', function ($breadcrumbs, $request_id){
	$mod_request = (new \App\Repositories\Backend\Operation\Compliance\Member\ContributionModificationRepository)->find($request_id);
	$breadcrumbs->parent('backend.compliance.employer.contrib_modification.index',$mod_request->employer_id);
	$breadcrumbs->push($mod_request->rctno, route('backend.compliance.employer.contrib_modification.profile',$request_id));
});

Breadcrumbs::register('backend.compliance.employer.contrib_modification.create', function ($breadcrumbs,$employer){
	$breadcrumbs->parent('backend.compliance.employer.contrib_modification.index',$employer);
	$breadcrumbs->push('New Contribution Modification Request', route('backend.compliance.employer.contrib_modification.create'));
});






