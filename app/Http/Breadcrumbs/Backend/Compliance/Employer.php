<?php
/**
 * Created by PhpStorm.
 * User: mluhanjo
 * Date: 9/11/19
 * Time: 6:53 PM
 */


Breadcrumbs::register('backend.compliance.employer.staff_relation.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push('Staff Employer', route('backend.compliance.employer.staff_relation.index'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.configuration', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Configuration', route('backend.compliance.employer.staff_relation.configuration'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.allocations', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Allocations', route('backend.compliance.employer.staff_relation.allocations'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.create_allocation', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Create Allocation', route('backend.compliance.employer.staff_relation.create_allocation'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.create_allocation_large', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Create Allocation', route('backend.compliance.employer.staff_relation.create_allocation_large'));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.create_allocation_bonus', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Create Allocation', route('backend.compliance.employer.staff_relation.create_allocation_bonus'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.allocation_profile', function ($breadcrumbs, $staff_employer_allocation) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.allocations');
    $breadcrumbs->push('Profile', route('backend.compliance.employer.staff_relation.allocation_profile',$staff_employer_allocation));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.edit_allocation', function ($breadcrumbs, $staff_employer_allocation) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.allocation_profile',$staff_employer_allocation);
    $breadcrumbs->push('Edit', route('backend.compliance.employer.staff_relation.edit_allocation',$staff_employer_allocation));
});



Breadcrumbs::register('backend.compliance.employer.staff_relation.reallocation_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Re-allocation', route('backend.compliance.employer.staff_relation.reallocation_page'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.my_employers_for_active_allocations', function ($breadcrumbs, $user, $isbonus, $with_contact) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('My Employers', route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations', [$user, $isbonus, $with_contact]));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.staff_employer_profile', function ($breadcrumbs, $staff_employer_id) {
    $staff_employer = \Illuminate\Support\Facades\DB::table('staff_employer')->where('id', $staff_employer_id)->first();
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',  $staff_employer->user_id, $staff_employer->isbonus, 0);
//    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Profile', route('backend.compliance.employer.staff_relation.staff_employer_profile', [$staff_employer_id]));
});



Breadcrumbs::register('backend.compliance.employer.staff_relation.pending_follow_ups_for_review', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Follow ups for Reviews', route('backend.compliance.employer.staff_relation.pending_follow_ups_for_review'));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.followup.staff_missing_target', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Follow ups Target Reminders', route('backend.compliance.employer.staff_relation.followup.staff_missing_target'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.review_follow_up', function ($breadcrumbs, $follow_up) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.pending_follow_ups_for_review');
    $breadcrumbs->push('Review', route('backend.compliance.employer.staff_relation.review_follow_up', $follow_up));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.create_follow_up_general', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push('Add Followup', route('backend.compliance.employer.staff_relation.create_follow_up_general', $employer));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.create_follow_up', function ($breadcrumbs, $staff_employer) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.staff_employer_profile', $staff_employer);
    $breadcrumbs->push('Add Followup', route('backend.compliance.employer.staff_relation.create_follow_up', $staff_employer));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.add_multiple_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.my_employers_for_active_allocations', access()->id(), 0,0);
    $breadcrumbs->push('Add Multiple Followup', route('backend.compliance.employer.staff_relation.add_multiple_page'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.edit_follow_up', function ($breadcrumbs, $follow_up_id) {
    $follow_up = \App\Models\Operation\Compliance\Member\StaffEmployerFollowUp::query()->find($follow_up_id);
    if(isset($follow_up->staff_employer_id)){
        $breadcrumbs->parent('backend.compliance.employer.staff_relation.staff_employer_profile', $follow_up->staff_employer_id);
    }else{
        $breadcrumbs->parent('backend.compliance.employer.profile', $follow_up->employer_id);
    }

    $breadcrumbs->push('Edit Follow Up', route('backend.compliance.employer.staff_relation.edit_follow_up', $follow_up_id));
});


//
Breadcrumbs::register('backend.compliance.employer.staff_relation.create_allocation_treasury', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Allocate Treasury', route('backend.compliance.employer.staff_relation.create_allocation_treasury'));
});



Breadcrumbs::register('backend.compliance.employer.staff_relation.performance_dashboard.bonus_employers', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Dormant Performance', route('backend.compliance.employer.staff_relation.performance_dashboard.bonus_employers'));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.performance_dashboard.treasury_employers', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Treasury Performance', route('backend.compliance.employer.staff_relation.performance_dashboard.treasury_employers'));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.open_allocate_new_staff_page', function ($breadcrumbs, $staff_employer_allocation_id) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.allocation_profile', $staff_employer_allocation_id);
    $breadcrumbs->push('Allocate new staff', route('backend.compliance.employer.staff_relation.open_allocate_new_staff_page', $staff_employer_allocation_id));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.all_employers_for_active_allocations', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('All Employers', route('backend.compliance.employer.staff_relation.all_employers_for_active_allocations'));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.performance_dashboard', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Collection Performance', route('backend.compliance.employer.staff_relation.performance_dashboard'));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.replace_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Replace Employers', route('backend.compliance.employer.staff_relation.replace_page'));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.assigned_pending_for_review', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Assigned for review', route('backend.compliance.employer.staff_relation.assigned_pending_for_review'));
});

Breadcrumbs::register('backend.compliance.employer.staff_relation.reversed_for_correction', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.index');
    $breadcrumbs->push('Reversed for correction', route('backend.compliance.employer.staff_relation.reversed_for_correction'));
});


Breadcrumbs::register('backend.compliance.employer.staff_relation.upload_excel_page_intern', function ($breadcrumbs, $staff_employer_allocation) {
    $breadcrumbs->parent('backend.compliance.employer.staff_relation.allocation_profile',$staff_employer_allocation);
    $breadcrumbs->push('Upload Excel', route('backend.compliance.employer.staff_relation.upload_excel_page_intern', $staff_employer_allocation));
});



/*Employer particular change*/

Breadcrumbs::register('backend.compliance.employer.change_particular.index', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push('Changes', route('backend.compliance.employer.change_particular.index',$employer));
});


Breadcrumbs::register('backend.compliance.employer.change_particular.create', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.change_particular.index', $employer);
    $breadcrumbs->push('Add', route('backend.compliance.employer.change_particular.create',$employer));
});

Breadcrumbs::register('backend.compliance.employer.change_particular.profile', function ($breadcrumbs, $employer_particular_change) {
    $breadcrumbs->parent('backend.compliance.employer.change_particular.index', $employer_particular_change->employer);
    $breadcrumbs->push('Profile', route('backend.compliance.employer.change_particular.profile',$employer_particular_change));
});

Breadcrumbs::register('backend.compliance.employer.change_particular.edit', function ($breadcrumbs, $employer_particular_change) {
    $breadcrumbs->parent('backend.compliance.employer.change_particular.profile', $employer_particular_change);
    $breadcrumbs->push('Edit', route('backend.compliance.employer.change_particular.edit',$employer_particular_change));
});


Breadcrumbs::register('backend.compliance.employer.change_particular.attach_document', function ($breadcrumbs, $employer_particular_change) {
    $breadcrumbs->parent('backend.compliance.employer.change_particular.profile', $employer_particular_change);
    $breadcrumbs->push('Attach Doc', route('backend.compliance.employer.change_particular.attach_document',$employer_particular_change));
});


Breadcrumbs::register('backend.compliance.employer.change_particular.edit_document', function ($breadcrumbs, $doc_pivot_id) {
    $uploaded_doc = \Illuminate\Support\Facades\DB::table('document_employer')->where('id', $doc_pivot_id)->first();
    $employer_particular_change = \App\Models\Operation\Compliance\Member\EmployerParticularChange::query()->where('id', $uploaded_doc->external_id)->first();
    $breadcrumbs->parent('backend.compliance.employer.change_particular.profile', $employer_particular_change);
    $breadcrumbs->push('Edit Doc', route('backend.compliance.employer.change_particular.edit_document',$doc_pivot_id));
});


Breadcrumbs::register('backend.compliance.employer.contrib_modification.index', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push('Contribution Modification', route('backend.compliance.employer.contrib_modification.index', $employer));
});

Breadcrumbs::register('backend.compliance.employer.online.profile', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push('Online', route('backend.compliance.employer.online.profile', $employer));
});


?>