<?php

Breadcrumbs::register('backend.finance.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.backend.finance.title.finance_menu'), route('backend.finance.menu'));
});

//Retreive receipts - Datatables
Breadcrumbs::register('backend.finance.receipt.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.title.receipt.retrieve.index'), route('backend.finance.receipt.index'));
});



//Receipt -> Action -selected view receipt
Breadcrumbs::register('backend.finance.receipt.edit', function ($breadcrumbs,$receipt) {
    $breadcrumbs->parent('backend.finance.receipt.index');
    $breadcrumbs->push(trans('labels.general.summary_detail'), route('backend.finance.receipt.edit',
        $receipt));
});


//Receipt -> cancel receipt
Breadcrumbs::register('backend.finance.receipt.cancel_reason', function ($breadcrumbs,$receipt) {
    $breadcrumbs->parent('backend.finance.receipt.edit',$receipt);
    $breadcrumbs->push(trans('labels.backend.finance.receipt.cancel_reason'), route('backend.finance.receipt.cancel_reason',
        $receipt));
});



//Receipt -> dishonor receipt
Breadcrumbs::register('backend.finance.receipt.dishonour_reason', function ($breadcrumbs,$receipt) {
    $breadcrumbs->parent('backend.finance.receipt.edit',$receipt);
    $breadcrumbs->push(trans('labels.backend.finance.receipt.dishonour_reason'), route('backend.finance.receipt.dishonour_reason',
        $receipt));
});

//Receipt -> replace cheque no
Breadcrumbs::register('backend.finance.receipt.new_cheque', function ($breadcrumbs,$receipt) {
    $breadcrumbs->parent('backend.finance.receipt.edit',$receipt);
    $breadcrumbs->push(trans('labels.backend.finance.header.receipt.dishonour.replace'), route('backend.finance.receipt.new_cheque',
        $receipt));
});

Breadcrumbs::register('backend.finance.receipt.employer', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.receipt.choose_employer'), route('backend.finance.receipt.employer'));
});


Breadcrumbs::register('backend.finance.receipt.employer.post', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.receipt.contribution_receipt'), route('backend.finance.receipt.employer.post'));
});




Breadcrumbs::register('backend.finance.receipt.administrative', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.receipt.administrative_receipt'), route('backend.finance.receipt.administrative'));
});

Breadcrumbs::register('backend.finance.receipt.edit_months', function ($breadcrumbs, $receipt) {
    $breadcrumbs->parent('backend.finance.receipt.profile_by_request', $receipt->id);
    $breadcrumbs->push(trans('labels.backend.member.edit_contribution_months'), route('backend.finance.receipt.edit_months', $receipt));
});

// Edit Months : Before Electronics
Breadcrumbs::register('backend.finance.legacy_receipt.edit_months', function ($breadcrumbs, $receipt) {
    $breadcrumbs->parent('backend.finance.legacy_receipt.profile', $receipt->id);
    $breadcrumbs->push(trans('labels.backend.member.edit_contribution_months'), route('backend.finance.legacy_receipt.edit_months', $receipt));
});

/*Interest adjust*/
Breadcrumbs::register('backend.compliance.interest_adjustment.create', function ($breadcrumbs, $receipt) {
    $breadcrumbs->parent('backend.finance.receipt.profile_by_request', $receipt->id);
    $breadcrumbs->push('Adjust Interest', route('backend.compliance.interest_adjustment.create', $receipt));
});


Breadcrumbs::register('backend.compliance.interest_adjustment.profile', function ($breadcrumbs, $interest_adjustment) {
    $breadcrumbs->parent('backend.finance.receipt.profile_by_request', $interest_adjustment->receipt_id);
    $breadcrumbs->push('Interest Adjustment Profile', route('backend.compliance.interest_adjustment.profile', $interest_adjustment));
});

Breadcrumbs::register('backend.compliance.interest_adjustment.edit', function ($breadcrumbs, $interest_adjustment) {
    $breadcrumbs->parent('backend.compliance.interest_adjustment.profile', $interest_adjustment);
    $breadcrumbs->push('Edit Receipt', route('backend.compliance.interest_adjustment.edit', $interest_adjustment));
});
/**
 * LEGACY RECEIPT===================
 */
//Retreive receipts - Datatables : Before Electronic
Breadcrumbs::register('backend.finance.legacy_receipt.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.title.receipt.retrieve.index'), route('backend.finance.legacy_receipt.index'));
});


//Create receipt employer : Before Electronics
Breadcrumbs::register('backend.finance.legacy_receipt.employer.post', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.receipt.contribution_receipt'), route('backend.finance.legacy_receipt.employer.post'));
});



//Receipt -> cancel receipt : Before Electronics
Breadcrumbs::register('backend.finance.legacy_receipt.cancel_reason', function ($breadcrumbs,$receipt) {
    $breadcrumbs->parent('backend.finance.legacy_receipt.edit',$receipt);
    $breadcrumbs->push(trans('labels.backend.finance.receipt.cancel_reason'), route('backend.finance.legacy_receipt.cancel_reason',
        $receipt));
});


//Receipt -> Action -selected view receipt :before electronic
Breadcrumbs::register('backend.finance.legacy_receipt.edit', function ($breadcrumbs,$receipt) {
    $breadcrumbs->parent('backend.finance.legacy_receipt.index');
    $breadcrumbs->push(trans('labels.general.summary_detail'), route('backend.finance.legacy_receipt.edit',
        $receipt));
});


//Receipt -> dishonor receipt :before electronic
Breadcrumbs::register('backend.finance.legacy_receipt.dishonour_reason', function ($breadcrumbs,$receipt) {
    $breadcrumbs->parent('backend.finance.legacy_receipt.edit',$receipt);
    $breadcrumbs->push(trans('labels.backend.finance.receipt.dishonour_reason'), route('backend.finance.legacy_receipt.dishonour_reason',
        $receipt));
});

//Receipt -> replace cheque no :before electronic
Breadcrumbs::register('backend.finance.legacy_receipt.new_cheque', function ($breadcrumbs,$receipt) {
    $breadcrumbs->parent('backend.finance.legacy_receipt.edit',$receipt);
    $breadcrumbs->push(trans('labels.backend.finance.header.receipt.dishonour.replace'), route('backend.finance.legacy_receipt.new_cheque',
        $receipt));
});

Breadcrumbs::register('backend.finance.legacy_receipt.employer', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.receipt.choose_employer'), route('backend.finance.legacy_receipt.employer'));
});

/* eND OF legacy Receipt  */

/*PAYMENT  */

Breadcrumbs::register('backend.finance.payment.profile', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.process_payment'), route('backend.finance.payment.profile'));
});

//End --payment

/* Start : Third Party Payers */
Breadcrumbs::register('backend.finance.thirdparty.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.menu');
    $breadcrumbs->push("Third Party Payers", route('backend.finance.thirdparty.index'));
});

Breadcrumbs::register('backend.finance.thirdparty.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.thirdparty.index');
    $breadcrumbs->push("Add New Third Party Payer", route('backend.finance.thirdparty.create'));
});

Breadcrumbs::register('backend.finance.thirdparty.show', function ($breadcrumbs, $thirdparty) {
    $breadcrumbs->parent('backend.finance.thirdparty.index');
    $breadcrumbs->push("Third Party Payer Profile", route('backend.finance.thirdparty.show', $thirdparty));
});

Breadcrumbs::register('backend.finance.thirdparty.edit', function ($breadcrumbs, $thirdparty) {
    $breadcrumbs->parent('backend.finance.thirdparty.show', $thirdparty->id);
    $breadcrumbs->push("Edit Third Party Payer", route('backend.finance.thirdparty.edit', $thirdparty));
});


Breadcrumbs::register('backend.finance.erp.menu.view', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.menu');
    $breadcrumbs->push('Erp Menu', route('backend.finance.erp.menu.view'));
});


/* End : Third Party Payers */