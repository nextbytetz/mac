<?php
/**
 * Created by PhpStorm.
 * User: mluhanjo
 * Date: 1/15/19
 * Time: 5:40 PM
 */

Breadcrumbs::register('backend.compliance.dependent.profile', function ($breadcrumbs, $dependent) {
    $breadcrumbs->parent('backend.compliance.dependent.index');
    $breadcrumbs->push('Profile', route('backend.compliance.dependent.profile', $dependent));
});


Breadcrumbs::register('backend.compliance.dependent.ready_for_export_to_erp', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.erp.menu.view');
    $breadcrumbs->push('Dependents', route('backend.compliance.dependent.ready_for_export_to_erp'));
});

Breadcrumbs::register('backend.compliance.dependent.create_erp_supplier', function ($breadcrumbs, $dependent_employee) {
    $breadcrumbs->parent('backend.compliance.dependent.ready_for_export_to_erp');
    $breadcrumbs->push('Create Supplier', route('backend.compliance.dependent.create_erp_supplier', $dependent_employee));
});