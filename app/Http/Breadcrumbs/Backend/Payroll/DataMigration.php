<?php
/**
 * Created by PhpStorm.
 * User: mluhanjo
 * Date: 4/17/19
 * Time: 8:31 PM
 */

/*Manual notification*/

Breadcrumbs::register('backend.claim.notification_report.administration', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push('Administration', route('backend.claim.notification_report.administration'));
});

Breadcrumbs::register('backend.claim.manual_notification.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Manual', route('backend.claim.manual_notification.index'));
});



Breadcrumbs::register('backend.claim.manual_notification.upload_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.manual_notification.index');
    $breadcrumbs->push('Upload bulk', route('backend.claim.manual_notification.upload_page'));
});

//
Breadcrumbs::register('backend.claim.manual_notification.complete_registration_page', function ($breadcrumbs, $notification_report) {
    $breadcrumbs->parent('backend.claim.manual_notification.index');
    $breadcrumbs->push('register', route('backend.claim.manual_notification.complete_registration_page', $notification_report));
});



Breadcrumbs::register('backend.claim.manual_notification.pending_synchronizations', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Pending Notification', route('backend.claim.manual_notification.pending_synchronizations'));
});

Breadcrumbs::register('backend.claim.manual_notification.sync_member_page', function ($breadcrumbs, $notification_report) {
    $breadcrumbs->parent('backend.claim.manual_notification.pending_synchronizations');
    $breadcrumbs->push('Sync Member', route('backend.claim.manual_notification.sync_member_page', $notification_report));
});

Breadcrumbs::register('backend.claim.manual_notification.create_employee', function ($breadcrumbs, $notification_report) {
    $breadcrumbs->parent('backend.claim.manual_notification.sync_member_page',$notification_report);
    $breadcrumbs->push('Sync Member', route('backend.claim.manual_notification.create_employee', $notification_report));
});

Breadcrumbs::register('backend.claim.manual_notification.list_all', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.notification_report.administration');
    $breadcrumbs->push('List All', route('backend.claim.manual_notification.list_all'));
});

Breadcrumbs::register('backend.claim.manual_notification.upload_all', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.notification_report.administration');
    $breadcrumbs->push('Upload All', route('backend.claim.manual_notification.upload_all'));
});

Breadcrumbs::register('backend.claim.manual_notification.dashboard', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.manual_notification.list_all');
    $breadcrumbs->push('Profile', route('backend.claim.manual_notification.dashboard', $incident));
});

Breadcrumbs::register('backend.claim.manual_notification.do_manual_payment', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.manual_notification.dashboard', $incident);
    $breadcrumbs->push('Payment', route('backend.claim.manual_notification.do_manual_payment', $incident));
});

Breadcrumbs::register('backend.claim.manual_notification.updateinfo', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.manual_notification.dashboard', $incident);
    $breadcrumbs->push('Edit', route('backend.claim.manual_notification.updateinfo', $incident));
});
/*end manual notification*/


/*Manual payroll member --*/

Breadcrumbs::register('backend.payroll.manual_member.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Members', route('backend.payroll.manual_member.index'));
});


Breadcrumbs::register('backend.payroll.manual_member.upload_page', function ($breadcrumbs, $member_type) {
    $breadcrumbs->parent('backend.payroll.manual_member.index');
    $breadcrumbs->push('Upload Bulk', route('backend.payroll.manual_member.upload_page', $member_type));
});




Breadcrumbs::register('backend.payroll.manual_member.dependents_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Dependent', route('backend.payroll.manual_member.dependents_page'));
});


Breadcrumbs::register('backend.payroll.manual_member.create_dependent', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.payroll.manual_member.dependents_page');
    $breadcrumbs->push('Create', route('backend.payroll.manual_member.create_dependent', $employee));
});

/*end - manual payroll member--*/



/*Merging member already exist on system but paid manually*/

Breadcrumbs::register('backend.compliance.dependent.dependents_ready_for_merge_manual', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Survivors', route('backend.compliance.dependent.dependents_ready_for_merge_manual'));
});


Breadcrumbs::register('backend.compliance.dependent.merge_manual_dependent_page', function ($breadcrumbs,$dependent, $employee) {
    $breadcrumbs->parent('backend.compliance.dependent.dependents_ready_for_merge_manual');
    $breadcrumbs->push('Update', route('backend.compliance.dependent.merge_manual_dependent_page', ['dependent'=> $dependent, 'employee' => $employee]));
});

Breadcrumbs::register('backend.compliance.dependent.dependents_already_merged_payroll', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Update', route('backend.compliance.dependent.dependents_already_merged_payroll'));
});


Breadcrumbs::register('backend.payroll.pensioner.pensioners_ready_for_merge_manual', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Pensioners', route('backend.payroll.pensioner.pensioners_ready_for_merge_manual'));
});


Breadcrumbs::register('backend.payroll.pensioner.merge_manual_pensioner_page', function ($breadcrumbs,$pensioner) {
    $breadcrumbs->parent('backend.payroll.pensioner.pensioners_ready_for_merge_manual');
    $breadcrumbs->push('Update', route('backend.payroll.pensioner.merge_manual_pensioner_page', ['pensioner'=> $pensioner]));
});

Breadcrumbs::register('backend.payroll.pensioner.pensioners_already_merged_payroll', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Update', route('backend.payroll.pensioner.pensioners_already_merged_payroll'));
});

/*merging member --end -- */