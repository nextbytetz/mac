<?php
/**
 * Created by PhpStorm.
 * User: mluhanjo
 * Date: 4/17/19
 * Time: 8:31 PM
 */

///*Notification reports*/
Breadcrumbs::register('backend.claim.manual_system_file.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('System files', route('backend.claim.manual_system_file.index'));
});


Breadcrumbs::register('backend.claim.manual_system_file.create', function ($breadcrumbs, $notification_report) {
    $breadcrumbs->parent('backend.claim.manual_system_file.index');
    $breadcrumbs->push('Create', route('backend.claim.manual_system_file.create', $notification_report));
});


Breadcrumbs::register('backend.claim.manual_system_file.death_incidents', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Death Incidents', route('backend.claim.manual_system_file.death_incidents'));
});


Breadcrumbs::register('backend.claim.manual_system_file.survivors', function ($breadcrumbs, $notification_report) {
    $breadcrumbs->parent('backend.claim.manual_system_file.death_incidents');
    $breadcrumbs->push('Survivors', route('backend.claim.manual_system_file.survivors', $notification_report));
});

Breadcrumbs::register('backend.claim.manual_system_file.create_dependent', function ($breadcrumbs, $notification_report) {
    $breadcrumbs->parent('backend.claim.manual_system_file.survivors',$notification_report);
    $breadcrumbs->push('Create', route('backend.claim.manual_system_file.create_dependent', $notification_report));
});


Breadcrumbs::register('backend.claim.manual_system_file.enrolled_pensioners_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Enrolled Pensioners', route('backend.claim.manual_system_file.enrolled_pensioners_page'));
});

Breadcrumbs::register('backend.claim.manual_system_file.enrolled_survivors_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Enrolled Survivors', route('backend.claim.manual_system_file.enrolled_survivors_page'));
});


//Manual payroll runs
Breadcrumbs::register('backend.payroll.manual_payroll_run.upload_page', function ($breadcrumbs, $member_type) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Upload', route('backend.payroll.manual_payroll_run.upload_page', $member_type));
});