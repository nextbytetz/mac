<?php
/**
 * Created by PhpStorm.
 * User: mluhanjo
 * Date: 1/14/19
 * Time: 11:39 PM
 */

/*** Pensioner ***/
Breadcrumbs::register('backend.payroll.pensioner.profile', function ($breadcrumbs, $pensioner) {
    $breadcrumbs->parent('backend.payroll.pensioner.index');
    $breadcrumbs->push('Profile', route('backend.payroll.pensioner.profile', $pensioner));
});


/*Create bank updates*/
Breadcrumbs::register('backend.payroll.create_new_bank', function ($breadcrumbs, $member_type, $resource, $employee) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($member_type, $resource,$employee);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Bank Info', route('backend.payroll.create_new_bank', ['member_type' => $member_type, 'resource' => $resource, 'employee' => $employee]));
});

Breadcrumbs::register('backend.payroll.bank_update.profile', function ($breadcrumbs, $bank_update) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($bank_update->member_type_id, $bank_update->resource_id, $bank_update->employee_id);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Bank Info', route('backend.payroll.bank_update.profile',['bank_update' => $bank_update]));
});

Breadcrumbs::register('backend.payroll.edit_new_bank', function ($breadcrumbs, $bank_update, $employee) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($bank_update->member_type_id, $bank_update->resource_id,$employee);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
//    $breadcrumbs->parent('backend.payroll.bank_update.profile',['bank_update' => $bank_update, 'employee' => $employee] );
    $breadcrumbs->push('Edit', route('backend.payroll.edit_new_bank',['bank_update' => $bank_update, 'employee' => $employee] ));
});



/*--end bank updates breadcrumbs*/



/*Status change --start--*/
Breadcrumbs::register('backend.payroll.status_change.create', function ($breadcrumbs, $member_type, $resource, $employee, $status_change_type) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($member_type, $resource,$employee);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Status Change', route('backend.payroll.status_change.create', ['member_type' => $member_type, 'resource' => $resource, 'employee' => $employee, 'status_change_type' => $status_change_type]));
});



Breadcrumbs::register('backend.payroll.status_change.edit', function ($breadcrumbs, $status_change) {
    $breadcrumbs->parent('backend.payroll.status_change.profile', $status_change);
    $breadcrumbs->push('Edit', route('backend.payroll.status_change.edit',['status_change' => $status_change]));
});

Breadcrumbs::register('backend.payroll.status_change.profile', function ($breadcrumbs, $status_change) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($status_change->member_type_id, $status_change->resource_id,$status_change->employee_id);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Status Change', route('backend.payroll.status_change.profile',['status_change' => $status_change]));
});


Breadcrumbs::register('backend.payroll.status_change.fill_child_disability_assess', function ($breadcrumbs, $status_change) {
    $breadcrumbs->parent('backend.payroll.status_change.profile', $status_change);
    $breadcrumbs->push('Edit', route('backend.payroll.status_change.fill_child_disability_assess',['status_change' => $status_change]));
});


/*end status change*/

/*Recovery --start--*/

Breadcrumbs::register('backend.payroll.recovery.create_manual_arrears', function ($breadcrumbs, $manual_payroll_member) {
//    $breadcrumbs->parent('backend.payroll.alert_monitor.index',6);
    $manual_payroll_member = (new \App\Repositories\Backend\Operation\Payroll\ManualPayrollMemberRepository())->find($manual_payroll_member);
    if($manual_payroll_member->member_type_id == 5){
        $breadcrumbs->parent('backend.payroll.pensioner.index');
    }elseif($manual_payroll_member->member_type_id == 4){
        $breadcrumbs->parent('backend.compliance.dependent.index');
    }
    $breadcrumbs->push('Create Arrears', route('backend.payroll.recovery.create_manual_arrears', ['manual_payroll_member' => $manual_payroll_member]));
});


Breadcrumbs::register('backend.payroll.recovery.create_manual_arrears_system_members', function ($breadcrumbs, $member_type,$resource, $employee,$arrears_months) {
//    $breadcrumbs->parent('backend.payroll.alert_monitor.index',6);
       if($member_type == 5){
        $breadcrumbs->parent('backend.payroll.pensioner.index');
    }elseif($member_type == 4){
        $breadcrumbs->parent('backend.compliance.dependent.index');
    }
    $breadcrumbs->push('Create Arrears', route('backend.payroll.recovery.create_manual_arrears_system_members', ['member_type' => $member_type, 'resource' => $resource, 'employee' => $employee, 'arrears_months' => $arrears_months]));
});



Breadcrumbs::register('backend.payroll.recovery.show_choose_type_form', function ($breadcrumbs, $member_type, $resource, $employee) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($member_type, $resource,$employee);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Choose type', route('backend.payroll.recovery.show_choose_type_form', ['member_type' => $member_type, 'resource' => $resource, 'employee' => $employee]));
});


Breadcrumbs::register('backend.payroll.recovery.create', function ($breadcrumbs, $member_type) {
    if($member_type == 5){
        $breadcrumbs->parent('backend.payroll.pensioner.index');
    }elseif($member_type == 4){
        $breadcrumbs->parent('backend.compliance.dependent.index');
    }
    $breadcrumbs->push('Capture', route('backend.payroll.recovery.create', ['member_type' => $member_type]));
});



Breadcrumbs::register('backend.payroll.recovery.edit', function ($breadcrumbs, $payroll_recovery) {
    $breadcrumbs->parent('backend.payroll.recovery.profile', $payroll_recovery);
    $breadcrumbs->push('Edit', route('backend.payroll.recovery.edit',$payroll_recovery));
});

Breadcrumbs::register('backend.payroll.recovery.profile', function ($breadcrumbs, $payroll_recovery) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($payroll_recovery->member_type_id, $payroll_recovery->resource_id,$payroll_recovery->employee_id);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Payment Recovery', route('backend.payroll.recovery.profile',$payroll_recovery));
});


Breadcrumbs::register('backend.payroll.recovery.open_cancel_page', function ($breadcrumbs, $payroll_recovery) {
    $breadcrumbs->parent('backend.payroll.recovery.profile', $payroll_recovery);
    $breadcrumbs->push('Cancel', route('backend.payroll.recovery.open_cancel_page', ['payroll_recovery' => $payroll_recovery]));
});

/*end recovery */



/*Verify*/
Breadcrumbs::register('backend.payroll.verification.index', function ($breadcrumbs, $member_type) {
    $breadcrumbs->parent('backend.payroll.menu');
    $breadcrumbs->push('Verification', route('backend.payroll.verification.index', ['member_type' => $member_type]));
});

Breadcrumbs::register('backend.payroll.verification.create', function ($breadcrumbs, $member_type, $resource, $employee) {
    $breadcrumbs->parent('backend.payroll.verification.index',$member_type);
    $breadcrumbs->push('Verify', route('backend.payroll.verification.create', ['member_type' => $member_type, 'resource'=> $resource, 'employee' => $employee]));
});




/*Beneficiary --start--*/

Breadcrumbs::register('backend.payroll.beneficiary_update.create', function ($breadcrumbs, $member_type, $resource, $employee) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($member_type, $resource,$employee);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Detail Update', route('backend.payroll.beneficiary_update.create', ['member_type' => $member_type, 'resource_id' => $resource, 'employee' => $employee]));
});


Breadcrumbs::register('backend.payroll.beneficiary_update.edit', function ($breadcrumbs, $payroll_beneficiary_update, $employee) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($payroll_beneficiary_update->member_type_id, $payroll_beneficiary_update->resource_id,$employee);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Edit', route('backend.payroll.beneficiary_update.edit', ['payroll_beneficiary_update' => $payroll_beneficiary_update, 'employee' => $employee]));
});


Breadcrumbs::register('backend.payroll.beneficiary_update.profile', function ($breadcrumbs, $payroll_beneficiary_update) {
    $payrolls = new \App\Repositories\Backend\Operation\Payroll\PayrollRepository();
    $profile_route = $payrolls->returnResourceProfileRoute($payroll_beneficiary_update->member_type_id, $payroll_beneficiary_update->resource_id,$payroll_beneficiary_update->employee_id);
    $breadcrumbs->parent($profile_route['route'], $profile_route['parameter']);
    $breadcrumbs->push('Detail Update', route('backend.payroll.beneficiary_update.profile', ['payroll_beneficiary_update' => $payroll_beneficiary_update]));
});

//end