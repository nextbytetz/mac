<?php
/**
 * Created by PhpStorm.
 * User: mluhanjo
 * Date: 1/13/19
 * Time: 2:11 PM
 */

/***  Payroll Administration  ***/
Breadcrumbs::register('backend.payroll.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push('Payroll', route('backend.payroll.menu'));
});

/*Pension Admin*/
Breadcrumbs::register('backend.payroll.pensioner.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.menu');
    $breadcrumbs->push('Pensioners', route('backend.payroll.pensioner.index'));
});

Breadcrumbs::register('backend.compliance.dependent.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.menu');
    $breadcrumbs->push('Dependents', route('backend.compliance.dependent.index'));
});


Breadcrumbs::register('backend.payroll.pension.run_approval.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.menu');
    $breadcrumbs->push('Run Approvals', route('backend.payroll.pension.run_approval.index'));
});

Breadcrumbs::register('backend.payroll.pension.run', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.pension.run_approval.index');
    $breadcrumbs->push('Process New', route('backend.payroll.pension.run'));
});


Breadcrumbs::register('backend.payroll.pension.run_approval.profile', function ($breadcrumbs, $payroll_run_approval) {
    $breadcrumbs->parent('backend.payroll.pension.run_approval.index');
    $breadcrumbs->push('Profile', route('backend.payroll.pension.run_approval.profile', $payroll_run_approval));
});


//Reconciliation
Breadcrumbs::register('backend.payroll.reconciliation.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.menu');
    $breadcrumbs->push('Reconciliations', route('backend.payroll.reconciliation.index'));
});

Breadcrumbs::register('backend.payroll.reconciliation.profile', function ($breadcrumbs, $payroll_reconciliation) {
    $breadcrumbs->parent('backend.payroll.reconciliation.index');
    $breadcrumbs->push('Profile', route('backend.payroll.reconciliation.profile', $payroll_reconciliation));
});

//Alert monitor
Breadcrumbs::register('backend.payroll.alert_monitor.index', function ($breadcrumbs, $alert_reference) {
    $breadcrumbs->parent('backend.payroll.menu');
    $breadcrumbs->push('Alert Monitor', route('backend.payroll.alert_monitor.index',$alert_reference));
});


//Manual Suspensions
Breadcrumbs::register('backend.payroll.suspension.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.menu');
    $breadcrumbs->push('Manual Suspensions', route('backend.payroll.suspension.index'));
});


Breadcrumbs::register('backend.payroll.suspension.profile', function ($breadcrumbs, $payroll_manual_suspension) {
    $breadcrumbs->parent('backend.payroll.suspension.index');
    $breadcrumbs->push('Profile', route('backend.payroll.suspension.profile',$payroll_manual_suspension));
});

Breadcrumbs::register('backend.payroll.suspension.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.suspension.index');
    $breadcrumbs->push('Run', route('backend.payroll.suspension.create'));
});


Breadcrumbs::register('backend.payroll.data_migration.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.menu');
    $breadcrumbs->push('Files Migration', route('backend.payroll.data_migration.menu'));
});

Breadcrumbs::register('backend.payroll.manual_payroll_run.open_sync_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.payroll.data_migration.menu');
    $breadcrumbs->push('Sync Manual Pensions', route('backend.payroll.manual_payroll_run.open_sync_page'));
});

///*MP Update*/
Breadcrumbs::register('backend.payroll.mp_update.create', function ($breadcrumbs, $member_type_id, $resource_id, $employee_id) {

    if($member_type_id == 5){
        /*pensioners*/
        $breadcrumbs->parent('backend.payroll.pensioner.profile', $resource_id);
    }elseif($member_type_id == 4){
        /*dependents*/
        $dependent = (new \App\Repositories\Backend\Operation\Compliance\Member\DependentRepository())->find($resource_id);
        $dependent_employee = $dependent->getDependentEmployee($employee_id);
        $breadcrumbs->parent('backend.compliance.dependent.profile', $dependent_employee->id);
    }
    $breadcrumbs->push('Update MP', route('backend.payroll.mp_update.create', ['member_type' => $member_type_id, 'resource' => $resource_id, 'employee' => $employee_id]));
});


Breadcrumbs::register('backend.payroll.mp_update.profile', function ($breadcrumbs, $payroll_mp_update) {
    if($payroll_mp_update->member_type_id == 5){
        /*pensioners*/
        $breadcrumbs->parent('backend.payroll.pensioner.profile', $payroll_mp_update->resource_id);
    }elseif($payroll_mp_update->member_type_id == 4){
        /*dependents*/
        $dependent = (new \App\Repositories\Backend\Operation\Compliance\Member\DependentRepository())->find($payroll_mp_update->resource_id);
        $dependent_employee = $dependent->getDependentEmployee($payroll_mp_update->employee_id);
        $breadcrumbs->parent('backend.compliance.dependent.profile', $dependent_employee->id);
    }
    $breadcrumbs->push('Update MP', route('backend.payroll.mp_update.profile', $payroll_mp_update));
});


Breadcrumbs::register('backend.payroll.mp_update.edit', function ($breadcrumbs, $payroll_mp_update) {
    $breadcrumbs->parent('backend.payroll.mp_update.profile', $payroll_mp_update);
    $breadcrumbs->push('Edit', route('backend.payroll.mp_update.edit', $payroll_mp_update));
});



Breadcrumbs::register('backend.payroll.beneficiary.open_hold_page', function ($breadcrumbs, $member_type_id, $resource_id, $employee_id) {
    if($member_type_id == 5){
        /*pensioners*/
        $breadcrumbs->parent('backend.payroll.pensioner.profile', $resource_id);
    }elseif($member_type_id == 4){
        /*dependents*/
        $dependent = (new \App\Repositories\Backend\Operation\Compliance\Member\DependentRepository())->find($resource_id);
        $dependent_employee = $dependent->getDependentEmployee($employee_id);
        $breadcrumbs->parent('backend.compliance.dependent.profile', $dependent_employee->id);
    }
    $breadcrumbs->push('Archive', route('backend.payroll.beneficiary.open_hold_page', ['member_type' => $member_type_id, 'resource' => $resource_id, 'employee' => $employee_id]));
});


/**
 * * Payroll retiree followup
 */
Breadcrumbs::register('backend.payroll.retiree.followup.create', function ($breadcrumbs, $member_type_id, $resource_id, $employee_id) {
    $route = (new \App\Repositories\Backend\Operation\Payroll\PayrollRepository())->returnResourceProfileRoute($member_type_id, $resource_id, $employee_id);
    $breadcrumbs->parent($route['route'], $route['parameter']);
    $breadcrumbs->push('Create', route('backend.payroll.retiree.followup.create', ['member_type' => $member_type_id, 'resource' => $resource_id, 'employee' => $employee_id]));
});

Breadcrumbs::register('backend.payroll.retiree.followup.edit', function ($breadcrumbs, $payroll_retiree_followup) {
    $member_type_id = $payroll_retiree_followup->member_type_id;  $resource_id = $payroll_retiree_followup->resource_id;  $employee_id = $payroll_retiree_followup->employee_id;
    $route = (new \App\Repositories\Backend\Operation\Payroll\PayrollRepository())->returnResourceProfileRoute($member_type_id, $resource_id, $employee_id);
    $breadcrumbs->parent($route['route'], $route['parameter']);
    $breadcrumbs->push('Edit', route('backend.payroll.retiree.followup.edit', $payroll_retiree_followup));
});

/**
 * Payroll retiree mp update
 */

Breadcrumbs::register('backend.payroll.retiree.mp_update.create', function ($breadcrumbs,$payroll_retiree_followup_id, $member_type_id, $resource_id, $employee_id) {
    $route = (new \App\Repositories\Backend\Operation\Payroll\PayrollRepository())->returnResourceProfileRoute($member_type_id, $resource_id, $employee_id);
    $breadcrumbs->parent($route['route'], $route['parameter']);
    $breadcrumbs->push('Create', route('backend.payroll.retiree.mp_update.create', ['payroll_retiree_followup' => $payroll_retiree_followup_id,   'member_type' => $member_type_id, 'resource' => $resource_id, 'employee' => $employee_id]));
});


Breadcrumbs::register('backend.payroll.retiree.mp_update.profile', function ($breadcrumbs,$payroll_retiree_mp_update) {
    $route = (new \App\Repositories\Backend\Operation\Payroll\PayrollRepository())->returnResourceProfileRoute($payroll_retiree_mp_update->member_type_id, $payroll_retiree_mp_update->resource_id, $payroll_retiree_mp_update->employee_id);
    $breadcrumbs->parent($route['route'], $route['parameter']);
    $breadcrumbs->push('MP Update', route('backend.payroll.retiree.mp_update.profile', [$payroll_retiree_mp_update]));
});



Breadcrumbs::register('backend.payroll.retiree.mp_update.edit', function ($breadcrumbs,$payroll_retiree_mp_update) {
    $breadcrumbs->parent('backend.payroll.retiree.mp_update.profile', $payroll_retiree_mp_update);
    $breadcrumbs->push('Edit', route('backend.payroll.retiree.mp_update.edit', [$payroll_retiree_mp_update]));
});


Breadcrumbs::register('backend.payroll.beneficiary.document_usage_status', function ($breadcrumbs, $member_type_id, $resource_id, $employee_id) {

    if($member_type_id == 5){
        /*pensioners*/
        $breadcrumbs->parent('backend.payroll.pensioner.profile', $resource_id);
    }elseif($member_type_id == 4){
        /*dependents*/
        $dependent = (new \App\Repositories\Backend\Operation\Compliance\Member\DependentRepository())->find($resource_id);
        $dependent_employee = $dependent->getDependentEmployee($employee_id);
        $breadcrumbs->parent('backend.compliance.dependent.profile', $dependent_employee->id);
    }
    $breadcrumbs->push('Document Status', route('backend.payroll.beneficiary.document_usage_status', ['member_type' => $member_type_id, 'resource' => $resource_id, 'employee' => $employee_id]));
});
