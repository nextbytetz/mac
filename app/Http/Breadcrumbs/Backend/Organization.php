<?php

Breadcrumbs::register('backend.organization.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.organization_header'), route('backend.organization.menu'));
});

//
////Annual Target - Fiscal year
Breadcrumbs::register('backend.organization.fiscal_year.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.backend.organization.fiscal_years'), route('backend.organization.fiscal_year.index'));
});
/* Add fiscal year*/
Breadcrumbs::register('backend.organization.fiscal_year.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.organization.fiscal_year.index');
    $breadcrumbs->push(trans('labels.general.add_new'), route('backend.organization.fiscal_year.create'));
});

/* edit fiscal year*/
Breadcrumbs::register('backend.organization.fiscal_year.edit', function ($breadcrumbs, $fiscal_year) {
    $breadcrumbs->parent('backend.organization.fiscal_year.index');
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.organization.fiscal_year.edit', $fiscal_year));
});
