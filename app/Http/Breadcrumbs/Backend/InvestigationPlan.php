<?php

Breadcrumbs::register('backend.claim.investigations.index', function ($breadcrumbs) {
	$breadcrumbs->parent('backend.claim.menu');
	$breadcrumbs->push('Investigation Plans', route('backend.claim.investigations.index'));
});

Breadcrumbs::register('backend.claim.investigations.defaults', function ($breadcrumbs) {
	$breadcrumbs->parent('backend.claim.investigations.index');
	$breadcrumbs->push('Investigation Plan Defaults', route('backend.claim.investigations.defaults'));
});


Breadcrumbs::register('backend.claim.investigations.profile', function ($breadcrumbs, $plan_id){
	$plan = (new App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository)->find($plan_id);
	$breadcrumbs->parent('backend.claim.investigations.index');
	$breadcrumbs->push($plan->plan_name, route('backend.claim.investigations.profile',$plan_id));
});

Breadcrumbs::register('backend.claim.investigations.create', function ($breadcrumbs){
	$breadcrumbs->parent('backend.claim.investigations.index');
	$breadcrumbs->push('Create New Plan', route('backend.claim.investigations.create'));
});


Breadcrumbs::register('backend.claim.investigations.edit', function ($breadcrumbs, $plan_id){
	$plan = (new App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository)->find($plan_id);
	$breadcrumbs->parent('backend.claim.investigations.profile',$plan_id);
	$breadcrumbs->push('Edit', route('backend.claim.investigations.edit',$plan_id));
});


Breadcrumbs::register('backend.claim.investigations.assign_files', function ($breadcrumbs, $plan_id){
	$plan = (new App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository)->find($plan_id);
	$breadcrumbs->parent('backend.claim.investigations.profile',$plan_id);
	$breadcrumbs->push('Assign Files', route('backend.claim.investigations.assign_files',$plan_id));
});




