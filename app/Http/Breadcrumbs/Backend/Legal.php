<?php

Breadcrumbs::register('backend.legal.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.legal_header'), route('backend.legal.menu'));
});

Breadcrumbs::register('backend.legal.case.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.legal.menu');
    $breadcrumbs->push(trans('labels.backend.legal.title.cases'), route('backend.legal.case.index'));
});

Breadcrumbs::register('backend.legal.case.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.legal.case.index');
    $breadcrumbs->push(trans('labels.backend.legal.title.create_case'), route('backend.legal.case.create'));
});

Breadcrumbs::register('backend.legal.case.show', function ($breadcrumbs, $case) {
    $breadcrumbs->parent('backend.legal.case.index');
    $breadcrumbs->push(trans('labels.backend.legal.title.case_profile'), route('backend.legal.case.show', $case));
});

Breadcrumbs::register('backend.legal.case.edit', function ($breadcrumbs, $case) {
    $breadcrumbs->parent('backend.legal.case.show', $case->id);
    $breadcrumbs->push(trans('labels.backend.legal.edit'), route('backend.legal.case.edit', $case));
});

Breadcrumbs::register('backend.legal.lawyer.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.legal.menu');
    $breadcrumbs->push(trans('labels.backend.legal.title.lawyers'), route('backend.legal.lawyer.index'));
});

Breadcrumbs::register('backend.legal.lawyer.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.legal.lawyer.index');
    $breadcrumbs->push(trans('labels.backend.legal.title.create_lawyer'), route('backend.legal.lawyer.create'));
});

Breadcrumbs::register('backend.legal.lawyer.show', function ($breadcrumbs, $lawyer) {
    $breadcrumbs->parent('backend.legal.lawyer.index');
    $breadcrumbs->push(trans('labels.backend.legal.title.lawyer_profile'), route('backend.legal.lawyer.show', $lawyer));
});

Breadcrumbs::register('backend.legal.lawyer.edit', function ($breadcrumbs, $lawyer) {
    $breadcrumbs->parent('backend.legal.lawyer.show', $lawyer->id);
    $breadcrumbs->push(trans('labels.backend.legal.edit_lawyer'), route('backend.legal.lawyer.edit', $lawyer));
});

Breadcrumbs::register('backend.legal.case.hearing.create', function ($breadcrumbs, $case) {
    $breadcrumbs->parent('backend.legal.case.show', $case->id);
    $breadcrumbs->push(trans('labels.backend.legal.addhearing'), route('backend.legal.case.hearing.create', $case));
});

Breadcrumbs::register('backend.legal.case.hearing.show', function ($breadcrumbs, $hearing) {
    $breadcrumbs->parent('backend.legal.case.show', $hearing->cases->id);
    $breadcrumbs->push(trans('labels.backend.legal.title.hearing_profile'), route('backend.legal.case.hearing.show', $hearing));
});

Breadcrumbs::register('backend.legal.case.hearing.edit', function ($breadcrumbs, $hearing) {
    $breadcrumbs->parent('backend.legal.case.show', $hearing->cases->id);
    $breadcrumbs->push(trans('labels.backend.legal.edithearing'), route('backend.legal.case.hearing.edit', $hearing));
});

Breadcrumbs::register('backend.legal.case.mention.create', function ($breadcrumbs, $case) {
    $breadcrumbs->parent('backend.legal.case.show', $case->id);
    $breadcrumbs->push(trans('labels.backend.legal.addmentioning'), route('backend.legal.case.mention.create', $case));
});

Breadcrumbs::register('backend.legal.case.mention.show', function ($breadcrumbs, $mention) {
    $breadcrumbs->parent('backend.legal.case.show', $mention->cases->id);
    $breadcrumbs->push(trans('labels.backend.legal.title.mention_profile'), route('backend.legal.case.mention.show', $mention));
});

Breadcrumbs::register('backend.legal.case.mention.edit', function ($breadcrumbs, $mention) {
    $breadcrumbs->parent('backend.legal.case.show', $mention->cases->id);
    $breadcrumbs->push(trans('labels.backend.legal.editmentioning'), route('backend.legal.case.mention.edit', $mention));
});
