<?php

Breadcrumbs::register('backend.system.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.backend.sysdef_title'), route('backend.system.menu'));
});

Breadcrumbs::register('backend.notification.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.backend.system.notification.title'), route('backend.notification.index'));
});

Breadcrumbs::register('backend.checker.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Pending Tasks", route('backend.checker.index'));
});

Breadcrumbs::register('backend.editor', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.editor'), route('backend.editor'));
});

Breadcrumbs::register('backend.users.substitute', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("My Substitute", route('backend.users.substitute', $user));
});

Breadcrumbs::register('backend.role.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.system.menu');
    $breadcrumbs->push(trans('labels.backend.role.title'), route('backend.role.index'));
});

Breadcrumbs::register('backend.role.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.role.index');
    $breadcrumbs->push(trans('labels.backend.role.create'), route('backend.role.create'));
});

Breadcrumbs::register('backend.role.edit', function ($breadcrumbs, $role) {
    $breadcrumbs->parent('backend.role.index');
    $breadcrumbs->push(trans('labels.backend.role.view'), route('backend.role.edit', $role));
});

Breadcrumbs::register('backend.user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.backend.user.title'), route('backend.user.index'));
});

Breadcrumbs::register('backend.user.edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('backend.user.index');
    $breadcrumbs->push(trans('labels.backend.user.view'), route('backend.user.edit', $user));
});

Breadcrumbs::register('backend.workflow.defaults', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.system.menu');
    $breadcrumbs->push(trans('labels.backend.workflow.view'), route('backend.workflow.defaults'));
});

Breadcrumbs::register('backend.workflow.allocation', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.system.menu');
    $breadcrumbs->push("Workflow Allocation", route('backend.workflow.allocation'));
});

Breadcrumbs::register('backend.workflow.my_pending', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Assigned Workflow", route('backend.workflow.my_pending'));
});

Breadcrumbs::register('backend.workflow.attended', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Attended Workflow", route('backend.workflow.attended'));
});

Breadcrumbs::register('backend.workflow.archived', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push('Archived Workflow', route('backend.workflow.archived'));
});

Breadcrumbs::register('backend.workflow.pending', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Pending Workflow", route('backend.workflow.pending'));
});

//reports
Breadcrumbs::register('backend.finance.report.index', function ($breadcrumbs, $category_id) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.report'), route('backend.finance.report.index', $category_id));
});
//reports / types
Breadcrumbs::register('backend.finance.report.receipt.performance', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.report'), route('backend.finance.report.receipt.performance'));
});


Breadcrumbs::register('backend.operation.report.summary_contrib_age_analysis', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.report.index',3);
    $breadcrumbs->push('Summary', route('backend.operation.report.summary_contrib_age_analysis'));
});


Breadcrumbs::register('backend.operation.report.summary_contrib_income_rcvable', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.finance.report.index',3);
    $breadcrumbs->push('Summary', route('backend.operation.report.summary_contrib_income_rcvable'));
});


Breadcrumbs::register('backend.workflow.subordinates', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Subordinates Workflow", route('backend.workflow.subordinates'));
});


Breadcrumbs::register('backend.workflow.subordinate_pending', function ($breadcrumbs, $user_id) {
    $breadcrumbs->parent('backend.workflow.subordinates');
    $user = (new  App\Repositories\Backend\Access\UserRepository)->find($user_id);
    $breadcrumbs->push($user->name, route('backend.workflow.subordinate_pending',$user_id));
});