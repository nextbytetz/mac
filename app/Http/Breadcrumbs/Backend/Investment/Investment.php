<?php

Breadcrumbs::register('backend.investment.menu', function ($breadcrumbs) {
	$breadcrumbs->parent('backend.home');
	$breadcrumbs->push('Investment', route('backend.investment.menu'));
});


Breadcrumbs::register('backend.investment.budget.index', function ($breadcrumbs) {
	$breadcrumbs->parent('backend.investment.menu');
	$breadcrumbs->push('Budget', route('backend.investment.budget.index'));
});

Breadcrumbs::register('backend.investment.budget.show', function ($breadcrumbs, $investment_budget_id){
	$inv_budget = \App\Models\Investment\InvestmentBudget::find($investment_budget_id);
	$breadcrumbs->parent('backend.investment.budget.index');
	$breadcrumbs->push(str_replace('/', '-', $inv_budget->fin_year->name), route('backend.investment.budget.show',$investment_budget_id));
});


Breadcrumbs::register('backend.investment.show', function ($breadcrumbs,$allocation,$allocation_id){
	$inv = \App\Models\Investment\InvestmentBudgetAllocation::find($allocation_id);
	$breadcrumbs->parent('backend.investment.budget.show',$inv->investment_budget_id);

	$breadcrumbs->push(ucwords(str_replace('_', ' ', $allocation)), route('backend.investment.budget.show',[
		'investment_type' => $allocation,
		'investment_allocation_id' => $allocation_id,
	]));
});



Breadcrumbs::register('backend.investment.equity.index', function ($breadcrumbs) {
	$breadcrumbs->parent('backend.investment.menu');
	$breadcrumbs->push('Equities', route('backend.investment.equity.index'));
});

Breadcrumbs::register('backend.investment.equity.show', function ($breadcrumbs, $equity_id){
	$inv_equity = \App\Models\Investment\Equity\InvestmentEquityIssuer::find($equity_id);
	$breadcrumbs->parent('backend.investment.equity.index');
	$breadcrumbs->push($inv_equity->issuer_name, route('backend.investment.budget.show',$equity_id));
});


Breadcrumbs::register('backend.investment.equity.create', function ($breadcrumbs){
	// $inv_budget = \App\Models\Investment\InvestmentBudget::find($investment_budget_id);
	$breadcrumbs->parent('backend.investment.equity.index');
	$breadcrumbs->push('Add New', route('backend.investment.equity.create'));
});

