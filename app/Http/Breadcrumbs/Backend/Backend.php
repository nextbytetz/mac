<?php

Breadcrumbs::register('backend.home', function ($breadcrumbs) {
    $breadcrumbs->push(trans('labels.backend.home'), route('backend.home'));
});

require __DIR__.'/Finance.php';
require __DIR__.'/Legal.php';
require __DIR__.'/Operation.php';
require __DIR__.'/Organization.php';
require __DIR__.'/System.php';
require __DIR__.'/Notification.php';
require __DIR__ . '/Payroll/Payroll.php';
require __DIR__ . '/Payroll/Pensioner.php';
require __DIR__ . '/Payroll/Dependent.php';
require __DIR__ . '/Payroll/DataMigration.php';
require __DIR__ . '/Payroll/ManualSystemFile.php';
require __DIR__ . '/Report.php';
require __DIR__ . '/Investment/Investment.php';
require __DIR__ . '/InvestigationPlan.php';

//compliance new
require __DIR__ . '/Compliance/Employer.php';