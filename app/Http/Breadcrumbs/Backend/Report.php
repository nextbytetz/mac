<?php

Breadcrumbs::register('backend.report.sla.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("SLA list", route('backend.report.sla.index'));
});