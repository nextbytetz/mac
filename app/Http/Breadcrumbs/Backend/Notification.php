<?php

Breadcrumbs::register('backend.letter.confirm', function ($breadcrumbs, $resource, $reference) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Confirm Create", route('backend.letter.confirm', ['resource' => $resource, 'reference' => $reference]));
});

Breadcrumbs::register('backend.letter.create', function ($breadcrumbs, $resource, $reference) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Create Letter", route('backend.letter.create', ['resource' => $resource, 'reference' => $reference]));
});

Breadcrumbs::register('backend.letter.show', function ($breadcrumbs, $letter) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Letter Profile", route('backend.letter.show', $letter));
});

Breadcrumbs::register('backend.letter.edit', function ($breadcrumbs, $letter) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push("Edit Letter", route('backend.letter.edit', $letter));
});

