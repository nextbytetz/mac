<?php

use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Claim\EmployeeOldCompensationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmploymentHistoryRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationHealthProviderRepository;
use App\Repositories\Backend\Operation\Claim\MedicalExpenseRepository;
use App\Repositories\Backend\Operation\Claim\WitnessRepository;

/***  Claims & Notifications  ***/
Breadcrumbs::register('backend.claim.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.claim_header'), route('backend.claim.menu'));
});

Breadcrumbs::register('backend.assessment.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push('Assessment', route('backend.assessment.menu'));
});

Breadcrumbs::register('backend.assessment.pd_calculator', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.assessment.menu');
    $breadcrumbs->push('PD Calculator', route('backend.assessment.pd_calculator'));
});

Breadcrumbs::register('backend.assessment.performance_report', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.assessment.menu');
    $breadcrumbs->push('Performance Report', route('backend.assessment.performance_report'));
});

Breadcrumbs::register('backend.assessment.impairment.all', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.assessment.menu');
    $breadcrumbs->push('Impairment Assessment', route('backend.assessment.impairment.all'));
});

Breadcrumbs::register('backend.assessment.impairment.choose', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.assessment.impairment.all');
    $breadcrumbs->push('Choose Notification', route('backend.assessment.impairment.choose'));
});

Breadcrumbs::register('backend.assessment.impairment.dashboard', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.assessment.impairment.all');
    $breadcrumbs->push('Impairment Assessment Dashboard', route('backend.assessment.impairment.dashboard', $incident));
});

Breadcrumbs::register('backend.assessment.impairment.create', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.assessment.impairment.choose');
    $breadcrumbs->push('Create Impairment Assessment', route('backend.assessment.impairment.create', $incident));
});

Breadcrumbs::register('backend.assessment.impairment.edit', function ($breadcrumbs, $impairment) {
    $breadcrumbs->parent('backend.assessment.impairment.dashboard', $impairment);
    $breadcrumbs->push('Edit Impairment Assessment', route('backend.assessment.impairment.edit', $impairment));
});

Breadcrumbs::register('backend.compliance.booking_group.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push("Manage Booking Groups", route('backend.compliance.booking_group.index'));
});

Breadcrumbs::register('backend.compliance.defaults', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push("Compliance Defaults", route('backend.compliance.defaults'));
});

Breadcrumbs::register('backend.compliance.booking_group.pending_upload', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.booking_group.index');
    $breadcrumbs->push("Pending Upload", route('backend.compliance.booking_group.pending_upload'));
});

Breadcrumbs::register('backend.compliance.booking_group.upload_manual', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.booking_group.index');
    $breadcrumbs->push("Upload Manual Contribution", route('backend.compliance.booking_group.upload_manual'));
});

//Employee Profile / choose_incident_type
Breadcrumbs::register('backend.compliance.employee.choose_incident_type', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.compliance.employee.profile', $employee);
    $breadcrumbs->push(trans('labels.backend.claim.choose_incident_type'), route('backend.compliance.employee.choose_incident_type',$employee));
});

// Register accident
Breadcrumbs::register('backend.claim.notification_report.register', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.compliance.employee.choose_incident_type', $employee);
    $breadcrumbs->push(trans('labels.backend.claim.new_notification'), route('backend.claim.notification_report.register',$employee));
});






/***** Compliance Breadcrumbs  ***/
Breadcrumbs::register('backend.compliance.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.compliance_header'), route('backend.compliance.menu'));
});

/*Search receipt to view profile*/
Breadcrumbs::register('backend.compliance.receipt.search', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.receipt.retrieve_receipt'), route('backend.compliance.receipt.search'));
});



//Receipt -> Profile -selected receipt
Breadcrumbs::register('backend.finance.receipt.profile_by_request', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('backend.compliance.receipt.search');
    $breadcrumbs->push(trans('labels.backend.finance.header.receipt.retrieve.index'), route('backend.finance.receipt.profile_by_request', $id));
});

//View Contribution -> receipt code
Breadcrumbs::register('backend.finance.receipt_code.show', function ($breadcrumbs, $receipt_code) {
    $receiptCode = new ReceiptCodeRepository();
    $thisReceiptCode = $receiptCode->find($receipt_code);
    $breadcrumbs->parent('backend.finance.receipt.profile_by_request', $thisReceiptCode->receipt->id);
    $breadcrumbs->push(trans('labels.backend.member.contribution'), route('backend.finance.receipt_code.show', $receipt_code));
});

//Edit contribution
Breadcrumbs::register('backend.finance.receipt_code.edit', function ($breadcrumbs, $receipt_code) {
    //$receiptCode = new ReceiptCodeRepository();
    //$thisReceiptCode = $receiptCode->find($receipt_code);
    $breadcrumbs->parent('backend.finance.receipt_code.show', $receipt_code->id);
    $breadcrumbs->push(trans('labels.backend.member.edit_contribution'), route('backend.finance.receipt_code.edit', $receipt_code));
});

/* Upload Contribution */
Breadcrumbs::register('backend.finance.receipt_code.linked_file', function ($breadcrumbs, $receipt_code) {
    $breadcrumbs->parent('backend.finance.receipt_code.show', $receipt_code->id);
    $breadcrumbs->push(trans('labels.backend.member.upload_contribution'), route('backend.finance.receipt_code.linked_file', $receipt_code));
});


//View Contribution -> add contribution track
Breadcrumbs::register('backend.compliance.contribution_track.create_new', function ($breadcrumbs, $receipt_code) {
    $breadcrumbs->parent('backend.finance.receipt_code.show',
        $receipt_code);
    $breadcrumbs->push(trans('labels.backend.table.add_track'), route('backend.compliance.contribution_track.create_new',$receipt_code));
});

//View Contribution -> edit contribution track
Breadcrumbs::register('backend.compliance.contribution_track.edit', function ($breadcrumbs, $contribution_track) {
    $breadcrumbs->parent('backend.finance.receipt_code.show',
        $contribution_track->receipt_code_id);
    $breadcrumbs->push(trans('labels.backend.table.edit_track'), route('backend.compliance.contribution_track.edit',$contribution_track));
});

/** Legacy receipt - contribution **/


/*Search receipt to view profile : Receipt before Electronic*/
Breadcrumbs::register('backend.finance.legacy_receipt.received_contributions', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.finance.receipt.retrieve_receipt'), route('backend.finance.legacy_receipt.received_contributions'));
});



//Receipt -> Profile -selected receipt : Receipt before Electronic*/
Breadcrumbs::register('backend.finance.legacy_receipt.profile', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('backend.finance.legacy_receipt.received_contributions');
    $breadcrumbs->push(trans('labels.backend.finance.header.receipt.retrieve.index'), route('backend.finance.legacy_receipt.profile', $id));
});

//View Contribution -> receipt code : Receipt before Electronic*/
Breadcrumbs::register('backend.finance.legacy_receipt_code.show', function ($breadcrumbs, $legacy_receipt_code) {
    $legacyReceiptCode = new \App\Repositories\Backend\Finance\Receipt\LegacyReceiptCodeRepository();
    $thisReceiptCode = $legacyReceiptCode->find($legacy_receipt_code);
    $breadcrumbs->parent('backend.finance.legacy_receipt.profile', $thisReceiptCode->legacyReceipt->id);
    $breadcrumbs->push(trans('labels.backend.member.contribution'), route('backend.finance.legacy_receipt_code.show', $legacy_receipt_code));
});

//Edit contribution : Receipt before Electronic*/
Breadcrumbs::register('backend.finance.legacy_receipt_code.edit', function ($breadcrumbs, $legacy_receipt_code) {
    //$receiptCode = new ReceiptCodeRepository();
    //$thisReceiptCode = $receiptCode->find($receipt_code);
    $breadcrumbs->parent('backend.finance.legacy_receipt_code.show', $legacy_receipt_code->id);
    $breadcrumbs->push(trans('labels.backend.member.edit_contribution'), route('backend.finance.legacy_receipt_code.edit', $legacy_receipt_code));
});

/* Upload Contribution : Receipt before Electronic*/
Breadcrumbs::register('backend.finance.legacy_receipt_code.linked_file', function ($breadcrumbs, $legacy_receipt_code) {
    $breadcrumbs->parent('backend.finance.legacy_receipt_code.show', $legacy_receipt_code->id);
    $breadcrumbs->push(trans('labels.backend.member.upload_contribution'), route('backend.finance.legacy_receipt_code.linked_file', $legacy_receipt_code));
});


/* end of receipt/contribution */


/***** Members Breadcrumbs  ***/
//Employers
Breadcrumbs::register('backend.compliance.employer.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.backend.member.employer.title.index'), route('backend.compliance.employer.index'));
});

//Merge Employers
Breadcrumbs::register('backend.compliance.employer.merge', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.index');
    $breadcrumbs->push("Merge Employers", route('backend.compliance.employer.merge'));
});

//UnMerge Employers
Breadcrumbs::register('backend.compliance.employer.unmerge', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer.index');
    $breadcrumbs->push("DeMerge Employers", route('backend.compliance.employer.unmerge'));
});

//Employer Profile
Breadcrumbs::register('backend.compliance.employer.profile', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.index');
    $breadcrumbs->push(trans('labels.backend.compliance.view_profile'), route('backend.compliance.employer.profile',$employer));
});

//Edit
Breadcrumbs::register('backend.compliance.employer.edit', function ($breadcrumbs,  $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile',$employer);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.compliance.employer.edit', $employer));
});


Breadcrumbs::register('backend.compliance.employer.edit_gen_info', function ($breadcrumbs,  $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile',$employer);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.compliance.employer.edit_gen_info', $employer));
});

Breadcrumbs::register('backend.compliance.employer.verification', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push(trans('labels.backend.member.verification.list'), route('backend.compliance.employer.verification',$employer));
});

Breadcrumbs::register('backend.compliance.employer.incident', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push("Online Notification Application", route('backend.compliance.employer.incident',$employer));
});

Breadcrumbs::register('backend.compliance.employer.advance_payment', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push("Advance Payment Requests", route('backend.compliance.employer.advance_payment', $employer));
});

Breadcrumbs::register('backend.compliance.employer.close_business', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push("Close Business", route('backend.compliance.employer.close_business',$employer));
});

Breadcrumbs::register('backend.compliance.employer.open_business', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push("Open Business", route('backend.compliance.employer.open_business',$employer));
});

Breadcrumbs::register('backend.compliance.employer.verification_profile', function ($breadcrumbs, $verification) {
    $breadcrumbs->parent('backend.compliance.employer.verification', $verification->employer->id);
    $breadcrumbs->push(trans('labels.backend.member.verification.profile'), route('backend.compliance.employer.verification_profile',$verification));
});

Breadcrumbs::register('backend.compliance.employer.incident_profile', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.compliance.employer.incident', $incident->employer->id);
    $breadcrumbs->push("Profile", route('backend.compliance.employer.incident_profile', $incident));
});

Breadcrumbs::register('backend.compliance.employer.advance_payment_profile', function ($breadcrumbs, $advance_payment) {
    $breadcrumbs->parent('backend.compliance.employer.advance_payment', $advance_payment->employer->id);
    $breadcrumbs->push("Advance Payment Profile", route('backend.compliance.employer.advance_payment_profile', $advance_payment));
});

Breadcrumbs::register('backend.compliance.closed_business', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('Closed Business from TRA'), route('backend.compliance.closed_business'));
});

Breadcrumbs::register('backend.compliance.closed_business.show', function ($breadcrumbs, $closed_business) {
    $breadcrumbs->parent('backend.compliance.closed_business');
    $breadcrumbs->push("Closed Business Profile (TRA)", route('backend.compliance.closed_business.show', $closed_business));
});

//
//Employer Write off Interest
Breadcrumbs::register('backend.compliance.employer.write_off_interest_approve', function ($breadcrumbs,
    $interest_write_off) {
    $employer = new EmployerRepository();
    $employer = $employer->getWriteOffEmployer($interest_write_off);
    $breadcrumbs->parent('backend.compliance.employer.profile',$employer->id);
    $breadcrumbs->push(trans('labels.backend.table.write_off_interest'), route('backend.compliance.employer.write_off_interest_approve', $interest_write_off));
});


//write off page
Breadcrumbs::register('backend.compliance.employer.write_off_interest_page', function ($breadcrumbs,  $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile',$employer);
    $breadcrumbs->push(trans('labels.backend.table.write_off_interest'), route('backend.compliance.employer.write_off_interest_page', $employer));
});


////Employer adjust Interest approve
Breadcrumbs::register('backend.compliance.employer.interest_profile', function ($breadcrumbs, $booking_interest) {
    $employer = new EmployerRepository();
    $employer = $employer->getBookingInterestEmployer($booking_interest);
    $breadcrumbs->parent('backend.compliance.employer.profile',$employer->id);
    $breadcrumbs->push(trans('labels.backend.table.interest'), route('backend.compliance.employer.interest_profile',$booking_interest));
});

//
//////Employer adjust Interest
Breadcrumbs::register('backend.compliance.employer.adjust_interest', function ($breadcrumbs, $booking_interest) {
    $breadcrumbs->parent('backend.compliance.employer.interest_profile',$booking_interest);
    $breadcrumbs->push(trans('labels.backend.table.adjust'), route('backend.compliance.employer.adjust_interest',$booking_interest));
});


/*Employer Interest Refund */

Breadcrumbs::register('backend.compliance.booking_interest.refund_overview', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile',$employer);
    $breadcrumbs->push('Interest Refund', route('backend.compliance.booking_interest.refund_overview', $employer));
});


Breadcrumbs::register('backend.compliance.booking_interest.refunding_page', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.booking_interest.refund_overview',$employer);
    $breadcrumbs->push('Selection', route('backend.compliance.booking_interest.refunding_page', $employer));
});

Breadcrumbs::register('backend.compliance.booking_interest.refund_profile', function ($breadcrumbs, $interest_refund) {
    $interestRefund = \App\Models\Finance\Receivable\BookingInterestRefund::find($interest_refund);
    $employer = $interestRefund->getEmployer();
    $breadcrumbs->parent('backend.compliance.booking_interest.refund_overview',$employer->id);
    $breadcrumbs->push('Profile', route('backend.compliance.booking_interest.refund_profile', $interest_refund));
});


Breadcrumbs::register('backend.compliance.booking_interest.refund.edit', function ($breadcrumbs, $interest_refund) {
    $breadcrumbs->parent('backend.compliance.booking_interest.refund_profile',$interest_refund);
    $breadcrumbs->push('Modify', route('backend.compliance.booking_interest.refund.edit', $interest_refund));
});


//--end employer


// ---EMPLOYEE
//Employers
Breadcrumbs::register('backend.compliance.employee.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.employees'), route('backend.compliance.employee.index'));
});


//Employee Profile
Breadcrumbs::register('backend.compliance.employee.profile', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.compliance.employee.index');
    $breadcrumbs->push(trans('labels.backend.compliance.view_profile'), route('backend.compliance.employee.profile',$employee));
});



/*Add new Employee*/
Breadcrumbs::register('backend.compliance.employee.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employee.index');
    $breadcrumbs->push(trans('labels.general.add_new'), route('backend.compliance.employee.create'));
});


/*Employee Edit*/
Breadcrumbs::register('backend.compliance.employee.edit', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.compliance.employee.profile', $employee);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.compliance.employee.edit',$employee));
});


/*Employee Add Contribution*/
Breadcrumbs::register('backend.compliance.employee.add_contribution', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.compliance.employee.profile', $employee);
    $breadcrumbs->push(trans('labels.backend.member.add_contribution'), route('backend.compliance.employee.add_contribution',$employee));
});



//Employee Profile / old compensation
Breadcrumbs::register('backend.compliance.employee.create_old_compensation', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.compliance.employee.profile', $employee);
    $breadcrumbs->push(trans('labels.backend.claim.add_old_compensation'), route('backend.compliance.employee.create_old_compensation',$employee));
});

Breadcrumbs::register('backend.compliance.employee.edit_old_compensation', function ($breadcrumbs, $employment_old_compensation_id) {
    $employment_old_compensation = new EmployeeOldCompensationRepository();
    $employment_old_compensation = $employment_old_compensation->findOrThrowException($employment_old_compensation_id);
    $breadcrumbs->parent('backend.compliance.employee.profile', $employment_old_compensation->employee_id);
    $breadcrumbs->push(trans('labels.backend.claim.edit_old_compensation'), route('backend.compliance.employee.edit_old_compensation',$employment_old_compensation_id));
});


////Employee Profile / employment history
Breadcrumbs::register('backend.compliance.employee.create_employment_history', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.compliance.employee.profile', $employee);
    $breadcrumbs->push(trans('labels.backend.member.add_employment_history'), route('backend.compliance.employee.create_employment_history',$employee));
});

Breadcrumbs::register('backend.compliance.employee.edit_employment_history', function ($breadcrumbs, $employment_history_id) {
    $employment_history = new EmploymentHistoryRepository();
    $employment_history = $employment_history->findOrThrowException($employment_history_id);
    $breadcrumbs->parent('backend.compliance.employee.profile', $employment_history->employee_id);
    $breadcrumbs->push(trans('labels.backend.member.edit_employment_history'), route('backend.compliance.employee.edit_employment_history',$employment_history_id));
});


////Employee Profile / Dependents
//add
Breadcrumbs::register('backend.compliance.dependent.create_new', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('backend.compliance.employee.profile', $employee);
    $breadcrumbs->push(trans('labels.backend.member.add_dependent'), route('backend.compliance.dependent.create_new',$employee));
});
//Edit
Breadcrumbs::register('backend.compliance.dependent.edit_dependent', function ($breadcrumbs, $dependent_id,$employee) {
 $breadcrumbs->parent('backend.compliance.employee.profile', $employee);
 $breadcrumbs->push(trans('labels.backend.member.edit_dependent'), route('backend.compliance.dependent.edit_dependent',[$dependent_id, $employee]));
});

// choose employee for notification report
Breadcrumbs::register('backend.claim.notification_report.choose_employee', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push("Choose Employee", route('backend.claim.notification_report.choose_employee'));
});

//notification report created
Breadcrumbs::register('backend.claim.notification_report.created', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.choose_employee');
    $breadcrumbs->push("Notification Created", route('backend.claim.notification_report.created', $incident));
});

//resource allocation --> backend.claim.notification_report.allocation
Breadcrumbs::register('backend.claim.notification_report.allocation', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push("Resource Allocation", route('backend.claim.notification_report.allocation'));
});

//resource allocation --> backend.claim.notification_report.allocation
Breadcrumbs::register('backend.claim.notification_report.defaults', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push('Notification Defaults', route('backend.claim.notification_report.defaults'));
});

Breadcrumbs::register('backend.claim.notification_report.checklist_allocation', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.notification_report.defaults');
    $breadcrumbs->push('Checklist Allocation', route('backend.claim.notification_report.checklist_allocation'));
});
/*Payroll defauls*/
Breadcrumbs::register('backend.claim.allocate_payroll_defaults', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.notification_report.defaults');
    $breadcrumbs->push('Payroll Defaults', route('backend.claim.allocate_payroll_defaults'));
});

Breadcrumbs::register('backend.claim.notification_report.information_dashboard', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push("Information Dashboard", route('backend.claim.notification_report.information_dashboard'));
});

Breadcrumbs::register('backend.claim.notification_report.performance_report', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.notification_report.administration');
    $breadcrumbs->push("Claim Performance Reports", route('backend.claim.notification_report.performance_report'));
});

Breadcrumbs::register('backend.claim.notification_report.view_performance_report', function ($breadcrumbs, $configurable) {
    $breadcrumbs->parent('backend.claim.notification_report.performance_report');
    $breadcrumbs->push($configurable->name, route('backend.claim.notification_report.performance_report', $configurable));
});

Breadcrumbs::register('backend.claim.notification_report.online_requests', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push("Online Notification Application", route('backend.claim.notification_report.online_requests'));
});

Breadcrumbs::register('backend.claim.notification_report.staff_performance_report', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push("Staff Performance Report", route('backend.claim.notification_report.staff_performance_report'));
});

Breadcrumbs::register('backend.claim.notification_report.online_account_validation', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push("Online Notification Account Validation", route('backend.claim.notification_report.online_account_validation'));
});

Breadcrumbs::register('backend.claim.notification_report.online_account_validated', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push("Online Notification Account Validated", route('backend.claim.notification_report.online_account_validated'));
});

Breadcrumbs::register('backend.claim.notification_report.online_account_validation_profile', function ($breadcrumbs, $claim) {
    $breadcrumbs->parent('backend.claim.notification_report.online_account_validation');
    $breadcrumbs->push("Online Notification Account Validation Profile", route('backend.claim.notification_report.online_account_validation_profile', $claim));
});

////Employee Profile / notification report
Breadcrumbs::register('backend.claim.notification_report.profile', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.compliance.employee.profile', $incident->employee_id);
    $breadcrumbs->push(trans('labels.backend.claim.notification_report'), route('backend.claim.notification_report.profile', $incident->id));
});

//Progressive Notification
//--> Update Checklist
Breadcrumbs::register('backend.claim.notification_report.update_checklist', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Update Notification Checklist", route('backend.claim.notification_report.update_checklist', $incident->id));
});

Breadcrumbs::register('backend.claim.notification_report.update_details', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Update Notification Details", route('backend.claim.notification_report.update_details', $incident->id));
});

//--> Create Benefit Payment
Breadcrumbs::register('backend.claim.notification_report.initiate_benefit_payment', function ($breadcrumbs, $incident, $benefit) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Create " . $benefit->name . " Payment", route('backend.claim.notification_report.initiate_benefit_payment', ['incident' => $incident->id, 'benefit' => $benefit->id]));
});

//--> Edit Benefit Payment (backend.claim.notification_report.edit_benefit_payment)
Breadcrumbs::register('backend.claim.notification_report.edit_benefit_payment', function ($breadcrumbs, $incident, $benefit, $eligible) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Edit " . $benefit->name . " Payment", route('backend.claim.notification_report.edit_benefit_payment', ['incident' => $incident->id, 'benefit' => $benefit->id, 'eligible' => $eligible->id]));
});

//--> Edit Td Payment Assessment (backend.claim.notification_report.td_assessment)
Breadcrumbs::register('backend.claim.notification_report.td_assessment', function ($breadcrumbs, $incident, $benefit, $eligible) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push($benefit->name . " Payment Assessment", route('backend.claim.notification_report.td_assessment', ['incident' => $incident->id, 'benefit' => $benefit->id, 'eligible' => $eligible->id]));
});

//--> Edit Pd Payment Assessment (backend.claim.notification_report.edit_benefit_payment)
Breadcrumbs::register('backend.claim.notification_report.pd_assessment', function ($breadcrumbs, $incident, $benefit, $eligible) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push($benefit->name . " Payment Assessment", route('backend.claim.notification_report.pd_assessment', ['incident' => $incident->id, 'benefit' => $benefit->id, 'eligible' => $eligible->id]));
});

/*Update OSH Data*/
Breadcrumbs::register('backend.claim.notification.update_osh_data', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push('Update OSH Data', route('backend.claim.notification.update_osh_data', ['incident' => $incident->id]));
});

//--> Archive Workflow ...
Breadcrumbs::register('backend.workflow.archive_workflow', function ($breadcrumbs, $resource_id, $wf_module_group_id, $type, $action) {
    $breadcrumbs->parent('backend.system.menu');
    $title = ($action) ? 'Archive Workflow' : 'Un-archive Workflow';
    $breadcrumbs->push($title, route('backend.workflow.archive_workflow', ['resource_id' => $resource_id, 'wf_module_group_id' => $wf_module_group_id, 'type' => $type, 'action' => $action]));
});

Breadcrumbs::register('backend.workflow.edit_archive_workflow', function ($breadcrumbs, $wf_archive) {
    $breadcrumbs->parent('backend.system.menu');
    $breadcrumbs->push('Edit Archive Workflow', route('backend.workflow.edit_archive_workflow', $wf_archive));
});

Breadcrumbs::register('backend.claim.notification_report.attend', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.checker.index');
    $breadcrumbs->push("Attend Notification", route('backend.claim.notification_report.attend', $incident->id));
});

//--> Add Contribution Progressive
Breadcrumbs::register('backend.claim.notification_report.add_contribution', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Add Contribution", route('backend.claim.notification_report.add_contribution', $incident->id));
});

//--> Edit Contribution Progressive
Breadcrumbs::register('backend.claim.notification_report.edit_contribution', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Edit Contribution", route('backend.claim.notification_report.edit_contribution', $incident->id));
});

//--> Register Employee Progressive
Breadcrumbs::register('backend.claim.notification_report.register_employee', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Register Employee", route('backend.claim.notification_report.register_employee', $incident->id));
});

/*notification received: Recall*/
Breadcrumbs::register('backend.claim.notification_report.recall', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push(trans('labels.backend.report.notification_report_received'), route('backend.claim.notification_report.recall'));
});

////Employee Profile / notification report / medical expense
Breadcrumbs::register('backend.claim.notification_report.create_medical_expense', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.backend.claim.add_medical_expense'), route('backend.claim.notification_report.create_medical_expense', $incident->id));
});

Breadcrumbs::register('backend.claim.notification_report.ackwlgletter', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Acknowledgement Letter", route('backend.claim.notification_report.ackwlgletter', $incident->id));
});

Breadcrumbs::register('backend.claim.notification_report.edit_documents', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.backend.claim.edit_document'), route('backend.claim.notification_report.edit_documents', $incident->id));
});

////Employee Profile / notification report / update medical expense
Breadcrumbs::register('backend.claim.notification_report.edit_medical_expense', function ($breadcrumbs, $medical_expense) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $medical_expense->notificationReport);
    $breadcrumbs->push(trans('labels.backend.claim.edit_medical_expense'), route('backend.claim.notification_report.edit_medical_expense', $medical_expense->id));
});


////Employee Profile / notification report / modify
Breadcrumbs::register('backend.claim.notification_report.modify', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.general.modify'), route('backend.claim.notification_report.modify', $incident->id));
});

//notification report / modify progressive
Breadcrumbs::register('backend.claim.notification_report.modify_progressive', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.general.modify'), route('backend.claim.notification_report.modify_progressive', $incident->id));
});


////Employee Profile / notification report / assign investigators
Breadcrumbs::register('backend.claim.notification_report.assign_investigators', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.backend.claim.assign_investigators'), route('backend.claim.notification_report.assign_investigators', $incident->id));
});


////Employee Profile / notification report / change investigatr
Breadcrumbs::register('backend.claim.notification_report.edit_investigator', function ($breadcrumbs, $notification_investigator) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $notification_investigator->notificationReport);
    $breadcrumbs->push(trans('labels.backend.claim.change_investigator'), route('backend.claim.notification_report.edit_investigator', $notification_investigator->id));
});


////Employee Profile / notification report / investigation reports / feed back update
Breadcrumbs::register('backend.claim.notification_report.edit_investigation_report', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.backend.claim.investigation_report'), route('backend.claim.notification_report.edit_investigation_report', $incident));
});


////Employee Profile / notification report / new health provider services
Breadcrumbs::register('backend.claim.notification_report.create_health_provider_service', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.backend.claim.add_health_provider_service'), route('backend.claim.notification_report.create_health_provider_service', $incident->id));
});


////Employee Profile / notification report / update health provider services
Breadcrumbs::register('backend.claim.notification_report.edit_health_provider_service', function ($breadcrumbs, $notification_health_provider) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $notification_health_provider->notificationReport);
    $breadcrumbs->push(trans('labels.backend.claim.edit_health_provider_service'), route('backend.claim.notification_report.edit_health_provider_service', $notification_health_provider->id));
});

// Current employee State
//create
Breadcrumbs::register('backend.claim.notification_report.create_current_employee_state', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.backend.claim.add_current_employee_state'), route('backend.claim.notification_report.create_current_employee_state', $incident->id));
});

//update
Breadcrumbs::register('backend.claim.notification_report.edit_current_employee_state', function ($breadcrumbs, $medical_expense) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $medical_expense->notificationReport);
    $breadcrumbs->push(trans('labels.backend.claim.edit_current_employee_state'), route('backend.claim.notification_report.edit_current_employee_state', $medical_expense->id));
});

/**
 *
 * NOTIFICATION CONTRIBUTION TRACKS
 */
Breadcrumbs::register('backend.claim.notification_contribution_track.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.home');
    $breadcrumbs->push(trans('labels.general.pending'), route('backend.claim.notification_contribution_track.index'));
});

Breadcrumbs::register('backend.claim.notification_contribution_track.profile', function ($breadcrumbs,  $notification_report) {
    $breadcrumbs->parent('backend.claim.notification_contribution_track.index');
    $breadcrumbs->push(trans('labels.general.profile'), route('backend.claim.notification_contribution_track.profile',$notification_report));
});



Breadcrumbs::register('backend.claim.notification_contribution_track.edit', function ($breadcrumbs, $track_id) {
    $notificationContributionTrack = new
    \App\Repositories\Backend\Operation\Claim\NotificationContributionTrackRepository();
    $track = $notificationContributionTrack->findOrThrowException($track_id);
    $breadcrumbs->parent('backend.claim.notification_contribution_track.profile', $track->notification_report_id);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.claim.notification_contribution_track.edit',
        $track_id));
});


/*edit monthly earning*/
Breadcrumbs::register('backend.claim.notification_report.edit_monthly_earning', function ($breadcrumbs, $notification_report) {
    $breadcrumbs->parent('backend.claim.notification_contribution_track.profile', $notification_report);
    $breadcrumbs->push(trans('labels.backend.claim.edit_gross_pay'), route('backend.claim.notification_report.edit_monthly_earning',$notification_report
));
});

/* End of contribution track -------*/

/*Approve rejection appeal*/

Breadcrumbs::register('backend.claim.notification_report.approve_rejection_appeal_page', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push('Rejection Appeal', route('backend.claim.notification_report.approve_rejection_appeal_page', $incident->id));
});

/*end approve rejection appeal*/



/*-----WITNESS---*/

//Create
Breadcrumbs::register('backend.claim.notification_report.create_accident_witness', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.backend.claim.add_witness'), route('backend.claim.notification_report.create_accident_witness', $incident->id));
});
//update
Breadcrumbs::register('backend.claim.notification_report.edit_accident_witness', function ($breadcrumbs, $witness) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $witness->accidents()->first()->notificationReport);
    $breadcrumbs->push(trans('labels.backend.claim.edit_witness'), route('backend.claim.notification_report.create_accident_witness', $witness->id));
});


/*Doctors Notification report Modification*/
//edit
Breadcrumbs::register('backend.claim.notification_report.doctors_modification', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push(trans('labels.backend.claim.modify_notification_report'), route('backend.claim.notification_report.doctors_modification', $incident->id));
});

Breadcrumbs::register('backend.claim.notification_report.close_incident', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Close Incident", route('backend.claim.notification_report.close_incident',$incident));
});

Breadcrumbs::register('backend.claim.notification_report.do_manual_payment', function ($breadcrumbs, $incident) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $incident);
    $breadcrumbs->push("Update Manual Payment", route('backend.claim.notification_report.do_manual_payment',$incident));
});

/* Medical Practitioner  */
//index
Breadcrumbs::register('backend.claim.medical_practitioner.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push(trans('labels.backend.claim.medical_practitioners'), route('backend.claim.medical_practitioner.index'));
});

//create
Breadcrumbs::register('backend.claim.medical_practitioner.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.medical_practitioner.index');
    $breadcrumbs->push(trans('labels.backend.claim.add_medical_practitioner'), route('backend.claim.medical_practitioner.create'));
});

//edit
Breadcrumbs::register('backend.claim.medical_practitioner.edit', function ($breadcrumbs,$medical_practitioner) {
    $breadcrumbs->parent('backend.claim.medical_practitioner.index');
    $breadcrumbs->push(trans('labels.backend.claim.edit_medical_practitioner'), route('backend.claim.medical_practitioner.edit',$medical_practitioner));
});
//--end medical practitioner ---------

/*  Health Provider  */
//index
Breadcrumbs::register('backend.claim.health_provider.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push(trans('labels.backend.claim.health_providers'), route('backend.claim.health_provider.index'));
});

//create
Breadcrumbs::register('backend.claim.health_provider.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.health_provider.index');
    $breadcrumbs->push(trans('labels.backend.claim.add_health_provider'), route('backend.claim.health_provider.create'));
});

//edit
Breadcrumbs::register('backend.claim.health_provider.edit', function ($breadcrumbs,$health_provider) {
    $breadcrumbs->parent('backend.claim.health_provider.index');
    $breadcrumbs->push(trans('labels.backend.claim.edit_health_provider'), route('backend.claim.health_provider.edit',$health_provider));
});

/*upload bulk*/
Breadcrumbs::register('backend.claim.health_provider.upload_bulk_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.health_provider.index');
    $breadcrumbs->push(trans('labels.general.upload_bulk'), route('backend.claim.health_provider.upload_bulk_page'));
});



//--end health provider ---------


/*  Insurances  */
//index
Breadcrumbs::register('backend.claim.insurance.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push(trans('labels.backend.claim.insurances'), route('backend.claim.insurance.index'));
});

//create
Breadcrumbs::register('backend.claim.insurance.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.insurance.index');
    $breadcrumbs->push(trans('labels.general.add_new'), route('backend.claim.insurance.create'));
});

//edit
Breadcrumbs::register('backend.claim.insurance.edit', function ($breadcrumbs,$insurance) {
    $breadcrumbs->parent('backend.claim.insurance.index');
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.claim.insurance.edit',$insurance));
});
//--end health provider ---------





/*Claim Assessment=====*/
Breadcrumbs::register('backend.claim.notification_report.claim_assessment_checklist', function ($breadcrumbs, $medical_expense) {
    $breadcrumbs->parent('backend.claim.notification_report.profile', $medical_expense->notificationReport);
    $breadcrumbs->push(trans('labels.backend.claim.claim_assessment'), route('backend.claim.notification_report.claim_assessment_checklist', $medical_expense->id));
});
//end claim assessment --------


/** Start: Inspection */
Breadcrumbs::register('backend.compliance.inspection.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.title'), route('backend.compliance.inspection.index'));
});
Breadcrumbs::register('backend.compliance.inspection.current', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.inspection.index');
    $breadcrumbs->push(trans('labels.general.current'), route('backend.compliance.inspection.current'));
});
Breadcrumbs::register('backend.compliance.inspection.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.inspection.index');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.create'), route('backend.compliance.inspection.create'));
});
Breadcrumbs::register('backend.compliance.inspection.show', function ($breadcrumbs, $inspection) {
    $breadcrumbs->parent('backend.compliance.inspection.index');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.dashboard'), route('backend.compliance.inspection.show', $inspection));
});
Breadcrumbs::register('backend.compliance.inspection.allocation', function ($breadcrumbs, $inspection) {
    $breadcrumbs->parent('backend.compliance.inspection.show', $inspection);
    $breadcrumbs->push("Inspection Resource Allocation", route('backend.compliance.inspection.allocation', $inspection));
});
Breadcrumbs::register('backend.compliance.inspection.task.create', function ($breadcrumbs, $inspection) {
    $breadcrumbs->parent('backend.compliance.inspection.show', $inspection->id);
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.task.create'), route('backend.compliance.inspection.task.create', $inspection));
});
Breadcrumbs::register('backend.compliance.inspection.edit', function ($breadcrumbs, $inspection) {
    $breadcrumbs->parent('backend.compliance.inspection.show', $inspection->id);
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.edit'), route('backend.compliance.inspection.edit', $inspection));
});
Breadcrumbs::register('backend.compliance.inspection.assign_inspectors', function ($breadcrumbs, $inspection) {
    $breadcrumbs->parent('backend.compliance.inspection.show', $inspection->id);
    $breadcrumbs->push("Assign Inspectors", route('backend.compliance.inspection.assign_inspectors', $inspection));
});
Breadcrumbs::register('backend.compliance.inspection.task.show', function ($breadcrumbs, $task) {
    $breadcrumbs->parent('backend.compliance.inspection.show', $task->inspection->id);
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.task.dashboard'), route('backend.compliance.inspection.task.show', $task));
});
Breadcrumbs::register('backend.compliance.inspection.task.edit', function ($breadcrumbs, $task) {
    $breadcrumbs->parent('backend.compliance.inspection.task.show', $task);
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.task.edit'), route('backend.compliance.inspection.task.edit', $task));
});
Breadcrumbs::register('backend.compliance.inspection.employer_task.show', function ($breadcrumbs, $employer_task) {
    $breadcrumbs->parent('backend.compliance.inspection.task.show', $employer_task->task);
    $breadcrumbs->push("Employer Inspection Task", route('backend.compliance.inspection.employer_task.show', $employer_task));
});
Breadcrumbs::register('backend.compliance.inspection.employer_task.attend', function ($breadcrumbs, $employer_task) {
    $breadcrumbs->parent('backend.compliance.inspection.task.show', $employer_task->task);
    $breadcrumbs->push("Attend Employer Task", route('backend.compliance.inspection.employer_task.attend', $employer_task));
});
Breadcrumbs::register('backend.compliance.inspection.employer_task.attach_document', function ($breadcrumbs, $employer_task) {
    $breadcrumbs->parent('backend.compliance.inspection.task.show', $employer_task->task);
    $breadcrumbs->push("Employer Inspection Task", route('backend.compliance.inspection.employer_task.attach_document', $employer_task));
});
Breadcrumbs::register('backend.compliance.inspection.employer_task.edit_document', function ($breadcrumbs, $doc_pivot_id) {
    $uploaded_doc = \Illuminate\Support\Facades\DB::table('document_employer_inspection_task')->where('id', $doc_pivot_id)->first();
    $employer_task = \App\Models\Operation\Compliance\Inspection\EmployerInspectionTask::query()->where('id', $uploaded_doc->employer_inspection_task_id)->first();
    //dd($employer_task->inspection);
    $breadcrumbs->parent('backend.compliance.inspection.task.show', $employer_task->task);
    $breadcrumbs->push('Edit Doc', route('backend.compliance.inspection.employer_task.edit_document', $doc_pivot_id));
});
Breadcrumbs::register('backend.compliance.inspection.employer_task.update_visitation', function ($breadcrumbs, $employer_task) {
    $breadcrumbs->parent('backend.compliance.inspection.employer_task.show', $employer_task);
    $breadcrumbs->push("Employer Inspection Task", route('backend.compliance.inspection.employer_task.update_visitation', $employer_task));
});
Breadcrumbs::register('backend.compliance.inspection.employer_task.manage_payroll', function ($breadcrumbs, $employer_task) {
    $breadcrumbs->parent('backend.compliance.inspection.employer_task.show', $employer_task);
    $breadcrumbs->push("Employer Inspection Task", route('backend.compliance.inspection.employer_task.manage_payroll', $employer_task));
});
Breadcrumbs::register('backend.compliance.inspection.employer_task.upload_assessment_template', function ($breadcrumbs, $employer_task) {
    $breadcrumbs->parent('backend.compliance.inspection.task.show', $employer_task->task);
    $breadcrumbs->push("Upload Assessment Schedule", route('backend.compliance.inspection.employer_task.upload_assessment_template', $employer_task));
});
Breadcrumbs::register('backend.compliance.inspection.profile.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.inspection.index');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.profile.title'), route('backend.compliance.inspection.profile.index'));
});
Breadcrumbs::register('backend.compliance.inspection.profile.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.inspection.profile.index');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.profile.create'), route('backend.compliance.inspection.profile.create'));
});
Breadcrumbs::register('backend.compliance.inspection.profile.show', function ($breadcrumbs, $profile) {
    $breadcrumbs->parent('backend.compliance.inspection.profile.index');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.profile.dashboard'), route('backend.compliance.inspection.profile.show', $profile));
});
Breadcrumbs::register('backend.compliance.inspection.profile.edit', function ($breadcrumbs, $profile) {
    $breadcrumbs->parent('backend.compliance.inspection.profile.show', $profile->id);
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.task.edit'), route('backend.compliance.inspection.profile.edit', $profile));
});


/** end: Inspection */



/*  Unregistered Employers */

// index
Breadcrumbs::register('backend.compliance.unregistered_employer.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.unregistered_employer'), route('backend.compliance.unregistered_employer.index'));
});
// create
Breadcrumbs::register('backend.compliance.unregistered_employer.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.index');
    $breadcrumbs->push(trans('labels.general.add_new'), route('backend.compliance.unregistered_employer.create'));
});

//Add from tra
Breadcrumbs::register('backend.compliance.unregistered_employer.add_tra', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.index');
    $breadcrumbs->push("Add New From TRA", route('backend.compliance.unregistered_employer.add_tra'));
});

//profile
Breadcrumbs::register('backend.compliance.unregistered_employer.profile', function ($breadcrumbs, $unregistered_employer) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.index');
    $breadcrumbs->push(trans('labels.general.profile'), route('backend.compliance.unregistered_employer.profile', $unregistered_employer));
});


//modify
Breadcrumbs::register('backend.compliance.unregistered_employer.edit', function ($breadcrumbs, $unregistered_employer) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.profile', $unregistered_employer);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.compliance.unregistered_employer.edit', $unregistered_employer));
});


//assign staff
Breadcrumbs::register('backend.compliance.unregistered_employer.assign_staff_page', function ($breadcrumbs, $unregistered_employer) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.profile', $unregistered_employer);
    $breadcrumbs->push(trans('labels.backend.member.staff_assignment'), route('backend.compliance.unregistered_employer.assign_staff_page', $unregistered_employer));
});


//change assign staff
Breadcrumbs::register('backend.compliance.unregistered_employer.assigned_staff_edit', function ($breadcrumbs, $unregistered_employer) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.profile', $unregistered_employer);
    $breadcrumbs->push(trans('labels.backend.member.change_staff_assignment'), route('backend.compliance.unregistered_employer.assigned_staff_edit', $unregistered_employer));
});


// Add follow up
Breadcrumbs::register('backend.compliance.unregistered_employer.follow_up_create', function ($breadcrumbs, $unregistered_employer) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.profile',$unregistered_employer);
    $breadcrumbs->push(trans('labels.backend.member.add_new_follow_up'), route('backend.compliance.unregistered_employer.follow_up_create', $unregistered_employer));
});

// edit follow up
Breadcrumbs::register('backend.compliance.unregistered_employer.follow_up_edit', function ($breadcrumbs, $followup_id) {
    $followups = new \App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerFollowUpRepository();
    $followup = $followups->findOrThrowException($followup_id);
    $breadcrumbs->parent('backend.compliance.unregistered_employer.profile', $followup->unregistered_employer_id);
    $breadcrumbs->push(trans('labels.backend.member.edit_follow_up'), route('backend.compliance.unregistered_employer.follow_up_edit', $followup_id));
});


//upload bulk active
Breadcrumbs::register('backend.compliance.unregistered_employer.upload_bulk', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.index');
    $breadcrumbs->push(trans('labels.backend.member.upload_unregistered_employers'), route('backend.compliance.unregistered_employer.upload_bulk'));
});


//upload bulk in active
Breadcrumbs::register('backend.compliance.unregistered_employer.upload_bulk_inactive', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.index');
    $breadcrumbs->push(trans('labels.backend.member.upload_inactive_unregistered_employers'), route('backend.compliance.unregistered_employer.upload_bulk_inactive'));
});





/***  Followup Inspection  ****/

Breadcrumbs::register('backend.compliance.unreg_followup_inspection.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unregistered_employer.index');
    $breadcrumbs->push(trans('labels.backend.member.unregistered_follow_up_inspection'), route('backend.compliance.unreg_followup_inspection.index'));
});


Breadcrumbs::register('backend.compliance.unreg_followup_inspection.current', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unreg_followup_inspection.index');
    $breadcrumbs->push(trans('labels.general.current'), route('backend.compliance.unreg_followup_inspection.current'));
});

Breadcrumbs::register('backend.compliance.unreg_followup_inspection.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unreg_followup_inspection.index');
    $breadcrumbs->push(trans('labels.general.add_new'), route('backend.compliance.unreg_followup_inspection.create'));
});
Breadcrumbs::register('backend.compliance.unreg_followup_inspection.show', function ($breadcrumbs, $inspection) {
    $breadcrumbs->parent('backend.compliance.unreg_followup_inspection.index');
    $breadcrumbs->push(trans('labels.general.profile'), route('backend.compliance.unreg_followup_inspection.show', $inspection));
});

Breadcrumbs::register('backend.compliance.unreg_followup_inspection_task.create_new', function ($breadcrumbs, $inspection) {
    $breadcrumbs->parent('backend.compliance.unreg_followup_inspection.show', $inspection);
    $breadcrumbs->push(trans('labels.general.add_new'), route('backend.compliance.unreg_followup_inspection_task.create_new',$inspection));
});

Breadcrumbs::register('backend.compliance.unreg_followup_inspection.edit', function ($breadcrumbs, $inspection) {
    $breadcrumbs->parent('backend.compliance.unreg_followup_inspection.show', $inspection);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.compliance.unreg_followup_inspection.edit', $inspection));
});

Breadcrumbs::register('backend.compliance.unreg_followup_inspection_task.show', function ($breadcrumbs, $task) {
    $tasks = new \App\Repositories\Backend\Operation\Compliance\Member\UnregisteredFollowupInspectionTaskRepository();
    $inspection_task = $tasks->findOrThrowException($task);
    $breadcrumbs->parent('backend.compliance.unreg_followup_inspection.show', $inspection_task->unregistered_followup_inspection_id);
    $breadcrumbs->push(trans('labels.general.task'), route('backend.compliance.unreg_followup_inspection_task.show', $task));
});

Breadcrumbs::register('backend.compliance.unreg_followup_inspection_task.edit', function ($breadcrumbs, $task) {
    $breadcrumbs->parent('backend.compliance.unreg_followup_inspection_task.show', $task);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.compliance.unreg_followup_inspection_task.edit', $task));
});

Breadcrumbs::register('backend.compliance.unregistered_inspection_profile.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unreg_followup_inspection.index');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.inspection.profile.title'), route('backend.compliance.unregistered_inspection_profile.index'));
});
Breadcrumbs::register('backend.compliance.unregistered_inspection_profile.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.unregistered_inspection_profile.index');
    $breadcrumbs->push(trans('labels.general.add_new'), route('backend.compliance.unregistered_inspection_profile.create'));
});
Breadcrumbs::register('backend.compliance.unregistered_inspection_profile.show', function ($breadcrumbs, $profile) {
    $breadcrumbs->parent('backend.compliance.unregistered_inspection_profile.index');
    $breadcrumbs->push(trans('labels.general.profile'), route('backend.compliance.unregistered_inspection_profile.show', $profile));
});
Breadcrumbs::register('backend.compliance.unregistered_inspection_profile.edit', function ($breadcrumbs, $profile) {
    $breadcrumbs->parent('backend.compliance.unregistered_inspection_profile.show', $profile);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.compliance.unregistered_inspection_profile.edit', $profile));
});







/** End of Followup Inspection **/


/* End of Unregistered Employers */




/**
 * EMPLOYER REGISTRATION==================
 */
//index
Breadcrumbs::register('backend.compliance.employer_registration.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.member.employer_registrations'), route('backend.compliance.employer_registration.index'));
});

//profile
Breadcrumbs::register('backend.compliance.employer_registration.profile', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer_registration.index');
    $breadcrumbs->push(trans('labels.general.profile'), route('backend.compliance.employer_registration.profile', $employer));
});

////name check
Breadcrumbs::register('backend.compliance.employer_registration.name_check', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer_registration.index');
    $breadcrumbs->push(trans('labels.backend.compliance.employer_name_check'), route('backend.compliance.employer_registration.name_check'));
});


/* upload employee list */
Breadcrumbs::register('backend.compliance.employer_registration.employee_list_file', function ($breadcrumbs,$employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push(trans('labels.backend.member.load_employee_list'), route('backend.compliance.employer_registration.employee_list_file',$employer));
});


//add
Breadcrumbs::register('backend.compliance.employer_registration.create', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.employer_registration.index');
    $breadcrumbs->push(trans('labels.general.add_new'), route('backend.compliance.employer_registration.create'));
});

Breadcrumbs::register('backend.compliance.employer_registration.edit', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer_registration.profile', $employer);
    $breadcrumbs->push(trans('labels.general.edit'), route('backend.compliance.employer_registration.edit', $employer));
});

/* eND OF EMPLOYER REGISTRATION */

//Start of ADES breadcrumbs

Breadcrumbs::register('backend.compliance.status', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.employer_status'), route('backend.compliance.status'));
});

Breadcrumbs::register('backend.compliance.payesdl', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push(trans('labels.backend.compliance_menu.payesdl'), route('backend.compliance.payesdl'));
});

//End of ADES breadcrumbs


/* Start of Employer business closure*/
Breadcrumbs::register('backend.compliance.employer.closure.index', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push('De-Registrations', route('backend.compliance.employer.closure.index', $employer));
});

Breadcrumbs::register('backend.compliance.employer.closure.create', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.closure.index', $employer);
    $breadcrumbs->push('Add', route('backend.compliance.employer.closure.create', $employer));
});

Breadcrumbs::register('backend.compliance.employer.closure.profile', function ($breadcrumbs, $employer_closure) {
    $employer  = $employer_closure->closedEmployer;
    $breadcrumbs->parent('backend.compliance.employer.closure.index', $employer);
    $breadcrumbs->push('Profile', route('backend.compliance.employer.closure.profile', $employer_closure));
});

Breadcrumbs::register('backend.compliance.employer.closure.edit', function ($breadcrumbs, $employer_closure) {
    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $employer_closure);
    $breadcrumbs->push('Edit', route('backend.compliance.employer.closure.edit', $employer_closure));
});

Breadcrumbs::register('backend.compliance.employer.closure.open_page_confirm_payrolls', function ($breadcrumbs, $employer_closure) {

    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $employer_closure);
    $breadcrumbs->push('Payroll(s)', route('backend.compliance.employer.closure.open_page_confirm_payrolls', $employer_closure));
});


Breadcrumbs::register('backend.compliance.employer.closure.attach_document', function ($breadcrumbs, $employer_closure) {
    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $employer_closure);
    $breadcrumbs->push('Attach Doc', route('backend.compliance.employer.closure.attach_document', $employer_closure));
});

Breadcrumbs::register('backend.compliance.employer.closure.edit_document', function ($breadcrumbs, $doc_pivot_id) {
    $uploaded_doc = \Illuminate\Support\Facades\DB::table('document_employer')->where('id', $doc_pivot_id)->first();
    $closure = \App\Models\Operation\Compliance\Member\EmployerClosure::query()->where('id', $uploaded_doc->external_id)->first();
    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $closure);
    $breadcrumbs->push('Edit Doc', route('backend.compliance.employer.closure.edit_document', $doc_pivot_id));
});


Breadcrumbs::register('backend.compliance.employer.closure.follow_up.create', function ($breadcrumbs, $employer_closure) {
    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $employer_closure);
    $breadcrumbs->push('Add Follow up', route('backend.compliance.employer.closure.follow_up.create', $employer_closure));
});


Breadcrumbs::register('backend.compliance.employer.closure.follow_up.edit', function ($breadcrumbs, $employer_closure_follow_up) {
    $closure = $employer_closure_follow_up->employerClosure;
    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $closure);
    $breadcrumbs->push('Edit Follow up', route('backend.compliance.employer.closure.follow_up.edit', $employer_closure_follow_up));
});

/*open*/

Breadcrumbs::register('backend.compliance.employer_closure.open.profile', function ($breadcrumbs, $employer_closure_reopen) {
    $employer_closure =$employer_closure_reopen->employerClosure;
    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $employer_closure);
    $breadcrumbs->push('Open Closure', route('backend.compliance.employer_closure.open.profile', $employer_closure_reopen));
});


Breadcrumbs::register('backend.compliance.employer_closure.open.create', function ($breadcrumbs, $employer) {
    $breadcrumbs->parent('backend.compliance.employer.profile', $employer);
    $breadcrumbs->push('Open Business', route('backend.compliance.employer_closure.open.create', $employer));
});


Breadcrumbs::register('backend.compliance.employer_closure.open.edit', function ($breadcrumbs, $employer_closure_reopen) {
    $breadcrumbs->parent('backend.compliance.employer_closure.open.profile', $employer_closure_reopen);
    $breadcrumbs->push('Modify', route('backend.compliance.employer_closure.open.edit', $employer_closure_reopen));
});


Breadcrumbs::register('backend.compliance.employer.closure.tempo_closure_status_followup_page', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.compliance.menu');
    $breadcrumbs->push('Closures', route('backend.compliance.employer.closure.tempo_closure_status_followup_page'));
});

Breadcrumbs::register('backend.compliance.employer.closure.fill_status_followup', function ($breadcrumbs, $employer_closure) {
    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $employer_closure);
    $breadcrumbs->push('Fill', route('backend.compliance.employer.closure.fill_status_followup', $employer_closure));
});
Breadcrumbs::register('backend.compliance.employer_closure.extension.create', function ($breadcrumbs, $employer_closure) {
    $breadcrumbs->parent('backend.compliance.employer_closure.extension.profile', $employer_closure);
    $breadcrumbs->push('Create', route('backend.compliance.employer_closure.extension.create', $employer_closure));
});

Breadcrumbs::register('backend.compliance.employer_closure.extension.profile', function ($breadcrumbs, $employer_closure_extension) {
    $breadcrumbs->parent('backend.compliance.employer.closure.profile', $employer_closure_extension->employerClosure);
    $breadcrumbs->push('Extension', route('backend.compliance.employer_closure.extension.profile', $employer_closure_extension));
});

Breadcrumbs::register('backend.compliance.employer_closure.extension.edit', function ($breadcrumbs, $employer_closure_extension) {
    $breadcrumbs->parent('backend.compliance.employer_closure.extension.profile', $employer_closure_extension);
    $breadcrumbs->push('Edit', route('backend.compliance.employer_closure.extension.edit', $employer_closure_extension));
});


Breadcrumbs::register('backend.assessment.hcp_hsp.menu', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.claim.menu');
    $breadcrumbs->push('HCP - HSP', route('backend.assessment.hcp_hsp.menu'));
});

Breadcrumbs::register('backend.assessment.hcp_hsp.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.assessment.hcp_hsp.menu');
    $breadcrumbs->push('Authorization', route('backend.assessment.hcp_hsp.index'));
});

Breadcrumbs::register('backend.assessment.hcp_hsp.billings', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.assessment.hcp_hsp.menu');
    $breadcrumbs->push('Billing', route('backend.assessment.hcp_hsp.billings'));
});

Breadcrumbs::register('backend.assessment.hcp_hsp.facilities', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.assessment.hcp_hsp.menu');
    $breadcrumbs->push('Facilities', route('backend.assessment.hcp_hsp.facilities'));
});

Breadcrumbs::register('backend.assessment.hcp_hsp.facility', function ($breadcrumbs, $facility_id) {
    $facility = \DB::table('main.facility_codes')->where('id',$facility_id)->first();
    $breadcrumbs->parent('backend.assessment.hcp_hsp.facilities');
    $breadcrumbs->push(ucwords(strtolower($facility->name)), route('backend.assessment.hcp_hsp.facility',  $facility_id));
});

/*End on employer business closure*/