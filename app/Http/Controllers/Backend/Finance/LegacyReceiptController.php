<?php

namespace App\Http\Controllers\Backend\Finance;

use App\DataTables\Compliance\ContributionLegacy\EmployerContributionLegacyDataTable;
use App\DataTables\Compliance\ContributionLegacy\GetReceiptContributionsDataTable;
use App\DataTables\Compliance\ContributionLegacy\GetReceiptInterestDataTable;
use App\DataTables\Finance\Receipt\LegacyReceiptDataTable;
use App\DataTables\WorkflowTrackDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Finance\Receipt\CancelReceiptRequest;
use App\Http\Requests\Backend\Finance\Receipt\ChooseEmployerRequest;
use App\Http\Requests\Backend\Finance\Receipt\DishonourReceiptRequest;
use App\Http\Requests\Backend\Finance\Receipt\ReplaceChequeRequest;
use App\Http\Requests\Backend\Finance\Receipt\StoreEmployerLegacyReceiptRequest;
use App\Http\Requests\Backend\Finance\Receipt\UpdateLegacyReceiptMonthsNewRequest;
use App\Http\Requests\Backend\Finance\Receipt\UpdateReceiptMonthsRequest;
use App\Http\Requests\Backend\Finance\Receipt\UploadManualContribution;
use App\Jobs\Finance\LoadManualContribution;
use App\Models\Finance\Receipt\LegacyReceipt;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Finance\Currency\CurrencyRepository;
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;

class LegacyReceiptController extends Controller
{

    use AttachmentHandler;

    protected $legacy_receipts;
    protected $banks;

    public function __construct() {
        $this->legacy_receipts = new LegacyReceiptRepository();
        $this->banks = new BankRepository();
        $this->middleware('access.routeNeedsPermission:upload_manual_receipts', ['only' => ['uploadContribution']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LegacyReceiptDataTable $dataTable)
    {
        //
        return $dataTable->render("backend/finance/legacy_receipt/index");
    }

    /**
     * @param LegacyReceiptDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */

    public function receivedContributions(EmployerContributionLegacyDataTable $dataTable)
    {
        //
        return $dataTable->render("backend/operation/compliance/member/contribution_legacy/index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Choose employer you wish to receipt
     */
    public function chooseEmployer()
    {
        return view("backend/finance/legacy_receipt/employer");
    }

    public function postChooseEmployer(ChooseEmployerRequest $request)
    {
        access()->hasWorkflowDefinition(7,1);
        $employer_id = $request->employer;
        $employer = new EmployerRepository();
        $payment_types = new PaymentTypeRepository();
        $currency = new CurrencyRepository();
        $bank = new BankRepository();
        $ContributionDateThreshold = Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1);
        return view("backend/finance/legacy_receipt/create_employer")
            ->withEmployerId($employer_id)
            ->withEmployer($employer->find($employer_id))
            ->withPaymentTypes($payment_types->getAll())
            ->withCurrencies($currency->getAll()->pluck('name', 'id'))
            ->withBanks($bank->getAll()->pluck('name', 'id'))
            ->withMaxEntries(sysdefs()->data()->total_allowed_contrib_months)
            ->withMonthThreshold(Carbon::parse($ContributionDateThreshold)->format("m"))
            ->withYearThreshold(Carbon::parse($ContributionDateThreshold)->format("Y"))
            ->withMinimumContribution(sysdefs()->data()->minimum_allowed_contribution)
            ->withThisDate(Carbon::now()->format('Y-n-j'));
    }


    public function contributionEntries()
    {
        return view('backend/finance/legacy_receipt/includes/monthly_contribution_entry')
            ->withSelectedEntries(request()->input('selected_entries'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployerLegacyReceiptRequest $request)
    {
        $receipt = $this->legacy_receipts->storeEmployerReceipt($request->all());
        return response()->json(['success' => true, 'id' => $receipt->id]);
    }



    /* Receipt Profile */
    public function profile($id,WorkflowTrackDataTable $workflowTrack, GetReceiptContributionsDataTable $contributionsDataTable, GetReceiptInterestDataTable $interestDataTable)
    {
        $legacy_receipt = $this->legacy_receipts->findOrThrowException($id);
        return view('backend/finance/legacy_receipt/profile')
            ->withLegacyReceipt($legacy_receipt)
            ->withWorkflowtrack($workflowTrack)
            ->withContributionDatatable($contributionsDataTable)
            ->withInterestDatatable($interestDataTable);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $legacy_receipt = $this->legacy_receipts->findOrThrowException($id);
        return view('backend.finance.legacy_receipt.action')
            ->withLegacyReceipt($legacy_receipt);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }






    public function editMonths(LegacyReceipt $legacyReceipt)
    {
//        workflow([['wf_module_group_id' => 7, 'resource_id' => $legacyReceipt->id]])->checkLevel(1);
        access()->hasWorkflowDefinition(7,1);
        $ContributionDateThreshold = Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1);
        /*edit receipt*/
        $payment_types = new PaymentTypeRepository();
        $currency = new CurrencyRepository();
        $bank = new BankRepository();
        $receipt_date = Carbon::parse($legacyReceipt->capture_date);
        $payment_date = Carbon::parse($legacyReceipt->rct_date);
        /*end edit receipt*/
        return view('backend/finance/legacy_receipt/edit')
            ->withLegacyReceipt($legacyReceipt)
            ->withThisDate(Carbon::now()->format('Y-n-j'))
            ->withContributionDateThreshold($ContributionDateThreshold)
            ->withMonthThreshold(Carbon::parse($ContributionDateThreshold)->format("m"))
            ->withYearThreshold(Carbon::parse($ContributionDateThreshold)->format("Y"))
            ->withMinimumContribution(sysdefs()->data()->minimum_allowed_contribution)
            ->withYearThreshold(Carbon::parse($ContributionDateThreshold)->format("Y"))
            /*edit receipt*/
            ->withReceiptDate($receipt_date)
            ->withPaymentDate($payment_date)
            ->withPaymentTypes($payment_types->getAll()->pluck('name', 'id'))
            ->withCurrencies($currency->getAll()->pluck('name', 'id'))
            ->withBanks($bank->getAll()->pluck('name', 'id'));
        /*end edit receipt*/
    }

    public function updateMonths(LegacyReceipt $legacyReceipt, UpdateLegacyReceiptMonthsNewRequest $request)
    {
        $this->legacy_receipts->updateMonths($legacyReceipt, $request->all());
        return response()->json(['success' => true, 'id' => $legacyReceipt->id]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /*
    * get active Contributions for this receipt
    */

    public function getContributionsByReceiptForDataTable($id) {
        return Datatables::of($this->legacy_receipts->getAllContributionsByReceipt($id))
            ->editColumn('amount', function($legacy_receipt_code) {
                return $legacy_receipt_code->amount_formatted;
            })
            ->editColumn('contrib_month', function($legacy_receipt_code) {
                return $legacy_receipt_code->contrib_month_formatted;
            })
            ->addColumn('workflow_status', function ($legacy_receipt_code) {
                return $legacy_receipt_code->workflow_status_label;
            })
            ->addColumn('status', function ($legacy_receipt_code) {
                return $legacy_receipt_code->linked_file_status_label;
            })
            ->addColumn('action', function ($legacy_receipt_code) {
                return $legacy_receipt_code->action_buttons;
            })
            ->rawColumns(['status', 'action', 'workflow_status'])
            ->make(true);
    }

    /*
     * get active interests for this receipt
     */

    public function getInterestByReceiptForDatatable($id) {
        return Datatables::of($this->legacy_receipts->getAllInterestByReceipt($id))
            ->editColumn('amount', function($legacy_receipt_code) {
                return $legacy_receipt_code->amount_formatted;
            })
            ->addColumn('late_months', function ($legacy_receipt_code) {
                return (!is_null($legacy_receipt_code->bookingInterest)) ? $legacy_receipt_code->bookingInterest->late_months : 0;
            })
            ->addColumn('interest', function ($legacy_receipt_code) {
                return (!is_null($legacy_receipt_code->bookingInterest)) ? $legacy_receipt_code->bookingInterest->amount : 0;
            })
            ->make(true);
    }




    /* Cancel Receipt */
    public function cancel($id, CancelReceiptRequest $request) {
        $this->legacy_receipts->cancel($id, $request->all());
        return redirect()->route('backend.finance.menu')->withFlashSuccess(trans('alerts.backend.finance.receipt.cancelled'));
    }

    /* cancel_reason */
    public function cancelReason($id)
    {
        $legacy_receipt= $this->legacy_receipts->findOrThrowException($id);
        return view('backend.finance.legacy_receipt.cancel.reason')
            ->withLegacyReceipt($legacy_receipt);
    }

//DISHONOR receipt
    public function dishonour($id, DishonourReceiptRequest $request) {
        $this->legacy_receipts->dishonour($id, $request->all());
        return redirect()->route('backend.finance.menu')->withFlashSuccess(trans('alerts.backend.finance.receipt.dishonoured'));
    }

    /* dishonour_reason */
    public function dishonourReason($id)
    {

        $legacy_receipt= $this->legacy_receipts->findOrThrowException($id);
        return view('backend.finance.legacy_receipt.dishonour.reason')
            ->withLegacyReceipt($legacy_receipt);

    }

//    new cheque
    public function newCheque(LegacyReceipt $legacy_receipt) {

        return view('backend.finance.legacy_receipt.dishonour.replace_cheque')
            ->withLegacyReceipt($legacy_receipt)
            ->withBanks($this->banks->getAll()->pluck('name', 'id'));

    }

    public function replaceCheque($id, ReplaceChequeRequest $request) {
        $this->legacy_receipts->replaceCheque($id, $request->all());

        return redirect()->route('backend.finance.menu')->withFlashSuccess(trans('alerts.backend.finance.receipt.replace_cheque'));
    }


    public function printReceipt($id)
    {
        $legacy_receipt = $this->legacy_receipts->findWithTrashed($id);
        return view("backend/finance/legacy_receipt/includes/print_receipt")
            ->withLegacyReceipt($legacy_receipt);
    }

    public function uploadManualContribution()
    {
        return view("backend/finance/legacy_receipt/upload");
    }

    public function uploadManualContributionList(UploadManualContribution $request)
    {
        if ($this->saveManualContributionFileAttachment()) {
            dispatch(new LoadManualContribution());
            /* $this->receipts->updateIfCompleteLevel2($receipt_code->receipt_id); */
            return response()->json(['success' => true, 'message' => "File uploaded, records will be saved in the database. Retry if no entries are updated."]);
        }
        return response()->json(['success' => false, 'message' => "Could not upload file"]);
    }


}
