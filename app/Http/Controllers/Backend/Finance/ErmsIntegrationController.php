<?php

namespace App\Http\Controllers\Backend\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use PDF;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException as ConnectException;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use Illuminate\Support\Facades\Response;
use DB;
use Illuminate\Support\Facades\Log;
use function \FluidXml\fluidxml;
use App\Models\Finance\Bill;
use App\Models\Operation\Compliance\Member\Employer;
use App\DataTables\WorkPlaceRiskAssesment\DataTableBase;

class ErmsIntegrationController extends Controller
{

    public function returnErmsMenu(){
        $claims_payable_count = DB::table('main.claims_payable')->where('sent_to_erms', false)->where('status_code','!=',201)->count();
        return view('backend.finance.erms_menu')->with('claims_payable_count', $claims_payable_count);
    }

    public function claimsPayableView(){
        return view('backend.finance.erms.claims_payable');
    }

    public function postAllClaimsPayable(){
        return 1234;
    }

   

    public function claimsPayableDatatable(){

        $columns = [
           'member_type',
           'notification_id',
           'trx_id',
           'payee_name',
           'benefit_type',
           'accountno',
           'bank_name',
           'amount',
           'erms_status_code',
           'action',
        ];


        $claims_payable = null;
        if (is_null(request()->input('status'))) {
            $claims_payable = DB::table('main.claims_payable')
            ->where('sent_to_erms', false)->where('status_code','!=',201);
        }else{
            $status_code = request()->input('status') == -1 ? null : request()->input('status');
            $claims_payable = DB::table('main.claims_payable')
            ->where('erms_status_code', $status_code);
        }

        $dataTable = DataTables::of($claims_payable)
        ->editColumn("amount", function($claims_payable){
            return number_format($claims_payable->amount,2) ;
        })
        ->editColumn("erms_status", function($claims_payable){
            return $claims_payable->erms_status_code;
        })
        ->editColumn("erms_status_code", function($claims_payable){
         $status = null;
         switch ($claims_payable->erms_status_code) {
           case null:
             $status = '<span class="tag tag-warning text-secondary"> Not sent</span>';
           break;
           case 200;
             $status = '<span class="tag tag-primary text-secondary"> Pending</span>';
           break;
           case 201;
             $status = '<span class="tag tag-success text-secondary"> Paid</span>';
           break;
           case 202;
             $status = '<span class="tag tag-info text-secondary"> Accepted</span>';
           break;
           case 400;
             $status = '<span class="tag tag-warning text-danger" data-toggle="tooltip" data-html="true" title="' . $claims_payable->error_message . '"> Failed</span>';
           break;

           default:
           break;
       }

         return $status;
    })
    ->addColumn("action", function($claims_payable){
        $button = "";
        if($claims_payable->erms_status_code == null || $claims_payable->erms_status_code == 400 ){
            $button =  '<button class="btn btn-sm btn-primary repost" erms_status_code="'.$claims_payable->erms_status_code.'" Id="'.$claims_payable->id.'"><span id="first" class="fa fa-spinner rotation-animation"></span> post</button>' ;
        }

        return $button;
    })->rawColumns(['action' , 'erms_status_code']);

    $base = new DataTableBase($claims_payable, $dataTable, $columns);
    return $base->render(null);

    }

    public function postClaimsPayable(Request $request){
        $claims_payable_id = (int)$request->Id;

        try {
            $client = new Client();
            $response =  $client->request('GET', config('constants.MAC_ERMS.erms_api').'/send_claim_payable/'. $claims_payable_id);
            $status = $this->statusCodeInWords($claims_payable_id);
            if($response->getStatusCode() === 200){
                return response()->json(array('success' => true, 'message' => 'Your request has been processed, status: '.$status.'...'));
            }else{
                logger($response->getStatusCode());
                return response()->json(array('error' => true, 'message' => 'Can\'t perfom this action, There is an error occurred!'));
            }
        }
        catch (\Exception $e) {
            logger($e);
            return response()->json(array('error' => true, 'message' => 'Exception error!'));
        }
    }

    public function statusCodeInWords($claims_payable_id){
        $claims_payable = DB::table('main.claims_payable')
        ->select(DB::raw("erms_status_code ,
              CASE 
              WHEN erms_status_code = '200'
              THEN 'Pending'
              WHEN erms_status_code = '201'
              THEN 'Paid'
              WHEN erms_status_code = '202'
              THEN 'Accepted'
              WHEN erms_status_code = '400'
              THEN 'Failed'
              ELSE 'not sent'
              END as erms_status_code"))
        ->where('id', $claims_payable_id)->first();
        if (!is_null($claims_payable)) {
            return $claims_payable->erms_status_code;
        }

    }

}
