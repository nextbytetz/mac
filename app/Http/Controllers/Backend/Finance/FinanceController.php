<?php

namespace App\Http\Controllers\Backend\Finance;

use App\DataTables\Finance\GetPendingFuneralGrantsDataTable;
use App\DataTables\Finance\GetPendingPaymentVoucherTransBatchDataTable;
use App\DataTables\Finance\GetPendingPaymentVoucherTransIndividualDataTable;
use App\DataTables\Finance\GetPendingProcessCompensationsDataTable;
use App\DataTables\Finance\GetPendingSurvivorGratuityDataTable;
use App\Http\Controllers\Controller;
use App\Jobs\Claim\ClaimPaymentExportToErp;
use App\Jobs\Erp\BulkUpdateInvoicePaymentFromErp;
use App\Jobs\ProcessPaymentVoucherTransactions\ProcessPaymentVoucherTransactions;
use App\Jobs\ProcessPayment\ProcessPayment;
use App\Models\Finance\Receivable\BookingInterestRefund;
use App\Repositories\Backend\Finance\PaymentVoucherRepository;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Services\Finance\ProcessPaymentPerBenefit;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

use App\Repositories\Backend\MacErp\ClaimsErpApiRepositroy;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use Log;
use DB;

class FinanceController extends Controller
{

    protected $claim_compensations;
    protected $payment_vouchers;
    protected $member_types;
    protected $payment_voucher_transactions;
    protected $dependents;
    protected $payroll_procs;
    protected $booking_interests;

    public function __construct() {
        $this->claim_compensations = new ClaimCompensationRepository();
        $this->payment_vouchers = new PaymentVoucherRepository();
        $this->member_types = new MemberTypeRepository();
        $this->payment_voucher_transactions = new PaymentVoucherTransactionRepository();
        $this->dependents = new DependentRepository();
        $this->payroll_procs = new PayrollProcRepository();
        $this->booking_interests = new BookingInterestRepository();

        $this->middleware('access.routeNeedsPermission:process_payment', ['only' => ['processPayment', 'processVoucherTransactions', 'payVoucher']]);
        $this->middleware('access.routeNeedsPermission:export_pv_to_erp', ['only' => ['exportPvTranToErp']]);
//        $this->middleware('access.routeNeedsPermission:process_voucher_transactions', ['only' => ['processVoucherTransactions']]);
    }


    //Payments Profile
    public function payment_profile(GetPendingProcessCompensationsDataTable $pendingProcessCompensationsDataTable, GetPendingPaymentVoucherTransIndividualDataTable $pendingPaymentVoucherTransIndividualDataTable, GetPendingPaymentVoucherTransBatchDataTable $pendingPaymentVoucherTransBatchDataTable, GetPendingSurvivorGratuityDataTable $pendingSurvivorGratuityDataTable, GetPendingFuneralGrantsDataTable $pendingFuneralGrantsDataTable)
    {

//        WorkflowTrackDataTable $workflowTrack
        return view('backend.finance.payment.profile')
            ->withPendingProcessCompensationsDatatable($pendingProcessCompensationsDataTable)
            ->withPendingPaymentVoucherTransIndividualDatatable($pendingPaymentVoucherTransIndividualDataTable)
            ->withPendingPaymentVoucherTransBatchDatatable($pendingPaymentVoucherTransBatchDataTable)
            ->withPendingSurvivorGratuityDatatable($pendingSurvivorGratuityDataTable)
            ->withPendingFuneralGrantsDatatable($pendingFuneralGrantsDataTable);
//            ->withWorkflowtrack($workflowTrack);

    }


    /**
     * @param $id
     * @return $this
     * @throws \App\Exceptions\GeneralException
     * Print payment voucher
     */
    public function printPaymentVoucher($id)
    {
        $payment_voucher = $this->payment_vouchers->findOrThrowException($id);
        $payment_description = $this->payment_vouchers->paymentDescription($id);
        $finance_wf_track = $this->payment_vouchers->financeWfApproval($id);
        $member_type_id = $payment_voucher->paymentVoucherTransactions->first()->member_type_id;

        $member = $this->member_types->getMember($member_type_id, $payment_voucher->paymentVoucherTransactions->first()->resource_id);
        $notification_report = $this->payment_vouchers->findNotificationReport($id);
        return view("backend/finance/payment/includes/print_payment_voucher")
            ->with(['payment_voucher' => $payment_voucher, 'member' => $member, 'payment_description' => $payment_description, 'finance_wf_track' => $finance_wf_track, 'notification_report' => $notification_report]);
    }


    /**
     * @param $id
     * @return $this
     * @throws \App\Exceptions\GeneralException
     * Print payment voucher per benefit <NEW>
     */
    public function printPaymentVoucherPerBenefit($id)
    {
        $payment_voucher = $this->payment_vouchers->findOrThrowException($id);
        $payment_description = $this->payment_vouchers->paymentDescription($id);
        $finance_officers = $this->payment_vouchers->getFinanceWfTrackForPvPerBenefit($id);
        $member_type_id = $payment_voucher->paymentVoucherTransactions->first()->member_type_id;
        $member = $this->member_types->getMember($member_type_id, $payment_voucher->paymentVoucherTransactions->first()->resource_id);
        $notification_report = $this->payment_vouchers->findNotificationReport($id);
        return view("backend/finance/payment/includes/print_payment_voucher_per_benefit")
            ->with(['payment_voucher' => $payment_voucher, 'member' => $member, 'payment_description' => $payment_description, 'finance_wf_track' => $finance_officers, 'notification_report' => $notification_report]);
    }

    /**
     *
     * Get Compensation pending to be processed
     */

    public function getPendingProcessCompensations(){
        return Datatables::of($this->claim_compensations->getPendings())

            ->editColumn('claim_id', function($claim_compensation) {
                return $claim_compensation->claim_id;
            })
            ->addColumn('payee', function($claim_compensation) {
                return $this->member_types->getMember($claim_compensation->member_type_id, $claim_compensation->resource_id)->name;
            })
            ->addColumn('amount', function($claim_compensation)  {
                return $claim_compensation->amount_formatted;
            })
            ->make(true);

    }


    /**
     *
     * Get Pending Funeral Grants
     */

    public function getPendingFuneralGrants(){
        return Datatables::of($this->dependents->pendingFuneralGrants())
            ->addColumn('claim_id', function($dependent) {
                return $this->dependents->findNotificationReport($dependent->dependent_id, $dependent->employee_id)->claim->id;
            })
            ->addColumn('payee', function($dependent) {
                return $dependent->fullname;
            })
            ->addColumn('amount', function($dependent)  {
                return number_2_format($dependent->funeral_grant);
            })
            ->make(true);

    }



    /*
    *
    * Get  Pending Survivor benefits
    */


    public function getPendingSurvivorGratuity(){
        return Datatables::of($this->dependents->pendingSurvivorGratuity())
            ->addColumn('claim_id', function($dependent) {
                return $this->dependents->findNotificationReport($dependent->dependent_id, $dependent->employee_id)
                    ->claim->id;
            })
            ->addColumn('payee', function($dependent) {
                return $dependent->fullname;
            })
            ->addColumn('amount', function($dependent)  {
                return number_2_format($dependent->survivor_gratuity_amount);
            })
            ->make(true);

    }


    /**
     * @return mixed
     * Get Pending Interest Refunds
     */

    public function getPendingInterestRefunds()
    {
        return Datatables::of($this->booking_interests->getPendingInterestRefundForPayment())
            ->addColumn('refund_id', function($interest_refund) {
                return $interest_refund->id;
            })
            ->addColumn('payee', function($interest_refund) {
                return $interest_refund->getEmployer()->name;
            })
            ->addColumn('amount', function($interest_refund)  {
                return number_2_format($interest_refund->amount);
            })
            ->make(true);
    }




    /*
*
* Get  pending payment voucher transaction to be processed (INDIVIDUAL i.e Employee and employer)
*/

    public function getPendingPaymentVoucherTransIndividual(){

        return Datatables::of($this->payment_voucher_transactions->pendingIndividual())
            ->editColumn('amount', function($payment_voucher_tran) {
                return $payment_voucher_tran->amount_formatted;
            })
            ->addColumn('benefit_resource', function($payment_voucher_tran) {
                return $payment_voucher_tran->benefit_resource;
            })
            ->addColumn('payee', function($payment_voucher_tran) {
                return $this->member_types->getMember($payment_voucher_tran->member_type_id, $payment_voucher_tran->resource_id)->name;
            })
            ->editColumn('benefit_type_id', function($payment_voucher_tran) {
                return ($payment_voucher_tran->benefitType) ? $payment_voucher_tran->benefitType->name : ' ';
            })
            ->make(true);
    }



    /*
*
* Get  pending payment voucher transaction to be processed (BATCH i.e Insurance)
*/

    public function getPendingPaymentVoucherTransBatch(){

        return Datatables::of($this->payment_voucher_transactions->pendingBatch())
            ->editColumn('amount', function($payment_voucher_tran) {
                return $payment_voucher_tran->amount_formatted;
            })
            ->addColumn('benefit_resource', function($payment_voucher_tran) {
                return $payment_voucher_tran->benefit_resource;
            })
            ->addColumn('payee', function($payment_voucher_tran) {
                return $this->member_types->getMember($payment_voucher_tran->member_type_id, $payment_voucher_tran->resource_id)->name;
            })
            ->editColumn('benefit_type_id', function($payment_voucher_tran) {
                return ($payment_voucher_tran->benefitType) ? $payment_voucher_tran->benefitType->name : ' ';
            })
            ->make(true);
    }


    /*
*
* Get  pending payment vouchers (Nt yet Dispatched or Paid)
*/

    public function getPendingPaymentVouchers(){

        return Datatables::of($this->payment_vouchers->getPendingPaid())
            ->editColumn('amount', function($payment_voucher) {
                return $payment_voucher->amount_formatted;
            })
            ->addColumn('benefit_resource', function($payment_voucher) {
                return ($payment_voucher->paymentVoucherTransactions()->count() > 0) ? (($payment_voucher->paymentVoucherTransactions->first()->member_type_id <> 3) ? $payment_voucher->paymentVoucherTransactions->first()->benefit_resource : ' ') : ' ';
            })
            ->addColumn('payee', function($payment_voucher) {
                return ($payment_voucher->paymentVoucherTransactions()->count() > 0) ?   ($this->member_types->getMember($payment_voucher->paymentVoucherTransactions->first()->member_type_id, $payment_voucher->paymentVoucherTransactions->first()->resource_id)->name) : ' ';
            })

            ->addColumn('action', function($payment_voucher) {
                return $payment_voucher->pay_button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }



    /**
     * @return mixed
     * Process payment to paid from claim compensations
     */

    public function processPayment(){
        $user_id = access()->id();
        dispatch(new ProcessPayment($user_id));
        return redirect()->route('backend.finance.payment.profile')->withFlashSuccess(trans('alerts.backend.finance.payment_processed'));


    }

    /**
     *
     * @return mixed
     * Process payment voucher Transactions
     */
    public function processVoucherTransactions($process_type){
        $user_id = access()->id();
        if ($process_type == 2){
            $this->payroll_procs->checkIfMonthlyPayrollIsRunThisMonth(3, Carbon::now());
        }

        dispatch(new ProcessPaymentVoucherTransactions($process_type,$user_id));
        return redirect()->route('backend.finance.payment.profile')->withFlashSuccess(trans('alerts.backend.finance.payment_voucher_trans_processed'));
    }

    /**
     *
     * @return mixed
     * Pay payment vouchers (Make payment)
     */
    public function payVoucher($payment_voucher_id){
        $payment_voucher = $this->payment_vouchers->pay($payment_voucher_id);

        return redirect('finance/payment/profile#make_payment')->withFlashSuccess(trans('alerts.backend.finance.payment_voucher_paid'));
    }

    public function postApErpApi(){
        $pv =  new ClaimsErpApiRepositroy();

        $transaction_ids = $pv->saveApiTransaction();
        // dd($transaction_ids);
        foreach ($transaction_ids as $transaction_id) {
            if(!is_null($transaction_id)){
                try {
                    $client = new Client();
                    $response =  $client->request('GET', config('constants.MAC_ERP.AP_TO_Q').$transaction_id);
                    $status = $response->getStatusCode();
                }
                catch (ClientError $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status:'.$status.' ClientError');

                }
                catch (ServerError $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status: '.$status.' ServerError');


                }
                catch (BadResponse $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status: '.$status,' BadResponse');

                }
                catch(\Exception $e){
                    $status = $e->getCode();
                    Log::info($transaction_id.' Status: '.$status.' Exception');

                }

                $pv->updateApPostedToQ($transaction_id, $status);
            }


        }
    }


    /**
     * @return mixed
     * @throws \Exception
     * Get pending claim assessed for export to ERP
     */
    public function getPendingExportPvTranForDataTable()
    {
        return Datatables::of($this->payment_voucher_transactions->getPendingExportPvTranForDataTable())
            ->editColumn('amount', function($pv_tran) {
                return $pv_tran->amount_formatted;
            })
            ->addColumn('employee_name', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['payee'];
            })
            ->addColumn('filename', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['filename'];
            })
//
            ->addColumn('memberno', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['memberno'];
            })
            ->addColumn('member_type', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['member_type'];
            })
            ->addColumn('benefit_type', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['benefit_type'];
            })
            ->addColumn('debit_account_expense', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['debit_account_expense'];
            })
            ->addColumn('credit_account_receivable', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['credit_account_receivable'];
            })
            ->addColumn('accountno', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['accountno'];
            })
            ->addColumn('bank_name', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['bank_name'];
            })
            ->addColumn('swift_code', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['swift_code'];
            })
            ->addColumn('bank_branch_id', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['bank_branch_id'];
            })
            ->addColumn('bank_address', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['bank_address'];
            })
            ->addColumn('country_name_bank', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['country_name_bank'];
            })
            ->addColumn('city_of_bank', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['city_of_bank'];
            })
            ->addColumn('state_or_region', function($pv_tran) {
                return  $this->payment_voucher_transactions->getApiFieldsForPendingExports($pv_tran)['state_or_region'];
            })

            ->make(true);
    }


    /*Export PV to ERP*/
    public function exportPvTranToErp()
    {
         dispatch(new ClaimPaymentExportToErp());
        return redirect()->route('backend.finance.payment.profile')->withFlashSuccess('Success, Export has been initiated!');
    }

    public function getErpPostedGl()
    {
        $request = Request();
        $client = new Client();
        $request = $client->get('http://10.10.10.20:8090/ords/apps/gl/glAck/19-02-2019/20-02-2019');
        $response = $request->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        // // Log::info($parsed_json['p_record_set'][0]['transaction_id']);
        // Log::info($parsed_json['p_record_set'][1]['transaction_id']);

        dump($parsed_json);

        die;

        foreach ($parsed_json as $key => $value) {
            echo $value[1]["transaction_id"] . ", " . $value[1]["status"] . "<br>";
        }

        // die;

        DB::table('receipts_gl')->where('trx_id',$parsed_json['p_record_set'][1]['transaction_id'])->update([
            'status_code' => $parsed_json['p_record_set'][1]['status'],
            'isposted'=>true,
            'updated_at' => Carbon::now()
        ]);
        dump('updated successfully');

    }

    /*Bulk update paid invoice from erp*/
    public function bulkUpdatePaidErpInvoices(){
        $min_date = Carbon::now()->startOfMonth();
        $max_date = Carbon::now()->endOfMonth();
        dispatch(new BulkUpdateInvoicePaymentFromErp($min_date, $max_date));
        return redirect()->route('backend.finance.menu')->withFlashSuccess('Success, Bulk update process hs been initiated!');
    }

}
