<?php

namespace App\Http\Controllers\Backend\Finance;

use App\DataTables\WorkflowTrackDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Finance\Receipt\StoreAdministrativeReceiptRequest;
use App\Http\Requests\Backend\Finance\Receipt\UpdateReceiptMonthsRequest;
use App\Jobs\Employee\LoadElectronicContribution;
use App\Jobs\Erp\PostGlToErp;
use App\Models\Finance\DishonouredCheque;
use App\Models\Finance\Receipt\Receipt;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\MacErp\ReceiptsErpApiRepositroy;
use App\Repositories\Backend\MacErp\RefundErpApiRepository;
use App\Repositories\Backend\Finance\DishonouredChequeRepository;
use App\Repositories\Backend\Finance\ThirdpartyRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Yajra\Datatables\Datatables;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Http\Requests\Backend\Finance\Receipt\CancelReceiptRequest;
use App\Http\Requests\Backend\Finance\Receipt\DishonourReceiptRequest;
use App\Http\Requests\Backend\Finance\Receipt\ReplaceChequeRequest;
use App\Http\Requests\Backend\Finance\Receipt\ChooseEmployerRequest;
use App\Http\Requests\Backend\Finance\Receipt\SupplierFormRequest;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Http\Requests\Backend\Finance\Receipt\StoreEmployerReceiptRequest;
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use App\Repositories\Backend\Finance\Currency\CurrencyRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Services\Receivable\CreateBooking;
use App\Services\Receivable\CalculateInterest;
use App\Repositories\Backend\Finance\FinCodeRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use PDF;
use Exception;

use App\DataTables\Report\Finance\ControlNoDatatable;
use App\DataTables\Report\Finance\GepGReconciliationDataTable;
use App\DataTables\Report\Finance\successfullyGltoERPDataTable;                                   
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use App\Models\Finance\Bill;
use App\Models\Auth\User;
use App\Models\MacErp\ErpSupplier;
use App\Models\Operation\Compliance\Member\Employer;
use App\Http\Requests\Backend\Report\DateRangeRequest;
use App\Models\Sysdef\DateRange;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use Illuminate\Support\Facades\Response;
use App\DataTables\Finance\Receipt\contributionRefundDataTable;
use App\Repositories\Backend\MacErp\ClaimsErpApiRepositroy;

//use Log;


use DB;
use Illuminate\Support\Facades\Log;
use function \FluidXml\fluidxml;

class ReceiptController extends Controller
{

    protected $receipts;
    protected $banks;
    protected $date_ranges;
    protected $employers;
    protected $query;
    public function __construct(ReceiptRepository $receipts, BankRepository $banks, EmployerRepository $employers) {
        $this->receipts = $receipts;
        $this->employers = $employers;
        $this->banks = $banks;
        $this->date_ranges= new DateRangeRepository();

        $this->middleware('access.routeNeedsPermission:create_administrative_receipt', ['only' => ['createAdministrative']]);
        $this->middleware('access.routeNeedsPermission:print_receipt', ['only' => ['printReceipt']]);
        $this->middleware('access.routeNeedsPermission:cancel_receipt', ['only' => ['cancelReason']]);
        $this->middleware('access.routeNeedsPermission:dishonored_cheque', ['only' => ['dishonourReason']]);
        //editMonthsPermission
        $this->middleware('access.routeNeedsPermission:edit_contribution_months', ['only' => ['editMonthsPermission']]);
        $this->middleware('access.routeNeedsPermission:all_functions', ['only' => ['resendBill']]);


    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend.finance.receipt.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function createAdministrative()
    {


        $payment_types = new PaymentTypeRepository();
        $currency = new CurrencyRepository();
        $bank = new BankRepository();
        $fin_codes = new FinCodeRepository();
        $users = new UserRepository();

        $thirdparties = new ThirdpartyRepository();
        return view("backend/finance/receipt/create_administrative")
        ->withFinCodes($fin_codes->getAllAdministrative()->pluck('name', 'id'))
        ->withPaymentTypes($payment_types->getAll())
        ->withCurrencies($currency->getAll()->pluck('name', 'id'))
        ->withBanks($bank->getAll()->pluck('name', 'id'))
        ->withThisDate(Carbon::now()->format('Y-n-j'))
        ->withStaffs($users->getAll()->pluck('name', 'id'))
        ->with("thirdparty_payers", $thirdparties->getAll()->pluck("name", "id"));
    }

    public function storeAdministrative(StoreAdministrativeReceiptRequest $request)
    {
        $receipt = $this->receipts->storeAdministrativeReceipt($request->all());

        try {
            if (env("CAN_POST_ERP", 0)) {
                $post_to_erp = $this->postReceiptErpApi($receipt->id, false);
            }
        } catch (\Exception $e) {
            Log::info('------=============');
            Log::info('Error occured');
            Log::info($e->getMessage());
            Log::info('------============');

        }


        return response()->json(['success' => true, 'id' => $receipt->id]);
    }

    public function chooseEmployer()
    {
        return view("backend/finance/receipt/employer");
    }

    /**
     * @param ChooseEmployerRequest $request
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function postChooseEmployer(ChooseEmployerRequest $request)
    {

        access()->hasWorkflowDefinition(5,1);
        $employer_id = $request->employer;

        $employer = new EmployerRepository();
        $payment_types = new PaymentTypeRepository();
        $currency = new CurrencyRepository();
        $bank = new BankRepository();
        $employer->checkIfHasTin($employer_id);

        //check Employers registered online with their associated users  
        $isonline=$employer->checkIfIsOnline($employer_id);
        $onlineusers=$employer->returnOnlineEmployerUser($employer_id);
        $ContributionDateThreshold = Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1);
        return view("backend/finance/receipt/create_employer")
        ->with("employer_id", $employer_id)
        ->with("employer", $employer->find($employer_id))
        ->with("payment_types", $payment_types->getAll())
        ->with("currencies", $currency->getAll()->pluck('name', 'id'))
        ->with("banks", $bank->query()->whereIn("id", [3, 4])->get()->pluck('name', 'id'))
        ->with("max_entries", sysdefs()->data()->total_allowed_contrib_months)
        ->with("month_threshold", Carbon::parse($ContributionDateThreshold)->format("m"))
        ->with("year_threshold", Carbon::parse($ContributionDateThreshold)->format("Y"))
        ->with("minimum_contribution", sysdefs()->data()->minimum_allowed_contribution)
        ->with("this_date", Carbon::now()->format('Y-n-j'))
        ->with('isonline',$isonline)
        ->with('onlineusers',$onlineusers);
    }

    public function contributionEntries()
    {
        return view('backend/finance/receipt/includes/monthly_contribution_entry')
        ->with("selected_entries", request()->input('selected_entries'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function bookedAmount()
    {
        $month = request()->input('month');
        $year = request()->input('year');
        $employer_id = request()->input('employer_id');
        $booking = new CreateBooking();
        $amount = $booking->bookedAmount($month, $year, $employer_id);
        $amount = number_format($amount, 2, '.', ',');
        return response()->json(['amount' => $amount]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function interestDueAmount()
    {
        $month = request()->input('month');
        $year = request()->input('year');
        $employer_id = request()->input('employer_id');
        $interest = new CalculateInterest();
        $due_amount = $interest->dueAmount($month, $year, $employer_id);
        $amount = number_format($due_amount, 2, '.', ',');
        return response()->json(['amount' => $amount]);
    }

    /**
     * @param StoreEmployerReceiptRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function store(StoreEmployerReceiptRequest $request)
    {

        $receipt = $this->receipts->storeEmployerReceipt($request->all());

        try {
            if (env("CAN_POST_ERP", 0)) {
                $this->postReceiptErpApi($receipt->id);
            }
        } catch (\Exception $e) {
            Log::info('------=============');
            Log::info('Error occured');
            Log::info($e->getMessage());
            Log::info('------============');

        }
        return response()->json(['success' => true, 'id' => $receipt->id]);
    }

    public function storeElectronic(Request $request)
    {
        $user = (new UserRepository())->find(211);
        access()->login($user);
        $input = $request->all();
        $return = $this->receipts->storeElectronic($input);
        //logger($return);
        if ($return['success']) {
            if (isset($return['id'])) {
                $this->postReceiptErpApi($return['id']);
            }
            //Record employees contributions, Call Job
            dispatch(new LoadElectronicContribution($return['id']));
        }
        access()->logout();
        return response()->json($return);
    }

    public function storeElectronicAdministrative(Request $request)
    {
        $user = (new UserRepository())->find(211);
        access()->login($user);
        $input = $request->all();
        $receipt = $this->receipts->storeElectronicAdministrative($input);
        if(isset($receipt->id)){
            $this->postReceiptErpApi($receipt->id);
        }
        access()->logout();
        return response()->json(['success' => true, 'receipt_id' => $receipt->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $receipt = $this->receipts->findWithTrashed($id);
        return view('backend.finance.receipt.action')
        ->with("receipt", $receipt);
    }

    public function printReceipt($id)
    {
        $receipt = $this->receipts->findWithTrashed($id);
        return view("backend/finance/receipt/includes/print_rec



            eipt")
        ->with("receipt", $receipt);
    }

    /**
     * Used considering workflows
     *
     * @param Receipt $receipt
     * @return mixed
     */
    public function editMonths(Receipt $receipt)
    {
        //workflow([['wf_module_group_id' => $receipt->wf_module_id, 'resource_id' => $receipt->id]])->checkLevel(2);
        //access()->hasWorkflowDefinition(5,2);
     return redirect()->back()->withFlashDanger('Access denied!');
     $wfModule = $receipt->wfModule;
     access()->hasWorkflowModuleDefinition($wfModule->wf_module_group_id, $wfModule->type, 2);
     return $this->returnEditMonthsView($receipt);
 }

    /**
     * Used with out considering workflows
     *
     * @param Receipt $receipt
     * @return mixed
     */
    public function editMonthsPermission(Receipt $receipt)
    {
        return $this->returnEditMonthsView($receipt);
    }

    /**
     * @param Receipt $receipt
     * @return mixed
     */
    public function returnEditMonthsView(Receipt $receipt)
    {
        $ContributionDateThreshold = Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1);
        return view('backend/finance/receipt/edit')
        ->with("receipt", $receipt)
        ->with("this_date", Carbon::now()->format('Y-n-j'))
        ->with("contribution_date_threshold", $ContributionDateThreshold)
        ->with("month_threshold", Carbon::parse($ContributionDateThreshold)->format("m"))
        ->with("year_threshold", Carbon::parse($ContributionDateThreshold)->format("Y"))
        ->with("minimum_contribution", sysdefs()->data()->minimum_allowed_contribution)
        ->with("year_threshold", Carbon::parse($ContributionDateThreshold)->format("Y"));
    }

    public function updateMonths(Receipt $receipt, UpdateReceiptMonthsRequest $request)
    {
        $this->receipts->updateMonths($receipt, $request->all());
        return response()->json(['success' => true, 'id' => $receipt->id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



// Receipt Profile -> find by receipt no
    public function profileByRequest($receipt, WorkflowTrackDataTable $workflowTrack)
    {
        $receipt_model = $this->receipts->query()->withTrashed()->where("id", $receipt)->first();
        if (!$receipt_model) {
            throw new ModelNotFoundException();
        }
        //return $dataTable->with('resource_id', $receipt->id)->render('backend.finance.receipt.profile', compact('receipt'));
        //->withReceipt($receipt);
        return view('backend/finance/receipt/profile')
        ->with("receipt", $receipt_model)
        ->with("workflowtrack", $workflowTrack);

    }

//    GET - VIEW ALL RECEIPT View
    public function get() {
        return Datatables::of($this->receipts->getForDataTable())
        ->editColumn('rct_date', function($receipt) {
            return $receipt->rct_date_formatted;
        })
        ->editColumn('amount', function($receipt) {
            return $receipt->amount_comma;
        })
        ->editColumn('iscancelled', function($receipt) {
            return $receipt->status_label;
        })
        ->editColumn('payment_type_id', function($receipt) {
            return $this->receipts->getPaymentTypeWithChequeno($receipt->id);
        })
        ->rawColumns(['iscancelled'])
        ->make(true);
    }

    public function reconcile(Receipt $receipt)
    {
        $this->receipts->reconcile($receipt);
        return redirect()->back()->withFlashSuccess("Success, Receipt has been Reconciled");
    }

//cancel receipt => NEED AUDIT LOGS
    public function cancel($id, CancelReceiptRequest $request) {
        $this->receipts->cancel($id, $request->all());
        return redirect()->route('backend.finance.menu')->withFlashSuccess(trans('alerts.backend.finance.receipt.cancelled'));
    }

    /* cancel_reason */
    public function cancelReason($id)
    {
        $receipt= $this->receipts->findOrThrowException($id);
        return view('backend.finance.receipt.cancel.reason')
        ->with("receipt", $receipt);
    }

    //DISHONOR receipt => NEED AUDIT LOGS
    public function dishonour($id, DishonourReceiptRequest $request) {
        $this->receipts->dishonour($id, $request->all());
        return redirect()->route('backend.finance.menu')->withFlashSuccess(trans('alerts.backend.finance.receipt.dishonoured'));
    }

    /* dishonour_reason */
    public function dishonourReason($id)
    {

        $receipt= $this->receipts->findOrThrowException($id);
        return view('backend.finance.receipt.dishonour.reason')
        ->with("receipt", $receipt);

    }

//    new cheque
    public function newCheque(Receipt $receipt) {

        return view('backend.finance.receipt.dishonour.replace_cheque')
        ->withReceipt($receipt)
        ->withBanks($this->banks->getAll()->pluck('name', 'id'));

    }

    public function replaceCheque($id, ReplaceChequeRequest $request) {
        $this->receipts->replaceCheque($id, $request->all());

        return redirect()->route('backend.finance.menu')->withFlashSuccess(trans('alerts.backend.finance.receipt.replace_cheque'));
    }

    /*
     * get active Contributions for this receipt
     */

    public function getContributionsByReceiptForDataTable($id) {

        return Datatables::of($this->receipts->getAllContributionsByReceipt($id))
        ->editColumn('amount', function($receipt_code) {
            return $receipt_code->amount_comma;
        })
        ->addColumn('contrib_month_formatted', function($receipt_code) {
            return $receipt_code->contrib_month_formatted;
        })
        ->addColumn('workflow_status', function ($receipt_code) {
            return $receipt_code->workflow_status_label;
        })
        ->addColumn('status', function ($receipt_code) {
            return $receipt_code->linked_file_status_label;
        })
        ->addColumn('action', function ($receipt_code) {
            return $receipt_code->action_buttons;
        })
        ->addColumn('member_diff_label', function ($receipt_code) {
            return $receipt_code->member_diff_label;
        })
        ->addColumn('pay_status_label', function ($receipt_code) {
            return $receipt_code->pay_status_label;
        })
        ->rawColumns(['status', 'action', 'workflow_status', 'member_diff_label', 'pay_status_label'])
        ->make(true);
    }

    /*
     * get active interests for this receipt
     */

    public function getInterestByReceiptForDatatable($id) {
        return Datatables::of($this->receipts->getAllInterestByReceipt($id))
        ->editColumn('amount', function($receipt_code) {
            return $receipt_code->amount_comma;
        })
        ->addColumn('contrib_month_formatted', function($receipt_code) {
            return $receipt_code->contrib_month_formatted;
        })
        ->addColumn('late_months', function ($receipt_code) {
            return ($receipt_code->bookingInterest()->count()) ? $receipt_code->bookingInterest->late_months : 0;
        })
        ->addColumn('interest', function ($receipt_code) {
            return ($receipt_code->bookingInterest()->count()) ? $receipt_code->bookingInterest->amount_formatted : 0;
        })
        ->make(true);
    }


    /* GePG Methods starts here  */

    public function viewEmployer()
    {

        // Log::info(print_r($request,true));
        // $employeeList= DB::table('main.employee_employer')->where('employer_id','=',$bill_id)->first();
        // return view('backend.operation.compliance.member.employer.index');

        return view("backend/finance/receipt/employercontrolno");

    }
    //Generate control numbers for non-contributions
    public function nonContributions()
    {
        $payment_types = new PaymentTypeRepository();
        $currency = new CurrencyRepository();
        $bank = new BankRepository();
        $fin_codes = new FinCodeRepository();
        $users = new UserRepository();

        $thirdparties = new ThirdpartyRepository();
        return view("backend/finance/receipt/create_administrative_control_no")
        ->withFinCodes($fin_codes->getAllAdministrativeCN()->pluck('name', 'id','gepg_gfs'))
        ->withPaymentTypes($payment_types->getAll())
        ->withCurrencies($currency->getAll()->pluck('name', 'id'))
        ->withBanks($bank->getAll()->pluck('name', 'id'))
        ->withThisDate(Carbon::now()->format('Y-n-j'))
        ->withStaffs($users->getAll()->pluck('name', 'id'))
        ->with("thirdparty_payers", $thirdparties->getAll()->pluck("name", "id"));

    }

    public function generateAdministrativeControlNo(Request $request){
        // dd($request->all());

        $amount = str_replace( ',', '', $request->amount);

        $bill_rand_no = mt_rand(1000, 1999);

        $latest = DB::table('portal.bills')->orderBy('id', 'desc')->limit(1)->get();
        $latest_bill = $latest[0]->id+1;
        $bill_no = $bill_rand_no.$latest_bill;

        $fn_code = DB::table('main.fin_codes')->find($request->fin_code_id);

        if($request->category == 0){
            $staff=DB::table('main.users')->find($request->staff_id);
            $staff_id = $staff->external_id;

            $third_party_id = null;
        }else{
            $staff_id = null;
            $third_party_id = $request->thirdparty_id;
        }

        $bill = new Bill;
        $bill->employer_id = null;
        $bill->bill_no=  $bill_no;
        $bill->bill_amount = $amount;
        $bill->non_contribution = $amount;
        $bill->bill_description = $fn_code->name;
        $bill->billed_item = $request->description;
        $bill->bill_status = 1;
        $bill->expire_date = Carbon::now()->addDays(28);
        $bill->contribution_due_date = Carbon::now()->addDays(28);
        $bill->user_id = access()->user()->id;
        $bill->mobile_number = '255655726617';
        $bill->bill_source ='MAC';
        $bill->staff_id = $staff_id;
        $bill->third_party_id = $third_party_id;
        $bill->gfs_code = $fn_code->gepg_gfs_code;
        $bill->save();
        $response =  $this->requestControlNumber($bill->bill_no);
// return $response;

        return redirect()->route("backend.finance.view.administrative.order_form", $bill->id);
    }


    public function getEmployers(){
        return Datatables::of($this->employers->getForDataTable())
        ->addColumn('name_formatted', function ($employer) {
            return $employer->name_formatted;
        })
        ->addColumn('doc_formatted', function ($employer) {
            return $employer->date_of_commencement_formatted;
        })
        ->addColumn('country', function ($employer) {
            return ($employer->district_id) ? $employer->district->region->country->name : (($employer->region_id) ? $employer->region->country->name : ' ');
        })
        ->addColumn('region', function ($employer) {
            return ($employer->district_id) ? $employer->district->region->name : (($employer->region_id) ? $employer->region->name : ' ');
        })
        ->make(true);
    }


    public function returnControlNumberForm($org_id){
        $employer=Employer::find($org_id);
        $pending_bill = Bill::where('employer_id', '=', $employer->id)->where('bill_status','!=',3)->first();
        $contribution=$this->returnContribution($employer);
        $last_contrib = DB::table('main.receipt_codes')
        ->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
        ->join('main.employers', 'receipts.employer_id', '=', 'employers.id')
        ->where('receipts.employer_id','=',$employer->id)
        ->orderBy('receipt_codes.contrib_month', 'desc')
        ->limit(1)->get();

        return view("backend/finance/receipt/bill")
        ->with('pending_bill', $pending_bill)
        ->with('employer', $employer)
        ->with('last_contrib', $last_contrib)
        ->with('contribution',$contribution);
    }


    public function generateBill(Request $request){
        $employer=Employer::find($request->employer_id);

        if($request->contribution <= 0){
            return response()->json(array('error' => true, 'message' => 'The bill can\'t be generated with zero amount'));
        }
        $pending_bill = Bill::where('employer_id', '=', $request->employer_id)->where('bill_status','<',3)->where('bills.expire_date', '>=', Carbon::now()->addDay())->first();

        if(!empty($pending_bill)){
            return response()->json(array('error' => true, 'message' => 'This employer have a pending bill'));
        }
        else{


            $bill_rand_no = mt_rand(1000, 1999);
            $latest = DB::table('portal.bills')->orderBy('id', 'desc')->limit(1)->get();
            $latest_bill = $latest[0]->id+1;
            $bill_no = $bill_rand_no.$latest_bill;
            $description = 'Contribution';
            $bill_item = 'Contribution for '.Carbon::parse($request->month)->format('F Y');


            $bill = new Bill;
            $bill->employer_id = $employer->id;
            $bill->bill_no= $bill_no;
            $bill->bill_amount = $request->contribution;
            $bill->bill_description = $description;
            $bill->billed_item = $bill_item;
            $bill->contribution = $request->contribution;
            $bill->member_count = $request->employee_count;
            $bill->bill_status = 1;
            if(Carbon::parse($request->contrib_month)->format('m') == Carbon::parse(Carbon::now())->format('m')){
                $bill->contribution_due_date = Carbon::now()->addMonth()->endOfMonth()->toDateTimeString();
                $bill->expire_date = Carbon::parse(Carbon::now())->addMonths(2)->endOfMonth()->toDateTimeString();

            }else{
                $bill->contribution_due_date = Carbon::now()->endOfMonth()->toDateTimeString();
                $bill->expire_date =  Carbon::parse(Carbon::now())->addMonth()->endOfMonth()->toDateTimeString();
            }
            $bill->user_id = access()->user()->id;
            $bill->mobile_number = '255699210954';
            $bill->bill_source ='MAC';

            $bill->save();
            $response =  $this->requestControlNumber($bill->bill_no, $employer->id);
            return $response;
        }
    }


    public function requestControlNumber($bill_no){
        $bill = Bill::where('bill_no','=',$bill_no)->first();


        if(!empty($bill->employer_id)){
            $employer=Employer::where('id','=',$bill->employer_id)->first();
            $payer_name = $employer->name;
            $payer_id = $employer->id;
            $payer_email = $employer->email;
            $sub_sp_code=1001;
        }elseif(!empty($bill->staff_id)) {
            $user=User::where('external_id','=',$bill->staff_id)->first();
            $payer_name = $user->firstname.' '.$user->middlename.' '.$user->lastname;
            $payer_id = $user->id;
            $payer_email = $user->email;
            $sub_sp_code=1002;
        }else{
            $thirdparty=DB::table('main.thirdparties')->where('id','=',$bill->third_party_id)->first();
            if(!empty($thirdparty->employer_id)){
                $employer=Employer::where('id','=',$thirdparty->employer_id)->first();
                $payer_name = $employer->name;
                $payer_id = $employer->id;
                $payer_email = $employer->email;
                $sub_sp_code=1002;
            }else{
                Log::info(print_r($thirdparty,true));
                $payer_name = $thirdparty->firstname.' '.$thirdparty->middlename.' '.$thirdparty->lastname;
                $payer_id = $thirdparty->id;
                $payer_email ='noreply@gepg.com';
                $sub_sp_code=1002;
            }

        }

        $end = Carbon::parse($bill->expire_date);
        $tdy = Carbon::now();
        $days_expires_after = $end->diffInDays($tdy);


        $data = [
            "payment_ref"=> $bill->bill_no,
            "sub_sp_code"=> $sub_sp_code,
            "amount"=> $bill->bill_amount,
            "desc"=> $bill->bill_description,
            "gfs_code"=> $bill->gfs_code,
            "payment_type"=> 1,
            "payerid"=> $payer_id,
            "payer_name"=> $payer_name,
            "payer_cell"=>$bill->mobile_number,
            // "payer_cell"=>str_ireplace(' ','',$employer->phone),
            "payer_email"=> $payer_email,
            "days_expires_after"=> $days_expires_after,
            "generated_by"=> 'WCF',
            "approved_by"=> 'DG_WCF'
        ];

        // Log::info(print_r($data,true));
        try {
            $client = new Client();
        // $response =  $client->request('POST', 'http://gepg.test/bills/post_bill', ['json' => $data]);
            $response =  $client->request('POST', 'http://192.168.1.9/api/bills/post_bill', ['json' => $data]);
            return $response->getBody();
        }
        catch (ClientError $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            // displayTest($req,$resp);
            return $resp;
        }
        catch (ServerError $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return $resp;
        }
        catch (BadResponse $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return $resp;
        }
        catch( Exception $e){
            echo "Error Occured!";
        }


    }

    public function returnContribution($organization){ // return contribution of the due month only

        $last_contrib = DB::table('main.receipt_codes')
        ->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
        ->join('main.employers', 'receipts.employer_id', '=', 'employers.id')
        ->where('receipts.employer_id','=',$organization->id)
        ->orderBy('receipt_codes.contrib_month', 'desc')
        ->limit(1)->get();

        // dd($last_contrib);

        // if(!empty($last_contrib)){ //

        if($last_contrib->count()){ // he has atleast one contribution

            $last_contrib_mnth = Carbon::parse($last_contrib[0]->contrib_month) ;
            $tdy = Carbon::now();

            $diff_now_last_contrib_mnth = $tdy->diffInMonths($last_contrib_mnth);
            // dd($diff_now_last_contrib_mnth);

            $employees = DB::table('main.employee_employer')
            ->where('employer_id', '=', $organization->id)->get();

            $totalgrosspay = $employees->sum('grosspay');
            $employees_count = $employees->count();

            // dd('Total gross '.$totalgrosspay.' for employees '.$employees_count);
            // dd($totalgrosspay->sum('grosspay'));
            // dd(count($totalgrosspay));
            if($diff_now_last_contrib_mnth == 0){ // hadaiwi due month
                // dd('hadaiwi');
                $contribution = 0;
                return $c = [
                    'amount' => round($contribution,2),
                    'employees_count' => $employees_count,
                    'last_contrib_mnth' => $last_contrib_mnth->format('F Y')
                ];

            }

            else{ //
                // dd('deni : organization - '.$organization->name.' -- gross: ' .$totalgrosspay);
                if($organization->employer_category_cv_id == 37){ // private organization
                    $contribution = ($totalgrosspay * 0.01)/12;

                    // dd('Contribution '.round($contribution, 2).' for employees '.$employees_count);

                    // return $contribution;
                    return $c = [
                        'amount' => round($contribution,2),
                        'employees_count' => $employees_count,
                        'last_contrib_mnth' => $last_contrib_mnth->format('F Y')
                    ];
                }
                else{ //====== public organization
                    $contribution = ($totalgrosspay * 0.005)/12;
                    // return $contribution;
                    return $c = [
                        'amount' => round($contribution,2),
                        'employees_count' => $employees_count,
                        'last_contrib_mnth' => $last_contrib_mnth->format('F Y')
                    ];

                }
            }

        }

        else{ // hana mchango hata mmoja

            $employees = DB::table('main.employee_employer')
            ->where('employer_id', '=', $organization->id)->get();

            $totalgrosspay = $employees->sum('grosspay');
            $employees_count = $employees->count();

            if($organization->employer_category_cv_id == 37){ // private organization
                $contribution = $totalgrosspay * 0.01;
                // return $contribution;
                return $c = [
                    'amount' => round($contribution,2),
                    'employees_count' => $employees_count,
                    'last_contrib_mnth' => 'Not paid any contribution'
                ];
            }
            else{ //====== public organization
                $contribution = $totalgrosspay * 0.005;
                return $c = [
                    'amount' => round($contribution,2),
                    'employees_count' => $employees_count,
                    'last_contrib_mnth' => 'Not paid any contribution'
                ];
            }

        }



    }


    public function downloadNormalBill($id){

        $bill = Bill::find($id);
        $payer_name = $this->returnAdminstrativePayerName($bill);
        if(!is_null($bill)){
            $pdf = PDF::loadView('backend/finance/payment/includes/normal_bill', ['bill'=>$bill,'payer_name'=>$payer_name])->setPaper('a4', 'portrait');
            $name = "Direct Deposit";
            $name = str_replace(" ", "_", $name) . ".pdf";
            return $pdf->stream($name);
        }
        else{
            return redirect()->back();
        }

    }


    public function downloadNmbTransfer($id){

        $bill = Bill::find($id);
        $payer_name = $this->returnAdminstrativePayerName($bill);
        if(!is_null($bill)){
            $pdf = PDF::loadView('backend/finance/payment/includes/nmb_transfer', ['bill'=>$bill,'payer_name'=>$payer_name])->setPaper('a4', 'portrait');
            $name = "NMB Transfer form";
            $name = str_replace(" ", "_", $name) . ".pdf";
            return $pdf->stream($name);
        }
        else{

            return redirect()->back();
        }
    }

    public function downloadCrdbTransfer($id){
        $bill = Bill::find($id);
        $payer_name = $this->returnAdminstrativePayerName($bill);
        if(!is_null($bill)){
            $pdf = PDF::loadView('backend/finance/payment/includes/crdb_transfer', ['bill'=>$bill ,'payer_name'=>$payer_name])->setPaper('a4', 'portrait');
            $name = "CRDB Transfer form";
            $name = str_replace(" ", "_", $name) . ".pdf";
            return $pdf->stream($name);
        }
        else{
            return redirect()->back();
        }

    }

    public function returnReconciliation()
    {

        // $reconcile_data = DB::table('main.successful_transactions_gepg')
        // ->get();
        // $success_trx = $reconcile_data->count();

        // //pending bills count
        // $pending_data = DB::table('portal.payments')
        // ->join('portal.bills','payments.bill_id','=','bills.bill_no')
        // ->join('portal.users','bills.user_id','=','users.id')
        // ->distinct('control_no')
        // ->where('paid_amount','=',null)
        // ->where('bill_status','!=',3)
        // ->get(['control_no']);
        // $pending_trx = $pending_data->count();

        // // dump($pending_trx);

        // $success_trx_GePG=DB::table("portal.gepgreconcile")->select('*')
        // ->whereNOTIn('SpBillId',function($query){
        //     $query->select('bill_id')->from('portal.payments');
        // })->get();

        // $success_trx_no_rct=$success_trx_GePG->count();


        return view('backend.finance.recon_menu');
        //->with(compact('success_trx','pending_trx','success_trx_no_rct'));

    }

    public function controlNumberReceipts(ControlNoDataTable $dataTable)
    {

        //  $query = DB::table('portal.payments')
        //        ->join('main.receipts','payments.control_no','=','receipts.pay_control_no')
        //        ->join('portal.bills','payments.bill_id','=','bills.bill_no')
        //        ->select('payments.control_no','receipts.rctno','payments.payer_name','bills.bill_amount','payments.paid_amount','payments.payment_date','payments.account_number','payments.bank_name')
        //        ->where('paid_amount','!=',null);

        // return Datatables::of($query)->make(true);

        return $dataTable->render('backend/finance/reconciliation/successfultrx');
    }

    public function controlNumberPending()
    {

        $pending_qx = DB::table('portal.payments')
        ->select([
            'payments.payer_name',
            'payments.control_no',
            'users.name',
            'users.phone',
            'bills.bill_amount',
            'bills.user_id',
            'bills.bill_status',
            'bills.bill_source',
            'bills.bill_description',
            'bills.created_at',
            'bills.expire_date',
            'bills.member_count',
            'bills.mobile_number',
        ])
        ->join('portal.bills','payments.bill_id','=','bills.bill_no')
        ->join('portal.users','bills.user_id','=','users.id')
        ->distinct('control_no')
        ->where('paid_amount','=',null);

        return Datatables::of($pending_qx)->editColumn('bill_status',function($data)
        {
            if($data->bill_status==2 && $data->expire_date<Carbon::now()){
                return '<label class="alert alert-warning"> Expired </label>';

            }elseif($data->bill_status==4){

                return '<label class="label label-danger"> Cancelled </label>';
            }elseif ($data->bill_status==2 && $data->expire_date>Carbon::now()) {
                return '<label class="label label-info"> Pending </label>';
            }


        })->editColumn('name',function($data) {
            if(strtoupper(trim($data->bill_source)) == 'MAC'){
                $user = DB::table('main.users')->find($data->user_id);
                if(!empty($user)){
                    return $user->firstname.' '.$user->lastname;
                }
            }else{
                return $data->name;
            }

        })
        ->rawColumns(['bill_status'])
        ->make(true);

    }

    public function viewSuccessfullTrx(ControlNoDatatable $dataTable, DateRangeRequest $request)
    {

        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' =>(isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' =>(isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend.finance.reconciliation.successfulltrx', ["request" => $request]);
        //return view('backend.finance.reconciliation.successfulltrx');

    }

    public function viewSuccessfullGePGTrx(GepGReconciliationDataTable $dataTable,DateRangeRequest $request)
    {


        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with([
            'from_date' =>(isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' =>(isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend.finance.reconciliation.daily_GePG_successfulltrx', ["request" => $request]);

    }


    public function viewFailedTrx()
    {


        return view('backend.finance.reconciliation.failedtrx');
    }


    public function getSuccessfullTrx()
    {
        return view('backend.finance.reconciliation.successfulltrx');
    }


    public function getFailedTrx()
    {

        // dd('Failed');
        return view('backend.finance.reconciliation.failedtrx');

    }

    public function requestReconciliation()
    {
        $trx_id=1025;
        $trx_date='2018-03-27'; //Will be automated to fetch and increment 1 day
        $recon_type=1;

        $data=[
            "trx_id"=> $trx_id,
            "trx_date"=>$trx_date,
            "recon_type"=> $recon_type,
        ];
        try {
            $client = new Client();

            // $response =  $client->request('POST', 'http://172.16.30.14:83/bills/reconcile', ['json' => $data]);
            return $response->getBody();
        }
        catch (ClientError $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            // displayTest($req,$resp);
            return $resp;
        }
        catch (ServerError $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return $resp;
        }
        catch (BadResponse $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return $resp;
        }
        catch( \Exception $e){
            echo "Error Occured!";
        }


    }






// ================================= MAC ----- ERP  ============================

    public function postReceiptErpApi($receipt_id, $employer=true){

        $receipts_gl =  new ReceiptsErpApiRepositroy();

        $transaction_ids = $receipts_gl->saveApiTransaction($receipt_id, $employer);

        // Log::info(print_r($transaction_ids, true));

        foreach ($transaction_ids as $transaction_id) {
            if(!is_null($transaction_id)){
                try {
                    $client = new Client();
                    $response =  $client->request('GET', config('constants.MAC_ERP.GL_TO_Q').$transaction_id);
                    $status = $response->getStatusCode();
                    // Log::info($status);
                }
                catch (ClientError $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status:'.$status.' ClientError');

                }
                catch (ServerError $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status: '.$status.' ServerError');


                }
                catch (BadResponse $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($transaction_id.' Status: '.$status,' BadResponse');

                }
                catch(\Exception $e){
                    $status = $e->getCode();
                    Log::info($transaction_id.' Status: '.$status.' Exception');

                }

                $receipts_gl->updateReceiptPostedToQ($transaction_id, $status);
            }

        }

    }

    public function returnErpMenu(){
        return view('backend.finance.erp_menu');
    }

    public function viewSupplier()
    {
        return view("backend/finance/receipt/employer_supplier");
    }

    public function returnSupplierForm($org_id)
    {
        $employer=Employer::find($org_id);
        $region = DB::table('main.regions')->where('id',$employer->region_id)->first();
        return view("backend/finance/receipt/new_supplier_erp")->with('employer', $employer)
        ->with('region', $region);

    }

    public function storeSupplier(SupplierFormRequest $request)
    {
        $supplier_exist=DB::table('main.erp_suppliers')->where('supplier_id',$request->supplier_id)->first();
        if($supplier_exist){
            return view("backend/finance/receipt/employer_supplier");

        }else{
            $random_id=DB::table('main.employers')->where('reg_no',$request->supplier_id)->first();
            $new_supplier=new ErpSupplier;
            $new_supplier->trx_id=mt_rand(1000, 1999).$random_id->id;
            $new_supplier->supplier_name=$request->supplier_name;
            $new_supplier->supplier_id=$request->supplier_id;
            $new_supplier->vendor_site_code=$request->vendor_site_code;
            $new_supplier->address_line1=$request->address_line1;
            $new_supplier->address_line2=$request->address_line2;
            $new_supplier->city=$request->city;
            $new_supplier->liability_account=$request->liability_account;
            $new_supplier->status_code=200;
            $new_supplier->pay_group='BENEFICIARIES';
            $new_supplier->user_id=access()->user()->id;
            $new_supplier->save();

            $this->postSupplierErpApi();

            return view("backend/finance/receipt/employer_supplier")->with('success','Supplier Created and Posted to ERP');
        }
    }

    public function storeSupplierEmployee(SupplierFormRequest $request)
    {
        $supplier_exist=DB::table('main.erp_suppliers')->where('supplier_id',$request->supplier_id)->first();
        if($supplier_exist){
            return view("backend/finance/receipt/employer_supplier");

        }else{
            $new_supplier=new ErpSupplier;
            $new_supplier->trx_id=mt_rand(1000, 1999).$request->supplier_id;
            $new_supplier->supplier_name=$request->supplier_name;
            $new_supplier->supplier_id=$request->supplier_id;
            $new_supplier->vendor_site_code=$request->vendor_site_code;
            $new_supplier->address_line1=$request->address_line1;
            $new_supplier->address_line2=$request->address_line2;
            $new_supplier->city=$request->city;
            $new_supplier->liability_account=$request->liability_account;
            $new_supplier->status_code=200;
            $new_supplier->pay_group='BENEFICIARIES';
            $new_supplier->user_id=access()->user()->id;
            $new_supplier->save();

            $this->postSupplierErpApi();

            return view("backend/finance/receipt/employee_supplier_erp")->with('success','Supplier Created and Posted to ERP');
        }
    }

    public function postSupplierErpApi()
    {
        $url ='http://192.168.1.9/mac_erp/public/api/post_supplier_to_q/';
        Log::info($url);

        try {

            $client = new Client();
            $response =  $client->request('GET', $url);
            $status = $response->getStatusCode();
        }
        catch (ClientError $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status:'.$status.' ClientError');
        }
        catch (ServerError $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status: '.$status.' ServerError');
        }
        catch (BadResponse $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status: '.$status,' BadResponse');

        }
        catch(\Exception $e){
            $status = $e->getCode();
            Log::info(' Status: '.$e->getMessage().' Exception');

        }

    }


    public function viewContribRefund(contributionRefundDataTable $dataTable, DateRangeRequest $request)
    {
        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' =>($date_range['from_date']) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' =>($date_range['to_date']) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])
        ->render('backend.finance.receipt.contrib_refund', ["request" => $request]);

    }



    public function refundToERP($refund_id=null)
    {
        // dd('Niko Hapa');

        $refund_receipt =  new RefundErpApiRepository();

        $transaction_ids = $refund_receipt->saveApiTransaction($refund_id);

        foreach ($transaction_ids as $transaction_id) {

            if(!is_null($transaction_id)){
                try {
                    $client = new Client();
                    $response =  $client->request('GET', config('constants.MAC_ERP.AP_TO_Q').$transaction_id);
                    $status = $response->getStatusCode();
                }
                catch (ClientError $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($refund_id.' Status:'.$status.' ClientError');

                }
                catch (ServerError $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($refund_id.' Status: '.$status.' ServerError');


                }
                catch (BadResponse $e) {

                    $req = $e->getRequest();
                    $status =$e->getStatusCode();
                    Log::info($refund_id.' Status: '.$status,' BadResponse');

                }
                catch(\Exception $e){
                    $status = $e->getCode();
                    Log::info($refund_id.' Status: '.$status.' Exception');

                }
                $refund_receipt->updateRefundPostedToQ($transaction_id, $status);

            }

        }
        return redirect()->route('backend.finance.view.contrib.refund')->with('success', ['your message,here']);;
    }



    public function refundBulkToERP()
    {
        dd('Success');
    }

    public function viewSupplierEmployee()
    {

        return view("backend/finance/receipt/employee_supplier_erp");


    }

    public function returnEmployeeSupplier($employee_id)
    {
        //Get employee as supplier
        $employee=DB::table('main.employees')
        ->join('main.employee_employer','employees.id','=','employee_employer.employee_id')
        ->where('employees.id','=',$employee_id)
        ->first();

        $employee_name=$employee->firstname.' '.$employee->middlename.' '.$employee->lastname;

        //Get employer for linking with regions
        $employer=DB::table('main.employers')->where('id','=',$employee->employer_id)
        ->first();

        //Get regions for updating supplier region
        $region = DB::table('main.regions')->where('id',$employer->region_id)->first();

        return view("backend/finance/receipt/new_employee_supplier_erp")->with('employee_name',$employee_name)
        ->with('region', $region)->with('employee', $employee)->with('employer',$employer);

    }


    public function administrativeOrderForms($bill_id)
    {

        $bill = Bill::select(['bills.id as b_id','bills.*','payments.*'])->leftjoin('portal.payments', 'bills.bill_no', '=', 'payments.bill_id')->where('bills.id',$bill_id)->first();
        if($bill){
            $payer_name = $this->returnAdminstrativePayerName($bill);
            return view("backend/finance/receipt/pay_form_non_contributions", compact('bill','payer_name'));
        }else{
            return redirect()->back();
        }

    }


    public function returnAdminstrativePayerName($bill)
    {
        $payer_name = '';
        if(is_null($bill->employer_id)){
            if(!empty($bill->staff_id)) {
                $user=User::where('external_id','=',$bill->staff_id)->first();
                $payer_name = $user->firstname.' '.$user->middlename.' '.$user->lastname;
            }else{
                $thirdparty=DB::table('main.thirdparties')->where('id','=',$bill->third_party_id)->first();
                if(!empty($thirdparty->employer_id)){
                    $employer=Employer::where('id','=',$thirdparty->employer_id)->first();
                    $payer_name = $employer->name;

                }else{
                    $payer_name = $thirdparty->firstname.' '.$thirdparty->middlename.' '.$thirdparty->lastname;

                }
            }
        }

        return $payer_name;
    }

    public 
    function viewSuccessfullyGltoERP(successfullyGltoERPDataTable $dataTable, DateRangeRequest $request)
    {

        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' =>(isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' =>(isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend.finance.erp_integration.successfullyGltoERP', ["request" => $request]);
        //return view('backend.finance.reconciliation.successfulltrx');

    }

    public function resendBill(){

        $bills = Bill::where('bill_status',1)->get();

        // dd(count($bills));
        foreach ($bills as $bill) {
            $response =  $this->reRequestControlNumber($bill);
            // dump($response);
        }

        return redirect()->back()->withFlashSuccess("Success, Bills Reposted");
        
    }

    public function reRequestControlNumber($bill){
        $organization =  Employer::find($org_id);
        $end = Carbon::parse($bill->expire_date);
        $tdy = Carbon::now();
        $days_expires_after = $end->diffInDays($tdy);

        if(!empty($bill->employer_id)){
            $employer=Employer::where('id','=',$bill->employer_id)->first();
            $payer_name = $employer->name;
            $payer_id = $employer->id;
            $payer_email =  empty($employer->email) ? 'noreply@gepg.com' : $employer->email;
            $sub_sp_code=1001;
        }elseif(!empty($bill->staff_id)) {
            $user=User::where('external_id','=',$bill->staff_id)->first();
            $payer_name = $user->firstname.' '.$user->middlename.' '.$user->lastname;
            $payer_id = $user->id;
            $payer_email =  empty($user->email) ? 'noreply@gepg.com' : $user->email;
            $sub_sp_code=1002;
        }else{
            $thirdparty=DB::table('main.thirdparties')->where('id','=',$bill->third_party_id)->first();
            if(!empty($thirdparty->employer_id)){
                $employer=Employer::where('id','=',$thirdparty->employer_id)->first();
                $payer_name = $employer->name;
                $payer_id = $employer->id;
                $payer_email =  empty($employer->email) ? 'noreply@gepg.com' : $employer->email;
                $sub_sp_code=1002;
            }else{
                Log::info(print_r($thirdparty,true));
                $payer_name = $thirdparty->firstname.' '.$thirdparty->middlename.' '.$thirdparty->lastname;
                $payer_id = $thirdparty->id;
                $payer_email ='noreply@gepg.com';
                $sub_sp_code=1002;
            }

        }


        $end = Carbon::parse($bill->expire_date);
        $tdy = Carbon::now();
        $days_expires_after = $end->diffInDays($tdy);
        if($days_expires_after == 0){
            $days_expires_after = 1;
        }

        $data = [
            "payment_ref"=> $bill->bill_no,
            "sub_sp_code"=> $sub_sp_code,
            "amount"=> $bill->bill_amount,
            "desc"=> $bill->bill_description,
            "gfs_code"=> $bill->gfs_code,
            "payment_type"=> 1,
            "payerid"=> $payer_id,
            "payer_name"=> $payer_name,
            "payer_cell"=>$bill->mobile_number,
            "payer_cell"=>trim($bill->mobile_number,'+'),
            "payer_email"=> $payer_email,
            "days_expires_after"=> $days_expires_after,
            "generated_by"=> 'WCF',
            "approved_by"=> 'DG_WCF'
        ];


        try {
            $client = new Client();
            // $response =  $client->request('POST', 'http://172.16.30.14:83/bills/post_bill', ['json' => $data]);
            // return $response->getBody();
        }

        catch (ClientError $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return response()->json(array('error' => true, 'message' => 'Client error. Status Code '.$resp));
                // return $resp;
        }
        catch (ServerError $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return response()->json(array('error' => true, 'message' => 'Server error. Status Code '.$resp));

        }
        catch (BadResponse $e) {

            $req = $e->getRequest();
            $resp =$e->getStatusCode();
            return response()->json(array('error' => true, 'message' => 'Bad request error. Status Code '.$resp));

        }
        catch( \ Exception $e){
            $resp = $e->getCode();
            // $this->adminSms('Hi Admin, Rabbit went Down');

               // Log::info($resp);
            return response()->json(array('error' => true, 'message' => 'Exception error. Status Code '.$resp));
        }

    }





}
