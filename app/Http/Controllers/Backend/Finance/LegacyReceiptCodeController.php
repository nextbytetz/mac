<?php

namespace App\Http\Controllers\Backend\Finance;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Finance\Receipt\UpdateLegacyReceiptMonthsRequest;
use App\Http\Requests\Backend\Finance\Receipt\UpdateReceiptCodeRequest;
use App\Jobs\LoadContribution;
use App\Jobs\LoadContributionLegacy;
use App\Jobs\RegisterContribution;
use App\Jobs\RegisterContributionLegacy;
use App\Models\Finance\Receipt\LegacyReceiptCode;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class LegacyReceiptCodeController extends Controller
{


use AttachmentHandler;

    protected $legacy_receipt_codes;
    protected $banks;

    public function __construct() {
        $this->legacy_receipt_codes = new LegacyReceiptCodeRepository();
        $this->banks = new BankRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Display the specified resource.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $legacy_receipt_code = $this->legacy_receipt_codes->findOrThrowException($id);
        return view('backend.operation.compliance.member.contribution_legacy.profile')
            ->withLegacyReceiptCode($legacy_receipt_code);
    }




    /**
     * @param ReceiptCode $receipt_code
     * @return mixed
     */
    public function edit(LegacyReceiptCode $legacy_receipt_code)
    {
        $this->middleware('access.routeNeedsPermission:edit_contribution_month');
        $ContributionDateThreshold = Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1);
        $contribDate = Carbon::parse($legacy_receipt_code->contrib_month);
        return view('backend.operation.compliance.member.contribution_legacy.edit')
            ->withLegacyReceiptCode($legacy_receipt_code)
            ->withThisDate(Carbon::now()->format('Y-n-j'))
            ->withContributionDateThreshold($ContributionDateThreshold)
            ->withMonth(Carbon::parse($contribDate)->format("m"))
            ->withYear(Carbon::parse($contribDate)->format("Y"))
            ->withYearThreshold(Carbon::parse($ContributionDateThreshold)->format("Y"));
    }

    /**
     * @param ReceiptCode $receipt_code
     * @param UpdateReceiptCodeRequest $request
     * @return mixed
     */
    public function update(LegacyReceiptCode $legacy_receipt_code, UpdateLegacyReceiptMonthsRequest $request)
    {
        $this->legacy_receipt_codes->update($legacy_receipt_code, $request->all());
        return redirect()->route('backend.finance.legacy_receipt_code.show', $legacy_receipt_code->id)->withFlashSuccess(trans('alerts.backend.finance.receipt.contribution.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function linkedFile(LegacyReceiptCode $legacy_receipt_code) {
        workflow([['wf_module_group_id' => 7, 'resource_id' => $legacy_receipt_code->legacyReceipt->id]])->checkLevel(3);
        access()->hasWorkflowDefinition(7,3);
        return view('backend.operation.compliance.member.contribution_legacy.store_linked_file')
            ->withLegacyReceiptCode($legacy_receipt_code);
    }

    public function linkedFileStore(LegacyReceiptCode $legacy_receipt_code) {
        if ($this->saveLinkedFileLegacyAttachment($legacy_receipt_code)) {
            dispatch(new LoadContributionLegacy($legacy_receipt_code));
            /* $this->receipts->updateIfCompleteLevel2($receipt_code->receipt_id); */
            return response()->json(['success' => true, 'message' => trans('alerts.backend.finance.receipt.linked_file_created')]);
        }
        return response()->json(['success' => false, 'message' => trans('exceptions.backend.finance.receipts.contribution.upload')]);
    }

    public function uploadProgress(LegacyReceiptCode $legacy_receipt_code)
    {
        return response($legacy_receipt_code->uploadPercent(), 200);
    }

    public function downloadError(LegacyReceiptCode $legacy_receipt_code)
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=download_error.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        $list = $legacy_receipt_code->legacyContributionTemps()->select(['error_report'])->where(['error' => 1])->get()->toArray();

        # add headers for each column in the CSV download
        //array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };
        return response()->stream($callback, 200, $headers);
    }


    /**
     * @param LegacyReceiptCode $legacy_receipt_code
     * @return mixed
     */
    public function registerContribution(LegacyReceiptCode $legacy_receipt_code) {
        workflow([['wf_module_group_id' => 7, 'resource_id' => $legacy_receipt_code->legacyReceipt->id]])->checkLevel(3);
        access()->hasWorkflowDefinition(7,3);
        return DB::transaction(function () use ($legacy_receipt_code) {
            $difference = $legacy_receipt_code->amount - $legacy_receipt_code->amount_imported;
            if (false) {
                return redirect()->back()->withFlashDanger(trans('exceptions.backend.contribution.amount_mismatch'));
            } else {
                dispatch(new RegisterContributionLegacy($legacy_receipt_code,access()->id()));
                return redirect()->route('backend.finance.legacy_receipt.profile', $legacy_receipt_code->legacy_receipt_id)->withFlashSuccess(trans('alerts.backend.contribution.contribution_loaded'));
            }
        });
    }


    /**
     * @param $id
     * @return mixed
     */

    public function getMemberContributionsForDataTable($id)
    {
        $contribution = new ContributionRepository();
        return Datatables::of($contribution->getMemberLegacyContributions($id))
            ->addcolumn('grosspay_formatted', function ($contribution) {
                return $contribution->grosspay_formatted;
            })
            ->addcolumn('employee', function ($contribution) {
                return $contribution->employee->name;
            })
            ->addcolumn('member_amount_formatted', function ($contribution) {
                return $contribution->member_amount_formatted;
            })
            ->make(true);
    }

}
