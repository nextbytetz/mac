<?php

namespace App\Http\Controllers\Backend\Finance;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Finance\Receipt\UpdateReceiptCodeRequest;
use App\Jobs\RegisterContribution;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Jobs\LoadContribution;
use Illuminate\Support\Facades\DB;

class ReceiptCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AttachmentHandler;

    protected $receipt_codes;
    protected $receipts;

    /**
     * ReceiptCodeController constructor.
     * @param ReceiptCodeRepository $receipt_codes
     */
    public function __construct(ReceiptCodeRepository $receipt_codes) {
        $this->receipt_codes = $receipt_codes;
        $this->receipts = new ReceiptRepository();
        $this->middleware('access.routeNeedsPermission:edit_contribution_month', ['only' => ['edit']]);
    }



    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function show($id)
    {
        $receipt_code = $this->receipt_codes->findOrThrowException($id);
        $duplicates = $this->receipt_codes->getDuplicates($receipt_code);
        $recent_receipt_code = $this->receipt_codes->getRecentReceiptCode($receipt_code->employer_id, $receipt_code->contrib_month);
        return view('backend.operation.compliance.member.contribution.profile')
            ->with("receipt_code", $receipt_code)
            ->with("duplicates", $duplicates)
            ->with("recent_receipt_code", $recent_receipt_code);
    }

    /**
     * @param ReceiptCode $receipt_code
     * @return mixed
     */
    public function edit(ReceiptCode $receipt_code)
    {
        //$this->middleware('access.routeNeedsPermission:edit_contribution_month');
        $ContributionDateThreshold = Carbon::now()->addMonths(sysdefs()->data()->total_allowed_contrib_months - 1);
        $contribDate = Carbon::parse($receipt_code->contrib_month);
        return view('backend.operation.compliance.member.contribution.edit')
            ->with("receipt_code", $receipt_code)
            ->with("this_date", Carbon::now()->format('Y-n-j'))
            ->with("contribution_date_threshold", $ContributionDateThreshold)
            ->with("month", Carbon::parse($contribDate)->format("m"))
            ->with("year", Carbon::parse($contribDate)->format("Y"))
            ->with("year_threshold", Carbon::parse($ContributionDateThreshold)->format("Y"));
    }

    /**
     * @param ReceiptCode $receipt_code
     * @param UpdateReceiptCodeRequest $request
     * @return mixed
     */
    public function update(ReceiptCode $receipt_code, UpdateReceiptCodeRequest $request)
    {
        $this->receipt_codes->update($receipt_code, $request->all());
        return redirect()->route('backend.finance.receipt_code.show', $receipt_code->id)->withFlashSuccess(trans('alerts.backend.finance.receipt.contribution.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /*
     * Get contributions tracks for this receipt code
     */
    public function getContributionsTracksForDataTable($id) {
        return Datatables::of($this->receipt_codes->getContributionTracks($id))
            ->editColumn('user_id', function($contribution_track) {
                return $contribution_track->user->username;
            })
            ->editColumn('created_at', function($contribution_track) {
                return $contribution_track->created_at_formatted;
            })
            ->editcolumn('status', function ($contribution_track) {
                return $contribution_track->status_label;
            })
            ->addcolumn('action', function ($contribution_track) {
                return $contribution_track->action_buttons;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function getMemberContributionsForDataTable($id)
    {
        $contribution = new ContributionRepository();
        return Datatables::of($contribution->getMemberContributions($id))
            ->addcolumn('grosspay_formatted', function ($contribution) {
                return $contribution->grosspay_formatted;
            })
            ->addcolumn('basicpay_formatted', function ($contribution) {
                return $contribution->basicpay_formatted;
            })
            /*->addcolumn('employee', function ($contribution) {
                return $contribution->employee->name;
            })*/
            ->addcolumn('member_amount_formatted', function ($contribution) {
                return $contribution->member_amount_formatted;
            })
            ->make(true);
    }


    public function linkedFile(ReceiptCode $receipt_code) {
        workflow([['wf_module_group_id' => 5, 'resource_id' => $receipt_code->receipt->id]])->checkLevel(3);
        access()->hasWorkflowDefinition(5,3);
        return view('backend.operation.compliance.member.contribution.store_linked_file')
            ->withReceiptCode($receipt_code);
    }

    public function linkedFileStore(ReceiptCode $receipt_code) {
        if ($this->saveLinkedFileAttachment($receipt_code)) {
            dispatch(new LoadContribution($receipt_code));
            /* $this->receipts->updateIfCompleteLevel2($receipt_code->receipt_id); */
            return response()->json(['success' => true, 'message' => trans('alerts.backend.finance.receipt.linked_file_created')]);
        }
        return response()->json(['success' => false, 'message' => trans('exceptions.backend.finance.receipts.contribution.upload')]);
    }

    public function uploadProgress(ReceiptCode $receipt_code)
    {
        return response($receipt_code->uploadPercent(), 200);
    }

    public function downloadError(ReceiptCode $receipt_code)
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=download_error.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        $list = $receipt_code->contributionTemps()->select(['error_report'])->where(['error' => 1])->get()->toArray();

        # add headers for each column in the CSV download
        //array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };
        return response()->stream($callback, 200, $headers);
    }


    /**
     * @param ReceiptCode $receipt_code
     * @return mixed
     */
    public function registerContribution(ReceiptCode $receipt_code) {
        workflow([['wf_module_group_id' => 5, 'resource_id' => $receipt_code->receipt->id]])->checkLevel(3);
        access()->hasWorkflowDefinition(5,3);
        return DB::transaction(function () use ($receipt_code) {
            $difference = $receipt_code->amount - $receipt_code->amount_imported;
            if (false) {
                return redirect()->back()->withFlashDanger(trans('exceptions.backend.contribution.amount_mismatch'));
            } else {

                dispatch(new RegisterContribution($receipt_code, access()->id()));
//                $this->receipt_codes->registerContribution($receipt_code);
                return redirect()->route('backend.finance.receipt.profile_by_request', $receipt_code->receipt_id)->withFlashSuccess(trans('alerts.backend.contribution.contribution_loaded'));
            }
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkExisting()
    {
        $input = request()->all();
        $return = $this->receipt_codes->checkExisting($input);
        return response()->json(['success' => true, 'message' => "", 'return' => $return]);
    }


}
