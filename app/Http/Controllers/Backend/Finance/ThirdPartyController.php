<?php

namespace App\Http\Controllers\Backend\Finance;

use App\Http\Requests\Backend\Finance\Receipt\StoreThirdpartyRequest;
use App\Http\Requests\Backend\Finance\Receipt\UpdateThirdpartyRequest;
use App\Models\Finance\Thirdparty;
use App\Repositories\Backend\Finance\ThirdpartyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ThirdPartyController extends Controller
{

    /**
     * @var ThirdpartyRepository
     */
    protected $thirdparty;

    /**
     * ThirdPartyController constructor.
     * @param ThirdPartyRepository $thirdparty
     */
    public function __construct(ThirdpartyRepository $thirdparty)
    {
        $this->thirdparty = $thirdparty;
        $this->middleware('access.routeNeedsPermission:manage_thirdparty', ['only' => ['create', 'edit', 'destroy', 'statusChange']]);
        //$this->middleware('access.routeNeedsPermission:edit_lawyer', ['only' => ['edit']]);
        //$this->middleware('access.routeNeedsPermission:delete_lawyer', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend/finance/receipt/thirdparty/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/finance/receipt/thirdparty/create');
    }

    /**
     * @param StoreThirdpartyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreThirdpartyRequest $request)
    {
        $input = $request->all();
        $thirdparty= $this->thirdparty->store($input);
        return response()->json(['success' => true, 'id' => $thirdparty->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param Thirdparty $thirdparty
     * @return $this
     */
    public function show(Thirdparty $thirdparty)
    {
        return view('backend/finance/receipt/thirdparty/show')
            ->with("thirdparty", $thirdparty);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Thirdparty $thirdparty
     * @return $this
     */
    public function edit(Thirdparty $thirdparty)
    {
        $category = ($thirdparty->employer_id) ? 1 : 0;
        return view("backend/finance/receipt/thirdparty/edit")
            ->with("thirdparty", $thirdparty)
            ->with("category", $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Thirdparty $thirdparty
     * @param UpdateThirdpartyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Thirdparty $thirdparty, UpdateThirdpartyRequest $request)
    {
        $this->thirdparty->update($thirdparty, $request->all());
        return response()->json(['success' => true, 'id' => $thirdparty->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Thirdparty $thirdparty
     * @return mixed
     */
    public function destroy(Thirdparty $thirdparty)
    {
        $this->thirdparty->destroy($thirdparty);
        return redirect()->route('backend.finance.thirdparty.index')->withFlashSuccess("Success, Third Party Payer has been deleted");
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return Datatables::of($this->thirdparty->getForDataTable())
            ->addColumn('receipts', function($query) {
                return $query->receipts()->count();
            })
            ->make(true);
    }

    public function getReceipt($id)
    {
        return Datatables::of($this->thirdparty->getReceipt($id))
            ->make(true);
    }

    public function statusChange(Thirdparty $thirdparty, $status)
    {
        $thirdparty->isactive = $status;
        $thirdparty->save();
        return redirect()->back()->withFlashSuccess("Success, Third Party Status has been updated");
    }

}
