<?php

namespace App\Http\Controllers\Backend\Finance;

use App\Http\Controllers\Controller;
//- Yajra\Datatables\Facades\Datatables;
use Yajra\Datatables\Datatables;
use App\Repositories\Backend\Finance\Currency\CurrencyRepository;
use App\Http\Requests\Backend\Finance\Currency\CreateCurrencyRequest;

class CurrencyController extends Controller
{

    protected $currencies;


    public function __construct(CurrencyRepository $currencies) {
        $this->currencies = $currencies;
           }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('backend.finance.currency.index');
    }

    public function get( ) {
        return Datatables::of($this->currencies->getForDataTable())

            ->addColumn('action', function($currency) {
                return $currency->action_buttons;
            })
                      ->make(true);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.finance.currency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCurrencyRequest $request)
    {
        //
        $this->currencies->create($request->all());
        return redirect()->route('backend.finance.currency.create')->withFlashSuccess(trans('alerts.backend.finance.currency.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $currency = $this->currencies->findOrThrowException($id);
        return view('backend.finance.currency.edit')
            ->withCurrency($currency);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, CreateCurrencyRequest $request)
    {
        //
        $this->currencies->update($id, $request->all());
        return redirect()->route('backend.finance.currency.index')->withFlashSuccess(trans('alerts.backend.finance.currency.updated'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
//        $this->currencies->destroy($id);
//        return redirect()->route('backend.finance.currency.index')->withFlashSuccess(trans('alerts.backend.finance.currency.deleted'));
        return view('backend.finance.currency.create');


    }


    public function show()
    {
        //

    }
}
