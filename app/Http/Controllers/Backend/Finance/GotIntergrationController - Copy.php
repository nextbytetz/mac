<?php

namespace App\Http\Controllers\Backend\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use PDF;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException as ConnectException;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use Illuminate\Support\Facades\Response;
use DB;
use Illuminate\Support\Facades\Log;
use function \FluidXml\fluidxml;
use App\Models\Finance\Bill;
use App\Models\Operation\Compliance\Member\Employer;

class GotIntergrationController1 extends Controller
{

	public function returnTreasuryMenu(){
		return view('backend.finance.govt_intergration.treasury.treasury_menu');
	}

	public function returnSummaryView()
	{
		$employer = Employer::find(9386);
		$pending_bill = Bill::select(['bills.id as b_id','bills.*','payments.*'])->leftjoin('portal.payments', 'bills.bill_no', '=', 'payments.bill_id')->where('bill_status','<',3)->where('bills.expire_date', '>=', Carbon::now()->addDay())->where('bills.employer_id',9386)->first();

		$contribution = $this->returnContribution();
		$chkdate = Carbon::parse($contribution['month'])->format('Y-m-28');
		$dd = Carbon::parse($chkdate)->subMonth()->endOfMonth()->format('Y-m-d');
		$previous = $this->returnSummaryDetails($dd);
		
		return view('backend.finance.govt_intergration.treasury.summary',compact('pending_bill','contribution','employer','previous'));
	}




	public function returnContribution(){

		$last_contrib = DB::table('main.receipt_codes')
		->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
		->join('main.employers', 'receipts.employer_id', '=', 'employers.id')
		->where('receipts.employer_id','=',9386)
		->where('receipts.iscancelled','!=',1)
		->orderBy('receipt_codes.contrib_month', 'desc')
		->limit(1)->get();

		$tdy = Carbon::parse(Carbon::now()->format('28-m-Y'));

		
		if($last_contrib->count() > 0){

			$last_contrib_mnth = Carbon::parse($last_contrib[0]->contrib_month);

			$diff_now_last_contrib_mnth = $last_contrib_mnth->diffInMonths($tdy, false);

			if($diff_now_last_contrib_mnth >= 2){
				$contrib_month = $tdy->subMonth()->format('F Y');
				return $this->returnSummaryDetails(Carbon::parse($contrib_month)->endOfMonth()->format('Y-m-d'));
			}

			elseif($diff_now_last_contrib_mnth == 1){
				$contrib_month = $tdy->format('F Y');
				return $this->returnSummaryDetails(Carbon::parse($contrib_month)->endOfMonth()->format('Y-m-d'));
			}
			else{ 
				$contrib_month = $last_contrib_mnth->addMonth()->format('F Y');
				return $this->returnSummaryDetails(Carbon::parse($contrib_month)->endOfMonth()->format('Y-m-d'));
			}

		}
        else{ // hana mchango hata mmoja 
        	$contrib_month = $tdy->subMonth()->format('F Y');
        	return $this->returnSummaryDetails(Carbon::parse($contrib_month)->endOfMonth()->format('Y-m-d'));
        }



    }


    public function returnSummaryDetails($checkdate)
    {
    	// dump('ccc '.$checkdate);
    	$gf_summary = DB::table('main.treasury_employees_votes_staging')
    	->where('funding_source', 'GF')
    	->where('checkDate',$checkdate)
    	->orderBy('id', 'desc')->limit(1)->get();

    	$cf_summary = DB::table('main.treasury_employees_votes_staging')
    	->where('funding_source', 'CF')
    	->where('checkDate',$checkdate)
    	->orderBy('id', 'desc')->limit(1)->get();

    	$summary_data = [ 
    		'month' => $checkdate,
    		'gf' => [
    			'amount' => isset($gf_summary[0]->Total_amount) ? $gf_summary[0]->Total_amount : 0,
    			'employees_count' => isset($gf_summary[0]->Total_Employees) ? $gf_summary[0]->Total_Employees : 0,
    			'votes_count' => isset($gf_summary[0]->Total_Votes) ? $gf_summary[0]->Total_Votes : 0,
    			'contrib_month' => Carbon::parse($checkdate)->format('F, Y'),
    		],
    		'cf' => [
    			'amount' => isset($cf_summary[0]->Total_amount) ? $cf_summary[0]->Total_amount : 0,
    			'employees_count' => isset($cf_summary[0]->Total_Employees) ? $cf_summary[0]->Total_Employees : 0,
    			'votes_count' => isset($cf_summary[0]->Total_Votes) ? $cf_summary[0]->Total_Votes : 0,
    			'contrib_month' => Carbon::parse($checkdate)->format('F, Y'),
    		]
    	];

    	return $summary_data;
    }

    public function employeesHeaderGot()
    {
    	$period='2019-05-31';
    	$pagesize=1000;
    	$request = Request();
    	try{
    		$client = new Client();
    		$request = $client->get('http://172.16.0.32/gsppapirestt/api/employees/getemployeeheader?checkdate='.$period.'&pageSize='.$pagesize);
    		$response = $request->getBody()->getContents();
    		$xml = simplexml_load_string($response);
    		$json = json_decode(json_encode($xml),true);
			// Log::info(print_r($json, true));
    		for ($i=0; $i < $json['TotalPages']; $i++) { 
			// for ($i=0; $i < 2; $i++) { 
    			$this->employeesBioData($period,($i+1),$pagesize);
    		}
    	}
    	catch( ConnectException $e){
    		Log::info('---- Connection error employees-----');
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch (ClientError $e) {
    		Log::info('---------Client error employees-----');
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch (\Error $e) {
    		Log::info('---------General error employees-----');
    		Log::info($e);
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch( \ Exception $e){
    		Log::info('---------Exception error employees -----');
    		Log::info($e);
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}

    }



    public function employeesBioData($period,$pagenumber,$pagesize)
    {
    	$request = Request();
    	try{
    		$client = new Client();
    		$request = $client->get('http://172.16.0.32/gsppapirestt/api/employees/getemployeedetails?checkdate='.$period.'&pageNo='.$pagenumber.'&pageSize='.$pagesize);
    		$response = $request->getBody()->getContents();
    		$xml = simplexml_load_string($response);
    		$json = json_decode(json_encode($xml),true);

    		for ($i=0; $i < $pagesize; $i++) { 
    			$sex = [1,2];
    			$key = array_rand($sex);
    			$emp_cate = [84,85,86];
    			$key_cat = array_rand($emp_cate);

				// Log::info(print_r($json['Employee'][$i],true));

    			$input = [
    				'CheckNumber' => $json['Employee'][$i]['CheckNumber'],
    				'firstname' => is_array($json['Employee'][$i]['FirstName']) ?  null: strtoupper(trim($json['Employee'][$i]['FirstName'])),
    				'middlename' => is_array($json['Employee'][$i]['MiddleName']) ?  null: strtoupper(trim($json['Employee'][$i]['MiddleName'])),
    				'lastname' => is_array($json['Employee'][$i]['LastName']) ?  null: strtoupper(trim($json['Employee'][$i]['LastName'])),
    				'VoteCode' => is_array($json['Employee'][$i]['VoteCode']) ?  null: strtoupper(trim($json['Employee'][$i]['VoteCode'])),
    				'DeptCode' => is_array($json['Employee'][$i]['DeptCode']) ?  null: strtoupper(trim($json['Employee'][$i]['DeptCode'])),
    				'DateHired' => Carbon::parse($json['Employee'][$i]['DateHired'])->format('Y-m-d'),
    				'dob' => Carbon::parse($json['Employee'][$i]['DateHired'])->format('Y-m-d'),
    				'PayGrade' => is_array($json['Employee'][$i]['PayGrade']) ?  null: strtoupper(trim($json['Employee'][$i]['PayGrade'])),
    				'PayStep' => is_array($json['Employee'][$i]['PayStep']) ?  null: strtoupper(trim($json['Employee'][$i]['PayStep'])),
    				'SalaryScale' => is_array($json['Employee'][$i]['SalaryScale']) ?  null: strtoupper(trim($json['Employee'][$i]['SalaryScale'])),
    				'emp_cate' => is_array($json['Employee'][$i]['EmploymentStatus']) ?  null: strtoupper(trim($json['Employee'][$i]['EmploymentStatus'])),
    				'job_title' => is_array($json['Employee'][$i]['Designation']) ?  null: strtoupper(trim($json['Employee'][$i]['Designation'])),
    				'national_id' => null,
    				'basicpay' => null,
    				'grosspay' => null,
    				'sex' => $sex[$key],
    				'employee_category_cv_id' => $emp_cate[$key_cat],
    			];

    			DB::transaction(function () use ($input) {
    				$this->generateMemberNumber($input);
    			});
    		}
    	}
    	catch( ConnectException $e){
    		Log::info('---- Connection error employees details-----');
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch (ClientError $e) {
    		Log::info('---------Client error employees details -----');
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch (\Error $e) {
    		Log::info('---------General error employees details -----');
    		Log::info($e);
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch( \ Exception $e){
    		Log::info('---------Exception error employees details -----');
    		Log::info($e);
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}

    }



    public function generateMemberNumber($input){ 

    	$employee_exist = DB::table('main.employees')->where('firstname', $input['firstname'])
    	->where('middlename',  $input['middlename'])
    	->where('lastname',  $input['lastname'])
    	->where('dob',  $input['dob'])->first(); 

    	if(empty($employee_exist)){
    		$registeredEmployeeId =  DB::table('main.employees')->insertGetId([
    			'firstname' => $input['firstname'],
    			'middlename' => $input['middlename'],
    			'lastname' => $input['lastname'],
    			'dob' => $input['dob'],
    			'gender_id' =>$input['sex'] ,
    			'nid' =>$input['national_id'],
    			'emp_cate' => $input['emp_cate'],
    			'VoteCode' => $input['VoteCode'],
    			'DeptCode' => isset($input['DeptCode']) ?  $input['DeptCode'] : null,
    			'PayGrade' => isset($input['PayGrade']) ?  $input['PayGrade'] : null,
    			'PayStep' => isset($input['PayStep']) ?  $input['PayStep'] : null,
    			'SalaryScale' => isset($input['SalaryScale']) ?  $input['SalaryScale'] : null,
    			'CheckNumber' => $input['CheckNumber'],
    			'memberno' => 0,
    			'created_at' => Carbon::now(),
    		]);

    		$memberno = checksum($registeredEmployeeId, sysdefs()->data()->employee_number_length);
    		DB::table('main.employees')->where('id', $registeredEmployeeId)->update([
    			'memberno' => $memberno,
    			'updated_at' => Carbon::now(),
    		]);

    		$input['memberno'] = $memberno;
    		$input['employee_id'] = $registeredEmployeeId;	

    	} else {
    		$input['memberno'] = $employee_exist->memberno;
    		$input['employee_id'] = $employee_exist->id;
    	}
    	$this->saveEmployeeEmployer($input);


    }


    public function saveEmployeeEmployer($input){
    	$employer = DB::table('main.employers')->where('vote',  $input['VoteCode'])->first();

    	if($employer){
    		$employee_exist = DB::table('main.employee_employer')
    		->where('employer_id', $employer->id)
    		->where('employee_id',  $input['employee_id'])
    		->whereNull('deleted_at')->first();

    		if (empty($employee_exist)) {
    			DB::table('main.employee_employer')
    			->insert([ 
    				'employee_id' =>  $input['employee_id'],
    				'employer_id' =>  $employer->id,
    				'basicpay' => $input['basicpay'],
    				'grosspay' => $input['grosspay'], 
    				'employee_category_cv_id' => $input['employee_category_cv_id'], 
    				'job_title' =>  isset($input['job_title']) ? $input['job_title'] : $input['CheckNumber'], 
    				'updated_at' => Carbon::now(),
    				'created_at' => Carbon::now(),
    			]); 
    		} else {
    			DB::table('main.employee_employer')
    			->where('id',$employee_exist->id)
    			->limit(1)
    			->update([ 
    				'basicpay' => $input['basicpay'],
    				'grosspay' => $input['grosspay'], 
    				'employee_category_cv_id' => $input['employee_category_cv_id'], 
                    'job_title' =>  isset($input['job_title']) ? $input['job_title'] : $input['CheckNumber'], 
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);
    		}
    	}

    }






    public function contributionHeader()
    {
    	dump('start');
    	Log::info('start');
		// die;	
    	$period='2019-04-30';
    	$pagesize=1000;
    	$request = Request();
    	try{
    		$client = new Client();
    		$request = $client->get('http://172.16.0.32/gsppapirestt/api/contributions/getcontributionheader/'.$period.'/'.$pagesize);
    		$response = $request->getBody()->getContents();
    		$xml = simplexml_load_string($response);
    		$json = json_decode(json_encode($xml),true);
    		Log::info(print_r($json, true));
    		dump('save contributions per page');
            die;
    		for ($i=0; $i < $json['TotalPages']; $i++) { 
			// for ($i=445; $i < 446; $i++) { 
			// 	$page = $i+1;
    			Log::info('Page '.$page);
    			$this->contributionHeaderDetails($period,$page,$pagesize);
    		}

    		dump('now saving GF');
    		Log::info('now saving GF');
    		$this->saveContributionSummary($period,'GOVERNMENT FUNDED');
    		dump('now saving CF');
    		Log::info('now saving CF');
    		$this->saveContributionSummary($period,'COUNCIL FUNDED');
    		dump('start saving non gf funded');
    		Log::info('start saving non gf funded');
    		$this->saveNonGfVote($period);
    		Log::info('done');

    	}
    	catch( ConnectException $e){
    		Log::info('---- Connection error header-----');
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch (ClientError $e) {
    		Log::info('---------Client error header-----');
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch (\Error $e) {
    		Log::info('---------General error header-----');
    		Log::info($e);
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch( \ Exception $e){
    		Log::info('---------Exception error header -----');
    		Log::info($e);
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}

    }



    public function contributionHeaderDetails($period,$pagenumber,$pagesize)
    {
    	$request = Request();
    	try{
    		$client = new Client();
    		$request = $client->get('http://172.16.0.32/gsppapirestt/api/contributions/getcontributiondetails?checkDate='.$period.'&pageNo='.$pagenumber.'&pageSize='.$pagesize);
    		$response = $request->getBody()->getContents();
    		$xml = simplexml_load_string($response);
    		$json = json_decode(json_encode($xml),true);
    		Log::info(print_r($json,true));
			// die;
    		for ($i=0; $i < $pagesize; $i++) { 
    			$sex = [1,2];
    			$key = array_rand($sex);
    			$emp_cate = [84,85,86];
    			$key_cat = array_rand($emp_cate);

				// Log::info(print_r($json['Employee'][$i],true));

    			$input = [
    				'CheckNumber' => $json['Employee'][$i]['CheckNumber'],
    				'FirstName' => is_array($json['Employee'][$i]['FirstName']) ?  null: strtoupper(trim($json['Employee'][$i]['FirstName'])),
    				'MiddleName' => !isset($json['Employee'][$i]['MiddleName'])  ?  '': strtoupper(trim($json['Employee'][$i]['MiddleName'])),
    				'LastName' => is_array($json['Employee'][$i]['LastName']) ?  null: strtoupper(trim($json['Employee'][$i]['LastName'])),
    				'VoteCode' => is_array($json['Employee'][$i]['VoteCode']) ?  null: strtoupper(trim($json['Employee'][$i]['VoteCode'])),
    				'VoteName' => is_array($json['Employee'][$i]['VoteName']) ?  null: strtoupper(trim($json['Employee'][$i]['VoteName'])),
    				'MonthlySalary' => is_array($json['Employee'][$i]['MonthlySalary']) ?  null: strtoupper(trim($json['Employee'][$i]['MonthlySalary'])),
    				'EmployeeContribution' => is_array($json['Employee'][$i]['EmployeeContribution']) ?  null: strtoupper(trim($json['Employee'][$i]['EmployeeContribution'])),
    				'EmployerContribution' => is_array($json['Employee'][$i]['EmployerContribution']) ?  null: strtoupper(trim($json['Employee'][$i]['EmployerContribution'])),
    				'FundingSource' => is_array($json['Employee'][$i]['FundingSource']) ?  null: strtoupper(trim($json['Employee'][$i]['FundingSource'])),
    				'checkDate' => $period,
    			];

    			DB::transaction(function () use ($input) {
    				$this->saveContributionDetails($input);
    			});
    		}


    	}
    	catch( ConnectException $e){
    		Log::info('---- Connection error -----');
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch (ClientError $e) {
    		Log::info('---------Client error -----');
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch (\Error $e) {
    		Log::info('---------General error -----');
    		Log::info($e);
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}
    	catch( \ Exception $e){
    		Log::info('---------Exception error -----');
    		Log::info($e);
    		Log::error(print_r($e->getMessage(),true));
    		Log::info('-------------');
    	}

    }



    public function saveContributionDetails($input){ 
		// Log::info(' deails');
    	$detail_exist = DB::table('main.treasury_employees_staging')
    	->where('CheckNumber',  $input['CheckNumber'])
    	->where('checkDate',  $input['checkDate'])
    	->where('VoteCode',  $input['VoteCode'])
    	->first();

    	if (empty($detail_exist)) {
			// Log::info('save emp details');
    		DB::table('main.treasury_employees_staging')->insert([
    			'CheckNumber' => $input['CheckNumber'],
    			'FirstName' => $input['FirstName'],
    			'MiddleName' => $input['MiddleName'],
    			'LastName' => $input['LastName'],
    			'VoteCode' => $input['VoteCode'],
    			'VoteName' => $input['VoteName'],
    			'MonthlySalary' => $input['MonthlySalary'],
    			'EmployeeContribution' => $input['EmployeeContribution'],
    			'EmployerContribution' => $input['EmployerContribution'],
    			'FundingSource' => $input['FundingSource'],
    			'checkDate' => $input['checkDate'],
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now(),
    		]);
    	} else {
    		DB::table('main.treasury_employees_staging')
    		->where('id',  $detail_exist->id)
    		->update([
    			'MonthlySalary' => $input['MonthlySalary'],
    			'EmployeeContribution' => $input['EmployeeContribution'],
    			'EmployerContribution' => $input['EmployerContribution'],
    			'FundingSource' => $input['FundingSource'],
    			'updated_at' => Carbon::now(),
    		]);
    	}

    }



    public function saveContributionSummary($period,$fund){


    	$employees_count = DB::table('main.treasury_employees_staging')
    	->select('checkNumber')
    	->where('checkDate',  $period)
    	->where('FundingSource', $fund)
    	->count();

    	$total_grosspay = DB::table('main.treasury_employees_staging')
    	->select('MonthlySalary')
    	->where('FundingSource', $fund)
    	->where('checkDate',  $period)->sum('MonthlySalary');

    	$total_contribution = DB::table('main.treasury_employees_staging')
    	->select('EmployerContribution')
    	->where('FundingSource', $fund)
    	->where('checkDate',  $period)->sum('EmployerContribution');

    	$vote_count = DB::table('main.treasury_employees_staging')
    	->distinct('VoteCode')
    	->where('FundingSource', $fund)
    	->where('checkDate',  $period)->count('VoteCode');

    	$calculated_contrib = $total_grosspay*0.005;

    	$diff = $calculated_contrib - $total_contribution;


    	if($diff < 1000){
    		if($fund == 'GOVERNMENT FUNDED'){
    			$fund = 'GF';
    		}else{
    			$fund = 'CF';
    		}
    		$summary_exist = DB::table('main.treasury_employees_votes_staging')
    		->where('checkDate',  $period)
    		->where('funding_source', $fund)
    		->first();

    		if(empty($summary_exist)){
    			DB::table('main.treasury_employees_votes_staging')->insert([
    				'Total_Votes' => $vote_count,
    				'Total_Employees' => $employees_count,
    				'Total_amount' => $total_contribution,
    				'checkDate' => $period,
    				'funding_source' => $fund,
    				'created_at' => Carbon::now(),
    				'updated_at' => Carbon::now(),
    			]);
    		}else{
    			DB::table('main.treasury_employees_votes_staging')
    			->where('checkDate',  $period)
    			->where('id',$summary_exist->id)
    			->update([
    				'Total_Votes' => $vote_count,
    				'Total_Employees' => $employees_count,
    				'Total_amount' => $total_contribution,
    				'checkDate' => $period,
    				'funding_source' => $fund,
    				'updated_at' => Carbon::now(),
    			]);
    		}

    		dump('saved summary '.$fund);
			// $this->saveNonGfVote($period);
    	}else{
			//there is a difference of more than 1000
    		dump('diff kubwa');
    	}


    }


    public function saveNonGfVote($period)
    {
    	$votes = DB::table('main.treasury_employees_staging')
    	->distinct('VoteCode')
    	->where('FundingSource', '!=','GOVERNMENT FUNDED')
    	->where('checkDate',  $period)->get();

    	foreach ($votes as $v) {


    		$e_count = DB::table('main.treasury_employees_staging')
    		->select('checkNumber')
    		->where('checkDate',  $period)
    		->where('FundingSource', '!=','GOVERNMENT FUNDED')
    		->where('VoteCode', $v->VoteCode)
    		->count();

    		$t_grosspay = DB::table('main.treasury_employees_staging')
    		->select('MonthlySalary')
    		->where('checkDate',  $period)
    		->where('FundingSource', '!=','GOVERNMENT FUNDED')
    		->where('VoteCode', $v->VoteCode)->sum('MonthlySalary');

    		$t_contribution = DB::table('main.treasury_employees_staging')
    		->select('EmployerContribution')
    		->where('FundingSource', '!=','GOVERNMENT FUNDED')
    		->where('checkDate',  $period)
    		->where('VoteCode', $v->VoteCode)->sum('EmployerContribution');

    		$c_contrib = $t_grosspay*0.005;


    		$summary_exist = DB::table('main.treasury_employees_votes_staging')
    		->where('checkDate',  $period)
    		->where('funding_source', 'CF')
    		->first();
    		dump('now saving vote '.$v->VoteCode.' empcount: '.$e_count.' grosspay '.$t_grosspay.' contribution '.$t_contribution.' vs cal '.$c_contrib);

    		if($summary_exist){
    			$vote_exits = DB::table('main.treasury_funding_votes')
    			->where('summary_id', $summary_exist->id)
    			->where('vote_code', $v->VoteCode)
    			->first();

    			if (empty($vote_exits)) {
    				DB::table('main.treasury_funding_votes')->insert([
    					'vote_code' => $v->VoteCode,
    					'employee_count' => $e_count,
    					'contrib_amount' => $c_contrib,
    					'summary_id' => $summary_exist->id,
    					'created_at' => Carbon::now(),
    					'updated_at' => Carbon::now(),
    				]);
    			} else {
    				DB::table('main.treasury_funding_votes')
    				->where('id',$vote_exits->id)
    				->update([
    					'employee_count' => $e_count,
    					'contrib_amount' => $c_contrib,
    					'updated_at' => Carbon::now(),
    				]);
    			}
    		}
			// }else{
			// 	// $this->saveContributionSummary($period,'non gov');
			// }



    	}

    	dump('non gov funding votes saved');
    }


    public function emailAdminError($period,$page)
    {

    }


    public function generateTreasuryBill(Request $request)
    {

    	DB::transaction(function () use ($request) {
    		$this->saveGfBill($request->contrib_month);
    		$this->saveCfBill($request->contrib_month);



    		$month = Carbon::parse($request->contrib_month)->format('Y-m-28');

    		$treasury_bills = Bill::where('contrib_month',$month)->where('is_treasury',true)->get();

    		if ($treasury_bills->count()) {
    			foreach ($treasury_bills as $t_bill) {
    				$this->requestTreasuryControlNumber($t_bill->bill_no);
    			}
    		}
    	});
    	return response()->json(array('success' => true));
    }


    public function saveGfBill($period)
    {

    	$summary_gf = DB::table('main.treasury_employees_votes_staging')
    	->where('checkDate',  $period)
    	->where('funding_source', 'GF')
    	->first();

    	if ($summary_gf) {
    		$bill_rand_no = mt_rand(1000, 1999);
    		$latest = DB::table('portal.bills')->orderBy('id', 'desc')->limit(1)->get();
    		$latest_bill = $latest[0]->id+1;
    		$bill_no = $bill_rand_no.$latest_bill;
    		$description = 'Contribution';
    		$bill_item = 'Contribution for '.Carbon::parse($period)->format('F Y');

    		$bill = new Bill;
    		$bill->employer_id = 9386;
    		$bill->bill_no= $bill_no;
    		$bill->bill_amount = $summary_gf->Total_amount;
    		$bill->bill_description = $description;
    		$bill->billed_item = $bill_item;
    		$bill->contribution = $summary_gf->Total_amount;
    		$bill->member_count = $summary_gf->Total_Employees;
    		$bill->bill_status = 1;
    		$bill->contrib_month = Carbon::parse($period)->format('Y-m-28');
    		if(Carbon::parse($period)->format('m') == Carbon::parse(Carbon::now())->format('m')){
    			$bill->contribution_due_date = Carbon::now()->addMonth()->endOfMonth()->toDateTimeString();
    			$bill->expire_date = Carbon::parse(Carbon::now())->addMonths(2)->endOfMonth()->toDateTimeString();
    		}else{
    			$bill->contribution_due_date = Carbon::now()->endOfMonth()->toDateTimeString();
    			$bill->expire_date =  Carbon::parse(Carbon::now())->addMonth()->endOfMonth()->toDateTimeString();
    		}
    		$bill->user_id = access()->user()->id;
    		$bill->mobile_number = '255699210954';
    		$bill->bill_source ='MAC';
    		$bill->is_treasury = true;
    		$bill->save();

    		DB::table('main.treasury_employees_votes_staging')
    		->where('checkDate',  $period)
    		->where('funding_source', 'GF')
    		->update([
    			'bill_id' => $bill->bill_no,
    			'updated_at' => Carbon::now(),
    		]);

    	}
    	
    	
    	
    }

    public function saveCfBill($period)
    {

    	$summary_cf = DB::table('main.treasury_employees_votes_staging')
    	->where('checkDate',  $period)
    	->where('funding_source', 'CF')
    	->first();

    	if ($summary_cf) {
    		$votes_cf = DB::table('main.treasury_funding_votes')
    		->where('summary_id',  $summary_cf->id)
    		->get();

    		
    		foreach ($votes_cf as $vote) {
    			$bill_rand_no = mt_rand(1000, 1999);
    			$latest = DB::table('portal.bills')->orderBy('id', 'desc')->limit(1)->get();
    			$latest_bill = $latest[0]->id+1;
    			$bill_no = $bill_rand_no.$latest_bill;
    			$description = 'Contribution';
    			$bill_item = 'Contribution for '.Carbon::parse($period)->format('F Y');

    			$bill = new Bill;
    			// $bill->employer_id = 9386;
    			$bill->bill_no= $bill_no;
    			$bill->bill_amount = $vote->contrib_amount;
    			$bill->bill_description = $description;
    			$bill->billed_item = $bill_item;
    			$bill->contribution = $vote->contrib_amount;
    			$bill->member_count = $vote->employee_count;
    			$bill->bill_status = 1;
    			$bill->contrib_month = Carbon::parse($period)->format('Y-m-28');
    			if(Carbon::parse($period)->format('m') == Carbon::parse(Carbon::now())->format('m')){
    				$bill->contribution_due_date = Carbon::now()->addMonth()->endOfMonth()->toDateTimeString();
    				$bill->expire_date = Carbon::parse(Carbon::now())->addMonths(2)->endOfMonth()->toDateTimeString();
    			}else{
    				$bill->contribution_due_date = Carbon::now()->endOfMonth()->toDateTimeString();
    				$bill->expire_date =  Carbon::parse(Carbon::now())->addMonth()->endOfMonth()->toDateTimeString();
    			}
    			$bill->user_id = access()->user()->id;
    			$bill->mobile_number = '255699210954';
    			$bill->bill_source ='MAC';
    			$bill->is_treasury = true;
    			$bill->vote_code = $vote->vote_code;
    			$bill->save();	

    			DB::table('main.treasury_funding_votes')
    			->where('summary_id',  $summary_cf->id)
    			->where('vote_code',  $vote->vote_code)
    			->update([
    				'bill_id' => $bill->bill_no,
    				'updated_at' => Carbon::now(),
    			]);

    		}
    	}

    }



    public function requestTreasuryControlNumber($bill_no){
    	$bill = Bill::where('bill_no','=',$bill_no)->first();


    	if(!empty($bill->employer_id)){
    		$employer=Employer::where('id','=',$bill->employer_id)->first();
    		$payer_name = $employer->name;
    		$payer_id = $employer->id;
    		$payer_email = $employer->email;
    	}else{
    		
    		$payer_name = 'COUNCIL';
    		$payer_id = $bill->vote_code;
    		$payer_email = 'juma.wahabu@wcf.go.tz';
    	}

    	$end = Carbon::parse($bill->expire_date);
    	$tdy = Carbon::now();
    	$days_expires_after = $end->diffInDays($tdy);


    	$data = [
    		"payment_ref"=> $bill->bill_no,
    		"sub_sp_code"=> 1001,
    		"amount"=> $bill->bill_amount,
    		"desc"=> $bill->bill_description,
    		"gfs_code"=> $bill->gfs_code,
    		"payment_type"=> 1,
    		"payerid"=> $payer_id,
    		"payer_name"=> $payer_name,
    		"payer_cell"=>$bill->mobile_number,
            // "payer_cell"=>str_ireplace(' ','',$employer->phone),
    		"payer_email"=> $payer_email,
    		"days_expires_after"=> $days_expires_after,
    		"generated_by"=> 'WCF',
    		"approved_by"=> 'DG_WCF'
    	];


    	try {
    		$client = new Client();
    		// $response =  $client->request('POST', 'http://gepg.test/bills/post_bill', ['json' => $data]);
    		// $response =  $client->request('POST', 'http://192.168.1.9/api/bills/post_bill', ['json' => $data]);
    		// return $response->getBody();
    	}
    	catch (ClientError $e) {

    		$req = $e->getRequest();
    		$resp =$e->getStatusCode();
    		return $resp;
    	}
    	catch (ServerError $e) {

    		$req = $e->getRequest();
    		$resp =$e->getStatusCode();
    		return $resp;
    	}
    	catch (BadResponse $e) {

    		$req = $e->getRequest();
    		$resp =$e->getStatusCode();
    		return $resp;
    	}
    	catch( \Error $e){
    		Log::info('Error Occured!');
    	}
    	catch( \Exception $e){
    		Log::info('Exception Error Occured!');
    	}


    }

    public function returnBillDatatable($bill_id,$type)
    {

        $b_exist = Bill::where('bill_no',$bill_id)->first();

        if($type == 'GF'){
          $bills = DB::table('portal.bills')
          ->select('*','bills.id as id')
          ->leftjoin('portal.payments', 'bills.bill_no', '=', 'payments.bill_id')
          ->leftjoin('main.employers', 'bills.employer_id', '=', 'employers.id')
          ->where('contrib_month',$b_exist->contrib_month)->where('is_treasury',true)->where('employer_id',9386);
          // Log::info('-----------gf');
      }else{
          $bills = DB::table('portal.bills')
          ->select('*','bills.id as id')
          ->leftjoin('portal.payments', 'bills.bill_no', '=', 'payments.bill_id')
          ->leftjoin('main.employers', 'bills.employer_id', '=', 'employers.id')
          ->where('contrib_month',$b_exist->contrib_month)->where('is_treasury',true)->where('employer_id','!=',9386);
          // Log::info('--------- cf');
          // Log::info(print_r($bills,true));
      }

      return Datatables::of($bills)
      ->editColumn('bill_amount', function($bill) {
        return number_format($bill->bill_amount,2);
    })
      ->addColumn('action', function($bill) {
        if (!is_null($bill->control_no)) {
            return '<a href="'.url('/finance/nmb_transfer/'.$bill->id).'" class="btn btn-md btn-success"><i class="fa fa-download"></i> NMB Form</a> &nbsp; <a href="'.url('/finance/crdb_transfer/'.$bill->id).'" class="btn btn-md btn-success"><i class="fa fa-download"></i> CRDB Form</a> ';
        } else {
            return ' <a  class="btn btn-md btn-warning text-white btnRefreshCn"><i class="fa fa-refresh"></i> Refresh To Get Control No#</a>';
        }

    })
      ->rawColumns(['action'])
      ->make(true);
  }



  public function getEmployeesChanges()
  {
    //personal action
    $period='2017-09-30';
    $pagesize=1000;
    $pagenumber = 1;
    $request = Request();
    try{

        $client = new Client();
        $request = $client->get('http://172.16.0.32/gsppapirestt/api/paactions?checkDate='.$period.'&pageNo='.$pagenumber.'&pageSize='.$pagesize);
        $response = $request->getBody()->getContents();
        $xml = simplexml_load_string($response);
        dump('-------------- start --- --------');
        
        $json = json_decode(json_encode($xml),true);


        // Log::info(print_r($json,true));

        for ($i=0; $i < $pagesize; $i++) { 
            // Log::info('--------Loop '.($i+1).' ------');
            dump('Loop '.($i+1).' starts');

            Log::info(print_r($json['Employee'][$i],true));

            $input = [
                'CheckNumber' => $json['Employee'][$i]['CheckNumber'],
                'firstname' => is_array($json['Employee'][$i]['FirstName']) ?  null: strtoupper(trim($json['Employee'][$i]['FirstName'])),
                'middlename' => (!isset($json['Employee'][$i]['MiddleName'])  ?  '' : (is_array($json['Employee'][$i]['MiddleName']) ? null : strtoupper(trim($json['Employee'][$i]['MiddleName'])))),
                'lastname' => is_array($json['Employee'][$i]['LastName']) ?  null: strtoupper(trim($json['Employee'][$i]['LastName'])),
                'VoteCode' => is_array($json['Employee'][$i]['VoteCode']) ?  null: strtoupper(trim($json['Employee'][$i]['VoteCode'])),
                'VoteName' => is_array($json['Employee'][$i]['VoteName']) ?  null: strtoupper(trim($json['Employee'][$i]['VoteName'])),
                'MonthlySalary' => is_array($json['Employee'][$i]['MonthlySalary']) ?  null: $json['Employee'][$i]['MonthlySalary'],
                'PreviousSalary' => is_array($json['Employee'][$i]['PreviousSalary']) ?  null: $json['Employee'][$i]['PreviousSalary'],
                'SalaryChange' => is_array($json['Employee'][$i]['SalaryChange']) ?  null: $json['Employee'][$i]['SalaryChange'],
                'ActionCategory' => is_array($json['Employee'][$i]['ActionCategory']) ?  null: strtoupper(trim($json['Employee'][$i]['ActionCategory'])),
                'reason' => is_array($json['Employee'][$i]['Reason']) ?  null: trim($json['Employee'][$i]['Reason']),
                'Gender' => is_array($json['Employee'][$i]['Gender']) ?  null: strtoupper(trim($json['Employee'][$i]['Gender'])),
                'sex' =>(!isset($json['Employee'][$i]['Gender'])  ?  0 : (is_array($json['Employee'][$i]['Gender']) ? 0 : strtoupper(trim($json['Employee'][$i]['Gender'] == 'M' ? 1:2)))),
                'dob' => is_array($json['Employee'][$i]['Birthdate']) ?  null: Carbon::parse($json['Employee'][$i]['Birthdate'])->format('Y-m-d'),
                'DeptCode' => is_array($json['Employee'][$i]['DeptCode']) ?  null: strtoupper(trim($json['Employee'][$i]['DeptCode'])),
                'DeptName' => is_array($json['Employee'][$i]['DeptName']) ?  null: trim($json['Employee'][$i]['DeptName']),
                'checkDate' => $period,
                'emp_cate' => 84,
                'PayGrade' => NULL,
                'PayStep' => NULL,
                'SalaryScale' => NULL, 
                'national_id' => (!isset($json['Employee'][$i]['NationalId'])  ?  '' : (is_array($json['Employee'][$i]['NationalId']) ? null : strtoupper(trim($json['Employee'][$i]['NationalId'])))), 
            ];


            $input['basicpay'] = $input['MonthlySalary'];
            $input['grosspay'] = $input['MonthlySalary'];
            $input['employee_category_cv_id'] = $input['emp_cate']; 


            // Log::info(print_r($input,true));
            switch ($input['ActionCategory']) {
                case 'NEW JOINER':
                $this->generateMemberNumber($input);
                break;
                case 'TERMINATION':
                $this->removeEmployee($input);
                break;
                case 'REINSTATED':
                Log::info('reinstated = ='.$input['ActionCategory']);
                break;
                case 'PROMOTION':
                $this->updateEmployeesJobData($input);
                break;
                default:
                break;
            }
            dump('Loop '.($i+1).' ends');
            Log::info('--------Loop '.($i+1).' ends------');
        }
        dump('Done');


    }
    catch( ConnectException $e){
        Log::info('---- Connection error -----');
        Log::error(print_r($e->getMessage(),true));
        Log::info('-------------');
    }
    catch (ClientError $e) {
        Log::info('---------Client error -----');
        Log::error(print_r($e->getMessage(),true));
        Log::info('-------------');
    }
    catch (\Error $e) {
        Log::info('---------General error -----');
        Log::info($e);
        Log::error(print_r($e->getMessage(),true));
        Log::info('-------------');
    }
    catch( \ Exception $e){
        Log::info('---------Exception error -----');
        Log::info($e);
        Log::error(print_r($e->getMessage(),true));
        Log::info('-------------');
    }

}



public function updateEmployeesBioData($input)
{
    //update bio data --> employee table
  DB::table('main.employees')
  ->where('CheckNumber',$input['CheckNumber'])
  ->update([
    'DeptCode'=> $input['DeptCode'],
    'DeptName'=> $input['DeptName'],
    'VoteCode'=> $input['VoteCode'],
    'sex'=> $input['Gender'],
    'gender_id'=> $input['sex'],
    'dob'=> $input['dob'],
    'updated_at' => Carbon::now(),
]);
}


public function updateEmployeesJobData($input)
{
    //update vote/salary  ---> employee employer table
  $employer = Employer::where('vote',$input['VoteCode'])->first();

  $employee = DB::table('main.employees')->where('CheckNumber',$input['CheckNumber'])->first();

  if (!empty($employee)) {
    DB::table('main.employee_employer')
    ->where('employee_id',$employee->id)
    ->where('payroll_id',null)
    ->update([
        'employer_id' => $employer->id,
        'grosspay' => $input['MonthlySalary'],
        'employee_category_cv_id' => $input['emp_cate'],
        'updated_at' => Carbon::now(),
    ]); 
}

$this->updateEmployeesBioData($input);


}



public function removeEmployee($input)
{
    //deleted_at and reason  --> employee_employer_table

    $employer = Employer::where('vote',$input['VoteCode'])->first();

    $employee = DB::table('main.employees')->where('CheckNumber',$input['CheckNumber'])->first();

    if (!empty($employee)) {
     if (!empty($employer)) {
      DB::table('main.employee_employer')
      ->where('employee_id',$employee->id)
      ->where('employer_id',$employer->id)
      ->update([
        'deleted_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'remove_reason' => $input['reason'],
    ]);
  }else{
    DB::table('main.employee_employer')
    ->where('employee_id',$employee->id)
    ->where('payroll_id',null)
    ->update([
        'deleted_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'remove_reason' => $input['reason'],
    ]); 
}
} 


}


public function updateEmployer_id()
{
    $bills = DB::table('portal.bills')->where('is_treasury',true)->where('employer_id',null)->get();

    foreach ($bills as $b) {
      $employer = Employer::where('vote',$b->vote_code)->first();
      if(!empty($employer)){
          DB::table('portal.bills')->where('vote_code',$b->vote_code)->update([
            'employer_id' => $employer->id,
        ]);
      }

  }
  dump('done');

}



}
