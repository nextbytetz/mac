<?php


namespace App\Http\Controllers\Backend\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Organization\FiscalYearRequest;
use App\Models\Organization\FiscalYear;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class FiscalYearController extends Controller
{


    protected $fiscal_years;


    public function __construct(){
        $this->fiscal_years = new FiscalYearRepository();


        $this->middleware('access.routeNeedsPermission:manage_annual_target', []);

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend.organization.fiscal_year.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $codeValue = new CodeValueRepository();
        $code_values = $codeValue->query()->where('code_id', 11)->get();
        return view('backend.organization.fiscal_year.create')
            ->withAnnualTargetTypes($code_values);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FiscalYearRequest $request)
    {
        //
        $this->fiscal_years->create($request->all());
        return redirect()->route('backend.organization.fiscal_year.index')->withFlashSuccess(trans('alerts.backend.organization.fiscal_year_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $fiscal_year = $this->fiscal_years->findOrThrowException($id);
        $codeValue = new CodeValueRepository();
        $code_values = $codeValue->query()->where('code_id', 11)->get();
        return view('backend.organization.fiscal_year.edit')
            ->withAnnualTargetTypes($code_values)
            ->withFiscalYear($fiscal_year);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FiscalYearRequest $request, $id)
    {
        //
        $this->fiscal_years->edit($id,$request->all());
        return redirect()->route('backend.organization.fiscal_year.index')->withFlashSuccess(trans('alerts.backend.organization.fiscal_year_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /*
 * Get for dataTable
 */
    public function get() {

        return Datatables::of($this->fiscal_years->getForDataTable())
                ->make(true);
    }



}
