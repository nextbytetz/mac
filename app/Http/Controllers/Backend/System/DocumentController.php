<?php

namespace App\Http\Controllers\Backend\System;

use App\Http\Requests\Backend\System\AttachDocRequest;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\DocumentResourceRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class DocumentController
 * @package App\Http\Controllers\Backend\System
 */
class DocumentController extends Controller
{
    public $resource;

    /**
     * DocumentController constructor.
     * @param DocumentResourceRepository $resource
     */
    public function __construct(DocumentResourceRepository $resource)
    {
        $this->resource = $resource;
    }

    /**
     * @param $resourceid
     * @param $reference
     * @param int $url_selector
     * @return mixed
     */
    public function attach($resourceid, $reference, $url_selector = 0)
    {
        $documentRepo = new DocumentRepository();

        $documents = $documentRepo->query()->whereIn('document_group_id', [$this->resource->getDocumentGroup($reference)])->where("ismandatory", 1)->pluck('name', 'id')->all();
        $resource = $this->resource->getResource($resourceid, $reference);

        return view('backend/system/document/attach_doc')
            ->with('resource',  $resource)
            ->with("reference", $reference)
            ->with("data", $this->resource->getResourceData($resource, $reference, $url_selector))
            ->with('documents',  $documents);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkIfOthers()
    {
        $document_id = request()->input("document_id");
        $docRepo = new DocumentRepository();
        if ($docRepo->checkIfOthers($document_id)) {
            $success = '1';
        } else {
            $success = '0';
        }
        return response()->json(['success' => $success]);
    }

    /**
     * @param $doc_resource_id
     * @param $reference
     * @return mixed
     */
    public function edit($doc_resource_id, $reference)
    {
        $uploaded_doc = $this->resource->find($doc_resource_id);
        $resource = $uploaded_doc->resource;
        $document = $uploaded_doc->document;
        return view('backend/system/document/edit_doc')
            ->with('resource',  $resource)
            ->with("reference", $reference)
            ->with("data", $this->resource->getResourceData($resource, $reference))
            ->with("doc_resource", $uploaded_doc)
            ->with('uploaded_document',  $document);
    }

    /**
     * @param $doc_resource_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function preview($doc_resource_id)
    {
        $uploaded_doc = $this->resource->find($doc_resource_id);
        if (test_uri()) {
            $url = url("/") . "/ViewerJS/#../storage/document_resource/{$uploaded_doc->id}.{$uploaded_doc->ext}";
        } else {
            $url = url("/") ."/public/ViewerJS/#../storage/document_resource/{$uploaded_doc->id}.{$uploaded_doc->ext}";
        }
        //$url = $employerTask->getEmployerTaskPathFileUrl($doc_pivot_id);
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded_doc->description, "id" => $doc_resource_id]);
    }

    /**
     * @param $doc_resource_id
     * @param $reference
     * @return mixed
     */
    public function delete($doc_resource_id, $reference)
    {
        $uploaded_doc = $this->resource->find($doc_resource_id);
        $resource = $uploaded_doc->resource;
        $data = $this->resource->getResourceData($resource, $reference);
        $this->resource->delete($doc_resource_id);
        $return = $data['return_url'];
        return redirect($return)->withFlashSuccess('Success, Document Detached');
    }

    /**
     * @param AttachDocRequest $request
     * @return mixed
     */
    public function store(AttachDocRequest $request)
    {
        $input = $request->all();
        $this->resource->store($input);
        $return_page = isset($input['return_page']) ? 1 : 0;
        if ($return_page) {
            $return = $input['url'];
        } else {
            $return = $input['return_url'];
        }
        return redirect($return)->withFlashSuccess('Success, Document attached');
    }

    /**
     * @param AttachDocRequest $request
     * @return mixed
     */
    public function update(AttachDocRequest $request)
    {
        $input = $request->all();
        $this->resource->update($input);
        return redirect($input['return_url'])->withFlashSuccess('Success, Document attached');
    }


    /*General document handling methods*/

    /*Attach document general*/
    public function attachDocumentGen($resource_id, $reference)
    {
        $resource = $this->resource->getResource($resource_id, $reference);
        $data = $this->resource->getResourceData($resource, $reference);
        $resource_info = [
            'return_url' => $data['return_url'],
            'get_resource_method' => $data['get_resource_method'],
            'resource_id' => $data['resource_id_doc'],
            'resource_key_name' =>isset( $data['resource_key_name']) ?  $data['resource_key_name'] : 'resource_id',
            'doc_table' => $data['doc_table'],
            'external_id' => (isset($data['external_id'])) ? $data['external_id'] : null,
        ];

        return view('backend/system/document/general/attach_doc_gen')
            ->with("data",$data )
            ->with("resource_info",$resource_info );

    }

    /**
     * @param AttachDocRequest $request
     * @return mixed
     */
    public function storeDocumentGen(AttachDocRequest $request)
    {

        $input = $request->all();
        $this->resource->storeGeneral($input);
        $return_page = isset($input['return_page']) ? 1 : 0;
        if ($return_page) {
            return redirect()->back()->withFlashSucess('Success, Document attached');
        } else {
            return redirect($input['return_url'])->withFlashSuccess('Success, Document attached');
        }
    }


    /*Edit document general*/
    public function editDocumentGen($doc_pivot_id, $resource_id, $reference)
    {

        $resource = $this->resource->getResource($resource_id, $reference);
        $data = $this->resource->getResourceData($resource, $reference);
        $doc_table = isset($data['doc_table']) ? $data['doc_table'] : 'document_resource';
        $doc_resource = DB::table($doc_table)->where('id', $doc_pivot_id)->first();
        $document_id = $doc_resource->document_id;
        $document =(new DocumentRepository())->find($document_id);
        $resource_info = [
            'return_url' => $data['return_url'],
            'get_resource_method' => $data['get_resource_method'],
            'resource_id' => $resource_id,
            'resource_key_name' =>isset( $data['resource_key_name']) ?  $data['resource_key_name'] : 'resource_id',
            'doc_table' => $data['doc_table'],
            'document_id' => $doc_resource->document_id,
            'doc_pivot_id' => $doc_pivot_id,
            'reference' => $reference
        ];

        return view('backend/system/document/general/edit_doc_gen')
            ->with("data",$data)
            ->with("resource_info",$resource_info)
            ->with("doc_resource",$doc_resource)
            ->with("document",$document);

    }



    /**
     * @param AttachDocRequest $request
     * @return mixed
     */
    public function updateDocumentGen(AttachDocRequest $request)
    {

        $input = $request->all();
        $this->resource->updateGeneral($input);
        return redirect($input['return_url'])->withFlashSuccess('Success, Document updated');
    }

    /**
     * @param $doc_resource_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function previewDocGen($doc_pivot_id, $resource_id, $reference)
    {
        $resource = $this->resource->getResource($resource_id, $reference);
        $data = $this->resource->getResourceData($resource, $reference);
        $doc_table = isset($data['doc_table']) ? $data['doc_table'] : 'document_resource';
        $doc_pivot = DB::table($doc_table)->where('id', $doc_pivot_id)->first();
        $get_resource_method = isset($data['get_resource_method']) ? $data['get_resource_method'] : 1;
        $resource_id_doc = $data['resource_id_doc'];
        $resource_info = $this->resource->getResourceInfoGeneral($resource_id_doc,$doc_pivot->document_id, $get_resource_method);
        $filename =$doc_pivot_id .  '.' . $doc_pivot->ext;

        $path = base_storage_path() . DIRECTORY_SEPARATOR . $resource_info['top_path'] . DIRECTORY_SEPARATOR . $resource_id_doc . DIRECTORY_SEPARATOR .  $filename ;
        return response()->json(['success' => true, "url" => $path, "name" => $doc_pivot->description, "id" => $doc_pivot_id]);
    }



    /*Delete document General*/
    public function deleteDocumentGen($doc_pivot_id, $resource_id, $reference)
    {

$return_url = $this->resource->deleteDocumentGen($doc_pivot_id, $resource_id, $reference);
        return redirect($return_url)->withFlashSuccess('Success, Document deleted');

    }
}
