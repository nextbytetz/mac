<?php

namespace App\Http\Controllers\Backend\System;


use App\DataTables\Sysdef\GetCodesDataTable;
use App\DataTables\Sysdef\GetCodeValuesDataTable;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\System\CreateCodeRequest;
use App\Http\Requests\Backend\System\CreateCodeValueRequest;
use App\Repositories\Backend\Sysdef\CodeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Http\Request;

class CodeController extends Controller
{

    protected $codes;
    protected $code_values;

    public function __construct( ) {
        $this->codes = new CodeRepository();
        $this->code_values = new CodeValueRepository();


    }

    /**
     * Display a listing of the resource.
     * @param GetCodesDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(GetCodesDataTable $dataTable)
    {
        return  $dataTable->render('backend/system/code/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend/system/code/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCodeRequest $request)
    {
        //
        $code = $this->codes->create($request->all());
        return redirect()->route('backend.system.code.index')->withFlashSuccess(trans('alerts.backend.system.code_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $code = $this->codes->findOrThrowException($id);
        return view('backend/system/code/edit')
            ->withCode($code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id, CreateCodeRequest $request)
    {
        $code = $this->codes->update($id,$request->all());
        return redirect()->route('backend.system.code.index')->withFlashSuccess(trans('alerts.backend.system.code_updated'));
    }

    /**
     * @param $id
     * @param GetCodeValuesDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     * Get code values for selected code
     */

    public function getCodeValues($id, GetCodeValuesDataTable $dataTable)
    {
        $code = $this->codes->findOrThrowException($id);
        return  $dataTable->with('code_id',$code->id)->render('backend/system/code/get_code_values',['code'=>$code]);

    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    /**
     * CODE VALUE METHODS ===================
     */
    // create
    public function codeValueCreate($id)
    {
        //
        $code = $this->codes->findOrThrowException($id);
        return view('backend/system/code/code_value_create')
            ->withCode($code);

    }
// store
    public function codeValueStore(CreateCodeValueRequest $request)
    {
        //
        $code_value = $this->code_values->create($request->all());
        return redirect()->route('backend.system.code.index')->withFlashSuccess(trans('alerts.backend.system.code_value_created'));
    }
    // edit
    public function codeValueEdit($code_value_id)
    {
        //
        $code_value = $this->code_values->findOrThrowException($code_value_id);
        return view('backend/system/code/code_value_edit')
            ->withCodeValue($code_value);
    }

    // update
    public function codeValueUpdate($code_value_id, CreateCodeValueRequest $request)
    {
        //
        $code_value = $this->code_values->update($code_value_id,$request->all());
        return redirect()->route('backend.system.code.index')->withFlashSuccess(trans('alerts.backend.system.code_value_updated'));
    }

    // delete
    public function codeValueDelete($code_value_id)
    {
        //
        try{
            $code_value = $this->code_values->findOrThrowException($code_value_id);
            $code_value->destroy($code_value_id);
            return redirect()->route('backend.system.code.index')->withFlashSuccess(trans('alerts.backend.system.code_value_deleted'));

        } catch ( \Illuminate\Database\QueryException $e) {
            check_foreign_key_constraint_fails($e->errorInfo[0]);
        }


    }

    //--end Code Values-----------

}
