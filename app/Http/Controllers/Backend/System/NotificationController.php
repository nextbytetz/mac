<?php

namespace App\Http\Controllers\Backend\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        return view("backend/system/notification/index")
            ->withNotifications(access()->user()->notifications()->offset(0)->limit(10)->get())
            ->withNextOffset(10);
    }

    public function fetchNext($offset)
    {
        return view("backend/system/notification/next")
            ->withNotifications(access()->user()->notifications()->offset($offset)->limit(10)->get())
            ->withNextOffset($offset + 10);
    }

    public function read($id)
    {
        $user = access()->user();
        $entity = $user->unreadNotifications()->where("id", $id)->first();
        if ($entity) {
            $entity->markAsRead();
        }
        return response()->json(['success' => true]);
    }

    public function get()
    {

    }

}
