<?php

namespace App\Http\Controllers\Backend\System;

use App\DataTables\WorkflowTrackDataTable;
use App\Exceptions\GeneralException;
use App\Http\Requests\Backend\Notification\CreateLetterRequest;
use App\Http\Requests\Backend\Notification\UpdateLetterDispatchRequest;
use App\Models\Notifications\Letter;
use App\Models\Operation\Claim\NotificationReport;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\DocumentResourceRepository;
use App\Repositories\Backend\Operation\Claim\NotificationLetterRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

/**
 * Class LetterController
 * @package App\Http\Controllers\Backend\System
 * @author Erick M. Chrysostom
 * @description Manages all letters to employer from different modules
 */
class LetterController extends Controller
{
    /**
     * @var LetterRepository
     */
    private $letters;

    /**
     * LetterController constructor.
     * @param LetterRepository $letters
     */
    public function __construct(LetterRepository $letters)
    {
        $this->letters = $letters;
    }

    /**
     * @param $resource
     * @param $reference
     * @param null $option
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process($resource, $reference, $option = NULL)
    {
        $codeValueRepo = new CodeValueRepository();
        $codeValue = $codeValueRepo->query()->where("reference", $reference)->first();
        $checkLegacy = 0;
        $prevUrl = url()->previous();

        //==> Check if letter had already been created, if yes open letter dashboard ...
        $query = $this->letters->checkIfExists($resource, $reference);
        $count_existing = $query->count();
        if ($count_existing) {
            //check for recurring letters ...
            if ($this->checkHasRecurring($reference)) {
                return redirect()->route("backend.letter.confirm", ['resource' => $resource, 'reference' => $reference])->with('prevurl', $prevUrl);
            } else {
                $letter = $query->first();
                return redirect()->route("backend.letter.show", $letter->id)->with('prevurl', $prevUrl);
            }
        }
        //==> Check if letter had been created before this letter module ...
        switch ($reference) {
            case 'CLACKNOWLGMNT':
                //Claim Acknowledgement Letter
                $checkLegacy = $this->processCLACKNOWLGMNT($resource);
                break;
        }
        if ($checkLegacy) {
            //Confirm Re-Create ...
            return redirect()->route("backend.letter.confirm", ['resource' => $resource, 'reference' => $reference])->with('prevurl', $prevUrl);
        }

        //Create
        return redirect()->route('backend.letter.create', ['resource' => $resource, 'reference' => $reference, 'option' => $option])->with('prevurl', $prevUrl);

    }

    /**
     * @param $reference
     * @return bool
     */
    public function checkHasRecurring($reference) {
        $return = false;
        switch ($reference) {
            case 'CLRMNDRLETTER':
            case 'CLEMPRMDLTR':
                //Notification Reminder Letter ...
                //Employer Reminder Letter (Inspection) ...
                $return = true;
                break;
        }
        return $return;
    }

    /**
     * @param $code_value
     * @param $resource
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function preview(Letter $letter)
    {
        //TODO: Write here code to enable preview of a letter
        $codeValueRepo = new CodeValueRepository();
        $codeValue = $codeValueRepo->query()->select(['reference'])->where("id", $letter->cv_id)->first();
        $reference = $codeValue->reference;
        $func = "preview" . $reference;
        $return = $this->letters->$func($letter);
        return $return;
    }

    /**
     * @param Letter $letter
     * @return |null
     */
    public function previewLegacy($reference, $resource)
    {
        $oldletter = NULL;
        //$codeValueRepo = new CodeValueRepository();
        switch ($reference) {
            case 'CLACKNOWLGMNT':
                //Claim Acknowledgement Letter ...
                $replacers = [
                    '/mac/public/template/assets/nextbyte/img/wcf_big_logo_no_background.png',
                    '/mac/public/template/assets/nextbyte/img/rehema_kabongo_signature.png'
                ];
                $oldletterquery = (new NotificationLetterRepository())->query()->where('notification_report_id', $resource)->first();
                $oldletter = $oldletterquery->acknowledgement;
                foreach ($replacers as $replacer) {
                    $oldletter = str_replace($replacer, public_url() . str_replace('/mac/public', '', $replacer), $oldletter );
                }
                break;
        }
        return $oldletter;
    }

    public function livePrint(Letter $letter)
    {
        $codeValueRepo = new CodeValueRepository();
        $codeValue = $codeValueRepo->query()->select(['reference'])->where("id", $letter->cv_id)->first();
        $reference = $codeValue->reference;
        $func = "preview" . $reference;
        $return = $this->letters->$func($letter, 1);
        return $return;
    }

    /**
     * @param Letter $letter
     * @return mixed
     */
    public function makePdf(Letter $letter)
    {
        $success = $this->letters->generatePdf($letter, 1);
        return redirect()->back()->withFlashSuccess('Success, Pdf format has been generated.');
    }

    public function open(Letter $letter)
    {

    }

    /**
     * @param Letter $letter
     * @param WorkflowTrackDataTable $workflowtrack
     * @return mixed
     */
    public function show(Letter $letter, WorkflowTrackDataTable $workflowtrack)
    {
        $codeValueRepo = new CodeValueRepository();
        $hasold = 0;
        $oldletterhtml = NULL;
        $wfmodule = $letter->wfModule;
        $docs_attached = (new DocumentResourceRepository())->getDocsAttachedNonRecurring($letter, "DRLETTER");
        //dd($wfmodule);
        $return = view("backend/letters/show")
                    ->with("letter", $letter)
                    ->with("workflowtrack", $workflowtrack)
                    ->with("wfmodule", $wfmodule)
                    ->with("docs_attached", $docs_attached);
                    //->with("hasold", $hasold)
                    //->with("oldletterhtml", $oldletterhtml);
        return $return;
    }

    public function edit(Letter $letter)
    {
        $letterInfo = $this->getResourceInfoForCreate($letter->resource_id, $letter->codeValue->reference, $letter->option);
        return view('backend/letters/edit')
                ->with('letter', $letter)
                ->with('letter_info', $letterInfo);
    }

    public function update(Letter $letter, CreateLetterRequest $request)
    {
        $input = $request->all();
        $letter = $this->letters->update($letter, $input);
        return response()->json(['success' => true, 'id' => $letter->id]);
    }

    public function updateDispatch(Letter $letter, UpdateLetterDispatchRequest $request)
    {
        $input = $request->all();
        $return = $this->letters->updateDispatch($letter, $input);
        return response()->json(['success' => true, 'id' => $letter->id]);
    }

    public function confirm($resource, $reference)
    {
        $codeValueRepo = new CodeValueRepository();
        $codeValue = $codeValueRepo->query()->where("reference", $reference)->first();
        $prevurl = session('prevurl');
        $letters = collect([]);
        $hasold = 0;
        //check if recurring & needs to create new ...
        //start: check for recurring letters (retrieve all previous letters) ...
        if ($this->checkHasRecurring($reference)) {
            $letters = $this->letters->getPrevLetters($resource, $reference);
        }
        //end: check for recurring letters ...
        //start : Support for legacy letter design
        switch ($reference) {
            case 'CLACKNOWLGMNT':
                //Claim Acknowledgement Letter ...
                $oldCheck = (new NotificationLetterRepository())->query()->where('notification_report_id', $resource);
                if ($oldCheck->count()) {
                    $hasold = 1;
                    //$oldletterhtml = $oldCheck->first()->acknowledgement;
                };
                break;
        }
        //end : Support for legacy letter design
        $letterInfo = $this->getResourceInfoForCreate($resource, $reference);
        return view("backend/letters/create_confirm")
                ->with("resource", $resource)
                ->with("reference", $reference)
                ->with("code_value", $codeValue)
                ->with("letterscount", $letters->count())
                ->with("prevurl", $prevurl)
                ->with("letter_info", $letterInfo)
                ->with("hasold", $hasold)
                ->with("letters", $letters);
    }

    public function create($resource, $reference, $option = NULL)
    {
        //logger($option);
        $codeValueRepo = new CodeValueRepository();
        $codeValue = $codeValueRepo->query()->where("reference", $reference)->first();
        $prevurl = session('prevurl');
        $letterInfo = $this->getResourceInfoForCreate($resource, $reference, $option);
        return view("backend/letters/create")
                ->with("resource", $resource)
                ->with("code_value", $codeValue)
                ->with("prevurl", $prevurl)
                ->with("letter_info", $letterInfo);
    }

    public function store($resource, $reference, CreateLetterRequest $request)
    {
             $input = $request->all();

        $letter = $this->letters->store($resource, $reference, $input);
              return response()->json(['success' => true, 'id' => $letter->id]);
    }

    public function printed(Letter $letter)
    {
        $codeValueRepo = new CodeValueRepository();
        $letter->isprinted = 1;
        $letter->save();
        //Create Letter Log
        $letter->logs()->create([
            'cv_id' => $codeValueRepo->OLLLTRPRNTD(),
            'user_id' => access()->id(),
        ]);
        return redirect()->back()->withFlashSuccess("Success, this letter has been marked printed.");
    }

    /**
     * @param $resource
     * @return mixed
     */
    public function processCLACKNOWLGMNT($resource)
    {
        //create Claim Acknowledgement Letter
        //==> Check if letter had already been created , especially for legacy notification had been produced this letter..., if yes redirect to dialog box to confirm re-creation...
        $incident = (new NotificationReportRepository())->find($resource);
        $count = $incident->letter()->count();
        return $count;
    }

    /**
     * @param $resource
     * @param $reference
     * @param $option
     * @return array
     */
    public function getResourceInfoForCreate($resource, $reference, $option = NULL)
    {
        $codeValueRepo = new CodeValueRepository();
        $codeValue = $codeValueRepo->query()->where("reference", $reference)->first();
        $reference = $codeValue->reference;
        $func = 'resource' . $reference;
        $output = $this->letters->$func($resource, $option);
/*        switch ($codeValue->id) {
            case $codeValueRepo->CLACKNOWLGMNT():
                //Claim Acknowledgement Letter
                $resource = $this->letters->resourceCLACKNOWLGMNT($resource);
                break;
            case $codeValueRepo->CLBNAWLTTR():
                //Benefit Award Letter
                $resource = $this->letters->resourceCLBNAWLTTR($resource);
                break;
        }*/
        return [
            'name' => $output['name'],
            'salutation' => $output['salutation'],
            'reference' => $output['reference'],
            'recipient' => $output['recipient'],
            'box_no' => $output['box_no'],
            'location' => $output['location'],
            'profile_url' => $output['profile_url'],
            'option' => (isset($output['option'])) ? $output['option'] : '',
            'resources' => $output['resources'],
            'extras' => $output['extras'],
        ];
    }

}
