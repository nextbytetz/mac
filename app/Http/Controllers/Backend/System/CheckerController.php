<?php

namespace App\Http\Controllers\Backend\System;

use App\Models\Sysdef\Code;
use App\Repositories\Backend\Operation\Claim\BenefitTypeClaimRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class CheckerController
 * @package App\Http\Controllers\Backend\System
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 */
class CheckerController extends Controller
{
    /**
     * @var CheckerRepository
     */
    protected $checkers;

    /**
     * CheckerController constructor.
     * @param CheckerRepository $checker
     */
    public function __construct(CheckerRepository $checker)
    {
        $this->checkers = $checker;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = (new CodeValueRepository())->getPendingCheckerCategories();
        $return = view("backend.system.checker.index")->with("categories", $categories);
        foreach ($categories as $category) {
            switch ($category->reference) {

                case "CHCNOTN":
                    //Notification & Claim
                    $return = $return->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all())
                                    ->with("benefit_ready_counts", $this->checkers->getBenefitReadyActiveUser()) //
                                    ->with("ready_to_initiate_counts", $this->checkers->getReadyToInitiateActiveUser())
                                    ->with("ready_to_initiate", $this->checkers->getReadyToInitiate())
                                    ->with("overview_counts", $this->checkers->getClaimChecklistOverviewActiveUser())
                                    ->with("overviews", $this->checkers->getClaimChecklistOverview())
                                    ->with("benefit_ready", (new BenefitTypeClaimRepository())->getClaimBenefits()->pluck("name", "id")->all()) //
                                    ->with('stage_counts', $this->checkers->getAllocatedActiveUser('CHCNOTN'))
                                    ->with("stages", (new CodeValueRepository())->getCodeValuesNotificationStageForSelect());
                    break;
                case "CCAEMPLYARRER":
                    //Employer Arrears

                    break;

                case "CCAEMPLYINSPCTN":
                    //Inspection Plan
                    $return = $return->with("stage_counts_inspection", $this->checkers->getAllocatedActiveUser("CCAEMPLYINSPCTN"))
                                    ->with("stages_inspection", (new CodeValueRepository())->getCodeValuesInspectionStageForSelect());
                    break;
                case "CCAEMPINSPCTNTSK":
                    //Employer Inspection Task
                    $return = $return->with("stage_counts_employer_task", $this->checkers->getAllocatedActiveUser("CCAEMPINSPCTNTSK"))
                                    ->with("stages_employer_task", (new CodeValueRepository())->getCodeValuesEmployerInspectionStageForSelect());
                    break;
                case "CCALETTER":
                    //Letter
                    $return = $return->with("stage_counts_letter", $this->checkers->getAllocatedActiveUser("CCALETTER"))
                        ->with("stages_letter", (new CodeValueRepository())->getCodeValuesStageForSelect([40]));
                    break;

                case "CCONTFCAPPCTN":
                    //Online Notification Application
                    $return = $return->with("stage_counts_online_notification_application", $this->checkers->getAllocatedActiveUser("CCONTFCAPPCTN"))
                                ->with("stages_online_notification_application", (new CodeValuePortalRepository())->getCodeValuesStageForSelect([3]));
                    break;



                case "CHCEMPDEREG":
                    //Employer De-registration
                    $return = $return->with("stage_counts_employer_deregistration", $this->checkers->getAllocatedActiveUser("CHCEMPDEREG"))
                        ->with("stages_employer_deregistration", (new CodeValueRepository())->getCodeValuesStageForSelect([63]));
                    break;

                case "CHCPAYALERT":
                    //Payroll Alert Tasks
                    $return = $return->with("stage_counts_payroll_alert", $this->checkers->getAllocatedActiveUser("CHCPAYALERT"))
                        ->with("stages_payroll_alert", (new CodeValueRepository())->getCodeValuesStageForSelect([64]))
                        ->with('cv_repo', (new CodeValueRepository()));
                    break;

                     case "CHACCRCLM":
                    //Accrual Claim
                      
                    break;
                      case "ARREARCMT":
                    //Arrear Commitment Options
                     ;
                      case "INSTMOUUPLOAD":
                    //Arrear Instalment MoU Upload
                     ;
                      
                    break;
            }
        }
        return $return;
    }

    /**
     * @param $code_value_id
     * @return mixed
     */
    public function getForDatatable($code_value_id)
    {
        $datatables = $this->checkers->getForDatatable($code_value_id);
        return $datatables->make(true);
    }


}
