<?php

namespace App\Http\Controllers\Backend\System;

use App\Models\Finance\Receipt\ReceiptRefund;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FactoryController extends Controller
{
    public function __invoke()
    {
        factory(ReceiptRefund::class, 10)->create();
        return "Success";
    }
}
