<?php

namespace App\Http\Controllers\Backend\System;

use App\DataTables\WorkflowTrackDataTable;
use App\Exceptions\GeneralException;
use App\Exceptions\WorkflowException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\System\UpdateArchiveWorkflowRequest;
use App\Http\Requests\Backend\System\UpdateWorkflowRequest;
use App\Models\Workflow\WfArchive;
use App\Models\Workflow\WfTrack;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use App\Repositories\Backend\Workflow\WfArchiveReasonCodeRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repositories\Backend\Workflow\WfModuleGroupRepository;
use App\Repositories\Backend\Workflow\WfDefinitionRepository;
use App\Repositories\Backend\Access\UserRepository;
use App\Models\Workflow\WfDefinition;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;
use App\DataTables\Sysdef\UsersReviewDataTable;

class WorkflowController extends Controller
{

    /**
     * @var
     */
    protected $moduleGroup;

    /**
     * @var WfDefinition
     */
    protected $definitions;

    /**
     * @var wf tracks
     */
    protected $wf_tracks;

    /**
     * @var
     */
    protected $users;

    /**
     * workflowController constructor.
     * @param WfModuleGroupRepository $moduleGroup
     * @param WfDefinitionRepository $definitions
     * @param UserRepository $users
     */
    public function __construct(WfModuleGroupRepository $moduleGroup, WfDefinitionRepository $definitions, UserRepository $users)
    {
        /* $this->middleware('access.routeNeedsPermission:assign_workflows'); */
        $this->moduleGroup = $moduleGroup;
        $this->definitions = $definitions;
        $this->users = $users;
        $this->wf_tracks = new WfTrackRepository();
        //allocation
        $this->middleware('access.routeNeedsPermission:workflow_allocation', ['only' => ['assignAllocation']]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function defaults()
    {
        return view('backend.system.workflow.defaults')
        ->with("groups", $this->moduleGroup->getAll())
        ->with("users", $this->users->getAll());
    }

    /**
     * @param WfDefinition $definition (Workflow definition id)
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers(WfDefinition $definition)
    {
        return response()
        ->json($definition->users->pluck("id")->all());
    }

    public function updateDefinitionUsers(WfDefinition $definition)
    {
        $this->definitions->updateDefinitionUsers($definition, ['users' => request()->input('users')]);
        return response()->json(['success' => true]);
    }

    /**
     * @param WfArchive $wf_archive
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws GeneralException
     */
    public function editArchiveWorkflow(WfArchive $wf_archive)
    {
        $wfArchiveReasonCodeRepo = new WfArchiveReasonCodeRepository();
        $wf_track = $wf_archive->wfTrack;
        $module = $wf_track->wfDefinition->wfModule;
        $action = 1;
        $workflow = new Workflow(['wf_module_group_id' => $module->wf_module_group_id, 'resource_id' => $$wf_track->resource_id, 'type' => $module->type]);
        access()->hasWorkflowModuleDefinition($module->wf_module_group_id, $module->type, $workflow->currentLevel());
        $record_users = (new UserRepository())->query()->where('unit_id', 16)->get()->pluck('name', 'id')->all();
        $wfArchiveReasonCode = $wfArchiveReasonCodeRepo->query()->where('wf_module_group_id', $module->wf_module_group_id)->first();
        if ($wfArchiveReasonCode) {
            $archive_reasons = $wfArchiveReasonCode->reasons()->pluck("name", "id")->all();
        } else {
            $archive_reasons = [];
        }
        $prevUrl = url()->previous();
        return view('backend/system/workflow/edit_archive')
            ->with('wf_track', $wf_track)
            ->with('wf_archive', $wf_archive)
            ->with('prevurl', $prevUrl)
            ->with('record_users', $record_users)
            ->with('action', $action)
            ->with('archive_reasons', $archive_reasons);
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @param $type
     * @param $action
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws GeneralException
     */
    public function archiveWorkflow($resource_id, $wf_module_group_id, $type, $action)
    {
        $wfModule = new WfModuleRepository();
        $wfArchiveReasonCodeRepo = new WfArchiveReasonCodeRepository();
        $module = $wfModule->getModule(['wf_module_group_id' => $wf_module_group_id, 'type' => $type]);

        if ($action) {
            $status = 0;
        } else {
            $status = 6;
        }
        $wftrackModel = $this->wf_tracks->getLastWorkflowModel($resource_id, $module, $status);
        if (!$wftrackModel->count()) {
            throw new GeneralException('There is no any pending level for archive process in this workflow');
        }
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id, 'type' => $type]);
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type, $workflow->currentLevel());
        $wf_track = $wftrackModel->first();
        if (!$action) {
            //un-archiving workflow
            $return = $this->wf_tracks->postArchiveWorkflow($wf_track, $action);
            return redirect()->back()->withFlashSuccess('Success, Workflow has been un-archived.');
        }
        $wfArchiveReasonCode = $wfArchiveReasonCodeRepo->query()->where('wf_module_group_id', $wf_module_group_id)->first();
        if ($wfArchiveReasonCode) {
            $archive_reasons = $wfArchiveReasonCode->reasons()->pluck("name", "id")->all();
        } else {
            $archive_reasons = [];
        }
        $record_users = (new UserRepository())->query()->where('unit_id', 16)->get()->pluck('name', 'id')->all();
        $prevUrl = url()->previous();
        return view('backend/system/workflow/archive')
            ->with('wf_track', $wf_track)
            ->with('prevurl', $prevUrl)
            ->with('record_users', $record_users)
            ->with('action', $action)
            ->with('archive_reasons', $archive_reasons);
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     * @throws GeneralException
     */
    public function canArchiveWorkflow($resource_id, $wf_module_group_id, $type)
    {
        $wfModule = new WfModuleRepository();
        $group = (new WfModuleGroupRepository())->find($wf_module_group_id);
        $success = $group->can_archive;
        $view = '';
        if ($success) {
            $module = $wfModule->getModule(['wf_module_group_id' => $wf_module_group_id, 'type' => $type]);
            $wf_track = $this->wf_tracks->getLastWorkflow($resource_id, $module);
            $view = (string) view('backend/system/workflow/includes/archive_option')
                ->with('wf_track', $wf_track)
                ->with('resource_id', $resource_id)
                ->with('wf_module_group_id', $wf_module_group_id)
                ->with('type', $type)
                ->with('module', $module);
        }
        return response()->json(['success' => $success, 'view' => $view ]);
    }

    /**
     * @param WfTrack $wf_track
     * @param $action
     * @param UpdateArchiveWorkflowRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postArchiveWorkflow(WfTrack $wf_track, $action, UpdateArchiveWorkflowRequest $request)
    {
        $input = $request->all();
        $return = $this->wf_tracks->postArchiveWorkflow($wf_track, $action, $input);
        return response()->json(['success' => true]);
    }

    /**
     * @param WfArchive $wf_archive
     * @param UpdateArchiveWorkflowRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateArchiveWorkflow(WfArchive $wf_archive, UpdateArchiveWorkflowRequest $request)
    {
        $input = $request->all();
        $return = $this->wf_tracks->updateArchiveWorkflow($wf_archive, $input);
        return response()->json(['success' => true]);
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @param $type
     * @return $this
     * @throws \App\Exceptions\GeneralException
     */
    public function getCompletedWfTracks($resource_id, $wf_module_group_id, $type)
    {
        $wf_tracks = $this->wf_tracks->getCompletedWfTracks($resource_id, $wf_module_group_id, $type);
        $module = (new WfModuleRepository())->getModuleInstance(['wf_module_group_id' => $wf_module_group_id, 'type' => $type]);
        return view("backend.includes.workflow.completed_tracks")
                        ->with("wf_tracks", $wf_tracks)
                        ->with("resource_id", $resource_id)
                        ->with("wf_module_group_id", $wf_module_group_id)
                        ->with("type", $type)
                        ->with("module", $module);
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @param $type
     * @return $this
     * @throws \App\Exceptions\GeneralException
     */
    public function getListWfTracks($resource_id, $wf_module_group_id, $type)
    {
        $wf_tracks = $this->wf_tracks->getCompletedWfTracks($resource_id, $wf_module_group_id, $type);
        return view("backend.includes.workflow.list_tracks")
                        ->with("wf_tracks", $wf_tracks);
    }

    public function getWfTracksForHtml($resource_id, $wf_module_group_id, $type)
    {
        $tracks = $this->wf_tracks->getPendingWfTracksForDatatable($resource_id, $wf_module_group_id, $type);
        //logger($tracks);
        $current_track = view("backend/includes/workflow/current_track")
                                ->with("wf_tracks", $tracks);
        $completed_tracks = $this->getCompletedWfTracks($resource_id, $wf_module_group_id, $type);
        //return ['current_track' => (string) $current_track, 'completed_tracks' => (string) $completed_tracks];
        return response()->json(['current_track' => (string) $current_track, 'completed_tracks' => (string) $completed_tracks]);
    }

    /**
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Exception
     */
    public function getWfTracksForDatatable($resource_id, $wf_module_group_id, $type)
    {
        return Datatables::of($this->wf_tracks->getPendingWfTracksForDatatable($resource_id, $wf_module_group_id, $type))
        ->editColumn('user_id', function($wf_track) {
            return $wf_track->username_formatted;
        })
        ->editColumn('receive_date', function ($wf_track) {
            return $wf_track->receive_date_formatted;
        })
        ->editColumn('forward_date', function ($wf_track) {
            return $wf_track->forward_date_formatted;
        })
        ->editColumn('status', function ($wf_track) {
            return $wf_track->status_narration;
        })
        ->editColumn('wf_definition_id', function ($wf_track) {
            return $wf_track->wfDefinition->level;
        })
        ->addColumn('action', function ($wf_track) {
            return $wf_track->status_narration;
        })
        ->addColumn('description', function ($wf_track) {
            return $wf_track->wfDefinition->description;
        })
        ->addColumn("aging", function ($wf_track) {
            return $wf_track->getAgingDays();
        })
        ->rawColumns(['user_id'])
        ->make(true);
    }

    public static function getWfTracks($resource_id, WorkflowTrackDataTable $dataTable)
    {
        $dataTable->with('resource_id', $resource_id)->render('backend.includes.workflow_track');
    }

    /**
     * @param $resource_id
     * @param $wf_module_group_id
     * @return mixed
     * @throws \Exception
     */
    public function getDeactivatedWfTracksForDataTable($resource_id, $wf_module_group_id) {

        return Datatables::of($this->wf_tracks->getDeactivatedWfTracksForDataTable($resource_id, $wf_module_group_id))
            ->editColumn('user_id', function($wf_track) {
                return $wf_track->username_formatted;
            })
            ->editColumn('receive_date', function ($wf_track) {
                return $wf_track->receive_date_formatted;
            })
            ->editColumn('forward_date', function ($wf_track) {
                return !is_null($wf_track->forward_date) ? $wf_track->forward_date_formatted : ' ';
            })
            ->editColumn('status', function ($wf_track) {
                return $wf_track->status_narration;
            })
            ->editColumn('wf_definition_id', function ($wf_track) {
                return $wf_track->wfDefinition->level;
            })
            ->addColumn('action', function ($wf_track) {
                return $wf_track->status_narration;
            })
            ->addColumn('description', function ($wf_track) {
                return $wf_track->wfDefinition->description;
            })
            ->addColumn("aging", function ($wf_track) {
                return $wf_track->getAgingDays();
            })
            ->rawColumns(['user_id'])
            ->make(true);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     * @description  Get Deactivated Claim workflow Tracks for this resource id
     */
    public function getDeactivatedClaimWfTracksForDataTable($id) {

        return Datatables::of($this->wf_tracks->getDeactivatedClaimWfTracksForDataTable($id))
            ->editColumn('user_id', function($wf_track) {
                return $wf_track->username_formatted;
            })
            ->editColumn('receive_date', function ($wf_track) {
                return $wf_track->receive_date_formatted;
            })
            ->editColumn('forward_date', function ($wf_track) {
                return !is_null($wf_track->forward_date) ? $wf_track->forward_date_formatted : ' ';
            })
            ->editColumn('status', function ($wf_track) {
                return $wf_track->status_narration;
            })
            ->editColumn('wf_definition_id', function ($wf_track) {
                return $wf_track->wfDefinition->level;
            })
            ->addColumn('action', function ($wf_track) {
                return $wf_track->status_narration;
            })
            ->addColumn('description', function ($wf_track) {
                return $wf_track->wfDefinition->description;
            })
            ->addColumn("aging", function ($wf_track) {
                return $wf_track->getAgingDays();
            })
            ->rawColumns(['user_id'])
            ->make(true);
    }

    /**
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function getWorkflowModalContent()
    {
        $wf_track_id = request()->input("wf_track_id");
        $statuses = [];
        $statuses[''] = "";
        $wf_track = $this->wf_tracks->find($wf_track_id);
        $workflow = new Workflow(['wf_module_id' => $wf_track->wfDefinition->wfModule->id, 'resource_id' => $wf_track->resource_id]);
        $wf_definition = $wf_track->wfDefinition;
        $assignStatus = $this->wf_tracks->assignStatus($wf_track_id);
        $is_optional = $wf_definition->is_optional;
        $isrestrictive = 0;
        if (!env("TESTING_MODE")) {
            $isrestrictive = $wf_definition->isrestrictive;
        }
        $has_next_start_optional = $wf_definition->has_next_start_optional;
        $next_level_designation = "";
        $next_level_description = "";
        $selective_levels = [];
        $isselective = 0;
        $isselectuser = 0;
        $select_users = [];
        if ($wf_definition->isselective) {
            //query selective levels
            $isselective = 1;
            $selective_levels = $workflow->selectiveLevels();
        }
        if ($wf_definition->assign_next_user) {
            //query users to select
            $isselectuser = 1;
            $select_users = $workflow->getUsersToSelect();
        }
        //logger($wf_definition->id);
        if ($wf_definition->is_approval) {
            $statuses['1'] = "Approved";
        } else {
            if ($wf_definition->assign_next_user) {
                $statuses['1'] = "Recommended";
            } else {
                $statuses['1'] = "Recommended";
            }
        }
        if ($workflow->currentLevel() <> 1) {
            $prevWfDefinition = $workflow->nextWfDefinition(-1, true);
            if ($prevWfDefinition->allow_rejection) {
                $statuses['2'] = "Reverse to level";
            }
        }
        if ($wf_definition->is_approval) {
            $statuses['3'] = "Declined";
        }
        if ($wf_definition->has_next_start_optional) {
            $statuses['4'] = "Seek Advice";
        }

        //$has_participated = $workflow->hasParticipated();
        $has_participated = false;
        $user_has_access = $workflow->userHasAccess(access()->id(), $workflow->currentLevel());
        //$restrictive_check = $workflow->restrictiveCheck();

        return view("backend/includes/workflow_process_modal")
            ->with("assign_status", $assignStatus)
            ->with("wf_track", $wf_track)
            ->with("has_participated", $has_participated)
            ->with("user_has_access", $user_has_access)
            ->with("previous_levels", $workflow->previousLevels())
            ->with("statuses", $statuses)
            ->with("next_level_designation", $next_level_designation)
            ->with("next_level_description", $next_level_description)
            ->with("isselective", $isselective)
            ->with("isselectuser", $isselectuser)
            ->with("isrestrictive", $isrestrictive)
            ->with("selective_levels", $selective_levels)
            ->with("is_optional", $is_optional)
            ->with("select_users", $select_users);
    }

    /**
     * @param WfTrack $wf_track
     * @param $action
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\GeneralException
     * @description Show the next level designation title
     */
    public function nextLevelDesignation(WfTrack $wf_track, $action)
    {
        $wf_definition = $wf_track->wfDefinition;
        $is_optional = $wf_definition->is_optional;
        $has_next_start_optional = $wf_definition->has_next_start_optional;
        if ($action == '4') {
            $select_next_start_optional = 1;
        } else {
            $select_next_start_optional = 0;
        }
        switch (true) {
            case ($has_next_start_optional And !$select_next_start_optional And !$is_optional):
            case (!$has_next_start_optional And !$select_next_start_optional And !$is_optional):
            case ($has_next_start_optional And !$select_next_start_optional And $is_optional): //Added recently
                //This logic computed from the truth table, may need re-design.
                //Skip Optional level
                $skip = true;
                break;
            default:
                //Continue Regularly
                $skip = false;
                break;
        }
        $next_level_designation = "";
        $next_level_description = "";
        $workflow = new Workflow(['wf_module_id' => $wf_track->wfDefinition->wfModule->id, 'resource_id' => $wf_track->resource_id]);
        if (!$wf_definition->isselective) {

        }
        $nextWfDefinition = $workflow->nextWfDefinition(1, $skip);
        if ($nextWfDefinition) {
            $next_level_designation = $nextWfDefinition->definition_designation;
            $next_level_description = $nextWfDefinition->description;
        }
        return response()->json(['next_level_designation' => $next_level_designation, 'success' => true, 'skip' => $skip, 'next_level_description' => $next_level_description]);
    }

    /**
     * @param WfTrack $wf_track
     * @param UpdateWorkflowRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws WorkflowException
     */
    public function updateWorkflow(WfTrack $wf_track, UpdateWorkflowRequest $request)
    {
        if ($wf_track->status) {
            throw new WorkflowException("This workflow has already been forwarded! Please reload the workflow table.");
        }
        $action = $request->input("action");
        switch ($action) {
            case 'assign':
                $input = ['user_id' => access()->id(), 'assigned' => 1];
                $success = true;
                $message = trans('alerts.backend.workflow.assigned');
                break;
            case 'approve_reject':
                $status = $request->input("status");
                $input = ['user_id' => access()->id(), 'status' => $status, 'comments' => $request->input("comments"), 'forward_date' => Carbon::now(), 'wf_definition' => $request->input("wf_definition"), 'select_user' => $request->input('select_user')];
                if ($status == '2') {
                    $input['level'] = (string) $request->input("level");
                }
                $success = true;
                $message = trans('alerts.backend.workflow.updated');
                break;
        }
        //Heavy Duty Call
        $wfModule = $this->wf_tracks->updateWorkflow($wf_track, $input, $action);
        return response()->json(['success' => $success, 'message' => $message, 'action' => $action, 'resource_id' => $wf_track->resource_id, 'wf_module_group_id' => $wfModule->wf_module_group_id, 'type' => $wfModule->type]);
    }

    /**
     * @param Request $request
     * @param string $cr
     * @return array
     */
    public function listControl(Request $request, $category = NULL) : array
    {
        $wfModuleRepo = new WfModuleRepository();
        $crRepo = new ConfigurableReportRepository();
        $control = [
            'wf_mode' => 1,
            'cr' => NULL,
            'show' => 0,
            'wfname' => 'Workflow',
            'wf_module_id' => NULL,
            'wf_definitions' => [],
            'filters' => [],
        ];
        $has_wf_module = $request->has('wf_module_id');
        if ($has_wf_module) {
            $control['show'] = 1;
            $wfmoduleid = $request->input('wf_module_id');
            $control['wf_module_id'] = $wfmoduleid;
            $wfmodule = $wfModuleRepo->find($wfmoduleid);
            if ($wfmodule) {
                $control['wfname'] = $wfmodule->name;
                if ($wfmodule->wf_mode == 2) {
                    $crid = $wfModuleRepo->getCR($wfmoduleid);
                    //manupulate report rendering ...
                    switch ($category) {
                        case "attended":
                            if ($crid == 38) {
                                $crid = 72;
                            }
                            break;
                    }
                    //control the data to be rendered to view ...
                    switch ($category) {
                        case "progress":
                            $wf_definitions = [];
                            break;
                        default:
                            $wf_definitions = access()->allWfDefinitions();
                            break;
                    }
                    $cr = $crRepo->query()->find($crid);
                    //dd($wf_definitions);
                    $control['wf_definitions'] = $wf_definitions;
                    if ($cr) {
                        $control['wf_mode'] = 2;
                        //$filters = $crRepo->setDataForSelectFilter(json_decode($cr->filter ?? '[]', true));
                        $control['cr'] = $cr;
                        //$control['filters'] = $filters;
                    }
                }
            }
        }
        //dd($control);
        return $control;
    }

    /**
     * Pending Workflow
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pending(Request $request)
    {
        $wfModuleRepo = new WfModuleRepository();
        $control = $this->listControl($request);
        //Log::info($has_wf_module);
        return view("backend/system/workflow/pending")
            ->with("wf_modules", $wfModuleRepo->getAllActive()->pluck('name', 'id')->all())
            ->with("group_counts", $wfModuleRepo->getActiveUser())
            ->with("state", "all")
            ->with("statuses", ['2' => 'All', '1' => 'Assigned to Me', '0' => 'Not Assigned', '3' => 'Assigned to User'])
            ->with("unregistered_modules", $wfModuleRepo->unregisteredMemberNotificationIds())
            ->with("users", $this->users->query()->where("id", "<>", access()->id())->get()->pluck('name', 'id'))
            ->with('control', $control);
    }

    public function getPending()
    {
        $datatables = $this->wf_tracks->getForWorkflowDatatable();
        return $datatables->make(true);
    }

    /**
     * Assigned Workflow
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myPending(Request $request)
    {
        //$wf_module_groups = new WfModuleGroupRepository();
        $wfModuleRepo = new WfModuleRepository();
        $control = $this->listControl($request);
        return view("backend/system/workflow/my_pending")
            ->with("wf_modules", $wfModuleRepo->getAllActive()->pluck('name', 'id', 'id')->all())
            ->with("group_counts", $wfModuleRepo->getAssignedActiveUser())
            ->with("unregistered_modules", $wfModuleRepo->unregisteredMemberNotificationIds())
            ->with("state", "assigned")
            ->with("control", $control);
    }

    /**
     * Archived Workflow
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function archived(Request $request)
    {
        $wfModuleRepo = new WfModuleRepository();
        $control = $this->listControl($request);
        return view('backend/system/workflow/archived')
            ->with('wf_modules', $wfModuleRepo->getAllActive()->pluck('name', 'id')->all())
            ->with('group_counts', $wfModuleRepo->getAssignedActiveUser(6))
            ->with('state', 'archived')
            ->with('statuses', ['2' => 'All', '1' => 'Archived by Me', '3' => 'Archived by User'])
            ->with('unregistered_modules', $wfModuleRepo->unregisteredMemberNotificationIds())
            ->with("users", $this->users->query()->where("id", "<>", access()->id())->get()->pluck('name', 'id'))
            ->with("control", $control);
    }

    /**
     * @return mixed
     * My pending with workflow pending type
     */
    public function myPendingWithType()
    {
        $wf_module_groups = new WfModuleGroupRepository();
        $wf_modules = new WfModuleRepository();
        return view("backend/system/workflow/my_pending")
            ->with("wf_modules", $wf_modules->getAllActive()->pluck('name', 'id')->all())
            ->with("group_counts", $wf_modules->getAssignedActiveUser())
//            ->with("unregistered_modules", $wf_modules->unregisteredMemberNotificationIds())
            ->with("state", "assigned");
    }

    /**
     * Attended Workflow
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function attended(Request $request)
    {
        $wfModuleRepo = new WfModuleRepository();
        $control = $this->listControl($request, "attended");
        return view("backend/system/workflow/attended")
            ->with("wf_modules", $wfModuleRepo->getAllActive()->pluck('name', 'id')->all())
            ->with("group_counts", $wfModuleRepo->getMyAttendedActiveUser())
            ->with("state", "attended")
            ->with("statuses", ['1' => 'Attended by Me', '3' => 'Attended by User'])
            ->with("unregistered_modules", $wfModuleRepo->unregisteredMemberNotificationIds())
            ->with("users", $this->users->query()->where("id", "<>", access()->id())->get()->pluck('name', 'id'))
            ->with("control", $control);
    }

    public function progress(Request $request)
    {
        $wfModuleRepo = new WfModuleRepository();
        $control = $this->listControl($request, "progress");
        $user = access()->user();
        $unit_id = $user->unit_id;
        return view("backend/system/workflow/progress")
            ->with("wf_modules", $wfModuleRepo->getAllActive()->pluck('name', 'id')->all())
            ->with("group_counts", $wfModuleRepo->getMyUnitActiveUser($unit_id))
            ->with("unit_id", $unit_id)
            ->with("state", "progress")
            ->with("statuses", ['2' => 'All', '1' => 'Assigned to Me', '0' => 'Not Assigned', '3' => 'Assigned to User'])
            ->with("unregistered_modules", $wfModuleRepo->unregisteredMemberNotificationIds())
            ->with("users", $this->users->query()->where("id", "<>", access()->id())->get()->pluck('name', 'id'))
            ->with("control", $control);
    }

    public function allocation()
    {
        $wf_module_groups = new WfModuleGroupRepository();
        $wf_modules = new WfModuleRepository();
        return view("backend/system/workflow/allocation")
            ->with("wf_modules", $wf_modules->getAllActive()->pluck('name', 'id')->all())
            //->with("group_counts", $wf_modules->getActiveUser())
            ->with("state", "full")
            ->with("statuses", ['2' => 'All', '1' => 'Assigned to Me', '0' => 'Not Assigned', '3' => 'Assigned to User'])
            ->with("unregistered_modules", $wf_modules->unregisteredMemberNotificationIds())
            ->with("users", $this->users->query()->get()->pluck('name', 'id'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws WorkflowException
     */
    public function assignAllocation()
    {
        $input = request()->all();
        $this->wf_tracks->assignAllocation($input);
        return response()->json(['success' => true, 'message' => 'Success, user have been assigned to the selected resource(s)']);
    }

    public function initiate()
    {
        $input = request()->all();
        $return = $this->wf_tracks->initiateWorkflow($input);
        return redirect()->back()->withFlashSuccess("Success, Workflow has been initialized.");
    }

    
    public function getUsersReview(UsersReviewDataTable $dataTable)
    { 
      return $dataTable->render('backend.system.workflow.users_review');
    }


  public function reversed(Request $request)
  {
    $wfModuleRepo = new WfModuleRepository();
    $control = $this->listControl($request);
    return view("backend/system/workflow/reversed")
    ->with("wf_modules", $wfModuleRepo->getAllActive()->pluck('name', 'id')->all())
    ->with("group_counts", $wfModuleRepo->getReversedActiveUser())
    ->with("state", "all")
    ->with("statuses", ['2' => 'All', '1' => 'Assigned to Me', '0' => 'Not Assigned', '3' => 'Assigned to User'])
    ->with("unregistered_modules", $wfModuleRepo->unregisteredMemberNotificationIds())
    ->with("users", $this->users->query()->where("id", "<>", access()->id())->get()->pluck('name', 'id'))
    ->with('control', $control);
}


public function getReversedPending()
{
    $datatables = $this->wf_tracks->getReversedForWorkflowDatatable();
    return $datatables->make(true);
}



public function subordinatesSummaryView()
{
 return view("backend/system/workflow/subordinates/subordinates_summary");
}


public function subordinatesSummaryDatatable()
{
    $datatables = $this->wf_tracks->getSubordinatesSummaryDatatable();
    return $datatables->make(true);
}


public function subordinatePending($user_id, Request $request)
{
    $user = $this->users->findOrThrowException($user_id);
    $wfModuleRepo = new WfModuleRepository();
    $control = $this->listSubControl($user_id, $request);
    return view("backend/system/workflow/subordinates/index")
    ->with("wf_modules", $wfModuleRepo->getAllActive()->pluck('name', 'id')->all())
    ->with("group_counts", $wfModuleRepo->getActiveSubordinates($user_id))
    ->with("state", "all")
    ->with("user_id", $user_id)
    ->with("user", $user)
    ->with("statuses", ['2' => 'All', '1' => 'Assigned to Me', '0' => 'Not Assigned', '3' => 'Assigned to User'])
    ->with("unregistered_modules", $wfModuleRepo->unregisteredMemberNotificationIds())
    ->with("users", $this->users->query()->where("id", "<>", $user_id)->get()->pluck('name', 'id'))
    ->with('control', $control);
}

public function getSubordinatePending($user_id)
{
    $datatables = $this->wf_tracks->getSubordinatesForWorkflowDatatable($user_id);
    return $datatables->make(true);
}


public function listSubControl($user_id, Request $request, $crid = NULL) : array
{
    $wfModuleRepo = new WfModuleRepository();
    $crRepo = new ConfigurableReportRepository();
    $control = [
        'wf_mode' => 1,
        'cr' => NULL,
        'show' => 0,
        'wfname' => 'Workflow',
        'wf_module_id' => NULL,
        'wf_definitions' => [],
        'filters' => [],
    ];
    $has_wf_module = $request->has('wf_module_id');
    if ($has_wf_module) {
        $control['show'] = 1;
        $wfmoduleid = $request->input('wf_module_id');
        $control['wf_module_id'] = $wfmoduleid;
        $wfmodule = $wfModuleRepo->find($wfmoduleid);
        if ($wfmodule) {
            $control['wfname'] = $wfmodule->name;
            if ($wfmodule->wf_mode == 2) {
                if (!$crid) {
                    $cr = $crRepo->query()->find($wfModuleRepo->getCR($wfmoduleid));
                } else {
                    $cr = $crRepo->query()->find($crid);
                }
                $wf_definitions = access()->allSubordinateWfDefinitions($user_id);
                    //dd($wf_definitions);
                $control['wf_definitions'] = $wf_definitions;
                if ($cr) {
                    $control['wf_mode'] = 2;
                        //$filters = $crRepo->setDataForSelectFilter(json_decode($cr->filter ?? '[]', true));
                    $control['cr'] = $cr;
                        //$control['filters'] = $filters;
                }
            }
        }
    }
        //dd($control);
    return $control;
}



}