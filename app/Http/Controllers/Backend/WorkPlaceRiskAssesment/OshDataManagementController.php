<?php

namespace App\Http\Controllers\Backend\WorkPlaceRiskAssesment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\WorkPlaceRiskAssesment\OshDataDataTable;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshDataManagementRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use Yajra\Datatables\Datatables;
use App\Repositories\Backend\Sysdef\GenderRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\DataTables\WorkPlaceRiskAssesment\OshEmployerDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshEmployerIncidentsDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshAccidentDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshDeseaseDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshDeathDatatable;
use App\DataTables\WorkPlaceRiskAssesment\DataTableBase;
use Carbon\Carbon;
use DB;



class OshDataManagementController extends Controller
{
    protected $data_management;
    protected $employers;

    public function __construct()
    {
        $this->data_management = new OshDataManagementRepository();
        $this->employers = new EmployerRepository();
        $this->code_values = new CodeValueRepository();
    }

    public function dataManagementMenu(){
      return view("backend/workplace_risk_assesment/osh_data/data_management_menu");
  }

  public function employerParticulars(OshEmployerDatatable $dataTable){

    $employer_categories = $this->data_management->employerCategories();
    $bussines_sectors = $this->data_management->bussinesSectors();
    return view("backend/workplace_risk_assesment/osh_data/employers")
    ->with("employer_categories",$employer_categories)
    ->with("bussines_sectors",$bussines_sectors)
    ->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all());
}

public function employeeDemographic(OshDataDataTable $dataTable)

{
  $employment_categories = $this->data_management->employmentCategories();
  $employer_categories = $this->data_management->employerCategories();
  $bussines_sectors = $this->data_management->bussinesSectors();
  $occupations = $this->data_management->jobTitles();
  $workplaces = $this->data_management->allworkplaces();
  $genders = (new GenderRepository())->query()->pluck("name", "id")->all();
  $unknown_gender = ['Unknown'];
  $genders = array_merge($unknown_gender,$genders);
  return view('backend/workplace_risk_assesment/osh_data/employees')
  ->with("employment_categories",$employment_categories)
  ->with("employer_categories",$employer_categories)
  ->with("bussines_sectors",$bussines_sectors)
  ->with("occupations",$occupations)
  ->with("workplaces",$workplaces)
  ->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all())
  ->with("genders", $genders);
}

public function oshClaims(Request $request,OshDeathDatatable $deathDataTable,OshDeseaseDatatable $deseaseDataTable, OshAccidentDatatable $dataTable)
{
       // dd($request->all());

    $employment_categories = $this->data_management->employmentCategories();
    $employer_categories = $this->data_management->employerCategories();
    $bussines_sectors = $this->data_management->bussinesSectors();
    // $accident_types = $this->data_management->accidentTypes();

    $accident_types = DB::table('code_values')
    ->where('code_id',58)->pluck('name','id');
    // dump($accident_types);

    $accident_causes = DB::table('code_values')
    ->where('code_id',57)->pluck('name','id');
     // dd($accident_causes);

    // $accident_causes = DB::table('codes')
    // ->where('id',15)
    // ->orWhere('id',47)->pluck('name','id');

    $disease_types = DB::table('codes')
    ->where('codes.id',18)
    ->join('code_values','code_values.code_id','=','codes.id')
    ->pluck('code_values.name','code_values.id');

    $disease_agents = DB::table('code_values')
    ->where('id',311)
    ->orWhere('id',312)
    ->orWhere('id',313)->pluck('name','id');

    $target_organs = DB::table('code_values')
    ->where('id',314)
    ->orWhere('id',315)
    ->orWhere('id',316)
    ->orWhere('id',317)->pluck('name','id');

    $benefit_types = DB::table('benefit_types')
    ->where('benefit_type_group_id','!=',4)
    ->orWhere('id',317)->pluck('name','id');

    $genders = (new GenderRepository())->query()->pluck("name", "id")->all();
    $unknown_gender = ['Unknown'];
    $genders = array_merge($unknown_gender,$genders);

    if (isset($request['incident'])) {
        if($request['incident'] == 1){
            return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_data/claims',[
               "accident_types" => $accident_types,
               "accident_causes" => $accident_causes,
               "disease_types" => $disease_types,
               "disease_agents" => $disease_agents,
               "target_organs" => $target_organs,
               "benefit_types" => $benefit_types,
               "regions" => (new RegionRepository())->query()->pluck("name", "id")->all(),
               "genders" => $genders
           ]);
        }elseif ($request['incident'] == 2) {
         $dataTable = $deseaseDataTable;
         return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_data/claims',[
           "accident_types" => $accident_types,
           "accident_causes" => $accident_causes,
           "disease_types" => $disease_types,
           "disease_agents" => $disease_agents,
           "target_organs" => $target_organs,
           "benefit_types" => $benefit_types,
           "regions" => (new RegionRepository())->query()->pluck("name", "id")->all(),
           "genders" => $genders
       ]); 
     }elseif ($request['incident'] == 3) {
         $dataTable = $deathDataTable;
         return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_data/claims',[
           "accident_types" => $accident_types,
           "accident_causes" => $accident_causes,
           "disease_types" => $disease_types,
           "disease_agents" => $disease_agents,
           "target_organs" => $target_organs,
           "benefit_types" => $benefit_types,
           "regions" => (new RegionRepository())->query()->pluck("name", "id")->all(),
           "genders" => $genders
       ]); 
     }

 }
 else{
    $request = [
       'incident' => 1
   ];

   return $dataTable->with('request',$request)->render('backend/workplace_risk_assesment/osh_data/claims',[
       "accident_types" => $accident_types,
       "accident_causes" => $accident_causes,
       "disease_types" => $disease_types,
       "disease_agents" => $disease_agents,
       "target_organs" => $target_organs,
       "benefit_types" => $benefit_types,
       "regions" => (new RegionRepository())->query()->pluck("name", "id")->all(),
       "genders" => $genders
   ]);
}
}

public function getEmployerDatatable(OshEmployerDatatable $datatable)
{
    $columns = [
     'name',
     'reg_no',
     'doc',
     'employer_category',
     'bussines_sector',
     'region',
     'counts',
 ];
 $query = $this->employers->getEmployerClaimDataTable();
 $dataTable = Datatables::of($query)
 ->addColumn('counts', function ($employer) {
    return ($employer->counts ? $employer->counts : 0);
});
 $base = new DataTableBase($query, $dataTable, $columns);
 return $base->render(null);


}

public function getEmployeeDataTable()
{

    $columns = [
     'employee_id',
     'filename',
     'gender',
     'incident_type',
     'fullname',
     'employer',
     'incident_date',
     'receipt_date',
     'region',
     'employment_category',
     'employer_category',
     'bussines_sector',
     'occupation',
     'age',
 ];
 $query = $this->data_management->getForDataTable();
 $dataTable = Datatables::of($query)
 ->editColumn('status', function ($notification_report) {
    return $notification_report->status_label;
})
 ->rawColumns(['status']);
 $base = new DataTableBase($query, $dataTable, $columns);
 return $base->render(null);
}

public function employee($employee_id, $not_id)
{
    $employee = $this->data_management->getEmployee($employee_id);
    $tasco = $this->data_management->tascoOccupation($employee_id,$not_id);
    if ($tasco->workplace_name == null) {
        $workplace = null;
    } else {
        $workplace = $tasco->workplace_regno.'-'.$tasco->workplace_name;
    }
    
    if ($tasco->datehired == null) {
        $experience = null;
    }else{
       $date_hired = Carbon::createFromFormat('Y-m-d', $tasco->datehired);
       $incident_date = Carbon::createFromFormat('Y-m-d', $tasco->incident_date);
       $diff = $date_hired->diffInMonths($incident_date);
       if ($diff < 12) {
           $experience = $diff." Month(s)";
       } else {
        $diff = $date_hired->diffInYears($incident_date);
        $experience = $diff." Year(s)";
    }
}
$employee_photo = null;
return view("backend/workplace_risk_assesment/osh_data/employee",compact('employee','employee_photo','experience','tasco','workplace'));
}

public function employer($employer_id,OshEmployerIncidentsDatatable $dataTable)
{
    $employer = $this->employers->findOrThrowException($employer_id);
    $workplaces = $this->employers->workplaces($employer_id);
    if (count($workplaces) == 0) {
        $workplaces = null;
    }
        // dd($workplaces);
    
    $interest_summary = $this->employers->interestPaymentSummary($employer_id);
    $booking_summary = $this->employers->bookingPaymentSummary($employer_id);
    $member_count = $this->employers->getMemberCount($employer_id);
    return $dataTable->with('employer', $employer_id)->render('backend/workplace_risk_assesment/osh_data/employer',[
        'employer' => $employer,
        "interest_summary"=> $interest_summary,
        "booking_summary"=> $booking_summary,
        "member_count"=> $member_count,
        "workplaces"=> $workplaces
    ]);
}

public function getAccident($accident_id){
    $accidents = DB::table('codes')
    ->select('code_values.name','code_values.id')
    ->where('codes.id',$accident_id)
    ->join('code_values','code_values.code_id','=','codes.id')
    ->get();

    return view('backend/workplace_risk_assesment/osh_data/includes/select_options/accident_causes')
    ->with("accident_causes", $accidents);
    
}

public function getDeseasesByOrgan($organ_id){
        // return $organ_id;
    $disease_causes = DB::table('code_values')
    ->select('disease_exposure_agents.name','disease_exposure_agents.id')
    ->where('code_values.id',$organ_id)
    ->join('disease_exposure_agents','disease_exposure_agents.code_value_id','=','code_values.id')
    ->get();
    return view('backend/workplace_risk_assesment/osh_data/includes/select_options/disease_causes')
    ->with("disease_causes", $disease_causes);
}

public function getDeseasesByAgent($agent_id){
        // return $agent_id;
    $disease_by_agents = DB::table('code_values')
    ->select('disease_exposure_agents.name','disease_exposure_agents.id')
    ->where('code_values.id',$agent_id)
    ->join('disease_exposure_agents','disease_exposure_agents.code_value_id','=','code_values.id')
    ->get();
    return view('backend/workplace_risk_assesment/osh_data/includes/select_options/disease_by_agents')
    ->with("disease_by_agents", $disease_by_agents);
}
}
