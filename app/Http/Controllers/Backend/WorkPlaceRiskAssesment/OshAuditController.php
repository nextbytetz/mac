<?php
namespace App\Http\Controllers\Backend\WorkPlaceRiskAssesment;
/*juma*/
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Services\Workflow\Workflow;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Carbon\Carbon;

use Yajra\Datatables\Facades\Datatables;
use App\DataTables\WorkflowTrackDataTable;
use App\DataTables\WorkPlaceRiskAssesment\OshAudit\OshAuditPeriodDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshAudit\OshAuditPeriodProfileDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshAudit\OshAuditPeriodEmployerDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshAudit\OshChecklistEmployersDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshAudit\UserOshAuditListDatatable;


use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditEmployerRepository;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditPeriodRepository;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Location\DistrictRepository;

use App\Models\WorkPlaceRiskAssesment\OshAuditFeedback;
use App\Models\WorkPlaceRiskAssesment\OshAuditQuestion;

class OshAuditController extends Controller
{

	protected $osh_audit_repository;


	public function __construct(OshAuditPeriodRepository $osh_audit_repository, UserRepository $users, OshAuditEmployerRepository $osh_audit_employer)
	{
		$this->osh_audit_repository = $osh_audit_repository;
		$this->users = $users;
		$this->osh_audit_employer = $osh_audit_employer;
		$this->wf_module_group_id = 28;
		$this->middleware('access.routeNeedsPermission:assign_staff_for_osh_audit', ['only' => ['assignStaffAudit']]);

	}

	public function index(OshAuditPeriodDatatable $dataTable, Request $request)
	{
		$fin_years = DB::table('fin_years')->get()->pluck('name', 'id');
		return $dataTable->with('request', $request->all())->render('backend/workplace_risk_assesment/osh_audit/view_periods', [
			'fin_years' => $fin_years,
		]);
	}

	public function auditListProfile($osh_audit_id, OshAuditPeriodProfileDatatable $dataTable, WorkflowTrackDataTable $workflowTrack, OshAuditPeriodEmployerDatatable $periodEmployerDataTable)
	{
		$osh_audit = $this->osh_audit_repository->findOrThrowException($osh_audit_id);
		$initiate_wf = $this->osh_audit_repository->canWfInitiated($osh_audit->period, $osh_audit->fin_year, $osh_audit->status);
		
		if (!empty($osh_audit->status)) {
			$dataTable = $periodEmployerDataTable;
			return $dataTable->with('osh_audit_period_id', $osh_audit_id)
			->render('backend/workplace_risk_assesment/osh_audit/audit_list_profile', [
				'osh_audit' => $osh_audit,
				'initiate_wf' => $initiate_wf,
				'workflowtrack' => $workflowTrack
			]);
		} else {
			return $dataTable->with('osh_audit', $osh_audit)
			->render('backend/workplace_risk_assesment/osh_audit/audit_list_profile', [
				'osh_audit' => $osh_audit,
				'initiate_wf' => $initiate_wf,
				'workflowtrack' => $workflowTrack
			]);
		}
	}

	public function employerAuditProfile($osh_audit_period_id, $employer_id)
	{
		$osh_audit_employer = $this->osh_audit_employer->auditEmployerProfile($osh_audit_period_id, $employer_id);
		$osh_audit_feedbacks = $this->osh_audit_employer->returnFeedbacks($osh_audit_employer->id); 
		$code_values = $this->osh_audit_employer->returnOshAuditCodeValues();
		$feedback_score = $this->osh_audit_employer->returnFeedbackScore($osh_audit_employer->id);
		$is_period_active =  $this->osh_audit_repository->isPeriodActive($osh_audit_employer->period, $osh_audit_employer->fin_year);

		return view("backend/workplace_risk_assesment/osh_audit/employer_audit_profile")
		->with('osh_audit_employer',$osh_audit_employer)
		->with('osh_audit_feedbacks',$osh_audit_feedbacks)
		->with('feedback_score',$feedback_score)
		->with('is_period_active',$is_period_active)
		->with('code_values',$code_values);
	}


	public function checklistView(OshChecklistEmployersDatatable $dataTable, Request $request)
	{
		
		$fin_years = $this->osh_audit_repository->returnFinYearsInitiated();
		return $dataTable->with('input', $request->all())
		->render('backend/workplace_risk_assesment/osh_audit/employers_for_audit',  
			[
				'users' => $this->users->query()->get()->pluck('name', 'id'),
				'fin_years' => $fin_years,
			]);
	}



	public function submitPeriodList(Request $request)
	{
		$rules = [
			'osh_audit_period_id' => 'required', 
			'fatal' => 'required', 
			'lti' => 'required', 
			'non_conveyance' => 'required', 
		];

		$validator = Validator::make(Input::all(),$rules);

		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			access()->hasWorkflowDefinition($this->wf_module_group_id, 1);
			$this->osh_audit_repository->initiateWf($request->all());
			return response()->json(['success' => true]);
		}
	}


	public function assignStaffAudit(Request $request)
	{
		$rules = [
			'assigned_user' => 'required', 
			'selected_employers' => 'required', 
		];

		$message = [
			'assigned_user.required' => 'Please select user to assign', 
			'selected_employers.required' => 'Please select atleast one employer to assign', 
		];

		$validator = Validator::make(Input::all(),$rules,$message);

		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$this->osh_audit_employer->assignAuditToUser($request->all());
			return response()->json(['success' => true]);
		}
	}

	public function updateFeedbackForm($osh_audit_employer_id)
	{
		$osh_audit_employer = $this->osh_audit_employer->findOrThrowException($osh_audit_employer_id); 
		$osh_audit_feedbacks = $this->osh_audit_employer->returnFeedbacks($osh_audit_employer_id); 
		$feedback_score = $this->osh_audit_employer->returnFeedbackScore($osh_audit_employer->id);
		$code_values = $this->osh_audit_employer->returnOshAuditCodeValues();
		$districts = new DistrictRepository();
		$is_codevalue_filled = $this->osh_audit_employer->isCodeValueFilled($osh_audit_employer_id);

		return view("backend/workplace_risk_assesment/osh_audit/update_feedback")
		->with('osh_audit_feedbacks',$osh_audit_feedbacks)
		->with('code_values',$code_values)
		->with('feedback_score',$feedback_score)
		->with('districts', $districts->query()->pluck("name", "id")->all())
		->with('is_codevalue_filled',$is_codevalue_filled)
		->with('osh_audit_employer',$osh_audit_employer);
	}


	public function updateFeedback(Request $request)
	{	
		$rules = [
			'answers.*' => 'required', 
		];
		$message = [
			'answers.*' => 'Please select the correct answer',
		];
		$validator = Validator::make(Input::all(),$rules,$message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$this->osh_audit_employer->updateFeedback($request->all());
			return response()->json(['success' => true]);
		}
	}


	public function profileNotificationsDatatable($osh_audit_employer_id, $type)
	{
		
		$query = $this->osh_audit_employer->returnProfileNotificationsForDatatable($osh_audit_employer_id, $type);
		return Datatables::of($query)
		->addColumn('reporting_date', function($data) {
			return Carbon::parse($data->reporting_date)->format('dS M, Y');
		})
		->filterColumn('name', function($data, $keyword) {
			return $data->whereRaw("CONCAT(employees.firstname,' ',employees.middlename,' ',employees.lastname) ilike ?", ["%{$keyword}%"]);
		})
		->make(true);
	}


	public function getCodeValueQuestions($osh_audit_employer_id, $code_value_id)
	{	
		$osh_feedback = $this->osh_audit_employer->returnQuestions($osh_audit_employer_id, $code_value_id);
		return response()->json(['success' => true, 'data' => $osh_feedback]);
	}


	public function updateFeedbackGeneralInfo(Request $request)
	{	
		
		$rules = [
			"workplace_name" => 'required',
			"workplace_address" => 'required',
			"workplace_district" => 'required',
			"workplace_location" => 'required',
			"contact_person" => 'required',
			//'phone' => 'required|phone:TZ',
		];
		$message = [
			"workplace_name.required" => 'Please write the  visited workplace name',
			"workplace_address.required" => 'Please write the  visited workplace address',
			"workplace_district.required" => 'Please select the district of visited workplace',
			"workplace_location.required" => 'Please write the  visited workplace location',
			"contact_person.required" => 'Please write the  name of the contact person of the  visited workplace',
			//"phone.required" => 'Please write the phone number of the contact person of the  visited workplace',
			//"phone.phone" => 'Please write the correct phone number'

		];
		$validator = Validator::make(Input::all(),$rules,$message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$this->osh_audit_employer->updateFeedbackGeneralInfo($request->all());
			return response()->json(['success' => true]);
		}
	}


	public function activatePeriod($osh_audit_period)
	{
		$this->osh_audit_repository->activatePeriod($osh_audit_period);
		return response()->json(['success' => true]);
	}

	public function viewAuditList($osh_audit_period_id,$fatal,$lti,$non_conveyance, WorkflowTrackDataTable $workflowTrack)
	{


		$osh_audit = $this->osh_audit_repository->findOrThrowException($osh_audit_period_id);
		$quaters = $this->osh_audit_repository->returnQuaters($osh_audit->period);
		$initiate_wf = $this->osh_audit_repository->canWfInitiated($osh_audit->period, $osh_audit->fin_year, $osh_audit->status);

		return view('backend/workplace_risk_assesment/osh_audit/list_view')
		->with('osh_audit',$osh_audit)
		->with('quaters',$quaters)
		->with('fatal', !empty($fatal) ? $fatal : 0)
		->with('lti', !empty($lti) ? $lti : 0)
		->with('workflowtrack',$workflowTrack)
		->with('initiate_wf', !empty($initiate_wf) ? $initiate_wf : 0)
		->with('non_conveyance', $non_conveyance);
	}

	public function returnQuaterAuditListDatatable($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater)
	{
		
		$query = $this->osh_audit_repository->returnQuaterAuditListForDatatable($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater);
		return Datatables::of($query)
		->addColumn('fatal', function($data) use($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater) {
			$notifications = $this->osh_audit_repository->returnQuaterNotificationIds($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater,$data->regno);
			return $notifications['fatal'];
		})
		->addColumn('lti', function($data) use($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater) {
			$notifications = $this->osh_audit_repository->returnQuaterNotificationIds($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater,$data->regno);
			return $notifications['lti'];
		})
		->addColumn('non_conveyance', function($data) use($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater) {
			$notifications = $this->osh_audit_repository->returnQuaterNotificationIds($osh_audit_period_id,$fatal,$lti,$non_conveyance,$quater,$data->regno);
			return $notifications['non_conveyance'];
		})
		->make(true);
	}


	public function returnSubmittedQuaterAuditListDatatable($osh_audit_period_id,$quater)
	{

		$query = $this->osh_audit_repository->returnSubmittedQuaterAuditListForDatatable($osh_audit_period_id,$quater);

		return Datatables::of($query)
		->addColumn('fatal', function($data) use ($quater) {
			$notifications = $this->osh_audit_repository->returnSubmittedQuaterNotificationIds($data->id,$quater);
			return count($notifications['fatal']);
		})
		->addColumn('lti', function($data) use ($quater) {
			$notifications = $this->osh_audit_repository->returnSubmittedQuaterNotificationIds($data->id,$quater);
			return count($notifications['lti']);
		})
		->addColumn('non_conveyance', function($data) use ($quater) {
			$notifications = $this->osh_audit_repository->returnSubmittedQuaterNotificationIds($data->id,$quater);
			return count($notifications['non_conveyance']);
		})
		->filterColumn('employer', function($data, $keyword) {
			return $data->whereRaw("employers.name) ilike ?", ["%{$keyword}%"]);
		})
		->filterColumn('regno', function($data, $keyword) {
			return $data->whereRaw("employers.reg_no) ilike ?", ["%{$keyword}%"]);
		})
		->make(true);
	}



	public function userAssignedList(UserOshAuditListDatatable $dataTable, Request $request)
	{ 
		
		$fin_years = $this->osh_audit_repository->returnFinYearsInitiated();
		return $dataTable->with('input', $request->all())
		->render('backend/workplace_risk_assesment/osh_audit/user_assigned_list',  
			[
				'fin_years' => $fin_years,
			]);
	}





}
