<?php

namespace App\Http\Controllers\Backend\WorkPlaceRiskAssesment;

use App\Http\Controllers\Controller;
use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Services\Workflow\Workflow;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshDataManagementRepository;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditPeriodRepository;
use App\Repositories\Backend\Finance\FinYearRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Reporting\DashboardRepository;
use App\DataTables\WorkPlaceRiskAssesment\OshAuditPeriodDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshEmployersReportSummaryDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshWorkPlaceReportDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshAuditEmployerSummaryDataTable;
use App\DataTables\WorkPlaceRiskAssesment\OshEmployerSummaryDatatable;
use App\DataTables\WorkPlaceRiskAssesment\OshSectorReportSummaryDataTable;
use App\DataTables\WorkPlaceRiskAssesment\OshSectorSummaryDataTable;
use App\DataTables\WorkPlaceRiskAssesment\OshCategorySummaryDataTable;
use App\DataTables\WorkPlaceRiskAssesment\OshEmployerCategoryReportSummaryDataTable;
use Illuminate\Support\Facades\Input;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use App\Http\Requests\Backend\Report\DateRangeRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Khill\Lavacharts\Lavacharts;
use Lava;
use Carbon\Carbon;



class OshReportController extends Controller
{
  protected $date_ranges;
  protected $incident_types;
  protected $regions;
  protected $businesses;
  protected $employers;
  protected $member_types;
  protected $insurances;
  protected $code_values;
  protected $districts;


  public function __construct(Request $request)
  {
   $this->date_ranges = new DateRangeRepository();

   if (request()->has("refresh_statistics")) {
    $value = request()->input("refresh_statistics");
    if ($value == 1) {
      $reportRepo = new ReportRepository();
      $reportRepo->refreshStatitics();
    }
  }

}


public function reportMenu()
{

 return view("backend/workplace_risk_assesment/osh_reports/report_menu");
}

public function tableReport(OshEmployersReportSummaryDatatable $dataTable)
{

 return $dataTable->render('backend/workplace_risk_assesment/osh_reports/osh_table_report');
}

public function oshEmployerSectorCategory(Request $request,OshSectorSummaryDataTable $sector,OshCategorySummaryDataTable $category, $report_id)
{
  // dd($report_id);
  $repo = new OshDataManagementRepository();
  $fin_years = $repo->finYears();
  $bussines_sectors = $repo->bussinesSectors();
  $categories = $repo->employerCategories();
  $workplaces = $repo->allworkplaces();
  switch ($report_id) {
    case 71:
    $dataTable = $sector;
    return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_reports/osh_sector_total_summary',[
      'fin_years' => $fin_years,
      'bussines_sectors' => $bussines_sectors
    ]);
    break;
    case 72:
    $dataTable = $category;
    return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_reports/osh_category_total_summary',[
      'fin_years' => $fin_years,
      'categories' => $categories
    ]);
    break;
    default:
    dd('not ready yet');
    break;
  }

}


public function oshEmployerReport(Request $request, OshAuditEmployerSummaryDataTable $audit,OshEmployerSummaryDatatable $employer,OshWorkPlaceReportDatatable $workplace, OshSectorReportSummaryDataTable $sector,OshEmployerCategoryReportSummaryDataTable $category, $report_id)
{
  // dd($report_id);
  $repo = new OshDataManagementRepository();
  $fin_years = $repo->finYears();
  $bussines_sectors = $repo->bussinesSectors();
  $categories = $repo->employerCategories();
  $workplaces = $repo->allworkplaces();
  switch ($report_id) {
    case 68:
    $dataTable = $audit;
    return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_reports/osh_audit_employer_report',['fin_years'=>$fin_years]);
    break;
    case 69:
    $dataTable = $employer;
    return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_reports/osh_employer_report_summary',['fin_years'=>$fin_years]);
    break;
    case 70:
    $dataTable = $workplace;
    return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_reports/osh_workplace_report',['fin_years'=>$fin_years,'workplaces'=>$workplaces]);
    break;
    case 71:
    $dataTable = $sector;
    return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_reports/osh_sector_report_summary',[
      'fin_years' => $fin_years,
      'bussines_sectors' => $bussines_sectors
    ]);
    break;
    case 72:
    $dataTable = $category;
    return $dataTable->with('request',$request->all())->render('backend/workplace_risk_assesment/osh_reports/osh_employer_category_report',[
      'fin_years' => $fin_years,
      'categories' => $categories
    ]);
    break;
    default:
    dd('not ready yet');
    break;
  }

}
public function graphicalReport(OshEmployersReportSummaryDatatable $dataTable)
{ 
  $dashboard = new DashboardRepository();
  $overall_claim_summary = $dashboard->getClaimSummary(1);
  $lost_days=$dashboard->lostTimeInjuries();
  $dataTable = Lava::DataTable();
  $data =DB::table('main.osh_employees_occupations')->select('occupation_name','idadi')->get()->pluck('occupation_name','idadi');
        // dd($data);
  $dataTable->addStringColumn('Employee')
  ->addNumberColumn('Occupations');           
  foreach($data as $key => $value){
    $dataTable->addRow([$value,$key]);
  }
  Lava::BarChart('MyStocks', $dataTable,[
    'title' => "Risk Employee by Occupations",
    'legend' => 'none',
    'is3D'=>false,
    'vAxis' => [
      'title'=>'Occupations'
    ],
    'hAxis' => [
      'title'=>'Number of cases'
    ],
    'height' => 400,
    'width' => 650      
  ]); 

//linechart graph

  $dataTable = Lava::DataTable();
  $dataTable->addStringColumn('Incident by Types')
  ->addNumberColumn('Percent')
  ->addRow(['Accidents', $overall_claim_summary['accident_overall_total']])
  ->addRow(['Deaths', $overall_claim_summary['death_overall_total']])
  ->addRow(['Diseases', $overall_claim_summary['disease_overall_total']]);
  Lava::PieChart('incident_types',  $dataTable, [
    'title'  => 'Distribution by incident types (Deaths,Accidents,Diseases)',
    'is3D'   => true,
    'height' => 400,
    'width' => 650, 
    'slices' => [
      ['offset' => 0.2],
      ['offset' => 0.25],
      ['offset' => 0.3]
    ]
  ]);


            //starting of audit summary

  $dataTable = Lava::DataTable();
  $data=DB::table('main.osh_incidents_locations')->select(DB::raw("fin_year,(fpremises+apremises) as onpremises,(fopremises+aopremises) as outpremises"))->orderby('fin_year','asc')->get()->toArray();
            //dd($data);
  $dataTable->addStringColumn('Year')
  ->addNumberColumn('Out of Premise')
  ->addNumberColumn('On Premise');
  foreach($data as $key => $value){
    $dataTable->addRow([$value->fin_year,$value->onpremises,$value->outpremises]);
  }
  Lava::LineChart('Temps', $dataTable,[
    'title'=> "Notifications by location trends(On premises and Out of premises)",
    'legend' => 'none',
    'width'=>800,
    'height'=>400,
    'vAxis' => [
      'title'=>'Number of claims'
    ],
    'hAxis' => [
      'title'=>'Financial Years'
    ],

  ]); 

  return view("backend/workplace_risk_assesment/osh_reports/osh_graphical_report")->with('overall_claim_summary',$overall_claim_summary)->with('lost_days',$lost_days);
}

}
