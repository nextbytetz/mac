<?php

namespace App\Http\Controllers\Backend\Access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Access\RoleRepository;

class RoleTableController extends Controller
{
    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @param RoleRepository $roles
     */
    public function __construct(RoleRepository $roles)
    {
        $this->roles = $roles;
    }

    public function __invoke()
    {
        return Datatables::of($this->roles->getForDataTable())
            ->addColumn('permissions', function ($role) {
                if ($role->all_functions == 1)
                    return '<span class="tag tag-success">'.trans('labels.general.all').'</span>';
                return (($role->permissions()->count() > 0) ?
                    "........" :
                    '<span class="tag tag-danger">'.trans('labels.general.none').'</span>');
            })
            ->rawColumns(['permissions',])
            ->make(true);
    }

}
