<?php

namespace App\Http\Controllers\Backend\Access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Access\UserRepository;


class UserTableController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function __invoke()
    {
        return Datatables::of($this->users->getForDataTable())
            ->addColumn('roles', function($user) {
                return $user->role_label;
            })
            ->rawColumns(['roles',])
            ->make(true);
    }

}
