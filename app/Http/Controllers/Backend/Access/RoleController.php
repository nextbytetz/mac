<?php

namespace App\Http\Controllers\Backend\Access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Access\Role;
use App\Repositories\Backend\Access\RoleRepository;
use App\Repositories\Backend\Access\PermissionRepository;
use App\Repositories\Backend\Access\PermissionGroupRepository;
use App\Http\Requests\Backend\Access\UpdateRoleRequest;
use App\Http\Requests\Backend\Access\StoreRoleRequest;

class RoleController extends Controller
{

    /**
     * @var RoleRepositoryContract
     */
    protected $roles;

    /**
     * @var PermissionGroupRepository
     */
    protected $groups;

    /**
     * @param RoleRepository  $roles
     * @param PermissionGroupRepository $groups
     */
    public function __construct( RoleRepository $roles,PermissionGroupRepository $groups) {
        $this->middleware('access.routeNeedsPermission:manage_role_permissions', ['except' => ['index','show']]);
        $this->roles = $roles;
        $this->groups = $groups;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.access.roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.access.roles.create')
            ->with("groups", $this->groups->getAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRoleRequest $request
     * @return mixed
     */
    public function store(StoreRoleRequest $request)
    {
        $this->roles->create($request->all());
        return redirect()->route('backend.role.index')->withFlashSuccess(trans('alerts.backend.roles.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $role
     * @return mixed
     */
    public function edit(Role $role)
    {
        return view('backend.access.roles.edit')
            ->withRole($role)
            ->withRolePermissions($role->permissions->pluck('id'))
            ->withGroups($this->groups->getAll());
    }

    /**
     *  Update the specified resource in storage.
     *
     * @param Role $role
     * @param UpdateRoleRequest $request
     * @return mixed
     */
    public function update(Role $role, UpdateRoleRequest $request)
    {
        $this->roles->update($role, $request->all());
        return redirect()->route('backend.role.edit', $role->id)->withFlashSuccess(trans('alerts.backend.roles.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @return mixed
     */
    public function destroy(Role $role)
    {
        $this->roles->destroy($role);
        return redirect()->route('backend.role.index')->withFlashSuccess(trans('alerts.backend.roles.deleted'));
    }
}
