<?php

namespace App\Http\Controllers\Backend\Access;

use App\Http\Requests\Backend\Access\PostMySustituteRequest;
use App\Repositories\Backend\Sysdef\OfficeRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\RoleRepository;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Access\PermissionRepository;
use App\Models\Auth\User;
use App\Http\Requests\Backend\Access\UpdateUserRequest;
use App\Http\Requests\Backend\Access\StoreUserRequest;
use App\Repositories\Backend\Sysdef\DesignationRepository;
use App\Repositories\Backend\Sysdef\UnitRepository;

/**
 * Class UserController
 * @package App\Http\Controllers\Backend\Access
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @var PermissionRepository
     */
    protected $permissions;

    /**
     * UserController constructor.
     * @param UserRepository $users
     * @param RoleRepository $roles
     * @param PermissionRepository $permissions
     */
    public function __construct(UserRepository $users, RoleRepository $roles, PermissionRepository $permissions)
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->permissions = $permissions;
    }

    public function index()
    {
        return view('backend.access.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     */
    public function store(StoreUserRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return mixed
     */
    public function edit(User $user)
    {
        $units = new UnitRepository();
        $offices = new OfficeRepository();
        $designations = new DesignationRepository();
        return view('backend.access.edit')
        ->with("user", $user)
        ->with("user_roles", $user->roles->pluck('id')->all())
        ->with("user_permissions", $user->permissions->pluck('id')->all())
        ->with("roles", $this->roles->getAll())
        ->with("permissions", $this->permissions->getAllPermissions())
        ->with("offices", $offices->getAll()->pluck('name', 'id'))
        ->with("units", $units->getAll()->pluck('name', 'id'))
        ->with("designations", $designations->getAll()->pluck('name', 'id'))
        ->with("alternative_approvers", (new UserRepository())->query()->where("id", "<>", $user->id)->get()->pluck("name", "id"))
        ->with("alternative_approvers_values", $user->alternatives()->pluck("alternative"));
    }

    /**
     * @param User $user
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $this->users->update($user, ['permissions' => $request->only('permission_user'), 'roles' => $request->only('assignees_roles'), 'data' => $request->except('permission_user', 'assignees_roles')]);
        return redirect()->route('backend.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     */
    public function destroy(User $user)
    {

    }

    public function search()
    {
        return $this->users->getStaffs(request()->input('q'));
    }

    public function getSubstitute(User $user)
    {
        if (!$user->available) {
            return redirect()->back()->withFlashWarning("User not available, can not make substitution");
        }
        return view("backend/access/substitute/index")
        ->with("user", $user)
        ->with("users", $this->users->query()->get()->pluck('name', 'id'));
    }

    public function postSubstitute(PostMySustituteRequest $request)
    {
        $output = $this->users->postSubstitute($request->all());
        switch ($output) {
            case 1:
                //Substituted by another user
                //return redirect()->back()->withFlashSuccess("Success, User has been substituted ...");
            return redirect()->route("backend.user.edit", $request->input("user_id"))->withFlashSuccess("Success, User has been substituted ...");
            default:
                //access()->logout();
            return redirect('/')->withFlashSuccess("Success, Substitute user has been enabled ... ");
        }
    }

    public function revokeSubstitute(User $user)
    {
        return $this->revokeSub($user);
    }

    public function selfRevokeSubstitute(User $user)
    {
        if (!($user->id == access()->id())) {
            return redirect()->back()->withFlashWarning("Action not allowed ...");
        }
        return $this->revokeSub($user);
    }

    private function revokeSub(Model $user)
    {
        $output = $this->users->revokeSubstitute($user);
        return redirect()->back()->withFlashSuccess("Success, User substitution has been revoked ...");
    }

}