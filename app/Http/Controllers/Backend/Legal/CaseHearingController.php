<?php

namespace App\Http\Controllers\Backend\Legal;

use App\Http\Controllers\Controller;
use App\Models\Legal\CaseHearing;
use App\Models\Legal\Cases;
use App\Models\Legal\Lawyer;
use App\Repositories\Backend\Legal\CaseHearingRepository;
use App\Http\Requests\Backend\Legal\StoreCaseHearingRequest;
use App\Repositories\Backend\Legal\CasePersonnelRepository;
use App\Repositories\Backend\Legal\CaseRepository;
use App\Repositories\Backend\Legal\LawyerRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

/**
 * Class CaseHearingController
 * @package App\Http\Controllers\Backend\Legal
 */
class CaseHearingController extends Controller
{

    /**
     * @var CaseHearingRepository
     */
    protected $hearings;

    public function __construct(CaseHearingRepository $hearings)
    {
        $this->hearings = $hearings;
        $this->middleware('access.routeNeedsPermission:create_case_proceeding', ['only' => ['create']]);
        $this->middleware('access.routeNeedsPermission:update_case_proceeding', ['only' => ['edit']]);
        $this->middleware('access.routeNeedsPermission:delete_case_proceeding', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Cases $case
     * @return mixed
     */
    public function create(Cases $case)
    {
        $maxYear = Carbon::now()->format('Y') + sysdefs()->data()->case_years_period;
        $personnel = new CasePersonnelRepository();
        $lawyers = new LawyerRepository();
        $lawyers_firm = $lawyers->getFirmsForInput();
        $lawyers_individual = $lawyers->query()->whereNull("employer_id")->get()->pluck("name_label", "id");
        return view('backend.legal.case.hearing.create')
            ->withCases($case)
            ->withLawyersIndividual($lawyers_individual)
            ->withLawyersFirm($lawyers_firm)
            ->withMaxYear($maxYear)
            ->withPersonnels($personnel->getAll()->pluck("name", "id")->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCaseHearingRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCaseHearingRequest $request)
    {
        $hearing = $this->hearings->store($request->all());
        return response()->json(['success' => true, 'id' => $hearing->id]);
    }


    /**
     * Display the specified resource.
     *
     * @param CaseHearing $hearing
     * @return mixed
     */
    public function show(CaseHearing $hearing)
    {
        return view("backend/legal/case/hearing/show")
            ->withHearing($hearing);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CaseHearing $hearing
     * @return mixed
     */
    public function edit(CaseHearing $hearing)
    {
        $maxYear = Carbon::now()->format('Y') + sysdefs()->data()->case_years_period;
        $personnel = new CasePersonnelRepository();
        $lawyers = new LawyerRepository();
        $lawyers_firm = $lawyers->getFirmsForInput();
        /*dd($lawyers_firm);*/
        $lawyers_individual = $lawyers->query()->whereNull("employer_id")->get()->pluck("name_label", "id");
        $filling_date = ($hearing->filling_date) ? Carbon::parse($hearing->filling_date) : null;
        $hearing_date = ($hearing->hearing_date) ? Carbon::parse($hearing->hearing_date) : null;
        $category = ($hearing->lawyer->employer_id) ? 1 : 0;
        $hearing_time = Carbon::parse($hearing->hearing_time);
        return view('backend/legal/case/hearing/edit')
            ->withFillingDate($filling_date)
            ->withHearingDate($hearing_date)
            ->withHearingTime($hearing_time)
            ->withCategory($category)
            ->withHearing($hearing)
            ->withCases($hearing->cases)
            ->withLawyersIndividual($lawyers_individual)
            ->withLawyersFirm($lawyers_firm)
            ->withMaxYear($maxYear)
            ->withPersonnels($personnel->getAll()->pluck("name", "id")->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CaseHearing $hearing
     * @param StoreCaseHearingRequest $request
     * @return mixed
     */
    public function update(CaseHearing $hearing, StoreCaseHearingRequest $request)
    {
        $this->hearings->update($hearing, $request->all());
        return response()->json(['success' => true, 'id' => $hearing->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CaseHearing $hearing
     * @return mixed
     */
    public function destroy(CaseHearing $hearing)
    {
        $id = $hearing->cases->id;
        $this->hearings->destroy($hearing);
        return redirect()->route('backend.legal.case.show', $id)->withFlashSuccess(trans('alerts.backend.case.hearing.deleted'));
    }

    /**
     * @return mixed
     */
    public function get($id){
        return Datatables::of($this->hearings->getForDataTable($id))
            ->editColumn('lawyer_id', function($case_hearing) {
                return $case_hearing->lawyer->firstname . ' ' . $case_hearing->lawyer->othernames;
            })
            ->editColumn('status', function($case_hearing) {
                return $case_hearing->status_label;
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function getNext()
    {
        //getNextForDataTable
        return Datatables::of($this->hearings->getNextForDataTable())
            ->editColumn('lawyer_id', function($case_hearing) {
                if (is_null($case_hearing->lawyer->employer_id)) {
                    $return = $case_hearing->lawyer->firstname . ' ' . $case_hearing->lawyer->othernames;
                } else {
                    $return = $case_hearing->lawyer->employer->name;
                }
                return $return;
            })
            ->editColumn('status', function($case_hearing) {
                return $case_hearing->status_label;
            })
            ->addColumn("case_title", function($case_hearing) {
                return $case_hearing->cases->title;
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function getLawyer($id){

        return Datatables::of($this->hearings->getLawyerHearingForDataTable($id))
            ->editColumn('status', function($hearing) {
                return $hearing->status_label;
            })
            ->addColumn("title", function ($hearing) {
                return $hearing->cases->title;
            })
            ->addColumn("hearing_date_label", function ($hearing) {
                return $hearing->hearing_date_label;
            })
            ->addColumn("fee_label", function ($hearing) {
                return $hearing->fee_label;
            })
            ->rawColumns(['status'])
            ->make(true);

    }

}
