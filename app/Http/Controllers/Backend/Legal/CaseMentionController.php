<?php

namespace App\Http\Controllers\Backend\Legal;

use App\Http\Requests\Backend\Legal\StoreCaseMentionRequest;
use App\Models\Legal\CaseMention;
use App\Models\Legal\Cases;
use App\Repositories\Backend\Legal\CaseMentionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Repositories\Backend\Legal\CasePersonnelRepository;
use App\Repositories\Backend\Legal\LawyerRepository;
use Yajra\Datatables\Datatables;

class CaseMentionController extends Controller
{
    protected $mentions;

    public function __construct(CaseMentionRepository $mentions)
    {
        $this->mentions = $mentions;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Cases $case
     * @return mixed
     */
    public function create(Cases $case)
    {
        $maxYear = Carbon::now()->format('Y') + sysdefs()->data()->case_years_period;
        $personnel = new CasePersonnelRepository();
        $lawyers = new LawyerRepository();
        $lawyers_firm = $lawyers->getFirmsForInput();
        $lawyers_individual = $lawyers->query()->whereNull("employer_id")->get()->pluck("name_label", "id");
        return view('backend/legal/case/mention/create')
            ->withCases($case)
            ->withLawyersIndividual($lawyers_individual)
            ->withLawyersFirm($lawyers_firm)
            ->withMaxYear($maxYear)
            ->withPersonnels($personnel->getAll()->pluck("name", "id")->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCaseMentionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCaseMentionRequest $request)
    {
        $mention = $this->mentions->store($request->all());
        return response()->json(['success' => true, 'id' => $mention->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param CaseMention $mention
     * @return mixed
     */
    public function show(CaseMention $mention)
    {
        return view("backend/legal/case/mention/show")
            ->withMention($mention);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CaseMention $mention
     * @return mixed
     */
    public function edit(CaseMention $mention)
    {
        $maxYear = Carbon::now()->format('Y') + sysdefs()->data()->case_years_period;
        $personnel = new CasePersonnelRepository();
        $lawyers = new LawyerRepository();
        $lawyers_firm = $lawyers->getFirmsForInput();
        $lawyers_individual = $lawyers->query()->whereNull("employer_id")->get()->pluck("name_label", "id");
        $mention_date = ($mention->mention_date) ? Carbon::parse($mention->mention_date) : null;
        $category = ($mention->lawyer->employer_id) ? 1 : 0;
        $mention_time = Carbon::parse($mention->mention_time);
        return view('backend/legal/case/mention/edit')
            ->withMentionDate($mention_date)
            ->withMentionTime($mention_time)
            ->withCategory($category)
            ->withMention($mention)
            ->withCases($mention->cases)
            ->withLawyersIndividual($lawyers_individual)
            ->withLawyersFirm($lawyers_firm)
            ->withMaxYear($maxYear)
            ->withPersonnels($personnel->getAll()->pluck("name", "id")->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CaseMention $mention
     * @param StoreCaseMentionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CaseMention $mention, StoreCaseMentionRequest $request)
    {
        $this->mentions->update($mention, $request->all());
        return response()->json(['success' => true, 'id' => $mention->id]);
    }

    /**
     *  Remove the specified resource from storage.
     *
     * @param CaseMention $mention
     * @return mixed
     */
    public function destroy(CaseMention $mention)
    {
        $id = $mention->cases->id;
        $this->mentions->destroy($mention);
        return redirect()->route('backend.legal.case.show', $id)->withFlashSuccess(trans('alerts.backend.case.mentioning.deleted'));
    }

    /**
     * @return mixed
     */
    public function get($id) {
        return Datatables::of($this->mentions->getForDataTable($id))
            ->addColumn("lawyer", function ($mention) {
                return $mention->lawyer->name;
            })
            ->make(true);
    }

    public function getLawyer($id){

        return Datatables::of($this->mentions->getLawyerHearingForDataTable($id))
            ->addColumn("title", function ($mentions) {
                return $mentions->cases->title;
            })
            ->addColumn("mention_date_label", function ($mentions) {
                return $mentions->mention_date_label;
            })
            ->addColumn("mention_time_label", function ($mentions) {
                return $mentions->mention_time_label;
            })
            ->make(true);

    }

}
