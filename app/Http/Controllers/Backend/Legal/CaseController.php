<?php

namespace App\Http\Controllers\Backend\Legal;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Legal\UpdateCaseRequest;
use App\Models\Legal\Cases;
use App\Models\Legal\CourtCategory;
use App\Models\Legal\District;
use App\Models\Legal\Lawyer;
use App\Http\Requests\Backend\Legal\StoreCaseRequest;
use App\Repositories\Backend\Legal\LawyerRepository;
use App\Repositories\Backend\Legal\CaseRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

/**
 * Class CaseController
 * @package App\Http\Controllers\Backend\Legal
 */
class CaseController extends Controller
{
    protected $cases;

    public function __construct(CaseRepository $cases) {
        $this->cases = $cases;
        $this->middleware('access.routeNeedsPermission:create_case', ['only' => ['create']]);
        $this->middleware('access.routeNeedsPermission:edit_case', ['only' => ['edit']]);
        $this->middleware('access.routeNeedsPermission:delete_case', ['only' => ['destroy']]);
        $this->middleware('access.routeNeedsPermission:close_case', ['only' => ['close']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.legal.case.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maxYear = Carbon::now()->format('Y') + sysdefs()->data()->case_years_period;
        $districts = District::query()->get();
        $categories = CourtCategory::query()->get();
        return view('backend.legal.case.create')
            ->withCourtCategories($categories->pluck('name','id'))
            ->withDistricts($districts->pluck('name','id'))
            ->withMaxYear($maxYear)
            ->withCases($this->cases->getAll()->pluck("title", "id")->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCaseRequest $request
     * @return mixed
     */
    public function store(StoreCaseRequest $request)
    {
        $id = $this->cases->store($request->all());
        return redirect()->route('backend.legal.case.show', $id)->withFlashSuccess(trans('alerts.backend.case.added'));
    }

    public function addNewLawyer(Request $request){

        $lawyer= new lawyer;
        $lawyer ->firstname= $request->firstname;
        $lawyer ->othernames= $request->othernames;
        $lawyer ->external_id= $request->external_id;
        $lawyer->save();
        return redirect()->route('backend.legal.case.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Cases $case
     * @return mixed
     */
    public function show(Cases $case)
    {
        return view('backend.legal.case.show')
            ->withCases($case);
    }

    /**
     * Show the form for editing the specified resource.
     * @param Cases $case
     * @return mixed
     */
    public function edit(Cases $case)
    {
        $maxYear = Carbon::now()->format('Y') + sysdefs()->data()->case_years_period;
        $districts = District::query()->get();
        $categories = CourtCategory::query()->get();
        $filling_date = ($case->filling_date) ? Carbon::parse($case->filling_date) : null;
        return view('backend.legal.case.edit')
                ->withCases($case)
                ->withCourtCategories($categories->pluck('name','id'))
                ->withDistricts($districts->pluck('name','id'))
                ->withMaxYear($maxYear)
                ->withFillingDate($filling_date)
                ->withOtherCases($this->cases->query()->where("id", "<>", $case->id)->get()->pluck("title", "id")->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Cases $case
     * @param UpdateCaseRequest $request
     * @return mixed
     */
    public function update(Cases $case, UpdateCaseRequest $request)
    {
        $this->cases->update($case, $request->all());
        return redirect()->route('backend.legal.case.show', $case->id)->withFlashSuccess(trans('alerts.backend.case.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cases $case
     * @return mixed
     */
    public function destroy(Cases $case)
    {
        $this->cases->destroy($case);
        return redirect()->route('backend.legal.case.index')->withFlashSuccess(trans('alerts.backend.case.deleted'));
    }

    /**
     * Close the case.
     *
     * @param Cases $case
     * @return mixed
     */
    public function close(Cases $case)
    {
        $this->cases->close($case);
        return redirect()->route('backend.legal.case.show', $case->id)->withFlashSuccess(trans('alerts.backend.case.closed'));
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return Datatables::of($this->cases->getForDataTable())
            ->editColumn("fee", function ($case) {
                return $case->fee_formatted;
            })
            ->make(true);
    }

}
