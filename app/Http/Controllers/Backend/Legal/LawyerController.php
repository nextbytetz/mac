<?php

namespace App\Http\Controllers\Backend\Legal;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Legal\UpdateLawyerRequest;
use App\Models\Legal\Lawyer;
use App\Repositories\Backend\Legal\LawyerRepository;
use App\Http\Requests\Backend\Legal\StoreLawyerRequest;
use Yajra\Datatables\Datatables;

class LawyerController extends Controller
{

    protected $lawyers;

    public function __construct(LawyerRepository $lawyers)
    {
        $this->lawyers = $lawyers;
        $this->middleware('access.routeNeedsPermission:create_lawyer', ['only' => ['create']]);
        $this->middleware('access.routeNeedsPermission:edit_lawyer', ['only' => ['edit']]);
        $this->middleware('access.routeNeedsPermission:delete_lawyer', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.legal.lawyer.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.legal.lawyer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLawyerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreLawyerRequest $request)
    {
        $input = $request->all();
        $lawyer = $this->lawyers->store($input);
        return response()->json(['success' => true, 'id' => $lawyer->id]);
    }


    /**
     * Display the specified resource.
     *
     * @param Lawyer $lawyer
     * @return mixed
     */
    public function show(Lawyer $lawyer)
    {
        return view('backend.legal.lawyer.show')
            ->withLawyer($lawyer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Lawyer $lawyer
     * @return mixed
     */
    public function edit(Lawyer $lawyer)
    {
        $category = ($lawyer->employer_id) ? 1 : 0;
        return view("backend/legal/lawyer/edit")
            ->withLawyer($lawyer)
            ->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Lawyer $lawyer
     * @param UpdateLawyerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Lawyer $lawyer, UpdateLawyerRequest $request)
    {
        $this->lawyers->update($lawyer, $request->all());
        return response()->json(['success' => true, 'id' => $lawyer->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Lawyer $lawyer
     */
    public function destroy(Lawyer $lawyer)
    {
        $this->lawyers->destroy($lawyer);
        return redirect()->route('backend.legal.lawyer.index')->withFlashSuccess(trans('alerts.backend.case.lawyers.deleted'));
    }

    public function get()
    {
        return Datatables::of($this->lawyers->getForDataTable())
            ->addColumn('case_hearings', function($lawyer) {
                return $lawyer->caseHearings()->count();
            })
            ->addColumn('case_mentionings', function($lawyer) {
                return $lawyer->caseMentionings()->count();
            })
            ->addColumn('name', function($lawyer) {
                return $lawyer->name;
            })
            ->make(true);
    }
}
