<?php
namespace App\Http\Controllers\Backend\Report;

use App\DataTables\Report\Finance\CancelledReceiptDataTable;
use App\DataTables\Report\Finance\DishonouredChequesDataTable;
use App\DataTables\Report\Finance\PaymentVouchersDataTable;
use App\DataTables\Report\Finance\ReceiptByFinanceCodeDataTable;
use App\DataTables\Report\Finance\ReceiptsGeneralReportDataTable;
use App\Models\Sysdef\DateRange;
use App\DataTables\Report\Finance\ReceiptsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Report\DateRangeRequest;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Finance\FinCodeRepository;
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use Carbon\Carbon;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\DataTables\Report\Finance\ReceiptPerformanceDataTable;
use App\Repositories\Backend\Reporting\ReportRepository;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use App\DataTables\Report\Finance\ControlNoDatatable;
use App\DataTables\Report\Finance\successfullyCNReconciliationDataTable;

class FinanceReportController extends Controller
{
    protected $receipts;
    protected $banks;
    protected $payment_types;
    protected $date_ranges;
    protected $employers;
    protected $employees;
    protected $insurances;
    protected $dependents;
    protected $pensioners;
    protected $member_types;
    protected $regions;
    protected $fin_codes;

    public function __construct(ReceiptRepository $receipts,ReportRepository $reports) {
        $this->receipts = $receipts;
        $this->reports = $reports;
        $this->banks = new BankRepository();
        $this->payment_types = new PaymentTypeRepository();
        $this->date_ranges= new DateRangeRepository();

        $this->employees = new EmployeeRepository();
        $this->employers = new EmployerRepository();
        $this->dependents = new DependentRepository();
        $this->insurances = new InsuranceRepository();
        $this->pensioners = new PensionerRepository();
        $this->member_types = new MemberTypeRepository();
        $this->regions =new RegionRepository();
        $this->fin_codes = new FinCodeRepository();
    }


    public function cancelledReceipt(CancelledReceiptDataTable $dataTable,DateRangeRequest $request){

        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/finance/cancelled_receipts', ["request" => $request]);


    }


    public function receiptByFinanceCode(ReceiptByFinanceCodeDataTable $dataTable,DateRangeRequest $request){

        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/finance/receipt_finance_codes', ["request" => $request]);

    }

    public function getReceipts(ReceiptsDataTable $dataTable, DateRangeRequest $request){

        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null, 'input'=>$date_range])->render('backend/report/finance/receipts', ["request" => $request, 'banks' => $this->banks->getAll()->pluck('name', 'id'), 'payment_types' => $this->payment_types->getAll()->pluck('name', 'id'),'regions' => $this->regions->getAll()->pluck('name', 'id'), 'fin_codes' => $this->fin_codes->getAll()->pluck('name', 'id'), 'total_amount' => $dataTable->getTotalAmountPaid()]);
    }

    public function receiptGeneralReport(ReceiptsGeneralReportDataTable $dataTable, DateRangeRequest $request)
    {
        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null, 'input'=>$date_range])->render('backend/report/finance/receipts_general_report', ["request" => $request, 'banks' => $this->banks->getAll()->pluck('name', 'id'), 'payment_types' => $this->payment_types->getAll()->pluck('name', 'id'),'regions' => $this->regions->getAll()->pluck('name', 'id'), 'fin_codes' => $this->fin_codes->getAll()->pluck('name', 'id'), 'total_amount' => $dataTable->getTotalAmountPaid()]);
    }


    public function dishonouredCheques(DishonouredChequesDataTable $dataTable,DateRangeRequest $request){

        $date_range=  $this->date_ranges->dateRange($request->all());

         return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/finance/dishonoured_cheques', ["request" => $request]);

    }

    public function receiptPerformance( ReceiptPerformanceDataTable $dataTable, DateRangeRequest $request )
{

    $date_range = $this->date_ranges->dateRange($request->all());

    return $dataTable->with([
        'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
        'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,
    ])->render('backend/report/finance/receipt_performance', ["request" => $request]);

}


    public function paymentVouchers( PaymentVouchersDataTable $dataTable, DateRangeRequest $request )
    {

        $date_range = $this->date_ranges->dateRange($request->all());

        return $dataTable->with([ 'input'=>$date_range ])->render('backend/report/finance/payment_vouchers', ["request" => $request, 'insurances' => $this->insurances->getAll()->pluck('name', 'id'), 'member_types' => $this->member_types->query()->where('id','<>',6)->get()->pluck('name', 'id'), 'total_amount' => $dataTable->findTotalAmount()]);



    }



    public function generalReceiptSummary(DateRangeRequest $request)
    {
        //
        return view('backend.report.finance.general_receipt_summary')
            ->with("request", $request)
            ->with("amount_collected", $this->receipts->getAmountCollectedDateRange($request))
            ->with("receipts_count", $this->receipts->countReceiptsDateRange($request))
            ->with("cancelled_receipts_count", $this->receipts->countCancelledReceiptsDateRange($request))
            ->with("dishonoured_cheques", $this->receipts->countDishonouredChequesDateRange($request))
            ->with("banks", $this->banks->getAll()->pluck('name', 'id'));
    }

public function successfullyCNReport(ControlNoDatatable $dataTable, DateRangeRequest $request)
{
 $date_range=  $this->date_ranges->dateRange($request->all());

return $dataTable->with([
'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
'to_date' =>(isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])
->render('backend.report.finance.successfullyCN_receipts', ["request" => $request]);


}

public function dailyReconciliation(successfullyCNReconciliationDataTable $dataTable, DateRangeRequest $request)
{
 $date_range=  $this->date_ranges->dateRange($request->all());
return $dataTable->with([
'from_date' =>(isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
'to_date' =>(isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])
->render('backend.report.finance.successfullyCNRecon_receipts', ["request" => $request]);


}




}