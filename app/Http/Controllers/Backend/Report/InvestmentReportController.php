<?php

namespace App\Http\Controllers\Backend\Report;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;
use App\Repositories\Backend\Investiment\InvestimentBudgetRepository;
use App\DataTables\Report\Investment\CorporateBondDataTable;
use App\DataTables\Report\Investment\DividentScheduleDataTable;
use App\DataTables\Report\Investment\YrsTreasuryBondDataTable;
use App\DataTables\Report\Investment\FixedDepositDataTable;
use App\DataTables\Report\Investment\InterestCallAccountDataTable;
use App\DataTables\Report\Investment\ListedEquitiesDataTable;
use App\DataTables\Report\Investment\TreasuryBillDataTable;
use App\DataTables\Report\Investment\TreasuryBondDataTable;
use App\DataTables\Report\Investment\UTTBondFundDataTable;
use App\DataTables\Report\Investment\MarketValueChangeDataTable;
use App\DataTables\Report\Investment\InvestmentPortfolioDataTable;
use App\DataTables\Report\Investment\ComplianceListedStockDataTable;
use App\DataTables\Report\Investment\ComplianceDepositHoldingDataTable;
use App\DataTables\Report\Investment\BudgetActualAmountDataTable;
use App\DataTables\Report\Investment\MaturingActualExpectedDataTable;
use App\DataTables\Report\Investment\IncomeFromInvestmentDataTable;
use App\DataTables\Report\Investment\AverageReturnFundDataTable;
use App\DataTables\Report\DateRangeRepository;
use App\DataTables\WorkflowTrackDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;
use Khill\Lavacharts\Lavacharts;
use Carbon\Carbon;
use Validator;
use Log;
use Lava;

use App\Models\Finance\FinYear;
use App\Models\Investment\InvestmentBudget;
use App\Models\Investment\InvestmentBudgetAllocation;

class InvestmentReportController extends Controller
{
	protected $date_ranges;
	protected $Investiment_type;
	protected $code_values; 

	public function __construct() {
		// $this->date_ranges = new DateRangeRepository();
		$this->investment_type =new InvestimentTypeRepository();
		$this->code_values = new CodeValueRepository();
	}

	public function corporateBond(Request $request, CorporateBondDataTable $dataTable) 
	{
	  return $dataTable->with('request',$request->all())->render('backend/report/investment/corporate_bond_schedule');
	} 
	public function dividentSchedule(DividentScheduleDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/divident_schedule');
	}
    public function yrsTreasuryBond(YrsTreasuryBondDataTable $dataTable)
	{
	  return  $dataTable->render('backend/report/investment/yrs_treasury_bond_schedule');
	}
	public function fixedDeposit(FixedDepositDataTable $dataTable)
	{
		return $dataTable->render("backend/report/investment/fixed_deposits_schedule");
	}
	public function interestCallAccount(InterestCallAccountDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/interest_call_account');
	}
	public function listedEquities(ListedEquitiesDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/listed_equities');
	}
	public function treasuryBill(Request $request, TreasuryBillDataTable $dataTable)
	{
		return $dataTable->with('request',$request->all())->render('backend/report/investment/treasury_bill_schedule');
	}

	public function treasuryBond(Request $request, TreasuryBondDataTable $dataTable)
	{
	    return $dataTable->with('request',$request->all())->render('backend/report/investment/treasury_bond_schedule');
	}
	public function uttBondFund(UTTBondFundDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/utt_bond_fund');
	}
	public function marketValueChange(MarketValueChangeDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/market_value_changes');
	}
	public function investmentPortfolio(InvestmentPortfolioDataTable $dataTable)
	{
        return $dataTable->render('backend/report/investment/investment_portfolio');
	}
	public function complianceListedStock(ComplianceListedStockDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/compliance_listed_stock');
	}
	public function complianceDepositHolding(ComplianceDepositHoldingDataTable $dataTable)
	{
		return  $dataTable->render('backend/report/investment/compliance_deposit_holding'); 
	}
	public function budgetActualAmount(BudgetActualAmountDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/budget_actual_amount');
	}
	public function maturingActualExpected(MaturingActualExpectedDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/budget_actual_amount');
	}
	public function incomeFromInvestment(IncomeFromInvestmentDataTable $dataTable)
	{
		return $dataTable->render('backend/report/investment/maturing_actual_expected_maturing');
	}
	public function averageReturnFund(AverageReturnFundDataTable $dataTable)
	{
        return $dataTable->render('backend/report/investment/average_return_fund');
	}
}


        

       
        
      