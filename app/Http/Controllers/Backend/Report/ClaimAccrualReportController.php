<?php

namespace App\Http\Controllers\Backend\Report;

use App\DataTables\Report\Claim\NotificationReportInitialExpenseDataTable;
use App\DataTables\Report\Claim\NotificationReportReceivedDataTable;
use App\DataTables\Report\Claim\ProcessedClaimsDataTable;
use App\DataTables\Report\Compliance\BookingContributionDataTable;
use App\DataTables\Report\Compliance\InterestRaisedPaidVarianceDataTable;
use App\DataTables\Report\Compliance\LoadContributionAgingDataTable;
use App\DataTables\Report\Compliance\NewEmployeesDataTable;
use App\DataTables\Report\Compliance\StaffLoadContributionPerformanceDataTable;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\BusinessRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Report\DateRangeRequest;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;
use App\DataTables\Report\Claim\ClaimAccrualReportDataTable;
use App\DataTables\Report\Claim\ClaimAccrualGeneralDataTable;
use App\DataTables\Report\Claim\ClaimAccrualReturnedDfpiDataTable;
use App\DataTables\Report\Claim\ClaimAccrualBookingDataTable;
use App\DataTables\Report\Claim\ClaimAccrualAssessmentDataTable;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use App\Repositories\Backend\Sysdef\RegionRepository;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;

class ClaimAccrualReportController extends Controller
{
	protected $date_ranges;
	protected $incident_types;
	protected $regions;
	protected $businesses;
	protected $employers;
	protected $member_types;
	protected $insurances;
	protected $code_values;
	protected $districts;

	public function __construct() {

		$this->date_ranges = new DateRangeRepository();
		$this->incident_types = new IncidentTypeRepository();
		$this->regions = new RegionRepository();
		$this->member_types = new MemberTypeRepository();
		$this->insurances = new InsuranceRepository();
		$this->code_values = new CodeValueRepository();
		$this->districts = new DistrictRepository();

	}

    /**
     * @param NotificationReportReceivedDataTable $dataTable
     * @param DateRangeRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */

    /*Accrual report  expenses*/
    // public function claimAccrualReportBooking(ClaimAccrualBookingDataTable $dataTable, DateRangeRequest $request )
    // {
    // 	$input = $request->all();
    // 	$search_flag = array_key_exists('search_flag', $input) ? $input['search_flag'] : 0;

    // 	if(($search_flag == 1)) {
    // 		$date_range = $this->date_ranges->dateRange($request->all());

    // 		return $dataTable->with([
    // 			'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
    // 			'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null, 'input' => $date_range])->render('backend.operation.report.claim.claim_accrual_report', $with_binding);

    // 	}else{
    // 		return $dataTable->render('backend.report.claim.accrued_booking');
    // 	}
    // }
    public function claimAccrualReportAssessment(ClaimAccrualAssessmentDataTable $dataTable)
    {
    	$accrual = new AccrualNotificationReportRepository();
    	// $employer_name = $accrual->employer();
    	return $dataTable->render('backend.report.claim.claim_accrual.accrued_assessment_report');
    	// return view('backend.report.claim.accrued_assessment_report',$with_binding);
    }
    public function returnedDasReport(ClaimAccrualReturnedDasDataTable $dataTable)
    {

    	return $dataTable->render('backend.report.claim.claim_accrual.returned_accrued_assessment');
    }
    public function returnedDFPIReport(ClaimAccrualReturnedDfpiDataTable $dataTable)
    {

    	return $dataTable->render('backend.report.claim.claim_accrual.un_booked_claim');
    }
    public function generalClaimAccrualReport(ClaimAccrualGeneralDataTable $dataTable, Request $request)
    {
      // dd($request->all());
     return $dataTable->render('backend.report.claim.claim_accrual.claim_accrual_report',$with_binding,[
        "regions" => (new RegionRepository())->query()->pluck("name", "id")->all()
    ]);
    
}

}
