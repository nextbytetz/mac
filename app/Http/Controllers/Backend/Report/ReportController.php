<?php

namespace App\Http\Controllers\Backend\Report;

use App\Jobs\Report\CreateConfigurableReportViews;
use App\Models\Reporting\ConfigurableReport;
use App\Models\Reporting\SlaAlert;
use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use App\Repositories\Backend\Reporting\ReportRepository;
use App\Repositories\Backend\Reporting\SlaAlertRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
//use Datatables;

/**
 * Class ReportController
 * @package App\Http\Controllers\Backend\Report
 */
class ReportController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function buildConfigurable($id = NULL)
    {
        dispatch(new CreateConfigurableReportViews());
        return redirect()->back()->withFlashSuccess("Success, Build has Completed");
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function refreshConfigurable($id = NULL)
    {
        $report = new ReportRepository();
        $report->refreshConfigurables($id);
        return redirect()->back()->withFlashSuccess("Success, Refresh has Completed");
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshConfigurableDt($id = NULL)
    {
        $report = new ReportRepository();
        $report->refreshConfigurables($id);
        return response()->json(['success' => true]);
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function buildConfigurableDt($id = NULL)
    {
        $crRepo = new ConfigurableReportRepository();
        $crRepo->createViews($id);
        return response()->json(['success' => true]);
    }

    /**
     * @param ConfigurableReport $cr
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewConfigurable(ConfigurableReport $cr)
    {
        $crRepo = new ConfigurableReportRepository();
        //dd($filters);
        return view('backend/report/configurable/index')
        ->with('cr', $cr);
    }

    /**
     * @param ConfigurableReport $cr
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConfigurableHeaders(ConfigurableReport $cr, Request $request)
    {
        $input = $request->all();
        $crRepo = new ConfigurableReportRepository();
        $dtcolumns = $crRepo->buildHeadersForDatatable($cr);
        return response()->json($dtcolumns);
    }

    /**
     * @param ConfigurableReport $cr
     * @return mixed
     * @throws \Exception
     */
    public function getConfigurableDatatable(ConfigurableReport $cr, Request $request)
    {
        $crRepo = new ConfigurableReportRepository();
        $input = $request->all();
        return Datatables::of($crRepo->getForDatatable($cr, $input))->make(true);
    }

    public function getReversedConfigurableDatatable(ConfigurableReport $cr, Request $request)
    {
        $crRepo = new ConfigurableReportRepository();
        $input = $request->all();
        return Datatables::of($crRepo->getReversedForDatatable($cr, $input))->make(true);
    }

    public function downloadConfigurableDt(ConfigurableReport $cr, Request $request)
    {
        $crRepo = new ConfigurableReportRepository();
        $input = $request->all();
        //logger($input);
        $filtersql = $crRepo->getWhereQuery($cr, $input);
        //logger($filtersql);
        return $crRepo->download($cr->id, 0, $filtersql);
    }

    /**
     * @param ConfigurableReport $cr
     * @return mixed
     */
    public function downloadConfigurableAll(ConfigurableReport $cr, Request $request)
    {
        $crRepo = new ConfigurableReportRepository();
        $input = $request->all();
        $filtersql = $crRepo->getWhereQuery($cr, $input);
        return $crRepo->download($cr->id, 0, $filtersql);
    }

    /*Refresh materialized view by name*/
    public function refreshMviewByName($mview_name)
    {
        $report = new ReportRepository();
        $report->refreshMaterializedViewByName($mview_name);
        return redirect()->back()->withFlashSuccess("Success, Refresh has Completed");
    }


    public function getSubordinatesConfigurableDatatable(ConfigurableReport $cr,$user_id, Request $request)
    {
        $crRepo = new ConfigurableReportRepository();
        $input = $request->all();
        return Datatables::of($crRepo->getSubordinatesForDatatable($cr,$user_id, $input))->make(true);
    }

    public function sla()
    {
        $slas = (new SlaAlertRepository())->getMyList();
        return view("backend/report/sla/index")
                    ->with("slas", $slas);
    }

    public function viewSla(SlaAlert $sla)
    {
        $slaRepo = new SlaAlertRepository();
        $query_filter = $slaRepo->getQueryFilter($sla);
        return view('backend/report/sla/view')
                        ->with("query_filter", $query_filter)
                        ->with('sla', $sla);
    }

}
