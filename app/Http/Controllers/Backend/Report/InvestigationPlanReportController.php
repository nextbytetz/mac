<?php

namespace App\Http\Controllers\Backend\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationOfficerRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanUserRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;
use App\Http\Requests\Backend\Operation\Claim\InvestigationPlan\CreateInvestigationPlanRequest;
use App\Http\Requests\Backend\Operation\Claim\InvestigationPlan\AssignPlanInvestigatorsRequest;
use App\Http\Requests\Backend\Operation\Claim\InvestigationPlan\UpdateInvestigationPlanRequest;
use App\Http\Requests\Backend\Operation\Claim\InvestigationPlan\AssignPlanFilesRequest;

use App\DataTables\Claim\InvestigationPlan\InvestigationPlanDataTable;
use App\DataTables\Report\Claim\InvestigationPlan\PlannedInvestigationDataTable;
use App\DataTables\Report\Claim\InvestigationPlan\InvestigatedReportWrittenDataTable;
use App\DataTables\Report\Claim\InvestigationPlan\InvestigatedReportUnWrittenDataTable;
use App\DataTables\Report\Claim\InvestigationPlan\GeneralInvestigationPlanReportDataTable;
use App\DataTables\Report\Claim\InvestigationPlan\UnPlannedInvestigationReportDataTable;
use App\DataTables\Report\Claim\InvestigationPlan\InvestigationReportDataTable;

class InvestigationPlanReportController extends Controller
{


	public function __construct()
	{
		$this->investigation_plan_repo = new InvestigationPlanRepository();
		$this->investigation_officer_repo = new InvestigationOfficerRepository();
		$this->investigator_repo = new InvestigationPlanUserRepository();
		$this->plan_files_repo = new InvestigationPlanNotificationsRepository();
	}

	public function generalInvestigationPlanReport(GeneralInvestigationPlanReportDataTable $dataTable)
	{
		$data = $this->investigator_repo->returnInvestigatorsForDatatable($plan_id);
		// $investigators = $this->investigation_officer_repo->returnAllInvestigationOfficers();
		// dd($data);
		return $dataTable->render('backend.report.claim.investigation_plan.general_investigation_plan');
	}
	public function investigationReports(InvestigationReportDataTable $dataTable,Request $request)
	{
		return $dataTable->render('backend.report.claim.investigation_plan.investigation_reports',[
			"regions" => (new RegionRepository())->query()->pluck("name", "id")->all()
		]);
	}
	// public function filterProfile($plan_id)
	// {

	// 	$profile = $this->investigation_plan_repo->findOrThrowException($plan_id);
	// 	$assigned_investigators = $this->investigator_repo->returnPlanInvestigatorsId($plan_id);
	// 	$investigators = $this->investigation_officer_repo->returnAllInvestigationOfficers();
	// 	return view('backend.report.claim.investigation_plan.general_investigation_plan')->with("profile", $profile)
	// 	->with("plan_investigators", $this->investigator_repo->returnPlanInvestigators($plan_id))
	// 	->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all())
	// 	->with('investigators',$investigators)->with('assigned_investigators',$assigned_investigators));
	// }
}
