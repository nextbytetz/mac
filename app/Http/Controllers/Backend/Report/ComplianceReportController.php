<?php
namespace App\Http\Controllers\Backend\Report;

use App\DataTables\Report\Compliance\AdvanceContribPaymentDataTable;
use App\DataTables\Report\Compliance\AgeAnalysisReceivablesDataTable;
use App\DataTables\Report\Compliance\ApprovedPendingContribDataTable;
use App\DataTables\Report\Compliance\BookedInterestDataTable;
use App\DataTables\Report\Compliance\BookedInterestNonContributorsDataTable;
use App\DataTables\Report\Compliance\BookingContributionDataTable;
use App\DataTables\Report\Compliance\ChangeOfMonthlyEarningDataTable;
use App\DataTables\Report\Compliance\ClosedBusinessDataTable;
use App\DataTables\Report\Compliance\ClosedBusinesseExtensionDataTable;
use App\DataTables\Report\Compliance\ClosedBusinessNeedActionAfterFollowupDataTable;
use App\DataTables\Report\Compliance\ContribRcvableClosedBusinessDataTable;
use App\DataTables\Report\Compliance\ContribRcvableGeneralDataTable;
use App\DataTables\Report\Compliance\ContribRcvableNewEntrantsDataTable;
use App\DataTables\Report\Compliance\ContribRcvableNonContribWholeYearDataTable;
use App\DataTables\Report\Compliance\ContribRcvableOpenBalanceDataTable;
use App\DataTables\Report\Compliance\ContribReceivableNonContributorDataTable;
use App\DataTables\Report\Compliance\ContributingEmployersDataTable;
use App\DataTables\Report\Compliance\ContributionReceivableDataTable;
use App\DataTables\Report\Compliance\ContributionReconciliationsDataTable;
use App\DataTables\Report\Compliance\DebtManagement\StaffEmployerMonthlyCollectionDataTable;
use App\DataTables\Report\Compliance\DormantEmployersDataTable;
use App\DataTables\Report\Compliance\EmployeesByEmployerDataTable;
use App\DataTables\Report\Compliance\EmployerCertificateIssuesDataTable;
use App\DataTables\Report\Compliance\EmployerChangeParticularsDataTable;
use App\DataTables\Report\Compliance\EmployerContributionsDataTable;
use App\DataTables\Report\Compliance\EmployerOverallReportDataTable;
use App\DataTables\Report\Compliance\EmployerRegistrationReportDataTable;
use App\DataTables\Report\Compliance\EmployerVerificationsDataTable;
use App\DataTables\Report\Compliance\EmployerWIthInterestOverpaymentDataTable;
use App\DataTables\Report\Compliance\ErroneousRefundPayementDataTable;
use App\DataTables\Report\Compliance\InterestRaisedPaidVarianceDataTable;
use App\DataTables\Report\Compliance\LargeContributorDataTable;
use App\DataTables\Report\Compliance\LoadContributionAgingDataTable;
use App\DataTables\Report\Compliance\NewEmployeesDataTable;
use App\DataTables\Report\Compliance\PrevContribRcvableNewEmployersDataTable;
use App\DataTables\Report\Compliance\ReceiptContributionDataTable;
use App\DataTables\Report\Compliance\ReceiptsForContribIncomeDataTable;
use App\DataTables\Report\Compliance\ReceiptsForPrevContribIncomeBookedDataTable;
use App\DataTables\Report\Compliance\ReceiptsForPrevContribIncomeDataTable;
use App\DataTables\Report\Compliance\ReceiptsForPrevContribIncomeNotBookedDataTable;
use App\DataTables\Report\Compliance\StaffContribCollectionPerformance;
use App\DataTables\Report\Compliance\DebtManagement\StaffEmployerFollowUpsDataTable;
use App\DataTables\Report\Compliance\StaffLoadContributionPerformanceDataTable;
use App\DataTables\Report\Compliance\DebtManagement\StaffMonthlyCollectionDataTable;
use App\DataTables\Report\Compliance\UnpaidInterestDataTable;
use App\DataTables\Report\Compliance\UnregisteredEmployersDataTable;
use App\DataTables\Report\Compliance\AgentEmployerDataTable;
use App\DataTables\Report\Compliance\UnsuccessfullyCNDataTable;
use App\DataTables\Report\Compliance\RegisteredNotContributingDataTable;
use App\DataTables\Report\Compliance\OnlineEmployerReturnEarningsDatatable;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Backend\Report\Traits\Compliance\ComplianceReportExternalTraitCtrl;
use App\Jobs\Compliance\ProcessBookingsForNonContributorsJob;
use App\Models\Auth\User;
use App\Models\Finance\FinYear;
use App\Models\Organization\FiscalYear;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Finance\FinYearRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Reporting\ReportRepository;
use App\Services\Compliance\PostBookingsForESkippedMonths;
use App\Services\Compliance\UpdateBookingsNotContributed;
use Illuminate\Http\Request;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Report\DateRangeRequest;
use App\DataTables\Report\Compliance\EmployerInspectionProfilesDataTable;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\EmployerInspectionProfilesReportRepository;



use App\Repositories\Backend\Sysdef\RegionRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;


class ComplianceReportController extends Controller
{
    use ComplianceReportExternalTraitCtrl;
    protected $date_ranges;
    protected $regions;
    protected $code_values;

    public function __construct() {
        $this->date_ranges = new DateRangeRepository();
        $this->regions =new RegionRepository();
        $this->code_values = new CodeValueRepository();

//        $this->middleware('access.routeNeedsPermission:refresh_data_compliance_report', ['only' => ['updateReceivableReportMViews', 'updateReceivableAgeAnalysisReportMViews']]);
        if(env('TESTING_MODE') == 1)
        {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
    }


    public function staffContributionPerformance(StaffLoadContributionPerformanceDataTable $dataTable, DateRangeRequest $request )
    {
        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' =>(isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' =>(isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/compliance/load_contribution_performance', ["request" => $request]);

    }


    public function loadContributionAging(LoadContributionAgingDataTable $dataTable, DateRangeRequest $request )
    {

        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/compliance/load_contribution_aging', ["request" => $request]);
    }


    public function bookingContributionVariance(BookingContributionDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/booking_contribution', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable()]);
        }

    }


    public function interestRaisedPaidVariance(InterestRaisedPaidVarianceDataTable $dataTable, DateRangeRequest $request )
    {
        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/compliance/interest_raised_paid_variance', ["request" => $request,'total_interest' => $dataTable->getTotalInterest(),'total_paid'=>$dataTable->getTotalAmountPaid()]);
    }

    public function newEmployeesRegistered(NewEmployeesDataTable $dataTable, DateRangeRequest $request )
    {
        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/compliance/new_employees_registered', ["request" => $request, 'total' => $dataTable->findTotalEmployees() ]);
    }


    /* Employer  Registration Report */
    public function employerRegistrationReport(EmployerRegistrationReportDataTable $dataTable, DateRangeRequest $request )
    {

        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $date_range['from_date'] = (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null;
        $date_range['to_date'] = (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null;

        $par = [
            'input' => $date_range];


        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if ($export_flag == 1) {
            /*Excel*/
            return  $dataTable->download($par['input']);
        } else {
            /*return table*/
            $office_zones = DB::table('office_zones')->get()->pluck('name', 'id');
            return $dataTable->with($par)->render('backend/report/compliance/employer_registration', ["request" => $request , 'regions' => $this->regions->getAll()->pluck('name', 'id'), 'employer_categories' => $this->code_values->query()->where('code_id',4)->where(function($query){
                $query->where('reference', 'ECPUB')->orWhere('reference', 'ECPRI');
            })->get()->pluck('name','id'), 'businesses' => $this->code_values->query()->where('code_id',5)->get()->pluck('name','id'), 'total' => $dataTable->findTotalEmployers() , 'office_zones'
            => $office_zones]);
        }


    }

    public function largeContributor(LargeContributorDataTable $dataTable)
    {
        return $dataTable->render('backend/report/compliance/large_contributor');
    }



    /* Employer  Overall Report */
    public function employerOverallReport(EmployerOverallReportDataTable $dataTable, DateRangeRequest $request )
    {
        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with(['input'=>$date_range])->render('backend/report/compliance/employer_overall_report', ["request" => $request , 'regions' => $this->regions->getAll()->pluck('name', 'id'), 'employer_categories' => $this->code_values->query()->where('code_id',4)->where(function($query){
            $query->where('reference', 'ECPUB')->orWhere('reference', 'ECPRI');
        })->get()->pluck('name','id'), 'businesses' => $this->code_values->query()->where('code_id',5)->get()->pluck('name','id'),'total' => $dataTable->findTotalEmployers() ]);

    }





    /*  Employer Registration Certificate Issuances */
    public function employerCertificateIssueReport(EmployerCertificateIssuesDataTable $dataTable, DateRangeRequest $request )
    {
        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null, 'input' => $date_range])->render('backend/report/compliance/employer_certificate_issue', ["request" => $request, 'total' => $dataTable->findTotalEmployers()]);
    }





    /* Unregistered Employer Report */
    public function unregisteredEmployersReport(UnregisteredEmployersDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $office_zones = DB::table('office_zones')->get()->pluck('name', 'id');

        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/unregistered_employers', ['total' => $dataTable->findTotalEmployers(), "request" => $request, 'regions' => $this->regions->getAll()->pluck('name', 'id'), 'office_zones' => $office_zones]);
        }
    }

    public function employerInterestOverpayment(EmployerWIthInterestOverpaymentDataTable $dataTable)
    {
//        $employers = new EmployerRepository();
//        $total_count = $employers->getEmployerInterestOverpayment()->count();
        return $dataTable->render('backend/report/compliance/employer_interest_overpayment', [ 'total_count' => $dataTable->findTotalEmployers() ]);
    }


    /***
     * EMPLOYEE REPORTS
     */


    /* Employer  Overall Report */
    public function employeesByEmployer(EmployeesByEmployerDataTable $dataTable, DateRangeRequest $request)
    {
        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with(['input'=>$date_range])->render('backend/report/compliance/employees_by_employer', ["request" => $request , 'total' => $dataTable->findTotalEmployees(), 'employer_name' => $dataTable->findEmployerName() ]);

    }
    /*End of employeee*/


    /*************
     *WCFePG Reports starts here

     */

    /*Agents managing employers online*/
    public function agentEmployer(AgentEmployerDataTable $dataTable)
    {

        return $dataTable->render('backend/report/compliance/agents_employer');

    }

    public function unsuccessfullyCNReport(UnsuccessfullyCNDataTable $dataTable,DateRangeRequest $request)
    {
        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/compliance/unsuccesfully_cn', ["request" => $request]);


    }

    public function registeredNotContributing(RegisteredNotContributingDataTable $dataTable,DateRangeRequest $request)
    {

        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/compliance/registered_not_contributing', ["request" => $request]);
    }


    public function onlineNoControlNumber(RegisteredNotContributingDataTable $dataTable,DateRangeRequest $request){

        $date_range=  $this->date_ranges->dateRange($request->all());
        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,])->render('backend/report/compliance/online_no_controlnumber', ["request" => $request]);


    }

    /*Contributing employers report*/
    public function ContributingEmployers(ContributingEmployersDataTable $dataTable,Request $request)
    {

        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $contributing_categories = DB::table('employer_contributing_categories')->get()->pluck('name', 'id');
        $office_zones = DB::table('office_zones')->get()->pluck('name', 'id');
        return $dataTable->with(['input'=>$date_range ])->render('backend/report/compliance/contributing_employers', ["request" => $request , 'regions' => $this->regions->getAll()->pluck('name', 'id'), 'employer_categories' => $this->code_values->query()->where('code_id',4)->where(function($query){
            $query->where('reference', 'ECPUB')->orWhere('reference', 'ECPRI');
        })->get()->pluck('name','id'), 'contributing_categories' => $contributing_categories, 'office_zones' => $office_zones,'total' => $dataTable->findTotalEmployers() ]);
    }



    /*Closed business report*/
    public function closedBusiness(ClosedBusinessDataTable $dataTable,Request $request)
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());

        $date_range['from_date'] = (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null;
        $date_range['to_date'] = (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null;
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            $office_zones = DB::table('office_zones')->get()->pluck('name', 'id');
            $regions = $this->regions->getAll()->pluck('name', 'id');
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/closed_business', ['total' => $dataTable->findTotalEmployers(), "request" => $request, 'office_zones' => $office_zones , 'regions' => $regions  ]);
        }
    }

    /*Closed business  extension report*/
    public function closedBusinessExtension(ClosedBusinesseExtensionDataTable $dataTable,Request $request)
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());

        $date_range['from_date'] = (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null;
        $date_range['to_date'] = (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null;
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {

            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/closed_business_extension', ['total' => $dataTable->findTotalEmployers(), "request" => $request  ]);
        }
    }


    /*Closed business  need action after status follow ups report*/
    public function closedBusinessNeedActionAfterStatusFollowup(ClosedBusinessNeedActionAfterFollowupDataTable $dataTable,Request $request)
    {
        $input = $request->all();
        $input['type'] = isset($input['type']) ? $input['type'] : 1;
        $date_range=  $this->date_ranges->dateRange($input);

        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {

            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/closed_business_need_action_after_followup', ['total' => $dataTable->findTotalEmployers(), "request" => $request  ]);
        }
    }


    /*Dormant employers report*/
    public function dormantEmployers(DormantEmployersDataTable $dataTable,Request $request)
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $office_zones = DB::table('office_zones')->get()->pluck('name', 'id');
        /*Get the date*/
        if(array_key_exists('search_flag',$input)){
            $cutoff_date =  $input['cutoff_year'] . '-' . $input['cutoff_month'] . '-'  . $input['cutoff_day'];
            $input['from_date'] = (isset($input['cutoff_year']) && isset($input['cutoff_month']) &&  isset($input['cutoff_day'])) ?  $cutoff_date : Carbon::now();
        }else{
            $input['from_date'] = Carbon::now();
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/dormant_employers', ['total' => $dataTable->findTotalEmployers(), "request" => $request, 'regions' => $this->regions->getAll()->pluck('name', 'id'), 'office_zones' => $office_zones]);
        }
    }

    /*Employers verification report*/
    public function employerVerifications(EmployerVerificationsDataTable $dataTable,Request $request)
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());

        $date_range['from_date'] = (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null;
        $date_range['to_date'] = (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null;
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/employer_verifications', ['total' => $dataTable->findTotalEmployers(), "request" => $request, ]);
        }
    }



    /*Contribution Receivable - schedule 2*/
    public function contributionReceivable(ContributionReceivableDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/contribution_receivables', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable(), 'no_of_employers' => $dataTable->getNoOfEmployers($date_range)]);
        }
    }


    /*Contribution reconciliations reports*/
    public function contributionReconciliations(ContributionReconciliationsDataTable $dataTable, Request $request){
        $input = $request->all();
        if(array_key_exists('search_flag',$input)){
            $contrib_date =  $input['contrib_year'] . '-' . $input['contrib_month'] . '-28';
            $input['from_date'] = (isset($input['contrib_year']) && isset($input['contrib_month'])) ?  $contrib_date : Carbon::now();
        }else{
            $input['from_date'] = Carbon::now();
        }
        $date_range=  $this->date_ranges->dateRange($input);
//        /*Don't search when initialize the page*/
//        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
//        if($submit_flag == 0){
//            $today = Carbon::parse(Carbon::now());
//            $date_range['from_date'] = standard_date_format($today->startOfMonth());
//        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/contribution_reconciliations', ["request" => $request, 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_paid_prev' => $dataTable->getTotalAmountPaidPrev()]);
        }
    }



    /*Approved - pending contributions reports*/
    public function approvedPendingContributions(ApprovedPendingContribDataTable $dataTable, DateRangeRequest $request){
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/approved_pending_contributions', ["request" => $request]);
        }
    }


    /*Staff contribution collection performance report*/
    public function staffContributionCollectionPerformance(StaffContribCollectionPerformance $dataTable, DateRangeRequest $request){
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/staff_contrib_collection_performance', ["request" => $request]);
        }
    }


    /*Booked interests*/
    public function bookedInterests(BookedInterestDataTable $dataTable, DateRangeRequest $request)
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/booked_interest', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable()]);
        }
    }


    public function unpaidInterests(UnpaidInterestDataTable $dataTable, DateRangeRequest $request)
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/unpaid_interest', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable()]);
        }
    }

    /*Contribution receivable for non contributors*/
    public function contribRcvableForNonContributors(ContribReceivableNonContributorDataTable $dataTable, DateRangeRequest $request){
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/contrib_rcvble_non_contributors', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable()]);
        }
    }

    /*Booked interest for non contributors*/
    public function bookedInterestForNonContributors(BookedInterestNonContributorsDataTable $dataTable, DateRangeRequest $request){
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;

        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {

            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/booked_interest_non_contributors', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable()]);
        }
    }


    /*Previous receivable for employers not in last receivale opening balance*/
    public function contribRcvableNewEmployers(ContribRcvableNewEntrantsDataTable $dataTable, DateRangeRequest $request){
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }else{
            $date_range['fin_year'] = $input['fin_year'];
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;

        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {

            $fin_years = DB::table('fin_years')->where('isactive',1)->orderBy('id','desc')->get()->pluck('name','id');
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/contrib_rcvable_new_employers', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable(), 'no_of_employers' => $dataTable->getNoOfEmployers($date_range), 'fin_years' => $fin_years]);
        }
    }




    /*COntribution Receivable for non contrbutors whole year - schedule 6*/
    public function contribRcvableNonContributorWholeYear(ContribRcvableNonContribWholeYearDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/contrib_rcvable_non_contrib_whole_year', ["request" => $request,'submit_flag' => 1, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable(), 'no_of_employers' => $dataTable->getNoOfEmployers($date_range)]);
        }
    }



    /*Employer contributions - Months which are already paid for*/
    public function employerContributions(EmployerContributionsDataTable $dataTable )
    {
        $input = request()->all();
        $date_range=  $this->date_ranges->dateRange(request()->all());
        /*Don't search when initialize the page*/

        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
//            $date_range['from_date'] = standard_date_format($today->startOfMonth());
//            $date_range['to_date'] = standard_date_format($today->startOfMonth());
            $date_range['cutoff_month'] = Carbon::parse(Carbon::now())->format('m');
            $date_range['cutoff_year'] = Carbon::parse(Carbon::now())->format('Y');
        }else{
            $date_range['cutoff_month'] = $input['cutoff_month'];
            $date_range['cutoff_year'] = $input['cutoff_year'];
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/employer_contributions', ["request" => $date_range,'submit_flag' => 1, 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'regions' => $this->regions->getAll()->pluck('name', 'id')]);
        }
    }


    /*Summary contribution quarterly*/
    public function summaryContribQuarterly(DateRangeRequest $request)
    {
        $receipt_repo = new ReceiptRepository();
        $booking_repo = new BookingRepository();
        $fiscal_year_repo = new FiscalYearRepository();

        $input = $request->all();
        $start_year = $input['from_year'];
        $from_date = standard_date_format($input['from_date']);
        $to_date = standard_date_format($input['to_date']);
        $category = isset($input['category']) ? $input['category'] : 1;
        $date_type = isset($input['general_type']) ? $input['general_type'] : 1;
        $no_of_employers = []; $collection = []; $target = [];
        if(array_key_exists('from_day', $input)){

            if($category == 1){
                /*Contributing categories*/
                /*Contrib collection*/
                $large_collection = $receipt_repo->getContribCollectionByEmployerContribCategoryForDateRange(1, $input['from_date'], $input['to_date'], $date_type);
                $total_collection_large = $large_collection['contrib_amount'];
                $large_employers = $large_collection['no_of_employers'];

                $medium_collection = $receipt_repo->getContribCollectionByEmployerContribCategoryForDateRange(2, $input['from_date'], $input['to_date'], $date_type);
                $total_collection_medium = $medium_collection['contrib_amount'];
                $medium_employers = $medium_collection['no_of_employers'];

                $small_collection = $receipt_repo->getContribCollectionByEmployerContribCategoryMediumSmallForDateRange( $input['from_date'], $input['to_date'], $date_type);
                $total_collection_small = $small_collection['contrib_amount'];
                $small_employers = $small_collection['no_of_employers'];

                $no_of_employers = ['small_employers' =>$small_employers,'medium_employers' =>$medium_employers,'large_employers' =>$large_employers ];
                $collection = ['large_collection' =>$total_collection_large,'medium_collection' =>$total_collection_medium,'small_collection' =>$total_collection_small];
                /*Target*/
                $days_diff = Carbon::parse($from_date)->diffInDays(Carbon::parse($to_date));
                $target_large = $fiscal_year_repo->getDaysTargetByTypeReference($start_year,'ATTLARGCO', $days_diff);
                $target_medium =$fiscal_year_repo->getDaysTargetByTypeReference($start_year,'ATTMEDIO',$days_diff);
                $target_small = $fiscal_year_repo->getDaysTargetByTypeReference($start_year,'ATTPUBCO', $days_diff);
                $target =  ['large_target' =>$target_large,'medium_target' =>$target_medium,'small_target' =>$target_small];
            }elseif($category == 2){
                /*Public /private*/
                /*collection*/
                $public_collection = $receipt_repo->getContribCollectionByEmployerTypeForDateRange(36, $input['from_date'], $input['to_date'],$date_type );
                $total_collection_public = $public_collection['contrib_amount'];
                $public_employers = $public_collection['no_of_employers'];

                $private_collection = $receipt_repo->getContribCollectionByEmployerTypeForDateRange(37, $input['from_date'], $input['to_date'],$date_type );
                $total_collection_private = $private_collection['contrib_amount'];
                $private_employers =$private_collection['no_of_employers'];
                $collection = ['public_collection' =>$total_collection_public,'private_collection' =>$total_collection_private];
                $no_of_employers = ['public_employers' =>$public_employers,'private_employers' =>$private_employers ];
                /*Target*/
                $days_diff = Carbon::parse($from_date)->diffInDays(Carbon::parse($to_date));
                $target_public = $fiscal_year_repo->getDaysTargetByTypeReference($start_year,'ATTPUBCO', $days_diff);
                $target_private =$fiscal_year_repo->getDaysTargetByTypeReference($start_year,'ATTPRIVCO',$days_diff);
                $target = ['public_target' =>$target_public,'private_target' =>$target_private];
            }


            return view('backend/report/compliance/summary_contrib_quarterly')
                ->with('request', $input)
                ->with('no_of_employers', $no_of_employers)
                ->with('target', $target)
                ->with('collection', $collection);

        }else{
            return view('backend/report/compliance/summary_contrib_quarterly')
                ->with('request', $input);


        }




    }



    /*Receivable - AGe Analysis*/
    public function receivableAgeAnalysis(AgeAnalysisReceivablesDataTable $dataTable, Request $request )
    {
        $input = $request->all();

        $date_range =  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        /*Get the date*/
        if(array_key_exists('search_flag',$input)){
            $cutoff_date =  $input['cutoff_year'] . '-' . $input['cutoff_month'] . '-'  . $input['cutoff_day'];
            $date_range['from_date'] = (isset($input['cutoff_year']) && isset($input['cutoff_month']) &&  isset($input['cutoff_day'])) ?  $cutoff_date : Carbon::now();
        }else{
            $date_range['from_date'] = Carbon::now();
        }

        $date_range['min_days'] = isset($input['min_days']) ? $input['min_days'] : null;
        $date_range['max_days'] = isset($input['max_days']) ? $input['max_days'] : null;
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/age_analysis_receivables', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable()]);
        }
    }


    /*Contribution Receivable - for closed businesses*/
    public function contribRcvableClosedBusiness(ContribRcvableClosedBusinessDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }else{
            $date_range['fin_year'] = $input['fin_year'];
        }

        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            $fin_years = DB::table('fin_years')->where('isactive',1)->orderBy('id','desc')->get()->pluck('name','id');

            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/contrib_rcvable_closed_business', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable(), 'no_of_employers' => $dataTable->getNoOfEmployers($date_range), 'fin_years' => $fin_years]);
        }


    }


    /*Contribution Receivable - General report - All receivables*/
    public function contribRcvableGeneralReport(ContribRcvableGeneralDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/contrib_rcvable_general', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable(), 'no_of_employers' => $dataTable->getNoOfEmployers($date_range)]);
        }
    }

    /*Contribution receivable for opening balance for selected fiscal year*/
    public function contribRcvableOpenBalance(ContribRcvableOpenBalanceDataTable $dataTable, Request $request )
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        /*Get the date*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 1){
            $cutoff_date =  $input['cutoff_year'] . '-' . $input['cutoff_month'] . '-'  . $input['cutoff_day'];
            $date_range['from_date'] = (isset($input['cutoff_year']) && isset($input['cutoff_month']) &&  isset($input['cutoff_day'])) ?  $cutoff_date : Carbon::now();
        }else{
            $date_range['from_date'] = Carbon::now();
        }


        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {

            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/contrib_rcvable_open_balance', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_variance' =>
                $dataTable->getTotalVariance(), 'no_of_employers' => $dataTable->getNoOfEmployers($date_range)]);
        }


    }

    /*Erroneous refund payment report*/
    public function erroneousRefundPayment(ErroneousRefundPayementDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/erroneous_refund_payment', ["request" => $request, 'total_booked' => $dataTable->getTotalBooked(), 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_receivable' => $dataTable->getTotalReceivable(), 'no_of_employers' => $dataTable->getNoOfEmployers($date_range)]);
        }
    }

    /*Advance contribution payment report*/
    public function advanceContribution(AdvanceContribPaymentDataTable $dataTable, DateRangeRequest $request )
    {

        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/advance_payment', ["request" => $request,'submit_flag' => 1, 'total_paid' =>
                $dataTable->getTotalAmountPaid()]);
        }


    }

    /*Receipt compliance - contributions*/
    public function receiptContributions(ReceiptContributionDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
            $date_range['general_type'] = 0;
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/receipt_contributions', ["request" => $request,'submit_flag' => 1, 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'regions' => $this->regions->getAll()->pluck('name', 'id')]);
        }
    }



    /*Change of monthly earning - reconciliations reports*/
    public function changeOfMonthlyEarning(ChangeOfMonthlyEarningDataTable $dataTable, Request $request){
        $input = $request->all();
        if(array_key_exists('search_flag',$input)){
            $contrib_date =  $input['contrib_year'] . '-' . $input['contrib_month'] . '-28';
            $input['from_date'] = (isset($input['contrib_year']) && isset($input['contrib_month'])) ?  $contrib_date : Carbon::now();
        }else{
            $input['from_date'] = Carbon::now();
        }
        $date_range=  $this->date_ranges->dateRange($input);

        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/change_of_monthly_earning', ["request" => $request, 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'total_paid_prev' => $dataTable->getTotalAmountPaidPrev()]);
        }
    }


    /*Summary report for contribution age analysis*/
    public function summaryContribAgeAnalysis()
    {
        $summary = DB::table('summary_contrib_rcvable_age_analysis')->first();
        $today = Carbon::now();
        $base_url = url("/") . "/" . "report/contribution_age_analysis?search_flag=&export_flag=0&cutoff_day=" . $today->format('j') .  "&cutoff_month=" . $today->format('n') .  "&cutoff_year=" . $today->format('Y') .  "&from_date=&dataTable_length=10";
        return view('backend/report/compliance/summary_contrib_age_analysis')
            ->with('summary', $summary)
            ->with('base_url', $base_url);
    }

    /*Summary report for contribution income and receivable - Target*/
    public function summaryContribIncomeRcvable(ContributionReceivableDataTable $dt_sch2, ContribRcvableNonContribWholeYearDataTable $dt_sch6, ContribRcvableNewEntrantsDataTable $dt_sch5, ContribRcvableClosedBusinessDataTable $dt_sch_closed)
    {
        $input = request()->all();
        $fin_years = DB::table('fin_years')->where('isactive',1)->orderBy('id','desc')->get()->pluck('name','id');

        if(isset($input['fin_year_id'])){
            $summary =  DB::table('summary_contrib_rcvable_income_mview')->where('fin_year_id', $input['fin_year_id'])->first();
        }else{
            $summary =  DB::table('summary_contrib_rcvable_income_mview')->orderBy('fin_year_id', 'desc')->first();
        }

        $base_url = url("/") . "/" . "report";
        $fin_year = DB::table('fin_years')->where('id', $summary->fin_year_id)->first();
        $today = Carbon::now()->startOfMonth();
        $rcvable_summary = null;

        /*When i searched*/
        if(isset($input['search_flag'])){
        /*Get end date*/
        $end_date = (comparable_date_format($today) < comparable_date_format($fin_year->end_date) ? standard_date_format($today) : $fin_year->end_date);
        /*Schedule 2 - receivable for contributors*/
        $data = ['from_date' => $fin_year->start_date, 'to_date' => $end_date, 'fin_year' => $fin_year->id, 'isfiscal' => 1];
        $sch2_receivable = $dt_sch2->searchResult($data)->sum('booked_amount');
        $sch2_employers = $dt_sch2->getNoOfEmployers($data);
        /*Schedule 6 - receivable for non contributors*/
        $data = ['from_date' => $fin_year->start_date, 'to_date' => $end_date, 'fin_year' => $fin_year->id, 'isfiscal' => 1];
        $sch6_receivable = $dt_sch6->searchResult($data)->sum('booked_amount');
        $sch6_employers = $dt_sch6->getNoOfEmployers($data);

        /*Schedule 5 - receivable for new entrants*/
        $data = ['from_date' => standard_date_format(getWCFLaunchDate()), 'to_date' => $end_date, 'fin_year' => $fin_year->id, 'isfiscal' => 1];
        $sch5_receivable = $dt_sch5->searchResult($data)->sum('booked_amount');
        $sch5_employers = $dt_sch5->getNoOfEmployers($data);

        /*Schedule  - receivable for closed business*/
        $data = ['from_date' =>$fin_year->start_date, 'to_date' => $end_date, 'fin_year' => $fin_year->id, 'isfiscal' => 1];
        $sch_closed_receivable = $dt_sch_closed->searchResult($data)->sum('booked_amount');
        $sch_closed_employers = $dt_sch_closed->getNoOfEmployers($data);

        $rcvable_summary = ['receivable_contributors' => $sch2_receivable   ,'receivable_contributors_employers' =>  $sch2_employers, 'receivable_non_contributors' =>  $sch6_receivable,  'receivable_non_contributors_employers' => $sch6_employers , 'receivable_new_entrants' =>  $sch5_receivable, 'receivable_new_entrants_employers' =>  $sch5_employers, 'receivable_closed_business' => $sch_closed_receivable, 'receivable_closed_business_employers' => $sch_closed_employers ];
        }

        return view('backend/report/compliance/summary_contrib_income_rcvable')
            ->with('fin_years', $fin_years)
            ->with('summary', $summary)
            ->with('rcvable_summary', $rcvable_summary)
            ->with('base_url', $base_url)
            ->with('fin_year', $fin_year);
    }


    /*Update materialized view for receivable summary and bookings*/
    public function updateReceivableReportMViews()
    {
        $input = request()->all();
        $isschedule = $input['isschedule'] ?? 0;

        /*schedules*/
        if($isschedule == 1){
            /*update schedule */

            (new UpdateBookingsNotContributed())->postAllSchedulesByFinYear($input['fin_year_id']);
        }else{
            /*Update bookings*/
            (new ReportRepository())->refreshMaterializedViewByNameConcurrently('bookings_mview');
            (new ReportRepository())->refreshMaterializedViewByName('summary_contrib_rcvable_income_mview');

        }

        return redirect()->back()->withFlashSuccess('Success, Data refreshed');
    }

    /*update materialized view for age analysis receivable*/
    public function updateReceivableAgeAnalysisReportMViews()
    {
        (new ReportRepository())->refreshMaterializedViewByName('summary_contrib_rcvable_age_analysis');
        return redirect()->back()->withFlashSuccess('Success, Data refreshed');
    }


    /*Receipts for contributions for fiscal year from summary income*/
    public function receiptsForContribIncomeSummary(ReceiptsForContribIncomeDataTable $dataTable, DateRangeRequest $request )
    {

        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/receipt_contrib_income_summary', ["request" => $request,'submit_flag' => 1, 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'no_of_employers' =>
                $dataTable->getNoOfEmployers($date_range)]);
        }
    }


    /*Receipts for previous contributions for fiscal year which were never booked from summary income - Not booked*/
    public function receiptsForPrevContribIncomeSummary(ReceiptsForPrevContribIncomeNotBookedDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/receipt_prev_contrib_income_summary', ["request" => $request,'submit_flag' => 1, 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'no_of_employers' =>
                $dataTable->getNoOfEmployers($date_range)]);
        }
    }

    /*Receipts for previous contributions for fiscal year which were never booked from summary income Booked*/
    public function receiptsForPrevContribIncomeSummaryBooked(ReceiptsForPrevContribIncomeBookedDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/receipt_prev_contrib_income_summary_booked', ["request" => $request,'submit_flag' => 1, 'total_paid' =>
                $dataTable->getTotalAmountPaid(), 'no_of_employers' =>
                $dataTable->getNoOfEmployers($date_range)]);
        }
    }


    /*Ends Here*/



    /*Start Debt Management ------reports------*/
    public function staffMonthlyCollectionPerformance(StaffMonthlyCollectionDataTable $dataTable, Request $request)
    {
        $input = $request->all();
        if(array_key_exists('search_flag',$input)){
            $contrib_date =  $input['contrib_year'] . '-' . $input['contrib_month'] . '-28';
            $input['from_date'] = (isset($input['contrib_year']) && isset($input['contrib_month'])) ?  $contrib_date : Carbon::now();
        }else{
            $input['from_date'] = Carbon::now();
        }
        $date_range=  $this->date_ranges->dateRange($input);
        $date_range['isintern'] = isset($input['category']) ? $input['category'] : 0;
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;

        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/debt_management/staff_monthly_collection', ["request" => $request]);
        }
    }


    public function staffEmployerFollowUps(StaffEmployerFollowUpsDataTable $dataTable, DateRangeRequest $request)
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());

        }
        /*search filter*/
        $date_range['feedback_id'] = isset($input['feedback_id']) ? $input['feedback_id'] : null;
        $date_range['user_id'] = isset($input['user_id']) ? $input['user_id'] : null;
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        $feedbacks = CodeValue::query()->where('is_active',1 )->where('code_id',25)->orderBy('name')->get()->pluck('name', 'id');
        $users = (new StaffEmployerRepository())->getQueryUsersInDebtMgt()->orderBy('firstname')->get()->pluck('name', 'id');
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {


            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/debt_management/staff_employer_follow_ups', ["request" => $request, 'feedbacks' => $feedbacks, 'users' => $users]);
        }
    }

    /*End Debt Management --------reports------*/


    public function changeParticulars(EmployerChangeParticularsDataTable $dataTable, DateRangeRequest $request)
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($request->all());
        /*Don't search when initialize the page*/
        $submit_flag = array_key_exists('search_flag', $input)  ? 1 : 0;
        if($submit_flag == 0){
            $today = Carbon::parse(Carbon::now());
            $date_range['from_date'] = standard_date_format($today->startOfMonth());
            $date_range['to_date'] = standard_date_format($today->startOfMonth());
        }
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/employer_change_particulars', ["request" => $request]);
        }
    }

//Return of earnings report
    public function employerReturnEarnings(OnlineEmployerReturnEarningsDatatable $dataTable)
    {
        return $dataTable->render('backend/report/compliance/online_employer_return_earnings');

    }


    public function legacyBookingsReport()
    {
        $input = request()->all();

        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            $bookings = DB::table('legacy_bookings as l')->select(
                'e.reg_no',
                'e.name',
                'e.doc',
                'l.rcv_date as contrib_month',
                'l.amount as booking_amount',
                'l.paid_amount'
            )
                ->join('employers as e', 'e.id', 'l.employer_id')
                ->whereNull('l.deleted_at')->orderBy('l.employer_id')->get()->toArray();
            $results = array();
            foreach ($bookings as $result) {

                $results[] = (array)$result;
                #or first convert it and then change its properties using
                #an array syntax, it's up to you
            }

            return Excel::create('legacybookings'. time(), function($excel) use ($results) {
                $excel->sheet('mySheet', function($sheet) use ($results)
                {
                    $sheet->fromArray((array)$results);
                });
            })->download('csv');
        }else {

            return view('backend/report/compliance/contrib_rcvable_legacy_bookings');
        }

    }
    
    /*Staff employer collection for selected month contrib*/
    public function staffEmployerMonthlyCollectionReport(StaffEmployerMonthlyCollectionDataTable $dataTable, DateRangeRequest $request)
    {
        $report_title = 'Staff Employer Monthly Collection';
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $dataTable->checkIfDateRangeNotExceed12Months($date_range);
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        $users=User::query()->where('unit_id', 15)->orderBy('firstname')->get()->pluck('name', 'id');
        $date_range['user_id'] = isset($input['user_id']) ? $input['user_id'] : null;

        if($export_flag == 1) {
            return $dataTable->download($date_range);
        }else{
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/debt_management/staff_employer_monthly_collection', ['report_title' => $report_title,'users'=>$users, 'request' => $request]);
        }

    }
     public function allocatedStaffDetail()
  {
     $this->inspection = new EmployerInspectionProfilesReportRepository();
      return $this->inspection->allocatedStaffDetail(request()->input('q'), request()->input('page'));  
    
  }
  public function inspectionStage()
  {
    
      $this->inspection_level = new EmployerInspectionProfilesReportRepository();
      return $this->inspection_level->inspectionStage(request()->input('b'), request()->input('page'));
  }


    /*starting of  returning inspection report */
    public function generalInspectionProfileDetails(EmployerInspectionProfilesDataTable $dataTable,  DateRangeRequest $request)
    {
         $input = $request->all();
        if(array_key_exists('search_flag',$input)){
            $visit_date =  $input['visit_date'] . '-' . $input['contrib_month'] . '-28';
            $input['from_date'] = (isset($input['contrib_year']) && isset($input['contrib_month'])) ?  $contrib_date : Carbon::now();
        }else{
            $input['from_date'] = Carbon::now();
        }
        $date_range=  $this->date_ranges->dateRange($input);

        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($date_range);
        }else {
       return $dataTable->render('backend/report/compliance/employer_inspection_profile', [
            "regions" => (new RegionRepository())->query()->pluck("name", "id")->all(),
            
            
        ]);
       
    }
}




}