<?php

namespace App\Http\Controllers\Backend\Report\Traits\Payroll;

use App\DataTables\Report\Compliance\EmployerContribution\EmployerContributionsExtAuditDataTable;
use App\Http\Requests\Backend\Report\DateRangeRequest;
use App\Repositories\Backend\Finance\FinYearRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class PermissionAttribute
 * @package App\Models\Access\Attribute
 */
trait PayrollPaymentSummaryTraitCtrl
{


    /*Get Date range for payment summary report*/
    public function getDateRangesPaySummary(array $input)
    {
        $period_type= $input['period_type'];
$period_title = null;
        switch($period_type){
            case 1://month
                $date = isset($input['from_date']) ? $input['from_date'] : getTodayDate();
                $start_date = Carbon::parse($date)->startOfMonth();
                $end_date = Carbon::parse($date)->endOfMonth();
                $period_title = month_year_date_format($start_date);
                break;

            case 2://quarter
                $quarter =  isset($input['quarter']) ? $input['quarter'] : 1;
                $fin_year_id = $input['fin_year_id'];
                $fin_year = (new FinYearRepository())->query()->where('id',$fin_year_id)->first();
                $date_ranges = (new FinYearRepository())->getDateRangesFromQuarterFinYear($fin_year_id, $quarter);
                $start_date = $date_ranges['start_date'];
                $end_date = $date_ranges['end_date'];
                $period_title =$fin_year->name . ' - Quarter: '. $quarter . ' (' . month_year_date_format($start_date) . ' to ' . month_year_date_format($end_date) . ')';
                break;

            case 3://annually
                $fin_year = (new FinYearRepository())->query()->where('id', $input['fin_year_id'])->first();
                $start_date = standard_date_format($fin_year->start_date);
                $end_date = standard_date_format($fin_year->end_date);
                $period_title =$fin_year->name;
                break;
        }

        return [
            'start_date' => standard_date_format($start_date),
            'end_date' => standard_date_format($end_date),
            'period_title' => $period_title
        ];

    }


    /**
     *Get Summary results
     */
    public function getPaymentSummaryResults(array $input)
    {

        $total_summary  = [];


/*Total Summary*/
        $payroll_runs_summary =$this->getBaseQueryPayRunsSummary($input)->first();
        $total_summary['arrears_amount'] = $payroll_runs_summary->arrears_amount;
        $total_summary['deduction_amount'] = $payroll_runs_summary->deductions_amount;
        $total_summary['net_amount'] = $payroll_runs_summary->net_amount;

        /*Total by member*/
        //pensioners
        $payroll_runs_member_summary = $this->getBaseQueryPayRunsSummary($input)->where('member_type_id', 5)->first();
        $total_summary['net_pensioner'] = $payroll_runs_member_summary->net_amount;
        $total_summary['count_pensioner'] = $payroll_runs_member_summary->no_of_payees;
        //dependents
        $payroll_runs_member_summary =$this->getBaseQueryPayRunsSummary($input)->where('member_type_id', 4)->where('isconstantcare', 0)->first();
        $total_summary['net_dependent'] = $payroll_runs_member_summary->net_amount;
        $total_summary['count_dependent'] =$payroll_runs_member_summary->no_of_payees;
        //constant care
        $payroll_runs_member_summary =$this->getBaseQueryPayRunsSummary($input)->where('member_type_id', 4)->where('isconstantcare', 1)->first();
        $total_summary['net_constant_care'] =$payroll_runs_member_summary->net_amount;
        $total_summary['count_constant_care'] = $payroll_runs_member_summary->no_of_payees;

        /*Suspended runs*/
        /*Total Summary*/
        $payroll_runs_summary =$this->getBaseQueryPaySuspendedRunsSummary($input)->first();
        $total_summary['suspended_amount'] = $payroll_runs_summary->net_amount;
        //pensioners
        $payroll_runs_member_summary = $this->getBaseQueryPaySuspendedRunsSummary($input)->where('member_type_id', 5)->first();
        $total_summary['count_pensioner_suspended'] = $payroll_runs_member_summary->no_of_payees;
        //dependents
        $payroll_runs_member_summary =$this->getBaseQueryPaySuspendedRunsSummary($input)->where('member_type_id', 4)->where('isconstantcare', 0)->first();
        $total_summary['count_dependent_suspended'] =$payroll_runs_member_summary->no_of_payees;
        //constant care
        $payroll_runs_member_summary =$this->getBaseQueryPaySuspendedRunsSummary($input)->where('member_type_id', 4)->where('isconstantcare', 1)->first();
        $total_summary['count_constant_care_suspended'] = $payroll_runs_member_summary->no_of_payees;
        return $total_summary;
    }

    /*runs*/
    public function getBaseQueryPayRunsSummary(array $input)
    {
        $date_ranges = $this->getDateRangesPaySummary($input);
        $start_date = $date_ranges['start_date'];
        $end_date = $date_ranges['end_date'];
        $payroll_run_repo = new PayrollRunRepository();
        $base_query = $payroll_run_repo->getQueryPayrollRunTotalSummary()
            ->whereBetween('run_date', [$start_date, $end_date]);

        return $base_query;
    }

    /*suspended runs*/
    public function getBaseQueryPaySuspendedRunsSummary(array $input)
    {
        $date_ranges = $this->getDateRangesPaySummary($input);
        $start_date = $date_ranges['start_date'];
        $end_date = $date_ranges['end_date'];
        $payroll_run_repo = new PayrollSuspendedRunRepository();
        $base_query = $payroll_run_repo->getQueryPayrollSuspendedRunTotalSummary()
            ->whereBetween('run_date', [$start_date, $end_date]);

        return $base_query;
    }
}