<?php

namespace App\Http\Controllers\Backend\Report\Traits\Compliance;

use App\DataTables\Report\Compliance\EmployerContribution\EmployerContributionsExtAuditDataTable;
use App\Http\Requests\Backend\Report\DateRangeRequest;

/**
 * Class PermissionAttribute
 * @package App\Models\Access\Attribute
 */
trait ComplianceReportExternalTraitCtrl
{


    /*Employer month contributions for external audit*/
    public function employerContributionsForExternalAudit(EmployerContributionsExtAuditDataTable $dataTable, DateRangeRequest $request)
    {
        $report_title = 'Employer Contributions Receipted';
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;

        if($export_flag == 1) {
            return $dataTable->download($date_range);
        }else{
            return $dataTable->with([
                'input' => $date_range])->render('backend/report/compliance/employer_contribution/employer_contrib_ext_audit', ['report_title' => $report_title, 'request' => $request]);
        }
    }

}