<?php
namespace App\Http\Controllers\Backend\Report;

use App\DataTables\Report\Finance\CancelledReceiptDataTable;
use App\DataTables\Report\Finance\ReceiptByFinanceCodeDataTable;
use App\DataTables\Report\Finance\FinanceReportsDataTable;
use App\DataTables\Report\Finance\ReceiptsDataTable;
use App\DataTables\Report\GeneralReportDataTable;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\DataTables\Report\Finance\ReceiptPerformanceDataTable;
use App\Repositories\Backend\Reporting\ReportRepository;
use Yajra\Datatables\Datatables;

class GeneralReportController extends Controller
{
    public function __construct(ReceiptRepository $receipts,ReportRepository $reports) {
        $this->receipts = $receipts;
        $this->reports = $reports;
    }

    public function index(GeneralReportDataTable $dataTable, $category_id){
        return $dataTable->with('category_id',$category_id)->render('backend/report/index');

    }
}