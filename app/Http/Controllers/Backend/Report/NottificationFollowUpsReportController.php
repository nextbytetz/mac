<?php

namespace App\Http\Controllers\Backend\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\DataTables\Report\Claim\NotificationFollowUpDataTable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Operation\ClaimFollowup\ClaimFollowup;
use App\Models\Operation\Claim\NotificationReport; 
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;

use App\Repositories\Backend\Operation\Claim\ClaimFollowupUpdateRepository;

class NottificationFollowUpsReportController extends Controller
{
    
    public function generalNotificationFollowUpREport(NotificationFollowUpDataTable $dataTable, Request $request)
    {

    	  return $dataTable->render('backend.report.claim.follow_ups_report',[
			"regions" => (new RegionRepository())->query()->pluck("name", "id")->all()
		]);
    }
}
