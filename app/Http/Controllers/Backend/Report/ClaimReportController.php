<?php
namespace App\Http\Controllers\Backend\Report;

use App\DataTables\Report\Claim\NotificationReportInitialExpenseDataTable;
use App\DataTables\Report\Claim\NotificationReportReceivedDataTable;
use App\DataTables\Report\Claim\ProcessedClaimsDataTable;
use App\DataTables\Report\Compliance\BookingContributionDataTable;
use App\DataTables\Report\Compliance\InterestRaisedPaidVarianceDataTable;
use App\DataTables\Report\Compliance\LoadContributionAgingDataTable;
use App\DataTables\Report\Compliance\NewEmployeesDataTable;
use App\DataTables\Report\Compliance\StaffLoadContributionPerformanceDataTable;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\BusinessRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Report\DateRangeRequest;

use App\Repositories\Backend\Sysdef\RegionRepository;
use Carbon\Carbon;


class ClaimReportController extends Controller
{
    protected $date_ranges;
    protected $incident_types;
    protected $regions;
    protected $businesses;
    protected $employers;
    protected $member_types;
    protected $insurances;
    protected $code_values;
    protected $districts;

    public function __construct() {

        $this->date_ranges = new DateRangeRepository();
        $this->incident_types = new IncidentTypeRepository();
        $this->regions = new RegionRepository();
        $this->businesses = new BusinessRepository();
        $this->employers = new EmployerRepository();
        $this->member_types = new MemberTypeRepository();
        $this->insurances = new InsuranceRepository();
        $this->code_values = new CodeValueRepository();
        $this->districts = new DistrictRepository();

    }

    /**
     * @param NotificationReportReceivedDataTable $dataTable
     * @param DateRangeRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function notificationReportReceived(NotificationReportReceivedDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();
        $search_flag = array_key_exists('search_flag', $input) ? $input['search_flag'] : 0;

        $with_binding = ["request" => $input,  'incident_types' => $this->incident_types->getAll()->pluck('name', 'id'),'regions' => $this->regions->getAll()->pluck('name', 'id'), 'districts' => $this->districts->getAll()->pluck('name', 'id'), 'businesses' => $this->code_values->query()->where('code_id',5)->get()->pluck('name','id'), 'employers' => $this->employers->getAll()->pluck('name', 'id') ];
        if(($search_flag == 1)){
            $date_range=  $this->date_ranges->dateRange($request->all());

            return $dataTable->with([
                'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
                'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,'input'=>$date_range])->render('backend/report/claim/notification_report_received', $with_binding);

        }else{

            return view('backend/report/claim/notification_report_received')->with($with_binding);
        }

    }


    /*Notification report initial expenses*/
    public function notificationReportInitialExpense(NotificationReportInitialExpenseDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();
        $search_flag = array_key_exists('search_flag', $input) ? $input['search_flag'] : 0;

        /*Binding*/
        $with_binding = ["request" => $input,  'incident_types' => $this->incident_types->getAll()->pluck('name', 'id'),'regions' => $this->regions->getAll()->pluck('name', 'id'), 'districts' => $this->districts->getAll()->pluck('name', 'id'), 'businesses' => $this->code_values->query()->where('code_id',5)->get()->pluck('name','id'), 'employers' => $this->employers->getAll()->pluck('name', 'id') ];

        if(($search_flag == 1)) {
            $date_range = $this->date_ranges->dateRange($request->all());

            return $dataTable->with([
                'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
                'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null, 'input' => $date_range])->render('backend/report/claim/notification_report_initial_expense', $with_binding);

        }else{
            return view('backend/report/claim/notification_report_initial_expense',$with_binding);
        }
    }



    /*Processed claims*/
    public function processedClaims(ProcessedClaimsDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());

        return $dataTable->with([
            'from_date' => (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null,
            'to_date' => (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null,'input'=>$date_range])->render('backend/report/claim/processed_claims', ["request" => $input,  'incident_types' => $this->incident_types->getAll()->pluck('name', 'id'),'regions' => $this->regions->getAll()->pluck('name', 'id'),'businesses' =>  $this->code_values->query()->where('code_id',5)->get()->pluck('name','id'),'employers' => $this->employers->getAll()->pluck('name', 'id')  ]);
    }





}