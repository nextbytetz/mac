<?php
namespace App\Http\Controllers\Backend\Report;

use App\DataTables\Report\Claim\NotificationReportReceivedDataTable;
use App\DataTables\Report\Compliance\BookingContributionDataTable;
use App\DataTables\Report\Compliance\InterestRaisedPaidVarianceDataTable;
use App\DataTables\Report\Compliance\LoadContributionAgingDataTable;
use App\DataTables\Report\Compliance\NewEmployeesDataTable;
use App\DataTables\Report\Compliance\StaffLoadContributionPerformanceDataTable;
use App\DataTables\Report\Payroll\MonthlyPensionPayrollDataTable;
use App\DataTables\Report\Payroll\PayrollBankUpdatesDataTable;
use App\DataTables\Report\Payroll\PayrollBeneficiariesDataTable;
use App\DataTables\Report\Payroll\PayrollChildExtensionsDataTable;
use App\DataTables\Report\Payroll\PayrollVerificationsDataTable;
use App\DataTables\Report\Payroll\PensionStatementDataTable;
use App\DataTables\Report\Traits\PdfSnappyExportTrait;
use App\Http\Controllers\Backend\Report\Traits\Payroll\PayrollPaymentSummaryTraitCtrl;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Member\Dependent;
use App\Models\Operation\Payroll\Pensioner;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Finance\FinYearRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use App\Repositories\Backend\Sysdef\BusinessRepository;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Report\DateRangeRequest;

use App\Repositories\Backend\Sysdef\RegionRepository;
use Carbon\Carbon;
use Khill\Lavacharts\Dashboards\Filters\DateRangeFilter;


class PayrollReportController extends Controller
{
    use PayrollPaymentSummaryTraitCtrl, PdfSnappyExportTrait;

    protected $date_ranges;
    protected $payroll_procs;
    protected $banks;

    public function __construct() {
        $this->date_ranges = new DateRangeRepository();
        $this->payroll_procs = new PayrollProcRepository();
        $this->banks = new BankRepository();
    }

    /**
     * @param BookingContributionDataTable $dataTable
     * @param DateRangeRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     * Notification report received over period of time
     */
    public function monthlyPensionPayroll(MonthlyPensionPayrollDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($input);
        if(array_key_exists('member_type',$input)){
            $date_range['member_type'] = $input['member_type'];
        }else{
            $date_range['member_type'] = 0;
        }


        /*Get approved payroll batch only*/
//        $payroll_procs = $this->payroll_procs->query()->where('payroll_proc_type_id',2)->whereHas('payrollRunApproval', function($query){
//            $query->where('wf_done', 1);
//        })->orderBy('id', 'desc')->get()->pluck('bquarter', 'id');
        $payroll_procs = $this->payroll_procs->query()->where('payroll_proc_type_id',2)->orderBy('id', 'desc')->get()->pluck('bquarter', 'id');
        return $dataTable->with('input', $date_range)->render('backend/report/payroll/monthly_pension', ["request" => $request,  'payroll_procs' => $payroll_procs,'banks' => $this->banks->getAll()->pluck('name', 'id'), 'total_amount' => $dataTable->findTotalAmount()]);
    }


    /*Payroll verifications reports for all beneficiaries*/
    public function payrollVerifications(PayrollVerificationsDataTable $dataTable, DateRangeRequest $request)
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $date_range['from_date'] = (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null;
        $date_range['to_date'] = (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null;

        $par = [
            'input' => $date_range];


        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($par['input']);
        }else{
            /*return table*/
            return $dataTable->with($par)->render('backend/report/payroll/payroll_verifications', ["request" => $request ]);
        }


    }

    /*Payroll beneficiaries reports*/
    public function payrollBeneficiaries(PayrollBeneficiariesDataTable $dataTable, DateRangeRequest $request)
    {
        $input = $request->all();
        $date_range=  $this->date_ranges->dateRange($request->all());
        $date_range['from_date'] = (isset($date_range['from_date']) And ($date_range['from_date'])) ? Carbon::parse($date_range['from_date'])->format('Y-m-d') : null;
        $date_range['to_date'] = (isset($date_range['to_date']) And ($date_range['to_date'])) ? Carbon::parse($date_range['to_date'])->format('Y-m-d') : null;
        $regions = Region::query()->orderBy('name')->pluck('name', 'id');
        $par = [
            'input' => $date_range];


        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1){
            /*Excel*/
            return  $dataTable->download($par['input']);
        }else{
            /*return table*/

            return $dataTable->with($par)->render('backend/report/payroll/payroll_beneficiaries', ["request" => $request, 'regions' => $regions]);
        }

    }



    /*Payroll Pensioner Statement reports*/
    public function pensionStatement(PensionStatementDataTable $dataTable, DateRangeRequest $request )
    {
        $input = $request->all();

        $date_range=  $this->date_ranges->dateRange($input);
        if(array_key_exists('member_type',$input)){
            $date_range['member_type'] = $input['member_type'];
        }else{
            $date_range['member_type'] = 0;
        }
        $dependents = isset($date_range['dependent_id']) ? Dependent::query()->where('id', $date_range['dependent_id'])->get()->pluck('name', 'id') : [];
        $pensioners = isset($date_range['pensioner_id']) ? Pensioner::query()->where('id', $date_range['pensioner_id'])->get()->pluck('name', 'id') : [];

        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        $export_pdf = isset($input['export_pdf']) ? $input['export_pdf'] : 0;
        if($export_flag == 1){

            if($export_pdf == 0){
                /*Excel*/
                return  $dataTable->download($date_range);
            }else {
                /*pdf*/
                return $dataTable->exportPdf($date_range);
            }
        }else{
            /*return table*/
            return $dataTable->with('input', $date_range)->render('backend/report/payroll/pension_statement', ["request" => $request, 'total_amount' => $dataTable->findTotalAmount(), 'dependents' => $dependents, 'pensioners' => $pensioners]);
        }


    }


    /*PayrollChildExtensions*/
    public function payrollChildExtensions(PayrollChildExtensionsDataTable $dataTable, DateRangeRequest $request)
    {

        $report_title = 'Payroll Child Extensions';
        $input = $request->all();
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        $extension_reasons=CodeValue::query()->where('is_active', 1)->where('code_id', 66)->orderBy('name')->get()->pluck('name', 'id');
        if($export_flag == 1) {
            return $dataTable->download($input);
        }
        if(array_key_exists('search_flag', $input)){
            return $dataTable->with([ 'input' => $input])->render('backend/report/payroll/payroll_child_extensions', ['report_title' => $report_title, 'request' => $input,'extension_reasons'=>$extension_reasons]);
        }else{
            return view('backend/report/payroll/payroll_child_extensions')->with(['request' => $input, 'report_title' => $report_title, 'extension_reasons'=>$extension_reasons]);
        }
    }



    /*Payroll bank updates*/
    public function payrollBankUpdates(PayrollBankUpdatesDataTable $dataTable, DateRangeRequest $request)
    {

        $report_title = 'Payroll Bank Updates';
        $input = $request->all();
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        if($export_flag == 1) {
            return $dataTable->download($input);
        }
        if(array_key_exists('search_flag', $input)){
            return $dataTable->with([ 'input' => $input])->render('backend/report/payroll/payroll_bank_updates', ['report_title' => $report_title, 'request' => $input]);
        }else{
            return view('backend/report/payroll/payroll_bank_updates')->with(['request' => $input, 'report_title' => $report_title]);
        }
    }


    /*Payroll payments summary*/
    public function payrollPaymentsSummary()
    {
        $report_title = 'Payroll Payments Summary';
        $input = request()->all();

        $fiscal_years = (new FinYearRepository())->query()->orderByDesc('start')->pluck('name', 'id');
        $total_summary =isset($input['search_flag']) ? $this->getPaymentSummaryResults($input) : [];
        $period_details = $this->getDateRangesPaySummary($input);
        $with_parameters = [
            'report_title' => $report_title, 'fiscal_years' => $fiscal_years, 'total_summary' => $total_summary
        ];
        $export_flag = isset($input['export_flag']) ? $input['export_flag'] : 0;
        $export_pdf = isset($input['export_pdf']) ? $input['export_pdf'] : 0;

        if($export_flag == 1){

            if($export_pdf == 1){
                $resource_data = [
                    'report_title' => $report_title . ': ' . $period_details['period_title'],
                    'with_parameters' => $with_parameters,
                    'orientation'=> 'Landscape'
                ];
                /*pdf*/
                return $this->exportToPdfSnappyForIncludeContent('backend/report/payroll/includes/payroll_payment_summary/payment_summary',$resource_data);
            }
        }else{

            return view('backend/report/payroll/payroll_payments_summary')->with($with_parameters);
        }

    }

}