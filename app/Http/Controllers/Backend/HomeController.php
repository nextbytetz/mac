<?php

namespace App\Http\Controllers\Backend;

use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerSizeType;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Legal\CaseRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Reporting\DashboardRepository;
use App\Repositories\Backend\Reporting\ReportRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    protected $general_stat  = 0;

    /**
     *  Create a new controller instance.
     *
     * HomeController constructor.
     */
    public function __construct()
    {
        if (request()->has("refresh_statistics")) {
            $value = request()->input("refresh_statistics");
            if ($value == 1) {
                $reportRepo = new ReportRepository();
                $reportRepo->refreshStatitics();
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function blank()
    {
        return view("backend/blank");
    }

    /**
     * @return HomeController|mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function generalStatistics()
    {
        $this->general_stat = 1;
        return $this->index();
    }

    /**
     * @return $this|mixed
     * @throws \App\Exceptions\GeneralException
     * Show the application dashboard.
     */
    public function index()
    {
        $user = access()->user();
        $executive_dashboard = access()->allowOnly("executive_dashboard");
        $selector = 0;
        if ($user->unit()->count()) {
            $unit_id = $user->unit->id;
            //return dd($executive_dashboard);
            switch($unit_id) {
                case 12:
                    //Finance
                    $selector = 12;
                    return ((!$executive_dashboard)) ?  $this->finance($selector) : $this->executiveDashboard($selector);
                    break;
                case 14:
                    //Claim Administration
                    $selector = 14;
                    return ((!$executive_dashboard)) ?  $this->claimAdministration($selector) : $this->executiveDashboard
                    ($selector);
                    break;

                case 17:
                    //Claim Assessment
                    $selector = 14;
                    return ((!$executive_dashboard)) ?  $this->claimAdministration($selector) : $this->executiveDashboard
                    ($selector);
                    break;

                case 15:
                    //Compliance
                    $selector = 15;
                    return ((!$executive_dashboard)) ?  $this->compliance($selector) : $this->executiveDashboard
                    ($selector);
                    break;

                case 6:
                    //Legal
                    $selector = 6;
                    return ((!$executive_dashboard)) ?  $this->legal($selector) : $this->executiveDashboard
                    ($selector);
                    break;
                default:
                    return ((!$executive_dashboard)) ?  $this->defaultView($selector) : $this->executiveDashboard
                    ($selector);
            }
        } else {
            return view('backend.home')
                ->withSelector($selector);
        }
    }

    public function editor()
    {
        return view("backend/editor");
    }

    /**
     * @return array
     */
    private function getFinanceSummary()
    {
        $receipt = new ReceiptRepository();
        $return = [];
        $return = array_merge($return, $receipt->getTotalMonthlyCollection());
        $return = array_merge($return, $receipt->getTotalMonthlyEmployerContribution());
        $return = array_merge($return, $receipt->getTotalMonthlyEmployerInterest());
        $return = array_merge($return, $receipt->getTotalMonthlyReceipts());
        $return = array_merge($return, $receipt->getTotalMonthlyCancelled());
        $return = array_merge($return, $receipt->getTotalMonthlyDishonoured());
        /* from compliance summary */
        $return = array_merge($return, $receipt->getTotalMonthlyContributionTable());
        $return = array_merge($return, $receipt->getTotalMonthlyInterestTable());
        return $return;
    }

    /*  COMPLIANCE  */
    /**
     * @return array
     * @throws \App\Exceptions\GeneralException
     */
    private function getComplianceSummary()
    {
        $receipt = new ReceiptRepository();
        $inspection = new InspectionRepository();
        $dashboard = new DashboardRepository();

        $return = [];
        $return = array_merge($return, $receipt->getTotalMonthlyEmployers());
        $return = array_merge($return, $receipt->getTotalMonthlyEmployerContribution());
        $return = array_merge($return, $receipt->getTotalMonthlyEmployerInterest());
        $return = array_merge($return, $dashboard->getInspectionSummary());
        $return = array_merge($return, $receipt->getTotalMonthlyCancelled());
        $return = array_merge($return, $receipt->getTotalMonthlyDishonoured());
        $return = array_merge($return, $receipt->getTotalMonthlyContributionTable());
        $return = array_merge($return, $receipt->getTotalMonthlyInterestTable());
        $return = array_merge($return, $dashboard->getBookingContributionVariance());

        //        employer summary
        $return = array_merge($return, $dashboard->getComplianceEmployerSummary());
//        employee summary
        $return = array_merge($return, $dashboard->getComplianceEmployeeSummary());
        $return = array_merge($return, $dashboard->complianceStaffPerformance());

        return $return;

    }

    /**
     * @return array
     * @throws \App\Exceptions\GeneralException
     */
    private function getEmployerSummary()
    {
        $dashboard = new DashboardRepository();
        $return = [];
//        employer summary
        $return = array_merge($return, $dashboard->getComplianceEmployerSummary());
        $return = array_merge($return, $dashboard->complianceStaffPerformance());
        return $return;
    }

    /* END COMPLIANCE */

    private function getClaimSummary()
    {
        $dashboard = new DashboardRepository();
        $return = $dashboard->getClaimSummary();
        return $return;
    }

    /**
     * @return array
     */
    private function getLegalSummary()
    {
        $case = new CaseRepository();
        $return = [];
        $return = array_merge($return, $case->getTotalMonthlyCases());
        $return = array_merge($return, $case->getTotalMonthlyClosedCases());
        $return = array_merge($return, $case->getTotalMonthlyCaseMentions());
        return $return;
    }

    /**
     * @param $selector
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    private function executiveDashboard($selector)
    {
        /* Director General - Executive Dashboard */
        $selector = 11;
        if ($this->general_stat == 1)
        {
            $compliance = $this->getComplianceSummary();
            $claim = $this->getClaimSummary();
            $finance = $this->getFinanceSummary();
            $legal = $this->getLegalSummary();
            return view('backend.home')
                ->with("selector", $selector)
                ->with("compliance_summary", $compliance)
                ->with("claim_summary", $claim)
                ->with("finance_summary", $finance)
                ->with("legal_summary", $legal)
                ->with("general_stat", $this->general_stat);
        } else {
            $compliance = $this->getEmployerSummary();
            return view('backend.home')
                ->with("selector", $selector)
                ->with("compliance_summary", $compliance)
                ->with("general_stat", $this->general_stat);
        }

    }

    /**
     * @param $selector
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    private function compliance($selector)
    {


        if ($this->general_stat == 1)
        {
            $compliance = $this->getComplianceSummary();
        } else{
//            $compliance = $this->getEmployerSummary();
            return   $this->staffEmployerPerformanceDashboard();
        }
        return view('backend.home')
            ->with("selector", $selector)
            ->with("compliance_summary", $compliance)
            ->with("general_stat", $this->general_stat);

    }

    private function finance($selector)
    {
        $finance = $this->getFinanceSummary();
        return view('backend.home')
            ->with("selector", $selector)
            ->with("finance_summary", $finance);

    }

    private function claimAdministration($selector)
    {
        $claim = $this->getClaimSummary();
        return view('backend.home')
            ->with("selector", $selector)
            ->with("claim_summary", $claim);
    }
    private function legal($selector)
    {
        $legal = $this->getLegalSummary();
        return view('backend.home')
            ->with("selector", $selector)
            ->with("legal_summary", $legal);
    }

    private function defaultView($selector)
    {
        return view('backend.home')
            ->with("selector", $selector);
    }


    /*Staff employer - debt collection module*/
    public function staffEmployerPerformanceDashboard()
    {
        $check_intern = StaffEmployerAllocation::query()
            ->join('staff_employer as su', 'su.staff_employer_allocation_id', 'staff_employer_allocations.id')
            ->where('staff_employer_allocations.isactive',1)->whereIn('staff_employer_allocations.category', [5])->where('su.user_id', access()->id())->count();

              if($check_intern == 0){
            return $this->staffEmployerPerformanceOfficerDashboard();
        }else{
            return $this->staffEmployerPerformanceInternDashboard();
        }

    }

    /*Dashboard collection performance for Officers*/
    public function staffEmployerPerformanceOfficerDashboard()
    {
        $staff_employer_repo = new StaffEmployerRepository();
        $staff_performances = $staff_employer_repo->getStaffEmployerPerformance();
        $total_summary = $staff_employer_repo->getStaffEmployerPerformanceTotalSummary();
        return view('backend/operation/compliance/member/employer/staff_relationship/performance_dashboard/collection_dashboard/index')
            ->with('staff_performances', $staff_performances)
            ->with('total_summary',$total_summary);
    }


    /*Dashboard collection performance for Intern*/
    public function staffEmployerPerformanceInternDashboard()
    {

        $staff_employer_repo = new StaffEmployerRepository();
        $staff_performances = $staff_employer_repo->getStaffEmployerPerformanceIntern();
        $total_summary = $staff_employer_repo->getStaffEmployerPerformanceTotalSummaryIntern();
        return view('backend/operation/compliance/member/employer/staff_relationship/performance_dashboard/collection_intern_dashboard/index')
            ->with('staff_performances', $staff_performances)
            ->with('total_summary',$total_summary);
    }

}
