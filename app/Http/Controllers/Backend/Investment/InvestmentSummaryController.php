<?php

namespace App\Http\Controllers\Backend\Investment;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;
use App\Repositories\Backend\Investiment\InvestimentBudgetRepository;
use App\Repositories\Backend\Investiment\InvestimentSummaryRepository;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Khill\Lavacharts\Lavacharts;
use Carbon\Carbon;
use Validator;
use Log;
use Lava;

use App\Models\Finance\FinYear;
use App\Models\Investiment\InvestimentFixedDeposit;
use App\Models\Investment\InvestmentBudget;
use App\Models\Investment\InvestmentBudgetAllocation;

class InvestmentSummaryController extends Controller
{

	public function __construct()
	{
		$this->code_values = new CodeValueRepository();
		$this->code = 57;
		$this->investiment_type = new InvestimentTypeRepository();
		$this->investment_budget = new InvestimentBudgetRepository();
		$this->investiment_summary = new InvestimentSummaryRepository();
	}

	public function calculate(Request $request){
        return $this->investiment_summary->calculate($request);
	}

	public function index(){
		$fin_years = DB::table('fin_years')->get();
		return view('backend.investment.investiment_types.investiment_summary.index',compact('fin_years'));
	}

	public function datatable($fin_id){
		return $this->investiment_summary->summaryDatatable($fin_id);
	}

	public function investmentType($fin_id, $budget_alocation_id, $reference){
		$investment_type = DB::table('code_values')->select('name','reference')->where('reference',$reference)->first();
		$period = DB::table('fin_years')->select('name','id')->where('id',$fin_id)->first();
		$budget = DB::table('main.investment_budget_allocation')
		->select('investment_budget_allocation.amount as amount_alocated','investment_budgets.amount as total_amount_invested')
		->join('investment_budgets','investment_budgets.id','=','investment_budget_allocation.investment_budget_id')
		->where('investment_budget_allocation.id',$budget_alocation_id)
		->first();

		return view('backend.investment.investiment_types.investiment_summary.investment_type',compact('fin_id','budget_alocation_id','reference','investment_type','period','budget'));

	}

	public function investmentTypeDatatable($fin_id, $budget_alocation_id, $reference){
		return $this->investiment_summary->investmentTypeDatatable($fin_id, $budget_alocation_id, $reference);

	}

	public function show($investment_id, $reference, $fin_id){
		$issuer = '';
		$investment = $this->investiment_summary->findInvestment($investment_id, $reference);
		if ($reference == 'INTTBILLS' || $reference == 'INTTBONDS') {
			$issuer = 'BOT';
		} elseif($reference == 'INTFDR') {
			$issuer = $investment->institution_name;
		}else{
			$issuer = $investment->issuer;
		}
		$currencies = DB::table('main.currencies')->get();
		$period = DB::table('fin_years')->select('name')->where('id',$fin_id)->first();
		$budget = $this->investiment_summary->budget($investment_id, $reference);

		$fin_years = DB::table('fin_years')->get();
		$coupon_tenure = DB::table('main.coupon_tenure')
		->select('years','days','coupon_rate','id')->get();
		$types = DB::table('investment_code_types')
		->where('code_value_reference','INTCBONDS')
		->pluck('name','id');

		return view('backend.investment.investiment_types.investiment_summary.show',compact('fin_id','reference','investment_id','period','budget','issuer','investment','currencies','fin_years','types','coupon_tenure'));

	}



	public function couponTenure($days){
         $coupon_rate = DB::table('main.coupon_tenure')->select('coupon_rate')->where('days',$days)->first();
         return response()->json($coupon_rate);
	}

	public function showDatatable($investment_id, $reference){
		return $investment = $this->investiment_summary->findInvestmentDatatable($investment_id, $reference);

	}

	public function showFixedDatatable($investiment_id){
        return $investment = $this->investiment_summary->showFixedDepositDatatable($investiment_id);
	}

	public function showTBondsDatatable($investiment_id){
        return $investment = $this->investiment_summary->showTBondsDatatable($investiment_id);
	}

	public function showTBillsDatatable($investiment_id){
        return $investment = $this->investiment_summary->showTBillsDatatable($investiment_id);
	}

	public function showCBondsDatatable($investiment_id){
        return $investment = $this->investiment_summary->showCBondsDatatable($investiment_id);
	}
}
