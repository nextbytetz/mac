<?php

namespace App\Http\Controllers\Backend\Investment;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;
use App\Repositories\Backend\Investiment\InvestimentBudgetRepository;
use App\DataTables\WorkflowTrackDataTable;

use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Khill\Lavacharts\Lavacharts;
use Carbon\Carbon;
use Validator;
use Log;
use Lava;

use App\Models\Finance\FinYear;
use App\Models\Investment\InvestmentBudget;
use App\Models\Investment\InvestmentBudgetAllocation;

class InvestmentBudgetController extends Controller
{

	public function __construct()
	{
		$this->code_values = new CodeValueRepository();
		$this->code = 60;
		$this->investment_budget = new InvestimentBudgetRepository();
		$this->wf_module_group_id = 29;
	}


	public function index()
	{
		$investment_types = $this->code_values->getCodeValuesByCodeForSelect($this->code);
		$fy = $this->investment_budget->query()->get();
		$fy_uploaded = [];
		foreach ($fy as $f) {
			array_push($fy_uploaded, $f->fin_year_id);
		}
		$fin_years = FinYear::whereNotIn('id',$fy_uploaded)->get();
		return view('backend.investment.investment_budget.index')
		->with('investment_types',$investment_types)->with('fin_years',$fin_years);
	}

	public function show($investment_budget_id,WorkflowTrackDataTable $workflowTrack)
	{
		$budget = $this->investment_budget->findOrThrowException($investment_budget_id);
		// $dataTable = $workflowTrack;
		return $workflowTrack->render('backend.investment.investment_budget.show', [
			'budget' => $budget,
			'workflowtrack' => $workflowTrack
		]);
	}



	public function set(Request $request)
	{
		
		$rules = [
			'fin_year_id' => 'required', 
			'budget_file' => 'required|mimes:xlsx,xls', 
		];

		$message = [
			'fin_year_id.required' => 'kindly select financial year', 
			'budget_file.*' => 'Kindly upload an excel (xls,xlsx) file', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			try {
				$this->investment_budget->uploadBudget($request);
				return response()->json(array("success"=>true));
			} catch (Exception $e) {
				return response()->json(array('errors' => true));
			}

		}
	}


	public function datatable()
	{
		$budgets =  $this->investment_budget->getForDatatable();
		return DataTables::of($budgets)
		->editColumn("amount", function($budgets){
			return number_format($budgets->amount,2) ;
		})
		->addColumn("action", function($data){
			if (empty($data->status) || ($data->status == 3) ) {
				return '<a href="" class="btn btn-success edit_budget_btn" data-id="'.$data->id.'" data-fin_year="'.$data->name.'" 
				data-fin_year_id="'.$data->fin_year_id.'">Edit</a>';
			}
		})
		->make(true);
	}


	public function edit($investment_budget_id)
	{
		$budget = $this->investment_budget->query()
		->select('*','investment_budgets.id as id','investment_budget_allocation.amount as allocated_amount','investment_budget_allocation.id as allocation_id')
		->join('fin_years', 'investment_budgets.fin_year_id', '=', 'fin_years.id')
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->where('investment_budgets.id',$investment_budget_id)->get();
		return response()->json(array('success' => true, 'budget' => $budget));

	}



	public function update(Request $request, $investment_budget_id)
	{

		$rules = [
			'edit_financial_year' => 'required', 
			'edit_total_budget' => 'required|numeric|min:0|not_in:0',
		];

		$message = [
			'edit_financial_year.required' => 'kindly select financial year', 
			'edit_total_budget.required' => 'Total budget can\'t be 0', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$budget_exist = $this->investment_budget->findByFinYearAndId($request->edit_budget_id,$request->edit_financial_year);
			if (count($budget_exist)) {
				$this->investment_budget->updateBudget($request);
				return response()->json(array("success"=>true));
			}else{
				return response()->json(array('errors' => ['financial_year' => 'Request failed! The edit for this financial year has failed... Please try again']));
			}


		}
	}


	public function budgetDatatable($investment_budget_id)
	{
		$budget =  $this->investment_budget->query()
		->select('*','investment_budgets.id as id','code_values.name as code_value','investment_budget_allocation.amount as allocated_amount','investment_budget_allocation.id as allocation_id')
		->join('fin_years', 'investment_budgets.fin_year_id', '=', 'fin_years.id')
		->join('investment_budget_allocation', 'investment_budget_allocation.investment_budget_id', '=', 'investment_budgets.id')
		->join('code_values', 'investment_budget_allocation.code_value_id', '=', 'code_values.id')
		// ->join('investment_code_types', 'investment_budget_allocation.code_value_id', '=', 'investment_code_types.id')
		->whereNotIn('code_values.reference',['INTRESTATE'])
		->where('investment_budgets.id',$investment_budget_id);
		return DataTables::of($budget)
		->editColumn("name", function($budget){
			if (!empty($budget->investment_code_type_id)) {
				return $this->investment_budget->returnInvestmentCodeTypeName($budget->investment_code_type_id);
			} else {
				return ucwords(strtolower($budget->code_value));
			}
		})->editColumn("allocated_amount", function($budget){
			return number_format($budget->allocated_amount,2) ;
		})->editColumn("percent", function($budget){
			return !empty($budget->percent) ? $budget->percent.'%' : '';
		})->make(true);
	}


	public function bondCalculator()
	{
		return view('backend.investment.calculator')->with('budget',$budget); 	
	}



	public function recommendBudget($investment_budget_id)
	{
		// access()->hasWorkflowDefinition($this->wf_module_group_id, 1);
		$wf_initiated = $this->investment_budget->recommendAndinitiateWf($investment_budget_id);
		if ($wf_initiated) {
			return response()->json(array("success"=>true));
		}else{
			return response()->json(array("success"=>false));
		}

	}


	public function returnAllocatedBudget($fin_year_id,$code_value_id)
	{
		$code_value = $this->code_values->query()->find($code_value_id);

		if (count($code_value)) {
			switch ($code_value->reference) {
				case 'INTFDR':
				dd($name);
				break;
				case 'INTTBILLS':
				dd($name);
				break;
				case 'INTTBONDS':
				dd($name);
				break;
				case 'INTCBONDS':
				dd($name);
				break;
				case 'INTEQUITIES':
				dd($name);
				break;
				case 'INTCIS':
				dd($name);
				break;
				case 'INTRESTATE':
				dd($name);
				break;
				case 'INTLOAN':
				$inv_budget = $this->investment_budget->returnLoanAllocatedBudget($fin_year_id,$code_value_id);
				return response()->json(array("success"=>true, 'allocated_budget' => $inv_budget));
				break;
				case 'INTCA':
				dd($name);
				break;
				default:
				return response()->json(array("success"=>false));
				break;
			}
		} else {
			return response()->json(array("success"=>false));	
		}
	}


}
