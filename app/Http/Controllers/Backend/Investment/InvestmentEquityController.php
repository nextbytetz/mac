<?php

namespace App\Http\Controllers\Backend\Investment;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use Log;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;
use App\Repositories\Backend\Investiment\InvestmentEquityRepository;

class InvestmentEquityController extends Controller
{

	public function __construct()
	{
		$this->code_values = new CodeValueRepository();
		$this->code_reference = 'INTEQUITIES';
		$this->investiment_type = new InvestimentTypeRepository();
		$this->investment_equity = new InvestmentEquityRepository();
	}

	public function index($budget_allocation_id=null)
	{

		$summary = $this->investment_equity->getSummary();
		return view('backend.investment.investiment_types.equities.index')->with('summary',$summary);
		// ->with('budget_allocation_id',$budget_allocation_id);
	}


	public function saveIssuer(Request $request)
	{
		$rules = [
			'issuer_source' => 'required',
			'issuer_reference' => 'required',
		];
		$arr = [];
		if (!empty($request->issuer_source)) {
			if ($request->issuer_source == 'MAC') {
				$arr = [
					'issuer'=>'required',
				];
			} else {
				$arr = [
					'issuer_name'=>'required',
				];
			}
		}

		$rules = array_merge($rules,$arr);
		$message = [
			'issuer.*' => 'Kindly select issuer', 
			'issuer_name.*' => 'kindly enter issuer name', 
			'issuer_reference.*' => 'kindly enter issuer reference', 
			'issuer_source.*' => 'kindly pick issuer source', 
		];
		
		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			if ($request->issuer_source == 'MAC') {
				$issuer_exist = $this->investment_equity->findByEmployerId($request->issuer);
			} else {
				$issuer_exist = $this->investment_equity->findByNameAndReference($request->issuer_name,$request->issuer_reference);
			}
			if (count($issuer_exist)) {
				return response()->json(array('errors' => [
					'issuer' => ['Issuer already exist'],
					'issuer_name' => ['Issuer already exist'],
				]));
			} 
			$return = $this->investment_equity->saveIssuer($request->all());
			return response()->json(array('success' => $return));
		}
	}


	public function datatable($budget_allocation_id = null)
	{
		$issuers = $this->investment_equity->getEquityForDatatable();
		return DataTables::of($issuers)
		->addColumn("total_shares", function($issuer){
			return number_format($issuer->total_shares,0);
		})->addColumn("shares_value", function($issuer){
			return number_format($issuer->shares_value,2);
		})->make(true);
	}


	public function show($equity_id)
	{
		$issuer = $this->investment_equity->findOrThrowException($equity_id);
		return view('backend.investment.investiment_types.equities.show')->with('issuer',$issuer);
	}

	public function updatesDatatable($equity_id)
	{
		$equity_updates = $this->investment_equity->getUpdatesForDatatable($equity_id);
		return DataTables::of($equity_updates)
		->editColumn("share_value", function($update){
			return number_format($update->share_value,4);
		})
		->editColumn("value_date", function($update){
			return Carbon::parse($update->value_date)->format('jS F, Y');
		})
		->editColumn("number_of_share", function($update){
			return number_format($update->number_of_share,0);
		})
		->editColumn("total_share_value", function($update){
			return number_format($update->total_share_value,2);
		})
		->make(true);

	}

	public function lotsDatatable($equity_id)
	{
		$equity_lots = $this->investment_equity->getLotsForDatatable($equity_id);
		return DataTables::of($equity_lots)
		->editColumn("price_per_share", function($lot){
			return number_format($lot->price_per_share,4);
		})
		->editColumn("purchase_date", function($lot){
			return Carbon::parse($lot->purchase_date)->format('jS F, Y');
		})
		->editColumn("number_of_share", function($lot){
			return number_format($lot->number_of_share,0);
		})
		->editColumn("remaining_share", function($lot){
			return number_format($lot->remaining_share,0);
		})
		->editColumn("total_price", function($lot){
			return number_format($lot->total_price,2);
		})
		->make(true);

	}


	public function create($investment_allocation_id=null)
	{
		$code_value = $this->investiment_type->getCodeTypeByReference($this->code_reference);
		$issuers = $this->investment_equity->getIssuers();
		return view('backend.investment.investiment_types.equities.create')
		->with('code_value',$code_value)->with('issuers',$issuers);
		// ->with('budget_allocation_id',$budget_allocation_id);
	}


	public function save(Request $request)
	{
		$rules = [
			'category' => 'required',
			'issuer' => 'required', 
			'purchase_date' => 'required|date', 
			'price_per_share' => 'required', 
			'total_issued_shares' => 'required|numeric',
			'number_of_shares' => 'required|numeric',
		];
		$cat = DB::table('main.investment_code_types')->where('id',$request->category)->first();

		if (!empty($cat->name)) {
			if ($cat->name == 'Listed Equities') {
				$rules = array_merge(
					$rules,
					[
						'broker' => 'required', 
						'commission' => 'required',
						'allotment_date' => 'required|date',
						'subscription_date' => 'required|date',
					]
				);
			}
		} 

		$message = [
			'category.required' => 'kindly select applicable category', 
			'issuer.required' => 'kindly select issuer', 
			'allotment_date.required' => 'kindly enter allotment date', 
			'purchase_date.required' => 'kindly enter purchase date', 
			'disbursement_date.required' => 'kindly enter disbursement date', 
			'subscription_date.required' => 'kindly enter subscription date', 
			'price_per_share.required' => 'kindly enter share price', 
			'total_issued_shares.required' => 'kindly enter total issued shares', 
			'number_of_shares.required' => 'kindly enter number of purchased shares',
			'broker.required' => 'kindly select broker', 
			'commission.required' => 'kindly enter commission',
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$allocation = $this->investment_equity->returnBudgetAllocationDetails(Carbon::parse($request->purchase_date)->format('Y-m-d'),$cat->id);
			if (empty($allocation)) {
				return response()->json(array('errors' => ['purchase_date' => ['The budget is not set for this date... Please try again']]));
			} else {
				$return = $this->investment_equity->saveNewEquity($request->all(),$cat,$allocation);
				return response()->json(array('success' => $return));
			}
		}
	}


	public function returnLotNumber($issuer_id)
	{
		$lot_number = $this->investment_equity->returnLotNumberForSave($issuer_id);
		if (!empty($lot_number)) {
			return response()->json(array("success"=>true, "lot"=>$lot_number));
		} else {
			return response()->json(array("success"=>false));
		}
	}	



	public function updatePrice(Request $request)
	{
		// dd($request->all());

		$rules = [
			'price_date' => 'required|date', 
			'price_per_share' => 'required|numeric',
			'equity_issuer_id' => 'required|numeric',
			'number_of_share' => 'required|numeric',
		];

		$message = [
			'price_per_share.*' => 'Price per share is required', 
			'price_date.*' => 'kindly choose price date', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$return = $this->investment_equity->updateLotPrice($request->all());
			return response()->json(array('success' => $return));
		}
	}


	public function salesDatatable($issuer_id)
	{
		$sales = $this->investment_equity->getSalesForDatatable($issuer_id);
		return DataTables::of($sales)
		->editColumn("share_value", function($sale){
			return number_format($sale->share_value,4);
		})
		->editColumn("selling_date", function($sale){
			return Carbon::parse($sale->selling_date)->format('jS F, Y');
		})
		->editColumn("number_of_share", function($sale){
			return number_format($sale->number_of_share,0);
		})
		->editColumn("total_share_value", function($sale){
			return number_format($sale->total_share_value,2);
		})
		->make(true);
	}


	public function sell(Request $request)
	{
		$rules = [
			'sell_lot' => 'required',
			'number_of_shares' => 'required|numeric',
			'price_per_share' => 'required|numeric',
			'selling_date' => 'required|date', 
			'broker' => 'required',
			'commission' => 'required|numeric'
		];

		$message = [
			'sell_lot.*' => 'Kindly select lot', 
			'number_of_shares.*' => 'Number of shares is required and should be number', 
			'price_per_share.*' => 'Price per share is required and should be a number', 
			'selling_date.*' => 'kindly enter price date',
			'broker.*' => 'kindly enter broker',
			'commission.*' => 'kindly enter broker commission',
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$return = $this->investment_equity->sellShares($request->all());
			return response()->json(array('success' => $return));
		}
	}


	public function dividendDatatable($equity_id)
	{
		$dividends = $this->investment_equity->getDividendsForDatatable($equity_id);
		return DataTables::of($dividends)
		->editColumn("dividend_declaration_date", function($dividend){
			return Carbon::parse($dividend->dividend_declaration_date)->format('jS F, Y');
		})
		->editColumn("dividend_payment_date", function($dividend){
			return Carbon::parse($dividend->dividend_payment_date)->format('jS F, Y');
		})
		->editColumn("number_of_share", function($dividend){
			return number_format($dividend->number_of_share,0);
		})
		->editColumn("share_value", function($dividend){
			return number_format($dividend->share_value,4);
		})
		->editColumn("total_share_value", function($dividend){
			return number_format($dividend->total_share_value,2);
		})
		->make(true);
	}

	public function addDividend(Request $request)
	{
		
		$rules = [
			'dividend_per_share' => 'required|numeric',
			'declaration_date' => 'required|date', 
			'dividend_payment_date' => 'required|date', 
			'id' => 'required', 
		];

		$message = [
			'dividend_per_share.*' => 'Dividend value per share is required', 
			'declaration_date.*' => 'kindly enter dividend declaration date',
			'dividend_payment_date.*' => 'kindly enter dividend payment date',
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$return = $this->investment_equity->saveDividend($request->all());
			return response()->json(array('success' => $return));
		}
	}

}
