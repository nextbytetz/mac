<?php

namespace App\Http\Controllers\Backend\Investment;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;
use App\Repositories\Backend\Investiment\InvestimentBudgetRepository;

use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Khill\Lavacharts\Lavacharts;
use Carbon\Carbon;
use Validator;
use Log;
use Lava;

use App\Models\Finance\FinYear;
use App\Models\Investiment\InvestimentFixedDeposit;
use App\Models\Investment\InvestmentBudget;
use App\Models\Investment\InvestmentBudgetAllocation;
use App\Models\Investment\InvestmentLoan;
use App\Models\Investment\InvestmentCallAccount;
use App\Models\Investiment\InvestimentTreasuryBill;
use App\Models\Investiment\InvestimentTreasuryBond;
use App\Models\Investiment\InvestimentCorporateBond;
use App\Http\Requests\Backend\Investment\FixedDepositRequest;
use App\Http\Requests\Backend\Investment\TreasuryBondRequest;
use App\Http\Requests\Backend\Investment\TreasuryBillRequest;
use App\Http\Requests\Backend\Investment\CorporateBondRequest;



class InvestmentController extends Controller
{

	public function __construct()
	{
		$this->code_values = new CodeValueRepository();
		$this->code = 60;
		$this->investiment_type = new InvestimentTypeRepository();
		$this->investment_budget = new InvestimentBudgetRepository();
	}
    //

	public function bondCalculate(Request $request){
		$bond_values = [];

		$settlement_date =  Carbon::parse($request->settlement)->format('Y-m-d');
		$maturity_date =  Carbon::parse($request->maturity)->format('Y-m-d');

		$price = $this->investiment_type->price($settlement_date, $maturity_date, $request->rate, $request->yield, $request->redemption, $request->frequency, $request->basis);
		$coupon_rate = ($coupon_rate / 100);
		$accrued_interest_price = ((($coupon_rate * $diff_now) / 365) * 100);
		$accrued =  round($accrued_interest_price, 2);
		$full_coupon = (365 / 2);
		$dirty_price = ($price + $accrued_interest_price);
		$actual = (($dirty_price / 100) * $face_value);
		$cost = (($accrued / 100) * $face_value);
		$bond_values = [
			'price' => $price,
			'dirty_price' => $dirty_price,
			'full_coupon' => $full_coupon,
			'accrued_interest_price' => $accrued_interest_price,
			'actual' => $actual,
			'cost' => $cost,
		];

		return $bond_values;


	}

	public function dashboard()
	{
		
		$data = Lava::DataTable();
		$data->addStringColumn('Reasons')
		->addNumberColumn('Percent')
		->addRow(array('Fixed Deposit', 5))
		->addRow(array('Treasury Bills', 2))
		->addRow(array('Treasury Bonds', 4))
		->addRow(array('Corporate Bonds',6))
		->addRow(array('Equities',8))
		->addRow(array('Collective Investment Schemes',23))
		->addRow(array('Real Estate',12))
		->addRow(array('Loans',3))
		->addRow(array('Call Accounts',4)
	);
		Lava::PieChart('Investment', $data, [
			'title' => 'Portfolio Budget (Fixed Deposit,Treasury Bills,Treasury Bonds, Real Estate,Loan Call Account)',
			'is3D' => true,
			'height' => 490,
			'width'  => 650,
			'slices' => [
				['offset' => 0.2],
				['offset' => 0.25],
				['offset' => 0.3]
			]
		]);

		return view('backend.investment.dashboard');
	}


	public function chooseTypeForm()
	{
		// $investment_types = $this->code_values->getCodeValuesByCodeForSelect(57);
		return view('backend.investment.choose_type_form');
	}


	public function chooseType(Request $request)
	{
		$rules = [
			'investment_type' => 'sometimes|required', 
		];
		$message = [
			'investment_type.required' => 'Please search and select investment type', 
		];
		$validator = Validator::make($request->all(),$rules, $message);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator);
		}else{

			switch ($request->reference) {
				case 'INTFDR':
				return $this->investiment_type->fixedDepositIndex();
				break;
				case 'INTTBILLS':
				return $this->investiment_type->treasuryBillIndex();
				break;
				case 'INTTBONDS':
				return $this->investiment_type->treasuryBondIndex();
				break;
				case 'INTCBONDS':
				return $this->investiment_type->corporateBondIndex();
				break;
				case 'INTEQUITIES':
				return redirect()->route('backend.investment.equity.create');
				break;
				case 'INTCIS':
				return redirect()->route('backend.investment.collective_scheme.index');
				break;
				case 'INTRESTATE':
				dd('Real Estate');
				break;
				case 'INTLOAN':
				return redirect()->route('backend.investment.loan.create');
				dd('Loans');
				break;
				case 'INTCA':
				return $this->investiment_type->callAccountIndex();
				break;
				default:
				return redirect()->back()->withErrors(['investment_type'=>'Investment type not found']);
				break;
			}
		}
	}

	public function returnInvestmentTypes()
	{
		$input = request()->all();
		$data = [];
		if(isset($input['q'])){
			$search = strtolower(trim($input['q']));
			$data = DB::table('main.code_values')
			->where('name', 'ilike', '%'.$search.'%')
			->where('code_id',$this->code)
			->get();
			if (count($data) < 1) {
				$data = DB::table('main.code_values')
				->where('reference', 'ilike', '%'.$search.'%')
				->where('code_id',$this->code)
				->get();
			}
		}

		return $data;
	}


	public function show($investment_type,$investment_allocation_id)
	{
		$name = (ucwords(str_replace('_', ' ', $investment_type)));
		$inv = $this->investment_budget->findAllocationOrThrowException($investment_allocation_id);

		switch ($inv->codeValue->reference) {
			case 'INTFDR':
			dd($name);
			break;
			case 'INTTBILLS':
			dd($name);
			break;
			case 'INTTBONDS':
			dd($name);
			break;
			case 'INTCBONDS':
			dd($name);
			break;
			case 'INTEQUITIES':
			return redirect()->route('backend.investment.equity.index',$inv->id);
			break;
			case 'INTCIS':
			return redirect()->route('backend.investment.collective_scheme.index',$inv->id);
			break;
			case 'INTRESTATE':
			dd($name);
			break;
			case 'INTLOAN':
			return view('backend.investment.investiment_types.loans.index')->with('loan_budget',$inv);
			break;
			case 'INTCA':
			dd($name);
			break;
			default:
			return redirect()->back()->withErrors(['investment_type'=>'Investment type not found']);
			break;
		}



	}

	public function budgetDatatable()
	{
		$budgets = DB::table('main.investment_budgets')
		->join('fin_years', 'investment_budgets.fin_year_id', '=', 'fin_years.id');
		return DataTables::of($budgets)
		->editColumn("amount", function($budgets){
			return number_format($budgets->amount,2) ;
		})
		->addColumn("action", function($budgets){
			return '<a href="" class="btn btn-success edit_budget">Edit</a>';
		})->make(true);
	}

	public function fixedDeposit(FixedDepositRequest $request){
		return $this->investiment_type->fixedDeposit($request);
	}

	public function fixedDepositStoreUpdate(FixedDepositRequest $request){
		return $this->investiment_type->fixedDepositStoreUpdate($request);
	}

	public function fixedDepositUpdate(Request $request){
		if($request->interest_received != null or $request->principal_redeemed != null or $request->penalty_received != null or $request->payment_date_received != null){
			$rules = [
				'payment_date_received' => 'sometimes|required|date', 
				'exchange_rate' => 'sometimes|required|numeric',
				'principal_redeemed' => 'required',
				'interest_received' => 'required',
			];

			$message = [
				'payment_date_received.required' => 'kindly enter Date of Payment Received', 
				'exchange_rate.required' => 'kindly enter exchange rate',
				'principal_redeemed.required' => 'kindly enter Principal Redeemed',
				'interest_received.required' => 'kindly enter Interest Received',
			];

			$validator = Validator::make(Input::all(),$rules, $message);
			if ($validator->fails()) {
				return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
			}else{
				return $this->investiment_type->updateFixedDeposit($request);
			}

		}elseif($request->interest_received == null and $request->principal_redeemed == null and $request->penalty_received == null and $request->payment_date_received == null and $request->certificate_number == null){
			return response()->json(array('error' => 'No update made'));
		}
		else{
			return $this->investiment_type->updateFixedDeposit($request);
		}
	}

	public function treasuryBondsUpdate(Request $request){
		// dd($request->all());
		$rules = [
			'interest_earned' => 'required',
			'payment_date' => 'required|date',
			'interest_date' => 'required|date', 
		];

		$message = [
			'payment_date.required' => 'kindly enter Payment Date',
			'interest_earned.required' => 'kindly enter Interest Earned', 
			'interest_date.required' => 'kindly enter Interest Date', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			return $this->investiment_type->treasuryBondsUpdate($request);
		}
	}

	public function treasuryBillsUpdate(Request $request){
		// dd($request->all());
		$rules = [
			'interest_earned' => 'required',
			'payment_date' => 'required|date',
			// 'interest_date' => 'required|date', 
		];

		$message = [
			'payment_date.required' => 'kindly enter Payment Date',
			'interest_earned.required' => 'kindly enter Interest Earned', 
			// 'interest_date.required' => 'kindly enter Interest Date', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			return $this->investiment_type->treasuryBillsUpdate($request);
		}
	}

	public function corporateBondsUpdate(Request $request){
		// dd($request->all());
		$rules = [
			'interest_earned' => 'required',
			'payment_date' => 'required|date',
			'interest_date' => 'required|date', 
		];

		$message = [
			'payment_date.required' => 'kindly enter Payment Date',
			'interest_earned.required' => 'kindly enter Interest Earned', 
			'interest_date.required' => 'kindly enter Interest Date', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			return $this->investiment_type->corporateBondsUpdate($request);
		}
	}


	public function getFixedDeposit($financial_year_id){
		return $this->investiment_type->getFixedDeposit($financial_year_id);

	}

	public function treasuryBills(TreasuryBillRequest $request){
		return $this->investiment_type->treasuryBills($request);
	}

	public function treasuryBillsStoreUpdate(TreasuryBillRequest $request){
		return $this->investiment_type->treasuryBillsStoreUpdate($request);
		
	}

	public function getTreasuryBills($financial_year_id){
		return $this->investiment_type->getTreasuryBills($financial_year_id);
	}

	public function treasuryBonds(TreasuryBondRequest $request){

		$table = 'main.investiment_treasury_bonds';
		$auction = $this->investiment_type->checkAuctionNumberDates($request->auction_number, $request->maturity_date,$table);

		if ($auction['exists'] && !$auction['proceed']) {
			return response()->json(array('auction_error' => 'Error! allowed maturity date is '.$auction['maturity_date'].'  for '.$request->auction_number.' auction number!'));
		}else{

			return $this->investiment_type->treasuryBonds($request);
		}
		
	}

	public function treasuryBondsStoreUpdate(TreasuryBondRequest $request){
		$table = 'main.investiment_treasury_bonds';
		$auction = $this->investiment_type->checkAuctionNumberDates($request->auction_number, $request->maturity_date,$table);

		if ($auction['exists'] && !$auction['proceed']) {
			return response()->json(array('auction_error' => 'Error! allowed maturity date is '.$auction['maturity_date'].'  for '.$request->auction_number.' auction number!'));
		}else{

			return $this->investiment_type->treasuryBondsStoreUpdate($request);
		}
		
	}

	public function getTreasuryBonds($financial_year_id){
		return $this->investiment_type->getTreasuryBonds($financial_year_id);

	}

	public function corporateBonds(CorporateBondRequest $request){
		
		$table = 'main.investiment_corporate_bonds';
		$auction = $this->investiment_type->checkAuctionNumberDates($request->auction_number, $request->maturity_date,$table);
        if ($auction['exists'] && !$auction['proceed']) {
			return response()->json(array('auction_error' => 'Error! allowed maturity date is '.$auction['maturity_date'].'  for '.$request->auction_number.' auction number!'));
		}else{

			return $this->investiment_type->corporateBonds($request);
		}
		
	}

	public function corporateBondsStoreUpdate(CorporateBondRequest $request){

		$table = 'main.investiment_corporate_bonds';
		$auction = $this->investiment_type->checkAuctionNumberDates($request->auction_number, $request->maturity_date,$table);
        if ($auction['exists'] && !$auction['proceed']) {
			return response()->json(array('auction_error' => 'Error! allowed maturity date is '.$auction['maturity_date'].'  for '.$request->auction_number.' auction number!'));
		}else{

			return $this->investiment_type->corporateBondsStoreUpdate($request);
		}
		
	}

	public function getCorporateBonds($financial_year_id){
		return $this->investiment_type->getCorporateBonds($financial_year_id);

	}

	public function loanCreate($investment_allocation_id=null)
	{
		$code_value = $this->code_values->findByReference('INTLOAN');
		$fin_years = $this->investment_budget->returnBudgetFinYearForSelect();
		return view('backend.investment.investiment_types.loans.create')
		->with('code_value',$code_value)
		->with('fin_years',$fin_years);
	}

	public function loanSave(Request $request)
	{
		$rules = [
			'fin_year' => 'required',
			'loan_type' => 'required',
			'loan_amount' => 'required|numeric|min:0|not_in:0',
			'loanee' => 'required', 
			'disbursement_date' => 'required|date', 
			'maturity_date' => 'required|date',
			'interest_rate'=> 'required|numeric|max:100',
			'penalty_rate'=> 'required|numeric|max:100',
			'tax_rate'=> 'required|numeric|max:100'
		];

		$message = [
			'fin_year.required' => 'kindly select financial year',
			'loan_type.required' => 'kindly select loan category', 
			'loan_amount.*' => 'Loan amount should not be zero', 
			'loanee.required' => 'kindly enter loanee name', 
			'disbursement_date.*' => 'kindly enter disbursement date', 
			'maturity_date.*' => 'kindly enter maturity date', 
			'interest_rate.required' => 'kindly enter interest rate',
			'penalty_rate.required' => 'kindly enter penalty rate',
			'tax_rate.required' => 'kindly enter tax rate',
			'interest_rate.max' => 'interest rate should not exceed 100',
			'penalty_rate.max' => 'penalty rate should not exceed 100',
			'tax_rate.max' => 'tax rate should not exceed 100',
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$allocation_budget  = InvestmentBudgetAllocation::where('fin_year_id',$request->fin_year)
			->where('code_value_id',$request->code_value_id)->first();
			$this->investiment_type->saveNewLoan($request);
			if (count($allocation_budget)) {
				return response()->json(array("success"=>true, 'allocation_id'=>$allocation_budget->id));
			} else {
				return response()->json(array("success"=>true));
			}
			
			
		}
	}

	public function loansDatatable($budget_allocation_id)
	{
		$loans =  $this->investiment_type->loansForDatatable($budget_allocation_id);
		return DataTables::of($loans)
		->editColumn("maturity_date", function($loan){
			return Carbon::parse($loan->maturity_date)->format('jS F, Y');
		})
		->editColumn("loan_amount", function($loan){
			return number_format($loan->loan_amount,2);
		})
		// ->addColumn("action", function($data){
		// 	return '<a href="" class="btn btn-success update_load_btn" data-id="'.$data->id.'">Update</a>';
		// })
		->make(true);
	}

	public function loanRepaymentDatatable($loan_id)
	{
		$loans =  $this->investiment_type->loanRepaymentForDatatable($loan_id);
		return DataTables::of($loans)
		->editColumn("received_repayment_date", function($loan){
			return Carbon::parse($loan->received_repayment_date)->format('jS F, Y');
		})
		->editColumn("interest_received_date", function($loan){
			return Carbon::parse($loan->interest_received_date)->format('jS F, Y');
		})
		->editColumn("received_repayment", function($loan){
			return number_format($loan->received_repayment,2);
		})
		->editColumn("interest_received", function($loan){
			return number_format($loan->interest_received,2);
		})->make(true);
	}



	

	public function loanShow($loan_id)
	{
		$loan = InvestmentLoan::find($loan_id);
		return view('backend.investment.investiment_types.loans.show')->with('loan',$loan);
	}


	public function loanUpdateForm($loan_id)
	{
		return view('backend.investment.investiment_types.loans.index');

	}

	public function saveLoanRepayment(Request $request)
	{
		$rules = [
			'received_repayment' => 'required',
			'interest_received' => 'required',
			'received_repayment_date' => 'required|date', 
			'interest_received_date' => 'required|date',
			'amount_disbursed' => 'required',
			'principal_redeemed' => 'required',
		];

		$message = [
			'received_repayment_date.*' => 'kindly enter received repayment date', 
			'interest_received_date.*' => 'kindly enter interest received date', 
			'received_repayment.required' => 'kindly enter received repayment',
			'interest_received.required' => 'kindly enter interest received',
			'amount_disbursed.required' => 'kindly enter amount disbursed',
			'principal_redeemed.required' =>  'kindly enter principal redeemed',
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
		// $exist = DB::table('main.investment_loan_repayments')->where('investment_loan_id',$request->investment_loan_id)->where('received_repayment_date')
			DB::table('main.investment_loan_repayments')->updateOrInsert(
				[
					"received_repayment" => number_format((float)$request->received_repayment, 2, '.', ''),
					"received_repayment_date" => !empty($request->received_repayment_date) ? Carbon::parse($request->received_repayment_date)->format('Y-m-d') : null,
				],
				[
					"interest_received" => number_format((float)$request->interest_received, 2, '.', ''),
					"interest_received_date" => !empty($request->interest_received_date) ? Carbon::parse($request->interest_received_date)->format('Y-m-d') : null,
					"amount_disbursed" => number_format((float)$request->amount_disbursed, 2, '.', ''),
					"principal_redeemed" => number_format((float)$request->principal_redeemed, 2, '.', ''),
					"investment_loan_id" => $request->investment_loan_id
				]);

			return response()->json(array("success"=>true));

		}
	}

	public function getEmployers(){
		return $this->investiment_type->getEmployers();

	}
	public function callShow()
	{
		// $calls = InvestmentCallAccount::find($id);
		return view('backend.investment.investiment_types.call_account.index')->with('calls','id',$calls);
	}
	public function callCreate()
	{
		return view('backend.investment.investiment_types.call_account.create')->with('calls',$calls);

	}
	public function callSave(Request $request)
	{
// dd($request->all());

		$rules = [
			// 'fin_year_id' => 'required', 
			'balance_amount' => 'required', 
			'balance_date' => 'required|date',
			'interest_rate' => 'required',
			'tax_rate' => 'required',
			'received_interest' => 'required', 
			'interest_income' => 'sometimes|required', 
			'interest_receivable' => 'sometimes|required', 
			
			
		];

		$message = [
			// 'fin_year_id.required' => 'kindly select financial year', 
			'balance_amount' => 'kindly enter balance amount', 
			'balance_date' => 'kindly enter maturity date',
			'interest_rate' => 'kindly enter auction date',
			'tax_rate' => 'kindly enter tax rate', 
			'received_interest' => 'kindly enter received interest', 
			'interest_income' => 'kindly enter interest income', 
			'interest_receivable' => 'kindly enter interest receivable',
		]; 

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			return $this->investiment_type->callCreate($request);
		}
	}
	public function callAccountDatatable()
	{

		$calls = InvestmentCallAccount::select('*')->get();
       // dd($calls);

      // return Datatables::of($calls)->make(true);
		return DataTables::of($calls)
		->editColumn("balance_amount", function($data){
			return number_format($data->balance_amount,2);
		})
		->editColumn("received_interest", function($data){
			return number_format($data->received_interest,2);
		})
		->editColumn("interest_income", function($data){
			return number_format($data->interest_income,2);
		})
		->editColumn("interest_receivable", function($data){
			return number_format($data->interest_receivable,2);
		})->make(true);

	}

}
