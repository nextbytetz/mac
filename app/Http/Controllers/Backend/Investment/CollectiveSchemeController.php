<?php

namespace App\Http\Controllers\Backend\Investment;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Khill\Lavacharts\Lavacharts;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use Log;
use Lava;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;
use App\Repositories\Backend\Investiment\CollectiveSchemeRepository;
use App\Models\Investment\CollectiveScheme\InvestmentCollectiveUpdate;


class CollectiveSchemeController extends Controller
{

	public function __construct()
	{
		$this->code_values = new CodeValueRepository();
		$this->code_reference = 'INTCIS';
		$this->investiment_type = new InvestimentTypeRepository();
		$this->investment_scheme = new CollectiveSchemeRepository();
	}

	public function index($budget_allocation_id=null)
	{
		$summary = $this->investment_scheme->getSummary();
		return view('backend.investment.investiment_types.collective_scheme.index')->with('summary',$summary);
		// ->with('budget_allocation_id',$budget_allocation_id);
	}

	public function addScheme(Request $request)
	{

		$rules = [
			'manager'=>'required',
			'scheme_name' => 'required', 
			'scheme_reference' => 'required',
		];

		$message = [
			'manager.*' => 'Kindly select scheme manager', 
			'scheme_name.*' => 'kindly enter scheme name', 
			'scheme_reference.*' => 'kindly enter scheme name', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$manager = DB::table('main.employers')->find($request->manager);
			if (!count($manager)) { 
				return response()->json(array('errors' => ['manager' => ['Manager not existing.. Kindly try again']]));
			}
			$scheme_exist = $this->investment_scheme->findSchemeByNameOrReference($request->manager,$request->scheme_name);
			if (count($scheme_exist)) {
				return response()->json(array('errors' => ['scheme_name' => ['Scheme already exist']]));
			} 
			$return = $this->investment_scheme->saveScheme($request->all());
			return response()->json(array('success' => $return));
		}
	}

	public function datatable($budget_allocation_id = null)
	{
		$schemes = $this->investment_scheme->getForDatatable();
		return DataTables::of($schemes)
		->editColumn("manager", function($scheme){
			return $scheme->manager;
		})->addColumn("total_units", function($scheme){
			return number_format($scheme->total_units,0);
		})->addColumn("units_value", function($scheme){
			return number_format($scheme->units_value,2);
		})->make(true);
	}


	public function show($scheme_id)
	{
		$scheme = $this->investment_scheme->findOrThrowException($scheme_id);
		return view('backend.investment.investiment_types.collective_scheme.show')->with('scheme',$scheme);
	}

	public function updatesDatatable($scheme_id)
	{
		$scheme_updates = $this->investment_scheme->getUpdatesForDatatable($scheme_id);
		return DataTables::of($scheme_updates)
		->editColumn("unit_value", function($update){
			return number_format($update->unit_value,4);
		})
		->editColumn("value_date", function($update){
			return Carbon::parse($update->value_date)->format('jS F, Y');
		})
		->editColumn("number_of_unit", function($update){
			return number_format($update->number_of_unit,0);
		})
		->editColumn("total_unit_amount", function($update){
			return number_format($update->total_unit_amount,2);
		})
		->make(true);

	}

	public function updateValue(Request $request)
	{
		
		$rules = [
			'value_date' => 'required|date', 
			'unit_value' => 'required|numeric',
		];

		$message = [
			'unit_value.*' => 'Unit value is required and should be a number', 
			'value_date.*' => 'kindly enter value date', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			// $update_exist = InvestmentCollectiveUpdate::where('scheme_id',$request->scheme_id)->where('value_date',Carbon::parse($request->value_date))->first();
			// if (count($update_exist)) {
			// 	return response()->json(array('errors' => ['value' => ['Unit value for this date has already been updated']]));
			// } else {
			$return = $this->investment_scheme->updateLotPrice($request->all());
			return response()->json(array('success' => $return));
			// }

		}
	}


	public function lotsDatatable($scheme_id)
	{
		$scheme_lots = $this->investment_scheme->getLotsForDatatable($scheme_id);
		return DataTables::of($scheme_lots)
		->editColumn("purchase_nav", function($lot){
			return number_format($lot->purchase_nav,4);
		})
		->editColumn("purchase_date", function($lot){
			return Carbon::parse($lot->purchase_date)->format('jS F, Y');
		})
		->editColumn("purchased_unit", function($lot){
			return number_format($lot->purchased_unit,0);
		})
		->editColumn("remaining_unit", function($lot){
			return number_format($lot->remaining_unit,0);
		})
		->addColumn("sold_units", function($lot){
			return number_format(($lot->purchased_unit - $lot->remaining_unit),0);
		})
		->editColumn("purchase_amount", function($lot){
			return number_format($lot->purchase_amount,2);
		})
		->make(true);

	}

	public function create($investment_allocation_id=null)
	{
		$managers = $this->investment_scheme->getManagers();
		return view('backend.investment.investiment_types.collective_scheme.create')
		->with('managers',$managers);	
	}


	public function save(Request $request)
	{

		$rules = [
			'manager' => 'required',
			'scheme' => 'required', 
			'category' => 'required', 
			'unit_value' => 'required|numeric', 
			'total_units' => 'required|numeric', 
			'purchase_date' => 'required|date',
			'disbursment_date' => 'required|date',
		];

		$message = [
			'manager.required' => 'kindly select manager', 
			'scheme.required' => 'kindly select applicable scheme', 
			'category.required' => 'kindly select appliacable category', 
			'unit_value.required' => 'kindly enter unit value', 
			'total_units.required' => 'kindly enter total units', 
			'purchase_date.required' => 'kindly enter purchase date', 
			'disbursment_date.required' => 'kindly enter disbursement date', 
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$allocation = $this->investment_scheme->returnBudgetAllocationDetails($request->purchase_date);
			if (!is_null($allocation)) {
				$return = $this->investment_scheme->saveSchemeUnits($request->all(), $allocation);
				return response()->json(array('success' => $return));
			}else{
				return response()->json(array('errors' => ['purchase_date' => ['Investment budget is not set for this date']]));
			}
		}
	}

	


	public function salesDatatable($equity_id)
	{
		$sales = $this->investment_scheme->getSalesForDatatable($equity_id);
		return DataTables::of($sales)
		->editColumn("selling_price", function($sale){
			return number_format($sale->selling_price,4);
		})
		->editColumn("selling_date", function($sale){
			return Carbon::parse($sale->selling_date)->format('jS F, Y');
		})
		->editColumn("number_of_unit", function($sale){
			return number_format($sale->number_of_unit,0);
		})
		->editColumn("total_unit_amount", function($sale){
			return number_format($sale->total_unit_amount,2);
		})
		->make(true);
	}


	public function sell(Request $request)
	{
		$rules = [
			'sell_lot' => 'required|numeric',
			'sell_units' => 'required|numeric',
			'selling_date' => 'required|date', 
			'sell_price' => 'required|numeric'
		];

		$message = [
			'sell_lot.*' => 'Kindly select Lot', 
			'sell_price.*' => 'Price per unit is required', 
			'selling_date.*' => 'kindly enter selling date',
			'sell_units.*' => 'kindly enter number of units',
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$return = $this->investment_scheme->sellShares($request->all());
			return response()->json(array('success' => $return));
		}
	}


	public function dividendDatatable($equity_id)
	{
		$dividends = $this->investment_scheme->getDividendsForDatatable($equity_id);
		return DataTables::of($dividends)
		->editColumn("dividend_declaration_date", function($dividend){
			return Carbon::parse($dividend->dividend_declaration_date)->format('jS F, Y');
		})
		->editColumn("dividend_payment_date", function($dividend){
			return Carbon::parse($dividend->dividend_payment_date)->format('jS F, Y');
		})
		->editColumn("total_unit", function($dividend){
			return number_format($dividend->total_unit,0);
		})
		->editColumn("dividend_nav", function($dividend){
			return number_format($dividend->dividend_nav,4);
		})
		->editColumn("dividend_income", function($dividend){
			return number_format($dividend->dividend_income,2);
		})
		->make(true);
	}




	public function addDividend(Request $request)
	{
		$rules = [
			'dividend_per_unit' => 'required|numeric',
			'declaration_date' => 'required|date', 
			'payment_date' => 'required|date', 
			'scheme_id' => 'required', 
		];

		$message = [
			'dividend_per_unit.*' => 'Dividend Per Unit is required', 
			'declaration_date.*' => 'kindly enter dividend declaration date',
			'payment_date.*' => 'kindly enter payment date',
		];

		$validator = Validator::make(Input::all(),$rules, $message);
		if ($validator->fails()) {
			return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$return = $this->investment_scheme->saveDividend($request->all());
			return response()->json(array('success' => $return));
		}
	}

	public function returnSchemes($manager_id)
	{
		$schemes = $this->investment_scheme->query()->where('manager_id', $manager_id)->get();
		if (!empty($schemes)) {
			return response()->json(array("success"=>true, "schemes"=>$schemes));
		} else {
			return response()->json(array("success"=>false));
		}
	}	

	public function returnLotNumber($scheme_id)
	{
		$lot_number = $this->investment_scheme->returnLotNumberForSave($scheme_id);
		if (!empty($lot_number)) {
			return response()->json(array("success"=>true, "lot"=>$lot_number));
		} else {
			return response()->json(array("success"=>false));
		}
	}	


}
