<?php

namespace App\Http\Controllers\Backend\Operation\Payroll\Traits;

/*Description: All methods for datatable on Payrolcontroller*/

use App\Http\Requests\Backend\Operation\Payroll\PayrollChildDisabilityAssesRequest;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Repositories\Backend\Operation\Payroll\PayrollChildExtensionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

trait PayrollChildExtensionTrait
{



    /*Fill child disability assessment*/
    public function fillChildDisabilityAssessment(PayrollStatusChange $statusChange)
    {
        $member_type_id = $statusChange->member_type_id;
        $resource_id = $statusChange->resource_id;
        $employee_id = $statusChange->employee_id;

        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        return view('backend/operation/payroll/pension_administration/status_changes/child_extension/fill_disabilty_assess')
            ->with('status_change', $statusChange)
            ->with('payroll_child_extension', $statusChange->payrollChildExtension)
            ->with([ 'resource' => $resource, 'member_type_id' => $member_type_id, 'employee_id' => $employee_id ]);
    }


    /*Store child disability assessment*/
    public function storeChildDisabilityAssessment(PayrollStatusChange  $statusChange,PayrollChildDisabilityAssesRequest $request)
    {
        $input = $request->all();
        (new PayrollChildExtensionRepository())->storeDisabilityAssessment($statusChange, $input);
        return redirect('payroll/status_change/profile/' . $statusChange->id)->withFlashSuccess('Success, Child Disability information saved');
    }


    public function updateLetterPreparedStatus(PayrollStatusChange $statusChange)
    {
        (new PayrollChildExtensionRepository())->updateLetterPreparedStatus($statusChange);
        return redirect('payroll/status_change/profile/' . $statusChange->id)->withFlashSuccess('Success, Letter is prepared');
    }

}