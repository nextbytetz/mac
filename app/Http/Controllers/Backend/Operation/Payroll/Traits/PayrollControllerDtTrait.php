<?php

namespace App\Http\Controllers\Backend\Operation\Payroll\Traits;

/*Description: All methods for datatable on Payrolcontroller*/

use Carbon\Carbon;
use Yajra\Datatables\Datatables;

trait PayrollControllerDtTrait
{


    /**
     * Get Payroll run approvals for datatatable
     */
    public function getPayrollRunApprovalForDataTable()
    {
        return Datatables::of($this->payroll_run_approvals->getPayrollRunApprovalsForDataTable()->orderBy('id', 'desc'))
            ->editColumn('total_net_amount', function ($run_approval) {
                return number_2_format( $run_approval->total_net_amount);
            })
            ->addColumn('run_date_formatted', function ($run_approval) {
                return Carbon::parse($run_approval->payrollProc->run_date)->format('M y');
            })
            ->editColumn('no_of_pensioners', function ($run_approval) {
                return   number_0_format($run_approval->no_of_pensioners);
            })
            ->editColumn('no_of_dependents', function ($run_approval) {
                return   number_0_format($run_approval->no_of_dependents);
            })
            ->addColumn('status', function ($run_approval) {
                return $run_approval->status_label;
            })
            ->rawColumns(['status'])
            ->make(true);

    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get monthly pension payroll by beneficiary for datataable per notification report
     */
    public function getMpByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)
    {
        $monthly_pensions = $this->payroll_runs->getMpForDataTable()->where('payroll_runs.employee_id',$employee_id)->where('payroll_runs.member_type_id', $member_type_id)->where('resource_id', $resource_id)->orderBy('run_date', 'desc');
        return Datatables::of($monthly_pensions)
            ->editColumn('amount', function ($payroll_run) {
                return number_2_format( $payroll_run->amount);
            })
            ->addColumn('run_date_formatted', function ($payroll_run) {
                return Carbon::parse($payroll_run->run_date)->format('M y');
            })
            ->editColumn('monthly_pension', function ($payroll_run) {
                return   number_2_format($payroll_run->monthly_pension);
            })
            ->editColumn('arrears_amount', function ($payroll_run) {
                return   number_2_format($payroll_run->arrears_amount);
            })
            ->editColumn('deductions_amount', function ($payroll_run) {
                return   number_2_format($payroll_run->deductions_amount);
            })
            ->addColumn('bank_details', function ($payroll_run) {
                return $payroll_run->bank_name . ' - ' . (isset($payroll_run->bank_branch_name) ? $payroll_run->bank_branch_name . ' - ' : '') . $payroll_run->run_accountno;
            })
            ->addColumn('status', function ($payroll_run) {
                return $payroll_run->wf_done == 1 ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get suspended monthly pension payroll by beneficiary for datatable per notification report
     */
    public function getSuspendedMpByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)
    {
        $monthly_pensions = $this->payroll_suspended_runs->getSuspendedMpForDataTable()->where('payroll_suspended_runs.employee_id',$employee_id)->where('member_type_id', $member_type_id)->where('resource_id', $resource_id)->orderBy('run_date', 'desc');
        return Datatables::of($monthly_pensions)
            ->editColumn('amount', function ($payroll_suspended_run) {
                return number_2_format( $payroll_suspended_run->amount);
            })
            ->addColumn('run_date_formatted', function ($payroll_suspended_run) {
                return Carbon::parse($payroll_suspended_run->run_date)->format('M y');
            })
            ->editColumn('monthly_pension', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->monthly_pension);
            })
            ->editColumn('arrears_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->arrears_amount);
            })
            ->editColumn('unclaimed_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->unclaimed_amount);
            })
            ->editColumn('deductions_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->deductions_amount);
            })

            ->addColumn('status', function ($payroll_suspended_run) {
                return $payroll_suspended_run->ispaid == 1 ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid' . "'>" . 'Paid' . "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
            })
            ->rawColumns(['status'])
            ->make(true);

    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get monthly pension payroll by payroll run approval for datataable per member type
     */
    public function getMpByRunApprovalForDataTable($member_type_id, $payroll_run_approval_id, $constant_care = 0)
    {
        $monthly_pensions = $this->payroll_runs->getMpForDataTableWithFilename()->where('member_type_id',$member_type_id)->where('payroll_run_approval_id',$payroll_run_approval_id )
            ->where('isconstantcare', $constant_care);
        return Datatables::of($monthly_pensions)
            ->editColumn('amount', function ($payroll_run) {
                return number_2_format( $payroll_run->amount);
            })
            ->addColumn('run_date_formatted', function ($payroll_run) {
                return Carbon::parse($payroll_run->run_date)->format('M y');
            })
            ->editColumn('monthly_pension', function ($payroll_run) {
                return   number_2_format($payroll_run->monthly_pension);
            })
            ->editColumn('arrears_amount', function ($payroll_run) {
                return   number_2_format($payroll_run->arrears_amount);
            })
            ->editColumn('deductions_amount', function ($payroll_run) {
                return   number_2_format($payroll_run->deductions_amount);
            })
            ->editColumn('unclaimed_amount', function ($payroll_run) {
                return   number_2_format($payroll_run->unclaimed_amount);
            })
            ->addColumn('bank_details', function ($payroll_run) {
                return $payroll_run->bank_name . ' - ' . (isset($payroll_run->bank_branch_name) ? $payroll_run->bank_branch_name . ' - ' : '') . $payroll_run->run_accountno;
            })
            ->editColumn('amount', function ($payroll_run) {
                return number_2_format($payroll_run->amount);
            })

            ->addColumn('status', function ($payroll_run) {
                return $payroll_run->wf_done == 1 ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
            })
            ->rawColumns(['status'])
            ->make(true);
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get suspended monthly pension payroll by payroll run approval for datataable per member type
     */
    public function getSuspendedMpByRunApprovalForDataTable($member_type_id, $payroll_run_approval_id)
    {
        $monthly_pensions = $this->payroll_suspended_runs->getSuspendedMpForDataTable()->where('payroll_runs.member_type_id',$member_type_id)->where('payroll_runs.payroll_run_approval_id',$payroll_run_approval_id );
        return Datatables::of($monthly_pensions)
            ->editColumn('amount', function ($payroll_suspended_run) {
                return number_2_format( $payroll_suspended_run->amount);
            })
            ->addColumn('run_date_formatted', function ($payroll_suspended_run) {
                return Carbon::parse($payroll_suspended_run->run_date)->format('M y');
            })
            ->editColumn('monthly_pension', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->monthly_pension);
            })
            ->editColumn('arrears_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->arrears_amount);
            })
            ->editColumn('deductions_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->deductions_amount);
            })
            ->editColumn('unclaimed_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->unclaimed_amount);
            })
            ->editColumn('amount', function ($payroll_suspended_run) {
                return number_2_format($payroll_suspended_run->amount);
            })

            ->addColumn('status', function ($payroll_suspended_run) {
                return $payroll_suspended_run->wf_done == 1 ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Approved' . "'>" . 'Approved' . "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
            })
            ->rawColumns(['status'])
            ->make(true);
    }


    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * @return mixed
     * @throws \Exception
     * Get new payees /beneficiary in this payroll
     */
    public function getNewPayeesInPayrollForDataTable($member_type_id, $payroll_run_approval_id, $constant_care_flag)
    {

        return Datatables::of($this->payroll_runs->getNewPayeesInPayrollForDataTable($member_type_id, $payroll_run_approval_id, $constant_care_flag))
            ->editColumn('amount', function ($payroll_run) {
                return number_2_format( $payroll_run->amount);
            })
            ->editColumn('monthly_pension', function ($payroll_run) {
                return   number_2_format($payroll_run->monthly_pension);
            })
            ->editColumn('arrears_amount', function ($payroll_run) {
                return   number_2_format($payroll_run->arrears_amount);
            })
            ->editColumn('deductions_amount', function ($payroll_run) {
                return   number_2_format($payroll_run->deductions_amount);
            })

            ->make(true);
    }

    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * @param $constant_care_flag
     * @return mixed
     * @throws \Exception
     * Get Payees with arrears by payroll approval
     */
    public function getPayeesWithArrearsForDataTable($payroll_run_approval_id)
    {

        $result_list = ($this->payroll_runs->getPayeeWithArrearsForDt($payroll_run_approval_id));
        return $this->generalRunReturnDtForModalSummary($result_list);
    }

    /**
     * @param $payroll_run_approval_id
     * @return mixed
     * @throws \Exception
     * Get children overage with no extension approval
     */
    public function getChildrenWithNoExtensionApprovalDt($payroll_run_approval_id)
    {
        $result_list = ($this->payroll_runs->getChildOverageWithNoExtensionForDt($payroll_run_approval_id));
        return $this->generalRunReturnDtForModalSummary($result_list);
    }

    /**
     * @param $payroll_run_approval_id
     * @return mixed
     * @throws \Exception
     * Other dependents completed cycles but still on payroll wrongly
     */
    public function getOtherDependentsFinishedTheirCyclesStillOnPayrollDt($payroll_run_approval_id)
    {
        $result_list = ($this->payroll_runs->getOtherDependentsFinishedTheirCyclesStillOnPayrollDt($payroll_run_approval_id));
        return $this->generalRunReturnDtForModalSummary($result_list);
    }


    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * @return mixed
     * @throws \Exception
     * Get deactivated pensioners just before this payroll (Were available on previous payroll)
     */
    public function getDeactivatedPensionersBeforeThisPayrollForDataTable($member_type_id, $payroll_run_approval_id)
    {

        return Datatables::of($this->payroll_runs->getBeneficiaryDeactivatedBeforeThisPayroll($member_type_id, $payroll_run_approval_id))
            ->addColumn('mp_formatted', function ($pensioner) {
                return   number_2_format($pensioner->mp);
            })
            ->addColumn('member_type_id', function ($pensioner) use($member_type_id) {
                return   $member_type_id;
            })
            ->addColumn('resource_id', function ($pensioner) {
                return   $pensioner->id;
            })

            ->make(true);
    }


    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * @return mixed
     * @throws \Exception
     * Get deactivated dependents just before this payroll (Were available on previous payroll)
     */
    public function getDeactivatedDependentsBeforeThisPayrollForDataTable($member_type_id, $payroll_run_approval_id, $constant_care_flag)
    {

        return Datatables::of($this->payroll_runs->getBeneficiaryDeactivatedBeforeThisPayroll($member_type_id, $payroll_run_approval_id, $constant_care_flag))
            ->addColumn('mp_formatted', function ($dependent) {
                return   number_2_format($dependent->survivor_pension_amount);
            })
            ->addColumn('member_type_id', function ($dependent) use($member_type_id) {
                return   $member_type_id;
            })
            ->addColumn('resource_id', function ($dependent) {
                return   $dependent->dependent_id;
            })

            ->make(true);
    }



    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * @return mixed
     * @throws \Exception
     * Get Suspended pensioners just before this payroll (Were available on previous payroll)
     */
    public function getSuspendedPensionersBeforeThisPayrollForDataTable($member_type_id, $payroll_run_approval_id)
    {
        return Datatables::of($this->payroll_runs->getBeneficiarySuspendedBeforeThisPayroll($member_type_id, $payroll_run_approval_id))
            ->addColumn('mp_formatted', function ($pensioner) {
                return   number_2_format($pensioner->mp);
            })
            ->addColumn('member_type_id', function ($pensioner) use($member_type_id) {
                return   $member_type_id;
            })
            ->addColumn('resource_id', function ($pensioner) {
                return   $pensioner->id;
            })

            ->make(true);
    }


    /**
     * @param $member_type_id
     * @param $payroll_run_approval_id
     * @return mixed
     * @throws \Exception
     * Get Suspended dependents just before this payroll (Were available on previous payroll)
     */
    public function getSuspendedDependentsBeforeThisPayrollForDataTable($member_type_id, $payroll_run_approval_id, $constant_care_flag)
    {

        return Datatables::of($this->payroll_runs->getBeneficiarySuspendedBeforeThisPayroll($member_type_id, $payroll_run_approval_id,$constant_care_flag))
            ->addColumn('mp_formatted', function ($dependent) {
                return   number_2_format($dependent->survivor_pension_amount);
            })
            ->addColumn('member_type_id', function ($dependent) use($member_type_id) {
                return   $member_type_id;
            })
            ->addColumn('resource_id', function ($dependent) {
                return   $dependent->dependent_id;
            })

            ->make(true);
    }



    /**
     * @param $payroll_run_approval_id
     * Get beneficiaries with no erp supplier id for those with no nmb and crdb bank details
     */
    public function getBeneficiariesWithNoErpSupplierIdForDataTable($payroll_run_approval_id)
    {
        return Datatables::of($this->payroll_runs->getBeneficiariesWithNoErpSupplierIdForDataTable( $payroll_run_approval_id))
            ->make(true);
    }


    /**
     * @param $document_type
     * Get pending documents from e-office which trigger beneficiary information updates
     */
    public function getPendingDocsForBeneficiaryUpdateDataTable($document_type)
    {

        return Datatables::of($this->payroll_repo->getPendingDocsForBeneficiaryUpdateDataTable($document_type)->orderBy('doc_receive_date', 'asc'))
//            ->addColumn('payee_name', function ($document) {
//                return   ($document->member_type_id == 5) ? $document->pensioner_name : $document->dependent_name;
//            })
            ->addColumn('received_date', function ($document) {
                return   isset($document->doc_receive_date) ? short_date_format($document->doc_receive_date) : ' ';
            })

            ->make(true);

    }


    /**
     * Payroll pending workflows
     *
     */
    public function payrollPendingWorkflows()
    {
        return Datatables::of($this->payroll_repo->getPayrollPendingWorkflowsForDataTable()->orderBy('created_at', 'asc'))
            ->make(true);
    }



    /**
     *
     * Get beneficiaries on Hold
     */
    public function getHeldBeneficiariesForDt()
    {
        return Datatables::of($this->payroll_repo->getHeldBeneficiariesForDt()->orderBy('member_name', 'asc'))
            ->editColumn('hold_date', function ($result) {
                return   short_date_format($result->hold_date);
            })
            ->make(true);

    }



    public function getAllSuspendedBeneficiariesForDt()
    {
        return Datatables::of($this->payroll_repo->getAllSuspendedBeneficiariesForDt()->orderBy('b.member_type_id', 'asc')->orderBy('b.member_name', 'asc'))

            ->editColumn('lastresponse', function ($result){
                return   short_date_format($result->lastresponse);
            })
            ->editColumn('suspended_date', function ($result) {
                return   short_date_format($result->suspended_date);
            })

            ->make(true);
    }

    /*Get beneficiaries with changed bank details for dt*/
    public function getMembersWithChangedBankForDt($payroll_run_approval_id)
    {
        return Datatables::of($this->payroll_runs->getBeneficiariesWithChangedBankDetailsThisPayroll($payroll_run_approval_id))
            ->make(true);
    }


    /**
     * @param $result_list
     * @return mixed
     * @throws \Exception
     * General Run return dt for modal
     */
    public function generalRunReturnDtForModalSummary($result_list)
    {
        return Datatables::of($result_list)
            ->editColumn('amount', function ($result_list) {
                return number_2_format( $result_list->amount);
            })->editColumn('monthly_pension', function ($result_list) {
                return   number_2_format($result_list->monthly_pension);
            })
            ->editColumn('arrears_amount', function ($result_list) {
                return   number_2_format($result_list->arrears_amount);
            })
            ->editColumn('deductions_amount', function ($result_list) {
                return   number_2_format($result_list->deductions_amount);
            })
            ->editColumn('unclaimed_amount', function ($result_list) {
                return   number_2_format($result_list->unclaimed_amount);
            })
            ->editColumn('amount', function ($result_list) {
                return number_2_format($result_list->amount);
            })

            ->rawColumns([])
            ->make(true);
    }


}