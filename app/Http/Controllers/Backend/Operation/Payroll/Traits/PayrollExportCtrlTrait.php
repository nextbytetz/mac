<?php

namespace App\Http\Controllers\Backend\Operation\Payroll\Traits;

/*Description: All methods for datatable on Payrolcontroller*/

use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

trait PayrollExportCtrlTrait
{


    /**
     * @param $payroll_run_approval_id
     * @param $bank_id
     * Export payees by bank payroll run approval
     */
    public function exportPayeesByBankPayrollRunApproval($payroll_run_approval_id, $bank_id)
    {
        $payees = (new PayrollRunRepository())->getPayeesByBankInPayrollRunForDt($payroll_run_approval_id, $bank_id)->get()->toArray();
        return $this->exportExcelGeneral($payees, 'payrollPayeesByBank'. time(), 'csv');
    }


}