<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\VerificationRequest;
use App\Models\Legal\District;
use App\Models\Location\Region;
use App\Models\Operation\Payroll\PayrollVerification;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollVerificationRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\IdentityRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Facades\Datatables;

class PayrollVerificationController extends Controller
{




    protected $code_values;
    protected $pensioners;
    protected $dependents;
    protected $notification_reports;
    protected $identities;
    protected $verifications;

    public function __construct()
    {
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->notification_reports = new NotificationReportRepository();
        $this->identities = new IdentityRepository();
        $this->verifications= new PayrollVerificationRepository();
        $this->code_values= new CodeValueRepository();
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($member_type_id)
    {
        //
        $title = ($member_type_id == 5) ? 'Verify Pensioners' : 'Verify Dependents';
        return view('backend/operation/payroll/pension_administration/verification/index/index')
            ->with('member_type_id', $member_type_id)
            ->with('title', $title);
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * Get resource based on the member type
     */
    public function getResource($member_type_id, $resource_id)
    {
        $payroll_repo = new PayrollRepository();
        $resource = $payroll_repo->getResource($member_type_id, $resource_id);
        return $resource;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @return mixed
     * Get pone of resource
     */
    public function getPhone($member_type_id, $resource_id)
    {
        $resource = $this->getResource($member_type_id, $resource_id);
        if($member_type_id == 5){
            /*Pensioner*/
            $phone = $resource->employee->phone;
        }elseif($member_type_id == 4){
            /*Dependents*/
            $phone = $resource->phone;
        }
        return $phone;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($member_type_id, $resource_id, $employee_id)
    {
        //
        $resource = $this->getResource($member_type_id, $resource_id);
        /*Check if can be verification can be initiated*/
        $this->verifications->checkIfBeneficiaryVerificationCanBeInitiated($member_type_id, $employee_id, $resource);
        /*For dependent: check if is child*/
        $dependent_type_id = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id)->dependent_type_id : null;
        $check_child = ($dependent_type_id == 3) ? 1 : 0;
        /*end check child*/
        $last_response = Carbon::parse($resource->lastresponse);
        $today = Carbon::now();
        $month_since_verification = isset($last_response) ? $last_response->diffInMonths($today) : ' ';
        $document_type = 63;//Payroll verification forms documents
        /*Location*/
        $regions = Region::query()->orderBy('name')->pluck('name', 'id');
        return view('backend/operation/payroll/pension_administration/verification/create/create')
            ->with(['resource' => $resource, 'member_type_id' => $member_type_id,  'month_since_verification' => $month_since_verification, 'document_type' => $document_type, 'check_child' => $check_child, 'employee_id' => $employee_id])
            ->with(['regions' => $regions]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VerificationRequest $request)
    {
        //
        $input = $request->all();
        $input['status_change_type_id'] = $this->code_values->findIdByReference('PSCVERIF');
        $input['remark'] = isset( $input['remark']) ?  $input['remark'] : 'Verification';
        $member_type_id = $input['member_type_id'];
        $resource_id = $input['resource_id'];
        $resource = $this->getResource($member_type_id, $resource_id);
        $document_type = 63;

        DB::transaction(function () use ($input, $resource,$member_type_id, $document_type) {
            $status_change = (new PayrollStatusChangeRepository())->initiateWfOnCreate($input);
            $input['payroll_status_change_id'] = $status_change->id;
            $verification = $this->verifications->create($input, $resource);
            (new PayrollRepository())->updateBeneficiaryDocumentPendingStatusPerType($member_type_id, $resource->id, $document_type);
        });
        return redirect('payroll/alert_monitor/index/7')->withFlashSuccess('Success, Verification has been done');

    }


    /*Edit verification*/
    public function edit(PayrollVerification $payrollVerification)
    {
        $member_type_id = $payrollVerification->member_type_id;
        $resource_id = $payrollVerification->resource_id;
        $resource = $this->getResource($member_type_id, $resource_id);
        $document_type = 63;
        $status_change = $payrollVerification->payrollStatusChange;
        $employee_id = $status_change->employee_id;
        $dependent_type_id = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id)->dependent_type_id : null;
        $check_child = ($dependent_type_id == 3) ? 1 : 0;
        /*end check child*/
        $last_response = Carbon::parse($resource->lastresponse);
        $today = Carbon::now();
        $month_since_verification = isset($last_response) ? $last_response->diffInMonths($today) : ' ';
        /*Location*/
        $regions = Region::query()->orderBy('name')->pluck('name', 'id');
        return view('backend/operation/payroll/pension_administration/verification/edit/edit')
            ->with('payroll_verification', $payrollVerification)
            ->with(['resource' => $resource, 'member_type_id' => $member_type_id,  'month_since_verification' => $month_since_verification, 'document_type' => $document_type, 'check_child' => $check_child, 'employee_id' => $employee_id])
            ->with(['regions' => $regions]);
    }



    public function update(PayrollVerification $payrollVerification, VerificationRequest $request )
    {
        $input = $request->all();
        $input['remark'] = isset( $input['remark']) ?  $input['remark'] : 'Verification';
        $this->verifications->update($payrollVerification, $input);
        return redirect('payroll/status_change/profile/'.$payrollVerification->payroll_status_change_id)->withFlashSuccess('Success, Verification has been updated');
    }
    /**
     * @param $member_type_id
     * @param $resource_id
     * @return mixed
     * Get verifications history of beneficiary for datatable
     */
    public function getVerificationsByBeneficiaryForDataTable($member_type_id, $resource_id)
    {
        return Datatables::of($this->verifications->getVerificationsByBeneficiaryForDataTable($member_type_id, $resource_id)->orderBy('id', 'desc'))

            ->addColumn('created_at_formatted', function ($verification) {
                return short_date_format($verification->created_at);
            })
            ->addColumn('verification_date_formatted', function ($verification) {
                return short_date_format($verification->verification_date);
            })

            ->addColumn('user', function ($verification) {
                return $verification->user->name;
            })
            ->addColumn('verification_form', function ($verification) {
                return  'Verification form - Folio No. ' .  $verification->folionumber;
            })
            ->addColumn('education', function ($verification) {
                return  ($verification->iseducation == 1) ? 'Still attend school' : ' ';
            })
            ->make(true);

    }




    /*Get pensioners closing for verification alerts - Check time if is less than 2 months*/
    public function getPensionersVerificationAlertsForDt()
    {
        return Datatables::of($this->pensioners->getVerificationAlertsForDt()->orderBy('lastresponse'))
//            ->addColumn('days_left', function ($pensioner) {
//                $last_response =  isset($pensioner->lastresponse) ? Carbon::parse($pensioner->lastresponse) : null;
//                $cut_off_date = isset($last_response) ? $last_response->addMonthNoOverflow(payroll_verification_limit_months()) : Carbon::now();
//                return isset($pensioner->lastresponse) ? ((comparable_date_format($cut_off_date) > comparable_date_format(Carbon::now())) ? Carbon::now()->diffInDays($cut_off_date) : 'Passed Deadline') : 'Passed Deadline';
//            })
            ->editColumn('days_left', function ($pensioner) {
                return ($pensioner->days_left < 0) ? ($pensioner->days_left . ' - Passed Deadline' ) : $pensioner->days_left ;
            })
            ->editColumn('lastresponse', function ($pensioner) {
                return  isset($pensioner->lastresponse) ? short_date_format($pensioner->lastresponse) : '';
            })
            ->editColumn('dob', function ($pensioner) {
                return  isset($pensioner->dob) ? short_date_format($pensioner->dob) : '';
            })
            ->editColumn('filename', function ($pensioner) {
                return  isset($pensioner->filename) ? $pensioner->filename :  'Manual case no - ' . $pensioner->case_no;
            })
            ->rawColumns([''])
            ->make(true);

    }



    /*Get dependents closing for verification alerts - Check time if is less than 2 months*/
    public function getDependentsVerificationAlertsForDt()
    {
        return Datatables::of($this->dependents->getVerificationAlertsForDt()->orderBy('lastresponse'))
            ->editColumn('days_left', function ($pensioner) {
                return ($pensioner->days_left < 0) ? ($pensioner->days_left . ' - Passed Deadline' ) : $pensioner->days_left ;
            })
            ->editColumn('lastresponse', function ($pensioner) {
                return  isset($pensioner->lastresponse) ? short_date_format($pensioner->lastresponse) : '';
            })
            ->editColumn('dob', function ($pensioner) {
                return  isset($pensioner->dob) ? short_date_format($pensioner->dob) : '';
            })
            ->editColumn('filename', function ($pensioner) {
                return  isset($pensioner->filename) ? $pensioner->filename :  'Manual case no - ' . $pensioner->case_no;
            })
            ->rawColumns([''])
            ->make(true);

    }

}
