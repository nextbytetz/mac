<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\ChooseRecoveryRequest;
use App\Http\Requests\Backend\Operation\Payroll\PayrollRecoveryCancelRequest;
use App\Http\Requests\Backend\Operation\Payroll\PayrollRecoveryRequest;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\ManualPayrollMemberRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollArrearRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRecoveryRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Facades\Datatables;

class PayrollRecoveryController extends Controller
{


    protected $code_values;
    protected $pensioners;
    protected $dependents;
    protected $payroll_recoveries;

    public function __construct()
    {
        $this->code_values = new CodeValueRepository();
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->payroll_recoveries = new PayrollRecoveryRepository();

        //cancel recovery
             $this->middleware('access.routeNeedsPermission:cancel_payroll_recovery', ['only' => ['openCancelPage', 'cancelApprovedRecovery']]);
    }

    /*Main wf module group id*/
    public function wfModuleGroupId()
    {
        return 13;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show Choose recovery type form
     */
    public function showChooseTypeForm($member_type_id, $resource_id, $employee_id)
    {
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $resource = $this-> getResource($member_type_id, $resource_id);
        $recovery_types = $this->code_values->query()->where('code_id', 22)->whereNotIn('reference', ['PRAROCH'])->pluck('name', 'id');
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
        return view('backend/operation/payroll/pension_administration/payroll_recovery/choose_type')
            ->with(['member_type_id' => $member_type_id, 'resource' => $resource , 'employee_id' => $employee_id, 'recovery_types' => $recovery_types, 'dependent_employee' => $dependent_employee]);
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * @return $this
     * Choose recovery type
     */
    public function chooseType(ChooseRecoveryRequest $request)
    {
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $input = $request->all();
        $member_type_id = $input['member_type_id'];
        $resource_id = $input['resource_id'];
        $employee_id = $input['employee_id'];
        $resource = $this->getResource($member_type_id,$resource_id );
        $recovery_type = $this->code_values->find($input['recovery_type']);
        $mp = $this->getMp($member_type_id, $resource_id, $employee_id);
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;

        return view('backend/operation/payroll/pension_administration/payroll_recovery/create/create')
            ->with(['member_type_id' => $member_type_id, 'resource' => $resource , 'employee_id' => $employee_id, 'recovery_type' => $recovery_type , 'mp' => $mp, 'dependent_employee' => $dependent_employee]);
    }

    /*Create Manual Arrears - for beneficiary from manual file (do not exist on system) - From manual payroll process*/
    public function createManualArrearsManualMembers($manual_member_id )
    {
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $manual_payroll_member = (new ManualPayrollMemberRepository())->find($manual_member_id);
        $manual_notification_report = $manual_payroll_member->manualNotificationReport($manual_payroll_member->incident_type_id)->first();
        $employee_id = $manual_notification_report->employee_id;
        $member_type_id = $manual_payroll_member->member_type_id;
        $resource_id = $manual_payroll_member->resource_id;
        $resource = $this->getResource($member_type_id,$resource_id );
        $recovery_type = $this->code_values->findByReference('PRTUNDP');
        $mp = $this->getMp($member_type_id, $resource_id, $employee_id);
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
        $pending_pay_months = $manual_payroll_member->pending_pay_months;
        $total_arrears = ($pending_pay_months) * $mp;
        /*check if can initiate new entry*/
        $this->payroll_recoveries->checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id, $recovery_type->id);
        return view('backend/operation/payroll/pension_administration/payroll_recovery/create/create_manual_arrears')
            ->with(['member_type_id' => $member_type_id, 'resource' => $resource , 'employee_id' => $employee_id, 'recovery_type' => $recovery_type , 'mp' => $mp, 'dependent_employee' => $dependent_employee, 'manual_payroll_member_id' => $manual_payroll_member->id, 'remark'=> $manual_payroll_member->remark, 'arrears_months' => $pending_pay_months,  'total_arrears' => $total_arrears]);
    }

    /*Create Manual Arrears - Beneficiary exist on the system - From manual payroll process*/
    public function createManualArrearsSystemMembers($member_type_id, $resource_id,$employee_id, $arrears_months )
    {
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $resource = $this->getResource($member_type_id,$resource_id );
        $recovery_type = $this->code_values->findByReference('PRTUNDP');
        $mp = $this->getMp($member_type_id, $resource_id, $employee_id);
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;

        $total_arrears = ($arrears_months) * $mp;
        /*check if can initiate new entry*/
        $this->payroll_recoveries->checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id, $recovery_type->id);
        return view('backend/operation/payroll/pension_administration/payroll_recovery/create/create_manual_arrears')
            ->with(['member_type_id' => $member_type_id, 'resource' => $resource , 'employee_id' => $employee_id, 'recovery_type' => $recovery_type , 'mp' => $mp, 'dependent_employee' => $dependent_employee, 'manual_payroll_member_id' => null, 'remark'=> '', 'arrears_months' => $arrears_months, 'total_arrears' => $total_arrears]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($member_type_id,  ChooseRecoveryRequest $request )
    {
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $input = $request->all();
//        $member_type_id = $input['member_type_id'];
        $resource_id = $input['resource_id'];
        $employee_id = $input['employee_id'];
        $resource = $this->getResource($member_type_id,$resource_id );
        $recovery_type = $this->code_values->find($input['recovery_type']);
        $mp = $this->getMp($member_type_id, $resource_id, $employee_id);
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
$arrears_pending = (new PayrollArrearRepository())->getNetPendingArrearsAmountForMember($member_type_id, $resource_id, $employee_id);
        /*check if can initiate new entry*/
        $this->payroll_recoveries->checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id, $recovery_type->id);
        return view('backend/operation/payroll/pension_administration/payroll_recovery/create/create')
            ->with(['member_type_id' => $member_type_id, 'resource' => $resource , 'employee_id' => $employee_id, 'recovery_type' => $recovery_type , 'mp' => $mp, 'dependent_employee' => $dependent_employee, 'arrears_pending' => $arrears_pending]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PayrollRecoveryRequest $request)
    {
        //
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $input = $request->all();
        $wf_module_group_id = $this->wfModuleGroupId();
        /*check if can initiate new entry*/
        $this->payroll_recoveries->checkIfCanInitiateNewEntry($input['member_type_id'], $input['resource_id'], $input['employee_id'], $input['recovery_type_id']);
        $payroll_recovery =  DB::transaction(function () use ($wf_module_group_id, $input) {
            /*create new recovery*/
            $payroll_recovery =  $this->payroll_recoveries->create($input);
            /*workflow start*/
            access()->hasWorkflowDefinition($wf_module_group_id, 1);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $payroll_recovery->id], [], ['comments' => $input['remark']]));
            /*end workflow*/
            return $payroll_recovery;
        });
        return redirect()->route('backend.payroll.recovery.profile',$payroll_recovery->id)->withFlashSuccess('Success, Payroll recovery has been created');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile(PayrollRecovery $payroll_recovery, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        //
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payroll_recovery->id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        /*end workflow*/
        $resource_id = $payroll_recovery->resource_id;
        $member_type_id = $payroll_recovery->member_type_id;
        $employee_id = $payroll_recovery->employee_id;
        $resource = $this->getResource($member_type_id, $resource_id);
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
        $recovery_type = $payroll_recovery->recoveryType;
        $wf_done = $payroll_recovery->wf_done;
        $mp = $this->getMp($member_type_id, $resource_id, $employee_id);
        /*For deduction*/
        $amount_per_cycle = ($recovery_type->reference == 'PRTOVEP') ? (($wf_done == 0) ? $this->payroll_recoveries->findAmountPerCycleForDeduction($payroll_recovery) : $payroll_recovery->payrollDeduction->amount)  : ($payroll_recovery->total_amount / $payroll_recovery->cycles);

        $last_cycle_amount = ($recovery_type->reference == 'PRTOVEP') ? (($wf_done == 0) ? $this->payroll_recoveries->findLastCycleAmountForDeduction($payroll_recovery) : $payroll_recovery->payrollDeduction->last_cycle_amount) : 0;
        $workflow_input = ['resource_id' => $payroll_recovery->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => 0 ];
        $check_if_pending_payment = $this->payroll_recoveries->checkIfIsPendingForPayment($payroll_recovery->id);

        return view('backend/operation/payroll/pension_administration/payroll_recovery/profile/profile')
            ->with(['resource' => $resource, 'member_type_id' => $member_type_id, 'payroll_recovery' => $payroll_recovery,'check_pending_level1' => $check_pending_level1, "workflow_track" => $workflowTrackDataTable, 'workflow_input' => $workflow_input, 'recovery_type' => $recovery_type, 'mp' => $mp, 'amount_per_cycle' => $amount_per_cycle, 'last_cycle_amount' => $last_cycle_amount, 'check_if_pending_payment' => $check_if_pending_payment, 'dependent_employee' => $dependent_employee]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollRecovery $payroll_recovery)
    {
        //
        $this->checkLevelRights($payroll_recovery->id, 1);
        $member_type_id = $payroll_recovery->member_type_id;
        $resource_id = $payroll_recovery->resource_id;
        $employee_id = $payroll_recovery->employee_id;
        $resource = $this->getResource($member_type_id, $resource_id);
        $mp = $this->getMp($member_type_id, $resource_id, $employee_id);
        $recovery_type = $payroll_recovery->recoveryType;
        $arrears_pending = (new PayrollArrearRepository())->getNetPendingArrearsAmountForMember($member_type_id, $resource_id, $employee_id);
        return view('backend/operation/payroll/pension_administration/payroll_recovery/edit/edit')
            ->with(['payroll_recovery' => $payroll_recovery, 'resource' => $resource, 'member_type_id' => $payroll_recovery->member_type_id, 'employee_id' => $employee_id,'recovery_type' => $recovery_type, 'mp' => $mp, 'arrears_pending' => $arrears_pending ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( PayrollRecovery $payroll_recovery, PayrollRecoveryRequest $request)
    {
        //
        $this->checkLevelRights($payroll_recovery->id, 1);
        $input = $request->all();
        $this->payroll_recoveries->update($payroll_recovery, $input);
        return redirect()->route('backend.payroll.recovery.profile', $payroll_recovery->id)->withFlashSuccess('Success, Payroll recovery has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayrollRecovery $payroll_recovery)
    {
        //
        $this->checkLevelRights($payroll_recovery->id, 1);
        $this->payroll_recoveries->undo($payroll_recovery);
        $url = $this->returnResourceProfileUrl($payroll_recovery->member_type_id,  $payroll_recovery->resource_id, $payroll_recovery->employee_id);
        return redirect($url)->withFlashSuccess('Success, Payroll recovery has been undone');
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * Get resource based on the member type
     */
    public function getResource($member_type_id, $resource_id)
    {
        $payroll_repo = new PayrollRepository();
        $resource = $payroll_repo->getResource($member_type_id, $resource_id);
        return $resource;
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * Get MP - Monthly pension
     */
    public function getMp($member_type_id, $resource_id, $employee_id)
    {
        if($member_type_id == 5){
            /*Pensioner*/
            $resource = $this->pensioners->find($resource_id);
            $mp = $resource->monthly_pension_amount;
        }elseif($member_type_id == 4){
            /*Dependents*/
            $mp = $this->dependents->getMonthlyPensionPerNotification($resource_id,$employee_id);

        }
        return $mp;
    }



    /*Return resource profile page url*/
    public function returnResourceProfileUrl($member_type_id, $resource_id, $employee_id)
    {
        $payrolls = new PayrollRepository();
        $url = $payrolls->returnResourceProfileUrl($member_type_id, $resource_id, $employee_id);
        return $url;
    }


    /**
     * @param PayrollRecovery $payroll_recovery
     * @return $this
     * open Cancel page
     */
    public function openCancelPage(PayrollRecovery $payroll_recovery)
    {
        $member_type_id = $payroll_recovery->member_type_id;
        $resource_id = $payroll_recovery->resource_id;
        $employee_id = $payroll_recovery->employee_id;
        $resource = $this->getResource($member_type_id, $resource_id);
        $recovery_type = $payroll_recovery->recoveryType;
        return view('backend/operation/payroll/pension_administration/payroll_recovery/cancel/cancel')
            ->with('resource', $resource)
            ->with('recovery_type', $recovery_type)
            ->with('payroll_recovery', $payroll_recovery);
    }


    /**
     * @param PayrollRecovery $payroll_recovery
     * @param PayrollRecoveryCancelRequest $request
     * @return mixed
     * Cancel already approved recovery which is pending for payment on next payroll
     */
    public function cancelApprovedRecovery(PayrollRecovery $payroll_recovery, PayrollRecoveryCancelRequest $request)
    {
        $input = $request->all();
        $this->payroll_recoveries->cancelApprovedRecovery($payroll_recovery, $input);
        $url = $this->returnResourceProfileUrl($payroll_recovery->member_type_id, $payroll_recovery->resource_id, $payroll_recovery->employee_id);
        return redirect($url)->withFlashSuccess('Success, Payroll recovery has been cancelled');
    }



    /**
     * @param $resource_id
     * @param $notification_report_id
     *Get recoverires per beneficiary i.e. Pensioner/ Dependant
     */
    public function getRecoveriesByBeneficiaryForDataTable($member_type_id, $resource_id, $notification_report_id)
    {
        return Datatables::of($this->payroll_recoveries->getByBeneficiaryForDataTable($member_type_id, $resource_id,$notification_report_id)->orderBy('id', 'desc'))
            ->addColumn('total_amount_formatted', function ($recovery) {
                return number_2_format( $recovery->total_amount);
            })
            ->addColumn('created_at_formatted', function ($recovery) {
                return short_date_format($recovery->created_at);
            })
            ->addColumn('wf_done_date_formatted', function ($recovery) {
                return   ($recovery->wf_done_date)  ? short_date_format($recovery->wf_done_date) : ' ';
            })
            ->addColumn('recovery_type', function ($recovery) {
                return $recovery->recoveryType->name;
            })
            ->addColumn('user', function ($recovery) {
                return $recovery->user->username;
            })
            ->addColumn('status', function ($recovery) {
                return $recovery->status_label;
            })
            ->rawColumns(['status', 'remark'])
            ->make(true);

    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get approved arrears by beneficiary for dataTable per notification report
     */
    public function getApprovedArrearsByBeneficiaryForDataTable($member_type_id, $resource_id, $notification_report_id)
    {
//        $recovery_type_id = $this->code_values->findByReference('PRTUNDP')->id;
        $arrears_cv_refs_array = (new PayrollArrearRepository())->getArrearsTypeReferencesArray();
        $arrears_cv_ids_array = $this->code_values->findIdsByReferences($arrears_cv_refs_array);
        $approved_arrears = $this->payroll_recoveries->getApprovedByBeneficiaryForDataTable($member_type_id, $resource_id,$notification_report_id)->whereIn('recovery_type_cv_id', $arrears_cv_ids_array)->orderBy
        ('id','desc');
        return Datatables::of($approved_arrears)
            ->editColumn('total_amount', function ($recovery) {
                return number_2_format( $recovery->total_amount);
            })
            ->addColumn('recycles', function ($recovery) {
                return $recovery->payrollArrear->recycles;
            })
            ->addColumn('amount', function ($recovery) {
                return $recovery->payrollArrear->amount;
            })
            ->addColumn('recovered_amount', function ($recovery) {
                return number_2_format($recovery->recovered_amount);
            })
            ->addColumn('balance_amount', function ($recovery) {
                return number_2_format(($recovery->total_amount - $recovery->recovered_amount));
            })
            ->addColumn('status', function ($recovery) {
                return ($recovery->payrollArrear->recycles < 1) ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Recovered' . "'>" . 'Recovered' . "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
            })
            ->rawColumns(['status', 'remark'])
            ->make(true);
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get approved unclaims by beneficiary for dataTable per notification report
     */
    public function getApprovedUnclaimsByBeneficiaryForDataTable($member_type_id, $resource_id, $notification_report_id)
    {
        $recovery_type_id = $this->code_values->findByReference('PRTUNCLP')->id;
        $approved_unclaims = $this->payroll_recoveries->getApprovedByBeneficiaryForDataTable($member_type_id, $resource_id,$notification_report_id)->where('recovery_type_cv_id', $recovery_type_id)
            ->orderBy
            ('id','desc');
        return Datatables::of($approved_unclaims)
            ->editColumn('total_amount', function ($recovery) {
                return number_2_format( $recovery->total_amount);
            })
            ->addColumn('recycles', function ($recovery) {
                return $recovery->payrollUnclaim->recycles;
            })
            ->addColumn('amount', function ($recovery) {
                return $recovery->payrollUnclaim->amount;
            })
            ->addColumn('recovered_amount', function ($recovery) {
                return number_2_format($recovery->recovered_amount);
            })
            ->addColumn('balance_amount', function ($recovery) {
                return number_2_format(($recovery->total_amount - $recovery->recovered_amount));
            })
            ->addColumn('status', function ($recovery) {
                return ($recovery->payrollUnclaim->recycles < 1) ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Recovered' . "'>" . 'Recovered' . "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
            })
            ->rawColumns(['status', 'remark'])
            ->make(true);
    }



    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Get approved deductions by beneficiary for dataTable per notification report
     */
    public function getApprovedDeductionsByBeneficiaryForDataTable($member_type_id, $resource_id, $notification_report_id)
    {
        $recovery_type_id = $this->code_values->findByReference('PRTOVEP')->id;
        $approved_deductions = $this->payroll_recoveries->getApprovedByBeneficiaryForDataTable($member_type_id, $resource_id,$notification_report_id)->where('recovery_type_cv_id', $recovery_type_id)
            ->orderBy
            ('id','desc');
        return Datatables::of($approved_deductions)
            ->editColumn('total_amount', function ($recovery) {
                return number_2_format( $recovery->total_amount);
            })
            ->addColumn('recycles', function ($recovery) {
                return $recovery->payrollDeduction->recycles;
            })
            ->addColumn('amount', function ($recovery) {

                return $recovery->payrollDeduction->amount;
            })
            ->addColumn('recovered_amount', function ($recovery) {
                return number_2_format($recovery->recovered_amount);
            })
            ->addColumn('balance_amount', function ($recovery) {
                return number_2_format(($recovery->total_amount - $recovery->recovered_amount));
            })
            ->addColumn('status', function ($recovery) {
                return ($recovery->payrollDeduction->recycles < 1) ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Recovered' . "'>" . 'Recovered' . "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending' . "'>" . 'Pending' . "</span>";
            })
            ->rawColumns(['status', 'remark'])
            ->make(true);
    }

    /**
     * @param $resource_id
     * @param $notification_report_id
     *Get pending payroll recoveries for datatable
     */
    public function getPendingRecoveriesForDataTable()
    {
        return Datatables::of($this->payroll_recoveries->getAllPendingsForDataTable()->orderBy('id', 'asc'))
            ->addColumn('total_amount_formatted', function ($recovery) {
                return number_2_format( $recovery->total_amount);
            })
            ->addColumn('created_at_formatted', function ($recovery) {
                return short_date_format($recovery->created_at);
            })
            ->addColumn('wf_done_date_formatted', function ($recovery) {
                return   ($recovery->wf_done_date)  ? short_date_format($recovery->wf_done_date) : ' ';
            })
            ->addColumn('recovery_type', function ($recovery) {
                return $recovery->recoveryType->name;
            })
            ->addColumn('user', function ($recovery) {
                return $recovery->user->username;
            })
            ->addColumn('status', function ($recovery) {
                return $recovery->status_label;
            })
            ->rawColumns(['status'])
            ->make(true);

    }

    /**
     * Check if can Initiate Action (Level ?/ No Workflow)
     */
    public function checkLevelRights($resource_id, $level)
    {
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowDefinition($wf_module_group_id, $level);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction($level);
    }
}
