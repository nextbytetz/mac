<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Backend\Operation\Payroll\Traits\PayrollChildExtensionTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\ChangeStatusRequest;
use App\Models\Operation\Payroll\PayrollStatusChange;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRecoveryRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollStatusChangeController extends Controller
{
    use PayrollChildExtensionTrait;

    protected $payroll_status_changes;
    protected $pensioners;
    protected $dependents;
    protected $code_values;
    protected $wf_modules;
    protected $notification_reports;
    protected $manual_notification_reports;


    public function __construct()
    {
        $this->payroll_status_changes = new PayrollStatusChangeRepository();
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->code_values = new CodeValueRepository();
        $this->wf_modules = new WfModuleRepository();
        $this->notification_reports = new NotificationReportRepository();
        $this->manual_notification_reports = new ManualNotificationReportRepository();

        //TODO: Apply permissions for the functions
        if(env('TESTING_MODE') == 1)
        {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
    }


    /*Main wf module group id*/
    public function wfModuleGroupId()
    {
        return $this->payroll_status_changes->wfModuleGroupId();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($member_type_id, $resource_id, $employee_id, $status_change_type)
    {
        //
        $payroll_repo = new PayrollRepository();
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $this->payroll_status_changes->checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id, $status_change_type);
        $resource = $this->getResource($member_type_id, $resource_id);
        $status_change_cv= $this->code_values->findByReference($status_change_type);
        $dependent_employee = ($member_type_id == 4) ?  $this->getDependentInput($resource_id,$employee_id)['dependent_employee'] : null;
        $isotherdep = isset($dependent_employee) ? $dependent_employee->isotherdep : 0;
        $dependent_type_id = isset($dependent_employee) ? $dependent_employee->dependent_type_id : null;
//        $child_overage_eligible_data = ($dependent_type_id == 3) ? $payroll_repo->getOverageChildEligibleData($member_type_id, $resource_id, $employee_id) : null;
        $arrears_summary = $payroll_repo->getArrearsSummaryOnActivation($member_type_id, $resource_id, $employee_id);

        $first_pay_flag = ($member_type_id == 4) ? $dependent_employee->firstpay_flag : $resource->firstpay_flag;
        /*Check if is was processed from manual and specified with arrears on data migration*/
        $first_pay_flag_system = $resource->getFirstPayFlagSystemAttribute($employee_id);

        $code_value_repo = new CodeValueRepository();
        $document_types = null;
        /*Opt parameters*/
        switch($status_change_cv->reference){
            case 'PSCCHDREI':
                $document_types = (new DocumentRepository())->queryDocumentsByArrayIds([63,16,138])->get();
                $arrears_summary = (new PayrollSuspendedRunRepository())->getArrearsSummaryForSuspendedPayments($member_type_id, $resource_id, $employee_id);
                break;
        }

        return view('backend/operation/payroll/pension_administration/status_changes/create')
            ->with(['member_type_id' => $member_type_id, 'resource' => $resource, 'dependent_employee' => $dependent_employee,'employee_id' => $employee_id, 'status_change_type' => $status_change_cv, 'arrears_summary' => $arrears_summary, 'first_pay_flag'=> $first_pay_flag, 'first_pay_flag_system' => $first_pay_flag_system, 'isotherdep' => $isotherdep, 'code_value_repo' => $code_value_repo, 'document_types' => $document_types]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChangeStatusRequest $request)
    {
        //
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $input = $request->all();
        $status_change = $this->payroll_status_changes->initiateWfOnCreate($input);
        $hasarrears = $status_change->hasarrears;
        if($hasarrears == 1){
            $arrears_month = $status_change->pending_pay_months;
            /*Has arrears*/
            return redirect()->route('backend.payroll.recovery.create_manual_arrears_system_members',[$status_change->member_type_id, $status_change->resource_id, $status_change->employee_id, $arrears_month])->withFlashSuccess('Success, Status change has been initiated! Proceed to initiate approval of arrears');
        }else {
            return redirect('payroll/status_change/profile/' . $status_change->id)->withFlashSuccess('Success, Status change approval has been initiated');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile(PayrollStatusChange $status_change, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        //
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $wf_type = $this->payroll_status_changes->getWfType($status_change);
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $status_change->id, 'type' => $wf_type]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        $current_wf_track = ($check_workflow) ?  $workflow->currentWfTrack() : null;

        $level_for_disability_change = $this->payroll_status_changes->getLevelForDisabilityAssessment($status_change);
        $check_if_can_initiate_disability_assess =($level_for_disability_change) ? $this->payroll_status_changes->checkIfCanInitiateActionByLevel($current_wf_track, $level_for_disability_change) : false;
        $level_for_letter =$this->payroll_status_changes->getLevelForLetterPreparation($status_change);
        $check_if_can_initiate_letter = ($level_for_letter)  ? $this->payroll_status_changes->checkIfCanInitiateActionByLevel($current_wf_track, $level_for_letter)  : false;

        $workflow_input = ['resource_id' => $status_change->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $wf_type];
        /*end workflow*/

        $resource_id = $status_change->resource_id;
        $member_type_id = $status_change->member_type_id;
        $employee_id = $status_change->employee_id;
        $resource = $this->getResource($member_type_id, $resource_id);
        $status_change_type =$status_change->statusChangeType;
        /*dependent input when processing dependent*/
        $dependent_input = ($member_type_id == 4) ?  $this->getDependentInput($resource_id,$employee_id) : null;

        /*Arrears summary*/
        $dependent_employee = ($member_type_id == 4) ?  $this->getDependentInput($resource_id,$employee_id)['dependent_employee'] : null;

        $first_pay_flag = ($member_type_id == 4) ? $dependent_employee->firstpay_flag : $resource->firstpay_flag;
        $first_pay_flag = ($status_change->firstpay_flag) ? $status_change->firstpay_flag : $first_pay_flag;
        $arrears_summary = null;
        /*Check if is was processed from manual and specified with arrears on data migration*/
        $first_pay_flag_system = $resource->getFirstPayFlagSystemAttribute($employee_id);
        if($status_change->wf_done == 0){
            $arrears_summary = ($first_pay_flag == 0 || $first_pay_flag_system == 0) ? (new PayrollRepository())->getArrearsSummaryOnActivation($member_type_id,$resource_id, $employee_id)['summary'] : null;
        }else{
            $arrears_summary = $status_change->arrears_summary;
        }
        /*document evidences*/
        $document_evidences = ($status_change->doc_evidence) ? (DB::table('document_payroll_beneficiary')->whereIn('id', json_decode($status_change->doc_evidence))->get()) : null;

        return view('backend/operation/payroll/pension_administration/status_changes/profile/profile')
            ->with(['resource' => $resource, 'member_type_id' => $member_type_id,'employee_id' => $employee_id, 'status_change' => $status_change,'check_pending_level1' => $check_pending_level1, "workflow_track" => $workflowTrackDataTable, 'workflow_input' => $workflow_input, 'dependent_input' => $dependent_input, 'status_change_type' => $status_change_type, 'arrears_summary' => $arrears_summary, 'document_evidences' => $document_evidences, 'document_repo' => (new DocumentRepository()), 'check_if_can_initiate_disability_assess' => $check_if_can_initiate_disability_assess, 'check_if_can_initiate_letter' => $check_if_can_initiate_letter]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollStatusChange $status_change)
    {
        //
        $this->checkLevelRights($status_change->id, 1);
        $member_type_id = $status_change->member_type_id;
        $resource_id = $status_change->resource_id;
        $employee_id = $status_change->employee_id;
        $resource = $this->getResource($member_type_id, $resource_id);
        $status_change_type =$status_change->statusChangeType;
        /*Arrears summary*/
        $dependent_employee = ($member_type_id == 4) ?  $this->getDependentInput($resource_id,$employee_id)['dependent_employee'] : null;
        $first_pay_flag = ($member_type_id == 4) ? $dependent_employee->firstpay_flag : $resource->firstpay_flag;
//        $first_pay_flag = ($status_change->firstpay_flag) ? $status_change->firstpay_flag : $first_pay_flag;
        $arrears_summary = null;

        if($status_change->wf_done == 0){
            $arrears_summary = ($first_pay_flag == 0) ? (new PayrollRepository())->getArrearsSummaryOnActivation($member_type_id,$resource_id, $employee_id)['summary'] : null;
        }else{
            $arrears_summary = $status_change->arrears_summary;
        }
        /*Document evidence*/
        $document_evidences = ($status_change->doc_evidence) ? (DB::table('document_payroll_beneficiary')->whereIn('id', json_decode($status_change->doc_evidence))->get()) : null;
        $child_continuation_reasons = null;
        $document_types = null;
        /*Opt parameters*/
        $code_value_repo = new CodeValueRepository();
        switch($status_change_type->reference){
            case 'PSCCHDREI':
                $document_types = (new DocumentRepository())->queryDocumentsByArrayIds([63,16,138])->get();
                $arrears_summary = (new PayrollSuspendedRunRepository())->getArrearsSummaryForSuspendedPayments($member_type_id, $resource_id, $employee_id);
                break;
        }
        return view('backend/operation/payroll/pension_administration/status_changes/edit')
            ->with(['status_change' => $status_change, 'resource' => $resource, 'member_type_id' => $member_type_id,'status_change_type' => $status_change_type, 'employee_id' => $employee_id ,'arrears_summary' => $arrears_summary, 'first_pay_flag' => $first_pay_flag, 'document_evidences' => $document_evidences, 'document_types' => $document_types, 'code_value_repo' => $code_value_repo]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ChangeStatusRequest $request, PayrollStatusChange $status_change)
    {
        //
        $input = $request->all();
        $this->payroll_status_changes->update($status_change, $input);
        return redirect('payroll/status_change/profile/' . $status_change->id )->withFlashSuccess('Success, Status change has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayrollStatusChange $status_change)
    {
        //
        $this->checkLevelRights($status_change->id, 1);
        $this->payroll_status_changes->undo($status_change);
        $url = (new PayrollRepository())->returnResourceProfileUrl($status_change->member_type_id,  $status_change->resource_id,$status_change->employee_id);
        return redirect($url)->withFlashSuccess('Success, Status change has been undone');
    }


    public function getDependentInput($dependent_id, $employee_id)
    {
        $dependent = $this->dependents->find($dependent_id);
        $dependent_employee = $dependent->employees()->where('employee_id', $employee_id)->first();
        $employee = (new EmployeeRepository())->find($employee_id);
        $dependent_type_id = isset($dependent_employee) ? $dependent_employee->pivot->dependent_type_id : null;
        $child_overage_eligible_data = ($dependent_type_id == 3) ? (new PayrollRepository())->getOverageChildEligibleData(4, $dependent_id, $employee_id) : null;
        return ['dependent_employee' => $dependent_employee->pivot, 'employee' => $employee, 'child_overage_eligible_data' => $child_overage_eligible_data];
    }


    /*Return resource profile page url*/
    public function returnResourceProfileUrl($member_type_id, $resource_id)
    {
        if($member_type_id == 5){
            /*Pensioner*/
            $url = 'payroll/pensioner/profile/'.$resource_id;
        }elseif($member_type_id == 4){
            /*dependent*/
            $url = 'compliance/dependent/profile/'.$resource_id;
        }
        return $url;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * Get resource based on the member type
     */
    public function getResource($member_type_id, $resource_id)
    {
        $payroll_repo = new PayrollRepository();
        $resource = $payroll_repo->getResource($member_type_id, $resource_id);
        return $resource;
    }



    /**
     * @param $resource_id
     * @param $notification_report_id
     *Get activation / deactivation status changes per beneficiary i.e. Pensioner/ Dependant
     */
    public function getActivationsByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)
    {
        return Datatables::of($this->payroll_status_changes->getActivationsForDataTable($member_type_id, $resource_id,$employee_id)->orderBy('id', 'desc'))

            ->addColumn('created_at_formatted', function ($status_change) {
                return short_date_format($status_change->created_at);
            })
            ->addColumn('wf_done_date_formatted', function ($status_change) {
                return ($status_change->wf_done_date)  ? short_date_format($status_change->wf_done_date) : ' ';
            })
            ->addColumn('status_change_type', function ($status_change) {
                return $status_change->statusChangeType->name;
            })
            ->addColumn('user', function ($recovery) {
                return $recovery->user->username;
            })
            ->addColumn('status', function ($recovery) {
                return $recovery->status_label;
            })
            ->rawColumns(['status'])
            ->make(true);

    }

    /**
     * @param $resource_id
     * @param $notification_report_id
     *Get   Suspensions/Reinstates status changes per beneficiary i.e. Pensioner/ Dependant
     */
    public function getSuspensionsReinstatesByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)
    {
        return Datatables::of($this->payroll_status_changes->getSuspensionsReinstatesForDataTable($member_type_id, $resource_id,$employee_id)->orderBy('id', 'desc'))

            ->addColumn('created_at_formatted', function ($status_change) {
                return short_date_format($status_change->created_at);
            })
            ->addColumn('wf_done_date_formatted', function ($status_change) {
                return ($status_change->wf_done_date) ? short_date_format($status_change->wf_done_date) : ' ';
            })
            ->addColumn('status_change_type', function ($status_change) {
                return $status_change->statusChangeType->name;
            })
            ->addColumn('user', function ($recovery) {
                return $recovery->user->username;
            })
            ->addColumn('status', function ($recovery) {
                return $recovery->status_label;
            })
            ->rawColumns(['status'])
            ->make(true);

    }




    /**
     * Check if can Initiate Action (Level ?/ No Workflow)
     */
    public function checkLevelRights($resource_id, $level)
    {
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowDefinition($wf_module_group_id, $level);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction($level);
    }

}
