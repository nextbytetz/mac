<?php

namespace App\Http\Controllers\Backend\Operation\Payroll\Retiree;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\PayrollMpUpdateRequest;
use App\Http\Requests\Backend\Operation\Payroll\Retiree\PayrollRetireeMpUpdateRequest;
use App\Models\Operation\Payroll\PayrollMpUpdate;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeMpUpdate;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollMpUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Operation\Payroll\Retiree\PayrollRetireeFollowupRepository;
use App\Repositories\Backend\Operation\Payroll\Retiree\PayrollRetireeMpUpdateRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollRetireeMpUpdateController extends Controller
{

    protected $payroll_retiree_mp_update_repo;
    /**
     *Construct Method for this class
     */
    public function __construct()
    {
        $this->payroll_retiree_mp_update_repo = new PayrollRetireeMpUpdateRepository();
    }

    /*Get wf module group id*/
    public function getWfModuleGroupId()
    {
        return $this->payroll_retiree_mp_update_repo->getWfModuleGroupId();
    }

    /**
     *Open list all page of PayrollRetireeMpUpdate
     */
    public function index()
    {
        return view('backend/operation/payroll/pension_administration/retiree/retiree_mp_update/index');
    }


    /**
     *Open page for adding new PayrollRetireeMpUpdate
     */
    public function create($payroll_retiree_followup_id,  $member_type_id, $resource_id, $employee_id)
    {
        $this->payroll_retiree_mp_update_repo->checkIfCanInitiateNewEntry($member_type_id, $resource_id, $employee_id);
        $payroll_repo = new PayrollRepository();
        $resource =$payroll_repo->getResource($member_type_id, $resource_id);
        $current_mp = $payroll_repo->getMp($member_type_id, $resource_id, $employee_id);
        $document_types = (new DocumentRepository())->queryDocumentsByArrayIds([63, 139])->get();
        $return_url = $payroll_repo->returnResourceProfileUrl($member_type_id, $resource_id, $employee_id);
        $payroll_retiree_followup = (new PayrollRetireeFollowupRepository())->find($payroll_retiree_followup_id);
        return view('backend/operation/payroll/pension_administration/retiree/retiree_mp_update/create')
            ->with('resource',$resource)
            ->with('employee_id',$employee_id)
            ->with('member_type_id',$member_type_id)
            ->with('document_types',$document_types)
            ->with('current_mp',$current_mp)
            ->with('return_url',$return_url)
            ->with('payroll_retiree_followup',$payroll_retiree_followup);
    }

    /**
     *Save new entry for PayrollRetireeMpUpdate
     */
    public function store(PayrollRetireeMpUpdateRequest $request)
    {
        $input = $request->all();
        $this->payroll_retiree_mp_update_repo->checkIfCanInitiateNewEntry($input['member_type_id'], $input['resource_id'], $input['employee_id']);
        $payrollRetireeMpupdate = $this->payroll_retiree_mp_update_repo->store($input);
        return redirect()->route('backend.payroll.retiree.mp_update.profile',['payroll_retiree_mp_update' => $payrollRetireeMpupdate->id])->withFlashSuccess('Success, Entry has been created');
    }

    /**
     *Open page for modifying existing PayrollRetireeMpUpdate
     */
    public function edit(PayrollRetireeMpUpdate $payrollRetireeMpUpdate)
    {
        $payroll_repo =(new PayrollRepository());
        $member_type_id = $payrollRetireeMpUpdate->member_type_id;
        $resource_id = $payrollRetireeMpUpdate->resource_id;
        $employee_id = $payrollRetireeMpUpdate->employee_id;
        $resource = $payroll_repo->getResource($member_type_id, $resource_id);

        /*Document evidence*/
        $document_types = (new DocumentRepository())->queryDocumentsByArrayIds([63,139])->get();
        $document_evidences = ($payrollRetireeMpUpdate->doc_evidence) ? (DB::table('document_payroll_beneficiary')->whereIn('id', json_decode($payrollRetireeMpUpdate->doc_evidence))->get()) : null;
        return view('backend/operation/payroll/pension_administration/retiree/retiree_mp_update/edit')->with('payroll_retiree_mp_update', $payrollRetireeMpUpdate)
            ->with('resource',$resource)
            ->with('employee_id',$employee_id)
            ->with('member_type_id',$member_type_id)
            ->with('document_types',$document_types)
            ->with('document_evidences',$document_evidences);
    }

    /**
     *Modify existing PayrollRetireeMpUpdate
     */
    public function update(PayrollRetireeMpupdateRequest $request, PayrollRetireeMpUpdate $payrollRetireeMpUpdate)
    {
        $input = $request->all();
        $payrollRetireeMpUpdate = $this->payroll_retiree_mp_update_repo->update($payrollRetireeMpUpdate, $input);
        return redirect()->route('backend.payroll.retiree.mp_update.profile',['payroll_retiree_mp_update' => $payrollRetireeMpUpdate->id])->withFlashSuccess('Success, Entry has been updated');
    }

    /**
     *Overview page to display data of PayrollRetireeMp Update
     */
    public function profile(PayrollRetireeMpUpdate $payrollRetireeMpUpdate, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        $payroll_repo = new PayrollRepository();
        /* Check workflow */
        $wf_module_group_id = $this->getWfModuleGroupId();
        $wf_type = $this->payroll_retiree_mp_update_repo->getWfType($payrollRetireeMpUpdate);
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payrollRetireeMpUpdate->id, 'type' => $wf_type]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        $current_wf_track = ($check_workflow) ?  $workflow->currentWfTrack() : null;
        $workflow_input = ['resource_id' => $payrollRetireeMpUpdate->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $wf_type];
        /*end workflow*/
        $member_type_id = $payrollRetireeMpUpdate->member_type_id;
        $resource_id = $payrollRetireeMpUpdate->resource_id;
        $employee_id = $payrollRetireeMpUpdate->employee_id;
        $resource =$payroll_repo->getResource($member_type_id, $resource_id);
        $return_url = $payroll_repo->returnResourceProfileUrl($member_type_id, $resource_id, $employee_id);
        /*document evidences*/
        $document_repo = new DocumentRepository();
        $document_evidences = ($payrollRetireeMpUpdate->doc_evidence) ? (DB::table('document_payroll_beneficiary')->whereIn('id', json_decode($payrollRetireeMpUpdate->doc_evidence))->get()) : null;
        return view('backend/operation/payroll/pension_administration/retiree/retiree_mp_update/profile/profile')
            ->with('payroll_retiree_mp_update', $payrollRetireeMpUpdate)
            ->with('resource',$resource)
            ->with('employee_id',$employee_id)
            ->with('member_type_id',$member_type_id)
            ->with('return_url',$return_url)
            ->with('document_evidences',$document_evidences)
            ->with('document_repo',$document_repo)
            ->with(['check_pending_level1' => $check_pending_level1, "workflow_track" => $workflowTrackDataTable, 'workflow_input' => $workflow_input]);
    }


    /**
     *Open page to display data of PayrollRetireeMpUpdate
     */
    public function show(PayrollRetireeMpUpdate $payrollRetireeMpupdate)
    {
        return view('resources/views/backend/operation/payroll/pension_administration/retiree/retiree_mp_update/show')->with('payroll_retiree_mp_update', $payrollRetireeMpupdate);
    }

    /**
     *delete/destroy PayrollRetireeMpUpdate
     */
    public function delete(PayrollRetireeMpUpdate $payrollRetireeMpUpdate)
    {
        $return_url = (new PayrollRepository())->returnResourceProfileUrl($payrollRetireeMpUpdate->member_type_id, $payrollRetireeMpUpdate->resource_id, $payrollRetireeMpUpdate->employee_id);
        $payrollRetireeMpUpdate = $this->payroll_retiree_mp_update_repo->delete($payrollRetireeMpUpdate);
        return redirect($return_url)->withFlashSuccess('Success, Entry has been undone');
    }

    /**
     *list all PayrollRetireeMpUpdate
     */
    public function getAllForDt()
    {
        $result_list = $this->payroll_retiree_mp_update_repo->getAllForDt();
        return DataTables::of($result_list)
            ->addIndexColumn()
            ->rawColumns([''])
            ->make(true);
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * @return mixed
     * List mp updates by member
     */
    public function getMpUpdatesByMemberForDt($member_type_id, $resource_id, $employee_id)
    {
        $result_list = $this->payroll_retiree_mp_update_repo->getMpUpdatesByMemberForDt($member_type_id, $resource_id, $employee_id)->orderByDesc('id');
        return DataTables::of($result_list)
            ->editColumn('wf_done_date', function ($query) {
                return short_date_format($query->wf_done_date);
            })
            ->editColumn('current_mp', function ($query) {
                return number_2_format($query->current_mp);
            })
            ->editColumn('social_security_mp', function ($query) {
                return number_2_format($query->social_security_mp);
            })
            ->editColumn('deduction_amount', function ($query) {
                $label = ($query->deduction_amount >= 0) ? 'Deduction: ' : 'Arrears: ';
                return $label . number_2_format($query->deduction_arrear_abs);
            })
            ->editColumn('user_id', function ($query) {
                return $query->user->name;
            })
            ->editColumn('status', function ($query) {
                return ($query->status_label);
            })
            ->rawColumns(['status'])
            ->make(true);
    }


}
