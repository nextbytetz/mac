<?php

namespace App\Http\Controllers\Backend\Operation\Payroll\Retiree;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\PayrollMpUpdateRequest;
use App\Http\Requests\Backend\Operation\Payroll\Retiree\PayrollRetireeFollowupRequest;
use App\Models\Operation\Payroll\PayrollMpUpdate;
use App\Models\Operation\Payroll\Retiree\PayrollRetireeFollowup;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollMpUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Operation\Payroll\Retiree\PayrollRetireeFollowupRepository;
use App\Repositories\Backend\Operation\Payroll\Retiree\PayrollRetireeMpUpdateRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollRetireeFollowupController extends Controller
{

    protected $payroll_retiree_followup_repo;

    public function __construct()
    {
        $this->payroll_retiree_followup_repo = new PayrollRetireeFollowupRepository();
    }

    /*Get wf module group id*/
    public function getWfModuleGroupId()
    {
        return (new PayrollRetireeMpUpdateRepository())->getWfModuleGroupId();
    }

    /**
     *Open page for adding new PayrollRetireeFollowup
     */
    public function create($member_type_id, $resource_id, $employee_id)
    {
        /*check access*/
        access()->hasWorkflowDefinition($this->getWfModuleGroupId(), 1);
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        $code_value_repo = new CodeValueRepository();
        $dob_parsed = Carbon::parse($resource->dob);
        $date_of_next_followup = Carbon::now()->addYear()->format('Y') . '-' . $dob_parsed->format('n') . '-' . $dob_parsed->format('j');
        return view('backend/operation/payroll/pension_administration/retiree/retiree_followup/create')
            ->with('resource',$resource)
            ->with('employee_id',$employee_id)
            ->with('member_type_id',$member_type_id)
            ->with('date_of_next_followup',$date_of_next_followup)
            ->with('code_value_repo',$code_value_repo);
    }


    /**
     *Save new entry for PayrollRetireeFollowup
     */
    public function store(PayrollRetireeFollowupRequest $request)
    {
        $input = $request->all();
        /*check access*/
        access()->hasWorkflowDefinition($this->getWfModuleGroupId(), 1);
        $payrollRetireeFollowup = $this->payroll_retiree_followup_repo->store($input);
        $feed_back_cv_ref = $payrollRetireeFollowup->retireeFeedback->reference;
        switch($feed_back_cv_ref){
            case 'PRFFSTLL'://still working
                $return_url = (new PayrollRepository())->returnResourceProfileUrl($payrollRetireeFollowup->member_type_id, $payrollRetireeFollowup->resource_id, $payrollRetireeFollowup->employee_id);
                return redirect($return_url . '#retiree_followup')->withFlashSuccess('Success, Entry has been created');
                break;
            default://Retired
                return redirect()->route('backend.payroll.retiree.mp_update.create', [$payrollRetireeFollowup->id,$payrollRetireeFollowup->member_type_id, $payrollRetireeFollowup->resource_id, $payrollRetireeFollowup->employee_id])->withFlashSuccess('Success, Entry has been created');
                break;
        }

    }

    /**
     *Open page for modifying existing PayrollRetireeFollowup
     */
    public function edit(PayrollRetireeFollowup $payrollRetireeFollowup)
    {
        $payroll_repo =(new PayrollRepository());
        $member_type_id = $payrollRetireeFollowup->member_type_id;
        $resource_id = $payrollRetireeFollowup->resource_id;
        $employee_id = $payrollRetireeFollowup->employee_id;
        $resource = $payroll_repo->getResource($member_type_id, $resource_id);
        $code_value_repo = new CodeValueRepository();
        $dob_parsed = Carbon::parse($resource->dob);
        $date_of_next_followup = Carbon::now()->addYear()->format('Y') . '-' . $dob_parsed->format('n') . '-' . $dob_parsed->format('j');
        $return_url = $payroll_repo->returnResourceProfileUrl($payrollRetireeFollowup->member_type_id, $payrollRetireeFollowup->resource_id, $payrollRetireeFollowup->employee_id);
        return view('backend/operation/payroll/pension_administration/retiree/retiree_followup/edit')->with('payroll_retiree_followup', $payrollRetireeFollowup)
            ->with('resource',$resource)
            ->with('employee_id',$employee_id)
            ->with('member_type_id',$member_type_id)
            ->with('date_of_next_followup',$date_of_next_followup)
            ->with('code_value_repo',$code_value_repo)
            ->with('return_url',$return_url);
    }


    /**
     *Modify existing PayrollRetireeFollowup
     */
    public function update(PayrollRetireeFollowupRequest $request, PayrollRetireeFollowup $payrollRetireeFollowup)
    {
        $input = $request->all();
        $payrollRetireeFollowup = $this->payroll_retiree_followup_repo->update($payrollRetireeFollowup, $input);
        $return_url = (new PayrollRepository())->returnResourceProfileUrl($payrollRetireeFollowup->member_type_id, $payrollRetireeFollowup->resource_id, $payrollRetireeFollowup->employee_id);
        $feed_back_cv_ref = $payrollRetireeFollowup->retireeFeedback->reference;
        if($payrollRetireeFollowup->payrollRetireeMpUpdate()->count() == 0){
            switch($feed_back_cv_ref){
                case 'PRFFSTLL'://still working
                    return redirect($return_url . '#retiree_followup')->withFlashSuccess('Success, Entry has been updated');
                    break;
                default://Retired

                    return redirect()->route('backend.payroll.retiree.mp_update.create', [$payrollRetireeFollowup->id,$payrollRetireeFollowup->member_type_id, $payrollRetireeFollowup->resource_id, $payrollRetireeFollowup->employee_id])->withFlashSuccess('Success, Entry has been updated');
                    break;
            }
        }else{
            return redirect($return_url . '#retiree_followup')->withFlashSuccess('Success, Entry has been updated');
        }


    }

    /**
     *Overview page to display data of PayrollRetireeFollowup
     */
    public function profile(PayrollRetireeFollowup $payrollRetireeFollowup)
    {
        return view('backend/operation/payroll/pension_administration/retiree/retiree_followup/profile/profile')->with('payroll_retiree_followup', $payrollRetireeFollowup);
    }

    /**
     *Open page to display data of PayrollRetireeFollowup
     */
    public function show(PayrollRetireeFollowup $payrollRetireeFollowup)
    {
        return view('backend/operation/payroll/pension_administration/retiree/retiree_followup/show')->with('payroll_retiree_followup', $payrollRetireeFollowup);
    }

    /**
     *delete/destroy PayrollRetireeFollowup
     */
    public function delete(PayrollRetireeFollowup $payrollRetireeFollowup)
    {
        $return_url = (new PayrollRepository())->returnResourceProfileUrl($payrollRetireeFollowup->member_type_id, $payrollRetireeFollowup->resource_id, $payrollRetireeFollowup->employee_id);
        $payrollRetireeFollowup = $this->payroll_retiree_followup_repo->delete($payrollRetireeFollowup);
        return redirect($return_url . '#retiree_followup')->withFlashSuccess('Success, Entry has been Deleted');
    }

    /*Retiree followup by member*/
    public function getRetireeFollowupsByMemberForDt($member_type_id, $resource_id, $employee_id)
    {
        $followups = $this->payroll_retiree_followup_repo->getQueryRetireeFollowupsByMember($member_type_id, $resource_id, $employee_id)->orderByDesc('date_of_follow_up');
        $latest_followup = $followups->first();
        $latest_followup_id = ($latest_followup) ? $latest_followup->id : null;
        return Datatables::of($followups)
            ->editColumn('date_of_follow_up', function ($query)  {
                return short_date_format($query->date_of_follow_up);
            })
            ->editColumn('date_of_next_follow_up', function ($query)  {
                return short_date_format($query->date_of_next_follow_up);
            })
            ->editColumn('retiree_feedback_cv_id', function ($query)  {
                return $query->retireeFeedback->name;
            })
            ->editColumn('follow_up_type_cv_id', function ($query)  {
                return $query->followupType->name;
            })
            ->editColumn('user_id', function ($query)  {
                return $query->user->name;
            })
            ->addColumn('actions', function ($query) use($latest_followup_id) {
                return  ($query->id == $latest_followup_id) ? '<a href="' . route('backend.payroll.retiree.followup.edit',[$query->id]) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Edit Followup' . '"></i></a>'  : '';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }



    /*Retiree alert by member type for dt*/
    public function getRetireeAlertByMemberTypeForDt($member_type_id)
    {
        $retirees = $this->payroll_retiree_followup_repo->getPayrollRetireeAlertsByMemberTypeForDt($member_type_id)->orderBy('b.dob');
        return Datatables::of($retirees)
            ->editColumn('next_followup_date', function ($query)  {
                return short_date_format($query->next_followup_date);
            })
            ->editColumn('dob', function ($query)  {
                return short_date_format($query->dob);
            })
            ->addColumn('age', function ($query)  {
                return age($query->dob, Carbon::now());
            })
            ->editColumn('days_left', function ($query)  {
                return ($query->days_left) < 0 ? ($query->days_left . ' passed deadline') : $query->days_left;
            })
            ->addColumn('actions', function ($query)  {
                return  '<a href="' . route('backend.payroll.retiree.followup.create',[$query->member_type_id, $query->resource_id, $query->employee_id]) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Fill Followup' . '"></i></a>' ;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }




}
