<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\CreateManualSurvivorRequest;
use App\Http\Requests\Backend\Operation\Payroll\UploadManualPayrollMemberRequest;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentTypeRepository;
use App\Repositories\Backend\Operation\Payroll\ManualPayrollMemberRepository;
use App\Repositories\Backend\Sysdef\GenderRepository;
use App\Repositories\Backend\Sysdef\IdentityRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class ManualPayrollMemberController extends Controller
{
    //
    protected $manual_payroll_members;

    public function __construct()
    {
        $this->manual_payroll_members = new ManualPayrollMemberRepository();
        $this->middleware('access.routeNeedsPermission:manage_payroll_data_migration', ['only' => [ 'uploadManualFile', 'createDependent' ]]);


        //On Backend
        if(env('TESTING_MODE') == 1)
        {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }

    }


    public function index()
    {
        return view('backend/operation/payroll/data_migration/manual_payroll_member/index');
    }

    /*open upload page*/
    public function uploadPage($member_type)
    {
        $no_of_uploaded_files = $this->manual_payroll_members->getForDataTable()->count();
        $file_url = $this->manual_payroll_members->uploadedFileUrl();
        return view('backend/operation/payroll/data_migration/manual_payroll_member/upload_bulk/upload_page')
            ->with('no_of_uploaded_files', $no_of_uploaded_files)
            ->with('uploaded_file_url', $file_url)
            ->with('member_type_id', $member_type);
    }

    /**
     * Upload manual files - notification report
     */
    public function uploadManualFile(UploadManualPayrollMemberRequest $request)
    {
        $input = $request->all();
        $this->manual_payroll_members->saveUploadedDocumentFromExcel($input);
        return redirect()->route('backend.payroll.manual_member.index', $input['member_type_id'])->withFlashSuccess('Success, document has been uploaded');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Dependent page
     */
    public function dependentsPage()
    {
        return view('backend/operation/payroll/data_migration/manual_payroll_member/dependent/index');
    }

    /*Create dependent*/
    public function createDependent($id)
    {
        $manual_dependent = $this->manual_payroll_members->find($id);
        $incident_type_id = $manual_dependent->incident_type_id;
        $manual_notification = $manual_dependent->manualNotificationReport($incident_type_id)->first();
        $employee = (isset($manual_notification)) ? $manual_notification->employee : null;
        return view('backend/operation/payroll/data_migration/manual_payroll_member/dependent/create')
            ->with([
                'employee' => $employee,
                'countries' => (new CountryRepository())->getAll()->pluck('name', 'id'),
                'genders' =>  (new GenderRepository())->getAll()->pluck('name', 'id'),
                'dependent_types' => (new DependentTypeRepository())->getAll()->pluck('name', 'id'),
                'location_types' => (new LocationTypeRepository())->getAll()->pluck('name', 'id'),
                'identities' => (new IdentityRepository())->getAll()->pluck('name', 'id'),
                'banks' => (new BankRepository())->getActiveBanksForSelect(),
                'bank_branches' => (new BankBranchRepository())->getAll()->pluck('name', 'id'),
                'dependent' => $manual_dependent
            ]);

    }

    /**
     * @param $employee
     * @param CreateManualSurvivorRequest $request
     *
     */
    public function storeDependent($employee_id, CreateManualSurvivorRequest $request)
    {
        $input = $request->all();
        $this->manual_payroll_members->createManualDependent($employee_id, $input);
        $hasarrears = isset($input['hasarrears']) ? $input['hasarrears'] : 0;
        if($hasarrears == 1){
            /*Has arrears*/
            return redirect()->route('backend.payroll.recovery.create_manual_arrears', $input['manual_dependent_id'])->withFlashSuccess('Success, Dependent has been synchronized! Proceed to initiate approval of arrears');
        }else{
            /*no arrears*/
            return redirect()->route('backend.payroll.manual_member.dependents_page', 5)->withFlashSuccess('Success, Dependent has been synchronized');
        }

    }



    public function getAllForDataTable()
    {
        return Datatables::of($this->manual_payroll_members->getForDataTable()->orderBy('issynced','asc'))
            ->addColumn('dob_formatted', function ($member) {
                return  isset($member->dob)  ? short_date_format($member->dob) : ' ';
            })
            ->addColumn('mp_formatted', function ($member) {
                return number_2_format($member->monthly_pension);
            })
            ->addColumn('member_type', function ($member) {
                return $member->memberType->name;
            })
//            ->addColumn('status', function ($member) {
//                return $member->status_label;
//            })
//
            ->rawColumns(['status', 'action'])
            ->make(true);
    }



    public function getUploadedPerMemberTypeDataTable($member_type)
    {
        return Datatables::of($this->manual_payroll_members->getForDataTable()->where('member_type_id',$member_type)->orderBy('issynced','asc'))
            ->addColumn('dob_formatted', function ($member) {
                return  isset($member->dob)  ? short_date_format($member->dob) : ' ';
            })
            ->addColumn('mp_formatted', function ($member) {
                return number_2_format($member->monthly_pension);
            })
            ->addColumn('member_type', function ($member) {
                return $member->memberType->name;
            })
            ->addColumn('action', function ($member) {
                return $member->add_button;
            })
//            ->addColumn('status', function ($member) {
//                return $member->status_label;
//            })
//
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    /*Get pending members with pending arrears for datatable*/
    public function getMembersWithPendingArrearsDataTable(){
        return Datatables::of($this->manual_payroll_members->getManualMembersWithPendingArrearsForDataTable())
            ->addColumn('dob_formatted', function ($member) {
                return  isset($member->dob)  ? short_date_format($member->dob) : ' ';
            })
            ->addColumn('mp_formatted', function ($member) {
                return number_2_format($member->monthly_pension);
            })
            ->addColumn('member_type', function ($member) {
                return $member->memberType->name;
            })
            ->addColumn('action', function ($member) {
                return $member->add_arrears_button;
            })
//            ->addColumn('status', function ($member) {
//                return $member->status_label;
//            })
//
            ->rawColumns(['status', 'action'])
            ->make(true);
    }




}
