<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\BeneficiaryUpdateRequest;
use App\Models\Location\Region;
use App\Models\Operation\Payroll\PayrollBeneficiaryUpdate;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollBeneficiaryUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollBeneficiaryUpdateController extends Controller
{


    protected $code_values;
    protected $pensioners;
    protected $dependents;
    protected $payroll_beneficiary_updates;
    protected $payrolls;

    public function __construct()
    {
        $this->code_values = new CodeValueRepository();
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->payroll_beneficiary_updates = new PayrollBeneficiaryUpdateRepository();
        $this->payrolls = new PayrollRepository();

    }


    /*Main wf module group id*/
    public function wfModuleGroupId()
    {
        return 15;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($member_type_id, $resource_id, $employee_id)
    {
        //
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        /*check if can initiate new entry*/
        $this->payroll_beneficiary_updates->checkIfCanInitiateNewEntry($member_type_id, $resource_id);
        $resource = $this->payrolls->getResource($member_type_id,$resource_id );
        $enrolled = $this->payrolls->checkIfResourceAlreadyEnrolled($member_type_id, $resource_id);
        $document_type = 65;
        $dependent_employee = ($member_type_id == 4) ?  $this->getDependentInput($resource_id,$employee_id)['dependent_employee'] : null;
        $isactive = ($member_type_id == 4) ? $dependent_employee->isactive : $resource->isactive;
        /*Location*/
        $regions = Region::query()->orderBy('name')->pluck('name', 'id');
        return view('backend/operation/payroll/pension_administration/beneficiary_updates/create/create')
            ->with(['member_type_id' => $member_type_id, 'resource' => $resource, 'enrolled' => $enrolled , 'document_type' => $document_type, 'dependent_employee' => $dependent_employee, 'employee_id' => $employee_id, 'isactive' => $isactive, 'regions' => $regions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BeneficiaryUpdateRequest $request)
    {
        //
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $input = $request->all();
        $wf_module_group_id = $this->wfModuleGroupId();
        $active = $this->payrolls->checkIfResourceAlreadyEnrolled($input['member_type_id'], $input['resource_id']);
        /*update document pending status*/
        $document_type = 65;
        $this->payrolls->updateBeneficiaryDocumentPendingStatusPerType($input['member_type_id'], $input['resource_id'], $document_type);
/*end document update*/

        if($active == true)
        {
            /*active*/
            return  $this->storeForActiveBeneficiary($wf_module_group_id, $input);
        }else{
            /*not active*/
            return   $this->storeForNotActiveBeneficiary($wf_module_group_id, $input);
        }


    }

    /*Store for the already active beneficiary*/
    public function storeForActiveBeneficiary($wf_module_group_id, $input){
        $beneficiary_update =  DB::transaction(function () use ($wf_module_group_id, $input) {
            /*create new update*/
            $beneficiary_update =  $this->payroll_beneficiary_updates->create($input);
            /*workflow start*/
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $beneficiary_update->id], [], ['comments' => $input['remark']]));
            /*end workflow*/

            return $beneficiary_update;
        });
        return redirect()->route('backend.payroll.beneficiary_update.profile',['beneficiary_update' => $beneficiary_update->id])->withFlashSuccess('Success, Beneficiary update entry has been created! Workflow has been initiated');
    }
    /*Store for not activated beneficiary - This does not need workflow*/
    public function storeForNotActiveBeneficiary($wf_module_group_id, $input){
        $beneficiary_update =  DB::transaction(function () use ($wf_module_group_id, $input) {
            /*create new update*/
            $resource =  $this->payroll_beneficiary_updates->updateWhenBeneficiaryIsNotActive($input);
            /*end workflow*/
            return $resource;
        });
        $url = $this->payrolls->returnResourceProfileUrl($input['member_type_id'], $input['resource_id'], $input['employee_id']);

        return redirect($url)->withFlashSuccess('Success, Beneficiary details have been updated');
    }


    public function profile(PayrollBeneficiaryUpdate $payrollBeneficiaryUpdate, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payrollBeneficiaryUpdate->id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        /*end workflow*/
        $resource_id = $payrollBeneficiaryUpdate->resource_id;
        $member_type_id = $payrollBeneficiaryUpdate->member_type_id;
        $employee_id = $payrollBeneficiaryUpdate->employee_id;
        $resource = $this->payrolls->getResource($member_type_id, $resource_id);
        $workflow_input = ['resource_id' => $payrollBeneficiaryUpdate->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => 0 ];
        $document_type = 65;
        $dependent_employee = ($member_type_id == 4) ?  $this->getDependentInput($resource_id,$employee_id)['dependent_employee'] : null;
        return view('backend/operation/payroll/pension_administration/beneficiary_updates/profile/profile')
            ->with(['resource' => $resource, 'member_type_id' => $member_type_id, 'beneficiary_update' => $payrollBeneficiaryUpdate,'check_pending_level1' => $check_pending_level1, "workflow_track" => $workflowTrackDataTable, 'workflow_input' => $workflow_input, 'unserialized_old_values' =>  $payrollBeneficiaryUpdate->unSerializedOldValues(), 'unserialized_new_values' =>  $payrollBeneficiaryUpdate->unSerializedNewValues(), 'document_type' => $document_type, 'dependent_employee' => $dependent_employee, 'employee_id' => $employee_id]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollBeneficiaryUpdate $payrollBeneficiaryUpdate, $employee_id)
    {
        //
        $this->checkLevelRights($payrollBeneficiaryUpdate->id, 1);
        $resource = $this->payrolls->getResource($payrollBeneficiaryUpdate->member_type_id, $payrollBeneficiaryUpdate->resource_id);
        $document_type = 65;
        /*Location*/
        $regions = Region::query()->orderBy('name')->pluck('name', 'id');
        return view('backend/operation/payroll/pension_administration/beneficiary_updates/edit/edit')->with([
            'beneficiary_update' => $payrollBeneficiaryUpdate,
            'resource' => $resource,
            'member_type_id' => $payrollBeneficiaryUpdate->member_type_id,
            'unserialized_new_values' =>  $payrollBeneficiaryUpdate->unSerializedNewValues(),
            'document_type' => $document_type,
            'employee_id' => $employee_id,
            'regions' => $regions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BeneficiaryUpdateRequest $request, PayrollBeneficiaryUpdate $payrollBeneficiaryUpdate)
    {
        //
        $input = $request->all();
        $this->payroll_beneficiary_updates->update($payrollBeneficiaryUpdate, $input);
        return redirect()->route('backend.payroll.beneficiary_update.profile',['beneficiary_update' => $payrollBeneficiaryUpdate->id, 'employee' => $input['employee_id']])->withFlashSuccess('Success, Beneficiary update entry has been modified');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayrollBeneficiaryUpdate $payrollBeneficiaryUpdate, $employee)
    {
        //
        $this->checkLevelRights($payrollBeneficiaryUpdate->id, 1);
        $this->payroll_beneficiary_updates->undo($payrollBeneficiaryUpdate);
        $url = $this->payrolls->returnResourceProfileUrl($payrollBeneficiaryUpdate->member_type_id,  $payrollBeneficiaryUpdate->resource_id, $employee);
        return redirect($url)->withFlashSuccess('Success, Beneficiary update entry has been undone');
    }


    public function getDependentInput($dependent_id, $employee_id)
    {
        $dependent = $this->dependents->find($dependent_id);
        $dependent_employee = $dependent->employees()->where('employee_id', $employee_id)->first();
        $employee = (new EmployeeRepository())->find($employee_id);
        return ['dependent_employee' => $dependent_employee->pivot, 'employee' => $employee];
    }


    /**
     * @param $resource_id
     * @param $notification_report_id
     *Get recoverires per beneficiary i.e. Pensioner/ Dependant
     */
    public function getBeneficiaryUpdatesForDataTable($member_type_id, $resource_id)
    {
        return Datatables::of($this->payroll_beneficiary_updates->getByBeneficiaryForDataTable($member_type_id, $resource_id)->orderBy('id', 'desc'))
            ->addColumn('created_at_formatted', function ($beneficiary_update) {
                return short_date_format($beneficiary_update->created_at);
            })
            ->addColumn('wf_done_date_formatted', function ($beneficiary_update) {
                return   ($beneficiary_update->wf_done_date)  ? short_date_format($beneficiary_update->wf_done_date) : ' ';
            })
            ->addColumn('user', function ($beneficiary_update) {
                return $beneficiary_update->user->username;
            })
            ->addColumn('status', function ($beneficiary_update) {
                return $beneficiary_update->status_label;
            })
            ->rawColumns(['status'])
            ->make(true);

    }




    /**
     * Check if can Initiate Action (Level ?/ No Workflow)
     */
    public function checkLevelRights($resource_id, $level)
    {
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowDefinition($wf_module_group_id, $level);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction($level);
    }
}
