<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\PayrollMpUpdateRequest;
use App\Models\Operation\Payroll\PayrollMpUpdate;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollMpUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollMpUpdateController extends Controller
{

    protected $pensioners;
    protected $dependents;
    protected $wf_modules;
    protected $payroll_mp_update_repo;
    protected $payroll_repo;

    public function __construct()
    {
        $this->payroll_repo = new PayrollRepository();
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->code_values = new CodeValueRepository();
        $this->wf_modules = new WfModuleRepository();
        $this->payroll_mp_update_repo = new PayrollMpUpdateRepository();


        //TODO: Apply permissions for the functions
    }


    /*Main wf module group id*/
    public function wfModuleGroupId()
    {
        return 24;
    }


    /*Create new entry Payroll Mp Update*/
    public function create($member_type_id, $resource_id, $employee_id)
    {
        $this->payroll_mp_update_repo->checkIfCanInitiateNewEntry($member_type_id, $resource_id,$employee_id);
        $resource = $this->payroll_repo->getResource($member_type_id,$resource_id );
        $mp = $this->payroll_repo->getMp($member_type_id, $resource_id, $employee_id);
        return view('backend/operation/payroll/pension_administration/mp_update/create/create')
            ->with('mp', $mp)
            ->with('member_type_id', $member_type_id)
            ->with('employee_id', $employee_id)
            ->with('resource', $resource);
    }

    /*Store payroll mp update*/
    public function store(PayrollMpUpdateRequest $request)
    {
        $input = $request->all();
        $wf_module_group_id = $this->wfModuleGroupId();
        /*check if can initiate new entry*/
        $this->payroll_mp_update_repo->checkIfCanInitiateNewEntry($input['member_type_id'], $input['resource_id'], $input['employee_id']);
        $payroll_mp_update =  DB::transaction(function () use ($wf_module_group_id, $input) {
            /*create mp update*/
            $payroll_mp_update =  $this->payroll_mp_update_repo->store($input);
            $type = $this->payroll_mp_update_repo->getWfModuleType();
            /*workflow start*/
            access()->hasWorkflowModuleDefinition($wf_module_group_id,$type, 1);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $payroll_mp_update->id, 'type' => $type ], [], ['comments' => $input['remark']]));
            /*end workflow*/
            return $payroll_mp_update;
        });
        return redirect()->route('backend.payroll.mp_update.profile',$payroll_mp_update->id)->withFlashSuccess('Success, Payroll Monthly Pension update has been created');
    }


    /*Profile*/
    public function profile(PayrollMpUpdate $payrollMpUpdate, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        //
        /* Check workflow */
        $type = $this->payroll_mp_update_repo->getWfModuleType();
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payrollMpUpdate->id, 'type' => $type ]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        /*end workflow*/
        $resource_id = $payrollMpUpdate->resource_id;
        $member_type_id = $payrollMpUpdate->member_type_id;
        $employee_id = $payrollMpUpdate->employee_id;
        $resource = $this->payroll_repo->getResource($member_type_id, $resource_id);
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
        $workflow_input = ['resource_id' => $payrollMpUpdate->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $type ];

        return view('backend/operation/payroll/pension_administration/mp_update/profile/profile')
            ->with(['resource' => $resource, 'member_type_id' => $member_type_id, 'payroll_mp_update' => $payrollMpUpdate,'check_pending_level1' => $check_pending_level1, "workflow_track" => $workflowTrackDataTable, 'workflow_input' => $workflow_input,'dependent_employee' => $dependent_employee, 'employee_id' => $employee_id]);
    }


    /**
     * Edit form for payroll mp update
     */
    public function edit(PayrollMpUpdate $payrollMpUpdate)
    {
        $type = $this->payroll_mp_update_repo->getWfModuleType();
//        $this->checkLevelRights($payrollMpUpdate->id, $type,1);
        $member_type_id = $payrollMpUpdate->member_type_id;
        $resource_id = $payrollMpUpdate->resource_id;
        $resource = $this->payroll_repo->getResource($member_type_id, $resource_id);
        return view('backend/operation/payroll/pension_administration/mp_update/edit/edit')
            ->with(['payroll_mp_update' => $payrollMpUpdate, 'resource' => $resource]);
    }



    /*Update*/
    public function update(PayrollMpUpdate $payrollMpUpdate, PayrollMpUpdateRequest $request)
    {
        $input = $request->all();
        $this->payroll_mp_update_repo->update($payrollMpUpdate, $input);
        return redirect()->route('backend.payroll.mp_update.profile',$payrollMpUpdate->id)->withFlashSuccess('Success, Payroll Monthly Pension update has been updated');
    }


    /*Undone*/
    public function undo(PayrollMpUpdate $payrollMpUpdate)
    {
        $return_url = $this->payroll_repo->returnResourceProfileUrl($payrollMpUpdate->member_type_id, $payrollMpUpdate->resource_id, $payrollMpUpdate->employee_id);
        $this->payroll_mp_update_repo->undo($payrollMpUpdate);
        return redirect($return_url)->withFlashSuccess('Success, Payroll Mp update has been undone');
    }


    /*Get mp updates by beneficiaries for DataTable*/
    public function getByBeneficiariesForDt($member_type_id, $resource_id, $employee_id)
    {
        return Datatables::of($this->payroll_mp_update_repo->getByBeneficiaryForDataTable($member_type_id, $resource_id, $employee_id)->orderBy('id', 'desc'))
            ->editColumn('old_mp', function ($mp_update) {
                return number_2_format($mp_update->old_mp);
            })
            ->editColumn('new_mp', function ($mp_update) {
                return  number_2_format($mp_update->new_mp);
            })
            ->addColumn('created_at_formatted', function ($mp_update) {
                return short_date_format($mp_update->created_at);
            })
            ->addColumn('wf_done_date_formatted', function ($mp_update) {
                return ($mp_update->wf_done_date)  ? short_date_format($mp_update->wf_done_date) : ' ';
            })
            ->addColumn('status', function ($mp_update) {
                return $mp_update->status_label;
            })
            ->addColumn('user', function ($mp_update) {
                return $mp_update->user->username;
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    /**
     * Check if can Initiate Action (Level ?/ No Workflow)
     */
    public function checkLevelRights($resource_id, $type, $level)
    {
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id,  $type, $level);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id, 'type' => $type]])->checkIfCanInitiateAction($level);
    }
}
