<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;


use App\DataTables\Report\Traits\ExcelExportTrait;
use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Backend\Operation\Payroll\Traits\PayrollControllerDtTrait;
use App\Http\Controllers\Backend\Operation\Payroll\Traits\PayrollExportCtrlTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\PayrollRunRequest;
use App\Http\Requests\Backend\Operation\Payroll\UpdateHoldStatusRequest;
use App\Http\Requests\Request;
use App\Jobs\Erp\BulkUpdateInvoicePaymentFromErp;
use App\Jobs\RunPayroll\PostEoffice\PostBeneficiaryToDms;
use App\Jobs\RunPayroll\PostEoffice\PostBulkBeneficiariesToDms;
use App\Jobs\RunPayroll\PostEoffice\PostPayrollToDms;
use App\Jobs\RunPayroll\RunPayroll;
use App\Jobs\RunPayroll\UndoPayrollRunApproval;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Dependent;
use App\Models\Operation\Payroll\Run\PayrollRunApproval;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\ManualPayrollMemberRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollAlertTaskRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunVoucherRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use App\Services\Payroll\ProcessPayroll;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;



class PayrollController extends Controller
{

    use AttachmentHandler, FileHandler, PayrollControllerDtTrait, PayrollExportCtrlTrait, ExcelExportTrait;

    protected $payroll_runs;
    protected $payment_vouchers;
    protected $payroll_procs;
    protected $payroll_repo;
    protected $payroll_run_approvals;
    protected $payroll_suspended_runs;
    protected $base = null;


    public function __construct() {
        $this->payroll_runs = new PayrollRunRepository();
        $this->payroll_procs = new PayrollProcRepository();
        $this->payroll_repo = new PayrollRepository();
        $this->payroll_run_approvals = new PayrollRunApprovalRepository();
        $this->payroll_suspended_runs = new PayrollSuspendedRunRepository();
//        $this->middleware('access.routeNeedsPermission:run_monthly_pension_payroll', ['only' => ['process', 'run']]);
        /* start : Initialise document store directory */
        $this->base = $this->real(payroll_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }


        //On Backend
        if(env('TESTING_MODE') == 1)
        {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }

    }


    /*Main wf module group id for payroll run approval*/
    public function wfModuleGroupId()
    {
        return 10;
    }


    /*Menu page*/
    public function index()
    {
        return view('backend/operation/payroll/menu');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Run payroll process
     */
    public function run(){
//        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $last_payroll_proc = $this->payroll_procs->getLastMonthlyPensionPayrollProc();
        $last_run_date = (isset($last_payroll_proc)) ? $last_payroll_proc->run_date : null;
        $next_run_date = (isset($last_run_date)) ? Carbon::parse($last_run_date)->addMonthsNoOverflow(1) : null;
        return view('backend.operation.payroll.run.process')
            ->with('next_run_date', $next_run_date);
    }

    /**
     * @param PayrollRunRequest $request
     * @return mixed
     * Process monthly pension payroll
     */
    public function process(PayrollRunRequest $request){
        $payroll_run_approval =  DB::transaction(function () use ($request) {
//            access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
            $input = $request->all();
            $payroll_run_approval = $this->payroll_repo->processPayroll($input);
            /*Dispatch payroll job*/
            dispatch(new RunPayroll($payroll_run_approval->id));
//            $process = new ProcessPayroll();
//            $process->processPayroll($payroll_run_approval->id);
            return $payroll_run_approval;
        });
        return redirect()->route('backend.payroll.pension.run_approval.profile',$payroll_run_approval->id)->withFlashSuccess(trans('alerts.backend.payroll.payroll_run'));
    }



    /*Open Payroll run approval Profile*/
    public function profileRunApproval(PayrollRunApproval $payrollRunApproval, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        $payroll_run_approval_id = $payrollRunApproval->id;
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->payroll_run_approvals->getWfType($payrollRunApproval);
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payroll_run_approval_id, 'type' => $type]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        $finance_payment_level =  ($check_workflow == 1) ?  $workflow->payrollFinancePaymentLevel() : 0;
        $current_level = $workflow->currentLevel();
        $current_wf_status = ($check_workflow == 1) ? $workflow->currentWfTrack()->status : 0;
        $check_finance_level = ($finance_payment_level == $current_level) ? 1 : 0;
        /*end workflow*/
        $previous_payroll_approval = $this->payroll_run_approvals->getPreviousPayrollApproval($payroll_run_approval_id);
        $data_summary = $this->payroll_runs->getPayrollRunApprovalDataSummary($payroll_run_approval_id);
        $workflow_input = ['resource_id' => $payroll_run_approval_id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $type ];
        $check_if_first_payroll = $this->payroll_run_approvals->checkIfIsTheFirstPayroll();
        return view('backend/operation/payroll/run/profile_new/profile')
            ->with(['payroll_run_approval' => $payrollRunApproval,'check_pending_level1' => $check_pending_level1, 'check_workflow' => $check_workflow,  "workflow_track" => $workflowTrackDataTable, 'workflow_input' =>  $workflow_input, 'data_summary' => $data_summary, 'previous_payroll_approval' => $previous_payroll_approval, 'check_finance_level' => $check_finance_level, 'check_if_first_payroll' => $check_if_first_payroll, 'current_wf_status' => $current_wf_status]);
    }


    /**
     * Initiate payroll run approval
     */
    public function initiateRunApproval(PayrollRunApproval $payrollRunApproval)
    {
        $input = request()->all();
        $type = $this->payroll_run_approvals->getWfType($payrollRunApproval);
        access()->hasWorkflowModuleDefinition($this->wfModuleGroupId(),$type, 1);
        $this->payroll_run_approvals->checkIfCanInitiateRunApproval($payrollRunApproval->id);
        $type = $this->payroll_run_approvals->getWfType($payrollRunApproval);
        /*workflow start*/
        $wf_module_group_id = $this->wfModuleGroupId();
        DB::transaction(function () use ($wf_module_group_id, $payrollRunApproval,$input, $type) {
            /*initiate*/
            $this->payroll_run_approvals->initiateRunApproval($payrollRunApproval->id);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $payrollRunApproval->id, 'type' => $type], [], ['comments' => $input['comments']]));

        });
        /*end workflow*/
        return redirect()->route('backend.payroll.pension.run_approval.profile',$payrollRunApproval->id)->withFlashSuccess('Success, Payroll approval has been initiated and workflows have been created');
    }



    /**
     * Undo payroll run approval
     */
    public function undoRunApproval(PayrollRunApproval $payrollRunApproval)
    {
        $type = $this->payroll_run_approvals->getWfType($payrollRunApproval);
        access()->hasWorkflowModuleDefinition($this->wfModuleGroupId(),$type, 1);
        DB::transaction(function () use ( $payrollRunApproval) {
            /*undo*/
            dispatch(new UndoPayrollRunApproval($payrollRunApproval->id));
        });
        /*end workflow*/
        return redirect()->route('backend.payroll.pension.run_approval.index')->withFlashSuccess('Success, Payroll approval has been Undone');
    }

    /*Show payroll run approvals*/
    public function runApprovalIndex()
    {
        return view('backend/operation/payroll/run/index');
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * Open beneficiary profile from url from payroll payments listing
     */
    public function openBeneficiaryProfile($member_type_id, $resource_id, $employee_id)
    {
        if($member_type_id == 5){
            /*pensioners*/
            return redirect()->route('backend.payroll.pensioner.profile',$resource_id);
        }elseif($member_type_id == 4){
            /*dependent*/
            $dependent_employee = DB::table('dependent_employee')->where('dependent_id', $resource_id)->where('employee_id', $employee_id)->first();
            return redirect()->route('backend.compliance.dependent.profile',$dependent_employee->id);
        }

    }


    /**
     * @param $payroll_run_approval_id
     * @return mixed
     * Process payment - generate voucher for banks
     */
    public function processPayment($payroll_run_approval_id)
    {
        $payroll_run_approval = $this->payroll_run_approvals->find($payroll_run_approval_id);
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payroll_run_approval_id]);
        $finance_payment_level = $workflow->payrollFinancePaymentLevel();
        $type = $this->payroll_run_approvals->getWfType($payroll_run_approval);
        access()->hasWorkflowModuleDefinition($this->wfModuleGroupId(),$type, $finance_payment_level);
        $this->payroll_run_approvals->processPayment($payroll_run_approval_id);
        return redirect()->route('backend.payroll.pension.run_approval.profile',$payroll_run_approval_id)->withFlashSuccess('Success, Payments have been initiated, execution will continue on the background. Process will take some few minutes.');
    }


    /**
     * @param $id
     * @return $this
     * @throws \App\Exceptions\GeneralException
     * Print payment voucher per bank in payroll
     */
    public function printPaymentVoucherPerBank($bank_id, $payroll_run_approval_id)
    {
        $payroll_run_approval = $this->payroll_run_approvals->find($payroll_run_approval_id);
        /*check if can initiate*/
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payroll_run_approval_id]);
        $finance_payment_level = $workflow->payrollFinancePaymentLevel();
        $type = $this->payroll_run_approvals->getWfType($payroll_run_approval);
        access()->hasWorkflowModuleDefinition($this->wfModuleGroupId(),$type, $finance_payment_level);

        /*end check*/
        $payment_run_vouchers = new PayrollRunVoucherRepository();

        $payment_voucher = $payroll_run_approval->payrollRunVouchers()->where('bank_id', $bank_id)->first();
        $payment_description = $payment_voucher->voucherDescription();
        $finance_officers = $payment_run_vouchers->getFinanceWfTrackForPvPerBenefit($payroll_run_approval_id);
        $bank = $payment_voucher->bank;
        return view("backend/finance/payment/includes/print_payment_voucher_pension")
            ->with(['payment_voucher' => $payment_voucher, 'bank' => $bank, 'payment_description' => $payment_description, 'payroll_run_approval' => $payroll_run_approval, 'finance_officers' => $finance_officers]);
    }



    /*Export Mp by member type for payroll run approval*/
    public function exportMpByMemberType($beneficiary_type,$payroll_run_approval_id){
        $this->payroll_run_approvals->exportMpByMemberType($beneficiary_type, $payroll_run_approval_id);
    }





    /**
     * Check if can Initiate Action (Level ?/ No Workflow)
     */
    public function checkLevelRights($resource_id, $level)
    {
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowDefinition($wf_module_group_id, $level);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction($level);
    }

    /**
     * @param $member_type
     * @param $resource
     * Post payroll file to e-office
     */
    public function postPayrollToDms($payroll_run_approval_id)
    {
        $payroll_run_approval = $this->payroll_run_approvals->find($payroll_run_approval_id);
        if($payroll_run_approval->isdmsposted == 0){
            dispatch(new PostPayrollToDms($payroll_run_approval));
        }
        return redirect()->route('backend.payroll.pension.run_approval.profile',$payroll_run_approval->id)->withFlashSuccess('Success, Payroll file has been posted to e-office');
    }



    /**
     * @param $member_type
     * @param $resource
     * Post beneficiary file to e-office
     */
    public function postBeneficiaryToDms($member_type_id, $resource_id, $employee_id)
    {
        $this->payroll_repo->postBeneficiaryToDms($member_type_id,$resource_id,$employee_id);
        $url = $this->payroll_repo->returnResourceProfileUrl($member_type_id,  $resource_id, $employee_id);
        return redirect($url)->withFlashSuccess('Success, File has been posted to E-office');
    }

    public function repostBeneficiaryToDms($member_type_id, $resource_id, $employee_id)
    {
        $this->payroll_repo->repostBeneficiaryToDms($member_type_id,$resource_id,$employee_id);
        $url = $this->payroll_repo->returnResourceProfileUrl($member_type_id,  $resource_id, $employee_id);
        return redirect($url)->withFlashSuccess('Success, File has been posted to E-office');
    }

    /**
     * @param $member_type
     * @param $resource
     * Post beneficiary files to e-office - All pending dependents and pensioners
     */
    public function postBulkBeneficiariesToDms()
    {
        $this->payroll_repo->postBulkBeneficiariesToDms();
        return redirect()->route('backend.payroll.data_migration.menu')->withFlashSuccess('Success, Files are being posted to E-office');
    }


    /**
     * @param $member_type
     * @param $resource
     * Update payroll beneficiary document from e-office
     */
    public function updateBeneficiaryDocument($member_type, $resource, Request $request)
    {
        $input = $request->all();

        /**
         * e-office integration, if source in the request is 2 then the request come from e-office and should return an acknowledgement.
         *
         */
        $beneficiary = $this->payroll_repo->getResource($member_type, $resource);
        $this->payroll_repo->updateBeneficiaryDocument($beneficiary, $member_type, $input);
        $beneficiary_dir = ($member_type == 5) ? 'pensioner' : 'dependent';
        $this->base = $this->base .  DIRECTORY_SEPARATOR . $beneficiary_dir . DIRECTORY_SEPARATOR . $beneficiary->id;
        $this->makeDirectory($this->base);
        if ($input['source'] == 2) {
            //Request came from e-office system, respond to e-office
            return response()->json(['message' => "SUCCESS"]);
        }


        /*Save Payroll Alert Task - Inbox <payroll_inbox_task>*/
//        (new PayrollAlertTaskRepository()->)

    }




    /**
     * @param NotificationReport $notificationReport
     * @param $pivotDocumentId
     * @return mixed
     * Open Document from e-office using e-Office document id
     */
    public function openBeneficiaryDocumentDocumentDms($pivotDocumentId)
    {
        /**
         * e-office Integration, open notification document from e-office system.
         */
        /*new--*/

        $path = $this->getPathFromDms($pivotDocumentId);
        //$path = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        return response()->file($path);
    }



    public function getPathFromDms($pivotDocumentId)
    {
        $base64pdf = $this->getBase64BeneficiaryEofficeDocument($pivotDocumentId);
        $base64decoded = base64_decode($base64pdf);
        $document = time() . $pivotDocumentId . '.pdf';
        $file = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        file_put_contents($file, $base64decoded);
        $path = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        return $path;
    }

    /**
     * @param $incident NotificationReport id
     * @param $documentType
     * @param $eOfficeDocumentId
     * @return mixed
     */
    public function getBase64BeneficiaryEofficeDocument($pivotDocumentId)
    {
        $document_pivot = DB::table('document_payroll_beneficiary')->where('id', $pivotDocumentId)->first();
        $documentType = $document_pivot->document_id;
        $eOfficeDocumentId = $document_pivot->eoffice_document_id;

        $client = new Client();
        $link = env('EOFFICE_APP_URL');
        $fileNumber = $document_pivot->resource_id;
        //TODO: Get url from e-office to pull document beneficiary file
        $guzzlerequest = $client->get($link . "/api/claim/file/document?fileNumber={$fileNumber}&documentType={$documentType}&documentId={$eOfficeDocumentId}", [
            'auth' => [
                env("EOFFICE_AUTH_USER"),
                env("EOFFICE_AUTH_PASS"),
            ],
        ]);
        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        $base64pdf = $parsed_json['document'];

        return $base64pdf;
    }




    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Http\JsonResponse
     * Display all documents of beneficiary on document center
     *
     */
    public function showAllBeneficiaryDocumentsTree($member_type_id, $resource_id)
    {
        $beneficiary = $this->payroll_repo->getResource($member_type_id, $resource_id);
        $this->base =  $beneficiary->directory;
        $this->makeDirectory($this->base);
        $this->id_no = $resource_id;
//        $this->notificationReportFolder = $beneficiary->pluck('id', 'id')->all();
        //$this->documents = $this->notification_reports->getProgressiveDocumentList($incident);
        $this->documents = $beneficiary->documents->pluck('name', 'id')->all();
        $uploadedDocuments = [];
        $documents = $beneficiary->documents()->orderBy('document_payroll_beneficiary.doc_receive_date', 'desc')->get();
        foreach ($documents as $document) {
            $uploadedDocuments += [$document->pivot->id => ($document->pivot->description . ': (Folio No. ' .$document->pivot->folio .') - '  . short_date_format( $document->pivot->doc_receive_date)) ];
        }

        $this->uploadedDocuments = $uploadedDocuments;


        $this->createTempDocumentFilesTree($beneficiary,$beneficiary->documents, $this->base);
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_general($node, (isset(request()->id) && request()->id === '#'));
//        Log::info(print_r($rslt,true));
        return response()->json($rslt);
    }



    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Http\JsonResponse
     * @deprecated
     * @description Not Used.
     */
    public function showValidatedBeneficiaryDocumentTreePerType($member_type_id, $resource_id, $document_type)
    {
        $beneficiary = $this->payroll_repo->getResource($member_type_id, $resource_id);
        $this->base =  $beneficiary->directory;
        $this->makeDirectory($this->base);
        $this->id_no = $resource_id;
//        $this->notificationReportFolder = $beneficiary->pluck('id', 'id')->all();
        //$this->documents = $this->notification_reports->getProgressiveDocumentList($incident);
        $this->documents = (new DocumentRepository())->query()->select(["name", "id"])->where("id",$document_type)->pluck('name', 'id')->all();
        $uploadedDocuments = [];

        $documents = $this->payroll_repo->getBeneficiaryDocumentValidatedPerType($member_type_id, $resource_id, $document_type);
        foreach ($documents as $document) {
            $uploadedDocuments += [$document->pivot->id => ($document->pivot->description . ': (Folio No. ' .$document->pivot->folio .') - '  . short_date_format( $document->pivot->doc_receive_date)) ];
        }

        $this->uploadedDocuments = $uploadedDocuments;


        $this->createTempDocumentFilesTree($beneficiary,$beneficiary->documents, $this->base);
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_general($node, (isset(request()->id) && request()->id === '#'));
//        Log::info(print_r($rslt,true));
        return response()->json($rslt);
    }


    /**
     * @param Model $notificationReport
     * @param $base
     */
    public function createTempDocumentFilesTree(Model $beneficiary, $documents, $base)
    {

//        if(env('SELF_DMS') == 0)

        foreach($documents as $document)
        {
            $document_id = $document->pivot->document_id;
            $source = $document->pivot->name;
            $path = $base . DIRECTORY_SEPARATOR . $document_id;
            $this->makeDirectory($path);
            if ($source == 'e-office')
            {
                /*Unlink all files on this folder - Of E-office Only*/
                $this->deleteAllFilesOnFolder($path);
                /*end unlink*/
                $uploaded_docs = $beneficiary->documents()->where('document_id', $document_id)->get();
                foreach($uploaded_docs as $uploadedDocument){
                    $document_pivot_id = $uploadedDocument->pivot->id;
                    $fp = fopen($path . '/'. $document_pivot_id  .".pdf","wb");
                    fwrite($fp,'Open from E-Office');
                    fclose($fp);
                }
                /*end creating temp*/
            }


        }
//        }
    }



    /**
     * @param NotificationReport $incident
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentBase64DocumentOld($member_type_id, $resource_id, $document_pivot_id)
    {
        $beneficiary = $this->payroll_repo->getResource($member_type_id, $resource_id);
        $document = $beneficiary->documents()->where('document_payroll_beneficiary.id', $document_pivot_id)->first();
        $pivot = $document->pivot;
        $documentType = $pivot->document_id;
        $eOfficeDocumentId = $pivot->eoffice_document_id;
        $base64doc = $this->getBase64EofficeDocument($resource_id, $documentType, $eOfficeDocumentId);
        $base64decoded = base64_decode($base64doc);
        $document = $pivot->id . '.pdf';
        $path = $beneficiary->directory . DIRECTORY_SEPARATOR . $documentType;
        $this->makeDirectory($path);
        $file = $path .  DIRECTORY_SEPARATOR . $document;
        file_put_contents($file, $base64decoded);
        if (test_uri()) {
            $url = url("/") . "/ViewerJS/#../storage/eoffice/" . $document;
        } else {
            $url = url("/") ."/public/ViewerJS/#../storage/eoffice/" . $document;
        }
        //$url = eoffice_path() . DIRECTORY_SEPARATOR . $document;
        $url = $beneficiary->url . DIRECTORY_SEPARATOR . $documentType . DIRECTORY_SEPARATOR . $document;
        return response()->json(['success' => true, "url" => $url, "name" => $pivot->description, "id" => $document_pivot_id]);
    }


    /*Current Base 64 document*/
    public function currentBase64Document($member_type_id, $resource_id, $document_pivot_id)
    {
        $beneficiary = $this->payroll_repo->getResource($member_type_id, $resource_id);
        $document = $beneficiary->documents()->where('document_payroll_beneficiary.id', $document_pivot_id)->first();
        $pivot = $document->pivot;
        $documentType = $pivot->document_id;
        $eOfficeDocumentId = $pivot->eoffice_document_id;
        $url = $this->previewDocumentFromEofficeGeneral($resource_id, $documentType, $eOfficeDocumentId);
        return response()->json(['success' => true, "url" => $url, "name" => $pivot->description, "id" => $document_pivot_id]);
    }

    /**
     * @param $incident NotificationReport id
     * @param $documentType
     * @param $eOfficeDocumentId
     * @return mixed
     */
    public function getBase64EofficeDocument($fileNumber, $documentType, $eOfficeDocumentId)
    {
        if (!env("TEST_DOCUMENT", 0)) {
            $client = new Client();
            $link = env('EOFFICE_APP_URL');
            $guzzlerequest = $client->get($link . "/api/claim/file/document?fileNumber={$fileNumber}&documentType={$documentType}&documentId={$eOfficeDocumentId}", [
                'auth' => [
                    env("EOFFICE_AUTH_USER"),
                    env("EOFFICE_AUTH_PASS"),
                ],
            ]);
            $response = $guzzlerequest->getBody()->getContents();
            $parsed_json = json_decode($response, true);
            $base64pdf = $parsed_json['document'];
        } else {
            $base64pdf = $this->getSampleBase64Pdf();
        }
        return $base64pdf;
    }


    /**
     * Open Data Migration menu
     */
    public function openDataMigrationMenu()
    {
        $notification_pending = (new ManualNotificationReportRepository())->getPendingSummary();
        $manual_member_pending = (new ManualPayrollMemberRepository())->getPendingSummary();
        $pending_system_survivor =(new DependentRepository())->getPendingManualMergeSummary();
        $pending_system_pensioner =(new PensionerRepository())->getPendingManualMergeSummary();
        return view('backend/operation/payroll/data_migration/menu')
            ->with('notification_pending', $notification_pending)
            ->with('manual_member_pending', $manual_member_pending)
            ->with('pending_system_survivor', $pending_system_survivor)
            ->with('pending_system_pensioner', $pending_system_pensioner);
    }



    /**
     * Get redirect routes depend on pending document type selected
     */
    public function getPendingDocsForBeneficiaryAction($member_type_id, $resource_id, $document_type)
    {
        return $this->payroll_repo->getPendingDocsForBeneficiaryAction($member_type_id, $resource_id, $document_type);
    }


    /**
     * Update use status of docs for beneficiaries
     */
    public function updateBeneficiaryDocUseStatus($doc_id, $status){
        $this->payroll_repo->updateBeneficiaryDocUseStatus($doc_id, $status);
        return redirect()->back();
    }


    public function updateBeneficiaryDocUsePendingStatus($doc_id, $use_status, $pending_status){
        $this->payroll_repo->updateBeneficiaryDocUsePendingStatus($doc_id, $use_status, $pending_status);
        return redirect()->back();
    }



    /*Start: -----Hold Beneficiary-----------*/
    /**
     * Open beneficiary hold page to enter hold reason
     * @param $member_type_id, $resource_id, $employee_id
     * @param $status i.e. 1 => hold , 2 => release hold
     */
    public function openHoldBeneficiaryPage($member_type_id, $resource_id, $employee_id)
    {
        /*Status change wf group*/
        access()->hasWorkflowDefinition(12, 1);
        $resource = $this->payroll_repo->getResource($member_type_id, $resource_id);
        //hold
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
        $isactive = ($member_type_id == 4) ? $dependent_employee->isactive : $resource->isactive;
        $hold_reason = ($member_type_id == 4) ? $dependent_employee->hold_reason : $resource->hold_reason;
        return view('backend/operation/payroll/pension_administration/hold/hold_page')
            ->with(['member_type_id' => $member_type_id, 'dependent_employee' => $dependent_employee, 'resource' => $resource, 'employee_id' => $employee_id, 'hold_reason' => $hold_reason , 'isactive' => $isactive]);

    }

    /*Hold beneficiary*/
    public function updateHoldBeneficiary(UpdateHoldStatusRequest $request)
    {
        /*Status change wf group*/
        access()->hasWorkflowDefinition(12, 1);
        $input = $request->all();
        $member_type_id = $input['member_type_id'];
        $resource_id = $input['resource_id'];
        $employee_id = $input['employee_id'];
        $this->payroll_repo->holdBeneficiary( $member_type_id, $resource_id, $employee_id, $input);
        $return_profile_url = $this->payroll_repo->returnResourceProfileUrl($member_type_id, $resource_id, $employee_id);
        return redirect($return_profile_url)->withFlashSuccess('Success, Beneficiary is on hold');
    }

    /*Release hold beneficiary*/
    public function releaseHoldBeneficiary($member_type_id, $resource_id, $employee_id)
    {
        /*Status change wf group*/
        access()->hasWorkflowDefinition(12, 1);
        $this->payroll_repo->releaseHoldBeneficiary( $member_type_id, $resource_id, $employee_id);
        $return_profile_url = $this->payroll_repo->returnResourceProfileUrl($member_type_id, $resource_id, $employee_id);
        return redirect($return_profile_url)->withFlashSuccess('Success, Beneficiary is released from hold');
    }


    /*--End-- hold beneficiary---*/


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * Reason for removing suspended payments
     */
    public function reasonForRemovingSuspendedPayments($member_type_id, $resource_id, $employee_id)
    {
        access()->hasWorkflowDefinition(12, 1);
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
        return view('backend/operation/payroll/pension_administration/suspended_payment/remove_payments')
            ->with(['member_type_id' => $member_type_id, 'resource' => $resource, 'employee_id' => $employee_id, 'dependent_employee' => $dependent_employee]);
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     */
    public function removeSuspendedPayments()
    {
        $input = request()->all();
        $input['deleted_by'] = access()->id();
        (new PayrollSuspendedRunRepository())->removeSuspendedPayments($input);
        $return_profile_url = $this->payroll_repo->returnResourceProfileUrl($input['member_type_id'], $input['resource_id'], $input['employee_id']);
        return redirect($return_profile_url)->withFlashSuccess('Success, Pending suspended payments removed');
    }


    /**
     * Manage document usages status for approval i.e. verification, bank docs, info updates doc
     *
     * @return \Illuminate\Http\Response
     */
    public function manageDocumentUsageStatusForApproval($member_type_id, $resource_id, $employee_id)
    {
        //
        $input = request()->all();
        access()->hasWorkflowDefinition(12, 1);
        $resource = $this->payroll_repo->getResource($member_type_id, $resource_id);
        $dependent_employee = ($member_type_id == 4) ? $resource->getDependentEmployee($employee_id) : null;
        $doc_ids_arr = [63,64,65];
$document_types = Document::query()->whereIn('id',$doc_ids_arr)->pluck('name', 'id');
        if(isset($input['document_id']))
        {
            $documents = $resource->documents()->where('document_payroll_beneficiary.document_id', $input['document_id'])->orderByDesc('document_payroll_beneficiary.doc_receive_date')->get();
        }else{
            $documents = $resource->documents()->orderByDesc('document_payroll_beneficiary.doc_receive_date')->whereIn('document_id', $doc_ids_arr)->limit(10)->get();
        }
        return view('backend/operation/payroll/pension_administration/profile/includes/documents_attached/documents_attached_status')
            ->with(['resource' => $resource, 'member_type_id' => $member_type_id,  'employee_id' => $employee_id, 'document_id' =>( $input['document_id'] ?? null),
                'documents' => $documents, 'document_types' => $document_types]);

    }



}
