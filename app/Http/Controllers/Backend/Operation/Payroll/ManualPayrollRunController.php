<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\Http\Controllers\Controller;
use App\Models\Operation\Claim\IncidentType;
use App\Models\Operation\Claim\MemberType;
use App\Repositories\Backend\Operation\Payroll\ManualPayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class ManualPayrollRunController extends Controller
{
    //

    protected $manual_payroll_run_repo;

    public function __construct()
    {
        $this->manual_payroll_run_repo = new ManualPayrollRunRepository();

        $this->middleware('access.routeNeedsPermission:manage_payroll_data_migration', ['only' => ['uploadPage', 'storeUploadManualPayrollRuns', 'openSyncPage', 'syncManualPensionsToMember']]);
    }


    /*open upload page*/
    public function uploadPage($member_type_id)
    {
        $no_of_uploaded_files = $this->manual_payroll_run_repo->getForDataTable()->where('member_type_id', $member_type_id)->count();
        $file_url = $this->manual_payroll_run_repo->uploadedFileUrl();
        return view('backend/operation/payroll/pension_administration/manual_payroll_run/upload_bulk/upload_page')
            ->with('no_of_uploaded_files', $no_of_uploaded_files)
            ->with('uploaded_file_url', $file_url)
            ->with('member_type_id', $member_type_id);
    }


    /*Store upload manual payroll runs*/
    public function storeUploadManualPayrollRuns()
    {
        $input = request()->input();
        $member_type_id = $input['member_type_id'];
        $this->manual_payroll_run_repo->saveUploadedDocumentFromExcel($input);
        return redirect()->route('backend.payroll.manual_payroll_run.upload_page',$member_type_id)->withFlashSuccess('Success, document has been uploaded');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Open Sync page
     */
    public function openSyncPage()
    {
        $member_types = MemberType::query()->whereIn('id', [4,5])->pluck('name', 'id');
        $incident_types = IncidentType::query()->whereIn('id', [1,2,3])->pluck('name', 'id');
        $member_sync_details = $this->manual_payroll_run_repo->getSyncMemberSummaryDetails();
        return view('backend/operation/payroll/pension_administration/manual_payroll_run/sync_with_existing_member/sync_page')
            ->with('member_types', $member_types)
            ->with('incident_types', $incident_types)
            ->with('member_sync_details', $member_sync_details);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     * Sync Manual Pensions to member
     */
    public function syncManualPensionsToMember()
    {
        $input = request()->all();
        $this->manual_payroll_run_repo->syncManualPayrollRunsWithMember($input);
        return response()->json(['success' => true, 'message' => 'Success, Manual payroll runs has been synced']);
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $employee_id
     * @return mixed
     * Get manual pensions for beneficiary
     */
    public function getManualPensionByBeneficiaryDt($member_type_id, $resource_id, $employee_id)
    {
        $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
            $mp_system =$resource->getMpAmountAttribute($employee_id);
        $result_list = $this->manual_payroll_run_repo->getManualPayrollRunsByBeneficiaryDt($member_type_id, $resource_id, $employee_id)->orderBy('manual_payroll_runs.payroll_month','asc');
        return Datatables::of($result_list)
            ->editColumn('payroll_month', function ($query) {
                return  short_date_format($query->payroll_month);
            })
            ->editColumn('monthly_pension', function ($query) {
                return  number_2_format($query->monthly_pension);
            })
            ->addColumn('mp_system', function ($query) use($mp_system){
                return  number_2_format($mp_system);
            })
            ->rawColumns(['monthly_pension', 'payroll_month', ''])
            ->make(true);
    }

    /**
     * @return mixed
     * Get beneficiary from manual payroll dt
     */
    public function getBeneficiaryFromManualPayrollDt()
    {

        return Datatables::of($this->manual_payroll_run_repo->getPayrollBeneficiariesFromManualDt())
            ->editColumn('member_type_id', function ($query) {
                return  (($query->member_type_id == 5) ?  'Pensioner' : 'Dependent' ) ;
            })
            ->addColumn('manual_payroll_runs_synced_count', function ($query) {
                $manual_payroll_runs_synced = $this->manual_payroll_run_repo->getManualPayrollRunsByBeneficiaryDt($query->member_type_id,$query->resource_id,$query->employee_id)->count();
                $manual_payroll_runs_synced_label = ($manual_payroll_runs_synced > 0) ? ('Synced - ' . $manual_payroll_runs_synced) : 'Not Synced';
                $manual_payroll_runs_synced_label = ($manual_payroll_runs_synced > 0) ? "<span class='tag tag-success'>". $manual_payroll_runs_synced_label . "</span>" : "<span class='tag tag-warning'>". $manual_payroll_runs_synced_label . "</span>";
                return  $manual_payroll_runs_synced . '<br/>' . $manual_payroll_runs_synced_label ;
            })
            ->editColumn('member_name', function ($query) {
                return  remove_extra_white_spaces($query->member_name);
            })

            ->rawColumns(['member_type_id', 'manual_payroll_runs_synced_count'])
            ->make(true);
    }


    /**
     * Get Manual payroll runs by request for dt
     */
    public function getManualPayrollRunsByRequestForDt()
    {
        $input = request()->all();
        return Datatables::of($this->manual_payroll_run_repo->getManualPayrollRunsByRequestForDt($input)->orderBy('manual_payroll_runs.payee_name','asc'))
            ->editColumn('payroll_month', function ($query) {
                return  short_date_format($query->payroll_month);
            })
            ->editColumn('monthly_pension', function ($query) {
                return  number_2_format($query->monthly_pension);
            })

            ->editColumn('member_type_id', function ($query) {

                return  ($query->memberType->name) ;
            })
            ->editColumn('status', function ($query) {
                return ($query->resource_id && $query->employee_id) ? 'Synced' : 'Not Synced';
            })
            ->rawColumns(['monthly_pension', 'payroll_month', ''])
            ->make(true);
    }


    /**
     * @param $member_type_id
     * @return mixed
     * Get member type for dt
     */
    public function getByMemberTypeForDt($member_type_id)
    {
        return Datatables::of($this->manual_payroll_run_repo->getForDataTable()->where('member_type_id', $member_type_id)->orderBy('upload_error','desc')->orderBy('resource_id','asc'))
            ->editColumn('payroll_month', function ($query) {
                return  short_date_format($query->payroll_month);
            })
            ->editColumn('monthly_pension', function ($query) {
                return  number_2_format($query->monthly_pension);
            })
            ->editColumn('member_type_id', function ($query) {
                return  ($query->memberType->name);
            })
            ->editColumn('status', function ($query) {
                return ($query->resource_id && $query->employee_id) ? 'Synced' : 'Not Synced';
            })
            ->rawColumns(['monthly_pension', 'payroll_month', ''])
            ->make(true);
    }
}
