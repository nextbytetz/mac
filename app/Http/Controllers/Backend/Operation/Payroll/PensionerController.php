<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\MergeManualPensionerRequest;
use App\Models\Operation\Payroll\Pensioner;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class PensionerController extends Controller
{


    protected $pensioners;


    public function __construct() {

        $this->pensioners = new PensionerRepository();
        $this->middleware('access.routeNeedsPermission:manage_payroll_data_migration',['only' => ['mergeManualPensioner', 'updateManualPayStatus']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend/operation/payroll/pension_administration/index/pensioners');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }

    /**
     * @param $id
     * Pensioner profile/dashboard
     */
    public function profile(Pensioner $pensioner)
    {
        $payroll_runs = new PayrollRunRepository();
        $mp_payment_summary = null;
        $check_if_is_paid = $payroll_runs->checkIfBeneficiaryIsPaidAtLeastOnce(5, $pensioner->id,$pensioner->employee_id);
        $mp_payment_summary = ($pensioner->firstpay_flag == 1 && isset($pensioner->notification_report_id) && $check_if_is_paid == true)   ?  $payroll_runs->getMpPaymentSummaryForMember(5, $pensioner->id, $pensioner->employee_id) : null;
        $last_payroll_run_date = ($check_if_is_paid == true) ? $payroll_runs->getMostRecentApprovedRunDate() : null;
        $check_if_has_doc = (new PayrollRepository())->checkIfBeneficiaryHasDocument(5, $pensioner->id);
        return view('backend/operation/payroll/pension_administration/profile/pensioner/pensioner_profile')
            ->with('pensioner', $pensioner)
            ->with('mp_payment_summary', $mp_payment_summary)
            ->with('last_payroll_run_date', $last_payroll_run_date)
            ->with('check_if_is_paid', $check_if_is_paid)
            ->with('check_if_has_doc', $check_if_has_doc);

    }


    /**
     * Get All Pensioners for DataTable already enrolled in payroll
     */
    public function getPensionersForDataTable()
    {
        return Datatables::of($this->pensioners->getEnrolledPensionersForDataTable())
            ->editColumn('mp', function ($pensioner) {
                return number_2_format($pensioner->mp);
            })
            ->editColumn('filename', function ($pensioner) {
                return isset($pensioner->filename) ? $pensioner->filename : $pensioner->case_no;
            })
            ->addColumn('status', function ($pensioner) {
                $suspense_status = ($pensioner->suspense_flag == 1) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Suspended'. "'>" . 'Suspended'. "</span>" : '';
                $active_status = ($pensioner->isactive == 1) ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active'. "'>" . 'Active'. "</span>" :
                    ( ($pensioner->isactive == 2) ? "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Deactivated'. "'>" . 'Deactivated'. "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Inactive'. "'>" . 'Inactive'. "</span>" ) ;
                return  $active_status .  ($suspense_status == null ? '': (' : '. $suspense_status));;
            })
            ->rawColumns(['status'])
            ->make(true);

    }



    /**
     * Get All Pensioners Ready / pending for payroll for DataTable
     */
    public function getReadyForPayrollDataTable()
    {
        return Datatables::of($this->pensioners->getReadyForPayrollForDataTable())
            ->editColumn('mp', function ($pensioner) {
                return number_2_format($pensioner->mp);
            })
            ->editColumn('filename', function ($pensioner) {
                return isset($pensioner->filename) ? $pensioner->filename : $pensioner->case_no;
            })
            ->make(true);

    }



    /**
     * Get All Active in payroll Pensioners for DataTable
     */
    public function getActivePensionersForDataTable()
    {
        return Datatables::of($this->pensioners->getActiveForDataTable())
            ->editColumn('mp', function ($pensioner) {
                return number_2_format($pensioner->mp);
            })
            ->editColumn('filename', function ($pensioner) {
                return isset($pensioner->filename) ? $pensioner->filename : $pensioner->case_no;
            })
            ->make(true);

    }

    public function getRegisteredPensioners()
    {
        return $this->pensioners->getRegisteredPensioners(request()->input('q'));
    }

    /*Get pensioners for select*/
    public function getPensionersForSelect()
    {
        return $this->pensioners->getPensionersForSelect(request()->input('q'), request()->input('page'));
    }



    /**
     * Open page for merging pensioner exist on the system but processed manually for payroll
     */
    public function pensionerReadyForMergeManual()
    {
        return view('backend/operation/payroll/data_migration/merge_manual/pensioner/index');
    }


    /*Merge pensioner who were paid manually*/
    public function mergeManualPensionerPage($id)
    {
        $pensioner = $this->pensioners->find($id);
        return view('backend/operation/payroll/data_migration/merge_manual/pensioner/merge')
            ->with([
                'pensioner' => $pensioner,
                'banks' => (new BankRepository())->getAll()->pluck('name', 'id'),
                'bank_branches' => (new BankBranchRepository())->getAll()->pluck('name', 'id'),
            ]);

    }

    /*Merge pensioner exist on the system but was paid manually payroll*/
    public function mergeManualPensioner($id, MergeManualPensionerRequest $request)
    {
        $input = $request->all();
        $pensioner = $this->pensioners->find($id);
        $this->pensioners->mergeManualPensioner($id, $input);
        $hasarrears = isset($input['hasarrears']) ? $input['hasarrears'] : 0;
        if($hasarrears == 1){
            /*Has arrears*/
            return redirect()->route('backend.payroll.recovery.create_manual_arrears_system_members', ['member_type_id' => 5, 'resource_id' => $id, 'employee_id' => $pensioner->employee_id, 'arrears_months' => $input['pending_pay_months']])->withFlashSuccess('Success, Pensioner has been merged and enrolled to payroll! Proceed to initiate approval of arrears');
        }else{
            /*no arrears*/
            return redirect()->route('backend.payroll.pensioner.pensioners_ready_for_merge_manual')->withFlashSuccess('Success, Pensioner has been merged and enrolled to payroll');
        }

    }





    /*Get pensioners ready for payroll (Pending for activation and already paid manually out of system) for datatable*/
    public function getManualMergeReadyForPayrollForDataTable()
    {
        return Datatables::of($this->pensioners->getReadyForPayrollForManualMergeForDataTable()->orderBy('pensioners.firstpay_manual','desc')->orderBy('pensioners.firstname'))
            ->editColumn('mp', function ($pensioner) {
                return number_2_format($pensioner->mp);
            })
            ->addColumn('status', function ($pensioner) {
                $paid_manual = ($pensioner->firstpay_manual == 1  && $pensioner->isactive == 1) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid Manual - Active'. "'>" . 'Paid Manual - Active'. "</span>" : '';
                $paid_manual_terminated = ($pensioner->firstpay_manual == 1 && $pensioner->isactive == 2) ? "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . 'Paid Manual - Terminated'. "'>" . 'Paid Manual - Terminated'. "</span>" : '';
                $not_yet_paid = ($pensioner->firstpay_manual == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not yet paid'. "'>" . 'Not yet paid'. "</span>" : '';
                $pending = ($pensioner->firstpay_manual == 2) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>" : '';
//                $merge_status = ($dependent->firstpay_manual <> 2) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Synced'. "'>" . 'Synced'. "</span>" : "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>" ;

                return  $paid_manual . $paid_manual_terminated . $not_yet_paid . $pending;
            })
            ->addColumn('action', function ($pensioner) {
                $already_paid_activate = '<a href="' .route('backend.payroll.pensioner.merge_manual_pensioner_page',['pensioner' =>  $pensioner->id])  . '" class="btn btn-xs btn-success dishonour_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Already paid - Activate' . '"></i> Already Paid - Activate </a> ';
                $already_paid_terminate = '';
//                $already_paid_terminate = '<a href="' . route('backend.payroll.pensioner.update_manualpay_status',['dependent' =>  $pensioner->id, 'employee' =>  $pensioner->employee_id, 'status' => 2]) . '" class="btn btn-xs btn-warning warning_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Already paid - Terminate' . '"></i> Already Paid - Terminate </a> ';
                $not_yet_paid = '<a href="' . route('backend.payroll.pensioner.update_manualpay_status',['dependent' =>  $pensioner->id, 'employee' =>  $pensioner->employee_id, 'status' => 0]) . '" class="btn btn-xs btn-primary save_button " ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Not yet paid' . '"></i> Not yet paid </a> ';

                $merge_button = ($pensioner->firstpay_manual == 2) ? ($already_paid_activate . '<br/>' . $already_paid_terminate . '<br/>' . $not_yet_paid) : '' ;


                return  $merge_button;

            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }


    /*Open already merged pensioners for payroll*/
    public function alreadyMergedManualPensionersPage()
    {
        return view('backend/operation/payroll/data_migration/merge_manual/pensioner/already_merged_for_payroll_datatable');
    }



    /*Get pensioners ready for payroll ( Who are already merged - (Pensioners migration with manual payroll process) for datatable*/
    public function getManualAlreadyMergedForPayrollForDataTable()
    {
        return Datatables::of($this->pensioners->getForDataTable()->where('firstpay_manual','<>', 2)->orderBy('pensioners.firstpay_manual','desc')->orderBy('pensioners.firstname'))
            ->editColumn('mp', function ($pensioner) {
                return number_2_format($pensioner->mp);
            })
            ->addColumn('status', function ($pensioner) {
                $paid_manual = ($pensioner->firstpay_manual == 1  && $pensioner->isactive == 1) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid Manual - Active'. "'>" . 'Paid Manual - Active'. "</span>" : '';
                $paid_manual_terminated = ($pensioner->firstpay_manual == 1 && $pensioner->isactive == 2) ? "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . 'Paid Manual - Terminated'. "'>" . 'Paid Manual - Terminated'. "</span>" : '';
                $not_yet_paid = ($pensioner->firstpay_manual == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not yet paid'. "'>" . 'Not yet paid'. "</span>" : '';

//
                return  $paid_manual . $paid_manual_terminated . $not_yet_paid ;
            })
            ->addColumn('action', function ($pensioner) {
                $reset = '<a href="' . route('backend.payroll.pensioner.update_manualpay_status',['dependent' =>  $pensioner->id, 'employee' =>  $pensioner->employee_id, 'status' => 3]) . '" class="btn btn-xs btn-primary save_button " ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Reset' . '"></i> Reset </a> ';

                $reset_button = ($pensioner->firstpay_manual <> 2) ? $reset  : '' ;


                return  $reset_button;

            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    /**
     * @return mixed
     * Get constant cares by pensioner for datatable
     */
    public function getConstantCaresByPensionerForDt($pensioner_id)
    {
        return Datatables::of($this->pensioners->getConstantCaresByPensionerForDt($pensioner_id)->orderByDesc('b.resource_id'))
            ->addColumn('status', function ($query) {
                $resource = (new PayrollRepository())->getResource($query->member_type_id, $query->resource_id);//constant care
                return $resource->getStatusLabelAttribute($query->employee_id);
            })
            ->editColumn('monthly_pension_amount', function ($query) {
                return number_2_format($query->monthly_pension_amount);
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    /**
     * @param $pensioner
     * @param $employee
     * @param $status
     * Update manual pay status i.e. 1 => paid manual, 2 => paid and terminated, 0 => not paid
     */
    public function updateManualPayStatus($id,$employee_id, $status)
    {
        $this->pensioners->updateManualPayStatus($id, $employee_id, $status);
        return redirect()->route('backend.payroll.pensioner.pensioners_ready_for_merge_manual')->withFlashSuccess('Success, Dependent status has been synced');
    }




}
