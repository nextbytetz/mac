<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\BankUpdateRequest;
use App\Models\Operation\Payroll\PayrollBankInfoUpdate;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollBankUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollBankUpdateController extends Controller
{


    protected $member_types;
    protected $pensioners;
    protected $dependents;
    protected $banks;
    protected $payroll_bank_updates;
    protected $bank_branches;
    protected $wf_modules;
    protected $payrolls;

    public function __construct()
    {
        $this->member_types = new MemberTypeRepository();
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->banks = new BankRepository();
        $this->payroll_bank_updates = new PayrollBankUpdateRepository();
        $this->bank_branches = new BankBranchRepository();
        $this->wf_modules = new WfModuleRepository();
        $this->payrolls = new PayrollRepository();

        //TODO: Apply permissions for the functions

    }

    /*Main wf module group id*/
    public function wfModuleGroupId()
    {
        return 11;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($member_type_id, $resource_id, $employee_id)
    {
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $this->payroll_bank_updates->checkIfCanInitiateNewEntry($member_type_id, $resource_id);
        $resource = $this->getResource($member_type_id, $resource_id);
        $banks = $this->banks->getActiveBanksForSelect();
        $document_type = 64;
        $dependent_employee = ($member_type_id == 4) ?  $this->getDependentInput($resource_id,$employee_id)['dependent_employee'] : null;
        $check_if_enrolled = $this->payrolls->checkIfResourceAlreadyEnrolled($member_type_id, $resource_id);
        $check_if_first_payee = $this->payrolls->checkIfIsTheFirstPayee($member_type_id, $resource_id, $employee_id);
        if($check_if_first_payee == false){
            $this->payrolls->checkIfThereIsDocumentForAction($member_type_id, $resource_id, $document_type);
        }

        return view('backend/operation/payroll/pension_administration/bank_info/update/create_new_bank')
            ->with('resource',$resource)
            ->with('member_type_id', $member_type_id)
            ->with('banks', $banks)
            ->with('document_type', $document_type)
            ->with('dependent_employee', $dependent_employee)
            ->with('employee_id', $employee_id)
            ->with('check_if_enrolled', $check_if_enrolled)
            ->with('check_if_first_payee', $check_if_first_payee);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($resource_id, BankUpdateRequest $request)
    {
        //
        $input = $request->all();
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $this->payroll_bank_updates->checkIfCanInitiateNewEntry($input['member_type_id'], $resource_id);
        $active = $this->payrolls->checkIfResourceAlreadyEnrolled($input['member_type_id'], $resource_id);
        /*update document pending status*/
        $document_type = 64;
        /*check if there is document for update*/
        $this->payrolls->updateBeneficiaryDocumentPendingStatusPerType($input['member_type_id'], $resource_id, $document_type);
        /*end document update*/
        return  $this->storeForActiveBeneficiary($wf_module_group_id, $input, $resource_id);
//             if($active == true)
//        {
//            /*active*/
//            return  $this->storeForActiveBeneficiary($wf_module_group_id, $input, $resource_id);
//        }else{
//            /*not active*/
//            return   $this->storeForNotActiveBeneficiary($input,$resource_id);
//        }

    }
    /*Store when beneficiary is already in enrolled in payroll - Initiate workflow*/
    public function storeForActiveBeneficiary($wf_module_group_id, $input, $resource_id)
    {
        $this->payroll_bank_updates->checkIfCanInitiateNewEntry($input['member_type_id'], $resource_id);
        $member_type_id = $input['member_type_id'];
        /*workflow start*/

        $bank_update =  DB::transaction(function () use ($wf_module_group_id, $resource_id, $input) {
            access()->hasWorkflowDefinition($wf_module_group_id, 1);
            /*create*/
            $bank_update = $this->payroll_bank_updates->create($resource_id, $input);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $bank_update->id]));
            return $bank_update;
        });
        /*end workflow*/
        return redirect()->route('backend.payroll.bank_update.profile', ['bank_update' => $bank_update->id])->withFlashSuccess('Success, New bank details have been created. Workflow has been initiated');
    }

    /*Update bank details when beneficiary is not enrolled in payroll*/
    public function storeForNotActiveBeneficiary($input, $resource_id)
    {
        $bank_update =  DB::transaction(function () use ( $input,$resource_id) {
            /*create new update*/
            $resource =  $this->payroll_bank_updates->updateWhenBeneficiaryIsNotActive($input, $resource_id);
            /*end workflow*/
            return $resource;
        });
        $url = $this->payrolls->returnResourceProfileUrl($input['member_type_id'], $resource_id, $input['employee_id']);

        return redirect($url)->withFlashSuccess('Success, Bank details have been updated');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile(PayrollBankInfoUpdate $bank_update,  WorkflowTrackDataTable $workflowtrack)
    {
        //
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $bank_update->id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        /*end workflow*/
        $resource_id = $bank_update->resource_id;
        $member_type_id = $bank_update->member_type_id;
        $employee_id = $bank_update->employee_id;
        $resource = $this->getResource($member_type_id, $resource_id);
        $workflow_input = ['resource_id' => $bank_update->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => 0 ];
        $document_type = 64;
        $dependent_employee = ($member_type_id == 4) ?  $this->getDependentInput($resource_id,$employee_id)['dependent_employee'] : null;
        return view('backend/operation/payroll/pension_administration/bank_info/profile/profile')
            ->with(['resource' => $resource, 'member_type_id' => $member_type_id, 'bank_update' => $bank_update,'check_pending_level1' => $check_pending_level1, "workflow_track" => $workflowtrack, 'workflow_input' => $workflow_input, 'document_type' => $document_type, 'dependent_employee' => $dependent_employee, 'employee_id' => $employee_id]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollBankInfoUpdate $bank_update, $employee_id)
    {
        //
        $this->checkLevelRights(1, $bank_update->id);
        $resource = $this->getResource($bank_update->member_type_id, $bank_update->resource_id);
        $banks = $this->banks->getActiveBanksForSelect();
        $bank_branches = $this->bank_branches->query()->where('bank_id', $bank_update->new_bank_id)->get()->pluck('name', 'id');
        $document_type = 64;
        $dependent_employee = ($bank_update->member_type_id == 4) ?  $this->getDependentInput($bank_update->resource_id,$employee_id)['dependent_employee'] : null;
        return view('backend/operation/payroll/pension_administration/bank_info/update/update_new_bank')
            ->with(['bank_update' => $bank_update, 'resource' => $resource, 'member_type_id' => $bank_update->member_type_id, 'banks' => $banks, 'bank_branches' => $bank_branches , 'document_type'
            => $document_type, 'dependent_employee' => $dependent_employee, 'employee_id' => $employee_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( PayrollBankInfoUpdate $bank_update, BankUpdateRequest $request)
    {
        //
        $this->checkLevelRights(1, $bank_update->id);
        $input = $request->all();
        $this->payroll_bank_updates->update($bank_update, $input);
//        $url = $this->returnResourceProfileUrl($bank_update->member_type_id,  $bank_update->resource_id);
        return redirect()->route('backend.payroll.bank_update.profile', ['bank_update' => $bank_update->id, 'employee'=> $input['employee_id']])->withFlashSuccess('Success, New bank details have been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayrollBankInfoUpdate $bank_update, $employee)
    {
        //
        $this->checkLevelRights(1, $bank_update->id);
        $this->payroll_bank_updates->undo($bank_update);
        $url = $this->payrolls->returnResourceProfileUrl($bank_update->member_type_id,  $bank_update->resource_id,$employee);
        return redirect($url)->withFlashSuccess('Success, Bank update has been undone');
    }

    public function getDependentInput($dependent_id, $employee_id)
    {
        $dependent = $this->dependents->find($dependent_id);
        $dependent_employee = $dependent->employees()->where('employee_id', $employee_id)->first();
        $employee = (new EmployeeRepository())->find($employee_id);
        return ['dependent_employee' => $dependent_employee->pivot, 'employee' => $employee];
    }


//    /*Return resource profile page url*/
//    public function returnResourceProfileUrl($member_type_id, $resource_id)
//    {
//        if($member_type_id == 5){
//            /*Pensioner*/
//            $url = 'payroll/pensioner/profile/'. $resource_id;
//        }elseif($member_type_id == 4){
//            /*dependent*/
//            $url = 'compliance/dependent/profile/'. $resource_id;
//        }
//        return $url;
//    }


    /*Erase current bank details for beneficiary not active yet*/
    public function eraseCurrentBankDetails($member_type_id, $resource_id, $employee_id)
    {
        $this->payroll_bank_updates->eraseCurrentBankDetails($member_type_id    ,$resource_id, $employee_id);
        $url = $this->payrolls->returnResourceProfileUrl($member_type_id,  $resource_id,$employee_id);
        return redirect($url)->withFlashSuccess('Success, Bank details have been updated');
    }

    /**
     * @param $member_type_id
     * @param $resource_id
     * Get resource based on the member type
     */
    public function getResource($member_type_id, $resource_id)
    {
        $payroll_repo = new PayrollRepository();
        $resource = $payroll_repo->getResource($member_type_id, $resource_id);
        return $resource;
    }


    /**
     * @param $resource_id
     * @param $notification_report_id
     *
     */
    public function getBankUpdatesByBeneficiaryForDataTable($member_type_id, $resource_id)
    {
        return Datatables::of($this->payroll_bank_updates->getByBeneficiaryForDataTable($member_type_id, $resource_id)->orderBy('id', 'desc'))
            ->addColumn('new_bank_name', function ($bank_update) {
                return  (isset($bank_update->new_bank_id) ?  $bank_update->newBank->name : ' ');
            })
            ->addColumn('new_bank_branch', function ($bank_update) {
                return  isset($bank_update->new_bank_branch_id) ? $bank_update->newBankBranch->name : ' ';
            })
            ->addColumn('created_at_formatted', function ($bank_update) {
                return short_date_format($bank_update->created_at);
            })
            ->addColumn('wf_done_date_formatted', function ($bank_update) {
                return ($bank_update->wf_done_date)  ? short_date_format($bank_update->wf_done_date) : ' ';
            })
            ->addColumn('status', function ($bank_update) {
                return $bank_update->status_label;
            })
            ->addColumn('user', function ($bank_update) {
                return $bank_update->user->username;
            })
            ->rawColumns(['status'])
            ->make(true);

    }



    /**
     * Check if can Initiate Action (Level ? / No Workflow)
     */
    public function checkLevelRights($level,$resource_id)
    {
        $wf_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowDefinition($wf_group_id, $level);
        workflow([['wf_module_group_id' => $wf_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction($level);
    }





}
