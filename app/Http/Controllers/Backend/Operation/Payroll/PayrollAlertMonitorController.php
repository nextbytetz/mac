<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\DataTables\Report\Traits\ExcelExportTrait;
use App\Http\Controllers\Controller;
use App\Models\Operation\Claim\NotificationReport;
use App\Repositories\Backend\Operation\Claim\ClaimRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\ManualPayrollMemberRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollAlertTaskRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRecoveryRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollVerificationRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Operation\Payroll\Retiree\PayrollRetireeFollowupRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollChildSuspensionRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollRetireeSuspensionRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollAlertMonitorController extends Controller
{
    //
    use ExcelExportTrait;

    protected $payroll_child_suspensions;
    protected $payroll_retiree_suspensions;
    protected $pensioners;
    protected $dependents;
    protected $notification_reports;
    protected $payrolls;


    public function __construct()
    {
        $this->payroll_child_suspensions = new PayrollChildSuspensionRepository();
        $this->payroll_retiree_suspensions= new PayrollRetireeSuspensionRepository();
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->notification_reports = new NotificationReportRepository();
        $this->payrolls = new PayrollRepository();

    }


    public function index($alert_reference)
    {
        $summary = $this->summary();
        return view('backend/operation/payroll/pension_administration/alert_monitor/index')->with(['alert_reference'=> $alert_reference, 'summary' => $summary]);
    }


    public function summary()
    {
        $child_suspension_count = $this->payroll_child_suspensions->getSuspensionPendingAction()->count();
        $child_limit_age_alert_count = $this->payroll_child_suspensions->getChildLimitAgeAlertsForDataTable()->count();
        $child_alert_disability_ext_count = $this->payroll_child_suspensions->getChildWithGrantedExtensionForDt('PAYCHCDIS')->count();
        $child_alert_edu_ext_count = $this->payroll_child_suspensions->getChildWithEducationExtensionDeadlineAlertsForDataTable()->count();
        /*retiree*/
        $retiree_limit_age_count = $this->payroll_retiree_suspensions->getPendingSuspensionForAction()->count();
        $retiree_pensioners_alerts_count = (new PayrollRetireeFollowupRepository())->getPayrollRetireeAlertsByMemberTypeForDt(5)->count();

        /*end retiree*/
        $pensioners_for_payroll_count = $this->pensioners->getReadyForPayrollForDataTable()->count();
        $dependents_for_payroll_count = $this->dependents->getReadyForPayrollForDataTable()->count();
        $claims_pending_cca = (new ClaimRepository())->getClaimsPendingCcaForDataTable()->count();
        /*start document pending for update*/
        $pending_bank_update_docs = $this->payrolls->getPendingDocsForBeneficiaryUpdateDataTable(64)->count();
        $pending_verification_docs = $this->payrolls->getPendingDocsForBeneficiaryUpdateDataTable(63)->count();
        $pending_beneficiary_details_docs = $this->payrolls->getPendingDocsForBeneficiaryUpdateDataTable(65)->count();
        /*end doc summary*/
        /*payroll recovery*/
        $pending_payroll_recoveries = (new PayrollRecoveryRepository())->getAllPendingsForDataTable()->count();
        /*end payroll recovery*/

        /*Beneficiaries with missing details*/
        $pensioners_with_missing_details_count = (new PensionerRepository())->getPensionersWithMissingDetails()->count();
        $dependents_with_missing_details_count = (new DependentRepository())->getDependentsWithMissingDetails()->count();
        /*end beneficiary with missing details*/

        /*Payroll pending workflow*/
        $payroll_pending_workflow_count = $this->payrolls->getPayrollPendingWorkflowsForDataTable()->count();
        /*end of payroll pending workflow*/

        /*Payroll verification alerts*/
        $payroll_pensioners_verification_alert_count = (new PensionerRepository())->getVerificationAlertsForDt()->get()->count();
        $payroll_dependents_verification_alert_count = (new DependentRepository())->getVerificationAlertsForDt()->get()->count();
        /*end verification alerts*/

        /*Beneficiaries on Hold*/
        $payroll_beneficiaries_on_hold_count = $this->payrolls->getHeldBeneficiariesForDt()->count();
        /*end beneficiaries on hold*/

        /*Get all suspended*/
        $payroll_all_suspended = $this->payrolls->getAllSuspendedBeneficiariesForDt()->count();
        return [
            'child_suspension_count' => $child_suspension_count,
            'child_limit_age_alert_count' => $child_limit_age_alert_count,
            'child_alert_edu_ext_count' => $child_alert_edu_ext_count,
            'child_alert_disability_ext_count' => $child_alert_disability_ext_count,
            'retiree_suspension_count' => $retiree_limit_age_count,
            'pensioners_for_payroll_count' => $pensioners_for_payroll_count,
            'dependents_for_payroll_count' => $dependents_for_payroll_count,
            'claims_pending_cca' => $claims_pending_cca,
            'pending_bank_update_docs' => $pending_bank_update_docs,
            'pending_verification_docs' => $pending_verification_docs,
            'pending_beneficiary_details_docs' => $pending_beneficiary_details_docs,
            'pending_payroll_recoveries' => $pending_payroll_recoveries,
            'dependents_with_missing_details_count' => $dependents_with_missing_details_count,
            'pensioners_with_missing_details_count' => $pensioners_with_missing_details_count,
            'payroll_pending_workflow_count' => $payroll_pending_workflow_count,
            'payroll_pensioners_verification_alert_count' => $payroll_pensioners_verification_alert_count,
            'payroll_dependents_verification_alert_count' => $payroll_dependents_verification_alert_count,
            'payroll_beneficiaries_on_hold_count' => $payroll_beneficiaries_on_hold_count,
            'payroll_all_suspended' => $payroll_all_suspended,
            'retiree_pensioners_alerts_count' => $retiree_pensioners_alerts_count,
        ];
    }

    /**
     * Refresh all payroll alert task checker
     */
    public function refreshAllPayrollAlertTaskChecker()
    {

        (new PayrollAlertTaskRepository())->initializeAlertTaskForAll();
        return redirect()->back()->withFlashSuccess('Success, Payroll Alert task has been refreshed');
    }
    /**
     * @param $member_type_id
     * @return mixed
     * Get beneficiaries with missing details for datatable
     */
    public function getBeneficiariesMissingDetailsForDataTable($member_type_id){

        if($member_type_id == 5){
            /*pensioners*/
            $beneficiaries = (new PensionerRepository())->getPensionersWithMissingDetails();
        }else{
            /*dependents*/
            $beneficiaries = (new DependentRepository())->getDependentsWithMissingDetails();
        }

        return Datatables::of($beneficiaries)
            ->addColumn('name', function ($beneficiary) use($member_type_id) {
                return $beneficiary->name;
            })
            ->addColumn('missing_details', function ($beneficiary) {
                return $beneficiary->missing_details_alert;
            })

            ->addColumn('employee_id_formatted', function ($beneficiary) use($member_type_id){
                $employee_id = ($member_type_id == 4) ? $beneficiary->employees()->first()->pivot->employee_id : $beneficiary->employee_id;
                return $employee_id;
            })
            ->rawColumns([''])
            ->make(true);
    }


    /**
     * @return mixed
     * Reset Alerts
     */
    public function resetAlerts()
    {
        //Document verification
        (new PayrollVerificationRepository())->resetDocumentStatusForAllInitiatedVerification();
        return redirect()->back()->withFlashSuccess('Success, Alerts have been reset');
    }


    /**
     * @return mixed
     * Export Alert Monitor
     */
    public function exportAlertMonitor($alert_index)
    {

        $result_query = null;
        switch ($alert_index){
            case 1://pensioners ready
                $result_query = $this->pensioners->getReadyForPayrollForDataTable();
                break;
            case 2://dependents ready
                $result_query = $this->dependents->getReadyForPayrollForDataTable();
                break;
            case 3:// child suspensions
                $result_query =$this->payroll_child_suspensions->getSuspensionPendingAction();
                break;
            case 4:// child limit age
                $result_query =  $this->payroll_child_suspensions->getChildLimitAgeAlertsForDataTable();
                break;
            case 5://retiree limit age
                $result_query =  $this->payroll_retiree_suspensions->getPendingSuspensionForAction();
                break;
            case 6://claims cca
                $result_query =  (new ClaimRepository())->getClaimsPendingCcaForDataTable();
                break;
            case 7://doc verification
                $result_query = $this->payrolls->getPendingDocsForBeneficiaryUpdateDataTable(63);
                break;
            case 8://doc bank updates
                $result_query = $this->payrolls->getPendingDocsForBeneficiaryUpdateDataTable(64);
                break;
            case 9://doc beneficiary updates
                $result_query = $this->payrolls->getPendingDocsForBeneficiaryUpdateDataTable(65);
                break;
            case 10:

                break;
            case 11://pensioner missing details
                $result_query =(new PensionerRepository())->getPensionersWithMissingDetails();
                break;
            case 12://dependents missing details
                $result_query =(new DependentRepository())->getDependentsWithMissingDetails();
                break;
            case 13://Pending workflow
                $result_query =  $this->payrolls->getPayrollPendingWorkflowsForDataTable();
                break;
            case 14://Pensioner verification alert
                $result_query = (new PensionerRepository())->getVerificationAlertsForDt();
                break;
            case 15://dependent verification alert
                $result_query = (new DependentRepository())->getVerificationAlertsForDt();
                break;
            case 16://on hold
                $result_query = $this->payrolls->getHeldBeneficiariesForDt();
                break;
            case 17://all suspended
                $result_query = $this->payrolls->getAllSuspendedBeneficiariesForDt();
                break;
            case 18://child alert edu
                $result_query = $this->payroll_child_suspensions->getChildWithEducationExtensionDeadlineAlertsForDataTable();
                break;
            case 19://child alert disability
                $result_query =$this->payroll_child_suspensions->getChildWithGrantedExtensionForDt('PAYCHCDIS');
                break;
            case 20://retiree pensioners
                $result_query = (new PayrollRetireeFollowupRepository())->getPayrollRetireeAlertsByMemberTypeForDt(5);
                break;
        }

        /*Get result to export*/
        $result_list = $result_query->get()->toArray();
        $results = array();
        foreach ($result_list as $result) {
            $results[] = (array)$result;
        }

        return $this->exportExcelGeneral($results, 'payrollAlertMonitor'. $alert_index, 'csv');
    }
}
