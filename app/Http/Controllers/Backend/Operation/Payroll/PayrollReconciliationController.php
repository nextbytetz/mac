<?php

namespace App\Http\Controllers\Backend\Operation\Payroll;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Models\Operation\Payroll\Run\PayrollReconciliation;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollReconciliationRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollReconciliationController extends Controller
{
    protected   $payroll_reconciliations;


    public function __construct()
    {
        $this->payroll_reconciliations = new PayrollReconciliationRepository();
    }

    /*Main wf module group id*/
    public function wfModuleGroupId()
    {
        return 14;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend/operation/payroll/reconciliation/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * @param PayrollReconciliation $payrollReconciliation
     * @param WorkflowTrackDataTable $workflowTrackDataTable
     * @return $this
     * @throws \App\Exceptions\GeneralException
     * Open profile of reconciliation approval
     */
    public function profile(PayrollReconciliation $payrollReconciliation, WorkflowTrackDataTable $workflowTrackDataTable)
    {

        $payrolls = new PayrollRepository();
        $member_type_id = $payrollReconciliation->member_type_id;
        $resource_id = $payrollReconciliation->resource_id;
        $notification_report_id = $payrollReconciliation->notification_report_id;
        $resource = $payrolls->getResource($member_type_id, $resource_id);
        $notification_report = (new NotificationReportRepository())->find($notification_report_id);
        $employee_id = $notification_report->employee_id;
        $mp =$payrolls->getMp($member_type_id, $resource_id, $employee_id);
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $payrollReconciliation->id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        $workflow_input = ['resource_id' => $payrollReconciliation->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => 0 ];
        /*end workflow*/
        /*start mp summary*/
        $payroll_runs = new PayrollRunRepository();
        $mp_payment_summary =  $payroll_runs->getMpPaymentSummaryForMember($member_type_id, $resource_id, $employee_id);
        $last_payroll_run_date = $payroll_runs->getMostRecentApprovedRunDate();
        /*end mp summary*/
        return view('backend/operation/payroll/reconciliation/profile/profile')
            ->with(['resource' => $resource, 'member_type_id'=>$member_type_id, 'notification_report_id' => $notification_report_id,'employee_id' => $employee_id, 'check_pending_level1' => $check_pending_level1,'check_workflow' => $check_workflow, "workflow_track" => $workflowTrackDataTable, 'workflow_input' => $workflow_input,'payroll_reconciliation' => $payrollReconciliation, 'mp'=>$mp, 'mp_payment_summary'=> $mp_payment_summary, 'last_payroll_run_date'=> $last_payroll_run_date]);
    }

    /**
     * @param PayrollReconciliation $payrollReconciliation
     * Undo / delete reconciliation
     */
    public function undo(PayrollReconciliation $payrollReconciliation)
    {
$this->payroll_reconciliations->undo($payrollReconciliation);
        return redirect()->route('backend.payroll.reconciliation.index')->withFlashSuccess('Success, Payroll Reconciliation process has been undone');
    }

    /**
     * @param PayrollReconciliation $payrollReconciliation
     * @return mixed
     * Initiate reconciliation approval
     */
    public function initiateReconciliation(PayrollReconciliation $payrollReconciliation)
    {
        $input = request()->all();
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        /*workflow start*/
        $wf_module_group_id = $this->wfModuleGroupId();
        DB::transaction(function () use ($wf_module_group_id, $payrollReconciliation,$input) {
            /*initiate*/
            $this->payroll_reconciliations->initiateReconciliation($payrollReconciliation);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $payrollReconciliation->id], [], ['comments' => $input['comments']]));

        });
        /*end workflow*/
        return redirect()->route('backend.payroll.reconciliation.profile',$payrollReconciliation->id)->withFlashSuccess('Success, Payroll Reconciliation approval has been initiated and workflows have been created');
    }





    /**
     * Process reconciliation manually from the application
     */
    public function processReconciliationJob()
    {
        $this->payroll_reconciliations->payrollSystemReconciliationJobChunk();
        return redirect()->route('backend.payroll.reconciliation.index')->withFlashSuccess('Success, Payroll Reconciliation process has been initiated, process will proceed on the background');
    }



    /**
     * Get Payroll run approvals for datatable
     */
    public function getAllForDataTable()
    {
        return Datatables::of($this->payroll_reconciliations->getAllForDataTable()->orderBy('payroll_reconciliations.id', 'desc'))
            ->addColumn('beneficiary', function ($payroll_reconciliation) {
                return($payroll_reconciliation->member_type_id == 4) ? $payroll_reconciliation->dependent_name : $payroll_reconciliation->pensioner_name;
            })
            ->addColumn('amount_formatted', function ($payroll_reconciliation) {
                return number_2_format($payroll_reconciliation->amount);
            })
            ->editColumn('type', function ($payroll_reconciliation) {
                return  $payroll_reconciliation->type_label;
            })
            ->editColumn('status', function ($payroll_reconciliation) {
                return   $payroll_reconciliation->status_label;
            })
            ->rawColumns(['status', 'type'])
            ->make(true);
    }


}
