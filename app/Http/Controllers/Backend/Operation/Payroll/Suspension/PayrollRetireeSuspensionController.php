<?php

namespace App\Http\Controllers\Backend\Operation\Payroll\Suspension;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\ManualSuspensionRequest;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollChildSuspensionRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollRetireeSuspensionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollRetireeSuspensionController extends Controller
{
    //

    protected $payroll_retiree_suspensions;

    public function __construct()
    {
        $this->payroll_retiree_suspensions = new PayrollRetireeSuspensionRepository();
    }




    /**
     * Get All suspensions for dataTable
     */
    public function getForDataTable()
    {
        return Datatables::of($this->payroll_retiree_suspensions->getForDataTable()->orderBy('id', 'asc'))
            ->editColumn('suspended_date', function ($payroll_retiree_suspension) {
                return short_date_format($payroll_retiree_suspension->created_at);
            })
            ->addColumn('dob_formatted', function ($payroll_retiree_suspension) {
                return   short_date_format($payroll_retiree_suspension->dob);
            })
            ->make(true);

    }



    /**
     * Get Pending retiree suspension for action for dataTable
     */
    public function getPendingForDataTable()
    {
        return Datatables::of($this->payroll_retiree_suspensions->getPendingSuspensionForAction()->orderBy('id', 'asc'))
            ->editColumn('suspended_date', function ($payroll_retiree_suspension) {
                return short_date_format($payroll_retiree_suspension->created_at);
            })
            ->addColumn('dob_formatted', function ($payroll_retiree_suspension) {
                return   short_date_format($payroll_retiree_suspension->dob);
            })
            ->addColumn('limit_option', function ($payroll_retiree_suspension) {
                return  ($payroll_retiree_suspension->isvoluntary == 1) ? 'Verify voluntary retirement' : 'Verify compulsory retirement';
            })
            ->make(true);

    }



    /**
     * Get Child alerts  who approach 18years in next two months
     */
    public function getChildLimitAgeAlertsForDataTable()
    {
        return Datatables::of($this->payroll_child_suspensions->getChildLimitAgeAlertsForDataTable()->orderBy('dob', 'asc'))
            ->addColumn('fullname', function ($dependent) {
                return $dependent->name;
            })
            ->addColumn('dob_formatted', function ($dependent) {
                return   short_date_format($dependent->dob);
            })
            ->addColumn('days_left', function ($dependent) {
                return $dependent->child_limit_age_days_left  ;
            })
            ->make(true);

    }




}
