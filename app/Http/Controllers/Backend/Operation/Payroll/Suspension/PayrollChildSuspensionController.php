<?php

namespace App\Http\Controllers\Backend\Operation\Payroll\Suspension;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\ManualSuspensionRequest;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollChildSuspensionRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollManualSuspensionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollReconciliationRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollChildSuspensionController extends Controller
{
    //

    protected $payroll_child_suspensions;

    public function __construct()
    {
        $this->payroll_child_suspensions = new PayrollChildSuspensionRepository();
    }




    /**
     * Get Payroll child suspensions for dataTable
     */
    public function getForDataTable()
    {
        return Datatables::of($this->payroll_child_suspensions->getForDataTable()->orderBy('id', 'asc'))
            ->editColumn('suspended_date', function ($payroll_child_suspension) {
                return short_date_format($payroll_child_suspension->suspended_date);
            })
            ->addColumn('dob_formatted', function ($payroll_child_suspension) {
                return   short_date_format($payroll_child_suspension->dob);
            })
            ->addColumn('limit_option', function ($payroll_child_suspension) {
                return ($payroll_child_suspension->reason) ? $payroll_child_suspension->reason : (($payroll_child_suspension->iseducation == 0) ? (sysdefs()->data()->payroll_child_limit_age . 'yrs Limit') : (sysdefs()->data()->payroll_child_education_limit_age . 'yrs - Education Limit'));
            })
            ->make(true);

    }




    /**
     * Get Payroll pending child suspension which await action for dataTable
     */
    public function getPendingForDataTable()
    {
        return Datatables::of($this->payroll_child_suspensions->getSuspensionPendingAction()->orderBy('id', 'asc'))
            ->editColumn('suspended_date', function ($payroll_child_suspension) {
                return short_date_format($payroll_child_suspension->suspended_date);
            })
            ->addColumn('dob_formatted', function ($payroll_child_suspension) {
                return   short_date_format($payroll_child_suspension->dob);
            })
            ->addColumn('limit_option', function ($payroll_child_suspension) {
                return  ($payroll_child_suspension->reason) ? $payroll_child_suspension->reason :   (($payroll_child_suspension->iseducation == 0) ? (sysdefs()->data()->payroll_child_limit_age . 'yrs Limit') : (sysdefs()->data()->payroll_child_education_limit_age . 'yrs - Education Limit'));
            })
            ->make(true);

    }




    /**
     * Get Child alerts  who approach 18years in next two months
     */
    public function getChildLimitAgeAlertsForDataTable()
    {
        return Datatables::of($this->payroll_child_suspensions->getChildLimitAgeAlertsForDataTable()->orderBy('dependents.dob', 'asc'))
            ->addColumn('fullname', function ($dependent) {
                return $dependent->fullname;
            })
            ->addColumn('dob_formatted', function ($dependent) {
                return   short_date_format($dependent->dob);
            })
            ->addColumn('days_left', function ($dependent) {
                $dep = (new DependentRepository())->find($dependent->dependent_id);
                return $dep->child_limit_age_days_left  ;
            })
            ->make(true);

    }

    /**
     * Get Child alerts  who approach 18years in next two months - with education extension deadline
     */
    public function getChildWithEducationExtensionDeadlineAlertsForDt()
    {
        return Datatables::of($this->payroll_child_suspensions->getChildWithEducationExtensionDeadlineAlertsForDataTable()->orderBy('dependent_employee.deadline_date', 'asc'))
            ->addColumn('fullname', function ($dependent) {
                return $dependent->fullname;
            })
            ->addColumn('dob_formatted', function ($dependent) {
                return   short_date_format($dependent->dob);
            })
            ->addColumn('days_left', function ($dependent) {
                $deadline_date = Carbon::parse($dependent->deadline_date);
                return ((comparable_date_format($dependent->deadline_date) > comparable_date_format(Carbon::now())) ? Carbon::now()->diffInDays($deadline_date) : 'Passed Deadline');
            })
            ->editColumn('deadline_date', function ($dependent) {
                return   short_date_format($dependent->deadline_date);
            })
            ->make(true);

    }

    /*Get Child with extension granted*/
    public function getChildWithExtensionDeadlineAlertsForDt($child_continuation_reason_cv_ref)
    {
        return Datatables::of($this->payroll_child_suspensions->getChildWithGrantedExtensionForDt($child_continuation_reason_cv_ref)->orderBy('dependent_employee.deadline_date', 'asc'))
            ->addColumn('fullname', function ($dependent) {
                return $dependent->fullname;
            })
            ->addColumn('dob_formatted', function ($dependent) {
                return   short_date_format($dependent->dob);
            })
            ->addColumn('days_left', function ($dependent) {
                $deadline_date = Carbon::parse($dependent->deadline_date);
                return ((comparable_date_format($dependent->deadline_date) > comparable_date_format(Carbon::now())) ? Carbon::now()->diffInDays($deadline_date) : 'Passed Deadline');
            })
            ->editColumn('deadline_date', function ($dependent) {
                return   short_date_format($dependent->deadline_date);
            })
            ->make(true);

    }


}
