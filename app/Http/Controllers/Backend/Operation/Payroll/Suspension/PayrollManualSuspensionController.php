<?php

namespace App\Http\Controllers\Backend\Operation\Payroll\Suspension;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Payroll\ManualSuspensionRequest;
use App\Models\Operation\Payroll\Suspension\PayrollManualSuspension;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollManualSuspensionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollReconciliationRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollSystemSuspensionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class PayrollManualSuspensionController extends Controller
{
    //

    protected $payroll_manual_suspensions;

    public function __construct()
    {
        $this->payroll_manual_suspensions = new PayrollManualSuspensionRepository();

        $this->middleware('access.routeNeedsPermission:payroll_manual_suspension', ['only' => ['process']]);
    }


    public function index()
    {
        return view('backend/operation/payroll/pension_administration/suspension/manual_suspension/index');
    }


    public function create()
    {
        return view('backend/operation/payroll/pension_administration/suspension/manual_suspension/create');
    }

    /*Profile*/
    public function profile(PayrollManualSuspension $payrollManualSuspension)
    {
        return view('backend/operation/payroll/pension_administration/suspension/manual_suspension/profile/profile')
            ->with('payroll_manual_suspension', $payrollManualSuspension);
    }


    /**
     * @param ManualSuspensionRequest $request
     * @return mixed
     * Process manual suspension triggered by use
     */
    public function process(ManualSuspensionRequest $request)
    {
        $input  = $request->all();
        return DB::transaction(function () use ($input) {
            $payrolls = new PayrollRepository();
            $manual_suspension = $this->payroll_manual_suspensions->create($input);
            $this->payroll_manual_suspensions->suspendUnverifiedBeneficiaries($manual_suspension->id);
            return redirect()->route('backend.payroll.suspension.index')->withFlashSuccess('Success, Suspension process has been initiated!');
        });
    }


    /**
     * Get Payroll run approvals for dataTable
     */
    public function getForDataTable()
    {
        return Datatables::of($this->payroll_manual_suspensions->getForDataTable()->orderBy('id', 'desc'))
            ->addColumn('created_at_formatted', function ($payroll_manual_suspension) {
                return short_date_format($payroll_manual_suspension->created_at);
            })
            ->addColumn('user', function ($payroll_manual_suspension) {
                return   $payroll_manual_suspension->user->username;
            })
            ->editColumn('remark', function ($payroll_manual_suspension) {
                return   truncateString($payroll_manual_suspension->remark, 80);
            })
            ->addColumn('run_date_formatted', function ($payroll_manual_suspension) {
                return   short_date_format($payroll_manual_suspension->run_date);
            })
            ->make(true);

    }

    /*Get Suspensions By Manual For Dt*/
    public function getSuspensionsByManualForDt($manual_suspension_id)
    {
        $suspensions = (new PayrollSystemSuspensionRepository())->getAllByManualSuspensionForDt($manual_suspension_id);
        return Datatables::of($suspensions->orderBy('b.lastresponse'))
            ->editColumn('lastresponse', function ($suspension) {
                return short_date_format($suspension->lastresponse);
            })
            ->addColumn('member_type', function ($suspension) {
                return ($suspension->member_type_id == 4) ? 'Dependent' : 'Pensioner';
            })

            ->make(true);

    }


}
