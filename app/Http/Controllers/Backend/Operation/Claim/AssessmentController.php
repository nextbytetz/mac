<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\Http\Requests\Backend\Operation\Claim\UpdateImpairmentAssessmentRequest;
use App\Models\Operation\Claim\ImpairmentAssessment;
use App\Models\Operation\Claim\NotificationReport;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Claim\DocumentResourceRepository;
use App\Repositories\Backend\Operation\Claim\ImpairmentAssessmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdImpairmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdInjuryRepository;
use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class AssessmentController
 * @package App\Http\Controllers\Backend\Operation\Claim
 */
class AssessmentController extends Controller
{
    public function menu()
    {
        return view('backend/operation/claim/assessment/menu');
    }

    public function pdCalculator()
    {
        $pd_body_part_injuries = (new PdInjuryRepository())->getAll()->pluck("name_fec", "id")->all();
        $pd_impairments = (new PdImpairmentRepository())->getAll()->pluck("name", "id")->all();
        return view('backend/operation/claim/assessment/pd_calculator')
                    ->with('pd_impairments', $pd_impairments)
                    ->with('pd_body_part_injuries', $pd_body_part_injuries);
    }

    public function performanceReport()
    {
        return view('backend/report/performance/assessment/staff/index');
    }

    public function allImpairment()
    {
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->find(55);
        if  (!$cr) {
            throw new ModelNotFoundException();
        }
        return view('backend/operation/claim/assessment/impairment/all')
                ->with('cr', $cr);
    }

    public function chooseImpairment()
    {
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->find(56);
        if  (!$cr) {
            throw new ModelNotFoundException();
        }
        return view('backend/operation/claim/assessment/impairment/choose')
                ->with('cr', $cr);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function impairmentDashboard(NotificationReport $incident)
    {
        $impairments = $incident->impairmentAssessments;
        //$docs_attached = collect([]);
        $docs_attached = (new DocumentResourceRepository())->getDocsAttachedNonRecurring($incident, "DRNOTIFCNINCDNT");
        //dd($impairments);
        return view('backend/operation/claim/assessment/impairment/dashboard')
                ->with('incident', $incident)
                ->with('employee', $incident->employee)
                ->with('impairments', $impairments)
                ->with('docs_attached', $docs_attached);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createImpairment(NotificationReport $incident)
    {
        /*        $impairmentAssessmentRepo = new ImpairmentAssessmentRepository();
                $check = $impairmentAssessmentRepo->query()->where('notification_report_id', $incident->id)->count();
                if ($check) {
                    return redirect()->route('backend.assessment.impairment.dashboard', $incident->id);
                }*/
        $users = (new UserRepository())->getAll()->pluck('name', 'id')->all();
        $regions = (new RegionRepository())->getAll()->pluck('name', 'id')->all();
        return view('backend/operation/claim/assessment/impairment/create')
                ->with('incident', $incident)
                ->with('users', $users)
                ->with('regions', $regions);
    }

    public function createImpairmentPost(NotificationReport $incident, UpdateImpairmentAssessmentRequest $request)
    {
        $input = $request->all();
        $impairment = (new ImpairmentAssessmentRepository())->create($incident, $input);
        $redirect_url = route('backend.assessment.impairment.dashboard', $incident->id);
        //logger($redirect_url);
        return response()->json(['success' => true, 'redirect_url' => $redirect_url, 'message' => '']);
    }

    /**
     * @param ImpairmentAssessment $impairment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editImpairment(ImpairmentAssessment $impairment)
    {
        $users = (new UserRepository())->getAll()->pluck('name', 'id')->all();
        $regions = (new RegionRepository())->getAll()->pluck('name', 'id')->all();
        $incident = $impairment->incident;
        return view('backend/operation/claim/assessment/impairment/edit')
                ->with('impairment', $impairment)
                ->with('incident', $incident)
                ->with('users', $users)
                ->with('regions', $regions);
    }

    public function updateImpairment(ImpairmentAssessment $impairment, UpdateImpairmentAssessmentRequest $request)
    {
        $input = $request->all();
        $impairment = (new ImpairmentAssessmentRepository())->update($impairment, $input);
        $redirect_url = route('backend.assessment.impairment.dashboard', $impairment->incident->id);
        return response()->json(['success' => true, 'redirect_url' => $redirect_url, 'message' => '']);
    }

    public function deleteImpairment(ImpairmentAssessment $impairment)
    {
        $incident = $impairment->incident;
        $checkModel = $incident->impairmentAssessments()->where('impairment_assessments.id', '<>', $impairment->id);
        $incident->impairment_assessment_id = $checkModel->count() ? $checkModel->orderByDesc('impairment_assessments.id')->first()->id : NULL;
        $incident->save();

        $impairment->delete();
        return redirect()->back()->withFlashSuccess('Success, Impairment Assessment has been deleted!');
    }

}
