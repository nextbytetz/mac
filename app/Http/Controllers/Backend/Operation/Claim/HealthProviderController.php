<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\DataTables\Claim\GetHealthProvidersDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\CreateHealthProviderRequest;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Operation\Claim\HealthProviderRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use Carbon\Carbon;

/**
 * Class HealthProviderController
 * @package App\Http\Controllers\Backend\Operation\Claim
 */
class HealthProviderController extends Controller
{

    use AttachmentHandler;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $health_providers;

    public function __construct(HealthProviderRepository $health_providers){
        $this->health_providers = $health_providers;

        $this->middleware('access.routeNeedsPermission:create_health_provider', ['only' => ['create', 'store']]);
        $this->middleware('access.routeNeedsPermission:edit_health_provider', ['only' => ['edit', 'update']]);
    }

    public function index(GetHealthProvidersDataTable $dataTable)
    {
        //
        return  $dataTable->render('backend/operation/claim/health_provider/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $districts = new DistrictRepository();
        return view('backend.operation.claim.health_provider.create')
            ->withDistricts($districts->getAll()->pluck('name', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateHealthProviderRequest $request)
    {
        //
        $health_provider = $this->health_providers->create($request->all());
        //Check if request is ajax (Registration through modals)
        if ($request->ajax()) {
            $return = response()->json(['success' => true, 'message' => "Success, Health Provider has been added"]);
        } else {
            $return = redirect()->route('backend.claim.health_provider.index')->withFlashSuccess(trans('alerts.backend.claim.health_provider_created'));
        }
        return $return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $districts = new DistrictRepository();
        $health_provider = $this->health_providers->findOrThrowException($id);
        return view('backend.operation.claim.health_provider.edit')
            ->withHealthProvider($health_provider)
            ->withDistricts($districts->getAll()->pluck('name', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateHealthProviderRequest $request, $id)
    {
        //
        $health_provider =  $this->health_providers->update($id, $request->all());
        return redirect()->route('backend.claim.health_provider.index')->withFlashSuccess(trans('alerts.backend.claim.health_provider_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $health_provider =  $this->health_providers->delete($id);
        return redirect()->route('backend.claim.health_provider.index')->withFlashSuccess(trans('alerts.backend.claim.health_provider_deleted'));

    }


    public function getHealthProviders()
    {
        return $this->health_providers->getHealthProviders(request()->input('q'), request()->input('page'));
    }


    public function getRegisteredHealthProviders()
    {
        return $this->health_providers->getRegisteredHealthProviders(request()->input('q'), request()->input('page'));
    }

    /**
     * Upload bulk health providers
     *
     */
    public function uploadBulkPage(){

        return view('backend.operation.claim.health_provider.upload_bulk');
    }




    /**
     *
     * Store Uploaded Bulk
     */
    public function uploadBulk(){
        $linked_file_input = 'linked_file';
        $file = request()->file('linked_file');
        $file_name = Carbon::parse('now')->format('M') . '_' . Carbon::parse('now')->format('Y').$file->getClientOriginalExtension();
        $base_dir = health_provider_bulk_dir() . DIRECTORY_SEPARATOR;

        if ($this->saveDocumentGeneral($linked_file_input, $file_name,$base_dir)) {
            $linked_file = $base_dir . $file_name;
            /* Excel chunk into database*/
            $this->health_providers->uploadBulk($linked_file);

            return response()->json(['success' => true, 'message' => trans('alerts.backend.finance.receipt.linked_file_created')]);
        }
        return response()->json(['success' => false, 'message' => trans('exceptions.backend.finance.receipts.contribution.upload')]);
    }




}
