<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\DataTables\Claim\OnlineAccountDataTable;
use App\DataTables\Compliance\Member\EmployerIncidentDataTable;
use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Events\Sockets\Notification\NewReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\ApproveRejectionAppealRequest;
use App\Http\Requests\Backend\Operation\Claim\AssignInvestigatorRequest;
use App\Http\Requests\Backend\Operation\Claim\ChooseEmployeeRequest;
use App\Http\Requests\Backend\Operation\Claim\ChooseIncidentTypeRequest;
use App\Http\Requests\Backend\Operation\Claim\ClaimAssessmentProgressiveRequest;
use App\Http\Requests\Backend\Operation\Claim\ClaimAssessmentRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateCurrentEmployeeStateRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateHealthProviderServiceRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMaeMemberMultipleRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMaeMemberRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMedicalExpenseRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateWitnessRequest;
use App\Http\Requests\Backend\Operation\Claim\DoctorNotificationReportUpdateRequest;
use App\Http\Requests\Backend\Operation\Claim\IncidentClosureRequest;
use App\Http\Requests\Backend\Operation\Claim\PdAssessmentProgressiveRequest;
use App\Http\Requests\Backend\Operation\Claim\PdAssessmentRequest;
use App\Http\Requests\Backend\Operation\Claim\RegisterEmployee;
use App\Http\Requests\Backend\Operation\Claim\RegisterNotificationRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateBankDetailsRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateChecklistRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateDetailsRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateInvestigationReportRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateMaeMemberMultipleRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateManualPaymentRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateMonthlyEarningRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateProgressiveNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdatingNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateIndividualContributionRequest;
use App\Jobs\Claim\CheckUploadedDocuments;
use App\Jobs\Claim\CheckUploadedDocumentsNotification;
use App\Jobs\Claim\PostNotificationDms;
use App\Models\Operation\Claim\BenefitTypeClaim;
use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\MedicalExpense;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Claim\NotificationHealthProvider;
use App\Models\Operation\Claim\NotificationInvestigator;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\Portal\Claim;
use App\Models\Operation\Claim\PortalIncident;
use App\Models\Operation\Claim\Witness;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Reporting\ConfigurableReport;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Operation\Claim\AccidentTypeRepository;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\BodyPartInjuryRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\ClaimRepository;
use App\Repositories\Backend\Operation\Claim\DeathCauseRepository;
use App\Repositories\Backend\Operation\Claim\DisabilityStateChecklistRepository;
use App\Repositories\Backend\Operation\Claim\DiseaseKnowHowRepository;
use App\Repositories\Backend\Operation\Claim\DocumentGroupRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\HealthProviderRepository;
use App\Repositories\Backend\Operation\Claim\HealthServiceChecklistRepository;
use App\Repositories\Backend\Operation\Claim\HealthStateChecklistRepository;
use App\Repositories\Backend\Operation\Claim\IncidentClosureRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Access\IncidentUserRepository;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use App\Repositories\Backend\Operation\Claim\MedicalExpenseRepository;
use App\Repositories\Backend\Operation\Claim\MedicalPractitionerRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Claim\NotificationDisabilityStateRepository;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Operation\Claim\NotificationHealthProviderRepository;
use App\Repositories\Backend\Operation\Claim\NotificationHealthProviderServiceRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationHealthProviderRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationHealthProviderPractitionerRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationHealthProviderServiceRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualMedicalExpenseRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationHealthStateRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationDisabilityStateRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationDisabilityStateAssessmentRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualClaimRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualClaimCompensationRepository;

use App\Repositories\Backend\Operation\Claim\NotificationInvestigatorRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\OccupationRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdAmputationRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdImpairmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdInjuryRepository;
use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\Repositories\Backend\Operation\Claim\WitnessRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use App\Repositories\Backend\Reporting\DashboardRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\JobTitleRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Models\Operation\ClaimAccrual\AccrualNotificationReport;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;

use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthState;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderService;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProviderPractitioner;
use App\Models\Operation\ClaimAccrual\AccrualNotificationHealthProvider;
use App\Models\Operation\ClaimAccrual\AccrualNotificationDisabilityState;
use App\Models\Operation\ClaimAccrual\AccrualMedicalExpense;
use App\Models\Operation\ClaimAccrual\AccrualDeaultDate;
use App\Models\Operation\ClaimAccrual\AccrualDeaultDateTrigger;

use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Sysdef\GenderRepository;
use App\Repositories\Backend\Sysdef\IdentityRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentTypeRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualDependentRepository;
use App\Http\Requests\Backend\Operation\Compliance\CreateDependentRequest;

use App\Http\Requests\Backend\Operation\Claim\AccrualDatesRequest;
use App\Models\Operation\ClaimAccrual\AccrualNotificationBenefit;
use App\Models\Operation\ClaimAccrual\AccrualBenefitDisability;
use App\Models\Operation\ClaimAccrual\AccrualClaim;

class ClaimAccrualController extends Controller
{

	use AttachmentHandler, FileHandler;

    /**
     * Display a listing of the resource.
     *updateDocument
     * @return \Illuminate\Http\Response
     */
    protected $notification_reports;
    protected $employees;
    protected $incident_types;
    protected $accident_types;
    protected $disease_know_hows;
    protected  $death_causes;
    protected $districts;
    protected  $member_types;
    protected  $insurances;
    protected  $medical_expenses;
    protected  $notification_investigators;
    protected  $health_providers;
    protected  $medical_practitioners;
    protected  $health_service_checklists;
    protected  $notification_health_providers;
    protected $health_state_checklists;
    protected $disability_state_checklists;
    protected $benefit_types;
    protected $claims;
    protected $witnesses;
    protected $claim_compensations;
    protected $banks;
    protected $bank_branches;
    protected $code_values;
    protected $employers;
    protected $dependents;
    protected $accrual_notification_reports;

    protected $countries;
    protected $genders;
    protected $identities;

    /* start : Handling Documents */
    protected $base = null;
    protected $documents = null;
    protected $uploadedDocuments = null;
    protected $notificationReportFolder = null;
    /* end : Handling Documents */

    /**
     * NotificationReportController constructor.
     * @param NotificationReportRepository $notification_reports
     * @param EmployeeRepository $employees
     * @param IncidentTypeRepository $incident_types
     * @param AccidentTypeRepository $accident_types
     * @param DiseaseKnowHowRepository $disease_know_hows
     * @param DeathCauseRepository $death_causes
     * @param DistrictRepository $districts
     * @param MemberTypeRepository $member_types
     * @param InsuranceRepository $insurances
     * @param MedicalExpenseRepository $medical_expenses
     * @param NotificationInvestigatorRepository $notification_investigators
     * @throws GeneralException
     */
    public function __construct(NotificationReportRepository $notification_reports, EmployeeRepository $employees, IncidentTypeRepository $incident_types, AccidentTypeRepository $accident_types, DiseaseKnowHowRepository $disease_know_hows, DeathCauseRepository $death_causes, DistrictRepository $districts, MemberTypeRepository $member_types, InsuranceRepository $insurances, MedicalExpenseRepository $medical_expenses, NotificationInvestigatorRepository $notification_investigators, AccrualNotificationReportRepository $accrual_notification_reports) {

      $this->notification_reports = $notification_reports;
      $this->accrual_notification_reports = $accrual_notification_reports;
      $this->employees = $employees;
      $this->incident_types = $incident_types;
      $this->accident_types = $accident_types;
      $this->disease_know_hows = $disease_know_hows;
      $this->death_causes = $death_causes;
      $this->districts = $districts;
      $this->member_types = $member_types;
      $this->insurances = $insurances;
      $this->medical_expenses = $medical_expenses;
      $this->accrual_medical_expenses = new AccrualMedicalExpenseRepository();
      $this->accrual_notification_health_states = new AccrualNotificationHealthStateRepository();
      $this->accrual_notification_disability_states = new AccrualNotificationDisabilityStateRepository();
      $this->notification_disability_states = new NotificationDisabilityStateRepository();
      $this->notification_investigators = $notification_investigators;
      $this->health_providers = new HealthProviderRepository();
      $this->medical_practitioners = new MedicalPractitionerRepository();
      $this->health_service_checklists = new HealthServiceChecklistRepository();
      $this->notification_health_providers = new NotificationHealthProviderRepository();
      $this->accrual_notification_health_providers = new AccrualNotificationHealthProviderRepository();
      $this->accrual_notification_health_provider_services = new AccrualNotificationHealthProviderServiceRepository();
      $this->notification_health_provider_services = new NotificationHealthProviderServiceRepository();
      $this->accrual_notification_health_provider_practitioners = new AccrualNotificationHealthProviderPractitionerRepository();
      $this->health_state_checklists = new HealthStateChecklistRepository();
      $this->disability_state_checklists = new DisabilityStateChecklistRepository();
      $this->benefit_types = new BenefitTypeRepository();
      $this->claims= new ClaimRepository();
      $this->witnesses = new WitnessRepository();
      $this->claim_compensations = new ClaimCompensationRepository();
      $this->accrual_claims = new AccrualClaimRepository();
      $this->accrual_claim_compensations = new AccrualClaimCompensationRepository();
      $this->banks = new BankRepository();
      $this->bank_branches = new BankBranchRepository();
      $this->code_values = new CodeValueRepository();
      $this->employers = new EmployerRepository();
      $this->dependents = new DependentRepository();
      $this->accrual_notification_reports = new AccrualNotificationReportRepository();
      $this->accrual_notification_disability_state_assessments = new AccrualNotificationDisabilityStateAssessmentRepository();

      $this->countries = new CountryRepository;
      $this->genders = new GenderRepository;
      $this->dependent_types = new DependentTypeRepository;
      $this->location_types = new LocationTypeRepository;
      $this->identities = new IdentityRepository;
      $this->manual_notification_reports = new ManualNotificationReportRepository();
      $this->accrual_dependents = new AccrualDependentRepository();
      

    }

    public function recall()
    {

      return view('backend.operation.claim.claim_accrual.recall_accrual')
      ->with("stages", $this->code_values->getCodeValuesNotificationStageForSelect())
      ->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all());

    }

    /**
     * @return mixed
     * @throws \Exception
     * @description Recall Notifications : Get for dataTable
     */
    public function getForDataTable()
    {

      return Datatables::of($this->accrual_notification_reports->getForDataTable())
      ->editColumn('status', function ($notification_report) {
        return $notification_report->status_label;
      })
      ->editColumn('current_wf_level', function ($notification_report) {
        return $this->accrual_notification_reports->getWfStatus($notification_report->current_wf_level);
      })
      ->rawColumns(['status','current_wf_level'])
      ->make(true);
    }

    public function profileAccrual(NotificationReport $incident, WorkflowTrackDataTable $workflowtrack)
    {

      $type = $this->accrual_notification_reports->returnFileWorkflowType($incident->id);
      return $this->accrual_notification_reports->profileAccrual($incident)
      ->with("workflowtrack", $workflowtrack)->with('type',$type);

        // return view('backend.operation.claim.claim_accrual.profile')->with("workflowtrack", $workflowtrack);
    }

    public function profileAccrualFromWorkflow($accrual_notification_report_id)
    {

      $accrual = $this->accrual_notification_reports->findOrThrowException($accrual_notification_report_id);
      return redirect('claim_accrual/profile/' . $accrual->notification_report_id . '#general');
    }

    public function initiateWorkflowAccrual($accrual_id,$type)
    {
      $accrual = $this->accrual_notification_reports->findOrThrowException($accrual_id);
      $return = $this->accrual_notification_reports->initiateAccrualWorkflow($accrual->id,$type);
      if ($return) {
        return redirect('claim_accrual/profile/' . $accrual->notification_report_id . '#general')->withFlashSuccess('Accrual Workflow initiated');
      }else{
        return redirect('claim_accrual/profile/' . $accrual->notification_report_id . '#general')->withFlashSuccess('Error Occured. Try again');
      }

    }


    // public function testAccrual($id, WorkflowTrackDataTable $workflowtrack)
    // {
    //   $accrual = $this->accrual_notification_reports->findOrThrowException($id);
    //   return view('backend/operation/claim/claim_accrual/workflow_test')->with("workflowtrack", $workflowtrack)->with('accrual', $accrual);
    // }

    public function createMedicalExpense(NotificationReport $incident)
    {

      $this->checkLevel1Rights($incident->id);
      return view('backend.operation.claim.claim_accrual.medical_expense.medical_expense_create')
      ->with("notification_report", $incident)
      ->with("insurances", $this->insurances->getAll()->pluck('name','id'))
      ->with("member_types", $this->member_types->getMedicalAidMemberTypes());
    }

    public function checkLevel1Rights($resource_id)
    {
      access()->hasWorkflowDefinition(3, 1);
      workflow([['wf_module_group_id' => 3, 'resource_id' => $resource_id]])->checkIfCanInitiateAction(1);
    }

    public function storeMedicalExpense($id, CreateMedicalExpenseRequest $request)
    {
      // dd($request->all());
      $medical_expense = $this->storeMedicalExp($id,$request);
      return redirect('claim_accrual/profile/' . $medical_expense->notification_report_id . '#medical_expense')->withFlashSuccess(trans('alerts.backend.claim.medical_expense_created'));

    }

    public function storeMedicalExp($notification_report_id, $input) {
      $input['amount'] = $this->returnAmount($input);
      $resource_id =  $this->member_types->getMemberTypeValue($notification_report_id, $input) ;
//        $this->checkIfExist($notification_report_id,$input['member_type_id'],$resource_id);
      if ($input['member_type_id'] == 6) {
            //WCF as Employer
        $input['member_type_id'] = 1;
      }
      $medical_expense = AccrualMedicalExpense::create(['notification_report_id' => $notification_report_id,'member_type_id' =>  $input['member_type_id'], 'resource_id' => $resource_id, 'amount' => str_replace(",", "", $input['amount']),'user_id' => access()->user()->id ]);
      return $medical_expense;
    }

    public function getMedicalExpensesForDatatable($id) {
      $first_medical_expense = $this->getFirstMedicalExpenseForNotification($id);
      return Datatables::of($this->notification_reports->getAccrualMedicalExpenses($id))
      ->editColumn('amount', function($medical_expense) {
        return $medical_expense->amount_formatted;
      })
      ->editColumn('member_type_id', function($medical_expense) {
        return !is_null($medical_expense->memberType) ? $medical_expense->memberType->name : null;
      })
      ->addColumn('name', function($medical_expense) {
        return $medical_expense->compensated_entity_name;
        // return 123;
      })
      ->addColumn('cost_incurred', function($medical_expense) use($id) {
        return number_format( $medical_expense->accrualNotificationHealthProviderServices->sum('amount') , 2 , '.' , ',' );

      })
      ->addColumn('action_buttons', function($medical_expense) use ($first_medical_expense)  {
        // return 123;
        return ($medical_expense->notificationReport->accrualNotificationDisabilityStates()->count()) ?  (($medical_expense->accrualNotificationDisabilityStates()->count()) ? $medical_expense->action_buttons : ' ') : (($medical_expense->id == $first_medical_expense->id) ? $medical_expense->action_buttons : ' ');
      })
      ->rawColumns(['action_buttons'])
      ->make(true);
    }

    public function getFirstMedicalExpenseForNotification($notification_report_id)
    {
      $medical_expense = AccrualMedicalExpense::where('notification_report_id', $notification_report_id)->orderBy('id')->first();
      return $medical_expense;
    }

    public function editMedicalExpense(AccrualMedicalExpense $medical_expense)
    {
      $this->checkLevel1Rights($medical_expense->notification_report_id);

      $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $medical_expense->notification_report_id]);
      $assessment_level = $workflow->claimAssessmentLevel();
      $workflow->checkIfCanInitiateActionMultiLevel(1, $assessment_level);

      return view('backend.operation.claim.claim_accrual.medical_expense.medical_expense_edit')
      ->with("medical_expense", $medical_expense)
      ->with("insurances", $this->insurances->getAll()->pluck('name','id'))
      ->with("member_types", $this->member_types->getMedicalAidMemberTypes())
      ->with("user_has_access1", $workflow->userHasAccess(access()->id(), 1 ))
      ->with("user_has_access2", $workflow->userHasAccess(access()->id(), $assessment_level ));
    }

    public function updateMedicalExpense($medical_expense_id, CreateMedicalExpenseRequest $request)
    {

      $medical_expense = $this->updateMedicalExp($medical_expense_id, $request);
      return redirect('claim_accrual/profile/' . $medical_expense->notification_report_id . '#medical_expense')->withFlashSuccess(trans('alerts.backend.claim.medical_expense_updated'));
    }

    public function returnAmount($input){
      $amount = $input['amount'] == null ? 0 : $input['amount'];
      return $amount;
    }

    public function updateMedicalExp($id,$input) {
      $input['amount'] = $this->returnAmount($input);
      $medical_expense = $this->findOrThrowExceptionMedicalExp($id);
      $resource_id =  $this->member_types->getMemberTypeValue($medical_expense->notification_report_id, $input) ;
      if ($input['member_type_id'] == 6) {
            //WCF as Employer
        $input['member_type_id'] = 1;
      }
      $medical_expense->update(['amount' => str_replace(",", "", $input['amount']), 'resource_id' => $resource_id, 'member_type_id' =>  $input['member_type_id']]);
      return $medical_expense;
    }

    public function findOrThrowExceptionMedicalExp($id)
    {
      $medical_expense = AccrualMedicalExpense::find($id);

      if (!is_null($medical_expense)) {
        return $medical_expense;
      }
      throw new GeneralException(trans('exceptions.backend.claim.medical_expense_not_found'));
    }

    public function createHealthProviderService(NotificationReport $incident)
    {
      $this->checkLevel1Rights($incident->id);
      $health_service_checklists = $this->health_service_checklists->getAll();
      $incident_date = $this->notification_reports->getIncidentDate($incident);
      $medical_expenses = AccrualMedicalExpense::where('notification_report_id', $incident->id)->doesntHave('accrualNotificationHealthProviders')->get()->pluck('compensated_entity_name_with_amount','id');
      return view('backend.operation.claim.claim_accrual.health_provider.health_provider_service_create')
      ->with("notification_report", $incident)
      ->with("health_providers", [])
      ->with("health_service_checklists", $health_service_checklists)
      ->with("incident_date", $incident_date)
      ->with("medical_expenses", $medical_expenses);

    }

    public function storeHealthProviderService($id, CreateHealthProviderServiceRequest $request)
    {
      $this->checkLevel1Rights($id);
      $health_provider_service = $this->storeHealthProviderSvc($id,$request->all());
      return redirect('claim_accrual/profile/' . $id . '#medical')->withFlashSuccess(trans('alerts.backend.claim.health_provider_service_created'));
    }

    public function storeHealthProviderSvc($id, $input)
    {
      return DB::transaction(function () use ($id, $input) {
            // Store notification health provider
        $notification_health_provider = $this->accrual_notification_health_providers->create($id, $input);
        $notification_health_provider_id = $notification_health_provider->id;
            // Store notification health provider practitioner
        $this->accrual_notification_health_provider_practitioners->create($notification_health_provider_id, $input);

            //Store notification health provider services

        foreach ($input as $key => $value) {
          if (strpos($key, 'feedback') !== false) {
            $health_service_checklist_id = substr($key, 8);
            if ($input['feedback' . $health_service_checklist_id] == 1) {
              $notification_report_provider_service = $this->accrual_notification_health_provider_services->create($notification_health_provider_id, $health_service_checklist_id, $input);
            }
          }
        }
//          Update Total medical service cost. MAE
//            $this->medical_expenses->updateTotalMedicalServicesCost($input['medical_expense_id']);
      });

    }

    public function getHealthProviderServices($id)
    {
      return $this->accrual_notification_health_provider_services->query()->whereHas("accrualNotificationHealthProvider", function ($query) use ($id) {
        $query->where('notification_report_id', $id);
      })->get();

    }

    public function getHealthProviderServices1($id)
    {
      return $this->notification_health_provider_services->query()->whereHas("notificationHealthProvider", function ($query) use ($id) {
        $query->where('notification_report_id', $id);
      })->get();

    }

     public function getHealthProviderServicesForDatatable1($id) {
      return Datatables::of($this->getHealthProviderServices1($id))
      ->editColumn('amount', function($notification_health_provider_service) {
        return $notification_health_provider_service->amount_formatted;
      })
      ->editColumn('from_date', function($notification_health_provider_service) {
        return ($notification_health_provider_service->from_date) ? $notification_health_provider_service->from_date_formatted : ' ';
      })
      ->editColumn('to_date', function($notification_health_provider_service) {
        return ($notification_health_provider_service->to_date) ? $notification_health_provider_service->to_date_formatted : ' ';
      })
      ->addColumn('health_provider', function($notification_health_provider_service) {
        return $notification_health_provider_service->notificationHealthProvider->healthProviderWithTrashed->name;
      })
      ->addColumn('medical_practitioner', function($notification_health_provider_service) {
        return $notification_health_provider_service->notificationHealthProvider->notificationHealthProviderPractitioner->medicalPractitioner->fullname_with_external_id;
      })
      ->editColumn('health_service_checklist_id', function($notification_health_provider_service) {
        return $notification_health_provider_service->healthServiceChecklist->name;
      })
      ->addColumn('medical_expense', function($notification_health_provider_service) {
        // if($notification_health_provider_service->accrual_medical_expense_id != null)
        return $notification_health_provider_service->notificationHealthProvider->accrualMedicalExpense->compensated_entity_name_with_amount;
      })
      ->addColumn('rank', function($notification_health_provider_service) {
        return $notification_health_provider_service->notificationHealthProvider->rank;
      })
      ->make(true);
    }

    public function getHealthProviderServicesForDatatable($id) {
      return Datatables::of($this->getHealthProviderServices($id))
      ->editColumn('amount', function($notification_health_provider_service) {
        return $notification_health_provider_service->amount_formatted;
      })
      ->editColumn('from_date', function($notification_health_provider_service) {
        return ($notification_health_provider_service->from_date) ? $notification_health_provider_service->from_date_formatted : ' ';
      })
      ->editColumn('to_date', function($notification_health_provider_service) {
        return ($notification_health_provider_service->to_date) ? $notification_health_provider_service->to_date_formatted : ' ';
      })
      ->addColumn('health_provider', function($notification_health_provider_service) {
        return $notification_health_provider_service->accrualNotificationHealthProvider->accrualHealthProviderWithTrashed->name;
      })
      ->addColumn('medical_practitioner', function($notification_health_provider_service) {
        return $notification_health_provider_service->accrualNotificationHealthProvider->accrualNotificationHealthProviderPractitioner->medicalPractitioner ? $notification_health_provider_service->accrualNotificationHealthProvider->accrualNotificationHealthProviderPractitioner->medicalPractitioner->fullname_with_external_id : null;
      })
      ->editColumn('health_service_checklist_id', function($notification_health_provider_service) {
        return $notification_health_provider_service->healthServiceChecklist->name;
      })
      ->addColumn('medical_expense', function($notification_health_provider_service) {
        // if($notification_health_provider_service->accrual_medical_expense_id != null)
        return $notification_health_provider_service->accrualNotificationHealthProvider->accrualMedicalExpense->compensated_entity_name_with_amount;
      })
      ->addColumn('rank', function($notification_health_provider_service) {
        return $notification_health_provider_service->accrualNotificationHealthProvider->rank;
      })
      ->make(true);
    }

    /**
     * @param NotificationHealthProvider $notification_health_provider
     * @return mixed
     * @throws GeneralException
     */
    public function editHealthProviderService(AccrualNotificationHealthProvider $notification_health_provider)
    {
      // dd($notification_health_provider->notificationReport);
      $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $notification_health_provider->notification_report_id]);
      $assessment_level = $workflow->claimAssessmentLevel();
      $workflow->checkIfCanInitiateActionMultiLevel(1, $assessment_level);

      $health_service_checklists = $this->health_service_checklists->getAll();
      $incident_date = $this->notification_reports->getIncidentDate($notification_health_provider->notificationReport);
      return view('backend.operation.claim.claim_accrual.health_provider.health_provider_service_edit')
      ->with("notification_health_provider", $notification_health_provider)
      ->with("health_providers", $this->health_providers->query()->where("id", $notification_health_provider->health_provider_id)->get()->pluck('fullname_info', 'id'))
      ->with("medical_practitioners", $this->medical_practitioners->query()->where("id", $notification_health_provider->accrualNotificationHealthProviderPractitioner->medical_practitioner_id)->get()->pluck('fullname_with_external_id', 'id'))
      ->with("health_service_checklists", $health_service_checklists)
      ->with("incident_date",$incident_date)
      ->with("medical_expenses", $this->accrual_medical_expenses->query()->where('notification_report_id', $notification_health_provider->notification_report_id)->get()->pluck('compensated_entity_name_with_amount', 'id'))
      ->with("user_has_access1", $workflow->userHasAccess(access()->id(), 1 ))
      ->with("user_has_access2", $workflow->userHasAccess(access()->id(), $assessment_level ));
    }

    /**
     * @param $notification_health_provider_id
     * @param CreateHealthProviderServiceRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function updateHealthProviderService($notification_health_provider_id, CreateHealthProviderServiceRequest $request)
    {
      $notification_health_provider = $this->accrual_notification_health_providers->findOrThrowException($notification_health_provider_id);

      $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $notification_health_provider->notification_report_id]);
      $assessment_level = $workflow->claimAssessmentLevel();
      $workflow->checkIfCanInitiateActionMultiLevel(1,$assessment_level);

      $notification_health_provider = $this->updateHealthProviderSvc($notification_health_provider_id,$request->all());
      return redirect('claim_accrual/profile/' . $notification_health_provider->notification_report_id . '#medical')->withFlashSuccess(trans('alerts.backend.claim.health_provider_service_updated'));
    }

    public function updateHealthProviderSvc($notification_health_provider_id, $input)
    {

      return DB::transaction(function () use ($notification_health_provider_id, $input) {
            // update notification health provider
        $notification_health_provider = $this->accrual_notification_health_providers->update($notification_health_provider_id, $input);

            // update notification health provider practitioner
        $this->accrual_notification_health_provider_practitioners->update($notification_health_provider_id, $input);

            //Update notification health provider services

        foreach ($input as $key => $value) {
          if (strpos($key, 'feedback') !== false) {
            $health_service_checklist_id = substr($key, 8);
            $notification_report_provider_service = $this->accrual_notification_health_provider_services->update($notification_health_provider_id, $health_service_checklist_id, $input);

          }
        }

            //          Update Total medical service cost. MAE
//            $this->medical_expenses->updateTotalMedicalServicesCost($input['medical_expense_id']);

        return $notification_health_provider;
      });

    }


    public function getAssessmentOpener($id) {
      $first_medical_expense = $this->accrual_medical_expenses->getFirstMedicalExpenseForNotification($id);
      $medical_expense_with_disability_state = $this->accrual_medical_expenses->getMedicalExpenseWithDisabilityState($id);
      $medical_expense_for_assessment = ($medical_expense_with_disability_state) ? $medical_expense_with_disability_state : $first_medical_expense;
      return Datatables::of($this->getMedicalExpenseEntities($id))
      ->addColumn('member_type_id', function($medical_expense_entity) use ($id, $medical_expense_for_assessment) {
        return !is_null($medical_expense_for_assessment->memberType) ? $medical_expense_for_assessment->memberType->name : null;
      })
      ->addColumn('name', function($medical_expense_entity) use ($id, $medical_expense_for_assessment) {
        return $medical_expense_for_assessment->compensated_entity_name;
      })
      ->addColumn('action_buttons', function($medical_expense_entity) use ($medical_expense_for_assessment)  {

        return $medical_expense_for_assessment->accrual_action_buttons;

      })
      ->rawColumns(['action_buttons'])
      ->make(true);
    }

    public function getMedicalExpenseEntities($id)
    {
      return DB::Table('accrual_medical_expenses')->select(['notification_report_id'])->distinct('notification_report_id')->where('notification_report_id', $id);
    }


    public function createCurrentEmployeeState(NotificationReport $incident)
    {
      $assessment_level = $this->getAssessmentLevel($incident->id);
      $this->checkLevelRights($incident->id, $assessment_level);

      $health_state_checklists = $this->health_state_checklists->getAll();
      $disability_state_checklists = $this->disability_state_checklists->getAll();
      $incident_date = $this->notification_reports->getIncidentDate($incident);
      return view('backend.operation.claim.claim_accrual.employee_states.current_employee_state_create')
      ->with("notification_report", $incident)
      ->with("health_state_checklists", $health_state_checklists)
      ->with("disability_state_checklists", $disability_state_checklists)
      ->with("incident_date", $incident_date)
      ->with("medical_expenses", $this->accrual_medical_expenses->query()->where('notification_report_id', $incident->id)->get()->pluck('compensated_entity_name_with_amount',
        'id'));
    }

    public function storeCurrentEmployeeState($id, CreateCurrentEmployeeStateRequest $request)
    {
      // dd($request->all());
      $assessment_level = $this->getAssessmentLevel($id);
      $this->checkLevelRights($id, $assessment_level);
      $current_employee_state = $this->storeCurrentEmployeeStt($id,$request->all());
      return redirect('claim_accrual/profile/' . $id . '#current_employee_state')->withFlashSuccess(trans('alerts.backend.claim.current_employee_state_created'));
    }

    public function storeCurrentEmployeeStt($id, $input)
    {
      return DB::transaction(function () use ($id, $input) {
            // Store notification health state
//            $notification_health_state =   $this->notification_health_states->create($id,$input);
//            $notification_health_state_id   = $notification_health_state->id;

        foreach ($input as $key => $value) {
          if (strpos($key, 'health_state') !== false) {
            $health_state_checklist_id = substr($key, 12);
            if ($input['health_state' . $health_state_checklist_id] == 1) {
              $notification_health_state = $this->accrual_notification_health_states->create($health_state_checklist_id,$id, $input);
            }
          }
        }
            //Store notification disability state

        foreach ($input as $key => $value) {
          if (strpos($key, 'disability_state') !== false) {
            $disability_state_checklist_id = substr($key, 16);
            if ($input['disability_state' . $disability_state_checklist_id] == 1) {
              $input['notification_report_id'] = $id;
              $notification_disability_state = $this->accrual_notification_disability_states->create($disability_state_checklist_id, $input);
            }
          }
        }

      });

    }

    // public function getDisabilityStateForDatatable($id) {
    //   return Datatables::of($this->getDisabilityState($id))
    //   ->addColumn('medical_expense', function($notification_disability_state) {
    //     return $notification_disability_state->medicalExpense->compensated_entity_name;
    //   })
    //   ->editColumn('from_date', function($notification_disability_state) {
    //     return $notification_disability_state->from_date_formatted;
    //   })
    //   ->editColumn('to_date', function($notification_disability_state) {
    //     return $notification_disability_state->to_date_formatted;
    //   })
    //   ->editColumn('disability_state_checklist_id', function($notification_disability_state) {
    //     return $notification_disability_state->disabilityStateChecklist->name;
    //   })


    //   ->make(true);
    // }

    public function getDisabilityStateForDatatable($id) {
      return Datatables::of($this->getAccrualDisabilityState($id))
      ->addColumn('medical_expense', function($notification_disability_state) {
        return $notification_disability_state->accrualMedicalExpense ? $notification_disability_state->accrualMedicalExpense->compensated_entity_name
        : null;
      })
      ->editColumn('from_date', function($notification_disability_state) {
        return $notification_disability_state->from_date_formatted;
      })
      ->editColumn('to_date', function($notification_disability_state) {
        return $notification_disability_state->to_date_formatted;
      })
      ->editColumn('disability_state_checklist_id', function($notification_disability_state) {
        return $notification_disability_state->disabilityStateChecklist->name;
      })


      ->make(true);
    }

    public function getHealthStateForDatatable($id) {

      return Datatables::of($this->getHealthState($id))
      ->editColumn('health_state_checklist_id', function($notification_health_state) {
        return $notification_health_state->healthStateChecklist->name;
      })
      ->make(true);
    }

    public function getHealthState($id)
    {
      return $this->accrual_notification_health_states->query()->where('notification_report_id', $id)->get();

    }

    public function getAccrualDisabilityState($id)
    {
      // return $this->accrual_notification_disability_states->query()->whereHas('accrualMedicalExpense', function ($query) use ($id) {
      //   $query->where('notification_report_id', $id);
      // })->orderBy('disability_state_checklist_id', 'asc')->get();

      return $this->accrual_notification_disability_states->query()->where('notification_report_id', $id)->orderBy('disability_state_checklist_id', 'asc')->get();

    }

     public function getDisabilityState($id)
    {
      return $this->notification_disability_states->query()->whereHas('medicalExpense', function ($query) use ($id) {
        $query->where('notification_report_id', $id);
      })->orderBy('disability_state_checklist_id', 'asc')->get();

    }

    public function editCurrentEmployeeState(AccrualMedicalExpense $medical_expense)
    {
      /*Check if has right*/
      $notification_report_id = $medical_expense->notification_report_id;
      $assessment_level = $this->getAssessmentLevel($notification_report_id);
      $this->checkLevelRights($notification_report_id, $assessment_level);
      /*end check*/
      $incident = $medical_expense->notificationReport;
      $health_state_checklists = $this->health_state_checklists->getAll();
      $disability_state_checklists = $this->disability_state_checklists->getAll();
      $incident_date = $this->notification_reports->getIncidentDate($incident);

      return view('backend.operation.claim.claim_accrual.employee_states.current_employee_state_edit')
      ->with("notification_report", $incident)
      ->with("medical_expense", $medical_expense)
      ->with("health_state_checklists", $health_state_checklists)
      ->with("disability_state_checklists", $disability_state_checklists)
      ->with("incident_date", $incident_date)
      ->with("medical_expenses", $this->accrual_medical_expenses->query()->where('notification_report_id', $incident->id)->get()->pluck('compensated_entity_name_with_amount',
        'id')) ;
    }

    public function updateCurrentEmployeeState($id, CreateCurrentEmployeeStateRequest $request)
    {
      $assessment_level = $this->getAssessmentLevel($id);
      $this->checkLevelRights($id, $assessment_level);
      $current_employee_state = $this->updateCurrentEmployeeStt($id,$request->all());
      return redirect('claim_accrual/profile/' . $id . '#current_employee_state')->withFlashSuccess(trans('alerts.backend.claim.current_employee_state_updated'));
    }

    public function updateCurrentEmployeeStt($id, $input)
    {

      return DB::transaction(function () use ($id, $input) {
            // update notification health state
        foreach ($input as $key => $value) {
          if (strpos($key, 'health_state') !== false) {
            $health_state_checklist_id = substr($key, 12);
                    //                    if ($input['feedback'. $health_service_checklist_id]) {
            $notification_health_state = $this->accrual_notification_health_states->update($health_state_checklist_id,$id, $input);
//                    }
          }
        }


            //Update notification health provider services

        foreach ($input as $key => $value) {
          if (strpos($key, 'disability_state') !== false) {
            $disability_checklist_id = substr($key, 16);
            $notification_disability_state = $this->accrual_notification_disability_states->update($disability_checklist_id, $input);
          }
        }

      });

    }

    public function getAssessmentLevel($id)
    {
      $wf_module_group_id =3;
      $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id]);
      $assessment_level = $workflow->claimAssessmentLevel();
      return $assessment_level;
    }

    public function checkLevelRights($resource_id, $level_id, $wf_module_group_id = 3){
      access()->hasWorkflowDefinition($wf_module_group_id,$level_id);
      workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction($level_id);
    }

    public function claimAssessmentChecklist(AccrualMedicalExpense $medical_expense) {
        //$medical_expense = $this->medical_expenses->findOrThrowException($medical_expense_id);
      $incident = $medical_expense->notificationReport;
      $incident_date = $this->notification_reports->getIncidentDate($incident);
      $monthly_earning_summary = $this->employees->getMonthlyEarningBeforeIncident($medical_expense->notificationReport->employee_id, $incident->id);
      $monthly_earning_from_contrib = $monthly_earning_summary['monthly_earning'];

      $monthly_earning_approved = ($incident->accrualClaim) ? $incident->accrualClaim->monthly_earning : $monthly_earning_from_contrib;
      $dob = $this->employees->getDob($medical_expense->notificationReport->employee_id);
      $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $medical_expense->notification_report_id]);
      $assessment_level = $workflow->claimAssessmentLevel();

      $date_of_mmi = ($incident->accrualClaim) ? $incident->accrualClaim->date_of_mmi : null;

      return view('backend.operation.claim.claim_accrual.assessment.claim_assessment_checklist')
      ->with("medical_expense", $medical_expense)
      ->with("claimed_expense", $this->accrual_medical_expenses->calculateExpenses($medical_expense->id, 1))
      ->with("notification_disability_states", $medical_expense->accrualNotificationDisabilityStates)
      ->with("age", Carbon::parse($dob)->diff(Carbon::parse($incident_date))->y)
      ->with("benefit_type_ids", $this->benefit_types->getbenefitIds())
      ->with("monthly_earning", $monthly_earning_summary)
      ->with("monthly_earning_approved", $monthly_earning_approved)
      ->with("user_has_access", $workflow->userHasAccess(access()->id(), $assessment_level ))
      ->with("date_of_mmi", $date_of_mmi);

    }

    public function claimAssessment($medical_expense_id, ClaimAssessmentRequest $request) {

      $medical_expense = $this->accrual_medical_expenses->findOrThrowException($medical_expense_id);
      // $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $medical_expense->notification_report_id]);
      // $assessment_level = $workflow->claimAssessmentLevel();
      // $workflow->checkLevel($assessment_level);

      $notification_report = $this->accrualClaimAssessment($medical_expense, $request->all());
      return redirect('claim_accrual/profile/' . $notification_report->id . '#general')->withFlashSuccess(trans('alerts.backend.claim.claim_assessed'));
    }

    public function accrualClaimAssessment($medical_expense, $input) {
      return DB::transaction(function () use ($medical_expense, $input) {
        $claim = $medical_expense->notificationReport->accrualClaim;
        // dd($input);

            //Notification Disability State Assessment
        foreach ($input as $key => $value) {
          if (strpos($key, 'days') !== false) {
            $notification_disability_state_id = substr($key, 4);
            $this->accrual_notification_disability_state_assessments->updateOrCreate($notification_disability_state_id, $input);
          }
        }

            //Update PI claim
        if (isset($input['pd'])) {
          $this->accrual_claims->updatePd($claim->id, $input);
        }
            //update claim - exit code

        return $medical_expense->notificationReport;

      });
    }


    public function updateOrCreate($notification_disability_state_id, $input) {
      $notification_disability_state_assessment = DB::table('accrual_benefit_disabilities')->updateOrCreate(
        ['notification_disability_state_id' => $notification_disability_state_id] , ['user_id' => access()->user()->id, 'assessed_days' => $input['days'.$notification_disability_state_id]] );
      return $notification_disability_state_assessment;
    }


    public function getCheckerDataTable()
    {
      return Datatables::of($this->accrual_notification_reports->getForCheckerDataTable())
      ->editColumn('status', function ($notification_report) {
        return $notification_report->status_label;
      })
      ->rawColumns(['status'])
      ->make(true);
    }

    /*dependents for death */
    public function create_new($employee_id, $notification_report_id)
    {
        //
      // $this->checkIfCanInitiateActionAtLevel($employee_id, 1);

      $employee = $this->employees->findOrThrowException($employee_id);
      // dd($employee);
      /*death date / child age limits*/
      $death_incident_query = $employee->notificationReports()->whereIn('incident_type_id',[3,4,5]);
      $death_date =  ($death_incident_query->count() > 0)  ? standard_date_format($death_incident_query->first()->incident_date) : standard_date_format(Carbon::now());
      $child_age_limit = sysdefs()->data()->payroll_child_limit_age;
        $child_age_limit_date = Carbon::parse($death_date)->subYears($child_age_limit)->format('Y-n-j');//18yrs
        /*end child / death date*/
        $other_dependents = $employee->accrualDependents;
        return view('backend.operation.claim.claim_accrual.dependents.create')
        ->with([
          'employee' => $employee,
          'countries' => $this->countries->getAll()->pluck('name', 'id'),
          'genders' =>  $this->genders->getAll()->pluck('name', 'id'),
          'dependent_types' => $this->dependent_types->getAll()->pluck('name', 'id'),
          'other_dependents' => $other_dependents->pluck('name', 'id'),
          'location_types' => $this->location_types->getAll()->pluck('name', 'id'),
          'identities' => $this->identities->getAll()->pluck('name', 'id'),
          'banks' => $this->banks->getActiveBanksForSelect(),
          'bank_branches' => $this->bank_branches->getAll()->pluck('name', 'id'),
          'death_date' => $death_date,
          'child_age_limit_date' => $child_age_limit_date,
          'notification_report_id' => $notification_report_id
        ]);
      }

      /*Check if can initiate action level*/
      public function checkIfCanInitiateActionAtLevel($employee_id, $level)
      {
        access()->hasWorkflowDefinition(3,$level);

        $resource_id = ($this->employees->findNotificationReportDeathIncident($employee_id)) ? $this->employees->findNotificationReportDeathIncident($employee_id)->id : 0;
        if ($resource_id) {
          $wf_module_id = (new WfModuleRepository())->claimBenefitModule()[0];
          $wf_resource = (new NotificationWorkflowRepository())->query()->where('notification_report_id', $resource_id)->where('wf_module_id', $wf_module_id)->first();

          if($wf_resource){
            $workflow = new Workflow(['wf_module_group_id' => 3, "resource_id" => $wf_resource->id, 'type'=> 10]);
            $workflow->checkIfCanInitiateAction($level);
          }

        }

      }

      public function store_new($employee_id, $notification_report_id,CreateDependentRequest $request)
      {
        // access()->hasWorkflowDefinition(3,1);
        $dependent = $this->accrual_dependents->create($employee_id,$request->all());
        return redirect('claim_accrual/profile/' . $notification_report_id . '#dependents')->withFlashSuccess(trans('alerts.backend.member.dependent_created'));
      }

        //Dependents
      public function getDependentsForDataTable($id) {
        $dependentType = new DependentTypeRepository();
        return Datatables::of($this->employees->getAccrualDependents($id))
        ->editColumn('survivor_gratuity_percent', function($dependent) {
          return $dependent->survivor_gratuity_percent;
        })
        ->editColumn('survivor_pension_percent', function($dependent) {
          return $dependent->survivor_pension_percent;
        })
        ->editColumn('funeral_grant_percent', function($dependent) {
          return $dependent->funeral_grant_percent;
        })
        ->editColumn('dob', function($dependent) {
          return $dependent->dob_formatted;
        })
        ->editColumn('dependent_type_id', function($dependent) use ($dependentType){
          return ($dependent->dependent_type_id) ? $dependentType->query()->where('id',
            $dependent->dependent_type_id)->first()->name : ' ';
        })
        ->addColumn('bank_id', function($dependent) {
          return ($dependent->bank_branch_id) ? $dependent->bankBranch->bank->name : ' ';
        })
        ->editColumn('bank_branch_id', function($dependent) {
          return ($dependent->bank_branch_id) ? $dependent->bankBranch->name : ' ';
        })
        ->addColumn('name', function($dependent) {
          return $dependent->firstname . " " . $dependent->middlename  . " " . $dependent->lastname;
        })

        ->make(true);
      }

      public function editDependent($id, $employee_id, $notification_report_id)
      {
        //
        // $this->checkIfCanInitiateActionAtLevel($employee_id, 1);
        $employee = $this->employees->findOrThrowException($employee_id);
        $other_dependents = $employee->accrualDependents;
        $dependent = $this->accrual_dependents->findOrThrowException($id);
        $pivot = $dependent->employees()->where('employee_id', $employee_id)->first()->pivot;
        /*death date / child age limits*/
        $death_incident_query = $employee->notificationReports()->whereIn('incident_type_id',[3,4,5]);
        $death_date =  ($death_incident_query->count() > 0)  ? standard_date_format($death_incident_query->first()->incident_date) : standard_date_format(Carbon::now());
        $child_age_limit = sysdefs()->data()->payroll_child_limit_age;
        $child_age_limit_date = Carbon::parse($death_date)->subYears($child_age_limit)->format('Y-n-j');//18yrs
        /*end child / death date*/
        return view('backend.operation.claim.claim_accrual.dependents.edit')
        ->with([
          'dependent' => $dependent,
          'countries' => $this->countries->getAll()->pluck('name', 'id'),
          'genders' => $this->genders->getAll()->pluck('name', 'id'),
          'dependent_types' => $this->dependent_types->getAll()->pluck('name', 'id'),
          'other_dependents' => $other_dependents->pluck('name', 'id'),
          'location_types' => $this->location_types->getAll()->pluck('name', 'id'),
          'identities' => $this->identities->getAll()->pluck('name', 'id'),
          'banks' => $this->banks->getAll()->pluck('name', 'id'),
          'bank_branches' => $this->bank_branches->query()->get()->pluck('name', 'id'),
          'employee' => $employee,
          'pivot' => $pivot,
          'death_date' => $death_date,
          'notification_report_id' => $notification_report_id
        ]);
      }

      public function updateDependent( $id, $notification_report_id, CreateDependentRequest $request)
      {

        $employee_id = $request['employee_id'];
        // access()->hasWorkflowDefinition(3,1);
        // $resource_id = ($this->accrual_dependents->findNotificationReport($id, $employee_id)) ? $this->accrual_dependents->findNotificationReport($id,$employee_id)->id : 0;
        // if ($resource_id){
        //   workflow([['wf_module_group_id' => 3, 'resource_id' => $resource_id]])->checkIfCanInitiateAction(1);
        // }

        $dependent = $this->accrual_dependents->update($id,$request->all());
        return redirect('claim_accrual/profile/' . $notification_report_id . '#dependents')->withFlashSuccess(trans('alerts.backend.member.dependent_updated'));
      }

      public function accrualDates()
      {
        $fin_year = $this->accrual_notification_reports->currentFinancialYear();
        $array = explode('/', $fin_year);
        $last_fin = $array[1];
        $fin_year_id = $this->accrual_notification_reports->getCurrentFinYearId();
        $date = AccrualDeaultDate::where('fin_year_id',$fin_year_id)->where('unit_id', auth()->user()->unit_id)->first();

        if (is_null($date)) {
          $from_date = null;
          $to_date = null;
        }else{
          $from_date = $date->from_date;
          $to_date = $date->to_date;
        }
        // dd('dd');
        /*Check if has right*/
        // $notification_report_id = $medical_expense->notification_report_id;
        // $assessment_level = $this->getAssessmentLevel($notification_report_id);
        // $this->checkLevelRights($notification_report_id, $assessment_level);
        // /*end check*/
        // $incident = $medical_expense->notificationReport;
        // $health_state_checklists = $this->health_state_checklists->getAll();
        // $disability_state_checklists = $this->disability_state_checklists->getAll();
        // $incident_date = $this->notification_reports->getIncidentDate($incident);

        return view('backend.operation.claim.claim_accrual.accrual_dates')
        ->with("last_fin", $last_fin)
        ->with("from_date", $from_date)
        ->with("to_date", $to_date);
        // ->with("medical_expense", $medical_expense)
        // ->with("health_state_checklists", $health_state_checklists)
        // ->with("disability_state_checklists", $disability_state_checklists)
        // ->with("incident_date", $incident_date)
        // ->with("medical_expenses", $this->accrual_medical_expenses->query()->where('notification_report_id', $incident->id)->get()->pluck('compensated_entity_name_with_amount',
        //   'id')) ;
      }

      public function updateAccrualDates(AccrualDatesRequest $request){
        // dd($request->all());
        $fin_year_id = $this->accrual_notification_reports->getCurrentFinYearId();
        $data = [
          'from_date' => $request->from_date,
          'to_date' => $request->to_date,
          'added_by' => auth()->user()->id,
          'unit_id' => auth()->user()->unit_id,
          'fin_year_id' => $fin_year_id
        ];

        $fin_year_id = $this->accrual_notification_reports->getCurrentFinYearId();

        $date = AccrualDeaultDate::where('fin_year_id',$fin_year_id)->where('unit_id', auth()->user()->unit_id)->first();
        if (is_null($date)) {
          AccrualDeaultDate::create($data);
        }else{
          $date->update($data);
          $data2 = [
            'from_date' => $date->from_date,
            'to_date' => $date->to_date,
            'added_by' => $date->added_by,
            'unit_id' => $date->unit_id,
            'accrual_deault_date_id' => $date->id,
            'changed_by' => auth()->user()->id,
            'fin_year_id' => $date->fin_year_id,
          ];
          AccrualDeaultDateTrigger::create($data2);
        }


        return redirect('claim_accrual/notification_report/accrual_dates')->withFlashSuccess('Accrual dates added');
      }

      public function removeAccrual($notification_report_id){
        // dd($notification_report_id);
        //remember to update the deleter
        // check if is allocated user
        return DB::transaction(function () use ($notification_report_id) {
          $notification = $this->notification_reports->findOrThrowException($notification_report_id);
          if ($notification->allocated != auth()->user()->id) {
            throw new GeneralException('You\'re not checklist user of this file.');
          }else{
            $notification->update(['is_accrued' => false, 'accrued_status' => 1]);
            $accrual_notification = AccrualNotificationReport::where('notification_report_id', $notification_report_id)->first();


            $benefits = AccrualNotificationBenefit::where('accrual_notification_report_id', $accrual_notification->id)->get();

            foreach ($benefits as $benefit) {
              $disabilities = AccrualBenefitDisability::where('accrual_notification_benefit_id', $benefit->id)->get();
              foreach ($disabilities as $disability) {
                $disability->delete();
              }
              $benefit->delete();
            }
            $deleted = $accrual_notification->delete();
            if ($deleted == true) {
              $deleted = AccrualClaim::where('notification_report_id', $notification_report_id)->delete();
              if($deleted == true){
                return redirect('claim_accrual/recall')->withFlashSuccess('Accrual file successfuly deleted!');
              }else{
                throw new GeneralException('Can\'t delete this file, an error occurred.');
              }

            }else{
              throw new GeneralException('Can\'t delete this file, an error occurred.');
            }

          }

        });
      }


    }
