<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;
use App\DataTables\Claim\InvestigationPlan\InvestigationPlanDataTable;
use Yajra\Datatables\Services\DataTable;
use Yajra\Datatables\Datatables;
use App\DataTables\WorkflowTrackDataTable;

use Carbon\Carbon;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationOfficerRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanUserRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;
use App\Http\Requests\Backend\Operation\Claim\InvestigationPlan\CreateInvestigationPlanRequest;
use App\Http\Requests\Backend\Operation\Claim\InvestigationPlan\AssignPlanInvestigatorsRequest;
use App\Http\Requests\Backend\Operation\Claim\InvestigationPlan\UpdateInvestigationPlanRequest;
use App\Http\Requests\Backend\Operation\Claim\InvestigationPlan\AssignPlanFilesRequest;

class InvestigationPlanController extends Controller
{

	public function __construct(){
		$this->investigation_plan_repo = new InvestigationPlanRepository();
		$this->investigation_officer_repo = new InvestigationOfficerRepository();
		$this->investigator_repo = new InvestigationPlanUserRepository();
		$this->plan_files_repo = new InvestigationPlanNotificationsRepository();
		$this->middleware('access.routeNeedsPermission:configure_notification_defaults', ['only' => ['defaults','saveDefaults']]);
	}

	public function index(InvestigationPlanDataTable $dataTable)
	{
		return $dataTable->render('backend.operation.claim.investigation_plan.index',[
			"is_coordinator" => count($this->investigation_officer_repo->findCordinatorByUserId(access()->user()->id))
		]);
	}


	public function defaults()
	{
		return view("backend/operation/claim/investigation_plan/defaults")
		->with("users", (new UserRepository())->query()->get()->pluck("name", "id"))
		->with("claim_users", (new UserRepository())->query()->where('unit_id',14)->whereIn('designation_id',[4,5,6,7])->get()->pluck("name", "id"))
		->with("investigation_masters", $this->investigation_officer_repo->getInvestigationMastersId())
		->with("authorised_investigators", $this->investigation_officer_repo->getAllInvestigatorsId());
	}

	public function saveDefaults(Request $request)
	{
		$result = $this->investigation_officer_repo->updateInvestigationOfficers($request->all());
		return $result ? response()->json(['success' => true, 'message' => 'Success, All changes have been updated!']) : response()->json(['success' => false, 'message' => 'Error, Something went wrong! Please try again']);

	}

	public function profile($plan_id,WorkflowTrackDataTable $workflowtrack)
	{
		$profile = $this->investigation_plan_repo->findOrThrowException($plan_id);
		$assigned_investigators = $this->investigator_repo->returnPlanInvestigatorsId($plan_id);
		$investigators = $this->investigation_officer_repo->returnAllInvestigationOfficers();
		$type = (empty($profile->total_budget) || $profile->total_budget <= 0) ? 1 : 2;
		return view("backend/operation/claim/investigation_plan/profile/show")->with("profile", $profile)
		->with("plan_investigators", $this->investigator_repo->returnPlanInvestigators($plan_id))
		->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all())
		->with("type",$type)
		->with('investigators',$investigators)->with('assigned_investigators',$assigned_investigators)->with("workflowtrack", $workflowtrack);
	}

	public function create()
	{
		$this->investigation_plan_repo->canUserCreateNewPlan(access()->user()->id);
		return view('backend.operation.claim.investigation_plan.profile.create')
		->with("types", $this->investigation_plan_repo->investigationTypes())
		->with("limits", $this->investigation_plan_repo->planMaximumLimits())
		->with("categories", $this->investigation_plan_repo->investigationCategories());
	}


	public function store(CreateInvestigationPlanRequest $request)
	{ 
		$this->investigation_plan_repo->canUserCreateNewPlan(access()->user()->id);
		$plan = $this->investigation_plan_repo->createPlan($request, $request->all());
		if ($request->investigation_category == 'Individual Investigation') {
			$this->investigator_repo->updateInvestigators($plan->id,[access()->user()->id]);
		}
		return redirect()->route('backend.claim.investigations.profile', $plan->id)->withFlashSuccess('Success, Investigation Plan has been created');
	}

	public function assignPlanInvestigators(AssignPlanInvestigatorsRequest $request)
	{
		$this->investigation_plan_repo->canUserEditPlan($request->plan_id, access()->user()->id);
		$this->investigation_plan_repo->findOrThrowException($request->plan_id);
		$this->investigator_repo->updateInvestigators($request->plan_id,$request->investigators);
		return response()->json(['success' => true, 'message' => 'Success, Investigators for this plan have been updated!']);
	}
	
	public function edit($plan_id)
	{
		$profile = $this->investigation_plan_repo->findOrThrowException($plan_id);
		$this->investigation_plan_repo->canUserEditPlan($plan_id, access()->user()->id);
		return view('backend.operation.claim.investigation_plan.profile.edit')
		->with("profile", $profile)
		->with("types", $this->investigation_plan_repo->investigationTypes())
		->with("limits", $this->investigation_plan_repo->planMaximumLimits())
		->with("categories", $this->investigation_plan_repo->investigationCategories());
	}

	public function update($plan_id, UpdateInvestigationPlanRequest $request)
	{
		$this->investigation_plan_repo->canUserEditPlan($plan_id, access()->user()->id);
		$plan = $this->investigation_plan_repo->updatePlan($plan_id, $request->all(),$request);
		return redirect()->route('backend.claim.investigations.profile', $plan->id)->withFlashSuccess('Success, Investigation Plan has been updated');
	}

	public function investigatorsProfileDatatable($plan_id)
	{
		$data = $this->investigator_repo->returnInvestigatorsForDatatable($plan_id);
		return DataTables::of($data)
		->addColumn('number_of_files', function($data) use($plan_id){
			return (new InvestigationPlanNotificationsRepository())->query()
			->where('assigned_to',$data->user_id)
			->where('investigation_plan_id',$plan_id)->count();
		})->make(true);
	}


	public function assignFilesView($plan_id)
	{
		$this->investigation_plan_repo->canUserEditPlan($plan_id, access()->user()->id);
		$profile = $this->investigation_plan_repo->findOrThrowException($plan_id);
		return view('backend.operation.claim.investigation_plan.profile.assign_files')
		->with("plan_investigators", $this->investigator_repo->returnPlanInvestigators($plan_id))
		->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all())
		->with("profile", $profile);	
	}

	public function assignFilesSave($plan_id, AssignPlanFilesRequest $request)
	{
		$this->investigation_plan_repo->canUserEditPlan($plan_id, access()->user()->id);
		return $this->plan_files_repo->saveInvestigatorPlanFiles($plan_id,$request->all());
	}

	public function removeAssignedFiles($plan_file_id)
	{
		return $this->plan_files_repo->removeInvestigatorPlanFiles($plan_file_id);
	}

	public function getFilesAllocation($plan_id)
	{	
		// files when allocating 
		if (request()->action == 'excel' || request()->action == 'pdf' || request()->action == 'csv' ) {
			return $this->plan_files_repo->exportExcel($plan_id, request()->action);
		}
		$datatables = $this->plan_files_repo->getFilesForAllocationDatatable($plan_id);
		return $datatables
		->addColumn('allocated', function($data)  use ($plan_id) {
			$user = $this->plan_files_repo->returnAssignedUserName($plan_id,$data->notification_report_id);
			return (count($user)) ? $user->user_name : '';
		})->addColumn('action', function($data) use ($plan_id) {
			if ($this->investigation_plan_repo->checkIfUserCanEditPlan($plan_id, access()->user()->id)) {
				$user = $this->plan_files_repo->returnAssignedUserName($plan_id,$data->notification_report_id);
				return (count($user)) ? '<button class="btn btn-danger btn-sm btn-remove-file" data-remove="'.$user->plan_notification_id.'">Remove <i class="icon fa fa-trash"></i></button>' : '';
			}
			return '';
		})->rawColumns(['action'])->make(true);
	}

	public function getAllocatedFiles($plan_id)
	{
		//already allocated 
		if (request()->action == 'excel' || request()->action == 'pdf' || request()->action == 'csv' ) {
			return $this->plan_files_repo->exportExcel($plan_id, request()->action, 'allocated_files');
		}
		$plan = $this->investigation_plan_repo->findOrThrowException($plan_id);
		$datatables = $this->plan_files_repo->getAllocatedFilesDatatable($plan_id);
		return $datatables->addColumn('allocated', function($data) use($plan_id,$plan) {
			if ($plan->plan_status > 2 && $plan->wf_done == 1) {
				$user = $this->plan_files_repo->returnRejectedAssignedUserName($plan_id,$data->notification_report_id);
			} else {
				$user = $this->plan_files_repo->returnAssignedUserName($plan_id,$data->notification_report_id);
			}
			return count($user) ? $user->user_name : '';
		})->addColumn('investigation_status', function($data) use ($plan_id) {
			return $this->plan_files_repo->returnInvestigationStatusLabel($plan_id, $data->notification_report_id);
		})->rawColumns(['investigation_status'])->make(true);
	}

	public function submitForReview($plan_id)
	{
		$plan_id = (int)$plan_id; 
		$plan = $this->investigation_plan_repo->findOrThrowException($plan_id);
		$this->investigation_plan_repo->canUserSubmitForReview($plan_id, access()->user()->id);
		$return = $this->investigation_plan_repo->initiatePlanWorkflow($plan_id);
		if ($return) {
			return redirect()->route('backend.claim.investigations.profile', $plan->id)->withFlashSuccess('Success, Investigation Plan has been submitted for review');
		}else{
			return redirect()->route('backend.claim.investigations.profile', $plan->id)->withFlashWarning('Error, Something went wrong');

		}
	}

	public function returnUserPendingAllocatedFilesDatatable($user_id)
	{
		$datatables = $this->plan_files_repo->getUserPendingAllocatedFilesDatatable($user_id);
		return $datatables->make(true);
	}

	public function returnUserAllocatedFilesDatatable($user_id)
	{
		$datatables = $this->plan_files_repo->getUserAllocatedFilesDatatable($user_id);
		return $datatables->make(true);
	}




}
