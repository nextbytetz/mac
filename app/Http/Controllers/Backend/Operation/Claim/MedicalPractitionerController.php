<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\DataTables\Claim\GetMedicalPractitionersDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\CreateMedicalPractitionerRequest;
use App\Repositories\Backend\Operation\Claim\MedicalPractitionerRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class MedicalPractitionerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   protected $medical_practitioners;

    public function __construct(MedicalPractitionerRepository $medical_practitioners){
        $this->medical_practitioners = $medical_practitioners;

        $this->middleware('access.routeNeedsPermission:create_medical_practitioner', ['only' => ['create', 'store']]);
        $this->middleware('access.routeNeedsPermission:edit_medical_practitioner', ['only' => ['edit', 'update']]);
}

    public function index(GetMedicalPractitionersDataTable $dataTable)
    {
        //
        return  $dataTable->render('backend/operation/claim/medical_practitioner/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
             return view('backend.operation.claim.medical_practitioner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMedicalPractitionerRequest $request)
    {
        //
        $medical_practitioner = $this->medical_practitioners->create($request->all());
        if ($request->ajax()) {
            $return = response()->json(['success' => true, 'message' => "Success, Medical Practitioner has been added"]);
        } else {
            $return = redirect()->route('backend.claim.medical_practitioner.index')->withFlashSuccess(trans('alerts.backend.claim.medical_practitioner_created'));
        }
        return $return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $medical_practitioner = $this->medical_practitioners->findOrThrowException($id);
        return view('backend.operation.claim.medical_practitioner.edit')
            ->withMedicalPractitioner($medical_practitioner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, CreateMedicalPractitionerRequest $request)
    {
        //
               $medical_practitioner =  $this->medical_practitioners->update($id,$request->all());
        return redirect()->route('backend.claim.medical_practitioner.index')->withFlashSuccess(trans('alerts.backend.claim.medical_practitioner_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $health_provider =  $this->medical_practitioners->delete($id);
        return redirect()->route('backend.claim.medical_practitioner.index')->withFlashSuccess(trans('alerts.backend.claim.medical_practitioner_deleted'));
    }

    public function getRegisteredPractitioners()
    {
        return $this->medical_practitioners->getRegisteredPractitioners(request()->input('q'), request()->input('page'));
    }

}
