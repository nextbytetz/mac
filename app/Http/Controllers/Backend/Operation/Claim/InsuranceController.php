<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\InsuranceRequest;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use Yajra\Datatables\Facades\Datatables;

class InsuranceController extends Controller
{


    protected $insurances;


    public function __construct(){
        $this->insurances = new InsuranceRepository();

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend.operation.claim.insurance.index');
    }

    /**
     * Get all insurances for DataTable
     */
    public function getForDataTable()
    {

        return Datatables::of($this->insurances->getForDataTable())
            ->make(true);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.operation.claim.insurance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsuranceRequest $request)
    {
        //
        $insurance = $this->insurances->create($request->all());
        return redirect()->route('backend.claim.insurance.index')->withFlashSuccess(trans('alerts.backend.claim.insurance_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $insurance = $this->insurances->findOrThrowException($id);
        return view('backend.operation.claim.insurance.edit')
            ->withInsurance($insurance);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InsuranceRequest $request, $id)
    {
        //
        $this->insurances->update($id, $request->all());
        return redirect()->route('backend.claim.insurance.index')->withFlashSuccess(trans('alerts.backend.claim.insurance_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
