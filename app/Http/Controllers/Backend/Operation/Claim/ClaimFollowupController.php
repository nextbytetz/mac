<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\DataTables\Claim\OnlineAccountDataTable;
use App\DataTables\Compliance\Member\EmployerIncidentDataTable;
use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Events\Sockets\Notification\NewReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\ApproveRejectionAppealRequest;
use App\Http\Requests\Backend\Operation\Claim\AssignInvestigatorRequest;
use App\Http\Requests\Backend\Operation\Claim\ChooseEmployeeRequest;
use App\Http\Requests\Backend\Operation\Claim\ChooseIncidentTypeRequest;
use App\Http\Requests\Backend\Operation\Claim\ClaimAssessmentProgressiveRequest;
use App\Http\Requests\Backend\Operation\Claim\ClaimAssessmentRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateCurrentEmployeeStateRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateHealthProviderServiceRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMaeMemberMultipleRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMaeMemberRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMedicalExpenseRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateWitnessRequest;
use App\Http\Requests\Backend\Operation\Claim\DoctorNotificationReportUpdateRequest;
use App\Http\Requests\Backend\Operation\Claim\IncidentClosureRequest;
use App\Http\Requests\Backend\Operation\Claim\PdAssessmentProgressiveRequest;
use App\Http\Requests\Backend\Operation\Claim\PdAssessmentRequest;
use App\Http\Requests\Backend\Operation\Claim\RegisterEmployee;
use App\Http\Requests\Backend\Operation\Claim\RegisterNotificationRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateBankDetailsRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateChecklistRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateDetailsRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateInvestigationReportRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateMaeMemberMultipleRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateManualPaymentRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateMonthlyEarningRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateProgressiveNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdatingNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateIndividualContributionRequest;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;
use App\Jobs\Claim\CheckUploadedDocuments;
use App\Jobs\Claim\CheckUploadedDocumentsNotification;
use App\Jobs\Claim\PostNotificationDms;
use App\Models\Operation\Claim\BenefitTypeClaim;
use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\MedicalExpense;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Claim\NotificationHealthProvider;
use App\Models\Operation\Claim\NotificationInvestigator;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\Portal\Claim;
use App\Models\Operation\Claim\PortalIncident;
use App\Models\Operation\Claim\Witness;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Reporting\ConfigurableReport;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Operation\Claim\AccidentTypeRepository;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\BodyPartInjuryRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\ClaimRepository;
use App\Repositories\Backend\Operation\Claim\DeathCauseRepository;
use App\Repositories\Backend\Operation\Claim\DisabilityStateChecklistRepository;
use App\Repositories\Backend\Operation\Claim\DiseaseKnowHowRepository;
use App\Repositories\Backend\Operation\Claim\DocumentGroupRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\HealthProviderRepository;
use App\Repositories\Backend\Operation\Claim\HealthServiceChecklistRepository;
use App\Repositories\Backend\Operation\Claim\HealthStateChecklistRepository;
use App\Repositories\Backend\Operation\Claim\IncidentClosureRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Access\IncidentUserRepository;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use App\Repositories\Backend\Operation\Claim\MedicalExpenseRepository;
use App\Repositories\Backend\Operation\Claim\MedicalPractitionerRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Claim\NotificationDisabilityStateRepository;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Operation\Claim\NotificationHealthProviderRepository;
use App\Repositories\Backend\Operation\Claim\NotificationInvestigatorRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\OccupationRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdAmputationRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdImpairmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdInjuryRepository;
use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\Repositories\Backend\Operation\Claim\WitnessRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use App\Repositories\Backend\Reporting\DashboardRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\JobTitleRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\Backend\Operation\Claim\FollowUp\FollowUpRequest;
use App\Repositories\Backend\Operation\Claim\ClaimFollowupRepository;
use App\Repositories\Backend\Operation\Claim\ClaimFollowupUpdateRepository;
use App\Models\Operation\ClaimFollowup\ClaimFollowup;
use App\Models\Operation\ClaimFollowup\ClaimFollowupUpdate;
use App\Models\Auth\User;
use App\DataTables\DataTableBase;
use App\Models\Access\Permission;
use App\Models\Operation\ClaimFollowup\ClaimFollowUpReviewHistory;


class ClaimFollowupController extends Controller
{
    use AttachmentHandler, FileHandler;
    
    public function __construct() { 
        $this->claim_follow_up = new ClaimFollowupRepository();
        $this->claim_follow_up_update = new ClaimFollowupUpdateRepository();
        $this->notification_report = new NotificationReportRepository();
    }

    /* followups functions */

    public function index(){
        return 1234;
    }

    public function createFollowUp($notification_report_id)
    {
        // dd($notification_report_id);
        $cv_repo = new CodeValueRepository();
        $notification = NotificationReport::find($notification_report_id);
        $employee = $notification->employee;

        $districts = DB::table('districts')->pluck('name','id');
        
        $follow_up_types = $cv_repo->query()->where('code_id',9)->orderBy('name', 'asc')->get()->pluck('name','reference');

        return view('backend/operation/claim/notification_report/includes/progressive/follow_ups/create')
        ->with('notification', $notification)
        ->with('employee', $employee)
        ->with('districts', $districts)
        ->with('follow_up_types', $follow_up_types);

    }

    /*Store follow up*/
    public function storeFollowUp(FollowUpRequest $request)
    {
        $input = $request->all();
        DB::transaction(function () use ($input) {
            $this->claim_follow_up->storeFollowUp($input);
        });
        return redirect('claim/notification_report/profile/' . $request->notification_report_id . '#follow_ups')->withFlashSuccess('Success, Follow up has been created');

    }

    /*Get follow ups for dataTable*/
    public function getClaimFollowupDatatable($incident_id)
    {
        return Datatables::of($this->claim_follow_up->getClaimFollowupDt($incident_id))

        ->editColumn('date_of_follow_up', function ($follow_up) {
            return short_date_format($follow_up->date_of_follow_up);
        })
        ->editColumn('user_id', function ($follow_up) {
            // return $follow_up->user_id;
            return $follow_up->user->name;
        })
        ->addColumn('follow_up_type', function ($follow_up) {
            return $follow_up->followUpType->name;
        })
        ->make(true);
    }

    public function editFollowUp($claim_followup_id, $follow_up_id)
    {

        $claim_followup = ClaimFollowup::find($claim_followup_id);
        $claim_followup_update = ClaimFollowupUpdate::find($follow_up_id);
        $notification_report_id = $claim_followup->notification_report_id;
        $cv_repo = new CodeValueRepository();
        $notification = NotificationReport::find($notification_report_id);
        $employee = $notification->employee;

        $in_follow_up = $this->claim_follow_up->isInFollowUp($notification);
        $follow_up_date_exceeded = $this->claim_follow_up->ifFollowUpDateExceeded($notification);
        
        $follow_up_types = $cv_repo->query()->where('code_id',9)->orderBy('name', 'asc')->get()->pluck('name','reference');

        $districts = DB::table('districts')->pluck('name','id');
        $postcodes = DB::table('postcodes')->pluck('ward_name','postcode');
        $postcode = DB::table('postcodes')->select('district_id','postcode')->where('id', $claim_followup_update->postcode_id)->first();
        $claim_follow_up = $claim_followup;

        return view('backend/operation/claim/notification_report/includes/progressive/follow_ups/edit')
        ->with('notification', $notification)
        ->with('employee', $employee)
        ->with('districts', $districts)
        ->with('postcodes', $postcodes)
        ->with('postcode', $postcode)
        ->with('claim_follow_up', $claim_follow_up)
        ->with('in_follow_up', $in_follow_up)
        ->with('follow_up_date_exceeded', $follow_up_date_exceeded)
        ->with('claim_followup_update', $claim_followup_update)
        ->with('follow_up_types', $follow_up_types);

    }

    /*Store follow up*/
    public function updateFollowUp($id, FollowUpRequest $request)
    {
        $input = $request->all();
        // dd($input);
        $this->claim_follow_up_update->updateFollowUp($id, $input);
        return redirect('claim/notification_report/profile/' . $request->notification_report_id . '#follow_ups')->withFlashSuccess('Success, Follow up has been updated');

    }

    public function deleteFollowUp($id){

        $follow_up = $this->claim_follow_up_update->findOrThrowException($id);
        $notification = $this->claim_follow_up->findOrThrowException($follow_up->claim_followup_id);
        $notification_report_id = $notification->notification_report_id; 
        // dd($notification_report_id);
        $this->claim_follow_up_update->deleteFollowUp($id);
        return redirect('claim/notification_report/profile/' . $notification_report_id . '#follow_ups')->withFlashSuccess('Success, Follow up has been deleted');
    }

    public function getFollowUps(){
        $staffs = User::select('users.id','users.firstname','users.middlename','users.lastname')
        ->join('claim_followups','claim_followups.user_id','=','users.id')
        ->distinct('user.id')->get();

        // dd($staffs);
        return view('backend/operation/claim/notification_report/includes/progressive/follow_ups/main_index')
        ->with('staffs', $staffs);
    }

    public function myFollowUps($user_id){

      return Datatables::of($this->claim_follow_up->getMyFollowUps($user_id))

      ->editColumn('user_id', function ($follow_up) {
        return $follow_up->user->name;
    })
      ->editColumn('employee', function ($follow_up) {
        return $follow_up->notificationReport->employee->name;
    })
      ->addColumn('case_no', function ($follow_up) {
        return $follow_up->notificationReport->filename;
    })

      ->addColumn('status', function ($follow_up) {
        return $follow_up->notificationReport->status_label;
    })
      ->addColumn('district', function ($follow_up) {
        return $follow_up->notificationReport->district->name;
    })
      ->addColumn('start_date', function ($follow_up) {
        return Carbon::parse($follow_up->start_date)->format('Y-m-d');
    })
      ->addColumn('end_date', function ($follow_up) {
        return Carbon::parse($follow_up->start_date)->addDays(7)->format('Y-m-d');
    })
      ->addColumn('missing_docs', function ($follow_up) {
        return $follow_up->missing_documents;
    })
      ->addColumn('followup_counts', function ($follow_up) {
        return $follow_up->followup_count;
    })
      ->editColumn('attendance_status', function ($follow_up){
        return $this->getStatusLabel($follow_up->followup_count);

    })
      ->editColumn('review_status', function ($follow_up){
        return $this->getReviewStatusLabel($follow_up->review_status);

    })
      ->rawColumns(['status','attendance_status','review_status'])
      ->make(true);
  }

  public function mainFollowUps(){
    // dd(request()->all());
    $input = request()->all();

    return Datatables::of($this->claim_follow_up->mainMyFollowUps($input))
    ->addIndexColumn()
    // ->editColumn('sn', function ($follow_up){
    //     return $follow_up['sn'];
    // })
    ->editColumn('staff', function ($follow_up) {
        $user = User::find($follow_up['user_name']);
        return $user->name;
    })
    ->editColumn('total_files', function ($follow_up){
        return $follow_up['total_files'];
    })
    ->editColumn('files_per_week', function ($follow_up) {
        return $follow_up['total_files_per_week'];
    })
    ->editColumn('1st_week_updated', function ($follow_up){
        return $follow_up['1st_week_updated'];
    })
    ->editColumn('1st_week_missed', function ($follow_up){
        return $follow_up['1st_week_missed'];
    })
    ->editColumn('1st_week', function ($follow_up){
        return $follow_up['1st_week'].'%';
    })
    ->editColumn('2nd_week_updated', function ($follow_up){
        return $follow_up['2nd_week_updated'];
    })
    ->editColumn('2nd_week_missed', function ($follow_up){
        return $follow_up['2nd_week_missed'];
    })
    ->editColumn('2nd_week', function ($follow_up){
        return $follow_up['2nd_week'].'%';
    })
    ->editColumn('3rd_week_updated', function ($follow_up){
        return $follow_up['3rd_week_updated'];
    })
    ->editColumn('3rd_week_missed', function ($follow_up){
        return $follow_up['3rd_week_missed'];
    })
    ->editColumn('3rd_week', function ($follow_up){
        return $follow_up['3rd_week'].'%';
    })
    ->editColumn('4th_week_updated', function ($follow_up){
        return $follow_up['4th_week_updated'];
    })
    ->editColumn('4th_week_missed', function ($follow_up){
        return $follow_up['4th_week_missed'];
    })
    ->editColumn('4th_week', function ($follow_up){
        return $follow_up['4th_week'].'%';
    })
    ->editColumn('overall', function ($follow_up){
        return $follow_up['overall'].'%';
    })
    ->make(true);

}

public function userFollowUps($user_id){
    $user = User::find($user_id);
    return view('backend/operation/claim/notification_report/includes/progressive/follow_ups/user_follow_up_index')
    ->with('user', $user);
}

public function getUserFollowUps($user_id){

    return Datatables::of($this->claim_follow_up->userFollowUps($user_id))
    ->editColumn('employee', function ($follow_up) {
        return $follow_up->notificationReport->employee->name;
    })
    ->addColumn('case_no', function ($follow_up) {
        return $follow_up->notificationReport->filename;
    })

    ->addColumn('status', function ($follow_up) {
        return $follow_up->notificationReport->status_label;
    })
    ->addColumn('district', function ($follow_up) {
        return $follow_up->notificationReport->district->name;
    })
    ->addColumn('start_date', function ($follow_up) {
        return Carbon::parse($follow_up->start_date)->format('Y-m-d');
    })
    ->addColumn('end_date', function ($follow_up) {
        return Carbon::parse($follow_up->start_date)->addDays(7)->format('Y-m-d');
    })
    ->addColumn('missing_documents', function ($follow_up) {
        return $follow_up->missing_documents;
    })
    ->addColumn('follow_ups', function ($follow_up) {
        return $follow_up->followup_count;
    })
    ->editColumn('attendance_status', function ($follow_up){
        return $this->getStatusLabel($follow_up->followup_count);

    })
    ->editColumn('review_status', function ($follow_up){
        return $this->getReviewStatusLabel($follow_up->review_status);

    })
    ->rawColumns(['status','attendance_status','review_status'])
    ->make(true);

}

public function getStatusLabel($count)
{
    $return = "";
    if($count > 0){
        $return = "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='Attended'> Attended </span>";
    }else{
        $return = "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='Not Attended '> Not Attended </span>";
    }
    return $return;
}

public function getReviewStatusLabel($id)
{

    $return = "";
    if ($id !== null) {
        switch ($id) {
            case 0:
            $return = "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='Submitted Waiting for review'> Submitted </span>";
            break;
            case 1:
            $return = "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='Approved'> Approved </span>";
            break;
            case 2:
            $return = "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='Reversed'> Reversed </span>";
            break;
            case 3:
            $return = "<span class='tag tag-info' data-toggle='tooltip' data-html='true' title='Resubmitted'> Resubmitted </span>";
            break;

            default:
            break;
        }
    }else{
        $return = "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='Not Submitted'> Not Submitted </span>";
    }
    
    return $return;
}

public function exportFollowUpPerfomance(){
    $this->claim_follow_up->exportToExcel(request()->all());
    return redirect()->back();
}

public function reviewHistory($notification_report_id){
    // return $notification_report_id;
    $claim = ClaimFollowup::where('notification_report_id', $notification_report_id)->first();

    $history = DB::table('claim_follow_up_review_histories')
    ->select('claim_follow_up_review_histories.created_at','claim_follow_up_review_histories.status_description','claim_follow_up_review_histories.remark','users.firstname','users.middlename','users.lastname')
    ->join('main.users','users.id', '=', 'claim_follow_up_review_histories.user_id')->where('claim_followup_id', $claim->id)->orderBy('claim_follow_up_review_histories.id', 'desc')->get();

    if (count($history) > 0) {
         // return $history;
     return response()->json(array("success"=>true, "data"=> $history->toArray())); 
 }else{
    return response()->json(array("success"=>false, "message"=> 'No History Yet')); 
}


}

public function approve($notification_report_id){
    // $allocated = $this->isAllocated($notification_report_id);
    // if (auth()->user()->can('review_claim_follow_up')) {
    $this->claim_follow_up->updateReviewStatus($notification_report_id,1);
    $this->updateReviewHistory($notification_report_id,1);
   //  }else{
   //     throw new GeneralException('You don\'t permission to assess this file..');
   // }

    return redirect('claim/notification_report/profile/' . $notification_report_id . '#follow_ups')->withFlashSuccess('Success, Follow up has been Approved');
}

public function reverse($notification_report_id){
    $input = request()->all();
    $this->claim_follow_up->updateReviewStatus($notification_report_id,2);
    $this->updateReviewHistory($notification_report_id,2, $input);
    return response()->json(array("success"=>true, "message"=> "FollowUp Reversed Successfuly!")); 
}


public function resubmit($notification_report_id){
    $allocated = $this->isAllocated($notification_report_id);
    if ($allocated) {
        $this->claim_follow_up->updateReviewStatus($notification_report_id,3);
        $this->updateReviewHistory($notification_report_id,3);
    }else{
       throw new GeneralException('You\'re not allocated to this file..');
   }

   return redirect('claim/notification_report/profile/' . $notification_report_id . '#follow_ups')->withFlashSuccess('Success, Follow up has been resubmitted');
}

public function complete($notification_report_id){
    $allocated = $this->isAllocated($notification_report_id);
    DB::transaction(function () use ($allocated, $notification_report_id ) {
        if ($allocated) {
            $this->claim_follow_up->updateReviewStatus($notification_report_id,0);
            $this->updateReviewHistory($notification_report_id,0);
        }else{
         throw new GeneralException('You\'re not allocated to this file..');
     }
 });

    return redirect('claim/notification_report/profile/' . $notification_report_id . '#follow_ups')->withFlashSuccess('Success, Follow up has been completed');
}

public function isAllocated($notification_report_id){
    $return = false;
    $notification = $this->notification_report->findOrThrowException($notification_report_id);
    if ($notification->allocated == auth()->user()->id) {
        $return = true;
    }
    return $return;
}

public function updateReviewHistory($notification_report_id, $status, $input = null){
    $claim_followup = ClaimFollowup::where('notification_report_id', $notification_report_id)->first();
    $this->claim_follow_up->calculateFollowup();
    $status_description = null;
    $data = [];

    switch ($status) {
        case 0:
        $status_description = 'Submitted for review';
        $user_id = auth()->user()->id;
        break;
        case 1:
        $status_description = 'Approved';
        $user_id = auth()->user()->id;
        break;
        case 2:
        $status_description = 'Reversed';
        $user_id = auth()->user()->id;
        break;
        case 3:
        $status_description = 'Resubmitted';
        $user_id = auth()->user()->id;
        break;
        
        default:
            # code...
        break;
    }

    $data = [
        'claim_followup_id' => $claim_followup->id,
        'remark' => null,
        'status_description' => $status_description,
        'remark' => $input == null ? null : $input['remark'],
        'user_id' => $user_id
    ];

    ClaimFollowUpReviewHistory::create($data);
}

public function updatesCsv($date){
    return $this->claim_follow_up->updatedFilesQuery($date);
}

}
