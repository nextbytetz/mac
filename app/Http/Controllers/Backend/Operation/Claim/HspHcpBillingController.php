<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\Http\Requests\Backend\Operation\Claim\UpdateImpairmentAssessmentRequest;
use App\Models\Operation\Claim\ImpairmentAssessment;
use App\Models\Operation\Claim\NotificationReport;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Claim\DocumentResourceRepository;
use App\Repositories\Backend\Operation\Claim\ImpairmentAssessmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdImpairmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdInjuryRepository;
use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use App\Repositories\Backend\Operation\Claim\HspHcpBillingRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Operation\Compliance\Member\Employer;
use Carbon\Carbon;
use DB;
use PDF;
use App\Repositories\Backend\Operation\Claim\HspBilling\HspBillSummaryRepository;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Storage\Traits\AttachmentHandler;
/**
 * Class AssessmentController
 * @package App\Http\Controllers\Backend\Operation\Claim
 */
class HspHcpBillingController extends Controller
{
  use FileHandler,AttachmentHandler;
    /**
     * Display a listing of the resource.
     *updateDocument
     * @return \Illuminate\Http\Response
     */
    protected $hcp_hsp;

    public function __construct(HspHcpBillingRepository $hcp_hsp) {
      $this->hcp_hsp = $hcp_hsp;
    }

    public function index()
    {

      return view('backend.operation.claim.assessment.hcp_hsp.hcp_hsp_billing');

    }

    public function billings()
    {

      return view('backend.operation.claim.assessment.hcp_hsp.billing.billings');

    }

    public function menu()
    {

      return view('backend.operation.claim.assessment.hcp_hsp.menu');

    }

    /**
     * @return mixed
     * @throws \Exception
     * @description Recall Notifications : Get for dataTable
     */
    public function getForDataTable()
    {
      return Datatables::of($this->hcp_hsp->getForDataTable())
      ->make(true);
    }

    public function getForBillingDataTable()
    {
      return Datatables::of($this->hcp_hsp->getForBillingDataTable())
      ->editColumn('date', function($data) {
        return Carbon::parse($data->date)->format('M Y');
      })
      ->editColumn('total_amount_vetted', function($data) {
        return number_format($data->total_amount_vetted, 2);
      })
      ->editColumn('total_amount_claimed', function($data) {
        return number_format($data->total_amount_claimed, 2);
      })
      ->editColumn('price_difference', function($data) {
        return number_format($data->price_difference, 2);
      })
      ->editColumn('status', function($data) {
        $return = "";
        switch ($data->vetting_status) {
          case 0:
                //if pending
          $return = "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='Not vetted'>Not Vetted</span>";
          break;
          case 1:
                //if approved
          $return = "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='vetted'>Vetted</span>";
          break;
          default:
          break;
        }
        return $return;
      })
      ->rawColumns(['status'])
      ->make(true);
    }

    public function getForVettingBillingDataTable($billing_id)
    {
      return Datatables::of($this->hcp_hsp->getForVettingBillingDataTable($billing_id))
      ->editColumn('bill_amount', function($data) {
        return number_format($data->bill_amount, 2);
      })
      ->editColumn('status', function($data) {
        $return = "";
        switch ($data->is_reviewed) {
          case false:
                //if pending
          $return = "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='Not Reviewed'>Not Reviewed</span>";
          break;
          case true:
                //if approved
          $return = "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='Reviewed'>Reviewed</span>";
          break;
          default:
          break;
        }
        return $return;
      })
      ->rawColumns(['status'])
      ->make(true);
    }

    public function profile($billing_id){

      $billing = $this->hcp_hsp->findOrThrowException($billing_id);
      if (!is_null($billing->notification_report_id)) {
        $notification_report = NotificationReport::find($billing->notification_report_id);
      }

      $employee = Employee::find($billing->employee_id);
      $employer = Employer::find($billing->employer_id);
      return view('backend.operation.claim.assessment.hcp_hsp.profile')
      ->with([
       'billing' => $billing,
       'notification_report' => $notification_report,
       'employer' => $employer,
       'employee' => $employee
     ]);
    }

    public function getForAuthBillingDataTable($auth_no)
    {
      $auth_no = (str_replace(",", "", $auth_no));
      return Datatables::of($this->hcp_hsp->getForAuthBillingDataTable($auth_no))
      ->editColumn('bill_amount', function($data) {
        return number_format($data->bill_amount, 2);
      })
      ->editColumn('status', function($data) {
        $return = "";
        switch ($data->is_reviewed) {
          case false:
                //if pending
          $return = "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='Not Reviewed'>Not Reviewed</span>";
          break;
          case true:
                //if approved
          $return = "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='Reviewed'>Reviewed</span>";
          break;
          default:
          break;
        }
        return $return;
      })
      ->rawColumns(['status'])
      ->make(true);
    }

    public function billingProfile($billing_id){
     $billing = (new HspBillSummaryRepository())->getBillWithStakeholders($billing_id);
     return view('backend.operation.claim.assessment.hcp_hsp.billing.hcp_bill_profile')
     ->with([
       'billing' => $billing,
     ]);}

     public function vettingProfile($billing_detail_id){
       $wf_level = (new HspBillSummaryRepository())->canVettBillDetail($billing_detail_id);
       $services = DB::table('hsp_bill_services')->select('hsp_bill_services.*','hsp_bill_details.facility_code','hsp_bill_services.id as id')
       ->join('hsp_bill_details', 'hsp_bill_details.id', '=', 'hsp_bill_services.hsp_detail_id')
       ->where('hsp_detail_id', $billing_detail_id)
       ->get();

       $bill_details = DB::table('hsp_bill_details')->where('id', $billing_detail_id)->first();

       return view('backend.operation.claim.assessment.hcp_hsp.billing.hcp_bill_services')
       ->with([
         'services' => $services,
         'bill_details' => $bill_details,
         'wf_level' => $wf_level
       ]);
     }

     public function postBillServices(Request $request, $id, $detail_id){
      $wf_level = (new HspBillSummaryRepository())->canVettBillDetail($detail_id);
      if ($wf_level == 6) {
       $sh_bill = $this->hcp_hsp->fixedShBill($detail_id);
       $update = DB::table('hsp_bill_services')->where('id',$id)
       ->update([
         ''.$request->name.'' => (float)(str_replace(",", "", $request->Value))
       ]);

       if ($request->name == 'vetted_price') {
        $bill = DB::table('hsp_bill_services')->select('price')->where('id',$id)->first();
        $diff = $this->hcp_hsp->difference($bill->price, $request->Value);
        DB::table('hsp_bill_services')->where('id',$id)->update(['price_defference' => $diff]);
      }else{
        $bill = DB::table('hsp_bill_services')->select('quantity')->where('id',$id)->first();
        $diff = $this->hcp_hsp->difference($bill->quantity, $request->Value);
        DB::table('hsp_bill_services')->where('id',$id)->update(['quantity_difference' => $diff]);
      }

      $service = DB::table('hsp_bill_services')->select(DB::raw('sum(vetted_price) as price'),
        DB::raw('count(id) as vetted_items'),
        DB::raw('sum(price_defference) as price_difference'),
        DB::raw('sum(quantity_difference) as quantity_difference'))
      ->where('hsp_detail_id', $detail_id)
      ->where('is_workrelated', true)
      ->first();

      return [
        'price' => !is_null($service) ? $service->price : 0,
        'vetted_items' => $service->vetted_items,
        'price_difference' => ($sh_bill - $service->price),
        'quantity_difference' => $service->quantity_difference,
      ];
    }

  }

  public function postRemarks(Request $request, $id, $detail_id){
    $wf_level = (new HspBillSummaryRepository())->canVettBillDetail($detail_id);
    if ($wf_level == 6 ) {
      $update = DB::table('hsp_bill_services')->where('id',$id)
      ->update([
        ''.$request->name.'' => $request->Value
      ]);
    }
  }

  public function calculateTotalService($detail_id){

    $service = DB::table('hsp_bill_services')->select(DB::raw('sum(vetted_quantity) as quantity'),DB::raw('sum(vetted_price) as price'))
    ->where('hsp_detail_id', $detail_id)
    ->first();
    if (!is_null($service)) {
      return ['price' => $service->price, 'quantity' => $service->quantity];
    }else{
      return ['price' => 0, 'quantity' => 0];
    }

  }

  public function calculateTotalDetail(Request $request, $service_id, $detail_id){
    $wf_level = (new HspBillSummaryRepository())->canVettBillDetail($detail_id);
    if ($wf_level) {
      $price = 0;
      $detail = null;
      $service = null;
      $update = DB::table('hsp_bill_services')->where('id',$service_id)
      ->update([
        'is_workrelated' => $request->Value,
      ]);

      $sh_bill = $this->hcp_hsp->fixedShBill($detail_id);
      if ($update) {

        $service = DB::table('hsp_bill_services')->select(DB::raw('sum(vetted_price) as price'),DB::raw('count(id) as vetted_items'), DB::raw('sum(price_defference) as p_diff'), DB::raw('sum(quantity_difference) as q_diff'))
        ->where('hsp_detail_id', $detail_id)
        ->where('is_workrelated', true)
        ->first();
        $all = DB::table('hsp_bill_services')->select(DB::raw('count(id) as vetted_items'))
        ->where('hsp_detail_id', $detail_id)
        ->first();
      }   
      if (!is_null($service)) {
        $detail = DB::table('hsp_bill_details')
        ->where('id', $detail_id)
        ->update([
          'vetted_amount' => !is_null($service->price) ? $service->price : 0,
          'vetted_items' => !is_null($service->vetted_items) ? $service->vetted_items : 0,
          'item_difference' => !is_null($service->vetted_items) ? ($all->vetted_items - $service->vetted_items): 0,
          'price_difference' => $sh_bill - (!is_null($service->price) ? $service->price : 0),
        ]);

      }else{
        $detail = DB::table('hsp_bill_details')
        ->where('id', $detail_id)
        ->update(['vetted_amount' => 0, 'vetted_items' => 0 ,'item_difference' => 0, 'price_difference' => 0]);
      }

      if (!is_null($detail)) {
        $detail = DB::table('hsp_bill_details')
        ->where('id', $detail_id)->first();

        return ['price' => $detail->vetted_amount, 'vetted_items' => $detail->vetted_items, 'item_difference' => $detail->item_difference, 'price_difference' => $detail->price_difference];
      }else{
        return ['price' => 0, 'vetted_items' => 0, 'item_difference' => 0, 'price_difference' => 0];
      }
    }

  }

  public function initiateVettingWorkflow($hsp_billing_id)
  {
    $bill_summary_repo = new HspBillSummaryRepository();
    $bill_summary_repo->checkBeforeInitiateWf($hsp_billing_id);
    $bill_summary_repo->initiateBillVettingWorkflow($hsp_billing_id);
    return redirect()->back()->withFlashSucces('Success, Workflow approval has been initiated');
  }

  public function hspFacilities()
  {
    return view('backend.operation.claim.assessment.hcp_hsp.facilities');
  }

  public function getFacilities()
  {
    return Datatables::of($this->hcp_hsp->getFacilitiesDataTable())->make(true);
  }

  public function viewFacility($facility_id)
  {
    $facility = DB::table('facility_codes')->select(['facility_code','name','shdescription','facility_codes.id'])
    ->leftJoin("api_stakeholders", "api_stakeholders.id", "=", "facility_codes.stakeholder_id")->where('facility_codes.id',$facility_id)->first();
    return view('backend.operation.claim.assessment.hcp_hsp.view_facility')->with(['facility'=>$facility]);
  }

  public function getFacilityItems($facility_id)
  {
    return Datatables::of($this->hcp_hsp->getFacilityItemsDataTable($facility_id))->make(true);
  }

  public function getTotalBillItems($detail_id){
    $detail = DB::table('hsp_bill_details')
    ->where('id', $detail_id)->first();

    return ['price' => $detail->vetted_amount, 'vetted_items' => $detail->vetted_items];
  } 

  public function postTotalBillItems($detail_id){
    return DB::transaction(function () use ($detail_id) {

      try {
        $wf_level = (new HspBillSummaryRepository())->canVettBillDetail($detail_id);
        if ($wf_level == 6) {

          $detail = DB::table('hsp_bill_details')
          ->where('id', $detail_id)->update(['is_reviewed' => true]);

          $this->hcp_hsp->recalculate($detail_id);

          return ['success' => true, 'message' => 'Bill Reviewed Successful!'];;
        }else{
          return ['error' => true, 'message' => 'Action failed! You can not submit this review at this stage'];
        }
      } catch (Exception $e) {
        return ['error' => true, 'message' => 'Action failed! An Error has occured!'];
      }
    });
  }

  public function previewPaymentAdvice($hsp_billing_id)
  {
    $hsp_repo = new HspBillSummaryRepository();
    $hsp_bill = $hsp_repo->findOrThrowException($hsp_billing_id);
    $name = $hsp_billing_id. ".pdf";

    if ($hsp_bill->payment_advice == 0 && $hsp_bill->current_wf_level == 13) {
      $pdf = PDF::loadView('backend.operation.claim.assessment.hcp_hsp.billing.includes.payment_advice_document', [
        'hsp_bill'=>$hsp_bill,
        'vetted_by' => $hsp_repo->returnUserNameAtWfLevel($hsp_billing_id,6),
        'reviewed_by' => $hsp_repo->returnUserNameAtWfLevel($hsp_billing_id,7),
        'prepared_by' => $hsp_repo->returnUserNameAtWfLevel($hsp_billing_id,13)
      ])->setPaper('a4', 'portrait');


      $this->makeDirectory($hsp_repo->getBillPath().DIRECTORY_SEPARATOR);
      try {
       $pdf->save($hsp_repo->getBillPath().DIRECTORY_SEPARATOR.$name);
     } catch (\Exception $e) {
      logger($e);
    }
  }
  $file_url = $hsp_repo->getBillPathFileUrl().DIRECTORY_SEPARATOR.$name;
  return response()->json(['success' => true, "url" => $file_url, "name" => $name]);

}

public function updatePaymentAdvice($hsp_billing_id,$level)
{
  (new HspBillSummaryRepository())->updatePaymentAdvice($hsp_billing_id,$level);
  return redirect()->back()->withFlashSucces('Success, Payment advice has been updated');
}

}