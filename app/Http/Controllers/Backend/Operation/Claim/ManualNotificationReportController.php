<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\ChooseEmployeeRequest;
use App\Http\Requests\Backend\Operation\Claim\CompleteManualNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateManualNotificationInfoRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateManualNotificationOshRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateManualPaymentRequest;
use App\Http\Requests\Backend\Operation\Claim\UploadAllManualNotificationRequest;
use App\Http\Requests\Backend\Operation\Claim\UploadManualNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateEmployeeFromManualFileRequest;
use App\Jobs\Claim\PostManualNotificationDms;
use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Reporting\Cr;
use App\Models\Sysdef\JobTitle;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\GenderRepository;
use App\Repositories\Backend\Sysdef\IdentityRepository;
use App\Repositories\Backend\Sysdef\JobTitleRepository;
use App\Repositories\Backend\Sysdef\UploadAttributeRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Facades\Datatables;

class ManualNotificationReportController extends Controller
{
    //
    use AttachmentHandler, FileHandler;

    protected $manual_notification_reports;
    protected $base;
    protected $notificationReportFolder = null;

    public function __construct()
    {
        $this->manual_notification_reports = new ManualNotificationReportRepository();
        $this->middleware('access.routeNeedsPermission:manage_manual_files', ['only' => [ 'uploadPage',  'updateInfo', 'doManualPayment', 'updateHasPayroll', 'sendToDms', 'updateAttendantThisFile' ]]);
        $this->middleware('access.routeNeedsPermission:manage_payroll_data_migration', ['only' => [ 'openCompleteRegistrationPage', 'syncMember' ]]);

        //On Backend
        if(env('TESTING_MODE') == 1)
        {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }


    }

    public function index()
    {
        return view('backend/operation/claim/manual_notification_report/index');
    }

    /*open upload page*/
    public function uploadPage()
    {
        $no_of_uploaded_files = $this->manual_notification_reports->getForDataTable()->count();
        $file_url = $this->manual_notification_reports->uploadedFileUrl();
        return view('backend/operation/claim/manual_notification_report/upload_bulk/upload_page')
            ->with('no_of_uploaded_files', $no_of_uploaded_files)
            ->with('uploaded_file_url', $file_url);
    }

    public function uploadAll()
    {
        $reference = (string) time() . (string) access()->id();
        return view('backend/operation/claim/manual_notification_report/upload_all/index')
            ->with('reference', $reference);
    }

    /**
     * @param UploadAllManualNotificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUploadAll(UploadAllManualNotificationRequest $request)
    {
        $input = $request->all();
        if ($input['file_preview']) {
            $success = 2; //preview notifications and map columns
            $data = $this->manual_notification_reports->createPreviewFromJson($input);
            $redirect_url = '#';
            return response()->json(['success' => $success, 'redirect_url' => $redirect_url, 'message' => '', 'data' => $data]);
        } else {
            $input = $request->except(['_token', 'contacts_file']);
            return $this->manual_notification_reports->createFromJson($input, $request);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws ModelNotFoundException
     */
    public function listAll()
    {
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->find(30);
        if  (!$cr) {
            throw new ModelNotFoundException();
        }
        return view('backend/operation/claim/manual_notification_report/list_all')
            ->with('cr', $cr);
    }

    /**
     * @param ManualNotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard(ManualNotificationReport $incident)
    {
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->getReportModel(30, $incident->id, 'resourceid');
        //dd($cr);
        return view('backend/operation/claim/manual_notification_report/dashboard')
            ->with('incident', $incident)
            ->With('cr', $cr);
    }

    /**
     * @param ManualNotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateInfo(ManualNotificationReport $incident)
    {
        $codeValueRepo = new CodeValueRepository();
        $incident_types = (new IncidentTypeRepository())->getAll()->pluck('name', 'id')->all();
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->getReportModel(30, $incident->id, 'resourceid');
        $districts = (new DistrictRepository())->getAll()->pluck('name', 'id')->all();
        $claim_status = $codeValueRepo->query()->select(['name', 'id'])->where('code_id', 54)->get()->pluck('name', 'id')->all();
        $rejection_reason = $codeValueRepo->query()->select(['name', 'id'])->where('code_id', 55)->get()->pluck('name', 'id')->all();
        $rejection_cv_id = $codeValueRepo->query()->select(['id'])->where(['code_id' => 54, 'reference' => 'MANNOTST02'])->first()->id;
        return view('backend/operation/claim/manual_notification_report/complete_registration/update_info')
            ->with('incident', $incident)
            ->with('incident_types', $incident_types)
            ->with('cr', $cr)
            ->with('districts', $districts)
            ->with('claim_status', $claim_status)
            ->with('rejection_reason', $rejection_reason)
            ->with('rejection_cv_id', $rejection_cv_id);
    }

    /**
     * @param ManualNotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function doManualPayment(ManualNotificationReport $incident)
    {
        if (!$incident->isupdated) {
            return redirect()->back()->withFlashWarning('Please update manual notification information first');
        }
        if ($incident->status_cv_id != (new CodeValueRepository())->query()->select(['id'])->where('reference', 'MANNOTST01')->first()->id) {
            return redirect()->back()->withFlashWarning('Please update paid status first');
        }
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->getReportModel(30, $incident->id, 'resourceid');
        return view('backend/operation/claim/manual_notification_report/complete_registration/update_payment')
            ->with('incident', $incident)
            ->with('cr', $cr);
    }

    /**
     * @param ManualNotificationReport $incident
     * @param UpdateManualNotificationInfoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdateInfo(ManualNotificationReport $incident, UpdateManualNotificationInfoRequest $request)
    {
        $input = $request->all();

        $return = $this->manual_notification_reports->postUpdateInfo($incident, $input);
        $redirect_url = route('backend.claim.manual_notification.dashboard', $incident->id);
        //logger($redirect_url);
        return response()->json(['success' => true, 'redirect_url' => $redirect_url, 'message' => '']);

    }

    public function postManualPayment(ManualNotificationReport $incident, UpdateManualPaymentRequest $request)
    {
        $input = $request->all();
        $return = $this->manual_notification_reports->postManualPayment($incident, $input);

        $redirect_url = route('backend.claim.manual_notification.dashboard', $incident->id);
//        return response()->json(['success' => true, 'redirect_url' => $redirect_url, 'message' => '']);
        return redirect()->back()->withFlashSuccess("Success, Manual payment(s) have been updated.");
    }



    /**
     * @param ManualNotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateOshData(ManualNotificationReport $incident)
    {
        $codeValueRepo = new CodeValueRepository();
        $incident_types = (new IncidentTypeRepository())->getAll()->pluck('name', 'id')->all();
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->getReportModel(30, $incident->id, 'resourceid');
        $districts = (new DistrictRepository())->getAll()->pluck('name', 'id')->all();
        $claim_status = $codeValueRepo->query()->select(['name', 'id'])->where('code_id', 54)->get()->pluck('name', 'id')->all();
        $rejection_reason = $codeValueRepo->query()->select(['name', 'id'])->where('code_id', 55)->get()->pluck('name', 'id')->all();
        $rejection_cv_id = $codeValueRepo->query()->select(['id'])->where(['code_id' => 54, 'reference' => 'MANNOTST02'])->first()->id;
        $status_cv_ref = ($incident->status_cv_id) ? $incident->status->reference : null;
        $accident_cause_types = $codeValueRepo->getCodeValuesByCodeForSelect(58);
        $accident_cause_agencies = $codeValueRepo->getCodeValuesByCodeForSelect(57);
        $cv_repo = $codeValueRepo;
        /*Health providers*/
        $hcp_ids = $incident->hcps()->pluck('health_providers.id')->toArray();//hcp synced
        $health_providers = HealthProvider::query()->whereIn('id', $hcp_ids)->get()->pluck('fullname_info', 'id');

        return view('backend/operation/claim/manual_notification_report/complete_registration/update_osh_data')
            ->with('incident', $incident)
            ->with('incident_types', $incident_types)
            ->with('cr', $cr)
            ->with('districts', $districts)
            ->with('claim_status', $claim_status)
            ->with('rejection_reason', $rejection_reason)
            ->with('rejection_cv_id', $rejection_cv_id)
            ->with('accident_cause_types', $accident_cause_types)
            ->with('accident_cause_agencies', $accident_cause_agencies)
            ->with('status_cv_ref', $status_cv_ref)
            ->with('cv_repo', $cv_repo)
            ->with('health_providers', $health_providers)
            ->with('hcp_ids', $hcp_ids);
    }

    /*Update OSH DATA*/
    public function postOshData(ManualNotificationReport $incident, UpdateManualNotificationOshRequest $request)
    {
        $input = $request->all();
        $return = $this->manual_notification_reports->postOshData($incident, $input);
        return redirect()->back()->withFlashSuccess("Success, OSH Data updated");
    }

    /**
     * Upload manual files - notification report
     */
    public function uploadManualFile(UploadManualNotificationReportRequest $request)
    {
        $input = $request->all();
        $this->manual_notification_reports->saveUploadedDocumentFromExcel($input);
        return redirect()->route('backend.claim.manual_notification.upload_page')->withFlashSuccess('Success, document has been uploaded');
    }



    /**
     * @param $id
     * Manual notification report
     */
    public function createEmployee($id)
    {
        $manual_notification_report = $this->manual_notification_reports->find($id);
        $manual_payroll_member = $manual_notification_report->manualPayrollMembers($manual_notification_report->incident_type_id)->first();
        return view('backend/operation/claim/manual_notification_report/employee/create')
            ->with('manual_notification_report', $manual_notification_report)
            ->with("countries", (new CountryRepository())->getAll()->pluck('name', 'id'))
            ->with("genders", (new GenderRepository())->getAll()->pluck('name', 'id'))
            ->with("location_types", (new LocationTypeRepository())->getAll()->pluck('name', 'id'))
            ->with("job_titles", (new JobTitleRepository())->getAll()->pluck('name', 'id'))
            ->with("identities", (new IdentityRepository())->getAll()->pluck('name', 'id'))
            ->with("employee_categories", (new CodeValueRepository())->query()->where('code_id',7)->get()->pluck('name', 'id'))
            ->with("marital_statuses", (new CodeValueRepository())->query()->where('code_id',8)->get()->pluck('name', 'id'))
            ->with('manual_payroll_member', $manual_payroll_member);
    }

    /*Store employee and auto sync with manual file*/
    public function storeEmployee($id, CreateEmployeeFromManualFileRequest $request)
    {
        $input = $request->all();
        $this->manual_notification_reports->createEmployee($id, $input);
        return  redirect()->route('backend.claim.manual_notification.pending_synchronizations')->withFlashSuccess('Success, Member has been synced to notification file');
    }


    public function openPendingSyncPage()
    {
        return view('backend/operation/claim/manual_notification_report/pending_sync/pending_sync');
    }

    /*open sync member page - employee and employer*/
    public function syncMemberPage($notification_report_id, $option = NULL)
    {
        $return_url = '';
        if ($option) {
            $return_url = route('backend.claim.manual_notification.dashboard', $notification_report_id);
        }
        $notification = $this->manual_notification_reports->find($notification_report_id);
        return view('backend/operation/claim/manual_notification_report/synchronize/sync_member')
            ->with('notification_report', $notification)
            ->with('return_url', $return_url);
    }

    /*Sync member*/
    public function syncMember($notification_report_id, ChooseEmployeeRequest $request)
    {
        $input = $request->all();
        if(isset($input['employer_unavailable'])){
            /*Redirect to unregistered*/
            return  redirect()->route('backend.compliance.employer_registration.name_check');
        }elseif(isset($input['employee_unavailable'])){
            /*Redirect to add new employee*/
            return  redirect()->route('backend.claim.manual_notification.create_employee', $notification_report_id);
        }else{
            $this->manual_notification_reports->updateSyncMemberStatus($notification_report_id, $input);
            if ($input['return_url']) {
                return redirect($input['return_url']);
            }
            return  redirect()->route('backend.claim.manual_notification.pending_synchronizations')->withFlashSuccess('Success, Member has been synced to notification file');
        }

    }


    public function openCompleteRegistrationPage($notification_report_id)
    {
        $incident_repo = new IncidentTypeRepository();
        $incident_types = $incident_repo->getAll()->pluck('name','id');
        $bank_repo = new BankRepository();
        $banks = $bank_repo->getActiveBanksForSelect();
        $notification = $this->manual_notification_reports->find($notification_report_id);
        $pensioner = $notification->manualPayrollMembers($notification->incident_type_id)->where('member_type_id', 5)->first();
        $pensioner = isset($pensioner) ? $pensioner : null;
        return view('backend/operation/claim/manual_notification_report/complete_registration/complete')
            ->with('notification_report', $notification)
            ->with('incident_types', $incident_types)
            ->with('banks', $banks)
            ->with('pensioner', $pensioner);
    }


    /*complete registration*/
    public function completeRegistration($notification_report_id, CompleteManualNotificationReportRequest $request)
    {
        $input = $request->all();
        $this->manual_notification_reports->completeRegistration($notification_report_id, $input);
        $hasarrears = isset($input['hasarrears']) ? $input['hasarrears'] : 0;
        if($hasarrears == 1){
            /*Has arrears*/
            return redirect()->route('backend.payroll.recovery.create_manual_arrears', $input['pensioner_id'])->withFlashSuccess('Success, Manual file has completed synchronization! Proceed to initiate approval of arrears');
        }else{
            /*no arrears*/
            return  redirect()->route('backend.claim.manual_notification.index')->withFlashSuccess('Success, Manual file has completed synchronization');
        }

    }

    /**
     * @param $notification_report_id
     * Destroy manual notification
     */
    public function destroy($notification_report_id)
    {
        $this->manual_notification_reports->destroy($notification_report_id);
        return  redirect()->back()->withFlashSuccess('Success, Manual file has been removed');
    }


    /*Update has payroll flag*/
    public function updateHasPayroll(ManualNotificationReport $manualNotificationReport, $status)
    {
        $this->manual_notification_reports->updateHasPayrollFlag($manualNotificationReport, $status);
        return  redirect()->back()->withFlashSuccess('Success, Payroll flag has been updated');
    }


    /*Update status*/
    public function updateStatus(ManualNotificationReport $manualNotificationReport, $status)
    {
        $this->manual_notification_reports->updateStatus($manualNotificationReport, $status);
        return redirect()->route('backend.claim.manual_notification.dashboard', $manualNotificationReport->id)->withFlashSuccess('Success, Status has been updated');
    }

    /**
     * @param ManualNotificationReport $manualNotificationReport
     * @param $action_type i.e. 1 => Attend, 0 => un Attend file
     */
    public function updateAttendantThisFile(ManualNotificationReport $manualNotificationReport,$action_type)
    {
        $this->manual_notification_reports->updateAttendantForThisFile($manualNotificationReport, access()->id(),$action_type);
        return redirect()->back()->withFlashSuccess('Success, Attendant updated');
    }

    /**
     * @param NotificationReport $notificationReport
     * @return mixed
     * Send to DMS
     */
    public function sendToDms(ManualNotificationReport $manualNotificationReport)
    {
        $manual_report = $this->manual_notification_reports->find($manualNotificationReport->id);
        dispatch(new PostManualNotificationDms($manual_report));
        return redirect()->back()->withFlashSuccess("Request to create file to eOffice has been made, if the file has not been created, try again later");
    }


    /**
     * @param NotificationReport $notificationReport
     * @param $pivotDocumentId
     * @return mixed
     * Open Document from e-office using e-Office document id
     */
    public function previewDocumentFromDms($document_pivot_id)
    {
        $document_pivot = DB::table('document_manual_notification_report')->where('id', $document_pivot_id)->first();
        $documentType = $document_pivot->document_id;
        $url = $this->previewDocumentFromEofficeGeneral($document_pivot->manual_notification_report_id, $documentType, $document_pivot->eoffice_document_id);
        return response()->json(['success' => true, "url" => $url, "name" => $document_pivot->description, "id" => $document_pivot_id]);
    }



    /**
     * @param NotificationReport $notificationReport
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDocumentLibrary(ManualNotificationReport $manualNotificationReport)
    {
        $this->base = $this->real(temporary_dir()  );
        $this->base = $this->base . DIRECTORY_SEPARATOR . 'manual_claims'. DIRECTORY_SEPARATOR . $manualNotificationReport->id;
        $this->makeDirectory($this->base);
        $this->id_no = $manualNotificationReport->id;
//        $this->notificationReportFolder = $manualNotificationReport->pluck('id', 'id')->all();
        $this->documents = $manualNotificationReport->documents->pluck('name', 'id')->all();
        $uploadedDocuments = [];

        foreach ($manualNotificationReport->documents as $document) {
            $uploadedDocuments += [$document->pivot->id => $document->pivot->description];
        }

        $this->uploadedDocuments = $uploadedDocuments;
        /*       dd($uploadedDocuments);*/
        /*<new> Temp tree*/
        $this->createTempDocumentFilesTree($manualNotificationReport, $this->base);
        /*end tree*/

        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_general($node, (isset(request()->id) && request()->id === '#'));


        return response()->json($rslt);
    }



    /*Create temporary document files*/
    public function createTempDocumentFilesTree($manualNotificationReport, $base)
    {

        if(env('SELF_DMS') == 0) {
            $manual_notification_report_id = $manualNotificationReport->id;
            $documents = $manualNotificationReport->documents;

            foreach ($documents as $document) {
                $document_id = $document->pivot->document_id;
                $source = $document->pivot->name;
                $path = $base . DIRECTORY_SEPARATOR . $document_id;
                $this->makeDirectory($path);
                if ($source == 'e-office') {
                    /*Unlink all files on this folder - Of E-office Only*/
                    $this->deleteAllFilesOnFolder($path);
                    /*end unlink*/
                    $uploaded_docs = $manualNotificationReport->documents()->where('document_id', $document_id)->get();
                    foreach ($uploaded_docs as $uploadedDocument) {
                        $document_pivot_id = $uploadedDocument->pivot->id;
                        $fp = fopen($path . '/' . $document_pivot_id . ".pdf", "wb");
                        fwrite($fp, 'Open from E-Office');
                        fclose($fp);
                    }


                    /*end creating temp*/
                }
            }
        }
    }

    /**
     * @return mixed
     * Get all for dataTable
     */
    public function getAllForDataTable()
    {
        return Datatables::of($this->manual_notification_reports->getForDataTable()->orderBy('issynced','asc')->orderBy('ismembersynced','desc'))
            ->addColumn('employee', function ($notification) {
                return (isset($notification->employee_id)) ? $notification->employee->name : $notification->employee_name;
            })
            ->addColumn('employer', function ($notification) {
                return (isset($notification->employer_id)) ? $notification->employer->name : $notification->employer_name;
            })
            ->addColumn('incident_type', function ($notification) {
                return (isset($notification->incident_type_id)) ? $notification->incidentType->name : '';
            })
            ->addColumn('incident_date_formatted', function ($notification) {
                return  isset($notification->incident_date) ?  short_date_format($notification->incident_date) : ' ';
            })
            ->addColumn('reporting_date_formatted', function ($notification) {
                return  isset($notification->reporting_date)  ? short_date_format($notification->reporting_date) : ' ';
            })
            ->addColumn('notification_date_formatted', function ($notification) {
                return  isset($notification->notification_date)  ?  short_date_format($notification->notification_date) : ' ';
            })
            ->addColumn('date_of_mmi_formatted', function ($notification) {
                return isset($notification->date_of_mmi)  ?  short_date_format($notification->date_of_mmi) : ' ';
            })
            ->addColumn('status', function ($notification) {
                return $notification->status_label .  $notification->remove_button;
            })
            ->addColumn('action', function ($notification) {
                return $notification->action_button;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }



    /*Get All pending fo payroll*/
    public function getAllForPayrollForDataTable()
    {
        return Datatables::of($this->manual_notification_reports->getAllForPayrollDataTable()->orderBy('id','desc')->orderBy('issynced','asc')->orderBy('ismembersynced','desc'))
            ->addColumn('employee', function ($notification) {
                return (isset($notification->employee_id)) ? $notification->employee->name : $notification->employee_name;
            })
            ->addColumn('employer', function ($notification) {
                return (isset($notification->employer_id)) ? $notification->employer->name : $notification->employer_name;
            })
            ->addColumn('incident_type', function ($notification) {
                return (isset($notification->incident_type_id)) ? $notification->incidentType->name : '';
            })
            ->addColumn('incident_date_formatted', function ($notification) {
                return  isset($notification->incident_date) ?  short_date_format($notification->incident_date) : ' ';
            })
            ->addColumn('reporting_date_formatted', function ($notification) {
                return  isset($notification->reporting_date)  ? short_date_format($notification->reporting_date) : ' ';
            })
            ->addColumn('notification_date_formatted', function ($notification) {
                return  isset($notification->notification_date)  ?  short_date_format($notification->notification_date) : ' ';
            })
            ->addColumn('date_of_mmi_formatted', function ($notification) {
                return isset($notification->date_of_mmi)  ?  short_date_format($notification->date_of_mmi) : ' ';
            })
            ->addColumn('status', function ($notification) {
                return $notification->status_label .  $notification->remove_button;
            })
            ->addColumn('action', function ($notification) {
                return $notification->action_button;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }


    /*Get Pending sync for datatable*/
    public function getPendingSyncForDataTable()
    {
        return Datatables::of($this->manual_notification_reports->getForDataTable()->where('ismembersynced', 0))
            ->addColumn('employee', function ($notification) {
                return (isset($notification->employee_id)) ? $notification->employee->name : $notification->employee_name;
            })
            ->addColumn('employer', function ($notification) {
                return (isset($notification->employer_id)) ? $notification->employer->name : $notification->employer_name;
            })
            ->addColumn('incident_type', function ($notification) {
                return (isset($notification->incident_type_id)) ? $notification->incidentType->name : '';
            })
            ->addColumn('incident_date_formatted', function ($notification) {
                return  isset($notification->incident_date) ?  short_date_format($notification->incident_date) : ' ';
            })
            ->addColumn('reporting_date_formatted', function ($notification) {
                return  isset($notification->reporting_date)  ? short_date_format($notification->reporting_date) : ' ';
            })
            ->addColumn('notification_date_formatted', function ($notification) {
                return  isset($notification->notification_date)  ?  short_date_format($notification->notification_date) : ' ';
            })
            ->addColumn('date_of_mmi_formatted', function ($notification) {
                return isset($notification->date_of_mmi)  ?  short_date_format($notification->date_of_mmi) : ' ';
            })
            ->addColumn('status', function ($notification) {
                return $notification->status_label;
            })
            ->addColumn('action', function ($notification) {
                return $notification->sync_member_button;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }


}
