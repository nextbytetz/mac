<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\NotificationContributionTrackRequest;
use App\Repositories\Backend\Operation\Claim\NotificationContributionTrackRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;

class NotificationContributionTrackController extends Controller
{


    protected $tracks;


    public function __construct(){
        $this->tracks = new NotificationContributionTrackRepository();

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend.operation.claim.notification_report.support.contribution_track.index');
    }

    /**
     * @param $notification_report_id
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function profile($notification_report_id)
    {
        $notificationReport = new NotificationReportRepository();
        $notification_report = $notificationReport->findOrThrowException($notification_report_id);
        $contrib_month = $this->tracks->findRequiredContribution($notification_report->incident_date);
        $check_if_pending_compliance = $this->tracks->checkIfPendingCompliance($notification_report);
        return view('backend.operation.claim.notification_report.support.contribution_track.profile')
            ->withNotificationReport($notification_report)
            ->withContribMonth($contrib_month)
            ->withCheckIfPendingCompliance($check_if_pending_compliance);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->tracks->checkIfCanUpdate($id);
        $track = $this->tracks->findOrThrowException($id);
        $notificationReport = new NotificationReportRepository();
        $notification_report = $notificationReport->findOrThrowException($track->notification_report_id);
        return view('backend.operation.claim.notification_report.support.contribution_track.edit')
            ->withNotificationReport($notification_report)
            ->withTrack($track);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotificationContributionTrackRequest $request, $id)
    {
        //
        $track = $this->tracks->findOrThrowException($id);
        $this->tracks->update($id, $request->all());
        return redirect()->route('backend.claim.notification_contribution_track.profile', $track->notification_report_id)->withFlashSuccess(trans('alerts.backend.claim.notification_contribution_track_updated'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @return mixed
     * Get Tracks for DataTable for this notification report
     */
    public function getForDataTable($notification_report_id)
    {

        return Datatables::of($this->tracks->getForDataTable($notification_report_id))
            ->editColumn('from_user_id', function($track) {
                return ($track->from_user_id) ? $track->fromUser->username : '(Officer)';
            })
            ->editColumn('user_id', function($track) {
                return ($track->user_id) ? $track->user->username : '(Officer)';
            })
            ->editColumn('unit_id', function($track) {
                return $track->unit->name;
            })
            ->addColumn('receive_date', function($track) {
                return $track->created_at_formatted;
            })
            ->addColumn('forwarded_date', function($track) {
                return ($track->forward_date) ? $track->forward_date_formatted : ' ';
            })

            ->make(true);
    }






    public function getPendingForDataTable()
    {

             /*get pending for compliance*/
        if(access()->user()->unit_id == 15){
            return $this->getPendingComplianceForDataTable();
        }elseif(access()->user()->unit_id == 14){
            /* get pending for claim */
            return $this->getPendingClaimForDataTable();
        }
    }



    /**
     * @return mixed
     * Get for DataTable All pending on compliance level
     */
    public function getPendingComplianceForDataTable()
    {

        return Datatables::of($this->tracks->getPendingComplianceForDataTable())
            ->addColumn('employee', function($notification_report) {
                return $notification_report->employee->name;
            })
            ->addColumn('employer', function($notification_report) {
                return $notification_report->employer->name;
            })
            ->addColumn('contrib_month', function($notification_report) {
                return $this->tracks->findRequiredContribution($notification_report->incident_date);
            })
            ->editColumn('incident_type_id', function($notification_report) {
                return $notification_report->incidentType->name;
            })
            ->editColumn('user_id', function($notification_report) {
                return $notification_report->user->username;
            })
            ->make(true);
    }





    /**
     * @return mixed
     * Get for DataTable all pending on claim administration level
     */
    public function getPendingClaimForDataTable()
    {

        return Datatables::of($this->tracks->getPendingClaimForDataTable())
            ->addColumn('employee', function($notification_report) {
                return $notification_report->employee->name;
            })
            ->addColumn('employer', function($notification_report) {
                return $notification_report->employer->name;
            })
            ->addColumn('contrib_month', function($notification_report) {
                return $this->tracks->findRequiredContribution($notification_report->incident_date);
            })
            ->editColumn('incident_type_id', function($notification_report) {
                return $notification_report->incidentType->name;
            })
            ->editColumn('user_id', function($notification_report) {
                return $notification_report->user->username;
            })
            ->make(true);
    }



}
