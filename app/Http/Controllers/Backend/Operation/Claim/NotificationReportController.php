<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\DataTables\Claim\OnlineAccountDataTable;
use App\DataTables\Compliance\Member\EmployerIncidentDataTable;
use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Events\Sockets\Notification\NewReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\ApproveRejectionAppealRequest;
use App\Http\Requests\Backend\Operation\Claim\AssignInvestigatorRequest;
use App\Http\Requests\Backend\Operation\Claim\ChooseEmployeeRequest;
use App\Http\Requests\Backend\Operation\Claim\ChooseIncidentTypeRequest;
use App\Http\Requests\Backend\Operation\Claim\ClaimAssessmentProgressiveRequest;
use App\Http\Requests\Backend\Operation\Claim\ClaimAssessmentRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateCurrentEmployeeStateRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateHealthProviderServiceRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMaeMemberMultipleRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMaeMemberRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateMedicalExpenseRequest;
use App\Http\Requests\Backend\Operation\Claim\CreateWitnessRequest;
use App\Http\Requests\Backend\Operation\Claim\DoctorNotificationReportUpdateRequest;
use App\Http\Requests\Backend\Operation\Claim\IncidentClosureRequest;
use App\Http\Requests\Backend\Operation\Claim\PdAssessmentProgressiveRequest;
use App\Http\Requests\Backend\Operation\Claim\PdAssessmentRequest;
use App\Http\Requests\Backend\Operation\Claim\RegisterEmployee;
use App\Http\Requests\Backend\Operation\Claim\RegisterNotificationRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateBankDetailsRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateChecklistRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateDetailsRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateInvestigationReportRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateMaeMemberMultipleRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateManualPaymentRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateMonthlyEarningRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdateProgressiveNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Claim\UpdatingNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateIndividualContributionRequest;
use App\Models\Operation\Claim\AssessmentDocument;
use App\Models\Operation\Claim\NotificationDisabilityStateAssessment;
use App\Models\Workflow\WfDocumentCheck;
use App\Models\Workflow\WfTrack;
use App\Notifications\Backend\Operation\Claim\Portal\DocumentUploadedOnline;
use App\Repositories\Backend\Operation\Claim\NotificationDisabilityStateAssessmentRepository;
use App\Repositories\Backend\Operation\Claim\Traits\ClaimCompensation\UndoClaimCompensationTrait;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;
use App\Jobs\Claim\CheckUploadedDocuments;
use App\Jobs\Claim\CheckUploadedDocumentsNotification;
use App\Jobs\Claim\PostNotificationDms;
use App\Models\Operation\Claim\BenefitTypeClaim;
use App\Models\Operation\Claim\HealthProvider;
use App\Models\Operation\Claim\MedicalExpense;
use App\Models\Operation\Claim\NotificationEligibleBenefit;
use App\Models\Operation\Claim\NotificationHealthProvider;
use App\Models\Operation\Claim\NotificationInvestigator;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\Portal\Claim;
use App\Models\Operation\Claim\PortalIncident;
use App\Models\Operation\Claim\Witness;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Reporting\ConfigurableReport;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Operation\Claim\AccidentTypeRepository;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\BodyPartInjuryRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\ClaimRepository;
use App\Repositories\Backend\Operation\Claim\DeathCauseRepository;
use App\Repositories\Backend\Operation\Claim\DisabilityStateChecklistRepository;
use App\Repositories\Backend\Operation\Claim\DiseaseKnowHowRepository;
use App\Repositories\Backend\Operation\Claim\DocumentGroupRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\HealthProviderRepository;
use App\Repositories\Backend\Operation\Claim\HealthServiceChecklistRepository;
use App\Repositories\Backend\Operation\Claim\HealthStateChecklistRepository;
use App\Repositories\Backend\Operation\Claim\IncidentClosureRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Access\IncidentUserRepository;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use App\Repositories\Backend\Operation\Claim\MedicalExpenseRepository;
use App\Repositories\Backend\Operation\Claim\MedicalPractitionerRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Claim\NotificationDisabilityStateRepository;
use App\Repositories\Backend\Operation\Claim\NotificationEligibleBenefitRepository;
use App\Repositories\Backend\Operation\Claim\NotificationHealthProviderRepository;
use App\Repositories\Backend\Operation\Claim\NotificationInvestigatorRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\OccupationRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdAmputationRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdImpairmentRepository;
use App\Repositories\Backend\Operation\Claim\Pd\PdInjuryRepository;
use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\Repositories\Backend\Operation\Claim\WitnessRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use App\Repositories\Backend\Reporting\DashboardRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\JobTitleRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use App\Exceptions\GeneralException;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Repositories\Backend\Operation\Claim\NotificationWorkflowRepository;

class NotificationReportController extends Controller
{
    use AttachmentHandler, FileHandler, UndoClaimCompensationTrait;
    /**
     * Display a listing of the resource.
     *updateDocument
     * @return \Illuminate\Http\Response
     */
    protected $notification_reports;
    protected $employees;
    protected $incident_types;
    protected $accident_types;
    protected $disease_know_hows;
    protected  $death_causes;
    protected $districts;
    protected  $member_types;
    protected  $insurances;
    protected  $medical_expenses;
    protected  $notification_investigators;
    protected  $health_providers;
    protected  $medical_practitioners;
    protected  $health_service_checklists;
    protected  $notification_health_providers;
    protected $health_state_checklists;
    protected $disability_state_checklists;
    protected $benefit_types;
    protected $claims;
    protected $witnesses;
    protected $claim_compensations;
    protected $banks;
    protected $bank_branches;
    protected $code_values;
    protected $employers;
    protected $dependents;

    /* start : Handling Documents */
    protected $base = null;
    protected $documents = null;
    //protected $documentGroups = null;
    protected $uploadedDocuments = null;
    protected $notificationReportFolder = null;
    /* end : Handling Documents */

    /**
     * NotificationReportController constructor.
     * @param NotificationReportRepository $notification_reports
     * @param EmployeeRepository $employees
     * @param IncidentTypeRepository $incident_types
     * @param AccidentTypeRepository $accident_types
     * @param DiseaseKnowHowRepository $disease_know_hows
     * @param DeathCauseRepository $death_causes
     * @param DistrictRepository $districts
     * @param MemberTypeRepository $member_types
     * @param InsuranceRepository $insurances
     * @param MedicalExpenseRepository $medical_expenses
     * @param NotificationInvestigatorRepository $notification_investigators
     * @throws GeneralException
     */
    public function __construct(NotificationReportRepository $notification_reports, EmployeeRepository $employees, IncidentTypeRepository $incident_types, AccidentTypeRepository $accident_types, DiseaseKnowHowRepository $disease_know_hows, DeathCauseRepository $death_causes, DistrictRepository $districts, MemberTypeRepository $member_types, InsuranceRepository $insurances, MedicalExpenseRepository $medical_expenses, NotificationInvestigatorRepository $notification_investigators) {
        $this->notification_reports = $notification_reports;
        $this->employees = $employees;
        $this->incident_types = $incident_types;
        $this->accident_types = $accident_types;
        $this->disease_know_hows = $disease_know_hows;
        $this->death_causes = $death_causes;
        $this->districts = $districts;
        $this->member_types = $member_types;
        $this->insurances = $insurances;
        $this->medical_expenses = $medical_expenses;
        $this->notification_investigators = $notification_investigators;
        $this->health_providers = new HealthProviderRepository();
        $this->medical_practitioners = new MedicalPractitionerRepository();
        $this->health_service_checklists = new HealthServiceChecklistRepository();
        $this->notification_health_providers = new NotificationHealthProviderRepository();
        $this->health_state_checklists = new HealthStateChecklistRepository();
        $this->disability_state_checklists = new DisabilityStateChecklistRepository();
        $this->benefit_types = new BenefitTypeRepository();
        $this->claims= new ClaimRepository();
        $this->witnesses = new WitnessRepository();
        $this->claim_compensations = new ClaimCompensationRepository();
        $this->banks = new BankRepository();
        $this->bank_branches = new BankBranchRepository();
        $this->code_values = new CodeValueRepository();
        $this->employers = new EmployerRepository();
        $this->dependents = new DependentRepository();

        $this->middleware('access.routeNeedsPermission:assign_investigation', ['only' => ['assignInvestigators', 'storeInvestigators', 'changeInvestigator']]);
        $this->middleware('access.routeNeedsPermission:unassign_investigation', ['only' => ['deleteInvestigator']]);
        $this->middleware('access.routeNeedsPermission:fill_investigation', ['only' => ['updateInvestigationReport']]);
        $this->middleware('access.routeNeedsPermission:activate_investigation', ['only' => ['investigate']]);
        $this->middleware('access.routeNeedsPermission:cancel_investigation', ['only' => ['cancelInvestigation']]);
        $this->middleware('access.routeNeedsPermission:create_notification', ['only' => ['register']]);
        $this->middleware('access.routeNeedsPermission:notification_appeal', ['only' => ['approveRejectionAppealPage']]);
        //allocation
        $this->middleware('access.routeNeedsPermission:resource_allocation', ['only' => ['assignAllocation']]);
        //getCloseIncident
        $this->middleware('access.routeNeedsPermission:close_incident', ['only' => ['getCloseIncident']]);
        //do manual payment
        $this->middleware('access.routeNeedsPermission:update_manual_payment', ['only' => ['doManualPayment', 'postUndoManualPayment']]);
        //undo approval
        $this->middleware('access.routeNeedsPermission:undo_notification_approval', ['only' => ['undoApproval']]);
        //notification defaults
        $this->middleware('access.routeNeedsPermission:configure_notification_defaults', ['only' => ['defaults']]);

        /* start : Initialise document store directory */
        $this->base = $this->real(notification_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }
        /* end : Initialise document store directory */

    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function administration()
    {
        return view('backend/operation/claim/administration/menu');
    }

    public function chooseEmployee()
    {
        return view("backend.operation.claim.notification_report.choose_employee");
    }

    public function postChooseEmployee(ChooseEmployeeRequest $request)
    {
        $input = $request->all();
        if (!isset($input['employer_unavailable'])) {
            if (!isset($input['employee_unavailable'])) {
                //Employee is available in the system
                $employee = $input['employee'];
            } else {
                //Employee is not available in the system
                $employee = 0;
            }
            $employer = $input['employer'];
        } else {
            $employer = 0;
            $employee = 0;
        }

        return redirect()->route("backend.compliance.employee.choose_incident_type", $employee)->with('employer', $employer);
    }

    /**
     * @param NotificationReport $incident
     * @return $this
     * @throws GeneralException
     */
    public function editNatureOfIncident(NotificationReport $incident)
    {
        //
        $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $incident->id]);
        $assessment_level = $workflow->claimAssessmentLevel();
        $workflow->checkLevel($workflow->claimAssessmentLevel());
        access()->hasWorkflowDefinition(3, $assessment_level);
        //$notification_report = $this->notification_reports->findOrThrowException($id);
        return view('backend.operation.claim.notification_report.support.doctors_modification')
            ->with("notification_report", $incident)
            ->with("natures_of_incident", $this->code_values->query()->where('code_id', 3)->get()->pluck('name','id'));

    }

    /**
     * @param $id
     * @param DoctorNotificationReportUpdateRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function updateNatureOfIncident($id , DoctorNotificationReportUpdateRequest $request )
    {
        //
        $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $id]);
        $assessment_level = $workflow->claimAssessmentLevel();
        $workflow->checkLevel($workflow->claimAssessmentLevel());
        access()->hasWorkflowDefinition(3,$assessment_level);
        $notification_report = $this->notification_reports->updateNatureOfIncident($id,$request->all());
        return redirect('claim/notification_report/profile/' . $notification_report->id . '#general')->withFlashSuccess(trans('alerts.backend.claim.notification_report_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_original($id)
    {
        //
        $this->checkLevel1Rights($id);
        $notification_report = $this->notification_reports->delete($id);
        return redirect()->route('backend.compliance.employee.profile', $notification_report->employee_id)->withFlashSuccess(trans('alerts.backend.claim.notification_report_undone'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        //
        $incident = $this->notification_reports->findOrThrowException($id);
        if ($incident->isprogressive) {
            // access()->assignedForNotification($incident);
            //Progressive Notification Report
            $this->notification_reports->undoProgressive($incident);
            //route ==> backend.claim.notification_report.choose_employee
            $return = redirect()->route('backend.claim.notification_report.choose_employee')->withFlashSuccess(trans('alerts.backend.claim.notification_report_undone'));
        } else {
            $this->checkLevel1Rights($id);
            if($incident->status == 1 || $incident->status == 0){
                /*approved*/
                $wf_module_group_id = 3;
            } elseif ($incident->status == 2){
                /*rejection*/
                $wf_module_group_id = 4;
            }
            $incident = $this->notification_reports->undo($id, $wf_module_group_id);
            $return = redirect()->route('backend.compliance.employee.profile', $incident->employee_id)->withFlashSuccess(trans('alerts.backend.claim.notification_report_undone'));
        }
        return $return;
    }


    /**
     * @return mixed
     * @throws \Exception
     * @description Recall Notifications : Get for dataTable
     */
    public function getForDataTable()
    {
        // Log::info(print_r($this->notification_reports->getForDataTable(),true));
        return Datatables::of($this->notification_reports->getForDataTable())
            ->editColumn('status', function ($notification_report) {
                return $notification_report->status_label;
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function recall()
    {
        //
        return view('backend.operation.claim.notification_report.recall_notifications')
            ->with("stages", $this->code_values->getCodeValuesNotificationStageForSelect())
            ->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all());
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     */
    public function ackwlgletter(NotificationReport $incident)
    {
        $pending_documents = $this->notification_reports->checkAllMandatoryDocumentsForLetter($incident->id);
        $documentRepo = new DocumentRepository();
        $count = $documentRepo->getTotalIncidentDocuments($incident->incident_type_id);
        $folio = $count + 1;
        return view('backend/operation/claim/notification_report/ackwlgletter')
            ->with("notification_report", $incident)
            ->with("pending_documents", $pending_documents)
            ->with("folio", $folio);
    }

    public function printLetter(NotificationReport $incident)
    {

    }

    public function openLetter(NotificationReport $incident)
    {

    }

    public function saveAckwlgletter(NotificationReport $notification_report)
    {
        $input = request()->all();
        $return = $this->notification_reports->saveAckwlgletter($notification_report, $input);
        return response()->json(['success' => true]);
    }

    public function issueAckwlgletter(NotificationReport $notification_report)
    {
        $return = $this->notification_reports->issueAckwlgletter($notification_report);
        return response()->json(['success' => true]);
    }

    /**
     * @param NotificationReport $incident
     * @param WorkflowTrackDataTable $workflowtrack
     * @return $this
     * @throws GeneralException
     */
    public function profile(NotificationReport $incident, WorkflowTrackDataTable $workflowtrack)
    {
        //Return the instance of the view
        return $this->notification_reports->profile($incident)
            ->with("workflowtrack", $workflowtrack);
    }

    public function editDocuments(NotificationReport $incident)
    {
        $documentGroup = new DocumentGroupRepository();
        $documentGroups = $documentGroup->getDocumentGroups($incident->incident_type_id);
        //dd($documentGroups);
        return view("backend/operation/claim/notification_report/edit_documents")
            ->with("notification_report", $incident)
            ->with("document_groups", $documentGroups)
            ->with("notification_report_documents", $incident->documents->pluck('id')->all());
    }




    public function updateDocuments($file_number, Request $request)
    {
        $input = $request->all();

        /**
         * e-office integration, if source in the request is 2 then the request come from e-office and should return an acknowledgement.
         *
         */
        $this->notification_reports->updateDocument($file_number, $input);
//        $this->base = $this->base . DIRECTORY_SEPARATOR . $notificationReport->id;
//        $this->makeNotificationReportDocumentDirectory($notificationReport);

        //to be deleted, only used for testing
        //$input["fileNumber"] = $file_number;
        //Log::info($input);

        if ($input['source'] == 2) {
            //Request came from e-office system, respond to e-office
            return response()->json(['message' => "SUCCESS"]);
        }

//        return redirect()->route('backend.claim.notification_report.profile', $notificationReport->id)->withFlashSuccess(trans('alerts.backend.notification.document_updated'));
    }



//    public function updateDocuments(NotificationReport $notificationReport, Request $request)
//    {
//        $input = $request->all();
//
//        /**
//         * e-office integration, if source in the request is 2 then the request come from e-office and should return an acknowledgement.
//         *
//         */
//        $this->notification_reports->updateDocument($notificationReport, $input);
//        $this->base = $this->base . DIRECTORY_SEPARATOR . $notificationReport->id;
//        $this->makeNotificationReportDocumentDirectory($notificationReport);
//
//        //to be deleted, only used for testing
//        //$input["fileNumber"] = $notificationReport->id;
//        //Log::info($input);
//
//        if ($input['source'] == 2) {
//            //Request came from e-office system, respond to e-office
//            return response()->json(['message' => "SUCCESS"]);
//        }
//
//        return redirect()->route('backend.claim.notification_report.profile', $notificationReport->id)->withFlashSuccess(trans('alerts.backend.notification.document_updated'));
//    }



    public function deleteEofficeDocument($incident, Request $request)
    {
        $input = $request->all();
        /**
         * e-office integration, if source in the request is 2 then the request come from e-office and should return an acknowledgement.
         *
         */
        $this->notification_reports->deleteEofficeDocument($incident, $input);
        return response()->json(['message' => "SUCCESS"]);
    }

    /**
     * @param $incident
     * @description e-office integration, synchronising notification files in MAC.
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncIncident($incident)
    {
        $count = $this->notification_reports->query()->where("id", $incident)->count();
        if ($count) {
            $return = ['message' => TRUE];
        } else {
            $return = ['message' => FALSE];
        }
        return response()->json($return);
    }


    /**
     * @param NotificationReport $notificationReport
     * @return mixed
     */
    public function reloadDocuments(NotificationReport $notificationReport)
    {
        $this->notification_reports->reloadDocuments($notificationReport->id);
        //Check for submitted documents
        dispatch(new CheckUploadedDocuments());
        return redirect('claim/notification_report/profile/' . $notificationReport->id . '#document')->withFlashSuccess('Documents have been reloaded');
    }

    /**
     * @param NotificationReport $notificationReport
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDocumentLibrary(NotificationReport $notificationReport)
    {
        $this->base = $this->base . DIRECTORY_SEPARATOR . $notificationReport->id;
        $this->makeDirectory($this->base);
        $this->notificationReportFolder = $notificationReport->pluck('id', 'id')->all();
        $this->documents = $notificationReport->documents->pluck('name', 'id')->all();
        $uploadedDocuments = [];

        $documents = $notificationReport->documents()->select(['document_notification_report.id', 'document_notification_report.description', DB::raw('document_notification_report.created_at::date as created_date'), DB::raw('document_notification_report.doc_receive_date::date as receive_date')])->orderByDesc('document_notification_report.eoffice_document_id')->get();
        foreach ($documents as $document) {
            $pivot = $document;
            $uploadedDocuments += [$pivot->id => $pivot->description . " [<b>C:</b><span class='underline'>{$pivot->created_date}</span>,<b>R:</b><span class='underline'>{$pivot->receive_date}</span>]"];
        }
        //logger($uploadedDocuments);
        /*       dd($uploadedDocuments);*/
        /*<new> Temp tree*/
        $this->createTempDocumentFilesTree($notificationReport, $this->base);
        /*end tree*/
        $this->uploadedDocuments = $uploadedDocuments;
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst($node, (isset(request()->id) && request()->id === '#'));
        return response()->json($rslt);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Http\JsonResponse
     * @deprecated
     * @description Not Used.
     */
    public function showProgressiveDocumentLibrary(NotificationReport $incident)
    {
        $this->base = $this->base . DIRECTORY_SEPARATOR . $incident->id;
        $this->makeDirectory($this->base);
        $this->notificationReportFolder = $incident->pluck('id', 'id')->all();
        //$this->documents = $this->notification_reports->getProgressiveDocumentList($incident);
        $this->documents = (new DocumentRepository())->query()->select(["name", "id"])->whereIn("id", $this->notification_reports->getProgressiveDocumentList($incident))->pluck('name', 'id')->all();
        $uploadedDocuments = [];

        foreach ($incident->documents as $document) {
            $uploadedDocuments += [$document->pivot->id => $document->pivot->description];
        }

        $this->uploadedDocuments = $uploadedDocuments;
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst($node, (isset(request()->id) && request()->id === '#'));
        return response()->json($rslt);
    }


    /**
     * @param Model $notificationReport
     * @param $base
     */
    public function createTempDocumentFilesTree(Model $notificationReport, $base)
    {

        if(env('SELF_DMS') == 0)
        {
            $notification_report_id = $notificationReport->id;
            $documents = DB::table('document_notification_report')->select(['document_id', 'name'])->where('notification_report_id',$notification_report_id)->get();


            foreach($documents as $document)
            {
                $document_id = $document->document_id;
                $source = $document->name;

                $path = $base . DIRECTORY_SEPARATOR . $document_id;
                $this->makeDirectory($path);
                if ($source == 'e-office')
                {
                    /*Unlink all files on this folder - Of E-office Only*/
                    $this->deleteAllFilesOnFolder($path);
                    /*end unlink*/

                    /*Create temp files*/
                    $uploadedDocuments = $notificationReport->documents()->where('document_notification_report.document_id',$document_id)->get();
                    foreach($uploadedDocuments as $uploadedDocument){
                        $document_pivot_id = $uploadedDocument->pivot->id;
                        $fp = fopen($path . '/'. $document_pivot_id  .".pdf","wb");
                        fwrite($fp,'Open from E-Office');
                        fclose($fp);
                    }
                    /*end creating temp*/
                }


            }
        }
    }


    /**
     * Download Document General function
     * Check if Self dms or eoffice
     */
    public function downloadNotificationReportDocumentGeneral(NotificationReport $notificationReport, $documentId)
    {

        $document = $notificationReport->documents()->where("document_notification_report.id", $documentId)->first();
        if ($document->pivot->name == 'e-office'){
            return  $this->downloadNotificationReportDocumentDms($notificationReport, $documentId);
        }else{

            return  $this->downloadNotificationReportDocument($notificationReport, $documentId);
        }
    }

    public function downloadNotificationReportDocument(NotificationReport $notificationReport, $documentId)
    {
        $document = $notificationReport->documents()->where("document_notification_report.id", $documentId)->first();
        return response()->download($document->link($notificationReport->id), $document->linkName($notificationReport->id));
    }

    public function downloadNotificationReportDocumentDms(NotificationReport $notificationReport, $pivotDocumentId)
    {
        /**
         * e-office Integration, download notification document from e-office system.
         */
        /*new--*/
        $document = $notificationReport->documents()->where('document_notification_report.id', $pivotDocumentId)->first();
        $documentType = $document->pivot->document_id;
        $eOfficeDocumentId = $document->pivot->eoffice_document_id;
        /*end--*/

        $path = $this->getPathFromDms($notificationReport, $documentType, $eOfficeDocumentId);
        //$document = $notificationReport->documents()->where("document_notification_report.id", $documentId)->first();
        $path = $this->getPathFromDms($notificationReport, $documentType, $eOfficeDocumentId);
        //$path = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        return response()->download($path, $notificationReport->file_name_specific_label_underscore);
    }


    /**
     * Open Document General function
     * Check if Self dms or eoffice
     */
    public function openNotificationReportDocumentGeneral(NotificationReport $notificationReport, $documentId)
    {

        $document = $notificationReport->documents()->where("document_notification_report.id", $documentId)->first();
        if ($document->pivot->name == 'e-office'){
            return  $this->openNotificationReportDocumentDms($notificationReport, $documentId);
        }else{

            return  $this->openNotificationReportDocument($notificationReport, $documentId);
        }
    }

    public function openNotificationReportDocument(NotificationReport $notificationReport, $documentId)
    {

        $document = $notificationReport->documents()->where("document_notification_report.id", $documentId)->first();

        return response()->file($document->link($notificationReport->id));
    }


    /**
     * @param NotificationReport $notificationReport
     * @param $pivotDocumentId
     * @return mixed
     * Open Document from e-office using e-Office document id
     */
    public function openNotificationReportDocumentDms(NotificationReport $notificationReport, $pivotDocumentId)
    {
        /**
         * e-office Integration, open notification document from e-office system.
         */
        /*new--*/
        $document = $notificationReport->documents()->where('document_notification_report.id', $pivotDocumentId)->first();
        $documentType = $document->pivot->document_id;
        $eOfficeDocumentId = $document->pivot->eoffice_document_id;
        /*end--*/

        $path = $this->getPathFromDms($notificationReport, $documentType, $eOfficeDocumentId);
        //$path = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        return response()->file($path);
    }



    public function getPathFromDms($notificationReport, $documentType, $eOfficeDocumentId)
    {
        $base64pdf = $this->getBase64EofficeDocument($notificationReport->id, $documentType, $eOfficeDocumentId);
        $base64decoded = base64_decode($base64pdf);
        $document = time() . '.pdf';
        $file = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        file_put_contents($file, $base64decoded);
        $path = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        return $path;
    }

    /**
     * @param $incident NotificationReport id
     * @param $documentType
     * @param $eOfficeDocumentId
     * @return mixed
     */
    public function getBase64EofficeDocument($incident, $documentType, $eOfficeDocumentId)
    {
        if (!env("TEST_DOCUMENT", 0)) {
            $client = new Client(['defaults' => [
                'verify' => false
            ]]);
            $link = env('EOFFICE_APP_URL');
            $fileNumber = $incident;
            $guzzlerequest = $client->get($link . "/api/claim/file/document?fileNumber={$fileNumber}&documentType={$documentType}&documentId={$eOfficeDocumentId}", [
                'auth' => [
                    env("EOFFICE_AUTH_USER"),
                    env("EOFFICE_AUTH_PASS"),
                ],
                'verify' => false,
            ]);
            $response = $guzzlerequest->getBody()->getContents();
            //logger($response);
            $parsed_json = json_decode($response, true);
            $base64pdf = $parsed_json['document'];
        } else {
            $base64pdf = $this->getSampleBase64Pdf();
        }
        return $base64pdf;
    }

    /**
     * @param NotificationReport $incident
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentBase64Document(NotificationReport $incident, $id)
    {
        $document_model = $incident->documents()->where('document_notification_report.id', $id);
        if ($document_model->count()) {
            $document = $document_model->first();
            $pivot = $document->pivot;
            $documentType = $pivot->document_id;
            $eOfficeDocumentId = $pivot->eoffice_document_id;
            $base64doc = $this->getBase64EofficeDocument($incident->id, $documentType, $eOfficeDocumentId);

            $base64decoded = base64_decode($base64doc);
            $document = $pivot->id . '.pdf';
            $file = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
            file_put_contents($file, $base64decoded);
            if (test_uri()) {
                $url = url("/") . "/ViewerJS/#../storage/eoffice/" . $document;
            } else {
                $url = url("/") ."/public/ViewerJS/#../storage/eoffice/" . $document;
            }
            WfDocumentCheck::query()->updateOrCreate(
                ['document_notification_report_id' => $pivot->id, 'user_id' => access()->id()],
                []
            );
            /*$url = eoffice_path() . "/" . $document;*/
            //$url = eoffice_path() . DIRECTORY_SEPARATOR . $document;
            //check if there is pending workflow ...
/*            $workflows = (new NotificationWorkflowRepository())->query()->select(["id"])->where("notification_report_id", $incident->id)->where("wf_done", 0)->get();
            foreach ($workflows as $workflow) {
                $wftrack = WfTrack::query()->select([
                    "wf_tracks.id",
                    "wf_definitions.need_check"
                ])
                    ->join('wf_definitions', 'wf_tracks.wf_definition_id', 'wf_definitions.id')
                    ->where("wf_tracks.resource_id", $workflow->id)
                    ->orderBy("wf_tracks.id", "desc")
                    ->limit(1)
                    ->first();
                if ($wftrack->need_check) {

                }
            }*/

            return response()->json(['success' => true, "url" => $url, "name" => $pivot->description, "id" => $id]);
        } else {
            return response()->json(['success' => true, "url" => "", "name" => "", "id" => 0]);
        }

    }

    public function currentDocumentFromPath(PortalIncident $incident, $documentId)
    {
        $document = $incident->documents()->where(pg_mac_portal() . ".document_incident.document_id", $documentId)->first() ;
        //if (file_exists(incident_dir() . DIRECTORY_SEPARATOR . $documentId . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : "."))) {
        if ($document) {
            $uploaded = $document->pivot;
            $filename = $documentId . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".");
        } else {
            $uploaded = DB::table(pg_mac_portal() . ".document_incident")->where('id', $documentId)->first();
            $filename = $uploaded->id . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".");
        }
        if (test_uri()) {
            $url = url("/") ."/ViewerJS/#../storage/incident/" . $incident->id . "/" . $filename;
        } else {
            $url = url("/") ."/public/ViewerJS/#../storage/incident/" . $incident->id . "/" . $filename;
        }
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded->description, "id" => $uploaded->id]);
    }

    public function currentOnlineDocumentFromPath(NotificationReport $incident, $documentId)
    {
        $uploaded = DB::table(pg_mac_portal() . ".document_notification_report")->where('id', $documentId)->first();
        $filename = $uploaded->id . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".");
        if (test_uri()) {
            $url = url("/") ."/ViewerJS/#../storage/notification_report/" . $incident->id . "/" . $filename;
        } else {
            $url = url("/") ."/public/ViewerJS/#../storage/notification_report/" . $incident->id . "/" . $filename;
        }
        //logger($url);
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded->description, "id" => $uploaded->id]);
    }

    public function verifyOnlineDocument(PortalIncident $incident, $documentId, $send, $status)
    {
        $return = $this->notification_reports->verifyOnlineDocument($incident, $documentId, $send, $status);
        return response()->json(['success' => true]);
    }

    public function verifyBranchOnlineDocument(NotificationReport $incident, $documentId, $send, $status)
    {
        $return = $this->notification_reports->verifyBranchOnlineDocument($incident, $documentId, $send, $status);
        return response()->json(['success' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function compensationSummaryApi(Request $request)
    {
        $claimCompensationRepo = new ClaimCompensationRepository();
        $input = $request->all();
        $incidentid = $input['incidentid'];
        $api_key = $input['api_key'];
        $compensation_summary = [];
        if (Hash::check(env("PORTAL_API_KEY"), $api_key)) {
            $incident = (new NotificationReportRepository())->find($incidentid);
            if ($incident) {
                if ($incident->isprogressive) {
                    $compensation_summary = $claimCompensationRepo->progressiveCompensationApprove($incident, 0);
                } else {
                    $compensation_summary = $claimCompensationRepo->claimCompensationApprove($incident->id, 0);
                }
            }
        }
        //logger($compensation_summary);
        return response()->json(['success' => true, 'compensation_summary' => $compensation_summary]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function documentUploadedOnline(Request $request)
    {
        $input = $request->all();
        $incidentid = $input['incidentid'];
        $api_key = $input['api_key'];
        $source = $input['source'];
        $doc_list = $input['doclist'];
        if (Hash::check(env("PORTAL_API_KEY"), $api_key)) {
            $docRepo = new DocumentRepository();
            $userRepo = new UserRepository();
            $notificationRepo = new NotificationReportRepository();
            $doclistsarr = $docRepo->query()->select(["name"])->whereIn("id", explode(",", $doc_list))->get()->pluck("name")->all();
            $doclists = implode(",", $doclistsarr);
            $when = Carbon::now()->addMinutes(6); //waiting for file synchronization from online portal to MAC
            switch($source) {
                case 1:
                    $incident = $notificationRepo->find($incidentid);
                    if ($incident->allocated) {
                        $url = route("backend.claim.notification_report.profile", $incident->id);
                        $user = $userRepo->find($incident->allocated);
                        $employee = $incident->employee->name;
                        $user->notify((new DocumentUploadedOnline($url, $doclists, $employee))->delay($when));
                    }
                    break;
                case 2:
                    $incident = (new PortalIncidentRepository())->find($incidentid);
                    $allocated = NULL;
                    if ($incident->allocated) {
                        $allocated = $incident->allocated;
                    } else {
                        if ($incident->notification_report_id) {
                            $related = $notificationRepo->find($incident->notification_report_id);
                            if ($related->allocated) {
                                $allocated = $related->allocated;
                            }
                        }
                    }
                    if ($allocated) {
                        $url = route("backend.compliance.employer.incident_profile", $incident->id);
                        $user = $userRepo->find($allocated);
                        $employee = $incident->employee->name;
                        $user->notify((new DocumentUploadedOnline($url, $doclists, $employee))->delay($when));
                    }
                    break;
            }
        }
        return response()->json(['success' => true]);
    }

    /**
     * @param NotificationReport $incident
     * @param $id
     * @param $sign
     * @return \Illuminate\Http\JsonResponse
     */
    public function otherBase64Document(NotificationReport $incident, $id, $sign)
    {
        $output = [];
        $operator = ">";
        $order = "asc";
        if ($sign < 0) {
            $operator = "<";
            $order = "desc";
        }
        $document = $incident->documents()->where('document_notification_report.id', $operator, $id)->orderBy('document_notification_report.id', $order)->limit(1)->first();
        if ($document) {
            $pivot = $document->pivot;
            $documentType = $pivot->document_id;
            $eOfficeDocumentId = $pivot->eoffice_document_id;
            $base64doc = $this->getBase64EofficeDocument($incident->id, $documentType, $eOfficeDocumentId);
            $output = ['success' => true, "base64" => $base64doc, "id" => $pivot->id];
        } else {
            $output = ['success' => false];
        }

        return response()->json($output);
    }

    public function destroyDocument(NotificationReport $notificationReport)
    {

    }


    public function uploadDocuments(NotificationReport $notificationReport, Request $request)
    {


        $path = $this->base . DIRECTORY_SEPARATOR . $notificationReport->id . DIRECTORY_SEPARATOR . $request->input("document_id");

        /*Unlink before replace it*/
        $folder = $path;
        $files = glob($folder . '/*');
        foreach($files as $file){
            //Make sure that this is a file and not a directory.
            if(is_file($file)){
                //Use the unlink function to delete the file.
                unlink($file);
            }
        }
        /*end removing files*/

        $this->saveNotificationReportDocument($notificationReport, $path);
        return response()->json(['success' => true]);
    }


    /**
     * @param NotificationReport $notificationReport
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Paused on 08-dec-2017
     */
    public function uploadDocuments_old(NotificationReport $notificationReport, Request $request)
    {

        $path = $this->base . DIRECTORY_SEPARATOR . $notificationReport->id . DIRECTORY_SEPARATOR . $request->input("document_id");
        $this->saveNotificationReportDocument($notificationReport, $path);
        return response()->json(['success' => true]);
    }

    /**
     * @param $employee_id
     * @param ChooseIncidentTypeRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function register($employee_id, ChooseIncidentTypeRequest $request)
    {

        $input = $request->all();

        //Removed check for workflow access, now only check the permission.
        //access()->hasWorkflowDefinition(3,1);
        if ($employee_id == 0) {
            //Choose incident for unregistered employee
            $employee = [];
        } else {
            //Choose incident for registered employee
            $employee = $this->employees->findOrThrowException($employee_id);
        }
        $incident_type = $this->incident_types->findOrThrowException($request['incident_type_id']);
        // ---Accident
        if ($request->input('incident_type_id') == 1) {
            return  $this->registerAccident($employee, $incident_type, $input);
        }
        // ---disease
        if ($request->input('incident_type_id') == 2) {
            return   $this->registerDisease($employee, $incident_type, $input);
        }
        // ---death
        if ($request->input('incident_type_id') == 3) {
            return $this->registerDeath($employee, $incident_type, $input);
        }
    }

    /**
     * @param $employee
     * @param $incident_type
     * @param array $input
     * @return mixed
     */
    public function registerAccident($employee, $incident_type, array $input = [])
    {
        //if $employee is empty array, incident is for unregistered employee
        $regions = new RegionRepository();
        $return = view('backend.operation.claim.notification_report.register_accident')
            ->with("employee", $employee)
            ->with("incident_type", $incident_type)
            ->with("regions", $regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
            ->with("accident_types", $this->accident_types->getAll()->pluck('name','id'));
        if (!empty($employee)) {
            $return = $return->with("employers", $this->employers->query()->whereHas('employees' , function($query) use ($employee) {
                $query->where('employee_id', $employee->id );
            })->get()->pluck('name', 'id'));
        } else {
            //Employee not registered...
            if ($input['employer']) {
                $return = $return->with("employer_name", $this->employers->query()->select(['name'])->where('id' , $input['employer'])->first()->name);
            }
            $return = $return->with("employer", $input['employer']);
        }
        return $return;
    }

    /**
     * @param $employee
     * @param $incident_type
     * @param array $input
     * @return mixed
     */
    public function registerDisease($employee, $incident_type, array $input = [])
    {
        //if $employee is empty array, incident is for unregistered employee
        $regions = new RegionRepository();
        $return = view('backend.operation.claim.notification_report.register_disease')
            ->with("employee", $employee)
            ->with("incident_type", $incident_type)
            ->with("regions", $regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'));
        if (!empty($employee)) {
            $return = $return->with("employers", $this->employers->query()->whereHas('employees' , function($query) use($employee){
                $query->where('employee_id',$employee->id );
            })->get()->pluck('name','id'));
        } else {
            //Employee not registered...
            if ($input['employer']) {
                $return = $return->with("employer_name", $this->employers->query()->select(['name'])->where('id' , $input['employer'])->first()->name);
            }
            $return = $return->with("employer", $input['employer']);
        }
        return $return;
    }

    /**
     * @param $employee
     * @param $incident_type
     * @return mixed
     * register death
     */
    public function registerDeath($employee, $incident_type, array $input = [])
    {
        //if $employee is empty array, incident is for unregistered employee
        $regions = new RegionRepository();
        $return = view('backend.operation.claim.notification_report.register_death')
            ->with("employee", $employee)
            ->with("incident_type", $incident_type)
            ->with("regions", $regions->getForSelect())
            ->with("death_causes", $this->death_causes->getAll()->pluck('name','id'))
            ->with("incident_occurrences", $this->code_values->getIncidentOccurrenceForSelect());
        if (!empty($employee)) {
            $return = $return->with("employers", $this->employers->query()->whereHas('employees' , function($query) use($employee) {

                $query->where('employee_id',$employee->id );
            })->get()->pluck('name','id'));
        } else {
            //Employee not registered...
            if ($input['employer']) {
                $return = $return->with("employer_name", $this->employers->query()->select(['name'])->where('id' , $input['employer'])->first()->name);
            }
            $return = $return->with("employer", $input['employer']);
        }
        return $return;
    }

    /**
     * @param $employee_id
     * @param RegisterNotificationRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function storeNew($employee_id, RegisterNotificationRequest $request)
    {
        $notification_report = $this->notification_reports->create($employee_id, $request->all());
        event(new NewReport($notification_report->id));
        return redirect()->route("backend.claim.notification_report.created", $notification_report->id)->withFlashSuccess(trans('alerts.backend.claim.notification_report_created'));
    }

    /**
     * @param NotificationReport $incident
     * @return $this
     */
    public function created(NotificationReport $incident)
    {
        return view("backend.operation.claim.notification_report.progressive.created")
            ->with("notification_report", $incident);
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function modify(NotificationReport $incident)
    {
        //check if initiated by checklist user
        access()->assignedForNotification($incident);

        $this->checkLevel1Rights($incident->id);
        // ---Accident
        if ($incident->incident_type_id == 1) {
            return  $this->modifyAccident($incident);
        }
        // ---disease
        if ($incident->incident_type_id == 2) {
            return   $this->modifyDisease($incident);
        }
        // ---death
        if ($incident->incident_type_id == 3) {
            return $this->modifyDeath($incident);
        }
    }

    /**
     * @param $notification_report
     * @return mixed
     * @throws GeneralException
     */
    public function modifyAccident($notification_report)
    {
        $regions = new RegionRepository();
        $districts = new DistrictRepository();
        $district_id = (!is_null($notification_report->district_id)) ? $notification_report->district_id : NULL;
        $region_id = (!is_null($notification_report->district_id)) ? $notification_report->district->region->id : NULL;
        $date_hired = $this->employees->getDateHired($notification_report->employee_id, $notification_report->employer_id);
        return view('backend.operation.claim.notification_report.edit_accident')
            ->with("notification_report", $notification_report)
            ->with("accident_types", $this->accident_types->getAll()->pluck('name','id'))
            ->with("districts", $districts->getAllByRegionPluck($region_id))
            ->with("regions", $regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
            ->with("district_id", $district_id)
            ->with("region_id", $region_id)
            ->with("incident_exposures", $this->code_values->query()->where('code_id',1)->get()->pluck('name','id'))
            ->with("body_part_injuries", $this->code_values->query()->where('code_id',2)->get()->pluck('name','id'))
            ->with("insurances", $this->insurances->getAll()->pluck('name','id'))
            ->with("member_types", $this->member_types->query()->where('id', 1)->orWhere('id', 2)->orWhere('id', 6)->pluck('name','id'))
            ->with("employers", $this->employers->query()->whereHas('employees' , function($query) use($notification_report) {
                $query->where('employee_id',$notification_report->employee_id );
            })->get()->pluck('name','id'))
            ->with("date_hired", $date_hired);
    }

    /**
     * @param $notification_report
     * @return mixed
     * @throws GeneralException
     */
    public function modifyDisease($notification_report)
    {
        $regions = new RegionRepository();
        $districts = new DistrictRepository();
        $district_id = (!is_null($notification_report->district_id)) ? $notification_report->district_id : NULL;
        $region_id = (!is_null($notification_report->district_id)) ? $notification_report->district->region->id : NULL;
        $date_hired = $this->employees->getDateHired($notification_report->employee_id, $notification_report->employer_id);
        return view('backend.operation.claim.notification_report.edit_disease')
            ->with("notification_report", $notification_report)
            ->with("disease_know_hows", $this->disease_know_hows->getAll()->pluck('name','id'))
            ->with("districts", $districts->getAllByRegionPluck($region_id))
            ->with("regions", $regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
            ->with("district_id", $district_id)
            ->with("region_id", $region_id)
            ->with("incident_exposures",$this->code_values->query()->where('code_id',1)->get()->pluck('name','id'))
            ->with("body_part_injuries", $this->code_values->query()->where('code_id',2)->get()->pluck('name','id'))
            ->with("insurances", $this->insurances->getAll()->pluck('name','id'))
            ->with("member_types", $this->member_types->query()->where('id', 1)->orWhere('id', 2)->orWhere('id', 6)->pluck('name','id'))
            ->with("employers", $this->employers->query()->whereHas('employees' , function($query) use($notification_report){
                $query->where('employee_id',$notification_report->employee_id );
            })->get()->pluck('name','id'))
            ->with("date_hired", $date_hired);
    }

    /**
     * @param $notification_report
     * @return mixed
     * @throws GeneralException
     */
    public function modifyDeath($notification_report)
    {
        $regions = new RegionRepository();
        $districts = new DistrictRepository();
        $district_id = (!is_null($notification_report->district_id)) ? $notification_report->district_id : NULL;
        $region_id = (!is_null($notification_report->district_id)) ? $notification_report->district->region->id : NULL;
        $date_hired = $this->employees->getDateHired($notification_report->employee_id, $notification_report->employer_id);
        return view('backend.operation.claim.notification_report.edit_death')
            ->with("notification_report", $notification_report)
            ->with("death_causes", $this->death_causes->getAll()->pluck('name','id'))
            ->with("districts", $districts->getAllByRegionPluck($region_id))
            ->with("regions", $regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
            ->with("district_id", $district_id)
            ->with("region_id", $region_id)
            //->withDistricts($this->districts->getAll()->pluck('name','id'))
            ->with("incident_exposures", $this->code_values->query()->where('code_id',1)->get()->pluck('name','id'))
            ->with("insurances", $this->insurances->getAll()->pluck('name','id'))
            ->with("member_types", $this->member_types->query()->where('id', 1)->orWhere('id', 2)->orWhere('id', 6)->pluck('name','id'))
            ->with("employers", $this->employers->query()->whereHas('employees' , function($query) use($notification_report){
                $query->where('employee_id',$notification_report->employee_id );
            })->get()->pluck('name','id'))
            ->with("date_hired", $date_hired);
    }

    /**
     * @param $id
     * @param UpdatingNotificationReportRequest $request
     * @return mixed
     */
    public function update($id , UpdatingNotificationReportRequest $request )
    {
        //
        $this->checkLevel1Rights($id);
        $notification_report = $this->notification_reports->update($id, $request->all());
        return redirect('claim/notification_report/profile/' . $notification_report->id . '#general')->withFlashSuccess(trans('alerts.backend.claim.notification_report_updated'));
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     */
    public function modifyProgressive(NotificationReport $incident)
    {
        // access()->assignedForNotification($incident);
        // ---Accident
        if ($incident->incident_type_id == 1) {
            return  $this->modifyProgressiveAccident($incident);
        }
        // ---disease
        if ($incident->incident_type_id == 2) {
            return   $this->modifyProgressiveDisease($incident);
        }
        // ---death
        if ($incident->incident_type_id == 3) {
            return $this->modifyProgressiveDeath($incident);
        }
    }

    /**
     * @param $incident
     * @return mixed
     */
    public function modifyProgressiveAccident($incident)
    {
        //if $employee is empty array, incident is for unregistered employee
        $regions = new RegionRepository();
        $employee = ($incident->employee()->count()) ? $incident->employee : [];
        $accident = $incident->accident;
        $accident_time = Carbon::parse($accident->accident_time);
        $return = view('backend.operation.claim.notification_report.edit_accident_progressive')
            ->with("incident", $incident)
            ->with("accident", $accident)
            ->with("accident_time", $accident_time)
            ->with("employee", $employee)
            ->with("incident_type", $incident->incidentType)
            ->with("regions", $regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
            ->with("accident_types", $this->accident_types->getAll()->pluck('name','id'));
        if (!empty($employee)) {
            $return = $return->with("employers", $this->employers->query()->whereHas('employees' , function($query) use ($employee) {
                $query->where('employee_id', $employee->id );
            })->get()->pluck('name', 'id'));
        } else {
            //Employee not registered...
            if ($incident->has_employer) {
                $return = $return->with("employer_name", $this->employers->query()->select(['name'])->where('id' , $incident->employer_id)->first()->name);
            }
            $return = $return->with("employer", ((is_null($incident->employer_id)) ? 0 : $incident->employer_id));
        }
        return $return;
    }

    /**
     * @param $incident
     * @return mixed
     */
    public function modifyProgressiveDisease($incident)
    {
        //if $employee is empty array, incident is for unregistered employee
        $regions = new RegionRepository();
        $employee = ($incident->employee()->count()) ? $incident->employee : [];
        $disease = $incident->disease;
        $return = view('backend.operation.claim.notification_report.edit_disease_progressive')
            ->with("incident", $incident)
            ->with("disease", $disease)
            ->with("employee", $employee)
            ->with("incident_type", $incident->incidentType)
            ->with("health_provider", $disease->healthProvider()->get()->pluck("fullname_info", "id")->all())
            ->with("medical_practitioner", $disease->medicalPractitioner()->get()->pluck("fullname", "id")->all())
            ->with("regions", $regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'));
        if (!empty($employee)) {
            $return = $return->with("employers", $this->employers->query()->whereHas('employees' , function($query) use($employee){
                $query->where('employee_id',$employee->id );
            })->get()->pluck('name','id'));
        } else {
            //Employee not registered...
            if ($incident->has_employer) {
                $return = $return->with("employer_name", $this->employers->query()->select(['name'])->where('id' , $incident->employer_id)->first()->name);
            }
            $return = $return->with("employer", ((is_null($incident->employer_id)) ? 0 : $incident->employer_id));
        }
        return $return;
    }

    /**
     * @param $incident
     * @return mixed
     */
    public function modifyProgressiveDeath($incident)
    {
        //if $employee is empty array, incident is for unregistered employee
        $regions = new RegionRepository();
        $employee = ($incident->employee()->count()) ? $incident->employee : [];
        $death = $incident->death;
        $return = view('backend.operation.claim.notification_report.edit_death_progressive')
            ->with("incident", $incident)
            ->with("death", $death)
            ->with("employee", $employee)
            ->with("incident_type",  $incident->incidentType)
            ->with("regions", $regions->getForSelect())
            ->with("death_causes", $this->death_causes->getAll()->pluck('name','id'))
            ->with("health_provider", $death->healthProvider()->get()->pluck("fullname_info", "id")->all())
            ->with("medical_practitioner", $death->medicalPractitioner()->get()->pluck("fullname", "id")->all())
            ->with("incident_occurrences", $this->code_values->getIncidentOccurrenceForSelect());
        if (!empty($employee)) {
            $return = $return->with("employers", $this->employers->query()->whereHas('employees' , function($query) use($employee){
                $query->where('employee_id',$employee->id );
            })->get()->pluck('name','id'));
        } else {
            //Employee not registered...
            if ($incident->has_employer) {
                $return = $return->with("employer_name", $this->employers->query()->select(['name'])->where('id' , $incident->employer_id)->first()->name);
            }
            $return = $return->with("employer", ((is_null($incident->employer_id)) ? 0 : $incident->employer_id));
        }
        return $return;
    }

    /**
     * @param NotificationReport $incident
     * @param UpdateProgressiveNotificationReportRequest $request
     * @return mixed
     */
    public function updateProgressive(NotificationReport $incident, UpdateProgressiveNotificationReportRequest $request)
    {
        $input = $request->all();
        $data = $this->notification_reports->updateProgressive($incident, $input);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess(trans('alerts.backend.claim.notification_report_updated'));
    }

    /**
     * @return $this
     */
    public function allocation()
    {
        return view('backend.operation.claim.notification_report.progressive.allocation')
            ->with('users', (new UserRepository())->query()->get()->pluck('name', 'id'))
            ->with("resource_status", $this->notification_reports->getResourceStatus())
            ->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all())
            ->with("stages", $this->code_values->getCodeValuesNotificationStageForSelect());

    }

    /**
     * @return mixed
     */
    public function getAllocation()
    {
        $datatables = $this->notification_reports->getResourceAllocationDatatable();
        return $datatables->make(true);
    }

    public function defaults()
    {
        return view('backend.operation.claim.notification_report.progressive.defaults');
    }

    public function checklistAllocation()
    {
        $incidentUserRepo = new IncidentUserRepository();
        $userRepo = new UserRepository();
        $checklistUsersId = $incidentUserRepo->getAll()->pluck('user_id')->all();
        return view('backend/operation/claim/notification_report/progressive/defaults/checklist_allocation')
            ->with('users', (new UserRepository())->query()->get()->pluck('name', 'id'))
            ->with('checklist_users_values', $checklistUsersId);
    }

    public function checklistRegionAllocationShow()
    {
        $incidentUserRepo = new IncidentUserRepository();
        $iuc_cv_id = (new CodeValueRepository())->IUCNOTFCTN();
        $regionRepo = new RegionRepository();
        $districts = $regionRepo->getWithDistrictForSelect();
        $missingRegions = $incidentUserRepo->getMissingRegions($iuc_cv_id);
        $missingDistricts = $incidentUserRepo->getMissingDistricts($iuc_cv_id);
        $checklist_users = $incidentUserRepo->query()->where('iuc_cv_id', $iuc_cv_id)->with('regions')->get();
        return view('backend/operation/claim/notification_report/progressive/defaults/region_allocation')
            ->with('missing_regions', $missingRegions)
            ->with('missing_districts', $missingDistricts)
            ->with('regions', $regionRepo->query()->get()->pluck('name', 'id')->all())
            ->with('districts', $districts)
            ->with('checklist_users', $checklist_users);
    }

    public function postChecklistAllocation(Request $request, $mode)
    {
        $input = $request->all();
        $return = $this->notification_reports->postChecklistAllocation($input, $mode);
        return response()->json(['success' => true, 'message' => 'Success, All changes have been updated!']);
    }

    public function searchRegisteredNotification()
    {
        return $this->notification_reports->searchRegisteredNotification(request()->input('q'), request()->input('page'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @description Method currently not used.
     * @deprecated
     */
    public function postDefaults(Request $request)
    {
        $input = $request->all();
        $return = $this->notification_reports->postChecklistAllocation($input);
        return response()->json(['success' => true, 'message' => 'Success, All changes have been updated!']);
    }

    /*Post Default by reference*/
    public function postDefaultsByReference($reference, Request $request)
    {
        $input = $request->all();
        $return = $this->notification_reports->postDefaultsByReference($reference, $input);
        return response()->json(['success' => true, 'message' => 'Success, All changes have been updated!']);
    }

    /*Open allocation page for payroll defaults*/
    public function allocationForPayrollDefaults()
    {
        $codeValueRepo = new CodeValueRepository();
        return view('backend/operation/payroll/defaults')
            ->with("users", (new UserRepository())->query()->get()->pluck("name", "id"))
            ->with("payroll_alert_officers", $codeValueRepo->getAllUsersByReference("PAYDALEUS"));
    }


    public function getAllocationEmployerGroup()
    {
        $allocation = $this->notification_reports->getResourceAllocationEmployerGroup();
        return view('backend.operation.claim.notification_report.includes.progressive.allocation_employer_group')->with("allocation", $allocation);
    }

    public function getAllocationStaffGroup()
    {
        $allocation = $this->notification_reports->getResourceAllocationStaffGroup();
        return view("backend.operation.claim.notification_report.includes.progressive.allocation_staff_group")->with("allocation", $allocation);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\WorkflowException
     */
    public function assignAllocation()
    {
        $input = request()->all();
        //logger($input);
        $this->notification_reports->assignAllocation($input);
        return response()->json(['success' => true, 'message' => 'Success, user have been assigned to the selected resource(s)']);
    }

    /**
     * @param NotificationReport $incident
     * @return $this
     */
    public function getStageComment(NotificationReport $incident)
    {
        $stages = $incident->stages();
        return view("backend.operation.claim.notification_report.includes.progressive.stage_comments")
            ->with("stages", $stages);
    }

    /**
     * @param NotificationReport $incident
     * @return $this
     */
    public function attend(NotificationReport $incident)
    {
        return view("backend.operation.claim.notification_report.progressive.attend")
            ->with("incident", $incident);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAttend(NotificationReport $incident)
    {
        $this->notification_reports->updateAttendedStatus($incident);
        //This incident is already attended.
        return redirect()->route("backend.claim.notification_report.profile", $incident->id);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Http\RedirectResponse
     */
    public function attendProfile(NotificationReport $incident)
    {
        if ($incident->attended) {
            //This incident is already attended.
            return redirect()->route("backend.claim.notification_report.profile", $incident->id);
        } else {
            //This incident is not attended.
            return redirect()->route("backend.claim.notification_report.attend", $incident->id);
        }
    }

    /**
     * @param NotificationReport $incident
     * @return $this
     */
    public function updateChecklist(NotificationReport $incident)
    {
        // access()->assignedForNotification($incident);
        $maximum_initial_treatment_date = '';
        $district_id = $incident->district_id;
        $region_id = ($incident->district_id) ? $incident->district->region->id : NULL;
        $health_state_checklists = $this->health_state_checklists->query()->where("isactive", 1)->get();

        /*if ($incident->treatments()->count()) {
            $maximum_initial_treatment_date = $incident->treatments()->max("initial_treatment_date");
        }*/
        $view = view('backend.operation.claim.notification_report.progressive.update_checklist')
            ->with("regions", (new RegionRepository())->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
            ->with("districts", (new DistrictRepository())->getAllByRegionPluck($region_id))
            ->with("health_state_checklists", $health_state_checklists)
            ->with("region_id", $region_id)
            ->with("district_id", $district_id)
            ->with('incident', $incident)
            ->with('maximum_initial_treatment_date', $maximum_initial_treatment_date);

        $view = $this->updateDetailsView($incident, $view);

        switch ($incident->incident_type_id) {
            case 1:
            case 2:
            case 4:
            case 5:
                //Accident
                //Disease
                $job_titles = (new JobTitleRepository())->query()->pluck("name", "id")->all();
                $view->with("job_titles", $job_titles)
                    ->with("insurances", $this->insurances->getAll()->pluck('name','id'))
                    ->with("member_types", $this->member_types->getMedicalAidMemberTypes());
                break;
            case 3:
                //Death
                break;
        }

        return $view;
    }

    public function updateDeathDetails(NotificationReport $incident)
    {
        access()->assignedForNotification($incident);
        $district_id = $incident->district_id;
        $region_id = ($incident->district_id) ? $incident->district->region->id : NULL;
        $death_incident_exposures = $this->code_values->query()->where("code_id", 19)->pluck("name", "id")->all();
        $accident_causes = $this->code_values->query()->where("code_id", 15)->pluck("name", "id")->all();
        $disease_causes = $this->code_values->query()->where("code_id", 16)->pluck("name", "id")->all();
        return view("backend/operation/claim/notification_report/progressive/update_death_details")
                ->with("death_causes", $this->death_causes->getAll()->pluck('name','id'))
                ->with("accident_causes", $accident_causes)
                ->with("disease_causes", $disease_causes)
                ->with("death_incident_exposures", $death_incident_exposures)
                ->with("incident", $incident)
                ->with("districts", (new DistrictRepository())->getAllByRegionPluck($region_id))
                ->with("regions", (new RegionRepository())->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
                ->with("district_id", $district_id)
                ->with("region_id", $region_id);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateDetails(NotificationReport $incident)
    {
        // access()->assignedForNotification($incident);
        $district_id = $incident->district_id;
        $region_id = ($incident->district_id) ? $incident->district->region->id : NULL;
        $view = view("backend.operation.claim.notification_report.progressive.update_details")
                ->with("incident", $incident)
                ->with("districts", (new DistrictRepository())->getAllByRegionPluck($region_id))
                ->with("regions", (new RegionRepository())->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
                ->with("district_id", $district_id)
                ->with("region_id", $region_id);

        $view = $this->updateDetailsView($incident, $view);
        return $view;
    }

    public function updateDetailsView($incident, $view)
    {
        switch ($incident->incident_type_id) {
            case 1:
            case 4:
                //Accident
                $bodyPartyInjuryRepo = new BodyPartInjuryRepository();
                $accident_causes = $this->code_values->query()->where("code_id", 15)->pluck("name", "id")->all();
                //$body_part_injuries = $this->code_values->query()->where("code_id", 2)->pluck("name", "id")->all();
                $body_part_injuries = $bodyPartyInjuryRepo->getForAccident()->pluck("name", "id")->all();
                $accident_incident_exposures = $this->code_values->query()->where("code_id", 17)->pluck("name", "id")->all();
                $body_part_injuries_values = $incident->injuries()->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $lost_body_parties_values = $incident->lostBodyParties()->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $lost_body_parties =  $bodyPartyInjuryRepo->getForInjuryGroupPluck($body_part_injuries_values);

                $view->with("accident_causes", $accident_causes)
                    ->with("body_part_injuries", $body_part_injuries)
                    ->with("accident_incident_exposures", $accident_incident_exposures)
                    ->with("lost_body_parties", $lost_body_parties)
                    ->with("body_part_injuries_values", $body_part_injuries_values)
                    ->with("lost_body_parties_values", $lost_body_parties_values);

                break;
            case 2:
            case 5:
                //Disease
                $bodyPartyInjuryRepo = new BodyPartInjuryRepository();
                $disease_causes = $this->code_values->query()->where("code_id", 16)->pluck("name", "id")->all();
                //$body_part_injuries = $this->code_values->query()->where("code_id", 2)->pluck("name", "id")->all();
                $body_part_injuries = $bodyPartyInjuryRepo->getForDisease()->pluck("name", "id")->all();
                $disease_incident_exposures = $this->code_values->query()->where("code_id", 18)->pluck("name", "id")->all();
                $body_part_injuries_values = $incident->injuries()->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $lost_body_parties_values = $incident->lostBodyParties()->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $lost_body_parties =  $bodyPartyInjuryRepo->getForInjuryGroupPluck($body_part_injuries_values);
                $job_titles = (new JobTitleRepository())->query()->pluck("name", "id")->all();
                $view->with("disease_causes", $disease_causes)
                    ->with("body_part_injuries", $body_part_injuries)
                    ->with("disease_incident_exposures", $disease_incident_exposures)
                    ->with("lost_body_parties", $lost_body_parties)
                    ->with("body_part_injuries_values", $body_part_injuries_values)
                    ->with("lost_body_parties_values", $lost_body_parties_values);
                break;
            case 3:
                //Death
                $death_incident_exposures = $this->code_values->query()->where("code_id", 19)->pluck("name", "id")->all();
                $accident_causes = $this->code_values->query()->where("code_id", 15)->pluck("name", "id")->all();
                $disease_causes = $this->code_values->query()->where("code_id", 16)->pluck("name", "id")->all();
                $view->with("death_causes", $this->death_causes->getAll()->pluck('name','id'))
                    ->with("accident_causes", $accident_causes)
                    ->with("disease_causes", $disease_causes)
                    ->with("death_incident_exposures", $death_incident_exposures);
                break;
        }
        return $view;
    }

    /**
     * @param NotificationReport $incident
     * @param UpdateDetailsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdateDetails(NotificationReport $incident, UpdateDetailsRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->postUpdateDetails($incident, $input);
        return response()->json(['success' => true, 'message' => 'Success, notification details has been updated.', 'url' =>route("backend.claim.notification_report.profile", $incident->id)]);
    }

    public function postUpdateDeathDetails(NotificationReport $incident, UpdateDetailsRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->postUpdateDeathDetails($incident, $input);
        return response()->json(['success' => true, 'message' => 'Success, notification details has been updated.', 'url' =>route("backend.claim.notification_report.profile", $incident->id)]);

    }

    /**
     * @return mixed
     */
    public function treatmentEntries()
    {
        $count = request()->input("count");
        $health_state_checklists = $this->health_state_checklists->query()->where("isactive", 1)->get();
        return view("backend.operation.claim.notification_report.includes.progressive.checklist.treatments.add")
            ->with("count", $count)
            ->with("member_types", $this->member_types->getMedicalAidMemberTypes())
            ->with("health_state_checklists", $health_state_checklists)
            ->with("insurances", $this->insurances->getAll()->pluck('name','id'));

    }

    public function addTdEligibleAssessment(NotificationEligibleBenefit $eligible)
    {

    }

    public function addTdDocumentUsed(NotificationEligibleBenefit $eligible, Request $request)
    {
        $this->validate($request, [
            'document_id' => 'required',
            'document_used' => 'required',
            'health_provider_id' => 'nullable',
            'document_date' => 'nullable|date',
        ]);
        $inputs = $request->all();
        $assessment_document = (new NotificationEligibleBenefitRepository())->addTdDocumentUsed($eligible, $inputs);
        return response()->json(['success' => true]);
    }

    public function deleteTdDocumentUsed(NotificationEligibleBenefit $eligible, AssessmentDocument $assessment_document)
    {
        if ($eligible->usedDocuments()->count() == 1 && $eligible->notificationDisabilityStateAssessments()->count() > 1) {
            return response()->json(['success' => false, 'message' => 'Can not remove all document reference(s) while there is at least one assessment registered.']);
        }
        $assessment_document->delete();
        return response()->json(['success' => true, 'message' => 'Success']);
    }

    public function listTdDocumentUsed(NotificationEligibleBenefit $eligible)
    {
        $incident = $eligible->incident;
        $document_available = $incident->documents()->whereNotIn("document_notification_report.id", function($query) {
                    $query->select(["document_notification_report_id"])->from("assessment_documents");
                })->pluck('documents.name', 'documents.id')->all();
        $append = '<option value=""></option>';
        foreach ($document_available as $key => $value) {
            if (true) {
                $append .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        $assessment_documents = (new NotificationEligibleBenefitRepository())->listTdDocumentUsed($eligible->id);
        $list = view("backend/operation/claim/notification_report/includes/progressive/benefits/td/documents_used")
                            ->with("assessment_documents", $assessment_documents);
        return response()->json(['assessment_documents' => (string) $list, 'document_available' => (string) $append]);
    }

    public function maeSubEntries()
    {
        $input = request()->all();
        $count = $input["count"];
        $assessment = $input["assessment"];
        return view("backend.operation.claim.notification_report.includes.progressive.benefits.mae_refund_member.add_multiple")
            ->with("count", $count)
            ->with("assessment", $assessment);
    }

    public function eligibleComputationSummary(NotificationEligibleBenefit $eligible)
    {
        switch ($eligible->benefit_type_claim_id) {
            case 1:
                //MAE Refund (Employee/Employer)
                $view = "backend/operation/claim/notification_report/includes/progressive/benefits/mae_refund_member/summary_entry";
                break;
            case 3:
                //Temporary Disablement
                $view = "backend/operation/claim/notification_report/includes/progressive/benefits/td/summary_entry";
                break;
            case 4:
                //Temporary Disablement Refund
                $view = "backend/operation/claim/notification_report/includes/progressive/benefits/td/summary_entry";
                break;
            case 5:
                //Permanent Disablement
                $view = "backend/operation/claim/notification_report/includes/progressive/benefits/pd/summary_entry";
                break;
            case 7:
                //Survivors
                $view = "backend/operation/claim/notification_report/includes/progressive/benefits/survivor/summary_entry";
                break;
            default:
                $view = "empty";
                break;
        }
        return view($view)
                ->with('assessment', $eligible)
                ->with('incident', $eligible->incident)
                ->with('hideoption', 1);
    }

    /**
     * @param NotificationReport $incident
     * @param UpdateChecklistRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdateChecklist(NotificationReport $incident, UpdateChecklistRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->postUpdateChecklist($incident, $input);
        return response()->json(['success' => true, 'message' => 'Success, notification checklist has been updated.', 'url' =>route("backend.claim.notification_report.profile", $incident->id)]);
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function completeChecklist(NotificationReport $incident)
    {
        $this->notification_reports->completeChecklist($incident);
        return redirect()->back()->withFlashSuccess("Success, Notification Checklist has been marked complete");
    }

    /**
     * @param NotificationReport $incident
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function updateEmployeeForChecklist(NotificationReport $incident, Request $request)
    {
        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->incorrectEmployeeInfoNotificationType()['group'];
        $type = $moduleRepo->incorrectEmployeeInfoNotificationType()['type'];
        $modules = $moduleRepo->incorrectEmployeeInfoNotificationModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "incorrectEmployeeInfoNotificationRegisterLevel");

        $request->session()->put('return_url', route("backend.claim.notification_report.profile", $incident->id));
        $employee = $incident->employee;
        return redirect()->route("backend.compliance.employee.edit", $employee->id);
    }

    /*    public function updateInvestigation(NotificationReport $incident)
        {
            return view("backend.operation.claim.notification_report.progressive.update_investigation")
                    ->with("incident", $incident);
        }

        public function postInvestigation(NotificationReport $incident)
        {

        }*/

    /**
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function editInvestigationReport(NotificationReport $incident)
    {

        (new InvestigationPlanNotificationsRepository())->checkFileStatusInInvestigationPlan($incident->id);
        if (!$incident->isprogressive) {
            workflow([['wf_module_group_id' => 3, 'resource_id' => $incident->id]])->checkIfCanInitiateAction(1);
            $feedbacks = $incident->investigationFeedbacks;
        } else {
            if (!$this->notification_reports->checkIfInvestigatorAssigned($incident->id)) {
                throw new GeneralException(trans('exceptions.backend.claim.investigation_report_update_by_assigned_investigators'));
            }
            $investigator = $incident->investigationStaffs()->limit(1)->first();
            $feedbacks = $investigator->feedbacks;
        }
        $districts = DB::table('districts')->pluck('name','id');

        $accident_causes = DB::table('codes')
            ->where('id',57)
            ->orWhere('id',15)->pluck('name','id');

        $disease_types = DB::table('codes')
            ->where('codes.id',18)
            ->join('code_values','code_values.code_id','=','codes.id')
            ->pluck('code_values.name','code_values.id');

        $disease_agents = DB::table('code_values')
            ->where('id',311)
            ->orWhere('id',312)
            ->orWhere('id',313)->pluck('name','id');

        $target_organs = DB::table('code_values')
            ->where('id',314)
            ->orWhere('id',315)
            ->orWhere('id',316)
            ->orWhere('id',317)->pluck('name','id');

        $districts = DB::table('districts')->pluck('name','id');


        $incident_date = $this->notification_reports->getIncidentDate($incident);

        $view = view('backend.operation.claim.notification_report.support.investigation_report_update')
            ->with("notification_report", $incident)
            ->with("incident_date", $incident_date)
            ->with("districts", $districts)
            ->with("accident_causes", $accident_causes)
            ->with("disease_types", $disease_types)
            ->with("disease_agents", $disease_agents)
            ->with("target_organs", $target_organs)
            ->with("investigation_feedbacks", $feedbacks);

        if ($incident->isprogressive) {
            $view = $view->with("investigator", $investigator);
        }
        return $view;
    }

    public function getDiseaseByAgent($agent_id){
        return $this->notification_reports->getDeseasesByAgent($agent_id);
    }

    public function getDiseaseByOrgan($organ_id){
        return $this->notification_reports->getDeseasesByOrgan($organ_id);
    }

    public function getAccidentById($accident_id){
        return $this->notification_reports->getAccident($accident_id);
    }

    public function updateInvestigationReport(NotificationReport $incident, UpdateInvestigationReportRequest $request)
    {
     (new InvestigationPlanNotificationsRepository())->checkFileStatusInInvestigationPlan($incident->id);
     if (!$incident->isprogressive) {
        workflow([['wf_module_group_id' => 3, 'resource_id' => $incident->id]])->checkIfCanInitiateAction(1);
    }
    $this->notification_reports->updateInvestigationFeedbacks($incident->id, $request->all(),$incident->employer_id);
    return redirect('claim/notification_report/profile/' . $incident->id . '#investigation')->withFlashSuccess(trans('alerts.backend.claim.investigation_report_feedback_updated'));
}
    // end investigators --------------------------------------

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadInvestigationDocument(NotificationReport $incident)
    {
        (new InvestigationPlanNotificationsRepository())->checkFileStatusInInvestigationPlan($incident->id);
        $error = "";
        if (!$this->notification_reports->checkIfInvestigatorAssigned($incident->id)) {
            $error = "Not allowed, only assigned investigators can upload the documents";
        }
        $id = $this->saveInvestigationDocument($incident);
        $docs = $incident->assetInvestigationDocuments($id);
        $output = [
            'error' => $error,
            'initialPreview' => [
                $docs[0]['source']
            ],
            'initialPreviewConfig' => $docs,
            'append' => true
        ];
        return response()->json($output);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteInvestigationDocument(NotificationReport $incident, $id)
    {
        $docId = 52;
        $docs = $incident->dirInvestigationDocuments($id);
        $document = $incident->investigationDocuments()->wherePivot('id', $id);
        //Delete from Database
        $document->newPivotStatementForId($docId)->where('id', $id)->delete();
        //Delete File from Directory
        $this->deleteDocument($docs[0]['source']);

        return response()->json(['success' => true]);
    }

    public function downloadInvestigationDocument(NotificationReport $incident, $id)
    {
        $docs = $incident->dirInvestigationDocuments($id);
        return response()->download($docs[0]['source'], $docs[0]['name']);
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function completeInvestigationReport(NotificationReport $incident)
    {
        $this->notification_reports->completeInvestigationReport($incident);
        return redirect()->back()->withFlashSuccess("Success, Filling investigation has been marked complete");
    }

    /**
     * @return mixed
     * @throws GeneralException
     */
    public function initiateWorkflow()
    {
        $input = request()->all();
        $return = $this->notification_reports->initiateWorkflow($input);
        return redirect()->back()->withFlashSuccess("Success, Workflow has been initialized.");
    }

    /**
     * @param NotificationReport $incident
     * @param BenefitTypeClaim $benefit
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function initiateBenefitPayment(NotificationReport $incident, BenefitTypeClaim $benefit)
    {
        // access()->assignedForNotification($incident);
        $this->notification_reports->initiateBenefitPayment($incident, $benefit->id);
        switch ($benefit->id) {
            case 1:
                //MAE Refund (Employee/Employer)
                return $this->createMaeRefundMember($incident, $benefit);
                break;
            case 2:
                //MAE Refund (HCP/HSP)
                //return $this->createMaeRefundThirdparty($incident, $benefit);
                break;
            case 3:
                //Temporary Disablement
                return $this->createTd($incident, $benefit);
                break;
            case 4:
                //Temporary Disablement Refund
                return $this->createTdRefund($incident, $benefit);
                break;
            case 5:
                //Permanent Disablement
                return $this->createPd($incident, $benefit);
                break;
            case 6:
                //Funeral Grant
                //return $this->editFuneralGrant($incident);
                break;
            case 7:
                //Survivors
                return $this->createSurvivor($incident, $benefit);
                break;
        }
    }

    /**
     * @param NotificationReport $incident
     * @param BenefitTypeClaim $benefit
     * @param NotificationEligibleBenefit $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function editBenefitPayment(NotificationReport $incident, BenefitTypeClaim $benefit, NotificationEligibleBenefit $eligible)
    {
        switch ($benefit->id) {
            case 1:
                //MAE Refund (Employee/Employer)
                return $this->editMaeRefundMember($incident, $benefit, $eligible);
                break;
            case 2:
                //MAE Refund (HCP/HSP)
                //return $this->editMaeRefundThirdparty($incident, $benefit, $eligible);
                break;
            case 3:
                //Temporary Disablement
                return $this->editTd($incident, $benefit, $eligible);
                break;
            case 4:
                //Temporary Disablement Refund
                //return $this->editTdRefund($incident, $benefit, $eligible);
                return $this->editTd($incident, $benefit, $eligible);
                break;
            case 5:
                //Permanent Disablement
                //return $this->editPd($incident, $benefit, $eligible);
                break;
            case 6:
                //Funeral Grant
                //return $this->editFuneralGrant($incident, $benefit, $eligible);
                break;
            case 7:
                //Survivors
                //return $this->editSurvivor($incident, $benefit, $eligible);
                break;
        }
    }

    public function cancelBenefitPayment(NotificationReport $incident, BenefitTypeClaim $benefit, NotificationEligibleBenefit $eligible)
    {
        switch ($benefit->id) {
            case 1:
                //MAE Refund (Employee/Employer)
                //return $this->editMaeRefundMember($incident, $benefit, $eligible);
                break;
            case 2:
                //MAE Refund (HCP/HSP)
                //return $this->editMaeRefundThirdparty($incident, $benefit, $eligible);
                break;
            case 3:
                //Temporary Disablement
                return $this->cancelTd($incident, $eligible);
                break;
            case 4:
                //Temporary Disablement Refund
                //return $this->editTdRefund($incident, $benefit, $eligible);
                return $this->cancelTd($incident, $eligible);
                break;
            case 5:
                //Permanent Disablement
                //return $this->editPd($incident, $benefit, $eligible);
                break;
            case 6:
                //Funeral Grant
                //return $this->editFuneralGrant($incident, $benefit, $eligible);
                break;
            case 7:
                //Survivors
                //return $this->editSurvivor($incident, $benefit, $eligible);
                break;
        }
    }

    public function deleteBenefitPayment(NotificationReport $incident, NotificationEligibleBenefit $eligible)
    {
        switch ($eligible->benefit_type_claim_id) {
            case 1:
                //MAE Refund (Employee/Employer)
                return $this->deleteMaeRefundMember($incident, $eligible);
                break;
            case 2:
                //MAE Refund (HCP/HSP)
                //return $this->editMaeRefundThirdparty($incident, $eligible);
                break;
            case 3:
                //Temporary Disablement
                //return $this->editTd($incident, $eligible);
                break;
            case 4:
                //Temporary Disablement Refund
                //return $this->editTdRefund($incident, $eligible);
                break;
            case 5:
                //Permanent Disablement
                return $this->deletePd($incident, $eligible);
                break;
            case 6:
                //Funeral Grant
                //return $this->editFuneralGrant($incident, $eligible);
                break;
            case 7:
                //Survivors
                return $this->deleteSurvivor($incident, $eligible);
                break;
        }
    }

    /**
     * @return mixed
     */
    public function initiateBenefitWorkflow()
    {
        $input = request()->all();
        $return = $this->notification_reports->initiateBenefitWorkflow($input);
        return redirect()->back()->withFlashSuccess("Success, Workflow has been initialized.");
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @return mixed
     */
    public function createMaeRefundMember(Model $incident, Model $benefit)
    {
        $health_service_checklists = $this->health_service_checklists->getAll();
        /*return view("backend.operation.claim.notification_report.progressive.benefits.mae_refund_member.create")
            ->with("incident", $incident)
            ->with("benefit", $benefit)
            ->with("member_types", $this->member_types->getWcfMemberTypes())
            ->with("health_service_checklists", $health_service_checklists)
            ->with("incident_date", $incident->incident_date);*/
        return view("backend.operation.claim.notification_report.progressive.benefits.mae_refund_member.create_multiple")
            ->with("incident", $incident)
            ->with("benefit", $benefit)
            ->with("member_types", $this->member_types->getWcfMemberTypes())
            ->with("assessment", 0)
            ->with("incident_date", $incident->incident_date);
    }

    /**
     * @param NotificationReport $incident
     * @param BenefitTypeClaim $benefit
     * @param CreateMaeMemberRequest $request
     * @return mixed
     */
    public function storeMaeRefundMember(NotificationReport $incident, BenefitTypeClaim $benefit, CreateMaeMemberRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->storeMaeRefundMember($incident, $benefit, $input);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, MAE Refund has been created.");
    }

    public function storeMaeRefundMemberMultiple(NotificationReport $incident, BenefitTypeClaim $benefit, CreateMaeMemberMultipleRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->storeMaeRefundMemberMultiple($incident, $benefit, $input);
        //return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, MAE Refund has been created.");
        $url = route("backend.claim.notification_report.profile", $incident->id);
        return response()->json(["success" => true, "url" => $url]);
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @param Model $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function editMaeRefundMember(Model $incident, Model $benefit, Model $eligible)
    {

        if ($eligible->isinitiated) {
            $moduleRepo = new WfModuleRepository();
            $group = $moduleRepo->maeRefundMemberType()['group'];
            $type = $moduleRepo->maeRefundMemberType()['type'];
            $modules = $moduleRepo->maeRefundMemberModule();
            $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "maeRefundMemberAdministrationLevel", $eligible->notification_workflow_id);
        }

        return $this->returnEditMaeRefundMember($incident, $benefit, $eligible, 0);
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @param Model $eligible
     * @param $assessment
     * @return mixed
     */
    public function returnEditMaeRefundMember(Model $incident, Model $benefit, Model $eligible, $assessment)
    {
        $medicalExpense = $eligible->medicalExpense;

        if ($medicalExpense->has_subs) {
            return view('backend.operation.claim.notification_report.progressive.benefits.mae_refund_member.edit_multiple')
                ->with("incident", $incident)
                ->with("eligible", $eligible)
                ->with("benefit", $benefit)
                ->with("assessment", $assessment)
                ->with("member_types", $this->member_types->getWcfMemberTypes())
                ->with("medical_expense", $medicalExpense)
                ->with("incident_date", $incident->incident_date);
        } else {
            $health_service_checklists = $this->health_service_checklists->getAll();
            $notificationHealthProvider = $medicalExpense->notificationHealthProviders()->first();
            $medical_practitioner_id = (($notificationHealthProvider->notificationHealthProviderPractitioner()->count()) ? $notificationHealthProvider->notificationHealthProviderPractitioner->medical_practitioner_id : NULL);
            return view("backend.operation.claim.notification_report.progressive.benefits.mae_refund_member.edit")
                ->with("incident", $incident)
                ->with("benefit", $benefit)
                ->with("health_service_checklists", $health_service_checklists)
                ->with("member_types", $this->member_types->getWcfMemberTypes())
                ->with("incident_date", $incident->incident_date)
                ->with("notification_health_provider", $notificationHealthProvider)
                ->with("health_providers", $this->health_providers->query()->where("id", $notificationHealthProvider->health_provider_id)->get()->pluck('fullname_info', 'id'))
                ->with("medical_practitioners", $this->medical_practitioners->query()->where("id", $medical_practitioner_id)->get()->pluck('fullname_with_external_id', 'id'))
                ->with("medical_practitioner_id", $medical_practitioner_id)
                ->with("eligible", $eligible);
        }
    }

    /**
     * @param NotificationReport $incident
     * @param BenefitTypeClaim $benefit
     * @param NotificationEligibleBenefit $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function assessMaeRefundMember(NotificationReport $incident, BenefitTypeClaim $benefit, NotificationEligibleBenefit $eligible)
    {
        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->maeRefundMemberType()['group'];
        $type = $moduleRepo->maeRefundMemberType()['type'];
        $modules = $moduleRepo->maeRefundMemberModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, 'maeRefundMemberAssessmentLevel', $eligible->notification_workflow_id);

        return $this->returnEditMaeRefundMember($incident, $benefit, $eligible, 1);
    }

    /**
     * @param NotificationReport $incident
     * @param NotificationEligibleBenefit $eligible
     * @param CreateMaeMemberRequest $request
     * @return mixed
     */
    public function updateMaeRefundMember(NotificationReport $incident, NotificationEligibleBenefit $eligible, CreateMaeMemberRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->updateMaeRefundMember($incident, $eligible, $input);
        return redirect()->route('backend.claim.notification_report.profile', $incident->id)->withFlashSuccess('Success, MAE Refund has been updated.');
    }

    public function updateMaeRefundMemberMultiple(NotificationReport $incident, NotificationEligibleBenefit $eligible, UpdateMaeMemberMultipleRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->updateMaeRefundMemberMultiple($incident, $eligible, $input);
        //return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, MAE Refund has been updated.");
        $url = route('backend.claim.notification_report.profile', $incident->id);
        return response()->json(['success' => true, 'url' => $url]);
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @return mixed
     */
    public function deleteMaeRefundMember(Model $incident, Model $eligible)
    {
        $this->notification_reports->deleteMaeRefundMember($incident, $eligible);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, MAE Refund has been deleted.");
    }

    public function createMaeRefundThirdparty(Model $incident, Model $benefit)
    {
        return view("backend.operation.claim.notification_report.progressive.benefits.mae_refund_thirdparty.create")
            ->with("incident", $incident)
            ->with("benefit", $benefit);
    }

    public function storeMaeRefundThirdparty(NotificationReport $incident, BenefitTypeClaim $benefit)
    {

    }

    public function editMaeRefundThirdparty(Model $incident, Model $benefit, $eligible)
    {
        return view("backend.operation.claim.notification_report.progressive.benefits.mae_refund_thirdparty.edit");
    }

    public function updateMaeRefundThirdparty(NotificationReport $incident, BenefitTypeClaim $benefit)
    {

    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @return mixed
     */
    public function createTd(Model $incident, Model $benefit)
    {
        $health_state_checklists = $this->health_state_checklists->query()->where("isactive", 1)->get()->pluck("name", "id")->all();
        $disability_state_checklists = $this->disability_state_checklists->getAll();
        return view("backend.operation.claim.notification_report.progressive.benefits.td.create")
            ->with("incident", $incident)
            ->with("health_state_checklists", $health_state_checklists)
            ->with("disability_state_checklists", $disability_state_checklists)
            ->with("incident_date", $incident->incident_date)
            ->with("benefit", $benefit);
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @return mixed
     */
    public function createTdRefund(Model $incident, Model $benefit)
    {
        return $this->createTd($incident, $benefit);
    }

    /**
     * @param NotificationReport $incident
     * @param BenefitTypeClaim $benefit
     * @param CreateCurrentEmployeeStateRequest $request
     * @return mixed
     */
    public function storeTd(NotificationReport $incident, BenefitTypeClaim $benefit, CreateCurrentEmployeeStateRequest $request)
    {
        $input = $request->all();
        //dd("Hi");
        $this->notification_reports->storeTd($incident, $benefit, $input);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, TD Payment has been created.");
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @param Model $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function editTd(Model $incident, Model $benefit, Model $eligible)
    {
        if ($eligible->isinitiated) {
            $moduleRepo = new WfModuleRepository();
            $group = $moduleRepo->claimBenefitType()['group'];
            $type = $moduleRepo->claimBenefitType()['type'];
            $modules = $moduleRepo->claimBenefitModule();
            $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "claimBenefitAdministrationLevel", $eligible->notification_workflow_id);
        }
        //$health_state_checklists = $this->health_state_checklists->getAll();
        $disability_state_checklists = $this->disability_state_checklists->getAll();
        $health_state_checklists = $this->health_state_checklists->query()->where("isactive", 1)->get()->pluck("name", "id")->all();
        //dd($health_state_checklists);
        $notification_health_state = $eligible->notificationHealthStates()->select(["health_state_checklist_id"])->first();
        //dd($notification_health_state);
        $health_state_checklist_id = $notification_health_state ? $notification_health_state->health_state_checklist_id : NULL;
        //dd($health_state_checklist_id);
        return view("backend.operation.claim.notification_report.progressive.benefits.td.edit")
                        ->with("incident", $incident)
                        ->with("health_state_checklists", $health_state_checklists)
                        ->with("disability_state_checklists", $disability_state_checklists)
                        ->with("health_state_checklist_id", $health_state_checklist_id)
                        ->with("incident_date", $incident->incident_date)
                        ->with("benefit", $benefit)
                        ->with("eligible", $eligible);
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function cancelTd(Model $incident, Model $eligible)
    {
        if ($eligible->isinitiated) {
            $moduleRepo = new WfModuleRepository();
            $group = $moduleRepo->claimBenefitType()['group'];
            $type = $moduleRepo->claimBenefitType()['type'];
            $modules = $moduleRepo->claimBenefitModule();
            $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "claimBenefitAdministrationLevel", $eligible->notification_workflow_id);
        }
        $this->notification_reports->cancelBenefit($incident, $eligible);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, Benefit Payment have been Cancelled.");
    }

    /**
     * @param NotificationReport $incident
     * @param NotificationEligibleBenefit $eligible
     * @param CreateCurrentEmployeeStateRequest $request
     * @return mixed
     */
    public function updateTd(NotificationReport $incident, NotificationEligibleBenefit $eligible, CreateCurrentEmployeeStateRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->updateTd($incident, $eligible, $input);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, TD Payment has been updated.");
    }

    public function deleteTd(Model $incident, Model $eligible)
    {
        //Not available for now
    }

    /**
     * @param NotificationReport $incident
     * @param BenefitTypeClaim $benefit
     * @param NotificationEligibleBenefit $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function tdAssessment(NotificationReport $incident, BenefitTypeClaim $benefit, NotificationEligibleBenefit $eligible)
    {

        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->claimBenefitType()['group'];
        $type = $moduleRepo->claimBenefitType()['type'];
        $modules = $moduleRepo->claimBenefitModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "claimBenefitAssessmentLevel", $eligible->notification_workflow_id);

        //$claimed_expense = (new NotificationEligibleBenefitRepository())->calculateTdTotals($eligible, 1);
        //$dob = $this->employees->getDob($incident->employee_id);
        //$monthly_earning = ['monthly_earning' => $incident->monthly_earning, 'contrib_month' => $incident->contrib_month];
        //$date_of_mmi = $incident->claim->date_of_mmi;
        //$document_used =  (new NotificationWorkflowRepository())->returnDocumentDetails($eligible->notification_workflow_id);
        /*$document_available = $incident->documents()->whereNotIn("document_notification_report.id", function($query) {
            $query->select(["document_notification_report_id"])->from("assessment_documents");
        })->pluck('documents.name', 'documents.id')->all();*/

        //$disability_state_checklists = $this->disability_state_checklists->getAll();
        /*if ($eligible->notificationDisabilityStates()->count() != $this->disability_state_checklists->getActiveCount()) {
            $return = (new NotificationDisabilityStateRepository())->insertMissingProgressive($eligible);
        }*/

        $notification_disability_states = $eligible->notificationDisabilityStates;
        $compensation_summary = (new ClaimCompensationRepository())->progressiveCompensationApprove($incident, 0);
        //dd($compensation_summary);
        return view('backend/operation/claim/notification_report/progressive/benefits/td/assessment')
        ->with("incident", $incident)
        ->with("eligible", $eligible)
        ->with("benefit", $benefit)
        ->with("incident_date", $incident->incident_date)
        ->with("end_month_date", Carbon::now()->endOfMonth()->format("Y-m-d"))
        ->with("notification_disability_states", $notification_disability_states)
        ->with("disability_state_checklists", (new DisabilityStateChecklistRepository())->getAll()->pluck("name", "id")->all())
        ->with("compensation_summary", $compensation_summary)
        //->with("document_available", $document_available)
        //->with("age", Carbon::parse($dob)->diff(Carbon::parse($incident->incident_date))->y)
        //->with("benefit_type_ids", $this->benefit_types->getbenefitIds())
        //->with("monthly_earning", $monthly_earning)
        //->with("monthly_earning_approved", $monthly_earning['monthly_earning'])
        ->with("user_has_access", true)
        //->with("date_of_mmi", $date_of_mmi)
        //->with("document_used", $document_used)
        //->with("claimed_expense", $claimed_expense)
        ;
    }

    /**
     * @param NotificationEligibleBenefit $eligible
     * @return \Illuminate\Http\JsonResponse
     */
    public function listTdAssessment(NotificationEligibleBenefit $eligible)
    {
        $assessments = $eligible->notificationDisabilityStateAssessments()->orderBy("notification_disability_state_assessments.to_date", "asc")->get();
        $compensation = (new ClaimCompensationRepository())->progressiveCompensationApprove($eligible->incident, 0, $eligible->id)['td'];
        if (count($compensation)) {
            $compensation = $compensation[0];
        }
        $list = view("backend/operation/claim/notification_report/includes/progressive/benefits/td/assessments")
                    ->with("assessments", $assessments);
        return response()->json(['assessments' => (string) $list, 'compensation' => $compensation]);
    }


    public function addTdAssessment(NotificationEligibleBenefit $eligible, Request $request)
    {

        $this->validate($request, [
            'disability_state_checklist_id' => 'required',
            'from_date' => 'required|date|after_or_equal:incident_date',
            //'to_date' => 'required|date|before_or_equal:end_month_date',
            'days' => 'required|numeric',
            'remarks' => 'nullable',
        ]);
        $inputs = $request->all();
        if ($inputs['days']) {
            $days = $inputs['days'] - 1; //TODO: MAY NEED CROSSCHECK
            $inputs['to_date'] = Carbon::parse($inputs['from_date'])->addDays($days)->format("Y-m-d");
        } else {
            $days = $inputs['days'];
            $inputs['to_date'] = NULL;
            $inputs['from_date'] = NULL;
        }
        $ndsa = $this->claims->addAssessmentProgressive($eligible,  $inputs);
        return response()->json(['success' => true]);
    }

    public function deleteTdAssessment(NotificationEligibleBenefit $eligible, NotificationDisabilityStateAssessment $assessment)
    {
        $assessment->forceDelete();
        $count = $eligible->notificationDisabilityStateAssessments()->count();
        if  (!$count) {
            $eligible->assessed = 0;
            $eligible->save();
        }
        return response()->json(['success' => true]);
    }

    /**
     * @param NotificationReport $incident
     * @param NotificationEligibleBenefit $eligible
     * @param ClaimAssessmentRequest $request
     * @return mixed
     */
    public function updateTdAssessment(NotificationReport $incident, NotificationEligibleBenefit $eligible, ClaimAssessmentProgressiveRequest $request)
    {
        $input = $request->all();
        (new NotificationWorkflowRepository())->updateDocumentDetails($eligible->notification_workflow_id, $input);
        $this->claims->claimAssessmentProgressive($incident, $eligible,  $input);
        return redirect('claim/notification_report/profile/' . $incident->id )->withFlashSuccess(trans('alerts.backend.claim.claim_assessed'));
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @return mixed
     */
    public function createPd(Model $incident, Model $benefit)
    {
        $this->notification_reports->createPd($incident, $benefit);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, PD Payment has been created.");
    }

    /**
     * @param Model $incident
     * @param Model $eligible
     * @return mixed
     */
    public function deletePd(Model $incident, Model $eligible)
    {
        $this->notification_reports->deletePd($incident, $eligible);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, PD Payment has been deleted.");
    }

    /**
     * @param NotificationReport $incident
     * @param BenefitTypeClaim $benefit
     * @param NotificationEligibleBenefit $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function pdAssessment(NotificationReport $incident, BenefitTypeClaim $benefit, NotificationEligibleBenefit $eligible)
    {

        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->claimBenefitType()['group'];
        $type = $moduleRepo->claimBenefitType()['type'];
        $modules = $moduleRepo->claimBenefitModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "claimBenefitAssessmentLevel", $eligible->notification_workflow_id);

        $bodyPartyInjuryRepo = new BodyPartInjuryRepository();
        switch ($incident->incident_type_id) {
            case 1:
                //Accident
                $body_part_injuries = $bodyPartyInjuryRepo->getForAccident()->pluck("name", "id")->all();
                $body_part_injuries_values = $incident->injuries()->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $lost_body_parties_values = $incident->lostBodyParties()->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $lost_body_parties =  $bodyPartyInjuryRepo->getForInjuryGroupPluck($body_part_injuries_values);
                $lost_body_part = $incident->accident->lost_body_part;
                break;
            case 2:
                //Disease
                $body_part_injuries = $bodyPartyInjuryRepo->getForDisease()->pluck("name", "id")->all();
                $body_part_injuries_values = $incident->injuries()->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $lost_body_parties_values = $incident->lostBodyParties()->select([DB::raw("body_part_injuries.id as id")])->pluck("id")->all();
                $lost_body_parties =  $bodyPartyInjuryRepo->getForInjuryGroupPluck($body_part_injuries_values);
                $lost_body_part = $incident->disease->lost_body_part;
                break;
        }
        $claimed_expense = (new NotificationEligibleBenefitRepository())->calculateExpensesForPd($eligible, 1);
        $pd_body_part_injuries = (new PdInjuryRepository())->getAll()->pluck("name_fec", "id")->all();
        $pd_impairments = (new PdImpairmentRepository())->getAll()->pluck("name", "id")->all();
        $pd_amputations = (new PdAmputationRepository())->getAll()->pluck("name_info", "id")->all();
        $dob = $this->employees->getDob($incident->employee_id);
        $monthly_earning = ['monthly_earning' => $incident->monthly_earning, 'contrib_month' => $incident->contrib_month];
        $date_of_mmi = $incident->claim->date_of_mmi;
        $assessment = $eligible->assessment;
        $claim = $incident->claim;
        $sysdef = sysdefs()->data();
        $minimum_allowed_cca = $sysdef->minimum_allowed_cca;
        $age = Carbon::parse($dob)->diff(Carbon::parse($incident->incident_date))->y;
        return view('backend/operation/claim/notification_report/progressive/benefits/pd/assessment')
            ->with("incident", $incident)
            ->with("benefit", $benefit)
            ->with("eligible", $eligible)
            ->with("incident_date", $incident->incident_date)
            ->with("notification_disability_states", $eligible->notificationDisabilityStates)
            ->with("dob", $dob)
            ->with("age", $age)
            ->with("benefit_type_ids", $this->benefit_types->getbenefitIds())
            ->with("monthly_earning", $monthly_earning)
            ->with("monthly_earning_approved", $monthly_earning['monthly_earning'])
            ->with("user_has_access", true)
            ->with("date_of_mmi", $date_of_mmi)
            ->with("body_part_injuries", $body_part_injuries) //body part injuries
            ->with("lost_body_parties", $lost_body_parties) //body part injuries
            ->with("body_part_injuries_values", $body_part_injuries_values) //body part injuries
            ->with("lost_body_parties_values", $lost_body_parties_values) //body part injuries
            ->with("lost_body_part", $lost_body_part)
            ->with("pd_body_part_injuries", $pd_body_part_injuries)
            ->with("pd_impairments", $pd_impairments)
            ->with("pd_amputations", $pd_amputations)
            ->with("claimed_expense", $claimed_expense)
            ->with("assessment", $assessment)
            ->with("claim", $claim)
            ->with("document_used", null)
            ->with("minimum_allowed_cca", $minimum_allowed_cca)
            ->with("natures_of_incident", $this->code_values->query()->where('code_id', 3)->get()->pluck('name','id'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPdOccupations()
    {
        $input = request()->all();
        return (new OccupationRepository())->getPdOccupations($input['q'], $input['page']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getPdAmputation()
    {
        return Datatables::of((new PdAmputationRepository())->query())
            ->make(true);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function scheduleOneCalculator(Request $request)
    {
        $input = $request->all();
        $pd = (new ClaimRepository())->scheduleOneCalculator($input);
        return response()->json(['success' => true, 'pd' => $pd['value'], 'derived_pd' => $pd['derived']]);
    }

    /**
     * @param NotificationReport $incident
     * @param NotificationEligibleBenefit $eligible
     * @param PdAssessmentProgressiveRequest $request
     * @return mixed
     */
    public function updatePdAssessment(NotificationReport $incident, NotificationEligibleBenefit $eligible, PdAssessmentProgressiveRequest $request)
    {
        $input = $request->all();
        $this->claims->updatePdAssessment($incident, $eligible,  $input);
        return redirect('claim/notification_report/profile/' . $incident->id )->withFlashSuccess(trans('alerts.backend.claim.claim_assessed'));
    }

    /**
     * @param Model $incident
     * @param Model $benefit
     * @return mixed
     */
    public function createSurvivor(Model $incident, Model $benefit)
    {
        $this->notification_reports->createSurvivor($incident, $benefit);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, Survivor Benefit has been created.");
    }

    public function deleteSurvivor(Model $incident, Model $eligible)
    {
        $this->notification_reports->deleteSurvivor($incident, $eligible);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, Survivor Benefit has been deleted.");
    }

    /**
     * @param NotificationReport $incident
     * @param NotificationEligibleBenefit $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function undoApprovedAssessment(NotificationReport $incident, NotificationEligibleBenefit $eligible)
    {
        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->claimBenefitType()['group'];
        $type = $moduleRepo->claimBenefitType()['type'];
        $modules = $moduleRepo->claimBenefitModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "claimBenefitAssessmentLevel", $eligible->notification_workflow_id);
        $this->undoClaimCompensationByEligibleBenefit($eligible->id);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, Undo Approved Assessment Completed");
    }

    /**
     * @param NotificationReport $incident
     * @param NotificationEligibleBenefit $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function processBenefitPayment(NotificationReport $incident, NotificationEligibleBenefit $eligible)
    {
        $moduleRepo = new WfModuleRepository();
        switch ($eligible->benefit_type_claim_id) {
            case 1:
                //MAE Refund (Employee/Employer)
                $group = $moduleRepo->maeRefundMemberType()['group'];
                $type = $moduleRepo->maeRefundMemberType()['type'];
                $modules = $moduleRepo->maeRefundMemberModule();
                $method = "maeRefundMemberPaymentLevel";
                break;
            default:
                //Claim Benefits Workflow
                $group = $moduleRepo->claimBenefitType()['group'];
                $type = $moduleRepo->claimBenefitType()['type'];
                $modules = $moduleRepo->claimBenefitModule();
                $method = "claimBenefitPaymentLevel";
                break;
        }

        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, $method, $eligible->notification_workflow_id);
        $this->notification_reports->processBenefitPayment($incident, $eligible);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, Benefit Payment has been processed!.");
    }

    /**
     * @param NotificationReport $incident
     * @param NotificationEligibleBenefit $eligible
     * @return mixed
     * @throws GeneralException
     */
    public function processMaePayment(NotificationReport $incident, NotificationEligibleBenefit $eligible)
    {
        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->maeRefundMemberType()['group'];
        $type = $moduleRepo->maeRefundMemberType()['type'];
        $modules = $moduleRepo->maeRefundMemberModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "maeRefundMemberPaymentLevel", $eligible->notification_workflow_id);

        $this->notification_reports->processBenefitPayment($incident, $eligible);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, Benefit Payment has been processed!.");
    }

    /*    public function storeTdRefund(NotificationReport $incident, BenefitTypeClaim $benefit)
        {

        }

        public function editTdRefund(Model $incident, Model $benefit, $eligible)
        {

        }

        public function updateTdRefund(NotificationReport $incident, BenefitTypeClaim $benefit)
        {

        }*/

    public function editPd(Model $incident, Model $benefit, $eligible)
    {

    }

    public function updatePd(NotificationReport $incident, BenefitTypeClaim $benefit)
    {

    }

    public function editFuneralGrant(Model $incident, Model $benefit, $eligible)
    {

    }

    public function updateFuneralGrant(NotificationReport $incident, BenefitTypeClaim $benefit)
    {

    }

    /**
     * @param NotificationReport $incident
     */
    public function recheckBenefitStatus(NotificationReport $incident)
    {
        dispatch(new CheckUploadedDocumentsNotification($incident->id, 0));
        return redirect()->back()->withFlashSuccess("Success, Benefit Status has been rechecked.");
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function addContribution(NotificationReport $incident)
    {
        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->missingContributionNotificationType()['group'];
        $type = $moduleRepo->missingContributionNotificationType()['type'];
        $modules = $moduleRepo->missingContributionNotificationModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "missingContributionNotificationRegisterLevel");

        $employer = $incident->employer;
        $contrib_month = Carbon::parse($incident->incident_date)->subMonth();
        $incident_month = Carbon::parse($incident->incident_date);
        $contrib_month_formatted = Carbon::parse($incident->incident_date)->subMonth()->format("F Y");
        $incident_month_formatted = Carbon::parse($incident->incident_date)->format("F Y");
        return view('backend/operation/claim/notification_report/progressive/add_contribution')
            ->with("contrib_month_formatted", $contrib_month_formatted)
            ->with("incident_month_formatted", $incident_month_formatted)
            ->with("contrib_month", $contrib_month)
            ->with("contrib_months", $this->get12ContribMonths(Carbon::parse($incident->incident_date)))
            ->with("incident_month", $incident_month)
            ->with("incident", $incident)
            ->with("employer", $employer);
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function editContribution(NotificationReport $incident)
    {
        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->incorrectContributionNotificationType()['group'];
        $type = $moduleRepo->incorrectContributionNotificationType()['type'];
        $modules = $moduleRepo->incorrectContributionNotificationModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "incorrectContributionNotificationRegisterLevel");

        $employer = $incident->employer;
        $contrib_month = Carbon::parse($incident->incident_date)->subMonth();
        $incident_month = Carbon::parse($incident->incident_date);
        $contrib_month_formatted = Carbon::parse($incident->incident_date)->subMonth()->format("F Y");
        $incident_month_formatted = Carbon::parse($incident->incident_date)->format("F Y");
        return view('backend/operation/claim/notification_report/progressive/edit_contribution')
            ->with("incident_month_formatted", $incident_month_formatted)
            ->with("contrib_month_formatted", $contrib_month_formatted)
            ->with("contrib_month", $contrib_month)
            ->with("contrib_months", $this->get12ContribMonths(Carbon::parse($incident->incident_date)))
            ->with("incident_month", $incident_month)
            ->with("monthly_earning", $incident->monthly_earning)
            ->with("incident", $incident)
            ->with("employer", $employer);
    }

    /**
     * @param $contrib_month
     * @return array
     */
    public function get12ContribMonths($contrib_month)
    {
        $count = 13;
        $months = [];
        while ($count) {
            $month = $contrib_month->format("Y-n-28");
            $months[$month] = $month;
            $contrib_month->subMonth();
            $count--;
        }
        return $months;
    }

    /**
     * @param NotificationReport $incident
     * @param CreateIndividualContributionRequest $request
     * @return mixed
     */
    public function postContribution(NotificationReport $incident, CreateIndividualContributionRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->postContribution($incident, $input);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, Employee Contribution on the month before incident has been updated.");
    }

    /**
     * @param Model $incident
     * @param $group
     * @param $type
     * @param array $modules
     * @param $levelFunc
     * @return bool
     * @throws GeneralException
     */
    public function checkProgressiveAccessLevel(Model $incident, $group, $type, array $modules, $levelFunc, $workflow = 0)
    {
        $moduleRepo = new WfModuleRepository();
        if ($workflow) {
            //Works for benefit workflows
            $notification_workflow = $incident->processes()->where("notification_workflows.id", $workflow)->select(["id", "wf_module_id"])->limit(1)->first();
        } else {
            //Works for enforceable & On-demand workflows
            $notification_workflow = $incident->processes()->whereIn("notification_workflows.wf_module_id", $modules)->select(["id", "wf_module_id"])->limit(1)->first();
        }
        //dd($notification_workflow);
        $workflowClass = new Workflow(['wf_module_id' => $notification_workflow->wf_module_id, "resource_id" => $notification_workflow->id]);
        if ($workflow) {
            $module = $notification_workflow->wf_module_id;
        } else {
            $module = $workflowClass->getModule(true);
        }
        $level = $moduleRepo->$levelFunc($module);
        $modulei = $moduleRepo->find($module);
        //dd($module);
        $workflowClass->checkLevel($level);
        access()->hasWorkflowModuleDefinition($group, $modulei->type, $level);
        return true;
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function registerEmployee(NotificationReport $incident)
    {
        $moduleRepo = new WfModuleRepository();
        $group = $moduleRepo->unregisteredMemberNotificationType()['group'];
        $type = $moduleRepo->unregisteredMemberNotificationType()['type'];
        $modules = $moduleRepo->unregisteredMemberNotificationModule();
        $this->checkProgressiveAccessLevel($incident, $group, $type, $modules, "unregisteredMemberNotificationRegisterLevel");
        return view('backend/operation/claim/notification_report/progressive/register_employee')
            ->with("incident", $incident)
            ->with("employee_id", $incident->employee_id)
            ->with("employer_id", $incident->employer_id);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCloseIncident(NotificationReport $incident)
    {
        return view("backend.operation.claim.notification_report.close")
            ->with("incident", $incident);
    }

    /**
     * @param NotificationReport $incident
     * @param IncidentClosureRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCloseIncident(NotificationReport $incident, IncidentClosureRequest $request)
    {
        $input = $request->all();
        $repo = new IncidentClosureRepository();
        //Log::error($input);
        $repo->postCloseIncident($incident, $input);
        return response()->json(['success' => true, 'id' => $incident->id]);
    }

    /**
     * @param NotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function doManualPayment(NotificationReport $incident)
    {
        return view("backend.operation.claim.notification_report.do_manual_payment")
            ->with("incident", $incident);
    }

    /**
     * @param NotificationReport $incident
     * @param UpdateManualPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDoManualPayment(NotificationReport $incident, UpdateManualPaymentRequest $request)
    {
        $input = $request->all();
        $this->notification_reports->updateManualPayment($incident, $input);
        return response()->json(['success' => true, 'id' => $incident->id]);
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     */
    public function postUndoManualPayment(NotificationReport $incident)
    {
        //TODO: restrict undo if reassessment has already been started...
        $return = $this->notification_reports->undoManualPayment($incident);
        return redirect()->back()->withFlashSuccess("Success, Manual Payment Update has been undone");
    }

    /**
     * @param NotificationReport $incident
     * @return mixed
     */
    public function undoApproval(NotificationReport $incident)
    {
        // access()->assignedForNotification($incident);
        $return = $this->notification_reports->undoApproval($incident);
        return redirect()->back()->withFlashSuccess('Success, Approval Process has been Undone');
    }

    public function undoBenefit(NotificationReport $incident)
    {
        // access()->assignedForNotification($incident);
        $return = $this->notification_reports->undoBenefit($incident);
        return redirect()->back()->withFlashSuccess('Success, Benefit Process has been Undone');
    }

/*    public function onlineRequests(EmployerIncidentDataTable $dataTable)
    {
        $count = (new PortalIncidentRepository())->query()->count();
        return $dataTable->with('count', $count)->render('backend/operation/claim/notification_report/online_requests', ['count' => $count]);
    }*/

    public function onlineRequests()
    {
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->find(59);
        if  (!$cr) {
            throw new ModelNotFoundException();
        }
        return view('backend/operation/claim/notification_report/online_requests')
            ->with('cr', $cr);

    }

    public function onlineAccountValidation(OnlineAccountDataTable $dataTable)
    {
        $count = (new \App\Repositories\Backend\Operation\Claim\Portal\ClaimRepository())->getPendingValidationForDatatable()->count();
        return $dataTable->with('validated', 0)->render('backend/operation/claim/notification_report/portal/online_account_validation', ["count" => $count]);
    }

    public function onlineAccountValidated()
    {
        //OnlineAccountDataTable $dataTable
        /*$count = (new \App\Repositories\Backend\Operation\Claim\Portal\ClaimRepository())->getValidatedForDatatable()->count();
        return $dataTable->with('validated', 1)->render('backend/operation/claim/notification_report/portal/online_account_validated', ["count" => $count]);*/
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->find(95);
        if  (!$cr) {
            throw new ModelNotFoundException();
        }
        return view('backend/operation/claim/notification_report/portal/online_account_validated')
            ->with('cr', $cr);
    }

    public function onlineAccountValidationProfile(Claim $claim)
    {
        return view("backend/operation/claim/notification_report/portal/online_account_validation_profile")
            ->with("claim", $claim)
            ->with("deactivate_reasons", (new CodeValuePortalRepository())->getAccountDeactivateReasonsForSelect());
    }

    /**
     * @param Claim $claim
     * @return \Illuminate\Http\JsonResponse
     * @throws GeneralException
     */
    public function showClaimAccountDocumentLibrary(Claim $claim)
    {
        $this->base = $this->real(claim_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }
        $this->base = $this->base . DIRECTORY_SEPARATOR . $claim->id;
        $this->makeDirectory($this->base);
        $this->onlineFolder = $claim->pluck('id', 'id')->all();
        $this->documents = $claim->documents->pluck('name', 'id')->all();
        $uploadedDocuments = [];
        foreach ($claim->documents as $document) {
            $uploadedDocuments += [$document->id => $document->name, "isverified_" . $document->id => $document->pivot->isverified];
        }
        $this->uploadedDocuments = $uploadedDocuments;
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_uploaded_online($node, (isset(request()->id) && request()->id === '#'), false);
        return response()->json($rslt);
    }

    public function currentOnlineAccountDocumentFromPath(Claim $claim, $documentId)
    {
        $document = $claim->documents()->where(pg_mac_portal() . ".claim_document.document_id", $documentId)->first() ;
        $uploaded = $document->pivot;
        if (test_uri()) {
            $url = url("/") ."/ViewerJS/#../storage/claim/" . $claim->id . "/" . $documentId . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".");
        } else {
            $url = url("/") ."/public/ViewerJS/#../storage/claim/" . $claim->id . "/" . $documentId . ((!empty($uploaded->ext)) ? "." . $uploaded->ext : ".");
        }
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded->description, "id" => $uploaded->id]);
    }

    public function onlineAccountValidateAction(Claim $claim, $status, Request $request)
    {
        $input = $request->all();
        $return = $this->notification_reports->onlineAccountValidateAction($claim, $status, $input);
        return redirect()->back()->withFlashSuccess("Success, your action has been successfully applied ...");
    }

    /**
     * @param NotificationReport $incident
     * @param Request $request
     * @return mixed
     */
    public function postRegisterEmployee(NotificationReport $incident, RegisterEmployee $request)
    {
        $input = $request->all();
        $this->notification_reports->postRegisterEmployee($incident, $input);
        return redirect()->route("backend.claim.notification_report.profile", $incident->id)->withFlashSuccess("Success, Employee Information has been registered.");
    }

    public function informationDashboard()
    {
        $dashboard = new DashboardRepository();
        $claim = $dashboard->getClaimSummary(1);
        $selector = 14;
        // dd($claim);
        return view("backend.home")
            ->with("selector", $selector)
            ->with("claim_summary", $claim);
    }

    public function performanceReport()
    {
        //$reports = (new CodeValueRepository())->query()->where("code_id", 62)->orderBy('sort', 'asc')->get();
        $reports = (new ConfigurableReportRepository())->query()->where("configurable_report_type_id", 4)->orderBy('sort', 'asc')->get();
        return view("backend/report/performance/claim/index")
                    ->with('reports', $reports);
    }

    public function viewPerformanceReport(ConfigurableReport $configurable)
    {
        return view("backend/report/performance/claim/show")
                ->with('configurable', $configurable);
    }

    public function staffPerformanceReport()
    {
        return view("backend/report/performance/claim/staff/index");
    }

    public function downloadConfigurableReport($report_id, $fin_year_id)
    {
        (new ConfigurableReportRepository())->download($report_id, $fin_year_id);
    }

    /**
     * @param MedicalExpense $medical_expense
     * @return mixed
     * @throws GeneralException
     */
    public function claimAssessmentChecklist(MedicalExpense $medical_expense) {
        //$medical_expense = $this->medical_expenses->findOrThrowException($medical_expense_id);
        $incident = $medical_expense->notificationReport;
        $incident_date = $this->notification_reports->getIncidentDate($incident);
        $monthly_earning_summary = $this->employees->getMonthlyEarningBeforeIncident($medical_expense->notificationReport->employee_id, $incident->id);
        $monthly_earning_from_contrib = $monthly_earning_summary['monthly_earning'];

        $monthly_earning_approved = ($incident->claim) ? $incident->claim->monthly_earning : $monthly_earning_from_contrib;
        $dob = $this->employees->getDob($medical_expense->notificationReport->employee_id);
        $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $medical_expense->notification_report_id]);
        $assessment_level = $workflow->claimAssessmentLevel();

        $date_of_mmi = ($incident->claim) ? $incident->claim->date_of_mmi : null;

        return view('backend.operation.claim.notification_report.support.claim_assessment_checklist')
            ->with("medical_expense", $medical_expense)
            ->with("claimed_expense", $this->medical_expenses->calculateExpenses($medical_expense->id, 1))
            ->with("notification_disability_states", $medical_expense->notificationDisabilityStates)
            ->with("age", Carbon::parse($dob)->diff(Carbon::parse($incident_date))->y)
            ->with("benefit_type_ids", $this->benefit_types->getbenefitIds())
            ->with("monthly_earning", $monthly_earning_summary)
            ->with("monthly_earning_approved", $monthly_earning_approved)
            ->with("user_has_access", $workflow->userHasAccess(access()->id(), $assessment_level ))
            ->with("date_of_mmi", $date_of_mmi);

    }

    /**
     * @param $medical_expense_id
     * @param ClaimAssessmentRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function claimAssessment($medical_expense_id, ClaimAssessmentRequest $request) {
        $medical_expense = $this->medical_expenses->findOrThrowException($medical_expense_id);
        $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $medical_expense->notification_report_id]);
        $assessment_level = $workflow->claimAssessmentLevel();
        $workflow->checkLevel($assessment_level);

        $notification_report = $this->claims->claimAssessment($medical_expense, $request->all());
        return redirect('claim/notification_report/profile/' . $notification_report->id . '#general')->withFlashSuccess(trans('alerts.backend.claim.claim_assessed'));
    }


    /**
     * -------CURRENT STATE OF EMPLOYEES NOTIFICATION REPORT ======================
     */

    /**
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function createCurrentEmployeeState(NotificationReport $incident)
    {
        $assessment_level = $this->getAssessmentLevel($incident->id);
        $this->checkLevelRights($incident->id, $assessment_level);
        $health_state_checklists = $this->health_state_checklists->query()->where("isactive", 1)->get()->pluck("name", "id")->all();
//        $health_state_checklists = $this->health_state_checklists->getAll();
        $disability_state_checklists = $this->disability_state_checklists->getAll();
        $incident_date = $this->notification_reports->getIncidentDate($incident);
        return view('backend.operation.claim.notification_report.support.current_employee_state_create')
            ->with("notification_report", $incident)
            ->with("health_state_checklists", $health_state_checklists)
            ->with("disability_state_checklists", $disability_state_checklists)
            ->with("incident_date", $incident_date)
            ->with("medical_expenses", $this->medical_expenses->query()->where('notification_report_id', $incident->id)->get()->pluck('compensated_entity_name_with_amount',
                'id'));
    }

    /**
     * @param $id
     * @param CreateCurrentEmployeeStateRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function storeCurrentEmployeeState($id, CreateCurrentEmployeeStateRequest $request)
    {
        $assessment_level = $this->getAssessmentLevel($id);
        $this->checkLevelRights($id, $assessment_level);
        $current_employee_state = $this->notification_reports->storeCurrentEmployeeState($id,$request->all());
        return redirect('claim/notification_report/profile/' . $id . '#current_employee_state')->withFlashSuccess(trans('alerts.backend.claim.current_employee_state_created'));
    }


    /**
     * @param MedicalExpense $medical_expense
     * @return mixed
     * @throws GeneralException
     */
    public function editCurrentEmployeeState(MedicalExpense $medical_expense)
    {
        /*Check if has right*/
        $notification_report_id = $medical_expense->notification_report_id;
        $assessment_level = $this->getAssessmentLevel($notification_report_id);
        $this->checkLevelRights($notification_report_id, $assessment_level);
        /*end check*/
        $incident = $medical_expense->notificationReport;
        $health_state_checklists = $this->health_state_checklists->query()->where("isactive", 1)->get()->pluck("name", "id")->all();
//        $health_state_checklists = $this->health_state_checklists->getAll();
        $disability_state_checklists = $this->disability_state_checklists->getAll();
        $incident_date = $this->notification_reports->getIncidentDate($incident);
        $notification_health_state = $incident->notificationHealthStates()->whereNull('notification_health_states.notification_eligible_benefit_id')->select(["health_state_checklist_id"])->first();
        //dd($notification_health_state);
        $health_state_checklist_id = $notification_health_state ? $notification_health_state->health_state_checklist_id : NULL;
        return view('backend.operation.claim.notification_report.support.current_employee_state_edit')
            ->with("notification_report", $incident)
            ->with("medical_expense", $medical_expense)
            ->with("health_state_checklists", $health_state_checklists)
            ->with("health_state_checklist_id", $health_state_checklist_id)
            ->with("disability_state_checklists", $disability_state_checklists)
            ->with("incident_date", $incident_date)
            ->with("medical_expenses", $this->medical_expenses->query()->where('notification_report_id', $incident->id)->get()->pluck('compensated_entity_name_with_amount',
                'id')) ;
    }

    /**
     * @param $id
     * @param CreateCurrentEmployeeStateRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function updateCurrentEmployeeState($id, CreateCurrentEmployeeStateRequest $request)
    {
        $assessment_level = $this->getAssessmentLevel($id);
        $this->checkLevelRights($id, $assessment_level);
        $current_employee_state = $this->notification_reports->updateCurrentEmployeeState($id,$request->all());
        return redirect('claim/notification_report/profile/' . $id . '#current_employee_state')->withFlashSuccess(trans('alerts.backend.claim.current_employee_state_updated'));
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getDisabilityStateForDatatable($id) {
        return Datatables::of($this->notification_reports->getDisabilityState($id))
            ->addColumn('medical_expense', function($notification_disability_state) {
                return $notification_disability_state->medicalExpense->compensated_entity_name;
            })
            ->editColumn('from_date', function($notification_disability_state) {
                return $notification_disability_state->from_date_formatted;
            })
            ->editColumn('to_date', function($notification_disability_state) {
                return $notification_disability_state->to_date_formatted;
            })
            ->editColumn('disability_state_checklist_id', function($notification_disability_state) {
                return $notification_disability_state->disabilityStateChecklist->name;
            })


            ->make(true);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getHealthStateForDatatable($id) {

        return Datatables::of($this->notification_reports->getHealthState($id))
            ->editColumn('health_state_checklist_id', function($notification_health_state) {
                return $notification_health_state->healthStateChecklist->name;
            })
            ->make(true);
    }


    //----End Current employeee State notification report --------------------

//END HEALTH PROVIDER SERVICES----------------------------------

    /**
     * @param NotificationReport $incident
     * @return mixed
     */
    public function createMedicalExpense(NotificationReport $incident)
    {
        $this->checkLevel1Rights($incident->id);
        return view('backend.operation.claim.notification_report.medical_expense_create')
            ->with("notification_report", $incident)
            ->with("insurances", $this->insurances->getAll()->pluck('name','id'))
            ->with("member_types", $this->member_types->getMedicalAidMemberTypes());
    }

    /**
     * @param $id
     * @param CreateMedicalExpenseRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function storeMedicalExpense($id, CreateMedicalExpenseRequest $request)
    {
        $medical_expense = $this->medical_expenses->create($id,$request);
        return redirect('claim/notification_report/profile/' . $medical_expense->notification_report_id . '#medical_expense')->withFlashSuccess(trans('alerts.backend.claim.medical_expense_created'));

    }

    /**
     * @param MedicalExpense $medical_expense
     * @return mixed
     * @throws GeneralException
     */
    public function editMedicalExpense(MedicalExpense $medical_expense)
    {
        $this->checkLevel1Rights($medical_expense->notification_report_id);

        $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $medical_expense->notification_report_id]);
        $assessment_level = $workflow->claimAssessmentLevel();
        $workflow->checkIfCanInitiateActionMultiLevel(1, $assessment_level);

        return view('backend.operation.claim.notification_report.medical_expense_edit')
            ->with("medical_expense", $medical_expense)
            ->with("insurances", $this->insurances->getAll()->pluck('name','id'))
            ->with("member_types", $this->member_types->getMedicalAidMemberTypes())
            ->with("user_has_access1", $workflow->userHasAccess(access()->id(), 1 ))
            ->with("user_has_access2", $workflow->userHasAccess(access()->id(), $assessment_level ));
    }

    /**
     * @param $medical_expense_id
     * @param CreateMedicalExpenseRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function updateMedicalExpense($medical_expense_id, CreateMedicalExpenseRequest $request)
    {
        $medical_expense = $this->medical_expenses->update($medical_expense_id, $request);
        return redirect('claim/notification_report/profile/' . $medical_expense->notification_report_id . '#medical_expense')->withFlashSuccess(trans('alerts.backend.claim.medical_expense_updated'));
    }


    /**
     * @param NotificationReport $incident
     * @return mixed
     */
    public function createHealthProviderService(NotificationReport $incident)
    {
        $this->checkLevel1Rights($incident->id);
        $health_service_checklists = $this->health_service_checklists->getAll();
        $incident_date = $this->notification_reports->getIncidentDate($incident);
        $medical_expenses = $this->medical_expenses->query()->where('notification_report_id', $incident->id)->doesntHave('notificationHealthProviders')->get()->pluck('compensated_entity_name_with_amount','id');
        return view('backend.operation.claim.notification_report.support.health_provider_service_create')
            ->with("notification_report", $incident)
            ->with("health_providers", [])
            ->with("health_service_checklists", $health_service_checklists)
            ->with("incident_date", $incident_date)
            ->with("medical_expenses", $medical_expenses);

    }

    public function storeHealthProviderService($id, CreateHealthProviderServiceRequest $request)
    {
        $this->checkLevel1Rights($id);
        $health_provider_service = $this->notification_reports->storeHealthProviderService($id,$request->all());
        return redirect('claim/notification_report/profile/' . $id . '#medical')->withFlashSuccess(trans('alerts.backend.claim.health_provider_service_created'));
    }

    /**
     * @param NotificationHealthProvider $notification_health_provider
     * @return mixed
     * @throws GeneralException
     */
    public function editHealthProviderService(NotificationHealthProvider $notification_health_provider)
    {
        $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $notification_health_provider->notification_report_id]);
        $assessment_level = $workflow->claimAssessmentLevel();
        $workflow->checkIfCanInitiateActionMultiLevel(1, $assessment_level);

        $health_service_checklists = $this->health_service_checklists->getAll();
        $incident_date = $this->notification_reports->getIncidentDate($notification_health_provider->notificationReport);
        return view('backend.operation.claim.notification_report.support.health_provider_service_edit')
            ->with("notification_health_provider", $notification_health_provider)
            ->with("health_providers", $this->health_providers->query()->where("id", $notification_health_provider->health_provider_id)->get()->pluck('fullname_info', 'id'))
            ->with("medical_practitioners", $this->medical_practitioners->query()->where("id", $notification_health_provider->notificationHealthProviderPractitioner->medical_practitioner_id)->get()->pluck('fullname_with_external_id', 'id'))
            ->with("health_service_checklists", $health_service_checklists)
            ->with("incident_date",$incident_date)
            ->with("medical_expenses", $this->medical_expenses->query()->where('notification_report_id', $notification_health_provider->notification_report_id)->get()->pluck('compensated_entity_name_with_amount', 'id'))
            ->with("user_has_access1", $workflow->userHasAccess(access()->id(), 1 ))
            ->with("user_has_access2", $workflow->userHasAccess(access()->id(), $assessment_level ));
    }

    /**
     * @param $notification_health_provider_id
     * @param CreateHealthProviderServiceRequest $request
     * @return mixed
     * @throws GeneralException
     */
    public function updateHealthProviderService($notification_health_provider_id, CreateHealthProviderServiceRequest $request)
    {
        $notification_health_provider = $this->notification_health_providers->findOrThrowException($notification_health_provider_id);

        $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $notification_health_provider->notification_report_id]);
        $assessment_level = $workflow->claimAssessmentLevel();
        $workflow->checkIfCanInitiateActionMultiLevel(1,$assessment_level);

        $notification_health_provider = $this->notification_reports->updateHealthProviderService($notification_health_provider_id,$request->all());
        return redirect('claim/notification_report/profile/' . $notification_health_provider->notification_report_id . '#medical')->withFlashSuccess(trans('alerts.backend.claim.health_provider_service_updated'));
    }

    public function sendToDms(NotificationReport $notificationReport)
    {
        dispatch(new PostNotificationDms($notificationReport));
        return redirect()->back()->withFlashSuccess("Request to create file to eOffice has been made, if the file has not been created, try again later");
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getMedicalExpensesForDatatable($id) {
        $first_medical_expense = $this->medical_expenses->getFirstMedicalExpenseForNotification($id);
        return Datatables::of($this->notification_reports->getMedicalExpenses($id))
            ->editColumn('amount', function($medical_expense) {
                return $medical_expense->amount_formatted;
            })
            ->editColumn('member_type_id', function($medical_expense) {
                return $medical_expense->memberType->name;
            })
            ->addColumn('name', function($medical_expense) {
                return $medical_expense->compensated_entity_name;
            })
            ->addColumn('cost_incurred', function($medical_expense) use($id) {
                return number_format( $medical_expense->notificationHealthProviderServices->sum('amount') , 2 , '.' , ',' );
            })
            ->addColumn('action_buttons', function($medical_expense) use ($first_medical_expense)  {
                return ($medical_expense->notificationReport->notificationDisabilityStates()->count()) ?  (($medical_expense->notificationDisabilityStates()->count()) ? $medical_expense->action_buttons : ' ') : (($medical_expense->id == $first_medical_expense->id) ? $medical_expense->action_buttons : ' ');
//            ->addColumn('action_buttons', function($medical_expense)  {
//                return count($medical_expense->notificationReport->notificationDisabilityStates) ? count($medical_expense->notificationDisabilityStates) ? $medical_expense->action_buttons : ' ' : $medical_expense->action_buttons;
            })
            ->rawColumns(['action_buttons'])
            ->make(true);
    }

    /*Get Medical Expense Entities Summary /  Assessment Checklist*/
    public function getAssessmentOpener($id) {
        $first_medical_expense = $this->medical_expenses->getFirstMedicalExpenseForNotification($id);
        $medical_expense_with_disability_state = $this->medical_expenses->getMedicalExpenseWithDisabilityState($id);
        $medical_expense_for_assessment = ($medical_expense_with_disability_state) ? $medical_expense_with_disability_state : $first_medical_expense;
        return Datatables::of($this->notification_reports->getMedicalExpenseEntities($id))
            ->addColumn('member_type_id', function($medical_expense_entity) use ($id, $medical_expense_for_assessment) {
//                $medical_expense = $this->medical_expenses->query()->where('notification_report_id', $id)->where('member_type_id', $medical_expense_entity->member_type_id)->first();
                return $medical_expense_for_assessment->memberType->name;
            })
            ->addColumn('name', function($medical_expense_entity) use ($id, $medical_expense_for_assessment) {
//                $medical_expense = $this->medical_expenses->query()->where('notification_report_id', $id)->where('member_type_id', $medical_expense_entity->member_type_id)->first();
                return $medical_expense_for_assessment->compensated_entity_name;
            })
            ->addColumn('action_buttons', function($medical_expense_entity) use ($medical_expense_for_assessment)  {

                return $medical_expense_for_assessment->action_buttons;

            })
            ->rawColumns(['action_buttons'])
            ->make(true);
    }


    /**
     * Check if can Initiate Action (Level 1 / No Workflow)
     */
    public function checkLevel1Rights($resource_id)
    {
        access()->hasWorkflowDefinition(3, 1);
        workflow([['wf_module_group_id' => 3, 'resource_id' => $resource_id]])->checkIfCanInitiateAction(1);
    }
    /*Check if can initiate action on a given workflow level*/
    public function checkLevelRights($resource_id, $level_id, $wf_module_group_id = 3){
        access()->hasWorkflowDefinition($wf_module_group_id,$level_id);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction($level_id);
    }

    /*Check if user designation is claim admin manager*/
    /**
     * @throws GeneralException
     */
    public function checkIfClaimAdminMgr()
    {
        /*Check designation claim admin manager*/
        if(checkUserDesignation(14,5)) {
        }else{
            throw new GeneralException('This action can be initiated by Claim Administration Manager');
        }
    }

    /**
     * @param $id
     * @return int|null
     * @throws GeneralException
     */
    public function getAssessmentLevel($id)
    {
        $wf_module_group_id =3;
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id]);
        $assessment_level = $workflow->claimAssessmentLevel();
        return $assessment_level;
    }

    //----End modifying ----------------

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function editMonthlyEarning($id)
    {
        $notification_report = $this->notification_reports->findOrThrowException($id);
        return view('backend.operation.claim.notification_report.support.edit_monthly_earning')
            ->withNotificationReport($notification_report);
    }


    public function updateMonthlyEarning($id, UpdateMonthlyEarningRequest $request)
    {
        $this->notification_reports->updateMonthlyEarning($id, $request->all());
        return redirect()->route('backend.claim.notification_contribution_track.profile', $id)->withFlashSuccess(trans('alerts.backend.claim.monthly_earning_updated'));
    }

    //--------end Medical expense-------------------

    /**
     * @param NotificationReport $incident
     * @return $this
     */
    public function assignInvestigators(NotificationReport $incident)
    {
        workflow([['wf_module_group_id' => 3, 'resource_id' => $incident->id]])->checkIfCanInitiateAction(1);
        $users = new UserRepository();
        return view('backend.operation.claim.notification_report.support.assign_investigators')
            ->with("notification_report", $incident)
            ->with("users", $users->query()->orderBy('firstname', 'asc')->get()->pluck('name','id'));
    }


    public function storeInvestigators($id, AssignInvestigatorRequest $request)
    {
        $investigator = $this->notification_investigators->create($id,$request);
        return redirect('claim/notification_report/profile/' . $id . '#investigation')->withFlashSuccess(trans('alerts.backend.claim.investigators_assigned'));
    }


    /**
     * @param NotificationInvestigator $notification_investigator
     * @return $this
     */
    public function editInvestigator(NotificationInvestigator $notification_investigator)
    {
        workflow([['wf_module_group_id' => 3, 'resource_id' => $notification_investigator->notification_report_id]])->checkIfCanInitiateAction(1);
        $users = new UserRepository();
        return view('backend.operation.claim.notification_report.support.change_investigator')
            ->with("notification_investigator", $notification_investigator)
            ->with("users", $users->query()->orderBy('firstname', 'asc')->get()->pluck('name','id'));
    }
//Change Investigator
    public function changeInvestigator($notification_investigator_id, AssignInvestigatorRequest $request)
    {
        $notification_investigator = $this->notification_investigators->update($notification_investigator_id,$request);
        workflow([['wf_module_group_id' => 3, 'resource_id' => $notification_investigator->notification_report_id]])->checkIfCanInitiateAction(1);

        return redirect('claim/notification_report/profile/' . $notification_investigator->notification_report_id . '#investigation')->withFlashSuccess(trans('alerts.backend.claim.investigator_changed'));
    }

//   Activate investigation for this notification
    public function investigate($id)
    {

        workflow([['wf_module_group_id' => 3, 'resource_id' => $id]])->checkIfCanInitiateAction(1);
        $notification_report = $this->notification_reports->investigate($id);
        return redirect('claim/notification_report/profile/' . $id . '#investigation')->withFlashSuccess(trans('alerts.backend.claim.activate_investigation'));
    }

//   Deactivate / Cancel investigation for this notification
    public function cancelInvestigation($id)
    {
        workflow([['wf_module_group_id' => 3, 'resource_id' => $id]])->checkIfCanInitiateAction(1);
        $notification_report = $this->notification_reports->cancelInvestigation($id);
        return redirect('claim/notification_report/profile/' . $id . '#investigation')->withFlashSuccess(trans('alerts.backend.claim.cancel_investigation'));
    }


    //delete investigators
    public function deleteInvestigator($notification_investigator_id)
    {

        $notification_investigator = $this->notification_investigators->findOrThrowException($notification_investigator_id);
        workflow([['wf_module_group_id' => 3, 'resource_id' => $notification_investigator->notification_report_id]])->checkIfCanInitiateAction(1);

        $notification_investigator->delete();
        return redirect('claim/notification_report/profile/' . $notification_investigator->notification_report_id . '#investigation')->withFlashSuccess(trans('alerts.backend.claim.investigator_deleted'));

    }
    /**
     * Investigation reports
     */
    public function getInvestigationFeedbacksForDatatable($id) {
        return Datatables::of($this->notification_reports->getInvestigationFeedbacks($id))
            ->editColumn('investigation_question_id', function($investigation_feedback) {
                return $investigation_feedback->investigationQuestion->name;
            })
            ->make(true);
    }

    //get investigators
    public function getInvestigatorsForDatatable($id) {
        return Datatables::of($this->notification_investigators->query()->where('notification_report_id',$id))
            ->editColumn('user_id', function($notification_investigators) {
                return $notification_investigators->user->name;
            })
            ->make(true);
    }

    /**
     *  HEALTH PROVIDER SERVICES -====================================
     */


    public function getHealthProviderServicesForDatatable($id) {
        return Datatables::of($this->notification_reports->getHealthProviderServices($id))
            ->editColumn('amount', function($notification_health_provider_service) {
                return $notification_health_provider_service->amount_formatted;
            })
            ->editColumn('from_date', function($notification_health_provider_service) {
                return ($notification_health_provider_service->from_date) ? $notification_health_provider_service->from_date_formatted : ' ';
            })
            ->editColumn('to_date', function($notification_health_provider_service) {
                return ($notification_health_provider_service->to_date) ? $notification_health_provider_service->to_date_formatted : ' ';
            })
            ->addColumn('health_provider', function($notification_health_provider_service) {
                return $notification_health_provider_service->notificationHealthProvider->healthProviderWithTrashed->name;
            })
            ->addColumn('medical_practitioner', function($notification_health_provider_service) {
                return $notification_health_provider_service->notificationHealthProvider->notificationHealthProviderPractitioner->medicalPractitioner->fullname_with_external_id;
            })
            ->editColumn('health_service_checklist_id', function($notification_health_provider_service) {
                return $notification_health_provider_service->healthServiceChecklist->name;
            })
            ->addColumn('medical_expense', function($notification_health_provider_service) {
                return $notification_health_provider_service->notificationHealthProvider->medicalExpense->compensated_entity_name_with_amount;
            })
            ->addColumn('rank', function($notification_health_provider_service) {
                return $notification_health_provider_service->notificationHealthProvider->rank;
            })
            ->make(true);
    }

    /**
     * CLAIM METHODS ========================================
     */

    /**
     * APPROVE/ elevate NOTIFICATION  to CLAIM--
     * */
    public function approveToClaim($id) {
        $wf_module_group_id = 3;
        $this->checkLevel1Rights($id);
        DB::transaction(function () use ($wf_module_group_id, $id) {
            $claim = $this->notification_reports->approveToClaim($id);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id]));
        });

        return redirect('claim/notification_report/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.claim.approve_notification_to_claim'));
    }

    // end --- claim elevation ----------------


    /**
     * REJECT NOTIFICATION  REPORT--===
     * */
    public function reject($id) {
        $wf_module_group_id = 4;
        access()->hasWorkflowDefinition($wf_module_group_id,1);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id]])->checkIfCanInitiateAction(1);
        $input = request()->all();
        DB::transaction(function () use ($wf_module_group_id, $id, $input) {
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id], [], ['comments' => $input['comments']]));
            //update status
            $this->notification_reports->updateStatus($id, 2);
        });
        return redirect('claim/notification_report/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.claim.notification_report_rejected'));
    }

    // end --- notification rejection ----------------

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function reverseStatus($id)
    {
        $notification_report = $this->notification_reports->findOrThrowException($id);
        $status = $notification_report->status;
        if ($status == 1){
            /*Approved*/
            $wf_module_group_id = 3;
        }elseif ($status == 2){
            /*Rejected*/
            $wf_module_group_id = 4;
        }
        $this->checkLevelRights($id,1,$wf_module_group_id);
        $this->notification_reports->reverseStatus($notification_report, $wf_module_group_id);
        return redirect('claim/notification_report/profile/' . $id . '#general')->withFlashSuccess(trans('Success, Notification report status has been reversed'));
    }

    /**
     * APPROVE REJECTION APPEAL
     * Approve appealed rejected Notification report and reverse to initial state for approval to claim.
     */

    /**
     * Approve appeal Page
     * @param NotificationReport $incident
     * @return mixed
     * @throws GeneralException
     */
    public function approveRejectionAppealPage(NotificationReport $incident)
    {
        /*Check designation claim admin manager*/
        //$this->checkIfClaimAdminMgr();
        //access()->allowWithThrow("notification_appeal");
        return view('backend.operation.claim.notification_report.support.approve_rejection_appeal')
            ->with("notification_report", $incident);
    }

    /**
     * @param $id
     * @param ApproveRejectionAppealRequest $request
     * @return mixed
     * @throws GeneralException
     *
     */
    public function approveRejectionAppeal($id, ApproveRejectionAppealRequest $request)
    {
        $wf_module_group_id = 4; //Claim Rejection
        $input = $request->all();
        $notification_report = $this->notification_reports->findOrThrowException($id);
        /*Check designation claim admin manager*/
        $this->checkIfClaimAdminMgr();

        $this->notification_reports->approveRejectionAppeal($notification_report, $input, $wf_module_group_id);
        return redirect('claim/notification_report/profile/' . $id . '#general')->withFlashSuccess(trans('Success, Notification report rejection appeal has been approved'));

    }

    /*End Approve Rejection Appeal */


    /**
     * VERIFY NOTIFICATION  REPORT--===
     * */
    public function verify($id) {
        $this->checkLevel1Rights($id);
        $notification_report = $this->notification_reports->verify($id);
        return redirect('claim/notification_report/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.claim.notification_report_verified'));
    }

    // end --- notification rejection ----------------


    /*
     * CLAIM ASSESSMENT
     */
    /**
     * ACCIDENT WITNESSES METHODS======================
     */

    /**
     * @param NotificationReport $incident
     * @return $this
     */
    public function createAccidentWitness(NotificationReport $incident)
    {
        //$this->checkLevel1Rights($id);
        // access()->assignedForNotification($incident);

        return view('backend.operation.claim.notification_report.support.witness_create')
            ->with("notification_report", $incident);
    }

    /**
     * @param $id
     * @param CreateWitnessRequest $request
     * @return mixed
     */
    public function storeAccidentWitness($id, CreateWitnessRequest $request)
    {
        $witness = $this->witnesses->create($id, $request->all());
        return redirect('claim/notification_report/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.claim.witness_created'));
    }

    /**
     * @param Witness $witness
     * @return $this
     */
    public function editAccidentWitness(Witness $witness)
    {
        $notification_report = $witness->accidents()->first()->notificationReport;
        access()->assignedForNotification($notification_report);

        return view('backend.operation.claim.notification_report.support.witness_edit')
            ->with("notification_report", $notification_report)
            ->with("witness", $witness);
    }

    /**
     * @param Witness $witness
     * @param CreateWitnessRequest $request
     * @return mixed
     */
    public function updateAccidentWitness(Witness $witness, CreateWitnessRequest $request)
    {
        $this->witnesses->update($witness, $request->all());
        $id = $witness->accidents()->first()->notificationReport->id;
        return redirect('claim/notification_report/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.claim.witness_updated'));
    }

    /**
     * @param Witness $witness
     * @return mixed
     */
    public function deleteAccidentWitness(Witness $witness)
    {
        $notification_report = $witness->accidents()->first()->notificationReport;
        access()->assignedForNotification($notification_report);

        $this->witnesses->delete($witness, $notification_report->accident->id);
        return redirect('claim/notification_report/profile/' . $notification_report->id . '#general')->withFlashSuccess(trans('alerts.backend.claim.witness_deleted'));
    }

//--End witness -------------------



    /**
     * @param ManualNotificationReport $incident
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateOshData(NotificationReport $incident)
    {
        $codeValueRepo = new CodeValueRepository();
        $incident_types = (new IncidentTypeRepository())->getAll()->pluck('name', 'id')->all();
        $crRepo = new ConfigurableReportRepository();
        $cr = $crRepo->getReportModel(30, $incident->id, 'resourceid');

        $accident_cause_types = $codeValueRepo->getCodeValuesByCodeForSelect(58);
        $accident_cause_agencies = $codeValueRepo->getCodeValuesByCodeForSelect(57);
        $cv_repo = $codeValueRepo;
        /*Health providers*/
        $hcp_ids = $incident->hcps()->pluck('health_providers.id')->toArray();//hcp synced
        $health_providers = HealthProvider::query()->whereIn('id', $hcp_ids)->get()->pluck('fullname_info', 'id');

        return view('backend/operation/claim/notification_report/includes/osh_data/update_osh_data')
            ->with('incident', $incident)
            ->with('accident_cause_types', $accident_cause_types)
            ->with('accident_cause_agencies', $accident_cause_agencies)
            ->with('cv_repo', $cv_repo)
            ->with('health_providers', $health_providers)
            ->with('hcp_ids', $hcp_ids);
    }

    /*Update OSH DATA*/
    public function postOshData(NotificationReport $incident)
    {
        $input = request()->all();
        $return = $this->notification_reports->postOshData($incident, $input);
        return redirect()->back()->withFlashSuccess("Success, OSH Data updated");
    }


    /**
     * @param NotificationReport $notificationReport
     * @param UpdateBankDetailsRequest $request
     * @return mixed
     */
    public function updateBankDetails(NotificationReport $notificationReport, UpdateBankDetailsRequest $request)
    {
//
        $this->checkLevel1Rights($notificationReport->id);

        return DB::transaction(function () use ( $notificationReport, $request) {
            $input = $request->all();
            $employers = new EmployerRepository();
            $dependents  = new DependentRepository();
            if (isset($request['employee_id']) And ($request['employee_id'])) {
                $this->employees->updateBankDetails( $request->all());
            }
            if (isset($request['employer_id']) and ($request['employer_id'])) {
                $bank_branch_id = (isset($input['employer_bank_branch_id'])) ? $input['employer_bank_branch_id'] : null;
                $account_no = (isset($input['employer_accountno']))? $input['employer_accountno'] : null;
                $employer_id = $request['employer_id'];
                $employers->updateBankDetails($employer_id, $bank_branch_id, $account_no);
            }
            if (isset($request['wcf_id']) And ($request['wcf_id'])) {
                $bank_branch_id = (isset($input['wcf_bank_branch_id'])) ? $input['wcf_bank_branch_id'] : null;
                $account_no = (isset($input['wcf_accountno'])) ? $input['wcf_accountno'] : null;
                $employer_id = $request['wcf_id'];
                $employers->updateBankDetails($employer_id ,$bank_branch_id, $account_no);
            }
            if (isset($request['insurance_id']) And ($request['insurance_id'])) {
                $this->insurances->updateMultipleInsurancesBankDetails($request->all());
            }
            if (isset($request['dependent_id']) And ($request['dependent_id'])) {

                $dependents->updateMultipleDependentsBankDetails($request->all());
            }
            return redirect('claim/notification_report/profile/' . $notificationReport->id . '#bank_details')->withFlashSuccess(trans('alerts.backend.claim.bank_details_updated'));
        });
    }



    public function getPendingClaimForDataTable()
    {
        $repo = new WfTrackRepository();
        return Datatables::of($repo->getPendingClaimSummary())
            ->make(true);
    }

    //end bank details -------------------------

    /**
     * Get claims which constant care assistant not yet added - For Datatable.
     */
    public function getClaimsPendingCcaForDataTable()
    {
        return Datatables::of($this->claims->getClaimsPendingCcaForDataTable())
            ->addColumn('filename', function($claim) {
                return $claim->notificationReport->filename;
            })
            ->addColumn('employee', function($claim) {
                return $claim->notificationReport->employee->name;
            })
            ->addColumn('incident_type', function($claim) {
                return $claim->notificationReport->incidentType->name;
            })
            ->addColumn('incident_date', function($claim) {
                return short_date_format($claim->notificationReport->incident_date);
            })
            ->addColumn('action', function($claim) {
                return          '<a href="' . route('backend.compliance.dependent.create_new', $claim->notificationReport->employee_id) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-add" data-toggle="tooltip" data-placement="top" title="' . 'Add Constant Care'. '"></i>Add Constant Care</a> ';
            })
            ->rawColumns(['action'])
            ->make(true);

    }


//--end claim--------------------------------------

    public function returnNotificationDocumentSelectOptions(NotificationReport $notificationReport, $document_id)
    {
     $uploadedDocuments = $notificationReport->documents()->where('document_notification_report.document_id', $document_id)->whereNotIn("document_notification_report.id", function($query) {
         $query->select('document_notification_report_id')->from('assessment_documents');
     })->get();

     $append = '<option value="">Select document</option>';
     foreach ($uploadedDocuments as $upload_doc) {
        if (!empty($upload_doc->pivot)) {
            $append .= '<option value="'.$upload_doc->pivot->id.'">'.$upload_doc->name.' - '.$upload_doc->pivot->doc_date.'</option>';
        }
    }
    return response()->json(['success' => true, 'options' => $append]);
}

public function returnAppendInputs(NotificationEligibleBenefit $eligible, $disability_type_id =null)
{
    $disability_state_checklists = $eligible->notificationDisabilityStates;
    $html = '';
    $user_has_access = true;
    foreach ($disability_state_checklists as $notification_disability_state){
        if ($eligible->notificationDisabilityStateAssessments()->where('notification_disability_state_id', $notification_disability_state->id)->count()) {
          $disability_state_assessment = $eligible->notificationDisabilityStateAssessments->where('notification_disability_state_id',$notification_disability_state->id)->first();
          $disability_state_assessment_days = $disability_state_assessment->days;
          $disability_state_assessment_remarks = $disability_state_assessment->remarks;
      } else {
          $disability_state_assessment_days = NULL;
          $disability_state_assessment_remarks = NULL;
      }

      if(!is_null( $disability_state_assessment_days)){
          $html .= '
          <div class="row">
          <div class="form-inline type-of-duty-assessment-div mt-2">
          <div class="col-sm-1">
          <div class="form-group">
          <label for="days'.$notification_disability_state->id.'" id="checklist">'.$notification_disability_state->disabilityStateChecklist->name.'</label>
          </div></div>
          <div class="col-sm-3">
          <div class="form-group">
          <input type="text" name="assessed_days'.$notification_disability_state->id.'" value="'.$disability_state_assessment_days.'" class="form-control number disability_state" id="days'.$notification_disability_state->id.'" placeholder="No. of Days" />
          </div>
          </div>
          <div class="col-sm-4">
          <div class="form-group">
          <label for="remarks'.$notification_disability_state->id.'" id="checklist">Remark(s)</label>
          <textarea class="form-control autosize" style="overflow: hidden; word-wrap: break-word; resize: horizontal; border-radius: 3px;" name="remarks'.$notification_disability_state->id.'" autocomplete="none" id="remarks'.$notification_disability_state->id.'">'.$disability_state_assessment_remarks.'</textarea>
          </div>
          <input name="benefit_type" type="text" class="hidden form-control number disability_state" id="benefit_type'.$notification_disability_state->id.'" placeholder="No. of Days" value="'.$notification_disability_state->disabilityStateChecklist->benefit_type_id.'"/> 
          <input name="percent_of_ed_ld" type="text" class="hidden form-control number disability_state" id="percent_of_ed_ld'.$notification_disability_state->id.'" placeholder="Percent Of ED/LD" value="'.$notification_disability_state->percent_of_ed_ld.'"/>&nbsp;&nbsp;&nbsp;&nbsp;
          </div></div></div>';
      }
      if ($disability_type_id == $notification_disability_state->disabilityStateChecklist->id) {
          $html .= '
          <div class="row">
          <div class="form-inline type-of-duty-assessment-div mt-2">
          <div class="col-sm-1">
          <div class="form-group">
          <label for="days'.$notification_disability_state->id.'" id="checklist">'.$notification_disability_state->disabilityStateChecklist->name.'</label>
          </div></div>
          <div class="col-sm-3">
          <div class="form-group">
          <input type="text" name="assessed_days'.$notification_disability_state->id.'" value="'.$disability_state_assessment_days.'" class="form-control number disability_state" id="days'.$notification_disability_state->id.'" placeholder="No. of Days" />
          </div>
          </div>
          <div class="col-sm-4">
          <div class="form-group">
          <label for="remarks'.$notification_disability_state->id.'" id="checklist">Remark(s)</label>
          <textarea class="form-control autosize" style="overflow: hidden; word-wrap: break-word; resize: horizontal; border-radius: 3px;" name="remarks'.$notification_disability_state->id.'" autocomplete="none" id="remarks'.$notification_disability_state->id.'">'.$disability_state_assessment_remarks.'</textarea>
          </div>
          <input name="benefit_type" type="text" class="hidden form-control number disability_state" id="benefit_type'.$notification_disability_state->id.'" placeholder="No. of Days" value="'.$notification_disability_state->disabilityStateChecklist->benefit_type_id.'"/> 
          <input name="percent_of_ed_ld" type="text" class="hidden form-control number disability_state" id="percent_of_ed_ld'.$notification_disability_state->id.'" placeholder="Percent Of ED/LD" value="'.$notification_disability_state->percent_of_ed_ld.'"/>&nbsp;&nbsp;&nbsp;&nbsp;
          </div></div>
          <a href="#" class="remove_item"><i class="icon fa fa-2x fa-times" aria-hidden="true" style="color: red; vertical-align: middle;"></i></a></div>';
      }
  }
  return response()->json(['success' => true, 'append' => $html]);
}

}
