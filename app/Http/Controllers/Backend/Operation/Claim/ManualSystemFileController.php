<?php

namespace App\Http\Controllers\Backend\Operation\Claim;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\ChooseEmployeeRequest;
use App\Http\Requests\Backend\Operation\Claim\CompleteManualNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Claim\ManualSystemFileCreateDependentRequest;
use App\Http\Requests\Backend\Operation\Claim\ManualSystemFileCreateRequest;
use App\Http\Requests\Backend\Operation\Claim\UploadManualNotificationReportRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateEmployeeFromManualFileRequest;
use App\Models\Sysdef\JobTitle;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\ManualSystemFileRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\GenderRepository;
use App\Repositories\Backend\Sysdef\IdentityRepository;
use App\Repositories\Backend\Sysdef\JobTitleRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class ManualSystemFileController extends Controller
{
    //
    protected $manual_system_files;
    protected $notification_reports;

    public function __construct()
    {
        $this->manual_system_files = new ManualSystemFileRepository();
        $this->notification_reports = new NotificationReportRepository();

        $this->middleware('access.routeNeedsPermission:modify_dependents_approved_incident',['only' => ['editExistingDependent']]);
        $this->middleware('access.routeNeedsPermission:manage_payroll_data_migration',['only' => ['create', 'survivorsForDeathIncidentFile']]);

        //On Backend
        if(env('TESTING_MODE') == 1)
        {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }

    }

    public function index()
    {
        return view('backend/operation/claim/manual_system_file/index');

    }


    /**
     * @param $notification_report_id
     * @return mixed
     * Create manual system file
     */
    public function create($notification_report_id)
    {
        $incident_repo = new IncidentTypeRepository();
        $incident_types = $incident_repo->getAll()->pluck('name','id');
        $bank_repo = new BankRepository();
        $banks = $bank_repo->getAll()->pluck('name','id');
        $notification = $this->notification_reports->find($notification_report_id);
        return view('backend/operation/claim/manual_system_file/create')
            ->with('notification_report', $notification)
            ->with('incident_types', $incident_types)
            ->with('banks', $banks);

    }



    public function store($notification_report_id, ManualSystemFileCreateRequest $request)
    {
        $input = $request->all();
        $notification_report= $this->notification_reports->find($notification_report_id);
        $pensioner = $this->manual_system_files->store($notification_report, $input);
        $hasarrears = isset($input['hasarrears']) ? $input['hasarrears'] : 0;
        if($hasarrears == 1 && isset($pensioner)){
            /*Has arrears*/
            return redirect()->route('backend.payroll.recovery.create_manual_arrears_system_members',['member_type' => 5, 'resource' => $pensioner->id, 'employee_id' =>$notification_report->employee_id, 'arrears_months' => $input['pending_pay_months']])->withFlashSuccess('Success, Manual system file and pensioner have been created! Proceed to initiate approval of arrears');
        }else{
            /*no arrears*/
            return  redirect()->route('backend.claim.manual_system_file.index')->withFlashSuccess('Success, Manual system file has been created');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Death incidents
     */
    public function deathIncidents()
    {
        return view('backend/operation/claim/manual_system_file/death_incident/index');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Death incident file to add survivora
     */
    public function survivorsForDeathIncidentFile($notification_report_id)
    {
        $notification = $this->notification_reports->find($notification_report_id);
        $mp_data_summary =     (new NotificationReportRepository())->getMpGeneralDataSummary($notification);
        return view('backend/operation/claim/manual_system_file/death_incident/death_incident_survivors')
            ->with('notification_report', $notification)
            ->with('mp_data_summary', $mp_data_summary);
    }


    /**
     * @param $notification_report_id
     * @return $this
     * Create dependent of manual system file death incident
     */
    public function createDependent($notification_report_id)
    {
        $notification_report = $this->notification_reports->find($notification_report_id);
        $mp = (new ClaimCompensationRepository())->findPayableMpForDeathIncident($notification_report);
        $employee = (new EmployeeRepository())->find($notification_report->employee_id);
        return view('backend/operation/claim/manual_system_file/death_incident/dependent/create')
            ->with([
                'notification_report' => $notification_report,
                'employee' => $employee,
                'countries' => (new CountryRepository())->getAll()->pluck('name', 'id'),
                'genders' =>  (new GenderRepository())->getAll()->pluck('name', 'id'),
                'dependent_types' => (new DependentTypeRepository())->getAll()->pluck('name', 'id'),
                'location_types' => (new LocationTypeRepository())->getAll()->pluck('name', 'id'),
                'identities' => (new IdentityRepository())->getAll()->pluck('name', 'id'),
                'banks' => (new BankRepository())->getAll()->pluck('name', 'id'),
                'bank_branches' => (new BankBranchRepository())->getAll()->pluck('name', 'id'),
                'mp' => $mp,
            ]);
    }

    /**
     * @param $notification_report_id
     * @return $this
     * Save dependent
     */
    public function storeDependent($notification_report_id, ManualSystemFileCreateDependentRequest $request)
    {
        $input = $request->all();
        $notification_report = $this->notification_reports->find($notification_report_id);

        $dependent =  $this->manual_system_files->createManualDependent($notification_report, $input);
        $hasarrears = isset($input['hasarrears']) ? $input['hasarrears'] : 0;
        if($hasarrears == 1){
            /*Has arrears*/
            return redirect()->route('backend.payroll.recovery.create_manual_arrears_system_members',['member_type' => 4, 'resource' => $dependent->id, 'employee_id' => $notification_report->employee_id, 'arrears_months' => $input['pending_pay_months']])->withFlashSuccess('Success, Dependent has been synchronized! Proceed to initiate approval of arrears');
        }else{
            /*no arrears*/
            return redirect()->route('backend.claim.manual_system_file.survivors', $notification_report->id)->withFlashSuccess('Success, Dependent has been synchronized');
        }
    }

    /*Edit existing dependent*/
    public function editExistingDependent($dependent_id, $employee_id)
    {
        return redirect()->route('backend.compliance.dependent.edit_dependent', [$dependent_id, $employee_id]);
    }


    /*Open enrolled pensioners page*/
    public function enrolledPensionersPage()
    {
        return view('backend/operation/claim/manual_system_file/enrolled_pensioners');
    }

    /*Open enrolled survivors page*/
    public function enrolledSurvivorsPage()
    {
        return view('backend/operation/claim/manual_system_file/enrolled_dependents');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Get notification reports which are pending and might hav been processed manually
     */
    public function getNotificationReportsForDataTable()
    {

        return Datatables::of($this->manual_system_files->getNotificationReportsForDataTable())
            ->addColumn('incident_date_formatted', function ($notification) {
                return  isset($notification->incident_date) ?  short_date_format($notification->incident_date) : ' ';
            })
            ->addColumn('action', function ($notification) {
//                 $check_has_manual = $this->manual_system_files->query()->where('notification_report_id', $notification->case_no)->count();
//                return  ($check_has_manual > 0) ?  '<a href="' . route('backend.claim.manual_system_file.create', $notification->case_no) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Reset </a> ' : '<a href="' . route('backend.claim.manual_system_file.create', $notification->case_no) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Mark as Processed Manual </a> ' ;

                return '<a href="' . route('backend.claim.manual_system_file.create', $notification->case_no) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Mark as Processed Manual </a> ' ;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Get death incidents
     */
    public function getDeathIncidentsForDataTable()
    {

        return Datatables::of($this->manual_system_files->getDeathIncidentsForDataTable())
            ->addColumn('incident_date_formatted', function ($notification) {
                return  isset($notification->incident_date) ?  short_date_format($notification->incident_date) : ' ';
            })
            ->addColumn('action', function ($notification) {
//                 $check_has_manual = $this->manual_system_files->query()->where('notification_report_id', $notification->case_no)->count();
//                return  ($check_has_manual > 0) ?  '<a href="' . route('backend.claim.manual_system_file.create', $notification->case_no) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Reset </a> ' : '<a href="' . route('backend.claim.manual_system_file.create', $notification->case_no) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Mark as Processed Manual </a> ' ;

                return '<a href="' . route('backend.claim.manual_system_file.survivors', $notification->case_no) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Add Survivor' . '"></i> Add Survivor </a> ' ;
            })
            ->rawColumns(['action'])
            ->make(true);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Get survivors for death incidents
     */
    public function getSurvivorsForDataTable($notification_report_id)
    {
        $notification = $this->notification_reports->find($notification_report_id);
        $employee_id = $notification->employee_id;
        return Datatables::of($this->manual_system_files->getDependentsForDeathIncident($employee_id))
            ->addColumn('mp', function ($dependent) {
                return  isset($dependent->survivor_pension_amount) ?  number_2_format($dependent->survivor_pension_amount) : 0;
            })
            ->addColumn('action', function ($dependent) {
                return  ($dependent->isactive == 0 && $dependent->survivor_pension_amount == 0) ?    '<a href="' . route('backend.compliance.dependent.merge_manual_dependent_page', ['dependent' => $dependent->dependent_id, 'employee' => $dependent->employee_id]) . '" class="btn btn-xs btn-success " ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Enroll into Payroll </a> ' :  '' ;
//return ($dependent->isactive == 0) ?  '<a href="' . route('backend.claim.manual_system_file.edit_existing_dependent', ['dependent' => $dependent->dependent_id, 'employee' => $dependent->employee_id]) . '" class="btn btn-xs btn-success " ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Sync' . '"></i> Edit </a>' : '';

            })
            ->rawColumns(['action'])
            ->make(true);
    }




    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Get pensioners enrolled who has manual system files
     */
    public function getEnrolledPensionersForDataTable()
    {
        return Datatables::of($this->manual_system_files->getEnrolledPensionersForDataTable())
            ->addColumn('mp', function ($pensioner) {
                return  isset($pensioner->mp) ?  number_2_format($pensioner->mp) : 0;
            })
            ->addColumn('dob_formatted', function ($pensioner) {
                return  isset($pensioner->dob) ?  short_date_format($pensioner->dob) : ' ';
            })
            ->rawColumns([''])
            ->make(true);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Get survivors enrolled who has manual system files
     */
    public function getEnrolledSurvivorsForDataTable()
    {
        return Datatables::of($this->manual_system_files->getEnrolledSurvivorsForDataTable())
            ->addColumn('mp', function ($dependent) {
                return  isset($dependent->survivor_pension_amount) ?  number_2_format($dependent->survivor_pension_amount) : 0;
            })
            ->addColumn('dob_formatted', function ($dependent) {
                return  isset($dependent->dob) ?  short_date_format($dependent->dob) : ' ';
            })
            ->rawColumns([''])
            ->make(true);
    }






}
