<?php

namespace App\Http\Controllers\Backend\Operation\Compliance;

use App\Http\Requests\Backend\Finance\Receipt\ChooseEmployerRequest;
use App\Http\Requests\Backend\Operation\Compliance\UploadBookingGroupManualRequest;
use App\Jobs\Finance\LoadBookingGroupManualContribution;
use App\Repositories\Backend\Operation\Compliance\BookingGroupRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\Finance\Receipt\LegacyReceiptTemp;


class BookingGroupController extends Controller
{
    use AttachmentHandler;

    protected $booking_group;

    public function __construct(BookingGroupRepository $booking_group)
    {
        $this->middleware('access.routeNeedsPermission:manage_booking_groups');
        $this->booking_group = $booking_group;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("backend/operation/compliance/booking_group/index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChooseEmployerRequest $request)
    {
        $input = $request->all();
        $this->booking_group->store($input);
        return redirect()->back()->withFlashSuccess("Success, Employer has been added as a booking group");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $return = $this->booking_group->destroy(request()->all());
        if ($return) {
            $message = "Success, Selected employer(s) have been removed to the booking group list";
        } else {
            $message = "No employer(s) selected!";
        }
        return redirect()->back()->withFlashSuccess($message);
    }

    public function pendingUpload()
    {
        return view("backend/operation/compliance/booking_group/pending_upload");
    }

    public function uploadManual(Request $request)
    {
        if ($request->has('employer_id')) {
            $group = $this->booking_group->query()->where("employer_id", $request->input("employer_id"))->first();
        } else {
            $group = [];
        }
        return view("backend/operation/compliance/booking_group/upload_manual")
            ->with("booking_groups", $this->booking_group->getForDataTable()->pluck('name', "employer_id"))
            ->with("group", $group);
    }

    public function postUploadManual(UploadBookingGroupManualRequest $request)
    {
        $employer_id = $request->input("employer_id");
        if ($this->saveManualBookingGroupFileAttachment($employer_id)) {
            dispatch(new LoadBookingGroupManualContribution($employer_id));
        }
        return response()->json(['success' => true, 'id' => $employer_id]);
    }

    public function getBookingGroupsForDatatable()
    {
        return Datatables::of($this->booking_group->getForDataTable())
            ->make(true);
    }

    public function downloadManualError()
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=booking_group_failed_entries.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        $list = LegacyReceiptTemp::query()->select(['votecode', 'employer_id', 'contrib_month', 'amount', 'rctno', 'rct_date', 'error', 'member_count'])->where(['haserror' => 1])->get()->toArray();

        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };
        return response()->stream($callback, 200, $headers);
    }

}
