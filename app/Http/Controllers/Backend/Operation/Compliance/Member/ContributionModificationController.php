<?php
namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use DB;
use Carbon\Carbon;
use App\Http\Requests\Backend\Operation\Compliance\ContributionModificationRequest;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionModificationRepository;
use Yajra\Datatables\Services\DataTable;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Exceptions\GeneralException;
use App\DataTables\WorkflowTrackDataTable;

class ContributionModificationController extends Controller
{

    protected $modification_repo;

    public function __construct(ContributionModificationRepository $modification_repo)
    {
        $this->modification_repo = $modification_repo;
    }

    public function index(Employer $employer)
    {
        return view('backend/operation/compliance/member/employer/contrib_modification/index')->with('employer', $employer);
    }

    public function create(Employer $employer)
    {
        $cv_repo = new CodeValueRepository();
        return view('backend/operation/compliance/member/employer/contrib_modification/create')->with('employer', $employer);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        if (request()->ajax())
        {
            $rules = ['request_type' => 'required', 'receipt_id' => 'required', 
            'request_attachment'=>'required|mimetypes:application/pdf,image/png,image/tiff,image/x-tiff,image/bmp,image/x-windows-bmp,image/gif,image/x-icon,image/jpeg,image/pjpeg,image/x-portable-bitmap',
            'receipt_attachment'=>'required|mimetypes:application/pdf,image/png,image/tiff,image/x-tiff,image/bmp,image/x-windows-bmp,image/gif,image/x-icon,image/jpeg,image/pjpeg,image/x-portable-bitmap', 
            'request_date' => 'required|date|date_format:Y-m-d'];

            $message = ['request_type.*' => 
            'Select type of request to modify', 
            'receipt_id.*' => 'Select receipt to modify', 
            'receipt_attachment.*' => 'Kindly upload a document in either pdf or image not more than 2MB',
            'request_attachment.*' => 'Kindly upload a document in either pdf or image not more than 2MB',
            'request_date.*' => 'Select date the request was received'];

            $input = $request->all();
            $optional = [];
            $optional_message = [];
            if (isset($input['request_type']))
            {
                $type_rules = [];
                switch ($input['request_type'])
                {
                    case '1':
                    //change months rules for validation
                    foreach ($input as $key => $value)
                    {
                        if (strpos($key, 'month_new') !== false)
                        {
                           $id = str_replace("month_new_", "", $key);
                            if ((int)$input['payment_type_'.$id] == 1) {
                                $before = Carbon::parse(Carbon::now()->subMonths(2)->endOfMonth())->format('Y-m-d');
                                $optional[$key] = 'required|date_format:Y-m-d|before_or_equal:'.$before;
                            }else{
                                $optional[$key] = 'required|date_format:Y-m-d';
                            }

                            $optional_message[$key . '.before_or_equal'] = 'Month is not eligible for interest';
                            $optional_message[$key . '.*'] = 'Select Valid Month';
                        }
                    }
                    break;
                    case '2':
                     //redistribute amount rules for validation
                    foreach ($input as $key => $value)
                    {
                        if (strpos($key, 'contrib_month') !== false)
                        {
                            $id = str_replace("contrib_month_", "", $key);
                            if ((int)$input['payment_type_'.$id] == 1) {
                                $before = Carbon::parse(Carbon::now()->subMonths(2)->endOfMonth())->format('Y-m-d');
                                $optional[$key] = 'required|date_format:Y-m-d|before_or_equal:'.$before;
                            }else{
                                $optional[$key] = 'required|date_format:Y-m-d';
                            }

                            $optional_message[$key . '.before_or_equal'] = 'Month is not eligible for interest';
                            $optional_message[$key . '.*'] = 'Select Valid Month';
                        }
                        if (strpos($key, 'contrib_amount') !== false)
                        {
                            $optional[$key] = 'required|numeric';
                            $optional_message[$key . '.*'] = 'Kindly enter valid amount';
                        }
                        if (strpos($key, 'employee_count') !== false)
                        {
                            $id = str_replace("employee_count_", "", $key);
                            if ((int)$input['payment_type_'.$id] == 2) {
                                $optional[$key] = 'required|numeric';
                                $optional_message[$key . '.*'] = 'Kindly enter valid employee count';
                            }
                        } if (strpos($key, 'payment_type') !== false)
                        {
                            $optional[$key] = 'required';
                            $optional_message[$key . '.*'] = 'Kindly select payment type';
                        }
                    }
                    break;
                    default:

                    break;
                }
            }

            $rules = array_merge($rules, $optional);
            $message = array_merge($message, $optional_message);

            $validator = Validator::make(Input::all() , $rules, $message);

            if ($validator->fails())
            {
                return response()
                ->json(array(
                    'errors' => $validator->getMessageBag()
                    ->toArray()
                ));
            }
            //check if receipt has pending modification request
            if ($this
                ->modification_repo
                ->checkReceiptHasPendingRequest($request->receipt_id, $request->receipt_type))
            {
                return response()
                ->json(array(
                    'errors' => ['receipt_id' => 'The selected receipt has pending modification request.']
                ));
            }
            switch ($input['request_type'])
            {
                case '1':
                $old = [];
                $new = [];
                $check_dupe_interests = [];
                $check_dupe_month = [];
                //other checks on change months 
                foreach ($input as $key => $value)
                {
                    if (strpos($key, 'month_new') !== false)
                    {
                        $id = str_replace("month_new_", "", $key);
                        $rct_code = (int)$request->receipt_type == 2 ? DB::table('main.legacy_receipt_codes')
                        ->find($id) : DB::table('main.receipt_codes')->find($id);
                        if ($rct_code->contrib_month != Carbon::parse($value)->format('Y-m-28'))
                        {
                            $old[$id] = $rct_code->contrib;
                            $new[$id] = Carbon::parse($value)->format('Y-m-28');
                        }

                        if ((int)$input["payment_type_" . $id] == 2)
                        {
                            array_push($check_dupe_month, Carbon::parse($value)->format('Y-m-28'));
                        }
                        else
                        {
                            array_push($check_dupe_interests, Carbon::parse($value)->format('Y-m-28'));
                        }
                    }
                }
                if ($this->hasDupes($check_dupe_month) || $this->hasDupes($check_dupe_interests))
                {
                    return response()->json(array('errors' => ['general' => 'You have submitted duplicate months for same payment type (Interest / Contribution). Kindly check and retry']));
                }

                if (!empty($new))
                {
                    //if there is no changes save changes
                    $result = $this
                    ->modification_repo
                    ->saveNew($request->all() , $request);
                    return $result ? response()->json(array(
                        'success' => true,
                        'request_id' => $result->id
                    )) : response()
                    ->json(array(
                        'errors' => ['general' => 'Something went wrong. Kindly retry']
                    ));
                }
                else
                {
                    return response()
                    ->json(array(
                        'errors' => ['general' => 'Data submitted is the same as the existing data. Kindly check and retry.']
                    ));
                }
                break;

                case '2':
                $new = [];
                $check_month = [];
                $check_interest_month = [];
                foreach ($input as $key => $value)
                {
                    if (strpos($key, 'contrib_month') !== false)
                    {
                        $key_id = str_replace("contrib_month_", "", $key);
                        $new[$key_id]['contrib_month'] = Carbon::parse($value)->format('Y-m-28');
                        if ((int)$input["payment_type_" . $id] == 2)
                        {
                            array_push($check_month, Carbon::parse($value)->format('Y-m-28'));
                        }
                        else
                        {
                            array_push($check_interest_duplicates, Carbon::parse($value)->format('Y-m-28'));
                        }
                    }
                }

                if ($this->hasDupes($check_month) || $this->hasDupes($check_interest_duplicates))
                {
                    return response()->json(array(
                        'errors' => ['general' => 'You have submitted duplicate months. Kindly check and retry']
                    ));
                }
                if (count($new) > 1)
                {
                    $result = $this
                    ->modification_repo
                    ->saveNew($request->all() , $request);
                    return $result ? response()->json(array(
                        'success' => true,
                        'request_id' => $result->id
                    )) : response()
                    ->json(array(
                        'errors' => ['general' => 'Something went wrong. Kindly retry']
                    ));
                }
                else
                {
                    return response()
                    ->json(array(
                        'errors' => ['general' => 'Data submitted is the same as the existing data. Kindly check and retry.']
                    ));
                }
                break;

                default:
                throw new GeneralException('Nothing selected');
                break;
            }
        }
        else
        {
            throw new GeneralException('Request not allowed');
        }
    }

    public function profile($contrib_mod_id, WorkflowTrackDataTable $workflowtrack)
    {
        $modification_request = $this
        ->modification_repo
        ->findOrThrowException($contrib_mod_id);
        $employer = Employer::find($modification_request->employer_id);
        return view('backend/operation/compliance/member/employer/contrib_modification/profile')
        ->with('employer', $employer)->with('modification_request', $modification_request)->with("workflowtrack", $workflowtrack);
    }

    public function returnRequestsDatatable($employer_id)
    {
        $data = $this
        ->modification_repo
        ->getRequestsForDataTable($employer_id);
        return DataTables::of($data)->editColumn('request_type', function ($data)
        {
            return $data->request_type_formatted;
        })->editColumn('status', function ($data)
        {
            return $data->stage_label;
        })
        ->rawColumns(['status'])
        ->make(true);
    }

    public function returnAllReceiptNo($employer_id)
    {
        $input = request()->all();
        $data = [];
        if (isset($input['q']))
        {
            $search = strtolower(trim($input['q']));

            $legacyreceipt = DB::table('main.legacy_receipts')->select(DB::raw('cast(rctno as bigint)') , 'amount', 'rct_date', 'description', 'employer_id', 'id', DB::raw("2 as receipt_type"))
            ->whereRaw("CAST(rctno AS TEXT) like '%" . $search . "%'")->where('employer_id', $employer_id)->whereNull('deleted_at');

            $MACreceipt = DB::table('main.receipts')->select('rctno', 'amount', 'rct_date', 'description', 'employer_id', 'id', DB::raw("1 as receipt_type"))
            ->where('employer_id', $employer_id)->whereRaw("CAST(rctno AS TEXT) like '%" . $search . "%'")->where('iscancelled', 0)
            ->whereNull('deleted_at')
            ->unionAll($legacyreceipt)->get();

            // $data = DB::table('main.receipts')->whereRaw("CAST(rctno AS TEXT) like '%" . $search . "%'")->where('employer_id', $employer_id)->get();
            $data = $MACreceipt;
        }
        return $data;
    }

    public function returnReceiptDetails($rct_id, $rct_type)
    {
        //return selected receipt details
        if ((int)$rct_type == 2)
        {
            $rct = DB::table('main.legacy_receipts')->where('id', $rct_id)->first();
        }
        else
        {
            $rct = DB::table('main.receipts')->where('id', $rct_id)->first();
        }
        return response()
        ->json(['success' => true, 'rct' => $rct]);
    }

    public function formFields($rct_id, $type, $rct_type)
    {
        if ((int)$rct_type == 2)
        {
            $rct_codes = DB::table('main.legacy_receipt_codes')->select('legacy_receipt_codes.*')
            ->join('main.legacy_receipts', 'legacy_receipt_codes.legacy_receipt_id', '=', 'legacy_receipts.id')
            ->where('legacy_receipt_id', $rct_id)->orderBy('fin_code_id', 'desc')
            ->orderBy('contrib_month', 'desc')
            ->whereNull('legacy_receipt_codes.deleted_at')
            ->get();
        }
        else
        {
            $rct_codes = DB::table('main.receipt_codes')->select('receipt_codes.*')
            ->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
            ->where('receipt_id', $rct_id)->whereNull('receipt_codes.deleted_at')
            ->orderBy('fin_code_id', 'desc')
            ->orderBy('contrib_month', 'desc')
            ->get();
        }
        switch ($type)
        {
            case '1':
                //change month
            $return = $this->changeMonthField($rct_codes);
            return response()->json(['success' => success, 'append_field' => $return]);
            break;
            case '2':
                //redistribute
                // hapa rct_id ni idadi ya miezi
            $return = $this->redistributeAmountField($rct_codes);
            return response()->json(['success' => success, 'append_field' => $return]);
            break;
            default:
            return response()->json(['success' => false]);
            break;
        }
    }

    public function changeMonthField($rcts)
    {
        $result = '';
        foreach ($rcts as $rct)
        {
            $select_name = $rct->fin_code_id == 1 ? 'Interest' : 'Contribution';
            $result .= '<div class="row mb-1" ><div class="col-sm-3"><label class="required"> Month</label><div class="form-inline"><div class="input-group" style="width:100%;"><input type="text" value="' . $rct->contrib_month . '" name="month_new_' . $rct->id . '" class="form-control datepicker2"><span class="input-group-addon"><i class="icon fa fa-calendar"></i></span></div></div><strong><span class="month_new_' . $rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-3"><label class="required">Payment Type</label><select class="form-control" name="payment_type_' . $rct->id . '"> <option value="' . $rct->fin_code_id . '"  selected="selected" >' . $select_name . '</option> </select> </div><div class="col-sm-3"><label class="required"> Amount</label><input type="text" class="form-control" readonly="readonly" value="' . number_format($rct->amount, 2) . '"></div><div class="col-sm-2"><label class="required">Employee Count</label><input type="text" class="form-control" readonly="readonly" value="' . $rct->member_count . '"></div></div>';
        }
        return $result .= '<div class="row mb-1" ><div class="col-sm-12"><strong><span class="general_error general_error_2 help-block label label-danger hidden submit_errors"></span></strong> </div></div>';
    }

    public function redistributeAmountField($rcts)
    {
        $result = '';
        foreach ($rcts as $rct)
        {
            $select_name = $rct->fin_code_id == 1 ? 'Interest' : 'Contribution';
            $result .= '<div class="row mb-1 row_' . $rct->id . '" ><div class="col-sm-3"><label class="required"> Month</label><div class="form-inline"><div class="input-group" style="width:100%;"><input type="text" readonly="readonly" value="' . $rct->contrib_month . '" name="contrib_month_' . $rct->id . '" class="form-control "><span class="input-group-addon"><i class="icon fa fa-calendar"></i></span></div></div><strong><span class="contrib_month_' . $rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-2"><label class="required">Payment Type</label><select class="form-control" name="payment_type_' . $rct->id . '"> <option value="' . $rct->fin_code_id . '"  selected="selected" >' . $select_name . '</option> </select> <strong><span class="payment_type_' . $rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-2"><label class="required"> Amount</label><input type="text" class="form-control namba contrib_amount" value="' . number_format($rct->amount, 2) . '" name="contrib_amount_' . $rct->id . '"><strong><span class="contrib_amount_' . $rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-2"><label class="required">Employee Count</label><input type="text" class="form-control namba"  value="' . $rct->member_count . '" name="employee_count_' . $rct->id . '"><strong><span class="employee_count_' . $rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong> </div><div class="col-sm-2 mt-1"> <a class="remove_field" id="' . $rct->id . '" href="#"><i class="icon fa fa-3x fa-times" aria-hidden="true" style="color:red"></i></a></div></div>';
        }
        return $result .= '<div class="row mb-1" ><div class="col-sm-12"><strong><span class="general_error general_error_2 help-block label label-danger hidden submit_errors"></span></strong> </div></div>';
    }

    public function addRedisitributionFields($number_of_fields)
    {
        $result = '';
        for ($i = 1;$i <= $number_of_fields;$i++)
        {
            $a = substr(md5(mt_rand()) , 0, 6);
            $result .= '<div class="row mb-1 row_' . $a . '" ><div class="col-sm-3"><label class="required"> Month</label><div class="form-inline"><div class="input-group" style="width:100%;"><input type="text" name="contrib_month_' . $a . '" class="form-control datepicker2"><span class="input-group-addon"><i class="icon fa fa-calendar"></i></span></div></div><strong><span class="contrib_month_' . $a . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-2"><label class="required">Payment Type</label><select class="form-control" name="payment_type_' . $a . '"> <option value="">Click to select</option> <option value="2" >Contribution</option><option value="1" >Interest</option> </select> <strong><span class="payment_type_' . $a . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-2"><label class="required"> Amount</label><input type="text" class="form-control namba contrib_amount" name="contrib_amount_' . $a . '"><strong><span class="contrib_amount_' . $a . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-2"><label class="required">Employee Count</label><input type="text" class="form-control namba" name="employee_count_' . $a . '"><strong><span class="employee_count_' . $a . '_error help-block label label-danger hidden submit_errors"></span></strong> </div><div class="col-sm-2 mt-1"> <a class="remove_field" id="' . $a . '" href="#"><i class="icon fa fa-3x fa-times" aria-hidden="true" style="color:red"></i></a></div></div>';
        }
        $result .= '<div class="row mb-1" ><div class="col-sm-12"><strong><span class="general_error general_error_2 help-block label label-danger hidden submit_errors"></span></strong> </div></div>';
        return response()->json(['success' => success, 'append_field' => $result]);
    }

    public function submitForReview($request_id)
    {
        $modification_request = $this
        ->modification_repo
        ->findOrThrowException($request_id);
        $return = $this
        ->modification_repo
        ->submitForReviewAndApproval($modification_request);
        if ($return)
        {
            return redirect()->route('backend.compliance.employer.contrib_modification.profile', $modification_request->id)
            ->withFlashSuccess('Success, Contribution Modification Request has been submitted for review and approval');
        }
        else
        {
            return redirect()
            ->route('backend.compliance.employer.contrib_modification.profile', $modification_request->id)
            ->withFlashWarning('Error! Something went wrong. Request Faield');
        }
    }

    public function edit($request_id)
    {
       $modification_request = $this
       ->modification_repo
       ->findOrThrowException($request_id);
       if ($modification_request->user_id != access()->user()->id) {
        throw new GeneralException('Access Denied! You are not authorised to perfom this action');
    }

    $employer = $modification_request->employer;
    return view('backend/operation/compliance/member/employer/contrib_modification/edit')->with(['employer'=>$employer,'modification_request' => $modification_request]);
}

public function previewAttachment($modification_id)
{
    $url = $this
    ->modification_repo
    ->getAttachmentPath($modification_id);
    return response()->json(['success' => true, "request_url" => $url['request_path'], "receipt_url" => $url['receipt_path']]);
}

function hasDupes($check_array)
{
    $dupe_array = array();
    foreach ($check_array as $val)
    {
        if (++$dupe_array[$val] > 1)
        {
            return true;
        }
    }
    return false;
}


public function returnEditformFields($request_id)
{
  $modification_request = $this
  ->modification_repo
  ->findOrThrowException($request_id);
  $result = '';

  switch ($modification_request->request_type) {
    case 1:
    $receipt_codes_exclude = [];
    foreach ($modification_request->months as $rct)
    {
        $payment_type_name = $rct->fin_code_id == 1 ? 'Interest' : 'Contribution';
        $result .= '<div class="row mb-1" ><div class="col-sm-3"><label class="required">Old Month</label><div class="form-inline"><div class="input-group" style="width:100%;"><input type="text" value="' . $rct->old_month. '" name="old_month_' . $rct->receipt_code_id . '" class="form-control" readonly="readonly"><span class="input-group-addon"><i class="icon fa fa-calendar"></i></span></div></div><strong><span class="old_month_' . $rct->receipt_code_id . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-3"><label class="required">New Month</label><div class="form-inline"><div class="input-group" style="width:100%;"><input type="text" value="' . $rct->new_month . '" name="month_new_' . $rct->receipt_code_id . '" class="form-control datepicker2"><span class="input-group-addon"><i class="icon fa fa-calendar"></i></span></div></div><strong><span class="month_new_' . $rct->receipt_code_id . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-3"><label class="required">Payment Type</label><select class="form-control" name="payment_type_' .$rct->receipt_code_id. '"> <option value="'.$rct->fin_code_id.'" >'. $payment_type_name.'</option></select> <strong><span class="payment_type_' .$rct->receipt_code_id. '_error help-block label label-danger hidden submit_errors"></span></strong></div></div>';
        array_push($receipt_codes_exclude, $rct->receipt_code_id);
    }
    // dd($receipt_codes_exclude);
    if ($modification_request->request_type == 2)
    {
        $rct_codes = DB::table('main.legacy_receipt_codes')->select('legacy_receipt_codes.*')
        ->join('main.legacy_receipts', 'legacy_receipt_codes.legacy_receipt_id', '=', 'legacy_receipts.id')
        ->whereNotIn('legacy_receipt_codes.id',$receipt_codes_exclude)
        ->where('legacy_receipt_id', $modification_request->receipt_id)->whereNull('legacy_receipt_codes.deleted_at')
        ->get();
    }
    else
    {
        $rct_codes = DB::table('main.receipt_codes')->select('receipt_codes.*')
        ->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
        ->whereNotIn('receipt_codes.id',$receipt_codes_exclude)
        ->where('receipt_id', $modification_request->receipt_id)->whereNull('receipt_codes.deleted_at')
        ->get();
    }

    foreach ($rct_codes as $code_rct) {
        $select_name = $code_rct->fin_code_id == 1 ? 'Interest' : 'Contribution';
        $result .= '<div class="row mb-1"><div class="col-sm-3"><label>Old Month</label><div class="form-inline"><div class="input-group" style="width:100%;"><input type="text" value="' . $code_rct->contrib_month . '" name="old_month_' . $code_rct->id . '" class="form-control" readonly="readonly"><span class="input-group-addon"><i class="icon fa fa-calendar"></i></span></div></div></div> <div class="col-sm-3"><label class="required">New Month</label><div class="form-inline"><div class="input-group" style="width:100%;"><input type="text" value="' . $code_rct->contrib_month . '" name="month_new_' . $code_rct->id . '" class="form-control datepicker2"><span class="input-group-addon"><i class="icon fa fa-calendar"></i></span></div><strong><span class="month_new_' . $code_rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong></div></div><div class="col-sm-3"><label class="required">Payment Type</label><select class="form-control" name="payment_type_' . $code_rct->id . '"> <option value="' . $code_rct->fin_code_id . '"  selected="selected" >' . $select_name . '</option> </select> </div></div>';
    }
    break;
    case 2:
    foreach ($modification_request->redistributions as $rct) 
    {
       $select_name = $rct->fin_code_id == 1 ? 'Interest' : 'Contribution';
       $result .= '<div class="row mb-1 row_'.$rct->id.'" ><div class="col-sm-3"><label class="required"> Month</label><div class="form-inline"><div class="input-group" style="width:100%;"><input type="text" name="contrib_month_' . $rct->id . '" class="form-control" value="'.$rct->contrib_month.'" datepicker2><span class="input-group-addon"><i class="icon fa fa-calendar"></i></span></div></div><strong><span class="contrib_month_' . $rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-2"><label class="required">Payment Type</label><select class="form-control" name="payment_type_' . $rct->id . '"><option value="' . $rct->fin_code_id . '"  selected="selected" >' . $select_name . '</option> </select> </div><div class="col-sm-2"><label class="required"> Amount</label><input type="text" class="form-control namba contrib_amount" name="contrib_amount_' . $rct->id . '" value="'.number_format($rct->amount,2).'"><strong><span class="contrib_amount_' . $rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong></div><div class="col-sm-2"><label class="required">Employee Count</label><input type="text" class="form-control namba" value="'.$rct->member_count.'" name="employee_count_' . $rct->id . '"><strong><span class="employee_count_' . $rct->id . '_error help-block label label-danger hidden submit_errors"></span></strong> </div><div class="col-sm-2 mt-1"> <a class="remove_field" id="' . $rct->id . '" href="#"><i class="icon fa fa-3x fa-times" aria-hidden="true" style="color:red"></i></a></div></div>';
   }
   break;

   default:
   break;
}

$result .= '<div class="row mb-1" ><div class="col-sm-12"><strong><span class="general_error general_error_2 help-block label label-danger hidden submit_errors"></span></strong> </div></div>';
return response()->json(['success' => success, 'append_field' => $result]);}



public function update($request_id, Request $request)
{
    if (request()->ajax())
    {
        $modification_request = $this->modification_repo->findOrThrowException($request_id);
        $rules = ['reason' => 'required', 'request_date' => 'required|date|date_format:Y-m-d',
        'request_attachment'=>'nullable|mimetypes:application/pdf,image/png,image/tiff,image/x-tiff,image/bmp,image/x-windows-bmp,image/gif,image/x-icon,image/jpeg,image/pjpeg,image/x-portable-bitmap',
        'receipt_attachment'=>'nullable|mimetypes:application/pdf,image/png,image/tiff,image/x-tiff,image/bmp,image/x-windows-bmp,image/gif,image/x-icon,image/jpeg,image/pjpeg,image/x-portable-bitmap'
    ];
    $message = ['request_date.*' => 'Select date the request was received',
    'receipt_attachment.*' => 'Kindly upload a document in either pdf or image not more than 2MB',
    'request_attachment.*' => 'Kindly upload a document in either pdf or image not more than 2MB'];

    $input = $request->all();
    $optional = [];
    $optional_message = [];

    $type_rules = [];
    switch ($modification_request->request_type)
    {
        case '1':
        foreach ($input as $key => $value)
        {
            if (strpos($key, 'month_new') !== false)
            {
                $optional[$key] = 'nullable|date_format:Y-m-d';
                $optional_message[$key . '.*'] = 'Select valid month';
            }
        }
        break;
        case '2':
        foreach ($input as $key => $value)
        {
            if (strpos($key, 'contrib_month') !== false)
            {
                $optional[$key] = 'required|date_format:Y-m-d';
                $optional_message[$key . '.*'] = 'Select Valid Contribution Month';
            }
            if (strpos($key, 'contrib_amount') !== false)
            {
                $optional[$key] = 'required|numeric';
                $optional_message[$key . '.*'] = 'Kindly enter valid amount';
            }
            if (strpos($key, 'employee_count') !== false)
            {
                $id = str_replace("employee_count_", "", $key);
                if ((int)$input['payment_type_'.$id] !== 1) {
                    $optional[$key] = 'required|numeric';
                    $optional_message[$key . '.*'] = 'Kindly enter valid employee count';
                }
            }
            if (strpos($key, 'payment_type') !== false)
            {
                $optional[$key] = 'required';
                $optional_message[$key . '.*'] = 'Kindly select payment type';
            }
        }
        break;
        default:

        break;
    }
    $rules = array_merge($rules, $optional);
    $message = array_merge($message, $optional_message);

    $validator = Validator::make(Input::all() , $rules, $message);

    if ($validator->fails())
    {
        return response()
        ->json(array(
            'errors' => $validator->getMessageBag()
            ->toArray()
        ));
    }

    if ($this
        ->modification_repo
        ->checkReceiptHasPendingRequest($request->receipt_id, $request->receipt_type,  $modification_request))
    {
        return response()
        ->json(array(
            'errors' => ['receipt_id' => 'The selected receipt has pending request.']
        ));
    }

    switch ($modification_request->request_type)
    {
        case '1':
        $old = [];
        $new = [];
        $check_dupe_month = [];
        $check_dupe_interests = [];
        foreach ($input as $key => $value)
        {
            if (strpos($key, 'month_new') !== false)
            {
                    $id = (int)str_replace("month_new_", "", $key); //receipt_code_id
                    $rct_code = $modification_request->request_type == 2 ? DB::table('main.legacy_receipt_codes')->find($id) : DB::table('main.receipt_codes')->find($id);
                    if (Carbon::parse($rct_code->contrib_month)->format('Y-m-28') != Carbon::parse($value)->format('Y-m-28'))
                    {
                        $old[$id] = Carbon::parse($rct_code->contrib_month)->format('Y-m-28');
                        $new[$id] = Carbon::parse($value)->format('Y-m-28');

                    }
                    if ((int)$input["payment_type_". $id] == 2)
                    {
                        array_push($check_dupe_month, Carbon::parse($value)->format('Y-m-28'));
                    }
                    else
                    {
                        array_push($check_dupe_interests, Carbon::parse($value)->format('Y-m-28'));
                    }
                }
            }
            if ($this->hasDupes($check_dupe_month) || $this->hasDupes($check_dupe_interests))
            {
                return response()->json(array(
                    'errors' => ['general' => 'You have submitted duplicate months. Kindly check and retry']
                ));
            }
            if (!empty($new))
            {
                $result = $this
                ->modification_repo
                ->updateEditedRequest($modification_request, $input, $request);
                return $result ? response()->json(array(
                    'success' => true,
                    'request_id' => $result->id
                )) : response()
                ->json(array(
                    'errors' => ['general' => 'Something went wrong. Kindly retry']
                ));
            }
            else
            {
                return response()
                ->json(array(
                    'errors' => ['general' => 'Data submitted is the same as the existing receipt data. Kindly check and retry.']
                ));
            }
            break;

            case '2':
            $new = [];
            $check_month = [];
            $check_interest_duplicates = [];
            foreach ($input as $key => $value)
            {
                if (strpos($key, 'contrib_month') !== false)
                {
                    $key_id = str_replace("contrib_month_", "", $key);
                    $new[$key_id]['contrib_month'] = Carbon::parse($value)->format('Y-m-28');
                    if ((int)$input["payment_type_" . $id] == 2)
                    {
                        array_push($check_month, Carbon::parse($value)->format('Y-m-28'));
                    }
                    else
                    {
                        array_push($check_interest_duplicates, Carbon::parse($value)->format('Y-m-28'));
                    }
                }
            }

            if ($this->hasDupes($check_month) || $this->hasDupes($check_interest_duplicates))
            {
                return response()->json(array(
                    'errors' => ['general' => 'You have submitted duplicate months. Kindly check and retry']
                ));
            }
            if (count($new) > 1)
            {
                $result = $this
                ->modification_repo
                ->updateEditedRequest($modification_request,$request->all(), $request);
                return $result ? response()->json(array(
                    'success' => true,
                    'request_id' => $result->id
                )) : response()
                ->json(array(
                    'errors' => ['general' => 'Something went wrong. Kindly retry']
                ));
            }
            else
            {
                return response()
                ->json(array(
                    'errors' => ['number_of_fields' => 'Kindly add month(s) to redistribute the receipted amount']
                ));
            }
            break;

            default:
            throw new GeneralException('Nothing selected');
            break;
        }
    }
    else
    {
        throw new GeneralException('Request not allowed');
    }
}

public function cancelRequest(Request $request)
{
   if (request()->ajax())
   {
    $rules = ['cancel_reason' => 'required'];
    $message = ['cancel_reason.*' => 'Cancel reason is required'];
    $validator = Validator::make(Input::all() , $rules, $message);
    if ($validator->fails())
    {
        return response()
        ->json(array(
            'errors' => $validator->getMessageBag()
            ->toArray()
        ));
    }

    $success = $this->modification_repo->cancelModificationRequest($request->all()) ? true: false;
    return response()->json(array('success' => $success));
}else{
    throw new GeneralException('Request not allowed');
}
}



}

