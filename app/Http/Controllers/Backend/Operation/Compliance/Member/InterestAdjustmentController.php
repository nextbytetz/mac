<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Requests\Backend\Operation\Compliance\InterestAdjustmentRequest;
use App\Models\Finance\Receipt\Receipt;
use App\Models\Finance\Receivable\BookingInterestRefund;
use App\Models\Finance\Receivable\InterestAdjustment;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\Receivable\InterestAdjustmentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleGroupRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class InterestAdjustmentController extends Controller
{

    protected $interest_adj_repo;

    public function __construct() {
        $this->interest_adj_repo = new InterestAdjustmentRepository();

    }
    public function wfModuleGroupId()
    {
        return $this->interest_adj_repo->wfModuleGroupId();
    }

    /**
     * @param Receipt $receipt
     * Create Interest Adjustment
     */
    public function create(Receipt $receipt){
        access()->hasWorkflowDefinition($this->wfModuleGroupId(), 1);
        $employer = $receipt->employer;
        $reason_types =(new CodeValueRepository())->query()->where('code_id', 61)->orderBy('name')->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/receivable/interest/adjustment/create')
            ->with('receipt', $receipt)
            ->with('employer', $employer)
            ->with('reason_types', $reason_types);
    }


    /**
     * @param InterestAdjustmentRequest $request
     * @return mixed
     * Store new interest adjustment
     */
    public function store(InterestAdjustmentRequest $request)
    {
        $input =$request->all();
        $interest_adjust = $this->interest_adj_repo->store($input);
        return redirect()->route('backend.compliance.interest_adjustment.profile', $interest_adjust->id)->withFlashSuccess('Success, Interest adjustment has been created');
    }

    /**
     * @param Receipt $receipt
     * @return mixed
     * Edit interest adjust
     */
    public function edit(InterestAdjustment $interestAdjustment){
        $receipt = $interestAdjustment->receipt;
        $reason_types =(new CodeValueRepository())->query()->where('code_id', 61)->orderBy('name')->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/receivable/interest/adjustment/edit')
            ->with('interest_adjustment', $interestAdjustment)
            ->with('receipt', $receipt)
            ->with('employer', $receipt->employer)
            ->with('reason_types', $reason_types);
    }

    public function update(InterestAdjustment $interestAdjustment, InterestAdjustmentRequest $request)
    {
        $input =$request->all();
        $interest_adjust = $this->interest_adj_repo->update($interestAdjustment, $input);
        return redirect()->route('backend.compliance.interest_adjustment.profile', $interest_adjust->id)->withFlashSuccess('Success, Interest adjustment has been updated');
    }

    public function profile(InterestAdjustment $interestAdjustment,  WorkflowTrackDataTable $workflowTrack){
        $receipt = $interestAdjustment->receipt;

        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->interest_adj_repo->getWfModuleType($interestAdjustment);
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $interestAdjustment->id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        $workflow_input = ['resource_id' => $interestAdjustment->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $type];
        $current_wf_track = $workflow->currentWfTrack();
        /*end workflow*/

        /*Start:level for letter*/
        $check_if_can_initiate_wf_letter = $this->interest_adj_repo->checkIfCanInitiateLetterWf($current_wf_track);
        /*docs*/

        $docs_attached = $this->interest_adj_repo->getDocsAttachedForAdjustment($interestAdjustment->id);
        $document_data = [
            'attach_route' => '',
            'doc_table' => 'document_employer'
        ];

        $pending_tasks = $this->interest_adj_repo->getPendingTasksBeforeInitiate($interestAdjustment);

        return view('backend/operation/compliance/member/employer/receivable/interest/adjustment/profile/profile')
            ->with("interest_adjustment",$interestAdjustment)
            ->with("workflow_track", $workflowTrack)
            ->with("workflow_input", $workflow_input)
            ->with("check_pending_level1", $check_pending_level1)
            ->with("check_workflow", $check_workflow)
            ->with("docs_attached", $docs_attached)
            ->with("check_if_can_initiate_wf_letter", $check_if_can_initiate_wf_letter)
            ->with('document_data', $document_data)
            ->with('pending_tasks', $pending_tasks)
            ->with('receipt', $receipt);
    }


    /*Initiate workflow*/
    public function initiateApproval(InterestAdjustment $interestAdjustment)
    {
        $input = request()->all();
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->interest_adj_repo->getWfModuleType($interestAdjustment);
        access()->hasWorkflowModuleDefinition($wf_module_group_id,$type, 1);
//        $this->employer_closure_open_repo->checkIfCanInitiateApproval($employerClosureReopen->id);
        /*workflow start*/
        $type = $this->interest_adj_repo->getWfModuleType($interestAdjustment);

        DB::transaction(function () use ($wf_module_group_id, $interestAdjustment,$input, $type) {
            $interest_adj_id = $interestAdjustment->id;
            /*initiate*/
            $this->interest_adj_repo->initiateApproval($interest_adj_id);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $interest_adj_id, 'type' => $type], [], ['comments' => $input['comments']]));
        });
        /*end workflow*/
        return redirect()->route('backend.compliance.interest_adjustment.profile',$interestAdjustment->id)->withFlashSuccess('Success, Workflow approval has been initiated');
    }

    /**
     * @param InterestAdjustment $interestAdjustment
     * @return mixed
     * Undo interest adjustment
     */
    public function undo(InterestAdjustment $interestAdjustment)
    {
        $receipt_id = $interestAdjustment->receipt_id;
        $this->interest_adj_repo->undoInterestAdjustment($interestAdjustment);
        return redirect()->route('backend.finance.receipt.profile_by_request', $receipt_id)->withFlashSuccess('Success, Interest adjustment has been undone');
    }

    /**
     * Get Interest Adjusted
     */
    public function getInterestAdjustedForDt($interest_adjustment_id)
    {
        $interest_adj = $this->interest_adj_repo->find($interest_adjustment_id);
        $new_payment_date = $interest_adj->new_payment_date;
        $payment_date = $interest_adj->payment_date;

        $result_list = $this->interest_adj_repo->getInterestAdjustedForDt($interest_adjustment_id)->orderBy('miss_month');
        return DataTables::of($result_list)
            ->addIndexColumn()
            ->editColumn('amount', function ($query) use($payment_date) {
                return number_2_format($query->getNewInterestDetailsByNewPayDateAttribute($payment_date)['interest_amount']);
            })
            ->editColumn('late_months', function ($query) use($payment_date) {
                return ($query->getNewInterestDetailsByNewPayDateAttribute($payment_date)['late_months']);
            })
            ->addColumn('new_late_months', function ($query) use($new_payment_date) {
                return $query->getNewInterestDetailsByNewPayDateAttribute($new_payment_date)['late_months'];
            })
            ->addColumn('new_interest', function ($query) use($new_payment_date) {
                return number_2_format($query->getNewInterestDetailsByNewPayDateAttribute($new_payment_date)['interest_amount']);
            })
            ->rawColumns([''])
            ->make(true);
    }

}
