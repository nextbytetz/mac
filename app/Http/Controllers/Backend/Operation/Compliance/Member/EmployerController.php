<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\DataTables\Compliance\Employer\InactiveReceiptsDataTable;
use App\DataTables\Compliance\Member\EmployerAdvancePaymentDataTable;
use App\DataTables\Compliance\Member\EmployerIncidentDataTable;
use App\DataTables\Compliance\Member\OnlineVerificationDataTable;
use App\DataTables\WorkflowTrackDataTable;
use App\Events\ApproveWorkflow;
use App\Events\NewWorkflow;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Finance\Receipt\ChooseEmployerRequest;
use App\Http\Requests\Backend\Operation\Compliance\BookingAdjustRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerClosureRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerEditGenInfoRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerOpenRequest;
use App\Http\Requests\Backend\Operation\Compliance\UpdateEmployerRegistrationRequest;
use App\Http\Requests\Backend\Operation\Compliance\UploadMergeEmployer;
use App\Http\Requests\Backend\Operation\Compliance\UploadUnMergeEmployer;
use App\Http\Requests\Backend\Operation\Compliance\WriteOffInterestRequest;
// use App\Http\Requests\Request;
use App\Models\Operation\Claim\NotificationReport;
use App\Models\Operation\Claim\PortalIncident;
use App\Models\Operation\Compliance\Member\EmployerAdvancePayment;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\Portal\ClaimRepository;
use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Http\Request;
use App\Jobs\Compliance\LoadMergeEmployers;
use App\Jobs\Compliance\LoadUnmergeEmployer;
use App\Jobs\RegisterEmployeeList;
use App\Models\Api\ClosedBusiness;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\OnlineEmployerVerification;
use App\Repositories\Backend\Api\ClosedBusinessRepository;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Claim\DocumentGroupRepository;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\RegisteredUnregisteredEmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Services\Scopes\IsApprovedScope;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Http\Requests\Backend\Operation\Compliance\InterestAdjustRequest;
use App\Repositories\Backend\Finance\Receivable\InterestWriteOffRepository;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\DataTables\Report\Compliance\LargeContributorsOnlineDatatable;
use App\DataTables\Report\Compliance\LargeContributorsCertificatesDatatable;
use Validator;
use Log;
use App\Models\Finance\Bill;

class EmployerController extends Controller
{

    use FileHandler, AttachmentHandler;

    protected $receipts;
    protected $employers;
    protected $receipt_codes;
    protected $booking_interests;
    protected $interest_write_offs;
    protected $fin_code_groups;
    protected $bookings;
    protected $wf_tracks;
    protected $countries;
    protected $regions;
    protected $districts;
    protected $location_types;
    protected $code_values;
    protected $document_groups;
    /* start : Handling Documents */
    protected $base = null;
    protected $documents = null;
    protected $uploadedDocuments = null;
    protected $onlineFolder = null;

    /* end : Handling Documents */
    //ADES start here
    protected $date_ranges;

    //ADES ends here

    public function __construct(EmployerRepository $employers, ReceiptCodeRepository $receipt_codes, BookingInterestRepository $booking_interest, InterestWriteOffRepository $interest_write_offs)
    {
        $this->employers = $employers;
        $this->receipt_codes = $receipt_codes;
        $this->booking_interests = $booking_interest;
        $this->interest_write_offs = $interest_write_offs;
        $this->fin_code_groups = new FinCodeGroupRepository();
        $this->bookings = new BookingRepository();
        $this->wf_tracks = new WfTrackRepository();
        $this->countries = new CountryRepository();
        $this->regions = new RegionRepository();
        $this->districts = new DistrictRepository();
        $this->location_types = new LocationTypeRepository();
        $this->code_values = new CodeValueRepository();
        $this->document_groups = new DocumentGroupRepository();
        //ADES starts here
        $this->date_ranges = new DateRangeRepository();
        //ADES ends here
        $this->middleware('access.routeNeedsPermission:view_ades_services', ['only' => ['closedBusiness', 'newEmployer', 'payesdl', 'employerStatus']]);
        $this->middleware('access.routeNeedsPermission:close_business', ['only' => ['getCloseBusiness']]);
        $this->middleware('access.routeNeedsPermission:open_business', ['only' => ['getOpenBusiness']]);
        //associateEmployers
        $this->middleware('access.routeNeedsPermission:merge_employer', ['only' => ['mergeEmployers', 'unMergeEmployers']]);
        $this->middleware('access.routeNeedsPermission:merge_online_notification', ['only' => ['postMergeIncident']]);
    }

    public function index()
    {
        //
        return view('backend.operation.compliance.member.employer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function edit($id)
    {
        //
        $wf_module_group_id = 6;
        //access()->hasWorkflowDefinition($wf_module_group_id, 1);
        $employer = $this->employers->findOrThrowException($id);
        $wfModule = $employer->wfModule;
        access()->hasWorkflowModuleDefinition($wfModule->wf_module_group_id, $wfModule->type, 1);
        $employers = $this->employers->query()->where("id", $employer->parent_id)->withoutGlobalScopes([IsApprovedScope::class])->get()->pluck('name', 'id');
        //$employers = [];
        return view('backend.operation.compliance.member.employer.registration.edit')
            ->with("employers", $employers)
            ->with("countries", $this->countries->getAll()->pluck('name', 'id'))
            ->with("location_types", $this->location_types->getAll()->pluck('name', 'id'))
            ->with("districts", $this->districts->getAll()->pluck('name', 'id'))
            ->with("regions", $this->regions->getAll()->pluck('name', 'id'))
            ->with("employer_categories", $this->code_values->query()->where('code_id', 4)->where(function ($query) {
                $query->where('reference', 'ECPUB')->orWhere('reference', 'ECPRI');
            })->get()->pluck('name', 'id'))
            ->with("employee_categories", $this->code_values->query()->where('code_id', 7)->get())
            ->with("business_sectors", $this->code_values->query()->where('code_id', 5)->get()->pluck('name', 'id'))
            ->with("documents", $this->document_groups->query()->where('id', 11)->first()->documents)
            ->with("employer", $employer);

    }


    /*Modify only information which not in particular change*/
    public function editGeneralInfo(Employer $employer)
    {
        $employers = $this->employers->query()->where("id", $employer->parent_id)->withoutGlobalScopes([IsApprovedScope::class])->get()->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/registration/edit_gen_info')
            ->with('employer', $employer)
            ->with("employer_categories", $this->code_values->query()->where('code_id', 4)->where(function ($query) {
                $query->where('reference', 'ECPUB')->orWhere('reference', 'ECPRI');
            })->get()->pluck('name', 'id'))
            ->with("business_sectors", $this->code_values->query()->where('code_id', 5)->get()->pluck('name', 'id'))
            ->with('employers', $employers);
    }

    /*Modify only information which not in particular change*/
    public function updateGeneralInfo( EmployerEditGenInfoRequest $request)
    {

        $input = $request->all();
        $employer = $this->employers->find($input['employer_id']);
        $this->employers->updateGeneralInfo($employer, $input);
        return redirect('compliance/employer/profile/' . $employer->id . '#general')->withFlashSuccess('Success, Employer info have been updated');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployerRegistrationRequest $request, $id)
    {
        //

        $employer = $this->employers->modifyRegister($id, $request->all());
        $user_id = access()->id();
        $employee_document_id = 37;
        $document_file = 'document_file' . $employee_document_id;

//        if (request()->hasFile($document_file)) {
//                 dispatch(new RegisterEmployeeList($employer, $user_id));
//        }
        return redirect('compliance/employer/profile/' . $employer->id . '#general')->withFlashSuccess(trans('alerts.backend.member.employer_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Employer Profile
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function profile($id)
    {
        $employer = $this->employers->findOrThrowException($id);
        $interest_summary = $this->employers->interestPaymentSummary($id);
        $booking_summary = $this->employers->bookingPaymentSummary($id);
        $member_count = $this->employers->getMemberCount($id);
        return view('backend.operation.compliance.member.employer.profile')
            ->with('employer', $employer)
            ->with('interest_summary', $interest_summary)
            ->with('booking_summary', $booking_summary)
            ->with('member_count', $member_count);
    }

    public function agentProfile($id)
    {
        $agents = $this->employers->getAgents($id);
        $agent_summary = $this->employers->getAgents($id)->first();
        return view('backend.operation.compliance.member.agent.agent_profile')->with(compact('agents', 'agent_summary'));
    }

    public function getRegisterEmployers()
    {
        $input = request()->all();
        return $this->employers->getRegisterEmployers($input['q'], $input['page']);
    }

    public function getNotificationRegisterEmployers()
    {
        $input = request()->all();
        return $this->employers->getNotificationRegisterEmployers($input['q'], $input['employee'], $input['page']);
    }

    public function getRegisterEmployersTin()
    {
        return $this->employers->getRegisterEmployersTin(request()->input('q'));
    }

    public function getAllStoredEmployers()
    {
        $repo = new RegisteredUnregisteredEmployerRepository();
        return $repo->getAllStoredEmployers(request()->input('q'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Get all employers without Scope
     */
    public function getAllEmployers()
    {
        return $this->employers->getAllEmployers(request()->input('q'));
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     * Employers with already approved notification report
     */
    public function getBeneficiaryEmployers()
    {
        return $this->employers->getBeneficiaryEmployers(request()->input('q'));
    }


    /**
     * Get All Active Employers for dataTable
     * @return mixed
     * @throws \Exception
     */
    public function get()
    {

        return Datatables::of($this->employers->getForDataTable())
            ->addColumn('name_formatted', function ($employer) {
                return $employer->name_formatted;
            })
            ->addColumn('doc_formatted', function ($employer) {
                return $employer->date_of_commencement_formatted;
            })
            ->addColumn('country', function ($employer) {
                return (($employer->district()->count()) ? (($employer->district->region()->count()) ? $employer->district->region->country->name : ' ') : (($employer->region()->count()) ? $employer->region->country->name : ' '));
            })
            ->addColumn('region', function ($employer) {
                return ($employer->district()->count()) ? (($employer->district->region()->count()) ? $employer->district->region->name : ' ') : (($employer->region()->count()) ? $employer->region->name : ' ');
            })
            ->make(true);
    }


    /**
     * @param $id
     * Get All employees registered of this employer
     */
    public function getRegisteredEmployees($id)
    {
        return Datatables::of($this->employers->getRegisteredEmployeesForDataTable($id))
//
            ->make(true);
    }




    /**
     * @param $id
     * @return mixed
     * Update Documents
     */


    /*
     * BOOKING ===================================
     *
     */

//    Get booking for DataTable for this employer

    public function getBookingsForDataTable($id)
    {
        return Datatables::of($this->employers->getBookings($id))
            ->addColumn('rcv_date_formatted', function ($booking) {
                return $booking->rcv_date_formatted;
            })
            ->editColumn('created_at', function ($booking) {
                return $booking->created_at_formatted;
            })
            ->editColumn('amount', function ($booking) {
                return $booking->amount_comma;
            })
            ->addColumn('paid_amount', function ($booking) {
                return number_2_format($booking->total_amount_paid);
            })
//            ->addColumn('paid_amount', function ($booking) {
//                return ($this->receipt_codes->query()->where('booking_id', $booking->id)->whereHas('receipt', function ($query) {
//                    $query->where('iscancelled', '=', 0);
//                })->whereHas('finCode', function ($query) {
//                    $query->where('fin_code_group_id', '=', $this->fin_code_groups->getContributionFinCodeGroupId());
//                })->count()) ? number_format(($this->receipt_codes->query()->where('booking_id', $booking->id)->whereHas('receipt', function ($query) {
//                    $query->where('iscancelled', '=', 0);
//                })->whereHas('finCode', function ($query) {
//                    $query->where('fin_code_group_id', '=', $this->fin_code_groups->getContributionFinCodeGroupId());
//                })->where('grouppaystatus',0)->sum('amount')), 2, '.', ',') : 0;
//            })
            ->make(true);
    }

    /*Get booking for missing months arrears*/
    public function getBookingsForMissingMonthsForDataTable($id)
    {
        return Datatables::of($this->employers->getBookingsForMissingMonthsForDataTable($id))
            ->addColumn('rcv_date_formatted', function ($booking) {
                return short_date_format($booking->rcv_date);
            })
            ->editColumn('booked_amount', function ($booking) {
                return number_2_format($booking->booked_amount);
            })

            ->make(true);
    }


    //End ============Booking --------------------------------


    /*
     *
     * CONTRIBUTIONS==========================================
     */

    /*
     * Get contribution for this  for dataTable
     */
    public function getContributionsForDataTable($id)
    {
        return Datatables::of($this->employers->getAllContributions($id))
            ->addColumn('contrib_month_formatted', function ($receipt_code) {
                return $receipt_code->contrib_month_formatted;
            })
            ->editColumn('receipt_id', function ($receipt_code) {
                return ($receipt_code->receipt()->count()) ? $receipt_code->receipt->rctno : ' ';
            })
            ->editColumn('amount', function ($receipt_code) {
                return $receipt_code->amount_comma;
            })
            ->addColumn('rct_date', function ($receipt_code) {
                return ($receipt_code->receipt()->count()) ? $receipt_code->receipt->rct_date : ' ';
            })
            ->addColumn('status', function ($receipt_code) {
                return $receipt_code->linked_file_status_label;
            })
            ->addColumn('action', function ($receipt_code) {
                return $receipt_code->action_buttons;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }


    /*
 * Get contribution for this  for dataTable
 */
    public function getLegacyContributionsForDataTable($id)
    {
        return Datatables::of($this->employers->getAllLegacyMergedContributions($id))
            ->addColumn('contrib_month_formatted', function ($legacyContrib) {
                return $legacyContrib->contrib_month_formatted;
            })
            ->editColumn('amount', function ($legacyContrib) {
                return $legacyContrib->amount_formatted;
            })
            ->addColumn('receipt_date', function ($legacyContrib) {
                return $legacyContrib->rct_date_formatted;
            })
            ->make(true);
    }


    public function getEmployerBills($employer)
    {

        $employer_bills = DB::table('portal.payments')
            ->join('portal.bills', 'payments.bill_id', '=', 'bills.bill_no')
            ->join('portal.users', 'bills.user_id', '=', 'users.id')
            ->distinct('control_no')
            ->where('employer_id', '=', $employer);
        return Datatables::of($employer_bills)
            ->editColumn('expire_date', function ($data) {
                return Carbon::parse($data->expire_date)->format('d-M-Y');

            })
            ->editColumn('bill_amount', function ($data) {
                return number_format($data->bill_amount,2);
            })
            ->editColumn('bill_status', function ($data) {
                if ($data->bill_status == 2 && $data->expire_date < Carbon::now()) {
                    return '<span class="btn btn-warning btn-sm"> Expired </span>';

                } elseif ($data->bill_status == 4) {

                    return '<span class="btn btn-danger btn-sm"> Cancelled </span>';
                } elseif ($data->bill_status == 2 && $data->expire_date > Carbon::now()) {
                    return '<span class="btn btn-info btn-sm"> Pending </span>';
                } elseif ($data->bill_status == 3) {
                    return '<span class="btn btn-success btn-sm">Paid</span>';
                }
            })->addColumn('generated_by', function ($data) {
                if ($data->bill_source == 'MAC') {
                    $mac_user = DB::table('main.users')->find($data->user_id);
                    if (count($mac_user)) {
                        return ucwords(strtolower($mac_user->firstname).' '.strtolower($mac_user->lastname));
                    } else {
                        return ' - 0' . substr($data->mobile_number, 4);
                    }
                }else{
                    return ucwords(strtolower($data->firstname)) . ' - 0' . substr($data->mobile_number, 4);
                }
            })->addColumn('action', function ($data) {
                if ($data->bill_status == 3) {
                    return '<a href="' . route("backend.compliance.print.employee.bills",
                            ['bill_id' => $data->bill_id, 'employer_id' => $data->employer_id]) . '" class="btn btn-primary btn-sm active"><i class="glyphicon glyphicon-edit"></i>Print Employees</a>';
                }
            })->rawColumns(['bill_status', 'action'])
            ->make(true);

    }


    public function printEmployeeBills($bill_id, $employer_id)
    {
        $employer = $this->employers->findOrThrowException($employer_id);
        $employees = DB::table('portal.bill_employee')
            ->select('employee_uploads.memberno', DB::raw("CONCAT(employee_uploads.firstname,' ',employee_uploads.middlename,' ',employee_uploads.lastname) as employee_name"), 'employee_uploads.dob as date_of_birth', 'employee_uploads.sex as gender', 'employee_uploads.job_title', 'employee_uploads.emp_cate as employment_category', 'bill_employee.basicpay', 'bill_employee.grosspay')
            ->join('main.employees', 'bill_employee.employee_id', '=', 'employees.id')
            ->join('portal.employee_uploads', 'employees.memberno', '=', 'employee_uploads.memberno')
            ->where('bill_id', '=', $bill_id)
            ->get();
        /*Print list of employees for Work permit*/
        return view("backend/operation/compliance/member/employer/includes/print_employees_work_permit")
            ->with('employees', $employees)
            ->withEmployer($employer);
    }


    public
    function getEmployerNotificationUsers($employer)
    {
        return Datatables::of((new ClaimRepository())->getUsersForEmployer($employer))
            ->addColumn("confirmed_formatted", function ($query) {
                return $query->confirmed_formatted;
            })
            ->addColumn("validated_formatted", function ($query) {
                return $query->validated_formatted;
            })
            ->addColumn("active_formatted", function ($query) {
                return $query->active_formatted;
            })
            ->addColumn("last_login_formatted", function ($query) {
                return $query->last_login_formatted;
            })
            ->addColumn('action', function ($query) {
                $b = '<button class="btn btn-success btn-sm btnResetPassword" role="button" data-user="' . $query->id . '" data-user_name="' . $query->name_formatted . '">Reset Password</button>';
                $b .= '&nbsp;<a class="btn btn-secondary btn-sm" href="' . route("backend.claim.notification_report.online_account_validation_profile", $query->id) . '">Profile</a>';
                $b .= ($query->isactive == true) ?
                    '&nbsp;<button class="btn btn-danger btn-sm btndeactivateNotification" role="button" data-user="' . $query->id . '" data-employer="' . $query->resource_id . '">Deactivate</button>' : '&nbsp;<button class="btn btn-info btn-sm btnactivateNotification" role="button" data-user="' . $query->id . '" data-employer="' . $query->resource_id . '">Activate</button>';

                return $b;
            })
            ->rawColumns(['confirmed_formatted', 'validated_formatted', 'active_formatted', 'action'])
            ->make(true);
    }

    public
    function deactivateNotificationUserform($user)
    {

        $user_details = DB::table('portal.claims')
            ->where('id', '=', $user)
            ->first();
        $employer_user = $this->employers->findOrThrowException($user_details->resource_id);

        return response()->json(array('onlineusername' => $user_details->firstname . ' ' . $user_details->firstname , 'email' => $user_details->email, 'phone' => $user_details->phone, 'employername' => $employer_user->name, 'employer_id' => $employer_user->id, 'user' => $user_details->id));

    }

    public function deactivateNotificationUser($user, Request $request)
    {
        $deactivate_user_account = DB::table('portal.claims')
            ->where('id', '=', $request->user_id)
            ->update(['isactive' => 0, 'mac_deactivated_by' => access()->id(),
                'mac_deactivated_at' => Carbon::now(), 'deactivate_reason' => $request->notificationreason]);
        return response()->json(array('success' => true));
    }


    public
    function getEmployerUsers($employer)
    {
        $employer_users = DB::table('portal.users')
            ->join('portal.employer_user', 'users.id', '=', 'employer_user.user_id')
            ->where('employer_id', '=', $employer);

        return Datatables::of($employer_users)->editColumn('user_type_cv_id', function ($data) {
            if ($data->user_type_cv_id == 1) {
                return '<span class="alert alert-light">Authorised</span>';

            } elseif ($data->user_type_cv_id == 2) {

                return '<span class="alert alert-light">Agent</span>';
            }


        })->editColumn('status', function ($data) {
            return ($data->isactive == true && $data->ismanage == true) ? '<span class="btn btn-success btn-sm">Active</span>' : '<span class="btn btn-danger btn-sm">Inactive</span>';
        })
            ->editColumn('name', function ($data) {
                return ucwords(strtolower($data->name));


            })
            ->editColumn('phone', function ($data) {
                return '0' . substr($data->phone, 4);

            })
            ->editColumn('last_login', function ($data) {
                return isset($data->last_login) ? Carbon::parse($data->last_login)->format('M d, Y H:i A') : '';

            })
            ->addColumn('action', function ($data) {
                $b = '<button class="btn btn-success btn-sm btnResetPassword" role="button" data-user="' . $data->user_id . '">Reset Password</button>
        <button class="btn btn-primary btn-sm btnchange" role="button" data-user="' . $data->user_id . '" data-employer="' . $data->employer_id . '">Change</button>';

                return ($data->isactive == true && $data->ismanage == true) ?
                    $b . ' <button class="btn btn-danger btn-sm btndeactivate" role="button" data-user="' . $data->user_id . '" data-employer="' . $data->employer_id . '">Deactivate</button>' : $b . ' <a href="#" class="btn btn-info btn-sm">Activate</a>';
            })
            ->rawColumns(['user_type_cv_id', 'status', 'action'])
            ->make(true);

    }


    /**
     *  LOAD CONTRIBUTION INTO SYSTEM + ADD NEW EMPLOYEES IF DO'NT EXIST=======
     *
     */


    //---end load ---contribution ----------------------------


    /**
     * Get inactive receipts for this  contributions
     */
    public
    function getInactiveReceiptsForDataTable($id)
    {
        $payment_type = new PaymentTypeRepository();
        return Datatables::of($this->employers->getInactiveReceipts($id))
            ->addColumn('payment_type', function ($receipt) use ($payment_type) {
                return ($receipt->payment_type_id == $payment_type->chequePayment()) ? $receipt->payment_type->name . ' - ' . $receipt->chequeno : $receipt->payment_type->name;
            })
            ->editColumn('amount', function ($receipt) {
                return $receipt->amount_comma;
            })
            ->editColumn('created_at', function ($receipt) {
                return $receipt->created_at_formatted;
            })
            ->editColumn('rct_date', function ($receipt) {
                return $receipt->rct_date_formatted;
            })
            ->addColumn('status', function ($receipt) {
                return $receipt->status_label;
            })
            ->rawColumns(['status'])
            ->make(true);
    }


    //END Contribution -------------------------------------

    /*
     * BOOKING INTERESTS ======================================
     */

    /*
 * Get all interests for this employer for dataTable
 */
    public
    function getInterestsForDataTable($id)
    {
        return Datatables::of($this->employers->getBookingInterests($id))
            ->addColumn('contrib_month_formatted', function ($booking_interest) {
                return $booking_interest->miss_month_formatted;
            })
            ->editColumn('amount', function ($booking_interest) {
                return $booking_interest->amount_formatted;
            })
            ->addcolumn('amount_paid', function ($booking_interest) {
                return number_format(($this->employers->getTotalInterestPaidForBookingId($booking_interest->booking_id)), 2, '.', ',');
            })
            ->addcolumn('remain_amount', function ($booking_interest) {
                return number_format(($booking_interest->amount - $this->employers->getTotalInterestPaidForBookingId($booking_interest->booking_id)), 2, '.', ',');
            })
            ->addColumn('status', function ($booking_interest) {
                return $booking_interest->contribution_status_label;
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    /*
* Get selected interest for this employer display on interest profile
*/
    public function viewSelectedInterest($booking_interest_id, $success_message = "", WorkflowTrackDataTable $workflowTrack)
    {
        /* Check workflow */
//        $wf_module_group_id = 2;
//        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $booking_interest_id]);
//        $check_workflow = $workflow->checkIfHasWorkflow();
//        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
//        $workflow_input = ['resource_id' => $booking_interest_id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => -1];
//        $current_wf_track = $workflow->currentWfTrack();
        /*end workflow*/
        $interest = $this->booking_interests->findOrThrowException($booking_interest_id);
        /*Start:level for letter*/
//        $check_if_can_initiate_wf_letter = $this->booking_interests->checkIfCanInitiateLetterWf($current_wf_track);
//        /*docs*/
        $docs_attached = $this->booking_interests->getDocsAttachedForAdjustment($booking_interest_id);

        return view('backend.operation.compliance.member.employer.interest_profile')
            ->with("booking_interest", $this->booking_interests->findOrThrowException($booking_interest_id))
            ->with("paid_amount", number_format($this->receipt_codes->getTotalInterestPaidForBookingId($interest->booking_id), 2, '.', ','))
            ->with("remain_amount", number_format(($interest->amount -
                $this->receipt_codes->getTotalInterestPaidForBookingId($interest->booking_id)), 2, '.', ','))
            ->with("contribution_paid_amount", number_format($this->receipt_codes->getTotalContributionPaidForBookingId($interest->booking_id), 2, '.', ','))
            ->with("first_paid_date", ($this->bookings->getFirstPaidReceipt($interest->booking_id)) ? $this->bookings->getFirstPaidReceipt($interest->booking_id)->rct_date_formatted : ' ')
//            ->with("workflowtrack", $workflowTrack)
//            ->with("workflow_input", $workflow_input)
//            ->with("check_pending_level1", $check_pending_level1)
//            ->with("check_workflow", $check_workflow)
            ->with("docs_attached", $docs_attached);
//            ->with("check_if_can_initiate_wf_letter", $check_if_can_initiate_wf_letter);
    }


    /*Complete wf for interest adjusted manually*/
    public function completeWfForInterestAdjustedManually($booking_interest_id)
    {
        access()->hasWorkflowDefinition(2,1);
        $this->booking_interests->completeWfForInterestAdjustedManually($booking_interest_id);
        return redirect()->back()->withFlashSuccess('Success, Workflow has been closed.');
    }

    //==End Booking Interest -----------------------------

    /*
     * BOOKING INTEREST REFUND =====================start============
     *
     */


    /* End of Booking Interest Refund ----------------------------*/


    /*
    * WRITE OFF EMPLOYER INTEREST =================================
    *
    */
    public
    function writtingOffInterestPage($id)
    {
        access()->hasWorkflowDefinition(1, 1);
        $employer = $this->employers->findOrThrowException($id);
        return view('backend.operation.compliance.member.employer.write_off_interest')
            ->withEmployer($employer);
    }

    public
    function writeOffInterest($id, WriteOffInterestRequest $request)
    {
        access()->hasWorkflowDefinition(1, 1);
        DB::transaction(function () use ($id, $request) {
            $interest_write_off = $this->employers->writeOffInterest($id, $request);
            //Initiate Workflow
            $check = workflow([['wf_module_group_id' => 1, 'resource_id' => $interest_write_off->id]])->checkIfHasWorkflow();
            if (!$check) {
                event(new NewWorkflow(['wf_module_group_id' => 1, 'resource_id' => $interest_write_off->id]));
            }
        });
        return redirect()->route('backend.compliance.employer.profile', $id)->withFlashSuccess(trans('alerts.backend.interest.write_off_interested'));
    }

    /**
     * @param $interest_write_off_id
     * @return mixed
     * Modify interest writte off when is at level 1 pending
     */

    public
    function writtenOffInterestsEdit($interest_write_off_id)
    {
        access()->hasWorkflowDefinition(1, 1);
        workflow([['wf_module_group_id' => 1, 'resource_id' => $interest_write_off_id]])->checkIfCanInitiateAction(1);
        $employer = $this->employers->getWriteOffEmployer($interest_write_off_id);
        $interest_write_off = $this->interest_write_offs->findOrThrowException($interest_write_off_id);
        return view('backend.operation.compliance.member.employer.write_off_interest_edit')
            ->withEmployer($employer)
            ->withInterestWriteOff($interest_write_off);
    }

    public
    function writtenOffInterestsUpdate($interest_write_off_id, WriteOffInterestRequest $request)
    {
        $employer = $this->employers->getWriteOffEmployer($interest_write_off_id);
        $id = $employer->id;
        DB::transaction(function () use ($id, $interest_write_off_id, $request) {
            $this->employers->checkIfDateRangeEligibleToBeWrittenOff($id, $request['min_date'], $request['max_date']);
            $interest_write_off = $this->booking_interests->updateWrittenOffInterest($id, $interest_write_off_id, $request);
        });
        return redirect()->route('backend.compliance.employer.profile', $employer->id)->withFlashSuccess(trans('alerts.backend.interest.interest_written_off_updated'));
    }

    // Get Interest written offs for this Employer for this write_off_id
    public
    function getInterestsWrittenOffForDataTable($interest_write_off_id)
    {

        return Datatables::of($this->booking_interests->query()->withTrashed()->where('interest_write_off_id', '=',
            $interest_write_off_id)->get())
            ->editColumn('miss_month', function ($booking_interests) {
                return $booking_interests->miss_month_formatted;
            })
            ->editColumn('amount', function ($booking_interests) {
                return $booking_interests->amount_formatted;
            })
            ->make(true);
    }

    // Get Interest written offs Approval Page
    public
    function ApproveInterestWrittenOff($interest_write_off_id, WorkflowTrackDataTable $workflowTrack)
    {
        $employer = $this->employers->query()->whereHas("bookingInterests",
            function ($query) use ($interest_write_off_id) {
                $query->withTrashed()->where('interest_write_off_id', '=', $interest_write_off_id);
            })->first();

        $interest_write_off = $this->interest_write_offs->findOrThrowException($interest_write_off_id);
        return view('backend.operation.compliance.member.employer.write_off_interest_approve')
            ->withEmployer($employer)
            ->withInterestWriteOff($interest_write_off)
            ->withWorkflowtrack($workflowTrack);

    }


    // Get Interest written offs for this employer for datatable
    public
    function getInterestWrittenOffOverview($id)
    {
        return Datatables::of($this->employers->getInterestWriteOffs($id))
            ->addColumn('status', function ($interest_write_off) {
                return $this->employers->getInterestWriteOffIsCompleteLabel($interest_write_off->id);
            })
            ->addColumn('min_date', function ($interest_write_off) {
                return Carbon::parse($interest_write_off->bookingInterests->min('miss_month'))->format('d-M-Y');

            })
            ->addColumn('max_date', function ($interest_write_off) {
                return Carbon::parse($interest_write_off->bookingInterests->max('miss_month'))->format('d-M-Y');
            })
            ->addColumn('amount', function ($interest_write_off) {
                return number_format($interest_write_off->bookingInterests->sum('amount'), 2, '.', ',');

            })
            ->addColumn('total_months', function ($interest_write_off) {
                return $interest_write_off->bookingInterests()->count();
            })
            ->rawColumns(['status'])
            ->make(true);
    }


    // End Write off -----------------------------------------------


    /*
     *  INTEREST ADJUSTMENT ========================================
     */

//    OPEN interest adjustment page
    public function editInterest($booking_interest_id)
    {
        /*check for docs*/
        $this->booking_interests->validateOnDocumentsForAdjust($booking_interest_id);
        access()->hasWorkflowDefinition(2, 1);

        workflow([['wf_module_group_id' => 2, 'resource_id' => $booking_interest_id]])->checkIfCanInitiateAction(1);

        $interest = $this->booking_interests->findOrThrowException($booking_interest_id);
        $reason_types = $this->code_values->query()->where('code_id', 61)->orderBy('name')->pluck('name', 'id');
        return view('backend.operation.compliance.member.employer.adjust_interest')
            ->withBookingInterest($interest)
            ->with('reason_types', $reason_types);
    }

    public function adjustInterest($booking_interest_id, InterestAdjustRequest $request)
    {
        DB::transaction(function () use ($booking_interest_id, $request) {
            $this->employers->interestAdjustment($booking_interest_id, $request);
            $wf_group_module_id = 2;
            //Initiate Workflow
            $check = workflow([['wf_module_group_id' => 2, 'resource_id' => $booking_interest_id]])->checkIfHasWorkflow();
            if (!$check) {
                event(new NewWorkflow(['wf_module_group_id' => $wf_group_module_id, 'resource_id' => $booking_interest_id]));
            }
        });
        return redirect()->route('backend.compliance.employer.interest_profile', $booking_interest_id)->withFlashSuccess(trans('alerts.backend.interest.adjust_interested'));
    }

    // Get Interests adjusted for this employer for dataTable
    public
    function getInterestAdjustedOverview($id)
    {
        return Datatables::of($this->employers->getInterestAdjusted($id))
            ->addColumn('miss_month_formatted', function ($interest_adjusted) {
                return ($interest_adjusted->miss_month_formatted);
            })
            ->editColumn('amount', function ($interest_adjusted) {
                return ($interest_adjusted->amount_formatted);
            })
            ->editColumn('adjust_amount', function ($interest_adjusted) {
                return ($interest_adjusted->adjust_amount_formatted);
            })
            ->addColumn('status', function ($interest_adjusted) {
                return $this->booking_interests->getInterestAdjustLabel($interest_adjusted->id);
            })
            ->rawColumns(['status'])
            ->make(true);
    }


    /* Interest Overpayment after Interest Adjustment of Sep 2017 from Board  */
    public
    function getEmployerInterestOverpayment()
    {
        return Datatables::of($this->employers->getEmployerInterestOverpayment())
            ->addColumn('interest', function ($employer) {
                return number_2_format($this->booking_interests->getTotalEmployerInterest($employer->id));
            })
            ->addColumn('amount_paid', function ($employer) {
                return number_2_format($this->receipt_codes->getTotalInterestPaidForEmployer($employer->id));
            })
            ->addColumn('balance', function ($employer) {
                return number_2_format(($this->booking_interests->getTotalEmployerInterest($employer->id) - $this->receipt_codes->getTotalInterestPaidForEmployer($employer->id)));
            })
            ->make(true);
    }


    //===End Interest Adjustment ------------------------------------------------


    /**
     * BOOKING PROFILE  ===================================
     * Adjustment
     */

    /*
* Get selected interest for this employer display on interest profile
*/
    public
    function bookingProfile($booking_id, $success_message = "")
    {
        $booking = $this->bookings->findOrThrowException($booking_id);
        return view('backend.operation.compliance.member.employer.booking_profile')
            ->withBooking($this->bookings->findOrThrowException($booking_id))
            ->withPaidAmount(number_format($this->receipt_codes->getTotalContributionPaidForBookingId($booking_id), 2, '.', ','))
            ->withRemainAmount(number_format(($booking->amount -
                $this->receipt_codes->getTotalContributionPaidForBookingId($booking_id)), 2, '.', ','));

    }

    public
    function bookingAdjustPage($booking_id)
    {
        $booking = $this->bookings->findOrThrowException($booking_id);
        return view('backend.operation.compliance.member.employer.booking_adjust')
            ->withBooking($booking);
    }

    public
    function bookingAdjust($booking_id, BookingAdjustRequest $request)
    {
        $booking = $this->bookings->adjust($booking_id, $request->all());
        return redirect()->route('backend.compliance.employer.booking_profile', $booking_id)->withFlashSuccess(trans('alerts.backend.compliance.booking_adjusted'));
    }

    // --End booking profile

    /*
* Get Employee Counts for this  for dataTable
*/
    public
    function getEmployeeCountsForDataTable($id)
    {

        return Datatables::of($this->employers->getEmployeeCounts($id))
            ->addColumn('employee_category', function ($employeeCount) {
                return $employeeCount->employeeCategory->name;
            })
            ->addColumn('gender', function ($employeeCount) {
                return $employeeCount->gender->name;
            })
            ->make(true);
    }

    public
    function onlineVerification(Employer $employer, OnlineVerificationDataTable $dataTable)
    {
        return $dataTable->with('employer', $employer)->render('backend/operation/compliance/member/employer/verification', ['employer' => $employer]);
        //return view("backend/operation/compliance/member/employer/verification")->with("employer", $employer);
    }

    public
    function advancePayment(Employer $employer, EmployerAdvancePaymentDataTable $dataTable)
    {
        return $dataTable->with('employer', $employer)->render('backend/operation/compliance/member/employer/advance_payment', ['employer' => $employer]);
    }

    public
    function onlineIncident(Employer $employer, EmployerIncidentDataTable $dataTable)
    {
        return $dataTable->with('employer', $employer)->render('backend/operation/compliance/member/employer/incident', ['employer' => $employer]);
    }

    public
    function onlineVerificationProfile(OnlineEmployerVerification $verification, WorkflowTrackDataTable $workflowTrack)
    {
        return view("backend/operation/compliance/member/employer/verification_profile")
            ->with("verification", $verification)
            ->with("workflowtrack", $workflowTrack);
    }

    public
    function advancePaymentProfile(EmployerAdvancePayment $advance_payment, WorkflowTrackDataTable $workflowTrack)
    {
        return view("backend/operation/compliance/member/employer/advance_payment_profile")
            ->with("advance_payment", $advance_payment)
            ->with("workflowtrack", $workflowTrack);
    }


    /**
     * @param PortalIncident $incident
     * @param WorkflowTrackDataTable $workflowTrack
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
public function onlineIncidentProfile(PortalIncident $incident, WorkflowTrackDataTable $workflowTrack)
{
    $docRepo = new DocumentRepository();
    $codeValuePortalRepo = new CodeValuePortalRepository();
    switch ($incident->incident_staging_cv_id) {
        case $codeValuePortalRepo->ISUPSUPDOC(): //Pending Supporting Documents
            $incident->incident_staging_cv_id = $codeValuePortalRepo->ISUPSUDCRVWD(); //Pending Supporting Documents (Officer Reviewed)
            $incident->save();
            (new PortalIncidentRepository())->initialize($incident);
            break;
    }
    $docs = $docRepo->getOptionalIncidentDocuments($incident);
        //dd($docs->toArray());
        return view("backend/operation/compliance/member/employer/incident_profile")
            ->with("incident", $incident)
            ->with("workflowtrack", $workflowTrack)
            ->with("employer", $incident->employer)
            ->with("employee", $incident->employee)
            ->with("documents", $docs)
            ->with("opted_docs", $incident->additionalDocs()->pluck(pg_mac_main() . ".documents.id")->all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
public function onlineIncidentStageUpdate(Request $request)
{
    $input = $request->all();
    $incidentid = $input['incidentid'];
    $api_key = $input['api_key'];
    if (Hash::check(env("PORTAL_API_KEY"), $api_key)) {
        $incidentRepo = new PortalIncidentRepository();
        $incident = $incidentRepo->find($incidentid);
        $incidentRepo->initialize($incident);
    }
    return response()->json(['success' => true]);
}

    /**
     * @param PortalIncident $incident
     * @param $check
     * @param $documentId
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function onlineIncidentOptionalDocs(PortalIncident $incident, $check, $documentId)
    {
        $return = (new PortalIncidentRepository())->onlineIncidentOptionalDocs($incident, $check, $documentId);
        return response()->json(['success' => true]);
    }

    function onlineNotificationReportOptionalDocs(NotificationReport $incident, $check, $documentId)
    {
        $return = (new NotificationReportRepository())->onlineOptionalDocs($incident, $check, $documentId);
        return response()->json(['success' => true]);
    }

    public function postIncidentComment(PortalIncident $incident)
    {
        $input = request()->all();
        $query = (new PortalIncidentRepository())->postIncidentComment($incident, $input);
        return view("backend.operation.compliance.member.employer.incident_comment")->with("queries", $incident->queries());
    }

    /**
     * @param PortalIncident $incident
     * @return \Illuminate\Http\JsonResponse
     */
    public function postMergeIncident(PortalIncident $incident)
    {
        $input = request()->all();
        $return = (new PortalIncidentRepository())->postMergeIncident($incident, $input);
        return response()->json(['success' => true]);
    }

    /**
     * @param OnlineEmployerVerification $verification
     * @return \Illuminate\Http\JsonResponse
     * @throws GeneralException
     */
    public
    function showVerificationDocumentLibrary(OnlineEmployerVerification $verification)
    {
        $this->base = $this->real(employer_verification_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }
        $this->base = $this->base . DIRECTORY_SEPARATOR . $verification->id;
        $this->makeDirectory($this->base);
        $this->onlineFolder = $verification->pluck('id', 'id')->all();
        $this->documents = $verification->documents->pluck('name', 'id')->all();
        $uploadedDocuments = [];
        foreach ($verification->documents as $document) {
            $uploadedDocuments += [$document->id => $document->name];
        }
        $this->uploadedDocuments = $uploadedDocuments;
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_uploaded_online($node, (isset(request()->id) && request()->id === '#'));
        return response()->json($rslt);
    }

    /**
     * @param PortalIncident $incident
     * @return \Illuminate\Http\JsonResponse
     * @throws GeneralException
     */
    public
    function showIncidentDocumentLibrary(PortalIncident $incident)
    {
        $this->base = $this->real(incident_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }
        $this->base = $this->base . DIRECTORY_SEPARATOR . $incident->id;
        $this->makeDirectory($this->base);
        $this->onlineFolder = $incident->pluck('id', 'id')->all();
        $this->documents = $incident->documents->pluck('name', 'id')->all();
        $uploadedDocuments = [];
        foreach ($incident->documents as $document) {
            $uploadedDocuments += [$document->id => $document->name, "isverified_" . $document->id => $document->pivot->isverified, $document->pivot->id => $document->name, "isverified_" . $document->pivot->id => $document->pivot->isverified];
        }
        //logger($uploadedDocuments);
        $this->uploadedDocuments = $uploadedDocuments;
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_uploaded_online($node, (isset(request()->id) && request()->id === '#'), true);
        return response()->json($rslt);
    }

    public function showNotificationReportDocumentLibrary(NotificationReport $incident)
    {
        $this->base = $this->real(notification_report_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }
        $this->base = $this->base . DIRECTORY_SEPARATOR . $incident->id;
        $this->makeDirectory($this->base);
        $this->onlineFolder = $incident->pluck('id', 'id')->all();
        $this->documents = $incident->uploadedDocuments->pluck('name', 'id')->all();
        $uploadedDocuments = [];
        foreach ($incident->uploadedDocuments as $document) {
            //$uploadedDocuments += [$document->id => $document->name, "isverified_" . $document->id => $document->pivot->isverified, $document->pivot->id => $document->name];
            //$uploadedDocuments += [$document->id => $document->name, "isverified_" . $document->id => $document->pivot->isverified, $document->pivot->id => $document->name, "isverified_" . $document->pivot->id => $document->pivot->isverified];
            $uploadedDocuments += [$document->pivot->id => $document->name, "isverified_" . $document->pivot->id => $document->pivot->isverified];
        }
        //logger($uploadedDocuments);
        $this->uploadedDocuments = $uploadedDocuments;
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_uploaded_online($node, (isset(request()->id) && request()->id === '#'), true);
        //logger($rslt);
        return response()->json($rslt);
    }

    public
    function showAdvancePaymentDocumentLibrary(EmployerAdvancePayment $advance_payment)
    {
        $this->base = $this->real(employer_advance_payment_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }
        $this->base = $this->base . DIRECTORY_SEPARATOR . $advance_payment->id;
        $this->makeDirectory($this->base);
        $this->onlineFolder = $advance_payment->pluck('id', 'id')->all();
        $this->documents = $advance_payment->documents->pluck('name', 'id')->all();
        $uploadedDocuments = [];
        foreach ($advance_payment->documents as $document) {
            $uploadedDocuments += [$document->id => $document->name];
        }
        $this->uploadedDocuments = $uploadedDocuments;
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_uploaded_online($node, (isset(request()->id) && request()->id === '#'));
        return response()->json($rslt);
    }

    /**
     * @param OnlineEmployerVerification $verification
     * @param $documentId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public
    function downloadVerificationDocument(OnlineEmployerVerification $verification, $documentId)
    {
        $document = $verification->documents()->where("document_online_employer_verification.document_id", $documentId)->first();
        return response()->download($document->linkEmployerVerification($verification->id), $document->linkNameEmployerVerification($verification->id));
    }

    public
    function downloadAdvancePaymentDocument(EmployerAdvancePayment $advance_payment, $documentId)
    {
        $document = $advance_payment->documents()->where("document_employer_advance_payment.document_id", $documentId)->first();
        return response()->download($document->linkEmployerAdvancePayment($advance_payment->id), $document->linkNameEmployerAdvancePayment($advance_payment->id));
    }

    public
    function downloadIncidentDocument(PortalIncident $incident, $documentId)
    {
        $document = $incident->documents()->where(pg_mac_portal() . ".document_incident.document_id", $documentId)->first();
        return response()->download($document->linkIncident($incident->id), $document->linkNameIncident($incident->id));
    }

    public
    function openVerificationDocument(OnlineEmployerVerification $verification, $documentId)
    {
        $document = $verification->documents()->where("document_online_employer_verification.document_id", $documentId)->first();
        return response()->file($document->linkEmployerVerification($verification->id));
    }

    public
    function openAdvancePaymentDocument(EmployerAdvancePayment $advance_payment, $documentId)
    {
        $document = $advance_payment->documents()->where("document_employer_advance_payment.document_id", $documentId)->first();
        return response()->file($document->linkEmployerAdvancePayment($advance_payment->id));
    }

    public
    function openIncidentDocument(PortalIncident $incident, $documentId)
    {
        $document = $incident->documents()->where(pg_mac_portal() . ".document_incident.document_id", $documentId)->first();
        return response()->file($document->linkIncident($incident->id));
    }

    //ADES functions start here==============================================================================
    public
    function employerStatus()
    {
        $parsed_json = null;
        return view('backend.operation.compliance.member.ades.employer_status')->with('parsed_json', $parsed_json);
    }


    public
    function employerStatusShow()
    {
        $request = Request();
        $client = new Client();
        $request = $client->get('https://gateway.tra.go.tz/employerservice/api/EmployerStatus/' . $request->tin);
        $response = $request->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        //dd($parsed_json);
        if ($parsed_json['StatusCode'] == 0) {
            if ($parsed_json['Payload'] == null) {
                $parsed_json = null;
                return view('backend.operation.compliance.member.ades.employer_status')->with('error', 'The TIN you have entered does not exist')->with('parsed_json', $parsed_json);
            } else {
                echo "There was something wrong";
            }
        } else {
            //dd($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerName']);
            return view('backend.operation.compliance.member.ades.employer_status')->with('parsed_json', $parsed_json);

        }

    }

    public
    function payesdl()
    {
        $parsed_json = null;

        return view('backend.operation.compliance.member.ades.employer_payesdl')->with('parsed_json', $parsed_json);

    }

    public
    function payesdlShow()
    {

        $request = Request();
        $monthyear = str_replace("-", "/", $request->monthyear);
        $client = new Client();
        $request = $client->get('https://gateway.tra.go.tz/employerservice/api/employmenttax/' . $request->tin . '/' . $monthyear);
        $response = $request->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        //dd($parsed_json);
        if ($parsed_json['StatusCode'] == 0) {
            if ($parsed_json['Payload'] == null) {
                $parsed_json = null;
                return view('backend.operation.compliance.member.ades.employer_payesdl')->with('error', 'The TIN you have entered does not exist')->with('parsed_json', $parsed_json);
            } else {
                echo "There was something wrong";
            }
        } else {
            // dd($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerName']);
            return view('backend.operation.compliance.member.ades.employer_payesdl')->with('parsed_json', $parsed_json);

        }

    }

    public
    function newEmployer()
    {
        $parsed_json = null;
        return view('backend.operation.compliance.member.ades.employer_newregistered')->with('parsed_json', $parsed_json);

    }

    public
    function newEmployerShow()
    {
        $request = Request();
        $client = new Client();
        $guzzlerequest = $client->get('https://gateway.tra.go.tz/employerservice/api/newemployers/' . $request->first_date . '/' . $request->second_date);
        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        //dd($parsed_json);
        if ($parsed_json['StatusCode'] == 0) {
            if ($parsed_json['Payload'] == null) {
                $parsed_json = null;
                return view('backend.operation.compliance.member.ades.employer_newregistered')->with('error', 'No Taxpayer registered with the specified date range')->with('parsed_json', $parsed_json);
            } else {
                echo "There was something wrong";
            }
        } else {
            //dd($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerName']);
            return view('backend.operation.compliance.member.ades.employer_newregistered')
                ->with('parsed_json', $parsed_json)->with('date_one', $request->first_date)->with('date_two', $request->second_date);

        }

    }

    public
    function closedBusiness()
    {
        $parsed_json = null;
        return view('backend.operation.compliance.member.ades.closed_business')->with('parsed_json', $parsed_json);

    }

    public
    function closedBusinessShow()
    {
        $request = Request();
        $client = new Client();
        $guzzlerequest = $client->get('https://gateway.tra.go.tz/employerservice/api/deregisteredemployers/' . $request->first_date . '/' . $request->second_date);
        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        //dd($parsed_json);
        if ($parsed_json['StatusCode'] == 0) {
            if ($parsed_json['Payload'] == null) {
                $parsed_json = null;
                return view('backend.operation.compliance.member.ades.closed_business')->with('error', 'No closed business within the specified date range ')->with('parsed_json', $parsed_json);
            } else {
                echo "There was something wrong";
            }
        } else {
            //dd($parsed_json['Payload'][0]['GeneralInformation']['BasicInformation']['TaxpayerName']);
            return view('backend.operation.compliance.member.ades.closed_business')
                ->with('parsed_json', $parsed_json)->with('date_one', $request->first_date)->with('date_two', $request->second_date);

        }

    }

//End of ADES functions

    /* start : Employer Closure */

    public
    function getCloseBusiness(Employer $employer)
    {
        return view("backend/operation/compliance/member/employer/closure/close_business")
            ->with("employer", $employer);
    }

    public
    function postCloseBusiness(Employer $employer, EmployerClosureRequest $request)
    {
        $input = $request->all();
        $repo = new EmployerClosureRepository();
        $repo->postCloseBusiness($input);
        return response()->json(['success' => true, 'id' => $employer->id]);
    }

    public
    function getClosedBusinessTRA()
    {
        return view("backend/operation/compliance/member/employer/closure/tra/index");
    }

    public
    function getClosedBusinessTRAForDatatable()
    {
        $repo = new ClosedBusinessRepository();
        return Datatables::of($repo->getForDataTable())
            ->addColumn("name", function ($query) {
                return $query->name;
            })
            ->addColumn("deregistration_date", function ($query) {
                return $query->deregistrationdate_label;
            })
            ->make(true);
    }

    public
    function showClosedBusinessTRA(ClosedBusiness $closed_business)
    {
        return view("backend/operation/compliance/member/employer/closure/tra/show")
            ->with("closed_business", $closed_business);
    }

    public
    function associateClosedBusinessTRA(ClosedBusiness $closed_business, ChooseEmployerRequest $request)
    {
        $input = $request->all();
        $repo = new ClosedBusinessRepository();
        $repo->associateEmployer($closed_business, $input);
        return redirect()->route("backend.compliance.employer.close_business", $input['employer']);
    }

    public
    function deleteClosedBusinessTRA()
    {
        $repo = new ClosedBusinessRepository();
        $repo->deleteClosedBusinessTRA(request()->all());
        return redirect()->back()->withFlashSuccess("Success, Selected employers have been removed to the closed business list");
    }

    public
    function getOpenBusiness(Employer $employer)
    {
        $closure = $employer->getRecentEmployerClosure();
        return view("backend/operation/compliance/member/employer/closure/open_business")
            ->with("employer", $employer)
            ->with('closure', $closure);
    }

    public
    function postOpenBusiness(Employer $employer, EmployerOpenRequest $request)
    {
        $input = $request->all();
        $repo = new EmployerClosureRepository();
        $repo->postOpenBusiness($input);
        return redirect()->route("backend.compliance.employer.profile", $employer->id)->withFlashSuccess("Success, Employer business has been actived in the system");
    }

    public
    function mergeEmployers()
    {
        return view("backend/operation/compliance/member/employer/registration/associate");
    }

    public
    function postMergeEmployers(UploadMergeEmployer $request)
    {
        $user = access()->id();
        $return = $this->saveMergeEmployerFileAttachment();
        if ($return['return']) {
            dispatch(new LoadMergeEmployers($user, $return['filename']));
        }
        return response()->json(['success' => true]);
    }

    public
    function unMergeEmployers()
    {
        return view("backend/operation/compliance/member/employer/registration/unmerge");
    }

    public
    function postUnMergeEmployers(UploadUnMergeEmployer $request)
    {
        $user = access()->id();
        $return = $this->saveUnMergeEmployerFileAttachment();
        if ($return['return']) {
            dispatch(new LoadUnmergeEmployer($user, $return['filename']));
        }
        return response()->json(['success' => true]);
    }

    //Pending : Querying and displaying business closures of a particular employer

    /* end : Employer Closure */
    //Large contributors file start here==============================================================================

//Large contributors registered online
    public
    function viewLargeContributor(LargeContributorsOnlineDatatable $dataTable)
    {
        return $dataTable->render('backend.operation.compliance.member.employer.large_contributor.largecontributor');

        // return view('backend.operation.compliance.member.employer.large_contributor.largecontributor');
    }

    public
    function viewLargeContributorCertificates(LargeContributorsCertificatesDatatable $dataTable)
    {

        return $dataTable->render('backend.operation.compliance.member.employer.large_contributor.largecontributorcertificate');
        // return $dataTable->render('backend.finance.reconciliation.successfulltrx');
        // return view('backend.finance.reconciliation.successfulltrx');

    }

    /*End of Large contributors file*/








    /*Get dishonoured cheques by employer for datatable*/
    public function getDishonouredChequesByEmployerForDt($employer_id)
    {
        return Datatables::of($this->employers->getDishonouredChequesByEmployerForDt($employer_id))
            ->addColumn('rct_no', function ($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count()) ? $dishonoured_cheque->receiptWithTrashed->rctno : 0;
            })
            ->addColumn('employer', function($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count())?$dishonoured_cheque->receiptWithTrashed->payer :0;
            })
            ->addColumn('employer_regno', function($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count())?$dishonoured_cheque->receiptWithTrashed->employer->reg_no :0;
            })
            ->addColumn('status', function ($dishonoured_cheque) {
                return $dishonoured_cheque->dishonoured_cheque_status_label;
            })
            ->addColumn('amount_formatted', function ($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count()) ? number_format( $dishonoured_cheque->receiptWithTrashed->amount , 2 , '.' , ',' ) : 0;
            })
            ->addColumn('payment_date', function($dishonoured_cheque) {
                return $dishonoured_cheque->receiptWithTrashed->rct_date_formatted;
            })
            ->addColumn('receipt_date', function($dishonoured_cheque) {
                return $dishonoured_cheque->receiptWithTrashed->created_at_formatted;
            })
            ->addColumn('contrib_month', function($dishonoured_cheque) {
                return $dishonoured_cheque->receiptWithTrashed->receiptCodes()->first()->contrib_month_formatted;
            })
            ->editColumn('old_bank_id', function($dishonoured_cheque) {
                return ($dishonoured_cheque->oldBank()->count()) ? $dishonoured_cheque->oldBank->name : "";
            })
            ->editColumn('old_bank_id', function ($dishonoured_cheque) {
                return ($dishonoured_cheque->oldBank()->count()) ? $dishonoured_cheque->oldBank->name : "";
            })
            ->editColumn('new_bank_id', function ($dishonoured_cheque) {
                return ($dishonoured_cheque->newBank()->count()) ? $dishonoured_cheque->newBank->name : "";
            })
            ->rawColumns(['status'])
            ->make(true);
    }


    /**
     * @param $employer_id
     * @return mixed
     * @throws \Exception
     * Get Incidents by employer with pay status for dt
     */
    public function getIncidentsWithPayStatusForDt($employer_id)
    {
        return Datatables::of($this->employers->getIncidentsWithPayStatusForDt($employer_id)->orderBy('n.incident_date', 'desc'))
            ->editColumn('pay_status', function ($incident) {
                return $incident->pay_status == 1 ?  "<span class='tag tag-success white_color'>" . 'Paid at least once' . "</span>" : (($incident->pay_status == 0) ? "<span class='tag tag-warning white_color'>" . 'Pending' . "</span>" : "<span class='tag tag-danger white_color'>" . 'Rejected' . "</span>");
            })
            ->editColumn('incident_date', function ($incident) {
                return short_date_format($incident->incident_date);
            })
            ->rawColumns(['pay_status'])
            ->make(true);
    }



}