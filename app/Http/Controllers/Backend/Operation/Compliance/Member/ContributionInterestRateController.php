<?php
namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use DB;
use Carbon\Carbon;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionInterestRateRepository;
use Yajra\Datatables\Services\DataTable;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Exceptions\GeneralException;
use App\DataTables\WorkflowTrackDataTable;

class ContributionInterestRateController extends Controller
{

    protected $modification_repo;

    public function __construct(ContributionInterestRateRepository $rate_repo)
    {
        $this->rate_repo = $rate_repo;
    }

    public function index()
    {
        $code_value_repo = (new CodeValueRepository());
        return view('backend/operation/compliance/member/employer/contrib_interest_rate/index');
    }

    public function create()
    {
        // code...
    }

    public function dataTable()
    {
       
        $data = $this->rate_repo->getRatesForDataTable();
        return DataTables::of($data)
        ->editColumn('rate', function ($data)
        {
            return $data->rate.'%';
        })
        ->editColumn('fin_code_id', function ($data)
        {
            return $data->fin_code_id == 1 ? 'Interest' : 'Contribution';
        })
        ->editColumn('employer_category_cv_id', function ($data)
        {
            return $data->employer_category_cv_id == 37 ? 'Private Employer' : 'Public Employer';
        })
        ->editColumn('start_date', function ($data)
        {
            return $data->start_date_formatted;
        })
        ->editColumn('wf_status', function ($data)
        {
            switch ($data->wf_status) {
                case 0:
                $status_label = '<span class="btn btn-warning btn-sm">Not Submitted</span>';
                break;
                case 1:
                $status_label = '<span class="btn btn-info btn-sm">On Progress</span>';
                break;
                case 2:
                $status_label = '<span class="btn btn-success btn-sm">Approved</span>';
                break;
                case 3:
                $status_label = $data->wf_done == 1 ? '<span class="btn btn-danger btn-sm">Rejected</span>' : '<span class="btn btn-danger btn-sm">Reversed</span>';
                break;
                case 3:
                $status_label = '<span class="btn btn-danger btn-sm">Cancelled</span>';
                break;
                default:
                $status_label = '';
                break;
            }
            return $status_label;
        })
        ->editColumn('end_date', function ($data)
        {
            return $data->end_date_formatted;
        })
        ->addColumn('name', function ($data)
        {
            return $data->long_name;
        })
        ->rawColumns(['status','wf_status'])
        ->make(true);
    }


    public function profile($contrib_rate_id, WorkflowTrackDataTable $workflowtrack)
    {
        $rate = $this->rate_repo
        ->findOrThrowException($contrib_rate_id);

        $public_contrib_rate = $this->rate_repo->getCurrentRate(2,36);
        $private_contrib_rate = $this->rate_repo->getCurrentRate(2,37);
        
        $private_interest_rate = $this->rate_repo->getCurrentRate(1,37);
        $public_interest_rate = $this->rate_repo->getCurrentRate(1,36);

        return view('backend/operation/compliance/member/employer/contrib_interest_rate/profile')
        ->with([
            "rate"=>$rate,
            "public_contrib_rate"=>$public_contrib_rate,
            "private_contrib_rate"=>$private_contrib_rate,
            "private_interest_rate"=>$private_interest_rate,
            "public_interest_rate"=>$public_interest_rate,
            "workflowtrack"=> $workflowtrack,
        ]);
    }

    public function post(Request $request)
    {
        if (request()->ajax())
        {
            $rules = [
              "rate_type" => 'required',
              "employer_category" => 'required',
              "new_rate" => 'required|numeric',
              "start_date" => 'required|date|date_format:Y-m-d',
              "description" => 'required',
              "request_attachment" => 'required'];

              $message = [
                "rate_type.*" => 'Kindly Select Applicable Rate Payment Type',
                "employer_category.*" => 'Kindly Select Applicable Employer Category',
                "new_rate.*" => 'Kindly enter valid rate',
                "start_date.*" => 'Kindly specify the start date (month) of new rate',
                "description.*" => 'Description is required',
                "request_attachment.*" => 'Kindly attach supporting document'
            ];

            $validator = Validator::make(Input::all() , $rules, $message);

            if ($validator->fails())
            {
                return response()
                ->json(array(
                    'errors' => $validator->getMessageBag()
                    ->toArray()
                ));
            }

            $pending = $this->rate_repo->checkPendingRateRequest($request->rate_type, $request->employer_category);
            if (count($pending)) {
                return response()->json(array('errors' => ['general' => 'There is a pending rate request for this payment type and employer category']));
            }

            $current = $this->rate_repo->getCurrentRate($request->rate_type, $request->employer_category);
            if(!empty($current->start_date)){
                if (Carbon::parse($current->start_date)->endOfMonth() >= Carbon::parse($request->start_date)->endOfMonth()) {
                    return response()->json(array('errors' => ['start_date' => 'The start date must be atleast a month after current rate start date']));
                }
            }

            $saved_request = $this->rate_repo->saveNewRate($request->all(), $request);
            return $saved_request ? response()->json(array(
                'success' => true,
                'request_id' => $saved_request->id
            )) : response()
            ->json(array(
                'errors' => ['general' => 'Something went wrong. Kindly retry']
            ));
            
        }
        else{
            throw new GeneralException('Request not allowed');}
        }

        public function returnCurrentRate($fin_code_id, $employer_cat)
        {
            $current = $this->rate_repo->getCurrentRate($fin_code_id, $employer_cat);
            return !empty($current->rate) ? response()->json(['success' => true, 'current_rate' => $current->rate, 'current_start_date' => $current->start_date]) : response()->json(['success' => false]);
        }


        public function previewRateAttachment($rate_request_id)
        {
            $url = $this
            ->rate_repo
            ->getRateAttachmentPath($rate_request_id);
            return response()->json(['success' => true, "request_url" => $url['request_path']]);
        }

        public function submitForApproval($rate_request_id)
        {
            $rate = $this
            ->rate_repo
            ->findOrThrowException($rate_request_id);
            $return = $this
            ->rate_repo
            ->submitRateForReviewAndApproval($rate);
            if ($return)
            {
                return redirect()->route('backend.compliance.employer.rates.profile', $rate->id)
                ->withFlashSuccess('Success, Contribution Interest Request has been submitted for review and approval');
            }
            else
            {
                return redirect()
                ->route('backend.compliance.employer.rates.profile', $rate->id)
                ->withFlashWarning('Something went wrong. Request Faield! Please try again');
            }
        }


        public function update($request_id, Request $request)
        {
            $rate = $this->rate_repo->findOrThrowException($request_id);
            if (request()->ajax())
            {
                $rules = [
                  "rate_type" => 'required',
                  "employer_category" => 'required',
                  "new_rate" => 'required|numeric',
                  "start_date" => 'required|date|date_format:Y-m-d',
                  "description" => 'required',
                  "request_attachment" => 'nullable'];

                  $message = [
                    "rate_type.*" => 'Kindly Select Applicable Rate Payment Type',
                    "employer_category.*" => 'Kindly Select Applicable Employer Category',
                    "new_rate.*" => 'Kindly enter valid rate',
                    "start_date.*" => 'Kindly specify the start date (month) of new rate',
                    "description.*" => 'Description is required',
                    "request_attachment.*" => 'Kindly attach supporting document'
                ];

                $validator = Validator::make(Input::all() , $rules, $message);

                if ($validator->fails())
                {
                    return response()
                    ->json(array(
                        'errors' => $validator->getMessageBag()
                        ->toArray()
                    ));
                }

                $pending = $this->rate_repo->checkPendingRateRequest($request->rate_type, $request->employer_category);
                if (count($pending)) {
                    return response()->json(array('errors' => ['general' => 'There is a pending rate request for this payment type and employer category']));
                }

                $current = $this->rate_repo->getCurrentRate($request->rate_type, $request->employer_category);
                if(!empty($current->start_date)){
                    if (Carbon::parse($current->start_date)->endOfMonth() >= Carbon::parse($request->start_date)->endOfMonth()) {
                        return response()->json(array('errors' => ['start_date' => 'The start date must be atleast a month after current rate start date']));
                    }
                }
                
                $saved_request = $this->rate_repo->saveEditeRateRequest($request->all(), $request, $rate);
                return $saved_request ? response()->json(array(
                    'success' => true,
                    'request_id' => $saved_request->id
                )) : response()
                ->json(array(
                    'errors' => ['general' => 'Something went wrong. Kindly retry']
                ));

            }
            else{
                throw new GeneralException('Request not allowed');}

            }

        }

