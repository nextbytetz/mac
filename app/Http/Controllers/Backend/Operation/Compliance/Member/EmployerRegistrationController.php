<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\DataTables\Compliance\Member\EmployeeCountDataTable;
use App\DataTables\Compliance\Member\EmployerRegistrationDataTable;
use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Exceptions\GeneralException;
use App\Http\Requests\Backend\Operation\Compliance\EmployerRegistrationRequest;
use App\Http\Requests\Backend\Operation\Compliance\UpdateEmployerRegistrationRequest;
use App\Http\Requests\Backend\Operation\Compliance\UploadEmployeeListRequest;
use App\Jobs\RegisterEmployeeList;
use App\Models\Operation\Compliance\Member\Employer;
use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Claim\DocumentGroupRepository;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Services\Scopes\IsApprovedScope;
use Illuminate\Http\Request;
use App\Services\Storage\Traits\FileHandler;
use App\Services\Storage\Traits\AttachmentHandler;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Operation\Compliance\Member\EmployeeUpload;
use Log;

class EmployerRegistrationController extends Controller
{

    use AttachmentHandler, FileHandler;

    protected $employers;
    protected $countries;
    protected $regions;
    protected $districts;
    protected $location_types;
    protected $code_values;
    protected $document_groups;
    /* start : Handling Documents */
    protected $base = null;
    protected $documents = null;
    protected $uploadedDocuments = null;
    protected $modelFolder = null;
    protected $basename = null;
    protected $id_no = null;
    /* end : Handling Documents */

    public function __construct() {
        $this->employers = new EmployerRepository();
        $this->countries = new CountryRepository();
        $this->regions = new RegionRepository();
        $this->districts = new DistrictRepository();
        $this->location_types = new LocationTypeRepository();
        $this->code_values = new CodeValueRepository();
        $this->document_groups = new DocumentGroupRepository();



        /* start : Initialise document store directory */
        $this->base = $this->real(employer_registration_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException('Base directory does not exist');
        }
        /* end : Initialise document store directory */


        $this->middleware('access.routeNeedsPermission:reissue_certificate', ['only' => ['reprintCertificate']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param EmployerRegistrationDataTable $dataTable
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(EmployerRegistrationDataTable $dataTable, Request $request)
    {
        if ($request->has('source')) {
            $input = ['source' => $request->input("source")];
        } else {
            $input = ['source' => 1];
        }
        return $dataTable->with($input)->render("backend/operation/compliance/member/employer/registration/index", ['source' => $input['source']]);

    }

    /**
     * @param $id
     * @param EmployeeCountDataTable $dataTable
     * @param WorkflowTrackDataTable $workflowTrack
     * @return mixed
     * @throws GeneralException
     * @description Employer Registration Profile
     */
public function profile($id, EmployeeCountDataTable $dataTable, WorkflowTrackDataTable $workflowTrack)
{
    $wf_module_group_id = 6;
    $employer = $this->employers->findWithoutScopeOrThrowException($id);
    $check_workflow = workflow([['wf_module_id' => $employer->wf_module_id, 'resource_id' => $id]])->checkIfHasWorkflow();
    $check_pending_level1 =  ($check_workflow == 1) ?  workflow([['wf_module_id' => $employer->wf_module_id, 'resource_id' => $id]])->checkIfIsLevel1Pending() : 0;
    $check_tin = (($this->employers->checkIfTinAlreadyExist($employer->tin)['tin_registered_employer'])->isNotEmpty() ? 1 : 0);
    return view('backend.operation.compliance.member.employer.registration.profile')
    ->with("employer", $employer)
    ->with("employee_counts_datatable", $dataTable)
    ->with("workflowtrack", $workflowTrack)
    ->with("check_workflow", $check_workflow)
    ->with("check_if_is_level1_pending", $check_pending_level1)
    ->with("check_tin", $check_tin);

}


    /**
     * Name check before adding new employer
     *
     */
    public function nameCheck()
    {
        $wf_module_group_id = 6;
        access()->hasWorkflowDefinition($wf_module_group_id,1);
        return view('backend.operation.compliance.member.employer.registration.name_check');
    }

    /**
     * Get all employers (unregistered + registered)
     */
    public function getAllEmployers()
    {

        return Datatables::of($this->employers->getAllUnregRegEmployers())
        ->editColumn('status', function ($employer) {
            return (($employer->status == 1) ?
                ($employer->approval_id == 1 ? "<span class='tag tag-success white_color'>" . trans('labels.general.registered') . "</span>"  :          "<span class='tag tag-info white_color'>" . trans('labels.general.pending') . "</span>" ): "<span class='tag tag-warning white_color'>" . trans('labels.general.unregistered') . "</span>" );
        })
        ->addColumn('actions', function ($employer) {
            return (($employer->status == 1) ? '<a href="' . route('backend.compliance.employer_registration.profile', $employer->id) . '" class="btn btn-xs btn-success replace_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.view') . '"></i></a> ' : '<a href="' . route('backend.compliance.unregistered_employer.profile', $employer->id) . '" class="btn btn-xs btn-warning " ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.view') . '"></i></a> ');
        })
//            -
        ->rawColumns(['status', 'actions'])
        ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $wf_module_group_id = 6;
        $type = (new WfModuleRepository())->employerRegistrationType()['type'];
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type, 1);

        return view('backend.operation.compliance.member.employer.registration.create')
//        ->with("employers", $this->employers->query()->withoutGlobalScopes([IsApprovedScope::class])->get()->pluck('name', 'id'))
        ->with("countries", $this->countries->getAll()->pluck('name', 'id'))
        ->with("location_types", $this->location_types->getAll()->pluck('name', 'id'))
        ->with("districts", $this->districts->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
        ->with("regions", $this->regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
        ->with("employer_categories", $this->code_values->query()->where('code_id',4)->where(function($query){
            $query->where('reference', 'ECPUB')->orWhere('reference', 'ECPRI');
        })->get()->pluck('name','id'))
        ->with("employee_categories", $this->code_values->query()->where('code_id',7)->get())
        ->with("business_sectors", $this->code_values->query()->where('code_id',5)->where('code_id', 5)->where(function($query){
            $query->where('reference', '<>', 'BSOSA')->where('reference', '<>', 'BSOTHRS');
        })->get()->pluck('name','id'))
        ->with("documents", $this->document_groups->query()->where('id', 11)->first()->documents);
    }


    /**
     * @param EmployerRegistrationRequest $request
     * Register employer information
     */
    public function register(EmployerRegistrationRequest $request)
    {
        $employer =  $this->employers->register($request->all());
        return redirect('compliance/employer_registration/profile/' . $employer->id . '#general')->withFlashSuccess(trans('alerts.backend.member.employer_registration_created'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $wf_module_group_id = 6;
        $this->checkLevel1Rights($id, $wf_module_group_id);

        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        $parent_employer = $this->employers->query()->where('id',$employer->parent_id)->withoutGlobalScopes([IsApprovedScope::class])->pluck('name', 'id');

        return view('backend.operation.compliance.member.employer.registration.edit')
        ->with("employers", $parent_employer)
        ->with("countries", $this->countries->getAll()->pluck('name', 'id'))
        ->with("location_types", $this->location_types->getAll()->pluck('name', 'id'))
        ->with("districts", $this->districts->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
        ->with("regions", $this->regions->query()->orderBy('name', 'asc')->get()->pluck('name', 'id'))
        ->with("employer_categories", $this->code_values->query()->where('code_id',4)->where(function($query){
            $query->where('reference', 'ECPUB')->orWhere('reference', 'ECPRI');
        })->get()->pluck('name','id'))
        ->with("employee_categories", $this->code_values->query()->where('code_id',7)->get())
        ->with("business_sectors", $this->code_values->query()->where('code_id', 5)->where(function($query){
            $query->where('reference', '<>', 'BSOSA')->where('reference', '<>', 'BSOTHRS');
        })->get()->pluck('name','id'))
        ->with("documents", $this->document_groups->query()->where('id', 11)->first()->documents)
        ->with("employer", $employer);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateEmployerRegistrationRequest $request)
    {
        //
        $wf_module_group_id = 6;
        $this->checkLevel1Rights($id, $wf_module_group_id);
        $employer = $this->employers->modifyRegister($id,$request->all());

        Log::info(print_r($employer,true));
        return redirect('compliance/employer_registration/profile/' . $employer->id . '#general')->withFlashSuccess(trans('alerts.backend.member.employer_registration_updated'));

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $wf_module_group_id = 6;
        $this->checkLevel1Rights($id, $wf_module_group_id);
        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        $this->employers->reject($id);
        return redirect()->route('backend.compliance.employer_registration.index')->withFlashSuccess(trans('alerts.backend.member.employer_registration_undone'));
    }



    public function showDocumentLibrary($id)
    {
        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        $documentRepository = new DocumentRepository();
        $this->base = $this->base . DIRECTORY_SEPARATOR . $id;
        $this->basename = basename($this->base);
        $this->makeDirectory($this->base);
        $this->modelFolder = $employer->pluck('id', 'id')->all();
        $this->id_no = ($employer->approval_id == 1) ? $this->modelFolder[basename($this->base)] : $id;
        $this->documents = $employer->documents->pluck('name', 'id')->all();
        $uploadedDocuments = [];
        foreach ($employer->documents as $document) {
            $uploadedDocuments += [$document->pivot->document_id => $documentRepository->query()->where('id',$document->pivot->document_id)->first()->name ];
        }

        /*dd($uploadedDocuments);*/
        $this->uploadedDocuments = $uploadedDocuments;
        $node = (isset(request()->id) And request()->id !== '#') ? request()->id : '/';
        $rslt = $this->lst_general($node, (isset(request()->id) && request()->id === '#'));
        return response()->json($rslt);
    }



    public function downloadDocument($id, $documentId)
    {
        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        $document = $employer->documents()->where("document_employer.document_id", $documentId)->first();
        return response()->download($document->linkEmployer($employer->id), $document->linkNameEmployer($employer->id));
    }

    public function openDocument($id, $documentId)
    {
        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        $document = $employer->documents()->where("document_employer.document_id", $documentId)->first() ;
        return response()->file($document->linkEmployer($employer->id));
    }


    /**
     * @param $id
     * @return mixed
     * Approve Employer Registration
     */

    public function approveRegistration($id) {
        // Log::info('hiiii'); die;
        
        $wf_module_group_id = 6;
        $wfModuleRepo = new WfModuleRepository();
        $type = $wfModuleRepo->employerRegistrationType()['type'];
        $group = $wfModuleRepo->employerRegistrationType()['group'];
        //$module = $wfModuleRepo->getModule(['wf_module_group_id' => $group, 'type' => $type]);
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type, 1);
        //$this->checkLevel1Rights($id, $wf_module_group_id);

        DB::transaction(function () use ($wf_module_group_id, $id, $group, $type) {
            $employer = $this->employers->approveRegistration($id);
            //event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $id]));
            event(new NewWorkflow(['wf_module_group_id' => $group, 'resource_id' => $id, 'type' => $type]));
        });

        return redirect('compliance/employer_registration/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.member.employer_registration_approved'));
    }


    /**
     * @param $id
     * Print Certificate
     */
    public function printCertificate($id){
        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        $this->employers->issueCertificate($id, 0);
        /* Name size according to length of the employer name */
        $name_css = $this->nameCertificateSize($employer->name_formatted);
        return view("backend/operation/compliance/member/employer/includes/certificate_registration")
        ->withEmployer($employer)
        ->withNameCss($name_css);
    }

    /**
     * @param $id
     * Re Print Certificate / Reissue
     */
    public function reprintCertificate($id){
        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        $this->employers->issueCertificate($id, 1);
        /* Name size according to length of the employer name */
        $name_css = $this->nameCertificateSize($employer->name_formatted);
        return view("backend/operation/compliance/member/employer/includes/certificate_registration")
        ->withEmployer($employer)
        ->withNameCss($name_css);
    }

    /**
     * @param $name
     * Change font of employer name on certificate depend on string length
     */
    public function nameCertificateSize($name){
        $name_css = '';
        if (strlen($name) > 60){
            $name_css =   '<div class="h4"> ' . $name .   '</div>';
        }else{
            $name_css =   '<div class="h2"> ' . $name .   '</div>';
        }
        return $name_css;
    }

    /**
     * @param $id
     * Print Issuance Reg No
     */
    public function printIssuance($id){
        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        return view("backend/operation/compliance/member/employer/includes/issuance_reg_no")
        ->withEmployer($employer);
    }




    /**
     * Check if can Initiate Action (Level 1 / No Workflow)
     */
    public function checkLevel1Rights($resource_id, $wf_module_group_id){
        access()->hasWorkflowDefinition($wf_module_group_id, 1);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction(1);
    }



    public function employeeListFile(Employer $employer) {
       $wf_module_group_id = 6;
       access()->hasWorkflowDefinition($wf_module_group_id,1);
       return view('backend.operation.compliance.member.employer.includes.upload_employee_list')
       ->withEmployer($employer);
   }

   public function uploadEmployeeList(Employer $employer, UploadEmployeeListRequest $request) {
    $input = [];
    $input = ['document_file37'=> request()->input('document_file37'), 'employer_document37' => 1];
    $this->employers->uploadDocuments($employer->id, $input);
    return response()->json(['success' => false, 'message' => trans('exceptions.backend.compliance
        .upload')]);
}


/*Load employee list into LIVE*/
public function loadEmployeeList(Employer $employer)
{
    $this->employers->registerEmployeeList($employer);
    return redirect('compliance/employer/profile/' . $employer->id . '#employees')->withFlashSuccess(trans('alerts.backend.member.loading_employee'));
}


public function undoLoadedEmployees(Employer $employer)
{
    $this->employers->undoLoadedEmployees($employer);
    return redirect('compliance/employer/profile/' . $employer->id . '#employees')->withFlashSuccess(trans('alerts.backend.member.undo_loaded_employees'));
}



//    public function uploadProgress(Employer $employer)
//    {
//        return response($employer->uploadPercent(), 200);
//    }

public function downloadError($id)
{
    $employer = $this->employers->findWithoutScopeOrThrowException($id);
    $headers = [
        'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
        ,   'Content-type'        => 'text/csv'
        ,   'Content-Disposition' => 'attachment; filename=download_error.csv'
        ,   'Expires'             => '0'
        ,   'Pragma'              => 'public'
    ];

    $upload_error = $employer->employeeTemps()->select(['error_report', 'firstname', 'middlename', 'lastname'])->where(['error' => 1])->get();

    /*internal error*/
    $document = new DocumentRepository();
    $employee_doc_id = $document->getEmployeeFileDocument();
    $internal_error = $employer->documents()->select(['upload_error', 'rows_imported'])->where('document_id',$employee_doc_id)->where('error', 1)->first();
    if($upload_error->count()){
        $list = $upload_error->toArray();
    }elseif($internal_error){
        $list = [ '0' => ['internal_error' => $internal_error->upload_error . ' on row ' .  ($internal_error->rows_imported + 1)]];
    }

        # add headers for each column in the CSV download
        //array_unshift($list, array_keys($list[0]));
    $callback = function() use ($list)
    {
        $FH = fopen('php://output', 'w');
        foreach ($list as $row) {
            fputcsv($FH, $row);
        }
        fclose($FH);
    };
    return response()->stream($callback, 200, $headers);
}




    /**
     * @param $id
     * @return mixed
     * Get all names which are similar to this employer
     */
    public function getNameClearance($id)
    {
        $employer = $this->employers->findWithoutScopeOrThrowException($id);
        $name= $employer->name;

        return Datatables::of($this->employers->getNameClearance($name))
        ->addColumn('doc_formatted', function ($employer) {
            return $employer->date_of_commencement_formatted;
        })
        ->addColumn('region', function ($employer) {
            return ($employer->district_id) ? $employer->district->region->name : ' ';
        })
        ->make(true);
    }


//======================================= MAC - GEPG

    public function returnEmployees(Employer $employer) 
    {
        $employees = EmployeeUpload::where('tin','=',$employer->tin)
        ->where('memberno','=',null)
        ->get();
        return DataTables::of($employees)
        ->addColumn("action", function($employees) {
         return 
         '<button type="button" class="btn btn-info btn-md btnEdit" data-edit="/compliance/online_uploaded_employees/'.$employees->id.'/edit">
         <i class="fa fa-pencil"></i> Edit
         </button>';
     })
        ->make(true);
    }

    //======================================= MAC - GEPG
}
