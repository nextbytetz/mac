<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\Http\Controllers\Backend\Finance\ReceiptController;
use App\Http\Requests\Backend\Finance\Receipt\SupplierFormRequest;
use App\Models\MacErp\ErpSupplier;
use App\Http\Requests\Backend\Operation\Compliance\CreateDependentRequest;
use App\Http\Requests\Backend\Operation\Payroll\CreateManualSurvivorRequest;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationWorkflowRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Sysdef\GenderRepository;
use App\Repositories\Backend\Sysdef\IdentityRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Services\Workflow\Workflow;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use Log;



class DependentController extends Controller
{

    protected  $dependents;
    protected $employees;
    protected $countries;
    protected $genders;
    protected $dependent_types;
    protected $location_types;
    protected $identities;
    protected $banks;
    protected $bank_branches;
    protected $notification_reports;
    protected $manual_notification_reports;

    public function __construct( DependentRepository $dependents, EmployeeRepository $employees, CountryRepository $countries, GenderRepository $genders, DependentTypeRepository $dependent_types, LocationTypeRepository $location_types, IdentityRepository $identities, BankRepository $banks, BankBranchRepository $bank_branches) {
        $this->dependents = $dependents;
        $this->employees = $employees;
        $this->countries = $countries;
        $this->genders = $genders;
        $this->dependent_types = $dependent_types;
        $this->location_types = $location_types;
        $this->identities = $identities;
        $this->banks = $banks;
        $this->bank_branches = $bank_branches;
        $this->notification_reports = new NotificationReportRepository();
        $this->manual_notification_reports = new ManualNotificationReportRepository();


//        $this->middleware('access.routeNeedsPermission:create_dependent', ['only' => ['create_new', 'store_new']]);

        $this->middleware('access.routeNeedsPermission:manage_payroll_data_migration',['only' => ['redistributeMpAllocationForManualSystemFile']]);

        $this->middleware('access.routeNeedsPermission:manage_payroll_data_migration',['only' => ['mergeManualDependent', 'updateManualPayStatus']]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store()
    {

    }

    public function create_new($employee_id)
    {
        //
        $this->checkIfCanInitiateActionAtLevel($employee_id, 1);

        $employee = $this->employees->findOrThrowException($employee_id);
        /*death date / child age limits*/
        $death_incident_query = $employee->notificationReports()->whereIn('incident_type_id',[3,4,5]);
        $death_date =  ($death_incident_query->count() > 0)  ? standard_date_format($death_incident_query->first()->incident_date) : standard_date_format(Carbon::now());
        $child_age_limit = sysdefs()->data()->payroll_child_limit_age;
        $child_age_limit_date = Carbon::parse($death_date)->subYears($child_age_limit)->format('Y-n-j');//18yrs
        /*end child / death date*/
        $other_dependents = $employee->dependents;
        return view('backend.operation.compliance.member.employee.dependent.create')
            ->with([
                'employee' => $employee,
                'countries' => $this->countries->getAll()->pluck('name', 'id'),
                'genders' =>  $this->genders->getAll()->pluck('name', 'id'),
                'dependent_types' => $this->dependent_types->getAll()->pluck('name', 'id'),
                'other_dependents' => $other_dependents->pluck('name', 'id'),
                'location_types' => $this->location_types->getAll()->pluck('name', 'id'),
                'identities' => $this->identities->getAll()->pluck('name', 'id'),
                'banks' => $this->banks->getActiveBanksForSelect(),
                'bank_branches' => $this->bank_branches->getAll()->pluck('name', 'id'),
                'death_date' => $death_date,
                'child_age_limit_date' => $child_age_limit_date
            ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_new($employee_id,CreateDependentRequest $request)
    {
        //

        access()->hasWorkflowDefinition(3,1);
        $dependent = $this->dependents->create($employee_id,$request->all());
        return redirect('compliance/employee/profile/' . $employee_id . '#dependents')->withFlashSuccess(trans('alerts.backend.member.dependent_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $dependent = $this->dependents->findOrThrowException($id);
        return view('backend.operation.compliance.member.employee.dependent.edit')
            ->with('dependent', $dependent)
            ->with(  'countries',$this->countries->getAll()->pluck('name', 'id'))
            ->with('genders',  $this->genders->getAll()->pluck('name', 'id'))
            ->with('dependent_types',$this->dependent_types->getAll()->pluck('name', 'id'))
            ->with('other_dependents',$this->dependents->query()->where('employee_id',$dependent->employee_id)->get()->pluck('name', 'id'))
            ->with('location_types',$this->location_types->getAll()->pluck('name', 'id'))
            ->with('identities',$this->identities->getAll()->pluck('name', 'id'))
            ->with('banks',$this->banks->getAll()->pluck('name', 'id'))
            ->with('bank_branches',$this->bank_branches->query()->get()->pluck('name', 'id'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * with tow parameter for employee
     */
    public function editDependent($id, $employee_id)
    {
        //
        $this->dependents->checkIfCanEditDependentException($id, $employee_id);
        $this->checkIfCanInitiateActionAtLevel($employee_id, 1);
        $employee = $this->employees->findOrThrowException($employee_id);
        $other_dependents = $employee->dependents;
        $dependent = $this->dependents->findOrThrowException($id);
        $pivot = $dependent->employees()->where('employee_id', $employee_id)->first()->pivot;
        /*death date / child age limits*/
        $death_incident_query = $employee->notificationReports()->whereIn('incident_type_id',[3,4,5]);
        $death_date =  ($death_incident_query->count() > 0)  ? standard_date_format($death_incident_query->first()->incident_date) : standard_date_format(Carbon::now());
        $child_age_limit = sysdefs()->data()->payroll_child_limit_age;
        $child_age_limit_date = Carbon::parse($death_date)->subYears($child_age_limit)->format('Y-n-j');//18yrs
        /*end child / death date*/
        return view('backend.operation.compliance.member.employee.dependent.edit')
            ->with([
                'dependent' => $dependent,
                'countries' => $this->countries->getAll()->pluck('name', 'id'),
                'genders' => $this->genders->getAll()->pluck('name', 'id'),
                'dependent_types' => $this->dependent_types->getAll()->pluck('name', 'id'),
                'other_dependents' => $other_dependents->pluck('name', 'id'),
                'location_types' => $this->location_types->getAll()->pluck('name', 'id'),
                'identities' => $this->identities->getAll()->pluck('name', 'id'),
                'banks' => $this->banks->getAll()->pluck('name', 'id'),
                'bank_branches' => $this->bank_branches->query()->get()->pluck('name', 'id'),
                'employee' => $employee,
                'pivot' => $pivot,
                'death_date' => $death_date,
            ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id, CreateDependentRequest $request)
    {
        //

        $employee_id = $request['employee_id'];
        access()->hasWorkflowDefinition(3,1);
        $resource_id = ($this->dependents->findNotificationReport($id, $employee_id)) ? $this->dependents->findNotificationReport($id,$employee_id)->id : 0;
        if ($resource_id){
            workflow([['wf_module_group_id' => 3, 'resource_id' => $resource_id]])->checkIfCanInitiateAction(1);
        }

        $dependent = $this->dependents->update($id,$request->all());
        return redirect('compliance/employee/profile/' . $employee_id. '#dependents')->withFlashSuccess(trans('alerts.backend.member.dependent_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     * Get all dependents by search
     */
    public function getRegisteredDependents()
    {
        return $this->dependents->getRegisteredDependents(request()->input('q'));
    }


    public function getGratuityDependents()
    {
        return $this->dependents->getGratuityDependents(request()->input('q'));
    }


    /*On PAYROLL MODULE*/

    /*Display all dependents*/
    public function index()
    {
        return view('backend/operation/payroll/pension_administration/index/dependents');
    }


    /*Dependent profile for pension administration*/
    public function profile($dependent_employee_id)
    {
        $dependent_employee = DB::table('dependent_employee')->where('id', $dependent_employee_id)->first();

        $dependent = $this->dependents->find($dependent_employee->dependent_id);
        $notification_report = $this->notification_reports->find($dependent_employee->notification_report_id);

        $manual_notification_report = $this->manual_notification_reports->find($dependent_employee->manual_notification_report_id);
        $employee = $this->employees->find($dependent_employee->employee_id);
        $dependent_type = $this->dependent_types->find($dependent_employee->dependent_type_id);

        $payroll_runs = new PayrollRunRepository();
        $check_if_is_paid = $payroll_runs->checkIfBeneficiaryIsPaidAtLeastOnce(4, $dependent_employee->dependent_id,$dependent_employee->employee_id);
        $mp_payment_summary =  ($dependent_employee->firstpay_flag == 1 && isset($dependent_employee->notification_report_id) && $check_if_is_paid == true)  ?  $payroll_runs->getMpPaymentSummaryForMember(4, $dependent->id, $dependent_employee->employee_id) : null;
        $last_payroll_run_date = $payroll_runs->getMostRecentApprovedRunDate();
        $start_payroll_date = isset($dependent_employee->manual_notification_report_id) ? $payroll_runs->getStartPayDateManualFile($manual_notification_report) : $payroll_runs->getStartPayDate($notification_report) ;
        $check_if_has_doc = (new PayrollRepository())->checkIfBeneficiaryHasDocument(4, $dependent->id);
        return view('backend/operation/payroll/pension_administration/profile/dependent/dependent_profile')
            ->with('dependent', $dependent)
            ->with('dependent_employee', $dependent_employee)
            ->with('notification_report', $notification_report)
            ->with('manual_notification_report', $manual_notification_report)
            ->with('employee', $employee)
            ->with('dependent_type', $dependent_type)
            ->with('mp_payment_summary', $mp_payment_summary)
            ->with('last_payroll_run_date', $last_payroll_run_date)
            ->with('start_payroll_date', $start_payroll_date)
            ->with('check_if_is_paid', $check_if_is_paid)
            ->with('check_if_has_doc', $check_if_has_doc);

    }

    /*Get dependents for datatable*/
    public function getDependentsForDataTable()
    {
        return Datatables::of($this->dependents->dependentAllWithPivot())
            ->editColumn('survivor_pension_amount', function ($dependent) {
                return number_2_format($dependent->survivor_pension_amount);
            })
            ->editColumn('filename', function ($dependent) {
                return isset($dependent->filename) ? $dependent->filename : $dependent->case_no;
            })
            ->make(true);
    }


    /*Get dependents for datatable - dependents already enrolled into payroll*/
    public function getPensionableDependentsForDataTable()
    {
        return Datatables::of($this->dependents->getPensionableDependentsForDataTable())
            ->editColumn('survivor_pension_amount', function ($dependent) {
                return number_2_format($dependent->survivor_pension_amount);
            })
            ->editColumn('filename', function ($dependent) {
                return isset($dependent->filename) ? $dependent->filename : $dependent->case_no;
            })
            ->addColumn('status', function ($dependent) {
                $suspense_status = ($dependent->suspense_flag == 1) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Suspended'. "'>" . 'Suspended'. "</span>" : null;
                $active_status = ($dependent->isactive == 1) ?  "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active'. "'>" . 'Active'. "</span>" :
                    ( ($dependent->isactive == 2) ? "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Deactivated'. "'>" . 'Deactivated'. "</span>" :  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Inactive'. "'>" . 'Inactive'. "</span>" ) ;
                return  $active_status . ($suspense_status == null ? '': (' : '. $suspense_status));
            })
            ->rawColumns(['status'])
            ->make(true);
    }


    /*Get dependents ready for payroll (Pending for activation) for datatable*/
    public function getReadyForPayrollForDataTable()
    {
        return Datatables::of($this->dependents->getReadyForPayrollForDataTable())
            ->editColumn('survivor_pension_amount', function ($dependent) {
                return number_2_format($dependent->survivor_pension_amount);
            })
            ->editColumn('filename', function ($dependent) {
                return isset($dependent->filename) ? $dependent->filename : $dependent->case_no;
            })
            ->make(true);
    }

    /*Get other dependents belong to employee for datatable*/
    public function getOtherByEmployeeForDataTable($dependent_employee_id)
    {
        $dependent_employee = DB::table('dependent_employee')->where('id', $dependent_employee_id)->first();

        return Datatables::of($this->dependents->dependentsWithPivot($dependent_employee->employee_id)->where('dependent_employee.dependent_id', '<>', $dependent_employee->dependent_id))
            ->editColumn('survivor_pension_amount', function ($dependent) {
                return number_2_format($dependent->survivor_pension_amount);
            })
            ->addColumn('action', function ($dependent) {
//                if($dependent->survivor_pension_percent > 0 && $dependent->survivor_pension_amount < 1){
                return '<a href="' . route('backend.compliance.dependent.rectify_mp', ['dependent' => $dependent->dependent_id, 'employee' => $dependent->employee_id ] ) . '" class="btn btn-xs btn-primary save_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Rectify MP' . '"></i></a> ';
//                }

            })
            ->make(true);
    }


    /*Get dependents for select*/
    public function getDependentsForSelect()
    {
        return $this->dependents->getDependentsForSelect(request()->input('q'), request()->input('page'));
    }


    /**
     * Open page for merging dependents exist on the system but processed manually for payroll
     */
    public function dependentReadyForMergeManual()
    {
        return view('backend/operation/payroll/data_migration/merge_manual/dependent/index');
    }


    /*Merge dependent who were paid manually*/
    public function mergeManualDependentPage($id, $employee_id)
    {
        $dependent = $this->dependents->find($id);
        $employee = $this->employees->findOrThrowException($employee_id);
        $pivot = $dependent->employees()->where('employee_id', $employee_id)->first()->pivot;
           return view('backend/operation/payroll/data_migration/merge_manual/dependent/merge')
            ->with([
                'employee' => $employee,
                'countries' => (new CountryRepository())->getAll()->pluck('name', 'id'),
                'genders' =>  (new GenderRepository())->getAll()->pluck('name', 'id'),
                'dependent_types' => (new DependentTypeRepository())->getAll()->pluck('name', 'id'),
                'location_types' => (new LocationTypeRepository())->getAll()->pluck('name', 'id'),
                'identities' => (new IdentityRepository())->getAll()->pluck('name', 'id'),
                'banks' => (new BankRepository())->getAll()->pluck('name', 'id'),
                'bank_branches' => (new BankBranchRepository())->getAll()->pluck('name', 'id'),
                'dependent' => $dependent,
                'pivot' => $pivot,
            ]);

    }

    public function mergeManualDependent($employee_id, CreateManualSurvivorRequest $request)
    {
        $input = $request->all();
        $this->dependents->mergeManualDependent($employee_id, $input);
         $hasarrears = isset($input['hasarrears']) ? $input['hasarrears'] : 0;
              if($hasarrears == 1){
            /*Has arrears*/
            return redirect()->route('backend.payroll.recovery.create_manual_arrears_system_members', ['member_type_id' => 4, 'resource_id' => $input['dependent_id'], 'employee_id' => $employee_id, 'arrears_months' => $input['pending_pay_months']])->withFlashSuccess('Success, Dependent has been merged and enrolled to payroll! Proceed to initiate approval of arrears');
        }else{
            /*no arrears*/
            if(isset($input['redirect_merge'])){
                $notification_report = $this->dependents->findNotificationReport($input['dependent_id'], $input['employee_id']);
                            return redirect()->route('backend.claim.manual_system_file.survivors', $notification_report->id)->withFlashSuccess('Success, Dependent has been merged and enrolled to payroll');
            }else{
                       return redirect()->route('backend.compliance.dependent.dependents_ready_for_merge_manual', 5)->withFlashSuccess('Success, Dependent has been merged and enrolled to payroll');
            }

        }

    }





    /*Get dependents ready for payroll (Pending for activation and already paid manually out of system) for datatable*/
    public function getManualMergeReadyForPayrollForDataTable()
    {
        return Datatables::of($this->dependents->getReadyForPayrollForManualMergeForDataTable()->orderBy('dependents.firstpay_manual', 'desc')->orderBy('employees.lastname'))
            ->editColumn('survivor_pension_amount', function ($dependent) {
                return number_2_format($dependent->survivor_pension_amount);
            })
            ->addColumn('status', function ($dependent) {
                $paid_manual = ($dependent->firstpay_manual == 1  && $dependent->isactive == 1) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid Manual - Active'. "'>" . 'Paid Manual - Active'. "</span>" : '';
                $paid_manual_terminated = ($dependent->firstpay_manual == 1 && $dependent->isactive == 2) ? "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . 'Paid Manual - Terminated'. "'>" . 'Paid Manual - Terminated'. "</span>" : '';
                $not_yet_paid = ($dependent->firstpay_manual == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not yet paid'. "'>" . 'Not yet paid'. "</span>" : '';
                $pending = ($dependent->firstpay_manual == 2) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>" : '';
//                $merge_status = ($dependent->firstpay_manual <> 2) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Synced'. "'>" . 'Synced'. "</span>" : "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>" ;

                return  $paid_manual . $paid_manual_terminated . $not_yet_paid . $pending;
            })
            ->addColumn('action', function ($dependent) {

                $already_paid_activate = '<a href="' . route('backend.compliance.dependent.merge_manual_dependent_page',['dependent' =>  $dependent->dependent_id, 'employee' =>  $dependent->employee_id]) . '" class="btn btn-xs btn-success dishonour_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Already paid - Activate' . '"></i> Already Paid - Activate </a> ';
                $already_paid_terminate = '';
//                $already_paid_terminate = '<a href="' . route('backend.compliance.dependent.update_manualpay_status',['dependent' =>  $dependent->dependent_id, 'employee' =>  $dependent->employee_id, 'status' => 2]) . '" class="btn btn-xs btn-warning warning_button" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Already paid - Terminate' . '"></i> Already Paid - Terminate </a> ';
                $not_yet_paid = '<a href="' . route('backend.compliance.dependent.update_manualpay_status',['dependent' =>  $dependent->dependent_id, 'employee' =>  $dependent->employee_id, 'status' => 0]) . '" class="btn btn-xs btn-primary save_button " ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Not yet paid' . '"></i> Not yet paid </a> ';
                $merge_button = ($dependent->firstpay_manual == 2) ? ($already_paid_activate . '<br/>' . $already_paid_terminate . '<br/>' . $not_yet_paid) : '' ;

                ;

                return  $merge_button;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }


    /**
     * @param $dependent
     * @param $employee
     * @param $status
     * Update manual pay status i.e. 1 => paid manual, 2 => paid and terminated, 0 => not paid
     */
    public function updateManualPayStatus($id,$employee_id, $status)
    {
        $this->dependents->updateManualPayStatus($id, $employee_id, $status);
        return redirect()->route('backend.compliance.dependent.dependents_ready_for_merge_manual')->withFlashSuccess('Success, Dependent status has been synced');
    }




    /**
     * Open page for already merged dependents exist on the system but processed manually for payroll
     */
    public function dependentAlreadyMergedManualPayroll()
    {
        return view('backend/operation/payroll/data_migration/merge_manual/dependent/already_merged_for_payroll_datatable');
    }


    /*Get dependents ready for payroll ( Who are already merged - (Survivors migration with manual payroll process) for datatable*/
    public function getManualAlreadyMergedForPayrollForDataTable()
    {
        return Datatables::of($this->dependents->dependentAllWithPivot()->where('dependents.firstpay_manual','<>', 2)->orderBy('dependents.firstpay_manual','desc')->orderBy('employees.firstname'))
            ->editColumn('survivor_pension_amount', function ($dependent) {
                return number_2_format($dependent->survivor_pension_amount);
            })
            ->addColumn('status', function ($dependent) {
                $paid_manual = ($dependent->firstpay_manual == 1  && $dependent->isactive == 1) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Paid Manual - Active'. "'>" . 'Paid Manual - Active'. "</span>" : '';
                $paid_manual_terminated = ($dependent->firstpay_manual == 1 && $dependent->isactive == 2) ? "<span class='tag tag-primary' data-toggle='tooltip' data-html='true' title='" . 'Paid Manual - Terminated'. "'>" . 'Paid Manual - Terminated'. "</span>" : '';
                $not_yet_paid = ($dependent->firstpay_manual == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not yet paid'. "'>" . 'Not yet paid'. "</span>" : '';
                $pending = ($dependent->firstpay_manual == 2) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Pending'. "'>" . 'Pending'. "</span>" : '';
//
                return  $paid_manual . $paid_manual_terminated . $not_yet_paid . $pending;
            })
            ->addColumn('action', function ($dependent) {
                $reset = '<a href="' . route('backend.compliance.dependent.update_manualpay_status',['dependent' =>  $dependent->dependent_id, 'employee' =>  $dependent->employee_id, 'status' => 3]) . '" class="btn btn-xs btn-primary save_button " ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Reset' . '"></i> Reset </a> ';
                $reset_button = ($dependent->firstpay_manual <> 2) ? $reset  : '' ;
                return  $reset_button;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }


    /**
     *Open dependents ready to be exported to erp as supplier
     */
    public function viewDependentsExportToErp()
    {
        //TODO: Remove this temporary export status
        /*Temporary - Update export status for already exported dependents*/
        $this->dependents->updateErpExportStatusAlreadyExported();
        return view('backend/operation/compliance/member/employee/dependent/export_to_erp/index');

    }

    /**
     * Get dependents for datatable to be exported to erp
     */
    public function getDependentReadyToBeExportedToErpForDataTable()
    {
        return Datatables::of($this->dependents->getDependentReadyToBeExportedToErpForDataTable())
            ->addColumn('dob_formatted', function ($dependent) {
                return short_date_format($dependent->dob);
            })
            ->make(true);
    }

    /*Return supplier registration form*/
    public function returnSupplierForm($dependent_employee_id)
    {


        //Get employee as supplier
        $dependent_employee=DB::table('main.dependent_employee')
            ->where('id','=',$dependent_employee_id)
            ->first();

        $dependent = $this->dependents->find($dependent_employee->dependent_id);
        /*employee*/
        $employee = (new EmployeeRepository())->find($dependent_employee->employee_id);
        /*employee employer*/
        $employee_employer=DB::table('main.employees')
            ->join('main.employee_employer','employees.id','=','employee_employer.employee_id')
            ->where('employees.id','=',$dependent_employee->employee_id)
            ->first();

        //Get employer for linking with regions
        $employer = (new EmployerRepository())->find($employee_employer->employer_id);

        //Get regions for updating supplier region
        $region = (isset($employer->region_id)) ? $employer->region : null;

        return view("backend/operation/compliance/member/employee/dependent/export_to_erp/create_supplier")->with('dependent',$dependent)
            ->with('region', $region)->with('employee', $employee)->with('employer',$employer);

    }

    /*Store supplier to erp API*/
    public function storeErpSupplier(SupplierFormRequest $request)
    {

//        $input = $request->all();
//        $this->dependents->storeErpSupplier($input);
//        return redirect()->route('backend.compliance.dependent.ready_for_export_to_erp')->withFlashSuccess('Supplier Created and Posted to ERP');
//
//

        $supplier_exist=DB::table('main.erp_suppliers')->where('supplier_id',$request->supplier_id)->first();
        if($supplier_exist){
            // return view("backend/operation/compliance/member/employee/dependent/export_to_erp/index")->withFlashSuccess('Supplier already created,run concurrent in ERP');
            return response()->json(array('success'=>['message' => 'Supplier already created,run concurrent in ERP']));

        }else{
            $supplier_id =$request->supplier_id;
            $random_id = str_replace('_', '',$supplier_id);

            $new_supplier=new ErpSupplier;
            $new_supplier->trx_id=mt_rand(1000, 1999).$random_id;
            $new_supplier->supplier_name=$request->supplier_name;
            $new_supplier->supplier_id=$request->supplier_id;
            $new_supplier->vendor_site_code=$request->vendor_site_code;
            $new_supplier->address_line1=$request->address_line1;
            $new_supplier->address_line2=$request->address_line2;
            $new_supplier->city=$request->city;
            $new_supplier->liability_account=$request->liability_account;
            $new_supplier->status_code=200;
            $new_supplier->pay_group='BENEFICIARIES';
            $new_supplier->user_id=access()->user()->id;
            $new_supplier->save();

            $this->postSupplierErpApi();

            /*Update erp status*/
            $this->dependents->updateErpExportStatus($request->dependent_id, $request->employee_id);

            return redirect()->route('backend.compliance.dependent.ready_for_export_to_erp')->withFlashSuccess('Supplier Created and Posted to ERP');

        }
    }

    public function postSupplierErpApi()
    {
        $url ='http://192.168.1.9/mac_erp/public/api/post_supplier_to_q/';
        Log::info($url);
        try {
            $client = new Client();
            $response =  $client->request('GET', $url);
            $status = $response->getStatusCode();
        }
        catch (ClientError $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status:'.$status.' ClientError');
        }
        catch (ServerError $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status: '.$status.' ServerError');
        }
        catch (BadResponse $e) {

            $req = $e->getRequest();
            $status =$e->getStatusCode();
            Log::info(' Status: '.$status,' BadResponse');

        }
        catch(\Exception $e){
            $status = $e->getCode();
            Log::info(' Status: '.$e->getMessage().' Exception');

        }

    }


    /*Rectify MP*/
    public function rectifyMp($id, $employee_id)
    {
        $dependent_employee =  $this->dependents->rectifyMp($id, $employee_id);
        return redirect()->route('backend.compliance.dependent.profile', $dependent_employee->id)->withFlashSuccess('Success, MP has been rectified');

    }

    /*Check if can initiate action level*/
    public function checkIfCanInitiateActionAtLevel($employee_id, $level)
    {
        access()->hasWorkflowDefinition(3,$level);

        $resource_id = ($this->employees->findNotificationReportDeathIncident($employee_id)) ? $this->employees->findNotificationReportDeathIncident($employee_id)->id : 0;
        if ($resource_id) {
            $incident =  ($this->employees->findNotificationReportDeathIncident($employee_id));
            if($incident->isprogressive == 1) {
                $wf_module_id = (new WfModuleRepository())->claimBenefitModule()[0];
                $wf_resource = (new NotificationWorkflowRepository())->query()->where('notification_report_id', $resource_id)->where('wf_module_id', $wf_module_id)->first();

                if ($wf_resource) {
                    $workflow = new Workflow(['wf_module_group_id' => 3, "resource_id" => $wf_resource->id, 'type' => 10]);
                    $workflow->checkIfCanInitiateAction($level);
                }
            }else{
                /*check for legacy*/
                $workflow = new Workflow(['wf_module_group_id' => 3, "resource_id" => $incident->id, 'type' => 1]);
                $workflow->checkIfCanInitiateAction($level);
            }

        }

    }


    /*Redistribute Mp Allocation for manual system file*/
    public function redistributeMpAllocationForManualSystemFile($incident_id)
    {
        $incident = $this->notification_reports->find($incident_id);
        $this->dependents->redistributeMpAllocationForManualSystemFile($incident);

        return redirect()->back()->withFlashSuccess('Success, Restribution has been done! Please confirm the new allocation before proceed.');
    }
}
