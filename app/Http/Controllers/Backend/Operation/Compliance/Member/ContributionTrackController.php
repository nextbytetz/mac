<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\Http\Controllers\Controller;
use App\Models\Operation\Compliance\Member\ContributionTrack;
use App\Repositories\Backend\Access\PermissionGroupRepository;
use App\Repositories\Backend\Access\RoleRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionTrackRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Http\Requests\Backend\Operation\Compliance\CreateContributionTrackRequest;

class ContributionTrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    protected $contribution_tracks;
    protected $receipt_codes;


    public function __construct(ContributionTrackRepository $contribution_tracks, ReceiptCodeRepository
    $receipt_codes) {
        $this->contribution_tracks = $contribution_tracks;
$this->receipt_codes = $receipt_codes;

        $this->middleware('access.routeNeedsPermission:create_contribution_track', ['only' => ['create_new', 'store']]);
        $this->middleware('access.routeNeedsPermission:edit_contribution_track', ['only' => ['edit', 'update']]);
    }



    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }


    public function create_new($receipt_code_id)
    {
        //
             $receipt_code = $this->receipt_codes->findOrThrowException($receipt_code_id);
        return view('backend.operation.compliance.member.contribution.create_track')
            ->withReceiptCode($receipt_code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateContributionTrackRequest $request)
    {
        //
        $this->contribution_tracks->create($request);
        return redirect()->route('backend.finance.receipt_code.show',$request['id'])->withFlashSuccess(trans('alerts.backend.contribution.track_created'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionTrack $contribution_track)
    {
        //
            $this->contribution_tracks->checkIfBelongToUser($contribution_track->id);
//        $contribution_track = $this->contribution_tracks->findOrThrowException($id);
        return view('backend.operation.compliance.member.contribution.edit_track')
            ->withContributionTrack($contribution_track);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,CreateContributionTrackRequest $request)
    {
        //
        $contribution_track = $this->contribution_tracks->update($id,$request);
        return redirect()->route('backend.finance.receipt_code.show',$contribution_track->receipt_code_id)->withFlashSuccess(trans('alerts.backend.contribution.track_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
