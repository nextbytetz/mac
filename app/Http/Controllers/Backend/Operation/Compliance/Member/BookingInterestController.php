<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Models\Finance\Receivable\BookingInterestRefund;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\BankRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Workflow\WfModuleGroupRepository;
use App\Services\Receivable\CalculateInterest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class BookingInterestController extends Controller
{

    protected $booking_interests;
    protected $employers;
    protected $bookings;


    public function __construct() {
        $this->booking_interests = new BookingInterestRepository();
        $this->employers = new EmployerRepository();
        $this->bookings = new BookingRepository();

        $this->middleware('access.routeNeedsPermission:recalculate_interest', ['only' => ['recalculateInterestForEmployer']]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }





    /**
     *
     * INTEREST REFUND *****************Start ******************
     */


    public function interestRefundProfile($interest_refund_id, WorkflowTrackDataTable $workflowtrack)
    {
        $wf_module_groups = new WfModuleGroupRepository();
        $wf_module_group_id = $wf_module_groups->getInterestRefundGroup();

        $banks = new BankRepository();
        $bank_branches = new BankBranchRepository();

        $interest_refund = BookingInterestRefund::query()->find($interest_refund_id);
        $employer = $this->employers->query()->whereHas('bookingInterests', function($query) use($interest_refund_id)
        {
            $query->whereHas('bookingInterestRefund', function($query) use($interest_refund_id){
                $query->where('id', $interest_refund_id);
            });
        })->first();


        $check_workflow = workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $interest_refund_id]])->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $interest_refund_id]])->checkIfIsLevel1Pending() : 0;

        return view('backend.operation.compliance.member.employer.receivable.interest.refund.profile')
            ->withInterestRefund($interest_refund)
            ->withEmployer($employer)
            ->withBanks($banks->getAll()->pluck('name', 'id'))
            ->withBankBranches($bank_branches->getAll()->pluck('name', 'id'))
            ->withCheckWorkflow($check_workflow)
            ->withCheckIfIsLevel1Pending($check_pending_level1)
            ->withWorkflowtrack($workflowtrack)
            ->withWfModuleGroupId($wf_module_group_id);
    }

    public function interestRefund($id)
    {
        $employer = $this->employers->findOrThrowException($id);
        return view('backend.operation.compliance.member.employer.receivable.interest.refund.index')
            ->withEmployer($employer);
    }


    public function refundPage($employer_id)
    {
        $employer = $this->employers->findOrThrowException($employer_id);
        return view('backend.operation.compliance.member.employer.receivable.interest.refund.refunding_page')
            ->withEmployer($employer);
    }


    /*refund selected*/
    public function refundSelected(Request $request)
    {
        $interest_refund =  $this->booking_interests->refundSelected($request->all());
        return redirect()->route('backend.compliance.booking_interest.refund_profile', $interest_refund->id)->withFlashSuccess(trans('alerts.backend.interest.interest_refund_created'));
    }



    public function modifyInterestRefund($interest_refund_id)
    {
        $interest_refund = BookingInterestRefund::query()->find($interest_refund_id);
        $employer = $this->employers->query()->whereHas('bookingInterests', function($query) use($interest_refund_id)
        {
            $query->whereHas('bookingInterestRefund', function($query) use($interest_refund_id){
                $query->where('id', $interest_refund_id);
            });
        })->first();
        return view('backend.operation.compliance.member.employer.receivable.interest.refund.edit')
            ->withInterestRefund($interest_refund)
            ->withEmployer($employer);

    }


    /*Action: Add refund on Modifying interest refund*/
    public function addInterestRefund($id, $interest_refund_id)
    {
        $this->booking_interests->addInterestRefund($id, $interest_refund_id);
        return redirect()->route('backend.compliance.booking_interest.refund.edit', $interest_refund_id)->withFlashSuccess('Success, Interest has been added');
    }
    /* Action : Remove interest from interest refund batch*/
    public function removeInterestRefund($id, $interest_refund_id)
    {
        $this->booking_interests->removeInterestRefund($id,$interest_refund_id);
        return redirect()->route('backend.compliance.booking_interest.refund.edit' , $interest_refund_id)->withFlashSuccess('Success, Interest has been removed');
    }


    /*Undo Interest Refund : Remove Interest refund*/
    public function undoInterestRefund($interest_refund_id)
    {
        $employer = $this->employers->query()->whereHas('bookingInterests', function($query) use($interest_refund_id)
        {
            $query->whereHas('bookingInterestRefund', function($query) use($interest_refund_id){
                $query->where('id', $interest_refund_id);
            });
        })->first();
        $this->booking_interests->undoInterestRefund($interest_refund_id);
        return redirect()->route('backend.compliance.booking_interest.refund_overview' , $employer->id)->withFlashSuccess('Success, Interest Refund has been undone');
    }


    /* Initiate / Approve Interest Refund*/
    public function initiateInterestRefund($interest_refund_id)
    {
        $wf_module_group_id = 9;
        $this->checkLevel1Rights($interest_refund_id, $wf_module_group_id);
        DB::transaction(function () use ($wf_module_group_id,$interest_refund_id) {
            $this->booking_interests->initiateInterestRefund($interest_refund_id);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $interest_refund_id]));
        });
        return redirect()->route('backend.compliance.booking_interest.refund_profile', $interest_refund_id)->withFlashSuccess('Success, Interest refund process has been initiated');
    }




    /*GET METHODS -------------------*/


    public function getInterestRefundedForDataTable($interest_refund_id)
    {
        return Datatables::of($this->booking_interests->getInterestRefundedForDataTable($interest_refund_id))
            ->addColumn('contrib_month_formatted', function($booking_interest) {
                return $booking_interest->miss_month_formatted;
            })
            ->editColumn('amount', function($booking_interest) {
                return $booking_interest->amount_formatted;
            })
            ->addColumn('amount_before', function($booking_interest) {
                return number_2_format($booking_interest->amount - $booking_interest->adjust_amount);
            })
            ->addColumn('late_months_before', function($booking_interest) {
                return ($booking_interest->old_late_months) ? $booking_interest->old_late_months : ' ';
            })
            ->addcolumn('amount_paid', function($booking_interest) {
                return  number_format(($this->employers->getTotalInterestPaidForBookingId($booking_interest->booking_id)), 2 , '.' , ',');
            })
            ->addcolumn('refunded_amount', function($booking_interest) {
                return ($booking_interest->refund_amount) ? number_2_format($booking_interest->refund_amount) : ' ';
            })
            ->addcolumn('rctno', function($booking_interest) {
                return $booking_interest->rctno_label;
            })
            ->rawColumns(['rctno'])
            ->make(true);
    }




    public function getInterestRefundOverviewForDataTable($employer_id) {
        return Datatables::of($this->booking_interests->getInterestRefundOverviewForDataTable($employer_id))
            ->addColumn('status', function ($interest_refund) {
                return $interest_refund->status_label;
            })
            ->addColumn('created_at_formatted', function ($interest_refund) {
                return  $interest_refund->created_at_formatted;

            })
            ->addColumn('amount_formatted', function ($interest_refund) {
                return  $interest_refund->amount_formatted;
            })

            ->rawColumns(['status'])
            ->make(true);
    }



    /*Get Interest eligible to be refunded */
    public function getInterestEligibleForRefundingForDataTable($employer_id) {
        $employer = $this->employers->findOrThrowException($employer_id);
        return Datatables::of($this->booking_interests->getInterestEligibleForRefundingForDataTable($employer))
            ->addColumn('contrib_month_formatted', function($booking_interest) {
                return $booking_interest->miss_month_formatted;
            })
            ->editColumn('amount', function($booking_interest) {
                return $booking_interest->amount_formatted;
            })
            ->addColumn('amount_before', function($booking_interest) {
                return number_2_format($booking_interest->amount - $booking_interest->adjust_amount);
            })
            ->addColumn('late_months_before', function($booking_interest) {
                return ($booking_interest->old_late_months) ? $booking_interest->old_late_months : ' ';
            })
            ->addcolumn('amount_paid', function($booking_interest) {
                return  number_format(($this->employers->getTotalInterestPaidForBookingId($booking_interest->booking_id))
                    , 2
                    , '.' , ',');
            })
            ->addcolumn('remain_amount', function($booking_interest) {
                return number_format(($booking_interest->amount - $this->employers->getTotalInterestPaidForBookingId($booking_interest->booking_id)), 2 , '.' , ',');
            })
            ->addcolumn('rctno', function($booking_interest) {
                return $booking_interest->rctno_label;
            })

            ->rawColumns(['rctno'])
            ->make(true);
    }

    /*Get Interest eligible to modified for certain refund batch (Edit Interest Refund) */
    public function getInterestEligibleForModifyRefundForDataTable($employer_id, $interest_refund_id) {
        $employer = $this->employers->findOrThrowException($employer_id);
        return Datatables::of($this->booking_interests->getInterestEligibleForModifyRefundForDataTable($employer,$interest_refund_id))
            ->addColumn('contrib_month_formatted', function($booking_interest) {
                return $booking_interest->miss_month_formatted;
            })
            ->editColumn('amount', function($booking_interest) {
                return $booking_interest->amount_formatted;
            })
            ->addColumn('amount_before', function($booking_interest) {
                return number_2_format($booking_interest->amount - $booking_interest->adjust_amount);
            })
            ->addColumn('late_months_before', function($booking_interest) {
                return ($booking_interest->old_late_months) ? $booking_interest->old_late_months : ' ';
            })
            ->addcolumn('amount_paid', function($booking_interest) {
                return  number_format(($this->employers->getTotalInterestPaidForBookingId($booking_interest->booking_id)), 2 , '.' , ',');
            })
            ->addcolumn('remain_amount', function($booking_interest) {
                return number_format(($booking_interest->amount - $this->employers->getTotalInterestPaidForBookingId($booking_interest->booking_id)), 2 , '.' , ',');
            })
            ->addcolumn('rctno', function($booking_interest) {
                return $booking_interest->rctno_label;
            })

            ->addcolumn('actions', function($booking_interest) use($interest_refund_id) {
                return ($booking_interest->booking_interest_refund_id) ? $booking_interest->getRemoveRefundButtonAttribute($interest_refund_id) : $booking_interest->getAddRefundButtonAttribute($interest_refund_id) ;
            })


            ->rawColumns(['rctno', 'actions'])
            ->make(true);
    }


    public function updateBankDetails($interest_refund_id, Request $request)
    {
        $this->booking_interests->updateBankDetails($interest_refund_id, $request->all());
        return redirect('/compliance/booking_interest/refund_profile/' . $interest_refund_id . '#bank_details')->withFlashSuccess(trans('alerts.backend.claim.bank_details_updated'));
    }





    /*End of Interest Refund --------------end--------------*/




    /**
     * Check if can Initiate Action (Level 1 / No Workflow)
     */
    public function checkLevel1Rights($resource_id,$wf_module_group_id){
        access()->hasWorkflowDefinition($wf_module_group_id,1);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id]])->checkIfCanInitiateAction(1);
    }


    /**
     * @param $employer_id
     * Recalculate Interest
     */
    public function recalculateInterestForEmployer($employer_id)
    {
        (new CalculateInterest())->calculateInterest($employer_id);
        return redirect()->route('backend.compliance.employer.profile', $employer_id)->withFlashSuccess('Success, has been recalculated');
    }



}
