<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Claim\CreateEmployeeOldCompensationRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateEmployeeRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateEmploymentHistoryRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateIndividualContributionRequest;
use App\Repositories\Backend\Access\PermissionGroupRepository;
use App\Repositories\Backend\Access\RoleRepository;
use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Claim\IncidentTypeRepository;
use App\Repositories\Backend\Operation\Claim\InsuranceRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentTypeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\GenderRepository;
use App\Repositories\Backend\Sysdef\IdentityRepository;
use App\Repositories\Backend\Sysdef\JobTitleRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Melihovv\Base64ImageDecoder\Base64ImageDecoder;
use Melihovv\Base64ImageDecoder\Base64ImageEncoder;


class EmployeeController extends Controller
{

    protected $employees;
    protected $employers;
    protected $insurances;
    protected $job_titles;
    protected $incident_types;
    protected $countries;
    protected $regions;
    protected $districts;
    protected $code_values;
    protected $genders;
    protected $location_types;
    protected $identities;

    public function __construct(EmployeeRepository $employees, EmployerRepository $employers, InsuranceRepository $insurances, JobTitleRepository $job_titles, IncidentTypeRepository $incident_types) {
        $this->employees = $employees;
        $this->employers = $employers;
        $this->insurances = $insurances;
        $this->job_titles = $job_titles;
        $this->incident_types = $incident_types;
        $this->countries = new CountryRepository();
        $this->regions = new RegionRepository();
        $this->districts = new DistrictRepository();
        $this->code_values = new CodeValueRepository();
        $this->genders = new GenderRepository();
        $this->location_types = new LocationTypeRepository();
        $this->identities = new IdentityRepository();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend.operation.compliance.member.employee.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.operation.compliance.member.employee.create')
        ->with("countries", $this->countries->getAll()->pluck('name', 'id'))
        ->with("genders", $this->genders->getAll()->pluck('name', 'id'))
        ->with("location_types", $this->location_types->getAll()->pluck('name', 'id'))
        ->with("job_titles", $this->job_titles->getAll()->pluck('name', 'id'))
        ->with("identities", $this->identities->getAll()->pluck('name', 'id'))
        ->with("employee_categories", $this->code_values->query()->where('code_id',7)->get()->pluck('name', 'id'))
        ->with("marital_statuses", $this->code_values->query()->where('code_id',8)->get()->pluck('name', 'id'))
        ->with("return_url", "");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateEmployeeRequest $request
     * @return mixed
     */
    public function store(CreateEmployeeRequest $request)
    {
        //
        $employee = $this->employees->store($request->all());
        $url = 'compliance/employee/profile/' . $employee->id . '#general';
        if ($request->session()->has('return_url')) {
            $url = $request->session()->get('return_url');
        }
        return redirect($url)->withFlashSuccess(trans('alerts.backend.member.employee_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @param Request $request
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function edit($id, Request $request)
    {
        $return_url = "";
        if ($request->session()->has('return_url')) {
            $return_url = $request->session()->get('return_url');
        }
        $employee = $this->employees->findOrThrowException($id);
        $employersIds = ($employee->employers()->count()) ? $employee->employers->pluck("id")->all() : [];
        $dob = Carbon::parse($employee->dob);
        $basicpay = ($employee->basicpay) ? $employee->basicpay : 0;
        $grosspay = ($employee->grosspay) ? $employee->grosspay : 0;
        $employers = $employee->employers()->select([DB::raw("employers.id as id")])->get()->pluck("id")->toArray();
        $allowance = $grosspay - $basicpay;
        $check_if_in_payroll = $employee->pensioners()->where('isactive', '<>', 0)->count();
        return view('backend.operation.compliance.member.employee.edit')
        ->with("employee", $employee)
        ->with("countries", $this->countries->getAll()->pluck('name', 'id'))
        ->with("genders", $this->genders->getAll()->pluck('name', 'id'))
        ->with("location_types", $this->location_types->getAll()->pluck('name', 'id'))
        ->with("job_titles", $this->job_titles->getAll()->pluck('name', 'id'))
        ->with("identities", $this->identities->getAll()->pluck('name', 'id'))
        ->with("employee_categories", $this->code_values->query()->where('code_id',7)->get()->pluck('name', 'id'))
        ->with("marital_statuses", $this->code_values->query()->where('code_id',8)->get()->pluck('name', 'id'))
        ->with("employer_ids", $employersIds)
        ->with("dob", $dob)
        ->with("employers", $this->employers->query()->whereIn("id", $employers)->pluck('name','id'))
        ->with("allowance", $allowance)
        ->with("return_url", $return_url)
        ->with("check_if_in_payroll", $check_if_in_payroll);
    }

    /**
     * @description Update the specified resource in storage.
     * @param CreateEmployeeRequest $request
     * @param $id
     * @return mixed
     */
    public function update(CreateEmployeeRequest $request, $id)
    {
        $input = $request->all();
        $employee = $this->employees->update($id, $input);
        $url = 'compliance/employee/profile/' . $employee->id . '#general';
        if (!empty($input['return_url'])) {
            $url = $input['return_url'];
        }
        return redirect($url)->withFlashSuccess(trans('alerts.backend.member.employee_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * @param $id
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function profile($id)
    {
        $employee = $this->employees->findOrThrowException($id);
        if(!is_null($employee->photo)){
            $photo = stream_get_contents($employee->photo);
            $dataUri = 'data:image/jpeg;base64,'.$photo;
            $decoder = new Base64ImageDecoder($dataUri, $allowedFormats = ['jpeg', 'png', 'gif']);
            $decoder->getFormat(); // 'png', or 'jpeg', or 'gif', or etc.
            $decoder->getDecodedContent(); // base64 decoded raw image bytes.
            $employee_photo=$decoder->getContent(); // base64 encoded raw image bytes.
            return view('backend.operation.compliance.member.employee.profile')
            ->with("employee", $employee)->with("employee_photo",$employee_photo);

        }else{
            $employee_photo=null;
            return view('backend.operation.compliance.member.employee.profile')
            ->with("employee", $employee)->with("employee_photo",$employee_photo);

        }

    }

    /*
     * Get All Active Employees for dataTable
     */
    public function get() {

        return Datatables::of($this->employees->getForDataTable())


//            ->addColumn('name', function ($employee) {
//                return $employee->name;
//            })
        ->make(true);
    }




    /*
 *
 * CONTRIBUTIONS==========================================
 */

    /**
     * @param $id
     * @return mixed
     *  Get contribution for this  for dataTable
     */
    public function getContributionsForDataTable($id) {
        return Datatables::of($this->employees->getAllContributions($id))
        ->addColumn('paydate_formatted', function($contribution) {
            return $contribution->receiptCode->contrib_month_formatted;
        })
        ->editColumn('grosspay', function($contribution) {
            return $contribution->grosspay_formatted;
        })
        ->editColumn('basicpay', function($contribution) {
            return $contribution->basicpay_formatted;
        })
        ->editColumn('member_amount', function($contribution) {
            return $contribution->member_amount_formatted;
        })
        ->make(true);
    }

    /**
     * @param $id
     * @return mixed
     *  Get contribution for this  for dataTable
     */
    public function getLegacyContributionsForDataTable($id) {
        return Datatables::of($this->employees->getAllLegacyContributions($id))
        ->addColumn('contribution_month_formatted', function($legacyContrib) {
            return $legacyContrib->contrib_month_formatted;
        })
        ->editColumn('salary', function($legacyContrib) {
            return $legacyContrib->basicpay_formatted;
        })
        ->addColumn('gross_pay_formatted', function($legacyContrib) {
            return $legacyContrib->grosspay_formatted;
        })
        ->addColumn('member_amount', function($legacyContrib) {
            return $legacyContrib->member_amount_formatted;
        })
        ->make(true);
    }


    // -end contribution ---------------------------------------

    /*
     * CLAIM METHODS =============================================
     */

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getAccidentNotificationsForDataTable($id) {
        return Datatables::of($this->employees->getAccidentNotifications($id))
        ->editColumn('accident_date', function($accident) {
            return $accident->accident_date_formatted;
        })
        ->editColumn('accident_type_id', function($accident) {
            return $accident->accidentType->name;
        })
        ->addColumn('status', function($accident) {
            return ($accident->notificationReport()->count()) ? $accident->notificationReport->status_label : '';
        })
        ->rawColumns(['status', 'description'])
        ->make(true);
    }

//          Diseases
    public function getDiseaseNotificationsForDataTable($id) {
        return Datatables::of($this->employees->getDiseaseNotifications($id))
        ->addColumn('status', function($disease) {
            return  ($disease->notificationReport()->count()) ? $disease->notificationReport->status_label : ' ';
        })
        ->editColumn('diagnosis_date', function($disease) {
            return $disease->diagnosis_date_formatted;
        })
        ->editColumn('reporting_date', function($disease) {
            return $disease->reporting_date_formatted;
        })
        ->rawColumns(['status'])
        ->make(true);
    }





//          Deaths
    public function getDeathNotificationForDatatable($id) {
        return Datatables::of($this->employees->getDeathNotification($id))
        ->addColumn('status', function($death) {
            return ($death->notificationReport()->count()) ? $death->notificationReport->status_label : ' ';
        })
        ->editColumn('death_cause_id', function($death) {
            return ($death->deathCause()->count()) ? $death->deathCause->name : ' ';
        })
        ->editColumn('death_date', function($death) {
            return $death->death_date_formatted;
        })
        ->editColumn('reporting_date', function($death) {
            return $death->reporting_date_formatted;
        })
        ->rawColumns(['status'])
        ->make(true);
    }

    /*Get Manual Notifications for dt*/
    public function getManualNotificationsForDataTable($id) {
        return Datatables::of($this->employees->getManualNotificationsForDt($id))
        ->editColumn('case_no', function($query) {
            return  $query->filename;
        })
        ->editColumn('incident_date', function($query) {
            return short_date_format($query->incident_date);
        })
        ->editColumn('reporting_date', function($query) {
            return short_date_format($query->reporting_date);
        })

        ->addColumn('status', function($query) {
            return $query->stage_status_label;
        })
        ->rawColumns(['status'])
        ->make(true);
    }



    //Employment history

    public function getEmploymentHistoryForDataTable($id) {
        return Datatables::of($this->employees->getEmploymentHistories($id))
        ->editColumn('from_date', function($employment_history) {
            return $employment_history->from_date_formatted;
        })
        ->editColumn('to_date', function($employment_history) {
            return $employment_history->to_date_formatted;
        })
        ->editColumn('job_title_id', function($employment_history) {
            return ($employment_history->jobTitle()->count()) ? $employment_history->jobTitle->name : ' ';
        })
        ->addColumn('employer', function($employment_history) {
            return is_null($employment_history->other_employer) ? $employment_history->employer->name : $employment_history->other_employer;
        })
        ->addColumn('insurance', function($employment_history) {
            return is_null($employment_history->other_insurance) ? $employment_history->insurance->name : $employment_history->other_insurance;
        })
        ->make(true);
    }



    //employee old compensations
    public function getOldCompensationsForDataTable($id) {
        return Datatables::of($this->employees->getOldCompensations($id))
        ->editColumn('incident_type_id', function($compensation_history) {
            return ($compensation_history->incidentType()->count()) ? $compensation_history->incidentType->name : ' ';
        })
        ->editColumn('amount', function($compensation_history) {
            return $compensation_history->amount_formatted;
        })
        ->editColumn('payment_date', function($compensation_history) {
            return $compensation_history->payment_date_formatted;
        })
        ->make(true);
    }




    //Dependents
    public function getDependentsForDataTable($id) {
        $dependentType = new DependentTypeRepository();
        return Datatables::of($this->employees->getDependents($id))

        ->editColumn('survivor_gratuity_percent', function($dependent) {
            return $dependent->survivor_gratuity_percent;
        })
        ->editColumn('survivor_pension_percent', function($dependent) {
            return $dependent->survivor_pension_percent;
        })
        ->editColumn('funeral_grant_percent', function($dependent) {
            return $dependent->funeral_grant_percent;
        })
        ->editColumn('dob', function($dependent) {
            return $dependent->dob_formatted;
        })
        ->editColumn('dependent_type_id', function($dependent) use ($dependentType){
            return ($dependent->dependent_type_id) ? $dependentType->query()->where('id',
                $dependent->dependent_type_id)->first()->name : ' ';
        })
        ->addColumn('bank_id', function($dependent) {
            return ($dependent->bank_branch_id) ? $dependent->bankBranch->bank->name : ' ';
        })
        ->editColumn('bank_branch_id', function($dependent) {
            return ($dependent->bank_branch_id) ? $dependent->bankBranch->name : ' ';
        })
        ->addColumn('name', function($dependent) {
            return $dependent->firstname . " " . $dependent->middlename  . " " . $dependent->lastname;
        })
        ->editColumn('other_dependency_type', function($dependent) {
            return ($dependent->other_dependency_type == 1) ? 'Full' :  (($dependent->other_dependency_type == 2) ? 'Partial' : '');
        })

        ->make(true);

    }



    /*
     * Employment History methods =================
     */

    public function createEmploymentHistory($id)
    {
        //

        $employee = $this->employees->findOrThrowException($id);
        return view('backend.operation.compliance.member.employee.employment_history_create')
        ->with('employee',$employee)
        ->with( 'job_titles' ,$this->job_titles->getAll()->pluck('name', 'id'))
        ->with(  'insurance',$this->insurances->getAll()->pluck('name', 'id'));
    }

    /**
     * @param $id
     * @param CreateEmploymentHistoryRequest $request
     * @return mixed
     * Store employment history
     */
    public function storeEmploymentHistory($id, CreateEmploymentHistoryRequest $request)
    {

        $employment_history = $this->employees->storeEmploymentHistory($id,$request);
        return redirect('compliance/employee/profile/' . $id . '#history')->withFlashSuccess(trans('alerts.backend.member.employment_history_created'));
    }


    public function editEmploymentHistory($employment_history_id)
    {
        $employment_history = $this->employees->getEmploymentHistory($employment_history_id);
        return view('backend.operation.compliance.member.employee.employment_history_edit')
        ->with('employment_history' ,$employment_history)
        ->with( 'with' ,$this->job_titles->getAll()->pluck('name', 'id'))
        ->with('employers',$this->employers->getAll()->pluck('name', 'id'))
        ->with('insurance', $this->insurances->getAll()->pluck('name', 'id'));
    }

    public function updateEmploymentHistory($employment_history_id, CreateEmploymentHistoryRequest $request)
    {
        $employment_history = $this->employees->updateEmploymentHistory($employment_history_id,$request);
        return redirect('compliance/employee/profile/' . $employment_history->employee_id . '#history')->withFlashSuccess(trans('alerts.backend.member.employment_history_updated'));
    }


    /**
     * @param $employee_old_compensation_id
     * @return mixed
     *  delete employment history
     */
    public function deleteEmploymentHistory($employment_history_id)
    {
        $employment_history = $this->employees->getEmploymentHistory($employment_history_id);
        $employment_history->delete();
        return redirect('compliance/employee/profile/' . $employment_history->employee_id . '#history')->withFlashSuccess(trans('alerts.backend.member.employment_history_deleted'));

    }
    //--end employment history


    /**
     * Employee Old Compensation (history) method===============================
     *
     */
    public function createOldCompensation($id)
    {
        //

        $employee = $this->employees->findOrThrowException($id);
        return view('backend.operation.claim.employee_old_compensation_create')
        ->with("employee", $employee)
        ->with("incident_types", $this->incident_types->query()->where("isregister", 1)->get()->pluck('name', 'id'))
        ->with("employers", $this->employers->getAll()->pluck('name', 'id'));

    }
//  Store Old compensation
    public function storeOldCompensation($id, CreateEmployeeOldCompensationRequest $request)
    {
        //
        $old_compensation = $this->employees->storeOldCompensation($id,$request);
        return redirect('compliance/employee/profile/' . $id . '#compensation')->withFlashSuccess(trans('alerts.backend.claim.old_compensation_created'));

    }

    //  Edit old compensation
    public function editEmployeeOldCompensation($employee_old_compensation_id)
    {
        $employment_old_compensation = $this->employees->getOldCompensation($employee_old_compensation_id);
        return view('backend.operation.claim.employee_old_compensation_edit')
        ->with("employee_old_compensation", $employment_old_compensation)
        ->with("incident_types", $this->incident_types->getAll()->pluck('name', 'id'));

    }

    public function getRegisterEmployees()
    {
        return $this->employees->getRegisterEmployees(request()->input('q'), request()->input('page'));
    }

    public function getRegisterEmployeesForNotification()
    {
        return $this->employees->getRegisterEmployeesForNotification(request()->input('q'), request()->input('page'));
    }

    public function getRegisterEmployeesForNotificationEmployer()
    {
        return $this->employees->getRegisterEmployeesForNotificationEmployer(request()->input('q'), request()->input('page'), request()->input('employer'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Employers with already approved notification report
     */
    public function getBeneficiaryEmployees()
    {
        return $this->employees->getBeneficiaryEmployees(request()->input('q'));
    }

    //  update old compensation
    public function updateOldCompensation($employee_old_compensation_id, CreateEmployeeOldCompensationRequest $request)
    {
        $employment_old_compensation = $this->employees->updateOldCompensation($employee_old_compensation_id,$request);
        return redirect('compliance/employee/profile/' . $employment_old_compensation->employee_id . '#compensation')->withFlashSuccess(trans('alerts.backend.claim.old_compensation_updated'));
    }

    /**
     * @param $employee_old_compensation_id
     * @return mixed
     *  delete old compensation
     */
    public function deleteEmployeeOldCompensation($employee_old_compensation_id)
    {
        $employment_old_compensation = $this->employees->getOldCompensation($employee_old_compensation_id);
        $employment_old_compensation->delete();
        return redirect('compliance/employee/profile/' . $employment_old_compensation->employee_id . '#compensation')->withFlashSuccess(trans('alerts.backend.claim.old_compensation_deleted'));

    }
    //end old compensation ---------------------------------------------


    // -- Choose incident type===========
    public function chooseIncidentType($id)
    {
        //
        //access()->hasWorkflowDefinition(3,1);
        if ($id == 0) {
            //Choose incident for unregistered employee
            $employee = [];
        } else {
            //Choose incident for registered employee
            $employee = $this->employees->findOrThrowException($id);
        }
        return view('backend.operation.compliance.member.employee.choose_incident_type')
        ->with("employee", $employee)
        //->with("incident_types", $this->incident_types->getAll()->pluck('name', 'id'))
        ->with("incident_types", $this->incident_types->query()->where("isregister", 1)->get()->pluck('name', 'id'))
        ->with("employee_id", $id);
    }
// ------end choose incident type---------

    //-----END CLAIM----------------------------

    /**
     * ADD INDIVIDUAL CONTRIBUTION ============
     */
    /**
     * @param $id
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function addContribution($id)
    {
        $employee = $this->employees->findOrThrowException($id);
        return view('backend.operation.compliance.member.employee.individual_contribution.add')
        ->withEmployee($employee)
        ->withEmployers($this->employers->query()->whereHas('employees' , function($query) use($employee){
            $query->where('employee_id',$employee->id );
        })->get()->pluck('name','id'));
    }

    public function storeContribution($id, CreateIndividualContributionRequest $request)
    {
        $employee = $this->employees->storeContribution($id, $request->all());
        return redirect('compliance/employee/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.member.employee_contribution_created'));
    }

    public function getRemovalDatatable($employee_id)
    {
        return Datatables::of($this->employees->getAllRemovals($employee_id))
        // ->addColumn('contribution_month_formatted', function($legacyContrib) {
        //     return $legacyContrib->contrib_month_formatted;
        // })
        // ->editColumn('salary', function($legacyContrib) {
        //     return $legacyContrib->basicpay_formatted;
        // })
        // ->addColumn('gross_pay_formatted', function($legacyContrib) {
        //     return $legacyContrib->grosspay_formatted;
        // })
        // ->addColumn('member_amount', function($legacyContrib) {
        //     return $legacyContrib->member_amount_formatted;
        // })
        ->make(true);
    }

    /* End of individual contribution ----------*/

}
