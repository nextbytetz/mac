<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\OnlineEmployerVerification;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Validator;
use Log;
use App\DataTables\WorkflowTrackDataTable;
use Dompdf\Dompdf;
use PDFSnappy as PDF2;
use PDF;
use Auth;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use Illuminate\Support\Facades\Input;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;

use Illuminate\Validation\ValidationException;
use App\Repositories\Backend\Operation\Compliance\Member\Online\MergeDeMergeEmployerPayrollRepository;
use App\Http\Requests\Backend\Operation\Compliance\Online\MergeEmployerPayrollsRequest;
use Illuminate\Support\Facades\File;

class EmployerPayrollMergeController extends Controller
{
  use FileHandler, AttachmentHandler;

  protected $employer_merge;

  public function __construct(MergeDeMergeEmployerPayrollRepository $employer_merge)
  {
    $this->employer_merge = $employer_merge;
  }


  public 
  function index($merge_request_id, WorkflowTrackDataTable $workflowtrack)
  {
    $merge_request = $this->employer_merge->findOrThrowException($merge_request_id);
    return view('backend.operation.compliance.member.employer.payroll_merging.profile')
    ->with("merge_request", $merge_request)->with("workflowtrack", $workflowtrack);
  }

  public 
  function store(MergeEmployerPayrollsRequest $request)
  {
    $result = $this->employer_merge->saveNewRequest($request);
    if (is_array($result)) {
      return response()->json($result);
    }
    return !empty($result->id) ? response()->json(['success' => true, 'request_id'=>$result->id]) : response()->json(['success' => false]);
  }

  public function mergeRequests($employer_id)
  {
    $merge_requests = $this->employer_merge->getEmployerRequestsForDatatable($employer_id);
    return DataTables::of($merge_requests)
    ->editColumn('created_at', function($data) {
      return $data->created_at_formatted;
    })->editColumn('user_id', function($data) {
      return $data->user_name;
    })->editColumn('status', function($data) {
      return $data->status_label;
    })
    ->editColumn('wf_done_date', function($data) {
      return $data->approval_date;
    })->rawColumns(['status'])->make(true);
  }

  public
  function mergeEmployerPayroll(Employer $employer)
  {
    return view('backend.operation.compliance.member.employer.merge_payroll')->with('employer',$employer);
  }

  public
  function postMergeEmployerPayroll(MergeEmployerPayrollsRequest $request)
  {
    $result = (new MergeDeMergeEmployerPayrollRepository())->mergeEmployerOnlinePayroll($request);
    return $result ? response()->json(['success' => true]) : response()->json(['success' => false]);
  }

  public
  function unMergeEmployerPayroll(Employer $employer)
  {
    dd($employer);
  }

  public
  function postUnMergeEmployerPayroll(Request $request)
  {
    dd($request->all());
    return response()->json(['success' => true]);
  }

  public function previewDocument($merge_request_id)
  {
    $employer_merge = $this->employer_merge->findOrThrowException($merge_request_id);
    $url =  $this->employer_merge->getAttachmentPathFileUrl($merge_request_id);
    return response()->json(['success' => true, "url" => $url, "name" => 'Request Attachment']);
  }

  public function getPayrollSummary($payroll_merge_id, $payroll_id, $type)
  {
    $data =  $this->employer_merge->getPayrollDetails($payroll_merge_id, $payroll_id);

    if ($type == 2) {
      $html = "<tbody><tr><td>Number Of Employees:</td><td>".number_format($data->employees_after,0)."</td><td style='text-align:right;'>".$this->returnViewDetailsButton($data->employees_after, $type, 'employees',$payroll_id)."</td></tr><tr><td>Number Of Contributions:</td><td>".number_format($data->contributions_after,0)."</td><td style='text-align:right;'>".$this->returnViewDetailsButton($data->contributions_after, $type,'receipts',$payroll_id)."</td></tr><tr><td>Number Of Arrears:</td><td>".number_format($data->arrears_after,0)."</td><td style='text-align:right;'>".$this->returnViewDetailsButton($data->arrears_after, $type,'arrears',$payroll_id)."</td></tr></tbody>";
    } else {
      $html = "<tbody><tr><td>Number Of Employees:</td><td>".number_format($data->number_of_employees,0)."</td><td style='text-align:right;'>".$this->returnViewDetailsButton($data->number_of_employees, $type,'employees',$payroll_id)."</td></tr><tr><td>Number Of Contributions:</td><td>".number_format($data->number_of_contributions,0)."</td><td style='text-align:right;'>".$this->returnViewDetailsButton($data->number_of_contributions, $type,'receipts',$payroll_id)."</td></tr><tr><td>Number Of Arrears:</td><td>".number_format($data->number_of_arrears,0)."</td><td style='text-align:right;'>".$this->returnViewDetailsButton($data->number_of_arrears, $type,'arrears',$payroll_id)."</td></tr></tbody>";
    }

    return response()->json(['success' => true, "summary" => $html ]);
  }

  
  public function returnViewDetailsButton($number_to_check, $type, $field, $payroll_id)
  {
    return !empty($number_to_check) ? "<button class='btn btn-sm btn-info view_summary_details' type=".$type." field=".$field." payroll=".$payroll_id.">View Details</button>" : "";
  }


  public function getViewDetails($payroll_merge_id, $payroll_id, $type, $waht_to_return)
  {
    switch ($waht_to_return) {
      case 'employees':
      $return =  $this->employer_merge->returnPayrollEmployeesDetails($payroll_merge_id, $payroll_id, $type);
      $title = 'Payroll '.$payroll_id.' employees '.($type == 2 ? ' after merging ': ' as of submission date');
      break;
      case 'arrears':
      $return =  $this->employer_merge->returnPayrollArrearsDetails($payroll_merge_id, $payroll_id, $type);
      $title = 'Payroll '.$payroll_id.' arrears '.($type == 2 ? ' after merging ': ' as of submission date');
      break;
      case 'receipts':
      $return =  $this->employer_merge->returnPayrollContributionsDetails($payroll_merge_id, $payroll_id, $type);
      $title = 'Payroll '.$payroll_id.' contributions '.($type == 2 ? ' after merging ': ' as of submission date');
      break;
      default:
      $return = "<thead><tr><th></th></tr></thead><tbody></tbody>";
      $title ='';
      break;
    }
    return response()->json(['success' => true, "details" => $return, 'title'=>$title ]);

  }



}

