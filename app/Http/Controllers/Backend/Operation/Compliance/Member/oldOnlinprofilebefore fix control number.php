<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Models\Operation\Claim\PortalIncident;
use App\Models\Operation\Compliance\Member\EmployerAdvancePayment;
use Illuminate\Http\Request;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\OnlineEmployerVerification;
use App\Repositories\Backend\Api\ClosedBusinessRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Validator;
use Log;
use App\Models\Finance\Bill;
use App\Services\Compliance\Traits\EmployeesExcel;
use App\DataTables\WorkflowTrackDataTable;
use Dompdf\Dompdf;
use PDFSnappy as PDF2;
use PDF;
use Auth;
use App\Repositories\Backend\Workflow\WfTrackRepository;

class OnlineProfileController extends Controller
{
  use FileHandler, AttachmentHandler, EmployeesExcel;

  protected $employers;

  public function __construct(EmployerRepository $employers)
  {
    $this->employers = $employers;

    $this->middleware('access.routeNeedsPermission:view_ades_services', ['only' => ['closedBusiness', 'newEmployer', 'payesdl', 'employerStatus']]);
    $this->middleware('access.routeNeedsPermission:close_business', ['only' => ['getCloseBusiness']]);
    $this->middleware('access.routeNeedsPermission:open_business', ['only' => ['getOpenBusiness']]);
    /*associateEmployers*/
    $this->middleware('access.routeNeedsPermission:merge_employer', ['only' => ['mergeEmployers', 'unMergeEmployers']]);
  }


  public 
  function index($employer_id)
  {
    $employer = $this->employers->findOrThrowException($employer_id);
    if ($employer->isonline == 0) {  
      return redirect()->route("backend.compliance.employer.profile", $employer_id)->withFlashWarning('Employer not registered online');
    }
    $interest_summary = $this->employers->interestPaymentSummary($employer_id);
    $booking_summary = $this->employers->bookingPaymentSummary($employer_id);
    $member_count = $this->employers->getMemberCount($employer_id);

    $pending_bill = Bill::select(['bills.id as b_id', 'bills.*', 'payments.*'])->leftjoin('portal.payments', 'bills.bill_no', '=', 'payments.bill_id')->where('bill_status', '<', 3)->where('bills.expire_date', '>=', Carbon::now()->addDay())->where('bills.employer_id', $employer_id)->first();

    $contribution = $this->returnContribution($employer);

    return view('backend.operation.compliance.member.employer.online_profile')
    ->with("employer", $employer)
    ->with("interest_summary", $interest_summary)
    ->with("booking_summary", $booking_summary)
    ->with("pending_bill", $pending_bill)
    ->with("contribution", $contribution)
    ->with("member_count", $member_count);
  }

  public function getAllUsers()
  {

    $input = request()->all();
    $data = [];
    if(isset($input['q'])){
      $search = strtolower(trim($input['q']));
      $data = DB::table('portal.users')
      ->where('name', 'ilike', '%'.$search.'%')
      ->get();
      if (count($data) < 1) {

       $data = DB::table('portal.users')
       ->where('email', 'ilike', '%'.$search.'%')
       ->get();
       if (count($data) < 1) {
        $data = DB::table('portal.users')
        ->where('phone', 'ilike', '%'.(int)$search.'%')
        ->get();
      }
    }
  }
  return $data;
}

public function getPayrollId($employer_id)
{
 $payroll_ids = DB::table('portal.employer_user')
 ->select('payroll_id','employer_id')
 ->groupBy('payroll_id','employer_id')
 ->where('employer_id',$employer_id)
 ->get();
 return response()->json(array("payroll_id"=>count($payroll_ids)));
}


public function addUser(Request $request, $employer_id)
{
 $rules = [
  'payroll_id' => 'required', 
  'user_id' => 'required', 
];
$message = [
  'user_id.required' => 'Please select user to add', 
  'payroll_id.required' => 'Please select applicable payroll', 
];

$validator = Validator::make($request->all(),$rules, $message);

if ($validator->fails()) {
  return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
}else{
  $user_manage = DB::table('portal.employer_user')->where('user_id',$request->user_id)->where('employer_id',$employer_id)->first();
  if (count($user_manage) > 0) {
   return response()->json(array("errors"=> ['user_id' => 'User is already managing this organisation online']));
 }else{
   $user_manage = DB::table('portal.employer_user')->insert([
    'user_id' => $request->user_id,
    'payroll_id' => $request->payroll_id,
    'employer_id' => $employer_id,
    'mac_added_by' => access()->id(),
    'mac_added_at' =>Carbon::now(),
    'created_at' =>Carbon::now(),
    'updated_at' =>Carbon::now(),
  ]);
   return response()->json(array("success"=>true));
 }
}
}


public 
function returnContribution($organization)
{
  /*return contribution of the current due month or current month only*/
  $last_contrib = DB::table('main.receipt_codes')
  ->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
  ->join('main.employers', 'receipts.employer_id', '=', 'employers.id')
  ->where('receipts.employer_id', '=', $organization->id)
  /* ->where('receipts.payroll_id','=',$payroll_id)*/
  ->where('receipts.iscancelled', '!=', 1)
  ->orderBy('receipt_codes.contrib_month', 'desc')
  ->limit(1)->get();


  $employees = DB::table('main.employee_employer')
  ->where('employer_id', $organization->id)
  /* ->where('payroll_id', $payroll_id)*/
  ->where('deleted_at', null)->get();

  $totalgrosspay = $employees->sum('grosspay');

  if ($organization->employer_category_cv_id == 37) { 
    $contrib_amount = ($totalgrosspay * 0.01);
  } else { 
    $contrib_amount = ($totalgrosspay * 0.005);
  }

  $tdy = Carbon::parse(Carbon::now()->format('28-m-Y'));

  $employees_count = $employees->count();
  if ($last_contrib->count() > 0) { 
    /* he has atleast one contribution*/
    $last_contrib_mnth = Carbon::parse($last_contrib[0]->contrib_month);
    $diff_now_last_contrib_mnth = $last_contrib_mnth->diffInMonths($tdy, false);
    if ($diff_now_last_contrib_mnth >= 2) {
      $contribution = [
        'amount' => $contrib_amount,
        'month' => $tdy->subMonth()->format('F Y'),
        'employees_count' => $employees_count,
        'last_contrib_mnth' => $last_contrib_mnth->format('F Y')
      ];
      return $contribution;
    } elseif ($diff_now_last_contrib_mnth == 1) {
      $contribution = [
        'amount' => $contrib_amount,
        'month' => $tdy->format('F Y'),
        'employees_count' => $employees_count,
        'last_contrib_mnth' => $last_contrib_mnth->format('F Y')
      ];
      return $contribution;
    } else { 
      /*anadaiwa due month*/
      $contribution = [
        'amount' => 0,
        'month' => $last_contrib_mnth->addMonth()->format('F Y'),
        'employees_count' => $employees_count,
        'last_contrib_mnth' => $last_contrib_mnth->format('F Y')
      ];
      return $contribution;
    }

  } else { 
    /* hana mchango hata mmoja*/
    $doc = Carbon::parse($organization->doc)->format('28-m-Y');
    $tdy = Carbon::parse($tdy)->format('28-m-Y');
    if ($tdy == $doc) {
      $contribution = [
        'amount' => $contrib_amount,
        'month' => Carbon::parse($tdy)->format('F Y'),
      ];
    } else {
      $contribution = [
        'amount' => $contrib_amount,
        'month' => Carbon::parse($tdy)->subMonth()->format('F Y'),
      ];
    }
    return $contribution;
  }
}


/*Get Missing contributions months */
public 
function getContributionsArrears($id)
{
 $pending_contributions = DB::table('portal.contribution_arrears')
 ->select('id as arrear_id', 'employer_id', 'contrib_month', 'payroll_id', 'status','bill_id','attachment')
 ->where('status',0)
 ->where('employer_id', '=', $id);

 return Datatables::of($pending_contributions)
 ->editColumn('contrib_month', function ($data) {
  return Carbon::parse($data->contrib_month)->format('M Y');
})->editColumn('status', function ($data) {
  if ($data->status == 0) {
    return '<span class="btn btn-sm btn-warning rounded-0">Arrear Pending</span>';
  } else {
    return $this->arrearBillStatus($data->bill_id);
  }
})->addColumn('payroll_user', function ($data) {
  $user = DB::table('portal.employer_user')
  ->select(['users.name', 'payroll_id'])
  ->join('portal.users', 'employer_user.user_id', '=', 'users.id')
  ->distinct('payroll_id')->where('employer_id', '=', $data->employer_id)->where('payroll_id', '=', $data->payroll_id)->first();
  if (!empty($user->name)) {
   $name = (int)$data->payroll_id == 1 ? ucwords(strtolower($user->name)) : ucwords(strtolower($user->name)).' - '.$data->payroll_id;
   return $name;
 }else{
  return $data->payroll_id;
}
})->addColumn('action', function ($data) {
  $upload_employees = '<button class="btn btn-success btn-sm uploadArrearModalButton" role="button" data-arrear_month="' . Carbon::parse($data->contrib_month)->format('M Y') . '" data-arrear_id="' . $data->arrear_id . '" data-payroll_id="' . $data->payroll_id. '">Upload Employees</button>';
  $already_uploaded = '<button class="btn btn-primary btn-sm" role="button" data-arrear_month="' . Carbon::parse($data->contrib_month)->format('M Y') . '" data-arrear_id="' . $data->arrear_id . '" data-payroll_id="' . $data->payroll_id. '" disabled>Already Uploaded</button>';
  /*logger($data->attachment.' '.$data->arrear_id)*/;
  $upload_employees_btn = is_null($data->attachment) ? $upload_employees : $already_uploaded;
  $upload_employees_btn = '';
  return '<button class="btn btn-danger btn-sm removearrearbtn" role="button" data-arrear_month="' . Carbon::parse($data->contrib_month)->format('M Y') . '" data-arrear="' . $data->arrear_id . '">Clear</button> &nbsp; '.$upload_employees_btn;
})->rawColumns(['action', 'payroll_user','status'])->make(true);
}


public function arrearBillStatus($bill_id)
{
  $bill = Bill::find($bill_id);
  $return = null;
  if (count($bill)) {
    switch ($bill->bill_status) {
      case 2:
      if($bill->expire_date < Carbon::now()->addDay()){
       $return = '<span class="btn btn-sm btn-warning rounded-0">Bill Expired</span>';
     }else{
       $return = '<span class="btn btn-smbtn-info rounded-0">Pending Bill</span>';
     }
     break;
     case 3:
     $return = '<span class="btn btn-smbtn-success rounded-0">Bill Paid &nbsp;</span>';
     break;
     case 4:
     $return = '<span class="btn btn-smbtn-danger rounded-0">Bill Cancelled</span>';
     break;
     default:
     $return = '<span class="btn btn-smbtn-warning rounded-0">Bill Expired</span>';
     break;
   } 
 }
 return $return;
}


public function uploadEmployeesArrear(Request $request, $arrear_id)
{

  $rules = [
    'employee_count' => 'required|numeric', 
    'total_grosspay' => 'required', 
    'attachment' => 'required|mimes:xlsx,xls|max:2000',  
  ];

  $message = [
    'employee_count.required' => 'Number of employees is required', 
    'employee_count.numeric' => 'Please write the total number of employees for this month', 
    'total_grosspay.required' => 'Total grosspay is required', 
    'attachment.required' => 'Please upload an excel (xls,xlsx)', 
    'attachment.mimes' => 'Attachment should be an excel (xls,xlsx)', 
    'attachment.max' => 'Attachment should be an excel (xls,xlsx) with size less than 2MB', 
  ];

  /*logger(print_r($request->all(), true))*/;
  $validator = Validator::make($request->all(),$rules, $message);

  if ($validator->fails()) {
    return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
  }else{
    $arrear  = DB::table('portal.contribution_arrears')->find($arrear_id);

    if (count($arrear) > 0) {
     $request->request->add(['employer_id' => $arrear->employer_id, 'id' => $arrear->id]);

     /* logger(print_r($request->all(), true))*/;

     $response = $this->uploadExcelToDb($request,'arrear',$arrear->payroll_id);
     if(!empty($response)){
      $data =  $response->getData();
    }
    if(!empty($data->errors)){
      return $response;
    } 
    else{

     $fileNameExt = $request->file('attachment')->getClientOriginalName();
     $filename = pathinfo($fileNameExt, PATHINFO_FILENAME);
     $fileNameExt = $request->file('attachment')->getClientOriginalExtension();
     $storeFileName = Carbon::parse($arrear->contrib_month)->format('F Y').'-'.$filename.'.'.$fileNameExt;
     $path = $request->file('attachment')->storeAs('public/payment/arrears/employee_list/'.$arrear->employer_id.'-'.access()->user()->id.'/',$storeFileName); 

     $arrear  = DB::table('portal.contribution_arrears')->where('id',$arrear_id)->update([
      'employee_count' => $request->employee_count,
      'total_grosspay' => str_replace( ',', '', $request->total_grosspay),
      'attachment' => $storeFileName,
      'mac_uploaded_by' => access()->user()->id,
      'mac_uploaded_at' => Carbon::now(),
    ]);

     return response()->json(array("success"=>true));
   }
 } else {
  return response()->json(array("errors"=>['message' => 'Arrear does not exist']));
}

}
}


public function clearArrears(Request $request, $arrear_id)
{
 if ($request->reason_for_remove == 'paid') {
  $response = $this->clearArrearPaid($arrear_id);
} elseif($request->reason_for_remove == 'commencement') {
  $response = $this->clearArrearCommencement($arrear_id);
}elseif($request->reason_for_remove == 'payroll') {
  $response = $this->clearArrearClosedPayroll($arrear_id);
}
return $response;

}


public function clearArrearCommencement($arrear_id)
{
 $arrear_posted = DB::table('portal.contribution_arrears')->where('id', $arrear_id)->first();
 $arrears = DB::table('portal.contribution_arrears')->where('employer_id', $arrear_posted->employer_id)->where('status',0)->where('payroll_id',$arrear_posted->payroll_id)->get();
 $employer = Employer::find($arrear_posted->employer_id);
 $count = 0;
 foreach ($arrears as $arrear) {
  if (Carbon::parse(Carbon::parse($employer->doc)->format('Y-m-d')) > Carbon::parse($arrear->contrib_month)->format('Y-m-d')) {
   DB::table('portal.contribution_arrears')
   ->where('id', $arrear->id)
   ->update([
    'status' => 1,
    'mac_updated_by' => access()->user()->id,
    'updated_at' => Carbon::now(),
    'mac_updated_at' => Carbon::now(),
    'mac_clear_reason' => 'Wrong Commencement',
  ]);

   $count ++;
 }
}
if ($count >= 1) {
  return response()->json(array('success' => ['message' => $count.' arrears have been cleared']));
} else {
  return response()->json(array('errors' => ['message' => 'No arrear to clear before commencement date '.Carbon::parse($employer->doc)->format('d-M-Y')]));
}

}

public 
function clearArrearClosedPayroll($arrear_id)
{
 $arrear_posted = DB::table('portal.contribution_arrears')->where('id', $arrear_id)->first();
 if (count($arrear_posted)) {
  $online_users = DB::table('portal.employer_user')
  ->where('employer_id',$arrear_posted->employer_id)
  ->where('payroll_id',$arrear_posted->payroll_id)
  ->where('ismanage',true)
  ->get();
  if (count($online_users)>0) {
    return response()->json(array('errors' => ['message' => 'Can\'t remove arrear! The Payroll is still active']));
  }else{
   DB::table('portal.contribution_arrears')
   ->where('employer_id',$arrear_posted->employer_id)
   ->where('payroll_id',$arrear_posted->payroll_id)
   ->update([
    'status' => 1,
    'mac_updated_by' => access()->user()->id,
    'updated_at' => Carbon::now(),
    'mac_updated_at' => Carbon::now(),
    'mac_clear_reason' => 'Payroll Closed',
  ]);
   return response()->json(array('success' => ['message' => 'Arrears have been cleared']));
 }
}else{
 return response()->json(array('errors' => ['message' => 'Arrear not found'])); 
}
}


public function clearArrearPaid($arrear_id)
{

  $posted_arrear = DB::table('portal.contribution_arrears')->find($arrear_id);

  if ($posted_arrear) {
   $arrears = DB::table('portal.contribution_arrears')->where('employer_id', $posted_arrear->employer_id)->where('status',0)->where('payroll_id',$posted_arrear->payroll_id)->get();
   $count = 0;
   foreach ($arrears as $clear_arrear) {
    $legacy_exist = $this->isContributionMonthInLegacy($clear_arrear->employer_id, Carbon::parse($clear_arrear->contrib_month)->format('Y-m-28'));
    $receipt_exist = $this->isContributionMonthInReceipt($clear_arrear->employer_id, Carbon::parse($clear_arrear->contrib_month)->format('Y-m-28'), $clear_arrear->payroll_id);
    $legacy_code_exist = $this->isContributionMonthInLegacyCodes($clear_arrear->employer_id, Carbon::parse($clear_arrear->contrib_month)->format('Y-m-28'));

    if ($legacy_exist || $receipt_exist || $legacy_code_exist) {
     DB::table('portal.contribution_arrears')->where('id', $clear_arrear->id)->update([
      'status' => 1,
      'mac_updated_by' => access()->user()->id,
      'updated_at' => Carbon::now(),
      'mac_updated_at' => Carbon::now(),
      'mac_clear_reason' => 'Already Paid']);
     $count ++;
   }
 }

 if ($count >= 1){
  return $count == 1 ? response()->json(array('success' => ['message' => Carbon::parse($posted_arrear->contrib_month)->format('F Y').' arrear has been cleared'])) :
  response()->json(array('success' => ['message' => $count.' arrears have been cleared']));
} else {
 return response()->json(array('errors' => ['message' => 'The employer has no receipt for '.Carbon::parse($posted_arrear->contrib_month)->format('F Y')]));
}
}else {
  return response()->json(array('errors' => ['message' => 'Arrear Not Found']));
}

}



public function ContributionsArrearsform($employer)
{

  /*Add Arrears form */
  $payroll_id = DB::table('portal.employer_user')
  ->select(['users.name', 'payroll_id'])
  ->join('portal.users', 'employer_user.user_id', '=', 'users.id')
  ->distinct('payroll_id')->where('employer_id', '=', $employer)->where('ismanage',true)->orderBy('payroll_id', 'desc')->get();
  $data = $this->employers->findOrThrowException($employer);
  return response()->json(array('employername' => $data->name, 'employer_id' => $data->id, 'payroll' => $payroll_id));

}


public function addContributionsArrears(Request $request, $employer_id)
{

  /*logger(print_r($request->all(), true))*/;
  $rules = [
    'reason_for_adding' => 'required',
    'payrollid' => 'required|numeric',
  ];

  $message = [
    'reason_for_adding.required' => 'Select Reason',
    'payrollid.required' => 'Please select Payroll',
    'payrollid.numeric' => 'Please select Payroll',
  ];

  if (!empty($request->reason_for_adding)) {
    if ($request->reason_for_adding == 'missing_month' || $request->reason_for_adding == 'forgot_employees' ) {
      $rules ['contrib_month'] = 'required';
      $message['contrib_month.required'] = 'contribution month is required';
    }
  }


  $validator = Validator::make($request->all(), $rules, $message);

  if ($validator->fails()) {
    return response()->json(array('errors' => $validator->getMessageBag()->toArray()));

  } else {
    $employer = $this->employers->findOrThrowException($employer_id);
    if ($request->reason_for_adding == 'missing_month' || $request->reason_for_adding == 'forgot_employees') {
     $month = Carbon::parse($request->contrib_month)->format('Y-m-28');
     if (Carbon::parse($employer->doc)->format('Y-m') <= Carbon::parse($month)->format('Y-m')) {
      /*commencement is less or equal*/
      if ($request->reason_for_adding == 'missing_month') {
       $response = $this->addArrearNotPaid($employer_id, $month, $request->payrollid);
     } else {
       $arrear_exist = DB::table('portal.contribution_arrears')->where('employer_id', $employer_id)
       ->where('contrib_month', $month)->where('payroll_id', $request->payrollid)->where('status',0)->first();
       if (count($arrear_exist) < 1) {
        $this->insertArrear($employer_id, $month, $request->payrollid, 'Forgot to pay contributions for some employees');
      }else{
        return response()->json(array('errors' => ['message' =>  Carbon::parse($month)->format('F Y').' arrear already exists']));
      }
    }
  } else {
    /*commencement is greater*/
    return response()->json(array('errors' => ['message' => 'Arrear can not be less than commencement date. Kindly check and rectify']));
  }
} else {
  /*commencement*/
  $response = $this->addArrearCommencement($employer_id);
}

return $response;
}
}


public function addArrearNotPaid($employer_id, $month, $payroll_id)
{
 $receipt_exist = $this->isContributionMonthInReceipt($employer_id, Carbon::parse($month)->format('Y-m-28'), $payroll_id);
 $legacy_exist = $this->isContributionMonthInLegacy($employer_id, Carbon::parse($month)->format('Y-m-28'));
 $legacy_code_exist = $this->isContributionMonthInLegacyCodes($employer_id, Carbon::parse($month)->format('Y-m-28'));

 if (!$receipt_exist && !$legacy_exist && !$legacy_code_exist) {
  $update_arrear = $this->updateContributionMonthInArrear($employer_id, Carbon::parse($month)->format('Y-m-28'), $payroll_id);
  if (!$update_arrear) {
   $this->insertArrear($employer_id, $month, $payroll_id, 'Missed month');
 }
}

return response()->json(array('success' => ['message' => Carbon::parse($month)->format('F Y') . ' Arrear have been added']));
}


public function addArrearCommencement($employer_id)
{
 $online_employers = Employer::join('portal.employer_user', 'employer_user.employer_id', '=', 'employers.id')
 ->select('employers.id', 'employers.doc', 'employer_user.payroll_id')
 ->where('ismanage',true)
 ->where('employers.id', $employer_id)->get();

 foreach ($online_employers as $employer) {

  if (Carbon::parse($employer->doc) < Carbon::parse('2015-07-01')) {
   $doc = Carbon::parse('2015-07-01 00:00:00');
 } else {
   $doc = Carbon::parse($employer->doc);
 }

 $tdy = Carbon::now();
 $diff_now_doc = $tdy->diffInMonths($doc);

 if ($diff_now_doc > 1) {

   $receipt_exist = $this->isContributionMonthInReceipt($employer->id, $doc->format('Y-m-28'), $employer->payroll_id);
   $legacy_exist = $this->isContributionMonthInLegacy($employer->id, $doc->format('Y-m-28'));
   if (!$receipt_exist && !$legacy_exist) {

    $arrear_month = $doc->format('Y-m-28');

    $arrear_exist = $this->updateContributionMonthInArrear($employer->id, $arrear_month, $employer->payroll_id);
    if (!$arrear_exist) {
     $this->insertArrear($employer->id, $arrear_month, $employer->payroll_id,'Commencement date');
   }

 }


 $arrears = $diff_now_doc - 2;
 for ($i = 0; $i < $arrears; $i++) {
  $doc = $doc->addMonth();
  $receipt_exist = $this->isContributionMonthInReceipt($employer->id, $doc->format('Y-m-28'), $employer->payroll_id);
  $legacy_exist = $this->isContributionMonthInLegacy($employer->id, $doc->format('Y-m-28'));
  if (!$receipt_exist && !$legacy_exist) {

   $arrear_exist = $this->isContributionMonthInArrear($employer->id, $doc->format('Y-m-28'), $employer->payroll_id);
   if (!$arrear_exist) {
    $this->insertArrear($employer->id, $doc->format('Y-m-28'), $employer->payroll_id,'Commencement date');
  }
}
}
}
}
return response()->json(array('success' => ['message' => 'Arrears added successfully']));

}


public function updateContributionMonthInArrear($employer_id, $contrib_month, $payroll_id)
{

 $arrear_exist = DB::table('portal.contribution_arrears')->where('employer_id', $employer_id)
 ->where('contrib_month', $contrib_month)->where('payroll_id', $payroll_id)->first();

 if (count($arrear_exist)>0) {
  if (isset($arrear_exist->bill_id)) {
          //check if anapending bill
   $arrear_pending = $this->isMonthInPendingBill($employer_id, $contrib_month, $payroll_id);
   if (!$arrear_pending) {
    $this->resetStatusArrear($arrear_exist);  
  } 
} else {
 $this->resetStatusArrear($arrear_exist);
}
return true;
} else {
  return false;
}

}



public function insertArrear($employer_id, $month, $payroll_id, $reason)
{
 DB::table('portal.contribution_arrears')->insert([
  'employer_id' => $employer_id,
  'payroll_id' => $payroll_id,
  'status' => 0,
  'contrib_month' => Carbon::parse($month)->format('Y-m-28'),
  'mac_added_by' => access()->user()->id,
  'mac_added_at' =>Carbon::now(),
  'created_at' => Carbon::now(),
  'updated_at' => Carbon::now(),
  'mac_add_reason' => $reason,
]);
}


public
function users($employer)
{
  $employer_users = DB::table('portal.users')
  ->join('portal.employer_user', 'users.id', '=', 'employer_user.user_id')
  ->where('employer_id', '=', $employer);

  return Datatables::of($employer_users)->editColumn('user_type_cv_id', function ($data) {
    if ($data->user_type_cv_id == 1) {
      return '<span class="alert alert-light">Authorised</span>';

    } elseif ($data->user_type_cv_id == 2) {

      return '<span class="alert alert-light">Agent</span>';
    }


  })->editColumn('status', function ($data) {
    return ($data->isactive == true && $data->ismanage == true) ? '<span class="btn btn-success btn-sm">Active</span>' : '<span class="btn btn-danger btn-sm">Inactive</span>';
  })
  ->editColumn('name', function ($data) {
    return ucwords(strtolower($data->name));


  })
  ->editColumn('phone', function ($data) {
    return '0' . substr($data->phone, 4);

  })
  ->editColumn('last_login', function ($data) {
    return isset($data->last_login) ? Carbon::parse($data->last_login)->format('M d, Y H:i A') : '';

  })
  ->addColumn('action', function ($data) {
    $b = '<button class="btn btn-success btn-sm btnResetPassword" role="button" data-user="' . $data->user_id . '">Reset Password</button>
    <button class="btn btn-primary btn-sm btnchange" role="button" data-user="' . $data->user_id . '" data-employer="' . $data->employer_id . '">Change</button>';

    return ($data->isactive == true && $data->ismanage == true) ?
    $b . ' <button class="btn btn-danger btn-sm btndeactivate" role="button" data-user="' . $data->user_id . '" data-employer="' . $data->employer_id . '">Deactivate</button>' : $b . ' <a href="#" class="btn btn-info btn-sm">Activate</a>';
  })
  ->rawColumns(['user_type_cv_id', 'status', 'action'])
  ->make(true);

}

public
function returnUser($user_id)
{
  $user = DB::table('portal.users')->find($user_id);
  if (isset($user->name)) {
    return response()->json(array('success' => 'true', 'user' => (array)$user));
  } else {
    return response()->json(array('error' => 'true'));
  }
}


public
function resetPassword(Request $request)
{
  $rules = [
    'user_id' => 'required',
    'reset_password' => 'required|min:7|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
  ];

  $message = [
    'user_id.required' => 'User Unknown',
    'reset_password.required' => 'Fill in the password to reset',
    'reset_password.min' => 'Password must be atleast seven characters with combination of letters and numbers',
    'reset_password.regex' => 'Password must contain letters and numbers!',
  ];
  $validator = Validator::make($request->all(), $rules, $message);

  if ($validator->fails()) {
    return response()->json(array('errors' => $validator->getMessageBag()->toArray()));

  } else {

    $input = $request->all();
    switch ($input['reset_user_type']) {
      case "claim":
      $table = "claims";
      break;
      default:
      $table = "users";
      break;
    }

    DB::table("portal.{$table}")->where('id', $request->user_id)->update([
      'password' => bcrypt($request->reset_password),
      'password_changed_at' => Carbon::parse(Carbon::now())->subMonths(3),
      'password_resetter' => access()->id(),
    ]);

    return response()->json(array('success' => true, 'new_password' => $request->reset_password));
  }
}



public
function changeAccountType($user_id)
{
  /*return form for changing online account type */
  $user = DB::table('portal.users')
  ->where('id', '=', $user_id)
  ->first();

  DB::table('portal.users')
  ->where('id', '=', $user_id)->update([
    'user_type_cv_id' => $user->user_type_cv_id == 1 ? 2 : 1,
    'category_changed_by' => access()->id(),
    'category_changed_at' => Carbon::now(),
  ]);

  return response()->json(array('success' => true));


}


public
function deactivateUserform($user, $employer)
{

  $user_details = DB::table('portal.users')
  ->where('id', '=', $user)
  ->first();
  $employer_user = $this->employers->findOrThrowException($employer);

  return response()->json(array('onlineusername' => $user_details->name, 'email' => $user_details->email, 'phone' => $user_details->phone, 'employername' => $employer_user->name, 'employer_id' => $employer_user->id, 'user' => $user_details->id));

}


public
function deactivateUser(Request $request, $user, $employer)
{

        // Log::info(print_r($request->all(),true));
  $user_organizations = DB::table('portal.employer_user')
  ->where('user_id', '=', $request->user_id)
  ->get();

  /* check number of orgnization user managing */
  if ($user_organizations->count() > 1) {
    /*traditional update way */
    DB::table('portal.employer_user')
    ->where('user_id', '=', $request->user_id)
    ->where('employer_id', '=', $request->employer_id)
    ->update([
      'ismanage' => 0, 
      'deactivate_reason' => $request->reason,
      'mac_deactivated_by' => access()->id(),
      'mac_deactivated_at' => Carbon::now()     
    ]);

  } elseif ($user_organizations->count() == 1) {
    /* deactivate one organization access */
    DB::table('portal.employer_user')
    ->where('user_id', '=', $request->user_id)
    ->where('employer_id', '=', $request->employer_id)
    ->update(['ismanage' => 0, 'deactivate_reason' => $request->reason, 
      'mac_deactivated_by' => access()->id(),'mac_deactivated_at' => Carbon::now()]);

    /*Deactivate user in user table*/
    $deactivate_user_account = DB::table('portal.users')
    ->where('id', '=', $request->user_id)
    ->update(['isactive' => 0, 'mac_deactivated_by' => access()->id(),
      'mac_deactivated_at' => Carbon::now()]);
  }

  return response()->json(array('success' => true));

}



public 
function annualEarnings($employer_id)
{

  $annual_earnings = DB::table('portal.annual_returns_summary as a')
  ->select('e.reg_no','a.fin_year' ,'a.payrollId','a.status','a.created_at','u.name as user_submitted','a.submitted_date', 'u.email', 'u.phone','a.id as id')
  ->join('main.employers as e', 'a.employer_id', '=', 'e.id')
  ->leftjoin('portal.users as u', 'a.user_id', '=', 'u.id')
  ->where('a.employer_id', $employer_id);

  return Datatables::of($annual_earnings)
  ->editColumn('status', function ($data) {
    return ($data->status == 'yes') ? '<span class="btn btn-success btn-sm">Submitted</span>' : '<span class="btn btn-warning btn-sm">Not Submitted</span>';
  })
  ->editColumn('user_submitted', function ($data) {
    return isset($data->user_submitted) ? ucwords(strtolower($data->user_submitted)) : 'N/A';
  })
  ->editColumn('submitted_date', function ($data) {
    return isset($data->submitted_date) ? Carbon::parse($data->submitted_date)->format('M d, Y H:i A') : 'N/A';
  })
  ->addColumn('action', function ($data) {
    return ($data->status == 'yes') ? '<button class="btn btn-danger btn-sm reverseAnnualReturn" role="button" data-fin_year="'.$data->fin_year.'" data-annual_earning_id="' .$data->id. '">Reverse submission</button>' : '<button class="btn btn-danger btn-sm" role="button" disabled readonly>Reverse submission</button>';
  })->rawColumns(['action','status'])
  ->make(true);
}

public function reverseAnnualEarning(Request $request)
{

  $rules = [
    'reverse_reason' => 'required|max:255',
  ];

  $message = [
    'reverse_reason.required' => 'The reversal reason is required',
    'reverse_reason.min' => 'Reason must be less than 255 characters',
  ];
  $validator = Validator::make($request->all(), $rules, $message);

  if ($validator->fails()) {
    return response()->json(array('errors' => $validator->getMessageBag()->toArray()));

  } else {
    DB::table('portal.annual_returns_summary')
    ->where('id',$request->annual_earning_id)
    ->update([
      'user_id' => null, 
      'submitted_date' => null, 
      'status' => 'no',
      'reversed_by' => access()->id(),
      'reversed_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
      'reverse_reason' => $request->reverse_reason    
    ]);
    return response()->json(array('success' => true));
  }

}


public 
function commitments($employer_id)
{

  $commitments_data = DB::table('portal.commitment_requests')
  ->select('commitment_requests.created_at as created_at','commitment_options.name as commitment_type','commitment_requests.status',
    'commitment_requests.id as id','users.firstname','users.middlename','users.lastname','users.email','users.phone',
    DB::raw("CONCAT(users.firstname,' ',users.lastname,' : ',REPLACE(users.phone, '+255', '0')) as user_name"))
  ->join('portal.commitment_options', 'commitment_requests.commitment_option_id', '=', 'commitment_options.id')
  ->join('portal.users', 'commitment_requests.user_id', '=', 'users.id')
  ->where('commitment_requests.employer_id', $employer_id);
  
  return DataTables::of($commitments_data)
  ->editColumn('created_at', function($data) {
    return !empty($data->created_at) ? Carbon::parse($data->created_at)->format('M d, Y H:i A') : ' ';
  })->editColumn('status', function($data) {
    return empty($data->status) ? '<span class="btn btn-warning btn-sm"> Pending </span>' : '<span class="btn btn-success btn-sm"> Attended </span>';
  })
  ->addColumn('action', function($data) {
    return empty($data->status) ? '<span class="btn btn-primary btn-sm update_commitment" data-id="'.$data->id.'"> Update Commitment </span>' : '<span class="btn btn-primary btn-sm view_commitment_resolution" data-id="'.$data->id.'"> View Resolution </span>';
  })
  ->filterColumn('user_name', function($data, $keyword) {
    return $data->whereRaw("CONCAT(users.firstname,' ',users.lastname,' : ',users.phone) ilike ?", ["%{$keyword}%"]);
  })
  ->filterColumn('commitment_type', function($data, $keyword) {
    return $data->whereRaw("commitment_options.name) ilike ?", ["%{$keyword}%"]);
  })->rawColumns(['user_name','status','action'])->make(true);

}


public function viewCommitment($commitment_id)
{

 $commitment = DB::table('portal.commitment_requests')
 ->select('commitment_requests.created_at as created_at','commitment_options.name as commitment_type','employers.name as employer',
  'commitment_requests.id as id','users.firstname','users.middlename','users.lastname','users.email','users.phone','employers.id as employer_id','employers.reg_no',
  DB::raw("CONCAT(users.firstname,' ',users.lastname,' : ',users.phone) as user_name"))
 ->join('portal.commitment_options', 'commitment_requests.commitment_option_id', '=', 'commitment_options.id')
 ->join('portal.users', 'commitment_requests.user_id', '=', 'users.id')
 ->join('main.employers', 'commitment_requests.employer_id', '=', 'employers.id')
 ->where('commitment_requests.id', $commitment_id)->first();

 switch ($commitment->commitment_type) {
   case 'Already paid contribution':
   return view('backend.operation.compliance.member.employer.includes.already_paid_commitment')->with('commitment',$commitment);
   break;
   case 'Book Appointment to visit WCF':
   return view('backend.operation.compliance.member.employer.includes.book_appointment_commitment')->with('commitment',$commitment);
   break;
   case 'Pay contribution arrears before next due date':
   return view('backend.operation.compliance.member.employer.includes.due_date_commitment')->with('commitment',$commitment);
   break;
   default:
   redirect()->back()->withFlashError('Unknown Commitment. Kindly refresh and try again');
   break;
 }

}

public 
function returnUpdateCommitmentModal($commitment_id)
{

  $commitment = DB::table('portal.commitment_requests')->join('portal.commitment_options', 'commitment_requests.commitment_option_id', '=', 'commitment_options.id')->where('commitment_requests.id',$commitment_id)->first();
  switch ($commitment->name) {
   case 'Already paid contribution':
   $options = ['already paid' => 'already paid','partially paid' => 'partially paid','not paid' => 'not paid'];
   break;
   case 'Book Appointment to visit WCF':
   $options = ['confirmed' => 'confirmed','rejected' => 'rejected'];
   break;
   case 'Pay contribution arrears before next due date':
   $options = ['ok' => 'ok'];
   break;
   default:
   $options = [];
   break;
 }

 if (count($options) > 0) {
  $inputs = '<div class="row"><div class="form-group"><div class="col-xs-4"><label for="resolution"><span class="resolution"></span> Commitment resolution:</label></div><div class="col-xs-6"><select name="resolution" id="resolution"  class="form-control">';
  foreach ($options as $key => $value) {
    $inputs .='<option value="'.$value.'"> '.ucwords(strtolower($value)).'</option>';
  }
  $inputs .='</select><span class="error_resolution error_message text-danger hidden"></div></div></div> <div class="row"><div class="form-group"><div class="col-xs-4"><label for="comment"><span class="comment"></span> Comment :</label></div><div class="col-xs-6"><textarea name="comment" id="comment"  class="form-control"></textarea><span class="error_comment error_message text-danger hidden"></div><div>';
  return response()->json(array('success'=>true, 'inputs' =>$inputs, 'name'=>$commitment->name));
}else{
  return response()->json(array('success'=>false));
}
}

public 
function returnCommitmentResolution($commitment_id)
{

  $commitment = DB::table('portal.commitment_requests')->join('portal.commitment_options', 'commitment_requests.commitment_option_id', '=', 'commitment_options.id')->where('commitment_requests.id',$commitment_id)->first();
  return count($commitment) ? response()->json(array('success'=>true, 'resolution' =>$commitment->resolutions,'comment'=>$commitment->comments, 'name'=>$commitment->name)) : response()->json(array('success'=>false));

}



public 
function updateCommitment(Request $request)
{
  $rules = [
    'commitment_id' => 'required', 
    'resolution' => 'required', 
    'comment' => 'required', 
  ];

  $message = [
    'commitment_id.required' => 'Something went wrong! Kindly refresh and try again', 
    'resolution.required' => 'Kindly select applicable resolution', 
    'comment.required' => 'Kindly provide comment', 
  ];

  $validator = Validator::make($request->all(),$rules, $message);

  if ($validator->fails()) {
    return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
  }else{

    DB::table('portal.commitment_requests')
    ->where('id',$request->commitment_id)
    ->update([
      'resolutions' => $request->resolution,
      'comments' => $request->comment,
      'mac_user' => Auth::user()->id,
      'status' => 1,
      'updated_at'=>Carbon::now()
    ]);
    return response()->json(array('success'=>true));
  }
}

public function alreadyPaidCommitmentDatatable($commitment_id)
{
 $commitment = DB::table('portal.commitment_descriptions')
 ->select('contrib_month','payment_date','amount','banks.name as bank')
 ->join('main.banks', 'commitment_descriptions.bank_id', '=', 'banks.id')
 ->where('commitment_descriptions.commitment_request_id', $commitment_id);
 return Datatables::of($commitment)
 ->editColumn('contrib_month', function ($data) {
  return !empty($data->contrib_month) ?  Carbon::parse($data->contrib_month)->format('M Y') : '';
})->editColumn('amount', function ($data) {
  return !empty($data->amount) ? number_format($data->amount,2) : '';
})->editColumn('payment_date', function ($data) {
  return !empty($data->payment_date) ?  Carbon::parse($data->payment_date)->format('j<\s\up>S</\s\up> M, Y') : '';
})->rawColumns(['payment_date'])->make(true)  ; 

}

public function beforeDueDateCommitmentDatatable($commitment_id)
{
 $commitment = DB::table('portal.commitment_descriptions')
 ->select('due_date')->where('commitment_descriptions.commitment_request_id', $commitment_id);
 return Datatables::of($commitment)->editColumn('due_date', function ($data) {
  return !empty($data->due_date) ?  Carbon::parse($data->due_date)->format('j<\s\up>S</\s\up> M, Y') : '';
})->rawColumns(['due_date'])->make(true)  ; 

}

public function appointmentCommitmentDatatable($commitment_id)
{
 $commitment = DB::table('portal.commitment_descriptions')
 ->select('appointment_date','office_id','booking_reason','offices.name as branch')
 ->join('main.offices', 'commitment_descriptions.office_id', '=', 'offices.id')
 ->where('commitment_descriptions.commitment_request_id', $commitment_id);
 return Datatables::of($commitment)->editColumn('appointment_date', function ($data) {
  return !empty($data->appointment_date) ?  Carbon::parse($data->appointment_date)->format('j<\s\up>S</\s\up> M, Y') : '';
})->rawColumns(['appointment_date'])->make(true)  ; 

}

public function installments($employer_id)
{
 $installments = DB::table('main.installment_requests')
 ->select('*','installment_requests.created_at as created_at','installment_requests.id as id')
 ->join('portal.users', 'installment_requests.user_id', '=', 'users.id')
 ->where('employer_id', $employer_id);
 return Datatables::of($installments)
 ->addColumn('user', function ($data) {
  return isset($data->user_id) ? ucwords(strtolower($data->name)) : '';
})->editColumn('created_at', function ($data) {
  return isset($data->created_at) ? Carbon::parse($data->created_at)->format('M d, Y H:i A') : 'N/A';
}) ->editColumn('status', function ($data) {
  switch ($data->status) {
    case 0:
    $v = '<span class="btn btn-warning btn-sm"> Pending </span>';
    break;
    case 1:
    $v = '<span class="btn btn-danger btn-sm"> Rejected </span>';
    break;
    case 2:
    $v = '<span class="btn btn-success btn-sm"> Approved</span>';
    break;
    default:
    break;
  }
  return $v;
})->rawColumns(['status'])
->make(true)  ; 

}

public function showInstallment($installment_id, WorkflowTrackDataTable $workflowTrack)
{
  $installment = DB::table('main.installment_requests')->select('installment_requests.*','installment_requests.created_at as created_at','employers.name as employer','users.name as user','installment_requests.id as id','installment_requests.*','employers.reg_no')->join('portal.users', 'installment_requests.user_id', '=', 'users.id')
  ->join('main.employers', 'installment_requests.employer_id', '=', 'employers.id')
  ->where('installment_requests.id',$installment_id)->first();

  $wfTrack = new WfTrackRepository();

  $wf_definition = $wfTrack->getWfModuleById($installment_id);

  return view('backend/operation/compliance/member/employer/includes/installment_profile')->with('installment',$installment)->with("workflowtrack", $workflowTrack)->with("wf_definition",$wf_definition);

}

public function getPortalDetails($portal_requests){
  $total = 0;
  $empty = array();
  $max = 0;
  $count = 0;

  $contrib_months = [];  

  foreach($portal_requests as $p){
    $total += $p->arrear_amount;
    if ($p->payment_phase > $max) {
      $max = $p->payment_phase;
    }
    $count = $p->track;
    array_push($contrib_months, Carbon::parse($p->contrib_month)->format('Y-m-28'));
  }

  $pay_description = $this->monthDescription($contrib_months);

  foreach($portal_requests as $p){
    if ($p->payment_phase == 1) {
      $first_due_date = Carbon::parse($p->due_date)->format('d F Y');
    }
    if($p->payment_phase == $max){
      $last_due_date = Carbon::parse($p->due_date)->format('d F Y');
    }
  }

  $count_in_words = convert_number_to_words($count);

  $empty['total'] = $total;
  $empty['first_due_date'] = $first_due_date;
  $empty['last_due_date'] = $last_due_date;
  $empty['pay_description'] = $pay_description;
  $empty['count'] = $count;
  $empty['count_in_words'] = $count_in_words;

  $number = $total;
  $locale = 'en_US';
  $fmt = numfmt_create($locale, \NumberFormatter::SPELLOUT);
  $in_words = numfmt_format($fmt, $total);
  $total_numbers_in_words= $in_words;

  $empty['total_numbers_in_words'] = $total_numbers_in_words;

  return $empty;
} 

function curl_get_file_contents($URL)
{
  $c = curl_init();
  curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($c, CURLOPT_URL, $URL);
  $contents = curl_exec($c);
  curl_close($c);
  dd(print_r($contents));

  if ($contents) return $contents;
  else return FALSE;
}

public function mouTemplate($instalment_id)
{
  $installment = DB::table('main.installment_requests')->select('installment_requests.*','installment_requests.created_at as created_at','employers.name as employer','users.name as user','installment_requests.id as id','installment_requests.*','employers.reg_no')->join('portal.users', 'installment_requests.user_id', '=', 'users.id')
  ->join('main.employers', 'installment_requests.employer_id', '=', 'employers.id')
  ->where('installment_requests.id',$instalment_id)->first(); 

  $instalment = DB::table('portal.installment_requests')->where('id',$installment->portal_request_id)
  ->where('mou_agreement','!=',null)->where('is_submitted',true)->first();
  // dd($instalment);
  // dd($instalment->mou_agreement);
  if(isset($instalment->mou_agreement)){
    if ($instalment->mou_agreement !== "" || $instalment->mou_agreement != null) {
      $client = new \GuzzleHttp\Client();
    // $response = $client->request('post','http://portal.test/api/get_mou_files/'.$instalment->id.'/'.env("PORTAL_API_KEY"));
      $appUrl = \Config::get('app.url');
        // dd($appUrl);

      if ($appUrl == "http://127.0.0.1/") {
        $response = $client->request('post','http://192.168.1.9/testmacportal/api/get_mou_files/'.$instalment->id.'/'.env("PORTAL_API_KEY"));
      }
      if ($appUrl == "http://192.168.1.9/") {
        $response = $client->request('post','http://192.168.1.9/testmacportal/api/get_mou_files/'.$instalment->id.'/'.env("PORTAL_API_KEY"));
      }
      if ($appUrl == "http://192.168.1.4/") {
        $response = $client->request('post','http://192.168.1.9/testmacportal/api/get_mou_files/'.$instalment->id.'/'.env("PORTAL_API_KEY"));
      }

      $response = $client->request('post','http://192.168.1.9/testmacportal/api/get_mou_files/'.$instalment->id.'/'.env("PORTAL_API_KEY"));
      $path = $response->getBody();
      return response()->file($path,[
        'Content-Disposition' => str_replace('%name', $instalment->mou_agreement, "inline; filename=\"%name\"; filename*=utf-8''%name")]);
    }

  }else{
   $installment = DB::table('main.installment_requests')->select('installment_requests.*','installment_requests.created_at as created_at','employers.name as employer','users.name as user','installment_requests.id as id','installment_requests.*','employers.reg_no')->join('portal.users', 'installment_requests.user_id', '=', 'users.id')
   ->join('main.employers', 'installment_requests.employer_id', '=', 'employers.id')
   ->where('installment_requests.id',$instalment_id)->first();

   $portal_requests = DB::table('portal.installment_payments')->select('*','instalments.track')
   ->join('portal.contribution_arrears','portal.contribution_arrears.installment_payment_id','=','portal.installment_payments.id')
   ->join('portal.instalments','portal.instalments.id','=','portal.installment_payments.instalment_id')
   ->where('installment_request_id',$installment->portal_request_id)
   ->get();

   $description = str_replace('Installment Payment For', '', $installment->description);
   $employer = Employer::find($installment->employer_id);

   $portal_details = $this->getPortalDetails($portal_requests);
   
   $pdf = PDF::loadView('backend/operation/compliance/member/employer/includes/mou_template/mou_print_template', ['installment'=>$installment,'employer' => $employer,'portal_details' => $portal_details])->setPaper('a4', 'portrait');
   $name = $employer->name."_mou_agreement";
   $name = str_replace(" ", "_", $name) . ".pdf";
   $pdf->getDomPDF()->set_option("enable_php", true);
   return $pdf->stream($name);
 }



}

public function instalmentAgreementTemplate($instalment_id){
  $installment = DB::table('main.installment_requests')->select('installment_requests.*','installment_requests.created_at as created_at','employers.name as employer','users.name as user','installment_requests.id as id','installment_requests.*','employers.reg_no')->join('portal.users', 'installment_requests.user_id', '=', 'users.id')
  ->join('main.employers', 'installment_requests.employer_id', '=', 'employers.id')
  ->where('installment_requests.id',$instalment_id)->first(); 

  $organization = Employer::find($installment->employer_id);

  $employerProfile = $this->returnUserPayrol($installment->user_id,$organization);
  $contact_person = $this->getContactPerson($organization->id,$employerProfile[0]);
  $accountants = DB::table('portal.installment_requests')->select('*')
  ->where('installment_requests.id',$instalment_id)
  ->join('portal.installment_accountants','installment_requests.id','=','installment_accountants.installment_request_id')
  ->get();
  $date_applied = DB::table('portal.installment_requests')->select('created_at')
  ->where('installment_requests.id',$instalment_id)
  ->first();
  if($date_applied){
    $date_created = $date_applied->created_at ? Carbon::parse($date_applied->created_at)->format('d F, Y') : null;
  }else{
    $date_created = null;
  }
  


  $contribution_arrears = $this->returnCommitmentsRawData($organization,$employerProfile[0]);
  $contribution_arrears_simplified = $this->returnCommitments($organization,$employerProfile[0]);
  $months = '';
  $arrear_total = 0;
  foreach ($contribution_arrears as $key => $value) {
    $months .= Carbon::parse($value->contrib_month)->format('F Y').',';
    $arrear_total += $value->arrear_amount;


  }

  $wfTrack = new WfTrackRepository();

  $approval_date = $wfTrack->getWfModuleDgApproval($instalment_id);

  $pdf = PDF2::loadView("backend/operation/compliance/member/employer/includes/mou_template/instalment_agreement",compact('employerProfile','contribution_arrears_simplified','accountants','months','arrear_total','date_created','approval_date'));
  // ->setPaper('a4', 'potrait');
  $name = $employer->name.'installment_agreement_form ';
  $name = str_replace(" ", "_", $name) . ".pdf";
  return $pdf->stream($name);
}


public function transmitterLetterTemplate($instalment_id){

  $installment = DB::table('main.installment_requests')->select('installment_requests.*','installment_requests.created_at as created_at','employers.name as employer','users.name as user','installment_requests.id as id','installment_requests.*','employers.reg_no')->join('portal.users', 'installment_requests.user_id', '=', 'users.id')
  ->join('main.employers', 'installment_requests.employer_id', '=', 'employers.id')
  ->where('installment_requests.portal_request_id',$instalment_id)->first();

  $organization = Employer::find($installment->employer_id);

  $portal_requests = DB::table('portal.installment_payments')->select('*','instalments.track')
  ->join('portal.contribution_arrears','portal.contribution_arrears.installment_payment_id','=','portal.installment_payments.id')
  ->join('portal.instalments','portal.instalments.id','=','portal.installment_payments.instalment_id')
  ->where('installment_request_id',$installment->portal_request_id)
  ->get();

  $description = str_replace('Installment Payment For', '', $installment->description);
  $employer = Employer::find($installment->employer_id);

  $portal_details = $this->getPortalDetails($portal_requests);
  $departure_date = Carbon::parse($installment->letter_date)->format('d F,Y');
  $letter_reference = $installment->letter_reference;
        // dd($portal_details);
  $pdf = PDF2::loadView('backend/operation/compliance/member/employer/includes/mou_template/mou_transmitter_pdf', ['installment'=>$installment,'employer' => $employer,'portal_details' => $portal_details, 'departure_date' => $departure_date, 'letter_reference' => $letter_reference]);
  // ->setPaper('a4', 'portrait');
  $name = $organization->name."_mou_transmitter_letter";
  $name = str_replace(" ", "_", $name) . ".pdf";
  // $pdf->getDomPDF()->set_option("enable_php", true);
  return $pdf->stream($name);
}

public function getWfModuleDepartureDate($instalment_id)
{

  $module = DB::table('main.wf_tracks')->select('forward_date')
  ->join('main.wf_definitions','wf_definitions.id','=','wf_tracks.wf_definition_id')->where('resource_id',$instalment_id)->where('resource_type','App\Models\Operation\Compliance\Member\InstallmentRequest')->where('level',12)->first();
  if($module){
    return $module->forward_date ? Carbon::parse($module->forward_date)->format('d F, Y') : null;  
  }else{
    return null;
  }


}


public function returnCommitmentsRawData($organization, $payroll_id){

  $arrears_contrib = DB::table('portal.contribution_arrears')->select('contribution_arrears.id','payment_option','payment_phase','due_date','arrear_amount','contrib_month','installment_requests.id as request_id')
  ->join('portal.installment_payments','installment_payments.id','=','contribution_arrears.installment_payment_id')
  ->join('portal.installment_requests','installment_requests.id','=','installment_payments.installment_request_id')
  ->where('contribution_arrears.employer_id','=',$organization->id)
  ->where('employee_count', '!=', null )
  ->where('total_grosspay', '!=', null )
  ->where('arrear_amount', '!=', null)
  ->where('contribution_arrears.payroll_id', $payroll_id)
  ->where('installment_payment_id', '!=', null)
  ->where('contribution_arrears.status', 0)   
  ->where('commitment_status', 0)  
  ->get();

  if (count($arrears_contrib) > 0) {
    return $arrears_contrib;
  }
}

public function getContactPerson($organization,$payroll_id)
{
  $contact_person = DB::table('main.staff_employer')
  ->select('email','phone','firstname','lastname')
  ->join('main.users','users.id','=','staff_employer.user_id')
  ->where('employer_id',$organization)
  ->first();

  $contact_persons = array();

  if (count($contact_person) > 0) {
    $contact_persons = [
      'flag' => 1,
      'name' => $contact_person->firstname.' '.$contact_person->lastname,
      'email' => $contact_person->email,
      'phone' => $contact_person->phone
    ];
    return $contact_persons;
  }else{
    $contact_persons = [
      'flag' => 0,
      'name' => 'Call Center',
      'email' => 'helpdesk@wcf.go.tz',
      'phone' => '0800 110028/0800 110029'
    ];
    return $contact_persons;
  }
}

public function returnPayrollId($user_id, $organization_id) {
    // $user = Auth::user();
  $employer_user = DB::table('portal.employer_user')->select('payroll_id')->where('user_id',$user_id)->where('employer_id', $organization_id)->get();

  return isset($employer_user[0]->payroll_id) ? $employer_user[0]->payroll_id : null;
}

public function returnUserPayrol($user_id,$organization) {

 $payrolId = $this->returnPayrollId($user_id,$organization->id);
 $user = DB::table('portal.users')->find($user_id);

 $employer = Employer::select('employers.id','employers.name','reg_no','regions.name as region','box_no','phone')
 ->where('employers.id',$organization->id)
 ->leftJoin('main.regions','regions.id','=','employers.region_id')
 ->first();
 $finyear = DB::table('main.fin_years')
 ->select('name')
 ->orderBy('name','desc')
 ->first();

 $box = !empty($employer->box_no) ? (int) filter_var($employer->box_no, FILTER_SANITIZE_NUMBER_INT) : null;

 $employerProfArray = [
   'id' => $employer->id,
   'name' => $employer->name,
   'registrationNo' => $employer->reg_no,
   'addres' => !empty($box) ? 'P.O BOX '.$box.', '.$employer->region : $employer->region,
   'fin_year' => $finyear->name,
   'phone' => $employer->phone,
   'user_email'=> $user->email,
   'user_title' => $user->user_type_cv_id == 1 ? 'Authorised Personnel (Employer)': 'Agent',
   'contact_person' => $user->name.', '.$user->phone.', '.$user->email,
 ];

 $employerProfile = array();
 $employerProfile[0] = $payrolId;
 $employerProfile[1] = $employerProfArray;

 return $employerProfile;
}

public function saveMouAgreementDate(Request $request, $instalment_id){
  $installment = DB::table('main.installment_requests')->where('installment_requests.id',$instalment_id)
  ->update([
   'legal_id' => auth()->user()->id,
   'agreement_date' => Carbon::parse($request->agreement_date)->format('Y-m-d'),
 ]);

  if($installment){
    return response()->json(array('success'=>'Agreement date updated successfully'));
  }

}

public function saveLetterDetails(Request $request, $instalment_id){
  $installment = DB::table('main.installment_requests')->where('installment_requests.id',$instalment_id)
  ->update([
   'rmo_id' => auth()->user()->id,
   'letter_date' => Carbon::parse($request->letter_date)->format('Y-m-d'),
   'letter_reference' => $request->letter_reference,
 ]);

  if($installment){
    return response()->json(array('success'=>'Letter details updated successfully'));
  }

}

public function monthDescription($month_data)
{
  $return = "";
  $description = "";


  $counter = 0;
  $in_range = 0;
  $loop = 0;
  foreach ($month_data as $value) {
    $loop++;
    $this_date = Carbon::parse($value);
    if ($counter > 0) {
      $past_date = Carbon::parse($month_data[$counter - 1]);
      $diff = abs($this_date->diffInMonths($past_date));
      if ($diff == 1) {
        $in_range = 1;
        if (count($month_data) == $loop) {
                            //Last Loop
          $description = $this->commaTrim($description);
          $description .= "- " . $this_date->format('F Y') . " ";
        }
      } else {
        if ($in_range == 1) {
          $last = substr($description, -2);
          if (strpos($last, ',') !== false) {
            $description = substr($description, 0, -2) . " ";
          }
          $description .= "- " . $past_date->format('F Y') . ", ";
          $in_range = 0;
        }
        $description .= $this_date->format('F Y') . ", ";
      }
    } else {
      $description .= $this_date->format('F Y') . ", ";
    }
    $counter++;
    if (count($month_data) == $loop) {
                    //Last Loop
      $description = $this->commaTrim($description);
    }
  }
  return $description;
}

private function commaTrim($string)
{
  $last = substr($string, -2);
  if (strpos($last, ',') !== false) {
    $string = substr($string, 0, -2) . " ";
  }
  return $string;
}

public function returnCommitments($organization, $payroll_id){

  $arrears_contrib = DB::table('portal.contribution_arrears')->select('contribution_arrears.id','payment_option','payment_phase','due_date','arrear_amount','contrib_month','installment_requests.id as request_id')
  ->join('portal.installment_payments','installment_payments.id','=','contribution_arrears.installment_payment_id')
  ->join('portal.installment_requests','installment_requests.id','=','installment_payments.installment_request_id')
  ->where('contribution_arrears.employer_id','=',$organization->id)
  ->where('employee_count', '!=', null )
  ->where('total_grosspay', '!=', null )
  ->where('arrear_amount', '!=', null)
  ->where('contribution_arrears.payroll_id', $payroll_id)
  ->where('installment_payment_id', '!=', null)
  ->where('contribution_arrears.status', 0)   
  ->where('commitment_status', 0)  
  ->get();
    // dd($arrears_contrib);

  if (count($arrears_contrib) > 0) {
        //check if finished commited
   $max = 1;
   $finished = 0;
   $rawdata = array();
   $data = array();
   $fill_data = array();
   $filtered_data = array();
   $filtered_once = array();
   $filtered_twice = array();
   $filtered_thrice = array();
   $filtered_fourtimes = array();
   $filtered_fivetimes = array();
   $filtered_sixtimes = array();
   $filtered_seventimes = array();
   $filtered_eighttimes = array();
   $filtered_ninetimes = array();
   $filtered_tentimes = array();
   $filtered_eleventimes = array();
   $filtered_twelvetimes = array();
   $commitment_details = array();
   $start_commit_date = array();
   $end_commit_date = array();
   $maxdate = Carbon::now()->format('Y-m-d');
   $mindate = Carbon::now()->format('Y-m-d');

   foreach ($arrears_contrib as $key => $value) {

    if ($value->due_date > $maxdate) {
      $maxdate = $value->due_date;
    }
    if ($value->due_date < $mindate) {
      $mindate = $value->due_date;
    }


    if ($value->payment_phase > $max) {
      $max = $value->payment_phase;
    }
    $payment_option = $value->payment_option;
    $payment_phase = $max;
    $due_date = $value->due_date;
    $arrear_amount = $value->arrear_amount;
    $rawdata[0] = $payment_option;
    $rawdata[1] = $value->payment_phase;
    $rawdata[2] = $due_date;
    $rawdata[3] = $value->contrib_month;
    $rawdata[4] = $arrear_amount;
    $rawdata[5] = $value->id;
    $rawdata[6] = $value->request_id;
    $data[] = $rawdata;

  }


  if ($payment_option == "once" && $payment_phase == 1) {
   $finished = 1;
   $filtered_data['once'] = $data;
   foreach ($data as $key => $value) {

    $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
    $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);
  }

  $filtered_data['once'] = $filtered_once;
}
if ($payment_option == "twice" && $payment_phase == 2) {
  $finished = 1;
  foreach ($data as $key => $value) {
    $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
    $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
    if ($value[1] == 1) {
      $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);
    }
    if ($value[1] == 2) {
      $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
    }

  }

  $filtered_data['once'] = $filtered_once;
  $filtered_data['twice'] = $filtered_twice;

}
if ($payment_option == "thrice" && $payment_phase == 3) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);

  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
}
if ($payment_option == "fourtimes" && $payment_phase == 4) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
}
if ($payment_option == "fivetimes" && $payment_phase == 5) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 5) {
    $filtered_fivetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
$filtered_data['fivetimes'] = $filtered_fivetimes;
}
if ($payment_option == "sixtimes" && $payment_phase == 6) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 5) {
    $filtered_fivetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 6) {
    $filtered_sixtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
$filtered_data['fivetimes'] = $filtered_fivetimes;
$filtered_data['sixtimes'] = $filtered_sixtimes;
}

if ($payment_option == "seventimes" && $payment_phase == 7) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 5) {
    $filtered_fivetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 6) {
    $filtered_sixtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 7) {
    $filtered_seventimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
$filtered_data['fivetimes'] = $filtered_fivetimes;
$filtered_data['sixtimes'] = $filtered_sixtimes;
$filtered_data['seventimes'] = $filtered_seventimes;
}

if ($payment_option == "eighttimes" && $payment_phase == 8) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 5) {
    $filtered_fivetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 6) {
    $filtered_sixtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 7) {
    $filtered_seventimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 8) {
    $filtered_eighttimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
$filtered_data['fivetimes'] = $filtered_fivetimes;
$filtered_data['sixtimes'] = $filtered_sixtimes;
$filtered_data['seventimes'] = $filtered_seventimes;
$filtered_data['eighttimes'] = $filtered_eighttimes;
}
if ($payment_option == "ninetimes" && $payment_phase == 9) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 5) {
    $filtered_fivetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 6) {
    $filtered_sixtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 7) {
    $filtered_seventimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 8) {
    $filtered_eighttimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 9) {
    $filtered_ninetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
$filtered_data['fivetimes'] = $filtered_fivetimes;
$filtered_data['sixtimes'] = $filtered_sixtimes;
$filtered_data['seventimes'] = $filtered_seventimes;
$filtered_data['eighttimes'] = $filtered_eighttimes;
$filtered_data['ninetimes'] = $filtered_ninetimes;
}
if ($payment_option == "tentimes" && $payment_phase == 10) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 5) {
    $filtered_fivetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 6) {
    $filtered_sixtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 7) {
    $filtered_seventimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 8) {
    $filtered_eighttimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 9) {
    $filtered_ninetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 10) {
    $filtered_tentimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
$filtered_data['fivetimes'] = $filtered_fivetimes;
$filtered_data['sixtimes'] = $filtered_sixtimes;
$filtered_data['seventimes'] = $filtered_seventimes;
$filtered_data['eighttimes'] = $filtered_eighttimes;
$filtered_data['ninetimes'] = $filtered_ninetimes;
$filtered_data['tentimes'] = $filtered_tentimes;
}

if ($payment_option == "eleventimes" && $payment_phase == 11) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 5) {
    $filtered_fivetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 6) {
    $filtered_sixtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 7) {
    $filtered_seventimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 8) {
    $filtered_eighttimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 9) {
    $filtered_ninetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 10) {
    $filtered_tentimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 11) {
    $filtered_eleventimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
$filtered_data['fivetimes'] = $filtered_fivetimes;
$filtered_data['sixtimes'] = $filtered_sixtimes;
$filtered_data['seventimes'] = $filtered_seventimes;
$filtered_data['eighttimes'] = $filtered_eighttimes;
$filtered_data['ninetimes'] = $filtered_ninetimes;
$filtered_data['tentimes'] = $filtered_tentimes;
$filtered_data['eleventimes'] = $filtered_eleventimes;
}

if ($payment_option == "twelvetimes" && $payment_phase == 12) {
 $finished = 1;
 foreach ($data as $key => $value) {
  $start_commit_date['start_commit_date'] = Carbon::parse($mindate)->format('F Y');
  $end_commit_date['end_commit_date'] = Carbon::parse($maxdate)->format('F Y');
  if ($value[1] == 1) {
    $filtered_once[] = $this->returnOnceInstalment($fill_data,$value);

  }
  if ($value[1] == 2) {
    $filtered_twice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 3) {
    $filtered_thrice[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 4) {
    $filtered_fourtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 5) {
    $filtered_fivetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 6) {
    $filtered_sixtimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 7) {
    $filtered_seventimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 8) {
    $filtered_eighttimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 9) {
    $filtered_ninetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 10) {
    $filtered_tentimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 11) {
    $filtered_eleventimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
  if ($value[1] == 12) {
    $filtered_twelvetimes[] = $this->returnOnceInstalment($fill_data,$value);
  }
}

$filtered_data['once'] = $filtered_once;
$filtered_data['twice'] = $filtered_twice;
$filtered_data['thrice'] = $filtered_thrice;
$filtered_data['fourtimes'] = $filtered_fourtimes;
$filtered_data['fivetimes'] = $filtered_fivetimes;
$filtered_data['sixtimes'] = $filtered_sixtimes;
$filtered_data['seventimes'] = $filtered_seventimes;
$filtered_data['eighttimes'] = $filtered_eighttimes;
$filtered_data['ninetimes'] = $filtered_ninetimes;
$filtered_data['tentimes'] = $filtered_tentimes;
$filtered_data['eleventimes'] = $filtered_eleventimes;
$filtered_data['twelvetimes'] = $filtered_twelvetimes;
}



$commitment_details[0] = $finished;
$commitment_details[1] = $payment_option;
$commitment_details[2] = $filtered_data;
$commitment_details[3] = $rawdata[6];
$commitment_details[4] = $start_commit_date;
$commitment_details[5] = $end_commit_date;
//is approved
$approved = DB::table('portal.installment_requests')->where([
  'id'=>$rawdata[6],
  'status'=>1
])->first();
if (count($approved) > 0) {
 return $commitment_details;
}else{
  $commitment_details[0] = 'not number';
  return $commitment_details;
}

}else{
 $commitment_details[0] = 'not number';
 return $commitment_details;
}

}


public function returnOnceInstalment($fill_data,$value){

  $fill_data['payment_option'] = $value[0];
  $fill_data['payment_phase'] = $value[1];
  $fill_data['due_date'] = $value[2];
  $fill_data['contrib_month'] = Carbon::parse($value[3])->format('F Y');
  $fill_data['arrear_amount'] = $value[4];
  $fill_data['arrear_id'] = $value[5];
  return $fill_data;

}




}
