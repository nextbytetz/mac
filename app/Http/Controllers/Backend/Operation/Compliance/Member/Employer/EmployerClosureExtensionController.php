<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member\Employer;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Compliance\AttachBusinessClosureDocRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateEmployerClosureRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerClosureExtensionRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerClosureFollowUpRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerOpenRequest;
use App\Http\Requests\Backend\Operation\Compliance\SyncEmployerClosureEofficeRequest;
use App\Http\Requests\Request;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureExtension;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Operation\Compliance\Member\EmployerClosureReopen;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\DataTables\EmployerContributionDataTable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureExtensionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class EmployerClosureExtensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    protected $employer_closure_extension_repo;
    /**
     *Construct Method for this class
     */
    public function __construct()
    {
        $this->employer_closure_extension_repo = new EmployerClosureExtensionRepository();
    }


    public function wfModuleGroupId()
    {
        return $this->employer_closure_extension_repo->getWfModuleGroupId();
    }

    /**
     *Open list all page of EmployerClosureExtension
     */
    public function index()
    {
        return view('backend/operation/compliance/member/employer/closure/extension/index');
    }

    /**
     *Open page for adding new EmployerClosureExtension
     */
    public function create(EmployerClosure $employerClosure)
    {
        return view('backend/operation/compliance/member/employer/closure/extension/create')
            ->with('employer_closure', $employerClosure);
    }

    /**
     *Save new entry for EmployerClosureExtension
     */
    public function store(EmployerClosureExtensionRequest $request)
    {
        $input = $request->all();
        $employerClosureExtension = $this->employer_closure_extension_repo->store($input);
        return redirect()->route('backend.compliance.employer_closure.extension.profile',['employer_closure_extension' => $employerClosureExtension->id])->withFlashSuccess('Success, Entry created');
    }

    /**
     *Open page for modifying existing EmployerClosureExtension
     */
    public function edit(EmployerClosureExtension $employerClosureExtension)
    {
//        dd($employerClosureExtension->employerClosure->employer);
        return view('backend/operation/compliance/member/employer/closure/extension/edit')->with('employer_closure_extension', $employerClosureExtension);
    }

    /**
     *Modify existing EmployerClosureExtension
     */
    public function update(EmployerClosureExtensionRequest $request, EmployerClosureExtension $employerClosureExtension)
    {
        $input = $request->all();
        $employerClosureExtension = $this->employer_closure_extension_repo->update($employerClosureExtension, $input);
        return redirect()->route('backend.compliance.employer_closure.extension.profile',['employer_closure_extension' => $employerClosureExtension->id])->withFlashSuccess(__('Success, Entry updated'));
    }

    /**
     *Overview page to display data of EmployerClosureExtension
     */
    public function profile(EmployerClosureExtension $employerClosureExtension, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $extension_id = $employerClosureExtension->id;
        $employer_closure = ($employerClosureExtension->employer_closure_id) ? $employerClosureExtension->employerClosure : null;
        $type = $this->employer_closure_extension_repo->getWfModuleType($extension_id);
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $extension_id, 'type'=> $type]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $current_wf_track = ($check_workflow) ?  $workflow->currentWfTrack() : null;
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        /*Start:level for letter*/
        $check_if_can_initiate_wf_letter = $this->employer_closure_extension_repo->checkIfCanInitiateLetterWf($current_wf_track);

        $workflow_input = ['resource_id' => $extension_id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $type ];
        /*end workflow*/

        /*Pending tasks*/
        $pending_tasks = $this->employer_closure_extension_repo->getPendingTasksBeforeInitiate($employerClosureExtension);

        /*docs*/
        $docs_attached = $this->employer_closure_extension_repo->getDocsAttached($employerClosureExtension->id);
        $document_data = [
            'attach_route' => '',
            'doc_table' => 'document_employer'
        ];
        /*end docs*/
        return view('backend/operation/compliance/member/employer/closure/extension/profile/profile')
            ->with('employer_closure_extension', $employerClosureExtension)
            ->with(['docs_attached' => $docs_attached, 'document_data' => $document_data, 'pending_tasks' => $pending_tasks])
            ->with(['check_workflow' => $check_workflow, 'check_pending_level1' => $check_pending_level1 , 'workflow_track' => $workflowTrackDataTable, 'workflow_input' => $workflow_input,'check_if_can_initiate_wf_letter'=> $check_if_can_initiate_wf_letter ]);
    }


    /**
     *delete/destroy EmployerClosureExtension
     */
    public function delete(EmployerClosureExtension $employerClosureExtension)
    {
        $employer_closure = ($employerClosureExtension->employer_closure_id) ? $employerClosureExtension->employerClosure : null;
        $employerClosureExtension = $this->employer_closure_extension_repo->delete($employerClosureExtension);
        return redirect()->route('backend.compliance.employer.closure.profile', $employer_closure->id)->withFlashSuccess(__('alert.general.deleted'));
    }



    /**
     * Initiate extension approval
     */
    public function initiateApproval(EmployerClosureExtension $employerClosureExtension)
    {
        $input = request()->all();
        $wf_module_group_id = $this->wfModuleGroupId();

        access()->hasWorkflowDefinition($wf_module_group_id, 1);
//        $this->employer_closure_open_repo->checkIfCanInitiateApproval($employerClosureReopen->id);
        /*workflow start*/
        $type = $this->employer_closure_extension_repo->getWfModuleType($employerClosureExtension);

        DB::transaction(function () use ($wf_module_group_id, $employerClosureExtension,$input, $type) {
            $extension_id = $employerClosureExtension->id;
            /*initiate*/
            $this->employer_closure_extension_repo->initiateApproval($extension_id);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $extension_id, 'type' => $type], [], ['comments' => $input['comments']]));
        });
        /*end workflow*/
        return redirect()->route('backend.compliance.employer_closure.extension.profile',$employerClosureExtension->id)->withFlashSuccess('Success, Workflow approval has been initiated');
    }


    /**
     *list all EmployerClosureExtension
     */
    public function getAllForDt()
    {
        $result_list = $this->employer_closure_extension_repo->getAllForDt();
        return DataTables::of($result_list)
            ->addIndexColumn()
            ->rawColumns([''])
            ->make(true);
    }




}
