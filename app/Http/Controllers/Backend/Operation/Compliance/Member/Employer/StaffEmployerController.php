<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member\Employer;


use App\Exceptions\WorkflowException;
use App\Http\Controllers\Backend\Operation\Compliance\Member\Employer\Traits\StaffEmployer\StaffEmployerDtTrait;
use App\Http\Controllers\Backend\Operation\Compliance\Member\Employer\Traits\StaffEmployer\StaffEmployerFollowupsTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Compliance\StaffEmployerAllocationNewStaffRequest;
use App\Http\Requests\Backend\Operation\Compliance\StaffEmployerAllocationRequest;
use App\Http\Requests\Backend\Operation\Compliance\StaffEmployerAllocationUpdateRequest;
use App\Http\Requests\Backend\Operation\Compliance\StaffEmployerAllocationUploadRequest;
use App\Http\Requests\Backend\Operation\Compliance\StaffEmployerFollowUpRequest;
use App\Http\Requests\Backend\Operation\Compliance\StaffEmployerFollowUpReviewRequest;
use App\Jobs\Compliance\StaffEmployerAllocation\StaffEmployerAllocationJob;
use App\Models\Auth\User;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use App\Models\Sysdef\Code;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\Traits\StaffEmployerFollowUpRepoTrait;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Services\Compliance\StaffEmployerAllocation\AllocateBonusEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateEmployersForIntern;
use App\Services\Compliance\StaffEmployerAllocation\AllocateLargeEmployers;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Facades\Datatables;

class StaffEmployerController extends Controller
{


    use StaffEmployerDtTrait, StaffEmployerFollowupsTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */


    protected $staff_employer_repo;


    public function __construct() {
        $this->staff_employer_repo = new StaffEmployerRepository();

        $this->middleware('access.routeNeedsPermission:staff_employer_assign', ['only' => ['createAllocationNonLarge','storeAllocationNonLarge', 'createAllocationLarge', 'storeAllocationLarge','createAllocationBonus', 'storeAllocationBonus', 'createAllocationTreasury','storeAllocationTreasury',  'reallocate', 'allocateNewStaff', 'openAllocateNewStaffPage', 'createAllocationStaffRegion', 'createAllocationIntern','storeAllocationIntern']]);
        $this->middleware('access.routeNeedsPermission:staff_employer_approve_allocation', ['only' => ['approveAllocation', 'editAllocation', 'revokeAllocation', 'replaceSmallMediumEmployers', 'allocateMediumEmployersForLargeStaff', 'reallocateBonusEmployersForUnassignedStaff', 'replaceEmployers', 'replacePage', 'removeEmployers']]);
        $this->middleware('access.routeNeedsPermission:review_staff_employer_followup', ['only' => ['openReviewFollowUpPage', 'storeFollowUpReview']]);
//        $this->middleware('access.routeNeedsPermission:refresh_data_compliance_report', ['only' => ['staffEmployerPerformanceDashboard']]);

//        if(env('TESTING_MODE') == 1)
//        {
//            ini_set('display_errors', 1);
//            ini_set('display_startup_errors', 1);
//            error_reporting(E_ALL);
//        }
    }

    public function cutOffDate()
    {
        return '2019-07-01';
    }


    public function index()
    {
        $pending_follow_ups = $this->staff_employer_repo->getPendingFollowUpsForReviewForDt()->where('needs_correction', '<>',1)->count();
        $my_pending_follow_ups = $this->staff_employer_repo->getPendingFollowUpsForReviewForDt()->where('needs_correction', '<>',1)->where('assigned_reviewer', access()->id())->count();
        $my_pending_follow_ups_for_correction = $this->staff_employer_repo->getPendingFollowUpsForReviewForDt()->where('needs_correction', '=',1)->where('staff_employer.user_id', access()->id())->count();
        return view('backend/operation/compliance/member/employer/staff_relationship/menu')
        ->with('pending_follow_ups', $pending_follow_ups)
        ->with('my_pending_follow_ups', $my_pending_follow_ups)
        ->with('my_pending_follow_ups_for_correction', $my_pending_follow_ups_for_correction);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Allocation general configuration
     */
    public function configuration()
    {
        $config = $this->staff_employer_repo->staffEmployerConfig();
        $compliance_staff = (new UserRepository())->query()->get()->pluck('name', 'id');
        $staff_for_large_contrib = isset($config->staff_for_large_contributors) ?  unserialize($config->staff_for_large_contributors) : [];
        $intern_staff = isset($config->intern_staff) ?  unserialize($config->intern_staff) : [];
        $compliance_officers = isset($config->compliance_officers) ?  unserialize($config->compliance_officers) : [];
        $staff_for_intern = (new UserRepository())->query()->where('unit_id',15)->where('designation_id','<>', 5)->whereNotIn('id',$compliance_officers)->get()->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/staff_relationship/configuration')
        ->with('staff_employer_config', $config)
        ->with('compliance_staff', $compliance_staff)
        ->with('staff_for_large_contrib', $staff_for_large_contrib)
        ->with('compliance_officers', $compliance_officers)
        ->with('intern_staff', $intern_staff)
        ->with('staff_for_intern', $staff_for_intern);
    }


    /*Update configuration*/
    public function  updateConfiguration()
    {
        $input = request()->all();
        $this->staff_employer_repo->updateStaffEmployerConfig($input);
        return redirect()->route('backend.compliance.employer.staff_relation.index')->withFlashSuccess('Success, Staff employer configuration updated.');
    }


    /*Create allocation for staff - employer non large contributors*/
    public function createAllocationNonLarge()
    {
        $config = $this->staff_employer_repo->staffEmployerConfig();
        $staff_for_large = ($config->staff_for_large_contributors != null) ?  unserialize($config->staff_for_large_contributors) : [];
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $compliance_staff = (new UserRepository())->query()->whereIn('id', $compliance_officers)->whereNotIn('id', $staff_for_large)->get()->pluck('name', 'id');
        $compliance_staff_ids = (new UserRepository())->query()->whereIn('id', $compliance_officers)->whereNotIn('id', $staff_for_large)->pluck('id');
        $no_of_staff = (new UserRepository())->query()->whereNotIn('id', $staff_for_large)->count();
        $no_of_employers = $no_of_staff * $config->non_large_employers_per_staff;
        $last_allocation = $this->staff_employer_repo->getRecentAllocationByType(0, 0);
        $cut_off_date = (isset($last_allocation)) ? $last_allocation->end_date : $this->cutOffDate();
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/active_non_large_employers')
        ->with('staff_employer_config', $config)
        ->with('no_of_staff', $no_of_staff)
        ->with('no_of_employers', $no_of_employers)
        ->with('compliance_staff', $compliance_staff)
        ->with('compliance_staff_ids', $compliance_staff_ids)
        ->with('cut_off_date', $cut_off_date);
    }


    /*Store allocation non large*/
    public function storeAllocationNonLarge(StaffEmployerAllocationRequest $request)
    {
        $input = $request->all();
        $allocation_id =   $this->staff_employer_repo->allocateForNonLargeEmployers($input);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $allocation_id)->withFlashSuccess('Success, Staff employer Allocation has been initiated! Process will proceed on the background.');
    }


    /*Create allocation for staff - employer large contributors*/
    public function createAllocationLarge()
    {
        $config = $this->staff_employer_repo->staffEmployerConfig();
        $staff_for_large = ($config->staff_for_large_contributors != null) ?  unserialize($config->staff_for_large_contributors) : [];
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $compliance_staff = (new UserRepository())->query()->whereIn('id', $staff_for_large)->get()->pluck('name', 'id');
        $compliance_staff_ids = (new UserRepository())->query()->whereIn('id', $staff_for_large)->pluck('id');
        $no_of_staff = (new UserRepository())->query()->whereIn('id', $staff_for_large)->count();
        $no_of_employers = $no_of_staff * $config->non_large_employers_per_staff;
        $last_allocation = $this->staff_employer_repo->getRecentAllocationByType(1, 0);
        $cut_off_date = (isset($last_allocation)) ? $last_allocation->end_date :$this->cutOffDate();
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/active_large_employers')
        ->with('staff_employer_config', $config)
        ->with('no_of_staff', $no_of_staff)
        ->with('no_of_employers', $no_of_employers)
        ->with('compliance_staff', $compliance_staff)
        ->with('compliance_staff_ids', $compliance_staff_ids)
        ->with('cut_off_date', $cut_off_date);
    }


    /*Store allocation large*/
    public function storeAllocationLarge(StaffEmployerAllocationRequest $request)
    {
        $input = $request->all();
        $allocation_id =   $this->staff_employer_repo->allocateForLargeEmployers($input);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $allocation_id)->withFlashSuccess('Success, Staff employer Allocation has been initiated! Process will proceed on the background.');
    }



    /*Create allocation for bonus*/
    public function createAllocationBonus()
    {
        $allocate_bonus_serv = new AllocateBonusEmployers();
        $config = $this->staff_employer_repo->staffEmployerConfig();
        $staff_for_large = ($config->staff_for_large_contributors != null) ?  unserialize($config->staff_for_large_contributors) : [];
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $compliance_staff = (new UserRepository())->query()->whereIn('id', $compliance_officers)->whereNotIn('id', $staff_for_large)->get()->pluck('name', 'id');
        $compliance_staff_ids = (new UserRepository())->query()->whereIn('id', $compliance_officers)->whereNotIn('id', $staff_for_large)->pluck('id');

        $intern_staff_ids = ($config->intern_staff != null) ?  unserialize($config->intern_staff) : [];
        $staff_for_intern = (new UserRepository())->query()->where('unit_id',15)->where('designation_id','<>', 5)->whereIn('id',$intern_staff_ids)->get()->pluck('name', 'id');
        $no_of_staff = (new UserRepository())->query()->whereIn('id', $compliance_officers)->count();
        $no_of_employers = $allocate_bonus_serv->dormantEmployersQuery()->count();
        $last_allocation = $this->staff_employer_repo->getRecentAllocationByType(0, 1);
        $cut_off_date = (isset($last_allocation)) ? $last_allocation->end_date :$this->cutOffDate();
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/bonus_employers')
        ->with('staff_employer_config', $config)
        ->with('no_of_staff', $no_of_staff)
        ->with('no_of_employers', $no_of_employers)
//            ->with('compliance_staff', $compliance_staff)
        ->with('staff_for_intern', $staff_for_intern)
        ->with('intern_staff_ids', $intern_staff_ids)
        ->with('compliance_staff_ids', $compliance_staff_ids)
        ->with('compliance_staff', $compliance_staff)
        ->with('cut_off_date', $cut_off_date);

    }


    /*Store allocation bonus*/
    public function storeAllocationBonus(StaffEmployerAllocationRequest $request)
    {
        $input = $request->all();
        $allocation_id =   $this->staff_employer_repo->allocateForBonusEmployers($input);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $allocation_id)->withFlashSuccess('Success, Staff employer Allocation has been initiated! Process will proceed on the background.');
    }



    /*Create allocation for staff - employer -TREASURY EMPLOYERS paid through hazina*/
    public function createAllocationTreasury()
    {
        $config = $this->staff_employer_repo->staffEmployerConfig();
        $staff_for_large = ($config->staff_for_large_contributors != null) ?  unserialize($config->staff_for_large_contributors) : [];
        $staff_for_large = ($config->staff_for_large_contributors != null) ?  unserialize($config->staff_for_large_contributors) : [];
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $compliance_staff = (new UserRepository())->query()->whereIn('id', $compliance_officers)->whereNotIn('id', $staff_for_large)->get()->pluck('name', 'id');
        $last_allocation = $this->staff_employer_repo->getRecentAllocationByType(0, 2);
        $cut_off_date = (isset($last_allocation)) ? $last_allocation->end_date : $this->cutOffDate();
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/treasury_employers')
        ->with('compliance_staff', $compliance_staff)
//            ->with('compliance_staff_ids', $compliance_staff_ids)
        ->with('cut_off_date', $cut_off_date);
    }


    /*Store allocation non large*/
    public function storeAllocationTreasury(StaffEmployerAllocationRequest $request)
    {
        $input = $request->all();
        $allocation_id =   $this->staff_employer_repo->allocateTreasuryEmployers($input);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $allocation_id)->withFlashSuccess('Success, Staff employer Allocation has been initiated! Process will proceed on the background.');
    }


    /*Create employer allocation for intern staff*/
    public function createAllocationIntern()
    {
        $config = $this->staff_employer_repo->staffEmployerConfig();
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $staff_for_intern = (new UserRepository())->query()->where('unit_id',15)->where('designation_id','<>', 5)->whereNotIn('id',$compliance_officers)->get()->pluck('name', 'id');
        $intern_staff = ($config->intern_staff) ?  unserialize($config->intern_staff) : [];
        $last_allocation = $this->staff_employer_repo->getRecentByCategory(5);
        $cut_off_date = (isset($last_allocation)) ? $last_allocation->end_date : $this->cutOffDate();
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/allocate_intern_staff')
        ->with('staff_for_intern', $staff_for_intern)
        ->with('intern_staff', $intern_staff)
        ->with('cut_off_date', $cut_off_date);
    }


    /*Store allocation for intern staff*/
    public function storeAllocationIntern(StaffEmployerAllocationRequest $request)
    {
        $input = $request->all();
        $allocation_id =   $this->staff_employer_repo->allocateForIntern($input);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $allocation_id)->withFlashSuccess('Success, Staff employer Allocation has been initiated! Process will proceed on the background.');
    }


    /*Allocate new staff/employee to employers when allocation is on progress*/
    public function openAllocateNewStaffPage($staff_employer_allocation_id)
    {
        $config = $this->staff_employer_repo->staffEmployerConfig();
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $users = (new UserRepository())->query()->whereIn('id', $compliance_officers)->get()->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/allocate_new_staff')
        ->with('users', $users)
        ->with('staff_employer_allocation_id', $staff_employer_allocation_id);
    }


    public function allocateNewStaff(StaffEmployerAllocationNewStaffRequest $request)
    {
        $input = $request->all();
        $staff_employer_allocation_id = $input['staff_employer_allocation_id'];
        $this->staff_employer_repo->allocateNewStaff($staff_employer_allocation_id, $input);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $staff_employer_allocation_id)->withFlashSuccess('Success, New Staff employer Allocation has been completed.');
    }


    /*Allocations list*/
    public function allocationsList()
    {
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/index');
    }


    /*allocation profile*/
    public function allocationProfile(StaffEmployerAllocation $staffEmployerAllocation)
    {
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/profile/profile')
        ->with('staff_employer_allocation', $staffEmployerAllocation);
    }

    /*approve staff employer allocation*/
    public function approveAllocation(StaffEmployerAllocation $staffEmployerAllocation)
    {

        $this->staff_employer_repo->approveAllocation($staffEmployerAllocation);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $staffEmployerAllocation->id)->withFlashSuccess('Success, Staff employer Allocation has been approved');
    }
    /*Undo staff employer allocation*/
    public function undoAllocation(StaffEmployerAllocation $staffEmployerAllocation)
    {
        $this->staff_employer_repo->undoAllocation($staffEmployerAllocation);
        return redirect()->route('backend.compliance.employer.staff_relation.index')->withFlashSuccess('Success, Staff employer Allocation has been undone');
    }

    /*Revoke allocations*/
    public function revokeAllocation(StaffEmployerAllocation $staffEmployerAllocation)
    {
        $this->staff_employer_repo->revokeAllocation($staffEmployerAllocation);
        return redirect()->route('backend.compliance.employer.staff_relation.index')->withFlashSuccess('Success, Staff employer Allocation has been approved');
    }

    /*Edit allocation*/
    public function editAllocation(StaffEmployerAllocation $staffEmployerAllocation)
    {
        $cut_off_date = $staffEmployerAllocation->start_date;
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/edit_allocation')
        ->with('staff_employer_allocation', $staffEmployerAllocation)
        ->with('cut_off_date', $cut_off_date);
    }


    /*update allocation*/
    public function updateAllocation(StaffEmployerAllocation $staffEmployerAllocation, StaffEmployerAllocationUpdateRequest $request)
    {
        $input = $request->all();
        $this->staff_employer_repo->updateAllocation($staffEmployerAllocation, $input);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $staffEmployerAllocation->id)->withFlashSuccess('Success, Staff employer Allocation has been updated');
    }


    /**
     * @param StaffEmployerAllocation $staffEmployerAllocation
     * Remove this staff from this allocation
     */
    public function removeStaffFromAllocation()
    {
        $input = request()->all();
        $this->staff_employer_repo->removeStaffFromAllocation($input['user_id'], $input['staff_employer_allocation_id']);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile',  $input['staff_employer_allocation_id'])->withFlashSuccess('Success, Staff has been removed');
    }


    /**
     * @return mixed
     * RE-ALLOCATION
     */
    public function reallocationPage()
    {
        $allocation_categories = StaffEmployerAllocation::query()->where('isactive', 1)->where('end_date', '>', standard_date_format(Carbon::now()))->get()->pluck('title','category');
//        $users = (new UserRepository())->query()->where('unit_id',15)->where('designation_id','<>', 5)->get()->pluck('name', 'id');
        $config = DB::table('staff_employer_configs')->first();
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $intern_staff = ($config->intern_staff != null) ?  unserialize($config->intern_staff) : [];
        $all_staff_arr = array_merge($compliance_officers, $intern_staff);
        $users = (new UserRepository())->query()->whereIn('id', $all_staff_arr)->get()->pluck('name', 'id');
        $employer_contrib_categories = DB::table('employer_contributing_categories')->orderBy('id')->pluck('name', 'id');
        $regions = Region::query()->orderBy('name')->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/reallocation/reallocate')
        ->with('allocation_categories', $allocation_categories)
        ->with('employer_contrib_categories', $employer_contrib_categories)
        ->with('users', $users)
        ->with('regions', $regions);
    }


    /*Reallocate staff to employer*/
    public function reallocate()
    {
        $input = request()->all();
        $this->staff_employer_repo->reallocateStaff($input);
        return response()->json(['success' => true, 'message' => 'Success, user have been assigned to the selected employer(s)']);
    }


    /*Complete Allocation check date*/
    public function completeAllocation(StaffEmployerAllocation $staffEmployerAllocation)
    {
        $this->staff_employer_repo->completeAllocation($staffEmployerAllocation);
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $staffEmployerAllocation->id)->withFlashSuccess('Success, Staff employer Allocation has been completed');
    }


    /*Get staff allocations fro reallocations*/
    public function getStaffAllocationsForReallocateForDt()
    {
        return Datatables::of($this->staff_employer_repo->getStaffAllocationsForReallocateForDt())

        ->editColumn('allocation_start_date', function ($allocation) {
            return  isset($allocation->allocation_start_date) ? short_date_format($allocation->allocation_start_date) : '';
        })
        ->editColumn('allocation_end_date', function ($allocation) {
            return  isset($allocation->allocation_end_date) ? short_date_format($allocation->allocation_end_date) : '';
        })
        ->make(true);
    }


    /*end of Reallocation*/


    /*Get staff employer allocation for Datatable*/
    public function getStaffEmployerAllocationsForDataTable()
    {
        return Datatables::of($this->staff_employer_repo->getStaffEmployerAllocationsForDataTable()->whereRaw("(staff_employer_allocations.isactive = 1  or (staff_employer_allocations.isactive = 0 and staff_employer_allocations.isapproved = 0 ))")->orderBy('start_date', 'desc'))

        ->addColumn('start_date_formatted', function ($allocation) {
            return short_date_format($allocation->start_date);
        })
        ->addColumn('end_date_formatted', function ($allocation) {
            return short_date_format($allocation->end_date);
        })
        ->editColumn('no_of_employers', function ($allocation) {
            return number_0_format($allocation->no_of_employers);
        })
        ->editColumn('no_of_staff', function ($allocation) {
            return number_0_format($allocation->no_of_staff);
        })
        ->addColumn('approval_status', function ($allocation) {
            return ($allocation->isapproved == 0) ?  $allocation->approval_status :   $allocation->approval_status  . ' '. $allocation->status_label;
        })
        ->editColumn('run_status', function ($allocation) {
            return $allocation->run_status_label;
        })
        ->addColumn('title', function ($allocation) {
            return $allocation->title;
        })
        ->rawColumns(['approval_status', 'run_status'])
        ->make(true);
    }


    /**
     * @param StaffEmployerAllocation $staffEmployerAllocation
     * @return mixed
     * Get staff allocated by allocation for datatable
     */
    public function getStaffAllocatedByAllocationForDataTable($staff_employer_allocation_id)
    {
        $staff_employer_allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
        $isbonus = $staff_employer_allocation->isbonus;
        return Datatables::of($this->staff_employer_repo->getStaffAllocatedByAllocationForDataTable($staff_employer_allocation_id)->orderBy('firstname', 'desc'))
        ->addColumn('staff_employer_allocation_id', function ($user) use($staff_employer_allocation_id) {
            return $staff_employer_allocation_id;
        })
        ->addColumn('fullname', function ($user) {
            return $user->name;
        })
        ->addColumn('no_of_employers', function ($user) use($staff_employer_allocation_id) {
            return $user->getNoOfRelationEmployers($staff_employer_allocation_id);
        })
        ->addColumn('no_of_employers_active', function ($user) use($staff_employer_allocation_id) {
            return $this->staff_employer_repo->getNoOfAllocatedEmployersForStaffByStatus($staff_employer_allocation_id,$user->id, 1);
        })
        ->addColumn('no_of_employers_dormant', function ($user) use($staff_employer_allocation_id) {
            return $this->staff_employer_repo->getNoOfAllocatedEmployersForStaffByStatus($staff_employer_allocation_id,$user->id, 2);
        })
        ->addColumn('no_of_employers_closed', function ($user) use($staff_employer_allocation_id) {
            return $this->staff_employer_repo->getNoOfAllocatedEmployersForStaffByStatus($staff_employer_allocation_id,$user->id, 3);
        })
        ->addColumn('no_of_employers_duplicate', function ($user) use($staff_employer_allocation_id) {
            return $this->staff_employer_repo->getNoOfAllocatedEmployersForStaffDuplicate($staff_employer_allocation_id,$user->id);
        })
        ->addColumn('actions', function ($user) use($isbonus) {
            return '<a href="' . route('backend.compliance.employer.staff_relation.my_employers_for_active_allocations',[$user->id,$isbonus,0 ]) .   '" class="btn btn-xs btn-primary" ><i class="icon fa fa-eye" data-toggle="tooltip" data-placement="top" title="' . 'View' . '"></i> View</a>' . '<br/> <br/>'. '<a href="#" id="' . 'remove_staff'. $user->id . '" class="btn btn-xs btn-secondary remove_staff" ><i class="icon fa fa-edit" data-toggle="tooltip" data-placement="top" title="' . 'Remove Staff' . '"></i> Remove Staff</a>';
        })
        ->rawCOlumns(['actions'])
        ->make(true);
    }








    /**
     * MY EMPLOYER METHODS
     */


    /*Staff employer relation Profile*/
    public function staffEmployerProfile($staff_employer_id)
    {
        $employer_repo = new EmployerRepository();
        $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
        $allocation = StaffEmployerAllocation::query()->find($staff_employer->staff_employer_allocation_id);
        $user = User::query()->find($staff_employer->user_id);
        $employer_id = $staff_employer->employer_id;
        $employer = $employer_repo->find($employer_id);
        $interest_summary =$employer_repo->interestPaymentSummary($employer_id);
        $booking_summary = $employer_repo->bookingPaymentSummary($employer_id);
        $collection_summary = $this->staff_employer_repo->getCollectionSummary($staff_employer_id);
        $general_contrib_summary = $employer_repo->getContribReceivableSummary($employer_id);

        $commitment_request_followups =  $this->commitmentRequestFollowup($employer_id, $staff_employer);
        
        $staff_id = count($staff_employer) ? $staff_employer->user_id : 6;
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/profile/profile')
        ->with('staff_employer', $staff_employer)
        ->with('employer', $employer)
        ->with("interest_summary", $interest_summary)
        ->with("booking_summary", $booking_summary)
        ->with("collection_summary", $collection_summary)
        ->with("general_contrib_summary", $general_contrib_summary)
        ->with('user', $user)
        ->with('commitment_array', $commitment_array)
        ->with('commitment_request_followups', $commitment_request_followups)
        ->with('allocation', $allocation);
    }

    public function createCommitmentArray($commitment_request_followups){
        $array = array();
        foreach ($commitment_request_followups as $c) {
            $array[$c->commitment_request_id] = $c->name.' | '.$this->staff_employer_repo->commitmentStatus($c->status);
        }

        return $array;
    }

    /**
     * @param $user_id
     * @param $isbonus
     * @param $with_contact
     * @return mixed
     * MY EMPLOYER PAGE
     */

    /*Open My employers for active allocation page ok*/
    public function openMyEmployersForActiveAllocationPage($user_id, $isbonus, $with_contact)
    {

        $title =$this->staff_employer_repo->getMyEmployerPageTitle($isbonus);
        return $this->openMyEmployerForActiveAllocationPageMain($user_id, $isbonus, $with_contact,$title, 1 );
    }


    /*Open My employers for active allocation page ok*/
    public function openMyEmployersForGeneralPage($user_id, $dt_script_id)
    {
        $title = '';
        switch($dt_script_id)
        {
            case 2:
            $title = 'My Employers - Not Contributed';
            break;
        }

        return $this->openMyEmployerForActiveAllocationPageMain($user_id, 0, 1,$title, $dt_script_id );
    }

    /*Open My Employers page - MAIN*/
    public function openMyEmployerForActiveAllocationPageMain($user_id, $isbonus, $with_contact, $title, $dt_script_id)
    {

        $no_of_follow_ups = $this->staff_employer_repo->getNoOfFollowUpsForAllocatedEmployers($user_id, $isbonus);
        $user = User::query()->find($user_id);
        $followups_target_data = $this->staff_employer_repo->getTargetFollowupSummaryDataForUser($user_id);
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/index')
        ->with('user_id', $user_id)
        ->with('user', $user)
        ->with('isbonus', $isbonus)
        ->with('with_contact', $with_contact)
        ->with('title', $title)
        ->with('no_of_follow_ups', $no_of_follow_ups)
        ->with('followups_target_data', $followups_target_data)
        ->with('dt_script_id', $dt_script_id);
    }

    /*End of my employer page*/


    /*Mark / un marked  attendance status*/
    public function updateAttendanceStatus($staff_employer_id, $status)
    {
        $this->staff_employer_repo->updateAttendanceStatus($staff_employer_id, $status);
        return redirect('compliance/employer/staff_relation/staff_employer_profile/' . $staff_employer_id)->withFlashSuccess('Success, Attendance status updated');
    }



    /**
     * @param $staff_employer_id
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * FOLLOW UPS METHODS
     */

    /*Create follow ups*/
    public function createFollowUp($staff_employer_id)
    {
        $cv_repo = new CodeValueRepository();
        $employer_repo = new EmployerRepository();
        $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
        /*Check if is owner*/
        $this->staff_employer_repo->checkIfUserIsOwner($staff_employer->user_id);
        $employer_id = $staff_employer->employer_id;
        $employer = $employer_repo->find($employer_id);
        if($staff_employer->isintern == 0){
            $follow_up_types = $cv_repo->query()->where('code_id',9)->orderBy('name', 'asc')->get()->pluck('name','reference');
        }else{
            $cv_refs_intern = $this->staff_employer_repo->getFollowupTypesReferencesByCategory(5);
            $follow_up_types = $cv_repo->query()->where('code_id',9)->whereIn('reference', $cv_refs_intern)->orderBy('name', 'asc')->get()->pluck('name','reference');
        }

        $commitment_request_followups =  $this->commitmentRequestFollowup($employer_id, $staff_employer);

        if (count($commitment_request_followups)) {
            $commitment_array = collect($this->createCommitmentArray($commitment_request_followups));
        }


        $feedback = $cv_repo->query()->where('code_id',25)->orderBy('name', 'asc')->get()->pluck('name','reference');
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/create')
        ->with('staff_employer', $staff_employer)
        ->with('employer', $employer)
        ->with('commitment_request_followups', $commitment_request_followups)
        ->with('follow_up_types', $follow_up_types)
        ->with('commitment_array', $commitment_array)
        ->with('feedback', $feedback);

    }

    /*Create Follow up - General*/
    public function createGeneralFollowUp($employer_id)
    {
        $cv_repo = new CodeValueRepository();
        $employer_repo = new EmployerRepository();
        $employer = $employer_repo->find($employer_id);
        $follow_up_types = $cv_repo->query()->where('code_id',9)->orderBy('name', 'asc')->get()->pluck('name','reference');
        $feedback = $cv_repo->query()->where('code_id',25)->orderBy('name', 'asc')->get()->pluck('name','reference');
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/create')
        ->with('employer', $employer)
        ->with('follow_up_types', $follow_up_types)
        ->with('feedback', $feedback);

    }

    /*Store follow up*/
    public function storeFollowUp(StaffEmployerFollowUpRequest $request)
    {
        $input = $request->all();
        $this->staff_employer_repo->storeFollowUp($input);
        if(isset($input['staff_employer_id'])){
            return redirect('compliance/employer/staff_relation/staff_employer_profile/' . $input['staff_employer_id'] . '#followups')->withFlashSuccess('Success, Follow up has been created');
        }else{
            return redirect('compliance/employer/profile/' . $input['employer_id'])->withFlashSuccess('Success, Follow up has been created');
        }

    }

    /*Store follow up- Multiple*/
    public function storeFollowUpMultiple(StaffEmployerFollowUpRequest $request)
    {
        $input = $request->all();
        $this->staff_employer_repo->storeFollowUp($input);
        return redirect('compliance/employer/staff_relation/follow_up/add_multiple_page?date_of_follow_up=' .short_date_format( $input['date_of_follow_up']))->withFlashSuccess('Success, Follow up has been created');

    }
    /*Edit follow up*/
    public function editFollowUp($follow_up_id)
    {
        $cv_repo = new CodeValueRepository();
        $employer_repo = new EmployerRepository();
        $follow_up = StaffEmployerFollowUp::query()->find($follow_up_id);
        $staff_employer = null;
        if(isset($follow_up->staff_employer_id)){
            $staff_employer = DB::table('staff_employer')->where('id', $follow_up->staff_employer_id)->first();
            /*Check if is owner*/
            $this->staff_employer_repo->checkIfUserIsOwner($staff_employer->user_id);
            $employer_id = $staff_employer->employer_id;

        }else{
            $employer_id = $follow_up->employer_id;
        }
        $employer = $employer_repo->find($employer_id);
        /*Start: Follow up types*/
        if($staff_employer->isintern == 0){
            $follow_up_types = $cv_repo->query()->where('code_id',9)->orderBy('name', 'asc')->get()->pluck('name','reference');
        }else{
            $cv_refs_intern = $this->staff_employer_repo->getFollowupTypesReferencesByCategory(5);
            $follow_up_types = $cv_repo->query()->where('code_id',9)->whereIn('reference', $cv_refs_intern)->orderBy('name', 'asc')->get()->pluck('name','reference');
        }

        $feedback = $cv_repo->query()->where('code_id',25)->orderBy('name', 'asc')->get()->pluck('name','reference');
        /*Get document*/
        $follow_up_type = CodeValue::query()->find($follow_up->follow_up_type_cv_id);
        $document_id = $this->staff_employer_repo->getDocumentIdByFollowUpType($follow_up_type);
        $document_attached = (isset($document_id)) ? $this->staff_employer_repo->getUploadedDoc($follow_up_id, $document_id) : null;

        $commitment_request_followups = $this->commitmentRequestFollowup($employer_id, $staff_employer);

        if (count($commitment_request_followups)) {
            $commitment_array = collect($this->createCommitmentArray($commitment_request_followups));
        }

        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/edit')
        ->with('staff_employer', $staff_employer)
        ->with('employer', $employer)
        ->with('follow_up', $follow_up)
        ->with('follow_up_types', $follow_up_types)
        ->with('feedback', $feedback)
        ->with('commitment_array', $commitment_array)
        ->with('document_attached', $document_attached);
    }

    public function commitmentRequestFollowup($employer_id, $staff_employer){
        $commitment_request_followups =  DB::table('main.staff_employer_commitment_totals')
        ->join('portal.commitment_requests','portal.commitment_requests.id','=', 'staff_employer_commitment_totals.commitment_request_id')
        ->join('portal.commitment_options','portal.commitment_options.id','=', 'portal.commitment_requests.commitment_option_id')
        ->where('staff_employer_commitment_totals.employer_id', $employer_id)
        ->where('staff_employer_commitment_totals.user_id', $staff_employer->user_id)->get();

        return $commitment_request_followups;
    }


    /*Update follow up*/
    public function updateFollowUp(StaffEmployerFollowUpRequest $request)
    {
        /*Update follow up*/
        $input = $request->all();
        $this->staff_employer_repo->updateFollowUp($input);
        if(isset($input['staff_employer_id'])){
            return redirect('compliance/employer/staff_relation/staff_employer_profile/' . $input['staff_employer_id'] . '#followups')->withFlashSuccess('Success, Follow up has been updated');
        }else{
            return redirect('compliance/employer/profile/' . $input['employer_id'])->withFlashSuccess('Success, Follow up has been updated');
        }

    }

    /*Delete of follow up*/
    public function deleteFollowup($follow_up_id)
    {
        $follow_up = StaffEmployerFollowUp::find($follow_up_id);
        $this->staff_employer_repo->deleteFollowUp($follow_up_id);
        if(isset($follow_up->staff_employer_id)){
            return redirect('compliance/employer/staff_relation/staff_employer_profile/' . $follow_up->staff_employer_id . '#general')->withFlashSuccess('Success, Follow up has been removed');
        }else{
            return redirect('compliance/employer/profile/' . $follow_up->employer_id)->withFlashSuccess('Success, Follow up has been removed');
        }

    }



    /*Get pending follow ups for review*/
    public function pendingFollowUpsForReview()
    {
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/includes/get_pending_for_review_dt');
    }

    public function openReviewFollowUpPage($follow_up_id)
    {
        $cv_repo = new CodeValueRepository();
        $employer_repo = new EmployerRepository();
        $follow_up = DB::table('staff_employer_follow_ups')->where('id', $follow_up_id)->first();
        $staff_employer = DB::table('staff_employer')->where('id', $follow_up->staff_employer_id)->first();
        $employer_id = $staff_employer->employer_id;
        $employer = $employer_repo->find($employer_id);
        $follow_up_types = $cv_repo->query()->where('code_id',9)->get()->pluck('name','id');
        $feedback = $cv_repo->query()->where('code_id',25)->get()->pluck('name','id');
        $visit_form = $this->staff_employer_repo->getUploadedDoc($follow_up_id, 73);
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/review')
        ->with('staff_employer', $staff_employer)
        ->with('employer', $employer)
        ->with('follow_up', $follow_up)
        ->with('follow_up_types', $follow_up_types)
        ->with('feedback', $feedback)
        ->with('visit_form', $visit_form);
    }


    /*STore review for follow up*/
    public function storeFollowUpReview(StaffEmployerFollowUpReviewRequest $request, $follow_up_id)
    {
        $input = $request->all();
        $this->staff_employer_repo->reviewFollowUp($follow_up_id, $input);
        return redirect('compliance/employer/staff_relation/follow_up/pending_for_review')->withFlashSuccess('Success, Follow up has been reviewed');
    }



    /*Get follow ups for dataTable*/
    public function getStaffEmployerFollowupsForDt($staff_employer_id)
    {
        return Datatables::of($this->staff_employer_repo->getStaffEmployerFollowupsForDt($staff_employer_id))

        ->editColumn('date_of_follow_up', function ($follow_up) {
            return short_date_format($follow_up->date_of_follow_up);
        })
        ->editColumn('date_of_reminder', function ($follow_up) {
            return  (isset($follow_up->date_of_reminder)) ? short_date_format($follow_up->date_of_reminder) : $follow_up->date_of_reminder;
        })
        ->editColumn('user_id', function ($follow_up) {
            return $follow_up->user->name;
        })
        ->addColumn('follow_up_type', function ($follow_up) {
            return $follow_up->followUpType->name;
        })
        ->editColumn('feedback', function ($follow_up) {
            return  (isset($follow_up->feedback_cv_id)) ?  $follow_up->feedback->name : null;
        })
        ->make(true);
    }


    /*Get follow ups by employer for dataTable*/
    public function getStaffEmployerFollowupsByEmployerForDt($employer_id)
    {
        return Datatables::of($this->staff_employer_repo->getStaffEmployerFollowupsByEmployerForDt($employer_id))

        ->editColumn('date_of_follow_up', function ($follow_up) {
            return short_date_format($follow_up->date_of_follow_up);
        })
        ->editColumn('date_of_reminder', function ($follow_up) {
            return  (isset($follow_up->date_of_reminder)) ? short_date_format($follow_up->date_of_reminder) : $follow_up->date_of_reminder;
        })

        ->make(true);
    }


    /*----End of follow ups----*/



    /*Get employers allocated by staff for active allocation datatable*/
    public function getEmployersAllocatedByStaffForActiveAllocationByCategoryDt($user_id,$category)
    {

        return Datatables::of($this->staff_employer_repo->getEmployersAllocatedForStaffActiveAllocationBYCategoryForDt($user_id, $category))

        ->editColumn('allocation_start_date', function ($allocation) {
            return short_date_format($allocation->allocation_start_date);
        })
        ->editColumn('allocation_end_date', function ($allocation) {
            return short_date_format($allocation->allocation_end_date);
        })
//            ->addColumn('receivable_amount', function ($allocation) {
//                $amount = $this->staff_employer_repo->getGeneralContribSummary($allocation->staff_employer_id)['receivable'];
//                return number_2_format($amount);
//            })
        ->editColumn('missing_months', function ($allocation) use($isbonus) {
//                $missing_months = $this->staff_employer_repo->getEmployerMissingMonthsCount($allocation->employer_id);
//                $missing_months = ($isbonus == 1 && $missing_months == 0) ? 'Non contributor' : number_0_format($missing_months);
//                return $missing_months;
            return number_0_format($allocation->missing_months);
        })
        ->addColumn('no_of_follow_ups', function ($allocation) {
//                $no_of_follow_ups = $this->staff_employer_repo->getNoOfFollowUps($allocation->staff_employer_id);
            return number_0_format($allocation->no_of_follow_ups);
        })
        ->editColumn('relation_status', function ($allocation) {
            return ($allocation->relation_status == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Active'. "'>" . 'Not Active' . "</span>" :
            "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active' . "'>" . 'Active' . "</span>";
        })
        ->editColumn('attendance_status', function ($allocation) {
            return ($allocation->attendance_status == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Attended'. "'>" . 'Not Attended' . "</span>" :
            "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Attended' . "'>" . 'Attended' . "</span>";
        })
        ->editColumn('isonline', function ($allocation) {
            return ($allocation->isonline == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Online'. "'>" . 'Not Online' . "</span>" :
            "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Online' . "'>" . 'Online' . "</span>";
        })

        ->editColumn('employer_status', function ($allocation) {
            return ($allocation->employer_status == 1) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active'. "'>" . 'Active' . "</span>" :
            ($allocation->employer_status == 2 ?  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Dormant' . "'>" . 'Dormant' . "</span>" :  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Closed' . "'>" . 'Closed' . "</span>")   ;
        })
        ->rawColumns(['relation_status', 'attendance_status', 'isonline', 'employer_status'])
        ->make(true);
    }



    /*Open page*/
    public function openAllEmployersPage()
    {
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/all_allocated_employers/index');
    }

    /*Export all allowacted employers*/
    public function exportAllAllocatedEmployers()
    {
        $result_list = $this->staff_employer_repo->getAllEmployersAllocatedForDt();
        return $this->staff_employer_repo->exportStaffEmployerGeneral($result_list);
    }


    /*Get pending follow ups for review for datatable*/
    public function getPendingFollowUpsForReviewForDt()
    {
        return Datatables::of($this->staff_employer_repo->getPendingFollowUpsForReviewForDt()->where('needs_correction', '<>', 1))

        ->editColumn('date_of_follow_up', function ($follow_up) {
            return short_date_format($follow_up->date_of_follow_up);
        })
        ->editColumn('assigned_reviewer', function ($follow_up) {
            return ($follow_up->assigned_reviewer);
        })
        ->make(true);
    }


    /*Open my pending follow up for review page*/
    public function assignedPendingFollowUpForReviewPage()
    {
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/review/get_my_pending_for_review_dt');
    }


    /*Get my pending follow ups for review for datatable  for assigned reviewer*/
    public function getMyPendingFollowUpsForReviewForDt()
    {
        return Datatables::of($this->staff_employer_repo->getPendingFollowUpsForReviewForDt()->where('needs_correction', '<>', 1)->where('assigned_reviewer', access()->id()))

        ->editColumn('date_of_follow_up', function ($follow_up) {
            return short_date_format($follow_up->date_of_follow_up);
        })
        ->editColumn('assigned_reviewer', function ($follow_up) {
            return ($follow_up->assigned_reviewer);
        })
        ->make(true);
    }


    /*Open my pending follow up for review page*/
    public function reversedPendingFollowUpsForCorrectionPage()
    {
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/review/get_my_pending_for_correction_dt');
    }



    /*Get my pending follow ups for review for datatable - Reversed to owner for correction*/
    public function getMyPendingFollowUpsForCorrectionForDt()
    {
        return Datatables::of($this->staff_employer_repo->getPendingFollowUpsForReviewForDt()->where('needs_correction', '=', 1)->where('staff_employer.user_id', access()->id()))

        ->editColumn('date_of_follow_up', function ($follow_up) {
            return short_date_format($follow_up->date_of_follow_up);
        })
        ->editColumn('assigned_reviewer', function ($follow_up) {
            return ($follow_up->assigned_reviewer);
        })
        ->make(true);
    }




    /*EXPORT TO EXCEL*/

    /*Export employers for staff allocated employers */
    public function exportToExcelMyEmployers($user_id, $isbonus, $with_contact, $dt_script_id)
    {
        $this->staff_employer_repo->exportToExcelMyEmployers($user_id, $isbonus, $with_contact, $dt_script_id);
        return redirect()->back();
    }

    /*Export employer follow ups*/
    public function exportEmployerFollowUps($staff_employer_id)
    {
        $this->staff_employer_repo->exportEmployerFollowUps($staff_employer_id);
        return redirect()->back();
    }



    /*END of MY employer methods*/




    /**
     * DASHBOARD
     */

    /*Moved to homeController for compliance*/
    public function staffEmployerPerformanceDashboard()
    {
        $staff_performances = $this->staff_employer_repo->getStaffEmployerPerformance();
        $total_summary = $this->staff_employer_repo->getStaffEmployerPerformanceTotalSummary();
        $performance_other_employers = $this->staff_employer_repo->getPerformanceSummaryForOtherEmployers();
        return view('backend/operation/compliance/member/employer/staff_relationship/performance_dashboard/collection_dashboard/index')
        ->with('staff_performances', $staff_performances)
        ->with('total_summary',$total_summary)
        ->with('performance_other_employers', $performance_other_employers);
    }


    /*Staff employer - Bonus/dormant Performance dashboard - KPI*/
    public function staffEmployerBonusPerformanceDashboard()
    {
        $staff_performances = $this->staff_employer_repo->getStaffEmployerDormantPerformance();
        $total_summary = $this->staff_employer_repo->getStaffEmployerDormantPerformanceTotalSummary();
        return view('backend/operation/compliance/member/employer/staff_relationship/performance_dashboard/dormant_performance/index')
        ->with('staff_performances', $staff_performances)
        ->with('total_summary',$total_summary);

    }

    /*Staff employer -Treasury Performance dashboard - KPI*/
    public function staffEmployerTreasuryPerformanceDashboard()
    {
        $staff_performances = $this->staff_employer_repo->getStaffEmployerTreasuryPerformance();
        $total_summary = $this->staff_employer_repo->getStaffEmployerTreasuryPerformanceTotalSummary();
        return view('backend/operation/compliance/member/employer/staff_relationship/performance_dashboard/treasury_performance/index')
        ->with('staff_performances', $staff_performances)
        ->with('total_summary',$total_summary);
    }



    /*Moved to homeController for compliance - Intern*/
    public function staffEmployerPerformanceInternDashboard()
    {
        $staff_performances = $this->staff_employer_repo->getStaffEmployerPerformanceIntern();
        $total_summary = $this->staff_employer_repo->getStaffEmployerPerformanceTotalSummaryIntern();
        return view('backend/operation/compliance/member/employer/staff_relationship/performance_dashboard/collection_intern_dashboard/index')
        ->with('staff_performances', $staff_performances)
        ->with('total_summary', $total_summary);
    }



    public function getStaffEmployerPerformanceForDt()
    {

    }



    /*Preview document*/
    public function previewDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $staff_employer_id = $uploaded_doc->external_id;
        $url = $this->staff_employer_repo->getDocPathFileUrl($doc_pivot_id);
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded_doc->description, "id" => $doc_pivot_id]);

    }



    /**
     * ALLOCATE STAFF FOR REGIONS
     */
    public function createAllocationStaffRegion()
    {
        $regions = Region::query()->orderBy('name')->get()->pluck('name', 'id');
        $config = DB::table('staff_employer_configs')->first();
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $users = User::query()->whereIn('id', $compliance_officers)->orderBy('office_id')->get();

        return view('backend/operation/compliance/member/employer/staff_relationship/allocate_regions/allocate_regions')
        ->with('regions', $regions)
        ->with('users', $users);
    }

    public function storeAllocationStaffRegion()
    {
        $input = request()->all();
        $this->staff_employer_repo->storeAllocationStaffRegion($input);
        return redirect()->route('backend.compliance.employer.staff_relation.index')->withFlashSuccess('Success, Staff region configuration updated.');
    }

    /*Allocate staff for regions*/


    /*Replace small employers with medium and medium plus*/
    public function replaceSmallMediumEmployers($staff_employer_allocation_id)
    {
        dispatch(new StaffEmployerAllocationJob($staff_employer_allocation_id, 6));
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $staff_employer_allocation_id)->withFlashSuccess('Success, Employer replacement process has been initiated.');
    }


    public function allocateMediumEmployersForLargeStaff($staff_employer_allocation_id)
    {
        dispatch(new StaffEmployerAllocationJob($staff_employer_allocation_id, 7));
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $staff_employer_allocation_id)->withFlashSuccess('Success, Employer process has been initiated.');
    }


//Reallocate bonus employers for unassigned staff
    public function reallocateBonusEmployersForUnassignedStaff( $staff_employer_allocation_id)
    {
        $check_if_run = DB::table('staff_employer')->whereNotNull('prev_user_id')->count();
        $user_id = 15;
        $serv = new AllocateBonusEmployers();
        if($check_if_run == 0)
        {
            $serv->reallocateBonusEmployersForUnassignedStaff($user_id, $staff_employer_allocation_id);
        }
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $staff_employer_allocation_id)->withFlashSuccess('Success, Employer process has been initiated.');
    }



    /**
     * REPLACE allocations with MEDIUM employers
     */

    public function replacePage()
    {

//        $users = (new UserRepository())->query()->where('unit_id',15)->where('designation_id','<>', 5)->get()->pluck('name', 'id');
        $config = DB::table('staff_employer_configs')->first();
        $compliance_officers = ($config->compliance_officers != null) ?  unserialize($config->compliance_officers) : [];
        $intern_officers = ($config->intern_staff != null) ?  unserialize($config->intern_staff) : [];
        $all_officers = array_merge($compliance_officers, $intern_officers);
        $users = (new UserRepository())->query()->whereIn('id', $all_officers)->get()->pluck('name_with_no_of_allocated_employers', 'id');
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/replacement/replace_employers')
        ->with('users', $users);
    }


    /*Replace employers with medium active*/
    public function replaceEmployers()
    {
        $input = request()->all();
        $this->staff_employer_repo->replaceEmployers($input);
        return response()->json(['success' => true, 'message' => 'Success, employer(s) have been replaced with Active Medium employers']);
    }

    /*Remove employers - revoke assignment*/
    public function removeEmployers()
    {
        $input = request()->all();
        $this->staff_employer_repo->removeEmployers($input);
        return response()->json(['success' => true, 'message' => 'Success, employer(s) allocations have been revoked/removed']);
    }


    /**
     * @return mixed
     * Allocation for intern bulk excel
     */
    public function openUploadInternAssignmentPage(StaffEmployerAllocation $staffEmployerAllocation)
    {
        $intern_staff = ($staffEmployerAllocation->selected_staff) ? unserialize($staffEmployerAllocation->selected_staff) : [];
        $staff_for_intern = User::query()->whereIn('id', $intern_staff)->get()->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/staff_relationship/allocation/intern_allocation/upload_intern_bulk_assignment')
        ->with('intern_staff', $intern_staff)
        ->with('staff_for_intern', $staff_for_intern)
        ->with('staff_employer_allocation', $staffEmployerAllocation);
    }

    /*Upload Intern Assignment*/
    public function storeUploadInternAssignment(StaffEmployerAllocationUploadRequest $request)
    {
        $input = $request->all();
        $staff_employer_allocation_id = $input['staff_employer_allocation_id'];
        $user = access()->id();
        if ($this->staff_employer_repo->saveBulkAssignmentFileAttachment()) {
            $service = new AllocateEmployersForIntern();
            $service->bulkAssignmentForIntern($input['staff'],$staff_employer_allocation_id );
        }
        return redirect()->route('backend.compliance.employer.staff_relation.allocation_profile', $staff_employer_allocation_id)->withFlashSuccess('Success, Employers has been assigned to intern');
    }




    /*Get staff allocations fro replacement*/
    public function getStaffAllocationsForReplacementForDt()
    {
        return Datatables::of($this->staff_employer_repo->getStaffAllocationsForReplacementForDt())

        ->editColumn('allocation_start_date', function ($allocation) {
            return short_date_format($allocation->allocation_start_date);
        })
        ->editColumn('allocation_end_date', function ($allocation) {
            return short_date_format($allocation->allocation_end_date);
        })
        ->editColumn('employer_status', function ($allocation) {
            return ($allocation->employer_status == 1) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active'. "'>" .  ($allocation->isduplicate == 1 ? 'Duplicate' : 'Active'). "</span>" :
            ($allocation->employer_status == 2 ?  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Dormant' . "'>" . 'Dormant' . "</span>" :  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Closed' . "'>" . 'Closed' . "</span>")   ;
        })
        ->rawColumns(['employer_status'])
        ->make(true);
    }



    /*Open index page missing followups*/
    public function staffMissingFollowupTargetReminderPage()
    {
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/staff_target_reminders/index');
    }


    /*Get Staff Employers by user*/
    public function getStaffEmployersByUserForSelect()
    {
        $input = request()->all();
        return $this->staff_employer_repo->getStaffEmployersByUserForSelect($input['q'], $input['page']);
    }

    /**
     * TEST SIMULATION
     */

    public function openTestSimulation()
    {
        return view('backend/operation/compliance/member/employer/staff_relationship/test_simulation/test_simulation');
    }

    public function testSimulation($type)
    {
        switch($type)
        {
            case 1:

            $this->staff_employer_repo->mainPostAndUpdateWeeklyMissingTargetFollowups();
            break;

            case 2:
            $this->staff_employer_repo->staffCollectionPerformanceCongratEmail(1);
            break;

            case 3:
            $this->staff_employer_repo->staffVerificationCongratEmail();
            break;

            case 4:
            $this->staff_employer_repo->staffBonusCongratEmail();
            break;


            case 5://Inbox & task (temp de-registration alert)
            (new EmployerClosureRepository())->initializeCheckerForAll('EMPDERREOP');
            break;
        }

        return redirect()->back()->withFlashSuccess('Success, simulated (Test)');
    }
}
