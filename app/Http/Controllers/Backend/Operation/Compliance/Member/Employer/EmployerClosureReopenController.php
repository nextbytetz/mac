<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member\Employer;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Compliance\AttachBusinessClosureDocRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateEmployerClosureRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerClosureFollowUpRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerOpenRequest;
use App\Http\Requests\Backend\Operation\Compliance\SyncEmployerClosureEofficeRequest;
use App\Http\Requests\Request;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Operation\Compliance\Member\EmployerClosureReopen;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\DataTables\EmployerContributionDataTable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class EmployerClosureReopenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */

    protected $employer_closure_open_repo;


    public function __construct() {
        $this->employer_closure_open_repo = new EmployerClosureReopenRepository();
    }


    public function wfModuleGroupId()
    {
        return 22;
    }



    public function index(Employer $employer)
    {

        return view('backend/operation/compliance/member/employer/closure/index')
            ->with('employer', $employer);
    }

    /*Create new open - Page*/
    function create(Employer $employer)
    {
        $closure = $employer->getRecentEmployerClosure();
        /*check if can create new*/
        $this->employer_closure_open_repo->checkIfCanCreateNew($closure->id);
        return view("backend/operation/compliance/member/employer/closure/open_business")
            ->with("employer", $employer)
            ->with('closure', $closure);
    }

    /*Store open business*/
    public function store(Employer $employer, EmployerOpenRequest $request)
    {
        $input = $request->all();
        $repo = new EmployerClosureReopenRepository();
        $closure_open =  $repo->postOpenBusiness($input);
        return redirect()->route("backend.compliance.employer_closure.open.profile", $closure_open->id)->withFlashSuccess("Success, Employer business open process has been created");
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployerClosureReopen $employerClosureReopen)
    {
        //
        $this->checkIfHasWfDefinition(1);
        $closure = $employerClosureReopen->employerClosure;
        $employer = $closure->closedEmployer;
        /*workflow*/
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $employerClosureReopen->id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        return view('backend/operation/compliance/member/employer/closure/open/edit/edit')
            ->with('employer_closure', $closure)
            ->with('closure_open', $employerClosureReopen)
            ->with('employer', $employer)
            ->with('check_workflow', $check_workflow);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployerOpenRequest $request, EmployerClosureReopen $employerClosureReopen)
    {
        //
        $this->checkIfHasWfDefinition(1);
        $input = $request->all();
        $this->employer_closure_open_repo->update($employerClosureReopen, $input);
        return redirect()->route('backend.compliance.employer_closure.open.profile', $employerClosureReopen->id)->withFlashSuccess('Success, Employer business closure open request updated.');

    }


    public function profile(EmployerClosureReopen $employerClosureReopen, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $employer_closure_open_id = $employerClosureReopen->id;
        $employer_closure = ($employerClosureReopen->employer_closure_id) ? $employerClosureReopen->employerClosure : null;
        $type = $this->employer_closure_open_repo->getWfModuleType($employer_closure_open_id);
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $employer_closure_open_id, 'type'=> $type]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;

        $workflow_input = ['resource_id' => $employer_closure_open_id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $type ];
        /*end workflow*/
        $employer = ($employer_closure) ? $employerClosureReopen->employerClosure->closedEmployer : $employerClosureReopen->employer;
        /*Pending tasks*/
        $pending_tasks = $this->employer_closure_open_repo->getPendingTasksBeforeInitiate($employerClosureReopen);

        /*end pending taks*/
        /*docs*/
        $docs_attached = $this->employer_closure_open_repo->getDocsAttached($employerClosureReopen->id);
        $document_data = [
            'attach_route' => '',
            'doc_table' => 'document_employer'
        ];
        /*end docs*/
        return view('backend/operation/compliance/member/employer/closure/open/profile/open_profile')
            ->with('employer_closure_open', $employerClosureReopen)
            ->with('employer_closure', $employer_closure)
            ->with('employer', $employer)
            ->with(['docs_attached' => $docs_attached, 'document_data' => $document_data, 'pending_tasks' => $pending_tasks])
            ->with(['check_workflow' => $check_workflow, 'check_pending_level1' => $check_pending_level1 , 'workflow_track' => $workflowTrackDataTable, 'workflow_input' => $workflow_input, ]);
    }

    /*Undo or reverse / delete  closure*/
    public function undo(EmployerClosureReopen $employerClosureReopen)
    {
        $employer_id = $employerClosureReopen->employerClosure->employer_id;
        $this->employer_closure_open_repo->undo($employerClosureReopen);
        return redirect()->route('backend.compliance.employer.profile', $employer_id)->withFlashSuccess('Success, Employer business closure request undone.');
    }


    /**
     * Initiate closure approval
     */
    public function initiateApproval(EmployerClosureReopen $employerClosureReopen)
    {
        $input = request()->all();
        $wf_module_group_id = $this->wfModuleGroupId();

        access()->hasWorkflowDefinition($wf_module_group_id, 1);
//        $this->employer_closure_open_repo->checkIfCanInitiateApproval($employerClosureReopen->id);
        /*workflow start*/
        $type = $this->employer_closure_open_repo->getWfModuleType($employerClosureReopen);

        DB::transaction(function () use ($wf_module_group_id, $employerClosureReopen,$input, $type) {
            /*initiate*/
            $this->employer_closure_open_repo->initiateApproval($employerClosureReopen->id);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $employerClosureReopen->id, 'type' => $type], [], ['comments' => $input['comments']]));
        });
        /*end workflow*/
        return redirect()->route('backend.compliance.employer_closure.open.profile',$employerClosureReopen->id)->withFlashSuccess('Success, Workflow approval has been initiated');
    }



    /*Preview document*/
    public function previewDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $closure_id = $uploaded_doc->external_id;
        $closure = $this->employer_closure_open_repo->find($closure_id);
        $url = $closure->getClosureDocPathFileUrl($doc_pivot_id);
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded_doc->description, "id" => $doc_pivot_id]);

    }




    /*GENERAL METHODS ----------*/

    /*Check if has wf definition rights*/
    public function checkIfHasWfDefinition($level)
    {
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowDefinition($wf_module_group_id,$level);
    }










}
