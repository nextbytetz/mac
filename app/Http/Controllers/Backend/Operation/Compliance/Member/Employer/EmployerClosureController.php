<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member\Employer;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Compliance\AttachBusinessClosureDocRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateEmployerClosureRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerClosureFillStatusFollowupRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerClosureFollowUpRequest;
use App\Http\Requests\Backend\Operation\Compliance\SyncEmployerClosureEofficeRequest;
use App\Http\Requests\Request;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Models\Operation\Compliance\Member\EmployerClosureFollowUp;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\DataTables\EmployerContributionDataTable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use App\Exceptions\GeneralValidationException;

class EmployerClosureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */

    protected $employer_closure_repo;


    public function __construct() {
        $this->employer_closure_repo = new EmployerClosureRepository();
    }


    public function wfModuleGroupId()
    {
        return 18;
    }



    public function index(Employer $employer)
    {

        return view('backend/operation/compliance/member/employer/closure/index')
        ->with('employer', $employer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Employer $employer)
    {
        //
        $this->checkIfHasWfDefinition(1);
        $check_if_has_pending_closure = $this->employer_closure_repo->checkIfEmployerHasPendingClosure($employer->id);
        if($check_if_has_pending_closure == false){
            $general_contrib_summary = (new EmployerRepository())->getContribReceivableSummary($employer->id);
            $tempo_types = (new CodeValueRepository())->getCvReferenceForSelectFilteredReferences(49, ['ETCLDORMANT', 'EDSUTYFRMEM']);
            $permanent_types = (new CodeValueRepository())->getCvReferenceForSelectFilteredReferences(49, ['ETCLPERMSETTL', 'EDSUTYFRMEM']);
            $dormant_sources = (new CodeValueRepository())->getCodeValuesReferenceByCodeForSelect(52);
            $close_date_cut_off = $this->employer_closure_repo->getClosureDateCutOff();
            return view('backend/operation/compliance/member/employer/closure/create/create_closure')
            ->with('employer', $employer)
            ->with('general_contrib_summary',$general_contrib_summary)
            ->with('tempo_types',$tempo_types)
            ->with('permanent_types',$permanent_types)
            ->with('dormant_sources',$dormant_sources)
            ->with('close_date_cut_off',$close_date_cut_off);
        }else{
            return redirect()->route('backend.compliance.employer.closure.index', $employer->id);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployerClosureRequest $request)
    {
        //
        $this->checkIfHasWfDefinition(1);
        $input = $request->all();
        $closure = $this->employer_closure_repo->store($input);
        return redirect()->route('backend.compliance.employer.closure.profile', $closure->id)->withFlashSuccess('Success, Employer business closure request created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployerClosure $employerClosure)
    {
        //
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $employer = $employerClosure->closedEmployer;
        /*workflow*/
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $employerClosure->id]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $tempo_types = (new CodeValueRepository())->getCvReferenceForSelectFilteredReferences(49, ['ETCLDORMANT', 'EDSUTYFRMEM','ETCLPERMSETTL']);
        $permanent_types = (new CodeValueRepository())->getCvReferenceForSelectFilteredReferences(49, ['ETCLPERMSETTL', 'EDSUTYFRMEM']);
        $dormant_sources = (new CodeValueRepository())->getCodeValuesReferenceByCodeForSelect(52);
        $close_date_cut_off = $this->employer_closure_repo->getClosureDateCutOff();
        return view('backend/operation/compliance/member/employer/closure/edit/edit_closure')
        ->with('employer_closure', $employerClosure)
        ->with('employer', $employer)
        ->with('check_workflow', $check_workflow)
        ->with('tempo_types',$tempo_types)
        ->with('permanent_types',$permanent_types)
        ->with('dormant_sources',$dormant_sources)
        ->with('close_date_cut_off',$close_date_cut_off);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEmployerClosureRequest $request, EmployerClosure $employerClosure)
    {
        //
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $input = $request->all();
        $this->employer_closure_repo->update($employerClosure, $input);
        return redirect()->route('backend.compliance.employer.closure.profile', $employerClosure->id)->withFlashSuccess('Success, Employer business closure request updated.');

    }


    public function profile(EmployerClosure $employerClosure, WorkflowTrackDataTable $workflowTrackDataTable)
    {
        /* Check workflow */
        $wf_module_group_id = $this->wfModuleGroupId();
        $employer_closure_id = $employerClosure->id;
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $employer_closure_id, 'type'=> $type]);
        $current_wf_track = $workflow->currentWfTrack();
        $check_workflow = $workflow->checkIfHasWorkflow();

        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        /*Start:level for letter*/
        $check_if_can_initiate_wf_letter = $this->employer_closure_repo->checkIfCanInitiateLetterWf($current_wf_track);

        /*end level for letter*/
        $workflow_input = ['resource_id' => $employer_closure_id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $type ];
        /*end workflow*/
        $employer = $employerClosure->closedEmployer;
        /*Pending tasks*/
        $docs_pending = $this->employer_closure_repo->getDocumentsNotSubmitted($employer_closure_id);
        /*end pending taks*/
        /*docs*/
        $docs_attached = $this->employer_closure_repo->getDocsAttachedNonRecurring($employer_closure_id);
        $wcp1_months = $this->employer_closure_repo->getMonthsForWcp1Document($employer_closure_id);

        /*end docs*/
        return view('backend/operation/compliance/member/employer/closure/profile/profile')
        ->with('employer_closure', $employerClosure)
        ->with('employer', $employer)
        ->with(['check_workflow' => $check_workflow, 'check_pending_level1' => $check_pending_level1 , 'check_if_can_initiate_wf_letter' => $check_if_can_initiate_wf_letter, 'workflow_track' => $workflowTrackDataTable, 'workflow_input' => $workflow_input, 'docs_pending' => $docs_pending, 'docs_attached' => $docs_attached, 'wcp1_months' => $wcp1_months]);
    }

    /*Undo or reverse / delete  closure*/
    public function undo(EmployerClosure $employerClosure)
    {
        $this->employer_closure_repo->undo($employerClosure);
        return redirect()->route('backend.compliance.employer.closure.index', $employerClosure->employer_id)->withFlashSuccess('Success, Employer business closure request undone.');
    }


    /**
     * Initiate closure approval
     */
    public function initiateApproval(EmployerClosure $employerClosure)
    {
        $input = request()->all();
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        $this->employer_closure_repo->checkIfCanInitiateApproval($employerClosure->id);
        /*workflow start*/
        DB::transaction(function () use ($wf_module_group_id, $employerClosure,$input, $type) {
            /*initiate*/
            $this->employer_closure_repo->initiateApproval($employerClosure->id);
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $employerClosure->id, 'type' => $type], [], ['comments' => $input['comments']]));

        });
        /*end workflow*/
        return redirect()->route('backend.compliance.employer.closure.profile',$employerClosure->id)->withFlashSuccess('Success, Workflow approval has been initiated');
    }


    /**
     * @param EmployerClosure $employerClosure
     * @return mixed
     * -----STart update payroll details - portal arrears--------
     *
     */

    public function openPageToConfirmEmployerPayroll(EmployerClosure $employerClosure)
    {
        $employer_repo = new EmployerRepository();
        $payroll_ids = $employer_repo->getEmployerPayrolls($employerClosure->employer_id)->pluck('payroll_name', 'payroll_id');
        return view('backend/operation/compliance/member/employer/closure/confirm_payrolls')
        ->with('payroll_ids', $payroll_ids)
        ->with('employer', $employerClosure->closedEmployer)
        ->with('employer_closure', $employerClosure);
    }

    public function confirmEmployerPayrollsForThisClosure(EmployerClosure $employerClosure)
    {
        $input = request()->all();
        $input['ispayroll_confirmed'] = isset($input['payroll_ids']) ? 1 : 0;
        $input['payroll_ids'] = isset($input['payroll_ids']) ? json_encode($input['payroll_ids']) : null;
        $this->employer_closure_repo->confirmPayrollsForThisClosure($employerClosure->id, $input);
        return redirect()->route('backend.compliance.employer.closure.profile',$employerClosure->id)->withFlashSuccess('Success, Payroll(s) have been confirmed');
    }


    /*---end portal contrib arrears payroll confirm----------*/

    /*Fill Status follow up for temporary closure after period = 6 months*/
    public function fillStatusFollowUp(EmployerClosure $employerClosure)
    {
//        $employer = $employerClosure->closedEmployer;
//        return view('backend/operation/compliance/member/employer/closure/tempo_closure_status_followup/fill_status_follow_up')
//            ->with('employer_closure', $employerClosure)
//            ->with('employer', $employer);

        $employer = $employerClosure->closedEmployer;
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $follow_up_types = (new CodeValueRepository())->query()->where('code_id',9)->get()->pluck('name','id');
        return view('backend/operation/compliance/member/employer/closure/follow_up/create/create')
        ->with('employer',  $employer)
        ->with('employer_closure',  $employerClosure)
        ->with('follow_up_types',  $follow_up_types)
        ->with('isstatus_followup',  1);
    }


    /*Store Status follow up for temporary closure after period = 6 months*/
    public function storeStatusFollowUp(EmployerClosure $employerClosure, EmployerClosureFillStatusFollowupRequest $request)
    {
        $input = $request->all();
        $outcome = $input['followup_outcome'];
        $employer = $employerClosure;
        $this->employer_closure_repo->storeStatusFollowUp($employerClosure, $input);
        if($outcome == 1){
            /*Still closed*/
            return redirect()->route('backend.compliance.employer.closure.profile',$employerClosure->id)->withFlashSuccess('Success, Status follow up has been updated');
        }elseif($outcome == 2 && $employer->employer->employer_status == 3){
            /*reopened and still closed open - reopened page*/
            return redirect()->route('backend.compliance.employer_closure.open.create',$employerClosure->employer_id)->withFlashSuccess('Success, Status follow up has been updated');
        }else{
            return redirect()->route('backend.compliance.employer.closure.profile',$employerClosure->id)->withFlashSuccess('Success, Status follow up has been updated');
        }
    }

    /**
     * @param EmployerClosure $employerClosure
     * @return mixed
     * Attach document
     */
    public function attachDocument(EmployerClosure $employerClosure)
    {
        $employer = $employerClosure->closedEmployer;
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $document_type_ids = $this->employer_closure_repo->getDocsIdsForAttachmentByClosureType( $employerClosure);
        $document_types = Document::query()->whereIn('id', $document_type_ids)->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/closure/document/attach_doc')
        ->with('employer',  $employer)
        ->with('employer_closure',  $employerClosure)
        ->with('document_types',  $document_types)
        ->with('iswcp1', 0);
    }



    public function storeDocument(AttachBusinessClosureDocRequest $request)
    {
        $input = $request->all();
        $closure_id = $input['employer_closure_id'];
        $closure = $this->employer_closure_repo->find($closure_id);
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($closure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $this->employer_closure_repo->saveClosureDocuments($input, $closure_id);
        $return_page = isset($input['return_page']) ? 1 : 0;
        $iswcp1 = $input['iswcp1'];
        if($return_page == 1)
        {
            if($iswcp1 == 0){
                return redirect()->route('backend.compliance.employer.closure.attach_document', $closure_id)->withFlashSuccess('Success, Document attached');
            }else{
                return redirect()->route('backend.compliance.employer.closure.attach_wcp1_document', $closure_id)->withFlashSuccess('Success, Document attached');
            }

        }else{
            if($iswcp1 == 0) {
                return redirect('compliance/employer/closure/profile/' . $closure_id . '#documents')->withFlashSuccess('Success, Document attached');
            }else{
                return redirect('compliance/employer/closure/profile/' . $closure_id . '#wcp')->withFlashSuccess('Success, Document attached');
            }
        }

    }

    /*attach wcp document*/
    public function attachWcp1Document(EmployerClosure $employerClosure)
    {
        $employer = $employerClosure->closedEmployer;
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $document_type_ids = [72]; //wcp1
        $document_types = Document::query()->whereIn('id', $document_type_ids)->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/closure/document/attach_doc')
        ->with('employer',  $employer)
        ->with('employer_closure',  $employerClosure)
        ->with('document_types',  $document_types)
        ->with('iswcp1', 1);
    }



    /**
     * @param $doc_pivot_id
     * @return mixed
     * Edit document
     */
    public function editDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $closure = $this->employer_closure_repo->find($uploaded_doc->external_id);
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($closure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $employer = $closure->closedEmployer;
        $document = $employer->documents()->where('document_employer.id', $doc_pivot_id)->first();
        return view('backend/operation/compliance/member/employer/closure/document/edit_doc')
        ->with('employer',  $employer)
        ->with('employer_closure',  $closure)
        ->with('uploaded_document',  $document);
    }


    /**
     * @return mixed
     * update document
     */
    public function updateDocument(AttachBusinessClosureDocRequest $request)
    {

        $input = $request->all();
        $closure_id = $input['employer_closure_id'];
        $closure = $this->employer_closure_repo->find($closure_id);
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($closure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $this->employer_closure_repo->saveClosureDocuments($input, $closure_id);
        return redirect('compliance/employer/closure/profile/' . $closure_id . '#documents')->withFlashSuccess('Success, Document attached');
    }


    /*Delete document*/
    public function deleteDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        /*Check if has right*/
        $closure = $this->employer_closure_repo->find($uploaded_doc->external_id);
        $type = $this->employer_closure_repo->getWfModuleType($closure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $closure_id = $uploaded_doc->external_id;
        $this->employer_closure_repo->deleteDocument($doc_pivot_id);
        return redirect('compliance/employer/closure/profile/' . $closure_id. '#documents')->withFlashSuccess('Success, Document Dettached');
    }


    /*Preview document*/
    public function previewDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $closure_id = $uploaded_doc->external_id;
        $closure = $this->employer_closure_repo->find($closure_id);
        $url = $closure->getClosureDocPathFileUrl($doc_pivot_id);
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded_doc->description, "id" => $doc_pivot_id]);

    }




    /*Get closures by employer for datatable*/
    public function getClosuresByEmployerForDataTable($employer_id)
    {
        return Datatables::of($this->employer_closure_repo->getClosureByEmployerForDataTable($employer_id)->orderBy('id', 'desc'))

        ->addColumn('close_date_formatted', function ($closure) {
            return $closure->close_date_formatted;
        })
        ->addColumn('open_date_formatted', function ($closure) {
            return $closure->open_date_formatted;
        })
        ->addColumn('approved_date_formatted', function ($closure) {
            return $closure->approved_date_formatted;
        })
        ->addColumn('status', function ($closure) {
            return $closure->status_label;
        })
        ->addColumn('close_type_name', function ($closure) {
            return $closure->close_type_name;
        })
        ->addColumn('extension_count', function ($closure) {
            return $closure->closureExtensions()->count();
        })
        ->rawColumns(['status'])
        ->make(true);
    }

    /*Tempo closure status follow ups page*/
    public function tempoClosureStatusFollowUpsPage()
    {
        return view('backend/operation/compliance/member/employer/closure/tempo_closure_status_followup/index');
    }

    /*Get temporary closed for follow up status of business*/
    public function getTemporaryClosedForFollowStatusForDt()
    {
        return Datatables::of($this->employer_closure_repo->getTemporaryClosedForFollowUpStatusForDt()->orderBy('employer_closures.wf_done_date', 'asc'))

        ->addColumn('close_date_formatted', function ($closure) {
            return short_date_format($closure->close_date);
        })
        ->addColumn('approved_date_formatted', function ($closure) {
            return short_date_format($closure->approved_date);
        })
        ->addColumn('application_date_formatted', function ($closure) {
            return short_date_format($closure->application_date);
        })
        ->addColumn('followup_ref_date_formatted', function ($closure) {
            return short_date_format($closure->followup_ref_date);
        })
        ->rawColumns([''])
        ->make(true);
    }



    /**
     *
     * CLOSURE FOLLOW UPS
     */

    /*Create follow up*/
    public function createFollowUp(EmployerClosure $employerClosure)
    {
        //
        $employer = $employerClosure->closedEmployer;
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $follow_up_types = (new CodeValueRepository())->query()->where('code_id',9)->get()->pluck('name','id');
        return view('backend/operation/compliance/member/employer/closure/follow_up/create/create')
        ->with('employer',  $employer)
        ->with('employer_closure',  $employerClosure)
        ->with('follow_up_types',  $follow_up_types)
        ->with('isstatus_followup',  0);

    }

    /*Store follow up*/
    public function storeFollowUp(EmployerClosureFollowUpRequest $request)
    {
        //
        $input = $request->all();
        $this->employer_closure_repo->storeFollowUp($input);
        return redirect('compliance/employer/closure/profile/' . $input['employer_closure_id'] . '#followups')->withFlashSuccess('Success, Follow up created');
    }


    /*Edit follow up*/
    public function editFollowUp(EmployerClosureFollowUp $employerClosureFollowUp)
    {
        //
        $employer_closure = $employerClosureFollowUp->employerClosure;
        /*Check if has right*/
        $type = $this->employer_closure_repo->getWfModuleType($employer_closure);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $employer = $employer_closure->closedEmployer;
        $follow_up_types = (new CodeValueRepository())->query()->where('code_id',9)->get()->pluck('name','id');
        /*Get document*/
        $follow_up_type = CodeValue::query()->find($employerClosureFollowUp->follow_up_type_cv_id);
        $document_id = $this->employer_closure_repo->getDocumentIdByFollowUpType($follow_up_type);
        $document_attached = (isset($document_id)) ? $this->employer_closure_repo->getUploadedDoc($employerClosureFollowUp->id, $document_id) : null;
        return view('backend/operation/compliance/member/employer/closure/follow_up/edit/edit')
        ->with('employer',  $employer)
        ->with('follow_up',  $employerClosureFollowUp)
        ->with('follow_up_types',  $follow_up_types)
        ->with('employer_closure', $employer_closure)
        ->with('document_attached', $document_attached)
        ->with('isstatus_followup',  $employerClosureFollowUp->isstatus_followup);

    }

    /*update follow up*/
    public function updateFollowUp(EmployerClosureFollowUpRequest $request)
    {
        //
        $input = $request->all();
        $follow_up_id = $input['follow_up_id'];
        $this->employer_closure_repo->updateFollowUp($follow_up_id,$input);
        return redirect('compliance/employer/closure/profile/' . $input['employer_closure_id'] . '#followups')->withFlashSuccess('Success, Follow up updated');
    }

    /*Store follow up*/
    public function deleteFollowUp(EmployerClosureFollowUp $employerClosureFollowUp)
    {
        $employer_closure_id = $employerClosureFollowUp->employer_closure_id;
        $this->employer_closure_repo->deleteFollowUp($employerClosureFollowUp->id);
        return redirect('compliance/employer/closure/profile/' . $employer_closure_id . '#followups')->withFlashSuccess('Success, Follow up deleted');
    }


    /*Preview document*/
    public function previewFollowUpDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $staff_employer_id = $uploaded_doc->external_id;
        $url = $this->employer_closure_repo->getFollowUpDocPathFileUrl($doc_pivot_id);
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded_doc->description, "id" => $doc_pivot_id]);

    }

    /*Get follow up data table by closure*/
    public function getFollowUpsByClosureForDataTable($employer_closure_id)
    {
        return Datatables::of($this->employer_closure_repo->getFollowUpsByClosureForDataTable($employer_closure_id)->orderBy('date_of_follow_up', 'desc'))

        ->addColumn('date_of_follow_up_formatted', function ($follow_up) {
            return $follow_up->date_of_follow_up_formatted;
        })
        ->addColumn('follow_up_type', function ($follow_up) {
            return $follow_up->followUpType->name;
        })
        ->addColumn('staff', function ($follow_up) {
            return $follow_up->user->username;
        })

        ->make(true);
    }




    /*---End of follow ups methods ------------*/


    /*GENERAL METHODS ----------*/

    /*Check if has wf definition rights*/
    public function checkIfHasWfDefinition($level)
    {
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowDefinition($wf_module_group_id,$level);
    }





    /**
     * E-OFFICE integrations
     */


    public function eofficeAlertsListingPage()
    {
        return view('backend/operation/compliance/member/employer/closure/eoffice_alerts');
    }

    /*Get pending closures from e-office need to be synced on MAC*/
    public function getPendingClosuresFromEofficeForDataTable()
    {
        return Datatables::of($this->employer_closure_repo->getPendingClosuresFromEofficeForDataTable()->orderBy('id', 'asc'))

        ->editColumn('doc_date', function ($closure) {
            return short_date_format($closure->doc_date);
        })
        ->editColumn('doc_receive_date', function ($closure) {
            return short_date_format($closure->doc_receive_date);
        })

        ->make(true);
    }


    /*open sync page*/
    public function openSyncClosureToEmployerPage($eoffice_employer_closure_id)
    {
        $closure_alert = DB::table('eoffice_employer_closures')->where('id', $eoffice_employer_closure_id)->first();
        return view('backend/operation/compliance/member/employer/closure/eoffice/sync_closure_to_employer')
        ->with('closure_alert', $closure_alert);
    }

    public function syncClosureToEmployer($eoffice_employer_closure_id,SyncEmployerClosureEofficeRequest $request)
    {
        $input = $request->all();
        $employer_closure = $this->employer_closure_repo->syncClosureFromEoffice($eoffice_employer_closure_id, $input);
        return redirect()->route('backend.compliance.employer.closure.profile', $employer_closure->id)->withFlashSuccess('Success, Employer business closure request created.');
    }



    /**
     * Store closure from eoffice
     */
    public function storeClosureFromEoffice(Request $request)
    {
        $input = $request->all();

        /**
         * e-office integration, if source in the request is 2 then the request come from e-office and should return an acknowledgement.
         *
         */
        $this->employer_closure_repo->saveClosureDocsFromEoffice($input);
        if ($input['source'] == 2) {
            //Request came from e-office system, respond to e-office
            return response()->json(['message' => "SUCCESS"]);
        }
    }

    public function storeClosureSupportingDocFromEoffice(Request $request)
    {
        $input = $request->all();

        /**
         * e-office integration, if source in the request is 2 then the request come from e-office and should return an acknowledgement.
         *
         */
        $this->employer_closure_repo->saveClosureDocsFromEoffice($input);
        if ($input['source'] == 2) {
            //Request came from e-office system, respond to e-office
            return response()->json(['message' => "SUCCESS"]);
        }
    }






    /**
     * @param NotificationReport $notificationReport
     * @param $pivotDocumentId
     * @return mixed
     * Open Document from e-office using e-Office document id
     */
    public function openBeneficiaryDocumentDocumentDms($pivotDocumentId)
    {
        /**
         * e-office Integration, open notification document from e-office system.
         */
        /*new--*/

        $path = $this->getPathFromDms($pivotDocumentId);
        //$path = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        return response()->file($path);
    }



    public function getPathFromDms($pivotDocumentId)
    {
        $base64pdf = $this->getBase64BeneficiaryEofficeDocument($pivotDocumentId);
        $base64decoded = base64_decode($base64pdf);
        $document = time() . $pivotDocumentId . '.pdf';
        $file = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        file_put_contents($file, $base64decoded);
        $path = eoffice_dir() . DIRECTORY_SEPARATOR . $document;
        return $path;
    }

    /**
     * @param $incident NotificationReport id
     * @param $documentType
     * @param $eOfficeDocumentId
     * @return mixed
     */
    public function getBase64BeneficiaryEofficeDocument($pivotDocumentId)
    {
        $document_pivot = DB::table('document_payroll_beneficiary')->where('id', $pivotDocumentId)->first();
        $documentType = $document_pivot->document_id;
        $eOfficeDocumentId = $document_pivot->eoffice_document_id;

        $client = new Client();
        $link = env('EOFFICE_APP_URL');
        $fileNumber = $document_pivot->resource_id;
        //TODO: Get url from e-office to pull document beneficiary file
        $guzzlerequest = $client->get($link . "/api/claim/file/document?fileNumber={$fileNumber}&documentType={$documentType}&documentId={$eOfficeDocumentId}", [
            'auth' => [
                env("EOFFICE_AUTH_USER"),
                env("EOFFICE_AUTH_PASS"),
            ],
        ]);
        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        $base64pdf = $parsed_json['document'];

        return $base64pdf;
    }


    public function reverseOnline(EmployerClosure $employerClosure)
    {
        $input = request()->all();
        if (empty($input['online_remarks'])) {
            throw new GeneralValidationException("Remarks is required");
        }
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->employer_closure_repo->getWfModuleType($employerClosure);
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        $this->employer_closure_repo->checkIfCanInitiateApproval($employerClosure->id);
        $this->employer_closure_repo->query()->where('id',$employerClosure->id)->update(['online_status'=>2, 'online_remarks'=>$input['online_remarks']]);
        /*end workflow*/
        return redirect()->route('backend.compliance.employer.closure.profile',$employerClosure->id)->withFlashSuccess('Success, Request has been reversed');
    }








}
