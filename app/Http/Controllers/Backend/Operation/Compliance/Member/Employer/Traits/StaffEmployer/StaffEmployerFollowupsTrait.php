<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member\Employer\Traits\StaffEmployer;


use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

trait StaffEmployerFollowupsTrait {



    /*Add Multiple Follow ups*/
    public function addMultipleFollowups()
    {
        $staff_employer_repo = new StaffEmployerRepository();
        $user = access()->user();
        $check_if_intern = $this->staff_employer_repo->checkIfStaffIsIntern($user->id);
        $cv_repo = new CodeValueRepository();
        if($check_if_intern){
            $cv_refs_intern = $staff_employer_repo->getFollowupTypesReferencesByCategory(5);
            $follow_up_types = $cv_repo->query()->where('code_id',9)->whereIn('reference', $cv_refs_intern)->orderBy('name', 'asc')->get()->pluck('name','reference');
        }else{
            $follow_up_types = $cv_repo->query()->where('code_id', 9)->orderBy('name', 'asc')->get()->pluck('name', 'reference');
        }
        $feedbacks= $cv_repo->query()->where('code_id',25)->orderBy('name', 'asc')->get()->pluck('name','reference');
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/add_multiple_followup/fill_followups')
            ->with('follow_up_types', $follow_up_types)
            ->with('feedbacks', $feedbacks);
    }



    /*Load added follow ups content by user and date on multiple follow up page*/
    public function loadFollowupsContentByUserDate()
    {
        $input = request()->all();
        $date_of_follow_up = $input['date_of_follow_up'];
        $user_id = access()->id();
        $follow_ups = $this->staff_employer_repo->getFollowupsByUserDateRange($user_id, standard_date_format($date_of_follow_up), standard_date_format($date_of_follow_up))->orderBy('staff_employer_follow_ups.id')->get();
        return view('backend/operation/compliance/member/employer/staff_relationship/my_employer/follow_ups/add_multiple_followup/existing_follow_ups_content')
            ->with('follow_ups', $follow_ups);
    }


    /**
     * @return mixed
     * REMINDERS ON MISSING FOLLOW UPS ---Start
     */
    public function exportMissingFollowupReminders()
    {
        return $this->staff_employer_repo->exportStaffMissingFollowUpsReminders();
    }

    /*---end reminder missing fpllowups--*/
}
