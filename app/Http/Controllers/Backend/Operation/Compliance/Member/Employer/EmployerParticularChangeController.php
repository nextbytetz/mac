<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member\Employer;

use App\DataTables\WorkflowTrackDataTable;
use App\Events\NewWorkflow;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Operation\Compliance\AttachEmployerParticularChangeDocRequest;
use App\Http\Requests\Backend\Operation\Compliance\EmployerParticularChangeRequest;
use App\Http\Requests\Backend\Operation\Compliance\SyncEmployerClosureEofficeRequest;
use App\Http\Requests\Request;
use App\Models\Legal\District;
use App\Models\Location\Country;
use App\Models\Location\LocationType;
use App\Models\Location\Region;
use App\Models\Operation\Claim\Document;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerParticularChange;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerParticularChangeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Services\Workflow\Workflow;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class EmployerParticularChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */

    protected $employer_change_repo;


    public function __construct() {
        $this->employer_change_repo = new EmployerParticularChangeRepository();
    }


    public function wfModuleGroupId()
    {
        return 23;
    }



    public function index(Employer $employer)
    {
        return view('backend/operation/compliance/member/employer/particular_change/index')
        ->with('employer', $employer);
    }


    /*Create new entry Payroll Mp Update*/
    public function create(Employer $employer)
    {
        $cv_repo = new CodeValueRepository();
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->employer_change_repo->getWfModuleType();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*check if can initiate new entry*/
        $this->employer_change_repo->checkIfCanInitiateNewEntry($employer->id);
        $duplicate_employers_merged = (new EmployerRepository())->query()->where('duplicate_id', $employer->id)->withoutGlobalScopes()->get()->pluck('name_with_reg_no', 'id');
        return view('backend/operation/compliance/member/employer/particular_change/create/create_new')
        ->with('employer', $employer)
        ->with("countries", Country::query()->get()->pluck('name', 'id'))
        ->with("location_types", LocationType::query()->get()->pluck('name', 'id'))
        ->with("districts", District::query()->get()->pluck('name', 'id'))
        ->with("regions", Region::query()->get()->pluck('name', 'id'))
        ->with("business_sectors", CodeValue::query()->where('code_id', 5)->get()->pluck('name', 'id'))
        ->with("name_change_types", $cv_repo->queryOnlyActive()->where('code_id', 44)->get()->pluck('name', 'reference'))
        ->with("registration_authorities", CodeValue::query()->where('code_id', 45)->get()->pluck('name', 'id'))
        ->with('duplicate_employers_merged', $duplicate_employers_merged);

    }


    public function store(EmployerParticularChangeRequest $request)
    {
        $input = $request->all();
        $wf_module_group_id = $this->wfModuleGroupId();
        /*check if can initiate new entry*/
        $this->employer_change_repo->checkIfCanInitiateNewEntry($input['employer_id']);
        $employer_change =  DB::transaction(function () use ($wf_module_group_id, $input) {
            /*create mp update*/
            $employer_change =  $this->employer_change_repo->store($input);
            return $employer_change;
        });
        return redirect()->route('backend.compliance.employer.change_particular.profile',$employer_change->id)->withFlashSuccess('Success, Employer particular changes has been created');
    }

    /*Profile*/
    public function profile(EmployerParticularChange $employerParticularChange, WorkflowTrackDataTable $workflowTrackDataTable){
        /* Check workflow */
        $type = $this->employer_change_repo->getWfModuleType($employerParticularChange);
        $wf_module_group_id = $this->wfModuleGroupId();
        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, "resource_id" => $employerParticularChange->id, 'type' => $type ]);
        $check_workflow = $workflow->checkIfHasWorkflow();
        $check_pending_level1 =  ($check_workflow == 1) ?  $workflow->checkIfIsLevelPending(1) : 0;
        /*end workflow*/
        $employer_id = $employerParticularChange->employer_id;
        $workflow_input = ['resource_id' => $employerParticularChange->id, 'wf_module_group_id'=> $wf_module_group_id, 'type' => $type ];
        /*Start -- docs--*/
        $docs_attached = $this->employer_change_repo->getDocsAttached($employerParticularChange->id);
        $pending_docs = $this->employer_change_repo->getPendingDocuments($employerParticularChange->id);
        /*End --docs--*/
        $reg_authorities = CodeValue::query()->where('code_id', 45)->get()->pluck('name', 'id');
        $employer_repo = (new EmployerRepository());

        /*check if changes type exists*/
        $change_repo = $this->employer_change_repo;
        return view('backend/operation/compliance/member/employer/particular_change/profile/profile')
        ->with([ 'employer_particular_change' => $employerParticularChange,'check_pending_level1' => $check_pending_level1, "workflow_track" => $workflowTrackDataTable, 'workflow_input' => $workflow_input, 'check_workflow' => $check_workflow, 'docs_attached' => $docs_attached, 'pending_docs' => $pending_docs,  'reg_authorities' => $reg_authorities ,'employer_repo' => $employer_repo, 'change_repo' => $change_repo]);


    }


    /*Edit*/
    public function edit(EmployerParticularChange $employerParticularChange){
        $cv_repo = new CodeValueRepository();
        $type = $this->employer_change_repo->getWfModuleType($employerParticularChange);
        $this->checkLevelRights($employerParticularChange->id, $type,1);
//        $new_values = unserialize($employerParticularChange->new_values);
        $employer = $employerParticularChange->employer;
        $duplicate_employers_merged = (new EmployerRepository())->query()->where('duplicate_id', $employer->id)->withoutGlobalScopes()->get()->pluck('name_with_reg_no', 'id');
        $name_new_values = $this->employer_change_repo->getNewValuesByChangeType($employerParticularChange, 'EMTCNAME');//unserialized
        $general_new_values = $this->employer_change_repo->getNewValuesByChangeType($employerParticularChange, 'EMTCGEN');//unserialized
        $reg_authority_new_values = $this->employer_change_repo->getNewValuesByChangeType($employerParticularChange, 'EMTCREGA');//unserialized
        $employer_repo = new EmployerRepository();
        $change_repo = $this->employer_change_repo;
        $cv_repo = new CodeValueRepository();
        return view('backend/operation/compliance/member/employer/particular_change/edit/edit_new')
        ->with(['employer_particular_change' => $employerParticularChange, 'employer' => $employer])
        ->with("countries", Country::query()->get()->pluck('name', 'id'))
        ->with("location_types", LocationType::query()->get()->pluck('name', 'id'))
        ->with("districts", District::query()->get()->pluck('name', 'id'))
        ->with("regions", Region::query()->get()->pluck('name', 'id'))
        ->with("business_sectors", CodeValue::query()->where('code_id', 5)->get()->pluck('name', 'id'))
        ->with("name_change_types", $cv_repo->queryOnlyActive()->where('code_id', 44)->get()->pluck('name', 'reference'))
        ->with("registration_authorities", CodeValue::query()->where('code_id', 45)->get()->pluck('name', 'id'))
        ->with('duplicate_employers_merged', $duplicate_employers_merged)
        ->with('employer_repo', $employer_repo)
        ->with('change_repo', $change_repo)
        ->with('cv_repo', $cv_repo)
        ->with(['name_new_values' => $name_new_values, 'general_new_values' => $general_new_values, 'reg_authority_new_values' => $reg_authority_new_values ]);
    }


    /*Update*/
    public function update(EmployerParticularChangeRequest $request, EmployerParticularChange $employerParticularChange)
    {
        $input = $request->all();
        $this->employer_change_repo->update($employerParticularChange, $input);
        return redirect()->route('backend.compliance.employer.change_particular.profile',$employerParticularChange->id)->withFlashSuccess('Success, Employer particular change has been updated');
    }




    /**
     * Initiate closure approval
     */
    public function initiateApproval(EmployerParticularChange $employerParticularChange)
    {
        $input = request()->all();
        $wf_module_group_id = $this->wfModuleGroupId();
        $type = $this->employer_change_repo->getWfModuleType($employerParticularChange);
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        $this->employer_change_repo->checkIfCanInitiateApproval($employerParticularChange->id);
        /*workflow start*/
        DB::transaction(function () use ($wf_module_group_id, $employerParticularChange,$input, $type) {
            /*initiate*/
            event(new NewWorkflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $employerParticularChange->id, 'type' => $type], [], ['comments' => $input['comments']]));
        });
        /*end workflow*/
        return redirect()->route('backend.compliance.employer.change_particular.profile',$employerParticularChange->id)->withFlashSuccess('Success, Workflow approval has been initiated');
    }




    /*Undo*/
    public function undo(EmployerParticularChange $employerParticularChange)
    {
        $employer_id = $employerParticularChange->employer_id;
        $this->employer_change_repo->undo($employerParticularChange);
        return redirect()->route('backend.compliance.employer.profile',$employer_id)->withFlashSuccess('Success, Employer particular change has been undone ');
    }



    /**
     * @param EmployerClosure $employerClosure
     * @return mixed
     * Attach document
     */
    public function attachDocument(EmployerParticularChange $employerParticularChange)
    {
        $employer = $employerParticularChange->employer;
        $type = $this->employer_change_repo->getWfModuleType($employerParticularChange);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        $document_type_ids = $this->employer_change_repo->getDocTypesIdsForParticularChange();
        $document_types = Document::query()->whereIn('id', $document_type_ids)->pluck('name', 'id');
        return view('backend/operation/compliance/member/employer/particular_change/document/attach_doc')
        ->with('employer',  $employer)
        ->with('employer_particular_change',  $employerParticularChange)
        ->with('document_types',  $document_types);
    }



    public function storeDocument(AttachEmployerParticularChangeDocRequest $request)
    {
        $input = $request->all();
        $employer_particular_change_id = $input['employer_particular_change_id'];
        $employer_particular_change = $this->employer_change_repo->find($employer_particular_change_id);
//        $closure = $this->employer_change_repo->find($employer_particular_change_id);
        /*Check if has right*/
        $type = $this->employer_change_repo->getWfModuleType($employer_particular_change);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $this->employer_change_repo->saveDocuments($input, $employer_particular_change_id);
        $return_page = isset($input['return_page']) ? 1 : 0;

        if($return_page == 1)
        {
            return redirect()->route('backend.compliance.employer.change_particular.attach_document', $employer_particular_change_id)->withFlashSuccess('Success, Document attached');
        }else{

            return redirect('compliance/employer/change_particular/profile/' . $employer_particular_change_id . '#documents')->withFlashSuccess('Success, Document attached');

        }

    }


    /**
     * @param $doc_pivot_id
     * @return mixed
     * Edit document
     */
    public function editDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $employer_particular_change = $this->employer_change_repo->find($uploaded_doc->external_id);
        /*Check if has right*/
        $type = $this->employer_change_repo->getWfModuleType($employer_particular_change);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $employer = $employer_particular_change->employer;
        $document = $employer->documents()->where('document_employer.id', $doc_pivot_id)->first();
        return view('backend/operation/compliance/member/employer/particular_change/document/edit_doc')
        ->with('employer',  $employer)
        ->with('employer_particular_change',  $employer_particular_change)
        ->with('uploaded_document',  $document);
    }


    /**
     * @return mixed
     * update document
     */
    public function updateDocument(AttachEmployerParticularChangeDocRequest $request)
    {
        $input = $request->all();
        $employer_particular_change_id = $input['employer_particular_change_id'];
        $employer_particular_change = $this->employer_change_repo->find($employer_particular_change_id);
        /*Check if has right*/
        $type = $this->employer_change_repo->getWfModuleType($employer_particular_change);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $this->employer_change_repo->saveDocuments($input, $employer_particular_change_id);
        return redirect('compliance/employer/change_particular/profile/' . $employer_particular_change_id . '#documents')->withFlashSuccess('Success, Document attached');
    }


    /*Delete document*/
    public function deleteDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        /*Check if has right*/
        $employer_particular_change_id = $this->employer_change_repo->find($uploaded_doc->external_id);
        $employer_particular_change = $this->employer_change_repo->find($employer_particular_change_id);
        $type = $this->employer_change_repo->getWfModuleType($employer_particular_change);
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id, $type,1);
        /*end check right*/
        $closure_id = $uploaded_doc->external_id;
        $this->employer_change_repo->deleteDocument($doc_pivot_id);
        return redirect('compliance/employer/change_particular/profile/' . $closure_id. '#documents')->withFlashSuccess('Success, Document Dettached');
    }


    /*Preview document*/
    public function previewDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer')->where('id', $doc_pivot_id)->first();
        $employer_particular_change_id = $uploaded_doc->external_id;
        $employer_particular_change = $this->employer_change_repo->find($employer_particular_change_id);
        $url = $employer_particular_change->getDocPathFileUrl($doc_pivot_id);
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded_doc->description, "id" => $doc_pivot_id]);

    }






    /*Changes history for datatable by employer*/
    public function getByEmployerForDt($employer_id)
    {
        return Datatables::of($this->employer_change_repo->getByEmployerForDataTable($employer_id)->orderBy('id', 'desc'))
        ->addColumn('approved_date_formatted', function ($change) {
            return $change->approved_date_formatted;
        })
        ->addColumn('status', function ($change) {
            return $change->status_label;
        })
        ->addColumn('user', function ($change) {
            return $change->payroll_id;
        })
        ->rawColumns(['status'])
        ->make(true);
    }


    /**
     * Check if can Initiate Action (Level ?/ No Workflow)
     */
    public function checkLevelRights($resource_id, $type, $level)
    {
        $wf_module_group_id = $this->wfModuleGroupId();
        access()->hasWorkflowModuleDefinition($wf_module_group_id,  $type, $level);
        workflow([['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id, 'type' => $type]])->checkIfCanInitiateAction($level);
    }

}
