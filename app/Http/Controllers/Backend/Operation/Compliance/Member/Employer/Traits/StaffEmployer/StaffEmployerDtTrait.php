<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member\Employer\Traits\StaffEmployer;


use App\Services\Storage\Traits\AttachmentHandler;
use App\Services\Storage\Traits\FileHandler;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

trait StaffEmployerDtTrait {


    /*Get employers allocated by staff for active allocation datatable*/
    public function getEmployersAllocatedByStaffForActiveAllocationDt($user_id,$isbonus, $with_contact)
    {
        $result_list= $this->staff_employer_repo->getEmployersAllocatedForStaffActiveAllocationForDt($user_id, $isbonus, $with_contact);
        return $this->myEmployersDtStructure($result_list);
    }


    /*Get my employers by general search criteria*/
    public function getEmployersAllocatedByStaffForGeneralDt($user_id,$db_script_id)
    {
        $result_list= $this->staff_employer_repo->getEmployersAllocatedForGeneralForDt($user_id, $db_script_id);
        return $this->myEmployersDtStructure($result_list);
    }


    /*My Employers page default dt structure*/
    public function myEmployersDtStructure($result_list)
    {
        return Datatables::of($result_list)

            ->editColumn('allocation_start_date', function ($allocation) {
                return short_date_format($allocation->allocation_start_date);
            })
            ->editColumn('allocation_end_date', function ($allocation) {
                return short_date_format($allocation->allocation_end_date);
            })
//            ->addColumn('receivable_amount', function ($allocation) {
//                $amount = $this->staff_employer_repo->getGeneralContribSummary($allocation->staff_employer_id)['receivable'];
//                return number_2_format($amount);
//            })
            ->editColumn('missing_months', function ($allocation) use($isbonus) {
//                $missing_months = $this->staff_employer_repo->getEmployerMissingMonthsCount($allocation->employer_id);
//                $missing_months = ($isbonus == 1 && $missing_months == 0) ? 'Non contributor' : number_0_format($missing_months);
//                return $missing_months;
                return number_0_format($allocation->missing_months);
            })
            ->addColumn('no_of_follow_ups', function ($allocation) {
//                $no_of_follow_ups = $this->staff_employer_repo->getNoOfFollowUps($allocation->staff_employer_id);
                return number_0_format($allocation->no_of_follow_ups);
            })
            ->editColumn('relation_status', function ($allocation) {
                return ($allocation->relation_status == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Active'. "'>" . 'Not Active' . "</span>" :
                    "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active' . "'>" . 'Active' . "</span>";
            })
            ->editColumn('attendance_status', function ($allocation) {
                return ($allocation->attendance_status == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Attended'. "'>" . 'Not Attended' . "</span>" :
                    "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Attended' . "'>" . 'Attended' . "</span>";
            })
            ->editColumn('isonline', function ($allocation) {
                return ($allocation->isonline == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Online'. "'>" . 'Not Online' . "</span>" :
                    "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Online' . "'>" . 'Online' . "</span>";
            })

            ->editColumn('employer_status', function ($allocation) {
                return ($allocation->employer_status == 1) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active'. "'>" . 'Active' . "</span>" :
                    ($allocation->employer_status == 2 ?  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Dormant' . "'>" . 'Dormant' . "</span>" :  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Closed' . "'>" . 'Closed' . "</span>")   ;
            })
            ->rawColumns(['relation_status', 'attendance_status', 'isonline', 'employer_status'])
            ->make(true);
    }



    /*Get all employers allocated in debt management for datatable*/
    public function getAllEmployersAllocatedForDt()
    {

        return Datatables::of($this->staff_employer_repo->getAllEmployersAllocatedForDt())

            ->editColumn('allocation_start_date', function ($allocation) {
                return short_date_format($allocation->allocation_start_date);
            })
            ->editColumn('allocation_end_date', function ($allocation) {
                return short_date_format($allocation->allocation_end_date);
            })
            ->editColumn('missing_months', function ($allocation)  {
                return number_0_format($allocation->missing_months);
            })
            ->editColumn('no_of_follow_ups', function ($allocation) {
//                $no_of_follow_ups = $this->staff_employer_repo->getNoOfFollowUps($allocation->staff_employer_id);
                return number_0_format($allocation->no_of_follow_ups);
            })
            ->editColumn('relation_status', function ($allocation) {
                return ($allocation->relation_status == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Active'. "'>" . 'Not Active' . "</span>" :
                    "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active' . "'>" . 'Active' . "</span>";
            })
            ->editColumn('attendance_status', function ($allocation) {
                return ($allocation->attendance_status == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Attended'. "'>" . 'Not Attended' . "</span>" :
                    "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Attended' . "'>" . 'Attended' . "</span>";
            })
            ->editColumn('isonline', function ($allocation) {
                return ($allocation->isonline == 0) ? "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Not Online'. "'>" . 'Not Online' . "</span>" :
                    "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Online' . "'>" . 'Online' . "</span>";
            })

            ->editColumn('employer_status', function ($allocation) {
                return ($allocation->employer_status == 1) ? "<span class='tag tag-success' data-toggle='tooltip' data-html='true' title='" . 'Active'. "'>" . 'Active' . "</span>" :
                    ($allocation->employer_status == 2 ?  "<span class='tag tag-warning' data-toggle='tooltip' data-html='true' title='" . 'Dormant' . "'>" . 'Dormant' . "</span>" :  "<span class='tag tag-danger' data-toggle='tooltip' data-html='true' title='" . 'Closed' . "'>" . 'Closed' . "</span>")   ;
            })
            ->rawColumns(['relation_status', 'attendance_status', 'isonline', 'employer_status'])
            ->make(true);
    }



    /**
     * REMINDERS ON FOLLOWUPS TARGET --- START----------
     */

    /*Get staff employer target followups reminders*/
    public function getUserMissingTargetRemindersForDt()
    {
        return Datatables::of($this->staff_employer_repo->getUserMissingTargetRemindersForDt())

            ->editColumn('week_start_date', function ($reminder) {
                return short_date_format($reminder->week_start_date);
            })
            ->editColumn('week_end_date', function ($reminder) {
                return short_date_format($reminder->week_end_date);
            })

            ->make(true);
    }

}
