<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Operation\Compliance\Member\EmployeeUpload;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeTempRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Operation\Compliance\Member\Employer;


class EmployeeUploadController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd('hi');
      $employee = EmployeeUpload::find($id);
      return response()->json($employee);
  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'firstname' => 'required', 
            'lastname' => 'required',  
            'dob' => 'required',  
            'sex' => 'required',  
            'job_title' => 'required',  
            'emp_cate' => 'required',  
            'salary' => 'required',  
            'grosspay' => 'required',  

        ];

        $message = [
            'firstname.required' => 'firstname is required', 
            'lastname.required' => 'Lastname is required',   
            'dob' => 'Date of birth is required',  
            'sex.required' => 'Location is required',  
            'job_title.required' => 'Job title is required',  
            'emp_cate.required' => 'Employment category is required',  
            'salary.required' => 'Salary is required',  
            'grosspay.required' => 'Gross pay is required',  
        ];

        // Log::info($request->firstname);

        $validator = Validator::make(Input::all(),$rules, $message);

        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));

        }else{

            $employeeupload = Employeeupload::find($id);
            $employeeupload->firstname = $request->firstname;
            $employeeupload->middlename = $request->middlename;
            $employeeupload->lastname = $request->lastname;
            $employeeupload->dob = $request->dob;
            $employeeupload->sex = $request->sex;
            $employeeupload->job_title = $request->job_title;
            $employeeupload->emp_cate = $request->emp_cate;
            $employeeupload->salary = $request->salary;
            $employeeupload->grosspay = $request->grosspay;
            $employeeupload->save();

            return response()->json(array("success"=>true));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateNumber($id){ //save to MAC employees & employee_employerS & PORTAL employee_uploadS -update memberno

      $uploadedmployees = Employeeupload::where('employer_id','=',$id)->get();
     // $user_id = $this->user_id;
     // $employer = Employer::find($id);
     // dd($employer->);
     // $employee = new EmployeeRepository();

      foreach ($uploadedmployees as $employeeTemp) {

        if($employeeTemp->memberno == NULL){
            $data = ['firstname' => $employeeTemp->firstname,'middlename' => $employeeTemp->middlename, 'lastname' => $employeeTemp->lastname, 'dob' => $employeeTemp->dob, 'gender_id' => NULL, 'sex' => $employeeTemp->gender, 'memberno' => 0,'emp_cate' => $employeeTemp->emp_cate, 'created_at' => Carbon::now()];
                // new employee, create first
            /* db builder*/
            $registeredEmployeeId =  DB::table('employees')->insertGetId(
                $data
            );

        // $registeredEmployee = $employee->findOrThrowException($registeredEmployeeId);
            /*end db builder*/
            /* to change member_no name index in the new mac operation */
            $memberno = checksum($registeredEmployeeId, sysdefs()->data()->employee_number_length);

        // $registeredEmployee->save();
            /*update memberno*/
            DB::table('employees')
            ->where('id', $registeredEmployeeId)
            ->update(['memberno' => $memberno, 'updated_at' => Carbon::now()]);


            DB::table('employee_employer')->insert([ 
                'employee_id' => $registeredEmployeeId, 
                'employer_id' => $id, 
                'basicpay' => $employeeTemp->salary, 
                'grosspay' => $employeeTemp->grosspay, 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ]);

            $employeeTemp->memberno = $memberno;
            $employeeTemp->save();
        }


    }
    return redirect('compliance/employer_registration/'.$id.'/upload_employee')->with('flash_success', 'WCF Numbers has been generated');


}



}
