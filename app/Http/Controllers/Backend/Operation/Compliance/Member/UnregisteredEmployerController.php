<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\DataTables\Compliance\Member\UnregisteredEmployerDataTable;
use App\DataTables\Compliance\Member\UnregisteredEmployerFollowUpsDataTable;
use App\Http\Requests\Backend\Operation\Compliance\CreateUnregisteredEmployerFollowUpRequest;
use App\Http\Requests\Backend\Operation\Compliance\CreateUnregisteredEmployerRequest;
use App\Http\Requests\Backend\Operation\Compliance\UpdateAssignedStaffRequest;
use App\Jobs\LoadUnregisteredEmployerBulk;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Api\NewTaxpayerRepository;
use App\Repositories\Backend\Location\CountryRepository;
use App\Repositories\Backend\Location\DistrictRepository;
use App\Repositories\Backend\Location\LocationTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerFollowUpRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Http\Controllers\Controller;
//use Yajra\Datatables\Facades\Datatables;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class UnregisteredEmployerController extends Controller
{

    use AttachmentHandler;

    protected $unregistered_employers;    protected $countries;
    protected $regions;
    protected $districts;
    protected $location_types;
    protected $code_values;
    protected $unregistered_employer_follow_ups;


    public function __construct() {
        $this->unregistered_employers = new UnregisteredEmployerRepository();
        $this->countries = new CountryRepository();
        $this->regions = new RegionRepository();
        $this->districts = new DistrictRepository();
        $this->location_types = new LocationTypeRepository();
        $this->code_values = new CodeValueRepository();
        $this->unregistered_employer_follow_ups = new UnregisteredEmployerFollowUpRepository();


        $this->middleware('access.routeNeedsPermission:compliance', ['only' => ['create', 'store', 'edit' , 'update' ,'destroy', 'register', 'markRegistered' ]]);


        $this->middleware('access.routeNeedsPermission:upload_bulk_unregistered', ['only' => ['uploadBulk', 'storeUploadedBulk', 'uploadBulkInActive' , 'storeUploadedBulkInactive' ]]);


        $this->middleware('access.routeNeedsPermission:assign_staff_for_unregistered', ['only' => ['assignStaffPage', 'assignStaff', 'editAssignedStaff' , 'updateAssignedStaff' ]]);

        $this->middleware('access.routeNeedsPermission:add_new_taxpayers', ['only' => ['addTra']]);


    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UnregisteredEmployerDataTable $dataTable)
    {
        //
        return $dataTable->render("backend/operation/compliance/member/unregistered_employer/index");

    }



    /*
*
*   Unregistered Profile
*/
    public function profile($id, UnregisteredEmployerFollowUpsDataTable $followUpsDataTable)
    {
//        $this->checkIfHasWorkflowDefinition();
        $unregistered_employers = new UnregisteredEmployerRepository(); // temporary
        $unregistered_employer = $unregistered_employers->findOrThrowException($id);
        $check_tin = ($unregistered_employer->tin) ? $unregistered_employers->findIfTinExistInEmployer($unregistered_employer->tin) : 0;
        return view('backend.operation.compliance.member.unregistered_employer.profile')
            ->with("unregistered_employer", $unregistered_employer)
            ->with("follow_ups_datatable", $followUpsDataTable)
            ->with("check_tin", $check_tin);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->checkIfHasWorkflowDefinition();
        return view('backend.operation.compliance.member.unregistered_employer.create')
            ->withCountries($this->countries->getAll()->pluck('name', 'id'))
            ->withLocationTypes($this->location_types->getAll()->pluck('name', 'id'))
            ->withDistricts($this->districts->getAll()->pluck('name', 'id'))
            ->withRegions($this->regions->getAll()->pluck('name', 'id'))
            ->withBusinessSectors($this->code_values->query()->where('code_id',5)->where('code_id', 5)->where(function($query){
                $query->where('reference', '<>', 'BSOSA')->where('reference', '<>', 'BSOTHRS');
            })->get()->pluck('name','name'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUnregisteredEmployerRequest $request)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $unregistered_employer = $this->unregistered_employers->create($request->all());
        return redirect('compliance/unregistered_employer/profile/' . $unregistered_employer->id . '#general')->withFlashSuccess(trans('alerts.backend.member.unregistered_employer_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->checkIfHasWorkflowDefinition();
//        $this->unregistered_employers->checkIfUserAssigned($id);
        $unregistered_employer = $this->unregistered_employers->findOrThrowException($id);
        return view('backend.operation.compliance.member.unregistered_employer.edit')
            ->withCountries($this->countries->getAll()->pluck('name', 'id'))
            ->withLocationTypes($this->location_types->getAll()->pluck('name', 'id'))
            ->withDistricts($this->districts->getAll()->pluck('name', 'id'))
            ->withRegions($this->regions->getAll()->pluck('name', 'id'))
            ->withUnregisteredEmployer($unregistered_employer)
            ->withBusinessSectors($this->code_values->query()->where('code_id',5)->where('code_id', 5)->where(function($query){
                $query->where('reference', '<>', 'BSOSA')->where('reference', '<>', 'BSOTHRS');
            })->get()->pluck('name','name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, CreateUnregisteredEmployerRequest $request)
    {
        $this->checkIfHasWorkflowDefinition();
//        $this->unregistered_employers->checkIfUserAssigned($id);
        $this->unregistered_employers->checkIfEmployerIsRegistered($id);
        $this->unregistered_employers->update($id,$request->all());
        return redirect('compliance/unregistered_employer/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.member.unregistered_employer_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $this->unregistered_employers->checkIfEmployerIsRegistered($id);
        $this->unregistered_employers->delete($id);
        return redirect('compliance/unregistered_employer')->withFlashSuccess(trans('alerts.backend.member.unregistered_employer_deleted'));
    }


    /**
     * @param $id
     * Register unregistered employer
     */
    public function register($id)
    {
        $this->checkIfHasWorkflowDefinition();
//        $this->unregistered_employers->checkIfUserAssigned($id);
        $this->unregistered_employers->checkIfEmployerIsRegistered($id);
        $employer_registration = $this->unregistered_employers->register($id);
        return redirect()->route('backend.compliance.employer_registration.edit', $employer_registration->id)->withFlashSuccess(trans('alerts.backend.member.employer_registration_created'));
    }



    public function markRegistered($id)
    {
        $this->checkIfHasWorkflowDefinition();
        $this->unregistered_employers->checkIfUserAssigned($id);
        $employer_registration = $this->unregistered_employers->markRegistered($id);
        return redirect('compliance/unregistered_employer/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.member.unregistered_employer_mark_as_registered'));
    }



    /**
     * Upload bulk Unregistered Employers
     * Upload = Active
     */
    public function uploadBulk(){
        //$this->checkIfHasWorkflowDefinition();
        return view('backend.operation.compliance.member.unregistered_employer.upload_bulk');
    }




    /**
     *
     * Store Uploaded Bulk
     */
    public function storeUploadedBulk(){
        $this->checkIfHasWorkflowDefinition();
        if ($this->saveUnregisteredEmployerBulk()) {
                       dispatch(new LoadUnregisteredEmployerBulk(1));

               return response()->json(['success' => true, 'message' => trans('alerts.backend.finance.receipt.linked_file_created')]);
        }
        return response()->json(['success' => false, 'message' => trans('exceptions.backend.finance.receipts.contribution.upload')]);
    }




    /**
     * Upload bulk Unregistered Employers
     * Upload = InActive
     */
    public function uploadBulkInActive(){
        //$this->checkIfHasWorkflowDefinition();
        return view('backend.operation.compliance.member.unregistered_employer.upload_bulk_inactive');

    }


    /**
          * @return \Illuminate\Http\JsonResponse
     * Store Uploaded bulk file for inactive unregistered
     */
    public function storeUploadedBulkInactive(){
        $this->checkIfHasWorkflowDefinition();
        if ($this->saveUnregisteredEmployerBulk()) {
            dispatch(new LoadUnregisteredEmployerBulk(0));

            return response()->json(['success' => true, 'message' => trans('alerts.backend.finance.receipt.linked_file_created')]);
        }
        return response()->json(['success' => false, 'message' => trans('exceptions.backend.finance.receipts.contribution.upload')]);
    }




    /**
     *
     * FOLLOW UPS METHODS ==========================================
     *
     */


    public function getFollowUpsForDataTable($id) {
        return Datatables::of($this->unregistered_employers->getFollowUps($id))
            ->editColumn('date_of_follow_up', function($follow_up) {
                return $follow_up->date_of_follow_up_formatted;
            })
            ->editColumn('follow_up_type_cv_id', function($follow_up) {
                return $follow_up->followUpType->name;
            })
            ->editColumn('user_id', function($follow_up) {
                return ($follow_up->user()->count()) ? $follow_up->user->username :'';
            })
            ->make(true);
    }


// create
    public function createFollowUp($id)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $this->unregistered_employers->checkIfUserAssigned($id);
        $this->unregistered_employers->checkIfEmployerIsRegistered($id);
        $unregistered_employer = $this->unregistered_employers->findOrThrowException($id);
        return view('backend.operation.compliance.member.unregistered_employer.follow_up.create')
            ->with("unregistered_employer", $unregistered_employer)
            ->with("follow_up_types", $this->code_values->query()->where('code_id',9)->get()->pluck('name','id'));

    }
//  store
    public function storeFollowUp($id, CreateUnregisteredEmployerFollowUpRequest $request)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $this->unregistered_employers->checkIfUserAssigned($id);
        $this->unregistered_employers->checkIfEmployerIsRegistered($id);
        $unregistered_employer_follow_up = $this->unregistered_employer_follow_ups->create($id, $request->all());
        return redirect('compliance/unregistered_employer/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.member.follow_up_created'));

    }

//edit
    public function editFollowUp($unregistered_employer_follow_up_id)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $unregistered_employer_follow_up = $this->unregistered_employer_follow_ups->findOrThrowException($unregistered_employer_follow_up_id);
        $this->unregistered_employers->checkIfUserAssigned($unregistered_employer_follow_up->unregistered_employer_id);
        $unregistered_employer_follow_up = $this->unregistered_employer_follow_ups->findOrThrowException($unregistered_employer_follow_up_id);
        return view('backend.operation.compliance.member.unregistered_employer.follow_up.edit')
            ->withUnregisteredEmployerFollowUp($unregistered_employer_follow_up)
            ->withFollowUpTypes($this->code_values->query()->where('code_id',9)->get()->pluck('name','id'));
    }

//      update
    public function updateFollowUp($unregistered_employer_follow_up_id, CreateUnregisteredEmployerFollowUpRequest $request)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $unregistered_employer_follow_up = $this->unregistered_employer_follow_ups->findOrThrowException($unregistered_employer_follow_up_id);
        $this->unregistered_employers->checkIfUserAssigned
        ($unregistered_employer_follow_up->unregistered_employer_id);
        $this->unregistered_employers->checkIfEmployerIsRegistered($unregistered_employer_follow_up->unregistered_employer_id);
        $this->unregistered_employer_follow_ups->update($unregistered_employer_follow_up_id,$request->all());
        return redirect('compliance/unregistered_employer/profile/' . $unregistered_employer_follow_up->unregistered_employer_id . '#general')->withFlashSuccess(trans('alerts.backend.member.follow_up_updated'));

    }

//  delete
    public function deleteFollowUp($unregistered_employer_follow_up_id)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $unregistered_employer_follow_up = $this->unregistered_employer_follow_ups->findOrThrowException($unregistered_employer_follow_up_id);
        $this->unregistered_employers->checkIfUserAssigned($unregistered_employer_follow_up->unregistered_employer_id);
        $this->unregistered_employers->checkIfEmployerIsRegistered($unregistered_employer_follow_up->unregistered_employer_id);
        $this->unregistered_employer_follow_ups->delete($unregistered_employer_follow_up_id);
        return redirect('compliance/unregistered_employer/profile/' . $unregistered_employer_follow_up->unregistered_employer_id . '#general')->withFlashSuccess(trans('alerts.backend.member.follow_up_deleted'));
    }


//    END of Follow ups methods ----------------------------------------




    /**
     * CHANGE ASSIGNED STAFF =========================================
     */

    public function selfAssignStaff($id)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $this->unregistered_employers->assignStaff($id, access()->user()->id);
        return redirect('compliance/unregistered_employer/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.member.assign_staff'));
    }


    /**
     * @param $id
     * @return mixed
     * Assign Staff by supervisor
     */
    public function assignStaffPage($id)
    {
        //permission
        $unregistered_employer = $this->unregistered_employers->findOrThrowException($id);
        $users = new UserRepository();
        return view('backend.operation.compliance.member.unregistered_employer.assign_staff')
            ->withUnregisteredEmployer($unregistered_employer)
            ->withUsers($users->query()->orderBy('firstname', 'asc')->get()->pluck('name','id'));
    }




    /**
     * @param $id
     * @return mixed
     * Assign staff by supervisor
     */
    public function assignStaff($id, UpdateAssignedStaffRequest $request)
    {
        //permission
        $user_id = $request['assigned_to'];
          $this->unregistered_employers->assignStaff($id,$user_id);
        return redirect('compliance/unregistered_employer/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.member.assign_staff'));
    }

    public function editAssignedStaff($id)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $unregistered_employer = $this->unregistered_employers->findOrThrowException($id);
        $users = new UserRepository();
        return view('backend.operation.compliance.member.unregistered_employer.change_assigned_staff')
            ->withUnregisteredEmployer($unregistered_employer)
            ->withUsers($users->query()->orderBy('firstname', 'asc')->get()->pluck('name','id'));
    }


    public function updateAssignedStaff($id, UpdateAssignedStaffRequest $request)
    {
        //
        $this->checkIfHasWorkflowDefinition();
        $this->unregistered_employers->changeAssignedStaff($id, $request->all());
        return redirect('compliance/unregistered_employer/profile/' . $id . '#general')->withFlashSuccess(trans('alerts.backend.member.assigned_staff_updated'));
    }
    /*--end of change assigned staff -----------------*/



    /**
     * Check if can Initiate Action (Level 1 / No Workflow)
     */
    public function checkIfHasWorkflowDefinition(){
        /* Employer Registration module */
        $wf_module_group_id = 6;
        access()->hasWorkflowDefinition($wf_module_group_id,1);

    }

    /**
     * @return mixed
     *
     */
    public function getUnregisteredUnassignedEmployers()
    {
        return $this->unregistered_employers->getUnregisteredUnassignedEmployers(request()->input('q'));
    }

    public function addTra()
    {
        return view("backend/operation/compliance/member/unregistered_employer/add_tra");
    }

    public function addTraPost(Request $request)
    {
        $repo = new NewTaxpayerRepository();
        $repo->moveToUnregistered($request->all());
        return redirect()->back()->withFlashSuccess("Success, Selected employers have been added to the unregistered list");
    }

    public function getTra()
    {
        $repo = new NewTaxpayerRepository();
        return Datatables::of($repo->getForDataTable())
            ->addColumn("name", function ($query) {
                return $query->name;
            })
            ->addColumn("registration_date", function ($query) {
                return $query->dateofregistration_label;
            })
            ->make(true);
    }

    public function removeTra()
    {

    }

}
