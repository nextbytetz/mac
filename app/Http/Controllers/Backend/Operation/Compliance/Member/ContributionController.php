<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Member;

use App\DataTables\WorkflowTrackDataTable;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\DataTables\EmployerContributionDataTable;

class ContributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    protected $contributions;
    protected $receipts;
    protected $receipt_codes;


    public function __construct(ContributionRepository $contributions, ReceiptRepository $receipts, ReceiptCodeRepository $receipt_codes) {
        $this->contributions = $contributions;
        $this->receipts = $receipts;
        $this->receipt_codes = $receipt_codes;
    }



    public function index(EmployerContributionDataTable $dataTable)
    {
        return $dataTable->render('backend/operation/compliance/member/contribution/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}
