<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Inspection;

use App\DataTables\Compliance\Inspection\DishonouredChequesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Report\DateRangeRequest;
use Carbon\Carbon;

class DishonouredChequesController extends Controller
{


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

public function getDishonouredCheques(DishonouredChequesDataTable $dataTable,DateRangeRequest $request){

        $now = Carbon::now();
        $now = Carbon::parse($now)->format('Y-m-d');
    return $dataTable->with([
        'from_date' => (isset($request['from_date']) And ($request['from_date'])) ? Carbon::parse($request['from_date'])->format('Y-m-d') :
            $now,
        'to_date' => (isset($request['to_date']) And ($request['to_date'])) ? Carbon::parse($request['to_date'])->format('Y-m-d') : $now,])->render('backend/operation/compliance/inspection/dishonoured_cheques', ["request" => $request]);


}

}
