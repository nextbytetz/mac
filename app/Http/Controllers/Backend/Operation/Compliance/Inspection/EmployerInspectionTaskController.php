<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Inspection;

use App\DataTables\WorkflowTrackDataTable;
use App\Http\Requests\Backend\Notification\AttachEmployerTaskDocRequest;
use App\Http\Requests\Backend\Operation\Inspection\UpdateAssessmentPayrollRequest;
use App\Http\Requests\Backend\Operation\Inspection\UpdateEmployerInspectionTaskRequest;
use App\Http\Requests\Backend\Operation\Inspection\UploadInspectionAssessmentRequest;
use App\Models\Operation\Compliance\Inspection\EmployerInspectionTask;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionAssessmentTemplateRepository;
use App\Services\Storage\Traits\AttachmentHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class
EmployerInspectionTaskController extends Controller
{
    use AttachmentHandler;

    public $employer_task;

    /**
     * EmployerInspectionTaskController constructor.
     * @param EmployerInspectionTaskRepository $employer_task
     */
    public function __construct(EmployerInspectionTaskRepository $employer_task)
    {
        $this->employer_task = $employer_task;
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @param WorkflowTrackDataTable $workflowtrack
     * @return mixed
     */
    public function show(EmployerInspectionTask $employerTask, WorkflowTrackDataTable $workflowtrack)
    {
        if (!$employerTask->attended And ($employerTask->allocated == access()->id())) {
            return redirect()->route('backend.compliance.inspection.employer_task.attend', $employerTask->id);
        }
        $task = $employerTask->inspectionTask;
        $inspection = $task->inspection;
        $employer = $employerTask->employer;
        $assessment_summary = $this->employer_task->assessmentSummary($employerTask);
        $remittance = $this->employer_task->remittanceSummary($employerTask);
        $docs_attached = $this->employer_task->getDocsAttachedNonRecurring($employerTask);
        $payrolls = $employerTask->payrolls;

        //dd($assessment_summary);

        $assessments = (new InspectionAssessmentTemplateRepository())->query()->where("employer_inspection_task_id", $employerTask->id)->orderBy("contrib_month", "asc");
        return view('backend/operation/compliance/inspection/employer/show')
                ->with("employer_task", $employerTask)
                ->with("inspection", $inspection)
                ->with("task", $task)
                ->with("employer", $employer)
                ->with("assessment_summary", $assessment_summary)
                ->with("remittance", $remittance)
                ->with("assessments", $assessments)
                ->with("docs_attached", $docs_attached)
                ->with("payrolls", $payrolls)
                ->with('workflowtrack', $workflowtrack);
    }

    public function attend(EmployerInspectionTask $employerTask)
    {
        $task = $employerTask->inspectionTask;
        $inspection = $task->inspection;
        $employer = $employerTask->employer;
        return view("backend/operation/compliance/inspection/attend")
                    ->with("employer_task", $employerTask)
                    ->with("inspection", $inspection)
                    ->with("employer", $employer)
                    ->with("task", $task);
    }

    public function postAttend(EmployerInspectionTask $employerTask)
    {
        $this->employer_task->updateAttendedStatus($employerTask);
        //This employer inspection task is already attended.
        return redirect()->route("backend.compliance.inspection.employer_task.show", $employerTask->id);
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @return mixed
     */
    public function updateVisitation(EmployerInspectionTask $employerTask)
    {
        $task = $employerTask->inspectionTask;
        $inspection = $task->inspection;
        $employer = $employerTask->employer;
        return view('backend/operation/compliance/inspection/employer/visitation')
                ->with("employer_task", $employerTask)
                ->with("inspection", $inspection)
                ->with("employer", $employer)
                ->with("task", $task);
    }

    public function managePayroll(EmployerInspectionTask $employerTask)
    {
        if (!$employerTask->review_start_date Or !$employerTask->review_end_date) {
            return redirect()->back()->withFlashDanger('Please update first review start date and review end date in visitation');
        }

        $payrolls = $employerTask->payrolls;
        $users = collect(DB::select("select a.payroll_id, b.name from portal.employer_user a join portal.users b on a.user_id = b.id where employer_id = {$employerTask->employer_id} order by payroll_id asc;"))->pluck("name", "payroll_id")->all();
        $task = $employerTask->inspectionTask;
        $inspection = $task->inspection;
        $employer = $employerTask->employer;
        //dd($users);
        return view('backend/operation/compliance/inspection/employer/payroll/manage')
                ->with('payrolls', $payrolls)
                ->with('users', $users)
                ->with('employer_task', $employerTask)
                ->with('inspection', $inspection)
                ->with('employer', $employer)
                ->with('task', $task);
    }

    public function payrollEntries(EmployerInspectionTask $employerTask)
    {
        $users = collect(DB::select("select a.payroll_id, b.name from portal.employer_user a join portal.users b on a.user_id = b.id where employer_id = {$employerTask->employer_id} order by payroll_id asc;"))->pluck("name", "payroll_id")->all();
        return view('backend/operation/compliance/inspection/employer/includes/payroll_entry')
                ->with("users", $users)
                ->with("selected_entries", request()->input('selected_entries'));
    }

    public function postPayroll(EmployerInspectionTask $employerTask, UpdateAssessmentPayrollRequest $request)
    {
        $result = $this->employer_task->postPayroll($employerTask, $request->all());
        return response()->json(['success' => true, 'message' => 'Success, All changes have been updated!']);
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @param UpdateEmployerInspectionTaskRequest $request
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function postUpdateVisitation(EmployerInspectionTask $employerTask, UpdateEmployerInspectionTaskRequest $request)
    {
        $input = $request->all();
        $return = $this->employer_task->postUpdateVisitation($employerTask, $input);
        return redirect()->route("backend.compliance.inspection.employer_task.show", $employerTask->id)->withFlashSuccess("Success, Employer Task Visitation has been Updated.");
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @return mixed
     */
    public function skipStage(EmployerInspectionTask $employerTask)
    {
        $return = $this->employer_task->skipStage($employerTask);
        return redirect()->route("backend.compliance.inspection.employer_task.show", $employerTask->id)->withFlashSuccess("Success, Previous Stage has been skipped");
    }

    /**
     * @return mixed
     */
    public function initiateWorkflow()
    {
        $input = request()->all();
        $return = $this->employer_task->initiateWorkflow($input);
        return redirect()->back()->withFlashSuccess("Success, Workflow has been initialized.");
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function assessmentTemplate(EmployerInspectionTask $employer_task, $inspection_task_payroll)
    {
        return $this->employer_task->assessmentTemplate($employer_task,"", $inspection_task_payroll);
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function downloadAssessmentSchedule(EmployerInspectionTask $employerTask, $inspection_task_payroll)
    {
        return $this->employer_task->downloadAssessmentSchedule($employerTask, $inspection_task_payroll);
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function downloadAssessmentError(EmployerInspectionTask $employerTask)
    {
        return $this->employer_task->downloadAssessmentError($employerTask);
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\GeneralException
     */
    public function uploadAssessmentTemplate(EmployerInspectionTask $employerTask)
    {
        $this->employer_task->checkUploadAssessmentTemplate($employerTask);
        $task = $employerTask->inspectionTask;
        $inspection = $task->inspection;
        $employer = $employerTask->employer;
        $payrolls = $employerTask->payrolls()->pluck("name", "id")->all();
        return view("backend/operation/compliance/inspection/employer/upload_assessment")
                ->with("employer_task", $employerTask)
                ->with("payrolls", $payrolls)
                ->with("task", $task)
                ->with("inspection", $inspection)
                ->with("employer", $employer);
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @param UploadInspectionAssessmentRequest $request
     * @return array|mixed
     */
    public function postUploadAssessmentTemplate(EmployerInspectionTask $employerTask, UploadInspectionAssessmentRequest $request)
    {
        if ($this->saveEmployerAssessmentSchedule($employerTask->id)) {
            //Process the excel file
            $return = $this->employer_task->processAssessmentTemplate($employerTask, $request->all());
        } else {
            $return = ['success' => false, "message" => "File not uploaded..."];
        }
        return response()->json($return);
    }

    /**
     * @param AttachEmployerTaskDocRequest $request
     * @return mixed
     */
    public function storeDocument(AttachEmployerTaskDocRequest $request)
    {
        $input = $request->all();
        $employer_inspection_task_id = $input['employer_inspection_task_id'];
        $employerTask = $this->employer_task->find($employer_inspection_task_id);
        $this->employer_task->saveEmployerTaskDocuments($employerTask, $input);
        $return_page = isset($input['return_page']) ? 1 : 0;
        if ($return_page) {
            $return = route("backend.compliance.inspection.employer_task.attach_document", $employerTask->id);
        } else {
            $return = route("backend.compliance.inspection.employer_task.show", $employerTask->id);
        }
        return redirect($return)->withFlashSuccess('Success, Document attached');
    }

    /**
     * @param EmployerInspectionTask $employerTask
     * @return mixed
     */
    public function attachDocument(EmployerInspectionTask $employerTask)
    {
        $documentRepo = new DocumentRepository();

        access()->assignedForEmployerInspectionTask($employerTask);
        $task = $employerTask->inspectionTask;
        $inspection = $task->inspection;
        $employer = $employerTask->employer;

        $documents = $documentRepo->query()->whereIn('document_group_id', [20])->where("ismandatory", 1)->pluck('name', 'id')->all();

        return view('backend/operation/compliance/inspection/employer/attach_doc')
            ->with('employer_task',  $employerTask)
            ->with("task", $task)
            ->with("inspection", $inspection)
            ->with("employer", $employer)
            ->with('documents',  $documents);
    }

    public function editDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer_inspection_task')->where('id', $doc_pivot_id)->first();
        $employerTask = $this->employer_task->find($uploaded_doc->employer_inspection_task_id);
        access()->assignedForEmployerInspectionTask($employerTask);
        $task = $employerTask->inspectionTask;
        $inspection = $task->inspection;
        $employer = $employerTask->employer;
        $document = $employerTask->documents()->where('document_employer_inspection_task.id', $doc_pivot_id)->first();
        return view('backend/operation/compliance/inspection/employer/edit_doc')
            ->with('employer_task',  $employerTask)
            ->with("task", $task)
            ->with("inspection", $inspection)
            ->with("employer", $employer)
            ->with('uploaded_document',  $document);
    }

    public function updateDocument(AttachEmployerTaskDocRequest $request)
    {
        $input = $request->all();
        $employer_inspection_task_id = $input['employer_inspection_task_id'];
        $employerTask = $this->employer_task->find($employer_inspection_task_id);
        $this->employer_task->saveEmployerTaskDocuments($employerTask, $input);
        $return = route("backend.compliance.inspection.employer_task.show", $employerTask->id);
        return redirect($return)->withFlashSuccess('Success, Document attached');
    }

    public function deleteDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer_inspection_task')->where('id', $doc_pivot_id)->first();
        $employerTask = $this->employer_task->find($uploaded_doc->employer_inspection_task_id);
        access()->assignedForEmployerInspectionTask($employerTask);
        $this->employer_task->deleteDocument($doc_pivot_id);
        $return = route("backend.compliance.inspection.employer_task.show", $employerTask->id);
        return redirect($return)->withFlashSuccess('Success, Document Dettached');
    }

    /**
     * @param EmployerInspectionTask $doc_pivot_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function previewDocument($doc_pivot_id)
    {
        $uploaded_doc = DB::table('document_employer_inspection_task')->where('id', $doc_pivot_id)->first();
        $employer_inspection_task_id = $uploaded_doc->employer_inspection_task_id;
        $employerTask = $this->employer_task->find($employer_inspection_task_id);
        if (test_uri()) {
            $url = url("/") . "/ViewerJS/#../storage/inspection/employer_task/{$employerTask->id}/{$uploaded_doc->id}.{$uploaded_doc->ext}";
        } else {
            $url = url("/") ."/public/ViewerJS/#../storage/inspection/employer_task/{$employerTask->id}/{$uploaded_doc->id}.{$uploaded_doc->ext}";
        }
        //$url = $employerTask->getEmployerTaskPathFileUrl($doc_pivot_id);
        return response()->json(['success' => true, "url" => $url, "name" => $uploaded_doc->description, "id" => $doc_pivot_id]);
    }

   
}
