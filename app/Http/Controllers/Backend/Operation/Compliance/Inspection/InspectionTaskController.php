<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Inspection;

use App\Exceptions\GeneralException;
use App\Http\Requests\Backend\Operation\Inspection\CreateInspectionTaskRequest;
use App\Http\Requests\Backend\Operation\Inspection\UpdateEmployerInspectionTaskRequest;
use App\Models\Operation\Compliance\Inspection\EmployerInspectionTask;
use App\Models\Operation\Compliance\Inspection\Inspection;
use App\Models\Operation\Compliance\Inspection\InspectionTask;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionProfileRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use Illuminate\Support\Facades\DB;
use App\Services\Storage\Traits\AttachmentHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;

class InspectionTaskController extends Controller
{
    use AttachmentHandler;

    public $inspection_task;

    public function __construct(InspectionTaskRepository $inspection_task)
    {
        $this->inspection_task = $inspection_task;
        //$this->middleware('access.routeNeedsPermission:assign_inspection', ['only' => ['create']]);
        //$this->middleware('access.routeNeedsPermission:assign_task_to_staff', ['only' => ['assignTask']]);
        //$this->middleware('access.routeNeedsPermission:edit_task_inspection', ['only' => ['edit']]);
        //$this->middleware('access.routeNeedsPermission:delete_task_inspection', ['only' => ['destroy']]);
    }

    /**
     *
     */
    public function index()
    {

    }

    /**
     * @param Inspection $inspection
     * @return mixed
     */
    public function create(Inspection $inspection)
    {
        access()->assignedForInspection();
        $profile = new InspectionProfileRepository();
      /*  if ($inspection->inspection_type_id == 3){
            $profiles = $profile->getAll()->pluck('name_with_unregistered_count', "id");
        }else{
            $profiles = $profile->getAll()->pluck('name_with_employer_count', "id");
        }*/
        $profiles = $profile->getAll()->pluck('name_with_employer_count', "id");
        $codeValueRepo = new CodeValueRepository();
        return view("backend/operation/compliance/inspection/task/create")
            ->with("inspection", $inspection)
            ->with("inspection_types", $codeValueRepo->getCodeValuesByCodeForSelect(29))
            ->with("profiles", $profiles);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param InspectionTask $task
     * @return mixed
     */
    public function edit(InspectionTask $task)
    {

        access()->assignedForInspection();
        $codeValueRepo = new CodeValueRepository();
        $profile = new InspectionProfileRepository();
        $unregisteredEmployer = new UnregisteredEmployerRepository();
        $employers = (($task->target_employer == 0) ? $task->employers->pluck("name", "id")->all() : []);
        $employersIds = (($task->target_employer == 0) ? $task->employers->pluck("id")->all() : []);
/*        $unregisteredEmployersIds = (($task->target_employer == 0) ? $task->unregisteredEmployers->pluck("id")->all() : []);
        $unregistered_employers = $unregisteredEmployer->query()->where('assign_to', 0)->get()->pluck('name','id');

        if ($task->inspection->inspection_type_id == 3){
            $profiles = $profile->getAll()->pluck('name_with_unregistered_count', "id");
        }else{
            $profiles = $profile->getAll()->pluck('name_with_employer_count', "id");
        }*/
        $profiles = $profile->getAll()->pluck('name_with_employer_count', "id");
        return view("backend/operation/compliance/inspection/task/edit")
            ->with("task", $task)
            ->with("inspection", $task->inspection)
            ->with("inspection_types", $codeValueRepo->getCodeValuesByCodeForSelect(29))
            /*->with("staffs", $task->users->pluck("name", "id")->all())
            ->with("staffs_ids", $task->users->pluck("id")->all())*/
            ->with("employers", $employers)
            ->with("employers_ids", $employersIds)
            /*->with("other_employers", $task->otherEmployers->pluck("employer_name")->all())
            ->with("unregistered_employers", $unregistered_employers)
            ->with("unregistered_employers_ids", $unregisteredEmployersIds)*/
            ->with("profiles", $profiles);
    }

    /**
     * @param CreateInspectionTaskRequest $request
     * @return mixed
     */
    public function store(CreateInspectionTaskRequest $request)
    {
         /* if($request->input('inspection_type') == 3)
        {
            //Follow up inspection
            $inspectionTask = $this->inspection_task->storeFollowup($request->all());
        }else{
            $inspectionTask = $this->inspection_task->store($request->all());
        }*/
         $check = true;
         $input = $request->all();
        switch ($input['target_employer']) {
         case '2':
             //Imported from Excel
             $filename = time();
             $input['filename'] = $filename;
             $path = inspection_task_dir();
             $fileindex = "document_file";
             if (!$this->saveDocumentGeneric($filename, $path, $fileindex)) {
                 $check = false;
             }
             break;
        }
        if ($check) {
            $return = $this->inspection_task->store($input);
        } else {
            $return = ['success' => false, "message" => "File not uploaded..."];
        }
        //$inspectionTask = $this->inspection_task->store($input);
        return response()->json($return);
        //return redirect()->route('backend.compliance.inspection.show', $inspectionTask->inspection_id)->withFlashSuccess(trans('alerts.backend.inspection_task.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InspectionTask $task
     * @param CreateInspectionTaskRequest $request
     * @return mixed
     */
    public function update(InspectionTask $task, CreateInspectionTaskRequest $request)
    {
        //        mark_change1
/*        if($task->inspection->inspectionType->id == 3)
        {
            //Follow up inspection
            $this->inspection_task->updateFollowup($task->id, $request->all());
        }else{
            $this->inspection_task->update($task, $request->all());
        }*/
        $check = true;
        $input['hasfile'] = 0;
        $input['filename'] = NULL;
        $input = $request->all();
        switch ($input['target_employer']) {
            case '2':
                $fileindex = "document_file";
                //Imported from Excel
                if ($request->hasFile($fileindex)) {
                    $filename = time();
                    $input['filename'] = $filename;
                    $input['hasfile'] = 1;
                    $path = inspection_task_dir();
                    if (!$this->saveDocumentGeneric($filename, $path, $fileindex)) {
                        $check = false;
                    }
                }
                break;
        }
        if ($check) {
            $return = $this->inspection_task->update($task, $input);
        } else {
            $return = ['success' => false, "message" => "File not uploaded..."];
        }
        return response()->json($return);
        //$this->inspection_task->update($task, $request->all());
        //return redirect()->route('backend.compliance.inspection.task.show', $task->id)->withFlashSuccess(trans('alerts.backend.inspection_task.updated'));
    }

    /**
     * Display the specified resource.
     *
     * @param InspectionTask $task
     * @return mixed
     */
    public function show(InspectionTask $task)
    {
        $default = ['0' => 'All'];
        $allocationsQuery = (new UserRepository())->query()->whereHas("employerInspectionTasks", function($query) use ($task){
            $query->where("inspection_task_id", $task->id);
        });
        //$users = $task->allocations()->select([DB::raw("concat_ws(' ', users.firstname, users.lastname) username"), DB::raw("users.id userid")])->pluck("username", "userid")->all();
        $users = $allocationsQuery->select(["id", "firstname", "lastname", DB::raw("concat_ws(' ', users.firstname, users.lastname) username"), DB::raw("users.id userid")])->pluck("username", "userid")->all();
        $allocations = $allocationsQuery->get();
        //dd($allocations);
        $inspection = $task->inspection;
        //dd($task->allocations()->toSql());
        //$target_staffs = $default + $task->allocations->pluck("name", "id")->all();
        $target_staffs = $default + $users;
        /* start : Set to display task of the specific user */
        $user_id = request()->has('user_id') ? request()->input("user_id") : 0;
        /* end : Set to display task of the specific user */
        $inspection_end = Carbon::parse($inspection->end_date);
        $inspection_start = Carbon::parse($inspection->start_date);
        $max_year = (int) sysdefs()->data()->inspection_years_period + (int) Carbon::now()->format("Y");

        //        mark_change1
/*        if($task->inspection->inspectionType->id == 3)
        {
            //Follow up inspection
            $view ="backend.operation.compliance.inspection.task.show";
        }else{
            $view ="backend/operation/compliance/inspection/task/show";
        }*/
        $view ="backend/operation/compliance/inspection/task/show";

        return view($view)
            ->with("task", $task)
            ->with("inspection", $inspection)
            ->with("max_year", $max_year)
            ->with("inspection_start", $inspection_start->format("Y-n-j"))
            ->with("inspection_end", (is_null($inspection_end) ? '' : Carbon::parse($inspection_end)->format("Y-n-j")))
            ->with("target_staffs", $target_staffs)
            ->with("allocations", $allocations)
            ->with("user_id", $user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param InspectionTask $task
     * @return mixed
     */
    public function destroy(InspectionTask $task)
    {
        access()->assignedForInspection();
        $inspection_id = $task->inspection->id;
        //        mark_change1
/*        if($task->inspection->inspectionType->id == 3)
        {
            //Follow up inspection
            $this->inspection_task->destroyFollowup($task->id);
        }else{
            $this->inspection_task->destroy($task);
        }*/
        $this->inspection_task->destroy($task);
        return redirect()->route('backend.compliance.inspection.show', $inspection_id)->withFlashSuccess(trans('alerts.backend.inspection_task.deleted'));
    }

    /**
     * @return mixed
     */
    public function getListForDataTable()
    {
        $inspection_id = request()->input("inspection_id");
        return Datatables::of($this->inspection_task->getForDataTable($inspection_id))
            ->addColumn("completed", function ($inspection_task) {
                return $inspection_task->completed_label;
            })
            ->rawColumns(['completed'])
            ->make(true);
    }

    public function getEmployersForDatatable($task, Request $request)
    {
        $input = $request->all();
        //Log::info($input);
        return Datatables::of($this->inspection_task->getEmployersForDatatable($task, $input))
            ->make(true);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchUsers()
    {
        return $this->inspection_task->getStaffs(request()->input('q'), request()->input('inspection_id'));
    }

    /**
     * Update Employer Inspection Task Entry
     *
     * @param EmployerInspectionTask $employer_inspection_task
     * @param UpdateEmployerInspectionTaskRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateEmployer(EmployerInspectionTask $employer_inspection_task, UpdateEmployerInspectionTaskRequest $request)
    {
        $employer_inspection_task = $this->inspection_task->updateEmployer($employer_inspection_task, $request->all());
        $this->saveInspectionContributionAnalysis($employer_inspection_task);
        return response()->json(['success' => true, 'id' => $employer_inspection_task->id, 'status' => $employer_inspection_task->status]);
    }

    /**
     * Download the list of employers specific in a certain inspection task.
     * @param $task
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function downloadEmployers(InspectionTask $task)
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=download_task_employers.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        //$task = $this->inspection_task->findOrThrowException($task_id);
        //        mark_change1
        /*if($task->inspection->inspectionType->id == 3)
        {
            //Follow up inspection
            $list = $this->inspection_task->downloadEmployersFollowup($task_id);
        }else{
            $list = $this->inspection_task->downloadEmployers($task_id);
        }*/
        $list = $this->inspection_task->downloadEmployers($task->id);
        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };
        return response()->stream($callback, 200, $headers);
    }

    /**
     * @param Inspection $inspection
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function assignTask(Inspection $inspection)
    {

        //mark_change1
/*        if($task->inspection->inspectionType->id == 3)
        {
            //Follow up inspection
            $this->inspection_task->assignFollowup($task->id);
        }else{
            $this->inspection_task->assign($task);
        }*/
        if ($inspection->progressive_stage == 2) {
            throw new GeneralException("Action cancelled, automatically allocation is only possible when inspection plan has not been approved yet ...");
        }

        $wfModuleRepo = new WfModuleRepository();
        $wfData = $wfModuleRepo->employerInspectionPlanType();
        $module = $wfModuleRepo->getModule(['wf_module_group_id' => $wfData['group'], 'type' => $wfData['type']]);
        $level = $wfModuleRepo->employerInspectionPlanAssignInspectorLevel($module);
        access()->hasWorkflowModuleDefinition($wfData['group'], $wfData['type'], $level);

        $this->inspection_task->assign($inspection);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.inspection_task.assigned'));
    }

}
