<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Inspection;

use App\DataTables\Compliance\Inspection\InspectionProfileDataTable;
use App\Http\Requests\Backend\Operation\Inspection\CreateInspectionProfileRequest;
use App\Models\Operation\Compliance\Inspection\InspectionProfile;
use App\Repositories\Backend\Sysdef\BusinessRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Services\Storage\DataFormat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionProfileRepository;
use Yajra\Datatables\Facades\Datatables;

class InspectionProfileController extends Controller
{

    public $profile;

    public function __construct(InspectionProfileRepository $profile)
    {
        $this->profile = $profile;
        //$this->middleware('access.routeNeedsPermission:create_inspection_profile', ['only' => ['create']]);
        //$this->middleware('access.routeNeedsPermission:edit_inspection_profile', ['only' => ['edit']]);
        //$this->middleware('access.routeNeedsPermission:delete_inspection_profile', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param InspectionProfileDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(InspectionProfileDataTable $dataTable)
    {
        return $dataTable->render("backend/operation/compliance/inspection/profile/index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        access()->assignedForInspection();
        $business = new CodeValueRepository();
        $regions = new RegionRepository();
        return view("backend/operation/compliance/inspection/profile/create")
            ->with("businesses", $business->query()->where('code_id', 5)->get()->pluck('name', 'id'))
            ->with("regions", $regions->getAll()->pluck('name', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateInspectionProfileRequest $request
     * @return mixed
     */
    public function store(CreateInspectionProfileRequest $request)
    {
        $profile = $this->profile->store($request->all());
        return redirect()->route('backend.compliance.inspection.profile.show', $profile->id)->withFlashSuccess(trans('alerts.backend.inspection.profile.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param InspectionProfile $profile
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(InspectionProfile $profile)
    {
        return view("backend/operation/compliance/inspection/profile/show")
            ->with("profile", $profile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param InspectionProfile $profile
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(InspectionProfile $profile)
    {
        access()->assignedForInspection();
        $business = new CodeValueRepository();
        $regions = new RegionRepository();
        $dataFormat = new DataFormat();
        $data = $dataFormat->dbToArrayData($profile->query);
        return view("backend/operation/compliance/inspection/profile/edit")
            ->with("profile", $profile)
            ->with("businesses", $business->query()->where('code_id', 5)->get()->pluck('name', 'id'))
            ->with("business_ids", (isset($data['business_id'])) ? $data['business_id'] : null)
            ->with("regions", $regions->getAll()->pluck('name', 'id'))
            ->with("region_ids", (isset($data['region_id'])) ? $data['region_id'] : null);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InspectionProfile $profile
     * @param CreateInspectionProfileRequest $request
     * @return mixed
     */
    public function update(InspectionProfile $profile, CreateInspectionProfileRequest $request)
    {
        $this->profile->update($profile, $request->all());
        return redirect()->route('backend.compliance.inspection.profile.show', $profile->id)->withFlashSuccess(trans('alerts.backend.inspection.profile.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param InspectionProfile $profile
     * @return mixed
     */
    public function destroy(InspectionProfile $profile)
    {
        access()->assignedForInspection();
        $this->profile->destroy($profile);
        return redirect()->route('backend.compliance.inspection.profile.index')->withFlashSuccess(trans('alerts.backend.inspection.profile.deleted'));
    }

    public function getEmployerListForDataTable(InspectionProfile $profile)
    {
        return Datatables::of($this->profile->getEmployerForDataTable($profile))
            ->addColumn("region", function ($employer) {
                return $employer->region->name;
            })
            ->addColumn("business", function ($employer) {
                return $employer->businesses()->first()->name;
            })
            ->make(true);
    }
    
    public function getUnregisteredEmployerListForDataTable(InspectionProfile $profile)
    {
        return Datatables::of($this->profile->getUnregisteredEmployerForDataTable($profile))
            ->addColumn("region", function ($employer) {
                return $employer->region->name;
            })
             ->make(true);
    }

}
