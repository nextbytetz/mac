<?php

namespace App\Http\Controllers\Backend\Operation\Compliance\Inspection;

use App\DataTables\Compliance\Inspection\InspectionDataTable;
use App\DataTables\WorkflowTrackDataTable;
use App\Http\Requests\Backend\Operation\Inspection\CreateInspectionRequest;
use App\Http\Requests\Backend\Operation\Inspection\UpdateInspectionRequest;
use App\Models\Operation\Compliance\Inspection\Inspection;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionTypeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;

class InspectionController extends Controller
{

    public $inspection;

    public function __construct(InspectionRepository $inspection)
    {
        $this->inspection = $inspection;
        //$this->middleware('access.routeNeedsPermission:register_inspection', ['only' => ['create']]); //edit_inspection
        //$this->middleware('access.routeNeedsPermission:edit_inspection', ['only' => ['edit']]);
        //$this->middleware('access.routeNeedsPermission:cancel_inspection', ['only' => ['cancel']]);
        //$this->middleware('access.routeNeedsPermission:complete_inspection', ['only' => ['complete']]);
    }

    /**
     * @param InspectionDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(InspectionDataTable $dataTable)
    {
        return $dataTable->with('current', 0)->render("backend/operation/compliance/inspection/index");
    }

    public function current(InspectionDataTable $dataTable)
    {
        return $dataTable->with('current', 1)->render("backend/operation/compliance/inspection/current");
    }

    public function systemRaised()
    {
        //return view("");
    }

    //TODO: Tables to delete; inspection_types, incident_user (after changing the implementation codes)
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        access()->assignedForInspection();
        $maxYear = (int) Carbon::now()->format('Y') + (int) sysdefs()->data()->inspection_years_period;
        $codeValueRepo = new CodeValueRepository();
        return view("backend/operation/compliance/inspection/create")
            ->with("inspection_types", $codeValueRepo->getCodeValuesReferenceByCodeForSelect(29))
            ->with("max_year", $maxYear);
    }

    /**
     * @param CreateInspectionRequest $request
     * @return mixed
     */
    public function store(CreateInspectionRequest $request)
    {
        $inspection = $this->inspection->store($request->all());
        return redirect()->route('backend.compliance.inspection.show', $inspection->id)->withFlashSuccess(trans('alerts.backend.inspection.created'));
    }

    public function allocation(Inspection $inspection)
    {
        $wfModuleRepo = new WfModuleRepository();
        $wfData = $wfModuleRepo->employerInspectionPlanType();
        $module = $wfModuleRepo->getModule(['wf_module_group_id' => $wfData['group'], 'type' => $wfData['type']]);
        $level = $wfModuleRepo->employerInspectionPlanAssignInspectorLevel($module);
        $canAssign = access()->user()->hasWorkflowModuleDefinition($wfData['group'], $wfData['type'], $level);
        return view("backend/operation/compliance/inspection/allocation")
                ->with("users", $inspection->users()->get()->pluck('name', 'id')->all())
                //->with("all_users", (new UserRepository())->query()->get()->pluck('name', 'id'))
                ->with("regions", (new RegionRepository())->query()->pluck("name", "id")->all())
                ->with("can_assign", $canAssign)
                ->with("resource_status", $this->inspection->getResourceStatus($inspection->id))
                ->with("stages", (new CodeValueRepository())->getCodeValuesEmployerInspectionStageForSelect())
                ->with("inspection", $inspection);
    }

    public function getAllocation($id)
    {
        $datatables = $this->inspection->getResourceAllocationDatatable($id);
        return $datatables->make(true);
    }

    public function getAllocationInspectionTypeGroup($inspection)
    {
        $allocation = $this->inspection->getResourceAllocationInspectionTypeGroup($inspection);
        return view("backend/operation/compliance/inspection/includes/allocation_inspection_type_group")->with("allocation", $allocation);
    }

    public function getAllocationStaffGroup($inspection)
    {
        $allocation = $this->inspection->getResourceAllocationStaffGroup($inspection);
        return view("backend/operation/compliance/inspection/includes/allocation_staff_group")->with("allocation", $allocation);
    }

    /**
     * @param Inspection $inspection
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignAllocation(Inspection $inspection)
    {
        $input = request()->all();
        logger("I have passed here");
        $output = (new InspectionTaskRepository())->assignAllocation($inspection, $input);
        return response()->json(['success' => true, 'message' => 'Success, user have been assigned to the selected resource(s)']);
    }

    /**
     * @param Inspection $inspection
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\GeneralException
     */
    public function assignInspectors(Inspection $inspection)
    {

        $wfModuleRepo = new WfModuleRepository();
        $wfData = $wfModuleRepo->employerInspectionPlanType();
        $module = $wfModuleRepo->getModule(['wf_module_group_id' => $wfData['group'], 'type' => $wfData['type']]);
        $level = $wfModuleRepo->employerInspectionPlanAssignInspectorLevel($module);
        //access()->hasWorkflowModuleDefinition($wfData['group'], $wfData['type'], $level);

        $inspectors = (new CodeValueRepository())->getAllUsersForSelectByReference("CDAUTHRSDINSPCTR");
        //dd($inspectors);
        return view("backend.operation.compliance.inspection.assign_inspectors")
                ->with("inspection", $inspection)
                ->with("inspectors", $inspectors);
    }

    /**
     * @param Inspection $inspection
     * @return \Illuminate\Http\JsonResponse
     */
    public function postInspectors(Inspection $inspection)
    {
        $input = request()->all();
        $return = $this->inspection->postInspectors($inspection, $input);
        return response()->json(['success' => true, 'message' => 'Success, All changes have been updated!']);
    }

    public function getInspectorForDatatable(Inspection $inspection)
    {
        return Datatables::of($this->inspection->getInspectorForDatatable($inspection->id))
            ->make(true);
    }

    /**
     * @param Inspection $inspection
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\WorkflowException
     */
    public function removeInspectors(Inspection $inspection)
    {
        $input = request()->all();
        $return = $this->inspection->removeInspectors($inspection, $input);
        return response()->json(['success' => true, 'message' => 'Success, All changes have been updated!']);
    }

    /**
     * @param Inspection $inspection
     */
    public function show(Inspection $inspection, WorkflowTrackDataTable $workflowtrack)
    {
        return view("backend/operation/compliance/inspection/show")
                    ->with("inspection", $inspection)
                    ->with("workflowtrack", $workflowtrack);
    }

    /**
     * @param Inspection $inspection
     */
    public function edit(Inspection $inspection)
    {
        access()->assignedForInspection();
        $maxYear = (int) Carbon::now()->format('Y') + (int) sysdefs()->data()->inspection_years_period;
        $codeValueRepo = new CodeValueRepository();
        $inspectionTypes = new InspectionTypeRepository();
        $startDate = Carbon::parse($inspection->start_date);
        $inspectionType = $inspection->inspectionType;
        $endDate = ($inspection->end_date)?Carbon::parse($inspection->end_date):null;
        return view("backend/operation/compliance/inspection/edit")
            ->with("max_year", $maxYear)
            ->with("inspection_types", $codeValueRepo->getCodeValuesReferenceByCodeForSelect(29))
            ->with("inspection_type", $inspectionType)
            ->with("inspection", $inspection)
            ->with("inspection_staffs", $inspection->users->pluck("name", "id")->all())
            ->with("inspection_staffs_ids", $inspection->users->pluck("id")->all())
            ->with("start_date", $startDate)
            ->with("end_date", $endDate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Inspection $inspection
     * @param CreateInspectionRequest $request
     * @return mixed
     */
    public function update(Inspection $inspection, UpdateInspectionRequest $request)
    {
        $this->inspection->update($inspection, $request->all());
        return redirect()->route('backend.compliance.inspection.show', $inspection->id)->withFlashSuccess(trans('alerts.backend.inspection.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cancel(Inspection $inspection)
    {
        access()->assignedForInspection();
        $this->inspection->cancel($inspection);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.inspection.cancelled'));
    }

    public function complete(Inspection $inspection)
    {
        access()->assignedForInspection();
        $this->inspection->complete($inspection);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.inspection.completed'));
    }

    public function getEmployerListForDataTable($employer_id)
    {
        return Datatables::of((new EmployerInspectionTaskRepository())->getEmployerListForDataTable($employer_id))
            //->rawColumns(['status'])
            ->make(true);
    }

    public function initiateWorkflow()
    {
        $input = request()->all();
        $return = $this->inspection->initiateWorkflow($input);
        return redirect()->back()->withFlashSuccess("Success, Workflow has been initialized.");
    }

}
