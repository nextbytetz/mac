<?php

namespace App\Http\Controllers\Backend\Operation\Compliance;

use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComplianceController extends Controller
{

    public function __construct()
    {
        //notification defaults
        $this->middleware('access.routeNeedsPermission:configure_compliance_defaults', ['only' => ['defaults']]);
    }

    public function defaults()
    {
        $codeValueRepo = new CodeValueRepository();
        return view("backend/operation/compliance/defaults")
            ->with("users", (new UserRepository())->query()->get()->pluck("name", "id"))
            ->with("inspection_masters", $codeValueRepo->getAllUsersByReference("CDEMPLYIMSR"))
            ->with("authorised_inspectors", $codeValueRepo->getAllUsersByReference("CDAUTHRSDINSPCTR"))
            ->with("temp_deregistration_reopen_officers", $codeValueRepo->getAllUsersByReference("CDAEMPREOPUS"));
    }

    public function postDefaults($reference, Request $request)
    {
        $input = $request->all();

        $codeValueRepo = new CodeValueRepository();
        $code = $codeValueRepo->query()->where("reference", $reference)->first();
        switch ($code->id) {
            case $codeValueRepo->CDEMPLYIMSR():
                //Employer Inspection Master
                if (isset($input['inspection_masters'])) {
                    $code->users()->sync($input['inspection_masters']);
                } else {
                    $code->users()->sync([]);
                }
                break;
            case $codeValueRepo->CDAUTHRSDINSPCTR():
                //Authorised Inspectors
                if (isset($input['authorised_inspectors'])) {
                    $code->users()->sync($input['authorised_inspectors']);
                } else {
                    $code->users()->sync([]);
                }
                break;

            case $codeValueRepo->CDAEMPREOPUS():
                //Officers for Employer Temporary De-registrations Re Open Alert
                if (isset($input['temp_deregistration_reopen_officers'])) {
                    $code->users()->sync($input['temp_deregistration_reopen_officers']);
                } else {
                    $code->users()->sync([]);
                }
                break;
            default:
                break;
        }
        //$return = $this->notification_reports->postDefaults($input);
        return response()->json(['success' => true, 'message' => 'Success, All changes have been updated!']);
    }

}
