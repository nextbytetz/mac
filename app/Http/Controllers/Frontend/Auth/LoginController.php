<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\UserRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Adldap\Laravel\Facades\Adldap;
use Illuminate\Http\Request;
//use App\Events\Login;
use App\Jobs\SendWelcomeMail;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('frontend.auth.login');
    }

    public function username()
    {
        return 'username';
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $user = new UserRepository();
        $userArray = explode(".", $username);
        $count = 0;
        $mac_user = "";
        $firstname = "";
        $lastname = "";
        foreach ($userArray as $value) {
            if ($count == 0) {
                $firstname = $value;
                $value = substr($value, 0, 1);
            } elseif ($count == 1) {
                $lastname = $value;
            }
            $mac_user .= $value;
            $count++;
        }
        try {
            $user_format = env('ADLDAP_USER_LOGIN', 'cn=%s,' . env('ADLDAP_BASEDN', ''));
            $userdn = sprintf($user_format, $username);
            //$userdn = $username;
            $check = Adldap::connect("default")->auth()->attempt($userdn, $password, true);
            //$check = true;
            if ($check) {
                /*$macUser = $user->query()->whereRaw("username ~* = ?", [$mac_user])->first();
                if (count($macUser)) {
                    $mac_user = $macUser->username;
                }*/
                $mac_user = strtolower($mac_user);
                $user->query()->updateOrCreate(
                    ['username' => $mac_user],
                    [
                        'samaccountname' => $username,
                        'email' => $username . env('EMAIL_DOMAIN'),
                        'password' => bcrypt($password),
                        'firstname' => ucfirst($firstname),
                        'lastname' => ucfirst($lastname),
                    ]
                );
                $attempt = Auth::attempt(['samaccountname' => $username, 'password' => $password], $request->has('remember'));
            } else {
                $attempt = false;
            }
        } catch (\Adldap\Auth\BindException $e) {
            // There was an issue binding / connecting to the server.
            //echo "There was a problem connecting to LDAP";
            $attempt = Auth::attempt(['samaccountname' => $username, 'password' => $password], $request->has('remember'));
        }
        return $attempt;
    }

    /**
     * Determine if the user has too many failed login attempts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), 3, 1
        );
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    public function testMail(){

      $emailJob = (new SendWelcomeMail('mihayo.mathayo'))->delay(Carbon::now()->addSeconds(3));
       dispatch($emailJob);
       echo 'Mail Sent';
    }

}
