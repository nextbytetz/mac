<?php

namespace App\Http\Requests\Backend\Organization;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class FiscalYearRequest.
 */
class FiscalYearRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {



        /**
         * Request Action Type; 1 => Create, 2 => Update
         */
        $input = request()->all();
        $create_validation = [];
        $update_validation = [];
        $basic = [];

        /*  Validation */
        if ($input['request_action_type'] == 1)
        {
            // create
            $create_validation = [
                'start_year' => [ 'required',
                    Rule::unique('fiscal_years')
                ],
            ];
        }else {
            // Update
            $fiscal_year_id = $input['fiscal_year_id'];
            $update_validation = [
                'start_year' => ['required',
                    Rule::unique('fiscal_years')->where(function ($query) use
                    ($fiscal_year_id) {
                        $query->where('id','<>', $fiscal_year_id);
                    })
                ],
            ];
        }

        /* Target type total */
//        foreach ($input as $key => $value) {
//            if (strpos($key, 'target_total') !== false) {
//                $basic[$key] = 'required';
//            }
//        }



        return array_merge( $basic, $create_validation, $update_validation);
    }

}