<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use Carbon\Carbon;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class BeneficiaryUpdateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $input = request()->all();
        $optional = [];

        $folio = [];

        /*Validate DOB*/
        $this->validateDob($input);

        if($input['check_if_enrolled'] == true)
        {
            /* if beneficiary already enrolled into payroll*/
            $folio = [
                'folionumber' => 'required|numeric'
            ];
        }else{
            $folio = [
                'folionumber' => 'nullable|numeric'
            ];
        }
        $basic = [
            'firstname' => 'required|alpha_spaces',
//            'middlename' => 'required',
            'lastname' => 'required|alpha_spaces',
            'dob' => 'required',
            'phone' => 'required|phone:TZ',
            'email'=> 'nullable|email',
        ];

        if(array_key_exists('remark', $input)){
            $optional = [
                'remark' => 'required',
            ];
        }

        return array_merge($basic, $optional, $folio);


    }

    public function sanitize()
    {

        /*verification date*/
        $input = $this->all();
        $input['phone'] = isset($input['phone'])   ? phone_255($input['phone']) : null;
        /*verification date*/
        $this->replace($input);
        return $this->all();

    }


    /*Validate DOB*/
    public function validateDob(array $input)
    {
        $member_type_id = $input['member_type_id'];
        if($member_type_id == 4){
            /*Dependents*/
            $employee_id = $input['employee_id'];
            $resource_id = $input['resource_id'];
            $resource = (new PayrollRepository())->getResource($member_type_id, $resource_id);
            $dependent_type = $resource->getDependentEmployee($employee_id)->dependent_type_id;
            switch ($dependent_type) {
                case 2: //wife
                    $age_limit = get_wife_age_limit();
                    $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                    $dob = [ 'dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                    if (comparable_date_format($input['dob']) > comparable_date_format($age_limit_date)){
                        throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                    }
                    break;
                case 1: //husband
                    $age_limit = get_husband_age_limit();
                    $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                    $dob = [ 'dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                    if (comparable_date_format($input['dob']) > comparable_date_format($age_limit_date)){
                        throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                    }
                    break;
                case 5: //mother
                    $age_limit = get_mother_age_limit();
                    $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                    $dob = ['dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                    if (comparable_date_format($input['dob']) > comparable_date_format($age_limit_date)){
                        throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                    }
                    break;
                case 4: //father
                    $age_limit = get_father_age_limit();
                    $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                    $dob = [ 'dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                    if (comparable_date_format($input['dob']) > comparable_date_format($age_limit_date)){
                        throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                    }
                    break;

                case 7: //Constant care assistant
                    $age_limit = get_constant_care_age_limit();
                    $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                    $dob = [ 'dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                    if (comparable_date_format($input['dob']) > comparable_date_format($age_limit_date)){
                        throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                    }
                    break;
                default:
                    $dob = [ 'dob' => 'required|date|date_format:Y-n-j|before_or_equal:this_date'];
            }

        }else{
            /*Pensioner*/
            $age_limit = get_employee_age_limit();
            $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
            $dob = [ 'dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
            if (comparable_date_format($input['dob']) > comparable_date_format($age_limit_date)){
                throw new GeneralException(trans('Age for this pensioner not correct.'));
            }
        }
    }
}
