<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class BankUpdateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = request()->all();
        $folio = [];
        if($input['check_if_enrolled'] == true)
        {
            /* if beneficiary already enrolled into payroll*/
            $folio = [
                'folionumber' => 'required|numeric'
            ];
        }else{
            $folio = [
                'folionumber' => 'nullable|numeric'
            ];
        }


        $basic = [
            'new_bank' => 'required',
            'new_branch' => 'nullable',
            'new_accountno' => 'required',

        ];

        return array_merge($folio, $basic);


    }

}
