<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class MergeManualPensionerRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = request()->all();
        $optional = [];
        $array = [];
        $basic = [
            'firstname' => 'required|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'dob' => 'required|date|before:this_date',
            'mp' => 'required',
            'phone' => 'nullable|phone:TZ',
//            'bank' => 'required',
//            'accountno' => 'required'

        ];

        if(array_key_exists('hasarrears', $input)){
            $array = ['hasarrears' => 'required'];
            $optional = array_merge($array, $optional);
            if($input['hasarrears'] == 1){
                $array = ['pending_pay_months' => 'required'];
            }
            $optional = array_merge($array, $optional);
        }


        return array_merge($basic, $optional);


    }

}
