<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Http\Requests\Request;

class ManualSuspensionRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $basic = [
            'run_date' => 'required',
            'remark' => 'required'

        ];
        return $basic;
    }

    public function sanitize()
    {
        $input = $this->all();
          $input['run_date'] = isset($input['run_date']) ? standard_date_format( $input['run_date']) : null;

        $this->replace($input);
        return $this->all();
    }

}