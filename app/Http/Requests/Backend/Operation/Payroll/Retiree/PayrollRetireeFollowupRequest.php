<?php

namespace App\Http\Requests\Backend\Operation\Payroll\Retiree;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Repositories\Backend\Operation\Payroll\Retiree\PayrollRetireeFollowupRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class PayrollRetireeFollowupRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function Rules()
    {
        $input = $this->all();
        $basic = [];
        $optional = [];
        $array = [];
        $action_type = $input['action_type'];
        $follow_up_type_cv_ref = $input['follow_up_type_cv_ref'];

        switch ($action_type){
            case 1:
                /*When Adding*/
                $basic = [
                    'date_of_follow_up' => 'required|date|date_format:Y-n-j|before_or_equal:today',
                    'retiree_feedback_cv_ref' => 'required',
                    'follow_up_type_cv_ref' => 'required',
                ];
                break;
            case 2:
                /*When Editing*/
                $basic = [
                    'date_of_follow_up' => 'required|date|date_format:Y-n-j|before_or_equal:today',
                    'retiree_feedback_cv_ref' => 'required',
                    'follow_up_type_cv_ref' => 'required',
                ];
                break;
        }
        //        if email
        if ($follow_up_type_cv_ref == 'FUEMAIL'){
            $contact=[
                'contact' => 'required|email'
            ];
            $optional = array_merge($optional, $contact);
        }
//        if phone
        if ($follow_up_type_cv_ref == 'FUPHONC'){
            $contact=[
                'contact' => 'required|phone:TZ'
            ];
            $optional = array_merge($optional, $contact);
        }

        $optional = array_merge($optional, optionalRequiredRequest('contact_person', $input));

        if(array_key_exists('date_of_next_follow_up', $input)){
            $array = [
                'date_of_next_follow_up' => 'required|date|date_format:Y-n-j|after_or_equal:today',
            ];
            $optional = array_merge($optional, $array);
        }
        /*other validations*/
        $this->otherValidations($input);

        return array_merge( $basic, $optional);
    }

    public function sanitize()
    {
        $input = $this->all();
        $cv_repo = new CodeValueRepository();
        $input['retiree_feedback_cv_id'] = isset($input['retiree_feedback_cv_ref']) ? $cv_repo->findIdByReference($input['retiree_feedback_cv_ref']) : null;
        $input['follow_up_type_cv_id'] = isset($input['follow_up_type_cv_ref']) ? $cv_repo->findIdByReference($input['follow_up_type_cv_ref']) : null;
        $input['date_of_follow_up'] = isset($input['date_of_follow_up']) ? standard_date_format($input['date_of_follow_up']) : null;
        if(array_key_exists('date_of_next_follow_up', $input)){
            $input['date_of_next_follow_up'] = isset($input['date_of_next_follow_up']) ? standard_date_format($input['date_of_next_follow_up']) : null;
        }
        $this->replace($input);
        return $this->all();
    }


    /**
     * @param array $input
     * @throws GeneralException
     * Other validations
     */
    public function otherValidations(array $input)
    {
        $action_type = $input['action_type'];
        $this->checkIfExists($input, $action_type);
        $this->checkIfDateIsGreaterThanPrevious($input,$action_type);
    }


    /*check if followup exists*/
    public function checkIfExists(array $input, $action_type)
    {
        $pay_retiree_follow_repo = new PayrollRetireeFollowupRepository();
        $base_query = $pay_retiree_follow_repo->query()->where('member_type_id', $input['member_type_id'])->where('resource_id', $input['resource_id'])->where('employee_id', $input['employee_id'])->where('follow_up_type_cv_id', $input['follow_up_type_cv_id'])->where('date_of_follow_up', $input['date_of_follow_up']);
        $check = 0;
        if($action_type == 1){
            /*new*/
            $check = $base_query->count();
        }else{
            /*edit*/
            $check = $base_query->where('id', '<>', $input['payroll_retiree_followup_id'])->count();
        }

        if($check > 0){
            throw new GeneralException('This entry already exists! Please check!');
        }
    }

    /*check if date is valid*/
    public function checkIfDateIsGreaterThanPrevious(array $input, $action_type)
    {
        $pay_retiree_follow_repo = new PayrollRetireeFollowupRepository();
        $base_query = $pay_retiree_follow_repo->query()->where('member_type_id', $input['member_type_id'])->where('resource_id', $input['resource_id'])->where('employee_id', $input['employee_id'])->where('date_of_follow_up','>', $input['date_of_follow_up']);
        if($action_type == 1){

        }else{
            $base_query  = $base_query->where('id', '<>', $input['payroll_retiree_followup_id']);
        }
        $check = $base_query->count();
        if($check > 0){
            throw new GeneralException('Date of followup can not be less than previous followup date! Please check!');
        }
    }
}
