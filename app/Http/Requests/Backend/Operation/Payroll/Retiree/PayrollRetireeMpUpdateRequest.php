<?php

namespace App\Http\Requests\Backend\Operation\Payroll\Retiree;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class PayrollRetireeMpUpdateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function Rules()
    {
        $input = $this->all();
        $basic = [];
        $optional = [];
        $array = [];
        $action_type = $input['action_type'];
        switch ($action_type){
            case 1:
                /*When Adding*/
                $basic = [
                    'social_security_mp' => 'required',
                    'retirement_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date',
                    'remark' => 'required',

                ];
                break;
            case 2:
                /*When Editing*/
                $basic = [
                    'social_security_mp' => 'required',
                    'retirement_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date',
                    'remark' => 'required',

                ];
                break;
        }

        $this->validationOnDocumentRequired($input);

        return array_merge( $basic, $optional);
    }

    public function sanitize()
    {
        $input = $this->all();
        $input['social_security_mp'] = isset($input['social_security_mp']) ? str_replace(",", "", $input['social_security_mp']) : null;
        $input['retirement_date'] = isset($input['retirement_date']) ? standard_date_format($input['retirement_date']) : null;
        /*Doc evidence*/
        $doc_evidence = [];
        foreach ($input as $key => $value) {
            if (strpos($key, 'doc_selected') !== false) {
                $doc_pivot_id = substr($key, 12);
                array_push($doc_evidence, $doc_pivot_id);
            }
        };
        $input['doc_evidence'] = count($doc_evidence) ? json_encode($doc_evidence) : null;

        $this->replace($input);
        return $this->all();
    }

    /**
     * @param array $input
     * @throws GeneralException
     * Validation on docs required
     */
    public function validationOnDocumentRequired(array $input)
    {
        /*Input per status type*/
        $docs_required = [];
        $docs_required = [63,139];

        if(count($docs_required)){
            /*Get all docs selected*/
            $docs_selected = [];
            foreach ($input as $key => $value) {
                if (strpos($key, 'doc_selected') !== false) {
                    $doc_pivot_id = substr($key, 12);
                    $doc_pivot = DB::table('document_payroll_beneficiary')->where('id', $doc_pivot_id)->first();
                    $document_type_id = $doc_pivot->document_id;
                    array_push($docs_selected, $document_type_id);
                }
            };
            /*check*/
            $check_exists = 1;
            foreach($docs_required as $doc)
            {
                $check = in_array($doc, $docs_selected);
                if($check == false){
                    $check_exists = 0;
                }
            }
            if($check_exists == 0){
                throw new GeneralException('Documents required for evidence not selected! Please check!');
            }

        }

    }

}
