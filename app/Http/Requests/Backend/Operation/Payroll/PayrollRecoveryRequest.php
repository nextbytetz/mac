<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class PayrollRecoveryRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $input = $this->all();

        $this->checkIfCycleAmountIsValid($input);

        $basic = [
            'total_amount' => 'required|min:1',
            'cycles' => 'required|numeric|min:1',
            'remark' => 'required|string',

        ];

        return $basic;


    }

    /*Check if cycle amunt is valid*/
    public function checkIfCycleAmountIsValid($input)
    {
        $recovery_ref = (new CodeValueRepository())->find($input['recovery_type_id'])->reference;
        if($recovery_ref == 'PRTOVEP'){
//            $total_balance = $input['mp'] + $input['arrears_pending'];
            $total_balance = $input['mp'];
            if($input['amount'] > $total_balance){
                throw new GeneralException('Amount per cycle can not be greater than monthly pension and arrears pending. Please check!');
            }
        }

    }


    public function sanitize()
    {
        $input = $this->all();
        $input['amount'] = isset($input['amount']) ? remove_thousands_separator($input['amount']) : null;
        $input['mp'] = isset($input['mp']) ? remove_thousands_separator($input['mp']) : null;
        $this->replace($input);
        return $this->all();
    }

}
