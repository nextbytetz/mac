<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class VerificationRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $input = $this->all();
        $optional = [];
        $array = [];
        if(array_key_exists('iseducation',$input)){
            $array = ['iseducation' => 'required'];
            $optional = array_merge($array, $optional);
        }
        if(array_key_exists('isdisabled',$input)){
            $array = ['isdisabled' => 'required'];
            $optional = array_merge($array, $optional);
        }
        $basic = [
            'folionumber' => 'required|numeric',
            'region_id' => 'required',
            'district_id' => 'required',
            'phone' => 'required|phone:TZ',
            'next_kin_phone' => 'nullable|phone:TZ',
            'verification_date' => 'required|date:Y-n-j|before_or_equal:today_date'
        ];

        return array_merge($basic, $optional);


    }

    public function sanitize()
    {

        /*verification date*/
        $input = $this->all();
        $input['verification_date'] = isset($input['verification_date'])   ? standard_date_format($input['verification_date']) : null;
        $input['phone'] = isset($input['phone'])   ? phone_255($input['phone']) : null;
        $input['next_kin_phone'] = isset($input['next_kin_phone'])   ? phone_255($input['next_kin_phone']) : null;
        /*verification date*/
        $this->replace($input);
        return $this->all();

    }

}
