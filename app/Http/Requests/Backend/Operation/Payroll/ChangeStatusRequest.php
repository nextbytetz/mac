<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class ChangeStatusRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = request()->all();

        $option = [];
        $array = [];
        if(array_key_exists('exit_date', $input))
        {
            $array = [
                'exit_date' => 'required',
            ];
            $option =array_merge($array, $option);
        }

        if(array_key_exists('firstpay_flag', $input))
        {
            $array =[
                'firstpay_flag' => 'required',
            ];
            $option =array_merge($array, $option);
        }

        /*Optional*/
        /*Hasarrears*/
        if(array_key_exists('hasarrears', $input))
        {
            $array =[
                'hasarrears' => 'required',
            ];
            $option =array_merge($array, $option);
        }
        /*pending_pay_months*/
        if(array_key_exists('pending_pay_months', $input))
        {
            $array =[
                'pending_pay_months' => 'required|min:0',
            ];
            $option =array_merge($array, $option);
        }


        /*Input per status type*/
        switch($input['status_change_type_ref']){
            case 'PSCCHDREI'://child continuation
                $validation = $this->childContinuationValidation($input);
                $option = array_merge($validation, $option);
                break;
        }
        $basic = [
            'remark' => 'required',
        ];

        /*Validate on documents required*/
        $this->validationOnDocumentRequired($input);

        return array_merge($basic, $option);


    }


    public function sanitize()
    {
        $input = $this->all();
        $input['child_continuation_reason_cv_id'] = isset($input['child_continuation_reason_cv_ref']) ? (new CodeValueRepository())->findIdByReference(($input['child_continuation_reason_cv_ref'])) : null;
        $input['education_level_cv_id'] = isset($input['education_level_cv_ref']) ? (new CodeValueRepository())->findIdByReference(($input['education_level_cv_ref'])) : null;
        $input['deadline_date'] = isset($input['deadline_date']) ? standard_date_format($input['deadline_date']) : null;

        /*Doc evidence*/
        $doc_evidence = [];
        foreach ($input as $key => $value) {
            if (strpos($key, 'doc_selected') !== false) {
                $doc_pivot_id = substr($key, 12);
                array_push($doc_evidence, $doc_pivot_id);
            }
        };
        $input['doc_evidence'] = count($doc_evidence) ? json_encode($doc_evidence) : null;
        $this->replace($input);
        return $this->all();
    }

    /*Child continuation on validation*/
    public function childContinuationValidation(array $input)
    {

        $basic = [
            'child_continuation_reason_cv_ref' => 'required'
        ];

        /*deadline date*/
        if(array_key_exists('deadline_date', $input))
        {
            $array =[
                'deadline_date' => 'required|date|after_or_equal:this_date',
            ];
            $basic =array_merge($array, $basic);
        }

        if($input['child_continuation_reason_cv_ref'] == 'PAYCHCOSCH'){
            $array = [
                'school_name' => 'required',
                'education_level_cv_ref' => 'required',
                'school_grade' => 'required',
            ];
            $basic =array_merge($array, $basic);
        }


        return $basic;
    }


    /**
     * @param array $input
     * @throws GeneralException
     * Validation on docs required
     */
    public function validationOnDocumentRequired(array $input)
    {
        /*Input per status type*/
        $docs_required = [];
        switch($input['status_change_type_ref']){
            case 'PSCCHDREI'://child continuation

                //depend on continuation reason
                switch($input['child_continuation_reason_cv_ref']){
                    case 'PAYCHCOSCH':
                        $docs_required = [63, 138]; //Todo need to add other document types as well - school confirmation
                        break;

                    case 'PAYCHCDIS':
                        $docs_required = [63, 16];
                        break;
                }

                break;
        }


        if(count($docs_required)){

            /*Get all docs selected*/
            $docs_selected = [];
            foreach ($input as $key => $value) {
                if (strpos($key, 'doc_selected') !== false) {
                    $doc_pivot_id = substr($key, 12);
                    $doc_pivot = DB::table('document_payroll_beneficiary')->where('id', $doc_pivot_id)->first();
                    $document_type_id = $doc_pivot->document_id;
                    array_push($docs_selected, $document_type_id);
                }
            };
            /*check*/
            $check_exists = 1;
            foreach($docs_required as $doc)
            {
                $check = in_array($doc, $docs_selected);
                if($check == false){
                    $check_exists = 0;
                }
            }


            if($check_exists == 0){
                throw new GeneralException('Documents required for evidence not selected! Please check!');
            }

        }

    }
}
