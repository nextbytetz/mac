<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class ChooseRecoveryRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $basic = [
            'recovery_type' => 'required',
        ];

        return $basic;


    }

}
