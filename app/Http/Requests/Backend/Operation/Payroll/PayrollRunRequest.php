<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Http\Requests\Request;

class PayrollRunRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $basic = [ 'run_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:claim_start_date'];
        return $basic;
    }

}