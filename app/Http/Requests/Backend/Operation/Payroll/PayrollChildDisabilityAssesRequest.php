<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class PayrollChildDisabilityAssesRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = request()->all();

        $option = [];
        $array = [];
        if(array_key_exists('deadline_date', $input))
        {
            $array = [
                'deadline_date' => 'required',
            ];
            $option =array_merge($array, $option);
        }


        $basic = [
            'period_provision_type' => 'required'
        ];

        return array_merge($basic, $option);


    }


    public function sanitize()
    {
        $input = $this->all();
        if (array_key_exists('deadline_date', $input))
        {
            $input['deadline_date'] = isset($input['deadline_date']) ? standard_date_format($input['deadline_date']) : null;
        }
        $this->replace($input);
        return $this->all();
    }





}
