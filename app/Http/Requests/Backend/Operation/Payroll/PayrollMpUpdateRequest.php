<?php

namespace App\Http\Requests\Backend\Operation\Payroll;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class PayrollMpUpdateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
//
        $basic = [
            'new_mp' => 'required',
            'remark' => 'required'
              ];

        return $basic;


    }



    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
        $input['new_mp'] = isset($input['new_mp'])   ? remove_thousands_separator($input['new_mp']) : null;
        $input['old_mp'] = isset($input['old_mp'])   ? remove_thousands_separator($input['old_mp']) : null;
        /*Dob*/
        $this->replace($input);
        return $this->all();

    }

}
