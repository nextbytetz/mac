<?php

namespace App\Http\Requests\Backend\Operation\Inspection;

use App\Http\Requests\Request;

class UpdateEmployerInspectionTaskRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //'status' => 'required',
           //'visit_date' => 'required|date|after_or_equal:inspection_start|before_or_equal:inspection_end',
           'visit_date' => 'required',
            /*             'findings' => 'nullable',
                        'exit_date' => 'nullable',
                        'doc' => 'nullable',*/
            //'last_inspection_date' => 'nullable|after_or_equal:review_end_date|before_or_equal:today_date|after_or_equal:visit_date',
            'last_inspection_date' => 'nullable|after_or_equal:visit_date',
            //'review_start_date' => 'required|date|after_or_equal:wcf_start_date|after_or_equal:last_review_end_date',
            'review_start_date' => 'required|date|after_or_equal:wcf_start_date',
            'review_end_date' => 'required|date|after_or_equal:review_start_date',
            //'contribution_file' => 'nullable|file|mimes:xls,xlsx',
            'resolutions' => 'required',
            'findings' => 'required',
        ];
    }

}