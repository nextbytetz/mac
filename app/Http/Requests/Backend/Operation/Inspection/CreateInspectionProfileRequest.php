<?php

namespace App\Http\Requests\Backend\Operation\Inspection;

use App\Http\Requests\Request;

class CreateInspectionProfileRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'region' => 'required_without:business',
            'business' => 'required_without:region',
        ];
    }

}