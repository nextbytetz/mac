<?php

namespace App\Http\Requests\Backend\Operation\Inspection;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class UploadInspectionAssessmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_file78' => [
                'required',
                'file',
                'mimetypes:application/excel,application/x-excel,application/x-msexcel,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.spreadsheetml.template,application/vnd.ms-excel.sheet.macroEnabled.12,application/vnd.ms-excel.template.macroEnabled.12,application/vnd.ms-excel.addin.macroEnabled.12,application/vnd.ms-excel.sheet.binary.macroEnabled.12,application/octet-stream',
            ],
            'payroll_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'document_file78.mimetypes' => 'Only Excel document types are allowed for upload',
            'document_file78.required' => 'Upload file',
            'document_file78.file' => 'Upload a valid file',
            'payroll_id.required' => 'Please select payroll associated with this assessment',
        ];
    }

}
