<?php

namespace App\Http\Requests\Backend\Operation\Inspection;

use App\Http\Requests\Request;
use Carbon\Carbon;

class UpdateInspectionRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $opt = [];
        $input = $this->all();
        $today = $input["today_date"];
        $previous_start = $input["previous_start_date"];
        $today_date = Carbon::parse($today);
        $previous_start_date = Carbon::parse($previous_start);
        if ($today_date->gt($previous_start_date)) {
            $opt = [
                'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:previous_start_date',
                'end_date' => 'required|date|date_format:Y-n-j|after_or_equal:start_date',
            ];
        } else {
            $opt = [
                'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:today_date',
                'end_date' => 'required|date|date_format:Y-n-j|after_or_equal:start_date',
            ];
        }
        $basic =  [
            'inspection_type' => 'required',
            'comments' => 'required',
        ];
        switch ($input['inspection_type']) {
            case "ITROUTINE":
            case "ITSPECIAL":
                $basic['trigger_category'] = "required";
                break;
        }
        $basic = array_merge($basic, $opt);
        return $basic;
    }

}