<?php

namespace App\Http\Requests\Backend\Operation\Inspection;

use App\Http\Requests\Request;

class CreateInspectionRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = $this->all();
        $return = [
            'inspection_type' => 'required',
            //'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:today_date',
            //'end_date' => 'nullable|date|date_format:Y-n-j|after_or_equal:start_date',
            'comments' => 'required',
        ];
        switch ($input['inspection_type']) {
            case "ITROUTINE":
            case "ITSPECIAL":
                $return['trigger_category'] = "required";
                break;
        }
        return $return;
    }

}