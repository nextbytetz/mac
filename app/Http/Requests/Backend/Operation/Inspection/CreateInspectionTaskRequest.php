<?php

namespace App\Http\Requests\Backend\Operation\Inspection;

use App\Http\Requests\Request;

class CreateInspectionTaskRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = $this->all();
        $return = [
            'inspection_type' => 'required',
            'name' => 'required',
            //'staffs' => 'required',
            'target_employer' => 'required',
            'deliverable' => 'nullable',
        ];
        $target_employer = $input["target_employer"];
        switch ($target_employer) {
            case 2:
                $return['other_employers'] = 'nullable';
                $return['inspection_profile_id'] = 'nullable';
                $return['employer_count'] = 'nullable';
                $attach_rule = (isset($input['inspection_id'])) ? 'nullable' : 'required';
                $return['document_file'] = [
                                                $attach_rule,
                                                'file',
                                                'mimetypes:application/excel,application/x-excel,application/x-msexcel,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.spreadsheetml.template,application/vnd.ms-excel.sheet.macroEnabled.12,application/vnd.ms-excel.template.macroEnabled.12,application/vnd.ms-excel.addin.macroEnabled.12,application/vnd.ms-excel.sheet.binary.macroEnabled.12,application/octet-stream',
                                            ];
                break;
            case 1:
                //Selected Inspection Profile
                $return['inspection_profile_id'] = 'required';
                $return['employer_count'] = 'required';
                $return['other_employers'] = 'nullable';
                $return['document_file'] = 'nullable';
                break;
            case 0:
                //Selected Individual Employers
                $return['employer'] = 'required';
                $return['other_employers'] = 'nullable';
                $return['inspection_profile_id'] = 'nullable';
                $return['employer_count'] = 'nullable';
                $return['document_file'] = 'nullable';
                break;
            default:
                $return['other_employers'] = 'required';
                $return['inspection_profile_id'] = 'nullable';
                $return['employer_count'] = 'nullable';
                $return['document_file'] = 'nullable';
        }
        return $return;
    }

    public function messages()
    {
        return [
            'document_file.mimetypes' => 'Only Excel document types are allowed for upload',
            'document_file.required' => 'Upload Excel file',
            'document_file.file' => 'Upload a valid file'
        ];
    }

}