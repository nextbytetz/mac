<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

class EmployerOpenRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'open_reason' => 'required',
            'open_date' => 'required|date|date_format:Y-n-j|after_or_equal:close_date',
        ];
    }


    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
        $input['open_date'] = isset($input['open_date'])   ? standard_date_format($input['open_date']) : null;
        /*Dob*/
        $this->replace($input);
        return $this->all();

    }
}
