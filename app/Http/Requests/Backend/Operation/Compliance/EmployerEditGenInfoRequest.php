<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;

class EmployerEditGenInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $input = $this->all();

        /*check if all parent is not the same as employer*/
        /*Check if is different employers*/
        if(isset($input['employer_id']) && isset( $input['parent_id'])){

            if($input['employer_id'] == $input['parent_id']){
                throw new GeneralException( 'Parent employer can not be the same as employer! Please check!');
            }


        }


        return [
//            'parent_id' => 'required',
//            'annual_earning' => 'required',
            'business_sector_cv_id' => 'required',
            'employer_category_cv_id' => 'required',
        ];
    }


    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
//        $input['annual_earning'] = isset($input['annual_earning'])   ? remove_thousands_separator($input['annual_earning']) : null;
        /*Dob*/
        $this->replace($input);
        return $this->all();

    }
}
