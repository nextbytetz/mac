<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

class EmployerClosureExtensionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function Rules()
    {
        $input = $this->all();
        $basic = [];
        $optional = [];
        $array = [];
        $action_type = $input['action_type'];
        switch ($action_type){
            case 1:
                /*When Adding*/
                $basic = [
                    'application_date' => 'required|date|date_format:Y-n-j|before_or_equal:today',
                    'new_reopen_date' => 'nullable|date|date_format:Y-n-j',

                ];
                break;
            case 2:
                /*When Editing*/
                $basic = [
                    'application_date' => 'required|date|date_format:Y-n-j|before_or_equal:today',
                    'new_reopen_date' => 'nullable|date|date_format:Y-n-j',

                ];
                break;
        }
        return array_merge( $basic, $optional);
    }

    public function sanitize()
    {
        $input = $this->all();
        $input['application_date'] = isset($input['application_date']) ? standard_date_format($input['application_date']) : null;
        $input['new_reopen_date'] = isset($input['new_reopen_date']) ? standard_date_format($input['new_reopen_date']) : null;

        $this->replace($input);
        return $this->all();
    }


}
