<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Repositories\Backend\Finance\Receivable\InterestAdjustmentRepository;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class InterestAdjustmentRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = $this->all();
        $this->otherValidation($input);
        $basic = [
            'receipt_id' => 'required',
            'remark' => 'required',
            'new_payment_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date|after_or_equal:wcf_start_date',
            'reason_type_cv_id' => 'required',
        ];

        return $basic;


    }


    public function sanitize()
    {
        $input = $this->all();
        $input['new_payment_date'] = isset($input['new_payment_date']) ? standard_date_format($input['new_payment_date']) : null;
        $this->replace($input);
        return $this->all();
    }



    public function otherValidation(array $input){
        $interest_adj_repo = new InterestAdjustmentRepository();
        /*check if has interest*/
        $interest_adj_repo->checkIfReceiptHasInterest($input['receipt_id']);

        /*check if new payment date is ok*/
        if(isset($input['new_payment_date'])){
            if(comparable_date_format($input['new_payment_date']) == comparable_date_format($input['payment_date'])){
                throw  new GeneralException('New payment date should be the same to current payment date! Please check');
            }
        }


    }
}
