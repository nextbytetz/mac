<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class EmployerRegistrationRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $return = [];
        $input = request()->all();

        foreach ($input as $key => $value) {

            if (strpos($key, 'male') !== false) {

                $return[$key] = 'nullable|numeric';
            }

            if (strpos($key, 'female') !== false) {
                $return[$key] = 'nullable|numeric';
            }

            if (strpos($key, 'employer_document') !== false) {
                $employer_document_id = substr($key, 17);
                if (array_key_exists('uploaded_file'.$employer_document_id, $input) ) {// when Updating
                    if (($input['employer_document' . $employer_document_id] == 1) && (!isset($input['uploaded_file' . $employer_document_id])) && (!request()->hasFile('document_file' . $employer_document_id))) {

                        $return['document_file' . $employer_document_id] = 'required';


                    }
                } else { // when inserting document
                    if (($input['employer_document' . $employer_document_id] == 1) && (!request()->hasFile('document_file' . $employer_document_id))) {

                        $return['document_file' . $employer_document_id] = 'required';
                    }
                }

                /*  check file type  when updating and inserting*/

                if (($input['employer_document' . $employer_document_id] == 1) && request()->hasFile('document_file' . $employer_document_id)){
                if ($employer_document_id == 37) {
                                /* check excel type for list of employees*/
                    $return['document_file' . $employer_document_id] = 'mimetypes:application/excel,application/x-excel,application/x-msexcel,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.spreadsheetml.template,application/vnd.ms-excel.sheet.macroEnabled.12,application/vnd.ms-excel.template.macroEnabled.12,application/vnd.ms-excel.addin.macroEnabled.12,application/vnd.ms-excel.sheet.binary.macroEnabled.12,application/octet-stream';
                } else {

                    /* check pdf, doc, image type for other documents */
                    $return['document_file' . $employer_document_id] = 'mimetypes:application/pdf,image/png,image/tiff,image/x-tiff,image/bmp,image/x-windows-bmp,image/gif,image/x-icon,image/jpeg,image/pjpeg,image/x-portable-bitmap,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.wordprocessingml.template,application/vnd.ms-word.document.macroEnabled.12,application/vnd.ms-word.template.macroEnabled.12';
                }
            }
            }
        }



//        if (array_key_exists('parent_employer_id', $input) ) {
//            $return['parent_employer_id'] = 'required';
//        }


        $basic = [
            'name' => 'required',
            'doc' => 'required|date|date_format:Y-n-j|before_or_equal:this_date',
            'tin' => 'required|digits:9',
            'employer_category_cv_id' => 'required',
            'annual_earning' => 'required',
            'email' => 'required|email',
            'phone'       => 'required|phone:TZ',
            'telephone'       => 'nullable|phone:TZ',
            'country_id' => 'required',
            'region_id' => 'required',
            'district_id' => 'required',
            'location_type_id' => 'required',
            'business_sector_cv_id' => 'required',
            'vote' => [
                Rule::unique('employers')->whereNotNull('vote')
            ]

        ];

        return array_merge($basic,   $return);

    }

}
