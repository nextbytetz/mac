<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;
use Carbon\Carbon;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class UpdateUnregisteredFollowupInspectionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $opt = [];
        $today = request()->input("today_date");
        $previous_start = request()->input("previous_start_date");
        $today_date = Carbon::parse($today);
        $previous_start_date = Carbon::parse($previous_start);
        if ($today_date->gt($previous_start_date)) {
            $opt = [
                'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:previous_start_date',
                'end_date' => 'required|date|date_format:Y-n-j|after_or_equal:start_date',
            ];
        } else {
            $opt = [
                'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:today_date',
                'end_date' => 'required|date|date_format:Y-n-j|after_or_equal:start_date',
            ];
        }
        $basic =  [
            'comments' => 'required',
            'staffs' => 'required',
        ];
        $basic = array_merge($basic, $opt);
        return $basic;
    }

}
