<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package
 */
class CreateEmploymentHistoryRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {



// Check if employer is filled
        if ((empty(request()->input('employer_id'))) && (empty(request()->input('other_employer')))) {
            $return_employer_rule = 'required';
        } else {
            $return_employer_rule = '';
        }


        $basic = [
            'job_title_id' => 'required',
            'min_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date',
            'max_date' => 'required|date|date_format:Y-n-j|after_or_equal:min_date',
            'employer_id' => $return_employer_rule,

        ];

        return $basic;


    }

}
