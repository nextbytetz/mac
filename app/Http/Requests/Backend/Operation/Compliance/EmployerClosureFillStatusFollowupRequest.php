<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

class EmployerClosureFillStatusFollowupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'followup_date' => 'required|date|date_format:Y-n-j|before_or_equal:today_date',
            'followup_outcome' => 'required',

            ];

    }

    public function sanitize()
    {
        $input = $this->all();
        $input['followup_date'] = isset($input['followup_date']) ? standard_date_format($input['followup_date']) : null;

        $this->replace($input);
        return $this->all();
    }
}
