<?php

namespace App\Http\Requests\Backend\Operation\Compliance\Online;

use App\Http\Requests\Request;
use Carbon\Carbon;
class MergeEmployerPayrollsRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = $this->all();
        $return = [
            'merge_type' => 'required',
            'old_payroll' => 'required',
            'merge_reason' => 'required',
            'document_file50' => 'required|mimes:pdf,jpg,png',
            'letter_date' => 'required'       
        ];

        if ($input['merge_type'] == 1) {
            $return['new_payroll'] = 'required';
        }else{
            $return['new_payroll_user'] = 'required';
            $return['doc'] = 'required';
        }

        return $return;
    }

    public function messages()
    {
        $input = $this->all();
        $return = [
            'old_payroll.*' => 'Select atleast one user (payroll) for merging',
            'document_file50.*' => 'Request letter should be an image or pdf file',
            'letter_date.*' => 'Request letter / Email Date is required',
            'merge_type' => 'merge type is required',
        ];

        if ($input['merge_type'] == 1) {
            $return['new_payroll.*'] = 'Select user';
        }else{
            $return['new_payroll_user.*'] = 'select user to manage new payrol';
            $return['doc.*'] = 'enter date of commencement';
        }

        return $return;
    }

}