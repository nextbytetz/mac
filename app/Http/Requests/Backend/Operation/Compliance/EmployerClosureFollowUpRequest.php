<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;
use App\Models\Sysdef\CodeValue;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class EmployerClosureFollowUpRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = $this->all();
        $contact = [];
        $optional = [];
//        if email
        $cv = CodeValue::query()->find(request()->input('follow_up_type_cv_id'));

        if(isset($cv)){
            //        if email
            if ($cv->reference == 'FUEMAIL'){
                $contact=[
                    'contact' => 'required|email'
                ];
            }
//        if phone
            if ($cv->reference == 'FUPHONC'){
                $contact=[
                    'contact' => 'required|phone:TZ'
                ];
            }
            /*visit*/
            if($input['action_type'] == 1) {
                if ($cv->reference == 'FUVSIT' || $cv->reference == 'FULETT'){
                    $contact = [
                        'document_file' => [
                            'required',
                            'file',
                            'mimetypes:application/pdf',
                        ],
                    ];
                }
            }else{
                /*when modifying*/
                if ($cv->reference == 'FUVSIT' || $cv->reference == 'FULETT'){
                    $contact = [
                        'document_file' => [
                            'nullable',
                            'file',
                            'mimetypes:application/pdf',
                        ],
                    ];
                }
            }
        }

        if(array_key_exists('followup_outcome', $input)){
            $array = [
                'followup_outcome' => 'required',
            ];
            $optional = array_merge($array, $optional);
        }

        if(array_key_exists('feedback', $input)){
            $array = [
                'feedback' => 'required',
            ];
            $optional = array_merge($array, $optional);
        }

        $basic = [
            'description' => 'required',
            'follow_up_type_cv_id' => 'required',
            'contact_person' => 'required',
            'date_of_follow_up' => 'required|date|date_format:Y-n-j|after_or_equal:close_date',

        ];

        return array_merge($basic, $contact, $optional);

    }


    /**
     * @return array
     */
    public function sanitize()
    {
        /*dob*/
        $input = $this->all();
        $input['date_of_follow_up'] = isset($input['date_of_follow_up'])   ? standard_date_format($input['date_of_follow_up']) : null;

            /*Dob*/
        $this->replace($input);
        return $this->all();

    }

}
