<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class CreateUnregisteredFollowupInspectionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
                    'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:today_date',
            'end_date' => 'required|date|date_format:Y-n-j|after_or_equal:start_date',
            'comments' => 'required',
            'staffs' => 'required',
        ];
        return $return;
    }

}
