<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class CreateUnregisteredEmployerRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $basic = [
            'name' => 'required',
            'tin' => 'digits:9',
            'phone'       => 'nullable|phone:TZ',
            'telephone'       => 'nullable|phone:TZ',
            'doc' => 'nullable|date|date_format:Y-n-j',

        ];

        return array_merge($basic);

    }

}
