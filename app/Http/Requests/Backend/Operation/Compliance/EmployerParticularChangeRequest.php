<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;

class EmployerParticularChangeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $basic = [];
        $optional = [];
        $array = [];

        /*Name change*/

        if(isset($input['name_change_type_cv_id'])){
            $name_change_type_id = $input['name_change_type_cv_id'];
            /*Merger*/
            if($name_change_type_id == 436 )
            {
                $array = [
                    'merge_employer_id' => 'required'
                ];
                $optional = array_merge($array, $optional);

                /*Check if is different employers*/
                if($input['employer_id'] == $input['merge_employer_id']){
                    throw new GeneralException( 'Employer to be merge can not be the same employer! Please check!');
                }

            }

            /*New name*/
            if($name_change_type_id == 438 )
            {
                $array = [
                    'new_name' => 'required'
                ];
                $optional = array_merge($array, $optional);
            }

            /*Other name change*/
            if($name_change_type_id == 440 )
            {
                $array = [
                    'other_name_change' => 'required'
                ];
                $optional = array_merge($array, $optional);
            }
        }




        $basic = [
            'country_id' => 'required',
            'region_id' => 'required',
            'district_id' => 'required',
            'location_type_id' => 'required',
            'remark' => 'required',
            'date_received' => 'required|before_or_equal:this_date',
            'doc' => 'required|before_or_equal:this_date|date',
            'tin' => 'required|max:9',
            'phone' => 'required|phone:TZ',
            'email' => 'required|email',
        ];

        return array_merge($basic, $optional);
    }


    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
        $input['date_received'] = isset($input['date_received'])   ? standard_date_format($input['date_received']) : null;
        $input['doc'] = isset($input['doc'])   ? standard_date_format($input['doc']) : null;
        /*Dob*/
        $this->replace($input);
        return $this->all();

    }
}
