<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class BookingAdjustRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $basic = [
            'comments' => 'required',
            'new_booking_amount' => 'required|numeric'

        ];

        return $basic;


    }

}
