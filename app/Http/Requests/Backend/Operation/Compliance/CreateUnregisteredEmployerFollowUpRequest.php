<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class CreateUnregisteredEmployerFollowUpRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $contact = [];
//        if email
        if (request()->input('follow_up_type_cv_id') == 92){
            $contact=[
                'contact' => 'nullable|email'
            ];
        }
//        if phone
        if (request()->input('follow_up_type_cv_id') == 91){
            $contact=[
                'contact' => 'nullable|phone:TZ'
            ];
        }
        $basic = [
            'description' => 'required|alpha_spaces',
            'follow_up_type_cv_id' => 'required',
            'contact_person' => 'nullable',
            'date_of_follow_up' => 'required|date|date_format:Y-n-j|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:inspection_start_date',
            'feedback' => 'nullable',
        ];

        return array_merge($basic, $contact);


    }

}
