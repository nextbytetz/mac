<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class CreateEmployeeFromManualFileRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = request()->all();
        $dob = [];

        if($input['incident_type'] != 3){
            $dob = [
                'dob' => 'required|date|before_or_equal:this_date',
            ];
        }

        $basic = [
            'firstname' => 'required|alpha_spaces',
            'middlename' => 'nullable|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'dob' => 'nullable|date|before_or_equal:this_date',
            'gender_id' => 'required',
            'employee_category_cv_id' => 'required',
            'basicpay' => "nullable|regex:/^\d*(\.\d{1,2})?$/",
            'allowance' => "nullable|regex:/^\d*(\.\d{1,2})?$/",
            //'employer' => 'required',
            'email' => 'nullable|email',
            'phone' => 'nullable|phone:TZ',
            'marital_status_cv_id'=> 'required',
            'passport_no' => 'required_unless:country_id,1',
            'work_permit_no' => 'required_unless:country_id,1',
            'residence_permit_no' => 'required_unless:country_id,1',
        ];

        return array_merge($basic, $dob);
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'passport_no.required_unless' => 'Passport Number is required',
            'work_permit_no.required_unless'  => 'Work Permit Number is required',
            'residence_permit_no.required_unless'  => 'Residence Permit Number is required',
            'basicpay.regex' => 'The amount should be in form of 0.00',
            'allowance.regex' => 'The amount should be in form of 0.00',
        ];
    }




}
