<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class InterestAdjustRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $basic = [
            'comments' => 'required',
            'new_payment_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date|after_or_equal:wcf_start_date',
            'reason_type_cv_id' => 'required',
        ];

        return $basic;


    }

}
