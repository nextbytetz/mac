<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

class AttachEmployerParticularChangeDocRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();

        $optional = [];
        $array = [];
        $basic = [];

        $basic =  [
            'document_id' => 'required',
            'document_file' => [
                'required',
                'file',
                'mimetypes:application/pdf',
            ],
        ];
        return array_merge($basic, $optional);
    }

    public function messages()
    {
        return [
            'document_file.mimetypes' => 'Only PDF files allowed for upload',
            'document_file.file' => 'Upload a valid file',
        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        /*sanitazie*/
        $input = $this->all();
        if($input['document_id']  == 72){
            /*doc - WCP-1*/
        }
        $input['date_reference'] = isset($input['date_reference'])   ? standard_date_format($input['date_reference']) : null;

        /**/
        $this->replace($input);
        return $this->all();

    }


}
