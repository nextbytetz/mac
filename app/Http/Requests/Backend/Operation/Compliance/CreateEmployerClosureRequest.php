<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use Carbon\Carbon;
use PhpParser\PrettyPrinter\Standard;

class CreateEmployerClosureRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $array = [];
        $basic = [];
        $optional = [];
        $this->checkIfExist($input);

        /*Optional - Dormant de-registration*/
        if(array_key_exists('dormant_source_cv_id', $input)){
            $array = ['dormant_source_cv_id' => 'required'];
            $optional = array_merge($array, $optional);

            if(array_key_exists('other_dormant_source', $input)){
                $array = ['other_dormant_source' => 'required'];
                $optional = array_merge($array, $optional);
            }

        }

        $basic = [
            'close_date' => 'required|date|date_format:Y-n-j|before_or_equal:close_date_cut_off',
            'application_date' => 'required|date|date_format:Y-n-j|before_or_equal:today',
            'specified_reopen_date' => 'nullable|date|date_format:Y-n-j|after_or_equal:application_date|after:close_date',
            'reason' => 'required',
            'closure_type' => 'required',
            'sub_type_cv_id' => 'required',
        ];

        return array_merge($optional, $basic);
    }

    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
        $input['close_date'] = isset($input['close_date'])   ? standard_date_format($input['close_date']) : null;
        $input['application_date'] = isset($input['application_date'])   ? standard_date_format($input['application_date']) : null;
        $input['specified_reopen_date'] = isset($input['specified_reopen_date'])   ? standard_date_format($input['specified_reopen_date']) : null;
        /*Dob*/

        /*Data when dormant closing*/
        if(array_key_exists('dormant_source_cv_id', $input)){
            //Data
            $data_when_dormant = $this->dataWhenDormantClosing($input);
            $input['close_date'] = ($input['dormant_source_cv_id'] == 'AUSDREGWCF')  ?   $data_when_dormant['close_date'] : $input['close_date'];
            $input['last_contribution'] =$data_when_dormant['last_contribution'];
        }

        $this->replace($input);
        return $this->all();

    }

    /*Check if exist*/
    public function checkIfExist(array $input)
    {
        if($input['action_type'] == 1){
            /*CREATE*/
            $check_if_exist = (new EmployerClosureRepository())->checkIfClosureExistForEmployer($input['employer_id'], $input['close_date']);
            if($check_if_exist == true){
                throw new GeneralException('This closure already exist for this employer! Please check!');
            }
        }else {
            /*edit*/
            $check_if_exist =EmployerClosure::query()->where('id', '<>', $input['employer_closure_id'])->where('employer_id', $input['employer_id'])->whereDate('close_date', standard_date_format($input['close_date']))->count();
            if($check_if_exist == true){
                throw new GeneralException('This closure already exist for this employer! Please check!');
            }
        }
    }


    /**
     * @param array $input
     */
    public function dataWhenDormantClosing(array $input)
    {
        $employer_id = $input['employer_id'];
        $employer = Employer::query()->find($employer_id);
        $last_contrib = $employer->last_contribution;
         $close_date = (isset($last_contrib['contrib_month'])) ? standard_date_format( $last_contrib['contrib_month']) : standard_date_format($employer->doc);
        $last_contrib_amount = $last_contrib['contrib_amount'];
        return ['last_contribution' => $last_contrib_amount, 'close_date' => $close_date];
    }
}
