<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Sysdef\CodeValueRepository;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class StaffEmployerFollowUpReviewRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = $this->all();
        $basic = [
            'review_action' => 'required',
            'comment' => 'required',
        ];

        return $basic;
    }





}


