<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Models\Auth\User;

class StaffEmployerAllocationNewStaffRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $this->validateUsersInfo($input);

        return [
            'staff' => 'required',
        ];
    }



    public function validateUsersInfo(array $input)
    {
        foreach ($input as $key => $value) {
            switch ($key) {
                case 'staff':
                    $selected_staff = $value;
                    foreach ($selected_staff as $staff) {
                        $this->checkUserMustHaveOfficeInformation($staff);
                    }
                    break;
            }
        }
    }


    public function checkUserMustHaveOfficeInformation($user_id)
    {
        $user = User::query()->find($user_id);
        if(!isset($user->office_id)){
            throw new GeneralException('User must be assigned office! Contact system administrator!');
        }
    }

}
