<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use Carbon\Carbon;

class ContributionModificationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $basic = [
          'request_type' => 'required',
          'receipt_id' => 'required',
          'request_date' => 'required|date|date_format:Y-m-d',
      ];
      $optional = [];
      if(isset($input['request_type'])){
          $type_rules = [];
          switch ($input['request_type']) {
             case '1':
             foreach ($input as $key => $value) {
                if (strpos($key,'month_new') !== false) {
                    $optional[$key] = 'required|date|date_format:Y-m-d';}
                }
                break;
                case '2':

                break;
                case '3':

                break;

                default:

                break;
            }
        }
        return array_merge($basic, $optional);
    }

    public function messages()
    {
        $input = $this->all();
        $basic = [
          'request_type.*' => 'Select type of request to modify',
          'receipt_id.*' => 'Select receipt to modify',
          'request_date' => 'Select date of receiving the request'
      ];
      $optional = [];
      if(isset($input['request_type'])){
          $type_rules = [];
          switch ($input['request_type']) {
             case '1':
             foreach ($input as $key => $value) {
                if (strpos($key,'month_new') !== false) {
                    $optional[$key] = 'Select valid new month';                  
                }
            }
            break;
            case '2':

            break;
            case '3':

            break;

            default:

            break;
        }
    }
    return array_merge($basic, $optional);

}
}
