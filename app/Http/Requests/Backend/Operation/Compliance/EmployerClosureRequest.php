<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

class EmployerClosureRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'close_date' => 'required|date|date_format:Y-n-j|before_or_equal:today_date',
            'reason' => 'required',
            'closure_type' => 'required',
            'employer_close_file' => [
                'required',
                'file',
                'mimetypes:application/pdf,image/png,image/tiff,image/x-tiff,image/bmp,image/x-windows-bmp,image/gif,image/x-icon,image/jpeg,image/pjpeg,image/x-portable-bitmap,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.wordprocessingml.template,application/vnd.ms-word.document.macroEnabled.12,application/vnd.ms-word.template.macroEnabled.12',
            ],
        ];
    }

    public function messages()
    {
        return [
            'employer_close_file.mimetypes' => 'Only PDF, Microsoft Word Document or Images are allowed for upload',
            'employer_close_file.required' => 'Upload closing business confirmation letter',
            'employer_close_file.file' => 'Upload a valid file',
        ];
    }

}
