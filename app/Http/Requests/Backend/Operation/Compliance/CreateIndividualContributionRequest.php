<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class CreateIndividualContributionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = request()->all();
        //logger($input);

        $basic = [
            'remarks' => 'nullable',
            'basic_pay' => 'required',
            'gross_pay' => 'required',
            'rctno' => 'sometimes|required',
            'contrib_month' => 'sometimes|required|date|date_format:Y-n-j|before_or_equal:this_date',
            'employer_id' => 'sometimes|required'
        ];
        if (isset($input['incident_type_id']) && $input['incident_type_id'] == 2) {
            $basic['employee_still_at_work'] = "required";
            if ($input['employee_still_at_work']) {
                $basic['contrib_month_atwork'] = "required";
            } else {
                $basic['contrib_month_retired'] = "required";
            }
        }

        if (isset($input['hired_on_incident_month']) And $input['hired_on_incident_month']) {
            $basic['datehired'] = "required|date";
        }

        return $basic;


    }

}
