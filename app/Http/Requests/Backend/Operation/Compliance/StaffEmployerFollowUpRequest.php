<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;
use App\Models\Sysdef\CodeValue;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class StaffEmployerFollowUpRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = $this->all();
        $contact = [];
        $optional = [];
        $array = [];
        $cv = (new CodeValueRepository())->findByReference($input['follow_up_type_cv_id']);
        $feedback_cv = (new CodeValueRepository())->findByReference($input['feedback_cv_id']);
        if(isset($cv)){
//        if email
            if ($cv->reference == 'FUEMAIL'){
                $contact=[
                    'contact' => 'required|email'
                ];
            }
//        if phone
            if ($cv->reference == 'FUPHONC'){
                $contact=[
                    'contact' => 'required|phone:TZ'
                ];
            }


            /*visit/letter*/
            if($input['action_type'] == 1) {
                if ($cv->reference == 'FUVSIT' || $cv->reference == 'FULETT'){
                    $contact = [
                        'document_file' => [
                            'required',
                            'file',
                            'mimetypes:application/pdf',
                        ],
                    ];
                }
            }else{
                /*when modifying*/
                if ($cv->reference == 'FUVSIT' || $cv->reference == 'FULETT'){
                    $contact = [
                        'document_file' => [
                            'nullable',
                            'file',
                            'mimetypes:application/pdf',
                        ],
                    ];
                }
            }
        }



        /*Feedback - others*/
        if(isset($feedback_cv)){
            if($feedback_cv->reference == 'SEFOTH'){
                $array = [
                    'remark' => 'required',
                ];
                $optional = array_merge($array, $optional);
            }
        }

        /*Reminder email*/
        if(array_key_exists('reminder_email', $input)){
            $array = [
                'reminder_email' => 'nullable|email',
            ];
            $optional= array_merge($optional, $array);
        }


        if(isset($input['staff_employer_id'])){
            $array = [
                'date_of_follow_up' => 'required|date|date_format:Y-n-j|after_or_equal:start_date|before_or_equal:end_date',
            ];
            $optional= array_merge($optional, $array);
        }


        $basic = [
            'remark' => 'nullable|max:500',
            'follow_up_type_cv_id' => 'required',
            'contact_person' => 'required',

            'feedback_cv_id' => 'required',
//            'reminder_date' => 'required|date|date_format:Y-n-j|after_or_equal:date_of_follow_up',
        ];

        return array_merge($basic, $contact, $optional);
    }


    /**
     * @return array
     */
    public function sanitize()
    {
        /*dob*/
        $input = $this->all();
        $input['date_of_follow_up'] = isset($input['date_of_follow_up'])   ? standard_date_format($input['date_of_follow_up']) : null;
        $input['reminder_date'] = isset($input['reminder_date'])   ? standard_date_format($input['reminder_date']) : null;
//        $input['follow_up_type_cv_id'] = isset($input['follow_up_type_cv_id']) ? (new CodeValueRepository())->findByReference(request()->input('follow_up_type_cv_id'))->id : null ;
//        $input['feedback_cv_id'] = isset($input['feedback_cv_id']) ? (new CodeValueRepository())->findByReference(request()->input('feedback_cv_id'))->id : null ;
        /*Dob*/

        /*Add input from multiple page*/
        $staff_employer = DB::table('staff_employer')->where('id', $input['staff_employer_id'])->first();
        $input['start_date'] = $staff_employer->start_date;
        $input['end_date'] = $staff_employer->end_date;
        $input['employer_id'] = $staff_employer->employer_id;
        $this->replace($input);
        return $this->all();

    }


    public function messages()
    {
        return [
            'document_file.mimetypes' => 'Only PDF files allowed for upload',

        ];
    }


}


