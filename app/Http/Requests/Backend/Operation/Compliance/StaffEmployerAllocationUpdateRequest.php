<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

class StaffEmployerAllocationUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:end_of_last_allocation',
            'end_date' => 'required|date|date_format:Y-n-j|after_or_equal:allocation_start_date',

        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
        $input['start_date'] = isset($input['start_date'])   ? standard_date_format($input['start_date']) : null;
        $input['end_date'] = isset($input['end_date'])   ? standard_date_format($input['end_date']) : null;
        /*Dob*/
        $this->replace($input);
        return $this->all();

    }

}
