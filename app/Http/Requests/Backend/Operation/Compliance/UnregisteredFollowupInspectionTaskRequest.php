<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class UnregisteredFollowupInspectionTaskRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'name' => 'required',
            'staffs' => 'required',
            'target_employer' => 'required',
        ];
        $target_employer = request()->input("target_employer");
        switch ($target_employer) {
            case 1:
                //Selected Inspection Profile
                $return['inspection_profile_id'] = 'required';
                $return['employer_count'] = 'required';
                 break;
            case 0:
                //Selected Individual Employers
                $return['employer'] = 'required';
                     break;
            default:

        }
        return $return;
    }

}
