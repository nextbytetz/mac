<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use App\Models\Operation\Compliance\Member\Employee;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class CreateDependentRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = $this->all();
        $identity = [];
        $basic = [];
        $dob = [];
        $optional = [];
        $array = [];
        $age_limit_date = null;
        /*Check co-existing for other dependency type*/
        $this->checkIfOtherDepTypeCoExists($input);
        $this->validateOtherDependents($input);
        $this->checkIfDependentExists($input);
        if (array_key_exists('identity', $input)){
//            when is none
            if ($input['identity'] == 5){
                $identity = [
                    'id_no' => 'nullable',
                ];
            }else{

                $identity = [
                    'id_no' => 'required',
                ];
            }
        }
        /*check age limit on DOB and Gender*/
        switch ($input['dependent_type']) {
            case 2: //wife
                $age_limit = get_wife_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }elseif($input['gender_type'] <> 2){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;
            case 1: //husband
                $age_limit = get_husband_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }elseif($input['gender_type'] <> 1){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;
            case 5: //mother
                $age_limit = get_mother_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }elseif($input['gender_type'] <> 2){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;
            case 4: //father
                $age_limit = get_father_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }elseif($input['gender_type'] <> 1){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;
            case 7: //Constant care assistant
                $age_limit = get_constant_care_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }
                break;

            case 8: //Brother
            case 10: //Half Brother
                if($input['gender_type'] <> 1){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;

            case 9: //Sister
            case 11: //Half Sister
                if($input['gender_type'] <> 2){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;

            case 12: //Grand Parent
                $age_limit = get_grand_parent_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }
                break;

            default:
                $dob = [ 'dependent_dob' => 'required|date|date_format:Y-n-j|before_or_equal:this_date'];
        }



        /*check for child inputs*/
        if(array_key_exists('beneficiary', $input) &&  $input['dependent_type'] == 3){

            /*check child age limits*/
            $this->checkIfChildIsBeneficiary($input);

            $birth_certificate = [];
            $array = [
                'isdisabled' => 'required',
                'iseducation' => 'required',
            ];

            if($input['action_type'] == 1){
                /*create*/
                $birth_certificate = [
                    'birth_certificate_no' => [ 'required',
                        Rule::unique('dependents')->whereNotNull('birth_certificate_no')
                    ]
                ];
            }else{
                /*when editing*/
                $dependent_id = $input['dependent_id'];
                $birth_certificate = [
                    'birth_certificate_no' => [ 'required',
                        Rule::unique('dependents')->whereNotNull('birth_certificate_no')->where(function ($query) use ($dependent_id) {
                            $query->where('id','<>', $dependent_id);
                        }),
                    ]
                ];
            }
            $optional = array_merge($array , $birth_certificate,$optional);


        }



        $basic = [
            'first_name' => 'required|alpha_spaces',
            'middle_name' => 'nullable|alpha_spaces',
            'last_name' => 'required|alpha_spaces',
            'gender_type' => 'required',
            'dependent_type' => 'required',
            'identity' => 'required',
            'email_address' => 'nullable|email',
            'country' => 'required',
            'location_type' => 'required',
            'funeral_grant_percent' => 'nullable|numeric|max:100|min:0',
            'dependency_percentage' => 'nullable|numeric|max:100|min:0',
        ];

        return array_merge($basic, $identity, $dob, $optional);


    }




    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
        $input['dependent_dob'] = standard_date_format($input['dependent_dob']);
        $input['isdisabled'] = isset($input['isdisabled']) ? $input['isdisabled'] : 0;
        $input['iseducation'] = isset($input['iseducation']) ? $input['iseducation'] : 0;
        /*Dob*/
        $this->replace($input);
        return $this->all();

    }


    /**
     * Validated if child is eligible for pension/ survivor grants
     * ONly for beneficiary and survivor pension receiver
     */
    public function checkIfChildIsBeneficiary(array $input){
        $dob_child_age_limit = standard_date_format($this->getDobForChildAgeLimit($input));// 18yrs from death date
        $dob_child_education_age_limit = standard_date_format($this->getDobForChildEducationAgeLimit($input));// 21yrs from death date
        $dob = standard_date_format($input['dependent_dob']);
        $exception = 'Child age do not qualify for survivor pensions! Please check!';

        if($input['isdisabled'] == 0 && $input['iseducation'] == 0){
            if(comparable_date_format($dob) < comparable_date_format($dob_child_age_limit)){
                throw new GeneralException($exception);
            }

        }elseif($input['isdisabled'] == 0 && $input['iseducation'] == 1){
            if(comparable_date_format($dob) < comparable_date_format($dob_child_education_age_limit)){
                throw new GeneralException($exception);
            }
        }

    }

    /*Dob date for payroll child age limit - 18yrs*/
    public function getDobForChildAgeLimit(array $input){
        $child_age_limit = sysdefs()->data()->payroll_child_limit_age;//18yrs
        $death_date = $input['death_date'];
        return Carbon::parse($death_date)-> subYears($child_age_limit);
    }

    /*Dob date for payroll child age limit fro education- 21yrs*/
    public function getDobForChildEducationAgeLimit(array $input){
        $child_age_limit = sysdefs()->data()->payroll_child_education_limit_age;//21yrs
        $death_date = $input['death_date'];
        return Carbon::parse($death_date)-> subYears($child_age_limit);
    }


    /*Other dependency type - co exists*/
    public function checkIfOtherDepTypeCoExists(array $input)
    {
        $this_other_dep_type = isset($input['other_dependency_type']) ? $input['other_dependency_type'] : null;
        if($this_other_dep_type){
            $employee = Employee::query()->find($input['employee_id']);
            $query = DB::table('dependent_employee')->where('employee_id', $employee->id)->whereRaw(" coalesce(other_dependency_type, 0) <> ?", [$this_other_dep_type]);
            if(isset($input['dependent_id'])) {
//            $check = $employee->dependents()->where('other_dependency_type', '<>', $this_other_dep_type)->whereNotNull('other_dependency_type')->first();
                $check = 0;
            }else{
                $check =$query->count();
            }
            if($check > 0){
                throw new GeneralException('There is other dependent with different dependency type! Please check!');
            }

        }

    }

    /*Validate other dependents*/
    public function validateOtherDependents(array $input)
    {
        /*check if Parents are Full only*/
        $relation = $input['dependent_type'];
        $other_dependency_type = isset($input['other_dependency_type']) ? $input['other_dependency_type'] : null;
        if(isset($other_dependency_type)){
            if($other_dependency_type != 1 && ($relation == 4 or $relation == 5)){
                throw new GeneralException('Dependency type selected not valid for this relation! Please check!');
            }
        }
    }


    /**
     * @param array $input
     * check if dependent exists
     */
    public function checkIfDependentExists(array $input){
        $check = (new DependentRepository())->query()->where('firstname', $input['first_name'])->where('lastname', $input['last_name'])->where('dob', standard_date_format($input['dependent_dob']));

        if(isset($input['dependent_id'])){
            $check = $check->where('id', '<>', $input['dependent_id']);
        }

        if($check->count() > 0){
            throw new GeneralException('This dependent already exists! Please check!');
        }
    }

}
