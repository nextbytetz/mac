<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

class UploadMergeEmployer extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_file50' => [
                'required',
                'file',
                'mimetypes:application/excel,application/x-excel,application/x-msexcel,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.spreadsheetml.template,application/vnd.ms-excel.sheet.macroEnabled.12,application/vnd.ms-excel.template.macroEnabled.12,application/vnd.ms-excel.addin.macroEnabled.12,application/vnd.ms-excel.sheet.binary.macroEnabled.12,application/octet-stream',
            ],
        ];
    }

    public function messages()
    {
        return [
            'document_file50.mimetypes' => 'Only Excel document types are allowed for upload',
            'document_file50.required' => 'Upload file',
            'document_file50.file' => 'Upload a valid file',
        ];
    }

}
