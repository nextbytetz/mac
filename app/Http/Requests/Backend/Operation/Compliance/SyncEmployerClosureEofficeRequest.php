<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

class SyncEmployerClosureEofficeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employer_id' => 'required',
            'close_date' => 'required|date|date_format:Y-n-j',
            'reason' => 'required',
            'closure_type' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
        $input['close_date'] = isset($input['close_date'])   ? standard_date_format($input['close_date']) : null;
        /*Dob*/
        $this->replace($input);
        return $this->all();

    }

}
