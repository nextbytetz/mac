<?php

namespace App\Http\Requests\Backend\Operation\Compliance;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class CreateEmployeeRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = request()->all();

        $basic = [
            'firstname' => 'required|alpha_spaces',
            'middlename' => 'nullable|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'dob' => 'required|date|date_format:Y-n-j|before_or_equal:this_date',
            'gender_id' => 'required',
            'employee_category_cv_id' => 'required',
            'basicpay' => "nullable|regex:/^\d*(\.\d{1,2})?$/",
            'allowance' => "nullable|regex:/^\d*(\.\d{1,2})?$/",
            //'employer' => 'required',
            'email' => 'nullable|email',
            'phone' => 'required|phone:TZ',
            'marital_status_cv_id'=> 'required',
            'passport_no' => 'required_unless:country_id,1',
            'work_permit_no' => 'required_unless:country_id,1',
            'residence_permit_no' => 'required_unless:country_id,1',
        ];
        if (empty($input['return_url'])) {
            $basic['employer'] = 'required';
        }
        return array_merge($basic);
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'passport_no.required_unless' => 'Passport Number is required',
            'work_permit_no.required_unless'  => 'Work Permit Number is required',
            'residence_permit_no.required_unless'  => 'Residence Permit Number is required',
            'basicpay.regex' => 'The amount should be in form of 0.00',
            'allowance.regex' => 'The amount should be in form of 0.00',
        ];
    }

}
