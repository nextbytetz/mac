<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

/**
 * Class
 * * @package
 */
class CreateCurrentEmployeeStateRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $return = [];
        $input = request()->all();

        foreach ($input as $key => $value) {

            if (strpos($key, 'percent_of_ed_ld') !== false) {
                $return[$key] = 'nullable|numeric|min:0|max:100';
                $return ['medical_expense'] = 'sometimes|required';
            }
            if (strpos($key, 'days') !== false) {
                $return[$key] = 'required|numeric';
            }


        }

        $basic = [
//            'medical_expense' => 'required',
            // 'health_state_checklist_id' => 'required',
        ];
        return array_merge($return, $basic);


    }

}
