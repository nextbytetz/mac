<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

/**
 * Class
 * * @package
 */
class CreateMedicalExpenseRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {
        // Check if date is set

        if (((request()->input('member_type_id')))== 3)  {
            $return_insurance_rule = 'required';
        } else {
            $return_insurance_rule = '';
        }
        $basic = [
            'member_type_id' => 'required',
            'insurance_id' => $return_insurance_rule,
            // 'amount' => 'required',
        ];

        return $basic;


    }

}
