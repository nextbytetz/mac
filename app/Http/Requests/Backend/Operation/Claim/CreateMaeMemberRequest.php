<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class CreateMaeMemberRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check if date is set
        $return = [];
        $input = request()->all();
        foreach ($input as $key => $value) {
            if (strpos($key, 'amount') !== false) {
                $return[$key] = 'required|min:1';
            }

            if (strpos($key, 'to_date') !== false){
                $id = substr($key,7);
                if (isset($input['feedback' . $id]) && $input['feedback' . $id] == 1) {
                    $return[$key] = 'nullable|date|before_or_equal:this_date|after_or_equal:from_date' . $id;

                    if (request()->input('incident_type')<> 3) {
                        $return[$key] = 'nullable|date|before_or_equal:this_date|after_or_equal:from_date' . $id;

                    }else {
                        //death
                        $return[$key] = 'nullable|date|before_or_equal:this_date|before_or_equal:incident_date|after_or_equal:from_date' . $id;

                    }

                }

            }
            if (strpos($key, 'from_date') !== false){
                $id = substr($key,9);

                if (isset($input['feedback' . $id]) && $input['feedback' . $id] == 1) {
                    //not death
                    if (request()->input('incident_type')<> 3) {
                        $return[$key] = 'nullable|date|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:incident_date';
                    }else {
                        //death
                        $return[$key] = 'nullable|date|before_or_equal:this_date|after_or_equal:wcf_start_date|before_or_equal:incident_date';
                    }
                }

            }
        }


        /**
         * Attend Date =======
         */
        //not death
        if (request()->input('incident_type')<> 3){
            $attend_date = [ 'attend_date' => 'nullable|date|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:incident_date',];}else {
            //death
            $attend_date = ['attend_date' => 'nullable|date|before_or_equal:this_date|after_or_equal:wcf_start_date|before_or_equal:incident_date',];
        }
// -- end attend date -------

        /**
         * Dismiss Date ========
         */
        //not death
        if (request()->input('incident_type')<> 3){
            $dismiss_date = [ 'dismiss_date' => 'nullable|date|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:attend_date',];}else {
            //death
            $dismiss_date = ['dismiss_date' =>'nullable|date|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:attend_date|before_or_equal:incident_date',];
        }

        //--End --dismiss date ----

        $basic = [
            'expense_amount' => 'required',
            'member_type_id' => 'required|integer',
            'health_provider_id' => 'required',
            'medical_practitioner_id' => 'nullable',
            'remarks' => 'nullable',

        ];

        return array_merge($return,$basic,$attend_date,$dismiss_date);
    }
}
