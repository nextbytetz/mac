<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Factory as ValidationFactory;
//use Illuminate\Foundation\Http\FormRequest;

class UpdateMaeMemberMultipleRequest extends Request
{
    /**
     * UpdateMaeMemberMultipleRequest constructor.
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     * @param ValidationFactory $validationFactory
     */
    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null, ValidationFactory $validationFactory)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $validationFactory->extend(
            'rctno_duplicate',
            function ($attribute, $status) {
                $return = true;
                if ($status) {
                    $return = false;
                }
                return $return;
            },
            ''
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'member_type_id' => 'required',
            'amount' => 'required|numeric',
            'receive_date' => 'nullable|after_or_equal:incident_date',
            'remarks' => 'nullable',
        ];

        $input = request()->all();

        if ($input['assessment']) {
            $return['assessed_amount'] = 'required';
        }

        $receipt = [];

        foreach ($input as $key => $value) {
            if (strpos($key, 'health_provider_id') !== false) {
                $id = substr($key, 18);
                $rctno = trim(strtolower($input['rctno' . $id]));
                $sub_amount = trim(strtolower($input['sub_amount' . $id]));
                if (isset($input['mae_sub_id' . $id])) {
                    $mae_sub_id = $input['mae_sub_id' . $id];
                } else {
                    $mae_sub_id = 0;
                }

                if (in_array($rctno, $receipt)) {
                    $return['rctno' . $id] = 'rctno_duplicate:1';
                } else {
                    $receipt[] = $rctno;
                    $return['rctno' . $id] = [
                        'required',
                        'string',
                        Rule::unique('mae_subs', 'rctno')->where(function ($query) use ($rctno, $sub_amount, $mae_sub_id) {
                            if ($mae_sub_id) {
                                $query->where("rctno", $rctno)->where("amount", $sub_amount)->where("id", "<>", $mae_sub_id);
                            } else {
                                $query->where("rctno", $rctno)->where("amount", $sub_amount);
                            }

                        }),
                    ];
                    if ($input['assessment']) {
                        /*$return['rctno' . $id] = [
                            'required',
                            'string',
                        ];*/
                    } else {
                        /*$return['rctno' . $id] = [
                            'required',
                            'string',
                            Rule::unique('mae_subs', 'rctno')->where(function ($query) use ($rctno, $sub_amount, $mae_sub_id) {
                                $query->where("rctno", $rctno)->where("amount", $sub_amount)->where("id", "<>", $mae_sub_id);
                            }),
                        ];*/
                    }

                }
                $return['health_provider_id' . $id] = 'nullable';
                $return['sub_amount' . $id] = 'required|numeric';
                if ($input['assessment']) {
                    $return['sub_assessed_amount' . $id] = 'required|numeric';
                    if ($input['sub_amount' . $id] != $input['sub_assessed_amount' . $id]) {
                        $return['remarks' . $id] = 'required';
                    }
                } else {
                    $return['sub_assessed_amount' . $id] = 'nullable|numeric';
                }
            }
        }

        return $return;
    }

    public function messages()
    {
        //return parent::messages();
        $messages = [];
        $input = request()->all();
        foreach ($input as $key => $value) {
            if (strpos($key, 'rctno') !== false) {
                $messages[$key . '.rctno_duplicate'] = 'Receipt Number is duplicated';
                $messages[$key . '.unique'] = 'Receipt Number and Amount combination is duplicated';
            }
            if (strpos($key, 'remarks') !== false) {
                $messages[$key . '.required'] = 'Please add remarks, amount differs';
            }
        }
        return $messages;
    }
}
