<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use Carbon\Carbon;

class ManualSystemFileCreateDependentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $input = $this->all();
        $identity = [];
        $basic = [];
        $dob = [];
        $optional = [];
        $array = [];
        $arrears = [];
        $age_limit_date = null;

        $this->checkIfOtherDepMonthsAreValid($input);

        if (array_key_exists('identity', $input)){
//            when is none
            if ($input['identity'] == 5){
                $identity = [
                    'id_no' => 'nullable',
                ];
            }else{

                $identity = [
                    'id_no' => 'required',
                ];
            }
        }
        /*check age limit on DOB and Gender*/
        switch ($input['dependent_type']) {
            case 2: //wife
                $age_limit = get_wife_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }elseif($input['gender_type'] <> 2){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;
            case 1: //husband
                $age_limit = get_husband_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }elseif($input['gender_type'] <> 1){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;
            case 5: //mother
                $age_limit = get_mother_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }elseif($input['gender_type'] <> 2){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;
            case 4: //father
                $age_limit = get_father_age_limit();
                $age_limit_date = Carbon::parse('now')->subYear($age_limit)->format('Y-n-j');
                $dob = [ 'dependent_dob'=>'required|date|date_format:Y-n-j|before_or_equal:this_date|before_or_equal:'. $age_limit_date];
                if (comparable_date_format($input['dependent_dob']) > comparable_date_format($age_limit_date)){
                    throw new GeneralException(trans('exceptions.backend.compliance.age_limit_not_correct'));
                }elseif($input['gender_type'] <> 1){
                    throw new GeneralException(trans('exceptions.backend.compliance.gender_not_correct'));
                }
                break;
            default:
                $dob = [ 'dependent_dob' => 'required|date|date_format:Y-n-j|before_or_equal:this_date'];
        }

        /*Arrears request option*/
        if(array_key_exists('ispaid', $input)){
            $array = ['ispaid' => 'required'];
            $optional = array_merge($array, $optional);
            if($input['ispaid'] == 1){
                $array = ['hasarrears' => 'required'];
                $optional = array_merge($array, $optional);
                /*check for other dep months paid*/
                if(isset($input['isotherdep'])){
                    if($input['isotherdep'] == 1){
                        $array = ['other_dep_months_paid' => 'required|numeric'];
                        $optional = array_merge($array, $optional);
                    }
                }

            }
            $optional = array_merge($array, $optional);
        }

        if(array_key_exists('hasarrears', $input)){
            $array = ['hasarrears' => 'required'];
            $optional = array_merge($array, $optional);
            if($input['hasarrears'] == 1){
                $array = ['pending_pay_months' => 'required'];
            }
            $optional = array_merge($array, $optional);
        }

        /*check for dependency type paid*/
        if(isset($input['isotherdep'])){
            if($input['isotherdep'] == 1){
                $array = ['other_dependency_type' => 'required'];
                $optional = array_merge($array, $optional);
            }
        }

        $basic = [
            'first_name' => 'required|alpha_spaces',
            'middle_name' => 'nullable|alpha_spaces',
            'last_name' => 'required|alpha_spaces',
            'gender_type' => 'required',
            'identity' => 'required',
            'email_address' => 'nullable|email',
            'country' => 'required',
            'location_type' => 'required',
            'mp' => 'required',
//            'bank' => 'required',
//            'accountno_dependent' => 'required',
            'dependent_type' => 'required',
            'phone_1' => 'nullable|phone:TZ',

        ];

        return array_merge($basic, $identity, $dob, $optional);


    }




    /**
     * @return array
     */
    public function sanitize()
    {

        /*dob*/
        $input = $this->all();
        $input['dependent_dob'] = standard_date_format($input['dependent_dob']);
        /*Dob*/

        $this->replace($input);
        return $this->all();

    }


    /**
     * Validated if child is eligible for pension/ survivor grants
     * ONly for beneficiary and survivor pension receiver
     */
    public function checkIfChildIsBeneficiary(array $input){
        $dob_child_age_limit = standard_date_format($this->getDobForChildAgeLimit($input));// 18yrs from death date
        $dob_child_education_age_limit = standard_date_format($this->getDobForChildEducationAgeLimit($input));// 21yrs from death date
        $dob = standard_date_format($input['dependent_dob']);
        $exception = 'Child age do not qualify for survivor pensions! Please check!';
        if($input['isdisabled'] == 0 && $input['iseducation'] == 0){
            if($dob < $dob_child_age_limit){
                throw new GeneralException($exception);
            }

        }elseif($input['isdisabled'] == 0 && $input['iseducation'] == 1){
            if($dob < $dob_child_education_age_limit){
                throw new GeneralException($exception);
            }
        }


    }

    /*Dob date for payroll child age limit - 18yrs*/
    public function getDobForChildAgeLimit(array $input){
        $child_age_limit = sysdefs()->data()->payroll_child_limit_age;//18yrs
        $death_date = $input['death_date'];
        return Carbon::parse($death_date)-> subYears($child_age_limit);
    }

    /*Dob date for payroll child age limit fro education- 21yrs*/
    public function getDobForChildEducationAgeLimit(array $input){
        $child_age_limit = sysdefs()->data()->payroll_child_education_limit_age;//21yrs
        $death_date = $input['death_date'];
        return Carbon::parse($death_date)-> subYears($child_age_limit);
    }


    /*Check if other dep months are valid*/
    public function checkIfOtherDepMonthsAreValid(array $input)
    {
        $months_payable = sysdefs()->data()->max_payable_months_otherdep_full;
        $total_months_inserted = 0;
        if(isset($input['other_dependency_type'])){
            if($input['other_dependency_type'] ==1){
                $months_paid = isset($input['other_dep_months_paid']) ? $input['other_dep_months_paid'] : 0;
                $pending_pay_months = isset($input['pending_pay_months']) ? $input['pending_pay_months'] : 0;
                $total_months_inserted = $months_paid + $pending_pay_months;

                $limit_payable_months = ($pending_pay_months > 0) ? ($months_payable -1) : $months_payable;
                /*exclude this month */
                if($total_months_inserted > $limit_payable_months)
                {
                    throw new GeneralException('Months payable not valid for this dependent! Please check');
                }
            }

        }
    }

}
