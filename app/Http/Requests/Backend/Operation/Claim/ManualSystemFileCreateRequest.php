<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;

class ManualSystemFileCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = request()->all();
        $pensioner = [];
        $pd =[];
        $array = [];
        $optional = [];
        $basic = [

        ];
        /*Arrears request option*/
        if(array_key_exists('ispaid', $input)){
            $array = ['ispaid' => 'required'];
            $optional = array_merge($array, $optional);
            if($input['ispaid'] == 1){
                $array = ['hasarrears' => 'required'];
                $optional = array_merge($array, $optional);
            }

            if(array_key_exists('hasarrears', $input)){
                if($input['hasarrears'] == 1){
                    $array = ['pending_pay_months' => 'required'];
                    $optional = array_merge($array, $optional);
                }

            }

        }
//        /*check if check correct is clicked*/
//        if(!array_key_exists('check_correct', $input)){
//            throw new GeneralException('Please check if all data are correct!');
//        }
        /*pd option*/
        if($input['incident_type'] != 3){
            /*not death*/
            $pd =[
                'date_of_mmi' =>'required|date|after_or_equal:notification_date',
                'pd' => 'required|numeric|min:30|max:100',
                'mp' =>'required',
                'dob' => 'required|date|before:this_date',
//                'bank' => 'required',
//                'accountno' => 'required',
                'phone' => 'nullable|phone:TZ'

            ];



        }



        return array_merge($basic,$pd, $pensioner, $optional);
    }
}
