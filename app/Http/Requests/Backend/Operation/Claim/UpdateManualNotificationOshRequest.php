<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use Illuminate\Foundation\Http\FormRequest;

class UpdateManualNotificationOshRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $incident_type_id = $input['incident_type_id'];
        $status_cv_ref = $input['status_cv_ref'];
        $array = [];
        $optional = [];
        $basic = [];

//        if($status_cv_ref == 'MANNOTST01'){
        if($status_cv_ref == 'NOVALIDATIONOW'){

            switch($incident_type_id){
                case 1:
                case 4:
                    /*Accident*/
                    $array = [
                        'accident_cause_agency' => 'required',
                        'accident_cause_type' => 'required',
                    ];

                    $optional = array_merge($optional, $array);
                    break;


                case 2:
                case 5:
                    /*Disease*/
                    $array = [
                        'disease_agent' => 'required',
                        'disease_target_organ' => 'required',
                        'disease_type' => 'required',
                        'disease_diagnosed' => 'required',
                    ];

                    $optional = array_merge($optional, $array);
                    break;

                case 3:

                    break;
            }
            /*Accident or Disease*/
            if($incident_type_id = 1 || $incident_type_id == 2)
            {
                $array = [
                    'injury_nature' => 'required',
                    'day_off' => 'required',
                    'light_duties' => 'required',
                    'pd' => 'required|max:100',
                    'rehabilitation' => 'required',
                    'bodily_location' => 'required',
                ];
                $optional = array_merge($optional, $array);
            }

            $array = [
                'hcp_name' => 'required',
            ];
            $optional = array_merge($optional, $array);
        }
        return array_merge($basic, $optional);

    }
}
