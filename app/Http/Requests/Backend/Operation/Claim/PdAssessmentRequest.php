<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

class PdAssessmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'pd' => 'required|numeric',
        ];
        $date_of_mmi = [];
        $input = request()->all();
        /*If pd > 30 should check for date of mmi if filled*/
        if ($input['pd'] > sysdefs()->data()->percentage_imparement_scale)
        {
            $date_of_mmi = [
                'date_of_mmi' => 'required|date|after_or_equal:incident_date|before_or_equal:today_date'
            ];
        }
        return array_merge($return, $date_of_mmi);
    }
}
