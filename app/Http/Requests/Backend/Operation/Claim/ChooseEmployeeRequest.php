<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

class ChooseEmployeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = request()->all();
        $return = [];
        if (!isset($input['employer_unavailable'])) {
            $return['employer'] = 'required';
            if (!isset($input['employee_unavailable'])) {
                $return['employee'] = 'required';
            }
        }
        return $return;
    }
}
