<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24-May-17
 * Time: 3:19 AM
 */

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

class UpdateBankDetailsRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $insurance = [];$employee=[]; $employer=[];
        $input = request()->all();
//        if(isset($input['employee_bank_id'])) {
//            $employee = [
//                'employee_bank_id' => 'required',
//                'employee_bank_branch_id' => 'required',
//                'employee_accountno' => 'required',
//            ];
//        }
//        if(isset($input['employer_bank_id'])) {
//            $employer = [
//                'employer_bank_id' => 'required',
//                'employer_bank_branch_id' => 'required',
//                'employer_accountno' => 'required',
//            ];
//        }
//
//
//            foreach ($input as $key => $value) {
//                if (strpos($key, 'insurance_bank_id') !== false) {
//                    $insurance[$key] = 'required';
//                }
//                if (strpos($key, 'insurance_bank_branch_id') !== false) {
//                    $insurance[$key] = 'required';
//                }
//                if (strpos($key, 'insurance_accountno') !== false) {
//                    $insurance[$key] = 'required';
//                }
//            }

        return array_merge($employee,$employer,$insurance);
    }

}