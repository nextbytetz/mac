<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
//use Illuminate\Foundation\Http\FormRequest;

class UpdateManualPaymentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = request()->all();
        $incident_type_id = $input['incident_type_id'];

        $return = [];

        switch ($incident_type_id) {
            case 1:
            case 2:
                //Accident
                //Disease
            $return['man_mae'] = 'required|numeric';
            $return['man_pd'] = 'required|numeric';
            $return['man_ppd'] = 'required|numeric';
            //$return['man_ptd'] = 'required|numeric';
            $return['man_ttd'] = 'required|numeric';
            $return['man_tpd'] = 'required|numeric';
                break;
            case 3:
                //Death
                $return['man_funeral_grant'] = 'required|numeric';
                break;
            case 4:
            case 5:
                $return['man_mae'] = 'required|numeric';
                //$return['man_pd'] = 'required|numeric';
                //$return['man_ppd'] = 'required|numeric';
                //$return['man_ptd'] = 'required|numeric';
                $return['man_ttd'] = 'required|numeric';
                $return['man_tpd'] = 'required|numeric';
                $return['man_funeral_grant'] = 'required|numeric';
                break;
        }

        $return['man_gme'] = 'nullable|numeric';

        return $return;
    }
}
