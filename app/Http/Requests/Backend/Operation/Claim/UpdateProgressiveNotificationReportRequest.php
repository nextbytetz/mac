<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

class UpdateProgressiveNotificationReportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $basic= [];
        $insurance =[];
        $payment_method =[];
        $input = request()->all();
        //accident
        if ($input['incident_type_id'] == 1 ) {
            $basic = [
                'accident_type_id' => 'required',
                'description' => 'required',
                'accident_place' => 'required',
                'accident_time' => 'required',
                'activity_performed' => 'required',
                'witness_name' => 'required_with:witness_supervisor_name,witness_phone,witness_supervisor_phone,witness_supervisor_unit',
                'witness_phone' => 'required_with:witness_name|phone:TZ',
                'witness_supervisor_phone' => 'required_with:witness_supervisor_name',
                'next_of_kin_phone' => 'nullable|required_with:next_of_kin|phone:TZ',
            ];
        }
        //disease
        if ($input['incident_type_id'] == 2 ) {
            $basic = [
                'disease_name' => 'required',
                'description' => 'required',
                'health_provider_id' => 'nullable',
                'medical_practitioner_id' => 'nullable',
            ];
        }
        //death
        if ($input['incident_type_id'] == 3 ) {
            $basic = [
                'death_cause_id' => 'required',
                'death_place' => 'required',
                'description' => 'required',
                'incident_occurrence_cv_id' => 'required',
                'activity_performed' => 'required',
                'health_provider_id' => 'nullable',
                'medical_practitioner_id' => 'nullable',
            ];
        }
        $employee_id = $input['employee_id'];
        if ($input['employee_id']) {
            //Employee is registered
            $basic['employer_id'] = 'required';
        } else {
            //Employee is not registered
            $basic['employee_name'] = 'required|alpha_spaces';
            $basic['employer_name'] = 'sometimes|required|alpha_spaces';
        }
        $general = [
            'incident_date' => [
                'required',
                'date',
                //'date_format:Y-n-j',
                'before_or_equal:this_date',
                //'after_or_equal:wcf_start_date',
            ],
            'reporting_date' => [
                'required',
                'date',
                //'date_format:Y-n-j',
                'after_or_equal:incident_date',
            ],
            'receipt_date' => [
                'required',
                'date',
                //'date_format:Y-n-j',
                'before_or_equal:this_date',
                'after_or_equal:reporting_date',
            ],
            'district_id' => 'required',
        ];
        return array_merge($basic, $general, $insurance, $payment_method);
    }
}
