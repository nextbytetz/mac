<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

class RegisterEmployee extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = request()->all();
        $return = [
            'employee_id' => 'required',
        ];
        //$return['employer_id'] = 'sometimes|nullable|required';
        if (!$input['has_employer']) {
            $return['employer_id'] = 'required';
        }

        return $return;
    }
}
