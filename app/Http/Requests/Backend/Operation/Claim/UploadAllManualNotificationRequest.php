<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class UploadAllManualNotificationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $basic = [];
        if (!$input['file_preview']) {
            $basic = [
                'has_payroll' => 'required',
            ];
            foreach ($input as $key => $value) {
                if (strpos($key, '_map') !== false) {
                    $basic[$key] = [
                        'required',
                    ];
                }
            }
        }
        return $basic;
    }
}
