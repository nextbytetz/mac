<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;


class InsuranceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {



        /**
         * Request Action Type; 1 => Create, 2 => Update
         */
        $input = request()->all();
        $create_validation = [];
        $update_validation = [];
        /*general*/
        $basic = [
            'name' => 'required',
        ];

        /*  Validation */
        if ($input['request_action_type'] == 1)
        {
            // create
            $create_validation = [
                'external_id' => [ 'required',
                    Rule::unique('insurances')
                ],
            ];
        }else {
            // Update
            $insurance_id = $input['insurance_id'];
            $update_validation = [
                'external_id' => ['required',
                    Rule::unique('insurances')->where(function ($query) use
                    ($insurance_id) {
                        $query->where('id','<>', $insurance_id);
                    })
                ],
            ];
        }



        return array_merge( $basic, $create_validation, $update_validation);
    }

}