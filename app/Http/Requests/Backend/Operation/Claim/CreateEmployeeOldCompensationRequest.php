<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

/**
 * Class
 * * @package
 */
class CreateEmployeeOldCompensationRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        // Check if date is set


        $basic = [
            'incident_type_id' => 'required',
            'who_paid' => 'required',
            'amount' => 'required|numeric',
            'payment_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date',
        ];

        return $basic;


    }

}
