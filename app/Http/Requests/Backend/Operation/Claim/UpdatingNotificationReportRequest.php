<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class
 * * @package
 */
class UpdatingNotificationReportRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $basic= [];
        $insurance =[];
        $payment_method =[];
        $input = request()->all();
        //accident
        if (request()->input('incident_type_id') == 1 ){
            $basic = [
                'accident_type_id' => 'required',
                'description' => 'required',
                'accident_place' => 'required',
                'accident_time' => 'required',
                'body_part_injury_cv_id' => 'required',
            ];
        }
//disease
        if (request()->input('incident_type_id') == 2 ){
            $basic = [
                'disease_know_how_id' => 'required',
                'disease_name' => 'required',
                'body_part_injury_cv_id' => 'required',
            ];
        }

        //death
        if (request()->input('incident_type_id') == 3 ){
            $basic = [
                'death_cause_id' => 'required',
                'death_place' => 'required',
                'certificate_number' => 'required',
                'district_id' => 'required',
            ];
        }
//       check insurance
        if (array_key_exists('pay_through_insurance_id', $input)){
            $insurance =[
                'pay_through_insurance_id' =>'required',
            ];
        }

        //check payment method when not WCF selected
        if (request()->input('initial_expense_member_type_id') <> 6) {
            $payment_method = [
                'pay_method' => 'required',
            ];
        }
        $employee_id = request()->input('employee_id');
        $notification_report_id = request()->input('notification_report_id');
        $general = [
//            'incident_date' =>'required|date|date_format:Y-n-j|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:claim_start_date',
            'incident_date' => ['required','date','date_format:Y-n-j','before_or_equal:this_date','after_or_equal:wcf_start_date',
                Rule::unique('notification_reports')->where(function ($query) use ($employee_id, $notification_report_id) {
                    $query->where('employee_id','=', $employee_id)->where('id', '<>', $notification_report_id)->where('status', '<>', 2)->whereNull('deleted_at');
                })
            ],

            'reporting_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:incident_date',
            'receipt_date' => 'required|date|date_format:Y-n-j|before_or_equal:this_date|after_or_equal:wcf_start_date|after_or_equal:reporting_date|after_or_equal:claim_start_date',
            'incident_exposure_cv_id' => 'required',
            'initial_expense_member_type_id'=> 'required',
            'employer_id'=> 'required',
            'district_id' => 'required',

        ];
        return array_merge($basic,$general,$insurance,$payment_method);


    }

}
