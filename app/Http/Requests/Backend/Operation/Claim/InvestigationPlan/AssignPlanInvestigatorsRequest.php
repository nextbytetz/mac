<?php
namespace App\Http\Requests\Backend\Operation\Claim\InvestigationPlan;

use App\Http\Requests\Request;

class AssignPlanInvestigatorsRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = $this->all();
        $return = ['number_of_investigators' => 'required', 'plan_id' => 'required|after_or_equal:today_date', 'investigators' => 'required|array'];
        if (!empty($input['number_of_investigators']))
        {
            $return['investigators'] .= '|min:'.(int)$input['number_of_investigators'].'|max:'.(int)$input['number_of_investigators'];
        }
        return $return;
    }

    public function messages()
    {
        $input = $this->all();

        if (!empty($input['number_of_investigators']))
        {
            return ['investigators.min' => 'This plan requires ' . (int)$input['number_of_investigators'] . ' investigators', 'investigators.max' => 'This plan requires ' . (int)$input['number_of_investigators'] . ' investigators only'];
        }

    }

}

