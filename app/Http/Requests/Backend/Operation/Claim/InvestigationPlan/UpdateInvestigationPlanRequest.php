<?php

namespace App\Http\Requests\Backend\Operation\Claim\InvestigationPlan;

use App\Http\Requests\Request;

class UpdateInvestigationPlanRequest extends Request
{

  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    $input = $this->all();
    $return = [
      'type' => 'required',
      'category' => 'required',
      'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:today_date',
      'end_date' => 'required|date|date_format:Y-n-j|after_or_equal:start_date',
      'reason' => 'required',
      'number_of_investigators' => 'required|numeric|min:1|max:'.(int)$input['max_officers'],
      'number_of_files' => 'required|numeric|min:1|max:'.(int)$input['max_files'],
    ];

    if ($input['budget_option'] == 1) {
      $attachment = [
        'total_budget' => 'required|numeric',
        'budget_attachment' => 'required|mimes:pdf'
      ];
      $return = array_merge($return, $attachment);
    }

       // dd($return);
    return $return;
  }

   // public function messages()
   // {
   //  $input = $this->all();
   //  if (!empty($input['budget_attachment'])) {
   //     return ['budget_attachment.mimes' => 'The plan budget attachment must be a pdf file'];
   // }

}