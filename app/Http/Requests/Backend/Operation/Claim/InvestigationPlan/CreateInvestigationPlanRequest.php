<?php

namespace App\Http\Requests\Backend\Operation\Claim\InvestigationPlan;

use App\Http\Requests\Request;
use Carbon\Carbon;
class CreateInvestigationPlanRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = $this->all();
        $return = [
            'type' => 'required',
            'category' => 'required',
            'start_date' => 'required|date|date_format:Y-n-j|after_or_equal:today_date',
            'end_date' => 'required|date|date_format:Y-n-j|after_or_equal:start_date',
            'reason' => 'required',
            'number_of_investigators' => 'required|numeric|min:1|max:'.(int)$input['max_officers'],
            'number_of_files' => 'required|numeric|min:1|max:'.(int)$input['max_files'],
        ];
        if ($input['budget_option'] == 1) {
            $attachment = [
                'total_budget' => 'required|numeric',
                'budget_attachment' => 'required|mimes:pdf'
            ];
            $return = array_merge($return, $attachment);
        }

        return $return;
    }

    public function messages()
    {
       $messages = [];
       $input = $this->all();
       if ($input['budget_option'] == 1) {
        $attachment_message = ['budget_attachment.*' => 'Kindly upload the plan budget attachment in pdf format'];
        $messages = array_merge($messages, $attachment_message);
    }
    return $messages;
}

}