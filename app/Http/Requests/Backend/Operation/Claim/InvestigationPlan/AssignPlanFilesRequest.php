<?php

namespace App\Http\Requests\Backend\Operation\Claim\InvestigationPlan;

use App\Http\Requests\Request;

class AssignPlanFilesRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = $this->all();
        $return = [
            'plan_id' => 'required',
            'assigned_user' => 'required',
            'id' => 'required|array|min:1',
        ];
        return $return;
    }

    public function messages()
    {
        $input = $this->all();
        return [
            'assigned_user.required' => 'Kinldy select user to assign', 
            'id.*' => 'Kindly select atleast one file from the list below to assign',
        ];
    }
}