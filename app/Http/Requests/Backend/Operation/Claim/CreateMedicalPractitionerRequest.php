<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class
 * * @package
 */
class CreateMedicalPractitionerRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {

        /**
         * Request Action Type; 1 => Create, 2 => Update
         */
        $input = request()->all();
        $create_validation = [];
        $update_validation = [];


        /*  Validation */
        if ($input['request_action_type'] == 1)
        {
            // create
            $create_validation = [
                'external_id' => [
                    'nullable',
                    Rule::unique('medical_practitioners')
                ],
                'national_id' => [
                    'nullable',
                    Rule::unique('medical_practitioners')
                ],
            ];
        } else {
            // Update
            $medical_practitioner_id = $input['medical_practitioner_id'];
            $update_validation = [
                'external_id' => [
                    'nullable',
                    Rule::unique('medical_practitioners')->where(function ($query) use ($medical_practitioner_id) {
                        $query->where('id','<>', $medical_practitioner_id);
                    })->whereNotNull('external_id')
                ],
                'national_id' => [
                    'nullable',
                    Rule::unique('medical_practitioners')->where(function ($query) use ($medical_practitioner_id) {
                        $query->where('id','<>', $medical_practitioner_id);
                    })->whereNotNull('national_id')
                ],
            ];
        }
        $basic = [
            'firstname' => 'required|alpha_spaces',
            'middlename' => 'nullable|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'phone' => 'required|phone:TZ',
        ];

        return array_merge($create_validation,$update_validation,$basic);


    }

}
