<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
/**
 * Class
 * * @package
 */
class CreateWitnessRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {
        $basic = [
            'witness_name' => 'required|alpha_spaces',
            'witness_phone' => 'required|phone:TZ',
            'witness_supervisor_phone' => 'required_with:witness_supervisor_name',
        ];
        return $basic;
    }

}
