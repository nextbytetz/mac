<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

/**
 * Class
 * * @package
 */
class ApproveRejectionAppealRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $basic = [
            'reason' => 'required',
        ];

        return $basic;
    }

}
