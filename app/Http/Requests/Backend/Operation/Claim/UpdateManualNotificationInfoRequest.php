<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use Illuminate\Foundation\Http\FormRequest;

class UpdateManualNotificationInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        return [
            'incident_date' => [
                'required',
                'date',
                //'date_format:Y-n-j',
                'before_or_equal:this_date',
                //'after_or_equal:wcf_start_date',
            ],
            'reporting_date' => [
                'required',
                'date',
                //'date_format:Y-n-j',
                'after_or_equal:incident_date',
            ],
            'notification_date' => [
                'required',
                'date',
                //'date_format:Y-n-j',
                'before_or_equal:this_date',
                'after_or_equal:reporting_date',
            ],
            'incident_type_id' => 'required',
            'registration_date' => 'nullable|date',
            'status_cv_id' => 'required',
            'district_id' => 'required',
            'reject_reason_cv_id' => 'required_if:status_cv_id,' . $input['rejection_cv_id'],
        ];
    }
}
