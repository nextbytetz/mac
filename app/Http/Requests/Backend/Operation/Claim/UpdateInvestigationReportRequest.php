<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24-May-17
 * Time: 3:19 AM
 */

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

class UpdateInvestigationReportRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $return = [];
        $input = request()->all(); 
        // dd($input['disease_agent']) ;      
        if(isset($input['disease_name_type'])){
            if($input['disease_name_type'] === '-- Select Disease Type --'){
                $input['disease_name_type'] == null;
            }
        }
        if(isset($input['accident_name_cause'])){
           if($input['accident_name_cause'] === '-- Select Disease Type --'){
                $input['accident_name_cause'] == null;
            } 
        }

        foreach ($input as $key => $value) {
            if (strpos($key, 'date') !== false) {
                if (strpos($key, 'investigation_date') !== false) {
                    $return[$key] = 'required|date|date_format:Y-n-j|after_or_equal:when_incident_occurred';
                } elseif (strpos($key, 'incident_date') !== false) {
                    $return[$key] = 'date|date_format:Y-n-j|before_or_equal:when_incident_occurred';
                }
                 else {
                    $return[$key] = 'date|date_format:Y-n-j|after_or_equal:when_incident_occurred';
                }
            }
            if (strpos($key, 'accident_cause') !== false){
                $return[$key] = 'required';
                }
                if (strpos($key, 'disease_type') !== false){
                $return[$key] = 'required';
                }
                if (strpos($key, 'disease_agent') !== false){
                $return[$key] = 'required';
                }
                if (strpos($key, 'target_organ') !== false) {
                $return[$key] = 'required';
                }
                if (strpos($key, 'workplace') !== false){
                $return[$key] = 'required';
                }
            if (strpos($key, 'boolean') !== false){
                $return[$key] = 'required';
            }
        }
        return $return;
    }

}