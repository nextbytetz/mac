<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class
 * * @package
 */
class CreateHealthProviderRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {

        /**
         * Request Action Type; 1 => Create, 2 => Update
         */
        $input = request()->all();
        $create_validation = [];
        $update_validation = [];


        /*  Validation */
        if ($input['request_action_type'] == 1)
        {
            // create
            $create_validation = [
                'external_id' => ['required',
                    Rule::unique('health_providers')
                ],
            ];
        } else {
            // Update
            $health_provider_id = $input['health_provider_id'];
            $update_validation = [
                'external_id' => ['required',
                    Rule::unique('health_providers')->where(function ($query) use ($health_provider_id) {
                        $query->where('id','<>', $health_provider_id);
                    })->whereNotNull('external_id')
                ],
            ];
        }
        $basic = [
            'name' => 'required',
            'district_id' => 'required',
            'is_contracted' => 'required',
            'facility_type' => 'required',
        ];

        return array_merge($create_validation, $update_validation, $basic);
    }

}
