<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24-May-17
 * Time: 3:19 AM
 */

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

class NotificationContributionTrackRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = request()->all();
        $action = [];


        if (array_key_exists('action', $input) ){
            if ($input['action'] == 0){
                $action = [
                    'actions' => 'required'
                ];
            }

        }

        $return = [
            'comments' => 'required',

        ];

        return array_merge($return, $action);
    }

}