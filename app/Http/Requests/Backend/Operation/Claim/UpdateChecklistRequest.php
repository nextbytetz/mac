<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Http\Requests\Request;

class UpdateChecklistRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dump(request()->all());
        $return = [
            // 'accident_cause_cv_id' => 'sometimes|required',
            'accident_date' => 'sometimes|required|date|before_or_equal:reporting_date',
            'death_date' => 'sometimes|required|date|before_or_equal:reporting_date',
            'diagnosis_date' => 'sometimes|required|date|before_or_equal:reporting_date',
            'datehired' => 'nullable|date|before_or_equal:incident_date',
            'last_work_date' => 'nullable|date|before_or_equal:incident_date',
            'paid_month_before' => 'required',
            'monthly_earning_checklist' => 'nullable',
            'salary_being_continued' => 'sometimes|required',
            'salary_full_continued' => 'sometimes|required_if:salary_being_continued,1',
            'accident_incident_exposure_cv_id' => 'nullable',
            'disease_incident_exposure_cv_id' => 'nullable',
            'death_incident_exposure_cv_id' => 'nullable',
            // 'disease_cause_cv_id' => 'sometimes|required',
            'death_cause_id' => 'sometimes|required', //only for death
            'return_to_work' => 'sometimes|required',
            'death_incident_date' => 'sometimes|required|date|before_or_equal:incident_date',
            'confirm_date' => 'sometimes|nullable',
            'health_provider_id' => 'sometimes|nullable',
            'medical_practitioner_id' => 'sometimes|nullable'
        ];

        $input = request()->all();
        if ($input['incident_type_id'] != 3) {
            //if incident is not death (accident or disease)
            $return['body_part_injury_id'] = 'required';
            $return['lost_body_part'] = 'required';
            $return['lost_body_part_id'] = "required_if:lost_body_part,1";
            $return['accident_cause_cv_id'] = 'sometimes|required';
            $return['disease_cause_cv_id'] = 'sometimes|required';
            foreach ($input as $key => $value) {
                if (strpos($key, 'health_provider_id') !== false) {
                    $id = substr($key, 18);
                    $return['health_provider_id' . $id] = 'required';
                    //$return['medical_practitioner_id' . $id] = 'required';
                    $return['member_type_id' . $id] = 'required';
                    $return['insurance_id' . $id] = 'required_if:member_type_id' . $id .',3|nullable';
                    $return['has_ed' . $id] = 'required';
                    $return['excuse_from_duty_days' . $id] = 'required_if:has_ed' . $id .',1|nullable|numeric|min:1';
                    $return['has_hospitalization' . $id] = 'required';
                    $return['hospitalization_days' . $id] = 'required_if:has_hospitalization' . $id . ',1|nullable|numeric|min:1';
                    $return['has_ld' . $id] = 'required';
                    $return['light_duty_days' . $id] = 'required_if:has_ld' . $id . ',1|nullable|numeric|min:1';
                    switch ($input['incident_type_id']) {
                        case 1:
                            //Accident
                        $return['initial_treatment_date' . $id] = 'required|date|after_or_equal:incident_date';
                        $return['last_treatment_date' . $id] = 'nullable|after_or_equal:initial_treatment_date' . $id;
                        break;
                        case 2:
                            //Disease
                        $return['initial_treatment_date' . $id] = 'required|date|before_or_equal:this_date';
                        $return['last_treatment_date' . $id] = 'nullable|before_or_equal:this_date|after_or_equal:initial_treatment_date' . $id;
                        break;
                        case 3:
                            //Death
                        $return['initial_treatment_date' . $id] = 'required|date|before_or_equal:incident_date';
                        $return['last_treatment_date' . $id] = 'nullable|before_or_equal:incident_date|after_or_equal:initial_treatment_date' . $id;
                        break;
                    }

                }
            }

            $return_to_work = request()->input('return_to_work');
            if ($return_to_work) {
                if (!empty($input['maximum_initial_treatment_date'])) {
                    //$return['return_to_work_date'] = 'required|date|after_or_equal:maximum_initial_treatment_date';
                    $return['return_to_work_date'] = 'required|date|before_or_equal:this_date';
                } else {
                    $return['return_to_work_date'] = 'required|date|before_or_equal:this_date';
                }
                $return['job_title_id'] = 'required';
            }
        }else{
            //is death == validate cause
            if ($input['death_cause_id'] == 1) {
               $return['accident_cause_cv_id'] = 'required';
           } elseif($input['death_cause_id'] == 2){
               $return['disease_cause_cv_id'] = 'required';
           }else{
            $return['accident_cause_cv_id'] = 'sometimes|required';
            $return['disease_cause_cv_id'] = 'sometimes|required';}
        }
        // dd($return);
        return $return;
    }

    public function messages()
    {
        return parent::messages(); // TODO: Change the autogenerated stub
    }

}
