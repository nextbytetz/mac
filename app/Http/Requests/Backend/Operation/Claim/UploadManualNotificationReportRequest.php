<?php

namespace App\Http\Requests\Backend\Operation\Claim;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;

class UploadManualNotificationReportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = request()->all();
        if(  request()->hasFile('document_file'))
        {
            //ok
            $file = request()->file('document_file');
            $ext =  $file->getClientOriginalExtension();
            if($ext != 'xlsx'){
                throw new GeneralException('Only xlsx format is allowed! Please check!');
            }

        }else{
            throw new GeneralException('You have not attached any file! Please check!');
        }
        $basic = [
//            'document_file' => 'mimetypes:application/excel,application/x-excel,application/x-msexcel,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.spreadsheetml.template,application/vnd.ms-excel.sheet.macroEnabled.12,application/vnd.ms-excel.template.macroEnabled.12,application/vnd.ms-excel.addin.macroEnabled.12,application/vnd.ms-excel.sheet.binary.macroEnabled.12,application/octet-stream',

        ];
        return $basic;
    }
}
