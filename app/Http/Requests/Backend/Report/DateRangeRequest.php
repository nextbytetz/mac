<?php

namespace App\Http\Requests\Backend\Report;

use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
use Carbon\Carbon;

/**
 * Class
 * * @package
 */
class DateRangeRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        // Check if date is set

        $input = $this->all();
        $this->validateDateRange($input);
        $optional = [];

        $basic = [
//            'from_date' => 'date|date_format:Y-n-j|before_or_equal:this_date',
            'from_date' => 'date|date_format:Y-n-j',
            'to_date' => 'date|date_format:Y-n-j|after_or_equal:from_date',
        ];

        $optional = $this->optionalRequest($input);

        return array_merge($basic, $optional);


    }


    public function sanitize()
    {

        /*from & to date*/

        $input = $this->all();

        if (isset( $input['from_date'] )){

            $input['from_date'] =  standard_date_format($input['from_date']) ;

        }
        if (isset( $input['to_date'] )) {
            $input['to_date'] = standard_date_format($input['to_date']);
        }
        /*from & to date*/

        $this->replace($input);
        return $this->all();

    }


    public function validateDateRange($input){
        /*check from date*/
        if(isset($input['from_day']) && isset($input['from_month']) && isset($input['from_year'])){
            $this->checkIfDateIsValid($input['from_day'], $input['from_month'], $input['from_year'],1);
        }

        /*check to date*/
        if(isset($input['to_day']) && isset($input['to_month']) && isset($input['to_year'])) {
            $this->checkIfDateIsValid($input['to_day'], $input['to_month'], $input['to_year'], 2);
        }
    }


    public function checkIfDateIsValid($day, $month, $year, $date_type){
        if (isset( $day) && isset( $month) && isset( $year)){
            $return = true;
            switch ($month){
                case 2:
                    $mod = $year % 4;
                    if($mod == 0){
                        //long feb
                        $return = ($day > 29) ? false : true;
                    }else{
                        $return = ($day > 28) ? false : true;
                    }

                    break;

                case 4:
                    $return = ($day > 30) ? false : true;
                    break;

                case 6:
                    $return = ($day > 30) ? false : true;
                    break;

                case 9:
                    $return = ($day > 30) ? false : true;
                    break;

                case 11:
                    $return = ($day > 30) ? false : true;
                    break;
            }
            $date_type_desc = ($date_type == 1) ? 'From Date' : 'To Date';

            if ($return == false){
                /*exception*/
                throw new GeneralException( $date_type_desc . ' not valid! Please check');
            }

        }

    }

    /**
     * @param array $input
     * All option requests when submitting form
     */
    public function optionalRequest(array $input)
    {
        $array = [];
        $optional = [];

        /*Fin Year*/
        if(array_key_exists('fin_year', $input)){
            $array = [
                'fin_year' => 'required',
            ];
            $optional = array_merge($array, $optional);

        }

        return $optional;
    }


}
