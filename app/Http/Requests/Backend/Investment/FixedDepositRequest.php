<?php
namespace App\Http\Requests\Backend\Investment;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class FixedDepositRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'fin_year_id' => 'required', 
            'opening_date' => 'required|date', 
            'maturity_date' => 'required|date',
            'interest_rate' => 'required', 
            'interest_type' => 'required', 
            'penalty_rate' => 'required',
            'amount_invested' => 'required', 
            'employer_id' => 'required', 
            'tax_rate' => 'required',
            'exchange_rate' => 'sometimes|required|numeric',
            'interest_type_description' => 'sometimes|required',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'kindly select fixed deposit type', 
            'fin_year_id.required' => 'kindly select financial year', 
            'opening_date.required' => 'kindly enter opening date', 
            'maturity_date.required' => 'kindly enter maturity',
            'interest_rate.required' => 'kindly enter interest rate', 
            'interest_type.required' => 'kindly enter interest type', 
            'penalty_rate.required' => 'kindly enter penalty rate',
            'amount_invested.required' => 'kindly enter amount invested', 
            'employer_id.required' => 'kindly enter institution name', 
            'tax_rate.required' => 'kindly enter tax rate',
            'exchange_rate.required' => 'kindly enter exchange rate',
            'interest_type_description.required' => 'kindly enter narration',
        ];
    }

    public function response(array $errors)
    {
        return response()->json(array('errors' => $errors));
    }

}