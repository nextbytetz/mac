<?php
namespace App\Http\Requests\Backend\Investment;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class TreasuryBondRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fin_year_id' => 'required', 
            'settlement_date' => 'required|date', 
            'maturity_date' => 'required|date',
            'auction_date' => 'required|date', 
            // 'first_coupon_date' => 'sometimes|required|date|before:next_coupon_date',
            // 'last_coupon_date' => 'sometimes|required|date|before:next_coupon_date',
            'next_coupon_date' => 'required|date',
            'coupon_rate' => 'required', 
            'auction_number' => 'required', 
            'tenure' => 'required',
            'amount_invested' => 'required', 
            'price' => 'sometimes|required', 
            'yield' => 'sometimes|required', 
            'tax_rate' => 'required',
            'source' => 'required',
            'holding_number' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fin_year_id.required' => 'kindly select financial year', 
            'settlement_date.required' => 'kindly enter settlement date', 
            'maturity_date.required' => 'kindly enter maturity',
            'auction_date.required' => 'kindly enter auction date',
            // 'first_coupon_date.required' => 'kindly enter first coupon date',
            // 'last_coupon_date.required' => 'kindly enter Last coupon date',
            'next_coupon_date.required' => 'kindly enter next coupon date', 
            'coupon_rate.required' => 'kindly enter coupon rate', 
            'auction_number.required' => 'kindly enter auction number', 
            'tenure.required' => 'kindly enter tenure',
            'amount_invested.required' => 'kindly enter amount invested', 
            'price.required' => 'kindly enter price', 
            'yield.required' => 'kindly enter yield', 
            'tax_rate.required' => 'kindly enter tax rate',
            'source.required' => 'kindly select source',
            'holding_number.required' => 'kindly enter holding number',
        ];
    }

    public function response(array $errors)
    {
        return response()->json(array('errors' => $errors));
    }

}