<?php
namespace App\Http\Requests\Backend\Investment;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class CorporateBondRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => 'sometimes|required',
            'certificate_number' => 'required',
            'type' => 'required',
            'fin_year_id' => 'required', 
            'settlement_date' => 'required|date', 
            'maturity_date' => 'required|date',
            'description' => 'required',
            'auction_date' => 'required|date', 
            // 'first_coupon_date' => 'sometimes|required|date|before:next_coupon_date',
            // 'last_coupon_date' => 'sometimes|required|date|before:next_coupon_date',
            'next_coupon_date' => 'required|date',
            'coupon_rate' => 'required', 
            'ipo_date' => 'required|date',
            'issuer' => 'required',
            'auction_number' => 'required', 
            'yield' => 'sometimes|required',
            'amount_invested' => 'required',
            'tenure' => 'required', 
            'source' => 'required',
        ];
    }

    public function messages()
    {
        return [
           'price.required' => 'kindly enter price',
            'certificate_number.required' => 'kindly select certificate number', 
            'type.required' => 'kindly select type', 
            'fin_year_id.required' => 'kindly select financial year', 
            'settlement_date.required' => 'kindly enter settlement date', 
            'maturity_date.required' => 'kindly enter maturity',
            'auction_date.required' => 'kindly enter auction date',
            // 'first_coupon_date.required' => 'kindly enter first coupon date',
            // 'last_coupon_date.required' => 'kindly enter Last coupon date',
            'next_coupon_date.required' => 'kindly enter next coupon date', 
            'coupon_rate.required' => 'kindly enter coupon rate', 
            'auction_number.required' => 'kindly enter auction number', 
            'ipo_date.required' => 'kindly enter IPO date',
            'issuer.required' => 'kindly enter Issuer',
            'description.required' => 'kindly enter description', 
            'yield.required' => 'kindly enter yield',
            'amount_invested.required' => 'kindly enter amount invested', 
            'tenure.required' => 'kindly enter tenure',
            'source.required' => 'kindly select source',
        ];
    }

    public function response(array $errors)
    {
        return response()->json(array('errors' => $errors));
    }

}