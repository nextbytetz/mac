<?php

namespace App\Http\Requests\Backend\Legal;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UpdateLawyerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $input = [];
        $rules = [
            'external_id' => [
                'nullable',
                'alpha_dash',
                Rule::unique('lawyers')->where(function ($query) {
                    $query->where('id', '<>', request()->input('lawyer_id'));
                }),
            ],
            'category' => 'required',
        ];
        if (request()->input("category") == 0)
        {
            //individual lawyer request
            $rules['firstname'] = 'required|alpha_spaces';
            $rules['middlename'] = 'nullable|alpha_spaces';
            $rules['lastname'] = 'required|alpha_spaces';
        }
        if (request()->input("category") == 1)
        {
            //Firm lawyer request
            $rules['employer_id'] = [
                'required',
                Rule::unique('lawyers')->where(function ($query) {
                    $query->where('id', '<>', request()->input('lawyer_id'));
                }),
            ];
            $rules['firstname'] = 'nullable';
            $rules['middlename'] = 'nullable';
            $rules['lastname'] = 'nullable';
        }
        return $rules;
    }
}