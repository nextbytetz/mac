<?php

namespace App\Http\Requests\Backend\Legal;

use App\Http\Requests\Request;

class StoreCaseHearingRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules = [
            'case_personnel_id' => 'required',
            'firstname' => 'required|alpha_spaces',
            'middlename' => 'nullable|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'hearing_date' => 'required|date|date_format:Y-n-j',
            'hearing_time' => 'required',
            'lawyer_id' => 'required',
            'fee' => 'nullable|required_with:filling_date',
            'filling_date' => 'nullable|date|date_format:Y-n-j|required_with:fee|before_or_equal:hearing_date',
            'status' => 'required',
            'category' => 'required',
        ];
        if (request()->input('status') == '2')
            $rules['status_description'] = 'required';

        return $rules;
    }

}