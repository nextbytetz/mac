<?php

namespace App\Http\Requests\Backend\Legal;

use App\Http\Requests\Request;

class StoreCaseMentionRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules = [
            'case_personnel_id' => 'required',
            'firstname' => 'required|alpha_spaces',
            'middlename' => 'nullable|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'mention_date' => 'required|date|date_format:Y-n-j',
            'mention_time' => 'required',
            'lawyer_id' => 'required',
            'category' => 'required',
        ];

        return $rules;
    }

}