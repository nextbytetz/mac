<?php

namespace App\Http\Requests\Backend\Legal;

use App\Http\Requests\Request;

/**
 * Class UpdateRoleRequest
 * @package App\Http\Requests\Backend\Access\Role
 */
class StoreLawyerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules = [
            'external_id' => 'nullable|alpha_dash|unique:lawyers',
            'category' => 'required',
        ];
        if (request()->input("category") == 0)
        {
            //individual lawyer request
            $rules['firstname'] = 'required|alpha_spaces';
            $rules['middlename'] = 'nullable|alpha_spaces';
            $rules['lastname'] = 'required|alpha_spaces';
        }
        if (request()->input("category") == 1)
        {
            //Firm lawyer request
            $rules['employer_id'] = 'required';
        }
        return $rules;
    }
}