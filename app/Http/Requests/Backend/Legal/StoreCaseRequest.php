<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 18-May-2017
 * Time: 19:19
 */

namespace app\Http\Requests\Backend\Legal;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;


class StoreCaseRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $basic = [
            'title' => 'required',
            'number' => 'required|unique:cases',
            'complainant' => 'required',
            'district_id' => 'required',
            'location' => 'nullable',
            'fee' => 'nullable|required_with:filling_date',
            'respondent' => 'required',
            'court_category_id' => 'required',
            'court' => 'required',
            'complainant_address' => 'nullable',
            'respondent_address' => 'nullable',
            'status' => 'required',
            'filling_date' => 'nullable|date|date_format:Y-n-j|required_with:fee',
            'fact' => 'nullable',
            'liability_type' => 'required',
        ];
        if (request()->input('liability_type') == 1) {
            $basic['liability'] = 'required';
            $basic['contentious_type'] = 'required';
        } else {
            $basic['liability'] = 'nullable';
            $basic['contentious_type'] = 'nullable';
        }

        return $basic;
    }

}