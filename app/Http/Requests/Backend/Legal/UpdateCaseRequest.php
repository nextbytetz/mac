<?php

namespace App\Http\Requests\Backend\Legal;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UpdateCaseRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $basic = [
            'title' => 'required',
            'number' => [
                'required',
                Rule::unique('cases')->where(function ($query) {
                    $query->where('id', '<>', request()->input('id'));
                }),
            ],
            'complainant' => 'required',
            'district_id' => 'required',
            'location' => 'nullable',
            'fee' => 'nullable|required_with:filling_date',
            'respondent' => 'required',
            'court_category_id' => 'required',
            'court' => 'required',
            'complainant_address' => 'nullable',
            'respondent_address' => 'nullable',
            'status' => 'required',
            'filling_date' => 'nullable|date|required_with:fee',
            'fact' => 'nullable',
            'liability_type' => 'required',
        ];
        if (request()->input('liability_type') == 1) {
            $basic['liability'] = 'required';
            $basic['contentious_type'] = 'required';
        } else {
            $basic['liability'] = 'nullable';
            $basic['contentious_type'] = 'nullable';
        }

        return $basic;
    }

}