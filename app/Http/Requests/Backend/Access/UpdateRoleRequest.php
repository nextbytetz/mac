<?php

namespace App\Http\Requests\Backend\Access;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class UpdateRoleRequest
 * @package App\Http\Requests\Backend\Access\Role
 */
class UpdateRoleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:3',
                Rule::unique('roles')->where(function ($query) {
                    $query->where('id', '<>', request()->input('role_id'));
                }),
            ],
            'description' => 'nullable|min:5',
        ];
    }
}