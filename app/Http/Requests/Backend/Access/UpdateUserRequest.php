<?php

namespace App\Http\Requests\Backend\Access;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use App\Repositories\Backend\Access\UserRepository;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $basic = [

        ];
        if (true) {
            $opt = [
                'email' => [
                    'nullable',
                    'email',
                    Rule::unique('users')->where(function ($query) {
                        $query->where('id', '<>', request()->input('user_id'));
                    }),
                ],
                'unit_id' => 'required',
                'designation_id' => 'required',
                'office_id' => 'required',
                'external_id' => 'nullable',
                'phone' => [
                    'nullable',
                    'phone:TZ',
                    Rule::unique('users')->where(function ($query) {
                        $query->where('id', '<>', request()->input('user_id'));
                    }),
                ],
            ];
            $basic = array_merge($basic, $opt);
        }
        return $basic;
    }
}