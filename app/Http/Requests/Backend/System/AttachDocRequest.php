<?php

namespace App\Http\Requests\Backend\System;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class AttachDocRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $array = [];
        $optional = [];

        /*Date reference*/
        if(array_key_exists('date_reference', $input))
        {
            $array =[
                'date_reference' => 'nullable',
            ];
            $optional = array_merge($optional, $array);
        }

        if(array_key_exists('document_id', $input))
        {
            $array =[
                'document_id' => 'nullable',
            ];
            $optional = array_merge($optional, $array);
        }

        $basic =  [

            'document_file' => [
                'required',
                'file',
                'mimetypes:application/pdf',
            ],
        ];
        if ($input['isother']) {
            $basic['document_title'] = 'required';
        } else {
            $basic['document_title'] = 'nullable';
        }
        return array_merge($optional, $basic);
    }


    public function messages()
    {
        return [
            'document_file.mimetypes' => 'Only PDF files allowed for upload',
            'document_file.file' => 'Upload a valid file',
        ];
    }


    public function sanitize()
    {
        $input = $this->all();
        $input['date_reference'] = isset($input['date_reference']) ? standard_date_format( $input['date_reference']) : null;
        $this->replace($input);
        return $this->all();
    }
}
