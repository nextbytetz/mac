<?php

namespace App\Http\Requests\Backend\System;

use App\Http\Requests\Request;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class UpdateWorkflowRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $basic = [];
        $input = $this->all();
        if (request()->input("assigned") == 1) {
            $basic = [
                'status' => 'required',
                'comments' => 'required_if:status,2,3,4',
                'level' => 'required_if:status,2',
            ];
            if ($input["isselective"] && $input['is_optional']) {
                $basic['wf_definition'] = 'required_if:status,1';
            }
            if ($input["isselectuser"]) {
                $basic['select_user'] = 'required_if:status,1';
            }
        }
        return $basic;
    }

    public function messages()
    {
        return [
            'comments.required_if' => 'Please enter your remarks to the forwarded person',
            'level.required_if' => 'Please select level to reverse',
            'wf_definition.required_if' => 'Please choose the level to forward',
        ];
    }

}