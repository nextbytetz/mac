<?php

namespace App\Http\Requests\Backend\System;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArchiveWorkflowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'from_date' => 'required|date|after_or_equal:wf_date',
            'to_date' => 'required|after_or_equal:from_date',
            'archive_reason_cv_id' => 'required',
            'instore' => 'required',
            'receiver_user' => 'required_if:instore,1',
        ];
    }
}
