<?php

namespace App\Http\Requests\Backend\Notification;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateLetterDispatchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'letter_date' => 'required',
            'reference' => 'required',
            'dispatch_number' => 'nullable',
            'delivery_date' => 'nullable',
            'received_by' => 'nullable',
            'receiver_phone_number' => 'nullable',
        ];
    }

}
