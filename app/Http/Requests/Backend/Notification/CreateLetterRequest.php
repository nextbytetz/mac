<?php

namespace App\Http\Requests\Backend\Notification;

use App\Http\Requests\Request;
//use Illuminate\Foundation\Http\FormRequest;

class CreateLetterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "reference" => "required",
            "box_no" => "required",
            "salutation" => "required",
            "location" => "required",
            "gender_id" => "nullable",
            "option" => "nullable",
        ];
    }
}
