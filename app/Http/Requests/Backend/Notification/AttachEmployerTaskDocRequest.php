<?php

namespace App\Http\Requests\Backend\Notification;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class AttachEmployerTaskDocRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $basic =  [
            'document_id' => 'required',
            'date_reference' => 'nullable',
            'document_file' => [
                'required',
                'file',
                'mimetypes:application/pdf',
            ],
        ];
        if($input['document_id']  == 79){
            $basic['document_title'] = 'required';
        } else {
            $basic['document_title'] = 'nullable';
        }
        return $basic;
    }


    public function messages()
    {
        return [
            'document_file.mimetypes' => 'Only PDF files allowed for upload',
            'document_file.file' => 'Upload a valid file',
        ];
    }

}
