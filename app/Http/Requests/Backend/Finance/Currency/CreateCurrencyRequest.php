<?php

namespace App\Http\Requests\Backend\Finance\Currency;

use App\Http\Requests\Request;

/**
 * Class UpdateUserPasswordRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class CreateCurrencyRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $basic = [
            'name' => 'required',
            'code' => 'required|min:1|max:3',
            'exchange_rate' => 'required|numeric',
        ];

        return $basic;
    }

}
