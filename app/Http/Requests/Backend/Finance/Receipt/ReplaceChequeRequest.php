<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-May-17
 * Time: 9:05 AM
 */

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;

class ReplaceChequeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $basic = [
            'new_chequeno' => 'required',
            'bank' => 'required',
        ];

        return $basic;


    }

}