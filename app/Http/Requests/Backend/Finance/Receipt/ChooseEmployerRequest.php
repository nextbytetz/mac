<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;

/**
 * Class ChooseEmployerRequest
 * @package App\Http\Requests\Backend\Finance\Receipt
 */
class ChooseEmployerRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'employer' => 'required',
        ];
    }
}