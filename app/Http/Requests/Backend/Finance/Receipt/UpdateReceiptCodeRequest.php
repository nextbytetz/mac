<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;

class UpdateReceiptCodeRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'rct_date' => 'required|date|date_format:Y-n-j|before:threshold_date',
        ];
    }

}