<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SupplierFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    return [
    'supplier_name'=> 'required',
    'supplier_id'=> 'required',
    'vendor_site_code'=> 'required',
    'address_line1'=> 'required',
    'city' => 'required',
    'liability_account'=> 'required'
        ];
    }
}
