<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;

/**
 * Class UpdateUserPasswordRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class DishonourReceiptRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $basic = [
            'dishonour_reason' => 'required',
            'dishonour_supporting_document' => [
                'required',
                'file',
                'mimetypes:application/pdf,image/png,image/tiff,image/x-tiff,image/bmp,image/x-windows-bmp,image/gif,image/x-icon,image/jpeg,image/pjpeg,image/x-portable-bitmap,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.wordprocessingml.template,application/vnd.ms-word.document.macroEnabled.12,application/vnd.ms-word.template.macroEnabled.12',
            ],
        ];

        return $basic;


    }

}
