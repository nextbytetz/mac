<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Factory as ValidationFactory;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;

class StoreEmployerReceiptRequest extends Request
{

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null, ValidationFactory $validationFactory)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $validationFactory->extend(
            'amount_duplicate',
            function ($attribute, $amount, $parameters) {
                $return = true;
                $receipt = new ReceiptRepository();
                $input = request()->all();
                $clause = [];
                foreach ($input as $key => $value) {
                    if (strpos($key, 'contribution_amount') !== false) {
                        $id = substr($key, 19);
                        if (isset($input['contribution_select'. $id]) And !empty($value)) {
                            $contribution_month = $input['contribution_month' . $id];
                            $contribution_year = $input['contribution_year' . $id];
                            $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                            $value = str_replace(",", "", $value);
                            $clause[] = ['fin_code_id' => 2, 'amount' => $value, 'contrib_month' => $date];
                        }
                    }
                    if (strpos($key, 'interest_amount') !== false) {
                        $id = substr($key, 15);
                        if (isset($input['interest_select'. $id]) And !empty($value)) {
                            $contribution_month = $input['contribution_month' . $id];
                            $contribution_year = $input['contribution_year' . $id];
                            $date = $contribution_year . '-' . str_pad($contribution_month, 2, "0", STR_PAD_LEFT) . '-28';
                            $value = str_replace(",", "", $value);
                            $clause[] = ['fin_code_id' => 1, 'amount' => $value, 'contrib_month' => $date];
                        }
                    }
                }
                //$check = $receipt->query()->where(["employer_id" => $parameters[1], "iscancelled" => 0, "rct_date" => $parameters[0], "amount" => $amount])->whereHas("receiptCodes", function ($query) use ($clause) {
                $check = $receipt->query()->where(["employer_id" => $parameters[1], "iscancelled" => 0, "rct_date" => $parameters[0], "amount" => $amount])->whereHas("receiptCodes", function ($query) use ($clause) {
                    $query->where(function ($query) use ($clause) {
                        foreach ($clause as $value) {
                            $query->orWhere(function ($query) use ($value) {
                                $query->where($value);
                            });
                        }
                    });
                })->count();
                if ($check) {
                    $return = false;
                }
                return $return;
            },
            ''
        );
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $rct_date = request()->input('rct_date');
        $reference = request()->input('bank_reference');
        $bank_id = request()->input('bank_id');
        $employer_id = request()->input('employer_id');

        $basic = [
            //'amount' => 'required',
            'payroll_id' => 'sometimes|required',
            'bank_id' => 'required',
            'fin_code_id' => 'required',
            'currency_id' => 'required',
            'payment_type_id' => 'required',
            'pay_months' => 'required',
            'bank_reference' => [
                'nullable',
                'alpha_dash',
                'min:5',
                Rule::unique('receipts')->where(function ($query) use ($rct_date, $bank_id) {
                    $query->where("rct_date", $rct_date)->where("bank_id", $bank_id);
                }),
            ],
        ];
        if (request()->input('payment_type_id') == 2 Or request()->input('payment_type_id') == 5) {
            $basic['chequeno'] = [
                'required',
                'alpha_dash',
                Rule::unique('receipts')->where(function ($query) use ($rct_date, $employer_id) {
                    $query->where("rct_date", $rct_date)->where(["employer_id" => $employer_id, "iscancelled" => 0]);
                }),
            ];
            $basic['rct_date'] = 'required|date|date_format:Y-n-j|before_or_equal:today_date';
            $basic['amount'] = 'required';
        } else {
            $basic['chequeno'] = 'nullable';
            $basic['rct_date'] = 'required|date|date_format:Y-n-j|before_or_equal:today_date|after_or_equal:start_date';
            $basic['amount'] = [
                'required',
                "amount_duplicate:'{$rct_date}',{$employer_id}",
                /*Rule::unique('receipts')->where(function ($query) use ($rct_date, $employer_id) {
                    $query->where("rct_date", $rct_date)->where(["employer_id" => $employer_id, "iscancelled" => 0]);
                }),*/
            ];
        }
        //$basic = array_merge($basic, $opt);
        $input = request()->all();
        foreach ($input as $key => $value) {
            if (strpos($key, 'contribution_amount') !== false) {
                $id = substr($key, 19);
                if (isset($input['contribution_select'. $id])) {
                    $basic[$key] = [
                        'required',
                    ];
                }
            }
            if (strpos($key, 'member_count') !== false) {
                $id = substr($key, 12);
                if (isset($input['contribution_select'. $id])) {
                    $basic[$key] = [
                        'required',
                        'integer',
                        'min:1',
                    ];
                }
            }
            if (strpos($key, 'interest_amount') !== false) {
                $id = substr($key, 15);
                if (isset($input['interest_select'. $id])) {
                    $basic[$key] = [
                        'required',
                    ];
                }
            }
            if (strpos($key, 'contribution_month') !== false) {
                $basic[$key] = [
                    'required',
                ];
            }
            if (strpos($key, 'contribution_year') !== false) {
                $basic[$key] = [
                    'required',
                ];
            }
        }
        return $basic;
    }

    public function messages()
    {
        $messages = [];
        $input = request()->all();
        foreach ($input as $key => $value) {
            if (strpos($key, 'contribution_amount') !== false) {
                $messages[$key . '.required'] = 'Contribution amount is required';
            }
            if (strpos($key, 'member_count') !== false) {
                $messages[$key. '.required'] = 'Member count is required';
            }
        }
        $messages['amount.amount_duplicate'] = "This amount for this employer for the same payment date and contribution month has already been recorded";
        return $messages;
        //return parent::messages(); // TODO: Change the autogenerated stub
    }

    public function sanitize()
    {
        $input = array_map('trim', $this->all());

        if (request()->input('payment_type_id') == 2 Or request()->input('payment_type_id') == 5) {
            $input['chequeno'] = strtoupper($input['chequeno']);
        }
        $input['amount'] = str_replace(",", "", $input['amount']);

        $this->replace($input);

        return $this->all();

    }

}