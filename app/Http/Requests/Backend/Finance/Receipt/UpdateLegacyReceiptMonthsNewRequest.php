<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UpdateLegacyReceiptMonthsNewRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $basic = [];
        $input = request()->all();
        $legacy_receipt_id = $input['legacy_receipt_id'];
        $rctno = request()->input('rctno');
        $basic = [
            'amount' => 'required',
            'currency_id' => 'required',
            'payment_type_id' => 'required',
//            'pay_months' => 'required',
            'capture_date' => 'required|date|date_format:Y-n-j|after_or_equal:rct_date',
            'rctno' => ['required','min:1',
                Rule::unique('legacy_receipts')->where(function ($query) use ($rctno, $legacy_receipt_id) {
                    $query->where("rctno", $rctno)->where('id', '<>', $legacy_receipt_id);
                }),],
        ];



        if($input['isnew'] == 0) {

            /*when is editing existing months entries*/
            foreach ($input as $key => $value) {

                if (strpos($key, 'contribution_amount') !== false) {
                    $id = substr($key, 19);
                    $basic[$key] = [
                        'required',
                    ];
                }
                if (strpos($key, 'member_count') !== false) {
                    $id = substr($key, 12);
                    $basic[$key] = [
                        'required',
                        'integer',
                    ];
                }
                if (strpos($key, 'interest_amount') !== false) {
                    $id = substr($key, 15);
                    $basic[$key] = [
                        'required',
                    ];
                }


            }

        }else{
            /*when adding from start (new)*/
            foreach ($input as $key => $value) {
                if (strpos($key, 'contribution_amount') !== false) {
                    $id = substr($key, 19);
                    if (isset($input['contribution_select'. $id])) {
                        $basic[$key] = [
                            'required',
                        ];
                    }
                }
                if (strpos($key, 'member_count') !== false) {
                    $id = substr($key, 12);
                    if (isset($input['contribution_select'. $id])) {
                        $basic[$key] = [
                            'required',
                            'integer',
                        ];
                    }
                }
                if (strpos($key, 'interest_amount') !== false) {
                    $id = substr($key, 15);
                    if (isset($input['interest_select'. $id])) {
                        $basic[$key] = [
                            'required',
                        ];
                    }
                }
                if (strpos($key, 'contribution_month') !== false) {
                    $basic[$key] = [
                        'required',
                    ];
                }
                if (strpos($key, 'contribution_year') !== false) {
                    $basic[$key] = [
                        'required',
                    ];
                }
            }
        }

        return $basic;
    }

}