<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class StoreAdministrativeReceiptRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $opt = [];
        if (request()->input('payment_type_id') == 2 Or request()->input('payment_type_id') == 5) {
            $opt['chequeno'] = 'required|alpha_dash';
        } else {
            $opt['chequeno'] = 'nullable';
        }
        $rct_date = request()->input('rct_date');
        $reference = request()->input('bank_reference');
        $bank_id = request()->input('bank_id');
        $basic = [
            'amount' => 'required',
            'bank_id' => 'required',
            'bank_code' => 'required',
            'currency_id' => 'required',
            'payment_type_id' => 'required',
            'fin_code_id' => 'required',
            'category' => 'required',
            'rct_date' => 'required|date|date_format:Y-n-j|before_or_equal:today_date',
            'description' => 'nullable',
            'bank_reference' => [
                'nullable',
                'alpha_dash',
                'min:5',
                Rule::unique('receipts')->where(function ($query) use ($rct_date, $reference, $bank_id) {
                    $query->where("rct_date", $rct_date)->where("bank_id", $bank_id);
                }),
            ],
        ];
        $basic = array_merge($basic, $opt);

        if (request()->input("category") == 0)
        {
            //staff payer request
            $basic['staff_id'] = 'required';
        }
        if (request()->input("category") == 1)
        {
            //firm payer request
            $basic['thirdparty_id'] = 'required';
        }
        return $basic;
    }

}