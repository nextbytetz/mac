<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UpdateReceiptMonthsRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $basic = [];
        $input = request()->all();
        foreach ($input as $key => $value) {
            if (strpos($key, 'contribution_amount') !== false) {
                $id = substr($key, 19);
                $basic[$key] = [
                    'required',
                ];
            }
            if (strpos($key, 'member_count') !== false) {
                $id = substr($key, 12);
                $basic[$key] = [
                    'required',
                    'integer',
                ];
            }
            if (strpos($key, 'interest_amount') !== false) {
                $id = substr($key, 15);
                $basic[$key] = [
                    'required',
                ];
            }
        }
        return $basic;
    }

}