<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;

/**
 * Class
 * * @package App\Http\Requests\Backend\Access\User
 */
class StoreLinkedFileRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $basic = [
        'linked_file' => 'required',
        ];

        return $basic;


    }

}
