<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UpdateThirdpartyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $input = [];
        $rules = [
            'external_id' => [
                'nullable',
                'alpha_dash',
                Rule::unique('thirdparties')->where(function ($query) {
                    $query->where('id', '<>', request()->input('thirdparty_id'));
                }),
            ],
            'category' => 'required',
        ];
        if (request()->input("category") == 0)
        {
            //individual third party payer request
            $rules['firstname'] = 'required|alpha_spaces';
            $rules['middlename'] = 'nullable|alpha_spaces';
            $rules['lastname'] = 'required|alpha_spaces';
        }
        if (request()->input("category") == 1)
        {
            //Firm third party payer request
            $rules['employer_id'] = [
                'required',
                Rule::unique('thirdparties')->where(function ($query) {
                    $query->where('id', '<>', request()->input('thirdparty_id'));
                }),
            ];
            $rules['firstname'] = 'nullable';
            $rules['middlename'] = 'nullable';
            $rules['lastname'] = 'nullable';
        }
        return $rules;
    }
}
