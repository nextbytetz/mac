<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UpdateLegacyReceiptMonthsRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $basic = [];
        $input = request()->all();
        foreach ($input as $key => $value) {
            if (strpos($key, 'contribution_amount') !== false) {
                $id = substr($key, 19);
                $basic[$key] = [
                    'required',
                ];
            }
            if (strpos($key, 'member_count') !== false) {
                $id = substr($key, 12);
                $basic[$key] = [
                    'required',
                    'integer',
                ];
            }
            if (strpos($key, 'interest_amount') !== false) {
                $id = substr($key, 15);
                $basic[$key] = [
                    'required',
                ];
            }
        }

        $rctno = request()->input('rctno');
        $rct_date = request()->input('rct_date');
        $employer_id = request()->input('employer_id');
        $legacy_receipt_id = request()->input('legacy_receipt_id');
        $basic = [
            'amount' => 'required',
            'currency_id' => 'required',
            'payment_type_id' => 'required',
            'capture_date' => 'required|date|date_format:Y-n-j|before:start_date|after_or_equal:rct_date',
            'rctno' => ['required','min:1',
                Rule::unique('legacy_receipts')->where(function ($query) use ($legacy_receipt_id) {
                    $query->where("id",'<>',  $legacy_receipt_id);
                }),],
        ];
        if (request()->input('payment_type_id') == 2  || request()->input('payment_type_id') == 5) {
            $basic['chequeno'] = [
                'required',
                'alpha_dash',
                Rule::unique('receipts')->where(function ($query) use ($rct_date, $employer_id) {
                    $query->where("rct_date", $rct_date)->where(["employer_id" => $employer_id, "iscancelled" => 0]);
                }),
            ];
            $basic['rct_date'] = 'required|date|date_format:Y-n-j|before_or_equal:today_date';

        } else {
            $basic['chequeno'] = 'nullable';
            $basic['rct_date'] = 'required|date|date_format:Y-n-j|before_or_equal:today_date|before:start_date';
        }

        return $basic;
    }

}