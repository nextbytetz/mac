<?php

namespace App\Http\Requests\Backend\Finance\Receipt;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreThirdpartyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules = [
            'external_id' => 'nullable|alpha_dash|unique:thirdparties',
            'category' => 'required',
        ];
        if (request()->input("category") == 0)
        {
            //individual third party payer request
            $rules['firstname'] = 'required|alpha_spaces';
            $rules['middlename'] = 'nullable|alpha_spaces';
            $rules['lastname'] = 'required|alpha_spaces';
        }
        if (request()->input("category") == 1)
        {
            //Firm third party payer request
            $rules['employer_id'] = 'required';
        }
        return $rules;
    }
}
