<?php

namespace App\Http\Middleware;

use Closure;

class RouteNeedsAssignedForNotification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*Find the possible implementation which can be satisfied by middleware*/
        if (true) {
            return redirect()
                ->back()
                ->withFlashDanger("Action cancelled! Only allocated user can perform the requested action...");
        }
        return $next($request);
    }
}
