<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class MySubstitute
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @param null $guard
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (!Auth::guard($guard)->user()->available) {
                if ($request->ajax()) {
                    return response("You are not allowed to perform any action, revoke your substitution to proceed", 405);
                } else {
                    return redirect()->route("backend.blank")->withFlashSuccess("You are not allowed to perform any action, to proceed you need to revoke your substitution");
                }
            }
        }

        return $next($request);
    }
}
