<?php

namespace App\Jobs;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Sysdef\GeneralRectificationRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Services\Compliance\StaffEmployerAllocation\AllocateNonLargeEmployers;
use App\Services\Scopes\IsactiveScope;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exceptions\JobException;

/*Job: For general rectification of any issue raised on existing features*/
class GeneralRectificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */


    protected $chunkSize = 100;



    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $repo = new GeneralRectificationRepository();

        //TODO <general_rectification> Interests raised but booking paid on time
//        $repo->rectifyInterestWhichBookingsWerePaidOnTime();



       //TODO<GENERAL rectifier> deactivate wf tracks for undone employer particular changes: Remove after run
//        $repo->rectifyEmployerParticularChangeUndoneButWfTrackNotDeactivated();

    }




}
