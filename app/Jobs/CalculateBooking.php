<?php

namespace App\Jobs;

use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Receivable\CreateBooking;

class CalculateBooking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $employers;

    protected $rcv_date;


    /**
     * CalculateBooking constructor.
     * @param $employers
     * @param null $rcv_date
     */
    public function __construct($employers, $rcv_date = null)
    {
        $this->employers = $employers;
        $this->rcv_date = $rcv_date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $booking = new CreateBooking();
        /*100 employers chunksize*/
        $employers = $this->employers;
        foreach($employers as $employer) {
            if (!$employer->employer_closure_id) {
                $booking->createBooking($employer->id, $this->rcv_date,null, null,null, $is_schedule  = 1);
            }
        }


    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}




//public function handle(EmployerRepository $employerRepository)
//{
////        $fin_code_groups = new FinCodeGroupRepository();
////        $booking = new CreateBooking();
////        $employerRepository->query()->whereHas('receiptCodes', function($query) use($fin_code_groups) {
////            $query->whereHas('finCode', function($query) use($fin_code_groups){ $query->where('fin_code_group_id','=',  $fin_code_groups->getContributionFinCodeGroupId());
////            })->whereHas('receipt', function($query) {
////                $query->where('iscancelled',0)->where('isdishonoured',0);
////            });
////        })->select(['id'])->chunk(100, function ($employers) use ($booking) {
////            foreach ($employers as $employer) {
////                $booking->createBooking($employer->id);
////            }
////        });
//
//
//    $employers = $this->employers;
//    foreach($employers as $employer){
//        $booking->createBooking($employer->id);
//    }
//
//
//}