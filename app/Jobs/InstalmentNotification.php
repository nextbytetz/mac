<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\InstalmentReminderNotification;
use DB;

class InstalmentNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        dump("in dispatch");
              /*loop in all instalments and chack the dates then minus five days 
              if match the day of today send email */

                $instalments = DB::table('portal.installment_payments')->select('installment_payments.due_date','installment_payments.contribution_arrears_id','contribution_arrears.contrib_month','installment_payments.employer_id','employers.name as employer_name','employers.email as employer_email','users.name as user_name','users.email as user_email','contribution_arrears.arrear_amount','installment_requests.user_id')
            ->join('portal.installment_requests','installment_requests.id','=','installment_payments.installment_request_id')
            ->join('main.employers','employers.id','=','installment_requests.employer_id')
            ->join('portal.users','users.id','=','installment_requests.user_id')
            ->join('portal.contribution_arrears','contribution_arrears.id','=','installment_payments.contribution_arrears_id')
            ->get();

            $today = Carbon::now()->format('Y-m-d');

            foreach ($instalments as $instalment) {
                $day = Carbon::parse($instalment->due_date)->subDays(5)->format('Y-m-d');
                dump($day);
                if ($today != $day) {
                    /* send notification */
                    $emails = array();
                    $employer = array();
                    $emp = DB::table('main.employers')->where('id',$instalment->employer_id)->get()->toArray();
                    $usr = DB::table('portal.users')->where('id',$instalment->user_id)->get()->toArray();
                    $employer['email'] = $emp;
                    $employer['name'] = $instalment->employer_name;

                    $user = array();
                    $user['email'] = $usr;
                    $user['name'] = $instalment->user_name;

                    $emails[0] = $employer;
                    $emails[1] = $user;

                    foreach ($emails as $key => $email) {
                        $this->sendInstalmenetReminderEmail($email['email'],$email['name'],$instalment->contrib_month,$instalment->arrear_amount,$instalment->due_date,$instalment->employer_id);
                    }

                }

            }
        }

        public function sendInstalmenetReminderEmail($email, $name, $contrib_month, $arrear_amount, $due_date, $employer_id){
            $email = json_decode(json_encode($email), FALSE);
            dump($email);
            try {
                \Notification::send($email, new InstalmentReminderNotification($name,$contrib_month,$arrear_amount, $due_date, $employer_id));
            } catch (Exception $e) {
                logger($e->getMessage());
            }
            dump("sent");
        }
    }
