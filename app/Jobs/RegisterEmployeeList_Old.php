<?php

namespace App\Jobs;

use App\Models\Operation\Compliance\Member\Employer;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeTempRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;

class RegisterEmployeeList_Old implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    protected $employer;

    protected $user_id;


//    public $tries = 15;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Employer $employer, $user_id)
    {
        //
        $this->employer = $employer;
        $this->user_id = $user_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

////
        $employer = $this->employer;
        $employer_id = $employer->id;
        $user_id = $this->user_id;
        $employee = new EmployeeRepository();
        $employeeTempRepo = new EmployeeTempRepository();
        $error = $employer->employeeTemps()->where('error', 1)->first();
        if (!$error) {
            /* Select all employee for this registered employer */
            $employer->employeeTemps()->select(['id', 'firstname', 'middlename', 'lastname', 'basicpay', 'grosspay', 'dob', 'gender', 'job_title' , 'employment_category'])->where(['error' => 0, 'employer_id' => $employer->id])->chunk(100, function ($employeeTemps) use ($employer, $employee,$user_id, $employeeTempRepo, $employer_id) {
//            $employeeTemps =  $employer->employeeTemps;
                foreach ($employeeTemps as $employeeTemp) {
                    /* Loop through all employee temps for the employee and insert in the employee table */
                    $firstname = strtolower($employeeTemp->firstname);
                    $lastname = strtolower($employeeTemp->lastname);
                    $dob = $employeeTemp->dob;
                    $check = $employee->query()->whereRaw("levenshtein(lower(firstname), :firstname) between 0 and 1 and levenshtein(lower(lastname), :lastname) between 0 and 1 and dob = :dob", ['firstname' => $firstname, 'lastname' => $lastname, 'dob' => $dob])->first();
//                    $check = NULL;
                    if (!$check) {
                        /**/
                        $gender_id = null;
                        if (in_array($employeeTemp->gender,male_gender())) {
                            $gender_id = 1;
                        } elseif(in_array($employeeTemp->gender,female_gender())) {
                            $gender_id = 2;
                        }

                        /* Save Employee Data */
                        $data = ['firstname' => $employeeTemp->firstname,'middlename' => $employeeTemp->middlename, 'lastname' => $employeeTemp->lastname, 'dob' => $employeeTemp->dob, 'gender_id' => $gender_id, 'sex' => $employeeTemp->gender, 'memberno' => 0,'emp_cate' => $employeeTemp->employment_category, 'created_at' => Carbon::now(), 'user_id' => $user_id];
                        // new employee, create first
                        $registeredEmployee = $employee->query()->create($data);

                        /* to change member_no name index in the new mac operation */
                        $employee_id = $registeredEmployee->id;
                        $registeredEmployee->memberno = checksum($employee_id, sysdefs()->data()->employee_number_length);
                            $registeredEmployee->save();


                    } else {
                        // existing employee
                        $registeredEmployee = $check;
                        /*remove from temp*/
                    }
                    /* Attach to employers */
                    $registeredEmployee->employers()->syncWithoutDetaching([$employer->id => ['basicpay'=> $employeeTemp->basicpay,'grosspay' => $employeeTemp->grosspay, 'job_title' => $employeeTemp->job_title   ]]);

                    /*remove from temp*/
                    $employeeTempRepo->destroy($employeeTemp->id);
                    /*rectify memberno*/
                    $employee->rectifyMemberno($employer_id);
                }

            });


        }

    }
}






////        $linked_file = $this->linked_file;
//$employer =  $this->employer;
//$employer_id =  $this->employer->id;
//$linked_file = $employer->employeeFile();
//$user_id = $this->user_id;
//$user_id = $this->user_id;
//$employee = new EmployeeRepository();
//
//
//$pivot = $employer->documents()->where("document_id", 37)->first()->pivot;
//$error = $employer->employeeTemps()->where('error', 1)->first();
///*update rows imported*/
//if (!count($error)) {
//
//    /** start : Uploading excel data into the database */
//    Excel::filter('chunk')
//        ->selectSheetsByIndex(0)
//        ->load($linked_file)
//        ->chunk(100, function ($result) use ($employer_id, $user_id, $employee) {
//            $rows = $result->toArray();
//            //let's do more processing (change values in cells) here as needed
//            //$counter = 0;
//            foreach ($rows as $row) {
//                $row['dob'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['dob'], 'YYYY-MM-DD');
//                $firstname = strtolower($row['firstname']);
//                $lastname = strtolower($row['lastname']);
//                $row['gender'] = strtoupper($row['gender']);
//                $dob = $row['dob'];
//                $check = $employee->query()->whereRaw("levenshtein(lower(firstname), :firstname) between 0 and 1 and levenshtein(lower(lastname), :lastname) between 0 and 1 and dob = :dob", ['firstname' => $firstname, 'lastname' => $lastname, 'dob' => $dob])->first();
//
//                if (!count($check)) {
//                    /**/
//                    $gender_id = null;
//                    $male_gender = ['MALE', 'M', 'ME', 'MWANAUME', 'MVULANA', 'MME' ];
//                    $female_gender =['FEMALE', 'F', 'KE', 'MWANAMKE', 'MSICHANA', 'MKE' ];
//                    if (in_array(trim($row['gender']),$male_gender)) {
//                        $gender_id = 1;
//                    } elseif(in_array(trim($row['gender']),$female_gender)) {
//                        $gender_id = 2;
//                    }
//
//                    /* Save Employee Data */
//                    $data = ['firstname' => $row['firstname'],'middlename' => $row['middlename'], 'lastname' => $row['lastname'], 'dob' => $row['dob'], 'gender_id' => $gender_id, 'sex' => $row['gender'], 'memberno' => 0,'emp_cate' => $row['employment_category'], 'created_at' => Carbon::now(), 'user_id' => $user_id];
//                    // new employee, create first
//                    $registeredEmployee = $employee->query()->create($data);
//
//                    /* to change member_no name index in the new mac operation */
//                    $employee_id = $registeredEmployee->id;
////                        $registeredEmployee->memberno = checksum($employee_id, sysdefs()->data()->employee_number_length);
//                    $registeredEmployee->memberno = checksum($employee_id, 8);
//                    $registeredEmployee->save();
//
//
//                } else {
//                    // existing employee
//                    $registeredEmployee = $check;
//                    /*remove from temp*/
//                }
//                /* Attach to employers */
//                $grosspay = $row['basicpay'] + $row['allowance'];
//                $registeredEmployee->employers()->syncWithoutDetaching([$employer_id => ['basicpay'=> $row['basicpay'],'grosspay' => $grosspay,'job_title' =>  $row['job_title']  ]]);
//
//                /*remove from temp*/
//
//                /*rectify memberno*/
////                        $employee->rectifyMemberno($employer_id);
//            }
//
//
//
//
//        }, true);
//}
//



