<?php

namespace App\Jobs\Compliance;


use App\Jobs\GeneralRectificationJob;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Services\Compliance\UpdateEmployerStatus;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Receivable\CalculateInterest;

class ProcessBookingsForNonContributorsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
//    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 3550;

    protected $employers;
    /**
     * Create a new job instance.
     *
     * PostInterest constructor.
     */
    public function __construct()
    {
//        $this->employers = $employers;
    }

    /**
     * Execute the job.
     *
     * @param EmployerRepository $employerRepository
     */
    public function handle()
    {
        $post_bookings = new \App\Services\Compliance\PostBookingsForNonContributors();
        $update_not_contributed = new \App\Services\Compliance\UpdateBookingsNotContributed();
        /***-------START LIVE BOOKINGS AND INTERESTS---------*/


        /*Delete bookings of treasury employers - public*/
        $update_not_contributed->deleteLiveBookingsOfTreasuryEmployers();

        /*update booking is pag flag <LIVE BOOKING>*/
        $update_not_contributed->updatePaidFlagForContributedBookings();

        /*Delete duplicates bookings from live booking*/
        $update_not_contributed->deleteDuplicateBookingEntries();
        /*end of duplicate entries*/

        /*Delete live booking with no interests*/
        $update_not_contributed->deleteLiveBookingInterestsWithNoBooking();


        /*Sync merged / de-merged employers*/
        $update_not_contributed->rectifyBookingIdOnReceiptCodesFrDuplicateEmployers();
        $update_not_contributed->unRectifyBookingIdOnReceiptCodesFrDuplicateEmployers();
        /*end of merge/de-merge*/

        /*Rectify booking - Receipt codes contrib month mismatch*/
        $update_not_contributed->rectifyBookingIdOnReceiptCodes();
        /*End rectify booking - code*/






        /**---------END OF LIVE BOOKINGS AND INTERESTS--------*/




        /**start - NON BOOKED BOOKINGS & INTERESTS------*/
        /*delete all existing non booked which are not eligible*/
//        $post_bookings->deleteAllBookingsAlreadyLiveBooked();

        /*delete booking interest non booked*/
        /*Delete interest whose booking have been deleted - NON BOOKED BOOKINGS*/
//        $post_bookings->deleteAllInterestWithNoBooking();


        /*destroy booking which have been deleted- NON BOOKED BOOKINGS*/
//        $post_bookings->destroyDeletedBookings();

        /*delete bookings for treasury employers*/
//        $post_bookings->deleteNonBookedBookingsOfTreasuryEmployers();

        /*-----------END OF NON BOOKED BOOKINGS AND INTEREST*/
        /*Reset bookings which have been deleted in non booked bookings*/

        /*post non booked booking for all employers*/
//        (new EmployerRepository())->postBookingForNonContributors();


        /*General Rectification*/
        dispatch(new GeneralRectificationJob());

    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }




    public function oldHandle()
    {
        $post_bookings = new \App\Services\Compliance\PostBookingsForNonContributors();
        /*update all bookings with amount less than minimum amount*/
        //TODO AFTER 1 WEEK REMOVE THIS


        /*RESET FUNCTIONS booking interests*/
        //TODO - RESET - remove this
        (new \App\Services\Compliance\UpdateBookingsNotContributed())->destroyDeletedBookingsWronglyCreated();
        (new \App\Services\Compliance\UpdateBookingsNotContributed())->resetBookingInterestsToNormal();
        (new \App\Services\Compliance\UpdateBookingsNotContributed())->resetInterestAddedToBills();


        /*delete all existing non booked which are not eligible*/
        $post_bookings->deleteAllBookingsAlreadyLiveBooked();


        //TODO: Sync merge / de-merged employers
        /*Sync merged / de-merged employers*/

        /*end of merge/de-merge*/

        /*update booking is pag flag <LIVE BOOKING>*/
        (new \App\Services\Compliance\UpdateBookingsNotContributed())->updatePaidFlagForContributedBookings();

        /*Delete duplicates bookings from live booking*/
        $post_bookings->deleteDuplicateBookingEntries();
        /*end of duplicate entries*/

        /*Delete bookings of treasury employers - public*/
        $post_bookings->deleteLiveBookingsOfTreasuryEmployers();
        $post_bookings->deleteNonBookedBookingsOfTreasuryEmployers();
        /*end of treasury employers*/

        /*delete booking interest non booked*/
        /*Delete interest whose booking have been deleted*/
        $post_bookings->deleteAllInterestWithNoBooking();

        /*Delete live booking with no interests*/
        $post_bookings->deleteLiveBookingInterestsWithNoBooking();

        /*destroy booking which have been deleted*/
        $post_bookings->destroyDeletedBookings();


        /*Reset bookings which have been deleted in non booked bookings*/
        //TODO: need to comment after 1 months
//        $post_bookings->resetDeletedBookingNotInLiveBookings();
        //TODO - RESET  - remove this after it is run
        (new \App\Services\Compliance\UpdateBookingsNotContributed())->destroyBookingInterestsAfterJuly();


        /*post non booked booking for all employers*/
        //TODO: Enabled after resolution on booking and interests
//        (new EmployerRepository())->postBookingForNonContributors();


    }


}










