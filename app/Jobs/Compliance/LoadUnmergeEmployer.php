<?php

namespace App\Jobs\Compliance;

use App\Services\Compliance\MergeDeMergeEmployers;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Exceptions\JobException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LoadUnmergeEmployer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 100;

    protected $user;

    protected $filename;

    /**
     * Create a new job instance.
     *
     * @param $user
     * @param null $filename
     */
    public function __construct($user, $filename = NULL)
    {
        $this->user = $user;
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     *
     * @throws JobException
     * @return void
     */
    public function handle()
    {
        $file = employer_registration_dir() . DIRECTORY_SEPARATOR . 'unmerge_employer' . DIRECTORY_SEPARATOR . $this->filename;
        $user = $this->user;
        logger($file);
        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['oldregno', 'replacedby'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new JobException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */

        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($file)
            ->chunk($this->chunkSize, function($result) use ($user) {
                $rows = $result->toArray();
                foreach ($rows as $row) {
                    $oldregno = single_space($row['oldregno']);
                    $replacedby = single_space($row['replacedby']);

                    /*Demerge employer*/
                    (new MergeDeMergeEmployers())->demergeEmployers($oldregno, $replacedby);

//                    $sql = <<<SQL
//do $$
//declare t record;
//declare other_tables record;
//begin
//    for t IN select column_name, table_name, table_schema
//            from information_schema.columns where column_name = 'employer_id' and table_name not in ('employer_certificate_issues', 'non_booked_bookings', 'bookings') and is_updatable = 'YES' and table_schema in ('portal', 'main')
//
//    loop
//
//         -- Check if column employer_orig (record original employer before merge) exists,
//        if not exists(SELECT 1 FROM information_schema.columns  WHERE table_name=t.table_name and column_name='employer_orig' and table_schema=t.table_schema) then
//            execute 'alter table ' || t.table_schema || '.' || t.table_name || ' add column employer_orig bigint null';
//        end if;
//
//        -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
//        if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then
//            execute 'update ' || t.table_schema || '.' || t.table_name || ' set ' || t.column_name || ' = {$replacedby} where employer_orig = {$replacedby} and employer_id = {$oldregno}';
//        else
//            execute 'update ' || t.table_schema || '.' || t.table_name || ' set ' || t.column_name || ' = {$oldregno} where employer_orig = {$oldregno} and employer_id = {$replacedby}';
//        end if;
//
//
//    end loop;
//
//    -- Check for other tables not using employer_id directly (using resource_id instead and member_type_id) in the {main schema}
//    for other_tables in SELECT UNNEST(ARRAY['payment_voucher_transactions','medical_expenses','claim_compensations','notification_eligible_benefits']) AS table_name
//    loop
//
//        -- Check if column resource_orig (record original resource_id/employer before merge) exists,
//        if not exists(SELECT 1 FROM information_schema.columns  WHERE table_name=other_tables.table_name and column_name='resource_orig' and table_schema='main') then
//            execute 'alter table main.' || other_tables.table_name || ' add column resource_orig bigint null';
//        end if;
//
//        -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
//        if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then
//            execute 'update main.' || other_tables.table_name || ' set resource_id = {$replacedby}  where resource_orig = {$replacedby} and resource_id = {$oldregno}  and member_type_id = 1';
//        else
//            -- update the original resource id in the table
//            execute 'update main.' || other_tables.table_name || ' set resource_id = {$oldregno}  where resource_orig = {$oldregno} and resource_id = {$replacedby}  and member_type_id = 1';
//        end if;
//
//    end loop;
//
//    -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
//    if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then
//        execute 'update employers set duplicate_id = null, deleted_at = null, user_set_duplicate = null where id = {$replacedby}';
//    else
//        execute 'update employers set duplicate_id = null, deleted_at = null, user_set_duplicate = null where id = {$oldregno}';
//    end if;
//
//
//end$$;
//SQL;
//                    DB::unprepared($sql);
                }
            }, true);

    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
