<?php

namespace App\Jobs\Compliance\StaffEmployerAllocation;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Services\Compliance\StaffEmployerAllocation\AllocateBonusEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateLargeEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateNonLargeEmployers;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Exceptions\JobException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/*Process all tasks for staff employer i.e. send reminder email to staff and */

class PostStaffEmployerFollowupReminderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 100;
    protected $users;
    protected $next_run_date;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users, $next_run_date)
    {
        $this->users = $users;
        $this->next_run_date= $next_run_date;
    }

    /**
     * Execute the job.
     *
     * @throws JobException
     * @return void
     */
    public function handle()
    {
        $staff_employer_repo = new StaffEmployerRepository();
        $users = $this->users;
        foreach($users as $user){
            /*Staff followup reminders main update*///
            (new StaffEmployerRepository())->mainPostAndUpdateWeeklyMissingTargetFollowups($user->id, $this->next_run_date);
        }




    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
