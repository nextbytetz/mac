<?php

namespace App\Jobs\Compliance\StaffEmployerAllocation;

use App\Services\Compliance\StaffEmployerAllocation\AllocateBonusEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateLargeEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateNonLargeEmployers;

use App\Services\Compliance\StaffEmployerAllocation\AllocateTreasuryEmployers;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Exceptions\JobException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StaffEmployerAllocationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 100;

    protected $action_type; // i.e 1 => non large employers, 2 => Large employers, 3 => Bonus employers

    protected $staff_employer_allocation_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($staff_employer_allocation_id, $action_type)
    {
        $this->action_type = $action_type;
        $this->staff_employer_allocation_id = $staff_employer_allocation_id;
    }

    /**
     * Execute the job.
     *
     * @throws JobException
     * @return void
     */
    public function handle()
    {
        $action_type = $this->action_type;
        $staff_employer_allocation_id = $this->staff_employer_allocation_id;
        switch($action_type){
            case 1:
                /*Non large employers*/
                $service = new AllocateNonLargeEmployers();
                $service->allocateEmployers($staff_employer_allocation_id);
                break;

            case 2:
                /*Large employers*/
                $service = new AllocateLargeEmployers();
                $service->allocateEmployersUsingRobin($staff_employer_allocation_id);
                break;

            case 3:
                /*Bonus employers -dormant*/
                $service = new AllocateBonusEmployers();
                $service->allocateEmployers($staff_employer_allocation_id);
                break;

            case 4:
                /*Bonus employers - Dormant  - TOP UP monthly*/
                $service = new AllocateBonusEmployers();
                $service->allocateEmployersTopUp($staff_employer_allocation_id);
                break;

            case 5:
                /*Allocate treasury employer*/
                $service = new AllocateTreasuryEmployers();
                $service->allocateEmployers($staff_employer_allocation_id);
                break;

            case 6:
                /*Replace small medium employers with medium plus*/
                $service = new AllocateNonLargeEmployers();
                $service->replaceSmallMediumEmployers($staff_employer_allocation_id);
                break;

            case 7:
                /*Allocate Medium + plus employers to large staff to reach limit of target 200*/
                $service = new AllocateLargeEmployers();
                $service->allocateStaffToMaxTarget($staff_employer_allocation_id);
                break;

                case 8:
                /*Allocate employers for intern*/
//                $service = new AllocateEmployeraForIntern();
//                $service->allocateStaffToMaxTarget($staff_employer_allocation_id);
                break;

        }

    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
