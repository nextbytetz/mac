<?php

namespace App\Jobs\Compliance\StaffEmployerAllocation;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Services\Compliance\StaffEmployerAllocation\AllocateBonusEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateLargeEmployers;
use App\Services\Compliance\StaffEmployerAllocation\AllocateNonLargeEmployers;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Exceptions\JobException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/*Process all tasks for staff employer i.e. send reminder email to staff and */

class StaffEmployerTasksJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 100;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @throws JobException
     * @return void
     */
    public function handle()
    {
        $staff_employer_repo = new StaffEmployerRepository();
        /*Update duplicate employers*/
        $staff_employer_repo->deactivateDuplicateEmployers();



        /*Send Congratulations email* on the first on month*/
        $today = Carbon::now();
        $last_day_month = Carbon::parse(Carbon::now())->startOfMonth();
        $seventh_day_month = $last_day_month->addDays(7);
        if(comparable_date_format($today) >= comparable_date_format($seventh_day_month)){
            $staff_employer_repo->staffCollectionPerformanceCongratEmail(0);

            $staff_employer_repo->staffCollectionPerformanceCongratEmail(1);

            /*Online verification*/
            $staff_employer_repo->staffVerificationCongratEmail();
            /*Online bonus*/
            $staff_employer_repo->staffBonusCongratEmail();

        }

        /*Save monthly collection performances - Save on the 7th Day*/
        $date_day = Carbon::now()->format('j');

        /*Officers - Permanent*/
        $check_if_all_performances_exists = $staff_employer_repo->checkIfAllPerformancesExists();
        /*check if all performances exists and day is 7th of the month*/
        if($date_day >= 7 && $check_if_all_performances_exists == false){
            $staff_employer_repo->saveStaffMonthlyCollectionPerformances();
        }

        /*Intern staff*/
        $check_if_all_performances_exists = $staff_employer_repo->checkIfAllPerformancesExists(1);
        /*check if all performances exists and day is 7th of the month*/
        if($date_day >= 7 && $check_if_all_performances_exists == false){
            $staff_employer_repo->saveStaffMonthlyCollectionPerformances(1);
        }


        /*Staff followup reminders main update*///

//        (new StaffEmployerRepository())->mainPostAndUpdateWeeklyMissingTargetFollowups();


        //        /*Send remainder for follow ups*/
        $staff_employer_repo->sendFollowUpReminders();

    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
