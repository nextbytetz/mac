<?php

namespace App\Jobs\Compliance;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Exceptions\JobException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CategorizeEmployerContributors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 100;

    protected $user;

    protected  $employers;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employers)
    {
        $this->employers = $employers;
    }

    /**
     * Execute the job.
     *
     * @throws JobException
     * @return void
     */
    public function handle()
    {
        $employers = $this->employers;
        $categorize_service = new \App\Services\Compliance\CategorizeEmployerContributors();
        foreach ($employers as $employer){
            $categorize_service->categorizeEmployer($employer->id);
        }

    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
