<?php

namespace App\Jobs\Compliance\BookingReconciliation;


use App\Jobs\GeneralRectificationJob;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Services\Compliance\UpdateBookingsNotContributed;
use App\Services\Compliance\UpdateEmployerStatus;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Receivable\CalculateInterest;

class RectifyPortalArrearsForExistingClosureReopenJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
//    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 3550;

    protected $employers;
    /**
     * Create a new job instance.
     *
     * PostInterest constructor.
     */
    public function __construct()
    {
//        $this->employers = $employers;
    }

    /**
     * Execute the job.
     *
     * @param EmployerRepository $employerRepository
     */
    public function handle()
    {
        $service = new UpdateBookingsNotContributed();
        $service->updatePortalArrearsForEmployerWithSinglePayrollAndReopenedSpecifiedEmployers();

    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }





}










