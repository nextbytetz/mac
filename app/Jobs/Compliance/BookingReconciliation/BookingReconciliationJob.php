<?php

namespace App\Jobs\Compliance\BookingReconciliation;


use App\Jobs\GeneralRectificationJob;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Services\Compliance\UpdateEmployerStatus;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Receivable\CalculateInterest;

class BookingReconciliationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
//    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 3550;

    protected $employers;
    /**
     * Create a new job instance.
     *
     * PostInterest constructor.
     */
    public function __construct()
    {
//        $this->employers = $employers;
    }

    /**
     * Execute the job.
     *
     * @param EmployerRepository $employerRepository
     */
    public function handle()
    {
        $employer_repo = new EmployerRepository();
        $post_bookings = new \App\Services\Compliance\PostBookingsForNonContributors();
        $update_not_contributed = new \App\Services\Compliance\UpdateBookingsNotContributed();
        /***-------START LIVE BOOKINGS AND INTERESTS---------*/



        /*Delete bookings of treasury employers - public*/
        $update_not_contributed->deleteLiveBookingsOfTreasuryEmployers();

        /*update booking is pag flag <LIVE BOOKING>*/
        $update_not_contributed->updatePaidFlagForContributedBookings();

        /*Delete duplicates bookings from live booking*/
        $update_not_contributed->deleteDuplicateBookingEntries();
        /*end of duplicate entries*/

        /*Delete live booking with no interests*/
        $update_not_contributed->deleteLiveBookingInterestsWithNoBooking();

        /*Sync merged / de-merged employers*/
//        $update_not_contributed->rectifyBookingIdOnReceiptCodesFrDuplicateEmployers();
//        $update_not_contributed->unRectifyBookingIdOnReceiptCodesFrDuplicateEmployers();
        /*end of merge/de-merge*/

        /*Rectify booking - Receipt codes contrib month mismatch*/
        $update_not_contributed->rectifyBookingIdOnReceiptCodes();
        /*End rectify booking - code*/

        /*Update booking in on receipt codes table (with no booking_id) for contrib paid in advance (booking)*/
        $update_not_contributed->updateBookingIdOnReceiptCodesWithNoBookingId();

        /*Auto register dormant de-registrations which hve paid after close date*/
        (new EmployerClosureReopenRepository())->autoUpdateStatusForDormantDeregistered();




        /**---------END OF LIVE BOOKINGS AND INTERESTS--------*/


        /*General Rectification*/
        dispatch(new GeneralRectificationJob());

        /*Checker*/

        //Update temporary de-registration reopen alert
        (new EmployerClosureRepository())->initializeCheckerForAll('EMPDERREOP');


        /*Update employer schedules for the last 5 years*/
        if(Carbon::now()->isSaturday()) {
            $update_not_contributed->postEmployerSchedulesFromBeginning(5);
        }


    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }





}










