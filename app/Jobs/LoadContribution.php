<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Finance\Receipt\ReceiptCode;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionTempRepository;
use App\Exceptions\JobException;

/**
 * Load contribution from uploaded schedule file into
 * the temporary table (contribution_temps) for reviewing before being
 * uploaded and stored to the contribution table (contributions).
 * Class LoadContribution
 * @package App\Jobs
 */
class LoadContribution implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 100;

    /**
     * @var
     */
    protected $receiptCode;

    /**
     * LoadContribution constructor.
     * @param ReceiptCode $receiptCode
     */
    public function __construct(ReceiptCode $receiptCode)
    {
        $this->receiptCode = $receiptCode;
    }

    /**
     * @param ContributionTempRepository $contribTemp
     * @throws JobException
     */
    public function handle(ContributionTempRepository $contribTemp)
    {
        $receiptCode = $this->receiptCode;
        $contribPercent = $receiptCode->contributionPercent();
        Excel::load($this->receiptCode->linkedFile(), function($reader) use ($receiptCode) {
            $objWorksheet = $reader->getActiveSheet();
            //exclude the heading
            $receiptCode->total_rows = $objWorksheet->getHighestRow() - 1;
            $receiptCode->rows_imported = 0;
            $receiptCode->amount_imported = 0;
            $receiptCode->upload_error = NULL;
            $receiptCode->error = 0;
            $receiptCode->save();
        });

        /* start: Deleting any contribution temps entry for this receipt, ready for re-uploading */
//        $contribTemp->query()->where(['receipt_code_id' => $this->receiptCode->id])->delete();
        DB::table('contribution_temps')->where('receipt_code_id', '=', $this->receiptCode->id)->delete();
        /*End: Deleting any contribution temps entry for this receipt*/

        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($this->receiptCode->linkedFile())
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['firstname','middlename','lastname','basicpay','grosspay','dob'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new JobException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($this->receiptCode->linkedFile())
            ->chunk($this->chunkSize, function($result) use ($receiptCode, $contribTemp, $contribPercent) {
                $rows = $result->toArray();
                //let's do more processing (change values in cells) here as needed
                //$counter = 0;
                foreach ($rows as $row) {
                    /* Changing date from excel format to unix date format ... */
                    $row['dob'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['dob'], 'YYYY-MM-DD');
                    /* start: Validating row entries */
                    $error_report = "";
                    $error = 0;
                    foreach ($row as $key => $value) {
                        if (trim($key) == 'dob') {
                            if (check_date_format($value) == 0) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                $row[$key] = NULL;
                            }
                        } elseif (in_array(trim($key), ['basicpay', 'grosspay'], true)) {
                            if ($value == 0 Or trim($value) == "" Or $value == NULL Or !is_numeric($value)) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                $row[$key] = NULL;
                            }
                        } elseif (trim($key) == 'middlename') {
                            if (word_check($value) == 0) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                            } }elseif (in_array(trim($key), ['firstname', 'lastname'], true)) {
                                if (trim($value) == "" Or $value == NULL) {
                                    $error = 1;
                                    $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                    $row[$key] = NULL;
                                }
                        }
//                        else {
//                            if (word_check($value) == 0 And trim($value) == "") {
//                                $error = 1;
//                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
//                            }
//                        }
                    }
                    /*create using PDO - Raw*/
                    DB::table('contribution_temps')->insert(
                        ['firstname' => $row['firstname'], 'middlename' => $row['middlename'], 'lastname' => $row['lastname'], 'basicpay' => $row['basicpay'], 'grosspay' => $row['grosspay'], 'dob' => $row['dob'], 'receipt_code_id' => $receiptCode->id, 'error' => $error, 'error_report' => $error_report ]
                    );
                    /* end: Validating row entries */
                }

//                $count = $contribTemp->query()->where(['receipt_code_id' => $receiptCode->id])->count();
                $count = DB::table('contribution_temps')->where(['receipt_code_id' => $receiptCode->id])->count();

                $receiptCode->rows_imported = $count;
                $sum = DB::table('contribution_temps')->where(['receipt_code_id' => $receiptCode->id])->sum('grosspay');
                $sum = $contribPercent * $sum;
                $receiptCode->amount_imported = $sum;
                $receiptCode->save();



            }, true);
        /** end : Uploading excel data into the database */

        /* start: check if there were syntax error during upload */
        $error = 0;
        $error = DB::table('contribution_temps')->where(['receipt_code_id' => $receiptCode->id, 'error' => 1])->count();
        if ($error > 0) {
            // There are syntax errors in the uploaded document
            $receiptCode->error = 1;
            $receiptCode->save();
        }
        /* end: check if there were syntax error during upload */
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {
        $this->receiptCode->upload_error = $e->getMessage();
        $this->receiptCode->save();
    }

}
