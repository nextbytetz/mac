<?php

namespace App\Jobs\Employee;

use App\Models\Finance\Receipt\Receipt;
use App\Models\Operation\Compliance\Member\Contribution;
use App\Models\Operation\Compliance\Member\Employee;
use App\Models\Operation\Compliance\Member\EmployeeTemp;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Log;

class RectifyEmployeeEmployerSyncOnUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $chunkSize = 100;



    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function handle()
    {

        $employees_not_synced_employers = Employee::query()->select('employees.id as employee_id', 'notification_reports.employer_id as employer_id')
            ->leftJoin('employee_employer','employee_employer.employee_id', 'employees.id' )
            ->join('notification_reports', 'notification_reports.employee_id', 'employees.id')
            ->whereNull('employee_employer.id')->groupBy('employees.id', 'notification_reports.employer_id')->get();
//        Log::info(print_r($employees_not_synced_employers,true));
        /*Foreach employee temp*/
        foreach($employees_not_synced_employers as $employees_not_synced_employer)
        {

            if(isset($employees_not_synced_employer->employee_id) && isset($employees_not_synced_employer->employer_id)){
                $employee = Employee::query()->find($employees_not_synced_employer->employee_id);
//            $contrib_employers = Contribution::query()->select('employee_id', 'employer_id')->where('employee_id', $employee->id)->groupBy('employee_id', 'employer_id')->get();
                if(isset($employee->id)){
                    $employee->employers()->attach($employees_not_synced_employer->employer_id);
                }


            }


        }

    }

    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {

    }

}
