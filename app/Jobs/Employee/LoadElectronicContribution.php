<?php

namespace App\Jobs\Employee;

use App\Models\Finance\Receipt\Receipt;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Exception;

class LoadElectronicContribution implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $chunkSize = 100;

    protected $receipt_id;

    public function __construct($id)
    {
        $this->receipt_id = $id;
    }

    /**
     * Execute the job.
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function handle()
    {
        $receiptRepo = new ReceiptRepository();
        $receipt = $receiptRepo->query()->select(['employer_id', 'id'])->find($this->receipt_id);
        $employer_id = $receipt->employer_id;

        //Schema name for portal
        $portal = pg_mac_portal();

        switch ($receipt->employer_id) {
            case 9386:
                //Ministry of Finance and Planning (Accountant General)

                $booking_id = $receipt->receiptCodes()->first()->booking_id;
                //==> Mass Insert Contribution Data ...
                $sql = <<<SQL
-- Inserting into Receipt Codes 
insert into receipt_codes (receipt_id, fin_code_id, amount, member_count, contrib_month, booking_id, employer_id, grouppaystatus, is_treasury, created_at) select a.id as receipt_id, 2 as fin_code_id, c.total_contribution_amount as amount, c.total_employees as member_count, to_char(c.check_date, 'YYYY-MM-28')::date as contrib_month, {$booking_id} as booking_id, d.id as employer_id, 1 as grouppaystatus, 't' as is_treasury, now() as created_at from main.receipts a join {$portal}.payments b on a.pay_control_no = b.control_no join main.treasury_votes_summary c on b.bill_id = c.bill_id join employers d on d.vote = c.vote_code left join (select employer_id, receipt_id from main.receipt_codes) e on e.employer_id = d.id and e.receipt_id = a.id where e.employer_id is null and d.is_treasury = 't' and a.id = {$receipt->id};

-- Insert for new Contributions
insert into contributions (receipt_code_id, employee_amount, grosspay, employee_id, salary, employer_id, created_at, contrib_month) select e.id receipt_code_id, d.employer_contribution employee_amount, d.grosspay, f.id employee_id, d.grosspay salary, g.id employer_id, now() created_at, e.contrib_month from main.receipts a join portal.payments b on a.pay_control_no = b.control_no join main.treasury_funding_summary c on b.bill_id = c.bill_id join treasury_employees_contribution d on c.funding_source = d.funding_source and c.check_date = d.check_date join employees f on d.check_number = f."CheckNumber" join employers g on g.vote = f."VoteCode" join receipt_codes e on e.employer_id = g.id left join (select receipt_code_id, employee_id from contributions) as h on h.receipt_code_id = e.id and f.id = h.employee_id where a.id = {$this->receipt_id} and e.receipt_id = {$this->receipt_id} and h.receipt_code_id is null;

SQL;
                DB::unprepared($sql);

                DB::statement("vacuum full verbose receipt_codes;");
                DB::statement("VACUUM ANALYZE receipt_codes;");
                DB::statement("vacuum full verbose contributions;");
                DB::statement("VACUUM ANALYZE contributions;");

                break;

            default:
                //==> Mass Insert Contribution Data ...
                $sql = <<<SQL
-- Inserting for Arrears
insert into contributions (receipt_code_id, employee_amount, grosspay, employee_id, salary, employer_id, created_at, contrib_month) select a.* from (select f.id as receipt_code_id, case when g.employer_category_cv_id = 36 then 0.5 * 0.01 * h.grosspay else 1 * 0.01 * h.grosspay end as employee_amount, h.grosspay, h.employee_id, h.basicpay as salary, h.employer_id, now() as created_at, e.contrib_month from receipts c join {$portal}.payments d on c.pay_control_no = d.control_no join receipt_codes f on c.id = f.receipt_id and f.fin_code_id = 2 join employers g on c.employer_id = g.id join {$portal}.contribution_arrears e on e.bill_id = d.bill_id and e.status = 1 and date_part('month', f.contrib_month::date) = date_part('month', e.contrib_month::date) and date_part('year', f.contrib_month::date) = date_part('year', e.contrib_month::date) join {$portal}.employee_employer_arrears h on e.id = h.contribution_arrear_id where c.employer_id = {$employer_id} and f.receipt_id = {$this->receipt_id}) as a left join (select receipt_code_id from contributions) as b on a.receipt_code_id = b.receipt_code_id where b.receipt_code_id is null and a.grosspay > 0;

-- Inserting for new Contributions
insert into contributions (receipt_code_id, employee_amount, grosspay, employee_id, salary, employer_id, created_at, contrib_month)  select a.* from (select f.id as receipt_code_id, case when g.employer_category_cv_id = 36 then 0.5 * 0.01 * k.grosspay else 1 * 0.01 * k.grosspay end as employee_amount, k.grosspay, h.id as employee_id, k.basicpay as salary, c.employer_id, now() as created_at, f.contrib_month from receipts c join receipt_codes f on c.id = f.receipt_id and f.fin_code_id = 2 join employers g on c.employer_id = g.id join {$portal}.payments j on j.control_no = c.pay_control_no join {$portal}.bill_employee k on k.bill_id = j.bill_id join employees h on h.id = k.employee_id  where c.employer_id = {$employer_id} and f.receipt_id = {$this->receipt_id}) as a left join (select receipt_code_id from contributions) as b on a.receipt_code_id = b.receipt_code_id where b.receipt_code_id is null and a.grosspay > 0;
SQL;
                DB::unprepared($sql);

                break;
        }

    }

    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {

    }

}
