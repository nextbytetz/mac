<?php

namespace App\Jobs;

use App\Models\Finance\Receipt\LegacyReceiptCode;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\MemberRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;

class RegisterContributionLegacy implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $legacyReceiptCode;

    protected $user_id;

    public function __construct(LegacyReceiptCode $legacyReceiptCode, $user_id)
    {
        $this->legacyReceiptCode = $legacyReceiptCode;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @param ContributionRepository $contribution
     */
    public function handle(ContributionRepository $contribution)
    {
        $legacy_receipt_code_id = null;
        //$employee = new EmployeeRepository();
        /* To be exchanged with employee repository later ... */
        $employee = new EmployeeRepository();
        $legacyReceiptCode = $this->legacyReceiptCode;
        $user_id = $this->user_id;
        $contribPercent = $legacyReceiptCode->contributionPercent();
        /* Select all contributions for this receipt code */
        $legacyReceiptCode->legacyContributionTemps()->select(['id', 'firstname','middlename','lastname','basicpay','grosspay','dob'])->where(['error' => 0, 'legacy_receipt_code_id' => $legacyReceiptCode->id])->chunk(100, function ($legacyContributionTemps) use ($contribution, $legacyReceiptCode, $contribPercent, $employee, $user_id) {
            foreach ($legacyContributionTemps as $legacyContributionTemp) {
                /* Loop through all contribution temps for the receipt and insert in the contributions table */
                $firstname = strtolower($legacyContributionTemp->firstname);
                $lastname = strtolower($legacyContributionTemp->lastname);
                $dob = $legacyContributionTemp->dob;
                $allowance = $legacyContributionTemp->grosspay - $legacyContributionTemp->basicpay;
                $check = $employee->query()->whereRaw("levenshtein(lower(firstname), :firstname) between 0 and 1 and levenshtein(lower(lastname), :lastname) between 0 and 1 and dob = :dob", ['firstname' => $firstname, 'lastname' => $lastname, 'dob' => $dob])->first();
                if (!$check) {
                    /*MAC Data */
                    $data = ['firstname' => $legacyContributionTemp->firstname,'middlename' => $legacyContributionTemp->middlename, 'lastname' => $legacyContributionTemp->lastname, 'dob' => $legacyContributionTemp->dob, 'memberno' => 0, 'user_id' => $user_id];
                    // new employee, create first
                    $contribEmployee = $employee->query()->create($data);

                    /* to change member_no name index in the new mac operation */
                    $employee_id = $contribEmployee->id;
                    $contribEmployee->memberno = checksum($employee_id, sysdefs()->data()->employee_number_length);
                    $contribEmployee->save();

                } else {
                    // existing employee
                    $contribEmployee = $check;

                }

                /* MAC Data */
                $employee_id = $contribEmployee->id;
                $employer_id = $legacyReceiptCode->legacyReceipt->employer_id;
                $contrib_month =  $legacyReceiptCode->contrib_month ;
                $employee_contribution = $contribution->findIfContributionExistByEmployee($employee_id,$employer_id,$contrib_month);
                /* Sync Employer to this employee */
                $contribEmployee->employers()->syncWithoutDetaching(([$employer_id => ['basicpay'=> $legacyContributionTemp->basicpay,'grosspay' => $legacyContributionTemp->grosspay]]));
                /* check if contribution exist for this employee for this month and this employer*/
                if (!$employee_contribution) {
                    $data = ['legacy_receipt_id' => $legacyReceiptCode->legacy_receipt_id, 'legacy_receipt_code_id' => $legacyReceiptCode->id, 'employee_amount' => $contribPercent * $legacyContributionTemp->grosspay, 'grosspay' => $legacyContributionTemp->grosspay, 'employee_id' => $employee_id, 'employer_id' => $legacyReceiptCode->legacyReceipt->employer_id, 'user_id' => $legacyReceiptCode->legacyReceipt->user_id, 'legacy_contribution_temp_id' => $legacyContributionTemp->id, 'salary' => $legacyContributionTemp->basicpay, 'contrib_month' => $contrib_month];
                    $contribution->query()->create($data);
                }else{
                    /* update contribution set legacy receipt and code if contribution exist */
                    $employee_contribution->update([
                        'legacy_receipt_id' =>  $legacyReceiptCode->legacy_receipt_id,
                        'legacy_receipt_code_id' => $legacyReceiptCode->id,
                    ]);
                }


            }
            /* Set all contributions to uploaded status */
            $legacyReceiptCode->update(['isuploaded' => 1]);

        });

    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {

    }
}
