<?php

namespace App\Jobs;


use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Receivable\CalculateInterest;

class PostInterest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
//    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 3550;

    protected $employers;
    /**
     * Create a new job instance.
     *
     * PostInterest constructor.
     */
    public function __construct($employers)
    {
        $this->employers = $employers;
    }

    /**
     * Execute the job.
     *
     * @param EmployerRepository $employerRepository
     */
    public function handle()
    {
        $interest = new CalculateInterest();
        /*100 employers chunk size*/
        $employers = $this->employers;
        foreach($employers as $employer){
            $interest->calculateInterest($employer->id);
        }

//        return true;


    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}












//
///**
// * Create a new job instance.
// *
// * PostInterest constructor.
// */
//public function __construct()
//{
//
//}
//
///**
// * Execute the job.
// *
// * @param EmployerRepository $employerRepository
// */
//public function handle(EmployerRepository $employerRepository)
//{
//    $interest = new CalculateInterest();
//    $employerRepository->query()->select(['id'])->chunk(100, function ($employers) use ($interest) {
//        foreach ($employers as $employer) {
//            $interest->calculateInterest($employer->id);
//        }
//    });
//
//
//
//
//
//}
//
///**
// * @param Exception $e
// */
//public function failed(Exception $e)
//{
//
//}



