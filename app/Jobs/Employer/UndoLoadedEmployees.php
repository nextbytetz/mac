<?php

namespace App\Jobs\Employer;

use App\Models\Operation\Compliance\Member\Employer;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeTempRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class UndoLoadedEmployees implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    protected $employees;

    protected $employer_id;

//    public $tries = 15;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employees, $employer_id)
    {
        //
        $this->employees = $employees;
        $this->employer_id = $employer_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $employees = $this->employees;
        $employer_id = $this->employer_id;
        $employee = new EmployeeRepository();
        $employeeTempRepo = new EmployeeTempRepository();
        foreach ($employees as $employeeTemp) {
            /* Loop through all employee temps for the employee and insert in the employee table */
            $firstname = strtolower($employeeTemp->firstname);
            $lastname = strtolower($employeeTemp->lastname);
            $dob = $employeeTemp->dob;
            $check = $employee->query()->whereRaw("levenshtein(lower(firstname), :firstname) between 0 and 1 and levenshtein(lower(lastname), :lastname) between 0 and 1 and dob = :dob", ['firstname' => $firstname, 'lastname' => $lastname, 'dob' => $dob])->first();

            if ($check){
                /* Detach from employer */
                $check->employers()->detach($employer_id);
            }


            /*delete from employee temps*/
            $employee_temp = $employeeTempRepo->query()->where('id', $employeeTemp->id)->first();
            $employee_temp->delete();

        }



    }
}





