<?php

namespace App\Jobs\Employer;

use App\Jobs\RegisterEmployeeList;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeTempRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


/**
 * Class CheckUploadProgress
 * @package App\Jobs\Employee
 */
class CheckUploadProgress implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $employer;

    /**
     * @var
     */
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * CheckUploadProgress constructor.
     * @param $id
     */
    public function __construct($id, $user_id)
    {
        $employer = new EmployerRepository();
         $this->employer = $employer->query()->find($id);
        $this->user_id = $user_id;


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user_id = $this->user_id;
        $employer = $this->employer;
        $employeeTemp = new EmployeeTempRepository();
        $employee = new EmployeeRepository();
        $document = new DocumentRepository();
        $employee_file_document = $document->getEmployeeFileDocument();
        /* Start : Get the pivot instance of the stored document */
        $pivot = $employer->documents()->where("document_id", $employee_file_document)->first()->pivot;


        /* End : Get the pivot instance of the stored document */

        /* start : Waiting for the chunk import to complete and calculate rows imported summary ... */
        while ($pivot->rows_imported != $pivot->total_rows) {
            $pivot->fresh();
            $count = $employeeTemp->query()->where(['employer_id' => $employer->id])->count();
            $pivot->rows_imported = $count;
            $pivot->save();
             sleep(4);

            continue;
        }
        /* end : Waiting for the chunk import to complete and claculate rows imported summary ... */

        /* start: check if there were syntax error during upload */
        $error = $employeeTemp->query()->where(['employer_id' => $employer->id, 'error' => 1])->count();
        if ($error) {
            // There are syntax errors in the uploaded document
            $pivot->error = 1;
            $pivot->hasinputerror = 1;
            $pivot->save();

        } else {
            if ($pivot->rows_imported) {
                $pivot->isuploaded = 1;
                $pivot->success = 1; //Should be removed and added to the end of RegisterLive Job
                $pivot->save();
            }
        }
        /* end: check if there were syntax error during upload */

        /* start : Check if all the employees has been uploaded in the employee_temps table */
//        if ($pivot->isuploaded) {
//            RegisterEmployeeList::dispatch($employer, $user_id);
//            dispatch(new RegisterEmployeeList($employer, $user_id));
//        }
        /* end : Check if all the employees has been uploaded in the employee_temps table */

    }
}
