<?php

namespace App\Jobs;

use App\Exceptions\GeneralException;
use App\Exceptions\JobException;
use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeTempRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Exception;

class LoadEmployeeList implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    protected $linked_file;
    protected $employer;
    protected $user_id;
    //protected $employer_id;

    protected $chunksize = 100;

    /**
     * LoadEmployeeList constructor.
     * @param $employer_id
     * @param $user_id
     * @throws GeneralException
     */
    public function __construct($employer_id,$user_id)
    {
//        $this->linked_file = $linked_file;
        $employer = new EmployerRepository();
        $this->user_id = $user_id;
        $this->employer = $employer->findWithoutScopeOrThrowException($employer_id);


    }

    /**
     * Execute the job
     *
     * @throws GeneralException
     */
    public function handle()
    {

//        $linked_file = $this->linked_file;
        $employer =  $this->employer;
        $employer_id =  $this->employer->id;
        $linked_file = $employer->employeeFile();
        $user_id = $this->user_id;
        $employeeTemp = new EmployeeTempRepository();
        $document = new DocumentRepository();
        $employee_file_document = $document->getEmployeeFileDocument();
        /* start: Deleting any employee temps entry for this employee, ready for re-uploading */
        DB::table('employee_temps')->where(['employer_id' => $employer_id])->delete();
        /*End: Deleting any employee temps entry for this employee */

        $pivot = $employer->documents()->where("document_id", $employee_file_document)->first()->pivot;

        /*update rows imported*/

        Excel::load($linked_file, function($reader) use ($pivot) {
            $objWorksheet = $reader->getActiveSheet();
            //exclude the heading
            $pivot->total_rows = $objWorksheet->getHighestRow() - 1;
            $pivot->rows_imported = 0;
            $pivot->upload_error = NULL;
            $pivot->error = 0;
            $pivot->isuploaded = 0;
            $pivot->success = 0;
            $pivot->hasfileerror = 0;
            $pivot->hasinputerror = 0;
            $pivot->save();
        });

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($linked_file)
            ->chunk($this->chunksize, function($result)  use($employeeTemp, $employer_id, $user_id, $pivot)  {
                $rows = $result->toArray();
                //let's do more processing (change values in cells) here as needed
                //$counter = 0;
                foreach ($rows as $row) {
                    /* Changing date from excel format to unix date format ... */
                    $row['dob'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['dob'], 'YYYY-MM-DD');
                    /* start: Validating row entries */
                    $error_report = "";
                    $error = 0;
                    foreach ($row as $key => $value) {
                        if (trim($key) == 'dob') {
                            if (check_date_format($value) == 0) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                $row[$key] = NULL;
                            }
                        } elseif (in_array(trim($key), ['allowance'], true)) {
                            if ( !is_numeric($value)) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                $row[$key] = NULL;
                            }
                        } elseif (in_array(trim($key), ['basicpay'], true)) {
                            if (!is_numeric($value)) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                $row[$key] = NULL;
                            }
                        } elseif (in_array(trim($key), ['firstname', 'lastname'], true)) {
                            if (trim($value) == "" Or $value == NULL) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                $row[$key] = NULL;
                            }

                        }


                        elseif (trim($key) == 'gender') {
                            $male_gender = ['MALE', 'M', 'ME', 'MWANAUME', 'MVULANA', 'MME' ];
                            $female_gender =['FEMALE', 'F', 'KE', 'MWANAMKE', 'MSICHANA', 'MKE' ];
                            $gender = array_merge($male_gender, $female_gender);
                            $value = strtoupper($value);
                            if (!in_array(trim($value), $gender)) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                            }
                        }

                    }

                    /* end: Validating row entries */
                    $allowance  = str_replace(",", "",   $row['allowance']) ;
                    $basicpay = str_replace(",", "",   $row['basicpay']) ;
                    $grosspay = ($allowance + $basicpay);
                    $firstname =  preg_replace('/[^A-Za-z\']/', '', $row['firstname']);
                    $middlename =  preg_replace('/[^A-Za-z\']/', '', $row['middlename']);
                    $lastname =   preg_replace('/[^A-Za-z\']/', '', $row['lastname']);
                    /* insert into db */
                    DB::table('employee_temps')->create(['firstname' => $firstname, 'middlename' => $middlename, 'lastname' => $lastname, 'basicpay' => $basicpay, 'grosspay' =>$grosspay,  'dob' => $row['dob'], 'employer_id' => $employer_id, 'gender' => $row['gender'],'job_title' => $row['job_title'], 'employment_category' => $row['employment_category'], 'error' => $error, 'error_report' => $error_report ]);

                    /*update rows imported*/
                    $pivot->rows_imported = DB::table('employee_temps')->where('employer_id', $employer_id)->count();
                    $pivot->isuploaded = 1;
                    $pivot->save();


                }


            }, true);

    }



    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }



    public  function gender_check($string)
    {
        $male_gender = $this->male_gender();
        $female_gender =$this->female_gender();
        $gender = array_merge($male_gender, $female_gender);
        $string = strtoupper($string);
        if (in_array($string, $gender)){
            return 1;
        }else{
            return 0;
        }
    }


    public function male_gender()
    {
        $male_gender = ['MALE', 'M', 'ME', 'MWANAUME', 'MVULANA', 'MME' ];
        return $male_gender;

    }





    public function female_gender()
    {
        $female_gender =['FEMALE', 'F', 'KE', 'MWANAMKE', 'MSICHANA', 'MKE' ];
        return $female_gender;

    }



    public function people_name_validation($string)
    {
        $name = preg_replace('/[^A-Za-z\']/', '', $string);
        return $name;

    }




}
