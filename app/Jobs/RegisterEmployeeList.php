<?php

namespace App\Jobs;

use App\Models\Operation\Compliance\Member\Employer;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeTempRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class RegisterEmployeeList implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    protected $employees;

    protected $user_id;

    protected $employer_id;

//    public $tries = 15;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employees, $employer_id, $user_id)
    {
        //
        $this->employees = $employees;
        $this->user_id = $user_id;
        $this->employer_id = $employer_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

////
        $employees = $this->employees;
        $user_id = $this->user_id;
        $employer_id = $this->employer_id;
        $employee = new EmployeeRepository();
        $employeeTempRepo = new EmployeeTempRepository();
        foreach ($employees as $employeeTemp) {
            /* Loop through all employee temps for the employee and insert in the employee table */
            $firstname = strtolower($employeeTemp->firstname);
            $lastname = strtolower($employeeTemp->lastname);
            $dob = $employeeTemp->dob;
            $check = $employee->query()->whereRaw("levenshtein(lower(firstname), :firstname) between 0 and 1 and levenshtein(lower(lastname), :lastname) between 0 and 1 and dob = :dob", ['firstname' => $firstname, 'lastname' => $lastname, 'dob' => $dob])->first();
//                    $check = NULL;
            if (!$check) {
                /**/
                $gender_id = null;
                $male_gender = ['MALE', 'M', 'ME', 'MWANAUME', 'MVULANA', 'MME' ];
                $female_gender =['FEMALE', 'F', 'KE', 'MWANAMKE', 'MSICHANA', 'MKE' ];

                if (in_array($employeeTemp->gender,$male_gender)) {
                    $gender_id = 1;
                } elseif(in_array($employeeTemp->gender,$female_gender)) {
                    $gender_id = 2;
                }

                /* Save Employee Data */
                $data = ['firstname' => $employeeTemp->firstname,'middlename' => $employeeTemp->middlename, 'lastname' => $employeeTemp->lastname, 'dob' => $employeeTemp->dob, 'gender_id' => $gender_id, 'sex' => $employeeTemp->gender, 'memberno' => 0,'emp_cate' => $employeeTemp->employment_category, 'created_at' => Carbon::now(), 'user_id' => $user_id];
                // new employee, create first
                /* db builder*/
                $registeredEmployeeId =  DB::table('employees')->insertGetId(
                    $data
                );
                $registeredEmployee = $employee->findOrThrowException($registeredEmployeeId);
                /*end db builder*/
                /* to change member_no name index in the new mac operation */
                $memberno = checksum($registeredEmployeeId, sysdefs()->data()->employee_number_length);
                $registeredEmployee->save();
                /*update memberno*/
                DB::table('employees')
                    ->where('id', $registeredEmployeeId)
                    ->update(['memberno' => $memberno]);
            } else {
                // existing employee
                $registeredEmployee = $check;
                /*remove from temp*/
            }
            /* Attach to employers */
            $registeredEmployee->employers()->syncWithoutDetaching([$employer_id => ['basicpay'=> $employeeTemp->basicpay,'grosspay' => $employeeTemp->grosspay, 'job_title' => $employeeTemp->job_title   ]]);

            /*remove from temp*/
            DB::table('employee_temps')->where('id',$employeeTemp->id)->delete();

        }
        /*rectify memberno*/
        $employee->rectifyMemberno($employer_id);


    }
}





