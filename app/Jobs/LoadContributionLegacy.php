<?php

namespace App\Jobs;

use App\Models\Finance\Receipt\LegacyReceiptCode;
use App\Models\Operation\Compliance\Member\LegacyContributionTemp;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Finance\Receipt\ReceiptCode;
use Maatwebsite\Excel\Facades\Excel;
use App\Exceptions\JobException;

/**
 * Load contribution from uploaded schedule file into
 * the temporary table (contribution_legacy_temps) for reviewing before being
 * uploaded and stored to the contribution table (contributions).
 * Class LoadContribution
 * @package App\Jobs
 */
class LoadContributionLegacy implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 100;

    /**
     * @var
     */
    protected $legacyReceiptCode;

    /**
     * LoadContribution constructor.
     * @param ReceiptCode $receiptCode
     */
    public function __construct(LegacyReceiptCode $legacyReceiptCode)
    {
        $this->legacyReceiptCode = $legacyReceiptCode;
    }

    /**
     * @param legacyContributionTemp $legacyContribTemp
     */
    public function handle(LegacyContributionTemp $legacyContribTemp)
    {
        $legacyReceiptCode = $this->legacyReceiptCode;
        Excel::load($this->legacyReceiptCode->linkedFile(), function($reader) use ($legacyReceiptCode) {
            $objWorksheet = $reader->getActiveSheet();
            //exclude the heading
            $legacyReceiptCode->total_rows = $objWorksheet->getHighestRow() - 1;
            $legacyReceiptCode->rows_imported = 0;
            $legacyReceiptCode->amount_imported = 0;
            $legacyReceiptCode->upload_error = NULL;
            $legacyReceiptCode->error = 0;
            $legacyReceiptCode->save();
        });

        /* start: Deleting any contribution temps entry for this receipt, ready for re-uploading */
        $legacyContribTemp->query()->where(['legacy_receipt_code_id' => $this->legacyReceiptCode->id])->delete();
        /*End: Deleting any contribution temps entry for this receipt*/

        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($this->legacyReceiptCode->linkedFile())
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['firstname','middlename','lastname','basicpay','grosspay','dob'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new JobException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($this->legacyReceiptCode->linkedFile())
            ->chunk($this->chunkSize, function($result) use ($legacyReceiptCode, $legacyContribTemp) {
                $rows = $result->toArray();
                //let's do more processing (change values in cells) here as needed
                //$counter = 0;
                foreach ($rows as $row) {
                    /* Changing date from excel format to unix date format ... */
                    $row['dob'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['dob'], 'YYYY-MM-DD');
                    /* start: Validating row entries */
                    $error_report = "";
                    $error = 0;
                    foreach ($row as $key => $value) {
                        if (trim($key) == 'dob') {

                            if (check_date_format($value) == 0) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                $row[$key] = NULL;
                            }
                        } elseif (in_array(trim($key), ['basicpay', 'grosspay'], true)) {
                            if ($value == 0 Or trim($value) == "" Or $value == NULL Or !is_numeric($value)) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => $key, 'entry' => $value]) . ", \r\n";
                                $row[$key] = NULL;
                            }
                        } elseif (trim($key) == 'middlename') {
                            if (word_check($value) == 0) {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                            }
                        } else {
                            if (word_check($value) == 0 And trim($value) == "") {
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                            }
                        }
                    }
                    $legacyContribTemp->query()->create(['firstname' => $row['firstname'], 'middlename' => $row['middlename'], 'lastname' => $row['lastname'], 'basicpay' => $row['basicpay'], 'grosspay' => $row['grosspay'], 'dob' => $row['dob'], 'legacy_receipt_code_id' => $legacyReceiptCode->id, 'error' => $error, 'error_report' => $error_report ]);
                    /* end: Validating row entries */
                }

            }, true);
        /** end : Uploading excel data into the database */

        /* start : Waiting for the chunk import to complete and calculate rows imported summary ... */
        while ($legacyReceiptCode->rows_imported != $legacyReceiptCode->total_rows) {
            $legacyReceiptCode->fresh();
            $count = $legacyContribTemp->query()->where(['legacy_receipt_code_id' => $legacyReceiptCode->id])->count();
            $legacyReceiptCode->rows_imported = $count;
            $legacyReceiptCode->save();
            sleep(4);
            continue;
        }
        /* end : Waiting for the chunk import to complete and claculate rows imported summary ... */

        /* start : Summing all gross amount from the uploaded document and updating the receipt_code */
        $sum = $legacyContribTemp->query()->where(['legacy_receipt_code_id' => $legacyReceiptCode->id])->sum('grosspay');
        $contribPercent = $legacyReceiptCode->contributionPercent();
        $sum = $contribPercent * $sum;
        $legacyReceiptCode->amount_imported = $sum;
        $legacyReceiptCode->save();
        /* end : Summing all gross amount from the uploaded document and updating the receipt_code */

        /* start: check if there were syntax error during upload */
        $error = 0;
        $error = $legacyContribTemp->query()->where(['legacy_receipt_code_id' => $legacyReceiptCode->id, 'error' => 1])->count();
        if ($error > 0) {
            // There are syntax errors in the uploaded document
            $legacyReceiptCode->error = 1;
            $legacyReceiptCode->save();
        }
        /* end: check if there were syntax error during upload */
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {
        $this->legacyReceiptCode->upload_error = $e->getMessage();
        $this->legacyReceiptCode->save();
    }

}
