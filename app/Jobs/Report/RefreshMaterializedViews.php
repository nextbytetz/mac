<?php

namespace App\Jobs\Report;

use App\Repositories\Backend\Reporting\ConfigurableReportRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class RefreshMaterializedViews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $view_name;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($view_name = null)
    {
        //
        $this->view_name = $view_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->view_name == null){
            $this->refreshMaterializedViews();
        }else{
            $this->refreshMaterializedViewByName($this->view_name);
        }
    }



/*Refresh all materilized views listed below*/
    public function refreshMaterializedViews()
    {
        $sql = <<<SQL
REFRESH MATERIALIZED VIEW summary_contrib_rcvable_age_analysis;
REFRESH MATERIALIZED VIEW summary_contrib_rcvable_income_mview;
REFRESH MATERIALIZED VIEW bookings_mview;
SQL;

        DB::unprepared($sql);
    }


//refresh view by name specified
    public function refreshMaterializedViewByName($view_name)
    {
        $sql = <<<SQL
REFRESH MATERIALIZED VIEW {$view_name};
SQL;
        DB::unprepared($sql);
    }

}
