<?php

namespace App\Jobs\ProcessPaymentVoucherTransactions;

use App\Http\Requests\Backend\Operation\Payroll\PayrollRunRequest;
use App\Models\Operation\Claim\ClaimCompensation;
use App\Repositories\Backend\Finance\PaymentVoucherRepository;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Payroll\ProcessPayroll;

class ProcessPVTransactionIndividual implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $user_id;

    protected $pv_transaction_individuals;
    protected $payroll_proc_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($pv_transaction_individuals,$payroll_proc_id, $user_id)
    {
        $this->user_id = $user_id;
        $this->pv_transaction_individuals = $pv_transaction_individuals;
        $this->payroll_proc_id = $payroll_proc_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pv_transaction_individuals = $this->pv_transaction_individuals;
        $user_id = $this->user_id;
        $payroll_proc_id = $this->payroll_proc_id;
        $payment_voucher_trans = new PaymentVoucherRepository();
        $payment_voucher_trans->processPaymentVoucherTransactionIndividual($pv_transaction_individuals,$payroll_proc_id, $user_id);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
