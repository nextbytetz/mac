<?php

namespace App\Jobs\Erp;


use App\Repositories\Backend\MacErp\ClaimsErpApiRepositroy;
use App\Repositories\Backend\MacErp\ReceiptsErpApiRepositroy;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use Exception;

use Illuminate\Support\Facades\Log;
// use function \FluidXml\fluidxml;

class BulkUpdateInvoicePaymentFromErp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $chunkSize = 100;

    protected $min_date;
    protected $max_date;
    protected $claim_payable_repo;



    public function __construct($min_date = '01-04-2019' , $max_date = '31-12-2019')
    {
        $today = Carbon::now();
        /*After dec 2019*/
        if(comparable_date_format($max_date) < comparable_date_format($today)){
            $this->min_date = Carbon::now()->subMonthNoOverflow(2)->startOfMonth();
            $this->max_date = standard_date_format(Carbon::now());
        }else{
            $this->min_date =   $min_date;
            $this->max_date = $max_date;
        }

        $this->claim_payable_repo = new ClaimsErpApiRepositroy();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $min_date = $this->min_date;
        $max_date = $this->max_date;
        $payments = $this->getInvoicePayments($min_date, $max_date);
        $input = [];
        foreach($payments as $key => $payment){
            foreach($payment as $key2 => $invoice){

                $input = [
                    'payment_date' => $invoice['payment_date'],
                    'payment_reference' => $invoice['document_number'],
                    'payment_method' => $invoice['payment_method_code'],
                    'ispaid' => 1,
                    'amount_paid' => $invoice['amount_paid']
                ];
                /*CHeck if invoice is from MAC*/
                if($invoice['payment_status_flag'] == 'Y' && isset($invoice['transaction_id'])){
//                if($invoice['payment_status_flag'] == 'Y'){
                    $trx_id =  $invoice['transaction_id'];

                    $claim_payable = $this->claim_payable_repo->findByTrxId($trx_id);

                    if(isset($claim_payable)){
                        /*If exists in claim payable*/
                        /*update*/
                        $this->updateInvoicePaymentOnMacFromErp($trx_id, $input);
                    }

                }elseif($invoice['payment_status_flag'] == 'Y' && !isset($invoice['transaction_id'])){
                    /*For manual payment need to compare with amount */
                    $trx_id = isset($invoice['transaction_id']) ? $invoice['transaction_id'] : $invoice['invoice_num'];
                    $claim_payable = $this->claim_payable_repo->findByTrxId($trx_id);
                    if(isset($claim_payable)){
                        if($claim_payable->amount == $input['amount_paid']){
                            /*update*/
                            $this->updateInvoicePaymentOnMacFromErp($trx_id, $input);
                        }
                    }
                }
            }

        }
    }


    /*Get invoice payments by date through api url*/
    public function getInvoicePayments($min_date, $max_date)
    {
        // $min_date = (isset($min_date)) ? $min_date : Carbon::now();
        // $max_date = (isset($max_date)) ? $max_date : Carbon::now();
        $min_date = Carbon::parse($min_date)->format('d-m-Y');
        $max_date = Carbon::parse($max_date)->format('d-m-Y');
        $client = new Client();
        $link = env('ERP_APP_URL');
        // $guzzlerequest = $client->get($link . "/api/erp/min_date={$min_date}max_date={$max_date}");
        $guzzlerequest = $client->get($link . "/ords/apps/ap/payments/{$min_date}/{$max_date}");

        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        return $parsed_json;
    }

    /**
     * @param $payments
     * Update payment references according to the payable source i.e refund , claim benefits
     */
    public function updateInvoicePaymentOnMacFromErp($trx_id, $input){
        DB::transaction(function () use ($trx_id,$input) {
            $claim_payable = $this->claim_payable_repo->findByTrxId($trx_id);
            if(isset($claim_payable)){
                /*Update claim_payable status and isposted*/
                $this->claim_payable_repo->updateClaimsPaymentStatus($trx_id);

                switch ($claim_payable->isclaim){
                    case 1:
                        /*claim - compensations payments*/
                        /*Update claim - eligible benefits*/
                        $this->claim_payable_repo->updateClaimPaymentInfoFromErp($trx_id, $input);
                        break;

                    case 2:
                        /*claim - payroll pensions*/
                        (new PayrollRunApprovalRepository())->updateRunVoucherPaymentInfoFromErp($trx_id, $input);
                        break;
                }
            }

        });
    }


}
