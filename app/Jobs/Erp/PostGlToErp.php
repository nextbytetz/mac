<?php

namespace App\Jobs\Erp;

use App\Models\Finance\Receipt\Receipt;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\MacErp\ReceiptsErpApiRepositroy;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use Exception;

use Illuminate\Support\Facades\Log;
// use function \FluidXml\fluidxml;

class PostGlToErp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $chunkSize = 100;

    protected $receipt_id;

    protected $employer_id;

    public function __construct($id)
    {
        $this->receipt_id = $id;
        // $this->employer_id=$employer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Log::info('hapa');
        // Log::info($this->receipt_id);
        $receipts_gl =  new ReceiptsErpApiRepositroy();

        $transaction_ids = $receipts_gl->saveApiTransaction($this->receipt_id, false);

        // Log::info(print_r($transaction_ids,true));
        // Log::info('5');
        foreach ($transaction_ids as $transaction_id) {
         if(!is_null($transaction_id)){
             try {
                $client = new Client();
                $response =  $client->request('GET', config('constants.MAC_ERP.GL_TO_Q').$transaction_id);
                $status = $response->getStatusCode();
            }
            catch (ClientError $e) {

                $req = $e->getRequest();
                $status =$e->getStatusCode();
                Log::info($transaction_id.' Status:'.$status.' ClientError');

            }
            catch (ServerError $e) {

                $req = $e->getRequest();
                $status =$e->getStatusCode();
                Log::info($transaction_id.' Status: '.$status.' ServerError');


            }
            catch (BadResponse $e) {

                $req = $e->getRequest();
                $status =$e->getStatusCode();
                Log::info($transaction_id.' Status: '.$status,' BadResponse');

            }
            catch(\Exception $e){
                $status = $e->getCode();
                Log::info($transaction_id.' Status: '.$status.' Exception');

            }

            $receipts_gl->updateReceiptPostedToQ($transaction_id, $status); 
        }


    }

}
}
