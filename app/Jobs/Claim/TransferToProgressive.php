<?php

namespace App\Jobs\Claim;

use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TransferToProgressive implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $incidentRepo = new NotificationReportRepository();

        $incidentRepo->transferToProgressive();

        //Filling Checklist || Missing Contribution
        /*$incidentRepo->query()->where(["isprogressive" => 0, 'status' => 0, 'wf_done' => 0, 'investigation_validity' => 0])->doesntHave("investigations")->chunk(20, function ($incidents) use ($incidentRepo) {
            foreach ($incidents as $incident) {
                $incidentRepo->transferToProgressive($incident);
            }
        });*/

        //Finished filling Investigation but check for missing contribution (Nothing to do)
        /*$incidentRepo->query()->where(["isprogressive" => 0, 'status' => 0, 'wf_done' => 0, 'investigation_validity' => 1])->has("investigations")->chunk(20, function ($incidents) {
            foreach ($incidents as $incident) {

            }
        });*/

        //Filling Investigation but check for missing contribution (Nothing to do)
        /*$incidentRepo->query()->where(["isprogressive" => 0, 'status' => 0, 'wf_done' => 0, 'investigation_validity' => 0])->has("investigations")->chunk(20, function ($incidents) {
            foreach ($incidents as $incident) {

            }
        });*/

    }
}
