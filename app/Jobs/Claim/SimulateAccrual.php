<?php

namespace App\Jobs\Claim;

use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationWorkflowRepository;
use App\Services\Finance\ProcessPaymentPerBenefit;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SimulateAccrual implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 8;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 43200; // 12 hours

    protected $level;

    public function __construct($level)
    {
        $this->level = $level;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (env("TESTING_MODE", 0)) {
            $workflows = (new NotificationWorkflowRepository())->query()->whereIn("id", function ($query) {
                $query->select('resource_id')
                    ->from("cr6")
                    ->where('resource_type', 'App\Models\Operation\Claim\NotificationWorkflow')
                    ->where("level", '>=', $this->level);
                          })->get();

            foreach ($workflows as $workflow) {
                $eligible = $workflow->eligibles()->whereNull("notification_eligible_benefits.parent_id")->first();
                    (new ClaimCompensationRepository())->progressiveCompensationApprove($eligible->incident, 1, $eligible->id);
                $payment = new ProcessPaymentPerBenefit($workflow->id, 85);
                $payment->processPayment();
            }
            $notifications = (new NotificationReportRepository())->query()->whereIn("id", function ($query) {
                $query->select('resource_id')
                    ->from("cr6")
                    ->where('resource_type', 'App\Models\Operation\Claim\NotificationReport')
                    ->where("level",'>=',$this->level);
            })->get();
            foreach ($notifications as $notification) {
                (new ClaimCompensationRepository())->claimCompensationApprove($notification->id, 1);
            }
        }
        /**
         * Checking the report through database
        -- start: claim accrual script
        select nr.filename, concat_ws(' ', e.firstname, e.lastname) employee, bt.name, a.amount from claim_compensations a join claims b on a.claim_id = b.id join notification_reports nr on b.notification_report_id = nr.id join benefit_types bt on a.benefit_type_id = bt.id join employees e on nr.employee_id = e.id where notification_eligible_benefit_id in (select id from notification_eligible_benefits where notification_workflow_id in (select resource_id from cr6 where resource_type in ('App\Models\Operation\Claim\NotificationWorkflow', 'App\Models\Operation\Claim\NotificationReport') and level = 7));
        -- end: claim accrual script
         */
    }
}
