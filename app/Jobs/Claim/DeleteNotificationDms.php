<?php

namespace App\Jobs\Claim;

use App\Exceptions\JobException;
use App\Services\Notifications\Api;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;

class DeleteNotificationDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Api;

    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @throws JobException
     */
    public function handle()
    {
        $fileNumber = $this->id;
        $url = env('EOFFICE_APP_URL') . "/api/claim/delete?fileNumber={$this->id}";
        $postFields = "{ \"fileType\":\"{$this->fileType}\", \"classification\":\"{$this->classification}\", \"fileNumber\":\"{$fileNumber}\", \"department\":\"{$this->department}\" }";
        $response = $this->sendPostJsonEoffice($url, $postFields);
        $data = json_decode($response);
        if (count($data)) {
            if ($data->message == "SUCCESS") {
                //file deleted successful

            }
        } else {
            throw new JobException("Failed to delete file from e-office");
        }
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
