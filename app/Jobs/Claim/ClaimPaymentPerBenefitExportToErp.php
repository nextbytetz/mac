<?php

namespace App\Jobs\Claim;

use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\MacErp\ClaimsErpApiRepositroy;
use App\Repositories\Backend\Operation\Claim\BenefitTypeRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Log;


class ClaimPaymentPerBenefitExportToErp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * @var NotificationReport
     */
    protected $notification_workflow_id;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification_workflow_id)
    {
        //
        $this->notification_workflow_id = $notification_workflow_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $claim_export_repo = new ClaimsErpApiRepositroy();

        DB::transaction(function () use($claim_export_repo) {

            $claim_export_repo->postApErpApi($this->notification_workflow_id);

//            $payment_voucher_trans = $payment_voucher_trans_repo->getPendingExportPvTranForDataTable()->get();
//            foreach ($payment_voucher_trans as $pv_tran) {
//
//                $api_fields = $payment_voucher_trans_repo->getApiFieldsForPendingExports($pv_tran);
//
//                /*Post to ERP Through API*/
//
//                    Log::info(print_r($api_fields));
//                /*end API*/
//
//                /*update is exported flag*/
//                 $payment_voucher_trans_repo->updateExportFlag($pv_tran->id);
//            }
        });
    }






}
