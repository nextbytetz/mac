<?php

namespace App\Jobs\Claim;

use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RecheckPostingDocumentDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $incidentId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($incidentId = NULL)
    {
        $this->incidentId = $incidentId;
    }

    /**
     *  Execute the job.
     *
     * @return bool
     * @throws \App\Exceptions\JobException
     */
    public function handle()
    {
        $repo = new NotificationReportRepository();
        $repo->recheckPostingDocumentDms($this->incidentId);
        return true;
    }

}
