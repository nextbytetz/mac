<?php

namespace App\Jobs\Claim;

use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class BulkUpdateEofficeIdDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $notification_reports;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->notification_reports = new NotificationReportRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $this->updateDocuments();
    }


    /**
     * Get Already stored documents
     */
    public function updateDocuments()
    {
        return DB::transaction(function () {
//            $documents = DB::Table('document_notification_report')->where('name', 'e-office')->whereNull('eoffice_document_id')->orderBy('id', 'asc')->get();
            $documents = DB::Table('document_notification_report')->whereNull('eoffice_document_id')->orderBy('id', 'asc')->get();
              foreach ($documents as $document) {
                $notification_report_id = $document->notification_report_id;
                $document_type = $document->document_id;
                /*UPDATE*/
                $this->updateInsertDocumentFiles($notification_report_id, $document_type);
            }
        });
    }




    /**
     * @param $notification_report_id
     * @param $document_type
     * Update or insert document files for each document type
     */
    public function updateInsertDocumentFiles($notification_report_id, $document_type)
    {
        /*--Return document from eOffice through api url--*/
        $documents = $this->getDocumentFromEOffice($notification_report_id,$document_type);

        /*--IF Array has more than one document--*/

        foreach($documents as $key => $document)
        {
            $eOfficeDocumentId =  $document['documentId'];
            $subject =  $document['subject'];
            /*update eOffice id on Mac for existing integrated document*/
            $this->updateExistingDocument($notification_report_id, $document_type,$eOfficeDocumentId, $subject );

            /*insert the following into Mac If the more than 1 doc from eoffice*/
            if ($this->checkIfEOfficeDocumentIdExist($eOfficeDocumentId) == false)
            {
                DB::Table('document_notification_report')->insert(['notification_report_id'=> $notification_report_id, 'document_id' => $document_type, 'name' => 'e-office', 'eoffice_document_id' => $eOfficeDocumentId, 'description' => $subject, 'created_at' => Carbon::now()]);
            }

        }

        /*--END IF--*/


    }

    public function updateExistingDocument($notification_report_id, $document_type, $eOfficeDocumentId, $subject)
    {
        $updated_document = DB::Table('document_notification_report')->where('notification_report_id', $notification_report_id)->where('document_id', $document_type)->whereNull('eoffice_document_id')->update([
            'eoffice_document_id' => $eOfficeDocumentId,
            'description' => $subject,
            'updated_at' => Carbon::now(),
            'name' => 'e-office',
        ]);

    }



    public function checkIfEOfficeDocumentIdExist($eOfficeDocumentId)
    {
        $document = DB::Table('document_notification_report')->where('eoffice_document_id', $eOfficeDocumentId)->first();

        if ($document)
        {
            return true;
        }else{
            return false;
        }
    }


    /*Get document files for certain document type from eOffice through api url*/
    public function getDocumentFromEOffice($notification_report_id, $document_type)
    {
        $client = new Client();
        $link = env('EOFFICE_APP_URL');
        $fileNumber = $notification_report_id;
        $guzzlerequest = $client->get($link . "/api/claim/file/document/sync?fileNumber={$fileNumber}&documentType={$document_type}", [
            'auth' => [
                env("EOFFICE_AUTH_USER"),
                env("EOFFICE_AUTH_PASS"),
            ],
        ]);
        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        return $parsed_json;
    }

}
