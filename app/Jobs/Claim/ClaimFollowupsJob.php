<?php

namespace App\Jobs\Claim;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Backend\Operation\Claim\ClaimFollowupRepository;

class ClaimFollowupsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
        $this->followup = new ClaimFollowupRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        dump('aaaa');
        $this->followup->syncFolloups();
        dump('pppp');

    }

}
