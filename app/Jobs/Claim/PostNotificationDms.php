<?php

namespace App\Jobs\Claim;

use App\Exceptions\JobException;
use App\Models\Operation\Claim\NotificationReport;
use App\Services\Notifications\Api;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Illuminate\Support\Facades\Log;

class PostNotificationDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Api;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * @var NotificationReport
     */
    protected $notificationReport;

    /**
     * Create a new job instance.
     *
     * PostNotificationDms constructor.
     * @param NotificationReport $notificationReport
     */
    public function __construct(NotificationReport $notificationReport)
    {
        $this->notificationReport = $notificationReport;
    }

    /**
     * Execute the job.
     *
     * @throws JobException
     */
    public function handle()
    {

        $fileNumber = $this->notificationReport->id;
        $fileName = $this->notificationReport->file_name_specific_label;
        $subject = $this->notificationReport->file_subject_specific;
        $description = "";
        $url = env('EOFFICE_APP_URL') . "/api/claim/add";
        $postFields = "{ \"fileType\":\"{$this->fileType}\", \"classification\":\"{$this->classification}\", \"fileNumber\":\"{$fileNumber}\", \"fileName\":\"{$fileName}\", \"department\":\"{$this->department}\", \"subject\":\"{$subject}\", \"description\":\"{$description}\" }";
        $response = $this->sendPostJsonEoffice($url, $postFields);
        $data = json_decode($response, true) ?? [];
        if (count($data) && isset($data['message'])) {
            if ($data['message'] == "SUCCESS") {
                //file posted successful
                $this->notificationReport->isdmsposted = 1;
                $this->notificationReport->save();
            }
        } else {
            throw new JobException("Failed to post document notification");
        }
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
