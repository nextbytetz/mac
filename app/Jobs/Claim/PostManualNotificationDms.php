<?php

namespace App\Jobs\Claim;

use App\Exceptions\JobException;
use App\Models\Operation\Claim\ManualNotificationReport;
use App\Models\Operation\Claim\NotificationReport;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Services\Notifications\Api;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Illuminate\Support\Facades\Log;

class PostManualNotificationDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Api;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * @var NotificationReport
     */
    protected $notificationReport;

    /**
     * Create a new job instance.
     *
     * PostNotificationDms constructor.
     * @param NotificationReport $notificationReport
     */
    public function __construct ($manual_notification_report)
    {
        $this->notificationReport = $manual_notification_report;

    }

    /**
     * Execute the job.
     *
     * @throws JobException
     */
    public function handle()
    {
        $fileType = 3; //TODO NEED TO BE PROVIDED
        $classification = 1;
        $department = 8;
        $manual = 'true';
        $fileNumber = 'AB1/457/' . $this->notificationReport->id;
        //$fileName = $this->notificationReport->file_name_specific_label;
        $fileName = $fileNumber . "/" . $this->notificationReport->incident_type;
        $subject = $this->notificationReport->file_subject_specific;
        $description = "";
        //TODO need to update method from e-office
        $url = env('EOFFICE_APP_URL') . "/api/claim/add";
        $postFields = "{ \"fileType\":\"{$fileType}\", \"classification\":\"{$classification}\", \"fileNumber\":\"{$fileNumber}\", \"fileName\":\"{$fileName}\", \"department\":\"{$department}\", \"subject\":\"{$subject}\", \"description\":\"{$description}\" }";
        $response = $this->sendPostJsonEoffice($url, $postFields);
        $data = json_decode($response, true);
      //        logger($data);
        if (count($data)) {
            if ($data['message'] == "SUCCESS") {
                //file posted successful
                $this->notificationReport->isdmsposted = 1;
                $this->notificationReport->save();
            }
        } else {
            throw new JobException("Failed to post document notification");
        }
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
