<?php

namespace App\Jobs\Claim;

use App\Repositories\Backend\Operation\Claim\BenefitTypeClaimRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckUploadedDocumentsNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;

    public $skipchecked;

    /**
     * Create a new job instance.
     *
     * @param null $id
     * @param int $skipchecked
     */
    public function __construct($id = NULL, $skipchecked = 1)
    {
        $this->id = $id;
        $this->skipchecked = $skipchecked;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $incidentRepo = new NotificationReportRepository();

        $incidentRepo->checkUploadedDocuments($this->id, $this->skipchecked);

    }


}
