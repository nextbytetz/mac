<?php

namespace App\Jobs\Claim;

use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PostDocumentDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $incident;

    public $documentId;

    public $source;

    /**
     * Create a new job instance.
     *
     * @param Model $incident
     * @param $documentId
     * @param int $source
     */
    public function __construct(Model $incident, $documentId, $source = 2)
    {
        $this->incident = $incident;
        $this->documentId = $documentId;
        $this->source = $source;
    }

    /**
     * Execute the job
     * @throws \App\Exceptions\JobException
     *
     * @return void
     */
    public function handle()
    {
        $repo = new NotificationReportRepository();
        switch ($this->source) {
            case 1: //For Branch Notification
                $repo->sendBranchOnlineDocumentToDms($this->incident, $this->documentId);
                break;
            default: //For Online Notification
                $repo->sendDocumentToDms($this->incident, $this->documentId);
                break;
        }
    }

}
