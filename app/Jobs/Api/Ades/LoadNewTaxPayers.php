<?php

namespace App\Jobs\Api\Ades;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use Exception;
use App\Models\Api\NewTaxpayer;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class LoadNewTaxPayers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 3550;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $last_taxpayer_fetch_date = Carbon::createFromFormat("Ymd", sysdefs()->data()->last_taxpayer_fetch_date);
        $last_taxpayer_fetch_date->addDay(1);
        //$next_taxpayer_fetch_date = $last_taxpayer_fetch_date->copy()->addDay(env("TRA_NEW_TAXPAYER_FETCH_INTERVAL"));
        $next_taxpayer_fetch_date = Carbon::now();

        //Fetch date and insert to the database
        $client = new Client();
        $guzzlerequest = $client->get('http://gateway.tra.go.tz/employerservice/api/newemployers/' . $last_taxpayer_fetch_date->format("Ymd") . '/' . $next_taxpayer_fetch_date->format("Ymd"));
        //$guzzlerequest = $client->get('http://mac_portal.gwanchi/debug');
        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);

        if ($parsed_json['StatusCode'] == 1) {
            if (!is_null($parsed_json['Payload'])) {
                foreach ($parsed_json['Payload'] as $payload) {
                    DB::table('new_taxpayers')->insert([
                        [
                            'tin' => $payload['GeneralInformation']['BasicInformation']['TaxpayerId'],
                            'firstname' => $payload['GeneralInformation']['BasicInformation']['FirstName'],
                            'middlename' => $payload['GeneralInformation']['BasicInformation']['MiddleName'],
                            'lastname' => $payload['GeneralInformation']['BasicInformation']['LastName'],
                            'taxpayername' => $payload['GeneralInformation']['BasicInformation']['TaxpayerName'],
                            'tradingname' => $payload['GeneralInformation']['BasicInformation']['TradingName'],
                            'dateofregistration' => $payload['GeneralInformation']['BasicInformation']['DateOfRegistration'],
                            'numberofemployees' => $payload['GeneralInformation']['BasicInformation']['NumberOfEmployees'],
                            'registrationstatuscode' => $payload['GeneralInformation']['BasicInformation']['RegistrationStatus']['Code'],
                            'registrationstatusreason' => $payload['GeneralInformation']['BasicInformation']['RegistrationStatus']['Reason'],
                            'plotnumber' => $payload['GeneralInformation']['AddressInformation']['PlotNumber'],
                            'blocknumber' => $payload['GeneralInformation']['AddressInformation']['BlockNumber'],
                            'street' => $payload['GeneralInformation']['AddressInformation']['Street'],
                            'postaladdress' => $payload['GeneralInformation']['AddressInformation']['PostalAddress'],
                            'postalcity' => $payload['GeneralInformation']['AddressInformation']['PostalCity'],
                            'postalcode' => $payload['GeneralInformation']['AddressInformation']['PostalCode'],
                            'district' => $payload['GeneralInformation']['AddressInformation']['District'],
                            'region' => $payload['GeneralInformation']['AddressInformation']['Region'],
                            'telephone1' => $payload['GeneralInformation']['ContactInformation']['Tel'],
                            'telephone2' => $payload['GeneralInformation']['ContactInformation']['Tel2'],
                            'mobile' => $payload['GeneralInformation']['ContactInformation']['Mobile'],
                            'email' => $payload['GeneralInformation']['ContactInformation']['Email'],
                            'fax' => $payload['GeneralInformation']['ContactInformation']['Fax'],
                            'businessactivitiescode' => $payload['BusinessActivities'][0]['Code'],
                            'businessactivitiesdescription' => $payload['BusinessActivities'][0]['Description'],
                            'ismain' => $payload['BusinessActivities'][0]['IsMain'],
                            'businesssectorcode' => $payload['BusinessSector']['Code'],
                            'businesssectordescription' => $payload['BusinessSector']['Description'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ],
                    ]);
                }
            }
            //Upon successive insert, update the last fetch date
            $sysdef = sysdefs()->data();
            $sysdef->last_taxpayer_fetch_date = $next_taxpayer_fetch_date->format("Ymd");
            $sysdef->save();
        }
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
