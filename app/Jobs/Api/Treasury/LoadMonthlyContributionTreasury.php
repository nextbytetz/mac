<?php

namespace App\Jobs\Api\Treasury;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use function \FluidXml\fluidxml;
use GuzzleHttp\Exception\ConnectException as ConnectException;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use App\Repositories\Backend\Sysdef\DateRangeRepository;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\Backend\Api\GotIntegrationRepository;

class LoadMonthlyContributionTreasury implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

   /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 36800;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
     //   
    }


    /**
     * Execute the job.
     *
     * @return void
     */
     public function handle()
    {
        
        $monthly_contributions = new GotIntegrationRepository();
        $period=$monthly_contributions->returnNextCheckdate();
        $loademployees =$monthly_contributions->contributionHeader($period);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }
}
