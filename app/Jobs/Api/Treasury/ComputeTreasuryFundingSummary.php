<?php

namespace App\Jobs\Api\Treasury;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Repositories\Backend\Api\GotIntegrationRepository;

class ComputeTreasuryFundingSummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

   /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */

    public $timeout =7200;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
       
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $GotFundSummary = new GotIntegrationRepository();
        $fundingsummary = $GotFundSummary->saveContributionSummary();
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }
}
