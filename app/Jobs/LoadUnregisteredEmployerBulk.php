<?php

namespace App\Jobs;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Services\Scopes\IsactiveScope;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exceptions\JobException;

class LoadUnregisteredEmployerBulk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */


    protected $chunkSize = 100;

    /**
     * @var
     */
    protected $unregisteredEmployer;

    /**
     * @var
     * is active flag for unregistered employers; 1 - Activate, 0 - Deactivate
     */
    protected $isactive;

    public function __construct($isactive)
    {
        //
        $this->unregisteredEmployer = new UnregisteredEmployerRepository();
        $this->isactive = $isactive;
        $this->handle();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $unregisteredEmployer = $this->unregisteredEmployer;
        $file_extension = request()->file('linked_file')->getClientOriginalExtension();
        $linked_file = unregistered_employer_bulk_dir() . DIRECTORY_SEPARATOR . Carbon::parse('now')->format('M') . '_' . Carbon::parse('now')->format('Y') . '.' . $file_extension;

        Excel::load($linked_file, function($reader)  {
            $objWorksheet = $reader->getActiveSheet();

        });

        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($linked_file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['name','tin','doc','business_activity','sector', 'no_of_employees','phone','telephone','email','p_o_box','region', 'postal_city', 'region', 'street', 'plot_no', 'block_no'];

        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new JobException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */

        /** start : Uploading excel data into the database */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($linked_file)
            ->chunk($this->chunkSize, function($result) use ($unregisteredEmployer) {
                $rows = $result->toArray();
                //let's do more processing (change values in cells) here as needed
                //$counter = 0;

                foreach ($rows as $row) {
                    /* start: Validating row entries */
                    $error_report = "";
                    $error = 0;

                    foreach ($row as $key => $value) {
                        if (trim($key) == 'doc') {
                            try {
                                $row['doc'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['doc'], 'YYYY-MM-DD');
                                /* Check if date of commencement is formatted successfully */
                                if($row['doc'] != Carbon::parse($row['doc'])->format('Y-m-d'))  {
                                    $row['doc'] = null;
                                };
                            } catch (\Exception $e) {
                                // an error occurred
                                $row['doc'] = null;
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";

                            }

                        } elseif (trim($key) == 'name') {
                            if (trim($value) == "") {
                                $row['name'] = null;
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                            }
                        } elseif (trim($key) == 'email') {
                            if (!filter_var(trim($value), FILTER_VALIDATE_EMAIL)) {
                                $row['email'] = null;
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                            }

                        }elseif (trim($key) == 'phone') {
                            try {
                                phone_255(trim($value));
                            } catch (\Exception $e) {
                                // an error occurred
                                $row['phone'] = null;
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                            }

                        }elseif (trim($key) == 'telephone') {
                            try {
                                phone_255(trim($value));
                            } catch (\Exception $e) {
                                // an error occurred
                                $row['telephone'] = null;
                                $error = 1;
                                $error_report = $error_report . trans('exceptions.backend.upload.entries', ['column' => "'" . $key . "'", 'entry' => $value]) . ", \r\n";
                            }

                        }

                    }
                    /* end: Validating row entries */

                    /* Get Region id from Region and Postal city*/
                    $regions = new RegionRepository();
                    $region = $regions->query()->where(function ($query) use ($row){
                        $query->where(DB::raw("LOWER(regexp_replace(" . DB::raw('name') . " , '[^a-zA-Z]', '', 'g'))"),'=',$string = preg_replace('/[^A-Za-z]/', '', strtolower($row['region'])))->orWhere(DB::raw("LOWER(regexp_replace(" . DB::raw('name') . " , '[^a-zA-Z]', '', 'g'))"),'=',$string = preg_replace('/[^A-Za-z]/', '', strtolower($row['postal_city'])) );
                    })->first();
                    /* Check if Tin already exist in the employer*/
                    $employer = new EmployerRepository();
                    $check_tin = $employer = $employer->checkIfTinAlreadyExist($row['tin'])['tin_employer'];
                    /* Insert into unregistered employers Table */
                    if (isset($row['name']) && !($check_tin)->isNotEmpty()) {
                        $unregistered_employer =  $unregisteredEmployer->query()->withoutGlobalScopes([IsactiveScope::class])->firstOrCreate(['tin' => isset($row['tin']) ? preg_replace('/[^A-Za-z0-9]/', '', $row['tin']) : null], ['name' => $row['name'], 'doc' => $row['doc'], 'business_activity' => $row['business_activity'], 'address' => $row['p_o_box'], 'street' => $row['street'], 'phone' =>  isset($row['phone']) ?  phone_255($row['phone'])  : null, 'telephone' => isset($row['telephone']) ?  phone_255($row['telephone'])  : null, 'email' => $row['email'], 'postal_city' => $row['postal_city'], 'location' => $row['region'], 'plot_number' => $row['plot_no'], 'block_no' => $row['block_no'],'sector' => $row['sector'], 'no_of_employees' => $row['no_of_employees'], 'region_id' => isset($region->id) ? $region->id : null  ]);

                        /* Update: Only for unassigned employers */
                        if($unregistered_employer->assign_to < 1 and $unregistered_employer->is_registered == 0){
                            /* Activate / Deactivate */
                            $unregistered_employer_updated =  $unregistered_employer->update(['tin' => isset($row['tin']) ? preg_replace('/[^A-Za-z0-9]/', '', $row['tin']) : null,'name' => $row['name'], 'doc' => $row['doc'], 'business_activity' => $row['business_activity'], 'address' => $row['p_o_box'], 'street' => $row['street'], 'phone' =>  isset($row['phone']) ?  phone_255($row['phone'])  : null, 'telephone' => isset($row['telephone']) ?  phone_255($row['telephone'])  : null, 'email' => $row['email'], 'postal_city' => $row['postal_city'], 'location' => $row['region'], 'plot_number' => $row['plot_no'], 'block_no' => $row['block_no'] ,'sector' => $row['sector'], 'no_of_employees' => $row['no_of_employees'], 'region_id' => isset($region->id) ? $region->id : null, 'isactive' => $this->isactive ]);

//                            $unregistered_employer_updated->update(['isactive' => $this->isactive]);
                        }

                    }

                }
            }, true);
        /** end : Uploading excel data into the database */

    }
}
