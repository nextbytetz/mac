<?php

namespace App\Jobs\Sockets;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Redis as LRedis;

class SendSocketNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $users;

    protected $message;

    /**
     * Create a new job instance.
     *
     * SendSocketNotification constructor.
     * @param $message
     * @param array $users
     */
    public function __construct($message, array $users = [])
    {
        $this->users = $users;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $redis = LRedis::connection();
        $redis->publish('message', json_encode(['text' => $this->message, 'users' => $this->users]));
    }
}
