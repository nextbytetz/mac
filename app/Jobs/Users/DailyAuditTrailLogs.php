<?php

namespace App\Jobs\Users;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\Backend\Access\UserRepository;
use Carbon\Carbon;

class DailyAuditTrailLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Daily job to export audit trails to auditor server.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // export of the logs happens here
        $daily_date=Carbon::today()->format('Y-m-d');
        $user_logs = new UserRepository();
        $export_logs=$user_logs->exportDailyAuditLogs();
    }
}
