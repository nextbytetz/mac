<?php

namespace App\Jobs\Workflow;

use App\Repositories\Backend\Workflow\WfTrackRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CloseLastLevel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $module;

    /**
     * @var
     */
    protected $user;

    /**
     * Create a new job instance.
     *
     * CloseLevel constructor.
     * @param $module
     * @param $user
     */
    public function __construct($module, $user)
    {
        $this->module = $module;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $wfTrackRepo = new WfTrackRepository();
        $wfTrackRepo->closeLastWorkflow($this->module, $this->user, "Obsolete Workflow Level");
    }
}
