<?php

namespace App\Jobs\Workflow;

use App\Repositories\Backend\Workflow\WfTrackRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TransferIntermediateWorkflow implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $fromModule;

    /**
     * @var
     */
    protected $toModule;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fromModule, $toModule)
    {
        $this->fromModule = $fromModule;
        $this->toModule = $toModule;
    }

    /**
     * Execute the job.
     * @throws \App\Exceptions\GeneralException
     * @return void
     */
    public function handle()
    {
        $wfTrackRepo = new WfTrackRepository();
        $wfTrackRepo->transferIntermediateWorkflow($this->fromModule, $this->toModule);
    }
}
