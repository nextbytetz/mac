<?php

namespace App\Jobs\Finance;

use App\Services\Finance\FinYear;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ManageFinancialYear implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $finYear;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->finYear = new FinYear();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->finYear->createNew();
    }
}
