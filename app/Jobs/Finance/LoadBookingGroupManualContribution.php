<?php

namespace App\Jobs\Finance;

use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\BookingGroupRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Exceptions\JobException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Finance\Receipt\LegacyReceiptTemp;

class LoadBookingGroupManualContribution implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 1000;

    protected $employer_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->employer_id = $id;
    }

    /**
     * @throws JobException
     * @throws \App\Exceptions\GeneralException
     */
    public function handle()
    {
        $file = contribution_dir() . DIRECTORY_SEPARATOR . 'manual_contribution' . DIRECTORY_SEPARATOR . 'booking_group' . DIRECTORY_SEPARATOR . $this->employer_id;
        $repo = new BookingGroupRepository();
        $employerRepo = new EmployerRepository();
        $legacyReceiptRepo = new LegacyReceiptRepository();
        $group = $repo->query()->where("employer_id", $this->employer_id)->first();
        $hasError = 0;
        LegacyReceiptTemp::query()->delete();
        Excel::load($file, function($reader) use ($group) {
            $objWorksheet = $reader->getActiveSheet();
            //exclude the heading
            $total_rows = $objWorksheet->getHighestRow() - 1;
            $group->manual_receipt_file_total = $total_rows;
            $group->manual_receipt_file_uploaded = 0;
            $group->upload_error = NULL;
            $group->isuploaded = 0;
            $group->error = 0;
            $group->hasfileerror = 0;
            $group->save();
        });
        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['votecode','employer','salary','contribution','contrib_month','rctno','member_count'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new JobException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */
        $group->refresh();
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($file)
            ->chunk($this->chunkSize, function($result) use ($group, $employerRepo, $legacyReceiptRepo, $hasError) {
                $rows = $result->toArray();
                foreach ($rows as $row) {
                    /* Changing date from excel format to unix date format ... */
                    $row['contrib_month'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['contrib_month'], 'YYYY-MM-DD');
                    $row['votecode'] = single_space(str_replace("'","", $row['votecode']));
                    $row['contribution'] = single_space($row['contribution']);
                    $employer = $employerRepo->query()->select(['id'])->where("vote", $row['votecode'])->first();

                    $hasError = 0;
                    $error  = "";
                    $data = [
                        'amount' => $row['contribution'],
                        'rctno' => $row['rctno'],
                        'rct_date' => $row['contrib_month'],
                        'member_count' => $row['member_count'],
                        'user_id' => 13,
                        'created_at' => Carbon::now(),
                        'currency_id' => 1,
                        'fin_code_id' => 2,
                        'iscomplete' => 1,
                        'isuploaded' => 1,
                    ];
                    switch (true) {
                        case empty($row['contribution']):
                            $error = "No contribution";
                            $hasError = 1;
                            break;
                        case $row['contribution'] == "-":
                            $error = "Invalid Contribution entry, contains -";
                            $hasError = 1;
                            break;
                        case !is_numeric($row['contribution']):
                            $error = "Invalid Contribution entry, contains text";
                            $hasError = 1;
                            break;
                        case !is_numeric($row['member_count']):
                            $error = "Invalid member count entry, contains text";
                            $hasError = 1;
                            break;
                    }
                    if ($employer) {
                        $data['employer_id'] = $employer->id;
                    } else {
                        $error = "Votecode not registered";
                        $hasError = 1;
                        $data['employer_id'] = NULL;
                    }
                    if ($hasError) {
                        $group->refresh();
                        $group->hasfileerror = 1;
                        $group->save();
                        $data['haserror'] = 1;
                        $data['error'] = $error;
                        LegacyReceiptTemp::query()->updateOrCreate([
                            'votecode' => $row['votecode'],
                            'contrib_month' => $row['contrib_month']
                        ], $data);
                    } else {
                        if ($employer) {
                            $legacyReceiptRepo->query()->updateOrCreate([
                                'employer_id' => $employer->id,
                                'contrib_month' => $row['contrib_month']
                            ], $data);
                            $group->refresh();
                            $nexttotal = $group->manual_receipt_file_uploaded;
                            $group->manual_receipt_file_uploaded = $nexttotal + 1;
                            $group->save();
                        }
                    }

                }
            }, true);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {
        $repo = new BookingGroupRepository();
        $group = $repo->query()->where("employer_id", $this->employer_id)->first();
        $group->manual_receipt_file_total = 0;
        $group->manual_receipt_file_uploaded = 0;
        $group->upload_error = $e->getMessage();
        $group->isuploaded = 0;
        $group->hasfileerror = 0;
        $group->error = 1;
        $group->save();
    }

}
