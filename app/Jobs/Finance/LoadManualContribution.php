<?php

namespace App\Jobs\Finance;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Exceptions\JobException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LoadManualContribution implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Number of chunks for uploading a excel file.
     *
     * @var int
     */
    protected $chunkSize = 1000;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @throws JobException
     */
    public function handle()
    {
        $file = contribution_dir() . DIRECTORY_SEPARATOR . 'manual_contribution' . DIRECTORY_SEPARATOR . 'file';
        $sysdef = sysdefs()->data();
        Excel::load($file, function($reader) use ($sysdef) {
            $objWorksheet = $reader->getActiveSheet();
            //exclude the heading
            $total_rows = $objWorksheet->getHighestRow() - 1;
            $sysdef->manual_receipt_file_total = $total_rows;
            $sysdef->manual_receipt_file_uploaded = 0;
            $sysdef->save();
        });

        DB::table('legacy_receipts')->where('isuploaded', 1)->delete();

        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['rct_date','rct_no','employer_name','employer_id','contrib_month','amount'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new JobException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */

        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($file)
            ->chunk($this->chunkSize, function($result) use ($sysdef) {
                $rows = $result->toArray();
                foreach ($rows as $row) {
                    /* Changing date from excel format to unix date format ... */
                    $row['rct_date'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['rct_date'], 'YYYY-MM-DD');
                    $row['contrib_month'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['contrib_month'], 'YYYY-MM-DD');
                    /* start: Validating row entries */

                    //$check_contribution = DB::table('legacy_receipts')->select('id')->where('rctno', $row['rct_no'])->count();
                    if (true) {
                        DB::table('legacy_receipts')->insert(
                            ['rctno' => $row['rct_no'], 'rct_date' => $row['rct_date'], 'amount' => $row['amount'], 'contrib_month' => $row['contrib_month'], 'employer_id' => $row['employer_id'], 'user_id' => 13, 'created_at' => Carbon::now(), 'currency_id' => 1, 'fin_code_id' => 2, 'iscomplete' => 1, 'isuploaded' => 1 ]
                        );
                    }
                    $sysdef->refresh();
                    $nexttotal = $sysdef->manual_receipt_file_uploaded;
                    $sysdef->manual_receipt_file_uploaded = $nexttotal + 1;
                    $sysdef->save();
                }
            }, true);

    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {
        $sysdef = sysdefs()->data();
        $sysdef->manual_receipt_file_total = 0;
        $sysdef->save();
    }

}
