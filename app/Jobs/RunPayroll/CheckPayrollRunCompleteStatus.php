<?php

namespace App\Jobs\RunPayroll;

use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckPayrollRunCompleteStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payroll_run_approval_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payroll_run_approval_id)
    {
        //
        $this->payroll_run_approval_id = $payroll_run_approval_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $payroll_run_approval_id = $this->payroll_run_approval_id;
        $payroll_run_approvals = new PayrollRunApprovalRepository();
        $payroll_run_approval = $payroll_run_approvals->find($payroll_run_approval_id);
        $status = 0;
        while($status == 0){
            sleep(5);
            $percentage = $payroll_run_approval->calculatePayrollCompletePercentage();
            $status = ($percentage == 100) ? 1 : 0;
            /*check if is complete*/
            if($status == 1){
                $payroll_run_approvals->updateAfterCompletion($payroll_run_approval_id);
                /*Notify process is complete*/
            }
            sleep(15);
        }
    }
}
