<?php

namespace App\Jobs\RunPayroll;

use App\Services\Payroll\ProcessPayroll;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ProcessDependents implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $payroll_run_approval_id;

    protected $employees;

    protected $months_paid;

    protected $run_date;

    protected $action_type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employees, $payroll_run_approval_id, $months_paid, $run_date, $action_type)
    {
        //
        $this->employees = $employees;
        $this->payroll_run_approval_id = $payroll_run_approval_id;
        $this->months_paid = $months_paid;
        $this->run_date = $run_date;
        $this->action_type = $action_type;
//        $this->handle();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $payroll_run_approval_id = $this->payroll_run_approval_id;
        $months_paid = $this->months_paid;
        $run_date = $this->run_date;
        $employees = $this->employees;
        $action_type = $this->action_type;
        $process_payroll = new ProcessPayroll();
        $process_payroll->processDependents($employees, $payroll_run_approval_id, $months_paid, $run_date, $action_type);
    }
}
