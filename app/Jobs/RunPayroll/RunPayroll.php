<?php

namespace App\Jobs\RunPayroll;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Payroll\ProcessPayroll;

class RunPayroll implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected  $input;

    protected $payroll_run_approval_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payroll_run_approval_id)
    {
        $this->payroll_run_approval_id = $payroll_run_approval_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $payroll_run_approval_id = $this->payroll_run_approval_id;
        $process_payroll = new ProcessPayroll();
        $process_payroll->processPayroll($payroll_run_approval_id);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
