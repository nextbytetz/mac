<?php

namespace App\Jobs\RunPayroll\Suspension;

use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollSystemSuspensionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SystemSuspensions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $member_type_id;
    protected $beneficiaries;
    protected $payroll_manual_suspension_id;
    /**
     * Create a new job instance.
     * @return void
     */
    public function __construct($beneficiaries, $member_type_id, $payroll_manual_suspension_id = null)
    {
        //
        $this->beneficiaries = $beneficiaries;
        $this->member_type_id = $member_type_id;
        $this->payroll_manual_suspension_id = $payroll_manual_suspension_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $system_suspension_repo = new PayrollSystemSuspensionRepository();
        $beneficiaries = $this->beneficiaries;
        $member_type_id = $this->member_type_id;
        $payroll_manual_suspension_id = $this->payroll_manual_suspension_id;
        foreach($beneficiaries as $resource) {
            $system_suspension_repo->create($resource->id, $member_type_id, $payroll_manual_suspension_id);
        }
    }
}
