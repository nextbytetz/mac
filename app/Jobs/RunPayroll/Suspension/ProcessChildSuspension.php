<?php

namespace App\Jobs\RunPayroll\Suspension;

use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollChildSuspensionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ProcessChildSuspension implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $child_dependents;
    protected $iseducation;
    protected $reason;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dependents, $iseducation = 0, $reason = null)
    {
        //
        $this->child_dependents = $dependents;
        $this->iseducation = $iseducation;
        $this->reason = $reason;
//        Log::info(print_r($this->child_dependents,true));
        $this->handle();
          }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $child_suspension_repo = new PayrollChildSuspensionRepository();
//        $dependents = $this->child_dependents;
        $iseducation = $this->iseducation;

        foreach($this->child_dependents as $resource) {
                  $input = ['dependent_id'=> $resource->id, 'iseducation' => $iseducation, 'reason' => $this->reason];
            $child_suspension_repo->create($input);
        }
    }
}
