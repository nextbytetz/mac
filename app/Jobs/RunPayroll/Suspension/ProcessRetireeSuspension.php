<?php

namespace App\Jobs\RunPayroll\Suspension;

use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollRetireeSuspensionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessRetireeSuspension implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $pensioners;
    protected $isvoluntary;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($pensioners, $isvoluntary)
    {
        //
        $this->pensioners = $pensioners;
        $this->isvoluntary = $isvoluntary;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $retiree_suspension_repo = new PayrollRetireeSuspensionRepository();
        $pensioners = $this->pensioners;
        $isvoluntary = $this->isvoluntary;
        foreach($pensioners as $resource) {
            $input = ['pensioner_id'=> $resource->id,'isvoluntary' => $isvoluntary];
            $retiree_suspension_repo->create($input);
        }
    }
}
