<?php

namespace App\Jobs\RunPayroll\PostEoffice;

use App\Exceptions\JobException;
use App\Services\Notifications\Api;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;


class PostBeneficiaryToDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Api;


    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * @var beneficiary
     */
    protected $beneficiary;

    /**
     * @var member_type_id
     */
    protected $member_type_id;

    protected $employee_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($beneficiary, $member_type_id, $employee_id)
    {
        $this->beneficiary = $beneficiary;
        $this->member_type_id = $member_type_id;
        $this->employee_id = $employee_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $fileType = ($this->member_type_id == 5) ? 6 : 7; //Dependents/pensioners
        $classification = 1;
        $fileNumber = $this->beneficiary->id;
        $fileName =  $this->beneficiary->filename_dms;
        $department = 8;
        $subject = ($this->member_type_id == 4) ? $this->beneficiary->getFileSubjectAttribute($this->employee_id) : $this->beneficiary->file_subject;
        $description = "";
        $url = env('EOFFICE_APP_URL') . "/api/claim/add";
        $postFields = "{ \"fileType\":\"{$fileType}\", \"classification\":\"{$classification}\", \"fileNumber\":\"{$fileNumber}\", \"fileName\":\"{$fileName}\", \"department\":\"{$department}\", \"subject\":\"{$subject}\", \"description\":\"{$description}\" }";
        $response = $this->sendPostJsonEoffice($url, $postFields);
        $data = json_decode($response);
        if (isset($data)) {
            if ($data->message == "SUCCESS") {
                //file posted successful
                $this->beneficiary->isdmsposted = 1;
                $this->beneficiary->save();
            }
        } else {
            throw new JobException("Failed to post Benecificary file");
        }
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
