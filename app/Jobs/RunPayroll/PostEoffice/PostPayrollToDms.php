<?php

namespace App\Jobs\RunPayroll\PostEoffice;

use App\Exceptions\JobException;
use App\Services\Notifications\Api;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;


class PostPayrollToDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Api;


    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * @var beneficiary
     */
    protected $payroll_run_approval;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payroll_run_approval)
    {
        $this->payroll_run_approval = $payroll_run_approval;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fileType = 4;
        $classification = 1;
        $fileNumber = $this->payroll_run_approval->id;
        $fileName =  $this->payroll_run_approval->filename;
        $department = 8;
        $subject = $this->payroll_run_approval->file_subject;
        $description = $this->payroll_run_approval->file_description;
        $url = env('EOFFICE_APP_URL') . "/api/claim/add";
        $postFields = "{ \"fileType\":\"{$fileType}\", \"classification\":\"{$classification}\", \"fileNumber\":\"{$fileNumber}\", \"fileName\":\"{$fileName}\", \"department\":\"{$department}\", \"subject\":\"{$subject}\", \"description\":\"{$description}\" }";
        $response = $this->sendPostJsonEoffice($url, $postFields);
                $data = json_decode($response);
        if (isset($data)) {
            if ($data->message == "SUCCESS") {
                //file posted successful
                $this->payroll_run_approval->isdmsposted = 1;
                $this->payroll_run_approval->save();
            }
        } else {
            throw new JobException("Failed to post payroll file");
        }
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
