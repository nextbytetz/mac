<?php

namespace App\Jobs\RunPayroll\PostEoffice;

use App\Exceptions\JobException;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Services\Notifications\Api;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;


class PostBulkBeneficiariesToDms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Api;


    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * @var beneficiary
     */
    protected $beneficiaries;

    /**
     * @var member_type_id
     */
    protected $member_type_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($beneficiaries, $member_type_id)
    {
        $this->beneficiaries = $beneficiaries;
        $this->member_type_id = $member_type_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $member_type_id = $this->member_type_id;
        $beneficiaries = $this->beneficiaries;
        foreach($beneficiaries as $beneficiary){
            $employee_id = $beneficiary->employee_id;
            /*Beneficiary resource_*/
            $beneficiary_resource = $this->getResource($member_type_id,$beneficiary);
            dispatch(new PostBeneficiaryToDms($beneficiary_resource, $member_type_id, $employee_id));
        }
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }


    /**
     * @param $member_type_id
     * @param $beneficiary
     * @return mixed
     * Get beneficiary resource
     */
    public function getResource($member_type_id, $beneficiary)
    {
        $payroll_repo = new PayrollRepository();
        if($member_type_id == 5){
            /*pensioners*/
            $resource = $payroll_repo->getResource($member_type_id, $beneficiary->id);
        }else{
            /*dependents*/
            $resource = $payroll_repo->getResource($member_type_id, $beneficiary->dependent_id);
        }
        return $resource;
    }



}
