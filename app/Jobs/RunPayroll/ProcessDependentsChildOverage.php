<?php

namespace App\Jobs\RunPayroll;

use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Services\Payroll\ProcessPayroll;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ProcessDependentsChildOverage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $payroll_run_approval_id;




    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payroll_run_approval_id)
    {
        //

        $this->payroll_run_approval_id = $payroll_run_approval_id;


//        $this->handle();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $eligible_children_overage = (new DependentRepository())->getEligibleChildrenOverageForPayroll()->get();
        foreach ($eligible_children_overage as $dependent_pivot){
            $dependent_id = $dependent_pivot->dependent_id;
            $employee_id = $dependent_pivot->employee_id;
            $payroll_run_approval_id = $this->payroll_run_approval_id;
            (new ProcessPayroll())->processArrearsForChildOverageEligible($dependent_id, $employee_id,$payroll_run_approval_id);
        }
    }
}
