<?php

namespace App\Jobs\RunPayroll\Payment;

use App\Http\Requests\Backend\Operation\Payroll\PayrollRunRequest;
use App\Models\Operation\Claim\ClaimCompensation;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\MacErp\ClaimsErpApiRepositroy;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Payroll\ProcessPayroll;

class ProcessPayrollVouchers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $payroll_run_approval_id;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payroll_run_approval_id)
    {
        $this->payroll_run_approval_id = $payroll_run_approval_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $claim_erp = new ClaimsErpApiRepositroy();
        $payroll_run_approval_id = $this->payroll_run_approval_id;
        $claim_erp->postApErpApiPensionPayroll($payroll_run_approval_id);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
