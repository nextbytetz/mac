<?php

namespace App\Jobs\RunPayroll;

use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollReconciliationRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class PayrollReconciliations implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $beneficiaries;
    protected $member_type_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($beneficiaries, $member_type_id)
    {
        //
        $this->beneficiaries = $beneficiaries;
        $this->member_type_id = $member_type_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $payroll_reconciliations = new PayrollReconciliationRepository();
        $beneficiaries = $this->beneficiaries;
        $member_type_id = $this->member_type_id;
        if(($beneficiaries->isNotEmpty())){
            /*process reconciliation for each beneficiary*/
            foreach ($beneficiaries as $beneficiary){
                $resource_id = ($member_type_id == 4) ? $beneficiary->dependent_id : $beneficiary->id;
                $payroll_reconciliations->processPayrollReconciliation($member_type_id, $resource_id, $beneficiary->notification_report_id);
            }
        }
    }
}
