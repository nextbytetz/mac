<?php

namespace App\Jobs\RunPayroll;

use App\Repositories\Backend\Operation\Payroll\PayrollAlertTaskRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PostPayrollAlertTaskChecker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $staging_cv_ref;
    protected $member_type_id;
    protected $resource_id;
    protected $isclose = 0;
    protected $opt_input = [];
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($staging_cv_ref, $member_type_id, $resource_id, $isclose = 0,array $opt_input = [])
    {
        //
        $this->staging_cv_ref = $staging_cv_ref;
        $this->member_type_id = $member_type_id;
        $this->resource_id = $resource_id;
        $this->isclose = $isclose;
        $this->opt_input = $opt_input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        if($this->isclose == 0){
            /*save new*/
            (new PayrollAlertTaskRepository())->savePayrollAlertTaskIndividual($this->staging_cv_ref,$this->member_type_id, $this->resource_id, $this->opt_input);
        }else{
            /*save task*/
            (new PayrollAlertTaskRepository())->closePayrollAlertCheckerTask($this->member_type_id, $this->resource_id, $this->staging_cv_ref);
        }
    }


}
