<?php

namespace App\Jobs\RunPayroll;

use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UndoPayrollRunApproval implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $payroll_run_approval_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payroll_run_approval_id)
    {
        //
        $this->payroll_run_approval_id = $payroll_run_approval_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $payroll_run_approvals = new PayrollRunApprovalRepository();
        $payroll_run_approval_id = $this->payroll_run_approval_id;
        $payroll_run_approvals->undoRunApproval($payroll_run_approval_id);
    }
}
