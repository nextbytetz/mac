<?php

namespace App\Jobs\Notifications;

use App\Services\Notifications\Msdgapi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSmsEga implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $phone;

    /**
     * @var
     */
    protected $msg;

    /**
     * Create a new job instance.
     *
     * SendSmsEga constructor.
     * @param $phone
     * @param $msg
     */
    public function __construct($phone, $msg)
    {
        $this->phone = $phone;
        $this->msg = $msg;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $msdg = new Msdgapi();
        $phone = str_replace("+255", "0", $this->phone);
        $datetime = date('Y-m-d H:i:s');
        $message  = array('message' =>  $this->msg,'datetime' => $datetime, 'sender_id' => 'WCF', 'mobile_service_id' => '222', 'recipients'=> $phone);
        $json_data = json_encode($message);
        $msdg->sendQuickSms(array('data' => $json_data, 'datetime' => $datetime));
    }
}
