<?php

namespace App\Jobs;

use App\Repositories\Backend\Operation\Compliance\Member\MemberRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class RegisterContribution implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $receiptCode;

    protected $user_id;

    protected $contribution_temps;

    protected $chunkSize = 100;

    public function __construct(ReceiptCode $receiptCode, $user_id)
    {
        $this->receiptCode = $receiptCode;
        $this->user_id = $user_id;

    }

    /**
     * Execute the job.
     *
     * @param ContributionRepository $contribution
     */
    public function handle(ContributionRepository $contribution)
    {

        $receipt_code_id = null;
        //$employee = new EmployeeRepository();
        /* To be exchanged with employee repository later ... */
        $employee = new EmployeeRepository();
        $receiptCode = $this->receiptCode;
        $linked_file = $receiptCode->linkedFile();
        $user_id = $this->user_id;
        $contribPercent = $receiptCode->contributionPercent();
        /* Select all contributions for this receipt code */
        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($this->receiptCode->linkedFile())
            ->chunk($this->chunkSize, function ($result) use ($receiptCode, $contribPercent, $employee, $user_id, $contribution) {
                $rows = $result->toArray();
                //let's do more processing (change values in cells) here as needed
                //$counter = 0;
                foreach ($rows as $row) {
                    /* Loop through all contribution temps for the receipt and insert in the contributions table */
                    $firstname = strtolower($row['firstname']);
                    $lastname = strtolower($row['lastname']);
                    $row['dob'] = \PHPExcel_Style_NumberFormat::toFormattedString($row['dob'], 'YYYY-MM-DD');
                    $dob = $row['dob'];
                                        $allowance = $row['grosspay'] - $row['basicpay'];
                    $check = $employee->query()->whereRaw("levenshtein(lower(firstname), :firstname) between 0 and 1 and levenshtein(lower(lastname), :lastname) between 0 and 1 and dob = :dob", ['firstname' => $firstname, 'lastname' => $lastname, 'dob' => $dob])->first();
                    if (!$check) {
                        /* MAC Data */
                        /* Old MAC Data */
                        $data = ['firstname' => $row['firstname'], 'middlename' => $row['middlename'], 'lastname' => $row['lastname'], 'dob' => $row['dob'], 'memberno' => 0, 'user_id' => $user_id];
                        // new employee, create first
//                    $contribEmployee = $employee->query()->create($data);
                        /* create using PDO - Raw*/
                        $employee_id = DB::table('employees')->insertGetId(
                            $data
                        );
                        /* to change member_no name index in the new mac operation */
                        $memberno = checksum($employee_id, sysdefs()->data()->employee_number_length);
                        /*update memberno*/
                        DB::table('employees')
                            ->where('id', $employee_id)
                            ->update(['memberno' => $memberno]);
                    } else {
                        // existing employee
//                    $contribEmployee = $check;
                        $employee_id = $check->id;
                    }

                    /* MAC Data */
                    $employer_id = $receiptCode->receipt->employer_id;
                    $data = ['receipt_code_id' => $receiptCode->id, 'employee_amount' => $contribPercent * $row['grosspay'], 'grosspay' => $row['grosspay'], 'employee_id' => $employee_id, 'user_id' => $receiptCode->receipt->user_id, 'salary' => $row['basicpay'], 'employer_id' => $employer_id];

                    /* start check if contribution already exist*/
                    $check_contribution = DB::table('contributions')->select('id')->where('employee_id', $employee_id)->where('receipt_code_id', $receiptCode->id)->first();
                    /*end checking*/


                    /*Insert using PDO -Raw*/
                    if(!$check_contribution){
                        DB::table('contributions')->insert($data);
                    }

                    $contribEmployee = $employee->findOrThrowException($employee_id);
                        $contribEmployee->employers()->syncWithoutDetaching(([$employer_id => ['basicpay' => $row['basicpay'], 'grosspay' => $row['grosspay']]]));

                }
                /* Set all contributions to uploaded status */
                $receiptCode->update(['isuploaded' => 1]);

            }, true);

    }

    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {

    }

}