<?php

namespace App\Jobs\ProcessPayment;

use App\Http\Requests\Backend\Operation\Payroll\PayrollRunRequest;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Payroll\ProcessPayroll;

class ProcessPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $user_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user_id = $this->user_id;
        $process_payment = new \App\Services\Finance\ProcessPayment();
        $process_payment->processPayment($user_id);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
