<?php

namespace App\Jobs\ProcessPayment;

use App\Http\Requests\Backend\Operation\Payroll\PayrollRunRequest;
use App\Models\Operation\Claim\ClaimCompensation;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Payroll\ProcessPayroll;

class ProcessFuneralGrant implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $user_id;

    protected $funeral_grants;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($funeral_grants, $user_id)
    {
        $this->user_id = $user_id;
        $this->funeral_grants = $funeral_grants;
//        $this->handle();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user_id = $this->user_id;
        $funeral_grants = $this->funeral_grants;
        $payment_voucher_trans = new PaymentVoucherTransactionRepository();
        $payment_voucher_trans->processFuneralGrant($funeral_grants, $user_id);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
