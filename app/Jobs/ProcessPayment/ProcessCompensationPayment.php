<?php

namespace App\Jobs\ProcessPayment;

use App\Http\Requests\Backend\Operation\Payroll\PayrollRunRequest;
use App\Models\Operation\Claim\ClaimCompensation;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Payroll\ProcessPayroll;

class ProcessCompensationPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $user_id;

    protected $pending_compensations;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($pending_compensations, $user_id)
    {
        $this->user_id = $user_id;
        $this->pending_compensations = $pending_compensations;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pending_compensations = $this->pending_compensations;
        $user_id = $this->user_id;
        $payment_voucher_trans = new PaymentVoucherTransactionRepository();
        $payment_voucher_trans->processCompensationPayment($pending_compensations, $user_id);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
