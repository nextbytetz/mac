<?php

namespace App\Jobs\ProcessPayment;

use App\Http\Requests\Backend\Operation\Payroll\PayrollRunRequest;
use App\Models\Operation\Claim\ClaimCompensation;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Payroll\ProcessPayroll;

class ProcessSurvivorGratuity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $user_id;

    protected $survivor_gratuities;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($survivor_gratuities, $user_id)
    {
        $this->user_id = $user_id;
        $this->survivor_gratuities = $survivor_gratuities;
//        $this->handle();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user_id = $this->user_id;
        $survivor_gratuities = $this->survivor_gratuities;
        $payment_voucher_trans = new PaymentVoucherTransactionRepository();
        $payment_voucher_trans->processSurvivorGratuity($survivor_gratuities, $user_id);
    }

    /**
     * @param Exception $e
     */
    public function failed(Exception $e)
    {

    }

}
