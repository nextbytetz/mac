<?php

namespace App\Jobs\ProcessPayment;

use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessInterestRefund implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user_id;
    protected $interest_refunds;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($interest_refunds, $user_id)
    {
        $this->user_id = $user_id;
        $this->interest_refunds = $interest_refunds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $user_id = $this->user_id;
        $interest_refunds = $this->interest_refunds;
        $payment_voucher_trans = new PaymentVoucherTransactionRepository();
        $payment_voucher_trans->processInterestRefunds($interest_refunds, $user_id);
    }
}
