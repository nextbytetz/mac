<?php

namespace App\Events\Sockets\Notification;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AssignedInvestigation
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $users;

    /* notification_report_id */
    public $id;

    /**
     * Create a new event instance.
     *
     * AssignedInvestigation constructor.
     * @param array $users
     * @param $id
     */
    public function __construct(array $users = [], $id)
    {
        $this->users = $users;
        $this->id = $id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
