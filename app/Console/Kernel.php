<?php

namespace App\Console;

use App\Jobs\Api\Ades\LoadClosedBusinesses;
use App\Jobs\Api\Ades\LoadNewTaxPayers;
use App\Jobs\Claim\CheckUploadedDocuments;
use App\Jobs\Claim\RecheckPostingDocumentDms;
use App\Jobs\Claim\ClaimFollowupsJob;
use App\Jobs\Compliance\BookingReconciliation\BookingReconciliationJob;
use App\Jobs\Compliance\CategorizeEmployerContributors;
use App\Jobs\Compliance\ProcessBookingsForNonContributorsJob;
use App\Jobs\Compliance\StaffEmployerAllocation\StaffEmployerTasksJob;
use App\Jobs\Employee\RectifyEmployeeEmployerSyncOnUpload;
use App\Jobs\Finance\ManageFinancialYear;
use App\Jobs\PostInterestWithBoardAdjust;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollChildSuspensionRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollRetireeSuspensionRepository;
use App\Repositories\Backend\Reporting\ReportRepository;
use App\Services\Receivable\CreateBooking;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\PostInterest;
use App\Jobs\CalculateBooking;
use App\Jobs\Erp\BulkUpdateInvoicePaymentFromErp;
use App\Jobs\Api\Treasury\LoadEmployeeTreasury;
use App\Jobs\Api\Treasury\LoadMonthlyContributionTreasury;
use App\Jobs\Api\Treasury\ComputeTreasuryFundingSummary;
use Illuminate\Support\Facades\Log;
use App\Jobs\Users\DailyAuditTrailLogs;
use App\Jobs\Finance\ReconcileDailyGepgPayments;
use App\Services\Receivable\CalculateInterest;
use App\Jobs\InstalmentNotification;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\ClaimFollowupRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;
use App\Repositories\Backend\Operation\Claim\HspHcpBillingRepository;


class Kernel extends ConsoleKernel
{


    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\MofEmployees::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {


//        $schedule->call('App\Http\Controllers\Backend\Finance\GotIntergrationController@contributionHeader');


        $schedule->call(function () {
            $followup = new ClaimFollowupRepository();
            $followup->syncFolloups();
        })->monthly();

        // $schedule->call(function () {
        //     $accrual = new AccrualNotificationReportRepository();
        //     $is_accrual_date = $accrual->isAccrualDate();

        //     if($is_accrual_date){
        //         $accrual->accrualNotifications();
        //     }
        // })->monthly();


         // /*notify five days before commitment payment*/
         //    $schedule->call(function () {
         //        $commitment = new CalculateInterest();
         //        $commitment->commitmentNotify();
         //    })->dailyAt('08:00'); 


    if (env("RUN_CRON", 1)) {

        /*notify five days before instalment payment*/
        $schedule->call(function () {
            $instalment = new CalculateInterest();
            $instalment->instalmentNotify();
        })->dailyAt('08:00'); 

        /*post interest instalment*/
        $schedule->call(function () {
            $interest = new CalculateInterest();
            $interest->interestWithInstalments();
        })->dailyAt('21:00'); 

        $schedule->call(function () {
            $this->postInterestSchedule();
        })->dailyAt('21:30');

        $schedule->call(function () {
            $employerRepository = new EmployerRepository();
            $employerRepository->calculateBookingSchedule();
        })->monthlyOn(1, '01:00');

        $schedule->call(function () {
            dispatch(new LoadNewTaxPayers());
        })->weekly()->fridays()->at('01:00');

        $schedule->call(function () {
            dispatch(new LoadClosedBusinesses());
        })->weekly()->fridays()->at('01:00');

            //Manage Finance Years (Runs in the first 15 days of July at 6:00a.m)
        $schedule->call(function () {
            dispatch(new ManageFinancialYear());
        })->cron('0 6 1-15 7 * *');

        //Refresh Dashboard Statistics
        $schedule->call(function () {
            $reportRepo = new ReportRepository();
            $reportRepo->refreshStatitics();
        })->cron('0 6 * * * *');

        //Refresh Configurable Report
        $schedule->call(function () {
            $reportRepo = new ReportRepository();
            $reportRepo->refreshConfigurables();
        })->cron('0 17 * * * *');

        /*Refresh compliance reports - receivable reports */
        $schedule->call(function () {
            $reportRepo = new ReportRepository();
            $reportRepo->refreshComplianceReportsMaterializedViews();
        })->monthlyOn(1,'23:00');

        /*Update staff employer tasks */
        $schedule->call(function () {
            dispatch(new StaffEmployerTasksJob());
        })->dailyAt('23:50');

        /*Post staff employer follow up reminders target */
        $schedule->call(function () {
            (new StaffEmployerRepository())->dispatchFollowupReminderForStaffJob();
        })->dailyAt('00:05');

            //Check for ready documents for benefit processing
        $schedule->call(function () {
                //dispatch(new CheckUploadedDocuments());
            (new NotificationReportRepository())->checkUploadedDocuments();
        })->hourlyAt(17);

            //Check for verified online uploaded documents but not posted in eoffice
        $schedule->call(function () {
                //dispatch(new RecheckPostingDocumentDms());
                (new NotificationReportRepository())->recheckPostingDocumentDms();
                (new NotificationReportRepository())->recheckPostingBranchOnlineDocumentDms(); //TODO: to be enabled after updating the server ...
            })->everyThirtyMinutes();
            //everyTenMinutes

        /*System payroll child suspensions*/
        $schedule->call(function () {
            $repo = new PayrollChildSuspensionRepository();
            $repo->processChildLimitAgeSuspensionJob();
        })->dailyAt('03:00');


        /*Auto categorize employer contributors*/
        $schedule->call(function () {
            (new EmployerRepository())->categorizeEmployerContributors();
        })->weekends()->at('01:00');

        /*Update ERP Payment status*/
        $schedule->call(function () {
            dispatch(new BulkUpdateInvoicePaymentFromErp());
        })->hourly();

        /*Update employer status*/
        $schedule->call(function () {
            (new EmployerRepository())->updateEmployersStatus();
        })->dailyAt( '04:00');


        /*Update bookings not contributed yet*/
        $schedule->call(function () {
            (new EmployerRepository())->updateBookingsNotContributed();
        })->dailyAt('04:20');

////            /*Post bookings for skipped months*/
        $schedule->call(function () {
            (new EmployerRepository())->postBookingForSkippedMonths();
        })->dailyAt('04:45');
//
//php
//            /*Booking reconciliation*/
        $schedule->call(function () {
            dispatch(new BookingReconciliationJob());
        })->dailyAt('05:10');

        /*Allocate staff for dormant monthly*/
        $schedule->call(function () {
            (new StaffEmployerRepository())->allocateBonusEmployersTopUp();
        })->monthlyOn('1', '05:45');

        /*Rectify employee employer for employees with notification reports*/
        $schedule->call(function () {
            dispatch(new RectifyEmployeeEmployerSyncOnUpload());
        })->monthlyOn('1', '06:00');


//                /*Load Employees from Treasury once-June 2019*/
        $schedule->call(function () {
            dispatch(new LoadEmployeeTreasury());
        })->monthlyOn(25, '22:00');
//
//             /*PAA Action to update Treasury employees biodata monthly */
//             /**End of PA Action job**/
//
//                /*Load contributions form Treasury monthly*/
        $schedule->call(function () {
            dispatch(new LoadMonthlyContributionTreasury());
        })->dailyAt('15:33');

            //->monthlyOn(26, '21:00');

        /*compute funding and votes summary*/
        $schedule->call(function () {
            dispatch(new ComputeTreasuryFundingSummary());
        })->dailyAt('11:39');

        /*Export user daily audit trails*/
        $schedule->call(function () {
            dispatch(new DailyAuditTrailLogs());
        })->dailyAt('11:36');

        /*Daily Gepg reconciliation*/
        $schedule->call(function () {
            dispatch(new ReconcileDailyGepgPayments());
        })->dailyAt('10:10');

        /* update schedule for claim followups */
        // $schedule->job(new ClaimFollowupsJob)->monthly();

    }


    $schedule->call(function () {
        $investigation_plan = new InvestigationPlanRepository();
        $investigation_plan->updateCompletionStatus();
    });
}



    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }

    /**
     * @throws \App\Exceptions\GeneralException
     */
    protected  function postInterestSchedule()
    {
        $employerRepository = new EmployerRepository();

        $employerRepository->query()->select(['id'])->chunk(100, function ($employers)  {
            if ($employers->count()) {
                dispatch(new PostInterest($employers));
            }
        });
    }

}
