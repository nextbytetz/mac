<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Log;


class MofEmployees extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mof:employees';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load MoF (Hazina) Employees details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
       // $schedule->call('App\Http\Controllers\Backend\Finance\GotIntergrationController@employeesHeaderGot')->everyMinute();
        // Log::info('Loaded '.Carbon::now()->format('Y-m-d h:i:s'));
    }
}
