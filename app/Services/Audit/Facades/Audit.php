<?php

namespace App\Services\Audit\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Access.
 */
class Audit extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'audit';
    }
}
