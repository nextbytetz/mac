<?php

namespace App\Services\Compliance;
/*
 *
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto update bookings which are not yet contributed
 */

use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\BookingInterest;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PostBookingsForESkippedMonths {

    protected $estimate_base = null;
    protected $closure = 0;

    public function __construct()
    {

    }



    /*Get start date for booking this employer*/
    public function getStartBookingDate(Model $employer)
    {
        if($this->checkIfIsNewEntrants($employer)){
            /*for new entrants start from the jul 2015*/
            $system_start_date = '2015-07-01';
        }else{
            $system_start_date = '2017-07-01';
        }


        $doc = isset($employer->doc) ? $employer->doc : $system_start_date;
        if(comparable_date_format($doc) < comparable_date_format($system_start_date)){
            return  standard_date_format($system_start_date);
        }else{
            return standard_date_format($doc);
        }
    }


    //    get booking_date
    public function getBookingDate($date) {
        $booking_date  = '28-'.  Carbon::parse($date)->format('M') . '-' . Carbon::parse($date)->format('Y');
        $format = 'd-M-Y';
        return  Carbon::createFromFormat($format, $booking_date);
    }



    /*Post bookings for employers for all eligible months*/
    public function postBookingsForASkippedMonths($employer_id){
        $employer = (new EmployerRepository())->find($employer_id);

        /*delete un eligible bookings*/
//        $this->deleteNonBookedBookingBeforeDoc($employer);
        $check_if_booked_months_tally = $this->checkIfNoOfBookingsTallyNoOfMonthsRequired($employer);
        if($check_if_booked_months_tally == false){
            /*If does not tally process skipped months*/
            $skipped_months = $this->getMonthRangeEligibleForBooking($employer);
            $employer_id = $employer->id;
            foreach($skipped_months as $month){
                $booking_date = $this->getBookingDate($month);
                $booked_amount =  (new BookingRepository())->getRecentContributionAmount($employer_id, $booking_date);
                $member_count = (new EmployerRepository())->getMemberCount($employer_id);
                //TODO remove this check if paid legacy after 1 month
                $check_if_paid_legacy = (new BookingRepository())->checkIfBookingDatePaidInLegacyReceipt($employer, $booking_date);
                /*create*/
                if(!$this->checkIfAlreadyBooked($employer, $booking_date) && !$this->checkIfMonthWasInEmployerClosure($employer, $booking_date) && !$check_if_paid_legacy){
                    $data = ['rcv_date'=>$booking_date, 'employer_id' => $employer_id, 'fin_code_id' => 2, 'amount'=>$booked_amount, 'user_id' => null, 'member_count'=>$member_count, 'description'=>'Employer monthly bookings', 'is_schedule' => 1];
                    $this->saveBooking($data);
                }
            }
        }

    }

    /*Save/ update skipped month*/
    public function saveBooking($data)
    {
        $booking_date_parsed = Carbon::parse($data['rcv_date']);
        //Check with trashed if booking exists
        $check_booking =DB::table('bookings')->whereMonth('rcv_date','=', $booking_date_parsed->format('n'))->whereYear('rcv_date','=',  $booking_date_parsed->format('Y'))->where('employer_id', $data['employer_id'])->first();

        if(isset($check_booking)){
            //If exists : Restore if deleted
            DB::table('bookings')->where('id', $check_booking->id)->update([
                'deleted_at' => null,
                'updated_at' => Carbon::now(),
                'amount' => $data['amount'],
                'member_count'=>$data['member_count'], 'description'=>$data['description'], 'is_schedule' => $data['is_schedule'],
                'duplicate_id' => null,
                'delete_reason' => null
            ]);

        }else{
            //Create new
            $booking = (new BookingRepository())->query()->create(['rcv_date'=>$data['rcv_date'], 'employer_id' => $data['employer_id'], 'fin_code_id' => $data['fin_code_id'], 'amount' =>$data['amount'], 'user_id' => null,  'member_count'=>$data['member_count'], 'description'=>$data['description'], 'is_schedule' => $data['is_schedule'],]);
        }
    }



    /*Get all months skipped for booking for this employer*/
    public function getMonthRangeEligibleForBooking(Model $employer)
    {
        $live_booked_months = Booking::query()->select('rcv_date')->where('employer_id', $employer->id)->pluck('rcv_date')->toArray();
        $today = Carbon::parse(Carbon::now())->endOfMonth();
        $start_date = $this->getStartBookingDate($employer);
        $start_date_01  =  '28-'.  Carbon::parse($start_date)->format('M') . '-' . Carbon::parse($start_date)->format('Y');
        $start_date_01 = standard_date_format($start_date_01);
        $date_array_y_n_j = [];
        $date_array_y_m_d = [];
        while(comparable_date_format($start_date_01) <= comparable_date_format($today)) {
            array_push($date_array_y_m_d,Carbon::parse($start_date_01)->format('Y-m-d') );
            array_push($date_array_y_n_j,standard_date_format($start_date_01));
            $start_date_01 = \Carbon\Carbon::parse($start_date_01)->addMonth(1);
        }
        $skipped_months = array_diff($date_array_y_m_d,$live_booked_months);
        return $skipped_months;
    }




    /**
     * @param Model $employer
     * @param $booking_date
     * Check if booking already exist on bookings
     */
    public function checkIfAlreadyBooked(Model $employer, $booking_date)
    {
        $bookings = new BookingRepository();
        $booking_date_parsed = Carbon::parse($booking_date);
        $check = $bookings->query()->where('employer_id', $employer->id)->whereMonth('rcv_date','=', $booking_date_parsed->format('m'))->whereYear('rcv_date','=', $booking_date_parsed->format('Y'))->count();

        if($check > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param Model $date_array_y_m_d
     * @param $booking_date
     * Check if applicable month was in employer closure
     */
    public function checkIfMonthWasInEmployerClosure(Model $employer, $booking_date){

        $check_closure = (new BookingRepository())->checkIfMonthWasInEmployerClosure($employer, $booking_date);
        return $check_closure;
    }


    /**
     * @param Model $employer
     * @param $booking_date
     * Delete non booked booking before doc incase doc is updated
     */
    public function deleteNonBookedBookingBeforeDoc(Model $employer){
        Booking::query()->where('employer_id', $employer->id)->whereDate('rcv_date', '<', standard_date_format($employer->doc))->wheredoesntHave('receiptCodes', function($query){
            $query->has('activeReceipt');
        })->delete();


        /*delete raised interests for deleted bookings*/
        BookingInterest::query()->whereDoesntHave('booking', function ($query) use($employer){
            $query->where('employer_id', $employer->id);
        })->delete();

    }


    /**
     * @param Model $employer
     * @return bool
     * Cut off date for new entrants to check receivable from jul 2015.
     */
    public function checkIfIsNewEntrants(Model $employer)
    {
        $date_registered = $employer->created_at;
        $cut_off_date = getElectronicReceiptStartDate();
        if(comparable_date_format($date_registered) > comparable_date_format($cut_off_date)){
            return true;
        }else{
            return false;
        }
    }



    public function checkIfNoOfBookingsTallyNoOfMonthsRequired(Model $employer)
    {
        /*check if no of bookings of employer tally no of months required*/
        $start_date = $this->getStartBookingDate($employer);
        $no_of_bookings = Booking::query()->where('employer_id', $employer->id)->count();
        $no_of_months_required = months_diff($start_date, Carbon::now()) + 1;
        if($no_of_bookings != $no_of_months_required){
            /*does not Tally*/
            return false;
        }else{
            /*tally*/
            return true;
        }
    }




}