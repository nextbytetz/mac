<?php

namespace App\Services\Compliance;
/*
 * class MergeUnMergeEmployers
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to Merge and split employers
 */

use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MergeDeMergeEmployers {


    public function __construct()
    {

    }

    /**
     * @param $old_regno - duplicate to be replaced
     * @param $replaced_by - Active reg no
     *
     */
    public function mergeEmployer($oldregno, $replacedby, $user)
    {
        $sql = <<<SQL
do $$ 
declare t record;
declare other_tables record;
begin
    for t IN select column_name, table_name, table_schema
            from information_schema.columns where column_name = 'employer_id' and table_name not in ('employer_certificate_issues', 'non_booked_bookings', 'bookings', 'staff_employer', 'contributing_category_employer') and is_updatable = 'YES' and table_schema in ('portal', 'main')

    loop
         -- Check if column employer_orig (record original employer before merge) exists,
        if not exists(SELECT 1 FROM information_schema.columns  WHERE table_name=t.table_name and column_name='employer_orig' and table_schema=t.table_schema) then
            execute 'alter table ' || t.table_schema || '.' || t.table_name || ' add column employer_orig bigint null';
        end if;
        
        -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
        if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then 
            execute 'update ' || t.table_schema || '.' || t.table_name || ' set ' || t.column_name || ' = {$oldregno}, employer_orig = {$replacedby} where ' || t.column_name || ' = {$replacedby}';
        else 
            execute 'update ' || t.table_schema || '.' || t.table_name || ' set ' || t.column_name || ' = {$replacedby}, employer_orig = {$oldregno} where ' || t.column_name || ' = {$oldregno}';
        end if;
    
    end loop;
    
    -- Check for other tables not using employer_id directly (using resource_id instead and member_type_id) in the {main schema}
    for other_tables in SELECT UNNEST(ARRAY['payment_voucher_transactions','medical_expenses','claim_compensations','notification_eligible_benefits']) AS table_name
    loop
        -- Check if column resource_orig (record original resource_id/employer before merge) exists,
        if not exists(SELECT 1 FROM information_schema.columns  WHERE table_name=other_tables.table_name and column_name='resource_orig' and table_schema='main') then
            execute 'alter table main.' || other_tables.table_name || ' add column resource_orig bigint null';
        end if;

        -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
         if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then 
         -- update the original resource id in the table
            execute 'update main.' || other_tables.table_name || ' set resource_id = {$oldregno}, resource_orig = {$replacedby} where resource_id = {$replacedby}  and member_type_id = 1';
         else 
             -- update the original resource id in the table
            execute 'update main.' || other_tables.table_name || ' set resource_id = {$replacedby}, resource_orig = {$oldregno} where resource_id = {$oldregno}  and member_type_id = 1';
         end if;

    end loop;
    
    -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
    if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then 
        execute 'update employers set duplicate_id = {$oldregno}, deleted_at = now(), user_set_duplicate = {$user} where id = {$replacedby}';
    else
        execute 'update employers set duplicate_id = {$replacedby}, deleted_at = now(), user_set_duplicate = {$user} where id = {$oldregno}';      
    end if;
    
end$$;
SQL;
        DB::unprepared($sql);


    }


    /**
     * @param $oldregno
     * @param $replacedby
     * demerge/splitting employers
     */
    public function demergeEmployers($oldregno, $replacedby)
    {
        $sql = <<<SQL
do $$ 
declare t record;
declare other_tables record;
begin
    for t IN select column_name, table_name, table_schema
            from information_schema.columns where column_name = 'employer_id' and table_name not in ('employer_certificate_issues', 'non_booked_bookings', 'bookings') and is_updatable = 'YES' and table_schema in ('portal', 'main')

    loop
    
         -- Check if column employer_orig (record original employer before merge) exists,
        if not exists(SELECT 1 FROM information_schema.columns  WHERE table_name=t.table_name and column_name='employer_orig' and table_schema=t.table_schema) then
            execute 'alter table ' || t.table_schema || '.' || t.table_name || ' add column employer_orig bigint null';
        end if;
    
        -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
        if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then 
            execute 'update ' || t.table_schema || '.' || t.table_name || ' set ' || t.column_name || ' = {$replacedby} where employer_orig = {$replacedby} and employer_id = {$oldregno}';
        else 
            execute 'update ' || t.table_schema || '.' || t.table_name || ' set ' || t.column_name || ' = {$oldregno} where employer_orig = {$oldregno} and employer_id = {$replacedby}';
        end if;
        
        
    end loop;
    
    -- Check for other tables not using employer_id directly (using resource_id instead and member_type_id) in the {main schema}
    for other_tables in SELECT UNNEST(ARRAY['payment_voucher_transactions','medical_expenses','claim_compensations','notification_eligible_benefits']) AS table_name
    loop
    
        -- Check if column resource_orig (record original resource_id/employer before merge) exists,
        if not exists(SELECT 1 FROM information_schema.columns  WHERE table_name=other_tables.table_name and column_name='resource_orig' and table_schema='main') then
            execute 'alter table main.' || other_tables.table_name || ' add column resource_orig bigint null';
        end if;

        -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
        if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then 
            execute 'update main.' || other_tables.table_name || ' set resource_id = {$replacedby}  where resource_orig = {$replacedby} and resource_id = {$oldregno}  and member_type_id = 1';
        else 
            -- update the original resource id in the table
            execute 'update main.' || other_tables.table_name || ' set resource_id = {$oldregno}  where resource_orig = {$oldregno} and resource_id = {$replacedby}  and member_type_id = 1';
        end if;

    end loop;
    
    -- Check if #oldregno has already been used as #replaceby in some previous merging, if true do the reverse merging
    if exists (select 1 from main.employers where duplicate_id = {$oldregno}) then
        execute 'update employers set duplicate_id = null, deleted_at = null, user_set_duplicate = null where id = {$replacedby}';
    else 
        execute 'update employers set duplicate_id = null, deleted_at = null, user_set_duplicate = null where id = {$oldregno}';
    end if; 
  
    
end$$;
SQL;
        DB::unprepared($sql);
    }








}