<?php

namespace App\Services\Compliance;
/*
 *
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto update bookings which are not yet contributed
 */

use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\BookingInterest;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PostBookingsForESkippedMonthsLegacy {

    protected $estimate_base = null;
    protected $closure = 0;

    public function __construct()
    {

    }

    public function getLatestContribution($employer)
    {
        $legacy_receipt = (new LegacyReceiptRepository())->getLatestContributionByEmployer($employer);
        return $legacy_receipt;
    }

    /*Get closest contribution for booking date*/
    public function getClosestContribution($employer, $booking_date)
    {
        $booking_date_parse = standard_date_format($booking_date);
        $contrib = 0;


        $same_month = DB::table('employer_contributions')->where('contrib_month', '=',$booking_date_parse)->where('employer_id', $employer->id)->first();

        if($same_month){
            $contrib = DB::table('employer_contributions')->where('contrib_month', '=',$booking_date_parse)->where('employer_id', $employer->id)->sum('contrib_amount');
            return $contrib;
        }

        $before_month = DB::table('employer_contributions')->where('contrib_month', '<',$booking_date_parse)->where('employer_id', $employer->id)->orderBy('contrib_month', 'desc')->first();
        if($before_month){
            $contrib =  DB::table('employer_contributions')->where('contrib_month', '=',standard_date_format($before_month->contrib_month))->where('employer_id', $employer->id)->orderBy('contrib_month', 'desc')->sum('contrib_amount');
            return $contrib;
        }

        $after_month = DB::table('employer_contributions')->where('contrib_month', '>',$booking_date_parse)->where('employer_id', $employer->id)->orderBy('contrib_month', 'asc')->first();
        if($after_month){
            $contrib   =  DB::table('employer_contributions')->where('contrib_month', '=',standard_date_format($after_month->contrib_month))->where('employer_id', $employer->id)->orderBy('contrib_month', 'asc')->sum('contrib_amount');
            return $contrib;

        }

        return $contrib;
    }



    /*Get start date for booking this employer*/
    public function getStartBookingDate(Model $employer)
    {
        $system_start_date = '2015-07-01';
//        $last_legacy_contrib = $this->getLatestContribution($employer);
//        if(isset($last_legacy_contrib->contrib_month)){
//            if(comparable_date_format($last_legacy_contrib->contrib_month) > comparable_date_format($system_start_date)){
//                return standard_date_format($last_legacy_contrib->contrib_month);
//            }else{
//                return   standard_date_format($system_start_date);
//            }
//        }else{
//            return  standard_date_format($system_start_date);
//        }
        return $system_start_date;
    }


    //    get booking_date
    public function getBookingDate($date) {
        $booking_date  = '28-'.  Carbon::parse($date)->format('M') . '-' . Carbon::parse($date)->format('Y');
        $format = 'd-M-Y';
        return  Carbon::createFromFormat($format, $booking_date);
    }



    /*Post bookings for employers for all eligible months*/
    public function postBookingsForASkippedMonthsLegacy($employer_id){
        $employer = (new EmployerRepository())->find($employer_id);
        $skipped_months = $this->getMonthRangeEligibleForBooking($employer);
        $employer_id = $employer->id;
        foreach($skipped_months as $month){
            $booking_date = $this->getBookingDate($month);
            $last_contrib = $this->getLatestContribution($employer);
            $close_contrib = $this->getClosestContribution($employer, $booking_date);
            $booked_amount = (isset($last_contrib)) ? $close_contrib : 0;
            $member_count = (new EmployerRepository())->getMemberCount($employer_id);
            $check_if_paid_legacy = (new BookingRepository())->checkIfBookingDatePaidInLegacyReceipt($employer, $booking_date);
            /*create*/
//            if(!$this->checkIfAlreadyBooked($employer, $booking_date) && !$this->checkIfMonthWasInEmployerClosure($employer, $booking_date) && !$check_if_paid_legacy){
            if(!$this->checkIfAlreadyBooked($employer, $booking_date) && !$this->checkIfMonthWasInEmployerClosure($employer, $booking_date) && !$this->checkIfAlreadyBookedLegacy($employer, $booking_date)){
//                $booking = (new BookingRepository())->query()->create(['rcv_date'=>$booking_date, 'employer_id' => $employer_id, 'fin_code_id' => 2, 'amount'=>$booked_amount, 'user_id' => null, 'member_count'=>$member_count, 'description'=>'Employer monthly bookings', 'is_schedule' => 1]);
                $paid_amount = $this->getPaidAmount($employer_id, $booking_date);
                $ispaid = ($paid_amount > 0) ? 1 : 0;
                $booking = DB::table('legacy_bookings')->insert(['rcv_date'=>$booking_date, 'employer_id' => $employer_id, 'fin_code_id' => 2, 'amount'=>$booked_amount, 'user_id' => null, 'member_count'=>$member_count, 'description'=>'Employer monthly Legacy bookings', 'is_schedule' => 1, 'ispaid' => $ispaid, 'paid_amount' => $paid_amount]);
            }
        }
    }


    /*Get all months skipped for booking for this employer*/
    public function getMonthRangeEligibleForBooking(Model $employer)
    {
        $live_booked_months = Booking::query()->select('rcv_date')->where('employer_id', $employer->id)->pluck('rcv_date')->toArray();
        $today = Carbon::parse(Carbon::now())->endOfMonth();
        $today = Carbon::parse('2017-07-01');
        $start_date = $this->getStartBookingDate($employer);
        $start_date_01  =  '28-'.  Carbon::parse($start_date)->format('M') . '-' . Carbon::parse($start_date)->format('Y');
        $start_date_01 = standard_date_format($start_date_01);
        $date_array_y_n_j = [];
        $date_array_y_m_d = [];
        while(comparable_date_format($start_date_01) <= comparable_date_format($today)) {
            array_push($date_array_y_m_d,Carbon::parse($start_date_01)->format('Y-m-d') );
            array_push($date_array_y_n_j,standard_date_format($start_date_01));
            $start_date_01 = \Carbon\Carbon::parse($start_date_01)->addMonth(1);
        }

        $skipped_months = array_diff($date_array_y_m_d,$live_booked_months);
        return $skipped_months;
    }




    /**
     * @param Model $employer
     * @param $booking_date
     * Check if booking already exist on bookings
     */
    public function checkIfAlreadyBooked(Model $employer, $booking_date)
    {
        $bookings = new BookingRepository();
        $booking_date_parsed = Carbon::parse($booking_date);
        $check = $bookings->query()->where('employer_id', $employer->id)->whereMonth('rcv_date','=', $booking_date_parsed->format('m'))->whereYear('rcv_date', '=',$booking_date_parsed->format('Y'))->count();
        if($check > 0){
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param Model $employer
     * @param $booking_date
     * Check if booking already exist on bookings
     */
    public function checkIfAlreadyBookedLegacy(Model $employer, $booking_date)
    {
        $booking_date_parsed = Carbon::parse($booking_date);
        $check = DB::table('legacy_bookings')->whereNull('deleted_at')->where('employer_id', $employer->id)->whereMonth('rcv_date','=', $booking_date_parsed->format('m'))->whereYear('rcv_date','=', $booking_date_parsed->format('Y'))->count();
        if($check > 0){
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param Model $date_array_y_m_d
     * @param $booking_date
     * Check if applicable month was in employer closure
     */
    public function checkIfMonthWasInEmployerClosure(Model $employer, $booking_date){

        $check_closure = (new BookingRepository())->checkIfMonthWasInEmployerClosure($employer, $booking_date);
        return $check_closure;
    }

    /**
     * @param Model $employer
     * @param $booking_date
     * Delete non booked booking before doc incase doc is updated
     */
    public function deleteNonBookedBookingBeforeDoc(Model $employer){
        Booking::query()->where('employer_id', $employer->id)->whereDate('rcv_date', '<', standard_date_format($employer->doc))->wheredoesntHave('receiptCodes', function($query){
            $query->has('activeReceipt');
        })->delete();


        /*delete raised interests for deleted bookings*/
        BookingInterest::query()->whereDoesntHave('booking', function ($query) use($employer){
            $query->where('employer_id', $employer->id);
        })->delete();

    }




    /**
     *
     * LEGACY CONTRIBUTION
     */

    /*Get Paid amount*/
    public function getPaidAmount($employer_id, $contrib_month)
    {
        $contrib_month = Carbon::parse($contrib_month);
        $paid_amount = DB::table('employer_contributions')->whereMonth('contrib_month','=', $contrib_month->format('n'))->whereYear('contrib_month','=', $contrib_month->format('Y'))->where('employer_id', $employer_id)->sum('contrib_amount');
        return $paid_amount;
    }


}