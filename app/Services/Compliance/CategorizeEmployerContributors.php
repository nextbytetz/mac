<?php

namespace App\Services\Compliance;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto categorize employers to contribution categories.
 */

use App\Models\Finance\Receipt\LegacyReceipt;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CategorizeEmployerContributors {
    /**
     * Services for auto categorize employers to contribution categories.
     *
     * @param string $model
     *
     * @return void
     */



    public function __construct()
    {

    }

    /*Get number of contributions to be used to categorize employers*/
    public function getNoOfContributions()
    {
        return 3;
    }


    /*Get Contributing employers*/
    public function getContributingEmployers()
    {
        $employers = DB::table('contributing_employers')
            ->join('employers', 'employers.id', 'contributing_employers.employer_id')
            ->where('employer_status','<>',3)
            ->where('is_treasury', false)
            ->get();
        return $employers;
    }

    /*Get recent employer has category already*/
    public function employerCategory($employer_id)
    {
        $category = DB::table('contributing_category_employer')->where('employer_id', $employer_id)->orderBy('id', 'desc')->first();
        return $category;
    }
    /*Get most recent contribution by employer*/
    public function getContributionMonthsByEmployer($employer_id)
    {
        $receipt_code_repo = new ReceiptCodeRepository();
        $no_of_contributions = $this->getNoOfContributions();
        $contrib_months = $receipt_code_repo->query()->distinct()->select('contrib_month')->whereHas('activeReceipt', function ($query) use($employer_id){
            $query->where('employer_id', $employer_id);
        })->where('contrib_month', '<' , standard_date_format(Carbon::now()))->orderBy('contrib_month', 'desc')->limit($no_of_contributions)->get();



        if(($contrib_months->isEmpty())){
              $contrib_months = LegacyReceipt::query()->distinct()->select('contrib_month')->where('employer_id', $employer_id)->where('contrib_month', '<' , standard_date_format(Carbon::now()))->orderBy('contrib_month', 'desc')->limit($no_of_contributions)->get();
        }

        return $contrib_months;
    }


    /*Get Contribution amount by contrib month*/
    public function getTotalContributionByMonth($employer_id,$contrib_month){
        $receipt_code_repo = new ReceiptCodeRepository();
        $contrib_amount = $receipt_code_repo->query()->whereHas('activeReceipt', function ($query) use($employer_id){
            $query->where('employer_id', $employer_id);
        })->whereDate('contrib_month', $contrib_month)->sum('amount');

        if($contrib_amount == 0){
            $contrib_amount = LegacyReceipt::query()->where('employer_id', $employer_id)->whereDate('contrib_month', $contrib_month)->sum('amount');
        }
        return $contrib_amount;
    }


    /*Get category group for each contribution amount for each contribution month*/
    public function getCategoryGroupByAmount($amount){
        $category = null;
        $large = DB::table('employer_contributing_categories')->where('id', 1)->first();

        if($amount >= $large->min_contribution ){
            /*large*/
            $category = $large;
            return $category;
        }else{
            /*others*/
            $category = DB::table('employer_contributing_categories')->where('min_contribution','<=', $amount)->where('max_contribution','>=', $amount)->first();
            return $category;
        }

    }

    /*Categorize employer - Check recent contribution belonging to categories*/
    public function getEmployerCategory($employer_id){
        $categories_array = [];
        $contrib_months = $this->getContributionMonthsByEmployer($employer_id);

        $employer_category_id =null;
        if(($contrib_months->isNotEmpty())){

            foreach ($contrib_months as $contrib_month) {
                $total_contrib_amount = $this->getTotalContributionByMonth($employer_id, $contrib_month['contrib_month']);

                $category = $this->getCategoryGroupByAmount($total_contrib_amount);
                if(isset($category)){
                    $categories_array[] = $category->id;
                }
            }
            if(isset($categories_array)){
                /*get minimum group*/
                $contributing_category = $this->employerCategory($employer_id);
                if(isset($contributing_category)){
                    /*If already categorized need check consistently*/
                    if(count(array_unique($categories_array)) == 1){
                        /*consistent update - category*/
                        $employer_category_id = max($categories_array);
                    }else{
                        /*not consistent should not change unless min category is larger than current category*/
                        $employer_category_id = $contributing_category->contributing_category_id;
                        $new_category_id = max($categories_array); //Min category from the last three months
                        if($new_category_id < $employer_category_id )
                        {
                            $employer_category_id = $new_category_id;
                        }

                    }
                }else{
                    /*If not categorized yet - Get minimum group*/
                    $employer_category_id = max($categories_array);
                }
            }
        }

        return $employer_category_id;
    }




    /*Update employer contributing category*/
    public function updateEmployerCategory($employer_id){
        $contributing_category_employer = DB::table('contributing_category_employer')->where('employer_id', $employer_id)->first();
        $category_id = $this->getEmployerCategory($employer_id);
          if(isset($category_id)){
            if(isset($contributing_category_employer)){
                $old_category_id = $contributing_category_employer->contributing_category_id;
                /*Update only if it change category*/
                if($old_category_id != $category_id){
                    DB::table('contributing_category_employer')->where('id', $contributing_category_employer->id)->update([
                        'contributing_category_id' => $category_id,
                        'previous_contributing_category_id' => $old_category_id,
                        'isassigned' => 0,
                        'updated_at' => Carbon::now(),
                        'change_date' => standard_date_format(Carbon::now()),
                    ]);

                     }
            }else{
                /*Insert for the first time*/

                $contributing_category_employer = DB::table('contributing_category_employer')->insert([
                    'employer_id' => $employer_id,
                    'contributing_category_id' => $category_id,
                    'created_at' => Carbon::now()
                ]);

            }


        }

    }


    /**
     * @param $employer_id
     * Categorize employer
     */

    public function categorizeEmployer($employer_id){
        $this->updateEmployerCategory($employer_id);
    }


//    /*Categorize all employers*/
//    public function categorizeEmployers()
//    {
//        $employers = $this->getContributingEmployers();
//        foreach($employers as $employer){
//            $this->updateEmployerCategory($employer->id);
//        }
//    }


    /*Reset for those entries updated as duplicate employers*/
    public function resetEmployersIdForDuplicateEmployers()
    {
        DB::statement("update contributing_category_employer set employer_id = employer_orig where employer_id <> employer_orig and employer_orig is not null");
    }


    /*Check if there are duplicate employers*/
    public function checkIfThereIsDuplicateEmployer()
    {
        $check = DB::table('contributing_category_employer')->whereNotNull('employer_orig')->whereRaw("employer_id <> employer_orig")->count();
        if($check > 0)
        {
            return true;
        }else{
            return false;
        }
    }

}