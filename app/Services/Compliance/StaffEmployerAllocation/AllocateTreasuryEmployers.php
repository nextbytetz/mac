<?php

namespace App\Services\Compliance\StaffEmployerAllocation;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto allocating staff to non large employers
 */


use App\Models\Auth\User;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AllocateTreasuryEmployers {
    /**
     * Services for allocating staff to treasury employers
     *
     * @param string $model
     *
     * @return void
     */
    protected $allocation_config;

    /**
     * AllocateNonLargeEmployers constructor.
     * @param $staff_employer_allocation_id
     */
    public function __construct()
    {
        $this->allocation_config = (new StaffEmployerRepository())->staffEmployerConfig();
    }

    /*Query for large contributors*/
    public function queryForTreasuryEmployers()
    {
        return Employer::query()
            ->where('employer_status', '<>',3)
            ->whereNull('duplicate_id')
            ->where(function ($query){
                $query->where('is_treasury', true)->orWhere('employers.id', 9386);
            });
    }


    /**
     * @param $staff_employer_allocation_id
     * Allocate Staff to employers
     */
    public function allocateEmployers($staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff_array = unserialize($allocation->selected_staff);


        /*Allocate staffs*/
        $this->allocateEmployersForStaff($selected_staff_array, $staff_employer_allocation_id);
    }

    /**
     * @param $selected_staff_array
     * @param $staff_employer_allocation_id
     * Staff Allocation-----
     */

    /*Allocate employers for each selected staff on this allocation for staff*/
    public function allocateEmployersForStaff($selected_staff_array, $staff_employer_allocation_id)
    {

        $staffs = User::query()->whereIn('id', $selected_staff_array)->inRandomOrder()->get();
        $no_of_staffs = $staffs->count();
        $no_of_employers = $this->queryForTreasuryEmployers()->count();
        $no_employers_per_staff =  ($no_of_staffs > 0) ? floor($no_of_employers / $no_of_staffs) : 0;
        $remainder_employers = fmod($no_of_employers, $no_of_staffs);

        foreach($staffs as $staff){
//            $check_if_already_allocated = $this->checkIfStaffAlreadyAllocated($staff->id, $staff_employer_allocation_id);
//            if($check_if_already_allocated == false){
            /*If not allocated yet - Allocate */
            $no_employers_per_staff_final = ($remainder_employers > 0) ? ($no_employers_per_staff + 1) : $no_employers_per_staff;

            $this->allocateEmployersForEachStaff($staff,$no_employers_per_staff_final, $staff_employer_allocation_id);
            $remainder_employers = $remainder_employers - 1;
//            }
        }
    }


    /*Allocate for each staff*/
    public function allocateEmployersForEachStaff($user, $no_of_employers_per_staff, $staff_employer_allocation_id){

        /*allocate main region*/
        $this->allocate($user->id,$no_of_employers_per_staff, $staff_employer_allocation_id);

    }






    /*Allocate staff to specified no of employers*/
    public function allocate($user_id, $no_of_employers,$staff_employer_allocation_id )
    {
        DB::transaction(function () use( $user_id, $no_of_employers, $staff_employer_allocation_id) {
            /*Contributing categories*/

            /*Round 1 - employers*/
            $employers = $this->getEmployers($no_of_employers, $staff_employer_allocation_id);


            $this->saveStaffEmployer($user_id, $employers, $staff_employer_allocation_id);

            /*update run status if complete*/
            (new StaffEmployerRepository())->updateRunStatus($staff_employer_allocation_id);

        });
    }

    /*Get employers to be allocated*/
    public function getEmployers($no_of_employers, $staff_employer_allocation_id)
    {
        $employers = $this->queryForTreasuryEmployers()->select(['employers.id'])
            ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                $join->on('staff_employer.employer_id', 'employers.id')
                    ->where('isactive', 1);
            })->whereNull('staff_employer.employer_id')->limit($no_of_employers)->inRandomOrder()->get()->toArray();
        return $employers;
    }



    /*Save staff employer*/
    public function saveStaffEmployer($user_id, $employer_ids, $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $start_date = $allocation->start_date;
        $end_date = $allocation->end_date;
        $unit_id = 5;
        $created_at = Carbon::now();
        foreach($employer_ids as $employer){
            $check_if_allocated = $this->checkIfEmployerAlreadyAllocated($employer['id'], $staff_employer_allocation_id);
            if($check_if_allocated == false){
                DB::table('staff_employer')->insert([
                    'user_id' => $user_id,
                    'employer_id' => $employer['id'],
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'unit_id' => $unit_id,
                    'created_at' => $created_at,
                    'staff_employer_allocation_id' => $staff_employer_allocation_id,
                    'isbonus' => 2,
                ]);
            }
        }
    }


    /*Check if employer is already allocated*/
    public function checkIfEmployerAlreadyAllocated($employer_id, $staff_employer_allocation_id)
    {
        $check = DB::table('staff_employer')->where('isactive', 1)->where('employer_id', $employer_id)->count();
        if($check > 0)
        {
            return true;

        }else{
            return false;
        }
    }

    /*check if staff already allocated*/
    public function checkIfStaffAlreadyAllocated($user_id, $staff_employer_allocation_id)
    {
        $check = DB::table('staff_employer')->where('user_id', $user_id)->where('staff_employer_allocation_id', $staff_employer_allocation_id)->count();
        if($check > 0)
        {
            return true;

        }else{
            return false;
        }
    }


}