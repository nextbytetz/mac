<?php

namespace App\Services\Compliance\StaffEmployerAllocation;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto allocating staff to non large employers
 */


use App\Exceptions\GeneralException;
use App\Exceptions\JobException;
use App\Models\Auth\User;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\Backend\Sysdef\DesignationRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class AllocateEmployersForIntern {


    /**
     * Services for allocating staff to non large employers
     *
     * @param string $model
     *
     * @return void
     */
    protected $chunkSize = 100;
    protected $allocation_config;

    /**
     * AllocateNonLargeEmployers constructor.
     * @param $staff_employer_allocation_id
     */
    public function __construct()
    {
        $this->allocation_config = (new StaffEmployerRepository())->staffEmployerConfig();
    }



    /**
     * UPLOAD EXCEL FOR BULK ASSIGNMENT FOR INTERN STAFF
     */

    public function bulkAssignmentForIntern($intern_user_id,$staff_employer_allocation_id)
    {
        $file = temporary_dir() . DIRECTORY_SEPARATOR . 'staff_employer' . DIRECTORY_SEPARATOR . 'file';

        /** start : Check if all excel headers are present */
        $headings = Excel::selectSheetsByIndex(0)
            ->load($file)
            ->takeRows(1)
            ->first()
            ->keys()
            ->toArray();
        $verifyArr = ['employer_regno'];
        foreach ($verifyArr as $key) {
            if (!in_array($key, $headings, true)) {
                throw new GeneralException(trans('exceptions.backend.upload.column_missing', ['column' => $key]));
            }
        }
        /** end : Check if all excel headers are present */

        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load($file)
            ->chunk($this->chunkSize, function($result) use ($intern_user_id, $staff_employer_allocation_id) {
                $rows = $result->toArray();

                foreach ($rows as $key => $row) {
//                    $reg_no = ($row['employer_regno']);
//                    foreach ($row as $key2 => $value) {
//                        Log::info(print_r(, true));
                        /*Assign*/
                //                        Log::info(print_r($row->employer_regno,true));
                        $this->allocateEmployerForInternFromExcel($row['employer_regno'], $intern_user_id, $staff_employer_allocation_id);
//                    }

                }
            }, true);


    }

    /*Allocate from excel*/
    public function allocateEmployerForInternFromExcel($employer_regno, $intern_user_id, $staff_employer_allocation_id)
    {
//        $employer = Employer::query()->select('id as employer_id')->where('id',$employer_regno)->get();
        $employer = DB::table('employers')->select('id as employer_id')->where('id',$employer_regno)->get();
        if($employer->count() > 0){
            $employer_ids = $employer->toArray();
                  (new StaffEmployerRepository())->saveStaffEmployer($intern_user_id,$employer_ids ,$staff_employer_allocation_id );
        }

        (new StaffEmployerRepository())->updateNoOfEmployersToAllocated($staff_employer_allocation_id);
    }



}