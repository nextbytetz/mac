<?php

namespace App\Services\Compliance\StaffEmployerAllocation;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto allocating staff to non large employers
 */


use App\Models\Auth\User;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\Backend\Sysdef\DesignationRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AllocateBonusEmployers {
    /**
     * Services for allocating staff to non large employers
     *
     * @param string $model
     *
     * @return void
     */
    protected $allocation_config;

    /**
     * AllocateNonLargeEmployers constructor.
     * @param $staff_employer_allocation_id
     */
    public function __construct()
    {
        $this->allocation_config = (new StaffEmployerRepository())->staffEmployerConfig();
    }


    public function dormantEmployersQuery()
    {
        return Employer::query()
    ->where('employers.employer_status',2)->whereNull('employers.duplicate_id')->where('is_treasury', false);
    }

    public function dormantEmployersQueryCount()
    {
        return $this->dormantEmployersQuery()
            ->leftJoin('staff_employer', function($join){
                $join->on('employers.id', 'staff_employer.employer_id')
                    ->where('staff_employer.isactive',1);
            })
            ->whereNull('staff_employer.id')->count();
    }

    /*Dormant employers for top up every month*/
    public function dormantEmployersForTopUpQuery()
    {
        return Employer::query()
            ->leftJoin('staff_employer', function($join){
                $join->on('employers.id', 'staff_employer.employer_id')
                    ->where('staff_employer.isactive',1);
            })
            ->whereNull('staff_employer.id')
            ->where('employers.employer_status',2)->whereNull('employers.duplicate_id')->where('employers.is_treasury', false);
    }

    /**
     * @param $staff_employer_allocation_id
     * Allocate Staff to employers
     */
    public function allocateEmployers($staff_employer_allocation_id)
    {

        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff_array = unserialize($allocation->selected_staff);

        /*Allocate staffs*/
        $this->allocateEmployersForStaff($selected_staff_array, $staff_employer_allocation_id);

        /*update run status if complete*/
        (new StaffEmployerRepository())->updateRunStatus($staff_employer_allocation_id);
    }

    /**
     * @param $selected_staff_array
     * @param $staff_employer_allocation_id
     * Staff Allocation-----
     */

    /*Allocate employers for each selected staff on this allocation for staff*/
    public function allocateEmployersForStaff($selected_staff_array, $staff_employer_allocation_id)
    {
//        $staffs = User::query()->whereIn('id', $selected_staff_array)->inRandomOrder()->get();
        $staffs = User::query()->whereIn('id', $selected_staff_array)->orderBy('office_id', 'desc')->get();
        $no_of_staffs = $staffs->count();
        $no_of_employers = $this->dormantEmployersQueryCount();
        $no_employers_per_staff =  ($no_of_staffs > 0) ? floor($no_of_employers / $no_of_staffs) : 0;
        $remainder_employers = fmod($no_of_employers, $no_of_staffs);
        foreach($staffs as $staff){
            $check_if_already_allocated = (new StaffEmployerRepository())->checkIfStaffAlreadyAllocated($staff->id, $staff_employer_allocation_id);
            if($check_if_already_allocated == false){
                /*If not allocated yet - Allocate */
                $no_employers_per_staff_final = ($remainder_employers > 0) ? ($no_employers_per_staff + 1) : $no_employers_per_staff;

                $this->allocateEmployersForEachStaff($staff,$no_employers_per_staff_final, $staff_employer_allocation_id);
                $remainder_employers = $remainder_employers - 1;
            }
        }
    }





    /*Allocate for each staff*/
    public function allocateEmployersForEachStaff($user, $no_of_employers_per_staff, $staff_employer_allocation_id){

        $this->allocate($user,$no_of_employers_per_staff, $staff_employer_allocation_id);
    }



    /*---End  of allocation --------*/



    /*Allocate staff to specified no of employers*/
    public function allocate($user, $no_of_employers,$staff_employer_allocation_id )
    {
        DB::transaction(function () use( $user, $no_of_employers, $staff_employer_allocation_id) {
            /*Get zone for regional officer*/

                $zone_id = ($user->office_id <> 1) ? (isset($user->office->region->zone_id) ? $user->office->region->zone_id : null) : null;

                $user_id = $user->id;
                /*1st allocation*/
                $employers = $this->getEmployers($no_of_employers, $staff_employer_allocation_id, null, $zone_id);
                $this->saveStaffEmployer($user_id, $employers, $staff_employer_allocation_id);

                /*2nd allocation*/
                $remained_employers = $no_of_employers - (isset($employers) ? sizeof($employers) : 0);

                if ($remained_employers > 0) {
                    $employers = $this->getEmployers($remained_employers, $staff_employer_allocation_id, null, null);
                                if (sizeof($employers) > 0) {

                        $this->saveStaffEmployer($user_id, $employers, $staff_employer_allocation_id);
                    }

                    /*update run status*/
                    (new StaffEmployerRepository())->updateRunStatus($staff_employer_allocation_id);
                }

        });

    }

    /*Get employers to be allocated*/
    public function getEmployers($no_of_employers, $staff_employer_allocation_id, $region_id = null, $zone_id = null)
    {
        if(isset($region_id)){
            $employers = $this->dormantEmployersQuery()->select(['employers.id'
            ])
                ->join('regions', 'employers.region_id', 'regions.id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('isactive', 1);
                })->where('regions.id', $region_id)->whereNull('staff_employer.employer_id')->limit($no_of_employers)->inRandomOrder()->get()->toArray();

        }elseif(isset($zone_id)){
            $employers = $this->dormantEmployersQuery()->select(['employers.id'  ])
                ->join('regions', 'employers.region_id', 'regions.id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('isactive', 1);
                })->where('regions.zone_id', $zone_id)->whereNull('staff_employer.employer_id')->limit($no_of_employers)->inRandomOrder()->get()->toArray();

        }else{
            $employers = $this->dormantEmployersQuery()->select(['employers.id'  ])
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer.isactive', 1);
                })->whereNull('staff_employer.employer_id')->limit($no_of_employers)->inRandomOrder()->get()->toArray();
        }

        return $employers;
    }




    /*Save staff employer*/
    public function saveStaffEmployer($user_id, $employer_ids, $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $start_date = $allocation->start_date;
        $end_date = $allocation->end_date;
        $unit_id = 5;
        $created_at = Carbon::now();
        foreach($employer_ids as $employer){
            $check_if_allocated = $this->checkIfEmployerAlreadyAllocated($employer['id'], $staff_employer_allocation_id);
            $check_if_allocated = false;
            if($check_if_allocated == false){
                DB::table('staff_employer')->insert([
                    'user_id' => $user_id,
                    'employer_id' => $employer['id'],
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'unit_id' => $unit_id,
                    'created_at' => $created_at,
                    'staff_employer_allocation_id' => $staff_employer_allocation_id,
                    'isbonus' => 1,
                    'isintern' => $allocation->isintern
                ]);
            }
        }


    }


    /*Check if employer is already allocated*/
    public function checkIfEmployerAlreadyAllocated($employer_id, $staff_employer_allocation_id = null)
    {
        $check = DB::table('staff_employer')->where('isactive', 1)->where('employer_id', $employer_id)->count();
        if($check > 0)
        {
            return true;

        }else{
            return false;
        }
    }


    /**
     *
     *TOP UP METHODS
     *
     */
    /**
     * @param $staff_employer_allocation_id
     * Allocate Staff to employers
     */
    public function allocateEmployersTopUp($staff_employer_allocation_id)
    {
//        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
//        $selected_staff_array = unserialize($allocation->selected_staff);

        /*Allocate staffs*/
        $this->allocateEmployersForStaffForTopUp($staff_employer_allocation_id);
    }

    /*Allocate employers for each selected staff on this allocation for staff - FOR TOP UP*/
    public function allocateEmployersForStaffForTopUp( $staff_employer_allocation_id)
    {
        $dormant_employers = $this->dormantEmployersForTopUpQuery()->select(['employers.id'
        ])->get()->toArray();

        foreach($dormant_employers as $employer_id){

            $next_user  = $this->getNextStaffForBonusTopUp();

            $this->saveStaffEmployerTopUp($next_user, $employer_id, $staff_employer_allocation_id);
//            }
        }
    }

    /*Get next staff for bonus top up*/
    public function getNextStaffForBonusTopUp()
    {
        $staff_employer = DB::table('staff_employers_count')->where('isbonus',1)->where('isactive',1)->first();
        return $staff_employer->user_id;
    }


    /*Save staff employer*/
    public function saveStaffEmployerTopUp($user_id, $employer_id, $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $start_date = standard_date_format(Carbon::now());
        $end_date = $allocation->end_date;
        $unit_id = 5;

        $created_at = Carbon::now();
//        foreach($employer_ids as $employer){
        $check_if_allocated = $this->checkIfEmployerAlreadyAllocated($employer_id['id'], $staff_employer_allocation_id);
        if($check_if_allocated == false){
            DB::table('staff_employer')->insert([
                'user_id' => $user_id,
                'employer_id' => $employer_id['id'],
                'start_date' => $start_date,
                'end_date' => $end_date,
                'unit_id' => $unit_id,
                'created_at' => $created_at,
                'staff_employer_allocation_id' => $staff_employer_allocation_id,
                'isbonus' => 1,
                'isactive' => 1
            ]);
//            }
        }

    }


    /**
     * @param $user_id
     * @param $staff_employer_allocation_id
     * REALLOCATE bonus - Tempo - rectification
     */
    /*Reallocate bonus employers for unassigned staff*/
    public function reallocateBonusEmployersForUnassignedStaff($user_id,$staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
        $selected_staff = unserialize($allocation->selected_staff);
        $new_staff = [$user_id];
        $new_selected_staff = array_merge($selected_staff, $new_staff);
        $no_of_employers_to_move = 29;

        foreach($selected_staff as $staff)
        {
            $employer_ids = $this->getBonusEmployersToMoveToOtherStaff($staff, $no_of_employers_to_move, $staff_employer_allocation_id)->toArray();
            foreach($employer_ids as $employer_id)
            {
                DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation_id)->where('user_id', $staff)->where('employer_id', $employer_id->employer_id)->update([
                    'user_id' => $user_id,
                    'prev_user_id' => $staff
                ]);
            }
        }

        /*Update Allocation*/
        $allocation->update([
            'no_of_staff' => sizeof($new_selected_staff),
            'selected_staff' => serialize($new_selected_staff),
        ]);
    }


    /*Get bonus employers to move to other unassigned staff - with no follow ups*/
    public function getBonusEmployersToMoveToOtherStaff($prev_user_id,$no_of_employers_to_move, $staff_employer_allocation_id)
    {
        $employers = DB::table('staff_employer')->select('staff_employer.employer_id as employer_id')
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->where('staff_employer.staff_employer_allocation_id', $staff_employer_allocation_id)
            ->where('staff_employer.isactive', 1)->where('staff_employer.user_id', $prev_user_id)->where('staff_employer.no_of_follow_ups', null)->where('employers.employer_status', 2)->limit($no_of_employers_to_move)->get();

        return $employers;
    }


    /**
     * @param $prev_user_id
     * @param $no_of_employers_to_move
     * @param $staff_employer_allocation_id
     * @return mixed
     * ALLOCATE BONUS EMPLOYERS FOR NEW STAFF / UNASSIGNED STAFF
     */

    /*Get bonus employers to move to other unassigned staff - with  follow ups*/
    public function getBonusEmployersToMoveToOtherStaffWithFollowUps($prev_user_id,$no_of_employers_to_move, $staff_employer_allocation_id)
    {
        $employers = DB::table('staff_employer')->select('staff_employer.employer_id as employer_id', DB::raw(" coalesce(staff_employer.no_of_follow_ups, 0)  as no_of_follow_ups"))
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->where('staff_employer.staff_employer_allocation_id', $staff_employer_allocation_id)
            ->where('staff_employer.isactive', 1)->where('staff_employer.user_id', $prev_user_id)->where('employers.employer_status', 2)->limit($no_of_employers_to_move)->orderByRaw("coalesce(staff_employer.no_of_follow_ups, 0) asc")->get();
        return $employers;
    }


    /*Redistribute bonus employers for new staff assigned*/
    public function redistributeBonusForNewStaff(array $input, $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
        $selected_staff = unserialize($allocation->selected_staff);
        $new_staff = $input['staff'];
        $new_selected_staff = array_merge($selected_staff, $new_staff);
        $allocated_bonus_employers_count = $this->getAllocatedBonusEmployersActiveCount($staff_employer_allocation_id);
        $no_of_new_staff = sizeof($new_staff);
        $no_of_existing_staff = sizeof($selected_staff);
        $total_staff = $no_of_new_staff + $no_of_existing_staff;
        $no_of_employers_per_staff_new = $allocated_bonus_employers_count / $total_staff;
        $no_of_employers_per_staff_new =  floor($no_of_employers_per_staff_new);
        $no_allocated_per_user_now = $this->getNoOfAllocatedBonusByUser($selected_staff[0], $staff_employer_allocation_id);
        $no_of_employers_to_move = $no_allocated_per_user_now - $no_of_employers_per_staff_new;
        $no_of_employers_to_move_per_new_staff = floor( $no_of_employers_to_move / $no_of_new_staff);

        foreach($new_staff as $staff)
        {
            $check_user_already_assigned = $this->checkIfStaffAlreadyAllocated($staff, $staff_employer_allocation_id);
            if($check_user_already_assigned == false){
                /*Reallocate / Replace employers*/
                $this->reallocateOrReplaceFromOtherStaffToNewStaff($staff, $selected_staff, $no_of_employers_to_move_per_new_staff, $staff_employer_allocation_id);
            }
        }
        /*Update Allocation*/
        $allocation->update([
            'no_of_staff' => sizeof($new_selected_staff),
            'selected_staff' => serialize($new_selected_staff),
        ]);
    }

    /*Reallocate for new staff from existing staff*/
    public function reallocateOrReplaceFromOtherStaffToNewStaff($new_user_id, $existing_staff, $no_of_employers_to_move,$staff_employer_allocation_id)
    {
        $Staff_employer_allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
        foreach($existing_staff as $staff)
        {
            $employer_ids = $this->getBonusEmployersToMoveToOtherStaffWithFollowUps($staff, $no_of_employers_to_move, $staff_employer_allocation_id)->toArray();
            foreach($employer_ids as $employer_id)
            {
                if($employer_id->no_of_follow_ups > 0){
                    /*Reallocate - if already followed up atleast once*/
                    (new StaffEmployerRepository())->deactivateUserAllocation($staff,$employer_id->employer_id, $staff_employer_allocation_id);

                    $this->saveStaffEmployerTopUp($new_user_id, $employer_id->employer_id, $staff_employer_allocation_id);

                }elseif($employer_id->no_of_follow_ups == 0){
                    /*Replace - if not followed up */
                    DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation_id)->where('user_id', $staff)->where('employer_id', $employer_id->employer_id)->update([
                        'start_date' => standard_date_format(Carbon::now()),
//                        'start_date' => $Staff_employer_allocation->start_date,
                        'user_id' => $new_user_id,
                        'prev_user_id' => $staff
                    ]);


                }

            }
        }

    }

    /*Get no of allocated bonus employers active*/
    public function getAllocatedBonusEmployersActiveCount($staff_employer_allocation_id)
    {
        $count = DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation_id)->where('isactive', 1)->count();
        return $count;
    }


    /*Get no of allocated bonus employers by user*/
    public function getNoOfAllocatedBonusByUser($user_id, $staff_employer_allocation_id)
    {
        $count = DB::table('staff_employer')->where('staff_employer_allocation_id', $staff_employer_allocation_id)->where('isactive', 1)->where('user_id', $user_id)->count();
        return $count;
    }

}