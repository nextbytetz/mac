<?php

namespace App\Services\Compliance\StaffEmployerAllocation;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto allocating staff to non large employers
 */


use App\Models\Auth\User;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AllocateNonLargeEmployers {
    /**
     * Services for allocating staff to non large employers
     *
     * @param string $model
     *
     * @return void
     */
    protected $allocation_config;
    protected $reallocate_trigger = 0;

    /**
     * AllocateNonLargeEmployers constructor.
     * @param $staff_employer_allocation_id
     */
    public function __construct()
    {
        $this->allocation_config = (new StaffEmployerRepository())->staffEmployerConfig();
    }




    /**
     * @param $staff_employer_allocation_id
     * Allocate Staff to employers
     */
    public function allocateEmployers($staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff_array = unserialize($allocation->selected_staff);

        /*Allocate regional staffs*/
        $this->allocateEmployersForRegionalStaff($selected_staff_array, $staff_employer_allocation_id);

        /*Allocate hq staffs*/
        $this->allocateEmployersForHqStaff($selected_staff_array, $staff_employer_allocation_id);
    }

    /**
     * @param $selected_staff_array
     * @param $staff_employer_allocation_id
     * HQ Allocation-----
     */

    /*Allocate employers for each selected staff on this allocation for dsm staff*/
    public function allocateEmployersForHqStaff($selected_staff_array, $staff_employer_allocation_id)
    {
        $hq_staffs = User::query()->whereIn('id', $selected_staff_array)->where('office_id','=', 1)->inRandomOrder()->get();
        if($hq_staffs->count() > 0){
            foreach($hq_staffs as $staff){
                $this->reallocate_trigger = 0;
                $check_if_already_allocated = $this->checkIfStaffAlreadyAllocated($staff->id, $staff_employer_allocation_id);
                if($check_if_already_allocated == false){
                    /*If not allocated yet - Allocate */
                    $this->allocateEmployersForEachHqStaff($staff, $staff_employer_allocation_id);
                }

            }
        }

    }


    /*Allocate for each hq staff*/
    public function allocateEmployersForEachHqStaff($user, $staff_employer_allocation_id){
        $config = $this->allocation_config;
        $no_of_employers_per_staff = $config->non_large_employers_per_staff;

        /*allocate main region*/
        $this->allocate($user->id,$no_of_employers_per_staff, $staff_employer_allocation_id, null);

    }



    /*---End  of HQ allocation --------*/

    /**
     * @param $user
     * @param $staff_employer_allocation_id
     * REGIONAL STAFF ALLOCATION--------------
     */



    /*Allocate employers for each selected staff on this allocation for Regional staff*/
    public function allocateEmployersForRegionalStaff($selected_staff_array, $staff_employer_allocation_id)
    {
        $regional_staff = User::query()->whereIn('id', $selected_staff_array)->where('office_id','<>', 1)->inRandomOrder()->get();
        if($regional_staff->count() > 0) {
            foreach ($regional_staff as $staff) {
                $this->reallocate_trigger = 0;
                $check_if_already_allocated = $this->checkIfStaffAlreadyAllocated($staff->id, $staff_employer_allocation_id);
                if ($check_if_already_allocated == false) {
                    /*if not allocated - allocate*/
                    $this->allocateEmployersForEachRegionalStaff($staff, $staff_employer_allocation_id);
                }
            }
        }
    }

    /*Allocate for each staff*/
    public function allocateEmployersForEachRegionalStaff($user, $staff_employer_allocation_id){
        $config = $this->allocation_config;
        $no_of_employers_per_staff = $config->non_large_employers_per_staff;
        $percent_main_region = $config->allocation_percent;
        /*Main region*/
        //TODO: this need a function of allocating regional officers to specific regions
        /*Check if officer in allocated regions for compliance*/
        $check = $user->allocatedRegions()->where('unit_id', 5)->count();
        $region_id = ($check > 0) ? $user->allocatedRegions()->where('unit_id', 5)->first()->pivot->region_id : null;
//        $main_region = $user->office->region;
        if(!isset($region_id)){
            /*Not allocated get default from users*/
//            $main_region = ($user->id == 28) ?  Region::query()->find(10) : $user->office->region;//Case for arusha give jj Moshi
            $main_region = $user->office->region;
            $main_region_id = $main_region->id;
        }else{
            /*allocated*/
            $main_region = Region::query()->find($region_id);
            $main_region_id = $region_id;
        }

        $main_region_employers = $no_of_employers_per_staff * 0.01 * $percent_main_region;

        /*allocate main region*/
        $this->allocate($user->id,$main_region_employers, $staff_employer_allocation_id, $main_region_id);
        /*zone*/
        $zone_id = $main_region->office_zone_id;

        /*Other regions*/
        $other_regions_no_of_employers = $no_of_employers_per_staff - $main_region_employers;
        $other_region_ids = Region::query()->where('office_zone_id', $zone_id)->where('id','<>',$main_region_id)->pluck('id')->toArray();
        $no_other_regions = sizeof($other_region_ids);
        $no_employers_per_other_regions = ($no_other_regions > 0)  ? ($other_regions_no_of_employers / $no_other_regions) : 0;
        $no_employers_per_other_regions = floor($no_employers_per_other_regions);
        $remainder_employers = fmod($other_regions_no_of_employers, $no_other_regions);

        if($no_employers_per_other_regions > 0){
            foreach($other_region_ids as $region_id){
                /*allocate other regions*/
                $no_employers_per_other_regions_final = ($remainder_employers > 0) ? ($no_employers_per_other_regions + 1) : $no_employers_per_other_regions;
                $this->allocate($user->id, $no_employers_per_other_regions_final, $staff_employer_allocation_id,$region_id);
                $remainder_employers = $remainder_employers - 1;
            }
        }else{
            /*If there is no other regions in zone Reallocate extra FROM all regions*/
            $this->reallocateExtraAllRegions($user->id, $other_regions_no_of_employers,$staff_employer_allocation_id);
        }

    }

    /*End of regional staff allocation----------*/



    /*Allocate staff to specified no of employers*/
    public function allocate($user_id, $no_of_employers,$staff_employer_allocation_id, $region_id = null, $zone_id = null )
    {
        DB::transaction(function () use($region_id, $user_id, $no_of_employers, $staff_employer_allocation_id, $zone_id) {
            /*Contributing categories*/
            $medium_plus = 2;
            $medium = 3;
            $small = 4;

            /*Round 1 - medium plus*/
            $medium_plus_employers = $this->getEmployers($no_of_employers, $medium_plus, $staff_employer_allocation_id, $region_id, $zone_id);
//        $insert_query = 'INSERT into staff_employer (employer_id, user_id, unit_id, start_date, end_date, created_at) ' . $medium_plus_employers->toSql();
//        DB::statement($insert_query);

            if(sizeof($medium_plus_employers) > 0){
                $this->saveStaffEmployer($user_id, $medium_plus_employers, $staff_employer_allocation_id);
            }

            /*round 2 - medium*/
            $medium_employers_diff = $no_of_employers - (isset($medium_plus_employers) ? sizeof($medium_plus_employers) : 0);

            if ($medium_employers_diff > 0) {
                $medium_employers = $this->getEmployers($medium_employers_diff, $medium, $staff_employer_allocation_id, $region_id, $zone_id);

                if(sizeof($medium_employers) > 0) {
                    $this->saveStaffEmployer($user_id, $medium_employers, $staff_employer_allocation_id);
                }



                /*Round 3 - Small*/
//                $small_employers_diff = $medium_employers_diff - (isset($medium_employers) ? sizeof($medium_employers) : 0);
//
//                if ($small_employers_diff > 0) {
//                    $small_employers = $this->getEmployers($small_employers_diff, $small, $staff_employer_allocation_id, $region_id, $zone_id);
//                    if(sizeof($small_employers) > 0) {
//                        $this->saveStaffEmployer($user_id, $small_employers, $staff_employer_allocation_id);
//                    }
//                }

                /*Round 4 if other region not match target get from main region*/
                $extra_employers_diff = $medium_employers_diff - (isset($medium_employers) ? sizeof($medium_employers) : 0);
//                $extra_employers_diff = $small_employers_diff - (isset($small_employers) ? sizeof($small_employers) : 0);

                if($extra_employers_diff > 0){

                    if($this->reallocate_trigger == 0){
                        /*Allocate to extra zonal*/
                        $this->reallocateExtraZonal($user_id, $extra_employers_diff,$staff_employer_allocation_id);
                    }elseif($this->reallocate_trigger == 1){
                        /*Reallocate extra FROM all regions*/
                        $this->reallocateExtraAllRegions($user_id, $extra_employers_diff,$staff_employer_allocation_id);
                    }

                }

            }

            /*update run status*/
            (new StaffEmployerRepository())->updateRunStatus($staff_employer_allocation_id);
        });
    }

    /*Reallocate extra when other regions donot have enough employers get from main region from the same zone*/
    public function reallocateExtraZonal($user_id, $no_of_employers,$staff_employer_allocation_id )
    {
        $user = User::find($user_id);
        $main_region = $user->office->region;
        $zone_id = $main_region->zone_id;
        $this->reallocate_trigger = 1;
        $this->allocate($user_id, $no_of_employers,$staff_employer_allocation_id, null, $zone_id);
    }

    /*Reallocate extra NationalWide when other regions do not have enough employers get from main region*/
    public function reallocateExtraAllRegions($user_id, $no_of_employers,$staff_employer_allocation_id )
    {

        $user = User::find($user_id);
        $this->allocate($user_id, $no_of_employers,$staff_employer_allocation_id, null, null);
    }


    /*Get employers to be allocated*/
    public function getEmployersOld($no_of_employers, $contrib_category,$staff_employer_allocation_id, $region_id = null, $zone_id = null)
    {
        $allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
        $start_date = standard_date_format($allocation->start_date);
        if (isset($region_id)){
            /*with region*/
            $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id'])
                ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
                ->join('regions', 'employers.region_id', 'regions.id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
                })->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->where('regions.id', $region_id)->whereNull('staff_employer.employer_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->limit($no_of_employers)->inRandomOrder()->get()->toArray();

        }elseif(isset($zone_id)){
            /*With zone id*/
            $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id'])
                ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
                ->join('regions', 'employers.region_id', 'regions.id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
                })->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->where('regions.zone_id', $zone_id)->whereNull('staff_employer.employer_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->limit($no_of_employers)->inRandomOrder()->get()->toArray();
        }else{
            /*with no region / no zone*/
            $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id' ])
                ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
                })->whereNull('staff_employer.employer_id')->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->limit($no_of_employers)->inRandomOrder()->get()->toArray();
        }

        return $employers;
    }

    /*gET EMPLOYERS to be allocated who have contributed within three months*/
    public function getEmployers($no_of_employers, $contrib_category,$staff_employer_allocation_id, $region_id = null, $zone_id = null, $cut_off_months = 3)
    {
        $allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
        $start_date = $allocation->start_date;
        $cut_off_date = Carbon::parse($start_date)->subMonth(3);
        if (isset($region_id)){
            /*with region*/
            $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id'])
                ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
                ->join('regions', 'employers.region_id', 'regions.id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer.isactive', 1);
//                        ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
                })->join('receipts', function($join) use($cut_off_date){
                    $join->on('receipts.employer_id','employers.id')
                        ->where('receipts.iscancelled',0)->where('receipts.isdishonoured', 0)->whereNull('receipts.deleted_at')->where('receipts.created_at', '>',$cut_off_date );
                })
                ->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->where('regions.id', $region_id)->whereNull('staff_employer.employer_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->limit($no_of_employers)->inRandomOrder()->groupBy('contributing_category_employer.employer_id')->get()->toArray();

        }elseif(isset($zone_id)){
            /*With zone id*/
            $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id'])
                ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
                ->join('regions', 'employers.region_id', 'regions.id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer.isactive', 1);
//                        ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
                })->join('receipts', function($join) use($cut_off_date){
                    $join->on('receipts.employer_id','employers.id')
                        ->where('receipts.iscancelled',0)->where('receipts.isdishonoured', 0)->whereNull('receipts.deleted_at')->where('receipts.created_at', '>',$cut_off_date );
                })->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->where('regions.zone_id', $zone_id)->whereNull('staff_employer.employer_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->limit($no_of_employers)->inRandomOrder()->groupBy('contributing_category_employer.employer_id')->get()->toArray();
        }else{
            /*with no region / no zone*/
            $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id' ])
                ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer.isactive', 1);
//                        ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
                })->join('receipts', function($join) use($cut_off_date){
                    $join->on('receipts.employer_id','employers.id')
                        ->where('receipts.iscancelled',0)->where('receipts.isdishonoured', 0)->whereNull('receipts.deleted_at')->where('receipts.created_at', '>',$cut_off_date );
                })
                ->whereNull('staff_employer.employer_id')->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->limit($no_of_employers)->inRandomOrder()->groupBy('contributing_category_employer.employer_id')->get()->toArray();
        }

        return $employers;
    }



    /*Save staff employer*/
    public function saveStaffEmployer($user_id, $employer_ids, $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
//        $start_date = ($allocation->isapproved == 0) ?  $allocation->start_date : standard_date_format(Carbon::now());// if not approved is in the beginning user allocation start_date, after approval need to take todays date
        $start_date = $allocation->start_date;
        $end_date = $allocation->end_date;
        $unit_id = 5;
        $created_at = Carbon::now();

        foreach($employer_ids as $employer){
                   $check_if_allocated = $this->checkIfEmployerAlreadyAllocated($employer->employer_id, $staff_employer_allocation_id);
            if($check_if_allocated == false){
                DB::table('staff_employer')->insert([
                    'user_id' => $user_id,
                    'employer_id' => $employer->employer_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'unit_id' => $unit_id,
                    'created_at' => $created_at,
                    'staff_employer_allocation_id' => $staff_employer_allocation_id
                ]);
            }

        }


    }


    /*Check if employer is already allocated*/
    public function checkIfEmployerAlreadyAllocated($employer_id, $staff_employer_allocation_id)
    {
        $check = DB::table('staff_employer')->where('isactive', 1)->where('employer_id', $employer_id)->count();
        if($check > 0)
        {
            return true;

        }else{
            return false;
        }
    }


    /*Check no of allocated employers per staff*/
    public function countNoOfAllocatedEmployersPerStaff($user_id,  $staff_employer_allocation_id){
        $count = DB::table('staff_employer')->where('user_id', $user_id)->where('staff_employer_allocation_id', $staff_employer_allocation_id)->count();
        return $count;
    }

    public function countRemainingEmployersPerStaff($user_id, $staff_employer_allocation_id)
    {
        $config = $this->allocation_config;
        $no_of_employers_per_staff = $config->non_large_employers_per_staff;
        $allocated = $this->countNoOfAllocatedEmployersPerStaff($user_id, $staff_employer_allocation_id);
        return $no_of_employers_per_staff - $allocated;
    }



    /**
     * Allocate Non large employers for new staff
     */
    public function allocateForNewStaff($staff_employer_allocation_id, $user_id)
    {
        $this->reallocate_trigger = 0;
        $user = User::query()->find($user_id);
        $user_region = $user->allocatedRegions()->where('unit_id', 5)->first();
        $user_region_pivot = isset($user_region) ? $user_region->pivot : null;
        $region_id = isset($user_region_pivot) ? $user_region_pivot->region_id : $user->office->region->id;
        $check_if_already_allocated = (new StaffEmployerRepository())->checkIfStaffAlreadyAllocated($user_id, $staff_employer_allocation_id);

        if ($check_if_already_allocated == false) {
            if($region_id == 1){
                /*Allocate as HQ staff*/
                $this->allocateEmployersForEachHqStaff($user, $staff_employer_allocation_id);
            }else{
                /*Allocate as regional officer*/

                $this->allocateEmployersForEachRegionalStaff($user, $staff_employer_allocation_id);
            }
        }
    }





    /*
     *
     * REPLACE SMALL EMPLOYERS ALLOCATED TO STAFF----------
     * */

    /*Replace all small + medium employers with Medium plus*/
    public function replaceSmallMediumEmployers($staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff_array = unserialize($allocation->selected_staff);

        //TODO remove rectify double allocation
//        $this->rectifyDoubleAllocation();
        /*Allocate regional staffs*/
        $this->replaceSmallForRegionalStaff($selected_staff_array, $staff_employer_allocation_id);

        /*Allocate hq staffs*/
        $this->reallocateMediumPlusUnallocated($staff_employer_allocation_id);

        /*Replace allocated small employers*/
//        $this->replaceAllocatedSmallEmployers($staff_employer_allocation_id);

        /*Replace dormant employers*/
//        $this->replaceDormantEmployers($staff_employer_allocation_id);
    }



    /*Allocate employers for each selected staff on this allocation for Regional staff*/
    public function replaceSmallForRegionalStaff($selected_staff_array, $staff_employer_allocation_id)
    {
        $regional_staff = User::query()->whereIn('id', $selected_staff_array)->where('office_id','<>', 1)->inRandomOrder()->get();
        if($regional_staff->count() > 0) {
            foreach ($regional_staff as $staff) {
                $this->reallocate_trigger = 0;
                $get_small_employers = $this->getQuerySmallEmployersPerStaff($staff->id, $staff_employer_allocation_id)->get();
                foreach($get_small_employers as $small_employer)
                {
                    $region_id = $small_employer->region_id;
                    $zone_id = $small_employer->zone_id;
                    $replacement_employer_region =$this->getQueryMediumEmployers($region_id)->first();
                    if(isset($replacement_employer_region)){
                        $employer_replacement = $replacement_employer_region;
                    }else{
                        $replacement_employer_zone =$this->getQueryMediumEmployers(null,$zone_id)->first();

                        /*Get from zone*/
                        if(isset($replacement_employer_zone)){
                            $employer_replacement = $replacement_employer_zone;
                        }else{
                            /*Get from all regions*/
                            $replacement_employer_all =   $this->getQueryMediumEmployers(null,null)->first();
                            $employer_replacement = $replacement_employer_all;
                        }

                    }
                    /*If replacement exists - Replace*/
                    if(isset($employer_replacement)){
                        $this->replaceSmallEmployer($small_employer->employer_id, $employer_replacement->employer_id, $small_employer->staff_employer_id);
                    }
                }

            }
        }
    }



    /*Get small employers per staff*/
    public function getQuerySmallEmployersPerStaff($user_id, $staff_employer_allocation_id)
    {
        $employers = DB::table('staff_employer')->select('employers.id as employer_id', 'employers.region_id as region_id', 'staff_employer.id as staff_employer_id', 'regions.office_zone_id as zone_id')
            ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'staff_employer.employer_id')
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->join('regions', 'regions.id', 'employers.region_id')
            ->where('user_id', $user_id)->where('staff_employer_allocation_id',$staff_employer_allocation_id )
            ->where('contributing_category_employer.contributing_category_id', 4);
        return $employers;
    }


    /*Get medium employers per staff*/
    public function getQueryMediumEmployers($region_id = null, $zone_id = null)
    {
        if(isset($region_id)){

            $employers = $this->queryForUnallocatedMediumEmployers()
                ->where('employers.region_id', $region_id)
                ->orderBy('contributing_category_employer.contributing_category_id', 'asc');

        }elseif($zone_id){
            $employers = $this->queryForUnallocatedMediumEmployers()
                ->where('regions.office_zone_id', $zone_id)
                ->orderBy('contributing_category_employer.contributing_category_id', 'asc');

        }else {
            $employers = $this->queryForUnallocatedMediumEmployers()
                ->orderBy('contributing_category_employer.contributing_category_id', 'asc');
        }
        return $employers;
    }


    /*Query for medium employers not allocated yet*/
    public function queryForUnallocatedMediumEmployers()
    {
        $employers = DB::table('employers')->select('employers.id as employer_id')
            ->leftJoin('staff_employer', function($join){
                $join->on('staff_employer.employer_id', 'employers.id')->where('staff_employer.isactive', 1);
            })
            ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'employers.id')
            ->join('regions', 'employers.region_id', 'regions.id')
            ->whereRaw('staff_employer.id is NULL')
            ->whereRaw('(contributing_category_employer.contributing_category_id  = 2 or contributing_category_employer.contributing_category_id = 3)')
            ->where('employers.is_treasury', false)->whereNull('employers.duplicate_id')->where('employers.employer_status', 1);

        return $employers;
    }


    /*Replace small employer on allocation*/
    public function replaceSmallEmployer($old_employer_id,$new_employer_id, $staff_employer_id)
    {
        DB::table('staff_employer')->where('id', $staff_employer_id)
            ->update([
                'employer_id' => $new_employer_id,
                'old_employer_id' => $old_employer_id,
                'updated_at' => Carbon::now()
            ]);
    }



    /**
     * Replace Medium Plus
     *
     */

    public function reallocateMediumPlusUnallocated($staff_employer_allocation_id)
    {
        $unallocated_employers = $this->queryForUnallocatedMediumEPlusOnlyEmployers()->get();
        foreach ($unallocated_employers as $unallocated_employer)
        {
            $employer_for_replacement = $this->getMediumSmallForReplacementMediumPlus($staff_employer_allocation_id)->first();

            $this->replaceSmallEmployer($employer_for_replacement->employer_id, $unallocated_employer->employer_id, $employer_for_replacement->staff_employer_id);
        }
    }

    /*Query for medium employers not allocated yet*/
    public function queryForUnallocatedMediumEPlusOnlyEmployers()
    {
        $employers = DB::table('employers')->select('employers.id as employer_id')
            ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'employers.id')
            ->join('regions', 'employers.region_id', 'regions.id')
            ->leftJoin('staff_employer', function($join){
                $join->on('staff_employer.employer_id', 'employers.id')->where('isactive', 1);
            })
            ->whereNull('staff_employer.id')
            ->whereRaw('contributing_category_employer.contributing_category_id  = 2')
            ->where('is_treasury', false)->whereNull('duplicate_id')->where('employer_status', 1);

        return $employers;
    }


    public function getMediumSmallForReplacementMediumPlus($staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff_array = unserialize($allocation->selected_staff);
        $hq_users = User::query()->whereIn('id', $selected_staff_array)->where('office_id',1)->pluck('id')->toArray();
        $employers = DB::table('employers')->select('employers.id as employer_id', 'staff_employer.id as staff_employer_id')
            ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'employers.id')
            ->join('regions', 'employers.region_id', 'regions.id')
            ->join('staff_employer', function($join) use($staff_employer_allocation_id){
                $join->on('staff_employer.employer_id', 'employers.id')->where('isbonus', 0)->where('isactive', 1)->where('staff_employer_allocation_id', $staff_employer_allocation_id);
            })
            ->whereRaw('(contributing_category_employer.contributing_category_id  = 3 or  contributing_category_employer.contributing_category_id  = 4)')
            ->whereIn('staff_employer.user_id',$hq_users)
            ->where('is_treasury', false)->whereNull('duplicate_id')->where('employer_status', 1)
            ->orderBy('staff_employer.no_of_follow_ups', 'desc');

        return $employers;
    }


//    /*Replace allocated small employers*/
//    public function replaceAllocatedSmallEmployers($staff_employer_allocation_id)
//    {
//
//    }
    /*Replace small employers for staff TODO tempo function*/
    public function replaceAllocatedSmallEmployers($staff_employer_allocation_id)
    {
        $small_employers = DB::table('staff_employer')->select('staff_employer.employer_id as employer_id', 'staff_employer.id as staff_employer_id')
            ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'staff_employer.employer_id')
            ->where('staff_employer.isactive', 1)->where('staff_employer_allocation_id',$staff_employer_allocation_id)
            ->where('contributing_category_employer.contributing_category_id',4)->get();

        foreach($small_employers as $employer){
            $new_medium = $this->getUnallocatedAllMediumsEmployers(1)->first();
            $this->replaceSmallEmployer($employer->employer_id,$new_medium->employer_id, $employer->staff_employer_id);
        }
    }


    /*Query for medium employers i.e. medium and medium plus not allocated yet*/
    public function getUnallocatedAllMediumsEmployers($limit)
    {
        $employers = DB::table('employers')->select('employers.id as employer_id')
            ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'employers.id')
            ->join('regions', 'employers.region_id', 'regions.id')
            ->leftJoin('staff_employer', function($join){
                $join->on('staff_employer.employer_id', 'employers.id')->where('isactive', 1);
            })
            ->whereNull('staff_employer.id')
            ->whereRaw('(contributing_category_employer.contributing_category_id  = 2 or contributing_category_employer.contributing_category_id  = 3)')
            ->where('is_treasury', false)->whereNull('duplicate_id')->where('employer_status', 1)->orderBy('contributing_category_employer.contributing_category_id')->limit($limit)->get();

        return $employers;
    }


    /*Replace dormant employers*/
    public function replaceDormantEmployers($staff_employer_allocation_id)
    {
        $employers = DB::table('staff_employer')->select('staff_employer.employer_id as employer_id', 'staff_employer.id as staff_employer_id')
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->where('staff_employer.isactive', 1)->where('staff_employer_allocation_id',$staff_employer_allocation_id)
            ->whereRaw("(employers.employer_status = 2)")->get();

        foreach($employers as $employer){
            $new_medium = $this->getUnallocatedAllMediumsEmployers(1)->first();
            $this->replaceSmallEmployer($employer->employer_id,$new_medium->employer_id, $employer->staff_employer_id);
        }

    }


    /**
     * Top up employers up to 200 for each staff has less than least no of employers to be allocated
     */
    public function maximizeEmployersForStaffOnCurrentAllocation()
    {
        $config = DB::table('staff_employer_configs')->first();
        $no_employers_per_staff = $config->non_large_employers_per_staff;
        $non_large_allocation_active = StaffEmployerAllocation::query()->where('category', 2)->where('isactive', 1)->first();
        $staffs = unserialize($non_large_allocation_active->selected_staff);
        foreach ($staffs as $staff)
        {
            $counter = 0;
            $new_medium_employer_ids  = [];
            $no_of_employers_allocated = DB::table('staff_employer')->where('user_id', $staff)->where('staff_employer_allocation_id', $non_large_allocation_active->id)->where('isactive',1)->count();
            $diff = $no_of_employers_allocated - $no_employers_per_staff;
            if($diff < 0)
            {
                $nos_to_add = -1 * $diff;
                while($counter < $nos_to_add){
                    $new_medium = $this->getUnallocatedAllMediumsEmployers(1)->toArray();
//
//                    $new_medium_employer_ids= ['employer_id' => $new_medium->employer_id];
//                    dd($new_medium->employer_id);
                          /*SAVE EMPLOYERS TO STAFF*/
                    $this->saveStaffEmployer($staff, $new_medium, $non_large_allocation_active->id);
                    $counter = $counter + 1;
                }

            }
        }

    }



}