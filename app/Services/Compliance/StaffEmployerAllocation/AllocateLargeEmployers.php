<?php

namespace App\Services\Compliance\StaffEmployerAllocation;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto allocating staff to non large employers
 */


use App\Models\Auth\User;
use App\Models\Location\Region;
use App\Models\Operation\Compliance\Member\StaffEmployerAllocation;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AllocateLargeEmployers {
    /**
     * Services for allocating staff to non large employers
     *
     * @param string $model
     *
     * @return void
     */
    protected $allocation_config;

    /**
     * AllocateNonLargeEmployers constructor.
     * @param $staff_employer_allocation_id
     */
    public function __construct()
    {
        $this->allocation_config = (new StaffEmployerRepository())->staffEmployerConfig();
    }

    /*Query for large contributors*/
    public function queryForLargeContributors()
    {
        return DB::table('contributing_category_employer')
            ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
            ->where('contributing_category_id', 1)
            ->where('employer_status', 1)
            ->whereNull('duplicate_id')
            ->where('is_treasury', false)
            ->where('employers.id', '<>',9386);//Hazina
    }


    /**
     * @param $staff_employer_allocation_id
     * Allocate Staff to employers
     */
    public function allocateEmployers($staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff_array = unserialize($allocation->selected_staff);


        /*Allocate staffs*/
        $this->allocateEmployersForStaff($selected_staff_array, $staff_employer_allocation_id);
    }

    /**
     * @param $selected_staff_array
     * @param $staff_employer_allocation_id
     * Staff Allocation-----
     */

    /*Allocate employers for each selected staff on this allocation for staff*/
    public function allocateEmployersForStaff($selected_staff_array, $staff_employer_allocation_id)
    {
        $large_category = 1;
        $staffs = User::query()->whereIn('id', $selected_staff_array)->inRandomOrder()->get();
        $no_of_staffs = $staffs->count();
        $no_of_employers = $this->queryForLargeContributors()->count();
        $no_employers_per_staff =  ($no_of_staffs > 0) ? floor($no_of_employers / $no_of_staffs) : 0;
        $remainder_employers = fmod($no_of_employers, $no_of_staffs);

        foreach($staffs as $staff){
            $check_if_already_allocated = (new StaffEmployerRepository())->checkIfStaffAlreadyAllocated($staff->id, $staff_employer_allocation_id);
            if($check_if_already_allocated == false){
                /*If not allocated yet - Allocate */
                $no_employers_per_staff_final = ($remainder_employers > 0) ? ($no_employers_per_staff + 1) : $no_employers_per_staff;

                $this->allocateEmployersForEachStaff($staff,$no_employers_per_staff_final, $staff_employer_allocation_id);
                $remainder_employers = $remainder_employers - 1;
            }
        }
    }


    /*Allocate for each staff*/
    public function allocateEmployersForEachStaff($user, $no_of_employers_per_staff, $staff_employer_allocation_id){

        /*allocate main region*/
        $this->allocate($user->id,$no_of_employers_per_staff, $staff_employer_allocation_id, null);

    }






    /*Allocate staff to specified no of employers*/
    public function allocate($user_id, $no_of_employers,$staff_employer_allocation_id, $region_id = null )
    {
        DB::transaction(function () use($region_id, $user_id, $no_of_employers, $staff_employer_allocation_id) {
            /*Contributing categories*/
            $large = 1;

            /*Round 1 - employers*/
            $employers = $this->getEmployers($no_of_employers, $large, $staff_employer_allocation_id, $region_id);


            $this->saveStaffEmployer($user_id, $employers, $staff_employer_allocation_id);

            /*update run status if complete*/
            (new StaffEmployerRepository())->updateRunStatus($staff_employer_allocation_id);

        });
    }

    /*Get employers to be allocated*/
    public function getEmployers($no_of_employers, $contrib_category,$staff_employer_allocation_id, $region_id = null)
    {

        if (isset($region_id)){
            /*with region*/
            $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id'])
                ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
                ->join('regions', 'employers.region_id', 'regions.id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
                })->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->where('regions.id', $region_id)->whereNull('staff_employer.employer_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->where('employers.id', '<>',9386)->limit($no_of_employers)->inRandomOrder()->get()->toArray();

        }else{
            /*with no region*/
            $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id' ])
                ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
                ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                    $join->on('staff_employer.employer_id', 'employers.id')
                        ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
                })->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->whereNull('staff_employer.employer_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->where('employers.id', '<>',9386)->limit($no_of_employers)->inRandomOrder()->get()->toArray();
        }

        return $employers;
    }




    /*Save staff employer*/
    public function saveStaffEmployer($user_id, $employer_ids, $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $start_date = $allocation->start_date;
        $end_date = $allocation->end_date;
        $unit_id = 5;
        $created_at = Carbon::now();
        foreach($employer_ids as $employer){
            $check_if_allocated = $this->checkIfEmployerAlreadyAllocated($employer->employer_id, $staff_employer_allocation_id);
            if($check_if_allocated == false){
                DB::table('staff_employer')->insert([
                    'user_id' => $user_id,
                    'employer_id' => $employer->employer_id,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'unit_id' => $unit_id,
                    'created_at' => $created_at,
                    'staff_employer_allocation_id' => $staff_employer_allocation_id
                ]);
            }
        }


    }


    /*Check if employer is already allocated*/
    public function checkIfEmployerAlreadyAllocated($employer_id, $staff_employer_allocation_id)
    {
        $check = DB::table('staff_employer')->where('isactive', 1)->where('employer_id', $employer_id)->count();
        if($check > 0)
        {
            return true;

        }else{
            return false;
        }
    }




    /**
     * ALLOCATE NEW STRATEGY - Round robin all employers
     */

    /*Allocate employers for each selected staff on this allocation for staff - FOR TOP UP*/
    public function allocateEmployersUsingRobin( $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff_array = unserialize($allocation->selected_staff);
        $count = 0;
        $staff_count = sizeof($selected_staff_array);
        $large_employers = $this->getLargeEmployers($staff_employer_allocation_id);
        foreach ($large_employers as $employer){

            $next_user = $selected_staff_array[$count];

            /*Get employer*/

            /*Allocate*/
            $this->saveStaffEmployerUsingRobin($next_user, $employer->employer_id, $staff_employer_allocation_id);
            if(($staff_count - 1) <= $count){

                $count = 0;
            }else{
                $count = $count + 1;
            }
        }

        /*update run status if complete*/
        (new StaffEmployerRepository())->updateRunStatus($staff_employer_allocation_id);
    }




    /*Get next staff for allocation with least no of employers*/
    public function getNextStaffForAllocation($staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $selected_staff_array = unserialize($allocation->selected_staff);
        $staff_employer_allocated_count = DB::table('staff_employers_count')->where('isbonus',0)->where('isactive',0)->whereIn('user_id', $selected_staff_array)->count();
        $count = 0;
        $staff_count = sizeof($selected_staff_array);
//        if(sizeof($selected_staff_array) == $staff_employer_allocated_count){
//            /*If all already assigned*/
//            $staff_employer = DB::table('staff_employers_count')->where('isbonus',0)->where('isactive',0)->whereIn('user_id', $selected_staff_array)->first();
//        }else{
//            $staff_employer = DB::table('users')->select('users.id as user_id')
//                ->leftJoin('staff_employers_count', function($join){
//                    $join->on('staff_employers_count.user_id', 'users.id')
//                        ->where('staff_employers_count.isbonus',0)->where('staff_employers_count.isactive',0);
//                } )
//                ->whereIn('users.id', $selected_staff_array)->whereNull('staff_employers_count.user_id')->first();
//        }
        foreach ($selected_staff_array as $staff){
            $next_user = $staff[$count];

            /*Get employer*/
            $employer = $this->getNextLargeEmployer($staff_employer_allocation_id);

            /*Allocate*/
            $this->saveStaffEmployerUsingRobin($next_user, $employer->employer_id, $staff_employer_allocation_id);
            if($staff_count <= $count){
                $count = 0;
            }else{
                $count = $count + 1;
            }
        }

//        return $staff_employer->user_id;
    }

    /*Get employers to be allocated using robin*/
    public function getNextLargeEmployer($staff_employer_allocation_id)
    {
        $contrib_category = 1; //large
        $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id' ])
            ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
            ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                $join->on('staff_employer.employer_id', 'employers.id')
                    ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
            })->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->whereNull('staff_employer.employer_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->first();


        return $employers;
    }

    public function getLargeEmployers($staff_employer_allocation_id)
    {
        $contrib_category = 1; //large
        $employers = DB::table('contributing_category_employer')->select(['contributing_category_employer.employer_id' ])
            ->join('employers', 'employers.id', 'contributing_category_employer.employer_id')
            ->leftJoin('staff_employer', function($join) use($staff_employer_allocation_id){
                $join->on('staff_employer.employer_id', 'employers.id')
                    ->where('staff_employer_allocation_id', $staff_employer_allocation_id);
            })->where('employers.employer_status',1)->whereNull('employers.duplicate_id')->whereNull('staff_employer.employer_id')->where('contributing_category_employer.contributing_category_id', $contrib_category)->where('employers.is_treasury', false)->where('employers.id', '<>',9386)->inRandomOrder()->get();


        return $employers;
    }



    /*Save staff employer using Robin*/
    public function saveStaffEmployerUsingRobin($user_id, $employer_id, $staff_employer_allocation_id)
    {
        $allocation = StaffEmployerAllocation::find($staff_employer_allocation_id);
        $start_date = $allocation->start_date;
        $end_date = $allocation->end_date;
        $unit_id = 5;
        $created_at = Carbon::now();
        $check_if_allocated = $this->checkIfEmployerAlreadyAllocated($employer_id, $staff_employer_allocation_id);
        if($check_if_allocated == false){
            DB::table('staff_employer')->insert([
                'user_id' => $user_id,
                'employer_id' => $employer_id,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'unit_id' => $unit_id,
                'created_at' => $created_at,
                'staff_employer_allocation_id' => $staff_employer_allocation_id
            ]);
        }
    }

    /*End of robin*/








    /**
     * Fill employers to reach limit of 200 - Allocate medium and medium plus
     */

    public function allocateStaffToMaxTarget($staff_employer_allocation_id)
    {
        $config = DB::table('staff_employer_configs')->first();
        $staff_employer_allocation = StaffEmployerAllocation::query()->find($staff_employer_allocation_id);
        $selected_staff = unserialize($staff_employer_allocation->selected_staff);
        foreach($selected_staff as $staff){
            $user = User::query()->find($staff);
            $no_of_target = $config->non_large_employers_per_staff;
            $no_allocated_employers = $user->relationEmployers()->where('staff_employer_allocation_id', $staff_employer_allocation_id)->where('isactive', 1)->count();
            $no_remained_employers = $no_of_target - $no_allocated_employers;

            /*Allocate employers*/
            $this->allocateEmployersToMaxTargetForStaff($user,$no_remained_employers, $staff_employer_allocation);
        }

        /*Replace small employers*/
        $this->replaceSmallEmployersForLargeStaff($staff_employer_allocation_id);
    }

    public function allocateEmployersToMaxTargetForStaff($user, $no_of_employers, $staff_employer_allocation)
    {
        $employer_ids =   $this->getUnallocatedAllMediumsEmployers($no_of_employers)->toArray();

        $this->saveStaffEmployer($user->id, $employer_ids, $staff_employer_allocation->id);

        /*Update no of employers on allocation*/
        $staff_employer_allocation->update([
            'no_of_employers' => $staff_employer_allocation->no_of_employers_allocated,

        ]);
    }


    /*Query for medium employers i.e. medium and medium plus not allocated yet*/
    public function getUnallocatedAllMediumsEmployers($limit)
    {
        $employers = DB::table('employers')->select('employers.id as employer_id')
            ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'employers.id')
            ->join('regions', 'employers.region_id', 'regions.id')
            ->leftJoin('staff_employer', function($join){
                $join->on('staff_employer.employer_id', 'employers.id')->where('isactive', 1);
            })
            ->whereNull('staff_employer.id')
            ->whereRaw('(contributing_category_employer.contributing_category_id  = 1 or contributing_category_employer.contributing_category_id  = 2 or contributing_category_employer.contributing_category_id  = 3)')
            ->where('is_treasury', false)->whereNull('duplicate_id')->where('employer_status', 1)->orderBy('contributing_category_employer.contributing_category_id')->limit($limit)->get();

        return $employers;
    }


    /*Replace small employers for large staff TODO tempo function*/
    public function replaceSmallEmployersForLargeStaff($staff_employer_allocation_id)
    {
        $small_employers = DB::table('staff_employer')->select('staff_employer.employer_id as employer_id', 'staff_employer.id as staff_employer_id')
            ->join('contributing_category_employer', 'contributing_category_employer.employer_id', 'staff_employer.employer_id')
            ->where('staff_employer_allocation_id', $staff_employer_allocation_id)->where('staff_employer.isactive',1)
            ->where('contributing_category_employer.contributing_category_id',4)->get();

        foreach($small_employers as $employer){
            $new_medium = $this->getUnallocatedAllMediumsEmployers(1)->first();
            $this->replaceSmallEmployer($employer->employer_id,$new_medium->employer_id, $employer->staff_employer_id);
        }
    }

    /*Replace small employer on allocation*/
    public function replaceSmallEmployer($old_employer_id,$new_employer_id, $staff_employer_id)
    {
        DB::table('staff_employer')->where('id', $staff_employer_id)
            ->update([
                'employer_id' => $new_employer_id,
                'old_employer_id' => $old_employer_id,
                'updated_at' => Carbon::now()
            ]);
    }


    /*Reactivate allocation to owner/user*/
    public function reactivateToUser($staff_employer_id)
    {
        $staff_employer = DB::table('staff_employer')->where('id', $staff_employer_id)->first();
        $allocation = StaffEmployerAllocation::query()->find($staff_employer->staff_employer_allocation_id);
        /*Update/Reactivate*/
        DB::table('staff_employer')->where('id', $staff_employer_id)->update([
            'isactive' => 1,
            'end_date' => $allocation->end_date,
            'replacement_type' => null,
            'updated_at' => Carbon::now()
        ]);
    }

}