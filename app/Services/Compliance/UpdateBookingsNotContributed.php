<?php

namespace App\Services\Compliance;
/*
 *
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto update bookings which are not yet contributed
 */

use App\Models\Finance\Receipt\LegacyReceipt;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Compliance\Traits\UpdateBookingsNotContributed\PostEmployerContribRcvableScheduleTrait;
use App\Services\Compliance\Traits\UpdateBookingsNotContributed\UpdatePortalContributionArrearsStatusTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UpdateBookingsNotContributed {

    use UpdatePortalContributionArrearsStatusTrait, PostEmployerContribRcvableScheduleTrait;

    protected $estimate_base = null;
    protected $closure = 0;

    public function __construct()
    {

    }


    /*Loop on all bookings and update*/
    public function updateAllBookingsNotContributed($employer_id)
    {
        $bookings = new BookingRepository();
        $employer = (new EmployerRepository())->find($employer_id);
        /*delete all before doc*/
        $this->deleteBookingBeforeDoc($employer);
        $bookings_not_contributed = $bookings->query()->where('employer_id', $employer_id)->whereDoesntHave('receiptCodes', function($query){
            $query->has('activeReceipt');
        })->get();
        foreach ($bookings_not_contributed as $booking){
            /*check if this month is in closure*/
            $check_if_in_closure = (new BookingRepository())->checkIfMonthWasInEmployerClosure($employer, $booking->rcv_date);
            if($check_if_in_closure == false){
                /*if no in closure - update booking*/
                $this->updateBooking($booking);
            }else{
                /*if in closure - delete*/
                $this->deleteBookingIfIsInBusinessClosure($employer, $booking);
            }
        }
    }

    public function updateBooking(Model $booking)
    {
        $old_booked_amount = $booking->amount;
        $employer = (new EmployerRepository())->find($booking->employer_id);
        $recent_amount  = (new BookingRepository())->getRecentContributionAmount($booking->employer_id, $booking->rcv_date);
//        $check_if_contrib_month_in_rct_code = $this->checkIfContribMonthPaidInReceiptCode($employer, $booking);
        $check_if_contributed_with_legacy = $this->checkIfPaidAndUpdatePayFlagWithLegacy($booking);
        if($recent_amount > 0 && $recent_amount != $old_booked_amount && $this->checkThresholdDifferenceIsOk( $employer,  $old_booked_amount, $recent_amount) && $check_if_contributed_with_legacy == false){

            $booking->update([
                'amount' => $recent_amount,
                'old_booked_amount' => $old_booked_amount,
                'ispaid' => 0,
            ]);
        }
    }





    /*Check if existing and new bookings should not exceed 100 % difference*/
    public function checkThresholdDifferenceIsOk(Model $employer, $old_booked_amount, $new_booked_amount)
    {
        $new_booked_amount = ($new_booked_amount > 0) ? $new_booked_amount : 1;

        $check = ($old_booked_amount / $new_booked_amount);
        //TODO: need to agree cut off threshold to update
        if($check > 12 && $employer->receipts()->count() > 0)
        {
            return false;
        }else{
            return true;
        }

    }


    /**
     * @param $employer_id
     * @param $booking_date
     * Update booking when receipting
     */
    public function updateBookingNonContributedByRcvDate($employer_id, $booking_date)
    {
        $bookings = new BookingRepository();
        $bookings_not_contributed = $bookings->query()->where('employer_id', $employer_id)->where('rcv_date', standard_date_format($booking_date))->whereDoesntHave('receiptCodes', function($query){
            $query->has('activeReceipt');
        })->first();
        if($bookings_not_contributed)
        {
            $this->updateBooking($bookings_not_contributed);
        }

    }





    /*Delete duplicate bookings entries*/
    public function deleteDuplicateBookingEntries()
    {
        $duplicate_bookings = DB::table("booking_duplicates")->select('employer_id', 'booking_month', 'booking_year')->get();
        foreach($duplicate_bookings as $booking){
            /*delete booking*/
            $booking_to_delete =   Booking::query()->where('employer_id', $booking->employer_id)
            // ->whereMonth('rcv_date','=', $booking->booking_month)->whereYear('rcv_date','=', $booking->booking_year)
            ->whereRaw("date_part('month', rcv_date) = " .  $booking->booking_month)
            ->whereRaw("date_part('year', rcv_date) = " .  $booking->booking_year)
            ->whereDoesntHave('receiptCodes', function ($query){
                $query->whereHas('receipt', function($query){
                    $query->where('iscancelled', 0);
                });
            })->orderBy('id', 'desc')->first();

            if($booking_to_delete){
                $booking_to_delete->update([
                    'deleted_at' => Carbon::now(),
                    'delete_reason' => 'Duplicate booking'
                ]);
            }

        }

    }



    /**
     * @param Model $employer
     * @param $booking
     * Delete booking if is in business closure
     */
    public function deleteBookingIfIsInBusinessClosure(Model $employer, $booking){

        $check_if_in_closure = (new BookingRepository())->checkIfMonthWasInEmployerClosure($employer, $booking->rcv_date);

        if($check_if_in_closure == true){
            $booking->update([
                'deleted_at' => Carbon::now(),
                'delete_reason' => 'Bookings is in Closure Period'
            ]);;
        }else{
            /*not in closure dont delete*/
        }
    }




    /**
     * @param Model $employer
     * @param $booking_date
     * Delete non booked booking before doc incase doc is updated
     */
    public function deleteBookingBeforeDoc(Model $employer){
        $doc_start_of_month = standard_date_format(Carbon::parse($employer->doc)->startOfMonth());
        Booking::query()->where('employer_id', $employer->id)->whereDate('rcv_date','<',$doc_start_of_month)->whereDoesntHave('receiptCodes', function ($query){
            $query->whereHas('receipt', function($query){
                $query->where('iscancelled', 0);
            });
        })->update([
            'deleted_at' => Carbon::now(),
            'delete_reason' => 'Bookings Before DOC'
        ]);;


    }


    /*Delete allinterests for deleted bookings*/
    public function deleteAllInterestWithNoBookingPerEmployer($employer_id)
    {
//        DB::table('non_booked_booking_interests')->join('non_booked_bookings', 'non_booked_bookings.id', 'non_booked_booking_interests.non_booked_booking_id')
//            ->where('non_booked_bookings.employer_id', $employer_id)->whereDate('non_booked_booking_interests.rcv_date', '<', standard_date_format($employer_doc));
        BookingInterest::query()->whereDoesntHave('booking', function($query) use($employer_id){
            $query->where('employer_id', $employer_id);
        })->whereNull('new_payment_date')->delete();
    }



    /*Delete live booking interests which dont have live bookings*/
    public function deleteLiveBookingInterestsWithNoBooking()
    {
        BookingInterest::query()->doesntHave('booking')->where('added_to_bill', false)->whereNull('new_payment_date')->update([
            'deleted_at' => Carbon::now(),
            'delete_reason' => 'Interest with no booking (Booking has been deleted'
        ]);
    }

    /*Check and update if paid on receipt code*/
    public function checkIfContribMonthPaidInReceiptCode(Model $employer, Model $booking)
    {
        $booking_date_parsed = Carbon::parse($booking->rcv_date);
        $receipt_code_count = ReceiptCode::query()->whereHas('activeReceipt', function($query) use($employer){
            $query->where('employer_id', $employer->id);
        })
        // ->whereMonth('contrib_month','=', $booking_date_parsed->format('m'))->whereYear('contrib_month','=', $booking_date_parsed->format('Y'))
        ->whereRaw("date_part('month', contrib_month) = " .  $booking_date_parsed->format('m'))
        ->whereRaw("date_part('year', contrib_month) = " .  $booking_date_parsed->format('Y'))
        ->count();

        if($receipt_code_count > 0){
            /*Paid*/
            $booking->update(['ispaid' => 1]);
            return true;
        }else{
            $booking->update(['ispaid' => 0]);
            return false;
        }
    }

    /*check if paid and update flag*/
    public function checkIfPaidAndUpdatePayFlagWithLegacy(Model $booking)
    {
        $paid_amount = (new BookingRepository())->getPaidAmountWithLegacy($booking->id);
        if($paid_amount > 0){
            /*Paid*/
            $booking->update(['ispaid' => 1]);
            return true;
        }else{
            $booking->update(['ispaid' => 0]);
            return false;
        }
    }


    /*Delete treasury employers where does not have */
    public function deleteLiveBookingsOfTreasuryEmployers()
    {
        Booking::query()->whereHas('employer', function($query){
            $query->where('is_treasury', true);
        })->whereDoesntHave('receiptCodes', function($query){
            $query->whereHas('receipt', function($query){
                $query->where('iscancelled',0);
            });
        })->update([
            'deleted_at' => Carbon::now(),
            'delete_reason' => 'Bookings of treasury employer'
        ]);
    }


    /**
     * Update paid flag for contributed bookings
     */
    public function updatePaidFlagForContributedBookings()
    {
        /*Does not have receipt*/
        (new BookingRepository())->query()->whereDoesntHave('receiptCodes', function($query){
            $query->has('activeReceipt')->whereHas('finCode', function($query){
                $query->where('fin_code_group_id', 3);
            });
        })->where('bookings.ispaid',1)->update([
            'ispaid' => 0
        ]);

        /*Does have receipt*/
        (new BookingRepository())->query()->whereHas('receiptCodes', function($query){
            $query->has('activeReceipt')->whereHas('finCode', function($query){
                $query->where('fin_code_group_id', 3);
            });
        })->where('bookings.ispaid',0)->update([
            'ispaid' => 1
        ]);


        /*update from contribution - ispaid = 0 for legacy receipts which are in booking with inactive receipt*/
        (new BookingRepository())->query()
        ->join('employer_contributions as c', function ($join){
            $join->on('c.employer_id', 'bookings.employer_id')
            ->whereRaw("date_part('year' :: text, bookings.rcv_date) = date_part('year' :: text, c.contrib_month) and date_part('month' :: text, bookings.rcv_date) = date_part('month' :: text, c.contrib_month)");
        })->where('bookings.ispaid', 0)
        ->update([
            'ispaid' => 1
        ]);




    }


    /**
     * Update booking id on receipt code for booking mismtach with booking id on receipt codes
     */
    public function rectifyBookingIdOnReceiptCodes()
    {
        $booking_receipt_codes = Booking::query()->select('bookings.id as booking_id', 'bookings.employer_id', 'receipt_codes.contrib_month as contrib_month', 'receipt_codes.id as code_id')
        ->join('receipt_codes', 'receipt_codes.booking_id', 'bookings.id')
        ->join('receipts', 'receipts.id', 'receipt_codes.receipt_id')
        ->where('receipts.isdishonoured', 0) ->where('receipts.iscancelled', 0)
        ->whereNull('receipts.deleted_at')->whereRaw("concat_ws('-', date_part('year' :: text, bookings.rcv_date), date_part('month' :: text, bookings.rcv_date)) <> concat_ws('-', date_part('year' :: text, receipt_codes.contrib_month), date_part('month' :: text, receipt_codes.contrib_month))")->get();


        foreach ($booking_receipt_codes as $booking_receipt_code)
        {
            $contrib_month = Carbon::parse($booking_receipt_code->contrib_month);
            $booking = Booking::query()
            // ->whereMonth('rcv_date','=',$contrib_month->format('m') )->whereYear('rcv_date','=',$contrib_month->format('Y') )
            ->whereRaw("date_part('month', rcv_date) = " .  $contrib_month->format('m'))
            ->whereRaw("date_part('year', rcv_date) = " .  $contrib_month->format('Y'))
            ->where('employer_id', $booking_receipt_code->employer_id)
            ->first();

            if($booking){
                ReceiptCode::query()->where('id', $booking_receipt_code->code_id)->update([
                    'booking_id' => $booking->id,
                    'booking_id_orig' => $booking_receipt_code->booking_id
                ]);
            }

        }
    }




    /**
     * Update booking id on receipt code of duplicate employers for corresponding employer
     */
    public function rectifyBookingIdOnReceiptCodesFrDuplicateEmployers()
    {
        /*Get bookings for duplicate employers*/
        $bookings_duplicate_employers = Booking::query()->select('bookings.id as booking_id','bookings.rcv_date', 'employers.id as employer_id', 'receipt_codes.id as code_id', 'employers.duplicate_id as active_employer_id')
        ->join('employers', 'employers.id', 'bookings.employer_id')
        ->join('receipt_codes', 'receipt_codes.booking_id', 'bookings.id')
        ->whereNotNull('employers.duplicate_id')->whereNull('bookings.deleted_at')->get();


        foreach ($bookings_duplicate_employers as $booking_duplicate_employer)
        {

//            $active_employer = Employer::query()->find($booking_duplicate_employer->active_employer_id);
//
            $parse_rcv_date = Carbon::parse($booking_duplicate_employer->rcv_date);
            $booking_active_employer = Booking::query()->where('employer_id', $booking_duplicate_employer->active_employer_id)
            // ->whereMonth('rcv_date','=', $parse_rcv_date->format('m'))->whereYear('rcv_date','=', $parse_rcv_date->format('Y'))
            ->whereRaw("date_part('month', rcv_date) = " .  $parse_rcv_date->format('m'))
            ->whereRaw("date_part('year', rcv_date) = " .  $parse_rcv_date->format('Y'))
            ->first();

            /*If booking exist*/
            if(isset($booking_active_employer->id)){
                /*update receipt code*/
                ReceiptCode::query()->where('id', $booking_duplicate_employer->code_id)->update([
                    'booking_id' => $booking_active_employer->id,
                    'booking_id_orig' => $booking_duplicate_employer->booking_id
                ]);

            }else{
                /*update booking - employer_id with active employer*/
                Booking::query()->where('id',$booking_duplicate_employer->booking_id)->update([
                    'employer_id' => $booking_duplicate_employer->active_employer_id,
                    'employer_orig' => $booking_duplicate_employer->employer_id,
                ]);
            }

        }

    }



    /**
     * Update booking id on receipt code of wrongly merged duplicate employers for corresponding employer - Restore state for those employers wrongly merged
     */
    public function unRectifyBookingIdOnReceiptCodesFrDuplicateEmployers()
    {
        /*Get bookings for wrongly duplicate employers*/
        $bookings_wrong_duplicate_employers = Booking::query()->select('bookings.id as booking_id', 'employers.id as employer_id', 'receipt_codes.id as code_id', 'receipt_codes.booking_id as duplicate_booking_id', 'receipt_codes.booking_id_orig as old_booking_id ')
        ->join('employers', 'employers.id', 'bookings.employer_id')
        ->join('receipt_codes', 'receipt_codes.booking_id_orig', 'bookings.id')
        ->whereNull('employers.duplicate_id')->whereNull('bookings.deleted_at')->get();


        foreach ($bookings_wrong_duplicate_employers as $booking_wrong_duplicate_employer)
        {
            /*update receipt code*/
            ReceiptCode::query()->where('id', $booking_wrong_duplicate_employer->code_id)->update([
                'booking_id' => $booking_wrong_duplicate_employer->booking_id,
                'booking_id_orig' => null
            ]);

        }

        /*Unrectify those updated employer id on bookings*/
        $bookings_wrong_duplicate_employers2 = Booking::query()->select('bookings.id as booking_id', 'employers.id as employer_id', 'bookings.employer_id as wrong_employer_id')
        ->join('employers', 'employers.id', 'bookings.employer_orig')
        ->whereNull('employers.duplicate_id')->whereNull('bookings.deleted_at')->get();

        foreach ($bookings_wrong_duplicate_employers2 as $booking_wrong_duplicate_employer)
        {
            /*Update booking - employer id*/
            Booking::query()->where('id',$booking_wrong_duplicate_employer->booking_id)->update([
                'employer_id' => $booking_wrong_duplicate_employer->employer_id,
                'employer_orig' => $booking_wrong_duplicate_employer->wrong_employer_id,
            ]);


        }

    }



    /**
     * Rectify duplicate bookings which are all contributed - Merge bookings of the same month for the same employer
     */

    /*Merge contributed duplicate bookings*/
    public function mergeContributedDuplicateBookings()
    {
        DB::transaction(function(){
            $booking_repo = new BookingRepository();
            $interest_repo = new BookingInterestRepository();
            $receipt_code_repo = new ReceiptCodeRepository();
            $duplicate_bookings = DB::table('booking_duplicates')->get();

            foreach($duplicate_bookings as $duplicate_booking){

                $booking_month = $duplicate_booking->booking_month;
                $booking_year = $duplicate_booking->booking_year;
                $employer_id = $duplicate_booking->employer_id;

                //Get min booking from all these duplicates for this employer
                $min_booking = $booking_repo->query()->where('employer_id',$employer_id )
                // ->whereMonth('rcv_date','=', $booking_month)->whereYear('rcv_date','=', $booking_year)
                ->whereRaw("date_part('month', rcv_date) = " .  $booking_month)
                ->whereRaw("date_part('year', rcv_date) = " .  $booking_year)
                ->orderBy('id')->first();

                $other_booking_ids =  $booking_repo->query()->select('id')->where('employer_id',$employer_id )
                // ->whereMonth('rcv_date','=', $booking_month)->whereYear('rcv_date','=', $booking_year)
                ->whereRaw("date_part('month', rcv_date) = " .  $booking_month)
                ->whereRaw("date_part('year', rcv_date) = " .  $booking_year)
                ->where('id','<>', $min_booking->id)->get()->toArray();
                $all_booking_ids = array_merge($other_booking_ids, [$min_booking->id]);


                $total_booked_amount = $booking_repo->query()->whereIn('id', $all_booking_ids)->sum('amount');

                /*--Start: Merge Booking Table--*/

                //update other bookings i.e. duplicate_id, deleted_at
                $booking_repo->query()->whereIn('id', $other_booking_ids)->update([
                    'duplicate_id' => $min_booking->id,
                    'deleted_at' => Carbon::now(),
                    'delete_reason' => 'Merged booking which are already contributed'
                ]);

                //Update min booking i.e. old booked amount, amount
                $booking_repo->query()->where('id','=', $min_booking->id)->update([
                    'old_booked_amount' => $min_booking->amount,
                    'amount' => $total_booked_amount
                ]);

                /*---End Booking table---*/


                /*--Start: Merge Booking Interest Table---*/

                //Update of min booking i.e. old_interest_amount, amount
                $min_interest = $min_booking->bookingInterest;
                if($min_interest) {
                    $total_interest_amount = $interest_repo->query()->whereIn('booking_id', $all_booking_ids)->sum('amount');
                    $interest_repo->query()->where('booking_id', $min_booking->id)->update([
                        'old_interest_amount' => $min_interest->amount,
                        'amount' => $total_interest_amount
                    ]);

                    //update interests of other bookings i.e. duplicate_id, deleted_at
                    $interest_repo->query()->whereIn('booking_id', $other_booking_ids)->update([
                        'duplicate_id' => $min_interest->id,
                        'deleted_at' => Carbon::now(),
                        'delete_reason' => 'Merged booking which are already contributed'
                    ]);
                }
                /*--End: Booking interests---*/


                /*--Start: Merge Receipt Code table--*/

                //Update other bookings i.e. old_booking_id
                foreach($other_booking_ids as $key => $other_booking_id){
                    foreach($other_booking_id as $booking_id){
                        $receipt_code_repo->query()->where('booking_id', $booking_id)->update([
                            'booking_id_orig' => $booking_id,
                            'booking_id' => $min_booking->id
                        ]);
                    }

                }


                /*--End: Receipt code--*/
            }
        });
}




    /**
     * Update Receipt codes with no bookings - Update booking_id on receipt codes table i.e. contrib paid in advance and Contrib month is less than or equal to this month
     */

    public function updateBookingIdOnReceiptCodesWithNoBookingId()
    {
        $end_month = Carbon::parse(getTodayDate())->endOfMonth();
        //Get receipt codes with no booking id
        $receipt_codes = ReceiptCode::query()->has('activeReceiptWithDishonor')->doesntHave('booking')->where('contrib_month', '<=', standard_date_format($end_month))->get();
        //Loop on receipt codes
        foreach($receipt_codes as $receipt_code){
            $booking = Booking::query()->where('employer_id', $receipt_code->employer_id)
            // ->whereMonth('rcv_date','=', Carbon::parse($receipt_code->contrib_month)->format('n'))->whereYear('rcv_date','=', Carbon::parse($receipt_code->contrib_month)->format('Y'))
            ->whereRaw("date_part('month', rcv_date) = " .  Carbon::parse($receipt_code->contrib_month)->format('n'))
            ->whereRaw("date_part('year', rcv_date) = " .  Carbon::parse($receipt_code->contrib_month)->format('Y'))
            ->first();
            if($booking){
                $receipt_code->update([
                    'booking_id' => isset($booking) ?  $booking->id : $receipt_code->booking_id,
                    'booking_id_orig' =>  isset($booking) ?  $receipt_code->booking_id : $receipt_code->booking_id_orig
                ]);
            }
        }

    }
}