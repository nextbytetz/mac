<?php
namespace App\Services\Compliance\Traits;

use Carbon\Carbon;
use Auth;
use DB;
use Log;
use Excel;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException as ConnectException;
use Guzzle\Http\Exception\ClientErrorResponseException as ClientError;
use Guzzle\Http\Exception\ServerErrorResponseException as ServerError;
use Guzzle\Http\Exception\BadResponseException as BadResponse;
use GuzzleHttp\Exception\ClientException as ClientException;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Shared_Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\Models\Operation\Compliance\Member\Employer;

use DateTime;
use DateTimeZone;
use App\Exceptions\WorkflowException;

trait EmployeesExcel {



  public function checkIfHasPendingBill($employer_id, $payroll_id)
  {
    $pending_bill = DB::table('portal.bills')->where('employer_id', '=', $employer_id)->where('payroll_id',$payroll_id)
    ->where('bill_status','<',3)->where('expire_date', '>=', Carbon::now())->where('dishonoured_cheque', null)->count();
    return $pending_bill;
  }


  public function uploadExcelToDb($request, $type, $payroll_id){

    if ($request->hasFile('attachment') || $request->hasFile('document_file37')) {

     $file = !is_null($request->attachment) ? $request->file("attachment") : $request->file("document_file37");

     if($file->isValid()){

      $path = ( !is_null($request->attachment) ? $request->file("attachment")->getRealPath() : $request->file('document_file37')->getRealPath());

      $data = Excel::selectSheetsByIndex(0)->load($path, function($reader) {
       $reader->sheet(0, function ($sheet) {
      // $highestrow = $sheet->getHighestRow();
      // $sheet->cell('E')->getStyle('E2:E'.$highestrow)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDD2);
       });
     })->get();

      $response = $this->checkColumnOnUploadedFile($path,$request);
      if(!empty($response)){
       $data =  $response->getData();
     }
     if(!empty($data->errors)){
       return $response;
     } 
     if(!empty($data) && $data->count()){ 
      if ($type != 'addbulk') {
       $declared =  ($type == 'save') ?  $this->declaredRegistration($request->employer_id) : $request->employee_count;
       $uploaded = 0;
       foreach ($data as $key => $value) {
         if(!empty($this->removeAllspaces($value->firstname))){
          $uploaded++;
        }
      }
      if($uploaded != $declared){ 
       return response()->json(array('errors' => ['employee_count' => 'Uploaded Employees : '.$uploaded.' doesn\'t match the number of employees  declared : '.$declared.' Kindly check and rectify'], 'id'=>$request->employer_id));
     }
   }

   $response = $this->checkTotalGrosspay($data, $request, $type);
   if(!empty($response)){
    $data =  $response->getData();
  }
  if(!empty($data->errors)){
    return $response;
  }else{

    $return = DB::transaction(function () use ($data , $request, $type, $payroll_id) { 
     if ($type == 'addbulk' || $type == 'save') {
      $employer = DB::table('main.employers')->find($request->id);
    }
    if ($type == 'advance_payment' ) {
      $advance_payments = new AdvancePaymentRepository();
      $ap_request = $advance_payments->store($request->all(), $request,$payroll_id);

      DB::table('portal.advance_payments')
      ->where('id',$ap_request->id)
      ->update([
        'has_uploaded' => true,
      ]);
      $request->request->add(['id'=>$ap_request->id]); 

    }

    $check_excel_errors = $this->checkExcelError($data,$request->employer_id,'without_wcf_number');
    if(!empty($check_excel_errors)){
      $data_excel_errors =  $check_excel_errors->getData();
    }
    if(!empty($data_excel_errors->errors)){
      return $check_excel_errors;
    } else {

      foreach ($data as $key => $value) { 
        if(!empty($this->removeAllspaces($value->firstname))){
         $new = $this->formatDateExcel($value->dob);
         $preg = preg_replace('/[^A-Za-z0-9\-]/', '/',$new);
         $newDate = date("Y-m-d", strtotime($preg));
         if(strtoupper($this->removeAllspaces($value->gender)) == 'F' || strtoupper($this->removeAllspaces($value->gender)) == 'FEMALE'){
          $sex = 2;
        }
        else{
          $sex = 1;
        }

        switch (strtoupper($this->removeAllspaces($value->employment_category))) {
          case 'TEMPORARY':
          $emp_cate = 'TEMPORARY';
          $employee_category_cv_id = 84;
          break;
          case 'PERMANENT':
          $emp_cate = 'PERMANENT';
          $employee_category_cv_id = 85;
          break;
          default:
          $emp_cate = 'SPECIFIC TASK';
          $employee_category_cv_id = 86;
          break;
        }
        $input = [
          'firstname' => strtoupper($this->removeAllspaces($value->firstname)),
          'middlename' => strtoupper($this->removeAllspaces($value->middlename)),
          'lastname' => strtoupper($this->removeAllspaces($value->lastname)),
          'employer_id' => $request->employer_id,
          'dob' => Carbon::parse($newDate)->format('Y-m-d'),
          'sex' => $sex,
          'basicpay' => str_replace( ',', '', $value->basicpay),
          'grosspay' => str_replace( ',', '', $value->grosspay),
          'job_title' => strtoupper($value->job_title),
          'employee_category_cv_id' => $employee_category_cv_id,
          'resource_id' => $request->id,
          'emp_cate' => $emp_cate,
          'payroll_id' => $payroll_id,
          'gender' => strtoupper($this->removeAllspaces($value->gender)),
          'national_id' => $this->removeAllspaces($value->national_id),
          'tin' => isset($employer->tin) ? $employer->tin : NULL,
        ];
        $this->generateMemberNumber($input, $type);
      }
    }

  }
});

    if(!empty($return)){
      $transaction_data =  $return->getData();
    }
    if(!empty($transaction_data->errors)){
      return $return;
    } 
    else{
      return response()->json(array('success' => true));
    }
  } 
}

}else{
  Log::info('---------------  attachment Error --------');
  Log::info(print_r($file->getErrorMessage(), true));
  Log::info('--------------- end error ----------');
  return response()->json(array('errors' => ['attachment' => ' There was an error in uploading the attachment. Kindly check and retry'],'id'=>$request->employer_id));
}

}
}



public function checkColumnOnUploadedFile($linked_file, $request, $type=null)
{

 try {
   $headings = Excel::selectSheetsByIndex(0)
   ->load($linked_file)
   ->takeRows(1)
   ->first()
   ->keys()
   ->toArray();
 }
 catch (\Error $e) {
   logger("=============== error");
   logger($e);
   return response()->json(array('errors' => ['attachment' => ' The attachment is empty. Make sure data is filled correctly on the first sheet of the attachment then try again']));
 }
 catch (\Exception $e) {
  logger("=============== exception");

  logger($e);
  return response()->json(array('errors' => ['attachment' => ' The attachment not correctly formatted. Make sure data is filled correctly on the first sheet then try again']));
}


if ($type == 'annual_return') {
  $verifyArr = ['firstname','middlename','lastname','age', 'sex', 'job_title','employment_category','annual_earnings'];
  foreach ($verifyArr as $key) {
    if (!in_array($key,$headings, true)) {
      switch ($key) {
        case 'job_title':
        $key = 'job title';
        break;
        case 'employment_category':
        $key = 'employment category';
        break;
        case 'annual_earnings':
        $key = 'annual earnings';
        break;
        default:
        $key = $key;
        break;
      }
      return response()->json(array('errors' => ['attachment' => strtoupper($key).' column is missing in the attachment. Kindly rectify and try again']));
    }
  }
}else{
  $header_array = ['wcf_number'];
  foreach ($header_array as $key) {
   if (in_array($key, $headings, true)) {
    return response()->json(array('errors' => ['attachment' => 'Headings format is not correct. Kindly rectify as per required format and try again'], 'id'=>$request->employer_id));
  }
}

$verifyArr = ['firstname','middlename','lastname','dob','basicpay','grosspay', 'gender', 'job_title', 'employment_category','national_id'];
foreach ($verifyArr as $key) {
 if (!in_array($key, $headings, true)) {
  return response()->json(array('errors' => ['attachment' => $key.' column is missing in the attachment. Kindly rectify and try again'], 'id'=>$request->employer_id));
}
}
}

    // if (count($headings) != count($verifyArr)) {
    //  // Log::info(count($verifyArr));
    //  return response()->json(array('errors' => ['attachment' => ' Your excel Employee list headings not formatted correctly. Kindly rectify and try again'], 'id'=>$request->employer_id));
    // }

}


public function editExcelToDb($request, $type, $payroll_id){

  if ($request->hasFile('document_file37')) {

   $file = $request->file("document_file37");

   if($file->isValid()){

    $path =  $request->file('document_file37')->getRealPath();

    $data = Excel::selectSheetsByIndex(0)->load($path, function($reader) {})->get();

    $response = $this->checkColumnOnEditEmployees($path,$request);
    if(!empty($response)){
     $data =  $response->getData();
   }
   if(!empty($data->errors)){
     return $response;
   } 
   if(!empty($data) && $data->count()){ 

    $return = DB::transaction(function () use ($data, $request, $type, $payroll_id) { 
      $employer = DB::table('main.employers')->find($request->employer_id);

      $check_excel_errors = $this->checkExcelError($data,$request->employer_id,'with_wcf_number');
      if(!empty($check_excel_errors)){
        $data_excel_errors =  $check_excel_errors->getData();
      }
      if(!empty($data_excel_errors->errors)){
        return $check_excel_errors;
      } else {

        foreach ($data as $key => $value) { 

         if(!empty($this->removeAllspaces($value->firstname))){
          if(strtoupper($this->removeAllspaces($value->gender)) == 'F' || strtoupper($this->removeAllspaces($value->gender)) == 'FEMALE'){
           $sex = 2;
           $gender = 'F';
         }
         else{
           $sex = 1;
           $gender = 'M';
         }


         switch (strtoupper($this->removeAllspaces($value->employment_category))) {
           case 'TEMPORARY':
           $emp_cate = 'TEMPORARY';
           $employee_category_cv_id = 84;
           break;
           case 'PERMANENT':
           $emp_cate = 'PERMANENT';
           $employee_category_cv_id = 85;
           break;
           default:
           $emp_cate = 'SPECIFIC TASK';
           $employee_category_cv_id = 86;
           break;
         }


         $input = [
           'firstname' => strtoupper($this->removeAllspaces($value->firstname)),
           'middlename' => strtoupper($this->removeAllspaces($value->middlename)),
           'lastname' => strtoupper($this->removeAllspaces($value->lastname)),
           'employer_id' => $request->employer_id,
           'dob' => Carbon::parse($value->dob)->format('Y-m-d'),
           'sex' => $sex,
           'basicpay' => str_replace( ',', '', $value->basicpay),
           'grosspay' => str_replace( ',', '', $value->grosspay),
           'job_title' => strtoupper($value->job_title),
           'employee_category_cv_id' => $employee_category_cv_id,
           'memberno' => $value->wcf_number,
           'resource_id' => $request->id,
           'emp_cate' => $emp_cate,
           'payroll_id' => $payroll_id,
           'gender' => $gender,
           'national_id' => $this->removeAllspaces($value->national_id),
           'tin' => !is_null($employer->tin) ? $employer->tin : NULL,
         ];



         $this->saveEmployee($input, $type);
       }

     }

     return response()->json(array('success' => true));
   }
 });

    if(!empty($return)){
      $edit_transaction_data =  $return->getData();
    }
    if(!empty($edit_transaction_data->errors)){
      return $return;
    } 
    else{
      return response()->json(array('success' => true));
    }

  }

}else{
  Log::info('---------------  attachment --------');
  Log::info(print_r($file->getErrorMessage(), true));
  Log::info('--------------- end error ----------');
  return response()->json(array('errors' => ['attachment' => ' There was an error in uploading the attachment. Kindly check and retry'],'id'=>$request->employer_id));
}

}
}


public function checkColumnOnEditEmployees($linked_file, $request)
{ 
  $headings = Excel::selectSheetsByIndex(0)
  ->load($linked_file)
  ->takeRows(1)
  ->first()
  ->keys()
  ->toArray();

  $verifyArr = ['wcf_number','firstname','middlename','lastname','dob','basicpay','grosspay', 'gender', 'job_title', 'employment_category','national_id'];
  foreach ($verifyArr as $key) {

   if (!in_array($key, $headings, true)) {
            // return response()->json(['success' => false, 'message'=> $key.' Column is missing in your excel']);
    return response()->json(array('errors' => ['attachment' => $key.' column is missing in the Employee list attachment. Kindly rectify and try again'], 'id'=>$request->employer_id));
  }
}

    // if (count($headings) != count($verifyArr)) {
    //  // Log::info(count($verifyArr));
    //  return response()->json(array('errors' => ['attachment' => ' Your excel Employee list header not formatted correctly. Kindly rectify and try again'], 'id'=>$request->employer_id));
    // }
}

public function checkTotalGrosspay($data, $request,$type){
  if($type == ('arrear' || 'advance_payment')){

    if (isset($request->total_grosspay)) {
     $totalgross = 0;
     foreach ($data as $key => $column) {
      if(!empty($this->removeAllspaces($column->firstname))) {
        $totalgross += (float)str_replace( ',', '', $this->removeAllspaces($column->grosspay));
      }
    }
    $totalgross = round($totalgross,2);
    if($totalgross != (float)str_replace( ',', '', $request->total_grosspay)){
     Log::info('Excel total '.$totalgross.' Declared total '.$request->total_grosspay);

     return response()->json(array('errors' => ['total_grosspay' => 'Total grosspay in your uploaded employees list attachment (Excel) is not as declared here. Please check and rectify.'],'id'=>$request->employer_id));
   }
 } 

} 

}

public function generateMemberNumber($input, $type){ 

  $diff_now_dob = 0;
  try{
   $dob = Carbon::parse($input['dob']);
   $tdy =   Carbon::parse(Carbon::now());
   $diff_now_dob = $tdy->diffInYears($dob);

 }catch( \ Exception $e){
   $resp = $e->getCode();
   Log::info('DOB Error');
   Log::info(print_r($input,true));
   return response()->json(array('errors' => ['attachment' => 'There was an error in date of birth (dob) the attachment. Kindly check and rectify']));
 }

 if ($diff_now_dob >= 15) {
   $employee_exist = DB::table('main.employees')->where('firstname', $input['firstname'])
   ->where('middlename',  $input['middlename'])
   ->where('lastname',  $input['lastname'])
   ->where('dob',  $input['dob'])->first(); 

   if(empty($employee_exist)){
    $registeredEmployeeId =  DB::table('main.employees')->insertGetId([
      'firstname' => $input['firstname'],
      'middlename' => $input['middlename'],
      'lastname' => $input['lastname'],
      'dob' => $input['dob'],
      'gender_id' =>$input['sex'] ,
      'nid' =>$input['national_id'],
      'emp_cate' => $input['emp_cate'],
      'memberno' => 0,
      'created_at' => Carbon::now(),
    ]);

    $memberno = checksum($registeredEmployeeId, sysdefs()->data()->employee_number_length);
    DB::table('main.employees')->where('id', $registeredEmployeeId)->update([
      'memberno' => $memberno,
      'updated_at' => Carbon::now(),
    ]);

    $input['memberno'] = $memberno;
    $input['employee_id'] = $registeredEmployeeId;  

  } else {
    $input['memberno'] = $employee_exist->memberno;
    $input['employee_id'] = $employee_exist->id;
  }
  $this->saveEmployee($input, $type);
}

}



public function saveEmployee($input, $type)
{
  switch ($type) {
   case 'addbulk':
      //akiverify organization or akiadd bulk or akiadd tu this will be called
   $this->saveEmployeeUpload($input);
   $this->saveEmployeeEmployer($input);
   $this->updateEmployeeHistory($input);
   break;
   case 'save':
      //akiregister new organization this will be called
   $this->saveEmployeeUpload($input);
   $this->saveEmployeeEmployer($input);
   $this->updateEmployeeHistory($input);
   break;
   case 'arrear':
      //akiadd kwenye arrear this will be called
   $this->saveEmployeesArrear($input);
   break;
   case 'advance_payment':
      //akiadd kwenye advance payment this will be called
   $this->saveAdvancePaymentEmployee($input);
   break;
   case 'edit':
      //akiedit bulk this will be called
   $this->editEmployee($input);
   break;
   default:
   break;
 }

}

public function saveAdvancePaymentEmployee($input)
{   
  $ap_months = DB::table('portal.advance_payment_months')->where('advance_payment_id',$input['resource_id'])->get();
  foreach ($ap_months as $ap_month) {
   DB::table('portal.advance_payment_employees')->insert([
     'employee_id' => $input['employee_id'],
     'basicpay' => $input['basicpay'],
     'grosspay' => $input['grosspay'],
     'job_title' => $input['job_title'],
     'employee_category_cv_id' => $input['employee_category_cv_id'],
     'advance_payment_month_id' => $ap_month->id,
     'created_at' => Carbon::now(),
     'updated_at' => Carbon::now(),
   ]);
 }
}



public function saveEmployeesArrear($input)
{
  $e_arrear_exist = DB::table('portal.employee_employer_arrears')
  ->where('contribution_arrear_id', $input['resource_id'])
  ->where('employee_id',  $input['employee_id'])
  ->where('employer_id',  $input['employer_id'])->first();

  if(empty($e_arrear_exist))
  {
   DB::table('portal.employee_employer_arrears')->insert([ 
     'employee_id' => $input['employee_id'],
     'employer_id' => $input['employer_id'],
     'contribution_arrear_id' => $input['resource_id'],
     'basicpay' => $input['basicpay'],
     'grosspay' => $input['grosspay'],
     'job_title' => $input['job_title'],
     'employee_category_cv_id' => $input['employee_category_cv_id'],
     'created_at' => Carbon::now(),
     'updated_at' => Carbon::now(),
   ]);
 }

}
public function  saveEmployeeUpload($input){
  $employee_upload_exist = DB::table('main.employee_uploads')
  ->where('employer_id', $input['resource_id'])
  ->where('firstname', $input['firstname'])
  ->where('middlename',  $input['middlename'])
  ->where('lastname',  $input['lastname'])
  ->where('payroll_id',  $input['payroll_id'])
  ->where('dob', $input['dob'])->first();

  if(empty($employee_upload_exist)){
   $employeeupload = DB::table('main.employee_uploads')->insert([
    'employer_id' => $input['resource_id'],
    'memberno' => $input['memberno'],
    'tin' => $input['tin'],
    'firstname' => $input['firstname'],
    'middlename' => $input['middlename'],
    'lastname' => $input['lastname'],
    'dob' => $input['dob'],
    'sex' => $input['sex'],
    'job_title' => $input['job_title'],
    'emp_cate' => $input['emp_cate'],
    'payroll_id' => $input['payroll_id'],
    'employee_category_cv_id' => $input['employee_category_cv_id'],
    'salary' => $input['basicpay'],
    'grosspay' => $input['grosspay'],
    'national_id' => $input['national_id']
  ]);

   return $employeeupload->id;
 }
 else{
  $employee_upload_exist = DB::table('main.employee_uploads')
  ->where('id',$employee_upload_exist->id)->update([
    'job_title' => $input['job_title'],
    'emp_cate' => $input['emp_cate'],
    'sex' => $input['sex'],
    'employee_category_cv_id' => $input['employee_category_cv_id'],
    'salary' => $input['basicpay'],
    'grosspay' => $input['grosspay'],
    'national_id' => $input['national_id']
  ]);
  return $employee_upload_exist->id;
}
}


public function saveEmployeeEmployer($input){
    // Log::info(print_r($input,true));
  $employee_exist = DB::table('main.employee_employer')
  ->where('employer_id', $input['resource_id'])
  ->where('employee_id',  $input['employee_id'])
  ->where('payroll_id', $input['payroll_id'])->whereNull('deleted_at')->first();

  if (empty($employee_exist)) {
   DB::table('main.employee_employer')
   ->insert([ 
     'employee_id' =>  $input['employee_id'],
     'employer_id' =>  $input['resource_id'],
     'basicpay' => $input['basicpay'],
     'grosspay' => $input['grosspay'], 
     'employee_category_cv_id' => $input['employee_category_cv_id'], 
     'job_title' =>  $input['job_title'], 
     'payroll_id' =>  $input['payroll_id'], 
     'updated_at' => Carbon::now(),
     'created_at' => Carbon::now(),
   ]); 
 } else {
   DB::table('main.employee_employer')
   ->where('id',$employee_exist->id)
   ->limit(1)
   ->update([ 
     'basicpay' => $input['basicpay'],
     'grosspay' => $input['grosspay'], 
     'employee_category_cv_id' => $input['employee_category_cv_id'], 
     'job_title' =>  $input['job_title'], 
     'updated_at' => Carbon::now(),
     'created_at' => Carbon::now(),
   ]);
 }

}



public function editEmployee($input)
{
  $employee_upload_exist = DB::table('portal.employee_uploads')
  ->where('employer_id',  $input['employer_id'])
  ->where('payroll_id',  $input['payroll_id'])
  ->where('memberno',  $input['memberno'])->first();

  if(!empty($employee_upload_exist)){ 

   $emp = DB::table('main.employees')
   ->where('memberno',$input['memberno'])
   ->first();

   if(!empty($emp)){
    DB::table('main.employees')
    ->where('id',$emp->id)
    ->limit(1)
    ->update([ 
      'nid' => $input['national_id'], 
      'job_title' =>  $input['job_title'], 
      'updated_at' => Carbon::now(),
      'created_at' => Carbon::now(),
    ]);

    DB::table('main.employee_employer')
    ->where('employee_id',$emp->id)
    ->where('employer_id',  $input['employer_id'])
    ->where('payroll_id',  $input['payroll_id'])
    ->limit(1)
    ->update([ 
      'basicpay' => $input['basicpay'],
      'grosspay' => $input['grosspay'], 
      'employee_category_cv_id' => $input['employee_category_cv_id'], 
      'job_title' =>  $input['job_title'], 
      'updated_at' => Carbon::now(),
      'created_at' => Carbon::now(),
    ]);

  }
  DB::table('main.employee_uploads')
  ->where('id',$employee_upload_exist->id)->update([
    'job_title' => $input['job_title'],
    'emp_cate' => $input['emp_cate'],
    'sex' => $input['sex'],
    'employee_category_cv_id' => $input['employee_category_cv_id'],
    'salary' => $input['basicpay'],
    'grosspay' => $input['grosspay'],
    'national_id' => $input['national_id']
  ]);


}



}


public function updateEmployeeHistory($input)
{
  DB::table('portal.employee_removal_history')
  ->where('employer_id', $input['resource_id'])
  ->where('employee_id',  $input['employee_id'])
  ->where('payroll_id', $input['payroll_id'])
  ->where('is_removed', true)
  ->update([
    'restored_by' => access()->user()->id,
    'is_removed' => false,
    'restored_on' => Carbon::now(),
    'restore_source' => 2,
    'updated_at' => Carbon::now(),
  ]);
}



public function returnContributionAmount($employer_id,$total_grosspay)
{

  $organization = DB::table('main.employer')->find($employer_id);

  if($organization->employer_category_cv_id == 37){ 
     // private organization
   $contrib_amount = ($total_grosspay * 0.01);

 }
 else{ 
   $contrib_amount = ($total_grosspay * 0.005);
 } 

 return $contrib_amount;
}


public function isContributionMonthInLegacy($employer_id, $contrib_month)
{

 $legacy_exist = DB::table('main.legacy_receipts')
 ->where('employer_id', $employer_id)
 // ->where('isuploaded', 1)
 ->where('contrib_month', $contrib_month)->first();

 if ($legacy_exist) {
  return true;
} else {
  return false;
}

}


public function isContributionMonthInLegacyCodes($employer_id, $contrib_month)
{

 $legacy_codes_exist = DB::table('main.legacy_receipts')
 ->join('main.legacy_receipt_codes', 'legacy_receipt_codes.legacy_receipt_id', '=', 'legacy_receipts.id')
 ->where('legacy_receipts.employer_id', $employer_id)
 // ->where('legacy_receipts.isuploaded', 0)
 ->where('legacy_receipt_codes.contrib_month', $contrib_month)->first();

 if ($legacy_codes_exist) {
  return true;
} else {
  return false;
}

}


public function isContributionMonthInReceipt($employer_id, $contrib_month, $payroll_id)
{

 $receipt_exist = DB::table('main.receipt_codes')
 ->join('main.receipts', 'receipt_codes.receipt_id', '=', 'receipts.id')
 ->join('main.employers', 'receipts.employer_id', '=', 'employers.id')
 ->where('iscancelled', 0)
 ->where('receipt_codes.employer_id', $employer_id)
 ->where('receipts.payroll_id', $payroll_id)
 ->where('receipt_codes.contrib_month', $contrib_month)
 ->first();

 if ($receipt_exist) {
  return true;
} else {
  return false;
}
}



public function  isContributionMonthInArrear($employer_id, $contrib_month, $payroll_id){ 

  $arrear_exist = DB::table('portal.contribution_arrears')->where('employer_id',$employer_id)
  ->where('contrib_month',$contrib_month)->where('payroll_id',$payroll_id)->first();

  if($arrear_exist){
   return true;
 }else{
   return false;
 }

}


protected function formatDateExcel($date)
{ 
  $return = $date; 
  if (gettype($date) === 'double') { 
   $birthday = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date); 

   $return = $birthday->format('n/j/Y'); 
 } 

 return $return; 
}


public function checkColumnOnRemoveEmployees($linked_file, $request)
{ 
  $headings = Excel::selectSheetsByIndex(0)
  ->load($linked_file)
  ->takeRows(1)
  ->first()
  ->keys()
  ->toArray();

  $verifyArr = ['wcf_number','firstname','middlename','lastname','dob','basicpay','grosspay', 'gender', 'job_title', 'employment_category','national_id'];
  foreach ($verifyArr as $key) {

   if (!in_array($key, $headings, true)) {
    return response()->json(array('errors' => ['remove_attachment' => $key.' column is missing in your Employee list attachment. Kindly rectify and try again'], 'id'=>$request->employer_id));
  }
}


}


public function removeEmployeesExcel($request, $type, $payroll_id){

  if ($request->hasFile('remove_attachment')) {

   $file = $request->file("remove_attachment");

   if($file->isValid()){

    $path =  $request->file('remove_attachment')->getRealPath();

    $data = Excel::selectSheetsByIndex(0)->load($path, function($reader) {})->get();

    $response = $this->checkColumnOnRemoveEmployees($path,$request);
    if(!empty($response)){
     $data =  $response->getData();
   }
   if(!empty($data->errors)){
     return $response;
   } 
   if(!empty($data) && $data->count()){ 

     $return = DB::transaction(function () use ($data , $request, $type, $payroll_id) { 

      foreach ($data as $key => $value) { 
       if (!empty($value->wcf_number)) {
        $time = Carbon::now();
        $employee_upload = DB::table('portal.employee_uploads')->where('memberno',$value->wcf_number)
        ->where('employer_id',$request->employer_id)->where('payroll_id', $payroll_id)
        ->whereNull('deleted_at')->first();

        if (count($employee_upload) > 0) {
         $employee_upload->delete();
       }


       $employee = DB::table('main.employees')->where('memberno', $value->wcf_number)->first();


       if (isset($employee->id)) {

         $employee_employer = DB::table('main.employee_employer')->where('employee_id',$employee->id)
         ->where('employer_id', $request->employer_id)
         ->where('payroll_id', $payroll_id)
         ->whereNull('deleted_at')->first();


         if (count($employee_employer) > 0) {

          DB::table('main.employee_employer')->where('id',$employee_employer->id)->update([
            'remove_reason' => $request->bulk_remove_reason,
            'deleted_at' => $time,
          ]);

          $employee_history = DB::table('portal.employee_removal_history')->where('employee_id',$employee->id)
          ->where('employer_id', $request->employer_id)
          ->where('payroll_id', $payroll_id)->first();

          if (count($employee_history) < 1) {

           DB::table('portal.employee_removal_history')->insert([
             'employee_id' => $employee->id,
             'employer_id' => $request->employer_id,
             'payroll_id' => $payroll_id,
             'user_id' => access()->user()->id,
             'employee_category_cv_id' => $employee_upload->employee_category_cv_id,
             'salary' => $employee_upload->salary,
             'grosspay' => $employee_upload->grosspay,
             'remove_reason' => $request->bulk_remove_reason,
             'remove_attachment' => '',
             'created_at' => $time,
             'updated_at' => $time,
           ]);
         }else{
           DB::table('portal.employee_removal_history')
           ->where('employee_id',$employee->id)
           ->where('employer_id', $request->employer_id)
           ->where('payroll_id', $payroll_id)->update([
             'user_id' => access()->user()->id,
             'employee_category_cv_id' => $employee_upload->employee_category_cv_id,
             'salary' => $employee_upload->salary,
             'grosspay' => $employee_upload->grosspay,
             'remove_reason' => $request->bulk_remove_reason,
             'is_removed' => true,
             'remove_attachment' => '',
             'created_at' => $time,
             'updated_at' => $time,
           ]);

         }
       }
     }

   }              

 }

 return response()->json(array('success' => true));
});

   }

 }else{
  Log::info('---------------  attachment --------');
  Log::info(print_r($file->getErrorMessage(), true));
  Log::info('--------------- end error ----------');
  return response()->json(array('errors' => ['attachment' => ' There was an error in uploading your attachment. Kindly check and retry'],'id'=>$request->employer_id));
}

}
}


public function checkExcelError($data,$employer_id,$type)
{

  $result = [];
  if($data){

    foreach ($data as $key => $value) {
      if (!empty($this->removeAllspaces($value['firstname']))) { 
       $grosspay_exception = $this->returnGrosspayExceptions($value);
       $basicpay_exception = $this->returnBasicpayExceptions($value);


       if ($grosspay_exception) {
        array_push($result, $grosspay_exception);
      }

      if ($basicpay_exception) {
        array_push($result, $basicpay_exception);
      }

      switch ($type) {
        case 'without_wcf_number':
        $dob_exception = $this->returnDobExceptions($value);
        $lastname_exception = $this->returnLastnameExceptions($value);
        if ($dob_exception) {
          array_push($result, $dob_exception);
        }
        if ($lastname_exception) {
          array_push($result, $lastname_exception);
        }
        break;
        case 'with_wcf_number':
        $wcf_number_exception = $this->returnWcfNumberExceptions($value);
        if ($wcf_number_exception) {
          array_push($result, $wcf_number_exception);
        }
        break;
        default:
        break;
      } 
    }

  }

  if (count($result)>0) {
    return response()->json(array('errors' => ['exceptions_found' => $result],'id'=>$employer_id));
  } else {
   return response()->json(array('success' => true));
 }

}else{
  return response()->json(array('success' => true));
}

}


public function returnBasicPayExceptions($value)
{
  $basicpay = str_replace( ',', '', $this->removeAllspaces($value['basicpay']));
  $basicpay = $this->removeAllspaces($basicpay);
  if (empty($basicpay)) {
   return [
     'name' => $value['firstname'].' '.$value['lastname'],
     'error' => 'Empty Basicpay'
   ];
 } else {
  if (!is_numeric($basicpay) ) {
   return [
     'name' => $value['firstname'].' '.$value['lastname'],
     'error' => 'Invalid Basicpay'
   ];
 }
}

}

public function returnGrosspayExceptions($value)
{

  $grosspay = str_replace( ',', '', $this->removeAllspaces($value['grosspay']));
  $grosspay = $this->removeAllspaces($grosspay);
  if (empty($grosspay)) {
   return [
     'name' => $value['firstname'].' '.$value['lastname'],
     'error' => 'Empty grosspay'
   ];
 } else {
  if (!is_numeric($grosspay) ) {
   return [
     'name' => $value['firstname'].' '.$value['lastname'],
     'error' => 'Invalid grosspay'
   ];
 }
}


}


public function returnDobExceptions($value)
{
  if (!empty($this->removeAllspaces($value['dob']))) {

   $new_date = $this->formatDateExcel($value['dob']);
   $preg_replace = preg_replace('/[^A-Za-z0-9\-]/', '/',$new_date);
   $validate = date("Y-m-d", strtotime($preg_replace));
   
   $is_valid_date = $this->isValidDate($validate,'Y-m-d');
   if (!$is_valid_date) {
    return [
      'name' => $value['firstname'].' '.$value['lastname'],
      'error' => 'Invalid Date of birth (dob)'
    ];
  }
}else{
  return [
    'name' => $value['firstname'].' '.$value['lastname'],
    'error' => 'Empty Date of birth (dob)'
  ];
}


}

public function returnLastnameExceptions($value)
{
  $lastname = $this->removeAllspaces($value['lastname']);
  if (empty($lastname)) {
   return [
     'name' => $value['firstname'].' '.$value['middlename'],
     'error' => 'Empty Lastname'
   ];
 }
}


public function returnWcfNumberExceptions($value)
{
  $wcf_number = $this->removeAllspaces($value['wcf_number']);
  if (empty($wcf_number)) {
   return [
     'name' => $value['firstname'].' '.$value['middlename'],
     'error' => 'Empty or Invalid WCF Number'
   ];
 }
}


public function isValidDate($str_dt, $str_dateformat, $str_timezone = 'Africa/Dar_es_Salaam') {

  $date = DateTime::createFromFormat($str_dateformat, $str_dt, new DateTimeZone($str_timezone));
  return $date && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0;
}


public function removeAllspaces($value_to_remove)
{
  if ($value_to_remove) {
   $value_to_remove = preg_replace('/\s+/', '', $value_to_remove);
 }

 return $value_to_remove;
}

public function requredField($value)
{
  return  $value.' is required';
}



public function resetStatusArrear($arrear)
{
 DB::table('portal.contribution_arrears')
 ->where('id', $arrear->id)
 ->update([
  'status' => 0,
  'bill_id' => null,
  'mac_updated_by' => access()->user()->id,
  'old_bill_id' => $arrear->bill_id,
  'updated_at' => Carbon::now(),
  'mac_updated_at' => Carbon::now(),
]);
}


public function isMonthInPendingBill($employer_id, $contrib_month, $payroll_id){ 

 $bill_pending = DB::table('portal.bills')
 ->where('employer_id',$employer_id)
 ->where('payroll_id', $payroll_id)
 ->where('contrib_month',$contrib_month)
 ->where('contribution','!=',null)
 ->where('bill_status', config('constants.bill.pending'))
 ->first();

 if($bill_pending){
  return true;
}else{
  return false;
}
}



public function saveArrearCommencement(array $input){

  $employer = Employer::find($input['reg_no']);
  $doc = $this->returnEmployerPayrollCommencement($employer->id, $input['payroll_id']);
  $doc = Carbon::parse(Carbon::parse($doc)->format('Y-m-28'));
  
  $tdy = Carbon::parse(Carbon::parse(Carbon::now())->format('Y-m-28'));
  $payroll_id = $input['payroll_id'];
  $diff_now_doc = $tdy->diffInMonths($doc);
  $payroll_id = $input['payroll_id'];
  if($diff_now_doc > 1){ 
    $receipt_exist = $this->isContributionMonthInReceipt($employer->id, Carbon::parse($doc)->format('Y-m-28'),$input['payroll_id']);
    $legacy_exist = $this->isContributionMonthInLegacy($employer->id, Carbon::parse($doc)->format('Y-m-28'));
    $legacy_code_exist = $this->isContributionMonthInLegacyCodes($employer->id, Carbon::parse($doc)->format('Y-m-28'));

    if(!$receipt_exist &&  !$legacy_exist && !$legacy_code_exist){
      $arrear_month = $doc->format('Y-m-28');
      $arrear_exist = $this->updateContributionMonthInArrear($employer->id, $arrear_month, $payroll_id);
      if (!$arrear_exist) {
       $this->insertArrear($employer->id, $arrear_month, $payroll_id,'Commencement date');
     }
   }
   $arrears =  $diff_now_doc - 2 ;
   for ($i=0; $i <$arrears ; $i++) { 
    $doc = $doc->addMonth();
    $receipt_exist = $this->isContributionMonthInReceipt($employer->id, Carbon::parse($doc)->format('Y-m-28'),$input['payroll_id']);
    $legacy_exist = $this->isContributionMonthInLegacy($employer->id, Carbon::parse($doc)->format('Y-m-28'));
    $legacy_code_exist = $this->isContributionMonthInLegacyCodes($employer->id, Carbon::parse($doc)->format('Y-m-28'));

    if(!$receipt_exist &&  !$legacy_exist && !$legacy_code_exist){ 
      $arrear_month = $doc->format('Y-m-28');
      $arrear_exist = $this->updateContributionMonthInArrear($employer->id, $arrear_month, $payroll_id);
      if (!$arrear_exist) {
       $this->insertArrear($employer->id, $arrear_month, $payroll_id,'Commencement date');
     }
   }
 }
}

}

public function updateContributionMonthInArrear($employer_id, $contrib_month, $payroll_id)
{

 $arrear_exist = DB::table('portal.contribution_arrears')->where('employer_id', $employer_id)
 ->where('contrib_month', $contrib_month)->where('payroll_id', $payroll_id)->first();
 $employer_user = DB::table('portal.employer_user')->where('employer_id',$employer_id)
 ->where('payroll_id',$payroll_id)->where('ismanage',true)->first();
 $employer = Employer::find($employer_id);

 if (count($arrear_exist) && $this->canAddMonthOnCommencement($employer_user, $employer, $contrib_month)) {
  if (!empty($arrear_exist->bill_id)) {
   $arrear_pending = $this->isMonthInPendingBill($employer_id, $contrib_month, $payroll_id);
   if (!$arrear_pending) {
    $this->resetStatusArrear($arrear_exist);  
  } 
} else {
 $this->resetStatusArrear($arrear_exist);
}
return true;
} else {
  return false;
}

}


public function insertArrear($employer_id, $month, $payroll_id, $reason)
{

  $employer_user = DB::table('portal.employer_user')->where('employer_id',$employer_id)
  ->where('payroll_id',$payroll_id)->where('ismanage',true)->first();
  $employer = Employer::find($employer_id);

  if ($this->canAddMonthOnCommencement($employer_user, $employer, $month)) {
    DB::table('portal.contribution_arrears')->insert([
      'employer_id' => $employer_id,
      'payroll_id' => $payroll_id,
      'status' => 0,
      'contrib_month' => Carbon::parse($month)->format('Y-m-28'),
      'mac_added_by' => access()->user()->id,
      'mac_added_at' =>Carbon::now(),
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
      'mac_add_reason' => $reason,
    ]);
  }
}


public function approvedUserPayrollId($employer_id)
{
  $approved_payroll = [];
  // if ($employer->source == 2) {
  //   //register user payroll normally ni 1
  //   $register_payroll = DB::table('portal.employer_user')
  //   ->select(['employer_user.user_id','employer_user.payroll_id'])
  //   ->leftjoin('main.employers', 'employer_user.employer_id', '=', 'employers.id')
  //   ->leftjoin('portal.users', 'employer_user.user_id', '=', 'users.id')
  //   ->where('employer_user.employer_id',$employer_id)
  //   ->where('employer_user.isown',1)
  //   ->where('employer_user.payroll_id',1)
  //   ->where('employer_user.stage',2)->pluck('payroll_id')->all();
  //   $approved_payroll = array_merge($register_payroll);
  // }
// dd($employer_id);
  //verified approved payrolls
  $verified_payroll = DB::table('main.online_employer_verifications')
  ->select(['online_employer_verifications.user_id','online_employer_verifications.payroll_id'])
  ->where('employer_id',$employer_id)
  ->where('status',1)->pluck('payroll_id')->all();
  // ->where('ismanage',true)

  $added_user =DB::table('portal.employer_user')
  ->select(['payroll_id'])
  ->where('employer_id',$employer_id)
  ->whereNotIn('payroll_id',$verified_payroll)->pluck('payroll_id')->all();

  foreach ($verified_payroll as $key => $value) {
    array_push($approved_payroll, $value);
  }

  foreach ($added_user as $key => $payroll) {
    array_push($approved_payroll, $payroll);
  }

  return array_unique($approved_payroll);

}

public function canAddMonthOnCommencement($employer_user, $employer, $contrib_month)
{
  // function inaangalia commencement na mwezi wa arrear unaotakiwa kuwa added
  $online_verification = DB::table('main.online_employer_verifications')->where('employer_id',$employer->id)->where('payroll_id',$employer_user->payroll_id)->first();
  $return = false;
  if (count($employer_user)) {
    $employer_doc = $this->returnEmployerPayrollCommencement($employer->id, $employer_user->payroll_id);
    $return = (Carbon::parse(Carbon::parse($employer_doc)->format('Y-m-d')) < Carbon::parse(Carbon::parse($contrib_month)->format('Y-m-d'))) ? true : false;
  }
  return $return;
}


public function didEmployerCloseThisMonth($employer_user, $contrib_month)
{
    // function inaangalia kama employer alifunga biashara kwenye mwezi wa arrear unaotakiwa kuwa added
  $return = false;
  if ($employer_user->has_ever_deregistered) {
      //check kama huo mwezi

  } 
  return $return;
}

public function returnEmployerPayrollCommencement($employer_id, $payroll_id){
  //based on WCF start date i.e July 2015
  $employer_doc = $this->payrollDoc($employer_id, $payroll_id);
  return (Carbon::parse(Carbon::parse($employer_doc)->format('Y-m-28'))  < Carbon::parse('2015-07-28')) ? Carbon::parse('2015-07-28')->format('Y-m-28') : Carbon::parse($employer_doc)->format('Y-m-d');

}

public function returnActivePayrolls($employer_id)
{
  //active approved payroll ids
  $active_payrolls = DB::table('portal.employer_user')
  ->where('employer_id',$employer_id)->where('ismanage',true)
  ->whereIn('payroll_id',$this->approvedUserPayrollId($employer_id))->pluck('payroll_id')->all(); 
  return $active_payrolls;
}

public function payrollDoc($employer_id, $payroll_id)
{
  //payroll commencement date (doc)
  $employer = Employer::select('doc')->where('id',$employer_id)->first();
  $employer_user = DB::table('portal.employer_user')->select('commencement_source','doc')
  ->where('employer_id',$employer_id)->where('payroll_id',$payroll_id)->first();
  $verification = DB::table('main.online_employer_verifications')->select('commencement_source','project_start_date','doc')
  ->where('employer_id',$employer_id)->where('payroll_id',$payroll_id)->first();

  if (!empty($employer_user->doc)) {$employer_doc = $employer_user->doc;} else {
    switch ((int)$employer_user->commencement_source) {
     case 2:
   //branch or payroll
     $employer_doc = !empty($verification->doc) ? Carbon::parse($verification->doc) : Carbon::parse(Carbon::parse($employer->doc)->format('Y-m-d'));
     break;
     case 3:
   //project
     $employer_doc = !empty($verification->project_start_date) ? Carbon::parse($verification->project_start_date) : Carbon::parse(Carbon::parse($employer->doc)->format('Y-m-d'));
     break;
     default:
   //employer original commencement
     $employer_doc = Carbon::parse(Carbon::parse($employer->doc));
     break;
   }

   DB::table('portal.employer_user')->where('employer_id',$employer_id)
   ->where('payroll_id',$payroll_id)->update([
    'doc' => Carbon::parse(Carbon::parse($employer_doc))->format('Y-m-d')
  ]);
 }
 return Carbon::parse(Carbon::parse($employer_doc))->format('Y-m-d');
}

public function isPayrollActive($employer_id, $payroll_id)
{
  $is_active = DB::table('portal.employer_user')->where('employer_id',$employer_id)
  ->where('payroll_id',$payroll_id)->where('ismanage',true)->first();
  return count($is_active) ? true : false;
}

public function getEmployerMainPayrollAttribute($employer_id)
{
  return DB::table('portal.employer_user')
  ->join('portal.users', 'employer_user.user_id', '=', 'users.id')
  ->where('employer_id',$employer_id)
  ->whereIn('payroll_id',$this->returnActivePayrolls($employer_id))
  ->where('ismanage',true)->orderBy('payroll_id','asc')->first();
}

public function updatePaidInstalmentArrear($arrear_id, $status){
   DB::table('portal.installment_payments')->where('contribution_arrears_id', $arrear_id)->update(['commitment_status'=> $status]);
}


public function getLeastDocForParticularChange($employer_id,$exclude_payroll)
{
  //return tarehe ndogo kabisa ya commencement ktk payroll au default employer doc
 $doc = [];
 $employer_doc = null;
 $payrolls = $this->approvedUserPayrollId($employer_id);
 if (($key = array_search($exclude_payroll, $payrolls)) !== false) {
  unset($payrolls[$key]);
}
foreach ($payrolls as $key => $value) {
 $doc[$value] = $this->payrollDoc($employer_id, $value);
}
if (empty($doc)) {
 $employer = Employer::find($employer_id);
 $employer_doc = Carbon::parse(Carbon::parse($employer->doc))->format('Y-m-d');
}
return !empty($doc) ? min($doc) : $employer_doc;
}


public function checkEmployerReceiptVsDoc($employer_id, $doc, $payroll_id = null)
{
  $doc = Carbon::parse($doc)->format('Y-m-d');
  if (!empty($payroll_id)) {
   $receipts = DB::table('main.receipt_codes')->where('employer_id',$employer_id)
   // ->where('payroll_id',$payroll_id)
   ->whereNull('deleted_at')
   ->where('contrib_month','<',$doc)->first();}
   else{
    $receipts = DB::table('main.receipt_codes')
    ->whereNull('deleted_at')
    ->where('employer_id',$employer_id)
    ->where('contrib_month','<',$doc)->first();}
    // dd($receipts);
    if (count($receipts)) {
     throw new WorkflowException('Approval Failed! Employer has Contribution before '.$doc); 
   }
 }



}
