<?php

namespace App\Services\Compliance\Traits\UpdateBookingsNotContributed;
/*
 *
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto update bookings which are not yet contributed
 */

use App\Models\Finance\Receipt\LegacyReceipt;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Finance\Receivable\Portal\ContributionArrear;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Repositories\Backend\Finance\FinYearRepository;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

trait PostEmployerContribRcvableScheduleTrait {



    /*
     * From the beginning
     */
    public function postEmployerSchedulesFromBeginning($limit_years = null)
    {
        if($limit_years == null){
            $fin_years = (new FinYearRepository())->query()->get();
        }else{
            $limit_year = Carbon::now()->subYears($limit_years);
            $limit_date = standard_date_format($limit_year);
            $fin_years = (new FinYearRepository())->query()->where('start_date','>=', standard_date_format($limit_date))->get();
        }

        DB::statement("truncate table  employer_contrib_schedules");

        foreach ($fin_years as $fin_year){
            $this->postAllSchedulesByFinYear($fin_year->id);
        }
    }

    /**
     * @param $fin_year_id
     * @param $scheduleid
     * Post employer schedule by fin year
     */
    public function postEmployerScheduleByFinYearAndSchedule($fin_year_id, $scheduleid)
    {
        $fin_year = (new FinYearRepository())->query()->where('id', $fin_year_id)->first();
        $start_date = $fin_year->start_date;
        $end_date = $fin_year->end_date;

        switch($scheduleid){
            case 2://schedule 2

                $sql = <<<SQL
        insert into employer_contrib_schedules (employer_id,scheduleid,fin_year_id) 
                select b.employer_id, {$scheduleid}, {$fin_year_id}
        FROM bookings_mview b
WHERE  b.ispaid = 1   and b.rcv_date >= '{$start_date}' and b.rcv_date <= '{$end_date}' and b.receipt_date <= '{$end_date}' group by b.employer_id
 
SQL;
                DB::unprepared($sql);

                break;

            case 6://Schedule 6

               $sql = <<<SQL
        insert into employer_contrib_schedules  (employer_id,scheduleid,fin_year_id)
                select e.id, {$scheduleid}, {$fin_year_id}
from employers e
join contributing_category_employer c on c.employer_id = e.id
left join bookings_mview b on b.employer_id = e.id   and b.rcv_date >= '{$start_date}' and b.rcv_date <= '{$end_date}'  and b.receipt_date::date <= '{$end_date}' 
where b.booking_id is null and e.approval_id = 1 and e.duplicate_id is null

SQL;
                DB::unprepared($sql);





                break;

        }

    }


    /**
     * @param $fin_year_id
     * Repost All schedules by fin year
     */
    public function postAllSchedulesByFinYear($fin_year_id)
    {
        $sql = <<<SQL
 delete from employer_contrib_schedules where fin_year_id = '{$fin_year_id}'

SQL;
        DB::unprepared($sql);


        /*Post*/
        $this->postEmployerScheduleByFinYearAndSchedule($fin_year_id, 2);
        $this->postEmployerScheduleByFinYearAndSchedule($fin_year_id, 6);
    }




}