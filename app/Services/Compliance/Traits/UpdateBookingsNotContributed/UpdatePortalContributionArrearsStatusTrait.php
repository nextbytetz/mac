<?php

namespace App\Services\Compliance\Traits\UpdateBookingsNotContributed;
/*
 *
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto update bookings which are not yet contributed
 */

use App\Models\Finance\Receipt\LegacyReceipt;
use App\Models\Finance\Receipt\ReceiptCode;
use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Finance\Receivable\Portal\ContributionArrear;
use App\Models\Operation\Compliance\Member\Employer;
use App\Models\Operation\Compliance\Member\EmployerClosure;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

trait UpdatePortalContributionArrearsStatusTrait {





    /**
     * Get employees with closures and contribution arrears on portal not paid (status = 0)
     */

    public function mainEmployersForUpdateStatusForContribArrearsWithBusinessClosure()
    {
        $employer_repo = new EmployerRepository();
        $start_date_limit_check = '2021-03-10';
        $closure_date_cuttoff = '2015-07-01';
        $closure_date_cuttoff = (comparable_date_format(getTodayDate()) <= comparable_date_format($start_date_limit_check)) ? standard_date_format($closure_date_cuttoff) : standard_date_format(Carbon::now()->subYears(1));
        $employers = $employer_repo->query()->whereHas('closures', function ($q) use($closure_date_cuttoff){
            $q->where('employer_closures.isbookingskip', 1)->whereDate('created_at', '>=', $closure_date_cuttoff);
        })->whereHas('portalContribArrears', function($q){
            $q->where('portal.contribution_arrears.status', 0);
        })->select('employers.id as id')->get();
        foreach ($employers as $employer){
            $first_closure = (new EmployerClosureRepository())->getFirstClosureByEmployer($employer->id);
            $first_close_date = ($first_closure->close_date_internal) ? standard_date_format($first_closure->close_date_internal) : $first_closure->close_date;
            $this->updateStatusForContribArrearsWithBusinessClosure($employer->id,$first_close_date);


        }
    }


    /*Get contrib arrears to update by employer*/
    public function updateStatusForContribArrearsWithBusinessClosure($employer_id, $start_contrib_date = null){
        $booking_repo = new BookingRepository();
        $employer_repo = new EmployerRepository();
        $employer = Employer::query()->find($employer_id);
        $inactive_status = 1;//TODO need to confirm
        /*update status only for employers with single payroll*/
        $check_if_has_multiple_payrolls = $employer_repo->checkIfEmployerHasMultiplePayrolls($employer->id);

        if($check_if_has_multiple_payrolls == false){
            /*loop on contrib arrears*/
//            $contrib_arrears = ContributionArrear::query()->doesntHave('mainBooking')->where('status', 0)->where('employer_id', $employer_id)->get();
            if($start_contrib_date){
                $contrib_arrears = ContributionArrear::query()->where('contrib_month', '>=', standard_date_format($start_contrib_date))->where('status', 0)->where('employer_id', $employer_id)->get();
            }else{
                $contrib_arrears = ContributionArrear::query()->where('status', 0)->where('employer_id', $employer_id)->get();
            }
            foreach ($contrib_arrears as $contrib_arrear){
                $contrib_month = $contrib_arrear->contrib_month;
                $check_if_on_closure = $booking_repo->checkIfMonthWasInEmployerClosure($employer, $contrib_month);
                if($check_if_on_closure){
                    $contrib_arrear->update([
                        'status' => $inactive_status,
                        'mac_clear_reason' => 'Business De-registration'
                    ]);
                }
            }
        }
    }


    /*Get contrib arrears to update by employer*/
    public function updateStatusForContribArrearsWithBusinessClosureWithPayroll($employer_id, $start_contrib_date = null, array $payroll_ids = [], $contrib_month_after_close_date = null){
        $booking_repo = new BookingRepository();
        $employer_repo = new EmployerRepository();
        $employer = Employer::query()->find($employer_id);
        $inactive_status = 1;
        /*update status only for employers with single payroll*/
        $check_if_has_multiple_payrolls = $employer_repo->checkIfEmployerHasMultiplePayrolls($employer->id);
        if($check_if_has_multiple_payrolls == false) {

            $payroll_ids =(count($payroll_ids) == 0) ? $employer_repo->getEmployerPayrolls($employer_id)->pluck('payroll_id') : $payroll_ids;
        }
        /*limit date to remove from arrears*/
        $limit_date = $contrib_month_after_close_date ?? standard_date_format(getTodayDate());
        /*loop on contrib arrears*/
        if(count($payroll_ids)){

            if($start_contrib_date){
                $contrib_arrears = ContributionArrear::query()->whereIn('payroll_id', $payroll_ids)->where('contrib_month', '>=', standard_date_format($start_contrib_date))->where('contrib_month', '<=', standard_date_format($limit_date))->where('status', 0)->where('employer_id', $employer_id)->get();
            }else{
                $contrib_arrears = ContributionArrear::query()->whereIn('payroll_id', $payroll_ids)->where('status', 0)->where('contrib_month', '<=', standard_date_format($limit_date))->where('employer_id', $employer_id)->get();
            }
            foreach ($contrib_arrears as $contrib_arrear){

                $contrib_month = $contrib_arrear->contrib_month;
                $check_if_on_closure = $booking_repo->checkIfMonthWasInEmployerClosure($employer, $contrib_month);
                if($check_if_on_closure){
                    $contrib_arrear->update([
                        'status' => $inactive_status,
                        'mac_clear_reason' => 'Business De-registration'
                    ]);
                }
            }
        }

    }

    /**
     * Update Portal Arrears for employer with Single Payroll and already reopened
     */
    public function updatePortalArrearsForEmployerWithSinglePayrollAndReopenedSpecifiedEmployers()
    {
        $employers_ids = $this->getOldClosureReopenEmployersToRemovePortalArrearsRectification();
        $employer_repo = (new EmployerClosureRepository());
        foreach ($employers_ids as $employer_open) {
            $closures = $employer_repo->query()->where('employer_id', $employer_open->employer_id)->where('wf_done', 1)->whereNotNull('open_date')->get();
            foreach ($closures as $closure){
                $open_date = $closure->open_date_internal ?? $closure->open_date;
                $this->updateStatusForContribArrearsWithBusinessClosureWithPayroll($closure->employer_id,$closure->close_date_internal,[],$open_date);
            }
        }
    }


    /**
     * Get old closure reopen employers to remove portal arrears
     */
    public function getOldClosureReopenEmployersToRemovePortalArrearsRectification()
    {
        $employers = (new EmployerClosureReopenRepository())->query()->select('c.employer_id'
        )
            ->join('employer_closures as c', 'c.id', 'employer_closure_reopens.employer_closure_id')
            ->join('portal.bills as b', 'b.employer_id', 'c.employer_id')
            ->where('employer_closure_reopens.wf_done', 1)->where('b.bill_status', 3)->whereDate('b.created_at', '<=',standard_date_format(getTodayDate()))->groupBy('c.employer_id');
        return $employers->get();
    }

}