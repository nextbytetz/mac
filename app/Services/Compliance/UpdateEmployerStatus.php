<?php

namespace App\Services\Compliance;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto categorize employers to contribution categories.
 */

use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UpdateEmployerStatus {
    /**
     * Services for auto categorize employers to contribution categories.
     *
     * @param string $model
     *
     * @return void
     */

//    protected $dormant_date;
    protected $check_input;


    public function __construct()
    {

    }


    /*Get number of months to update status to dormant if employer not contributed*/
    public function getNoOfMonthsForDormantStatus()
    {
        return 12;
    }

    /*Get month for checking limit to update status*/
    public function getDateThreshold()
    {
        $now = Carbon::now();
        $months = $this->getNoOfMonthsForDormantStatus();
        $date = $now->subMonths($months);
        return $date->startOfMonth();
    }

    /*check if employer is contributor*/
    public function checkEmployerHasReceipt(Model $employer){
        $check_has_receipt =$employer->receipts()->where('iscancelled', 0)->where('isdishonoured', '=', 0)->count();
        if($check_has_receipt > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $employer_id
     * @return string
     * @throws \App\Exceptions\GeneralException
     * Get dormant date - date when employer became dormant
     */
    public function getDormantDate($employer_id)
    {
        $receipt_repo = new ReceiptRepository();
        $check_input = $this->check_input;
        $check_on_contrib_last_12 = $check_input['check_on_contrib_last_12'];
        $check_on_has_receipt = $check_input['check_on_has_receipt'];
        $check_on_date_registered =  $check_input['check_on_date_registered'];
        $limit_date = $check_input['limit_date'];
        $dormant_date = null;
        if($check_on_contrib_last_12 == 1){
            $contrib = (new ReceiptCodeRepository())->query()->whereDate('contrib_month', '<', $limit_date)->whereHas('receipt', function($query){
                $query->where('iscancelled', 0)->where('isdishonoured', '=', 0);
            })->first();

            $dormant_date = $contrib->contrib_month;
        }elseif($check_on_has_receipt == 1){
            $receipt = $receipt_repo->query()->where('employer_id', $employer_id)->where('iscancelled', 0)->where('isdishonoured', '=', 0)->whereDate('rct_date', '<', $limit_date)->first();
            $dormant_date = $receipt->rct_date;
        }elseif($check_on_date_registered == 1){
            $employer = (new EmployerRepository())->find($employer_id);
            $dormant_date = isset($employer->wf_done_date) ? $employer->wf_done_date : $employer->created_at;
        }
        $no_months_dormant = $this->getNoOfMonthsForDormantStatus();
        $dormant_date = Carbon::parse($dormant_date)->addMonths($no_months_dormant);

        return standard_date_format($dormant_date);
    }


    /*Check if employer has contributed later than last date threshold*/
    public function getEmployerStatus($employer_id)
    {
        $employer = $this->getEmployer($employer_id);
        $limit_date = standard_date_format($this->getDateThreshold());
        $status = 0;
        $receipt_repo = new ReceiptRepository();
        $date_registered = $employer->created_at;
        $check_if_is_contributor = $this->checkEmployerHasReceipt($employer);
        $check_on_contrib_last_12 = 0; $check_on_has_receipt = 0; $check_on_date_registered = 0;
        $check_contribution = 0;
        if($check_if_is_contributor == true){
            /*Check if has contributed atleast one month last 12 months*/
            $check_contribution = $receipt_repo->query()->where('employer_id', $employer_id)->where('iscancelled', 0)->where('isdishonoured', '=', 0)->whereHas('receiptCodes', function($query) use($limit_date){
                $query->whereDate('contrib_month', '>', $limit_date);
            })->count();
            $check_on_contrib_last_12 = ($check_contribution == 0) ? 1 : 0;

            /*If has not paid for any contrib month - check is has receipt within the period*/
            if ($check_contribution == 0){
                $check_contribution = $receipt_repo->query()->where('employer_id', $employer_id)->where('iscancelled', 0)->where('isdishonoured', '=', 0)->whereDate('rct_date', '>', $limit_date)->count();
                $check_on_has_receipt = ($check_contribution == 0) ? 1 : 0;
            }

        }else{
            /*non contributor - check from the date registered*/
            $check_contribution = DB::table('employers')->where('id', $employer_id)->whereRaw("coalesce(wf_done_date::date,created_at::date) > ?", [$limit_date])->count();
            $check_on_date_registered = ($check_contribution == 0) ? 1 : 0;

        }

        $this->check_input = ['check_on_contrib_last_12' => $check_on_contrib_last_12, 'check_on_has_receipt' => $check_on_has_receipt, 'check_on_date_registered' => $check_on_date_registered, 'limit_date' => $limit_date ];


        if($check_contribution > 0){
            /*Active - has contribution/ or registered within time and do not have receipt*/
            $status = 1;
        }elseif ($check_contribution == 0){
            /*Dormant - if has no contribution and registered before limit date*/
            $status = 2;
        }else{
            /*keep the same status*/
            $status = $employer->employer_status;
        }
        return $status;
    }

    /*Employer object*/
    public function getEmployer($employer_id){
        $employer = (new EmployerRepository())->find($employer_id);
        return $employer;
    }



    /*Update employer status*/
    public function updateEmployerStatus($employer_id){
        $employer = $this->getEmployer($employer_id);
        if($employer->is_treasury == false){
            /*non treasury - employers*/
            $status = $this->getEmployerStatus($employer_id);
            $last_dormant_date = isset($employer->dormant_date) ? $employer->dormant_date : null;
//            $new_dormant_date = ($status == 2) ? $this->getDormantDate($employer_id) : (isset($last_dormant_date) ? $last_dormant_date : null);
            $employer->update([
                'employer_status' => $status,
                'dormant_date' => ($status == 2) ? $this->getDormantDate($employer_id) : $last_dormant_date,
            ]);
        }elseif($employer->is_treasury == true){
            /*treasury employers - set active*/
            $employer->update([
                'employer_status' => 1,
                'dormant_date' => null
            ]);
        }

    }

}