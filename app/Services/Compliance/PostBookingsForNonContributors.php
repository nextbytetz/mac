<?php

namespace App\Services\Compliance;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto post bookings for non booked contribution of all employers
 */

use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\BookingInterest;
use App\Models\Finance\Receivable\NonBookedBooking;
use App\Models\Finance\Receivable\NonBookedBookingInterest;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Receivable\CalculateInterest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PostBookingsForNonContributors {

    protected $estimate_base = null;
    protected $closure = 0;
    protected $monthly_earning = 0;

    public function __construct()
    {

    }


    public function minimumBookingAmount()
    {
        return 800;
    }

    /*Get start date for booking this employer*/
    public function getStartBookingDate(Model $employer)
    {
        $system_start_date = '2017-07-01';
        $doc = isset($employer->doc) ? $employer->doc : $system_start_date;
        if(comparable_date_format($doc) < comparable_date_format($system_start_date)){
            return  standard_date_format($system_start_date);
        }else{
            return standard_date_format($doc);
        }
    }


    //    get booking_date
    public function getBookingDate($date) {
        $booking_date  = '28-'.  Carbon::parse($date)->format('M') . '-' . Carbon::parse($date)->format('Y');
        $format = 'd-M-Y';
        return  Carbon::createFromFormat($format, $booking_date);
    }



    /*Main function : post booking*/
    public function postBookings($employer_id)
    {
        $employer = (new EmployerRepository())->find($employer_id);
        /*delete all bookings already live booked and exist in non booked bookings*/
//        $this->deleteAllBookingsAlreadyLiveBooked($employer);
        $this->monthly_earning = $this->getBookingAmountNonContributor($employer);


        /*Post bookings for all months*/
        $this->postBookingsForAllMonths($employer);
    }


    /*Post bookings for employers for all eligible months*/
    public function postBookingsForAllMonths(Model $employer)
    {
        /*Restart when is first time / doc changed / annual earning changed*/
        if($this->checkIfNeedToRestartBooking($employer)){
            $this->deleteNonBookedBookingBeforeDoc($employer);
            $this->destroyDeletedBookings();
            /*Delete all before doc bookings*/
            $start_date = $this->getStartBookingDate($employer);
            $start_date_01  = Carbon::parse($start_date)->startOfMonth();
            $this->loopAllMonthsPostBooking($employer, $start_date_01);

        }elseif($this->checkIfIsNewMonthForBooking($employer->id)){
            /*<new> month book new month*/
            $start_date_01  = Carbon::now()->startOfMonth();
            $this->loopAllMonthsPostBooking($employer, $start_date_01);

            /*Update interest for all bookings - new month cummulative*/
            $this->postNonBookedInterestForAllBookings($employer);
        }
    }



    /*Loop all months based on specified starting date*/
    public function loopAllMonthsPostBooking(Model $employer, $start_date_01){
        $today = Carbon::now();
        while(comparable_date_format($start_date_01) <= comparable_date_format($today)) {
            $booking_date = $this->getBookingDate($start_date_01);

            /*Get booking amount*/
            $amount = $this->getBookingAmount($employer, $booking_date);
            /*Save*/
            if($this->closure == 0){
                /*if booking date was not within closure period*/
                $this->saveNonBookedBooking($employer, $amount, $booking_date);
//                    }
            }
            /*increment*/
            $start_date_01 = \Carbon\Carbon::parse($start_date_01)->addMonth(1);
        }
    }



    /*Check is new month to process new booking*/
    public function checkIfIsNewMonthForBooking($employer_id)
    {
        $booking = NonBookedBooking::query()->where('employer_id', $employer_id)->orderBy('rcv_date', 'desc')->first();
        $last_booked_month = $booking->rcv_date;
        if(isset($booking)){
            if(comparable_date_format($last_booked_month) < comparable_date_format(Carbon::now())){
                /*proceed to update booking since there is change on annual_earning*/
                return true;
            }else{
                /*no need to proceed booking - No change*/
                return false;
            }
        }else{
            return false;
        }
    }




    /**
     * @param Model $date_array_y_m_d
     * @param $booking_date
     * Check if applicable month was in employer closure
     */
    public function checkIfMonthWasInEmployerClosure(Model $employer, $booking_date){

        $check_closure = (new BookingRepository())->checkIfMonthWasInEmployerClosure($employer, $booking_date);
        return $check_closure;
    }


    /**
     * @param Model $employer
     * @param $booking_date
     * Check if booking already exist on bookings
     */
    public function checkIfAlreadyBooked(Model $employer, $booking_date)
    {
        $bookings = new BookingRepository();
        $check = $bookings->query()->where('employer_id', $employer->id)->whereDate('rcv_date', $booking_date)->count();

        if($check > 0){
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param Model $employer
     * Check if booking need to be redone based on change on monthly earning / doc
     */
    public function checkIfNeedToRestartBooking(Model $employer)
    {
        $booking = NonBookedBooking::query()->where('employer_id', $employer->id)->orderBy('rcv_date', 'desc')->first();
        if(isset($booking)){
            if($booking->annual_earning != $employer->annual_earning || $booking->doc != $employer->doc || $employer->closures()->count() > 0 || $this->checkIfNoOfBookingsTallyNoOfMonthsRequired($employer) == false){
                /*proceed to restart booking since there is change on annual_earning*/
                return true;
            }else{
                /*no need to restart booking - No change*/

                return false;
            }
        }else{
            /*No booking - restart for first time*/
            return true;
        }
    }


    public function checkIfNoOfBookingsTallyNoOfMonthsRequired(Model $employer)
    {
        /*check if no of bookings of employer tally no of months required*/
        $no_of_bookings = NonBookedBooking::query('employer_id', $employer->id)->count();
        $no_of_months_required = months_diff($employer->doc, Carbon::now()) + 1;

        if($no_of_bookings != $no_of_months_required){
            /*Tally*/
            return true;
        }else{
            /*does not tally*/
            return false;
        }
    }


//    /*Delete if already booked on the live*/
//    public function deleteIfAlreadyBooked(Model $employer, $booking_date){
//
////        $check = $this->checkIfAlreadyBooked($employer, $booking_date);
////        if($check){
//
//        NonBookedBooking::query()->where('employer_id', $employer->id)->whereDate('rcv_date', $booking_date)->delete();
////        $this->deleteAllInterestWithNoBookingPerEmployer($employer->id);
////        }
//    }


    /*Get Booking amount for specified booking date*/
    public function getBookingAmount(Model $employer, $booking_date)
    {

        /*check if business was not closed*/
        if(!$this->checkIfMonthWasInEmployerClosure($employer,$booking_date)){
            $this->closure = 0;
            return $this->monthly_earning;

        }else{
            $this->closure = 1;
            return 0;
        }
    }


    /*Get booking amount for non contributor*/
    public function getBookingAmountNonContributor(Model $employer)
    {
        $annual_earning = $employer->annual_earning;
        $monthly_earning = $annual_earning / 12;
        $contrib_percent = $employer->employer_category_cv_id == 37 ? private_contrib_percent() : public_contrib_percent();
        $contrib_percentage = $contrib_percent * 0.01;
        $amount = $monthly_earning * $contrib_percentage;
        $this->estimate_base = 'Monthly Earning';
        $minimum_amount = $this->minimumBookingAmount();
        $amount = ($amount > $minimum_amount) ? $amount : $minimum_amount;
        return $amount;
    }

    /**
     * @param Model $employer
     * @param $booking_date
     * @return int
     * Get most recent booking before specified booking date
     */
    public function getMostRecentBookingAmountBefore(Model $employer, $booking_date)
    {
        $bookings = new BookingRepository();
        $booking_before = $bookings->query()->where('employer_id', $employer->id)->whereDate('rcv_date','<', $booking_date)->orderBy('rcv_date', 'desc')->first();
        $booking_after = $bookings->query()->where('employer_id', $employer->id)->whereDate('rcv_date','>', $booking_date)->orderBy('rcv_date', 'asc')->first();
        if(isset($booking_before)){
            $this->estimate_base = 'Recent Booking';
            return $booking_before->amount;
        }elseif(isset($booking_after)){
            $this->estimate_base = 'Recent Booking';
            return $booking_after->amount;
        }else{
            return 0;
        }
    }

    /**
     * @param Model $employer
     * @param $booking_date
     */
    public function getNonBookedBooking(Model $employer, $booking_date){

    }

    /*Save non booked booking*/
    public function saveNonBookedBooking(Model $employer, $amount, $booking_date)
    {

        if($this->checkIfExistInNonBookedBooking($employer, $booking_date)){

            /*exist - update*/
            $query =     NonBookedBooking::query()->where('employer_id', $employer->id)->whereDate('rcv_date', standard_date_format($booking_date));
            $non_booked_booking_id = $query->first()->id;
            $query->update([
                'amount' => $amount,
                'estimate_base' => $this->estimate_base,
                'annual_earning' => isset( $employer->annual_earning) ?  $employer->annual_earning : 0,
                'doc' => standard_date_format($employer->doc),
                'updated_at' => Carbon::now(),
            ]);
        }else{
                /*new - create*/
            $non_booked_booking_id = DB::table('non_booked_bookings')->insertGetId([
                'employer_id' => $employer->id,
                'amount' => $amount,
                'rcv_date' => standard_date_format($booking_date),
                'fin_code_id' => 2,
                'estimate_base' => $this->estimate_base,
                'annual_earning' =>isset( $employer->annual_earning) ?  $employer->annual_earning : 0,
                'doc' => standard_date_format($employer->doc),
                'created_at' => Carbon::now(),
            ]);

        }
        /*Post interest*/
        $this->postNonBookedInterest($non_booked_booking_id, $amount, $booking_date);

    }

    /**
     * @param Model $employer
     * @param $booking_date
     * @return bool
     * Check if exists in non booked bookings
     */
    public function checkIfExistInNonBookedBooking(Model $employer, $booking_date)
    {
        $check =    NonBookedBooking::query()->where('employer_id', $employer->id)->whereDate('rcv_date', $booking_date)->count();

        if($check > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param Model $employer
     * @param $booking_date
     * Delete non booked booking before doc incase doc is updated
     */
    public function deleteNonBookedBookingBeforeDoc(Model $employer){
        NonBookedBooking::query()->where('employer_id', $employer->id)->whereDate('rcv_date','<', standard_date_format($employer->doc))->delete();
        /*Delete interest*/
        $this->deleteAllInterestWithNoBookingPerEmployer($employer->id);
    }







    /*Get all months skipped for booking for this employer*/
    public function getMonthsAlreadyLiveBooked(Model $employer)
    {

        $live_booked_months =     Booking::query()->select('rcv_date')->where('employer_id', $employer->id)->pluck('rcv_date')->toArray();
        $non_booked_bookings_months =     NonBookedBooking::query()->select('rcv_date')->where('employer_id', $employer->id)->pluck('rcv_date')->toArray();
        $months_no_live_booked = array_diff($live_booked_months, $non_booked_bookings_months);

        return $months_no_live_booked;
    }



//    /*Delete all bookings already live booked*/
//    public function deleteAllBookingsAlreadyLiveBooked(Model $employer)
//    {
//        $months_no_live_booked = $this->getMonthsAlreadyLiveBooked($employer);
//        NonBookedBooking::query()->whereIn('rcv_date', $months_no_live_booked)->where('employer_id', $employer->id)->delete();
//        $this->deleteAllInterestWithNoBookingPerEmployer($employer->id);
//    }




    /**
     * POST INTEREST - NON BOOKED
     */
    public function postNonBookedInterest($non_booked_booking_id, $booked_amount, $booking_date){
        $calculate_interest = new CalculateInterest();
        $today = Carbon::now();
        $late_months = $calculate_interest->getLateMonths($booking_date, $today);
        $interest_percent = sysdefs()->data()->contribution_penalty_percent;
        $interest_raised = $calculate_interest->getInterest($interest_percent,$late_months,$booked_amount);
        /*save interest*/
        $this->saveNonBookedInterest($non_booked_booking_id, $interest_raised, $booking_date, $late_months);
    }



    /**
     * Post interest for all bookings- Recalculate all interest for the new month
     */
    public function postNonBookedInterestForAllBookings(Model $employer){
        $today = Carbon::now();
        $start_date = $this->getStartBookingDate($employer);
        $start_date_01  = Carbon::parse($start_date)->startOfMonth();
        while(comparable_date_format($start_date_01) <= comparable_date_format($today)) {
            $booking_date = $this->getBookingDate($start_date_01);
            $booking = NonBookedBooking::query()->where('rcv_date', $booking_date)->where('employer_id', $employer->id)->first();
            $this->postNonBookedInterest($booking->id, $booking->amount,$booking->rcv_date);
            $start_date_01 = \Carbon\Carbon::parse($start_date_01)->addMonth(1);
        }
    }






    /**
     * @param $non_booked_booking_id
     * @param $interest_raised
     * @param $booking_date
     */
    public function saveNonBookedInterest($non_booked_booking_id, $interest_raised, $booking_date,$late_months)
    {
        if($this->checkIfInterestDoesNotExist($non_booked_booking_id)){
            /*not exist - create new*/
            DB::table('non_booked_booking_interests')->insert([
                'rcv_date' => standard_date_format($booking_date),
                'non_booked_booking_id' => $non_booked_booking_id,
                'interest_percent' => sysdefs()->data()->contribution_penalty_percent,
                'amount' => $interest_raised,
                'late_months' => $late_months,
                'created_at' => Carbon::now(),
            ]);
        }else{
            /*exist -update*/
            DB::table('non_booked_booking_interests')->where('non_booked_booking_id', $non_booked_booking_id)->update([
                'amount' => $interest_raised,
                'late_months' => $late_months,
            ]);
        }

    }

    public function checkIfInterestDoesNotExist($non_booked_booking_id){
        $check =     NonBookedBookingInterest::query()->where('non_booked_booking_id', $non_booked_booking_id)->count();
        if($check > 0){
            /*exist*/
            return false;
        }else{
            return true;
        }
    }








//    /*Delete all before DOC incase has changed*/
    public function deleteAllInterestWithNoBooking()
    {

        NonBookedBookingInterest::query()->doesntHave('nonBookedBooking')->delete();
    }
//
    /*Delete allinterests for deleted bookings*/
    public function deleteAllInterestWithNoBookingPerEmployer($employer_id)
    {
        NonBookedBookingInterest::query()->whereDoesntHave('nonBookedBooking', function($query) use($employer_id){
            $query->where('employer_id', $employer_id);
        })->delete();
    }


    /**
     *
     * FUNCTION TO DELETE ALL BOOKINGS
     */
    public function deleteAllBookingsAlreadyLiveBooked()
    {
        /*delete non booked bookings*/
        NonBookedBooking::query()->whereHas('employer', function($query){
            $query->has('bookings');
        })->delete();

        /*Delete interest whose booking have been deleted*/
//        $this->deleteAllInterestWithNoBooking();
    }


    public function deleteBookingsBelongToDuplicateEmployers()
    {
        NonBookedBooking::query()->whereHas('employer', function($query){
            $query->whereNotNull('duplicate_id');
        })->delete();
    }



    /**
     * UPDATE ALL FUNCTIONS
     */
    /*<temp> function to update all bookings less than minimum*/
    public function updateBookingLessThanMinimum()
    {
        NonBookedBooking::query()->where('amount', '<', $this->minimumBookingAmount())->update([
            'amount' =>$this->minimumBookingAmount()
        ]);
    }

//    /*Delete live bookings for employers with no receipts*/
//    public function deleteLiveBookingsForEmployersWithNoReceipts()
//    {
//        Booking::query()->whereDoesntHave('receiptCodes', function($query){
//            $query->whereHas('receipt', function($query){
//                $query->where('iscancelled',0);
//            });
//        })->whereHas('employer', function ($query){
//            $query->doesntHave('legacyReceipts');
//        })->delete();
//
//
//        /*Delete live booking interest with no booking*/
////        $this->deleteLiveBookingInterestsWithNoBooking();
//
//    }





    public function deleteNonBookedBookingsOfTreasuryEmployers()
    {
        NonBookedBooking::query()->whereHas('employer', function($query){
            $query->where('is_treasury', true);
        })->delete();
    }




    /*Reset deleted bookings not in live bookings*/
    public function resetDeletedBookingNotInLiveBookings()
    {
        DB::table('non_booked_bookings')->whereRaw("deleted_at is null")->update([
            'deleted_at' => NULL,
        ]);
    }

    /*Destroy deleted bookings*/
    public function destroyDeletedBookings(){
        DB::table('non_booked_booking_interests')->whereRaw("deleted_at is not null")->delete();
        DB::table('non_booked_bookings')->whereRaw("deleted_at is not null")->delete();
    }


//    /*Destroy deleted bookings - LIVE BOOKINGS*/
//    public function destroyDeletedLiveBookings(){
//        DB::table('booking_interests')->whereRaw("deleted_at is not null")->delete();
//        DB::table('bookings')->whereRaw("deleted_at is not null")->delete();
//    }



}