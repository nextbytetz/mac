<?php

namespace App\Services\Receivable;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto calculate interest for contribution contributed late (exceed grace period)
 */
use App\Models\Finance\Receivable\BookingInterestBoardAdjustment;
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\BookingInterest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CalculateInterestBoardAdjust {
    /**
     * Services for calculating booking interest for each employer - Late contribution.
     *
     * @param string $model
     *
     * @return void
     */

    private $interest;
    private $booking;
    private $receipt;
    private $hasreceipt; //has receipt flag
    private $amount; //booked amount / paid amount
    private $payment_types;
    private $receipt_codes;


    public function __construct()
    {
        $this->interest = new BookingInterestRepository();
        $this->booking = new BookingRepository();
        $this->receipt = new ReceiptRepository();;
        $this->payment_types = new PaymentTypeRepository();
        $this->receipt_codes = new ReceiptCodeRepository();
    }

//    get paid date ; if booking is already paid if not yet paid get day today
    public function getPaidDate($booking_id) {
        $receipt = $this->booking->getFirstPaidReceipt($booking_id);
        if (!is_null($receipt)){
            $this->hasreceipt = 1;
            /* Sum all amounts of paid receipts of this booking */
            $this->amount = $this->receipt_codes->getTotalContributionPaidForBookingId($booking_id);

            return $receipt->rct_date;
            /* == Commented cheque payment consideration on 26th Sep 2017 ======*/
//            return ($receipt->payment_type_id == $this->payment_types->chequePayment()) ? $receipt->created_at : $receipt->rct_date;
            //-------------

        }
        $this->amount =  $this->booking->query()->where('id', $booking_id)->select(['amount'])->first()->amount;
        $this->hasreceipt = 0;
        return Carbon::now();
    }

//    get late months
    public function getLateMonths($rcv_date, $paid_date) {
        $rcv_date = $this->checkMonthIsBeforeJul2016($rcv_date);
        $late_months = months_diff($rcv_date, $paid_date);
        $late_months = $late_months - sysdefs()->data()->contribution_grace_period; //exclude grace period
        return ($late_months >= 0) ? $late_months : 0;
    }

    /**
     * @param $rcv_date
     * Check if Miss month is  before JUl 2016 (Grace period given by fund) for contribution before that date
     */
    public function checkMonthIsBeforeJul2016($rcv_date)
    {
//        if($rcv_date < '2016-06-01'){
//            $rcv_date = '2016-05-01';
//        }
        if($rcv_date < '2017-09-01'){
            $rcv_date = '2017-08-01';
        }
        return $rcv_date;
    }


//    calculate interest -> need a forumla update
    public function getInterest($interest_percentage, $late_months, $amount) {
//        $interest_raised = ($interest_percentage * 0.01) * $late_months * $amount;
        if ($late_months < 1){
            $late_months = 0;
        }
        $interest_raised =   $amount * (pow((1 + $interest_percentage * 0.01), $late_months) - 1);
        return $interest_raised;
    }
//
//if already exist -> update interest
    public function update($booking_id, $late_months, $hasreceipt, $amount) {
        $booking_interest = $this->interest->checkIfBookingHasInterest($booking_id);
        $interest_percent = $booking_interest->interest_percent;
        $interest_raised = $this->getInterest($interest_percent, $late_months, $amount);
        /*If Interest has remittance: Save Old info before update*/
        if ($this->receipt_codes->getTotalInterestPaidForBookingId($booking_id) > 0) {
            $this->updateAdjustment($booking_interest, $late_months, $amount);
        }
        $booking_interest->update(['late_months' => $late_months,'amount' => $interest_raised, 'hasreceipt' => $hasreceipt]);


    }

    //if new create interest
    public function create($booking_id,$late_months,$amount,$interest_percent,$rcv_date,$hasreceipt) {
        $interest_raised = $this->getInterest($interest_percent,$late_months,$amount);
        $booking_interest = BookingInterest::create(['booking_id'=>$booking_id, 'miss_month' => $rcv_date, 'late_months' => $late_months, 'interest_percent'=>$interest_percent, 'amount' => $interest_raised,'hasreceipt' => $hasreceipt]);
    }

//    Delete if interest exist but rct date does n't exceed grace period
    public function delete($booking_id, $late_months,$amount) {
        $booking_interest = $this->interest->checkIfBookingHasInterest($booking_id);

        if (!is_null($booking_interest)) {
            /*Delete if does not have any remittance*/
            if ($this->receipt_codes->getTotalInterestPaidForBookingId($booking_id) == 0){
                $booking_interest->delete();
            }else{
                $this->updateAdjustment($booking_interest, $late_months, $amount);
            }
        }
    }

    /*Update interest when is adjusted for instance already has remittance*/
    public function updateAdjustment(Model $booking_interest, $late_months, $amount)
    {
        $interest_amount = $booking_interest->amount;
        $interest_percent = sysdefs()->data()->contribution_penalty_percent;
        $new_interest_raised = $this->getInterest($interest_percent,$late_months,$amount);
        $old_late_months = $booking_interest->late_months;
        if ($late_months < 1)
        {
            $late_months = 0;
        }
        $booking_interest->update(['amount' => $new_interest_raised,
            'adjust_amount' => (-1 * ($interest_amount - $new_interest_raised)),
            'adjust_note' => 'Board Waiver Of Sep 2017',
            'isadjusted' => 1,
            'is_board_waived' => 1,
            'late_months' => $late_months,
            'old_late_months' => $old_late_months,
        ]);
    }




//post interest raised into Booking Interest
    public function postInterest($rcv_date,$late_months,$interest_percent,$booking_id,$amount,$hasreceipt)
    {
        if ($late_months > 0) //exceed grace period
        {
            /* check if interest already exist*/
            if ($this->interest->checkIfBookingHasInterest($booking_id) == null) {
                //raise and post interest - Post New
                $this->create($booking_id, $late_months, $amount, $interest_percent, $rcv_date, $hasreceipt);
            } else {
                /* if exist and booking not yet receipted update using posted interest percent -> recalculate and
 update*/
                $this->update($booking_id, $late_months, $hasreceipt, $amount);
            }
        } else {
//            delete if not exceeding grace period
            $this->delete($booking_id, $late_months, $amount);
        }
    }

//    calculate, post / create or update interest into Booking_interest
    public function createInterest($booking_id) {
        $rcv_date = Carbon::parse($this->booking->query()->where('id', $booking_id)->select(['rcv_date'])->first()
            ->rcv_date);
        $paid_date= $this->getPaidDate($booking_id);
        $late_months = $this->getLateMonths($rcv_date, $paid_date);
        $interest_percent = sysdefs()->data()->contribution_penalty_percent;
        $this->postInterest($rcv_date, $late_months, $interest_percent, $booking_id, $this->amount, $this->hasreceipt);
        $this->interest->recheckPaidStatus($booking_id); // recheck paid status
    }

    //loop for all missing contributions/ bookings or with late submission
    public function calculateInterest($employer_id) {
        $bookings = $this->getBookingForPenalty($employer_id);
        foreach ($bookings as $booking){
            $this->createInterest($booking->id);
        }
    }

    public function dueAmount($month, $year, $employer_id)
    {
        $amount = $this->interest->employerDueAmount($month, $year, $employer_id);
        return $amount;
    }



    //get all bookings with late submission or not submitted
    public function getBookingForPenalty($employer_id) {
        $bookings = Booking::where('employer_id','=',$employer_id)->whereDate('rcv_date','<',Carbon::parse(Carbon::now()->subMonth(1))->format('Y-m-d'))->whereDoesntHave('bookingInterest', function($query){
            $query->where('iswrittenoff','=',1);
        })->select(['id'])->get();

    /*keep track of board adjustment*/
    $this->createBoardAdjustment() ;

        return $bookings;
    }


    /* Create booking interest board adjustments */
    public function createBoardAdjustment()
    {
        $adjust_date = '2017-Sep-30';
        $description = 'Board Waiver';
$board_adjustment = BookingInterestBoardAdjustment::updateOrCreate(['adjustment_date' => $adjust_date], ['description' => $description]);
    }


}