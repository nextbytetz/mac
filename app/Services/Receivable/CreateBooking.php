<?php

namespace App\Services\Receivable;

/*
 * class CreateBooking
 * Author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto create booking for each employer
 */

use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Compliance\UpdateBookingsNotContributed;
use Carbon\Carbon;

class CreateBooking {
    /**
     * Services for calculating booking for each employer
     *
     * @param string $model
     *
     * @return void
     *
     */
    private $employer;
    private $booking;
    private $receipt_code;
    private $is_schedule = 0;

    public function __construct()
    {
        $this->employer = new EmployerRepository();
        $this->booking = new BookingRepository();
        $this->receipt_code = new ReceiptCodeRepository();
    }

//    get booking_date
    public function getBookingDate() {
        $booking_date  = '28-'.  Carbon::parse(Carbon::now())->format('M') . '-' . Carbon::parse(Carbon::now())->format('Y');
        $format = 'd-M-Y';
        return  Carbon::createFromFormat($format, $booking_date);
    }

//    CHECK IF BOOKING ALREADY EXIST for this employer_id
    public function checkIfExist($id, $rcv_date) {
//        $booking = $this->booking->query()->where('employer_id','=', $id)
//            ->whereDate('rcv_date', '=', Carbon::parse($rcv_date)->format('Y-m-d'))
//            ->first();
        $parsed_rcv_date = Carbon::parse($rcv_date);
        $booking = $this->booking->query()->where('employer_id','=', $id)
        ->whereRaw("date_part('month', rcv_date) = " . $parsed_rcv_date->format('m'))
        ->whereRaw("date_part('year', rcv_date) = " . $parsed_rcv_date->format('Y'))
        // ->whereMonth('rcv_date','=', $parsed_rcv_date->format('m'))->whereYear('rcv_date','=', $parsed_rcv_date->format('Y'))
        ->first();
        return $booking;
    }

    //if new create interest
    public function create($fin_code_id, $id, $rcv_date, $amount, $user_id, $member_count, $description) {
        $booking = $this->booking->query()->create(['rcv_date'=>$rcv_date, 'employer_id' => $id, 'fin_code_id' => $fin_code_id, 'amount'=>$amount, 'user_id' => $user_id, 'member_count'=>$member_count, 'description'=>$description, 'is_schedule' => $this->is_schedule]);
        return $booking;
    }

//    Function: CREATE BOOKING -> for each employer_id
    public function createBooking($id, $rcv_date = null, $amount = null, $member_count = null,
      $user_id = null, $is_schedule = 0 ) {
        if (is_null($rcv_date)) {
            $rcv_date = $this->getBookingDate();
        }
        /*assign flag*/
        $this->is_schedule = $is_schedule;
//       check if booking already exist for this employer_id
        $booking = $this->checkIfExist($id, $rcv_date);

        if (is_null($amount)) {
            $amount =  $this->receipt_code->getRecentContribution($id, $rcv_date);
        }

        if (!$booking)
        {    //If doesn't exist -> create booking
            $fin_code_id = $this->employer->getFinCodeId($id); // NEED UPDATES ACCORDING TO FINANCE CODES PROVIDED
            if (is_null($member_count)) {
                $member_count = $this->employer->getMemberCount($id);
            }
            $description= "Employer monthly bookings";
            return $this->create($fin_code_id, $id, $rcv_date, $amount, $user_id, $member_count, $description);
        } else {
//            $booking->amount = $amount;
//            $booking->save();
            $update_booking_service = new UpdateBookingsNotContributed();
            $update_booking_service->updateBooking($booking);
            return $booking;
        }
    }

    public function bookedAmount($month, $year, $employer_id)
    {
        $booking = $this->booking->employerBooking($month, $year, $employer_id);
        if ($booking) {
            $amount = $booking->amount;
        } else {
            $amount = 0;
        }
        return $amount;
    }

    public function manualBooking($id, $rcv_date)
    {

    }

}