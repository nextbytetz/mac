<?php

namespace App\Services\Receivable;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto calculate interest for contribution contributed late (exceed grace period)
 */
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Models\Finance\Receivable\Booking;
use App\Models\Finance\Receivable\BookingInterest;
use Carbon\Carbon;
use App\Notifications\InstalmentReminderNotification;
use DB;
use App\Models\Operation\Compliance\Member\Employer;

class CalculateInterest {
    /**
     * Services for calculating booking interest for each employer - Late contribution.
     *
     * @param string $model
     *
     * @return void
     */

    private $interest;
    private $booking;
    private $receipt;
    private $hasreceipt; //has receipt flag
    private $amount; //booked amount / paid amount
    private $payment_types;
    private $receipt_codes;


    public function __construct()
    {
        $this->interest = new BookingInterestRepository();
        $this->booking = new BookingRepository();
        $this->receipt = new ReceiptRepository();;
        $this->payment_types = new PaymentTypeRepository();
        $this->receipt_codes = new ReceiptCodeRepository();
    }

//    get paid date ; if booking is already paid if not yet paid get day today
    public function getPaidDate($booking_id) {
//        $receipt = $this->booking->getFirstPaidReceipt($booking_id);
        $receipt = $this->booking->getFirstPaidReceiptWithLegacy($booking_id);

        if (!is_null($receipt)){
            $this->hasreceipt = 1;
            /* Sum all amounts of paid receipts of this booking */
            $this->amount = $this->receipt_codes->getTotalContributionPaidForBookingId($booking_id);

            /*<NEW> Check if interest has been adjusted */
            $interest_adjusted = $this->getInterestIfAdjusted($booking_id);
            if(isset($interest_adjusted))
            {
                /*Interest adjusted - Take new payment date*/
                $rct_date = isset($interest_adjusted->new_payment_date) ? $interest_adjusted->new_payment_date : $receipt->rct_date;
            }else{
                /*Interest not adjusted*/
                $rct_date = $receipt->rct_date;
            }
            return $rct_date;
            /* == Commented cheque payment consideration on 26th Sep 2017 ======*/
//            return ($receipt->payment_type_id == $this->payment_types->chequePayment()) ? $receipt->created_at : $receipt->rct_date;
            //-------------

        }
        $this->amount =  $this->booking->query()->where('id', $booking_id)->select(['amount'])->first()->amount;
        $this->hasreceipt = 0;
        return Carbon::now();
    }

//    get late months
    public function getLateMonths($rcv_date, $paid_date) {
        $rcv_date = $this->checkMonthIsBeforeJul2016($rcv_date);
        $late_months = months_diff($rcv_date, $paid_date);
        $late_months = $late_months - sysdefs()->data()->contribution_grace_period; //exclude grace period
        return ($late_months >= 0) ? $late_months : 0;
    }

    /**
     * @param $rcv_date
     * Check if Miss month is  before JUl 2016 (Grace period given by fund) for contribution before that date
     */
    public function checkMonthIsBeforeJul2016($rcv_date)
    {
//        if($rcv_date < '2016-06-01'){
//            $rcv_date = '2016-05-01';
//        }
        if($rcv_date < '2017-09-01'){
            $rcv_date = '2017-08-01';
        }
        return $rcv_date;
    }


    /*Check If Interest is adjusted*/
    public function getInterestIfAdjusted($booking_id)
    {
        $booking_interest_adjusted = BookingInterest::query()->where('booking_id', $booking_id)->where('isadjusted',1)->first();
        return $booking_interest_adjusted;
    }


//    calculate interest -> need a forumla update
    public function getInterest($interest_percentage, $late_months, $amount) {
//        $interest_raised = ($interest_percentage * 0.01) * $late_months * $amount;
        $interest_raised =   $amount * (pow((1 + $interest_percentage * 0.01), $late_months) - 1);
        return $interest_raised;
    }
//
//  if already exist -> update interest
    public function update($booking_id, $late_months, $hasreceipt, $amount) {
        $booking_interest = $this->interest->checkIfBookingHasInterest($booking_id);
        $interest_percent = $booking_interest->interest_percent;
        $interest_raised = $this->getInterest($interest_percent, $late_months, $amount);
        $booking_interest->update(['late_months' => $late_months,'amount' => $interest_raised, 'hasreceipt' => $hasreceipt]);
    }

    //if new create interest
    public function create($booking_id,$late_months,$amount,$interest_percent,$rcv_date,$hasreceipt) {
        $interest_raised = $this->getInterest($interest_percent,$late_months,$amount);
        $booking_interest = BookingInterest::create(['booking_id'=>$booking_id, 'miss_month' => $rcv_date, 'late_months' => $late_months, 'interest_percent'=>$interest_percent, 'amount' => $interest_raised,'hasreceipt' => $hasreceipt]);
    }

//Delete if interest exist but rct date does n't exceed grace period and which has no remittance
    public function delete($booking_id) {
        $booking_interest = $this->interest->checkIfBookingHasInterest($booking_id);

        $amount_paid = $this->receipt_codes->getTotalInterestPaidForBookingId($booking_id);
        $is_adjusted = ($booking_interest) ? $booking_interest->isadjusted : 0;
        if (!is_null($booking_interest) && $amount_paid == 0 && $is_adjusted == 0) {
            $booking_interest->delete();
        }else{
            /*Update i.e late months and amount if already paid atleast once*/
            if(isset($booking_interest)){
                $booking_interest->update([
                    'amount' => 0,
                    'late_months' => 0
                ]);
            }
        }
    }


//Post interest raised into Booking Interest
    public function postInterest($rcv_date,$late_months,$interest_percent,$booking_id,$amount,$hasreceipt)
    {

        if ($late_months > 0) //exceed grace period
        {
            /* check if interest already exist*/
            if ($this->interest->checkIfBookingHasInterest($booking_id) == null) {
                //raise and post interest - Post New
                $this->create($booking_id, $late_months, $amount, $interest_percent, $rcv_date, $hasreceipt);
            } else {
                /* if exist and booking not yet receipted update using posted interest percent -> recalculate and
                update*/
                $this->update($booking_id, $late_months, $hasreceipt, $amount);
            }
        } else {
            //            delete if not exceeding grace period
            $this->delete($booking_id);
        }
    }


//    calculate, post / create or update interest into Booking_interest
    public function createInterest($booking_id) {
       $booking_first = $this->booking->query()->where('id', $booking_id)->first();
       $employer = Employer::find($booking_first->employer_id);

       $rcv_date = Carbon::parse($this->booking->query()->where('id', $booking_id)->select(['rcv_date'])->first()
        ->rcv_date);
       $paid_date= $this->getPaidDate($booking_id);
       $late_months = $this->getLateMonths($rcv_date, $paid_date);
       // $interest_percent = sysdefs()->data()->contribution_penalty_percent;
       $interest_percent = $employer->contributionInterestRate($booking_first->rcv_date,1)*100;
       $this->postInterest($rcv_date, $late_months, $interest_percent, $booking_id, $this->amount, $this->hasreceipt);
        $this->interest->recheckPaidStatus($booking_id); // recheck paid status
    }

    //loop for all missing contributions/ bookings or with late submission
    public function calculateInterest($employer_id) {
        $bookings = $this->booking->getBookingForPenalty($employer_id);

        foreach ($bookings as $booking){
            $this->createInterest($booking->id);
        }
    }

    /*Due amount*/
    public function dueAmount($month, $year, $employer_id)
    {
        $amount = $this->interest->employerDueAmount($month, $year, $employer_id);
        return $amount;
    }


    /* interest calculate to those have instalments*/
    public function interestWithInstalments(){
        $instalments = \DB::table('portal.contribution_arrears')->where('installment_payment_id','!=',null)->get();
        if(count($instalments) > 0){
            foreach ($instalments as $instalment) {
                $employer = Employer::find($instalment->employer_id);

                $principle = $instalment->arrear_amount;
                $rcv_date = $instalment->contrib_month;
                $today = Carbon::now()->format('Y-m-d');
                $late_months = $this->getLateMonths($rcv_date, $today);
                // $late_months = months_diff($rcv_date, $today);
                // $interest_percentage = 10;
                $interest_percentage = $employer->contributionInterestRate($instalment->contrib_month,1)*100;
                $interest = $this->getInterest($interest_percentage, $late_months, $principle);

                /*save interest for instalment*/
                $this->saveInterest($instalment,$interest);
            }
        }
    }

    public function saveInterest($instalment,$interest){
        $save = DB::table('portal.contribution_arrears')->where('id',$instalment->id)->update([
          'interest' => $interest
      ]);
    }

    /* notify portal users five days before instalments payments*/
    public function instalmentNotify(){
      /*loop in all instalments and chack the dates then minus five days 
      if match the day of today send email */

        $instalments = DB::table('portal.installment_payments')->select('installment_payments.due_date','installment_payments.contribution_arrears_id','contribution_arrears.contrib_month','installment_payments.employer_id','employers.name as employer_name','employers.email as employer_email','users.name as user_name','users.email as user_email','contribution_arrears.arrear_amount','installment_requests.user_id')
    ->join('portal.installment_requests','installment_requests.id','=','installment_payments.installment_request_id')
    ->join('main.employers','employers.id','=','installment_requests.employer_id')
    ->join('portal.users','users.id','=','installment_requests.user_id')
    ->join('portal.contribution_arrears','contribution_arrears.id','=','installment_payments.contribution_arrears_id')
    ->get();

    $today = Carbon::now()->format('Y-m-d');

    foreach ($instalments as $instalment) {
        $day = Carbon::parse($instalment->due_date)->subDays(5)->format('Y-m-d');
        if ($today == $day) {
            /* send notification */
            $emails = array();
            $employer = array();
            $emp = DB::table('main.employers')->where('id',$instalment->employer_id)->get()->toArray();
            $usr = DB::table('portal.users')->where('id',$instalment->user_id)->get()->toArray();
            $employer['email'] = $emp;
            $employer['name'] = $instalment->employer_name;

            $user = array();
            $user['email'] = $usr;
            $user['name'] = $instalment->user_name;

            $emails[0] = $employer;
            $emails[1] = $user;

            foreach ($emails as $key => $email) {
                $this->sendInstalmenetReminderEmail($email['email'],$email['name'],$instalment->contrib_month,$instalment->arrear_amount,$instalment->due_date,$instalment->employer_id);
            }

        }

    }

}

public function sendInstalmenetReminderEmail($email, $name, $contrib_month, $arrear_amount, $due_date, $employer_id){
    $email = json_decode(json_encode($email), FALSE);
    try {
        \Notification::send($email, new InstalmentReminderNotification($name,$contrib_month,$arrear_amount, $due_date, $employer_id));
    } catch (Exception $e) {
        logger($e->getMessage());
    }
}

// public function commitmentNotify(){
//        /*loop in all commitment and chack the dates then minus five days 
//        if match the day of today send email */

//         $instalments = DB::table('portal.commitment_requests')->select('installment_payments.due_date','installment_payments.contribution_arrears_id','contribution_arrears.contrib_month','installment_payments.employer_id','employers.name as employer_name','employers.email as employer_email','users.name as user_name','users.email as user_email','contribution_arrears.arrear_amount','installment_requests.user_id')
//     ->join('portal.commitment_options','commitment_options.id','=','commitment_requests.commitment_option_id')
//     ->join('portal.commitment_descriptions','commitment_requests.id','=','commitment_descriptions.commitment_request_id')
//     ->join('main.employers','employers.id','=','commitment_requests.employer_id')
//     ->join('portal.users','users.id','=','commitment_requests.user_id')
//     ->join('portal.arrears_commitment','arrears_commitment.request_id','=','commitment_requests.user_id')
//     ->join('portal.contribution_arrears','contribution_arrears.id','=','installment_payments.contribution_arrears_id')
//     ->get();

//     $today = Carbon::now()->format('Y-m-d');

//     foreach ($instalments as $instalment) {
//         $day = Carbon::parse($instalment->due_date)->subDays(5)->format('Y-m-d');
//         if ($today == $day) {
//             /* send notification */
//             $emails = array();
//             $employer = array();
//             $emp = DB::table('main.employers')->where('id',$instalment->employer_id)->get()->toArray();
//             $usr = DB::table('portal.users')->where('id',$instalment->user_id)->get()->toArray();
//             $employer['email'] = $emp;
//             $employer['name'] = $instalment->employer_name;

//             $user = array();
//             $user['email'] = $usr;
//             $user['name'] = $instalment->user_name;

//             $emails[0] = $employer;
//             $emails[1] = $user;

//             foreach ($emails as $key => $email) {
//                 $this->sendInstalmenetReminderEmail($email['email'],$email['name'],$instalment->contrib_month,$instalment->arrear_amount,$instalment->due_date,$instalment->employer_id);
//             }

//         }

//     }
// }

}