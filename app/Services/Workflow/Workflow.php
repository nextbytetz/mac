<?php

namespace App\Services\Workflow;

use App\Events\Sockets\BroadcastWorkflowUpdated;
use App\Exceptions\WorkflowException;
use App\Models\Operation\Compliance\Member\EmployerParticularChange;
use App\Models\Workflow\WfDefinition;
use App\Repositories\Backend\Access\PortalUserRepository;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receivable\InterestAdjustmentRepository;
use App\Repositories\Backend\Finance\Receivable\InterestWriteOffRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationWorkflowRepository;
use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskWorkflowRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerAdvancePaymentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureExtensionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerParticularChangeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollBankUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollBeneficiaryUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollMpUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRecoveryRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\Backend\Operation\Payroll\Retiree\PayrollRetireeMpUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollReconciliationRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditPeriodRepository;
use App\Repositories\Backend\Investiment\InvestimentBudgetRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;

use App\Repositories\Backend\Workflow\WfAllocationRepository;
use App\Repositories\Backend\Workflow\WfGroupRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\Backend\Workflow\WfDefinitionRepository;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Models\Operation\Compliance\Member\InstallmentRequest;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\Online\MergeDeMergeEmployerPayrollRepository;
use App\Repositories\Backend\Operation\Claim\HspBilling\HspBillSummaryRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionModificationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionInterestRateRepository;

/**
 * Class Workflow
 *
 * Class to process all application workflows at different levels. It controls swiftly
 * the process of forwarding the application process and handle the proper completion of
 * each workflow.
 *
 * @author     Erick Chrysostom <e.chrysostom@nextbyte.co.tz>
 * @category   MAC
 * @package    App\Services\Workflow
 * @subpackage None
 * @copyright  Copyright (c) Workers Compensation Fund (WCF) Tanzania
 * @license    Not Applicable
 * @version    Release: 1.02
 * @link       None
 * @since      Class available since Release 1.0.0
 */

class Workflow
{

    /**
     * @var
     */
    private $level;

    /**
     * @var
     */
    private $wf_module_group_id;

    /**
     * @var
     */
    private $wf_module_id;

    /**
     * @var
     */
    private $resource_id;

    /**
     * @var
     */
    public $wf_definition_id;

    /**
     * @var WfDefinitionRepository
     */
    private $wf_definition;

    /**
     * @var WfModuleRepository
     */
    private $wf_module;

    /**
     * @var WfTrackRepository
     */
    private $wf_track;


    /**
     * Workflow constructor.
     * @param array $input
     * @throws GeneralException
     */
    public function __construct(array $input)
    {
        /** sample 1 $input : ['wf_module_group_id' => 4, 'resource_id' => 1] **/
        /** sample 2 $input : ['wf_module_group_id' => 3, 'resource_id' => 1, 'type' => 4] **/
        /** sample 3 $input : ['wf_module_group_id' => 3] **/
        /** sample 4 $input : ['wf_module_group_id' => 3, 'type' => 4] **/
        /** sample 4 $input : ['wf_module_id' => 3, 'type' => 4] **/
        $this->wf_definition = new WfDefinitionRepository();
        $this->wf_module = new WfModuleRepository();
        $this->wf_track = new WfTrackRepository();
        $this->resource_id = (isset($input['resource_id']) ? $input['resource_id'] : null);
        foreach ($input as $key => $value) {
            switch ($key) {
                case 'wf_module_group_id':
                $this->wf_module_group_id = $input['wf_module_group_id'];
                $this->wf_module_id = $this->wf_module->getModule($input);
                break;
                case 'wf_module_id':
                $wf_module_id = $value;
                $this->wf_module_id = $wf_module_id;
                $this->wf_module_group_id = $this->wf_module->getGroup($wf_module_id);
                break;
            }
        }
        /**
         * Get current values .....
         */
        $this->wf_definition_id = $this->wf_definition->getDefinition($this->wf_module_id, $this->resource_id);
    }

    /**
     * @param bool $skip
     * @param int $wf_definition
     * @return null
     */
    public function nextLevel($skip = false, $wf_definition = 0)
    {
        if ($wf_definition) {
            $definition = $this->wf_definition->getCurrentLevel($wf_definition);
            $return = $definition->level;
        } else {
            $return = $this->wf_definition->getNextLevel($this->wf_definition_id, $skip);
        }
        return $return;
    }

    /**
     * @return null
     */
    public function prevLevel()
    {
        return $this->wf_definition->getPrevLevel($this->wf_definition_id);
    }

    /**
     * @return mixed
     */
    public function lastLevel()
    {
        return $this->wf_definition->getLastLevel($this->wf_module_id);
    }

    /**
     * @return mixed
     */
    public function currentLevel()
    {
        $wf_definition = $this->wf_definition->getCurrentLevel($this->wf_definition_id);
        return $wf_definition->level;
    }

    /**
     * @return mixed
     */
    public function previousLevels()
    {
        $levels = $this->wf_definition->getPreviousLevels($this->wf_definition_id, $this->resource_id);
        return $levels;
    }

    /**
     * @return mixed
     */
    public function selectiveLevels()
    {
        $levels = $this->wf_definition->getSelectiveLevels($this->wf_definition_id);
        return $levels;
    }

    public function getUsersToSelect()
    {
        $users = $this->wf_definition->getUsersToSelect($this->wf_definition_id);
        return $users;
    }

    /**
     * @param bool $accuracy | if accuracy is set true, fetch accurate module depend on the resource_id. This happen when same group and type have active workflow modules still pending.
     * @return mixed
     */
    public function getModule($accuracy = false)
    {
        $return = $this->wf_module_id;
        if ($accuracy) {
            switch ($this->wf_module_group_id) {
                case 3:
                case 4:
                if (!is_null($this->resource_id)) {
                        //$incident = (new NotificationReportRepository())->query()->where("id", $this->resource_id)->first();
                    $workflow = (new NotificationWorkflowRepository())->query()->where("id", $this->resource_id)->first();
                    $incident = $workflow->notificationReport;
                    if ($incident->isprogressive) {
                            //Progressive Notification Report
                        $module = $this->wf_module->query()->select(["type", "wf_module_group_id"])->where("id", $this->wf_module_id)->first();
                        $moduleIds = $this->wf_module->query()->where(["type" => $module->type, "wf_module_group_id" => $module->wf_module_group_id])->pluck("id")->all();
                        $workflow = $incident->workflows()->whereIn("notification_workflows.wf_module_id", $moduleIds)->select(["wf_module_id"])->limit(1)->first();
                        $return = $workflow->wf_module_id;
                    } else {

                    }
                }
                break;
            }
        }
        return $return;
    }

    /**
     * Level for performing claim assessment level
     * @return int|null
     */
    public function claimAssessmentLevel()
    {
        $assessment_level = null;
        switch ($this->wf_module_id) {
            // Basic Procedure - wf_module
            case 3:
            $assessment_level = 4;
            break;
            // Proposed Procedure 1 Occupation Accident, Disease & Death
            case 10:
            $assessment_level = 7;
            break;
            case 18:
            case 76:
                //Claim Benefits
            $assessment_level = 7;
            break;
        }
        return $assessment_level;
    }


    /**
     * Level for performing payroll payment level
     * @return int|null
     */
    public function payrollFinancePaymentLevel()
    {
        $finance_level = null;
        switch ($this->wf_module_id) {
            // Basic Procedure - wf_module
            case 28:
            $finance_level = 12;
            break;

            /*Procedure: 9L*/
            case 63:
            $finance_level = 9;
            break;

        }
        return $finance_level;
    }

    /**
     * @param $sign
     * @param $skip
     * @return mixed
     */
    public function nextDefinition($sign, $skip = false)
    {
        return $this->wf_definition->getNextDefinition($this->wf_module_id, $this->wf_definition_id, $sign, $skip);
    }

    /**
     * @param $level
     * @return mixed
     */
    public function levelDefinition($level)
    {
        return $this->wf_definition->getLevelDefinition($this->wf_module_id, $level);
    }

    /**
     * @param $level
     * @return mixed
     */
    public function previousLevelUser($level)
    {
        return $this->wf_track->previousLevelUser($this->wf_module_id, $level, $this->resource_id);
    }

    /**
     * @param $sign
     * @param $skip
     * @return mixed
     */
    public function nextWfDefinition($sign, $skip = false)
    {
        return $this->wf_definition->getNextWfDefinition($this->wf_module_id, $this->wf_definition_id, $sign, $skip);
    }

    /**
     * @return mixed
     */
    public function currentWfDefinition()
    {
        $wf_definition = $this->wf_definition->getCurrentLevel($this->wf_definition_id);
        return $wf_definition;
    }

    public function hasActiveWfDefinition()
    {
        $count = $this->wf_track->query()->where(['resource_id' => $this->resource_id, 'wf_definition_id' => $this->wf_definition_id, 'status' => 0])->count();
        return $count;
    }

    /**
     * @return mixed
     */
    public function currentWfTrack()
    {
        return $this->wf_track->getRecentResourceTrack($this->wf_module_id, $this->resource_id);
    }

    /**
     * @return mixed
     */
    public function currentTrack()
    {
        $wfTrack = $this->currentWfTrack();
        return $wfTrack->id;
    }

    /**
     * @param $level
     * @throws GeneralException
     * @description check if the resource is at specified workflow and has already assigned ...
     */
    public function checkLevel($level)
    {
        //dd($this->wf_definition_id);
        $wfTrack = $this->currentWfTrack();

        if ((int) $level == (int) $this->currentLevel()) {
            if ($wfTrack->assigned == 1) {
                $return = ['success' => true];
            } else {
                $return = ['success' => false, 'message' => trans('exceptions.backend.workflow.level_not_assigned')];
            }
            //dd($return);
        } else {
          $return = ['success' => false, 'message' => trans('exceptions.backend.workflow.level_error')];
      }
      if (!$return['success']) {
        throw new GeneralException($return['message']);
    };
}

    /**
     * @param array ...$levels
     * @throws GeneralException
     * @description Check for Multi level, if the resource is at specified workflow and has already assigned
     */
    public function checkMultiLevel(...$levels)
    {
        $wfTrack = $this->currentWfTrack();
        $return = [];
        foreach ($levels as $level) {
            if ($level == $this->currentLevel()) {
                if ($wfTrack->assigned == 1) {
                    $return = ['success' => true];
                } else {
                    $return = ['success' => false, 'message' => trans('exceptions.backend.workflow.level_not_assigned')];
                }
                break;
            }
        }
        if (empty($return)) {
            $return = ['success' => false, 'message' => trans('exceptions.backend.workflow.level_error')];
        }
        if (!$return['success']) {
            throw new GeneralException($return['message']);
        };
    }

    /**
     * @param $id
     * @param $level
     * @return bool
     * @description check if user has access to a specific level
     */
    public function userHasAccess($id, $level)
    {
        if (env("TESTING_MODE", 0)) {
            $return = true;
        } else {

            $return = $this->wf_definition->userHasAccess($id, $level, $this->wf_module_id);
        }
        return $return;
    }

    /**
     * @description check if current definition has restrictive to only allocated user
     * @return bool
     */
    public function restrictiveCheck()
    {
        $return = true;
        $definition = $this->currentWfDefinition();
        if ($definition) {
            if ($definition->isrestrictive) {
                $track = $this->currentWfTrack();
                if ($track) {
                    if ($track->user_id != access()->id()) {
                        $return = false;
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Write a workflow for the first time
     * @param $input
     * @param int $source, determine whether source is 2 => online or 1 => internally.
     * @throws GeneralException
     * @description Create a new workflow log
     */
    public function createLog($input, $source = 1)
    {
        $insert = [
            'status' => 1,
            'resource_id' => $input['resource_id'],
            'assigned' => 0,
            'comments' => $input['comments'],
            'wf_definition_id' => $this->wf_definition_id,
            'receive_date' => Carbon::now(),
            'forward_date' => Carbon::now(),
            'allocated' => access()->id(),
        ];
        $track = new WfTrackRepository();
        $wf_track = $track->query()->create($insert);
        switch ($source) {
            case 1:
            $user = access()->user();
            $wf_track->user_id = $input['user_id'];
            $wf_track->assigned = 1;
            $user->wfTracks()->save($wf_track);
            break;
            case 2:
            $userRepo = new PortalUserRepository();
            $user = $userRepo->query()->find($input['user_id']);
            $wf_track->user_id = $input['user_id'];
            $wf_track->assigned = 1;
            $user->wfTracks()->save($wf_track);
            break;
        }

        //update Resource Type for the current wftrack
        $this->updateResourceType($wf_track);
        $nextInsert = $this->upNew($input);
        $wf_track = $track->query()->create($nextInsert); // changed

        //update Resource Type for the next wftrack
        $this->updateResourceType($wf_track);
    }

    /**
     * @param Model $wfTrack
     * @throws GeneralException
     * @description Update the resource type form different resources.
     */
    private function updateResourceType(Model $wfTrack)
    {
        $resourceId = $wfTrack->resource_id;
        $wfModule = $wfTrack->wfDefinition->wfModule;
        $moduleGroupId = $wfModule->wfModuleGroup->id;
        //$this->wf_module_group_id
        switch ($moduleGroupId) {
            case 1:
                //Interest Waiving
            $interestWriteOff = (new InterestWriteOffRepository())->query()->find($resourceId);
            $interestWriteOff->wfTracks()->save($wfTrack);
            break;
            case 2:
            case 67://TODO <wf_module_check>
                //Interest Adjustment
            $bookingInterest = (new BookingInterestRepository())->query()->find($resourceId);
            $bookingInterest->wfTracks()->save($wfTrack);
            break;
            case 3:
            case 4:
                //Claim & Notification Processing
            $type = $wfModule->type;
            if ($type >= 4) {
                    //This is progressive notification
                $workflow = (new NotificationWorkflowRepository())->query()->find($resourceId);
                $workflow->wfTracks()->save($wfTrack);
            } else {
                    //This is legacy notification report
                $notificationReport = (new NotificationReportRepository())->query()->find($resourceId);
                $notificationReport->wfTracks()->save($wfTrack);
            }
            break;
            case 5:
                //Contribution Receipt
            $receipt = (new ReceiptRepository())->query()->find($resourceId);
            $receipt->wfTracks()->save($wfTrack);
            break;
            case 6:
                //Employer Registration
            $employer = (new EmployerRepository())->findWithoutScopeOrThrowException($resourceId);
            $employer->wfTracks()->save($wfTrack);
            break;
            case 7:
                //Contribution Before Electronic Receipt
            $legacyReceipt = (new LegacyReceiptRepository())->query()->find($resourceId);
            $legacyReceipt->wfTracks()->save($wfTrack);
            break;

            case 11:
                //Payroll bank updates
            $bank_update = (new PayrollBankUpdateRepository())->query()->find($resourceId);
            $bank_update->wfTracks()->save($wfTrack);
            break;

            case 12:
                //Payroll Status change
            $status_change = (new PayrollStatusChangeRepository())->query()->find($resourceId);
            $status_change->wfTracks()->save($wfTrack);
            break;

            case 13:
                //Payroll  Recovery
            $payroll_recovery = (new PayrollRecoveryRepository())->query()->find($resourceId);
            $payroll_recovery->wfTracks()->save($wfTrack);
            break;
            case 10:
                //Payroll  run processing
            $payroll_run_approval = (new PayrollRunApprovalRepository())->query()->find($resourceId);
            $payroll_run_approval->wfTracks()->save($wfTrack);
            break;

            case 14:
                //Payroll  reconciliations
            $payroll_reconciliations = (new PayrollReconciliationRepository())->query()->find($resourceId);
            $payroll_reconciliations->wfTracks()->save($wfTrack);
            break;

            case 15:
                //Payroll  beneficiary update
            $payroll_beneficiary_updates = (new PayrollBeneficiaryUpdateRepository())->query()->find($resourceId);
            $payroll_beneficiary_updates->wfTracks()->save($wfTrack);
            break;
            case 16:
                //Employer Advance Payment
            $advance = (new EmployerAdvancePaymentRepository())->query()->find($resourceId);
            $advance->wfTracks()->save($wfTrack);
            break;
            case 17:
                //Online Notification Application
            $incident = (new PortalIncidentRepository())->query()->find($resourceId);
            $incident->wfTracks()->save($wfTrack);
            break;
            case 18:
                //Employer business closure
            $closure = (new EmployerClosureRepository())->query()->find($resourceId);
            $closure->wfTracks()->save($wfTrack);
            break;
            case 19:
                //Letter Issuance
            $letter = (new LetterRepository())->query()->find($resourceId);
            $letter->wfTracks()->save($wfTrack);
            break;
            case 20:
                //Employer Inspection Plan
            $inspection = (new InspectionRepository())->query()->find($resourceId);
            $inspection->wfTracks()->save($wfTrack);
            break;
            case 21:
                //Employer Inspection Task
            $workflow = (new EmployerInspectionTaskWorkflowRepository())->query()->find($resourceId);
            $workflow->wfTracks()->save($wfTrack);
            break;
            case 22:
                //Employer Closure Reopen
            $employerReopen = (new EmployerClosureReopenRepository())->query()->find($resourceId);
            $employerReopen->wfTracks()->save($wfTrack);
            break;
            case 23:
                //Employer Particular Changes
            $employer_change = (new EmployerParticularChangeRepository())->query()->find($resourceId);
            $employer_change->wfTracks()->save($wfTrack);
            break;
            case 24:
                //Payroll Monthly Pension Updates

            $mp_update = (new PayrollMpUpdateRepository())->query()->find($resourceId);
            $mp_update->wfTracks()->save($wfTrack);
            break;
            case 26:
            /* Contribution Receivables */
            $ir_update = InstallmentRequest::find($resourceId);
            $ir_update->wfTracks()->save($wfTrack);

            break;
            case 27:
            /* Contribution Liability */

            break;
            case 28:
            /* osh audit */
            $osh_audit = (new OshAuditPeriodRepository())->query()->find($resourceId);
            $osh_audit->wfTracks()->save($wfTrack);
            break;
            case 29:
            /* Investment budget */
            $investment_budget = (new InvestimentBudgetRepository())->find($resourceId);
            $investment_budget->wfTracks()->save($wfTrack);
            break;

            case 30:
            /* Employer closure extension */
            $closure_extension = (new EmployerClosureExtensionRepository())->find($resourceId);
            $closure_extension->wfTracks()->save($wfTrack);
            break;
            case 31:
            /* Claim accrual */
            $claim_accrual = (new AccrualNotificationReportRepository())->find($resourceId);
            $claim_accrual->wfTracks()->save($wfTrack);
            break;
            case 32:
            /* Investigation Plan */
            $claim_accrual = (new InvestigationPlanRepository())->find($resourceId);
            $claim_accrual->wfTracks()->save($wfTrack);
            break;

            case 33:
            /* Interest Adjustment (Receipt)*/
            $interest_adj = (new InterestAdjustmentRepository())->find($resourceId);
            $interest_adj->wfTracks()->save($wfTrack);
            break;
            case 34:
            /* Payroll Retiree MP update*/
            $payroll_retiree_mp_update = (new PayrollRetireeMpUpdateRepository())->find($resourceId);
            $payroll_retiree_mp_update->wfTracks()->save($wfTrack);
            break;
            case 35:
            /* Employer Payroll Merging*/
            $employer_payroll_merge = (new MergeDeMergeEmployerPayrollRepository())->find($resourceId);
            $employer_payroll_merge->wfTracks()->save($wfTrack);
            break;
            case 36:
            /* HSP Bill Vetting */
            $hsp_bill_vetting = (new HspBillSummaryRepository())->find($resourceId);
            $hsp_bill_vetting->wfTracks()->save($wfTrack);
            break;

            case 37:
            /* Contribution Modification */
            $contrib_modification = (new ContributionModificationRepository())->find($resourceId);
            $contrib_modification->wfTracks()->save($wfTrack);
            break;

            case 38:
            /* Contribution / Interest Rate Adjustment */
            $payment_rate = (new ContributionInterestRateRepository())->find($resourceId);
            $payment_rate->wfTracks()->save($wfTrack);
            break;
            default:
            break;
        }


    }

    /**
     * @param $input_update
     * @description Update the existing workflow
     */
    public function updateLog($input_update)
    {
        $track = new WfTrackRepository();
        $wf_track = $track->find($this->currentTrack());
        $wf_track->update($input_update);
    }

    /**
     * Assigning a workflow
     * @deprecated since version 1.00
     */
    public function assign()
    {
        $track = new WfTrackRepository();
        $wf_track = $track->find($this->currentTrack());
        $wf_track->user_id = access()->id();
        $wf_track->save();
    }

    /**
     * @param array $input
     * @throws GeneralException
     * @description create the next level information/workflow log.
     */
    public function forward(array $input)
    {
        $wf_track = new WfTrackRepository();
        //Process for optional workflow level, check if has been selected...
        $currentTrack = $this->currentWfTrack();
        $select_next_start_optional = (($currentTrack->status == 4) ? 1 : 0);
        $currentDefinition = $currentTrack->wfDefinition;
        $skip = false;
        switch (true) {
            case ($currentDefinition->has_next_start_optional And !$select_next_start_optional And !$currentDefinition->is_optional):
            case (!$currentDefinition->has_next_start_optional And !$select_next_start_optional And !$currentDefinition->is_optional):
            case ($currentDefinition->has_next_start_optional And !$select_next_start_optional And $currentDefinition->is_optional): //Added Recently
                //This logic computed from the truth table, may need re-design.
                //Skip Optional level
            $skip = true;
            break;
            default:
                //Continue Regular
            $skip = false;
            break;
        }
        $nextInsert = $this->upNew($input, $skip);
        $newTrack = $wf_track->query()->create($nextInsert);

        //update Resource Type for the next wftrack
        $this->updateResourceType($newTrack);
    }

    /**
     * Create the next workflow for the next level to be assigned
     * to the next available user/staff
     * @param $input
     * @param $skip
     * @return array
     */
    public function upNew(array $input, $skip = false)
    {
        $insert = [
            'status' => 0,
            'resource_id' => $input['resource_id'],
            'assigned' => 0,
            'parent_id' => $this->currentTrack(),
            'receive_date' => Carbon::now(),
            //'wf_definition_id' => $this->nextDefinition($input['sign']),
        ];

        if ($input['sign'] == -1) {
            $level = $input['level'];
            $insert['wf_definition_id'] = $this->levelDefinition($level);
            $user = $this->previousLevelUser($level);
            if (!is_null($user)) {
                $insert['assigned'] = 1;
                $insert['user_id'] = $user;
                $insert['allocated'] = $user;
                if ($input['source'] == 1) {
                    $insert['user_type'] = "App\Models\Auth\User";
                } elseif ($input['source'] == 2) {
                    $insert['user_type'] = "App\Models\Auth\PortalUser";
                }
            }

        } else {
            $wf_definition = 0;  //if user specified a specific level to forward
            $assigned = 0;
            if (isset($input['wf_definition'])) {
                if ($input['wf_definition']) {
                    $wf_definition = $input['wf_definition'];
                    $insert['wf_definition_id'] = $wf_definition;
                } else {
                    $insert['wf_definition_id'] = $this->nextDefinition($input['sign'], $skip);
                }
            } else {
                $insert['wf_definition_id'] = $this->nextDefinition($input['sign'], $skip);
            }
            $level = $this->nextLevel($skip, $insert['wf_definition_id']);

            $user = 0;
            if (isset($input['select_user'])) {
                if ($input['select_user']) {
                    $user = $input['select_user'];
                    $assigned = 1;
                }
            }
            if (!$user) {
                $group = (new WfGroupRepository())->query()->where("id", $this->wf_module_group_id)->first();
                if ($group->autolocate) {
                    $nextWfDefinition = $this->wf_definition->find($insert['wf_definition_id']);
                    if($nextWfDefinition->ref_level){
                        /*Get allocation by ref level*/
                        $user = (new WfAllocationRepository())->getAllocationByRefLevel($nextWfDefinition, $input['resource_id']);
                        $assigned = 1;
                    }else{
                        /*Get allocation*/
                        $user = (new WfAllocationRepository())->getAllocation($nextWfDefinition,  $this->currentLevel(), $input['resource_id']);
                        $assigned = 1;
                    }
                }
            }
            if (!is_null($user)) {
                $insert['assigned'] = $assigned;
                $insert['user_id'] = $user;
                $insert['allocated'] = $user;
                $insert['user_type'] = "App\Models\Auth\User";
            }
        }
        //throw new WorkflowException($insert['status']);
        event(new BroadcastWorkflowUpdated($this->wf_module_id, $this->resource_id, $level));
        return $insert;
    }

    /**
     * Check if user has participated in the previous module level.
     * User should not participate twice in the same module.
     * @return bool
     */
    public function hasParticipated()
    {
        $return = $this->wf_track->hasParticipated($this->wf_module_id, $this->resource_id, $this->currentLevel());
        if ($return And env("TESTING_MODE")) {
            $return = false;
        }
        return $return;
    }

    /**
     * @param $input
     * @param $input_update
     * @throws GeneralException
     * @description Workflow Approve Action -- Forward to next level or complete level
     * @deprecated since version 1.00
     */
    public function wfApprove($input,$input_update)
    {
        $this->updateLog($input_update);
        if (!is_null($this->nextLevel())) {
            $this->forward($input);
        }
    }

    /**
     * @description Remove/Deactivate wf_tracks when resource id is cancelled / undone / removed.
     */
    public function wfTracksDeactivate()
    {
        $track = new WfTrackRepository();
        $wf_tracks = $track->query()->where('resource_id', $this->resource_id)->whereHas('wfDefinition', function ($query) {
            $query->where('wf_module_id', $this->wf_module_id);
        })->orderBy('id','desc');
        $wf_tracks->delete();
    }

    /**
     * @return bool
     * @description check if resource has workflow
     */
    public function checkIfHasWorkflow()
    {
        if ($this->currentWfTrack()){
            $return = true;
        } else {
            $return = false;
        }
        return $return;
    }

    /**
     * @return bool
     * @description Check if the workflow resource have had a completed workflow module trip
     */
    public function checkIfExistWorkflowModule()
    {
        $return = false;
        switch ($this->wf_module_group_id) {
            case 4:
            case 3:
                //Claim & Notification Processing
            $notificationReport = (new NotificationReportRepository())->query()->select(['isprogressive', 'id'])->find($this->resource_id);
            if ($notificationReport->isprogressive) {
                    //This is progressive notification
                if ($notificationReport->workflows()->where(["wf_module_id" => $this->wf_module_id, "wf_done" => 1])->limit(1)->count()) {
                    $return = true;
                }
            } else {
                    //This is legacy notification report
                $return = $this->wf_track->checkIfExistWorkflowModule($this->resource_id, $this->wf_module_id);
            }
            break;
            default:
            $return = $this->wf_track->checkIfExistWorkflowModule($this->resource_id, $this->wf_module_id);
            break;
        }
        return $return;
    }

    /**
     * @return bool
     * @deprecated
     */
    public function checkIfExistDeclinedWorkflowModule()
    {
        $return = false;
        switch ($this->wf_module_group_id) {
            case 4:
            case 3:
                //Claim & Notification Processing
            $notificationReport = (new NotificationReportRepository())->query()->select(['isprogressive', 'id'])->find($this->resource_id);
            if ($notificationReport->isprogressive) {
                    //This is progressive notification
                if ($notificationReport->workflows()->where(["wf_module_id" => $this->wf_module_id, "wf_done" => 2])->limit(1)->count()) {
                    $return = true;
                }
            } else {
                    //This is legacy notification report
                $return = $this->wf_track->checkIfExistDeclinedWorkflowModule($this->resource_id, $this->wf_module_id);
            }
            break;
            default:
            $return = $this->wf_track->checkIfExistDeclinedWorkflowModule($this->resource_id, $this->wf_module_id);
            break;
        }
        return $return;
    }

    /**
     * @description Check if is at Level 1 Pending
     * @return bool
     */
    public function checkIfIsLevel1Pending()
    {
        $return = $this->checkIfIsLevelPending(1);
        return $return;
    }

    /**
     * @description Check if is at defined Level Pending
     * @param $level_id
     * @return bool
     */
    public function checkIfIsLevelPending($level_id)
    {
        $current_level  = $this->currentLevel();
        $current_status = $this->currentWfTrack()->status;
        if ((int) $current_level == $level_id && $current_status == 0){
            $return = true;
        } else {
            $return = false;
        }
        return $return;
    }

    /**
     * @param $level
     * @throws GeneralException
     * @description check if can initiate a level
     */
    public function checkIfCanInitiateAction($level)
    {

        if ($this->checkIfHasWorkflow()) {
            $this->checkLevel($level);
        }
    }

    /**
     * @param $level1
     * @param null $level2
     * @throws GeneralException
     */
    public function checkIfCanInitiateActionMultiLevel($level1, $level2 = null)
    {
        if ($this->checkIfHasWorkflow()) {
            $this->checkMultiLevel($level1, $level2);
        }
    }



}