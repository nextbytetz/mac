<?php

namespace App\Services\Auth;

use Illuminate\Auth\EloquentUserProvider as BaseUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class UserProvider extends BaseUserProvider {

    /**
     * Create a new database user provider.
     *
     * UserProvider constructor.
     * @param \Illuminate\Contracts\Hashing\Hasher $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array                                      $credentials
     *
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials) {
        $password = $credentials['password'];
        /**
         * Based on the first MAC version encryption algorithms
         */
        $passwordenc = md5(md5("SSRA".$password.":security"))."123456:code".md5(":security".$password);
        return $user->password == $passwordenc;
    }

}