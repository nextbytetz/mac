<?php

namespace App\Services\Claim;

use App\Repositories\Backend\Operation\Claim\DocumentRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


/**
 * Class Alert.
 */
class NotificationDocEOfficeAutoSync
{


    protected $notification_reports;

    protected $notification_report_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification_report_id)
    {
        //
        $this->notification_reports = new NotificationReportRepository();
        $this->notification_report_id = $notification_report_id;
    }

    /**
     * Get Already stored documents
     */
    public function updateDocuments()
    {
        return DB::transaction(function () {
                   $documentTypes = new DocumentRepository();
            $documents = $documentTypes->query()->get();
            foreach ($documents as $document) {
                $notification_report_id = $this->notification_report_id;
                $document_type = $document->id;
                /*UPDATE*/
                $this->updateInsertDocumentFiles($notification_report_id, $document_type);
            }
        });
    }




    /**
     * @param $notification_report_id
     * @param $document_type
     * Update or insert document files for each document type
     */
    public function updateInsertDocumentFiles($notification_report_id, $document_type)
    {

        /*--Return document from eOffice through api url--*/
        $documents = $this->getDocumentFromEOffice($notification_report_id,$document_type);

        if (count($documents)){

            /*--IF Array has more than one document--*/

            foreach($documents as $key => $document)
            {

                $eOfficeDocumentId =  $document['documentId'];
                $subject =  $document['subject'];
                /*update eOffice id on Mac for existing integrated document*/
                if ($this->checkIfDocumentIdNotSynced($notification_report_id, $document_type) == true && $this->checkIfEOfficeDocumentIdExist($eOfficeDocumentId) == false){
                    $this->updateExistingDocument($notification_report_id, $document_type,$eOfficeDocumentId, $subject );
                }


                /*insert the following into Mac If the more than 1 doc from eoffice*/
                if ($this->checkIfEOfficeDocumentIdExist($eOfficeDocumentId) == false)
                {
                    DB::Table('document_notification_report')->insert(['notification_report_id'=> $notification_report_id, 'document_id' => $document_type, 'name' => 'e-office', 'eoffice_document_id' => $eOfficeDocumentId, 'description' => $subject, 'created_at' => Carbon::now()]);
                }

            }

            /*--END IF--*/

        }




        /*Detach Docs not synced*/

        $this->detachDocuments($notification_report_id);

    }

    public function updateExistingDocument($notification_report_id, $document_type, $eOfficeDocumentId, $subject)
    {
        $updated_document = DB::Table('document_notification_report')->where('notification_report_id', $notification_report_id)->where('document_id', $document_type)->whereNull('eoffice_document_id')->update([
            'eoffice_document_id' => $eOfficeDocumentId,
            'description' => $subject,
            'updated_at' => Carbon::now(),
            'name' => 'e-office',
        ]);

    }



    public function checkIfEOfficeDocumentIdExist($eOfficeDocumentId)
    {
        $document = DB::Table('document_notification_report')->where('eoffice_document_id', $eOfficeDocumentId)->first();

        if ($document)
        {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $document_id
     * @return bool
     * check if document exist in pivot table and not yet synced with e-office
     */
    public function checkIfDocumentIdNotSynced($notificatopn_report_id,$document_id)
    {
        $document = DB::Table('document_notification_report')->where('document_id', $document_id)->where('notification_report_id', $notificatopn_report_id)->whereNull('eoffice_document_id')->first();

        if ($document)
        {
            return true;
        }else{
            return false;
        }
    }



    /*Get document files for certain document type from eOffice through api url*/
    public function getDocumentFromEOffice($notification_report_id, $document_type)
    {
        $client = new Client();
        $link = env('EOFFICE_APP_URL');
        $fileNumber = $notification_report_id;
        $guzzlerequest = $client->get($link . "/api/claim/file/document/sync?fileNumber={$fileNumber}&documentType={$document_type}", [
            'auth' => [
                env("EOFFICE_AUTH_USER"),
                env("EOFFICE_AUTH_PASS"),
            ],
        ]);
        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        return $parsed_json;
    }


    /**
     * @param $notification_report_id
     * @param $document_type
     * Detach documents which are not in the e-office
     */
    public function detachDocuments($notification_report_id)
    {
        $notification_report = $this->notification_reports->find($notification_report_id);
        $documentsNotSynced = DB::Table('document_notification_report')->where('notification_report_id', $notification_report_id)->whereNull('eoffice_document_id')->get();
        foreach ($documentsNotSynced as $document) {
            $document_pivot_id = $document->id;
            DB::table('document_notification_report')->where('id',$document_pivot_id)->delete();

            }

    }


}




