<?php

namespace App\Services\Alert\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Access.
 */
class Alert extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'alert';
    }
}
