<?php

namespace App\Services\Payroll;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to auto calculate interest for contribution contributed late (exceed grace period)
 */
use App\Jobs\RunPayroll\CheckPayrollRunCompleteStatus;
use App\Jobs\RunPayroll\PostEoffice\PostBeneficiaryToDms;
use App\Jobs\RunPayroll\PostEoffice\PostPayrollToDms;
use App\Jobs\RunPayroll\ProcessDep;
use App\Jobs\RunPayroll\ProcessDependents;
use App\Jobs\RunPayroll\ProcessDependentsChildOverage;
use App\Jobs\RunPayroll\ProcessPensioners;
use App\Models\Operation\Compliance\Member\Dependent;
use App\Models\Operation\Payroll\PayrollArrear;
use App\Models\Operation\Payroll\Run\PayrollSuspendedRun;
use App\Repositories\Backend\Finance\BankBranchRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\Operation\Claim\ManualNotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollArrearRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollDeductionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\Backend\Operation\Payroll\Suspension\PayrollSystemSuspensionRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRecoveryTransactionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollSuspendedRunRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollUnclaimRepository;
use App\Repositories\Backend\Operation\Payroll\PensionerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProcessPayroll {
    /**
     * Services for calculating booking interest for each employer - Late contribution.
     *
     * @param string $model
     *
     * @return void
     */

    protected $pensioners;
    protected $dependents;
    protected $notification_reports;
    protected $payroll_procs;
    protected $payroll_runs;
    protected $payroll_recovery_trans;
    protected $code_values;
    protected $payroll_repo;
    protected $payroll_run_approvals;
    protected $payroll_suspended_runs;


    public function __construct()
    {
        $this->pensioners = new PensionerRepository();
        $this->dependents = new DependentRepository();
        $this->notification_reports = new NotificationReportRepository();
        $this->payroll_procs = new PayrollProcRepository();
        $this->payroll_runs = new PayrollRunRepository();
        $this->payroll_recovery_trans = new PayrollRecoveryTransactionRepository();
        $this->code_values = new CodeValueRepository();
        $this->payroll_repo = new PayrollRepository();
        $this->payroll_run_approvals = new PayrollRunApprovalRepository();
        $this->payroll_suspended_runs = new PayrollSuspendedRunRepository();
    }

    /**
     * Process Payroll
     */
    public function processPayroll($payroll_run_approval_id){
        return DB::transaction(function () use ($payroll_run_approval_id) {
            $run_date = $this->payroll_procs->getPayrollRunDate($payroll_run_approval_id);
            $months_paid = 1;
            /*Process Active and eligible only*/
            $this->processPensionersChunk($payroll_run_approval_id,$months_paid, $run_date,1);
            $this->processDependentsChunk($payroll_run_approval_id,$months_paid, $run_date,1);
            $this->processDependentsChildrenOverageJob($payroll_run_approval_id);

            /*Process Suspended */
            $this->processPensionersChunk($payroll_run_approval_id,$months_paid, $run_date,0);
            $this->processDependentsChunk($payroll_run_approval_id,$months_paid, $run_date,0);
            /*Check completion status*/
            $this->processPayrollCompletionStatus($payroll_run_approval_id);
        });
    }


    /**
     * @param $payroll_run_approval_id
     *  Calculate and check if payroll is complete
     */
    public function processPayrollCompletionStatus($payroll_run_approval_id)
    {
        dispatch(new CheckPayrollRunCompleteStatus($payroll_run_approval_id));
    }




    /**
     * @param $payroll_run_approval_id
     * @param $months_paid
     * @param $run_date
     * @param int $action_type i.e. 1 => Run all active, 2 = Run for suspended dependents
     */
    public function processDependentsChunk($payroll_run_approval_id,$months_paid, $run_date, $action_type = 1){

        if($action_type == 1){
            /*active*/
            $employees = $this->dependents->getEmployeesWithActiveDependentsForPayroll($run_date);
        }elseif($action_type == 0){
            /*Suspended*/
            $employees = $this->dependents->getEmployeesWithSuspendedDependentsForPayroll($run_date);
        }
//        $employees = $this->dependents->getEmployeesWithActiveDependentsForPayroll();
        $employees->chunk(100, function ($employees) use ($payroll_run_approval_id,$months_paid,$run_date,$action_type) {
            dispatch(new ProcessDependents($employees,$payroll_run_approval_id, $months_paid, $run_date, $action_type));
        });

    }

    /*process dependents */
    public function processDependents($employees, $payroll_run_approval_id,$months_paid,$run_date,$action_type ){
        DB::transaction(function () use ($employees, $payroll_run_approval_id,$months_paid,$run_date,$action_type) {
            $dependentRepo = new DependentRepository();
            $notificationReport = new NotificationReportRepository();
            $manualNotificationReport = new ManualNotificationReportRepository();
            $payroll_repo = new PayrollRepository();
            $member_type_id = 4; // dependent
            $months_paid_this_payroll = $months_paid;
            foreach ($employees as $employee) {
                /*Loop on all dependents for this employee who are eligible*/
                if($action_type == 1){
                    /*active*/
                    $dependents = $dependentRepo->getEligibleDependentsForPayroll($run_date)->where('dependent_employee.employee_id',
                        $employee->id)->get();
                }elseif($action_type == 0){
                    /*suspended*/
                    $dependents = $dependentRepo->getEligibleSuspendedDependentsForPayroll($run_date)->where('dependent_employee.employee_id',
                        $employee->id)->get();
                }

                foreach ($dependents as $dependent_with_pivot) {
                    /*Start parameters*/
                    $months_paid = $months_paid_this_payroll;
                    $ismanual = 0;
                    $notification_report_id = null;
                    $input = [];
                    $new_payee_flag = 0;
                    $firstpay_flag = $dependent_with_pivot->firstpay_flag;
                    $employee_id = $dependent_with_pivot->employee_id;
                    $dependent = $this->dependents->find($dependent_with_pivot->dependent_id);
                    $dependent_employee = $dependent->getDependentEmployee($employee_id);
                    $constant_care_flag = ($dependent_with_pivot->dependent_type_id == 7) ? 1 : 0;

                    $check_if_paid_on_system = 0;//Flag to check if paid on the system yet
                    $last_payroll_deduction = 0; //Last payroll deduction for other dependents with less than 1 month
                    $last_payroll_deduction_months = 0;//Last payroll deduction months for other dependents with less than 1 month
                    $deadline_date = $dependent_employee->deadline_date;
                    $check_deadline_date_eligible = true;
                    /*Check if passed deadline*/
                    if($deadline_date){
                        if(comparable_date_format( $deadline_date) < comparable_date_format($run_date)){
                            $check_deadline_date_eligible = false;
                        }
                    }

                    /*Process only if is deadline date eligible*/
                    if($check_deadline_date_eligible){
                        /*Get months paid for other dependents - full - check remained cycles*/
                        if ($dependent_employee->isotherdep == 1 && isset($dependent_employee->recycles_pay_period)) {
                            $recycles_pay_period = $dependent_employee->recycles_pay_period;//remained cycles for other dependents
                            /*check for last payroll deductions*/
//                            $last_payroll_deduction_months = ($months_paid < 1) ? (1 - $months_paid) : 0;
                            $last_payroll_deduction_months = ($recycles_pay_period < $months_paid) ? ($months_paid - $recycles_pay_period) : 0;
                            $months_paid = ($recycles_pay_period < $months_paid) ? $recycles_pay_period : $months_paid;

                        }
                        $total_months = $months_paid;
                        /*end parameters*/


                        /*Get start pay date*/
                        if (isset($dependent_with_pivot->notification_report_id)) {
                            $notification_report = $notificationReport->findOrThrowException($dependent_with_pivot->notification_report_id);
                            $notification_report_id = $notification_report->id;
                            if ($firstpay_flag == 0) {
                                $start_pay_date = $this->payroll_runs->getStartPayDate($notification_report);
                                $new_payee_flag = 1;
                            }
                        } elseif (isset($dependent_with_pivot->manual_notification_report_id)) {
                            /*Manual notification file*/
                            $ismanual = 1;
                            $manual_notification_report = $manualNotificationReport->find($dependent_with_pivot->manual_notification_report_id);
                            $check_if_firstpaid = $firstpay_flag;//Check if already paid on manual payroll

                            if ($check_if_firstpaid == 0) {
                                $start_pay_date = $this->payroll_runs->getStartPayDateManualFile($manual_notification_report);
                                $new_payee_flag = 1;
                            }
                        }
                        /*Get total months including arrears period - for first payee*/
                        if ($new_payee_flag == 1 && $firstpay_flag == 0) {
                            $total_months = $this->getFirstPayMonths($start_pay_date, $run_date);
                            $total_months = $payroll_repo->getFirstPayMonthsForDependents($dependent_employee, $total_months);
                        }

                        /*New payee flagCheck if is the first to be paid on the system - For manual files*/
                        $check_if_paid_on_system = $dependent->payrollRuns()->count();
                        if ($check_if_paid_on_system == 0) {
                            $new_payee_flag = 1;
                        }else{
                            /*Reset if already paid atleast once*/
                            $firstpay_flag = 0;
                            $total_months = $months_paid;
                            $new_payee_flag = 0;
                        }

                        /*Payments start*/
                        $survivor_pension = $dependent_with_pivot->survivor_pension_amount;
                        $mp_amount = (($dependent_with_pivot->survivor_pension_amount) * $total_months);
                        $last_payroll_deduction = $last_payroll_deduction_months *$survivor_pension;
                        $payments = $this->processPayments($member_type_id, $dependent_with_pivot->dependent_id, $payroll_run_approval_id, $mp_amount, $employee_id, $ismanual, $last_payroll_deduction);
                        $payable_months = $total_months + $payments['suspended_months'];
                        $bank_branch_id = $dependent->bank_branch_id;
                        $bank_id = isset($bank_branch_id) ? (new BankBranchRepository())->find($bank_branch_id)->bank_id : $dependent->bank_id;
//                    $bank_id = $dependent_with_pivot->bank_id
                        /*Payments end*/
                        $input = ['payroll_run_approval_id' => $payroll_run_approval_id, 'member_type_id' => $member_type_id, 'resource_id' => $dependent_with_pivot->dependent_id, 'amount' => $payments['net_payable_amount'], 'months_paid' => $payable_months, 'accountno' => ($dependent_with_pivot->accountno) ? $dependent_with_pivot->accountno : 0, 'bank_branch_id' => $bank_branch_id, 'bank_id' => $bank_id, 'monthly_pension' => $dependent_with_pivot->survivor_pension_amount, 'employee_id' => $dependent_with_pivot->employee_id, 'arrears_amount' => $payments['total_net_arrears'], 'deductions_amount' => $payments['total_deductions'], 'unclaimed_amount' => $payments['total_unclaims'], 'new_payee_flag' => $new_payee_flag, 'isconstantcare' => $constant_care_flag];


                        /*check if payable months are greater than 0*/
//                    if($payable_months > 0){
                        /*Save payroll run active / suspended*/
                        if ($action_type == 1) {
                            /*save payroll run for active dependents*/
                            $payroll_run = $this->payroll_runs->create($input);
                        } elseif ($action_type == 0) {
                            /*Save payroll run for suspended dependens*/
                            $payroll_suspended_run = $this->payroll_suspended_runs->create($input);
                        }
//                    }

                    }
                }
            }
        });

    }


    /**
     * @param $payroll_run_approval_id
     * @param $months_paid
     * @param $run_date
     * @param int $action_type i.e. 1 => Run all active, 2 = Run for suspended dependents
     */
    public function processDependentsChildrenOverageJob($payroll_run_approval_id){

        dispatch(new ProcessDependentsChildOverage($payroll_run_approval_id));

    }

    /**
     * @param $payroll_proc_id
     * @param $months_paid
     * Process pensioners for all active and suspended runs
     */
    public function processPensionersChunk($payroll_run_approval_id,$months_paid, $run_date, $action_type = 1){

        if($action_type == 1){
            /*active*/
            $pensioners = $this->pensioners->getEligibleForPayroll($run_date);
        }elseif($action_type == 0){
            /*Suspended*/
            $pensioners = $this->pensioners->getEligibleSuspendedForPayroll($run_date);
        }
        $pensioners->chunk(100, function ($pensioners) use ($payroll_run_approval_id,
            $months_paid,$run_date, $action_type) {
            dispatch(new ProcessPensioners($pensioners,$payroll_run_approval_id, $months_paid, $run_date, $action_type));
        });
    }


    /* process pensioners */
    public function processPensioners($pensioners, $payroll_run_approval_id,$months_paid, $run_date, $action_type){
        DB::transaction(function () use ($pensioners, $payroll_run_approval_id,$months_paid,$run_date, $action_type) {
            $member_type_id = 5; // pensioner
            $months_paid_this_payroll = $months_paid;
            foreach ($pensioners as $pensioner) {
                /*Start parameters*/
                $input = [];
                $months_paid = $months_paid_this_payroll;
                $ismanual = 0;
                $notification_report_id = null;
                $new_payee_flag = 0;
                $total_months = $months_paid;
                $check_if_paid_on_system = 0;
                /*end parameters*/
                if(isset($pensioner->notification_report_id)){
                    /*Notification report file*/
                    $notification_report = $this->notification_reports->findOrThrowException
                    ($pensioner->notification_report_id);
                    $notification_report_id = $notification_report->id;
                    if ($pensioner->firstpay_flag == 0) {
                        $start_pay_date = $this->payroll_runs->getStartPayDate($notification_report);
                        $total_months = $this->getFirstPayMonths($start_pay_date, $run_date);
                        $new_payee_flag = 1;
                    }
                }elseif(isset($pensioner->manual_notification_report_id)){
                    /*Manual notification file*/
                    $ismanual = 1;
                    $manual_notification_report = $pensioner->manualNotificationReport;
                    $check_if_firstpaid = $pensioner->firstpay_flag;//Check if already paid on manual payroll

                    if($check_if_firstpaid == 0){
                        $start_pay_date = $this->payroll_runs->getStartPayDateManualFile($manual_notification_report);
                        $total_months = $this->getFirstPayMonths($start_pay_date, $run_date);
                        $new_payee_flag = 1;
                    }
                }

                /*New Payee flag - Check if is the first to be paid on the system*/
                $check_if_paid_on_system = $pensioner->payrollRuns()->count();
                if($check_if_paid_on_system == 0)
                {
                    $new_payee_flag = 1;
                }else{
                    /*Reset if already paid atleast once*/
                    $firstpay_flag = 0;
                    $total_months = $months_paid;
                    $new_payee_flag = 0;
                }

                /*Payments start*/
                $mp_amount = (($pensioner->monthly_pension_amount) * $total_months);
                $payments = $this->processPayments($member_type_id,$pensioner->id,$payroll_run_approval_id, $mp_amount, $pensioner->employee_id, $ismanual);
                $payable_months = $total_months + $payments['suspended_months'];
                /*Payments end*/
                $bank_branch_id = $pensioner->bank_branch_id;
                $bank_id = isset($bank_branch_id) ? (new BankBranchRepository())->find($bank_branch_id)->bank_id : $pensioner->bank_id;
                $input = ['payroll_run_approval_id' => $payroll_run_approval_id, 'member_type_id' => $member_type_id, 'resource_id' => $pensioner->id, 'amount' => $payments['net_payable_amount'], 'months_paid' => $payable_months, 'accountno' => ($pensioner->accountno) ? $pensioner->accountno : 0, 'bank_branch_id' => $bank_branch_id, 'bank_id' => $bank_id, 'monthly_pension' => $pensioner->monthly_pension_amount, 'employee_id' => $pensioner->employee_id, 'arrears_amount' => $payments['total_net_arrears'], 'deductions_amount' => $payments['total_deductions'], 'unclaimed_amount' => $payments['total_unclaims'], 'new_payee_flag' => $new_payee_flag, 'isconstantcare' => 0];

                /*check if payable months are greater than 0*/
//                if($payable_months > 0){
//            Create Payroll Run
                if($action_type == 1){
                    /*Save run for active and eligible*/
                    $payroll_run = $this->payroll_runs->create($input);
                }elseif($action_type == 0){
                    /*Save run for suspended pensioners*/
                    $payroll_suspended_run = $this->payroll_suspended_runs->create($input);
                }

                // update recycle : Payment Type with period payment
                if ($pensioner->compensated_payment_type_id == 2) {
                    $this->updateRecycl($pensioner, $total_months);
                }
//                }
            }
        });
    }


    /**
     * @param $pensioner
     * @param $total_months
     * Update recycles
     */

    public function updateRecycl($pensioner, $total_months){
        $pensioner =   $pensioner->update(['recycl' => $pensioner->recycl - $total_months]);
        if ($pensioner->recycl < 1){
            $this->pensioners->deactivate($pensioner);
        }
    }


    /* Difference of Months for first pay beneficiary*/
    public function getFirstPayMonths($start_pay_date, $payroll_date)
    {
        $payroll_runs = new PayrollRunRepository();
        $months = $payroll_runs->getFirstPayMonthDiff($start_pay_date, $payroll_date);
        return round($months, 2, 1);
    }


    /**
     * Process Net Payment for Beneficiary -- start---------
     *
     */

    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $payroll_run_approval_id
     * @param $mp_amount => total mp including accumulation for first pay months
     * @param $ismanual - flag to imply f file is from manual notification ( 1 => Manual notification, 0 => Notification report)
     * Process payments
     */
    public function processPayments($member_type_id, $resource_id, $payroll_run_approval_id, $mp_amount, $employee_id, $ismanual = 0, $last_payroll_deduction = 0)
    {
        $resource = $this->payroll_repo->getResource($member_type_id, $resource_id);
        $recovery = $this->processRecovery($member_type_id,$resource_id, $payroll_run_approval_id, $employee_id);
        $suspended_amount = ($resource->suspense_flag == 3) ? $this->processSuspendedPayments($member_type_id, $resource_id,$employee_id)['suspended_payments'] : 0;
        $suspended_months = ($resource->suspense_flag == 3) ? $this->processSuspendedPayments($member_type_id, $resource_id,$employee_id)['suspended_months'] : 0;

        $net_payable_amount = $mp_amount + $recovery['net_recovery'] + $suspended_amount;
        $accumulation_arrears =   $this->processAccumulationArrears($member_type_id, $resource_id,$employee_id,$mp_amount);
//        $accumulation_arrears = ($ismanual == 0) ?  $this->processAccumulationArrears($member_type_id, $resource_id,$employee_id,$mp_amount) : 0; // only apply to payee processed start from system <not from manually notification>
        $total_net_arrears = $recovery['total_underpayment'] + $suspended_amount  + $accumulation_arrears; // Total arrears amount for this payroll i.e. underpayments and suspended payments and first pay accumulation arrears
        $total_deductions = $recovery['total_deductions'] + $last_payroll_deduction;
        $total_unclaimed = $recovery['total_unclaims'];

        return ['net_payable_amount' => $net_payable_amount, 'total_net_arrears' => $total_net_arrears, 'total_deductions' => $total_deductions, 'total_unclaims' => $total_unclaimed,  'suspended_months' => $suspended_months, 'firstpay_arrears' => $accumulation_arrears];
    }

    /*End processing Net payments --------end*/


    /**
     * Process Arrears in  Suspended payment -- start---
     */

    public function processSuspendedPayments($member_type_id, $resource_id, $employee_id)
    {
        $payroll_suspended_runs = new PayrollSuspendedRunRepository();
        $suspended_payments = $payroll_suspended_runs->pendingSuspendedPaymentsByBeneficiary($member_type_id, $resource_id, $employee_id)->sum('amount');
        $suspended_months = $payroll_suspended_runs->pendingSuspendedPaymentsByBeneficiary($member_type_id, $resource_id,$employee_id)->sum('months_paid');
        return ['suspended_payments'=> $suspended_payments, 'suspended_months' => $suspended_months];
    }

    /*---end 0f suspended payments ----*/

    /**
     * PROCESS PAYMENT RECOVERY ----Start-------------
     */

    /*Process net Recovery payment for beneficiary - find net amount to be added / deducted*/
    public function processRecovery($member_tye_id, $resource_id, $payroll_run_approval_id, $employee_id)
    {
        $resource = $this->payroll_repo->getResource($member_tye_id,$resource_id);
        $total_underpayment = ($resource->active_payroll_arrears_count > 0)  ?     $this->processArrears($member_tye_id,$resource_id,$payroll_run_approval_id, $employee_id) : 0;
        $total_unclaims = ($resource->active_payroll_unclaims_count > 0)  ?     $this->processUnclaims($member_tye_id,$resource_id,$payroll_run_approval_id,$employee_id) : 0;
        $total_deductions = ($resource->active_payroll_deductions_count > 0)  ?     $this->processDeductions($member_tye_id,$resource_id,$payroll_run_approval_id, $employee_id) : 0;
        $total_arrears = $total_underpayment + $total_unclaims; // total arrears i.e underpayment and unclaims on recovery payment
        $net_recovery = $total_arrears - $total_deductions;

        $recoveries = ['net_recovery' => $net_recovery, 'total_deductions' => $total_deductions,'total_underpayment' => $total_underpayment, 'total_arrears' => $total_arrears, 'total_unclaims' => $total_unclaims];

        return $recoveries;
    }

    /*--Process arrears for beneficiary if have pending underpayment */
    public function processArrears($member_type_id, $resource_id, $payroll_run_approval_id, $employee_id)
    {
//        $recovery_type_cv_id = $this->code_values->findByReference('PRTUNDP')->id;
        $payroll_arrears = new PayrollArrearRepository();
        $arrears_cv_refs_array = $payroll_arrears->getArrearsTypeReferencesArray();
        $arrears_cv_ids_array = $this->code_values->findIdsByReferences($arrears_cv_refs_array);
        $arrears = $payroll_arrears->queryForActiveArrears()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $arrears_cv_ids_array, $employee_id){
            $query->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->whereIn('recovery_type_cv_id', $arrears_cv_ids_array)->where('employee_id', $employee_id);
        })->select(['id','payroll_recovery_id', 'amount'])->get();
        $total_amount = 0;
        /*Loop on the arrears pending*/
        if(($arrears->isNotEmpty())){
            foreach($arrears as $arrear){
                $total_amount = $total_amount + $arrear->amount;
                /*Save into recovery transactions */
                $input = ['payroll_run_approval_id' => $payroll_run_approval_id, 'payroll_recovery_id' => $arrear->payroll_recovery_id, 'amount' => $arrear->amount];
                $this->payroll_recovery_trans->create($input);
            }
        }

        return $total_amount;
    }


    /*--Process unclaims for beneficiary if have pending unclaims */
    public function processUnclaims($member_type_id, $resource_id, $payroll_run_approval_id,$employee_id)
    {
        $recovery_type_cv_id = $this->code_values->findByReference('PRTUNCLP')->id;
        $payroll_unclaims = new PayrollUnclaimRepository();
        $unclaims = $payroll_unclaims->queryForActiveUnclaims()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $recovery_type_cv_id,$employee_id){
            $query->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->where('recovery_type_cv_id', $recovery_type_cv_id)->where('employee_id', $employee_id);
        })->select(['id','payroll_recovery_id', 'amount'])->get();;
        $total_amount = 0;
        /*Loop on the unclaims pending*/
        if(($unclaims->isNotEmpty())){
            foreach($unclaims as $unclaim){
                $total_amount = $total_amount + $unclaim->amount;
                /*Save into recovery transactions */
                $input = ['payroll_run_approval_id' => $payroll_run_approval_id, 'payroll_recovery_id' => $unclaim->payroll_recovery_id,'amount' => $unclaim->amount ];
                $this->payroll_recovery_trans->create($input);
            }
        }
        return $total_amount;
    }


    /*--Process deductions for beneficiary if have pending deductions */
    public function processDeductions($member_type_id, $resource_id, $payroll_run_approval_id, $employee_id )
    {
        $recovery_type_cv_id = $this->code_values->findByReference('PRTOVEP')->id;
        $payroll_deductions = new PayrollDeductionRepository();
        $deductions = $payroll_deductions->queryForActiveDeductions()->whereHas('payrollRecovery', function($query) use($member_type_id, $resource_id, $recovery_type_cv_id,$employee_id){
            $query->where('member_type_id',$member_type_id)->where('resource_id', $resource_id)->where('recovery_type_cv_id', $recovery_type_cv_id)->where('employee_id', $employee_id);
        })->select(['id','payroll_recovery_id', 'recycles', 'last_cycle_amount', 'amount'])->get();
        $total_amount = 0;
        /*Loop on the deductions pending*/
        if(($deductions->isNotEmpty())){
            foreach($deductions as $deduction){
                /*Check which amount per cycle to use i.e 1 => last_cycle_amount else use amount*/
                $deduction_amount = ($deduction->recycles == 1) ? $deduction->last_cycle_amount : $deduction->amount;
                $total_amount = $total_amount + $deduction_amount;
                /*Save into recovery transactions */
                $input = ['payroll_run_approval_id' => $payroll_run_approval_id, 'payroll_recovery_id' => $deduction->payroll_recovery_id, 'amount' => $deduction_amount];
                $this->payroll_recovery_trans->create($input);
            }
        }
        return $total_amount;
    }


    /**
     * @param $member_type_id
     * @param $resource_id
     * @param $notification_report_id
     * @param $net_payable_amount
     * Get accumulation arrears for first payee
     */
    public function processAccumulationArrears($member_type_id, $resource_id,$employee_id, $net_payable_mp)
    {
        $accumulation = 0;
        if($this->payroll_repo->checkIfIsTheFirstPayee($member_type_id,$resource_id,$employee_id)){
            $mp = $this->payroll_repo->getMp($member_type_id, $resource_id, $employee_id);
            $accumulation = ($net_payable_mp > $mp) ? ($net_payable_mp - $mp) : 0;
        }
        return $accumulation;
    }

    /**
     * Process Arrears for child overage (of now) but eligible at ime of death
     */
    public function processArrearsForChildOverageEligible($dependent_id, $employee_id, $payroll_run_approval_id)
    {
        $member_type_id = 4;
        $resource = $this->getResource($member_type_id, $dependent_id, $employee_id);
        $dependent_employee = $resource->getDependentEmployee($employee_id);
        $child_overage_data = (new PayrollRepository())->getOverageChildEligibleData($member_type_id, $dependent_id, $employee_id);
        $payable_months = $child_overage_data['first_pay_months'];
        $net_payable_amount = ($resource->active_payroll_arrears_count > 0)  ?  $this->processArrears($member_type_id,$dependent_id,$payroll_run_approval_id, $employee_id) : 0;
        if($net_payable_amount > 0){
            $input = ['payroll_run_approval_id' => $payroll_run_approval_id, 'member_type_id' => $member_type_id, 'resource_id' => $dependent_employee->dependent_id, 'amount' => $net_payable_amount, 'months_paid' => $payable_months, 'accountno' => ($resource->accountno) ? $resource->accountno : 0, 'bank_branch_id' =>$resource->bank_branch_id, 'bank_id' =>  $resource->bank_id, 'monthly_pension' => $dependent_employee->survivor_pension_amount,  'employee_id' => $employee_id , 'arrears_amount' => $net_payable_amount, 'deductions_amount' => 0,'unclaimed_amount' =>0, 'new_payee_flag' => 1, 'isconstantcare' => 0];

            $payroll_run = $this->payroll_runs->create($input);
        }

    }

    /*Get Beneficiary resource based on member type and resource*/
    public function getResource($member_type_id, $resource_id)
    {
        return $this->payroll_repo->getResource($member_type_id, $resource_id);
    }

    /* end --process payment recovery-----*/




    /**
     * PAYROLL APPROVAL WF COMPLETE ----------------Start ---------------
     *
     */
    /*Update after wf complete*/
    public function updateOnWfComplete($payroll_run_approval_id)
    {
        DB::transaction(function () use ( $payroll_run_approval_id) {
            /*update recovery*/
            $this->updateRecovery($payroll_run_approval_id);
            /*update suspended payment flag*/
            $this->updateSuspendedPayments($payroll_run_approval_id);
            /*update first pay flag*/
            $this->updateFirstPayFlag($payroll_run_approval_id);
            /*Update recycles remained for other dependents - full after payment*/
            $this->updateRecyclesForOtherDependentsFull($payroll_run_approval_id);

            /*Post payroll file into e-office afer wf_complete*/
            $this->postPayrollFileToDms($payroll_run_approval_id);
        });
    }
    /*end update after wf complete*/






    /* Update RECOVERY -------------start---------------*/

    /*update first pay flag for all first paid beneficiary on this payroll*/

    /*update first pay --start--*/
    public function updateFirstPayFlag($payroll_run_approval_id)
    {
        DB::transaction(function () use ( $payroll_run_approval_id) {
            $this->updatePensionersFirstFlag($payroll_run_approval_id);
            $this->updateDependentsFirstFlag($payroll_run_approval_id);
        });
    }
    /*pensioners*/
    public function updatePensionersFirstFlag($payroll_run_approval_id){
        DB::transaction(function () use ( $payroll_run_approval_id) {
            /*Eligible and Active*/
            $payroll_run_approval = $this->payroll_run_approvals->find($payroll_run_approval_id);
            $run_date = $payroll_run_approval->payrollProc->run_date;
            $pensioners_new_comers = $this->pensioners->getEligibleForPayroll($run_date)->where('firstpay_flag', 0)->get();
            foreach ($pensioners_new_comers as $new_comer) {
                $check_if_in_payroll = $this->payroll_runs->checkIfBeneficiaryHasPayrollRun(5, $new_comer->id, $new_comer->employee_id, $payroll_run_approval_id);
                if ($check_if_in_payroll) {
                    /*update first pay flag*/
                    $new_comer->update(['firstpay_flag' => 1]);
                }
            }
            /*Eligible and suspended*/
            $pensioners_new_comers_susp = $this->pensioners->getEligibleSuspendedForPayroll($run_date)->where('firstpay_flag', 0)->get();
            foreach ($pensioners_new_comers_susp as $new_comer_susp) {
                $check_if_in_payroll = $this->payroll_suspended_runs->checkIfBeneficiaryHasPayrollRun(5, $new_comer_susp->id, $new_comer_susp->employee_id, $payroll_run_approval_id);
                if ($check_if_in_payroll) {
                    /*update first pay flag*/
                    $new_comer_susp->update(['firstpay_flag' => 1]);
                }
            }
        });
    }

    /*dependents*/
    public function updateDependentsFirstFlag($payroll_run_approval_id){
        DB::transaction(function () use ( $payroll_run_approval_id) {
            /*Eligible and Active*/
            $dependent_new_comers = $this->dependents->getEligibleDependentsForPayroll()->where('firstpay_flag', 0)->get();
            foreach ($dependent_new_comers as $new_comer) {
                $check_if_in_payroll = $this->payroll_runs->checkIfBeneficiaryHasPayrollRun(4, $new_comer->dependent_id, $new_comer->employee_id, $payroll_run_approval_id);
                if ($check_if_in_payroll) {
                    /*update first pay flag*/
                    $this->dependents->updateFirstPayFlag($new_comer->dependent_id, $new_comer->employee_id, 1);
                }
            }
            /*Eligible and suspended*/
            $dependent_new_comers_susp = $this->dependents->getEligibleSuspendedDependentsForPayroll()->where('firstpay_flag', 0)->get();
            foreach ($dependent_new_comers_susp as $new_comer_susp) {
                $check_if_in_payroll = $this->payroll_suspended_runs->checkIfBeneficiaryHasPayrollRun(4, $new_comer_susp->dependent_id, $new_comer_susp->employee_id, $payroll_run_approval_id);
                if ($check_if_in_payroll) {
                    /*update first pay flag*/
                    $this->dependents->updateFirstPayFlag($new_comer_susp->dependent_id, $new_comer_susp->employee_id, 1);
                }
            }

            /*Eligible and Active - Child over age new comers*/
            $dependent_overage_new_comers = $this->dependents->getEligibleChildrenOverageForPayroll()->get();
            foreach ($dependent_overage_new_comers as $new_comer) {
                $check_if_in_payroll = $this->payroll_runs->checkIfBeneficiaryHasPayrollRun(4, $new_comer->dependent_id, $new_comer->employee_id, $payroll_run_approval_id);
                if ($check_if_in_payroll) {
                    /*update first pay flag*/
                    $this->dependents->updateFirstPayFlag($new_comer->dependent_id, $new_comer->employee_id, 1);
                }
            }

        });

    }

    /*end update first pay flag*/


    /*Update all payroll recoveries after wf complete*/

    public function updateRecovery($payroll_run_approval_id)
    {
        DB::transaction(function () use ( $payroll_run_approval_id) {
            /*update arrears*/
            $this->updateArrearsTransactions($payroll_run_approval_id);
            /*Update deductions*/
            $this->updateDeductionsTransactions($payroll_run_approval_id);
            /*Update unclaims*/
            $this->updateUnclaimsTransactions($payroll_run_approval_id);
            /*Update recovery trans paid flag*/
            $this->payroll_recovery_trans->updatePaidFlag($payroll_run_approval_id);

        });
    }




    /*update arrears transaction*/
    public function updateArrearsTransactions($payroll_run_approval_id)
    {
        $arrears_repo = new PayrollArrearRepository();
        $arrears_trans = $this->payroll_recovery_trans->getPendingArrearsForUpdate($payroll_run_approval_id);
        foreach($arrears_trans as $arrears_tran){
            $arrears_repo->updateRecycle($arrears_tran->payroll_recovery_id);
        }

    }

    /*update deductions transaction*/
    public function updateDeductionsTransactions($payroll_run_approval_id)
    {
        $deductions_repo = new PayrollDeductionRepository();
        $deduction_trans = $this->payroll_recovery_trans->getPendingDeductionsForUpdate($payroll_run_approval_id);
        foreach($deduction_trans as $deduction_tran){
            $deductions_repo->updateRecycle($deduction_tran->payroll_recovery_id);
        }

    }

    /*Update Unclaims transaction*/
    public function updateUnclaimsTransactions($payroll_run_approval_id)
    {
        $unclaims_repo = new PayrollUnclaimRepository();
        $unclaims_trans = $this->payroll_recovery_trans->getPendingUnclaimsForUpdate($payroll_run_approval_id);
        foreach($unclaims_trans as $unclaims_tran){
            $unclaims_repo->updateRecycle($unclaims_tran->payroll_recovery_id);
        }

    }
    /* ------update recovery -----------end-------*/



    /*--Update Suspended payments for Reinstated Beneficiary on this payroll ---on WF DONE-----start-------------------*/

    /*Update suspended payments for reinstated beneficiary*/
    public function updateSuspendedPayments($payroll_run_approval_id){
        DB::transaction(function () use ( $payroll_run_approval_id) {
            /*update pensioner*/

            $this->updatePensionerSuspendedPayments($payroll_run_approval_id);
            /*update dependents*/
            $this->updateDependentSuspendedPayments($payroll_run_approval_id);

        });
    }


    /*Update Reinstated pensioners*/
    public function updatePensionerSuspendedPayments($payroll_run_approval_id)
    {
//        $reinstated_pensioners = $this->pensioners->getReinstatedForPayroll()->get();
        $pensioners_with_arrears = $this->payroll_runs->getPayeesWithArrearsByMemberType($payroll_run_approval_id, 5);
        foreach($pensioners_with_arrears as $pensioners_with_arrear)
        {
            $reinstated_pensioner = $this->payroll_repo->getResource($pensioners_with_arrear->member_type_id, $pensioners_with_arrear->resource_id);
            $pensioner_id = $reinstated_pensioner->id;
            /*update*/

            if($this->payroll_runs->checkIfBeneficiaryHasPayrollRun(5,$reinstated_pensioner->id,$reinstated_pensioner->employee_id,$payroll_run_approval_id)){

                $this->payroll_suspended_runs->updatePaidStatusAfterWfDone(5,$reinstated_pensioner->id, $payroll_run_approval_id);
                /*Update suspense flag*/
                $resource = $this->payroll_repo->getResource(5,$pensioner_id);
                $this->pensioners->updateSuspenseFlagAfterReinstatedPayroll($resource);
            };
        }
    }


    /*Update Reinstated dependents*/
    public function updateDependentSuspendedPayments($payroll_run_approval_id)
    {
//        $reinstated_dependents = $this->dependents->getReinstatedForPayroll()->get();
        $dependents_with_arrears = $this->payroll_runs->getPayeesWithArrearsByMemberType($payroll_run_approval_id, 4);

        foreach($dependents_with_arrears as $dependents_with_arrear)
        {
            $reinstated_dependent = $this->payroll_repo->getResource($dependents_with_arrear->member_type_id, $dependents_with_arrear->resource_id);
            $dependent_id = $reinstated_dependent->id;
            $employee_id = $dependents_with_arrear->employee_id;
            /*update*/
            if($this->payroll_runs->checkIfBeneficiaryHasPayrollRun(4,$dependent_id,$employee_id,$payroll_run_approval_id)){
                $this->payroll_suspended_runs->updatePaidStatusAfterWfDone(4,$dependent_id, $payroll_run_approval_id);
                /*Update suspense flag*/
                $resource = $this->payroll_repo->getResource(4,$dependent_id);
                $this->dependents->updateSuspenseFlagAfterReinstatedPayroll($resource);
            };
        }
    }


    /*------end suspended payments --------end-------------*/


    /**
     * ----Update recycles for other dependents for  Full dependency---
     */
    public function updateRecyclesForOtherDependentsFull($payroll_run_approval_id)
    {
        /*Payroll runs*/
        $pay_other_dependents = DB::table('dependent_employee as d')->select('d.dependent_id', 'd.employee_id', 'd.recycles_pay_period', 'd.isactive','p.months_paid')
            ->join('payroll_runs as p', function($join){
                $join->on('p.resource_id', 'd.dependent_id')->where('p.member_type_id', 4);
            })->where('p.payroll_run_approval_id', $payroll_run_approval_id)->where('d.isotherdep', 1)->get();

        /*Update for active run*/
        $this->updateRecyclesOnFromActiveAndSuspendedOtherdep($pay_other_dependents, $payroll_run_approval_id);

        /*Suspended Runs*/
        $pay_suspended_other_dependents = DB::table('dependent_employee as d')->select('d.dependent_id', 'd.employee_id', 'd.recycles_pay_period', 'd.isactive','p.months_paid')
            ->join('payroll_suspended_runs as p', function($join){
                $join->on('p.resource_id', 'd.dependent_id')->where('p.member_type_id', 4);
            })->where('p.payroll_run_approval_id', $payroll_run_approval_id)->where('d.isotherdep', 1)->get();

        /*Update for suspended run*/
        $this->updateRecyclesOnFromActiveAndSuspendedOtherdep($pay_suspended_other_dependents, $payroll_run_approval_id, 1);


    }

    /*Loop on collection of other dep from active and suspended runs*/
    public function updateRecyclesOnFromActiveAndSuspendedOtherdep($pay_other_dependents, $payroll_run_approval_id, $issuspended = 0)
    {
        foreach ($pay_other_dependents as $pay_other_dependent){
            $dependent_id = $pay_other_dependent->dependent_id;
            $dependent = Dependent::query()->find($dependent_id);
            $employee_id = $pay_other_dependent->employee_id;
            $current_recycles_pay_period = $pay_other_dependent->recycles_pay_period;
            $months_paid = $pay_other_dependent->months_paid;
            $new_recycles_pay_period = 0;
            $isactive = $pay_other_dependent->isactive;
            $get_suspended_months = PayrollSuspendedRun::query()->where('member_type_id', 4)->where('resource_id', $dependent_id)->where('paid_payroll_run_approval_id', $payroll_run_approval_id)->sum('months_paid');
            $suspended_months = ($issuspended == 0) ? $get_suspended_months : 0;
            $months_paid = $months_paid - $suspended_months;
            /*Get new recycles remained*/
            if(isset($current_recycles_pay_period)){
                $new_recycles_pay_period = $current_recycles_pay_period - $months_paid;
                $new_recycles_pay_period = ($new_recycles_pay_period >= 0) ? $new_recycles_pay_period : 0;
            }

            /*check active status after this payment*/
            if($new_recycles_pay_period == 0){
                /*check if has pending suspended payments*/
                $check_suspended_payments = (new PayrollSuspendedRunRepository())->pendingSuspendedPaymentsByBeneficiary(4,$dependent_id, $employee_id)->count();
                if($check_suspended_payments == 0){
                    /*           $isactive = 2;*/
                    $suspend_reason = 'Have already completed cycles for payable months';
                    (new PayrollStatusChangeRepository())->suspend($dependent, $suspend_reason);
                }
            }

            /*Update flag and recycles of dependent employee*/
            DB::table('dependent_employee')->where('dependent_id', $dependent_id)->where('employee_id', $employee_id)->where('isotherdep', 1)->update([
                'recycles_pay_period' => $new_recycles_pay_period,
                'isactive' => $isactive
            ]);

        }
    }
    /*---Full dependency update recycles -----*/

    /*Suspend all constant care taker after Payroll processing-- start--*/
    public function suspendConstantCareAssistants()
    {
        $payroll_system_suspensions = new PayrollSystemSuspensionRepository();
        $assistants =$this->dependents->query()->whereHas('employees', function($query){
            $query->where('dependent_type_id', 7)->where('isactive', 1);
        })->get();
        foreach($assistants as $assistant){
            $payroll_system_suspensions->suspend(4, $assistant->id);
        }

    }

    /*Suspend all constant care taker ---end--*/






    /**
     * @param $payroll_run_approval_id
     * On workflow complete - post payroll file to dms - eoffice
     */
    public function postPayrollFileToDms($payroll_run_approval_id)
    {
        $payroll_run_approval = $this->payroll_run_approvals->find($payroll_run_approval_id);
        dispatch(new PostPayrollToDms($payroll_run_approval));
    }


    /*---Payroll approval wf complete -------------End ---------------------------------------------*/





}