<?php

namespace App\Services\System\Database;

use App\Exceptions\WorkflowException;
use Illuminate\Support\Facades\DB;

/**
 * Class CheckForeignReference
 * @package App\Services\System\Database
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 * @description Check whether the attribute of a specified table has been used in any of the referencing child table.
 * This check aid in restricting deletion of parent data rows referenced by child data rows
 */
class CheckForeignReference
{
    /**
     * @var
     */
    public $table;

    /**
     * @var
     * Column name
     */
    public $attribute;

    /**
     * @var
     */
    public $value;

    /**
     * CheckForeignReference constructor.
     * @param $table
     * @param $attribute
     * @param $value
     */
    public function __construct($table, $attribute, $value)
    {
        $this->table = $table;
        $this->attribute = $attribute;
        $this->value = $value;
    }

    /**
     * @throws WorkflowException
     */
    public function __invoke()
    {
        $check = DB::select("select * from check_foreign_reference(?, ?, ?)", [$this->table, $this->attribute, $this->value]);
        $status = $check[0]->status;
        if ($status) {
            throw new WorkflowException(trans('Unable delete this entry, it has been referred somewhere else'));
        }
    }

}