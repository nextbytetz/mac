<?php

namespace App\Services\System;

use App\Models\Sysdef\Sysdef;

class System
{

    public function __invoke()
    {
        return Sysdef::find(1)->first();
    }

    public function data()
    {
        return Sysdef::find(1)->first();
    }

}