<?php

namespace App\Services\System\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class System
 * @package App\Services\System\Facades
 */
class System extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'system';
    }
}