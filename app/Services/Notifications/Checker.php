<?php

namespace App\Services\Notifications;

/**
 * Class Checker
 * @package App\Services\Notifications
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 * @description Manages the check inbox and tasks specified to a user.
 */
class Checker
{

    public function __construct()
    {
    }

}