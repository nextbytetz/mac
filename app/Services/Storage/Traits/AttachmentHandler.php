<?php

namespace App\Services\Storage\Traits;

use App\Exceptions\GeneralException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait AttachmentHandler {

    protected function saveContributionAttachment($receipt_code) {
        if (request()->hasFile('excel')) {
            $file = request()->file('excel');
            if ($file->isValid()) {
                $file->move(contribution_dir(), 1 . "." . $file->getClientOriginalExtension());
            }
        }
    }

    /**
     * @param $extension
     * @return mixed
     * @throws GeneralException
     */
    protected function checkExcel($extension) {
        if ($extension == 'xls'|| $extension == 'xlsx') {
            return $extension; // Allowed proceed
        }
        throw new GeneralException("error: " . trans('exceptions.backend.finance.receipt_codes.linked_file_not_allowed'));
    }


    /**
     * @param $receipt_code
     * @return bool
     * @throws GeneralException
     */
    public function saveLinkedFileAttachment($receipt_code)
    {
        if (request()->hasFile('linked_file')) {
            $file = request()->file('linked_file');
            if ($file->isValid()) {
                //Check the excel file
                $this->checkExcel($file->getClientOriginalExtension());
                $receipt_code->linked_file = $receipt_code->id . "." . $file->getClientOriginalExtension();
                $receipt_code->size = $file->getSize();
                $receipt_code->mime = $file->getMimeType();
                $receipt_code->rows_imported = 0;
                $receipt_code->total_rows = 0;
                $receipt_code->amount_imported = 0;
                $receipt_code->upload_error = NULL;
                $receipt_code->save();
                $file->move(contribution_dir() . DIRECTORY_SEPARATOR . $receipt_code->receipt->id . DIRECTORY_SEPARATOR, $receipt_code->id . "." . $file->getClientOriginalExtension());

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function saveManualContributionFileAttachment()
    {
        if (request()->hasFile('document_file49')) {
            $file = request()->file('document_file49');
            if ($file->isValid()) {
                //Check the excel file
                $file->move(contribution_dir() . DIRECTORY_SEPARATOR . 'manual_contribution' . DIRECTORY_SEPARATOR, 'file');

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $employer_id
     * @return bool
     */
    public function saveManualBookingGroupFileAttachment($employer_id)
    {
        if (request()->hasFile('upload_manual_post_file')) {
            $file = request()->file('upload_manual_post_file');
            if ($file->isValid()) {
                //Check the excel file
                $file->move(contribution_dir() . DIRECTORY_SEPARATOR . 'manual_contribution' . DIRECTORY_SEPARATOR . 'booking_group' . DIRECTORY_SEPARATOR, $employer_id);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public function saveMergeEmployerFileAttachment()
    {
        $filename = "";
        if (request()->hasFile('document_file50')) {
            $file = request()->file('document_file50');
            if ($file->isValid()) {
                //Check the excel file
                $filename = "file." . $file->getClientOriginalExtension();
                $file->move(employer_registration_dir() . DIRECTORY_SEPARATOR . 'merge_employer' . DIRECTORY_SEPARATOR , $filename);
                $return = true;
            } else {
                $return = false;
            }
        } else {
            $return = false;
        }
        return [
            'return' => $return,
            'filename' => $filename,
        ];
    }

    public function saveEmployerAssessmentSchedule($id)
    {
        if (request()->hasFile('document_file78')) {
            $file = request()->file('document_file78');
            if ($file->isValid()) {
                //Check the excel file
                $file->move(inspection_contribution_analysis_dir() . DIRECTORY_SEPARATOR  , $id);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $filename
     * @param $path
     * @param $fileindex
     * @return bool
     */
    public function saveDocumentGeneric($filename, $path, $fileindex)
    {
        if (request()->hasFile($fileindex)) {
            $file = request()->file($fileindex);
            if ($file->isValid()) {
                //Check the excel file
                $file->move($path . DIRECTORY_SEPARATOR  , $filename);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public function saveUnMergeEmployerFileAttachment()
    {
        $filename = "";
        if (request()->hasFile('document_file51')) {
            $file = request()->file('document_file51');
            if ($file->isValid()) {
                //Check the excel file
                $filename = "file." . $file->getClientOriginalExtension();
                $file->move(employer_registration_dir() . DIRECTORY_SEPARATOR . 'unmerge_employer' . DIRECTORY_SEPARATOR , $filename);
                $return = true;
            } else {
                $return = false;
            }
        } else {
            $return = false;
        }
        return [
            'return' => $return,
            'filename' => $filename,
        ];
    }

    /**
     * @param $legacy_receipt_code
     * @return bool
     * @throws GeneralException
     */
    public function saveLinkedFileLegacyAttachment($legacy_receipt_code)
    {
        if (request()->hasFile('linked_file')) {
            $file = request()->file('linked_file');
            if ($file->isValid()) {
                //Check the excel file
                $this->checkExcel($file->getClientOriginalExtension());
                $legacy_receipt_code->linked_file = $legacy_receipt_code->id . "." . $file->getClientOriginalExtension();
                $legacy_receipt_code->size = $file->getSize();
                $legacy_receipt_code->mime = $file->getMimeType();
                $legacy_receipt_code->rows_imported = 0;
                $legacy_receipt_code->total_rows = 0;
                $legacy_receipt_code->amount_imported = 0;
                $legacy_receipt_code->upload_error = NULL;
                $legacy_receipt_code->save();
                $file->move(contribution_legacy_dir() . DIRECTORY_SEPARATOR . $legacy_receipt_code->legacyReceipt->id . DIRECTORY_SEPARATOR, $legacy_receipt_code->id . "." . $file->getClientOriginalExtension());

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $employer_inspection_task
     * @return bool
     */
    public function saveInspectionContributionAnalysis($employer_inspection_task)
    {
        if (request()->hasFile('contribution_file')) {
            $file = request()->file('contribution_file');
            if ($file->isValid()) {
                $file->move(inspection_contribution_analysis_dir() . DIRECTORY_SEPARATOR , $employer_inspection_task->id  . "." . $file->getClientOriginalExtension());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $closure_id
     * @return bool
     */
    public function saveEmployerClosureDocument($closure_id)
    {
        if (request()->hasFile('employer_close_file')) {
            $employer_id = request()->input("employer_id");
            $file = request()->file('employer_close_file');
            if ($file->isValid()) {
                $path = employer_closure_dir() . DIRECTORY_SEPARATOR . $employer_id . DIRECTORY_SEPARATOR;
                $this->makeDirectory($path);
                $file->move($path , $closure_id  . "." . $file->getClientOriginalExtension());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $closure_id
     * @return bool
     */
    public function saveDishonourSupportingDocument($dishonoured_cheque_id)
    {
        if (request()->hasFile('dishonour_supporting_document')) {
            $receipt_id = request()->input("receipt_id");
            $file = request()->file('dishonour_supporting_document');
            if ($file->isValid()) {
                $path = dishonour_receipt_dir() . DIRECTORY_SEPARATOR . $receipt_id . DIRECTORY_SEPARATOR;
                $this->makeDirectory($path);
                $file->move($path , $dishonoured_cheque_id  . "." . $file->getClientOriginalExtension());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $notificationReport
     * @param $path
     * @return bool
     */
    public function saveNotificationReportDocument(Model $notificationReport, $path)
    {
        $uploadedDocument = $notificationReport->documents()->where("document_id", request()->input("document_id"))->first()->pivot;

        if (request()->hasFile('document_file')) {
            $file = request()->file('document_file');
            if ($file->isValid()) {
                $uploadedDocument->name = $file->getClientOriginalName();
                $uploadedDocument->description = $file->getClientOriginalName();
                $uploadedDocument->size = $file->getSize();
                $uploadedDocument->mime = $file->getMimeType();
                $uploadedDocument->ext = $file->getClientOriginalExtension();
                $uploadedDocument->save();
                $file->move($path , $uploadedDocument->id  . "." . $file->getClientOriginalExtension());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param Model $incident
     * @return bool
     */
    public function saveInvestigationDocument(Model $incident)
    {
        $return = 0;
        $docId = 52; //WCF Investigation Report
        if (request()->hasFile('images')) {

            $path = investigation_dir();
            //Clear All Saved Pictures
            //$incident->investigationDocuments()->sync([]);
            //Delete Current Documents
            /*$docs = $incident->dirInvestigationDocuments();
            foreach ($docs as $doc) {
                $this->deleteDocument($doc);
            }*/
            $index = request()->input('index');
            $images = request()->file('images');
            foreach ($images as $image) {
                if ($image->isValid()) {
                    $data = [
                        'name' => $image->getClientOriginalName(),
                        'description' => $image->getClientOriginalName(),
                        'size' => $image->getSize(),
                        'mime' => $image->getMimeType(),
                        'ext' => $image->getClientOriginalExtension(),
                        'index' => $index
                    ];
                    $incident->investigationDocuments()->attach([
                        $docId => $data
                    ]);
                    $id = $incident->investigationDocuments()->wherePivot('notification_report_id', $incident->id)->wherePivot('index', $index)->orderByDesc("document_investigation.id")->limit(1)->first()->pivot->id;
                    $image->move($path , $id  . "." . $image->getClientOriginalExtension());
                    $return = $id;
                } else {
                    $return = 0;
                }
            }

        } else {
            $return = 0;
        }
        return $return;
    }

    /**
     * @param Model $model
     * @param $document_file_name
     * @param $path
     * @param $uploadedDocument
     * @return bool
     */
    public function saveDocument(Model $model, $document_file_name ,$path, $uploadedDocument)
    {
        if (request()->hasFile($document_file_name)) {
            $file = request()->file($document_file_name);
            if ($file->isValid()) {
                $uploadedDocument->name = $file->getClientOriginalName();
                $uploadedDocument->size = $file->getSize();
                $uploadedDocument->mime = $file->getMimeType();
                $uploadedDocument->ext = $file->getClientOriginalExtension();
                $uploadedDocument->save();
                $this->makeDocumentDirectory($model);
                $file->move($path , $uploadedDocument->document_id  . "." . $file->getClientOriginalExtension());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return bool
     * @throws GeneralException
     */
    public function saveUnregisteredEmployerBulk()
    {
        if (request()->hasFile('linked_file')) {
            $file = request()->file('linked_file');
            if ($file->isValid()) {
                //Check the excel file
                $this->checkExcel($file->getClientOriginalExtension());
                $file_name = Carbon::parse('now')->format('M') . '_' . Carbon::parse('now')->format('Y');
                $file->move(unregistered_employer_bulk_dir() . DIRECTORY_SEPARATOR, $file_name .  "." . $file->getClientOriginalExtension());

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $linked_file_input
     * @param $file_name
     * @param $base_dir
     * @return bool
     * @throws GeneralException
     */
    public function saveDocumentGeneral($linked_file_input, $file_name, $base_dir)
    {
        if (request()->hasFile($linked_file_input)) {
            $file = request()->file($linked_file_input);
            if ($file->isValid()) {
                //Check the excel file
                $this->checkExcel($file->getClientOriginalExtension());
                $file->move($base_dir, $file_name);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }



    /**
     * @param $linked_file_input
     * @param $file_name
     * @param $base_dir
     * @return bool
     * @throws GeneralException
     * Save to specified path and filename for all type of documents
     */
    public function saveDocumentBasic($linked_file_input, $file_name, $base_dir)
    {
        if (request()->hasFile($linked_file_input)) {
            $file = request()->file($linked_file_input);
            if ($file->isValid()) {

                $file->move($base_dir, $file_name);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }



    /**
     * @param $filename
     */
    public function deleteDocument($filename){
        //Storage::delete($filename);
        try {
            unlink($filename);
        } catch (\Exception $e) {

        }
    }

    /**
     * @param $directory_name
     */
    public function deleteDirectory($directory_name){
        Storage::deleteDirectory($directory_name);
    }

    /**
     * @param $path
     */
    public function deleteAllFilesOnFolder($path)
    {
        $folder = $path;
        $files = glob($folder . '/*');
        foreach($files as $file){
            //Make sure that this is a file and not a directory.
            if(is_file($file)){
                //Use the unlink function to delete the file.
                unlink($file);
            }
        }
    }

    /*Get doc extension*/
    public function getDocExtension($file_name_input)
    {
        $file = request()->file($file_name_input);
        if($file->isValid()) {
            return $file->getClientOriginalExtension();
        }else{
            return null;
        }
    }


    /**
     * @param $incident NotificationReport id
     * @param $documentType
     * @param $eOfficeDocumentId
     * @return mixed
     * Return base 64 pdf from eoffice
     */
    public function getBase64EofficeDocumentGeneral($file_number, $document_type, $eoffice_document_id)
    {
        $client = new Client(['defaults' => [
            'verify' => false
        ]]);
        $link = env('EOFFICE_APP_URL');
        $guzzlerequest = $client->get($link . "/api/claim/file/document?fileNumber={$file_number}&documentType={$document_type}&documentId={$eoffice_document_id}", [
            'auth' => [
                env("EOFFICE_AUTH_USER"),
                env("EOFFICE_AUTH_PASS"),
            ],
            'verify' => false,
        ]);
        $response = $guzzlerequest->getBody()->getContents();
        $parsed_json = json_decode($response, true);
        $base64pdf = $parsed_json['document'];

        return $base64pdf;
    }

    /*General method for previewing doc from eoffice*/
    public function previewDocumentFromEofficeGeneral($file_number, $document_type, $eoffice_document_id)
    {
        $base64doc = $this->getBase64EofficeDocumentGeneral($file_number, $document_type, $eoffice_document_id);
        $base64decoded = base64_decode($base64doc);
        $document = $eoffice_document_id . '.pdf';
        $path = eoffice_dir() . DIRECTORY_SEPARATOR . $document_type;
        $this->makeDirectory($path);
        $file = $path .  DIRECTORY_SEPARATOR . $document;
        file_put_contents($file, $base64decoded);
        $url = eoffice_path() . DIRECTORY_SEPARATOR . $document_type . DIRECTORY_SEPARATOR . $document;
        return $url;
    }


    public function getSampleBase64Pdf()
    {
        $return = "JVBERi0xLjMKJf////8KNiAwIG9iago8PAovVHlwZSAvRXh0R1N0YXRlCi9jYSAxCj4+CmVuZG9iago1IDAgb2JqCjw8Ci9UeXBlIC9QYWdlCi9QYXJlbnQgMSAwIFIKL01lZGlhQm94IFswIDAgNTk1LjI4IDg0MS44OV0KL0NvbnRlbnRzIDMgMCBSCi9SZXNvdXJjZXMgNCAwIFIKPj4KZW5kb2JqCjQgMCBvYmoKPDwKL1Byb2NTZXQgWy9QREYgL1RleHQgL0ltYWdlQiAvSW1hZ2VDIC9JbWFnZUldCi9FeHRHU3RhdGUgPDwKL0dzMSA2IDAgUgo+PgovRm9udCA8PAovRjEgNyAwIFIKL0YyIDggMCBSCj4+Cj4+CmVuZG9iagoxMSAwIG9iago8PAovVHlwZSAvUGFnZQovUGFyZW50IDEgMCBSCi9NZWRpYUJveCBbMCAwIDU5NS4yOCA4NDEuODldCi9Db250ZW50cyA5IDAgUgovUmVzb3VyY2VzIDEwIDAgUgo+PgplbmRvYmoKMTAgMCBvYmoKPDwKL1Byb2NTZXQgWy9QREYgL1RleHQgL0ltYWdlQiAvSW1hZ2VDIC9JbWFnZUldCi9FeHRHU3RhdGUgPDwKL0dzMSA2IDAgUgo+PgovRm9udCA8PAovRjIgOCAwIFIKL0YxIDcgMCBSCj4+Cj4+CmVuZG9iagoxMiAwIG9iago8PAovUHJvZHVjZXIgKHBkZm1ha2UpCi9DcmVhdG9yIChwZGZtYWtlKQovQ3JlYXRpb25EYXRlIChEOjIwMTcxMjE3MTI0NTE2WikKPj4KZW5kb2JqCjE0IDAgb2JqCjw8Ci9UeXBlIC9Gb250RGVzY3JpcHRvcgovRm9udE5hbWUgL0lDVU5LVStSb2JvdG8tUmVndWxhcgovRmxhZ3MgNAovRm9udEJCb3ggWy03MzYuODE2NDA2IC0yNzAuOTk2MDk0IDExNDguNDM3NSAxMDU2LjE1MjM0NF0KL0l0YWxpY0FuZ2xlIDAKL0FzY2VudCA5MjcuNzM0Mzc1Ci9EZXNjZW50IC0yNDQuMTQwNjI1Ci9DYXBIZWlnaHQgNzEwLjkzNzUKL1hIZWlnaHQgNTI4LjMyMDMxMwovU3RlbVYgMAovRm9udEZpbGUyIDEzIDAgUgo+PgplbmRvYmoKMTUgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL0NJREZvbnRUeXBlMgovQmFzZUZvbnQgL0lDVU5LVStSb2JvdG8tUmVndWxhcgovQ0lEU3lzdGVtSW5mbyA8PAovUmVnaXN0cnkgKEFkb2JlKQovT3JkZXJpbmcgKElkZW50aXR5KQovU3VwcGxlbWVudCAwCj4+Ci9Gb250RGVzY3JpcHRvciAxNCAwIFIKL1cgWzAgWzkwOCA4NzMuMDQ2ODc1IDQ3My4xNDQ1MzEgMjQ3LjU1ODU5NCAzMjYuNjYwMTU2IDI0Mi42NzU3ODEgMjQyLjY3NTc4MSA1MjkuNzg1MTU2IDU5Ni42Nzk2ODggNTUwLjc4MTI1IDU1MS43NTc4MTMgMzQ3LjE2Nzk2OSA1NzAuMzEyNSAzMzguMzc4OTA2IDg3Ni40NjQ4NDQgNTQzLjk0NTMxMyA1MTUuNjI1IDU2MS4wMzUxNTYgNTIzLjQzNzUgNTYxLjAzNTE1NiA1NjEuMDM1MTU2IDU5My4yNjE3MTkgNTUxLjI2OTUzMSA2NTAuODc4OTA2IDI2My4xODM1OTQgNjUyLjM0Mzc1IDUwNi44MzU5MzggNTYxLjUyMzQzOCA1NjEuNTIzNDM4IDU2MS41MjM0MzggNTYxLjUyMzQzOCA0MTIuMTA5Mzc1IDU2MS41MjM0MzggNTYxLjUyMzQzOCA1NjEuNTIzNDM4IDE5Ni4yODkwNjMgNTYxLjUyMzQzOCA2MTUuNzIyNjU2IDU2OC4zNTkzNzUgNDk1LjYwNTQ2OSA0ODQuMzc1IDY4Ny41IDU1My43MTA5MzggMzQxLjc5Njg3NSAzNDcuNjU2MjUgNTM4LjA4NTkzOCA1NjMuOTY0ODQ0IDU2MS41MjM0MzggNTYxLjUyMzQzOCA1NTEuNzU3ODEzIDU1Mi43MzQzNzUgNjIyLjU1ODU5NCA2ODEuMTUyMzQ0IDc1MS40NjQ4NDQgODg3LjIwNzAzMSA1NjEuNTIzNDM4IDI3MS45NzI2NTYgNzEyLjg5MDYyNSA2MDAuNTg1OTM4IDYzNi4yMzA0NjkgNjMwLjg1OTM3NSAyNzUuODc4OTA2IDYyNi45NTMxMjUgNjU1Ljc2MTcxOSA3MTIuODkwNjI1IDQ5NS42MDU0NjkgNjg3LjUgNjQ4LjQzNzUgNTk4LjYzMjgxMyAxNzQuMzE2NDA2XV0KPj4KZW5kb2JqCjcgMCBvYmoKPDwKL1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUwCi9CYXNlRm9udCAvSUNVTktVK1JvYm90by1SZWd1bGFyCi9FbmNvZGluZyAvSWRlbnRpdHktSAovRGVzY2VuZGFudEZvbnRzIFsxNSAwIFJdCi9Ub1VuaWNvZGUgMTYgMCBSCj4+CmVuZG9iagoxOCAwIG9iago8PAovVHlwZSAvRm9udERlc2NyaXB0b3IKL0ZvbnROYW1lIC9FT1JOU1orUm9ib3RvLU1lZGl1bQovRmxhZ3MgNAovRm9udEJCb3ggWy03MzIuNDIxODc1IC0yNzAuOTk2MDk0IDExNjkuOTIxODc1IDEwNTYuMTUyMzQ0XQovSXRhbGljQW5nbGUgMAovQXNjZW50IDkyNy43MzQzNzUKL0Rlc2NlbnQgLTI0NC4xNDA2MjUKL0NhcEhlaWdodCA3MTAuOTM3NQovWEhlaWdodCA1MjguMzIwMzEzCi9TdGVtViAwCi9Gb250RmlsZTIgMTcgMCBSCj4+CmVuZG9iagoxOSAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvQ0lERm9udFR5cGUyCi9CYXNlRm9udCAvRU9STlNaK1JvYm90by1NZWRpdW0KL0NJRFN5c3RlbUluZm8gPDwKL1JlZ2lzdHJ5IChBZG9iZSkKL09yZGVyaW5nIChJZGVudGl0eSkKL1N1cHBsZW1lbnQgMAo+PgovRm9udERlc2NyaXB0b3IgMTggMCBSCi9XIFswIFs5MDggNzA5Ljk2MDkzOCA1NDEuMDE1NjI1IDg3MC4xMTcxODggNTM2LjYyMTA5NCA2MzkuMTYwMTU2IDU2OS4zMzU5MzggNTE2LjExMzI4MSAyNTUuMzcxMDk0IDMzMi41MTk1MzEgNTU2LjE1MjM0NCA2OTAuNDI5Njg4IDM1NC40OTIxODggNTgyLjAzMTI1IDUyMy40Mzc1IDY2NS41MjczNDQgNTY2Ljg5NDUzMSA2MDMuNTE1NjI1IDM1MS41NjI1IDI0OS4wMjM0MzggNTY0LjQ1MzEyNSAyNTUuMzcxMDk0IDQ4Ni44MTY0MDZdXQo+PgplbmRvYmoKOCAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTAKL0Jhc2VGb250IC9FT1JOU1orUm9ib3RvLU1lZGl1bQovRW5jb2RpbmcgL0lkZW50aXR5LUgKL0Rlc2NlbmRhbnRGb250cyBbMTkgMCBSXQovVG9Vbmljb2RlIDIwIDAgUgo+PgplbmRvYmoKMiAwIG9iago8PAovVHlwZSAvQ2F0YWxvZwovUGFnZXMgMSAwIFIKPj4KZW5kb2JqCjEgMCBvYmoKPDwKL1R5cGUgL1BhZ2VzCi9Db3VudCAyCi9LaWRzIFs1IDAgUiAxMSAwIFJdCj4+CmVuZG9iagoxNiAwIG9iago8PAovTGVuZ3RoIDM4MgovRmlsdGVyIC9GbGF0ZURlY29kZQo+PgpzdHJlYW0KeJxdUz1vhDAM3fkVGa/DiV4SoJUQUnVdGPqhXjtVHYCYE1IJKHAD/74hz6WnRoIn23l+tjHxsXwsbTeL+NUNzYlm0XbWOJqGi2tI1HTubHSQwnTNzFZ4N301RrEnn5Zppr607SDyPBIifvPhaXaL2D2Yoaab1ffiDLnOnsXu43gKntNlHL+pJzuL26gohKHWp3uqxueqJxEH6r40Pt7Ny96z/m68LyMJGewDSmoGQ9NYNeQqe6Yov/WnyFt/iois+RdmUt1e3xYedFKIzyvTBMjuA0g4Mx0ghTNtAEmAhGN3AAKkgBZ0CQup0wOcChbHYGXQSzOkZieENCwJBY0saR1AIaaQTCGLQkkSRSgQJMpVKFCiFcV6cpvIKgt6hpu65cbENgfJ+WFpngrnZ+BYBeBULAPRjLUBintlHnpNYCWgJ+hOYpoaA9DQ01wz9BK0nPCHglNmxde6H7+bsK7KutbbGjYX5/wGht0Pq7cuXWdp+z3GYVxZ4fkB2qbXDQplbmRzdHJlYW0KZW5kb2JqCjIwIDAgb2JqCjw8Ci9MZW5ndGggMjgyCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nF1RTWuEMBC9+yvmuD0srtaNLEigbC8e+kFtT6UHTUYJ1BhiPPjvm2SsWxowjzfz3piZSa/1Y62Vg/TVTqJBB73S0uI8LVYgdDgonWQ5SCXcxuItxtYkqTc36+xwrHU/QVUlAOmbT8/OrnB4kFOHdyH2YiVapQc4fFybGGkWY75xRO3glHAOEntf7qk1z+2IkEbrsZY+r9x69K6b4n01CHnkGT1JTBJn0wq0rR4wqU7+8Kr3hyeo5b/0Zur6v2rwkDEOnzdaYASWEUiCc4QzSVgfobwndiFWECN7QRLGNoBdx8hVbPVLKkzBMo+Qb7/ZKgrKXfhXaOu3gdBh2MY+PbFY6wcXVxYnFmalNO5bNZMJrvj9AE7ak9gKZW5kc3RyZWFtCmVuZG9iagoxNyAwIG9iago8PAovTGVuZ3RoIDMyMjkKL0ZpbHRlciAvRmxhdGVEZWNvZGUKPj4Kc3RyZWFtCnicbVcJUBRXGn7v9TEzzAxzzygDMs3AIAiCwKBiorgBPEiEgGVmDCgGUMRYElTARNSsokjQxCPJpuImqY3XVlZtW+KReCWxsjksJYfupnS3KlETU2pMJNGsOs3+r2cGBiJU9+vvdffr77++/82SxqW1SIMyUSGqqqudU4OCf9vgyK2DiRD+Eo7EpxdVh/FtODIXzmlpCELsgZOrummJK4SL4DSjobE2fJ+ud3re08vmBjGpQyh6Y93CJS1BbKTrVcxtmLcwiE1l8M5RhOmjgPK3L5xteOg35FQrd0/9bBpJx+4L07beTQyc01SoFwPU0GfpxxBSPS3rYGLB3UR5oqZCWSfyzwFHFdqGnXgtvksKyRYGM+OYtcxZNoVtZM9xKdx87hL/OL8R/q+peNUkZQUHehHZ0TTEhdaLpuSIHfGAU9FptAm9Acd11Igk1IpmoNnwbBVm0Sl41ipXISt5HRl7xyK9/CoyMwLSsnnIym5ARk5EVn4IsvEnkZE8icwqFr4Bf6JuhIhGIAnryQgsRWM4H4rOy0xyGpF6BDqEx4xMjDHB5SEyLT/LbaNXzORxIwUrvWL75rjM4fH2aHrFVz8+Id1Jr1T5OaF31U9OzUsZSq80q2um5bnpVVTzU4/mCvRKu6m5Ijina6oqzo2jV/q0xFirjlKQDHoVUHQVts4fUiCJFrxYKrXgJdIqemqw4KWSIR7mFsXjpWAR5BYzHGxlwGMapEMGtEjSGU0m81hRZxRRNz3zylmjnPXdSESFPpFkOPeT+PF+BSAAyDDeL7EEwZsSFxxUwUGtDGLURUkXnNAHJ4hRjL6YOUoQTAJjwtiEGQF7scAMDzxETuXKP8nvYd1lwsgyJoEAJ97dzqkCraT5nom0BGaRWe1kFsR4K0KcA/hr0KMSjtLCukF+HFDiwvzUANSDySqAAcBQ5oQLUlYGIOXGJrcpG5uyOUcg0BwIkBfusZp7dzjxXg+rhxSb2XuducdOQ8NQCmqTHKkjqEUOo+gK+ccBCzucoa+wAFjDYGcRB0s/qVUGUZsBr0sGHDXCJ/zb+atfMmsNMJ+v0ZljzWnmh8xspcgapfi+B0StUXKrwggoe5J5d4LHm5OYne3N8bgTeJs7Jzc3O8vuMFkdtqSs3NFeOmm1M4xRt2T322cwvvnyrKry+p6mz1uPf8ekBAKTV7pWr2uvjC9a9t76PUcmTa8tz/e/7juxRx6y+QnTvqJxp2fNKCoHry9HiF8G1sehZokdFt/ndcWdYatVAFTOSKvDwAnAGfaHDYAtDHQAdNQ5mHEqmaSyUeeojJKx3/Ah3ZmjsAMscwStc5sUc1UWtwlGFWA8R9X48TelTecP7CM9M+vK59twD3d888M9bN4zbTt3fvxBYCw5Oa/KPzkQQz450Xz/Z6h6gsp7r7NasMoGdq2W7Ipdot0oqkMxVdLIOTiMMUhNn4vJ6DoRczaGVHbNjlkUQ57pio/JoMPNmF4YpM4YXCnp+sIlokiLJLNaqQi1UbLj/ohyCcjrVUJoNtkEuz07a7SD5xmwVPB6PMT/g/xt63cv/OeXwMPavzbvmr964jfba54z4bPqRit23xr2t96N8vey/NTabeta5yxg3uxsNT/bSuURaob9GmomClnQY1KUVXFxlFHUdUdEMVwuGACmdmoxE8xTCfWHgjFKlgjGtGiELNZmZd1JQlYupGGygDcFSPqvOF7+ISB/hTPXbXxphfwpiQtc4kT5i/M/fb6ypWkDSDYqAe8/DN43oBj0iKRzxoY0aEiIlVJEzsF5Yu7nAtUxJIKLRfEccjisPNQF8jqsdoURZErJistbL2CTFj935ZUr8o2ejtvrNyxr7iTJb/aul7//cexb9zvwKFmz69jJI9tPHoN8L4Zq/wy4edB8SZM8vC/fNUBDE+YUCyBWKexYTUjf7N2iZkCg9XYj3OrS6WP1pFKMNUqJ/feERELvMYJFIJXgyxyPJxnKmeY4VLInWLus3W6z0jr3eLNpRuSSCwt+Lz94/eCHN3vqZpTWzsLD3i65daqte8l1rqW6qgZ7JhbkDPft6jx68rWiyqnjx4yf8MSzT2x+t2r3nBmVU2g2QErgl7hz0AH0aLzER1PVEXmjiCOV3jkoG6hxOmgIGRKvj5AgKpk5uVmUYoIHr+zpkeVJayZPXjOJzcNDx0ydOmb0lCl0L6DuvU4auYnIisokZLP3+dMCy1vC39ID0NNvWSgZ2otQhoiNEtsfcr1RUkcmgGik0mDKtrlNVlouNt7tMpnc3mwT3nPixKgJqSPKH5OvShI3Uf5fV2D7+NFRRx3YS2q7sApYLYNe+DtE2YjKJegkD1a1AUIWDSDaEKl3inZFK5UCtaGNrA1QK5clJFaKSM1StXyEy/gef+2WyaBMrZvk1oCXHJtX3XZfBkGC2Mztvc4N404Bo2GoSGLjXXRdMNMaio1SBvGDu4zOKFkHlIUzkoaVsO6ERJLszTEnulizQ5VD3AlEZbOaQWe83LCt8o0D++Vrr+JXsQXXYMsrOHBo9/Yj5P2du98l0nn52J49OP+LsnO4aM878olz3C1M5N5r03+W72LYgWKUDsTP8zaIbbmkioitop5CJNfBUhoMYISg0LCDJuoj6GfTaCpF4VCBH202Gusvd+z4pCt/Mj/cu/fiRWZtV3XXKfNx9YGqrvvPUi8OAb1zACMHcqLFkiY2jnpRYxSHdkdU8QOZxQCICQOlpYeDPRTA0BBnS3eEbFPOmshGHuY8ntBmpVLqWGFNq+XYjh24/sP20i0lSR2Ni2rOnSPzenrAgqkrD1YkxHyaOWdOARgByYCpPjK/QXbGomIJxQ3r86sBmBickTsdmoP9Gi0xalrSksWg9CnDwB5DtwXZ0ZAByOTNQZSh26M0U9AaMvbJFvYK2/TZ0kvybay+/EEPf4VtrmhahVfurZ9Wu28u9mBm6B2c8u2hypZ39iaLe6i3zbAj+xN0FxX0l6II6Q6nqaInYTfyAHjKV8MSJSqR3UWKUiYhZwXYGmZjAUbmo8DpJvl/xHOJJMu/B7bhnV/hcnkPbA9LSTypgO+XyTNYHfhpKEqELp6Q5KHrJhhFbfcDqln5fhhoAWhNg6ItcYxD6XtGydTPzGCik5IteA82bnEDemLCgD4EheUYnW2CppjgSR5NRdvrBQknoxWNonsWvuzGqSP12gvynW+bL41b3LB9Wef8fcd/udXWtGVS4ZaWNpJ0H6c/v/DeD+duV5dublu9cspSPPL22++vwBefOw65MRV6ZwfoRDKqlkzDUwb2p7Dj4wDEKf0pjvYnKVoTKoQIw0DXJUe/mQnKk2KcUUqKlJAcT2KystHMsg9uTVRboDVRy3KZDevkt+r+ieOv/OWn9vsvti1fvxVPP1Ml/3jzDfnOlsD219a04eaquvqJrZ+KlxvOrmh8vn526YLa53c1HPzX0jPrl7cvQMG9JumCeOpBATVKd3rAbkwJXRgoe3+aYRAz0m+MRssFsymkv9nKFpIcyMzelddzlfdty0lgKtSrArFs3rIN0aEdCcPT35RDUY3kiHGG9vgMZBKf8cdkCm9KBlYix+iot6OUgWqzeUCqOP6QKibQi+RB+VFy9aN/1Pdc1C08ePxaz8qlLxc+8nLTKpJ0F2e0kNS7qGENzrq140gbPvPn96nPOuG0EpgzsJPtLzphkN6GGja0aV75ha6Ti5nzoDUGNEFCym/AByiM4lwTVRjOENQTqnicUdJEZohFwKApyV4HyMtoKNw7FzEjl1Xf8OaPrKh0uuU1mMcV5L/3EuSb5qPMIyWVrBcxnaiG24u2stPRTP42Ws4Wo3IuDvBGVMIKqJhoUSv5O1Kzi9Eybi2aS86jdHY3GsJmohKuHJnZ9aiM/RpNJd1oOZZB89egEnink7mCdCJKKxY1pb79GG/0H8a9beKauP0aZvasdBGnuVyF8wtEXJUukjQRpwrpIpPmKhKZpKIyn9vv6nB1TKnpcBW56ubUiGySMsKN2g5/hktE5b75cJ7uE8R8v7Pvstbvz0sXWboMqyzT4YcF6kML1CsLwPuBdJFLK3aJjKfU97hPXFXgFPML/E5BcBWKJ0t94skCp+D3p4t8H0dX8He9wlaVJvKp6aI6uEK5T8x3isjf0RFEbkFc1dHh7AALwvjkQHwYo8ET+ZET4IHCw3hVqXJnlVtw0gm34BaAob8gXdSkFZf7CoGiABSj0sSUwnRRmyamwqBL25+M210d5b4j+YhF1YfVqH267whKYa42+J2iGxZ3tR82or45aqU+TcxvP+xCM337U1GB8whKZa4W+NP/D84iXnUKZW5kc3RyZWFtCmVuZG9iagoxMyAwIG9iago8PAovTGVuZ3RoIDY5NTEKL0ZpbHRlciAvRmxhdGVEZWNvZGUKPj4Kc3RyZWFtCnicpVoJWJTV3j/nvMssbLMwgILCMDKYYiAwkGamN7Ws7FIugaWiooapKC6oiUtuiBiu4G7d2yJY+fpmKVgKN7UkM9Tyqqlp2acZrddSgTl8/3NmBmaAe5/7PR/P8y7/eYf3/Jfffz0zK3f2BKRDCWggynxhwtgs5PrbDkfKC/CBmz4DR5cpOeM99J9wvDJ17NzpLhIvg1PU+Dmzotz063B6cnruBM/zX+H09aQp8ya6aPIZQrFzX5g6a66L7vYIfCds4vRJU1109+eA/hBh9lWElnXLuDkmqM8fKFzLnx791Xg/u9Z+/dTW+nDnV7oE7Uwgdey7bDGENFOoP3wwuD6cPq5L4O/x/hsEx1i0F91Gt/EgvBBvIYh0IzmkUvAXRgqlIhHni9elv0ibpHvyTPmKJkYzT7NVc0GboH1De1vXX/e+7qSuXj9F/5n+ll+o30y/9/zq/bv5L/K/EWAMeDBgTMBHgShwUuDWwNqg+KDSoO8NvQ1bDe8bfjNajU8btxtVk8b0pOkV03Fzd/NU8xtml5yDUDEKQcOQ5OY2kIlOgpEMdDe0Ay1GhWgu+hJloafQSLQSZaIRKJn0QR+io/DtYJqJgsk2FCVYkZ/YGwWLa5BBUlCwHIYs+E1klKvYGxFS/LsrqDtScSDpjtUgDOcDQb0TYsINSNsdHcCPPXi/NRhuD5D0xx7oyu+EZx5Jjglhd+LoIX26d2B3UkpcdIcgdifPGDUoMZzdadblPd/bxu60Cyel9erI7nTDBqbYQ9mdfk7mEymd2J3f0qynXN/zv6AWju/D7gKCjQE6md0F9kmMjTAyZlSDXgPMRg3Mzw4boCaY8ExVCYZTWjCepS5mp+nBeLYaFAGf5UTg2aA6gK/QFcQWQG065I+C0AzV32A0mnop/gYF1bKzzM86fg6oRQoamK6Q+PB9JKJvBicQEMjQN0MVCYL/VCXXReO6aPlln97/turv+iSAXxRi2Bfofzuhp9VqtApGjI1YsGIHtgpdnX3I0RT6M63E/teJQCkmTqek1L8uaZz5JK/BSOY6R5PRBWQ0mLoMIfkZkCAQZaAWbjys+fApAiEa3IQWCK2H8AfCn0mAkchZd130/JLQU0eSjDajGduwGa74NXwJX7pXZaLRG6jNXCUpDc+Lr9enkVSS1TBB3Ozc5TwBDAFvIfQJQQFwBaHBKuJadbGIYTkc5l47EIhADyEBIZmAERQoMQ50WN893Xo+/DY8ZtpP6InNSZjYYmMdoYkpqak4Cf/ceJwOmXDJNjBxdHb0fXRhDQ4SujdE0t+FgBLxyQnTxPuBE11THRkh9UMWNEYNDAltnxMzEGYPEQBEANMP5jhA8XCjCi3sBBhUbQslGBRjrWJi/BmTLDZjcEhSYqpFtkXbHUabI8mIV9bUpPSPemDwwAULjx2T+tH6YufY/v39S4JLCsmuYiyD764Blb0gHQUs+qO+qhwQyEAiGxTsjbmwVpgDFCn6WkUfr8qShxuAlM2YJCSnJIYEy9F2PLyi4tPUKQ88MCVV7I0je/TtO7JPH9AJhGw8XAYHQJ28FujcagHXy9g7ZB5cRzXVCf8SnwJNdkLZqrZzJONSa1BC3FxyXLXmUu2o5ZDvGL//SMcvOpJRqr+3YVVDM6WaXd/UGtQQ3CKQFI0cjuSUlKREk9FiDQHtpqSGyjha1lgddjsZconWvXR5ydlbTpv4XuG4gqQZBfTC9M0m0llbEIytv0e/5iymt6hzyKvH0v6Sflqo+fuGwDXbQAc9ERJLAawadL+LfRk4ln28hylZNqiohWGRmdkKnmB1WMVSGneE9hAnSqb6nyXTTnhnEaj0G1CWEVCvN5k51gzxbU3IsW7w9gKmKUQkV3BQ9V7y45SkKOZ7drsNZDYyiwx86Rh+VqjA2TkjC+wVFcKBUprvdJCTs6dnPtXolJGHE7CVEQ1V/dycsMUEWEzwsZGH4OJ7eAoCIohHBUFmPAkG1c+bp9DWTOEH5QWfejEl9i7Y7s0UhALgqg/AqEa2AILGq0FevsiR44GfD4tGIIxtkA/s4BajAGACWihwWnOtEgyGSmL+B9BJSgzVMD4tFuafN/bsqSzv97A+3jFy3I0bwp7inHcPG0t02eNyixuHA8qfa6oTk0FzQSgcPaL6R3RyZwSzG+U8lIa1jp5mL4wY1A5eujJz7CKLJViGoIAcoSxApDiSGUfP5X9TfBkb511bf4n+UvlW0Zo3y4pW7Saxu2ghPUUDdjYU4cRG3f6Llz9RL190WZWUAm8BaJAqBgY1688P2PAL8w7vbZDmZ1BJi6vp/CQe3jFwwTEVyrRESrv1/KB/xX7hubdSIoSNmm1OJPbOXxcIK6+GXDMXVu6E8lSRe347eGobADwEZ9ADrmAggj1EOBDhHGlaP55B+YXZtCUyKGG1Lswlhqa4gZfsAV6ySwDcW8qr+e6ZlHfm4Cy5YnL+pAK/yhsfPAI4nFv07lOZdKWzO6mZNfOlF5yJ5FjdtsZbAEiCpkJEuwVSdUZd0Tw19L5ubOlQgxLltnUoMBca1l4ObQ5vJJTnSz9+ATnh36FUala0yY+hnGEiskUcsIXNCyFWeyxPF8ldkpJcuLDYOGjAQ4JDLTEQ8By2KEtwiNDVoF/y3qufY/zD/lkzxq+onHlszqEvRTv1e3a7bR19e1bUMyveX112aPjYmVmPPl2afuh1Grgp3bBm5GNXPnl2HFhxDMh7COS1o7GqLrZrsxV1II3OI2cEEBFMNCFCx3jXsewLZ2+DgANaWqgIg2KthbPaxTtGJNu72O3c/UJAHi5WcIgYEgKOANLaHUk8lgtkBr3xxl+v7T34feWScRNyJ2PL20NvVrz86YwKaXVu9iIc+cTQPsNmpS0/eHjjk9PSH31kQN8R855dt/f5NzJHTWX+WgT+GiWzIrgDylJNHcMZzyaDIoANtfEtXhrmHV4MrYIewE7gZY+/4HJ31eQjbKi3YMHEFh2rAacxpkSZjLHJ3IeYLGLU9arPp+vKK6bjwmsV61cdSBu+f/lGYrxLz67Ll5Hz49X0HHVKH50qp3Hlp8Aez4M96rhXDVYNXl5lALYMPl7F84N3yAP5FIMB4hycfdhjcEkKBBaR0eFGkc3uCn/BIaTv+I3avdKcY9Mu04ZZFzZ+8Jt2r7Y4e822rUvnjhy1OwvHYhS588+Ci+9mr/ysynaohuX9IpopduYRsSOapprDI5iKzAbF3+0lPur1gRJXr6mV4lXJv5WuzT66Dmuja0+A8lV252+rj82oKNPNOPHxdxXbC3YPG7pn+Q5ivEPP5DvvSBfnFtGLtF488OUmZ8PGs0yOTDpC+B3k6IC6oKVqdIydLR9tUPxq24llnHWf8OWRgwcFLofAfFsNCuURC5za2OL3Fv6MBYNOPsJF+2QGE0S01CQjd4jYVJ4XHHBPUnkRaWRWy7xVfTxHV3bvzOyrD2bmvb2yNKfq8I+VJSv3PjO8fOVmYnfi7kVzG66e+T3r2ZwNmwtHLcaJ/zpwehf+edsZlnHTwDtyobJkHo+8PN6n2OkIREdXsQPKYCVPYIsk9o68ArAb1OAWSToalMhaOPvIg+08hiWGtPZ30RbdBfydJ75kOxlx5xo2Xyr5cUn17i2v7CrFL34+ntbdKKGNa6o//vvmv5WQVY9+sfnta7NOzltWmp+T8dLEl17P2ffVzE8XLduy4NxsJlUB+M0liOAGqG70RpOvVD5lvIdoruea3Yt7VABpp+KK4eYA9w5mRoAqQug65fQ77+OKT78cVKG8uOjEMVLlHPjnTsHccJxxkwc6Pg86jkADvdJxROt442dQgmoZTHxDSwdvSolgmU4WbVHMe02gTOTyXgLea2Klr3g+nF77mVJ6Yh3W772Jw0KrO7xVcvD0cfXVPRH41I0GnItTij7DyW9Q5/fvbqe/Nqz5id5ctx/4XNv0B56DTkO3G+7iUwOsaXgS82ogUlnzYGHdw9peuQ8+mNtrbEK/fgn39+0LHhSBkNSJ18x6NMirHGpTNfvEVp3ItayL94pgqp5/yKppwQatnA2uwufOnauOk7jdpMcx5xD8y128gC6D5jeNdCRlwD90SaI/RHp/NNtr7c7tFR8+jHiKDxVp2Zr9LL3R42gkmozmo0K0FZWjSlSD9KNU2f04RR4kj5AnynPkFXKJ/Kb8gXxc1o8CVnmlZGSdJ/BbOauiYjx++hp9Dp+7gP+YR5fJqHFMHs6ifZyFwO1owOg97nlzUQsePZrqAEQHFwQ7MOWgePULhEepuxCeocazuzEI57JWU/ZuPNksApomu1fTZOdNUwcGHTh7lRnAcFJKisPgrieSuFU13B+juri9EcqNWBtesXppSdPc4grnx6duvTR57tImRCfSpsqShSte2b5htZBIVuRitGrGO99f/McYNc6uLDr6P1c+mFlYtHRRAQFJR4FdkkDSUDTEK321GZG0JCmF1KqCgQdISF5ejb6BJRXVEmrg0LBaHTglpbki6sIlwNgqJjUOwnenDy+cWbilAgsXPqujP9P55OvlJGHRruG563cUnbj71b5/0n/SDMDsTPBOPXAXAKh3hwk9sKRv45967wDeJhEZIBERwWEAJzSZY93lJ9dhqqi/UvfDVfHKTz9eESqWFb/yMlm1etVygUylh+hR7MBJd3B//AA9S48H/PjPc1fo+bprX16H3ns6cBbI9WZF3dFYF28WYMcS4Z1Mme5idRamrVhASSxgo2OL/YWOsTwJxXZ01WredaalVXx2m99VbKakeqozIdkeLXNRMI/ZLEgLdTPH5S5vqj3jXJI7dnpdddVPpdvqSzcsfXkjvTV15fIrywvF5KnlCT0/zPvo6rUP5xzumVA+5eD5842vzd+6+e4rxWLHlbNyVq26UsTio6lpqWDgkzYzYtMU7+lUQs9QcP99u0jmzny5B/t2OaTqC9IBJHusxVuYtgM3xJtlMd5rMqMSwdXWWG1GyRGTRC5U0tXE1Ek8vWr3J+zd05oSRKO8HbQ90avw9bw7GohoHg6jPYWvPyt8FQMvf72qFOITtqN5MoSznZ3V+3yL4Fiu5lRHkqVNXpQ1FpvD7smMQvqSLV99fOStTbsPHVyRm7dwOe655+nPDmysPFuxfsXSEpz3/MKk/qdfe/1M8NeXQus+X/PmS9Mmzhuft3Ny2SnzkSPGG8dXlywEvC8DVEVI+1AwoOqpduaCzdMt5N1Eq5oAHkc08d6FPYSfCG9pLDZZ1LBCzMLqMuRwGFCMAI5g8QQT4a/R55tQ8MxPDn/3x6nTtBE/i4eeGfNq5N/m5Revk/btEO9eW0Zvf3mN/o77Ox/F63GZ5JyeO2LA/ksHN5VUgIU60Ss4GX2NtOAVLGOIzZnJNYqDqimWKzNYXpE9Wnx/PL3y2NLVb62bUMPsOwQiUQwgTUaJ7WCHT/yY5JL3OE9FbtCwoM7GOjHON/5BMhrrhPPSY/UHpbDNrHbcDW8+wWetoVBJSWE8aEsts69/nwY9+lYNrjRo8EmDfnzOyvpD75mXa8SUKDKIgNKx1RWpreQILa/G3/6JdRuW4hGnnNNwbHHZa5vpRTLE+Z6kXD1XUJPoLPEntzYuWLkWI9eMWBwEfPt5sOBT+HlaWtVPlF3tq8+wy3tkwKpCrS+LVov7EK46V5HLjeOFDc4eJI+85mzcISk7aRysfx9CQg1Uan5oaDtjVx6I2+2vm0sTpOf9tey6CEjms2EZuUxmZqlYgAoCcvKdX8/TnXjqufr6f+KpdOc5shevdv7gvIw30ReJjYQCN1bgphK4kdEDKtJo258Fc8y07hZVJAl8yRherCTh37D/R3TBGjr3owbh4YbjbL5F0JymOkkHEd2IIj3RyyfrN4sY4NM/i63cLJiwipnEuipA0WSJ5dlHw0tAU6qkW0vvKm/TPzeQ9djv7b3Yb+2R2kMfnBbOVlSeFEjZOVq9uwz3OpV9Bv9lTxk98hXBArbQH/98sYFew0FOxmkkaOJzPonrAL2Q1tU1Q1cZWvt/msiFARHWZjzHiBAgQv6/szo2f67Zs+eLj6bvicyfkZd9+TIZXFkp7Cl+/v2ah7Ynvvji6OLG4aB6jJbTbmST/CJoXoeUwHjQNO+vTKmhMhE0BnMIvDaWLJ/36xL7+kM6vGb4bPvyeXXkr9/iV/GwQfOn02T63XC6kF4vz5zx+Lt4GPMdO7wztdU7wTZEE5tiMjuSCYGWzWQiqbN/Wxa79RBJ3zEudtkveWTgN3QSfXPwjDmAkfioMzgbd06bMZiW0yy3P0aAP0qo638IHF5eCIhjPpaEJxMVGqQdO9j+CuGTnF+hFmY9eZYa5urJw1xTDzm+rdU8g0rfiYIk+PNOnF/+cyfualZZN8SGVcQ1xnS1qGPqjhzOqSjX5Rz/8KeKzSuUp4e+XQCd6T0c/zJJrkezCnDyXc3B2p34ty1nGO8O0EEaiKGHaDq4nTzcHJWwjgdHHO8Th2oZbPxqVa3MB4YBOtdOFWaAYYN5cE6rUSOsqKmpdGaTomPOJfhYCP6hlL6Ln5kq/NbYi9R0BS6ympqEu7z66oLS2+nb+JYQH4OZeX8vQGwU/FyjD+9W2EdPrL3zqcBl16zLs39hQBLgPCWFz/Q8tYDQt/drmQsPTcw5WXD+HlXoO11iv/+T/jxqR5ed8+ZvLCaLBw5bcK1g/a0F9CP6Qwp9ls6Ttos36nOHPf7B9UNbNlUzrDoADydBp0Ge3QyfnN88BYb2AZKfv0+P4Bo/uIuQVMGvuuH0hd+rVr2ctwFLSsO903VXPs0vKlnpySbxsIoO9ftP6NURsU3Dx4Kc3CbF8exx0rmjSljh7E3Gk6XOfJY5AqFWZGsV8IwbAtFquFegbttZIMyHLfHek1dtEG8wfLaY2Eaet3mSIMk2d0WyDRtZojVYY1z51mYtw5d++mVW1pxV9Cb9BD+0Yhv9llbh6IWlRcX0uqQcrZq4o7u1YvHRq6TMeXv1fKzZunDK3KlMU/OhCrsE6IpE4/6rmcC/6zzUzi7MdY7fH9Q5sjMZtf/hzn+Fi9dYiQ0s3PnCgKBoCLURK7Qq7nkB36m69Di9o6i0rpSAc4RcxyGRVSn0SvUn+OqRKX9z0L3EcGRy9ls4+bNFeDCe/MM5bKW/0aa8P+g3Cb3wo9tBnu7Q+Z+FSOuPMlWJ70+2kzl9WnCfPYn2h67Ijw8CJddF7+fOsGaWYVmSNZuTxMdrK+ixEnqvCZXQ4we/atzUJDzYcFxIbjwp9m48J3QD3h5CSP4JkGLy7IPz1SK8mYrwTl4R3tVHOzteel5wuC4B/MJ2H1jbH5rKeYvV8BLgu4j7cNTfcIeukd9W01176ZehofTEXvpqFf6s6h3hbqNWOSpcr08TY3JyGr4G/jDaCqjoAXedPN2eD5699p1V0bsyx+7K3KvtkzS83wvgm88BPgFJY1A6QUY1m8G7hOZi3R7rmrfzqKPRnHqc3HK+F/diwSe3/nWx+rZxr3HtnCUbdi2bN7AnuUjO7aEzH6b3rl6jznMf5S9Stq3f5+gK/B8AaOfxTs5737i1P7r3jfOqqly/BQBPlrQ8ajypYr1fM3Z8inMfM/lUEs0lGJFcP66QkLvJw3w31Jgkaaudtupq8s0RcVzDTghaW8RJsG4QRJBDsK4W9WxH27zCbJNyEHZnE7Av21ZKIhPwgotUX0X1l8hX5OvGHOc1EimsY3JFwPudvNtop5r0Cb2t/R6W8akmyWr86hnn9XLn96ebxH31aa4czyJgFc+TZjRE1QfzEYC+Ze7uA3NPc6P6CbhtKS94Tx3ZLxXc7YVoi/H0Fji7inT4BQfRO3doGc7Y+vrrxXQ76e08Lim3T3z5/Y51q17eLoDUMSSSBJAPAANGBPb3Ku5YUHdYSQANwzdJZKnb8hLvPma1Y3IfC/ybiR3fEu9nbD2v045Se7EhmSzxkZ257chO1zKw4/M6Eq5WVZGVN50fk8n/Q96ooMGS4kwgtc78xrNuXR/nnFpQe2ZsRiQ2tPoBQ0CrfCKaQsHhRI1gBNdzQJkYW4a3HcZdduFd9MLRszVX7vxwTlJ205oTo07RmjeJZGpYjYObht/FZuLtLX6enOfTsfmMrnzcpKV94lNCSL67dDhXjdeBmogXj+CbVq4Wq8txokEtV6qhaRsDCnmF5DIe0qE7eA+ivQ4N8JoUtx6uevexIuLNrcagSl67zJLGhXKIRA6GDQvuTeY1vkGGO/cLyaWlBUKHLUsR34fVSzJkSwuKBqRrbV3c3UgHN9J9wnZz9Spo+a+1jD7DGGgwonx7KY2kYRP99vupKGin8Ip5WFxKP38srbmnUqCnqvr88PunhdN4FuupztIvRpycd9vU3FeV0+qvMPyFsr6qnl7lfRXmu5HlPBf1USVzcHNM8CnHfKpdT23mtefCHFRGGk004rsQPGwbxXJMmy4l/UKvk8Pvvvb3dySlMfoUrTcQTL4Vrjbad+59d6fwNeNhBGB5Po+33tb7Dz3Gf229XkL/hktC58ZfhDs7dqwVX97JfriEJolWxH51IiIDYmUy4a0yLARJSLCZk5yWJXiTtI6uxd8IRShLbkRlwgkUghvA9DuQDv+G1sCxXXwIjRKPo57iC6hInIyKyCeoj7gWPUfeR0UaAa0WR6KpYhQaI+bD8wT0PL/mo0zxCEoTs1GB5EB5JBytlR5AEWIgWiMuRKPhGAXHTDimC08iE1zL4ZgGxzK8HnWC6xDxJ7RbLAKe7qD7hKvIKi1Gc8RpKJJ8iJaTT5FdfAqViUth3YXIAUcW8OYQp8BnDahMmonmywLqrumAHoJnW0kPdEDaD89OoSApFEVI/qiMOFCMJMJ3H4ZjP18nHdaYKiUA/zVoBJ6AJiko7glFl5a+D+NXMg7ipuXKsk77dMKY0T0UHBcVNTB7gIIzeygkTsHdrD0UIS5qkCLEDHom3ZYRVRhVODirMGpQ1AtjsxQxhl/hwYTCjPgoBQ1Nz4bzsHSr0i8jvPl2QkZG7x6KyF4j8tcUZsALJrtfMJm/AP7f2UOR4p6IUgR7WvrT6criAeFKvwEZ4VZr1EClKi1dqRoQbs3I6KHIzTxGuX6CybnVxClytx6K1vWGoelKv3AFZRQWuiibVVlcWBheCBJ46Cpf+iBGrT/o5/0BaGDgQbw4jT9ZbLOGsw9sVpsVOMwY0EPRxT0xNH0gsGgFFvVxSpeBPRS/OCUGLv5x+2JxQVTh0PSKfoDb8Qe1qGBYegXqItycnhGu2ODlUQUHDaj5MyZlQJzSr+BgFBqZvi8GDQivQDHCzQEZPf4XcHpd6QplbmRzdHJlYW0KZW5kb2JqCjkgMCBvYmoKPDwKL0xlbmd0aCAyMjE2Ci9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nM2bz24bNxDG7/sU+wJhOPxPIPAhaBugtxa+BTnYK6togRRoDbSv3yGXFCl7NXJSckUYm8SSEf40nO/jcEjDzPHrHeAfTgFzfl6+TjD/O33+gi8dJj7/MSnhmZEezCyUZhq0EXrWgjlljTYzaGbBWT3//TS9/+Hpn9+Xp18/fZyX54kzr4VXANYoy7VzYn7bS8/Ln9P7T88w//Y8HV/iSOOYspp7V/MYYGAUN3J/HowbXyEqHmGZ0l7Z/XmE5Uwqb7ivefBFYwyH/ecLhGNCWwey5gElGPdgvd0dSPEaxJ3oducougLFODIMo6vCM4auCs8Yuio8Y+iq8NxYVwVkBF2BEywk7yi6qniG0FXFM4SuKp4hdFXx3FZXFcgQutLABJfDrFcVzxi6Kjxj6KrwjKGrwnNjXRWQIXQFngUtDaOrwjOGrgrPGLoqPGPoqvDcWFcFZARdIYMU42yvCs4Qqio4Q4iq4AyhqYJzW0kVjhEUpQ1zfpyNVcEZQlEFZwhFFZwhFFVwbquowjGCogJQkdIJbYOj/N8yjiVxVKGV54CzbZ2SIC2+JM85pH6zmhClktENUIqSQk+pSGh/lEpFiFLJZ3+USkGhwqqksz8LAqxLUpbNDgjn/y3M4Sv8xHH6a4KtY6yP9+l1mI1lGgDTe7YOBeeUF2K+/zq9/0lgrTrfH6fPHzjngI/AR+Kj7mb+Zb7/efrxfvrlfw2Ok8S40KAMPbq+mw0mWzSA8L3Bx+Lj8PHp7/DaQzMy4dGvuOJa0mSP+Cz4HPB5ahobqXSWOklwXCen5chWMceNUUCODJCij3kB4m5+JxRTEFpxcY7wHZDNmBQHpp3xoGgmlfLUN42IQkuxwmmg8xSSSkBXUXHMRgMIQQFDIPE42NsDMlsrkSo0aVYU/ET8hKJTEDA9REhMaiq+bWSjmPfoGZYEkMdVoxDigLo87Xo/JO0emgGBFNltKSKhE1HIEZ10+7CGKLpHuxABYqw9xWsR0mskPBP4o0rl6ISohfw9i9rSNGoCVZ7WbzJq5pwuJpNeIxfyHczrTxAj7JuR1k5IxdMG0bUb1Ypc9ZFCw1mBQ3owNnBcDSg8+d/w2E72UqaqmJw0SAMHIFyihKwg26VQ8CDtGIYet9+EBy0pTRp6kGYgwUlDAki3raKGNlx7D0FyE+8heGLJcFzrlk13VpXKqXX0+z2HonN3s3JMSu+SJYZJC+HCBdViOawE+PX1LiZD5fTSVEG1yVwd9dxYXhnOsYfJUDmdTEaI7iaD5ZdwSl8sdLKaXqbHsa3poEoEykSSQBIhQDDj8V0ZKQ4bDnStYv9+F3pLrB5XhUudXeB1BdLUkRQzmmNCkXTCZL8p3hNJ2laLtQeRNEPVPdSsLk0rjNqSro26UffE17vWPdSk5brnUCwp2lN7SxKGaRMkty2zkB5xxdJtLQhwix3bbBSAlDk/VwhoVxXXTkMRxN3feddGuBddHJtqn5AxsqvOCVJxKOtH1PhypY/0/Rqm4mW7aZhK1ErDst6vHFKJEd5vV2JVGqYmBEqdI46rhmN42msYeDhV2NxBhRJU4ybDRshcS6gLNUau5NsJ3VpcMkEaS1JKkRwGwRQkGWXQw/pebsB0MACC7CYbHipS4zRbqKjtZ0YURZiePo0UYtR6j3MyoLSOn/Y47UrmyoyoUEACeEr9pVBQqGxILc0I9wsMhLuwHy6NlCjrhgWFZPhhAPOSApBYDINhAHEn+KGS8ZZsupgNiTdUtUGS+nVk2bYsFN7gJBjDBT06zowEZqXL07iUFlB7j6FQQlXep1lLjXrmMY8X+ijtqIrHkKGAVH09lWJHHJv7i/VMcb+5bYr+otNibdt7DH52g94hSYgoDYQQdjdLIWhOR3g8VVMhLG3P0cBY5kJKXCHxG42GfMSpC12uYjoYGkE3VpuEJO3nONTsXejcRoGnLZa0PRyHCkWuajKcTBbYfItlsL4wnm9u9LLr8FzoKn66T9ByJ+WwugvJR8NAgsj7lBflRNj2dXAggkiEUIQe1kPSduOmbLjHprwCdT0u1Ra3LBFdnIaKx257I4oidhXbuWztItSob2i29qlbqAmBUkhFc5U1aFMXUZZ5u33UrWRKzrwUtnWPcNkFPxkuXBTEqeGqeLmc1uWCC0XxbYe6jXswnkkbzrzoOOVLQbwqOB+6OgpBA/n466HxZkwyC8pdy5mtc0LwTMdeVtVZhNTkC0GL/+5y6E2hxjZsH9OjpudSd3opZVTDNlVlelQo4EUzSOSGUFPDi4K/cKnlMe2TcvnUPoMNlo5egHMkSNButpvNE+RghF1u2pDhEcXkXiqoS/NZMeO80jQV2Kq0VHzzTsAar65mSBH2N0Ny1sYyQwI1nic3HLUyQ2p6Nswwfl9XgD1uAJGhgHVtiN3wpedRnQGDuXDhws1DTBWuhDd+vWXNT6dfDVNZe2ZACe1omlNveU3is/vfnXaRBE40ntwpsuuOsqX9aVwqlRH4BkVxw8pUcFS10YoO0+Zmu20XVEiLPsydoEEe05zBCiXaOU29EFAJ3P+I4oqY9zyioKaibQ++NnpKsBeMPi47XRuGVCigrnR7XvVE62YOtm+GKNyCQPgFOUzRcqaX6zyd2kKN72E4hn4QftmFInt7+dLnIINAu/2dT4pu/zuf5DT2tz5q9J2tj5qWeKW7i/VRqXrhBsj5uUkP66NCAaXrcLK+pccNEO1wbvz2PZTX1pevguS2Z+Or7o5p6wEzlqKCXFQeLjteww1a5XhXqW7qeBTd/o53NVZdd/3U6IPt+knUIP12u7fKEcnpqR1x2SgGD53OfcgMri7o9i0GtQUm7YWrPL5pxirHvA8nPeSgp+1h/1utzjL0inABmIyCSBDhUFBWWg6/eV5fU1YaiwvPeNp0xwOYht6He1vu0G6ApuXVYvXYvhlhOJPGCKApMkH8bf3UbwV3RvEfQ05TAAplbmRzdHJlYW0KZW5kb2JqCjMgMCBvYmoKPDwKL0xlbmd0aCA1NTI4Ci9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nM1dXY8kq5F9719Rf8Bcvkmk0TxY67W0b2vNm+WH7uouy5ZsaX2l3b+/QAJJVmWeHNtEJroqT7vm6nI64BwigiAQNx7++Y0I/zNpwSZ/u//tTdz+7+2Pfwpffb7x21/ftPTMKi/szTnOlDM3I9mknTX2JgxzYgpf/ePr7Zf/+Prfv9y//vD7397uv75x5o30WghnteNmmuTt57769f73t19+/6u4/fnXt8czFmUnpp3hfqpgrGDCam7V+WCCxbgwVpoKRjqmjdfufDAyItDecl/BhD+stVycP01CTkwaNwlVwQgtGffCeXc6Gs0riqniOh3EwiI1MT8OjQqaMXhU0IxBpIJmDCYVNBdTqcAYgkvcsVGIlKCMwaIEZQwKJShj8CdBuZg8CcMIzLHOMh4XyxDkqWiG4E9FMwSFKpohWFTRXEukCmMILmnNxqFSBjMGkzKYMYiUwYzBowzmYhplFEOwSCiWRh6DRgXNGDwqaMYgUkEzBpMKmoupVGCMwCUzCTYKkzKWIXiUsQzBooxlCA5lLNcyKIMYgj8mTJMYxaWraMbgUEEzBosKmjF4VNBczKQCYwguiYkN49YVMGMwKYMZg0gZzBg8ymAuplFGMQKL9BQWyTCnRhXNEDyqaIYgUkUzBJMqmmupVGEMwaVAnjFoZOwgDApAxiCPsYPwJq6MaykTEAzBFqmZGSa9XdGMwZuCZgzyFDRjMKiguZhGBcYIXFJesWHSCgXMEEwqYIYgUgEzBI8KmGtpVFAMwaKwYu0wKYWKZgweFTRjEKmgGYNJBc3FVCowhuCS4myYDWnGMgaPZixjsGjGMgaHZiwXM2gGMQJ/pJ/YNIxLV9EMwaGKZggWVTRD8KiiuZZJFcYQXLJunPtFBcwYTMpgxiBSBjMGjzKYi2mUUQzBImUGul9U0YzBo4JmDCIVNGMwqaC5mEoFxhBc4nqU+0UzlDFYlKCMQaEEZQz+JCgXkydhGIE5wqmB7hdVNEPwp6IZgkIVzRAsqmiuJVKFMQSXwtIZh0oZzBhMymDGIFIGMwaPMpiLaZRRDMEiwQe6X1TRjMGjgmYMIhU0YzCpoLmYSgXGiVx6+o/e4j/x33i8/c+buG20Dvrtj/y9CCtJsbCLau1ubnLMO+Unffvxt7df/lME5Lcfj7c/fuOci/CR4aO+3/ifbj/+6+13P97++98b2TvGnbf+YGQdPib/acPHdUMQb/rb4EAobZSoI/M68hQ+Po7Y9fc2nkk98WhwNHj8ld/D5yN87uHzGT5f4fNoTHLP/04/cEIoFheD/El0HUeWioUFbvWER9Z5Usy8JnsiCMJhfNpxDhGEWRCiLMi+KKxjIkAxB3Yg+P1dGNnJyUg4spDzyhNhZMOUE9zr27csD58ZWZkl3VcyZNgKrVfqaIa+3zyLExn2zG+ZJx1RKMm0sN4f2MlknhSb2O4zJoOWOCW0NhiJm2cnrViXEb3nn7OWxFntjC4ACz7dZN2BndyicEKFz7RCsOsoesWkf64vsgFGCgg39tpl/1RpP1W3eDytPRfBnXGTVkK58JVa77XK/LSzuCBqvMULEC0O44Ko8RjPR9Q4jQuixms8H1HjOC6IWs/xfEjBG1mgNN7jCUjW/1kR94KfZnnYrkzYr+KEKsvUlDybRHIZvIlX/3GW512J+ecGD3PFuDRCWzy6+X4LfztrxLdm2y4e3pS/e++GTPrgtgdxNQoje3bv+tkmyO3Srw8geGTh7ziy02zi1moBR07bkc9bk/x++43UYV8NqinTHPnZxeiESXPBzGS90BiTzuvUd7WIDsoSPSuB16nILEmuQ7HKxFwSgGgUYbsGP5Kz4GiGNbKxK/u199IzAHJMKqvkhAGYEuy8OnMdzSCCM1c6zx2YI7lIxfm12YXKwcD8cz/HLjCotOYDqHhwlfTEVIycdTVOhBIWjguyr6Xw8/f9vLpGVpDBPuKn36hO1g6OaNT4+3/mT5BT8ZjZHD/x5/T3X/04pFTpcAlASZFByBmADHug1AVoV0JzzRyPjucOod+zwrm832XHP63gfgSfUsZKxEgeAJKF4F953XISUiOTuCWXkEzy0TeeDkLntZWTx3YIEyHdk8i5LDd2idDk9OTE9M07SB62qCkgwTaTfraTfM9Lp8STPeProDDGBP8BI/mYZzDZL6CS/TSulV+E4HPJwcmv8jOJ0iIUUeP6bT2t0iLuNEqr+KK06btH+Z5CaZEpitKqNcD0/z8pFNeGQM1PiZLbPgNffMmXlFTfBGpYs9wE+moMys3egIiBogp+ZUQj++XUG+lFMJRofKiSyu4rI8IGQfPaSwmRvPpQWXu5X+DVTdJ21lzHphAMWHewkNaZ1ror+GI4CuWDiMre3XcJS6+YC36DxmtYBe9WSGa9UWGZfZsNIDwzqZnStwVamracKJ89dwpxRlCjzyclhTjD6dkQ56SDrVD3s8UiztAUYvbJk0noBNkpJm0MmDfWjdpeKmnvzkeLKw+r74GnCxy2Isa9EKSeQXomXTyXra6e60r0VqcBGlG0+WOWHFWIv4Ww51GUZtbwsKYgOmmbmKYcSfa3VSuKCM1p7iBEkfhNojhondwbhbm/uoPp537pgEZxkClEGXgJvIlUJ4R6xvDN8L+ozgtdyjJx/V3BMJaVYX/EwOz3eLogoiSJooWJSu+kcgMgDSA3AN0FcnNkK1ofDC2ewXwwZKi+Sb9WEY9G/XxKP37mLGD5uV/Y3igiWsGiAVb8sKBBynVXxPi9Epth+qyITd7NcopKGutZJJGaMJicB60gmrRkit777uKtECJUE1/OF1yWnC2GEZaCTTI3TAE4owjFoo0aNBfrlexytCDJuQi0Xq7aU30PrqS3zMWaBYlHD6tFCebUVPbVUt1C4wwCKInjNM4gWhR7zuB9kT6a8BOZQsyOYDp5oXUGZXSq5HbqIktfzX31LU2zsU3ApIzHIDxfjv09ibSB0Qfw8QC6C3w8hOa8kBLN2AeZA4VG3XGgkq9SVIQkpEQT0p7lfswqMjtP3VVEcKa82o62XaaMyyR+8N7Z8xBWOy6EkxCICvaXYTZnN+vZ66c5VUBw7tsyojoGsjp4lSKVdsIZav2gMks9xcwyq6NFDlGksmOVvbRsn6dKKxr/DM0TvX+GRj/ZP0MTdCfzzw5GfVbWV1+NQlkBqJqsa0LTmMvsH5oaHzwBt1NAVJR1K9bq6Kcpxo1wZsJgyrWnp3OJzUqQKLokFTSHCAllzsTjpFQqCFHkg/Y0eg06OXFhHkJEL3Bw9HMFDkFJpxEk559wQRwI3KwxBAIHTVFcxyxqSeAkhesYeMu8ATVvLkcT5WZS53NOzry18RYFAqK+nhPIbSaw3+S0SobsYhYdJSxI8d4EpxobJsrXY12/GxV/Vc/7aBLqZRYV737fTojiZkLEj5/coyzPNwcp42mEVNp1cJBYkKN8UUqSngKI+S4jiXCi9Xjv6oO1wnk06uerWKYMQK6fJqnqg5MmZtGcS8qXzN0snl2FM/hlUoHCOb+dqqrHuF2r+cJijBsJApVEIAf/tdSw+GMklyQQGpk1VDwfSvSvnIuNf+aXHwGepEtmOyWQlHOrKJNGk9DCIj/HhTYa6xwXGuqDzJdEo+5cKWkrW2h8ScQ0Ueq+eS1wJvIldbzfuVP0U25ut+e3BNUshlmjZfQnAZioOfP23f3UsZU/gGAgx03EKzipGhwjHstxQ0uNXiSRnQYTSWSoj64eWiuSaNSdguP2Zojq2LhnEUkAqi12KQE3zVlNWAds4juFUo+8PDpfoo1tAbwWGg4uSw8Ssvt18Fe/b0hJkefiG743EtPTP4wOn0+bBrLPZx49VYGTiBoafaxoFCBNqtKPv62qoFF3otGqMHcq1wtNmijHNrP7RagqwjI+7dQYPZ4o1Pd8wgkWfs94potAiFIfYZoQmKRmGKJwfOmttBXRfS3K01VhJiZlvF92aKOrjmDhAqI/oUCjn3xCgSboTnV99mjUzergRtpoHCYAqp5QZL2lK5ELi44pu1Od9liWA0GXO2uY43PqCIFoqoKJpQ2gOPF4FaG45nj1yC5l03l/jWE7Hh+1KoIQRT6TtDs5HLWoxuri/aIiPYm7qAgAVe550p5xhpidWb1T4FMK0Ytz1FE9BIstSoMvjgDUKpLcsLVeJ++XDmgUBCL5Sfejr67EmxfecQehnV9pe2Qo2gQTXrFDJZjgtJG1G4HTs3ejXXPa21TQFOVGO2kYqG3YpXdaS1Wla0/aOqqdC75i6osBQaAb6yQ3pxCa85o6IRTSr7Plu/e2igD2bCqiynnJEcITGjn52NB0wkhKIyfPCRs5wTVDL/6QxGOJP5qqKLUkN0Hg9Oy4uaUqZY5ZKcQfmaJxc2l7TGmjmNzpeKVKHPa5o8G6u3cXGy7EA02JgdlF4Mol1jn3T7IhACTXte5EqC5u3YmgzXebSEiOpmkvI1Y8PKqMGDRFyYi1dRaa4lKCVo7pvX5XaaU+FxY0/Orq8ekUF0JAS8v+zUgy/K0WJDQHmDb7uNUgvKcD6PNzPgjN+mCRNKhF80Tv16BlO5hfgwxFVm17OCooLSM734Qrt1RNfFBXTQQpYw51QTtd8hAgMa/L1dYs12lnukuvENlQRVwI6Vj1DhDpg0wPwKhbcc6q9qGvSjV6gEwhcsbtvmjB7AJ11QJuGd9rNaWXxVvbBnVOdE3xLZr4ohMCIsvgJajq+xxdy3eAoj4OwZvIqjQr6lthH6sE7AEan3nccHl1SLiZ/Dqr7h4uK/IKCDj6uRUQEIqjKhmFC2cvo/9oHSEKsUMMb5wf2qx+WH0hVtmuw4i0WY79SXvSRudHWSMVBLR9V7Op3kwKQHJnE8E6984mQrK6h/X++pYDZbo/Pjg7vxcJp7BtF1NeaOzf57PRXoTmvL5GEMWDKtEFV+1Bd7T0M8UbNdAUpTsa58R9jeLjYHanu1ISvnIO9cXXR5t971dOJvDaxob6CFBs11a77dvubGl1DqC44l4ltMow9yqh1c7TGISCrFwCjbqXTG/vcZOcmEFTiCarVJrP3imS6cpM4fvtoo2Vxny+LlTZN10c5suL2HYQglpdLi/rlVZvEJrTyk4hikvKTn/KLmTpdDT6YOl0CJUsooTTs9e84ovTdv1BoNo3CIgjSq2Y2Ok9lESvzf12jCAtk4HREo6f1i5nTksxTfUJoxK6lR7XJKUBCFYJIOfb2DvVE7nfx+od1o6xbqs9R1Bptedo/gbSHgCV7v0TOD17JUrULRWhKZqWipH4dN3+VXpefruGomjPXp/n3g6XY8JLNaljULkOUzZXkDteAm41CCDZrMPsmcASTOh0URyhqAmsHFbWV2JpwjaA5AShA6OPJnQIqiMTOjQ9ezULjdDROFmIQ01X7qT/dE6WiGc2+29alts+4p2v2xx0fV1ZiPAbGgwGvHXXdgpThkTwELJxskcIJfnpIBz93NNBBCVyiqb1Axp1S2ZOaf0ATSEWiam3G0mqQeMy4ajNVAmdOpdBaCb1xLXDAEpU1Fzao4ndEIrz/CaE4ly/CdrjtHQ3QpESzP1+90Yt4Kg/4ZSQtC2FE1IKpz6bwqnuLUtlcAdiWLfrkLiFpLVuqXOrUsO8cgZCqTeicz8FtXr6jUQ7kGH+lZLJ9qnOzvelQ2wiPNcYNH2vKmgycn8Ijn6uPwRXMp3CoVH3HkNqfiZ5DAmuyFI00PhDNFfgZPBMzF4voVblSiVQMEV05NI9jULmzg8k5XywguDK5bO5AV9WjgeJ3h2YqF57oyuUcrEbljyerDF6pMbm9lqnTCFEPFR5PUI6Vnk9REpWeAW52OboP/hLjn7+OwoNRaYQSwxZuuUQxJN6YtNeq6vL9ROAey6MECRdnRGCK4qv4HQNkz6D82b4uuD1wVf9h9bISfQHoSN7Efxw1I3rPe0LRbKfb9DoD1rebVGWJtWggEfs9T8Sa2L17vnn2cS9cRBDdNVKxZN0fF2LRVaHBa0yluuBbEd+aAftNNahHTQUXfSIRt2pTii9FeiUB4Cq3bMk5YGdFI7pvRYun7w+o9t5K7eCBasaPkEAsHMWjdwANOdfLIRohr5YeLiszsnEI/uRPS17NOrWrb72CjNJnz44IU0dFO3lFslD0LTX8afeEdtrmkLRyN3XrMwBtNoi6YVXr00PCHQJoHt5RqLJpfV82dsYpiYXvAiIhro4fpJMCCuFwCg4v6ytPMI1WA4KIX10rSBo9RHN29HFnE+qk0pkihwDJnF+LHn8/jGgiNfu9ho4Cb68RltSPv09M+eCtiXvA4GZy6cyx8jaukAE42TKm9fEIOKhwlWE9PrsFETX95HVRpmORt3svyeJPTcEqnpuX3z19Gt/z004zuROC5zzlQmAqWvXbukBiUodmeaaxgfxiS8V3TWM8PzGBwjNebEhRCGpfB+4cncaH7Rv3tAoDDKFKOVhORtFFhsKo5nZ6TvzojC1NIp3r9bybJoFAwGqFwbLw1o0/g9AoCa+vh24m/Ph63O5vrGYjSegCKcoPlc5f2vjw5qvItGZA1Tnv5ADEX10ZXerOWjUI68m5aYoNAeAKpWh9cz/QaI3amLTXrsXvyzQ2lazcy25ZT4OrjCQtp8f2fk+QnDxCznQOKe/kHNkqIuDJmQr2fUIq5UXZJO9dE7bfoCiLBOaom1iR3obTkgV76JtZwOCqprgoIvYY6nkKopL01FmJIsPlcZNGoFReYJe7h6TyA1Act7zNNAe7QtpT4fjBIVNMj4X55zHpjnlRZrg0RmlJAaSH6SJVqJ7kAbOD3m1PRz93Gp7OBV0so5Iulcv0XiNJFl6aIpcKVpaZhHmwrhjaq+1z+Zu36Sh75zi0ZnpAJXi6Gy1RNG6ayjWKj7AtvkwRQ30S1WDJBFe4QKuoGVmghCFX+/VteWh54Q1FAjRiXkyZBeyt6GPRt286+MaJSLJk6EJKS0WyO/6eMHsXhsrmdcm0eOugaXSCG8QhuQ62Tw1711XZSMoAMDFAStARn9DEQ0+VOUBmj+yt2aOBt26+7L6mUBT0Iw11wdpn5uPN5T3WseYZpsjqL/ibAqcmATCkI6odh67qf1a+m6AjdQgYAPl4AHM61LwANTFD/yhWXVUHV3QHO29bdNehKEoQUfcb+ueSm9OTiFAljO51+kmL9bUMcru8Kvr653cWKsFwpQSAIZZrQOMCuKr+fPON4qtKdTpwHIXOkIA2fmJewDmYiFCE0h2JIgG3atQb/rGkOR20HppmgSXRp1E72xpZvaa6Jhlgb44yZ17BrtSTwjw1JxO2zLGUHlBCMij8XL6qnGsOc/v76CZKc+tbuTe2jDn5Le1DpbSpUeFABxdShlZZKc3eXntiiyjjAzRqE5b+rShOv8PEQKwEQplbmRzdHJlYW0KZW5kb2JqCnhyZWYKMCAyMQowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwMDI4NzggMDAwMDAgbiAKMDAwMDAwMjgyOSAwMDAwMCBuIAowMDAwMDE2MzY5IDAwMDAwIG4gCjAwMDAwMDAxNjkgMDAwMDAgbiAKMDAwMDAwMDA1OSAwMDAwMCBuIAowMDAwMDAwMDE1IDAwMDAwIG4gCjAwMDAwMDE4MzEgMDAwMDAgbiAKMDAwMDAwMjY4MiAwMDAwMCBuIAowMDAwMDE0MDgwIDAwMDAwIG4gCjAwMDAwMDA0MDggMDAwMDAgbiAKMDAwMDAwMDI5NiAwMDAwMCBuIAowMDAwMDAwNTM2IDAwMDAwIG4gCjAwMDAwMDcwNTUgMDAwMDAgbiAKMDAwMDAwMDYzMSAwMDAwMCBuIAowMDAwMDAwODk3IDAwMDAwIG4gCjAwMDAwMDI5NDIgMDAwMDAgbiAKMDAwMDAwMzc1MiAwMDAwMCBuIAowMDAwMDAxOTc5IDAwMDAwIG4gCjAwMDAwMDIyNDYgMDAwMDAgbiAKMDAwMDAwMzM5NyAwMDAwMCBuIAp0cmFpbGVyCjw8Ci9TaXplIDIxCi9Sb290IDIgMCBSCi9JbmZvIDEyIDAgUgo+PgpzdGFydHhyZWYKMjE5NzAKJSVFT0YK";
        return $return;
    }

    public function getSampleBase64Pdf2()
    {
        $return = "JVBERi0xLjcKCjEgMCBvYmogICUgZW50cnkgcG9pbnQKPDwKICAvVHlwZSAvQ2F0YWxvZwog' +
        'IC9QYWdlcyAyIDAgUgo+PgplbmRvYmoKCjIgMCBvYmoKPDwKICAvVHlwZSAvUGFnZXMKICAv' +
        'TWVkaWFCb3ggWyAwIDAgMjAwIDIwMCBdCiAgL0NvdW50IDEKICAvS2lkcyBbIDMgMCBSIF0K' +
        'Pj4KZW5kb2JqCgozIDAgb2JqCjw8CiAgL1R5cGUgL1BhZ2UKICAvUGFyZW50IDIgMCBSCiAg' +
        'L1Jlc291cmNlcyA8PAogICAgL0ZvbnQgPDwKICAgICAgL0YxIDQgMCBSIAogICAgPj4KICA+' +
        'PgogIC9Db250ZW50cyA1IDAgUgo+PgplbmRvYmoKCjQgMCBvYmoKPDwKICAvVHlwZSAvRm9u' +
        'dAogIC9TdWJ0eXBlIC9UeXBlMQogIC9CYXNlRm9udCAvVGltZXMtUm9tYW4KPj4KZW5kb2Jq' +
        'Cgo1IDAgb2JqICAlIHBhZ2UgY29udGVudAo8PAogIC9MZW5ndGggNDQKPj4Kc3RyZWFtCkJU' +
        'CjcwIDUwIFRECi9GMSAxMiBUZgooSGVsbG8sIHdvcmxkISkgVGoKRVQKZW5kc3RyZWFtCmVu' +
        'ZG9iagoKeHJlZgowIDYKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAwMDEwIDAwMDAwIG4g' +
        'CjAwMDAwMDAwNzkgMDAwMDAgbiAKMDAwMDAwMDE3MyAwMDAwMCBuIAowMDAwMDAwMzAxIDAw' +
        'MDAwIG4gCjAwMDAwMDAzODAgMDAwMDAgbiAKdHJhaWxlcgo8PAogIC9TaXplIDYKICAvUm9v' +
        'dCAxIDAgUgo+PgpzdGFydHhyZWYKNDkyCiUlRU9G";
        return $return;
    }

    public function saveUnMergeEmployerPayrollFileAttachment()
    {
        if (request()->hasFile('document_file51')) {
            $file = request()->file('document_file51');
            if ($file->isValid()) {
                //Check the excel file
                $file->move(employer_registration_dir() . DIRECTORY_SEPARATOR . 'payroll_unmerging' . DIRECTORY_SEPARATOR , "file");
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function saveMergeEmployerPayrollFileAttachment($payroll_merge_id)
    {
        if (request()->hasFile('document_file50')) {
            $file = request()->file('document_file50');
            if ($file->isValid()) {
                $file->move(employer_registration_dir().DIRECTORY_SEPARATOR.'payroll_merging'.DIRECTORY_SEPARATOR.request()->employer_id.DIRECTORY_SEPARATOR.$payroll_merge_id.DIRECTORY_SEPARATOR, $payroll_merge_id.'.'.request()->file('document_file50')->getClientOriginalExtension());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function saveContribModificationAttachments($contrib_modification_id)
    {
        if (request()->hasFile('request_attachment')) {

           //  if (request()->hasFile('receipt_attachment')) {
           //      $receipt = request()->file('receipt_attachment');
           //      if ($receipt->isValid()) {
           //         $receipt->move('contrib_modification'.DIRECTORY_SEPARATOR.request()->employer_id.DIRECTORY_SEPARATOR.$contrib_modification_id.DIRECTORY_SEPARATOR, 'receipt_attachment.'.request()->file('receipt_attachment')->getClientOriginalExtension());
           //     }
           // }

         $file = request()->file('request_attachment');
         if ($file->isValid()) {
            $file->move('contrib_modification'.DIRECTORY_SEPARATOR.request()->employer_id.DIRECTORY_SEPARATOR.$contrib_modification_id.DIRECTORY_SEPARATOR, 'request_attachment.'.request()->file('request_attachment')->getClientOriginalExtension());
            return true;
        }
    } 
    return false;

}

}
