<?php

namespace App\Services\Storage;

use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use App\Repositories\Backend\Sysdef\BusinessRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Sysdef\RegionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;

/**
 * Class DataFormat
 * @package App\Services\Storage
 */
class DataFormat
{
    /**
     * @param array $input
     * @return string
     */
    public function arrayToDb(array $input)
    {
        $serialized = base64_encode(serialize($input));
        return $serialized;
    }

    /**
     * @param $input
     * @return mixed
     */
    public function dbToArray($input)
    {
        $unserialized = unserialize(base64_decode($input));
        return $unserialized;
    }

    /**
     * @param $input
     * @return string
     */
    public function dbArrayInfo($input)
    {
        $return = "";
        $array = $this->dbToArray($input);
        foreach ($array as $key => $value) {
            switch ($key) {
                case 'business_id':
                    $business = new CodeValueRepository();
                    $list = $business->query()->where('id', $value)->get()->pluck('name')->all();
                    $return .= "Business : ";
                    $return .= implode(", ", $list);
                    $return .= "<br/>";
                    break;
                case 'region_id':
                    $region = new RegionRepository();
                    $list = $region->find($value)->pluck('name')->all();
                    $return .= "Region : ";
                    $return .= implode(", ", $list);
                    $return .= "<br/>";
                    break;
            }
        }
        return $return;
    }

    /**
     * @param $input
     * @return mixed
     */
    public function dbToArrayQuery($input)
    {
        $employer = new EmployerRepository();
        $query = $employer->query();
        $array = $this->dbToArray($input);
        foreach ($array as $key => $value) {
            switch ($key) {
                case 'business_id':
                    $business = new CodeValueRepository();
                    $list = $business->query()->where('id', $value)->get()->pluck('id')->all();
                    $query = $query->whereHas("businesses", function($subQuery) use ($list) {
//                        $subQuery->whereIn("business_id", $list);
                        $subQuery->whereIn("business_sector_cv_id", $list);
                    });
                    break;
                case 'region_id':
                    $region = new RegionRepository();
                    $list = $region->find($value)->pluck('id')->all();
                    $query = $query->whereIn("region_id", $list);
                    break;
            }
        }
        return $query;
    }

    /**
     * @param $input
     * @return array
     */
    public function dbToArrayData($input)
    {
        $output = [];
        $array = $this->dbToArray($input);
        foreach ($array as $key => $value) {
            switch ($key) {
                case 'business_id':
                    $business = new CodeValueRepository();
                    $list = $business->query()->where('id', $value)->get()->pluck('id')->all();
                    $output['business_id'] = $list;
                    break;
                case 'region_id':
                    $region = new RegionRepository();
                    $list = $region->find($value)->pluck('id')->all();
                    $output['region_id'] = $list;
                    break;
            }
        }
        return $output;
    }




/**
 * Unregistered Employers -> Follow up Type Inspection =========
 */
    /**
     * @param $input
     * @return mixed
     */
    public function dbToArrayQueryFollowup($input)
    {
        $employer = new UnregisteredEmployerRepository();
        $query = $employer->query();
        $array = $this->dbToArray($input);

        foreach ($array as $key => $value) {
            switch ($key) {
                case 'business_id':
                    $business = new CodeValueRepository();
                    $list = $business->query()->where('id', $value)->get()->pluck('name')->all();
                    $query = $query->whereIn("sector", $list);
                    break;
                case 'region_id':
                    $region = new RegionRepository();
                    $list = $region->find($value)->pluck('id')->all();
                    $query = $query->whereIn('region_id', $list );
                                     break;
                }
        }
//        /* Pick only which are no are assigned yet*/
//        $query->where('assign_to', 0);
        return $query;
    }

    /**
     * @param $input
     * @return array
     */
    public function dbToArrayDataFollowup($input)
    {
        $output = [];
        $array = $this->dbToArray($input);
        foreach ($array as $key => $value) {
            switch ($key) {
                case 'business_id':
                    $business = new CodeValueRepository();
                    $list = $business->query()->where('id', $value)->get()->pluck('id')->all();
                    $output['business_id'] = $value;
                    break;
                case 'region_id':
                    $region = new RegionRepository();
                    $list = $region->find($value)->pluck('id')->all();
                    $output['region'] = $list;
                    break;

            }
        }
        return $output;
    }

/*End of followup type inspection---------*/


}