<?php

namespace App\Services\Access;

use App\Exceptions\GeneralException;
use App\Repositories\Backend\Reporting\SlaAlertRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditEmployerRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class Access
 * @package App\Services\Access
 * @author Erick M. Chrysostom <e.chrysostom@nextbyte.co.tz>
 * @description Manages all the authentication instances and values of the logged in user
 */
class Access
{
    /**
     * @return Authenticatable|null
     */
    public function user()
    {
        return auth()->user();
    }

    /**
     * Return if the current session user is a guest or not.
     *
     * @return mixed
     */
    public function guest()
    {
        return auth()->guest();
    }

    /**
     * @return mixed
     */
    public function logout()
    {
        return auth()->logout();
    }

    /**
     * Get the currently authenticated user's id.
     *
     * @return mixed
     */
    public function id()
    {
        return auth()->id();
    }

    /**
     * @param Authenticatable $user
     * @param bool            $remember
     */
    public function login(Authenticatable $user, $remember = false)
    {
        return auth()->login($user, $remember);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function loginUsingId($id)
    {
        return auth()->loginUsingId($id);
    }



    /**
     * Checks if the current user has a Role by its name or id.
     *
     * @param string $role Role name.
     *
     * @return bool
     */
    public function hasRole($role)
    {
        $return = false;
        if ($user = $this->user()) {
            $return = $user->hasRole($role);
        }
        if (!$return) {
            if ($this->substitutingCount()) {
                $subs = $this->user()->substitutingUsers;
                foreach ($subs as $sub) {
                    if ($sub->hasRole($role)) {
                        $return = true;
                        break;
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Checks if the user has either one or more, or all of an array of roles.
     *
     * @param  $roles
     * @param bool $needsAll
     *
     * @return bool
     */
    public function hasRoles($roles, $needsAll = false)
    {
        $return = false;
        if ($user = $this->user()) {
            $return = $user->hasRoles($roles, $needsAll);
            if (!$return) {
                if ($this->substitutingCount()) {
                    $subs = $this->user()->substitutingUsers;
                    foreach ($subs as $sub) {
                        if ($sub->hasRoles($roles, $needsAll)) {
                            $return = true;
                            break;
                        }
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Check if there is no admin in the system, then allow user management to set admin
     * Check
     * @return bool
     */
    public function allowUserManagement()
    {
        if ($user = $this->user()) {
            $perm = $user->allow('manage_users');
            if (!$perm) {
                if ($this->substitutingCount()) {
                    $subs = $this->user()->substitutingUsers;
                    foreach ($subs as $sub) {
                        if ($sub->allow('manage_users')) {
                            $perm = true;
                            break;
                        }
                    }
                }
            }
            $userrepository = new UserRepository();
            $admin = $userrepository->checkAdminIfDontExists();
            return ($perm Or $admin);
        }
        return false;
    }

    /**
     * Check if user has a default workflow module defined by a workflow group
     * @param $wf_module_group_id
     * @param $level
     * @param array $param
     * @return bool
     * @throws GeneralException
     */
    public function hasWorkflowDefinition($wf_module_group_id, $level, array $param = [])
    {
        $return = false;
        if ($user = $this->user()) {

            $return = $user->hasWorkflowDefinition($wf_module_group_id, $level);
        }
        if (!$return) {
            throw new GeneralException(trans('auth.workflow_error'));
        }
        return $return;
    }

    /**
     * Check if user has a specific workflow module specified by workflow module group and type
     * @param $wf_module_group_id
     * @param $type
     * @param $level
     * @return bool
     * @throws GeneralException
     */
    public function hasWorkflowModuleDefinition($wf_module_group_id, $type, $level)
    {
        $return = false;

        if ($user = $this->user()) {
            $return = $user->hasWorkflowModuleDefinition($wf_module_group_id, $type, $level);
        }
        if (!$return) {
            throw new GeneralException(trans('auth.workflow_error'));
        }
        return $return;
    }

    /**
     * Check if the current user has a permission by its name or id.
     *
     * @param string $permission Permission name or id.
     *
     * @param int $issensitive
     * @return bool
     */
    public function allow($permission, $issensitive = 0)
    {
        $return = false;
        if ($user = $this->user()) {
            $return = $user->allow($permission, $issensitive);
            if (!$return) {
                if ($this->substitutingCount()) {
                    $subs = $this->user()->substitutingUsers;
                    foreach ($subs as $sub) {
                        if ($sub->allow($permission, $issensitive)) {
                            $return = true;
                            break;
                        }
                    }
                }
            }
        }

        return $return;
    }

    /**
     * @param $permission
     * @return \Illuminate\Http\JsonResponse
     */
    public function allowWithThrow($permission)
    {
        $return = $this->allow($permission);
        if (! $return) {
            if (request()->ajax()) {
                //return response(trans('auth.general_error'));
                return response()->json(['success' => false, 'message' => trans('auth.general_error')]);
            }
            return redirect()
            ->back()
            ->withFlashDanger(trans('auth.general_error'));
        }
    }

    /**
     * @param $permission
     * @return bool
     */
    public function allowOnly($permission)
    {
        $return = false;
        if ($user = $this->user()) {
            $return = $user->allowOnly($permission);
            if (!$return) {
                if ($this->substitutingCount()) {
                    $subs = $this->user()->substitutingUsers;
                    foreach ($subs as $sub) {
                        if ($sub->allowOnly($permission)) {
                            $return = true;
                            break;
                        }
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Check an array of permissions and whether or not all are required to continue.
     *
     * @param  $permissions
     * @param  $needsAll
     *
     * @return bool
     */
    public function allowMultiple($permissions, $needsAll = false)
    {
        $return = false;
        if ($user = $this->user()) {
            $return = $user->allowMultiple($permissions, $needsAll);
            if (!$return) {
                if ($this->substitutingCount()) {
                    $subs = $this->user()->substitutingUsers;
                    foreach ($subs as $sub) {
                        if ($sub->allowMultiple($permissions, $needsAll)) {
                            $return = true;
                            break;
                        }
                    }
                }
            }
        }

        return $return;
    }

    /**
     * @param  $permission
     *
     * @return bool
     */
    public function hasPermission($permission)
    {
        return $this->allow($permission);
    }

    /**
     * @param  $permissions
     * @param  $needsAll
     *
     * @return bool
     */
    public function hasPermissions($permissions, $needsAll = false)
    {
        return $this->allowMultiple($permissions, $needsAll);
    }

    /**
     * @return mixed
     */
    public function getWorkflowPendingCount()
    {
        return (new WfTrackRepository())->getPendingCount();
    }

    /**
     * @param int $hasnotify
     * @return mixed
     */
    public function getMyWorkflowPendingCount($hasnotify = 0)
    {
        return (new WfTrackRepository())->getMyPendingCount($hasnotify);
    }

    /**
     * @return bool
     */
    public function isSubstituted()
    {
        return ($this->user()->substitution()->count() > 0);
    }

    /**
     * @return mixed
     */
    public function substitutingCount()
    {
        return $this->user()->substitutingUsers()->count();
    }

    public function alternateCount()
    {
        return $this->user()->alternates()->count();
    }

    /**
     * @return string
     */
    public function substitutingUsersList()
    {
        $users = $this->user()->substitutingUsers;
        $return = "";
        foreach ($users as $user) {
            $return .= $user->name . ", ";
        }
        return $return;
    }

    /**
     * Return all users including user substituting
     * @return array
     */
    public function allUsers()
    {
        $return = [];
        $user = $this->user();
        if ($this->substitutingCount()) {
            $return = $user->substitutingUsers()->select(['id'])->pluck("id")->toArray();
        }
        $return[] = $this->id();
        return $return;
    }

    /**
     * @return mixed
     */
    public function allWfDefinitions()
    {
        $return = [];
        $user = $this->user();
        //dd($this->substitutingCount());
        if ($this->substitutingCount()) {
            $others = $user->substitutingUsers;
            //dd($others);
            foreach ($others as $other) {
                $return = array_merge($return, $other->workflowDefinitions()->pluck("wf_definitions.id")->all());
            }
        }

        $return = array_merge($return, $user->workflowDefinitions()->pluck("wf_definitions.id")->all());
        return $return;
    }

    public function allSubordinateWfDefinitions($user_id)
    {
        $return = [];
        $user = $this->user()->find($user_id);
        if (count($user->subordinates)) {
            $others = $user->subordinates;
            foreach ($others as $other) {
                $return = array_merge($return, $other->workflowDefinitions()->pluck("wf_definitions.id")->all());
            }
        }

        $return = array_merge($return, $user->workflowDefinitions()->pluck("wf_definitions.id")->all());
        return $return;
    }

    /**
     * @description All users who this user is acting as alternative
     * @return array
     */
    public function alternateUsers()
    {
        $return = [];
        $user = $this->user();
        if ($this->alternateCount()) {
            $return = $user->alternates()->select(['user_id'])->pluck("user_id")->toArray();
        }
        return $return;
    }

    /**
     * @return mixed
     */
    public function getMyCheckerPendingCount()
    {
        return (new CheckerRepository())->getMyPendingCount();
    }
    /**
     * @return mixed
     */
    public function getMyCheckerPendingNotifyCount()
    {
        return (new CheckerRepository())->getMyPendingNotifyCount();
    }

    /**
     * @return mixed
     */
    public function getMyCheckerPendingShouldCount()
    {
        return (new CheckerRepository())->getMyPendingShouldCount();
    }

    public function checkMySlaCount()
    {
        return (new SlaAlertRepository())->checkCount();
    }

    /**
     * @param null $moduleid
     * @return mixed
     */
    public function getMyReadyToUnAchiveWorkflows($moduleid = NULL)
    {
        $query = DB::table('wf_track_general')->where([
            'user_id' => $this->id(),
            'status' => 6,
            'unarchiveready' => 1,
        ]);
        if ($moduleid) {
            $query->where(['wf_module_id' => $moduleid]);
        }
        return $query->count();
    }

    /**
     * @param Model $incident
     * @throws GeneralException
     */
    public function assignedForNotification(Model $incident)
    {
        //if (!env('TESTING_MODE')) {
        if (!(in_array($incident->allocated, $this->allUsers()))) {
            throw new GeneralException("Action cancelled! Only allocated user can perform the requested action...");
        }
        //}
    }

    /**
     * @param Model $inspection
     * @throws GeneralException
     */
    public function assignedForInspection()
    {
        $codeValue = (new CodeValueRepository())->query()->where("reference", "CDEMPLYIMSR")->first();  //Employer Inspection Master
        $inspection_masters = $codeValue->users()->pluck("users.id")->all();
        if (!count(array_intersect($inspection_masters, $this->allUsers()))) {
            throw new GeneralException("Action cancelled! Only allocated inspection master can perform the requested action...");
        }
    }


    /**
     * @param Model $employerTask
     * @throws GeneralException
     */
    public function assignedForEmployerInspectionTask(Model $employerTask)
    {
        if (!(in_array($employerTask->allocated, $this->allUsers()))) {
            throw new GeneralException("Action cancelled! Only allocated user can perform the requested action...");
        }
    }


    public function getMyPendingOshAuditCount()
    {
        return (new OshAuditEmployerRepository())->getMyPendingOshAuditCount();
    }


    public function getReversedWorkflowPendingCount()
    {
        return (new WfTrackRepository())->getReversedCount();
    }

    public function getSubornitesPendingCount()
    {
        return (new WfTrackRepository())->getSubornitesPendingCount();
    }
}
