<?php

namespace App\Services\Finance\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class FinYear
 * @package App\Services\Finance\Facades
 * For querying finance years from the database.
 */
class FinYear extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'fin_year';
    }
}