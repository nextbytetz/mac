<?php

namespace App\Services\Finance;
/*
 * class ProcessPaymentPerBenefit
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to process claim compensation payments per benefit initiated
 */

use App\Jobs\Claim\ClaimPaymentExportToErp;
use App\Jobs\Claim\ClaimPaymentPerBenefitExportToErp;
use App\Repositories\Backend\Finance\PaymentVoucherRepository;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\MacErp\ClaimsErpApiRepositroy;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProcessPaymentPerBenefit {
    /**
     *
     * @param string $model
     *
     * @return void
     */

    protected $payment_voucher_transactions;
    protected $payment_vouchers;
    protected $user_id;
    protected $notification_eligible_benefit_id;
    protected $claim_compensations;
    protected $notification_workflow_id;



    public function __construct($notification_workflow_id, $user_id)
    {

        $this->claim_compensations = new ClaimCompensationRepository();
        $this->payment_voucher_transactions = new PaymentVoucherTransactionRepository();
        $this->payment_vouchers = new PaymentVoucherRepository();
//        $this->notification_eligible_benefit_id = $notification_eligible_benefit_id;
        $this->user_id = $user_id;
        $this->notification_workflow_id = $notification_workflow_id;
    }


    /*Process all payments per benefit approval*/
    public function processPayment()
    {
        DB::transaction(function ()   {
            $this->generatePVTransactions();
            $this->generatePV();
            $this->postApErpApi();
        });
    }




    /**
     * Generate PV transactions from claim compensation payments.
     */
    public function generatePVTransactions(){
        $notification_workflow_id = $this->notification_workflow_id;
        $user_id = $this->user_id;
        $pending_compensation_claims_query = $this->claim_compensations->query()->where('ispaid', 0)->whereHas('notificationEligibleBenefit', function($query) use($notification_workflow_id){
            $query->where('notification_workflow_id', $notification_workflow_id);
        });
        if($pending_compensation_claims_query->count() > 0){
            $this->payment_voucher_transactions->processCompensationPayment($pending_compensation_claims_query->get(), $user_id);
        }
    }


    /**
     * Generate Payment voucher from pv trans generated per benefit
     */
    public function generatePV(){
        $notification_workflow_id = $this->notification_workflow_id;
        $user_id = $this->user_id;
        $pending_pv_trans_query = $this->payment_voucher_transactions->getDistinctPendingPVTran()->whereHas('claimCompensation', function($query) use($notification_workflow_id){
            $query->whereHas('notificationEligibleBenefit', function($query)use($notification_workflow_id){
                $query->where('notification_workflow_id', $notification_workflow_id);
            });
        });
        if($pending_pv_trans_query->count()){
            $this->payment_vouchers->processPaymentVoucherTransactionIndividualPerBenefit($pending_pv_trans_query->get(),null, $user_id, $notification_workflow_id);
        }
    }


    /**
     * Post to AP ERP Api
     */
    public function postApErpApi()
    {
        $notification_workflow_id = $this->notification_workflow_id;
//        $claim_erp_api_repo = new ClaimsErpApiRepositroy();
//        $claim_erp_api_repo->postApErpApi($notification_workflow_id);
        dispatch(new ClaimPaymentPerBenefitExportToErp($notification_workflow_id));
    }


}