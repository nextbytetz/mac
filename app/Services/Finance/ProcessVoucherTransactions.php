<?php

namespace App\Services\Finance;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to process payment vouchers for claim compensation.
 */

use App\Exceptions\GeneralException;
use App\Repositories\Backend\Finance\PaymentVoucherRepository;
use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProcessVoucherTransactions {
    /**
     * Services for calculating booking interest for each employer - Late contribution.
     *
     * @param string $model
     *
     * @return void
     */

    protected $payroll_procs;
    protected $payment_vouchers;
    protected $payment_voucher_transactions;

    public function __construct()
    {

        $this->payroll_procs = new PayrollProcRepository();
        $this->payment_vouchers = new PaymentVoucherRepository();
        $this->payment_voucher_transactions = new PaymentVoucherTransactionRepository();
    }

    /**
     * Process Payment
     */
    public function processPaymentVouchers($process_type, $user_id){
        return DB::transaction(function () use($process_type, $user_id){
            $payroll_proc_id =   $this->payment_vouchers->payrollProc($process_type, $user_id)->id;
            if ($process_type == 1){
                $this->checkIfCanProcess($process_type);
                $this->payment_vouchers->processPaymentVoucherTransactionIndividualChunk($payroll_proc_id, $user_id);
            }else if ($process_type == 2){
                $this->checkIfCanProcess($process_type);
                $this->payment_vouchers->processPaymentVoucherTransactionBatchChunk($payroll_proc_id, $user_id);
            }

        });
    }

    public function checkIfCanProcess($process_type){
        if ($process_type == 1) {
            if (!$this->payment_voucher_transactions->pendingIndividual()->first()) {
                throw new GeneralException(trans('exceptions.general.nothing_to_process'));
            }
        }

        if ($process_type == 2) {
            if ($this->payment_voucher_transactions->pendingBatch()->first()) {
                throw new GeneralException(trans('exceptions.general.nothing_to_process'));
            }
        }

    }



}