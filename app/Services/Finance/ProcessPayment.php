<?php

namespace App\Services\Finance;
/*
 * class CalculateInterest
 * @author: Martin Luhanjo <m.luhanjo@nextbyte.co.tz>
 * Class to process claim compensation payments
 */

use App\Repositories\Backend\Finance\PaymentVoucherTransactionRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProcessPayment {
    /**
     * Services for calculating booking interest for each employer - Late contribution.
     *
     * @param string $model
     *
     * @return void
     */

    protected $payroll_procs;
    protected $payment_voucher_transactions;
    protected $user_id;



    public function __construct()
    {

        $this->payroll_procs = new PayrollProcRepository();
        $this->payment_voucher_transactions = new PaymentVoucherTransactionRepository();

    }

    /**
     * Process Payment
     */
    public function processPayment($user_id){

        return DB::transaction(function () use($user_id) {

            $this->payment_voucher_transactions->processPayment($user_id);
        });
    }





}