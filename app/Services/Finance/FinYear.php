<?php

namespace App\Services\Finance;

use App\Repositories\Backend\Finance\FinYearRepository;
use Carbon\Carbon;

/**
 * Class FinYear
 *
 * To Query the information about the financial year
 *
 * @package App\Services\Finance
 * @author Erick Chrysostom <e.chrysostom@nextbyte.co.tz>
 * @category   MAC
 * @package    App\Services\Finance
 * @subpackage None
 * @copyright  Copyright (c) Workers Compensation Fund (WCF) Tanzania
 * @license    Not Applicable
 * @version    Release: 1.01
 * @link       None
 * @since      Class available since Release 1.0.0
 */
class FinYear
{

    public $id;

    public $finYearRepo;

    public function __construct($id = -1)
    {
        if (request()->has("fin_year_id")) {
            $this->id = request()->input("fin_year_id");
        } else {
            $this->id = $id;
        }
        $this->finYearRepo = new FinYearRepository();
    }

    public function getAll()
    {
        return $this->finYearRepo->query()->where("isactive", 1)->orderBy("id", "desc")->get()->pluck('name', 'id');
    }

    public function currentFinYearID()
    {
        $data = $this->finYearRepo->query()->select(["id"])->orderBy("id", "desc")->first();
        return $data->id;
    }

    public function queryData()
    {
        switch ($this->id) {
            case -1:
                //Return the current financial year
                $return = $this->finYearRepo->query()->orderBy("id", "desc")->first();
                break;
            default:
                $return = $this->finYearRepo->query()->where("id", $this->id)->orderBy("id", "desc")->first();
                if (!$return) {
                    //Return the current financial year
                    $return = $this->finYearRepo->query()->orderBy("id", "desc")->first();
                }
                break;
        }
        return $return;
    }

    public function queryPrevData()
    {
        switch ($this->id) {
            case -1:
                //Return for the current financial year
                $return = $this->finYearRepo->query()->where("id", "<", $this->currentFinYearID())->orderBy("id", "desc")->first();
                break;
            default:
                //Return for the specified financial year
                $return = $this->finYearRepo->query()->where("id", "<", $this->id)->orderBy("id", "desc")->first();
                if (!$return) {
                    //Return for the current financial year
                    $return = $this->finYearRepo->query()->where("id", "<", $this->currentFinYearID())->orderBy("id", "desc")->first();
                }
                break;
        }
        return $return;
    }

    public function start()
    {
        if ($this->queryData())
            return $this->queryData()->start;
        return "";
    }

    public function startDate()
    {
        if ($this->queryData())
            return $this->queryData()->start_date;
        return "";
    }

    public function end()
    {
        if ($this->queryData())
            return $this->queryData()->end;
        return "";
    }

    public function endDate()
    {
        if ($this->queryData())
            return $this->queryData()->end_date;
        return "";
    }

    public function year()
    {
        if ($this->queryData())
            return $this->queryData()->name;
        return "";
    }

    public function finYearID()
    {
        return $this->queryData()->id;
    }

    public function prevFinYearID()
    {
        return $this->queryPrevData()->id;
    }

    public function prevEndDate()
    {
        if ($this->queryPrevData())
            return $this->queryPrevData()->end_date;
        return "";
    }

    public function prevYear()
    {
        return $this->queryPrevData()->name;
    }

    public function createNew()
    {
        $date = Carbon::now();
        $month = $date->format("n");
        $year = $date->format("Y");
        $query = $this->finYearRepo->query()->orderBy("id", "desc");
        if ($query->count()) {
            if ($month > 6) {
                $query = $this->finYearRepo->query()->where("start", $year);
                if (!$query->count()) {
                    $this->finYearRepo->query()->create([
                        'name' => $year . "/" . ($year + 1),
                        'start' => $year,
                        'end' => ($year + 1),
                        'start_date' => $year . '-07-01',
                        'end_date' => ($year + 1) . '-06-30',
                        'isactive' => 1,
                    ]);
                }
            }
        } else {
            for ($x = 2015; $x <= $year; $x++) {
                $isactive = 1;
                if ($x <= 2016) {
                    $isactive = 0;
                }
                $this->finYearRepo->query()->create([
                    'name' => $x . "/" . ($x + 1),
                    'start' => $x,
                    'end' => ($x + 1),
                    'start_date' => $x . '-07-01',
                    'end_date' => ($x + 1) . '-06-30',
                    'isactive' => $isactive,
                ]);
            }
        }
    }

}