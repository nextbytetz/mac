<?php

namespace App\DataTables\Finance;


use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MedicalPractitionerRepository;
use Yajra\Datatables\Services\DataTable;

class GetPendingProcessCompensationsDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        $claim_compensations = new ClaimCompensationRepository();
        return $this->datatables
            ->eloquent($this->query())
        ->addColumn('claim_id', function($claim_compensation) {
                return $claim_compensation->claim_id;
            })
            ->addColumn('payee', function($claim_compensation) {
                return $claim_compensation->compensated_entity_name;
            })
            ->addColumn('amount', function($claim_compensation) use($claim_compensations) {
                return $claim_compensations->query()->where('claim_id',$claim_compensation->claim_id)->where('member_type_id',$claim_compensation->member_type_id)->where('resource_id',$claim_compensation->resource_id)->where('ispaid',0)->sum('amount');
            });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $claim_compensations = new ClaimCompensationRepository();
//        $query = $claim_compensations->query();
        $query = $claim_compensations->query()->distinct('claim_id')->select(['claim_id','member_type_id', 'resource_id'])->where('ispaid', 0)->whereNotNull('notification_eligible_benefit_id');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(route('backend.finance.payment.get_pending_process_compensations', []))
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => true,
                'info' => false,

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'claim_id', 'name' => 'claim_id', 'title' => trans('labels.backend.claim.claim_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'payee', 'name' => 'payee', 'title' => trans('labels.general.payee'),'orderable' => true, 'searchable' => true],

            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.general.amount'), 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pendingprocesscompensations_' . time();
    }
}
