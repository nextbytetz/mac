<?php

namespace App\DataTables\Finance\Receipt;

use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use Yajra\Datatables\Services\DataTable;

class LegacyReceiptDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('paid_by', function($legacy_receipt) {
                return (isset($legacy_receipt->payer) And ($legacy_receipt->payer)) ?  $legacy_receipt->payer : ($legacy_receipt->employer()->count()) ? $legacy_receipt->employer->name : ' ';
            })
            ->addColumn('receipt_no', function($legacy_receipt) {
                return $legacy_receipt->rctno_formatted;
            })
            ->addColumn('payment_type', function($legacy_receipt) {
                return ($legacy_receipt->paymentType()->count()) ?   ($legacy_receipt->payment_type_id == 2 ? $legacy_receipt->paymentType->name . '-' . $legacy_receipt->chequeno : $legacy_receipt->paymentType->name) : ' ' ;
            })
            ->addColumn('amount_formatted', function($legacy_receipt) {
                return $legacy_receipt->amount_formatted;
            })
            ->addColumn('payment_date', function($legacy_receipt) {
                return $legacy_receipt->rct_date_formatted;
            })
            ->addColumn('receipt_date', function($legacy_receipt) {
                return $legacy_receipt->created_at_formatted;
            })
            ->addColumn('status', function($legacy_receipt) {
                return $legacy_receipt->status_label;
            })
            ->rawColumns(['status']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $legacy_receipts = new LegacyReceiptRepository();
        $query = $legacy_receipts->query()->whereNotNull('rctno');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => true,
                'info' => false,

                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/finance/legacy_receipt/' + aData['id'] + '/edit';
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'receipt_no', 'name' => 'rctno', 'title' => trans('labels.backend.finance.receipt.receipt_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'paid_by', 'name' => 'payer', 'title' => trans('labels.backend.finance.receipt.payer'),'orderable' => true, 'searchable' => true],

            ['data' => 'description', 'name' => 'description', 'title' => trans('labels.backend.finance.receipt.pay_for'), 'orderable' => false, 'searchable' => true],

            ['data' => 'amount_formatted', 'name' => 'amount', 'title' => trans('labels.general.amount'), 'orderable' => true, 'searchable' => true],
            ['data' => 'payment_type', 'name' => 'payment_type_id', 'title' => trans('labels.backend.finance.receipt.payment_type'), 'orderable' => false, 'searchable' => true],
            ['data' => 'payment_date', 'name' => 'rct_date', 'title' => trans('labels.backend.finance.receipt.rct_date'), 'orderable' => true, 'searchable' => true],
            ['data' => 'receipt_date', 'name' => 'created_at', 'title' => trans('labels.backend.finance.receipt.captured_date'), 'orderable' => true, 'searchable' => true],
            ['data' => 'status', 'name' => 'status', 'title' => trans('labels.general.status'), 'orderable' => false, 'searchable' => true],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'legacyreceipts' . time();
    }
}
