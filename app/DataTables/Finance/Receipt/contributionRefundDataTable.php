<?php

namespace App\DataTables\Finance\Receipt;

use App\User;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Log;


class contributionRefundDataTable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
       return $this->datatables->of($this->query())
       ->addColumn('file', function ($query) {
        if($query->is_posted_erp == false){
          return '<a  class="btn btn-sm btn-success text-white" href="'.route('backend.finance.post.contrib.refund', $query->id).'">
          <i class="fa fa-paper-plane-o"> </i> Post to ERP
          </a>';  
      }else{
        return '<button  class="btn btn-sm btn-info text-white" href="#" disabled="disabled"><i class="fa fa-paper-plane-o"> </i> Posted ERP </button>';
    }

})
       ->rawColumns(['file']);
   }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = DB::table("main.receipt_refunds")->select('receipt_refunds.*','receipts.rctno', 'receipts.payer', 'receipts.description')
        ->join('main.receipts', 'receipt_refunds.receipt_id', '=', 'receipts.id');

        if (isset($this->from_date) && isset($this->to_date)) {  
           $query = $query->where('receipt_refunds.created_at', '>=', $this->from_date . ' 00:00:00')->where('receipt_refunds.created_at', '<=', $this->to_date . ' 23:59:00');       
       }

       return $this->applyScopes($query);

   }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
       return $this->builder()
       ->columns($this->getColumns())
       ->parameters([
        'dom' => 'Bfrtip',
        'buttons' => ['csv', 'excel','print', 'reset', 'reload'],

    ]);
   }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'receipt_no' => ['data' => 'rctno', 'name' => 'rctno'],
            'payer' => ['data' => 'payer', 'name' => 'payer'],
            'description' => ['data' => 'description', 'name' => 'description'],
            'amount' =>[ 'data' => 'amount', 'name' => 'amount'],
            'action' =>[ 'data' => 'file', 'name' => 'file'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contrib_refund' . time();
    }
}
