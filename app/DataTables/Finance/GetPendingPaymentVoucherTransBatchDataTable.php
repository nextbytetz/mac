<?php

namespace App\DataTables\Finance;

use Yajra\Datatables\Services\DataTable;

class GetPendingPaymentVoucherTransBatchDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(route('backend.finance.payment.get_pending_payment_vouchers_trans_batch', []))
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => true,
                'info' => true,

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'benefit_resource', 'name' => 'benefit_resource', 'title' => trans('labels.backend.claim.benefit_resource'), 'orderable' => false, 'searchable' => false],
            ['data' => 'payee', 'name' => 'payee', 'title' => trans('labels.general.payee'), 'orderable' => true, 'searchable' => false],
            ['data' => 'benefit_type_id', 'name' => 'benefit_type_id', 'title' => trans('labels.backend.claim.benefit_type'),'orderable' => true, 'searchable' => false],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.general.amount'), 'orderable' => false, 'searchable' => false],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pendingpaymentvouchers_' . time();
    }
}
