<?php

namespace App\DataTables\Finance;


use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\MedicalPractitionerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\DependentRepository;
use Yajra\Datatables\Services\DataTable;

class GetPendingSurvivorGratuityDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {



    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $dependents = new DependentRepository();
//        $query = $claim_compensations->query();
        $query = $dependents->pendingSurvivorGratuity();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(route('backend.finance.payment.get_pending_survivor_gratuity', []))
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => true,
                'info' => false,

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'claim_id', 'name' => 'claim_id', 'title' => trans('labels.backend.claim.claim_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'payee', 'name' => 'payee', 'title' => trans('labels.general.payee'),'orderable' => true, 'searchable' => true],

            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.general.amount'), 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pendingsurvivorgratuity_' . time();
    }
}
