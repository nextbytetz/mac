<?php

namespace App\DataTables;

use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use Yajra\Datatables\Services\DataTable;

class EmployerContributionDataTable extends DataTable
{




    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('complete_status', function($receipt) {
                return $receipt->complete_status_label;
            })
            ->addColumn('rct_date_formatted', function($receipt) {
                return $receipt->rct_date_formatted;
            })
            ->addColumn('amount_comma', function($receipt) {
                return $receipt->amount_comma;
            })
            ->addColumn('employer', function($receipt) {
                return (!is_null($receipt->employer)) ? $receipt->employer->name : '';
            })
            ->addColumn('created_at_formatted', function ($receipt) {
                return $receipt->created_at_formatted;
            })
            ->addColumn("status_label", function ($receipt) {
                return $receipt->status_label;
            })
            ->rawColumns(['iscancelled', 'complete_status', 'status_label']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $receipt = new ReceiptRepository();
        $query = $receipt->getContributionForDataTable();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'buttons' => ['csv', 'excel', 'reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                            this.api().columns([0,2]).every(function () {			    
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });				
                            });
                            window.LaravelDataTables['dataTableBuilder'].buttons().container()
                                .insertBefore( '#dataTableBuilder' );
                        }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/finance/receipt/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'rctno', 'name' => 'rctno', 'title' => trans('labels.backend.table.receipt.rctno')],
            ['data' => 'payer', 'name' => 'payer', 'title' => trans('labels.backend.table.receipt.employer'), 'orderable' => false, 'searchable' => true],
            ['data' => 'amount_comma', 'name' => 'amount', 'title' => trans('labels.backend.table.amount')],
            ['data' => 'rct_date_formatted', 'name' => 'rct_date', 'title' => trans('labels.backend.table.receipt.rct_date'), 'orderable' => true,'searchable' => true],
            ['data' => 'created_at_formatted', 'name' => 'created_at', 'title' => trans('labels.backend.table.created_at'), 'orderable' => true, 'searchable' => true],
            ['data' => 'complete_status', 'name' => 'complete_status', 'title' => trans('labels.backend.table.receipt.stage'), 'orderable' => false, 'searchable' => false],
            ['data' => 'status_label', 'name' => 'status_label', 'title' => trans('labels.backend.table.receipt.status'), 'orderable' => false, 'searchable' => false],
            ['data' => 'id', 'name' => 'id', 'visible' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employercontribution_' . time();
    }
}
