<?php

namespace App\DataTables\WorkPlaceRiskAssesment;

use App\User;
use Yajra\Datatables\Services\DataTable;
use DB;
class OshAuditPeriodProfileDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
        ->of($this->query());
        // ->eloquent($this->query());
        // ->addColumn('action', 'workplaceriskassesment/oshauditperiodprofiledatatable.action');

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = DB::table("main.osh_audit_checklist_employers_summary")->where('fin_year',$this->osh_audit->fin_year);
        return $query;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax('')
        ->parameters([
            'dom'     => 'frtip',
            'order'   => [[0, 'desc']],
            'buttons' => [
                'create',
                'export',
                'print',
                'reset',
                'reload',
                ],'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        var employer_id = aData.regno.substr(8, 7);
                        // alert(employer_id);
                        var osh_audit_period_id = ".$this->osh_audit->id.";
                        document.location.href = '".url('/')."/workplace_risk_assesment/audit/'+osh_audit_period_id+'/employer_profile/'+employer_id;
                        }).hover(function() {
                            $(this).css('cursor','pointer');
                            }, function() {
                                $(this).css('cursor','auto');
                                });
                            }",

                        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'employer',
            'regno',
            'hq_district',
            'hq_region',
            'fatals_premises',
            'accidents_premises',
            'disease',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'osh_audit_profile_' . time();
    }
}
