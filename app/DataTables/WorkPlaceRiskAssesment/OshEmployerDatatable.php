<?php

namespace App\DataTables\WorkPlaceRiskAssesment;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;

class OshEmployerDatatable extends DataTable
{
    // protected $employers;

    // public function __construct()
    // {
    //     $this->employers = new EmployerRepository();
    // }
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
             return $this->datatables
             ->eloquent($this->query())
            ->addColumn('name_formatted', function ($employer) {
                return $employer->name_formatted;
            })
            ->addColumn('doc_formatted', function ($employer) {
                return $employer->date_of_commencement_formatted;
            })
            ->addColumn('country', function ($employer) {
                return (($employer->district()->count()) ? (($employer->district->region()->count()) ? $employer->district->region->country->name : ' ') : (($employer->region()->count()) ? $employer->region->country->name : ' '));
            })
            ->addColumn('region', function ($employer) {
                return ($employer->district()->count()) ? (($employer->district->region()->count()) ? $employer->district->region->name : ' ') : (($employer->region()->count()) ? $employer->region->name : ' ');
            });
            // ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->employers = new EmployerRepository();
        
        $query = $this->employers->getEmployerClaimDataTable();
        // $query = $this->employers->getForDataTable();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
                        'buttons' => [
                            'create',
                            'export',
                            'print',
                            'reset',
                            'reload',
                        ],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           'name_formatted',
            'reg_no',
            'doc_formatted',
            'country',
            'region',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'workplaceriskassesmentoshemployerdatatable_' . time();
    }
}
