<?php

namespace App\DataTables\WorkplaceRiskAssesment;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshDataManagementRepository;

class OshDeathDatatable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $repo = new OshDataManagementRepository();
        $this->query = $repo->getForClaimsDataTable($this->request);
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax(route("backend.workplace_risk_assesment.data_management.osh_claims",$this->request))
                    ->parameters([
               'dom' => 'Bfrtip',
                        'buttons' => ['excel','csv','reset', 'reload','colvis'],
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => true,
                        'info' => true,
                        'responsive' => true,
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/workplace_risk_assesment/data_management/employee/' + aData['employee_id'] + '/' + aData['case_no'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
                [ 'data' => 'filename', 'name' => 'filename', 'title' => "case no", 'orderable' => true, 'searchable' => true],
                [ 'data'=> 'gender', 'name'=> 'gender' ,'title' => "gender", 'orderable' => true, 'searchable' => false],
                [ 'data' => 'incident_type', 'name'=> 'incident_types.name','title' => "Incident Type"],
                [ 'data'=> 'fullname', 'name'=> 'fullname', 'orderable' => false,'title' => "Fullname",  'searchable' => false],
                [ 'data'=>'employer', 'name'=> 'employers.name','title' => "Employer",  'orderable' => true, 'searchable' => true],
                 [ 'data'=> 'incident_date', 'name'=> 'incident_date', 'orderable' => true,'title' => "Incident date",  'searchable' => true,'visible' => true],
                [ 'data'=>'receipt_date','name'=>'receipt_date', 'orderable' => true,'title' => "Receipt date",  'searchable' => false,'visible' => true],
                [ 'data'=>'reporting_date','name'=>'reporting_date', 'orderable' => true,'title' => "Reporting date",  'searchable' => false,'visible' => true],
                [ 'data'=>'region','name'=>'region','title' => "Region", 'searchable' => false,'visible' => true],
                [ 'data'=> 'benefit_type' , 'name'=> 'benefit_type','title' => "Benefit Type", 'searchable' => false],
                [ 'data'=> 'amount' , 'name'=> 'amount','title' => "Amount Paid", 'searchable' => false],
                [ 'data'=> 'injury_outcome' , 'name'=> 'injury_outcome','title' => "Injury Outcome", 'searchable' => false],
                [ 'data'=> 'progressive_status' , 'name'=> 'progressive_status','title' => "Status", 'searchable' => false],
                [ 'data'=> 'reasons' , 'name'=> 'reasons','title' => "Reason", 'searchable' => false,'visible' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'workplaceriskassesmentoshdeathdatatable_' . time();
    }
}

