<?php

namespace App\DataTables\WorkPlaceRiskAssesment;
use App\User;
use Yajra\Datatables\Services\DataTable;
use DB;
use Log;
class OshEmployersReportSummaryDatatable extends DataTable
{
     protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
         $query = DB::table("main.reports")->select('reports.id','reports.name as report_name','report_categories.name as section','report_types.name as report_type')
                 ->join('main.report_categories','report_categories.id','=','reports.report_category_id')
                 ->join('main.report_types','report_types.id','=','reports.report_type_id')
         ->where('report_category_id',6)->OrderBy('reports.name','asc');
       
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
    return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                 // 'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                // 'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                    if(aData['id'] == 71){
                    document.location.href = '". url("workplace_risk_assesment/reports/osh_sector_category_summary")."/' + aData['id'];
                    }
                    else if(aData['id'] == 72){
                    document.location.href = '". url("workplace_risk_assesment/reports/osh_sector_category_summary")."/' + aData['id'];
                    }else{
                    document.location.href = '". url("workplace_risk_assesment/reports/osh_report_summary")."/' + aData['id'];
                    }
                        
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
                'report_name',
                'report_type',
                'section'
               ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'oshemployersreportsummarydatatable_' . time();
    }
}
