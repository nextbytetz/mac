<?php

namespace App\DataTables\WorkPlaceRiskAssesment;

use App\User;
use Yajra\Datatables\Services\DataTable;
use DB;
use Log;


class OshAuditEmployerSummaryDataTable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
        ->editColumn('period', function($data) {
            if ($data->period == 1) {
               $period = 'Jul - Dec'; 
            }else{
               $period = 'Jan - June';
            }
            
            return $period;
        });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {   
        // dd($this->request['request']['fin_y']);
     
       $query = DB::table('main.osh_audit_ratings')
        ->select('osh_audit_ratings.employer_id','employers.name as employer','osh_audit_ratings.period','osh_audit_ratings.fin_year','ohsa','training','ergonomics','personal_protective_equipment','machine_use_maintainance','chemicals','emergency_preparedness','adorr','health_welfare','rate','average_score')
        ->join('employers','employers.id','=','osh_audit_ratings.employer_id');

        if (isset($this->request['request']['fin_y'])) {
        if ($this->request['request']['fin_y'] != null) {

            $query->where('osh_audit_ratings.fin_year',$this->request['request']['fin_y']);
        }
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->Columns($this->getColumns())
            ->minifiedAjax(route("backend.workplace_risk_assesment.reports.osh_report_summary",['report_id'=>68,'request'=>$this->request]))
            ->parameters([
                 'dom' => 'Bfrtip',
                        'buttons' => ['excel','csv','reset', 'reload','colvis'],
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => true,
                        'info' => true,
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
             // 'regno'=>['title'=>'id','searchable'=>true,'orderable'=>true],
            'employer'=>['title'=>'Employer','name'=>'employers.name','searchable'=>true,'orderable'=>true],
             'period'=> ['title'=>' Period','searchable'=>false,'orderable'=>true],
             'fin_year'=>['title'=>' Financial year','searchable'=>false,'orderable'=>true],
             'ohsa'=>['title'=>' Occupational Health and Safety Administration','searchable'=>false,'orderable'=>true],
             'training'=>['title'=>'Training','searchable'=>false,'orderable'=>true],
             'ergonomics'=>['title'=>'Ergonomics','searchable'=>false,'orderable'=>true],
             'personal_protective_equipment'=>['title'=>'Personal Protective Equipment (PPE)','searchable'=>false,'orderable'=>true],
             'machine_use_maintainance'=>['title'=>'Machine Use And Maintainance','searchable'=>false,'orderable'=>true],
             'chemicals'=>['title'=>'Chemicals','searchable'=>false,'orderable'=>true],
             'emergency_preparedness'=>['title'=>'Emergency Preparedness','searchable'=>false,'orderable'=>true], 
             'adorr'=>['title'=>'Accident and Dangerous Occurence Reporting and Recording','searchable'=>false,'orderable'=>true],
             'health_welfare'=>['title'=>'Health and Welfare','searchable'=>false,'orderable'=>true],
             'average_score'=>['title'=>'Average Score','searchable'=>false,'orderable'=>true],
             'rate'=>['title'=>'Rate','searchable'=>false,'orderable'=>true],

        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'oshauditemployersummarydatatable_' . time();
    }
}
