<?php

namespace App\DataTables\WorkPlaceRiskAssesment;

use App\User;
use Yajra\Datatables\Services\DataTable;
use DB;

class OshSectorSummaryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
        ->addColumn('injury_rate', function($data) {
            // return  $data->contributed_employees;
            $injury_rate = 0;
            $fatals = $data->fatal_and_non_fatal;
            $employees = $data->contributed_employees;
            if ($fatals > 0) {
               $injury_rate = ($fatals/$employees) * 100000; 
            }else{
               $injury_rate = 0;
            }
            
            return round($injury_rate,2);
        })
     ->addColumn('frequency_rate_max', function($data) {
            $frequency_rate_max = 0;
            $employees = $data->contributed_employees;
            $max_hours = $employees * 45 * 48;
            $accidents = $data->accidents_workplace_reported;
            if ($accidents > 0) {
               $frequency_rate_max = ($accidents/$max_hours) * 1000000; 
            }else{
               $frequency_rate_max = 0;
            }
            
            return round($frequency_rate_max,2);
        })
        ->addColumn('frequency_rate_min', function($data) {
            $frequency_rate_min = 0;
            $employees = $data->contributed_employees;
            $min_hours = $employees * 40 * 48;
            $accidents = $data->accidents_workplace_reported;
            if ($accidents > 0) {
               $frequency_rate_min = ($accidents/$min_hours) * 1000000; 
            }else{
               $frequency_rate_min = 0;
            }
            
            return round($frequency_rate_min,2);
        })
        ->addColumn('accident_rate_max', function($data) {
            $accident_rate_max = 0;
            $employees = $data->contributed_employees;
            $max_hours = $employees * 45 * 48;
            $accidents = $data->total_days_lost;
            if ($accidents > 0) {
               $accident_rate_max = ($accidents/$max_hours) * 1000000; 
            }else{
               $accident_rate_max = 0;
            }
            
            return round($accident_rate_max,2);
        })
        ->addColumn('accident_rate_min', function($data) {
            $accident_rate_min = 0;
            $employees = $data->contributed_employees;
            $min_hours = $employees * 40 * 48;
            $accidents = $data->total_days_lost;
            if ($accidents > 0) {
               $accident_rate_min = ($accidents/$min_hours) * 1000000; 
            }else{
               $accident_rate_min = 0;
            }
            
            return round($accident_rate_min,2);
        })
        ->addColumn('fatality_rate', function($data) {
            $fatality_rate = 0;
            $fatals = $data->occupational_fatal;
            $employees = $data->contributed_employees;
            if ($fatals > 0) {
               $fatality_rate = ($fatals/$employees) * 100000; 
            }else{
               $fatality_rate = 0;
            }
            
            return round($fatality_rate,2);
        })
         ->addColumn('desease_rate', function($data) {
            $desease_rate = 0;
            $fatals = $data->disease;
            $employees = $data->contributed_employees;
            if ($fatals > 0) {
               $desease_rate = ($fatals/$employees) * 100000; 
            }else{
               $desease_rate = 0;
            }
            
            return round($desease_rate,2);
        });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {   

        $query = DB::table('main.osh_employers_summary_report')
        ->select('d.business_sector_cv_id', 'd.b_sector',
            DB::raw("(sum(osh_employers_summary_report.total_claims)) as total_claims"),
            DB::raw("(sum(osh_employers_summary_report.fatals_premises)) as fatals_premises"),
            DB::raw("(sum(osh_employers_summary_report.fatals_out)) as fatals_out"),
            DB::raw("(sum(osh_employers_summary_report.accidents_premises)) as accidents_premises"),
            DB::raw("(sum(osh_employers_summary_report.accidents_out)) as accidents_out"),
            DB::raw("(sum(osh_employers_summary_report.disease)) as disease"),
            DB::raw("(sum(osh_employers_summary_report.total_days_lost)) as total_days_lost"),
            DB::raw("(sum(osh_employers_summary_report.accidents_workplace_reported)) as accidents_workplace_reported"),
            DB::raw("(sum(osh_employers_summary_report.occupational_fatal)) as occupational_fatal"),
            DB::raw("(sum(osh_employers_summary_report.fatal_and_non_fatal)) as fatal_and_non_fatal"),
            DB::raw("(sum(osh_employers_summary_report.on_premise_fatals_and_accidents)) as on_premise_fatals_and_accidents"),
            DB::raw("(sum(d.contributed_employees)) as contributed_employees"),'osh_employers_summary_report.fin_year')
        ->join(DB::raw('(select zz.name as b_sector, z.business_sector_cv_id,z.employer_id,z.contributed_employees,z.fin_year from
        (select b.business_sector_cv_id,eee.id as employer_id,eee.contributed_employees,eee.fin_year from (select ee.fin_year,e.id,ee.contributed_employees from 
        (select osh_audit_employees_contributed.employer_id,osh_audit_employees_contributed.contributed_employees,osh_audit_employees_contributed.fin_year
        from main.osh_audit_employees_contributed) ee join main.employers as e on e.id=ee.employer_id) eee join main.employer_sectors as b on b.employer_id=eee.id) z
        join main.code_values as zz on zz.id=z.business_sector_cv_id) as d'),function($join){
            $join->on('d.employer_id','=','osh_employers_summary_report.employer_id');
        })
        ->whereRaw('osh_employers_summary_report.fin_year = d.fin_year')
        ->groupBy('d.business_sector_cv_id','d.b_sector','osh_employers_summary_report.fin_year');
        // ->distinct('osh_employers_summary_report.fin_year','osh_employers_summary_report.employer_id');

        if (isset($this->request['request']['fin_y'])) {
        if ($this->request['request']['fin_y'] != null) {

            $query->where('osh_employers_summary_report.fin_year',$this->request['request']['fin_y']);
        }
        }
        if (isset($this->request['request']['bussines_sector'])) {
        if ($this->request['request']['bussines_sector'] != null) {

            $query->where('d.business_sector_cv_id',$this->request['request']['bussines_sector']);
        }
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->Columns($this->getColumns())
            ->minifiedAjax(route("backend.workplace_risk_assesment.reports.osh_sector_category_summary",['report_id'=>71,'request'=>$this->request]))
            ->parameters([
                 'dom' => 'Bfrtip',
                        'buttons' => ['excel','csv','reset', 'reload','colvis'],
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => true,
                        'info' => true,
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'b_sector'=>['title'=>'Sector','searchable'=>true,'orderable'=>true],
             'fin_year'=>['title'=>' Financial year','searchable'=>true,'orderable'=>true],
             'total_claims'=>['title'=>' Total Notification','searchable'=>false,'orderable'=>true],
             'on_premise_fatals_and_accidents'=>['title'=>'Notifications occurred on premise','searchable'=>false,'orderable'=>true],
             'injury_rate'=>['title'=>'Injury Rate','searchable'=>false,'orderable'=>true],
             'frequency_rate_max'=>['title'=>'Max Accident frequency rate','searchable'=>false,'orderable'=>true],
             'frequency_rate_min'=>['title'=>'Min Accident frequency rate','searchable'=>false,'orderable'=>true],
             'accident_rate_max'=>['title'=>'Max Accident severity rate','searchable'=>false,'orderable'=>true],
             'accident_rate_min'=>['title'=>'Min Accident severity rate','searchable'=>false,'orderable'=>true], 
             'fatality_rate'=>['title'=>'Workplace Fatality rate','searchable'=>false,'orderable'=>true],
             'desease_rate'=>['title'=>'Occupationa Diseases rate','searchable'=>false,'orderable'=>true],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'workplaceriskassesmentoshsectorsummarydatatable_' . time();
    }
}
