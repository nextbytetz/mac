<?php

namespace App\DataTables\WorkplaceRiskAssesment;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshDataManagementRepository;

class OshDeseaseDatatable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
         ->eloquent($this->query());

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $repo = new OshDataManagementRepository();
        $this->query = $repo->getForClaimsDataTable($this->request);
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax(route("backend.workplace_risk_assesment.data_management.osh_claims",$this->request))
                    ->parameters([
                        'dom' => 'Bfrtip',
                        'buttons' => ['excel','csv','reset', 'reload','colvis'],
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => true,
                        'info' => true,
                        'responsive' => true,
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/workplace_risk_assesment/data_management/employee/' + aData['employee_id'] + '/' + aData['case_no'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
                'filename' => [ 'title' => "case no", 'orderable' => true, 'searchable' => true],
                'gender' => [ 'name'=> 'gender' ,'title' => "gender", 'orderable' => true, 'searchable' => false],
                'incident_type' => [ 'name'=> 'incident_types.name','title' => "Incident Type"],
                'fullname' => [ 'name'=> 'fullname', 'orderable' => false,'title' => "Fullname",  'searchable' => false],
                'employer' => [ 'name'=> 'employers.name','title' => "Employer",  'orderable' => true, 'searchable' => true],
                'incident_date' => [ 'orderable' => true,'title' => "Incident date",  'searchable' => true,'visible' => true],
                'receipt_date' => [ 'orderable' => true,'title' => "Receipt date",  'searchable' => false,'visible' => true],
                [ 'data'=>'reporting_date','name'=>'reporting_date', 'orderable' => true,'title' => "Reporting date",  'searchable' => false,'visible' => true],
                'region' => [ 'title' => "Region", 'searchable' => false,'visible' => true],
                'disease' => [ 'name'=> 'disease','title' => "Disease name", 'searchable' => false],
                'diagnosis_date' => [ 'name'=> 'diagnosis_date','title' => "Diagnosis Date", 'searchable' => false],
                'disease_typ' => [ 'name'=> 'disease_typ','title' => "Disease Type", 'searchable' => false],
                'disease_agent_type' => [ 'name'=> 'disease_agent_type','title' => "Disease Agent Type", 'searchable' => false],
                'disease_agent' => [ 'name'=> 'disease_agent','title' => "Disease Agent", 'searchable' => false],
                'disease_organ_type' => [ 'name'=> 'disease_organ_type','title' => "Target Organ Type", 'searchable' => false],
                'disease_organ' => [ 'name'=> 'disease_organ','title' => "Target Organ", 'searchable' => false],
                [ 'data'=> 'benefit_type' , 'name'=> 'benefit_type','title' => "Benefit Type", 'searchable' => false],
                [ 'data'=> 'amount' , 'name'=> 'amount','title' => "Amount Paid", 'searchable' => false],
                [ 'data'=> 'mae' , 'name'=> 'mae','title' => "Hospitalization + ED(wcp-5)", 'searchable' => false],
                [ 'data'=> 'lightduty' , 'name'=> 'lightduty','title' => "Light Duty(wcp-5)", 'searchable' => false],
                // [ 'data'=> 'percent_outcomes' , 'name'=> 'percent_outcomes','title' => "Percent Disability", 'searchable' => false],
                [ 'data'=> 'injury_outcome' , 'name'=> 'injury_outcome','title' => "Injury Outcome", 'searchable' => false],
                 [ 'data'=> 'progressive_status' , 'name'=> 'progressive_status','title' => "Status", 'searchable' => false],
                 [ 'data'=> 'reasons' , 'name'=> 'reasons','title' => "Reason", 'searchable' => false,'visible' => false],
                



        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'workplaceriskassesmentdeseasedatatable_' . time();
    }
}

