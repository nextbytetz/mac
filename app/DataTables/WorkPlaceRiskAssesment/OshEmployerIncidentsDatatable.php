<?php

namespace App\DataTables\WorkplaceRiskAssesment;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshDataManagementRepository;

class OshEmployerIncidentsDatatable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $repo = new OshDataManagementRepository();
        $this->query = $repo->employerIncidentsDatatable($this->employer);
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    // ->minifiedAjax('')
                    ->parameters([
               'dom' => 'Bfrtip',
                        'buttons' => ['excel','reset', 'reload','colvis'],
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => true,
                        'info' => true,
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/workplace_risk_assesment/data_management/employee/' + aData['employee_id'] + '/' + aData['case_no'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [ 'data'=> 'employee_id' , 'name'=> 'employee_id','title' => "ID", 'orderable' => false, 'searchable' =>  false, 'visible' => true],
                [ 'data'=> 'filename' , 'name'=> 'filename' ,'title' => "case no", 'orderable' => true, 'searchable' => true],
                [ 'data'=> 'gender' , 'name'=> 'gender' ,'title' => "gender", 'orderable' => true, 'searchable' => false],
                [ 'data'=> 'incident_type' , 'name'=> 'incident_types.name','title' => "Incident Type"],
                [ 'data'=> 'fullname' , 'name'=> 'fullname', 'orderable' => false,'title' => "Fullname",  'searchable' => false],
                [ 'data'=> 'employer' , 'name'=> 'employers.name','title' => "Employer",  'orderable' => true, 'searchable' => true],
                [ 'data'=> 'incident_date' , 'name'=> 'notification_reports.incident_date', 'orderable' => true,'title' => "Incident date",  'searchable' => true],
                [ 'data'=> 'receipt_date' , 'name'=> 'notification_reports.receipt_date', 'orderable' => true,'title' => "Receipt date",  'searchable' => false],
                [ 'data'=> 'region' , 'name'=> 'regions.name','title' => "Region", 'searchable' => false],
                [ 'data'=> 'employment_category' , 'name'=> 'employment_category','title' => "Employee Category", 'searchable' => false],
                [ 'data'=> 'occupation' , 'name'=> 'occupation','title' => "Occupation", 'searchable' => false],
                [ 'data'=> 'age' , 'name'=> 'age','title' => "age", 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'workplaceriskassesmentoshemployerincidentsdatatable_' . time();
    }
}

