<?php

namespace App\DataTables\WorkPlaceRiskAssesment\OshAudit;
/*juma*/
use Yajra\Datatables\Services\DataTable;
use DB;
class OshAuditPeriodProfileDatatable extends DataTable
{

    public function dataTable()
    {
        return $this->datatables
        ->of($this->query());
    }


    public function query()
    {
        $query = DB::table("main.osh_audit_checklist_employers_summary")->where('fin_year',$this->osh_audit->fin_year);
        return $query;
    }


    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax('')
        ->parameters([
           'processing'=> true,
           'info'=> true,
           'paging'=> true,
           'searching' => true,
           'buttons' => [],
           'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).click(function() {
                var employer_id = aData.regno.substr(8, 7);
                        // alert(employer_id);
                var osh_audit_period_id = ".$this->osh_audit->id.";
                document.location.href = '".url('/')."/workplace_risk_assesment/audit/'+osh_audit_period_id+'/employer_profile/'+employer_id;
                }).hover(function() {
                    $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                        });
                    }",

                ]);
    }


    protected function getColumns()
    {
        return [
            'employer',
            'regno',
            'hq_district',
            'hq_region',
            'fatals_premises',
            'accidents_premises',
            'disease',
        ];
    }

    
    protected function filename()
    {
        return 'osh_audit_profile_' . time();
    }
}
