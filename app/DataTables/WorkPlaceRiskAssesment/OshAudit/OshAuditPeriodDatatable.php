<?php

namespace App\DataTables\WorkPlaceRiskAssesment\OshAudit;
/*juma*/
use App\Models\WorkPlaceRiskAssesment\OshAuditPeriod;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditPeriodRepository;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Facades\DB;

class OshAuditPeriodDatatable extends DataTable
{

  public function dataTable()
  {

    $input = $this->request;

    $db = $this->datatables
    ->eloquent($this->query())
    ->editColumn('period', function($data) {
      return  ($data->period == 1) ? 'Jul - Dec ': 'Jan - Jun';
    })
    ->addColumn('employees_count', function($data) use($input){
     $osh_audit_period = new OshAuditPeriodRepository();
     if(empty($data->status)){
       $return = $osh_audit_period->returnPeriodEmployersCount($data->id,$input['fatal'], $input['lti'],$input['non_conveyance']);
     }else{
      $return = !empty($data->number_of_employers) ? $data->number_of_employers : 0;
    }

    return $return;
  })
    ->addColumn('workflow_status', function($data) {
      $osh_audit_period = new OshAuditPeriodRepository();
      return  $osh_audit_period->getWfStatusLabelOshAuditPeriod($data->status,$data->period, $data->fin_year);
    });

    return  $db->rawColumns(['workflow_status']);
  }


  public function query()
  {

    $osh_audit_period = new OshAuditPeriodRepository();
    $this->query = $osh_audit_period->query()->select('id','fin_year','period','status','fin_year_id','is_active','number_of_employers','fatal','lost_time_injury','non_conveyance')->orderBy('fin_year_id','desc')->orderBy('period','desc');

    if (!empty($this->request['financial_year'])) {
      $filtered_period = $osh_audit_period->returnPeriodIds($this->request['financial_year'],$this->request['fatal'], $this->request['lti'],$this->request['non_conveyance']);
      $this->query->addSelect(DB::raw("'".$this->request['fatal']."' as request_fatal"))
      ->addSelect(DB::raw("'".$this->request['lti']."' as request_lti"))
      ->addSelect(DB::raw("'".$this->request['non_conveyance']."' as request_non_conveyance"))
      ->whereIn('id',$filtered_period);
    }
    return $this->applyScopes($this->query);
  }


  public function html()
  {
    return $this->builder()
    ->columns($this->getColumns())
    ->minifiedAjax('')
    ->parameters([
      'processing'=> true,
      'info'=> true,
      'paging'=> true,
      'searching' => true,
      'buttons' => [],
      'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $(nRow).click(function() {
          let id = aData['id'];
          let c_fatal = 0;
          let c_lti = 0;
          let c_non_conveyance = 0;
          if(aData['status'] == null){
           c_fatal = (aData['request_fatal'] ? aData['request_fatal'] : 0);
           c_lti = (aData['request_lti'] ? aData['request_lti'] : 0);
           c_non_conveyance = (aData['request_non_conveyance'] ? aData['request_non_conveyance'] : 0);
           }else{
            c_fatal = (aData['fatal'] ? aData['fatal'] : 0);
            c_lti = (aData['lost_time_injury'] ? aData['lost_time_injury'] : 0);
            c_non_conveyance = (aData['non_conveyance'] ? aData['non_conveyance'] : 0);
          }
          document.location.href = '".url('/')."/workplace_risk_assesment/audit/view_audit_list/'+id+'/'+c_fatal+'/'+c_lti+'/'+c_non_conveyance; 
          }).hover(function() {
            $(this).css('cursor', 'alias');                        
            }, function() {
              $(this).css('cursor','auto');
              });
            }",
          ]);
  }


  protected function getColumns()
  {

    $array = [
      'fin_year',
      'period',
      'employees_count'  => ['title'=>'Number of Employers','orderable' => false, 'searchable' => false],
      'workflow_status' => ['orderable' => false, 'searchable' => false],
    ];
    return $array;

  }


  protected function filename()
  {
    return 'osh_audit' . time();
  }
}
