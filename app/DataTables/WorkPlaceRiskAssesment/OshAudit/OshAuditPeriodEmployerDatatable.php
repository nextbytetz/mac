<?php

namespace App\DataTables\WorkPlaceRiskAssesment\OshAudit;
/*juma*/
use App\User;
use Yajra\Datatables\Services\DataTable;
use DB;

class OshAuditPeriodEmployerDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $query = DB::table("main.osh_audit_period_employers")
        ->select('employers.name','employers.id as employer_id','employers.reg_no','osh_audit_period_employers.*','osh_audit_period.id as osh_audit_period_id')
        ->join('main.employers','employers.id','=','osh_audit_period_employers.employer_id')
        ->join('main.osh_audit_period','osh_audit_period.id','=','osh_audit_period_employers.osh_audit_period_id')
        ->where('osh_audit_period_id',$this->osh_audit_period_id);

        // DB::raw('COALESCE(number_of_fatal,0) + COALESCE(number_of_days_lost,0) +  COALESCE(total_claims,0) as subtotal')

        return $query;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax('')
        ->parameters([
            'processing'=> true,
            'info'=> true,
            'paging'=> true,
            'searching' => true,
            'buttons' => [],
            'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    var employer_id = aData.employer_id;
                    var osh_audit_period_id = aData.osh_audit_period_id
                    document.location.href = '".url('/')."/workplace_risk_assesment/audit/'+osh_audit_period_id+'/employer_profile/'+employer_id;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                            });
                        }",

                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => ['title'=> 'Employer', 'data'=> 'name' , 'name'=> 'employers.name'],
            'reg_no' => ['title'=> 'Reg No#', 'data'=> 'reg_no' , 'name'=> 'employers.reg_no'],
            'number_of_fatal' => ['title'=> 'Fatal', 'data'=> 'number_of_fatal' , 'name'=> 'osh_audit_period_employers.number_of_fatal'],
            'number_of_days_lost' => ['title'=> 'Total Days Lost', 'data'=> 'number_of_days_lost' , 'name'=> 'osh_audit_period_employers.number_of_days_lost'],
            'not_conveyance_accident' => ['title'=> 'Non Conveyance Claims', 'data'=> 'not_conveyance_accident' , 'name'=> 'not_conveyance_accident']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'osh_audit_period_employers_' . time();
    }
}
