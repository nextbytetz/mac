<?php

namespace App\DataTables\WorkPlaceRiskAssesment\OshAudit;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Models\WorkPlaceRiskAssesment\OshAuditPeriodEmployer;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditEmployerRepository;
use DB;

class UserOshAuditListDatatable extends DataTable
{


    public function dataTable()
    {
        return $this->datatables->of($this->query())
        ->editColumn('number_of_fatal', function($data){
            return empty($data->number_of_fatal) ? 0 : $data->number_of_fatal;
        })
        ->editColumn('number_of_days_lost', function($data){
            return empty($data->number_of_days_lost) ? 0 : $data->number_of_days_lost;
        }) ->editColumn('not_conveyance_accident', function($data){
            return empty($data->not_conveyance_accident) ? 0 : $data->not_conveyance_accident;
        });
    }


    public function query()
    {
      $query =  DB::table("main.osh_audit_period_employers")
      ->select('employers.name','employers.id as employer_id','employers.reg_no','osh_audit_period_employers.*','osh_audit_period_employers.id as osh_audit_period_employer_id','osh_audit_period.*','osh_audit_period.id as osh_audit_period_id','users.firstname','users.lastname')
      ->join('main.employers','employers.id','=','osh_audit_period_employers.employer_id')
      ->join('main.osh_audit_period','osh_audit_period.id','=','osh_audit_period_employers.osh_audit_period_id')
      ->leftJoin('main.users','users.id','=','osh_audit_period_employers.user_id')
      ->where('osh_audit_period_employers.user_id',access()->user()->id)
      ->where('is_active',true);

      if (!empty($this->input['financial_year'])) {
        $query->where('osh_audit_period.fin_year',$this->input['financial_year']);
    }

    if (!empty($this->input['period'])) {
        $query->where('osh_audit_period.period',$this->input['period']);
    }

    $query->orderBy('is_submitted',asc);

    return $query;
}


public function html()
{
  return $this->builder()
  ->columns($this->getColumns())
  ->minifiedAjax('')
  ->parameters([
     'dom' => 'lBfrtip',
     'processing'=> true,
     'info'=> true,
     'paging'=> true,
     'searching' => true,
     'buttons' => ['excel','csv'],
     'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).click(function() {
            document.location.href = '".url('/')."/workplace_risk_assesment/audit/'+aData['id']+'/employer_profile/'+aData.employer_id; 
            }).hover(function() {
                $(this).css('cursor','pointer');                        
                }, function() {
                    $(this).css('cursor','auto');
                    });
                }",

            ]);
}


protected function getColumns()
{

  return [
    'name' =>  [ 'data'=> 'name' , 'name'=> 'employers.name'],
    'reg_no' => [ 'data'=> 'reg_no' , 'name'=> 'employers.reg_no'],
    'fin_year' => [ 'data'=> 'fin_year' , 'name'=> 'osh_audit_period.fin_year'],
    'fatal' => [ 'title' => 'Number Of Fatal', 'data'=> 'number_of_fatal' , 'name'=> 'osh_audit_period_employers.number_of_fatal'],
    'number_of_days_lost' => [ 'data'=> 'number_of_days_lost' , 'name'=> 'osh_audit_period_employers.number_of_days_lost'],
    'not_conveyance_accident' => [ 'title' => 'Non Conveyance Claims' ,'data'=> 'not_conveyance_accident' , 'name'=> 'osh_audit_period_employers.not_conveyance_accident'],
];

}


protected function filename()
{
  return 'osh_audit' . time();
}


}
