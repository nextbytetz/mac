<?php
namespace App\DataTables\WorkPlaceRiskAssesment\OshAudit;
/*juma*/
use App\Models\WorkPlaceRiskAssesment\OshAuditPeriod;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditPeriodRepository;
use Yajra\Datatables\Services\DataTable;
use DB;

class OshChecklistEmployersDatatable extends DataTable
{

  public function dataTable()
  {
   $db = $this->datatables
   ->of($this->query());
   if (access()->user()->hasPermission('assign_staff_for_osh_audit')) {
     $db->addColumn('select', function($data) {
      $osh_repo = new OshAuditPeriodRepository();
      $period_active = $osh_repo->isPeriodActive($data->period, $data->fin_year);
      if ($period_active) {
       return '<input type="checkbox" class="select_employers" name="selected_employers[]" value="'.$data->osh_audit_period_employer_id.'">';
     } 
   });
   }
   return  $db->addColumn('assigned_staff', function($data) {
    return $data->firstname.' '.$data->lastname;
  })->filterColumn('assigned_staff', function($data, $keyword) {
    return $data->whereRaw("CONCAT(firstname, ' ', lastname) ilike ?", ["%{$keyword}%"]);
  })->rawColumns(['select']);
}


public function query()
{
  $query =  DB::table("main.osh_audit_period_employers")
  ->select('employers.name','employers.id as employer_id','employers.reg_no','osh_audit_period_employers.*','osh_audit_period_employers.id as osh_audit_period_employer_id','osh_audit_period.*','osh_audit_period.id as osh_audit_period_id','users.firstname','users.lastname')
  ->join('main.employers','employers.id','=','osh_audit_period_employers.employer_id')
  ->join('main.osh_audit_period','osh_audit_period.id','=','osh_audit_period_employers.osh_audit_period_id')
  ->leftJoin('main.users','users.id','=','osh_audit_period_employers.user_id')
  ->where('is_active',true);

   if (!empty($this->input['financial_year'])) {
    $query->where('osh_audit_period.fin_year',$this->input['financial_year']);
  }
 

  if (!empty($this->input['period'])) {
    $query->where('osh_audit_period.period',$this->input['period']);
  }

  return $query;
}


public function html()
{
  return $this->builder()
  ->columns($this->getColumns())
    // ->minifiedAjax('')
  ->minifiedAjax((route("backend.workplace_risk_assesment.audit.checklist_view",$this->input)))
  ->parameters([
   'dom' => 'lBfrtip',
   'processing'=> true,
   'info'=> true,
   'paging'=> true,
   'searching' => true,
   'buttons' => ['excel','csv'],
   'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
    $('td', nRow).click(function() {
      let col = $(this).index()+1;
      var colText = $('#employer_for_audit-table thead th:nth-child('+col+')').text();
      if(colText){

        document.location.href = '".url('/')."/workplace_risk_assesment/audit/'+aData['id']+'/employer_profile/'+aData.employer_id; 
      }
      }).hover(function() {
        let col = $(this).index()+1;
        var colText = $('#employer_for_audit-table thead th:nth-child('+col+')').text();
        if(colText){
          $(this).css('cursor','pointer');                        
          }}, function() {
            $(this).css('cursor','auto');
            });
          }",

        ]);
}


protected function getColumns()
{

 if (access()->user()->hasPermission('assign_staff_for_osh_audit')) {
  $array['select'] =  ['title' => '' ,'orderable' => false, 'searchable' => false];
}

$array['name'] =  [ 'data'=> 'name' , 'name'=> 'employers.name'];
$array['reg_no'] = [ 'data'=> 'reg_no' , 'name'=> 'employers.reg_no'];
$array['fin_year'] =  [ 'data'=> 'fin_year' , 'name'=> 'osh_audit_period.fin_year'];
$array['assigned_staff'] =  [ 'data'=> 'assigned_staff' , 'name'=> 'assigned_staff'];

return $array;

}


protected function filename()
{
  return 'osh_audit' . time();
}
}