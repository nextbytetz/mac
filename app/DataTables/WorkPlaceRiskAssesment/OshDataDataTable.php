<?php

namespace App\DataTables\WorkPlaceRiskAssesment;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshData;

class OshDataDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    { 
        return $this->datatables
            ->eloquent($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $employees = new OshData();
        $query = $employees->getEmployeeData();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

    return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                'buttons' => ['reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTableBuilder'];
                }",
                'searching' => true,
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/workplace_risk_assesment/data_management/employee/' + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

        return [
            ['data' => 'memberno', 'memberno' => 'memberno', 'title' => trans('labels.backend.member.memberno'), 'orderable' => true, 'searchable' => true],
            ['data' => 'fullname', 'name' => 'fullname', 'title' => trans('labels.general.name')],
            ['data' => 'dob', 'name' => 'dob', 'title' => trans('labels.general.dob'), 'orderable' => true, 'searchable' => true],
            ['data' => 'category', 'name' => 'category', 'title' => trans('labels.backend.table.category'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employer_name', 'title' => trans('labels.general.employer_name')],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'oshdatadatatable_' . time();
    }
}
