<?php

namespace App\DataTables\Report;

use App\Repositories\Backend\Reporting\ReportRepository;
use Yajra\Datatables\Services\DataTable;

class GeneralReportDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */

    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())

            ->editColumn('name', function($report) {
                return  $report->name  . (($report->is_new) ? '<span   style="color:green;" class="badge-summary blink">
                            New
                        </span>' : '');
            })
            ->addColumn('report_categories', function($report) {
                return  $report->report_categories_label;
            })
            ->editColumn('report_type_id', function($report) {
                return  $report->reportType->name;
            })

            ->rawColumns(['report_categories', 'name']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */




    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'processing'=> true,
                'info'=> true,
                'paging'=> true,
                'searching' => true,
                'buttons' => [],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/report/' + aData['url_name'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.backend.table.report_name')],
            ['data' => 'report_type_id', 'name' => 'report_type_id', 'title' => trans('labels.backend.table.report_type'), 'orderable' => false, 'searchable' => false],
            ['data' => 'report_categories', 'name' => 'report_categories', 'title' => trans('labels.backend.table.report_category'), 'orderable' => false, 'searchable' => false],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'receiptperformance' . time();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $report = new ReportRepository();

        if($this->category_id > 0){
            /* view department reports*/
            $query = $report->query()->whereHas('reportCategories' , function ($query){
                $query->where('report_category_id', $this->category_id);
            });
        } else {
            /* View all reports */
            $query = $report->query()->orderBy('name', 'asc');
        }


        return $this->applyScopes($query);
    }
}
