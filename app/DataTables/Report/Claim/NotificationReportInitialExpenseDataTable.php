<?php

namespace App\DataTables\Report\Claim;


use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;

class NotificationReportInitialExpenseDataTable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        $member_types = new MemberTypeRepository();
        $notification_reports = new NotificationReportRepository();
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('incident_type_id', function($notification_report) {
                return $notification_report->incidentType->name;
            })
            ->editColumn('employee_id', function($notification_report) {
                return  ($notification_report->employee_id) ? $notification_report->employee->name : $notification_report->employee_name;
            })
            ->addColumn('employer', function($notification_report) {
                return  ($notification_report->employer_id) ? $notification_report->employer->name : $notification_report->employer_name;
            })

            ->addColumn('incident_date', function($notification_report)  {
                return $notification_report->incident_date;
            })
            ->addColumn('receipt_date', function($notification_report) {
                return $notification_report->receipt_date;
            })
            ->addColumn('initial_expense', function($notification_report) use($member_types) {
                return   $member_types->getMember($notification_report->initial_expense_member_type_id,$notification_report->initial_expense_resource_id)->name;
            })
            ->addColumn('paid_through', function($notification_report)  {
                return ($notification_report->payThroughInsurance()->count()) ? $notification_report->payThroughInsurance->name : 'Cash';
            })
            ->editColumn('status', function($notification_report) {
                return $notification_report->status_label;
            })
            ->rawColumns(['status']);


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;

        $notification_reports = new NotificationReportRepository();
        $this->query = $notification_reports->query()->where(function ($query) use($from_date,$to_date) {
            $query->whereHas('accident', function ($query)  use($from_date,$to_date) {
                $query->where('receipt_date','>=', $from_date)->where('receipt_date','<=', $to_date);
            })->orWhereHas('disease', function ($query)  use($from_date,$to_date) {
                $query->where('receipt_date','>=', $from_date)->where('receipt_date','<=', $to_date);
            })->orWhereHas('death', function ($query)  use($from_date,$to_date) {
                $query->where('receipt_date','>=', $from_date)->where('receipt_date','<=', $to_date);
            });
        });

//         with member type
        if (isset($this->input['member_type_id'])) {
            if ($this->input['member_type_id'] > 0) {
                $this->query = $this->queryWithMemberType();
            }
        }
//        with type => payment method
        if (isset($this->input['type'])) {
            if (($this->input['type'] >= 0)) {
                $this->query = $this->queryWithType();
            }
        }
//        with insurance
        if (isset($this->input['insurance_id'])) {
            if ($this->input['insurance_id'] > 0) {
                $this->query = $this->queryWithInsurance();
            }
        }

//        with Employer
        if (isset($this->input['employer_id'])) {
            if ($this->input['employer_id'] > 0) {
                $this->query = $this->queryWithEmployer();
            }
        }

        return $this->applyScopes($this->query);
    }

    /*
  * query date range with member type selected
  */
    public function queryWithMemberType()
    {
        return  $this->query->where('initial_expense_member_type_id', $this->input['member_type_id']);
    }


    /*
* query date range with with payment method / TYPE selected
*/
    public function queryWithType()
    {
        return  $this->query->where('is_cash', $this->input['type']);
    }


    /*
* query date range with with insurance selected
*/
    public function queryWithInsurance()
    {
        return  $this->query->where('pay_through_insurance_id', $this->input['insurance_id']);
    }


    /*
* query date range with with Employer selected
*/
    public function queryWithEmployer()
    {
        return  $this->query->where('initial_expense_member_type_id', 1)->where('initial_expense_resource_id', $this->input['employer_id']);
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/claim/notification_report/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => trans('labels.backend.legal.case_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'incident_type_id', 'name' => 'incident_type_id', 'title' => trans('labels.backend.claim.incident_type'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employee_id', 'name' => 'employee_id', 'title' => trans('labels.backend.compliance.employee'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer', 'name' => 'employer', 'title' => trans('labels.backend.compliance.employer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'incident_date', 'name' => 'incident_date', 'title' => trans('labels.backend.table.incident_date'), 'orderable' => false, 'searchable' => false],
                        ['data' => 'receipt_date', 'name' => 'receipt_date', 'title' => trans('labels.backend.claim.receipt_date'), 'orderable' => false, 'searchable' => false],
            ['data' => 'initial_expense', 'name' => 'initial_expense', 'title' => trans('labels.backend.claim.initial_expense'), 'orderable' => false, 'searchable' => false],
            ['data' => 'paid_through', 'name' => 'paid_through', 'title' => trans('labels.general.paid_through'), 'orderable' => false, 'searchable' => false],
            ['data' => 'status', 'name' => 'status', 'title' => trans('labels.general.status'), 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'notificationreportinitialexpense_' . time();
    }
}
