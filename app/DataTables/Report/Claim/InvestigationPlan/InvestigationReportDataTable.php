<?php

namespace App\DataTables\Report\Claim\InvestigationPlan;

use App\User;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanReportRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;


class InvestigationReportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
        ->eloquent($this->query())
        ->editColumn('incident_date', function($data) {
            return Carbon::parse($data->incident_date)->format('d M, Y');
        })
        ->editColumn('notification_date', function($data) {
            return Carbon::parse($data->notification_date)->format('d M, Y');
        })
        ->editColumn('registration_date', function($data) {
            return Carbon::parse($data->registration_date)->format('d M, Y');
        })
        ->addColumn('wf_initiated_at', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->wf_initiated_at) ? Carbon::parse($plan->wf_initiated_at)->format('d M,Y') :'' ;
        })->addColumn('wf_done_date', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            if (!empty($plan->wf_done_date)) {
                return $plan->plan_status==2 ? Carbon::parse($plan->wf_done_date)->format('d M,Y') :'' ;
            }
            return '';
        })
        ->addColumn('wf_initiator', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->wf_initiator) ? (new UserRepository())->find($plan->wf_initiator)->name : '';
        })
        ->addColumn('plan_name', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->plan_name) ? $plan->plan_name : '';
        })
        ->addColumn('user_id', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->user_id) ? (new UserRepository())->find($plan->user_id)->name : '';
        })
        ->addColumn('start_date', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->start_date) ? Carbon::parse($plan->start_date)->format('d M,Y') :'';
        })
        ->addColumn('end_date', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->end_date) ? Carbon::parse($plan->end_date)->format('d M,Y') :'';
        })
        ->addColumn('duration', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->duration) ? $plan->duration : '';
        })
        ->addColumn('created_at', function($data) {
            $plan = (new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->created_at) ? Carbon::parse($plan->created_at)->format('d M,Y') :'';
        })->editColumn('investigation_status', function($data) {
            $plan =(new InvestigationPlanNotificationsRepository())->returnPlanByFilesId($data->id);
            return !empty($plan->investigation_status) ? $plan->investigation_status : '';
        })->rawColumns(['investigation_status']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
      $repo = new InvestigationPlanReportRepository();
      $query = $repo->getInvestigationReportForDatatable();
      
      return $this->applyScopes($query);
  }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->parameters([
            'dom'     => 'Bfrtip',
            'order'   => [[0, 'desc']],
            'buttons' => [
                'excel',
                'reset',
                'reload',
                'colvis',
            ],
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
       return [
        ['data' => 'filename', 'name' => 'filename', 'title' => 'Case No', 'orderable' => true, 'searchable' => true],
        ['data' => 'incident_type', 'name' => 'incident_type', 'title' => 'Incident', 'orderable' => true, 'searchable' => true],
        ['data' => 'employee', 'name' => 'employee', 'title' => 'Employee', 'orderable' => true, 'searchable' => true],
        ['data' => 'employer', 'name' => 'employer.name', 'title' => 'Employer ', 'orderable' => true, 'searchable' => true],
        ['data' => 'incident_date', 'name' => 'notification_reports.incident_date', 'title' => 'Incident Date', 'visible' => false,'orderable' => true, 'searchable' => true],
        ['data' => 'notification_date', 'name' => 'notification_reports.receipt_date', 'title' => "Notification Date", 'visible' => false, 'orderable' => false, 'searchable' => false],
        ['data' => 'registration_date', 'name' => 'notification_reports.created_at', 'title' => "Registration Date", 'orderable' => false, 'searchable' => false],
        ['data' => 'district', 'name' => 'districts.name', 'title' => "District", 'orderable' => false, 'visible' => false,'searchable' => false],
        ['data' => 'region', 'name' => 'regions.name', 'title' => "Region", 'orderable' => false, 'visible' => false,'searchable' => false],
        // ['data' => 'feedback', 'name' => 'feedback', 'title' => "financial year", 'orderable' => false, 'searchable' => false],
        ['data' => 'wf_initiated_at', 'name' => 'wf_initiated_at', 'title' => 'Plan Initiate Date', 'visible' => false, 'orderable' => true, 'searchable' => true],
        ['data' => 'wf_done_date', 'name' => 'wf_done_date', 'title' => 'Investigation Approval date', 'visible' => false, 'orderable' => true, 'searchable' => true],
        ['data' => 'wf_initiator', 'name' => 'wf_initiator', 'title' => "Workflow inititor", 'visible' => false, 'orderable' => false, 'searchable' => false],
        ['data' => 'plan_name', 'name' => 'plan_name', 'title' => "Investigation Plan Number",  'visible' => false,'orderable' => false, 'searchable' => false],
        ['data' => 'checklist_user', 'name' => 'checklist_user', 'title' => "Checklist user", 'orderable' => false, 'searchable' => false],
        ['data' => 'user_id', 'name' => 'user_id', 'title' => "Assigned Investigator", 'orderable' => false, 'visible' => false, 'searchable' => false],
        ['data' => 'start_date', 'name' => 'start_date', 'title' => "Investigation Start date", 'orderable' => false,'visible' => false, 'searchable' => false],
        ['data' => 'end_date', 'name' => 'end_date', 'title' => 'Investigation End date', 'orderable' => true, 'visible' => false, 'searchable' => true],
        ['data' => 'duration', 'name' => 'duration', 'title' => 'Investigation date', 'orderable' => true,  'visible' => false, 'searchable' => true],
        ['data' => 'created_at', 'name' => 'created_at', 'title' => "Investigation report Date", 'visible' => false, 'orderable' => false, 'searchable' => false],
        ['data' => 'investigation_status', 'name' => 'investigation_status', 'title' => "Investigation Status", 'orderable' => false, 'searchable' => false],


    ];
}

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'investigation_report' . time();
    }
}
