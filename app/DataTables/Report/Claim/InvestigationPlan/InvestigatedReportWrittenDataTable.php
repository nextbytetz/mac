<?php

namespace App\DataTables\Report\Claim\InvestigationPlan;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Access\UserRepository;
use Carbon\Carbon;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationOfficerRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;

class InvestigatedReportWrittenDataTable extends DataTable
{
    public function dataTable()
    {
        return $this->datatables
        ->eloquent($this->query())
        ->editColumn('investigation_category_cv_id', function($investigation) {
            return $investigation->name;
        })->editColumn('start_date', function($investigation) {
            return Carbon::parse($investigation->start_date)->format('d F, Y');
        })
        ->editColumn('end_date', function($investigation) {
            return Carbon::parse($investigation->end_date)->format('d F, Y');
        })
        ->editColumn('created_at', function($investigation) {
            return Carbon::parse($investigation->created_at)->format('d F, Y');
        })
        ->editColumn('wf_initiated_at', function($investigation) {
            return Carbon::parse($investigation->wf_initiated_at)->format('d F, Y');
        })
        ->editColumn('wf_done_date', function($investigation) {
            return Carbon::parse($investigation->wf_done_date)->format('d F, Y');
        })
       ->editColumn('registration_date', function($investigation) {
        return Carbon::parse($investigation->registration_date)->format('d F, Y');
    })
        ->editColumn('feedback', function($investigation) {
        return Carbon::parse($investigation->feedback)->format(' Y');
    })
    ->editColumn('plan_status', function($investigation) {
        $status = new InvestigationPlanRepository();
        return $status->returnPlanStatus($investigation->plan_status);
    })->rawColumns(['plan_status','wf_done_date']);
}


public function query()
{
    $report = new InvestigationPlanRepository();
    $this->query = $report->getGeneralInvestigationPlanForDataTable();
    return $this->applyScopes($this->query);
}


public function html()
{
    return $this->builder()
    ->columns($this->getColumns())
    ->minifiedAjax(route('backend.operation.report.investigated_report_written',['request'=>$this->request]))
    ->parameters([
        'dom'     => 'Bfrtip',
        'order'   => [[0, 'desc']],
        'buttons' => [

                // 'reset',
            'reload',
            'export',
            'colvis',
        ],
        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).click(function() {
                document.location.href = '". url("/") . "/operation/report/investigated_report_written/' + aData['id'];
                }).hover(function() {
                    $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                        });
                    }",
                ]);
}

protected function getColumns()
{


        
    return [
        // ['data' => 's_no', 'name' => 's_no', 'title' => "S/N", 'orderable' => false, 'searchable' => false],
        ['data' => 'filename', 'name' => 'filename', 'title' => 'Claim No', 'orderable' => true, 'searchable' => true],
        ['data' => 'incident_type', 'name' => 'incident_type', 'title' => 'incident_type', 'orderable' => true, 'searchable' => true],
        ['data' => 'employees', 'name' => 'employees', 'title' => 'employee Name', 'orderable' => true, 'searchable' => true],
        ['data' => 'employer', 'name' => 'employer.name', 'title' => 'employer Name', 'orderable' => true, 'searchable' => true],
        ['data' => 'incident_date', 'name' => 'incident_date', 'title' => 'incident_date', 'orderable' => true, 'searchable' => true],
        ['data' => 'receipt_date', 'name' => 'notification_reports.receipt_date', 'title' => "Receipt_date (Notification Date)", 'orderable' => false, 'searchable' => false],
        ['data' => 'registration_date', 'name' => 'notification_reports.created_at', 'title' => "registration_date", 'orderable' => false, 'searchable' => false],
        ['data' => 'district', 'name' => 'districts.name', 'title' => "district", 'orderable' => false, 'searchable' => false],
        ['data' => 'region', 'name' => 'regions.name', 'title' => "region", 'orderable' => false, 'searchable' => false],
        ['data' => 'feedback', 'name' => 'feedback', 'title' => "fin_year", 'orderable' => false, 'searchable' => false],
        ['data' => 'wf_initiated_at', 'name' => 'wf_initiated_at', 'title' => 'Investigation Plan inititation date', 'orderable' => true, 'searchable' => true],
        ['data' => 'wf_done_date', 'name' => 'wf_done_date', 'title' => 'Investigation Plan Approval date', 'orderable' => true, 'searchable' => true],
        ['data' => 'user', 'name' => 'user', 'title' => "Workflow inititor", 'orderable' => false, 'searchable' => false],
        ['data' => 'plan_name', 'name' => 'plan_name', 'title' => "Investigation Plan Number", 'orderable' => false, 'searchable' => false],
        ['data' => 'checklist_user', 'name' => 'checklist_user', 'title' => "Checklist user", 'orderable' => false, 'searchable' => false],
        ['data' => 'investigator', 'name' => 'investigator', 'title' => "Assigned Investigator", 'orderable' => false, 'searchable' => false],
        ['data' => 'start_date', 'name' => 'start_date', 'title' => "Investigation Plan Start date", 'orderable' => false, 'searchable' => false],
        ['data' => 'end_date', 'name' => 'end_date', 'title' => 'Investigation Plan End date', 'orderable' => true, 'searchable' => true],
        ['data' => 'created_at', 'name' => 'created_at', 'title' => "Investigation report Date (Create date)", 'orderable' => false, 'searchable' => false],
       ['data' => 'plan_status', 'name' => 'plan_status', 'title' => "Investigation Status", 'orderable' => false, 'searchable' => false],

    ];

}


protected function filename()
{
    return 'reportclaiminvestigationplaninvestigatedreportwrittendatatable_' . time();
}
}
