<?php

namespace App\DataTables\Report\Claim\InvestigationPlan;

use App\User;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationOfficerRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanNotificationsRepository;
use App\DataTables\Override;

class GeneralInvestigationPlanReportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
     return $this->datatables->eloquent($this->query())

     ->editColumn('investigation_category_cv_id', function($investigation) {
        return $investigation->name;
    })->editColumn('start_date', function($investigation) {
        return Carbon::parse($investigation->start_date)->format('d F, Y');
    })
    ->editColumn('end_date', function($investigation) {
        return Carbon::parse($investigation->end_date)->format('d F, Y');
    })
    ->editColumn('created_at', function($investigation) {
        return Carbon::parse($investigation->created_at)->format('d F, Y');
    })
    ->editColumn('wf_initiated_at', function($investigation) {
        return Carbon::parse($investigation->wf_initiated_at)->format('d F, Y');
    })->editColumn('registration_date', function($investigation) {
        return Carbon::parse($investigation->registration_date)->format('d F, Y');
    })
    ->editColumn('wf_done_date', function($investigation) {
        return Carbon::parse($investigation->wf_done_date)->format('d F, Y');
    })
    ->editColumn('feedback', function($investigation) {
        return Carbon::parse($investigation->feedback)->format(' Y');
    })
    ->editColumn('plan_status', function($investigation) {
        $status = new InvestigationPlanRepository();
        return $status->returnPlanStatus($investigation->plan_status);
    })
     // ->editColumn('checklist_user', function($investigation) {
     //        $checklist = new InvestigationPlanRepository();
     //        return $checklist;
     //    })

    ->rawColumns(['plan_status','wf_initiated_at','wf_done_date']);


}
public function query()
{
   $report = new InvestigationPlanRepository();
   $this->query = $report->getGeneralInvestigationPlanForDataTable();
   return $this->applyScopes($this->query);
}
public function html()
{
    return $this->builder()
    ->columns($this->getColumns())
    ->ajax(route("backend.operation.report.general_investigation_plan"))
    ->parameters([
        'dom'     => 'Bfrtip',
        'order'   => [[0, 'desc']],
        'buttons' => [
            'export',
            'reload',
            'colvis',
        ],
        'searching' => true,
        'serverSide' => true,
        'stateSave' => true,
        'paging' => true,
        'info' => true,
        'visible' => true,
    ]);
}
protected function getColumns()
{
    return [
       // ['data' => 's_no', 'name' => 's_no', 'title' => "S/N", 'orderable' => false, 'searchable' => false],
        ['data' => 'filename', 'name' => 'filename', 'title' => 'Claim No', 'orderable' => true, 'searchable' => true],
        ['data' => 'incident_type', 'name' => 'incident_type', 'title' => 'Incident type', 'orderable' => true, 'searchable' => true],
        ['data' => 'employees', 'name' => 'employees', 'title' => 'Employee', 'orderable' => true, 'searchable' => true],
        ['data' => 'employer', 'name' => 'employer.name', 'title' => 'Employer ', 'orderable' => true, 'searchable' => true],
        ['data' => 'incident_date', 'name' => 'notification_reports.incident_date', 'title' => 'Incident date', 'orderable' => true, 'searchable' => true],
        ['data' => 'receipt_date', 'name' => 'notification_reports.receipt_date', 'title' => "Notification Date", 'orderable' => false, 'searchable' => false],
        ['data' => 'created_at', 'name' => 'notification_reports.created_at', 'title' => "Registration date", 'orderable' => false, 'searchable' => false],
        ['data' => 'district', 'name' => 'districts.name', 'title' => "district", 'orderable' => false, 'visible' => false,'searchable' => false],
        ['data' => 'region', 'name' => 'regions.name', 'title' => "region", 'orderable' => false, 'searchable' => false],
        ['data' => 'feedback', 'name' => 'feedback', 'title' => "financial year", 'orderable' => false, 'searchable' => false],
        ['data' => 'wf_initiated_at', 'name' => 'wf_initiated_at', 'title' => 'Investigation Plan inititation date', 'orderable' => true, 'searchable' => true],
        ['data' => 'wf_done_date', 'name' => 'wf_done_date', 'title' => 'Investigation Plan Approval date', 'orderable' => true, 'searchable' => true],
        ['data' => 'user', 'name' => 'user', 'title' => "Workflow inititor", 'orderable' => false, 'searchable' => false],
        ['data' => 'plan_name', 'name' => 'plan_name', 'title' => "Investigation Plan Number",  'visible' => false,'orderable' => false, 'searchable' => false],
        ['data' => 'checklist_user', 'name' => 'checklist_user', 'title' => "Checklist user", 'orderable' => false, 'searchable' => false],
        ['data' => 'investigator', 'name' => 'investigator', 'title' => "Assigned Investigator", 'orderable' => false, 'searchable' => false],
        ['data' => 'start_date', 'name' => 'start_date', 'title' => "Investigation Plan Start date", 'orderable' => false, 'searchable' => false],
        ['data' => 'end_date', 'name' => 'end_date', 'title' => 'Investigation plan end date', 'orderable' => true, 'searchable' => true],
        ['data' => 'duration', 'name' => 'duration', 'title' => 'Investigation date', 'orderable' => true,  'visible' => false, 'searchable' => true],
        ['data' => 'created_at', 'name' => 'created_at', 'title' => "Investigation report Date", 'visible' => false, 'orderable' => false, 'searchable' => false],
        ['data' => 'plan_status', 'name' => 'plan_status', 'title' => "Investigation Status", 'orderable' => false, 'searchable' => false],


    ];
}

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'General_Investigation_report_' . time();
    }
}
