<?php

namespace App\DataTables\Report\Claim\InvestigationPlan;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationOfficerRepository;

class InvestigatedReportUnWrittenDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
     public function dataTable()
    {

         return $this->datatables
        ->eloquent($this->query())
        ->editColumn('investigation_category_cv_id', function($investigation) {
            return $investigation->name;
        })->editColumn('start_date', function($investigation) {
            return Carbon::parse($investigation->start_date)->format('d F, Y');
        })
        ->editColumn('created_at', function($investigation) {
            return Carbon::parse($investigation->created_at)->format('d F, Y');
       })
        ->editColumn('created_at', function($investigation) {
            return Carbon::parse($investigation->created_at)->format('d F, Y');
    })
        ->editColumn('plan_status', function($investigation) {
            $status = new InvestigationPlanRepository();
            return $status->returnPlanStatusLabel($investigation->plan_status);
        })->rawColumns(['plan_status']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
       
        $investigation = new InvestigationPlanRepository();
        $this->query = $investigation->getInvestigationPlanForDataTable();
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax(route('backend.claim.investigations.index',['request'=>$this->request]))
        ->addAction(['width' => '80px'])
        ->parameters([
            'dom'     => 'Bfrtip',
            'order'   => [[0, 'desc']],
            'buttons' => [
                'export',
                // 'csv',
                // 'reset',
                'reload',
            ],
            'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/claim/investigations/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
      return [
            ['data' => '', 'name' => 'ID', 'title' => "S/No", 'orderable' => false, 'searchable' => false],
            ['data' => 'plan_name', 'name' => 'plan_name', 'title' => 'Claim No', 'orderable' => true, 'searchable' => true],
            ['data' => 'investigation_category_cv_id', 'name' => 'investigation_category_cv_id', 'title' => 'incident_type', 'orderable' => true, 'searchable' => true],
            ['data' => 'start_date', 'name' => 'start_date', 'title' => 'employee Name', 'orderable' => true, 'searchable' => true],
            ['data' => 'duration', 'name' => 'duration', 'title' => 'employer Name', 'orderable' => true, 'searchable' => true],
            ['data' => 'incident_date', 'name' => 'incident_date', 'title' => 'incident_date', 'orderable' => true, 'searchable' => true],
            ['data' => 'user', 'name' => 'user', 'title' => "Receipt_date (Notification Date)", 'orderable' => false, 'searchable' => false],
            ['data' => 'registration_date', 'name' => 'registration_date', 'title' => "registration_date", 'orderable' => false, 'searchable' => false],
            ['data' => 'user', 'name' => 'user', 'title' => "district", 'orderable' => false, 'searchable' => false],
            ['data' => 'user', 'name' => 'user', 'title' => "region", 'orderable' => false, 'searchable' => false],
            ['data' => 'user', 'name' => 'user', 'title' => "fin_year", 'orderable' => false, 'searchable' => false],
             ['data' => 'duration', 'name' => 'duration', 'title' => 'Investigation Plan inititation date', 'orderable' => true, 'searchable' => true],
            ['data' => 'plan_status', 'name' => 'plan_status', 'title' => 'Investigation Plan Approval date', 'orderable' => true, 'searchable' => true],
            ['data' => 'user', 'name' => 'user', 'title' => "Workflow inititor", 'orderable' => false, 'searchable' => false],
            ['data' => 'user', 'name' => 'user', 'title' => "Investigation Plan Number", 'orderable' => false, 'searchable' => false],
            ['data' => 'user', 'name' => 'user', 'title' => "Checklist user", 'orderable' => false, 'searchable' => false],
            ['data' => 'user', 'name' => 'user', 'title' => "Assigned Investigator", 'orderable' => false, 'searchable' => false],
            ['data' => 'user', 'name' => 'user', 'title' => "Investigation Plan Start date", 'orderable' => false, 'searchable' => false],
             ['data' => 'duration', 'name' => 'duration', 'title' => 'Investigation plan end date', 'orderable' => true, 'searchable' => true],
            ['data' => 'plan_status', 'name' => 'plan_status', 'title' => 'Investigation date', 'orderable' => true, 'searchable' => true],
            ['data' => 'user', 'name' => 'user', 'title' => "Investigation report Date (Create date)", 'orderable' => false, 'searchable' => false],
            ['data' => 'user', 'name' => 'user', 'title' => "Investigation Status", 'orderable' => false, 'searchable' => false],
           
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportclaiminvestigationplaninvestigatedreportunwrittendatatable_' . time();
    }
}
