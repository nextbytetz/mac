<?php

namespace App\DataTables\Report\Claim;

use App\User;
use Yajra\Datatables\Services\DataTable;
use DB;
use Log;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;

class ClaimAccrualAssessmentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
       return $this->datatables->eloquent($this->query());
   }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
     $accrual = new  AccrualNotificationReportRepository();
     $this->query = $accrual->getForDataTable();
     return $this->applyScopes($this->query);
 }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
         return $this->builder()
            ->Columns($this->getColumns())
            ->minifiedAjax(route("backend.operation.report.accrued_assessment",['report_id'=>105,'request'=>$this->request]))
            ->parameters([
                 'dom' => 'Bfrtip',
                        'buttons' => ['excel','csv', 'reload','colvis'],
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => true,
                        'info' => true,
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);





 }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
       
       return [
        [ 'data'=> 'id' , 'name'=> 'accrual_notification_benefit.id','title' => "S/No", 'orderable' => false, 'searchable' =>  false, 'visible' => true],
        [ 'data'=> 'incident_type' , 'name'=> 'incident_types','title' => "Incident Type", 'orderable' => false, 'searchable' =>  false, 'visible' => true],
        [ 'data'=> 'filename' , 'name'=> 'filename' ,'title' => "Clam no", 'orderable' => true, 'searchable' => true],
        [ 'data'=> 'employee' , 'name'=> 'employees.name','title' => "Employee Name",  'orderable' => true, 'searchable' => true],
        [ 'data'=> 'employer' , 'name'=> 'employers.name','title' => "Employer",  'orderable' => true, 'searchable' => true],
        [ 'data'=> 'occupation' , 'name'=> 'job_titles.name','title' => "Occupation", 'searchable' => false],
        [ 'data'=> 'gender' , 'name'=> 'gender' ,'title' => "Sex", 'orderable' => true, 'searchable' => false],
        [ 'data'=> 'dob' , 'name'=> 'dob.name','title' => "Date of Birth"],
        [ 'data'=> 'main_activity' , 'name'=> 'main_activity', 'orderable' => false,'title' => "MAIN ACTIVITY OF ESTABLISHMENT",  'searchable' => false], 
        [ 'data'=> 'incident_date' , 'name'=> 'notification_reports.incident_date', 'orderable' => true,'title' => "Date of Incident",  'searchable' => true],
        [ 'data'=> 'incident_time' , 'name'=> 'notification_reports.incident_time', 'orderable' => true,'title' => 'Incident Time',  'searchable' => false],
        [ 'data'=> 'age' , 'name'=> 'age','title' => "age", 'searchable' => false],
        [ 'data'=> 'gme' , 'name'=> 'notification_reports.receipt_date','title' => "GME", 'searchable' => false],
        [ 'data'=> 'accident_cause' , 'name'=> 'accident_cause_by_agent.name','title' => "CAUSE OF ACCIDENT", 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => "NATURE OF ACCIDENT", 'orderable' => true,'searchable' => true],
        [ 'data'=> '' , 'name'=> '','title' => 'ACCIDENT OUTCOME', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'No. of Days Lost', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'LIGHT DUTIES', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => '% of PERMANENT DISABILITY', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'DATE OF MMI/DEATH', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'DATE OF PAYMENT', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'No. OF MONTHS', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'NAME OF DEPENDANTS', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'No OF CHILDREN', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'MAE', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'TTD', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'TPD', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'PPD (Lumpsum)', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'PPD (Pension)', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'PPD (Cumulative Pension)', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'PTD', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'REH', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'CAC', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'FUNERAL GRANT', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'SPOUSE 1', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'SPOUSE 2', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'CHILD 1', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'CHILD 2', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'CHILD 3', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'CHILD 4', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'PARENT 1', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'PARENT 2', 'orderable' => true, 'searchable' => false],
          [ 'data'=> '' , 'name'=> '','title' => 'TOTAL', 'orderable' => true, 'searchable' => false],
        [ 'data'=> '' , 'name'=> '','title' => 'REMARK', 'orderable' => true, 'searchable' => false]
       
    ];
}

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportclaimclaimaccrualassessmentdatatable_' . time();
    }
}
