<?php

namespace App\DataTables\Report\Claim;

use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;

class NotificationReportReceivedDataTable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        $notification_reports = new NotificationReportRepository();
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('incident_type_id', function($notification_report) {
                return $notification_report->incidentType->name;
            })
            ->editColumn('employee_id', function($notification_report) {
                return  ($notification_report->employee_id) ? $notification_report->employee->name : $notification_report->employee_name;
            })
            ->addColumn('employer', function($notification_report) {
                return  ($notification_report->employer_id) ? $notification_report->employer->name : $notification_report->employer_name;
            })
            ->addColumn('business', function($notification_report) {
                $return = ($notification_report->employer_id) ? (($notification_report->employer->sectors()->count() > 0) ? $notification_report->employer->sectors()->first()->name : "") : null;
                //$return = '';
                return $return;
            })
            ->addColumn('region', function($notification_report) {
                     //$return = ($notification_report->district()->count()) ? $notification_report->district->region->name : "";
                $return = ($notification_report->district_id) ? (($notification_report->district->region()->count() > 0) ? $notification_report->district->region->name : ' ') : '';
                return $return;
            })
            ->addColumn('district', function($notification_report) {
                $return = ($notification_report->district_id) ? $notification_report->district->name : "";
                return $return;
            })
            ->addColumn('incident_date', function($notification_report) use($notification_reports) {
                //return $notification_reports->findIncidentDate($notification_report->id);
                return $notification_report->incident_date;
            })
            ->addColumn('receipt_date', function($notification_report) use($notification_reports) {
                //return $notification_reports->findReceiptDate($notification_report->id);
                return $notification_report->receipt_date;
            })
            ->editColumn('status', function($notification_report) {
                return $notification_report->status_label;
            })
            ->rawColumns(['status']);


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;

        $notification_reports = new NotificationReportRepository();
        $this->query = $notification_reports->query()->whereRaw("created_at::date between ? and ?", [$from_date, $to_date]);;
//         with incident
        if (isset($this->input['incident_type_id'])) {
            if ($this->input['incident_type_id'] > 0) {
                $this->query = $this->queryWithIncidentType();
            }
        }
//        with status
        if (isset($this->input['status_id'])) {
            if (($this->input['status_id'] >= 0) && ($this->input['status_id'] < 3)) {
                $this->query = $this->queryWithStatus();
            }
        }
//        with region
        if (isset($this->input['region_id'])) {
            if ($this->input['region_id'] > 0) {
                $this->query = $this->queryWithRegion();
            }
        }
//        with district
        if (isset($this->input['district_id'])) {
            if ($this->input['district_id'] > 0) {
                $this->query = $this->queryWithDistrict();
            }
        }
//        with Business
        if (isset($this->input['business_id'])) {
            if ($this->input['business_id'] > 0) {
                $this->query = $this->queryWithBusiness();
            }
        }


//        with Employer
        if (isset($this->input['employer_id'])) {
            if ($this->input['employer_id'] > 0) {
                $this->query = $this->queryWithEmployer();
            }
        }

        return $this->applyScopes($this->query);
    }

    /*
  * query date range with incident type selected
  */
    public function queryWithIncidentType()
    {
        return  $this->query->where('incident_type_id', $this->input['incident_type_id']);
    }


    /*
* query date range with with Status selected
*/
    public function queryWithStatus()
    {
        return  $this->query->where('status', $this->input['status_id']);
    }


    /*
* query date range with with region selected
*/
    public function queryWithRegion()
    {
        /*return $this->query->whereHas('employer', function ($query) {
            $query->where('region_id', $this->input['region_id']);
        });*/
        return $this->query->whereHas('district', function ($query) {
            $query->whereHas("region", function ($query) {
                $query->where("regions.id", $this->input['region_id']);
            });
        });
    }

    /*
* query date range with with district selected
*/
    public function queryWithDistrict()
    {
        return $this->query->where("district_id", $this->input['district_id']);
    }


    /*
* query date range with with business selected
*/
    public function queryWithBusiness()
    {
        return $this->query->whereHas('employer', function ($query) {
            $query->whereHas('sectors', function ($query) {
                $query->where('business_sector_cv_id',$this->input['business_id']);
            });
        });

    }

    /*
* query date range with with employer selected
*/
    public function queryWithEmployer()
    {
        return $this->query->where('employer_id', $this->input['employer_id']);
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/claim/notification_report/profile/' + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => "ID", 'orderable' => true, 'searchable' => false],
            ['data' => 'filename', 'name' => 'id', 'title' => trans('labels.backend.legal.case_no'), 'orderable' => false, 'searchable' => true],
            ['data' => 'incident_type_id', 'name' => 'incident_type_id', 'title' => trans('labels.backend.claim.incident_type'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employee_id', 'name' => 'employee_id', 'title' => trans('labels.backend.compliance.employee'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer', 'name' => 'employer', 'title' => trans('labels.backend.compliance.employer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'business', 'name' => 'business', 'title' => trans('labels.general.business'), 'orderable' => false, 'searchable' => false],
            ['data' => 'incident_date', 'name' => 'incident_date', 'title' => trans('labels.backend.table.incident_date'), 'orderable' => false, 'searchable' => false],
            ['data' => 'receipt_date', 'name' => 'receipt_date', 'title' => trans('labels.backend.claim.receipt_date'), 'orderable' => false, 'searchable' => false],
            ['data' => 'region', 'name' => 'region', 'title' => "Region", 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'district', 'name' => 'district', 'title' => "District", 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => "Registration Date", 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'status', 'name' => 'status', 'title' => trans('labels.general.status'), 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'notificationreportreceived_' . time();
    }
}
