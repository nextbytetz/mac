<?php

namespace App\DataTables\Report\Claim;

use App\User;
use Yajra\Datatables\Services\DataTable;
use DB;
use Log;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualClaimReportRepository;


class ClaimAccrualGeneralDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->eloquent($this->query());
            // ->addColumn('action', 'report/claim/claimaccrualpaiddatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {


        $claim_accrual = new  AccrualClaimReportRepository();
         // dd($claim_accrual);
        $this->query =$claim_accrual->getClaimAccrualReportForDatatable();
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
      return $this->builder()
      ->Columns($this->getColumns())
            // ->minifiedAjax(route("backend.operation.report.claim_accrual_report",['report_id'=>108,'request'=>$this->request]))
      ->parameters([
        'dom'     => 'Bfrtip',
        'order'   => [[0, 'desc']],
        'buttons' => [
            'export',
            'reload',
            'colvis',
        ],
        'searching' => true,
        'serverSide' => true,
        'stateSave' => true,
        'paging' => true,
        'info' => true,
        'visible' => true,
    ]);
  }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

     return [
        [ 'data'=> 'id' , 'name'=> 'accrual_notification_benefit.id','title' => "S/No", 'orderable' => false, 'searchable' =>  false, 'visible' => true],
        [ 'data'=> 'incident_type' , 'name'=> 'incident_types','title' => "Incident Type", 'orderable' => false, 'searchable' =>  false, 'visible' => true],
        [ 'data'=> 'filename' , 'name'=> 'filename' ,'title' => "Claim No", 'orderable' => true, 'searchable' => true],
        [ 'data'=> 'employee' , 'name'=> 'employees.name','title' => "Employee",  'orderable' => true, 'searchable' => true],
        [ 'data'=> 'employer' , 'name'=> 'employers.name','title' => "Employer",  'orderable' => true, 'searchable' => true],
        // [ 'data'=> 'incident_date' , 'name'=> 'notification_reports.incident_date', 'title' => "Date of Incident",'visible' => false,'orderable' => true, 'searchable' => true],
        // [ 'data'=> 'incident_time' , 'name'=> 'notification_reports.incident_time', 'orderable' => true,'title' => 'Incident Time',  'searchable' => false, 'visible' => false],
        // [ 'data'=> 'occupation' , 'name'=> 'job_titles.name','title' => "Occupation", 'searchable' => false, 'visible' => false],
        // [ 'data'=> 'date_of_birth' , 'name'=> 'accrual_pensioners.dob','title' => "Date of Birth", 'visible' => false],     
        [ 'data'=> 'age' , 'name'=> 'day_hours','title' => "Age", 'searchable' => false, 'visible' => false],
        // [ 'data'=> 'activity_performed' , 'name'=> 'accidents.activity_performed', 'title' => "Main Activity Of Establishment",  'orderable' => false,'searchable' => false,'visible'=>false], 
        // [ 'data'=> 'accident_cause' , 'name'=> 'code_values.name','title' => "Cause Of Accident",'visible'=>false, 'searchable' => false],
        // [ 'data'=> 'nature' , 'name'=> 'accident_types.name','title' => "Nature Of Accident", 'orderable' => true,'searchable' => true],
        // [ 'data'=> 'accident_place' , 'name'=> 'accidents.accident_place','title' => 'Location Of Injuries ', 'orderable' => true, 'visible' => false,'searchable' => false],
        // [ 'data'=> 'dependant' , 'name'=> 'accrual_dependents.dependant','title' => 'Name Of dependant', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'days' , 'name'=> 'days','title' => 'No. of Days Lost', 'orderable' => true, 'visible'=>false,'searchable' => false],
        // [ 'data'=> 'percent_of_ed_ld' , 'name'=> 'percent_of_ed_ld','title' => 'Light Duties', 'orderable' => true, 'visible'=>false, 'searchable' => false],
        // [ 'data'=> 'pd_percent', 'name'=> 'pd_percent','title' => '% of Permenent Disability', 'orderable' => true, 'visible'=>false, 'searchable' => false],
        [ 'data'=> 'date_of_mmi' , 'name'=> 'date_of_mmi','title' => 'DATE OF MMI/DEATH', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'funeral_grant' , 'name'=> 'funeral_grant','title' => 'Funeral Grant', 'orderable' => true,'visible'=>false, 'searchable' => false],
        // [ 'data'=> 'parent' , 'name'=> 'employees.name','title' => 'PARENT 1', 'orderable' => true,'visible'=>false, 'searchable' => false],
        // [ 'data'=> 'payment' , 'name'=> 'accrual_claim_compersations.created_at','title' => 'DATE OF PAYMENT', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'pay_period' , 'name'=> 'accrual_pesnioners.pay_period','title' => 'No. OF MONTHS', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'gender' , 'name'=> 'gender' ,'title' => "Sex", 'orderable' => true, 'searchable' => false,'visible' => false],



        //        [ 'data'=> 'outcome' , 'name'=> 'incident_types.name','title' => 'Accident Outcome', 'orderable' => true, 'visible' => false,'searchable' => false],

        // [ 'data'=> 'gme' , 'name'=> 'notification_reports.receipt_date','title' => "GME", 'visible'=>false, 'searchable' => false],       
        // [ 'data'=> 'accident_place' , 'name'=> '','title' => 'No OF CHILDREN', 'orderable' => true, 'visible'=>false, 'searchable' => false],
        // [ 'data'=> 'amount' , 'name'=> 'accrual_medical_expenses.amount','title' => 'MAE', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'amount_ttd' , 'name'=> '','title' => 'TTD', 'orderable' => true, 'visible'=>false, 'searchable' => false],
        // [ 'data'=> 'amount_tpd' , 'name'=> '','title' => 'TPD', 'orderable' => true, 'visible'=>false, 'searchable' => false],
        // [ 'data'=> 'amount_pd' , 'name'=> 'pd','title' => 'PPD (Lumpsum)', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'amount_pd' , 'name'=> '','title' => 'PPD (Pension)', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'amount_ptd_cumlative' , 'name'=> '','title' => 'PPD (Cumulative Pension)', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'ptd' , 'name'=> '','title' => 'PTD', 'orderable' => true,  'visible'=>false,'searchable' => false],
        // [ 'data'=> 'reh' , 'name'=> '','title' => 'REH', 'orderable' => true, 'visible'=>false,'searchable' => false],
        // [ 'data'=> 'cac' , 'name'=> '','title' => 'CAC', 'orderable' => true, 'visible'=>false,'searchable' => false],
        //  [ 'data'=> 'spouse' , 'name'=> '','title' => 'SPOUSE 1', 'orderable' => true,'visible'=>false, 'searchable' => false],
       //  // [ 'data'=> 'spuse' , 'name'=> '','title' => 'SPOUSE 2', 'orderable' => true, 'visible'=>false,'searchable' => false],
       //  [ 'data'=> 'child' , 'name'=> '','title' => 'CHILD 1', 'orderable' => true, 'visible'=>false,'searchable' => false],
       //  [ 'data'=> '' , 'name'=> '','title' => 'CHILD 2', 'orderable' => true, 'visible'=>false,'searchable' => false],
       //  [ 'data'=> '' , 'name'=> '','title' => 'CHILD 3', 'orderable' => true,'visible'=>false, 'searchable' => false],
       //  [ 'data'=> '' , 'name'=> '','title' => 'CHILD 4', 'orderable' => true, 'visible'=>false,'searchable' => false],
       //  [ 'data'=> '' , 'name'=> '','title' => 'PARENT 2', 'orderable' => true, 'visible'=>false,'searchable' => false],
       // [ 'data'=> '' , 'name'=> '','title' => 'TOTAL', 'orderable' => true, 'visible'=>false,'searchable' => false],
       //  [ 'data'=> '' , 'name'=> '','title' => 'REMARK', 'orderable' => true, 'visible'=>false,'searchable' => false]

    ];
}

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Claim_Accrual_report_' . time();
    }
}
