<?php

namespace App\DataTables\Report\Claim;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Operation\Claim\ClaimFollowupRepository;
use App\Repositories\Backend\Operation\Claim\ClaimFollowUpReportRepository;


class NotificationFollowUpDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
        ->eloquent($this->query());
            // ->addColumn('action', 'report/claim/notificationfollowupdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $report = new ClaimFollowUpReportRepository();
        $this->query = $report->getClaimFollowupReportForDatatable();
        return $this->applyScopes($this->query);
    }


    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax('')
        ->parameters([
            'dom'     => 'Bfrtip',
            'order'   => [[0, 'desc']],
            'buttons' => [
                'export',
                'reload',
                'colvis',
            ],
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           ['data' => 'filename', 'name' => 'filename', 'title' => 'Claim No', 'orderable' => true, 'searchable' => true],
           ['data' => 'incident_type', 'name' => 'incident_type', 'title' => 'Incident Type', 'orderable' => true, 'searchable' => true],
           ['data' => 'week_date', 'name' => 'week_date', 'title' => 'Weakly', 'orderable' => true, 'searchable' => true],
           ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Monthly', 'orderable' => true, 'searchable' => true],
           ['data' => 'date_of_follow_up', 'name' => 'date_of_follow_up', 'title' => 'Quarterly ', 'orderable' => true, 'searchable' => true],
    // ['data' => 'monthly', 'name' => 'monthly', 'title' => 'Annualy', 'orderable' => true, 'searchable' => true],
           ['data' => 'incident_date', 'name' => 'notification_reports.incident_date', 'title' => 'Incident date', 'orderable' => true, 'searchable' => true],
           ['data' => 'district', 'name' => 'districts.name', 'title' => "District", 'orderable' => false, 'visible' => true,'searchable' => false],
           ['data' => 'region', 'name' => 'regions.name', 'title' => "Region", 'orderable' => false, 'searchable' => false],
           ['data' => 'contact_user', 'name' => 'contact_user', 'title' => "Contact Person", 'orderable' => false, 'searchable' => false],
         // ['data' => '', 'name' => '', 'title' => "Contact Person", 'orderable' => false, 'searchable' => false],

       ];
   }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Claim_FollowUp_report' . time();
    }
}
