<?php
namespace App\DataTables\Report\Payroll;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollChildExtensionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollVerificationRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class PayrollChildExtensionsDataTable extends DataTable
{
    protected $query;


    /*Results from query() method*/
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('wf_done_date', function($query) {
                return short_date_format($query->wf_done_date);
            })
            ->editColumn('deadline_date', function($query) {
                return short_date_format($query->deadline_date);
            })
            ->rawColumns(['']);
    }

    /*Get query source of DataTable*/
    public function query()
    {
        $input = $this->input;
        $this->query = $this->searchResult($input);
        return $this->query;
    }

    /*Search Results*/
    public function searchResult($data)
    {
        $child_extension_repo = (new PayrollChildExtensionRepository());
        $this->query = $child_extension_repo->query()->select(
            'b.filename',
            'b.member_name',
            'b.employee_name',
            'cv_ext.name as extension_reason',
 DB::raw(" (case when payroll_child_extensions.period_provision_type = 1 then 'Specific Period' else  'Lifetime' end) as  period_provision "),
            'payroll_child_extensions.deadline_date',
            'payroll_child_extensions.school_name',
            'cv_edu.name as education_level',
            'payroll_child_extensions.school_grade',
            's.wf_done_date',
            's.member_type_id',
            's.resource_id',
            's.employee_id'
        )
            ->join('payroll_status_changes as s', 's.id', 'payroll_child_extensions.payroll_status_change_id')
            ->join('payroll_beneficiaries_view as b', function ($join){
                $join->on('b.resource_id', 's.resource_id')->whereRaw("b.member_type_id = s.member_type_id")->whereRaw("b.employee_id = s.employee_id");
            })->join('code_values as cv_edu', 'cv_edu.id', 'payroll_child_extensions.education_level_cv_id' )
            ->join('code_values as cv_ext', 'cv_ext.id', 'payroll_child_extensions.child_continuation_reason_cv_id' )
            ->whereBetween('s.wf_done_date', [$data['from_date'], $data['to_date']]);

        /*query with type*/
        if(isset($data['type'])){
            $this->query = $this->queryWithType($data);
        }

        return $this->query;
    }

    /*Search query with type*/
    public function queryWithType($data)
    {
        $type_id = $data['type'];
        return $this->query->where('payroll_child_extensions.child_continuation_reason_cv_id', $type_id);
    }


    /*Total*/
    public function total($data)
    {
        $total = $this->searchResult($data)->sum('total');
        return $total;
    }

    /*Export / Download*/
    public function download($data){

        $members = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($members as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');
    }


    /*Optional method if you want to use html builder.*/
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                         document.location.href = '". url("/") . "/payroll/beneficiary_profile/' + aData['member_type_id'] + '/' + aData['resource_id'] + '/' + aData['employee_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /*Get Columns*/
    protected function getColumns()
    {
        return [
            ['data' => 'filename', 'name' => 'filename', 'title' => 'filename', 'orderable' => false, 'searchable' => false],
            ['data' => 'member_name', 'name' => 'member_name', 'title' => 'Member Name', 'orderable' => false, 'searchable' => false],
            ['data' => 'employee_name', 'name' => 'employee_name', 'title' => 'Employee Name', 'orderable' => false, 'searchable' => false],
            ['data' => 'extension_reason', 'name' => 'cv_edu.name', 'title' => 'Extension Reason', 'orderable' => false, 'searchable' => false],
            ['data' => 'period_provision', 'name' => 'period_provision', 'title' => 'Period Provision', 'orderable' => false, 'searchable' => false],
            ['data' => 'deadline_date', 'name' => 'deadline_date', 'title' => 'Deadline Date', 'orderable' => false, 'searchable' => false],
            ['data' => 'school_name', 'name' => 'school_name', 'title' => 'School Name', 'orderable' => false, 'searchable' => false, 'visible'=> false],
            ['data' => 'education_level', 'name' => 'education_level', 'title' => 'Education Level', 'orderable' => false, 'searchable' => false, 'visible'=> false],
            ['data' => 'school_grade', 'name' => 'school_grade', 'title' => 'School Grade', 'orderable' => false, 'searchable' => false, 'visible'=> false],
            ['data' => 'wf_done_date', 'name' => 'wf_done_date', 'title' => 'Approved Date', 'orderable' => false, 'searchable' => false],
        ];
    }

    /*Get filename for export file*/
    public function filename()
    {
        return 'payrollchildextensions' . date('YmdHis');
    }


}


