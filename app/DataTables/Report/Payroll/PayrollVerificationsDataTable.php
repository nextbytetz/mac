<?php
namespace App\DataTables\Report\Payroll;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollVerificationRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class PayrollVerificationsDataTable extends DataTable
{
    protected $query;


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables->of($this->query())
            ->editColumn('monthly_pension_amount', function($member) {
                return number_2_format($member->monthly_pension_amount);
            })
            ->editColumn('dob', function($member) {
                return short_date_format($member->dob);
            })
            ->editColumn('lastresponse', function($member) {
                return short_date_format($member->lastresponse);
            })
            ->editColumn('next_verification_date', function($member) {
                return short_date_format($member->next_verification_date);
            })
            ->rawColumns(['status']);
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query = $this->searchResult($this->input);

        return$this->applyScopes($this->query);
    }


    /**
     * @param $data
     * @return mixed
     */
    public function searchResult($data){

        $from_date = standard_date_format($data['from_date']);
        $to_date = standard_date_format($data['to_date']);
        //Search date range
        $this->query =(new PayrollVerificationRepository())->getQueryVerificationAllDistinct();

        if (isset($data['member_type_id'])) {
            if (($data['member_type_id'] > 0)) {
                $this->query = $this->queryWithMemberType($data);
            }
        }

        /*Verified/unverified*/
        if (isset($data['type'])) {
            if (($data['type'] == 2)) {
                /*unverified*/
                $this->query = $this->query->whereRaw("(coalesce(v.verification_date::date, b.lastresponse::date) < ? and b.lastresponse::date < ?)", [$from_date, $from_date]);
            }else{
                /*verified - default*/
                $this->query = $this->query->whereNotNull('v.id')->whereRaw("coalesce(v.verification_date::date, b.lastresponse::date) >= ? and  coalesce(v.verification_date::date, b.lastresponse::date) <= ?", [$from_date, $to_date]);
            }
        }
        return $this->query->orderBy('lastresponse');

    }


    /*
     * query date range with member type selected
     */
    public function queryWithMemberType($data)
    {
        if($data['member_type_id']  ==  1){
            /*Search for contact care assistant*/
            return  $this->query->where('b.member_type_id', 4)->where('b.dependent_type_id', 7);

        }elseif($data['member_type_id']  ==  4){
            /* dependents*/
            return  $this->query->where('b.member_type_id', $data['member_type_id'])->where('b.dependent_type_id','<>', 7);
        }else{
            /*pensioner */
            return  $this->query->where('b.member_type_id', $data['member_type_id']);
        }

    }


    public function download($data){

        $members = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($members as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                   document.location.href = '". url("/") . "/payroll/beneficiary_profile/' + aData['member_type_id'] + '/' + aData['resource_id'] + '/' + aData['employee_id'];
                                       }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'filename', 'name' => 'filename', 'title' => 'Filename', 'orderable' => false, 'searchable' => false],
            ['data' => 'member_name', 'name' => 'member_name', 'title' => 'Beneficiary', 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employer_name', 'title' => 'Employer', 'orderable' => false, 'searchable' => false],
            ['data' => 'employee_name', 'name' => 'employee_name', 'title' => 'Employee', 'orderable' => false, 'searchable' => false],
            ['data' => 'district', 'name' => 'district', 'title' => 'District', 'orderable' => false, 'searchable' => false, 'visible' => true],
            ['data' => 'region', 'name' => 'region', 'title' => 'Region', 'orderable' => false, 'searchable' => false, 'visible' => true],
            ['data' => 'phone', 'name' => 'phone', 'title' => 'Phone', 'orderable' => false, 'searchable' => false, 'visible' => trur],
            ['data' => 'dob', 'name' => 'dob', 'title' => 'DOB', 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'monthly_pension_amount', 'name' => 'monthly_pension_amount', 'title' => 'Monthly pension', 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'bank_name', 'name' => 'bank_name', 'title' => 'Bank', 'orderable' => false, 'searchable' => false],
//            ['data' => 'bank_branch_name', 'name' => 'bank_branches.name', 'title' => 'Bank branch', 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'accountno', 'name' => 'accountno', 'title' => 'Accountno', 'orderable' => false, 'searchable' => false],
            ['data' => 'lastresponse', 'name' => 'lastresponse', 'title' => 'Last verification', 'orderable' => false, 'searchable' => false],
            ['data' => 'next_verification_date', 'name' => 'next_verification_date', 'title' => 'Next verification Date', 'orderable' => false, 'searchable' => false],
            ['data' => 'days_left', 'name' => 'days_left', 'title' => 'Day left', 'orderable' => false, 'searchable' => false],
            ['data' => 'relationship', 'name' => 'relationship', 'title' => 'Relationship', 'orderable' => false, 'searchable' => false],
        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'payrollbeneficiaries' . time();
    }
}


