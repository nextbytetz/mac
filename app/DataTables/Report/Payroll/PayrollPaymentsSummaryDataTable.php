<?php
namespace App\DataTables\Report\Payroll;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollChildExtensionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollVerificationRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class PayrollPaymentsSummaryDataTable extends DataTable
{
    protected $query;

    /*Results from query() method*/
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->rawColumns(['']);
    }

    /*Get query source of DataTable*/
    public function query()
    {
        $input = $this->input;
        $this->query = $this->searchResult($input);
        return $this->query;
    }

}


