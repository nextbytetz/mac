<?php
namespace App\DataTables\Report\Payroll;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class PayrollBeneficiariesDataTable extends DataTable
{
    protected $query;


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables->of($this->query())
            ->editColumn('monthly_pension_amount', function($member) {
                return number_2_format($member->monthly_pension_amount);
            })
            ->editColumn('dob', function($member) {
                return short_date_format($member->dob);
            })
            ->editColumn('lastresponse', function($member) {
                return short_date_format($member->lastresponse);
            })
            ->editColumn('last_payroll_date', function($member) {
                return  isset($member->last_payroll_date) ? short_date_format($member->last_payroll_date) : '';
            })
            ->editColumn('payroll_start_date', function($member) {
                return  isset($member->payroll_start_date) ? short_date_format($member->payroll_start_date) : '';
            })
            ->editColumn('total_pension_paid', function($member) {
                return isset($member->total_pension_paid) ? number_2_format($member->total_pension_paid) : 0;
            })
            ->editColumn('gme', function($member) {
                return isset($member->gme) ? number_2_format($member->gme) : null;
            })
            ->rawColumns(['status']);
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query = $this->searchResult($this->input);

        return$this->applyScopes($this->query);
    }



    public function searchResult($data){



        //Search date range
        $this->query = DB::table('payroll_beneficiaries_view as b')->select(
            'b.filename',
            'b.member_name',
            'b.employee_name',
            'b.dob',
            'b.monthly_pension_amount',
            'banks.name as bank_name',
            'bank_branches.name as bank_branch_name',
            'b.accountno',
            'b.lastresponse',
            'b.relationship',
            'b.member_type_id',
            'b.resource_id',
            'b.employee_id',
            'b.pd',
            'b.gender',
            'er.name as region_name',
            'dis.name as district_name',
            'b.hold_reason',
            'b.suspension_reason',
            'b.payroll_start_date',
            'pt.last_payroll_date',
            'pt.total_pension_paid',
            'b.gme',
            'b.survivor_pension_percent'
        )
            ->leftJoin('banks', 'banks.id', 'b.bank_id')
            ->leftJoin('bank_branches', 'bank_branches.id', 'b.bank_branch_id')
            ->leftJoin('regions as er', 'er.id', 'b.member_region_id')
            ->leftJoin('districts as dis', 'dis.id', 'b.member_district_id')
            ->leftJoin('payroll_all_runs_member_totals_view as pt', function($join){
                $join->on('pt.resource_id', 'b.resource_id')->whereRaw(" pt.member_type_id = b.member_type_id")->whereRaw(" pt.employee_id = b.employee_id");
            })->whereRaw(" (b.monthly_pension_amount > 0 or b.pd > 0) ")->where('b.isactive', '<>', 0);


        if (isset($data['member_type_id'])) {
            if (($data['member_type_id'] > 0) ) {
                $this->query = $this->queryWithMemberType($data);
            }
        }

        if (isset($data['status_id'])) {

            if (($data['status_id'] > 0) ) {
                $this->query = $this->queryWithStatus($data);
            }
        }else{
            $this->query =  $this->query->where('b.isactive', '<>', 0);
        }
        if (isset($data['region_id'])) {
            if (($data['region_id'] > 0) ) {
                $this->query = $this->queryWithRegion($data);
            }
        }

        if (isset($data['category'])) {
            if (($data['category'] > 0) ) {
                $this->query = $this->queryWithCategory($data);
            }
        }
        return $this->query;

    }


    /*
     * query date range with member type selected
     */
    public function queryWithMemberType($data)
    {
        if($data['member_type_id']  ==  1){
            /*Search for contact care assistant*/
            return  $this->query->where('b.member_type_id', 4)->where('b.dependent_type_id', 7);

        }elseif($data['member_type_id']  ==  4){
            /* dependents*/
            return  $this->query->where('b.member_type_id', $data['member_type_id'])->where('b.dependent_type_id','<>', 7);
        }else{
            /*pensioner */
            return  $this->query->where('b.member_type_id', $data['member_type_id']);
        }

    }


    /*Query with status*/
    public function queryWithStatus($data)
    {
        $status_id = $data['status_id'] ;
        switch($status_id){
            case 1:
                //Active
                return  $this->query->where('b.isactive', 1);
                break;
            case 2:
                //suspended
                return  $this->query->where('b.isactive', '=', 1)->where('b.suspense_flag', '=', 1);
                break;

            case 3:
                //Deactivated

                return  $this->query->where('b.isactive', '=', 2);
                break;

            case 4:
                //pending
                return  $this->query->where('b.isactive', 0);
                break;

            case 5:
                //hold
                return  $this->query->where('b.isactive', '=', 3);
                break;

        }

    }

    /*Query with region*/
    public function queryWithRegion($data)
    {
        $region_id = $data['region_id'];
        return $this->query->where('b.employer_region_id', $region_id);
    }



    /*Query with category*/
    public function queryWithCategory($data)
    {
        $category = $data['category'];
        switch($category){
            case 1://System filed
                return  $this->query->where('b.firstpay_manual', '=', 0)->whereNotNull('b.notification_report_id');
                break;

            case 2: //System but ENrolled from Manual
                return  $this->query->where('b.firstpay_manual', '=', 1)->whereNotNull('b.notification_report_id');
                break;

            case 3://Manual case file
                return  $this->query->where('b.firstpay_manual', '=', 1)->whereNotNull('b.manual_notification_report_id');
                break;
        }
    }



    public function download($data){

        $members = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($members as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                         document.location.href = '". url("/") . "/payroll/beneficiary_profile/' + aData['member_type_id'] + '/' + aData['resource_id'] + '/' + aData['employee_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'filename', 'name' => 'b.filename', 'title' => 'Filename', 'orderable' => true, 'searchable' => true],
            ['data' => 'member_name', 'name' => 'b.member_name', 'title' => 'Beneficiary', 'orderable' => true, 'searchable' => true],
            ['data' => 'employee_name', 'name' => 'b.employee_name', 'title' => 'Employee', 'orderable' => true, 'searchable' => true],
            ['data' => 'dob', 'name' => 'b.dob', 'title' => 'DOB', 'orderable' => true, 'searchable' => true],
            ['data' => 'monthly_pension_amount', 'name' => 'b.monthly_pension_amount', 'title' => 'Monthly pension', 'orderable' => true, 'searchable' => true],
            ['data' => 'bank_name', 'name' => 'banks.name', 'title' => 'Bank', 'orderable' => false, 'searchable' => false],
            ['data' => 'bank_branch_name', 'name' => 'bank_branches.name', 'title' => 'Bank branch', 'orderable' => false, 'searchable' => false],
            ['data' => 'accountno', 'name' => 'b.accountno', 'title' => 'Accountno', 'orderable' => true, 'searchable' => true],
            ['data' => 'lastresponse', 'name' => 'b.lastresponse', 'title' => 'Last verification', 'orderable' => true, 'searchable' => true],
            ['data' => 'relationship', 'name' => 'b.relationship', 'title' => 'Relationship', 'orderable' => true, 'searchable' => true],
            ['data' => 'pd', 'name' => 'b.pd', 'title' => 'PD', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'gender', 'name' => 'b.gender', 'title' => 'Gender', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'district_name', 'name' => 'dis.name', 'title' => 'District Region', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'region_name', 'name' => 'er.name', 'title' => 'Member Region', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'hold_reason', 'name' => 'b.hold_reason', 'title' => 'Hold Reason', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'suspension_reason', 'name' => 'b.suspension_reason', 'title' => 'Suspension Reason', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'payroll_start_date', 'name' => 'pt.payroll_start_date', 'title' => 'Date of MMI/Death', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'last_payroll_date', 'name' => 'pt.last_payroll_date', 'title' => 'Last payroll date', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'total_pension_paid', 'name' => 'pt.total_pension_paid', 'title' => 'Total Pension Paid', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'gme', 'name' => 'b.gme', 'title' => 'GME', 'orderable' => true, 'searchable' => false,'visible'=> false],
            ['data' => 'survivor_pension_percent', 'name' => 'b.survivor_pension_percent', 'title' => 'Survivor Pension Percent', 'orderable' => true, 'searchable' => false,'visible'=> false],
        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'payrollbeneficiaries' . time();
    }
}


