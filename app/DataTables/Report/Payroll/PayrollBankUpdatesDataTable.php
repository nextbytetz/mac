<?php
namespace App\DataTables\Report\Payroll;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollBankUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollChildExtensionRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollVerificationRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class PayrollBankUpdatesDataTable extends DataTable
{
    protected $query;


    /*Results from query() method*/
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('wf_done_date', function($query) {
                return short_date_format($query->wf_done_date);
            })

            ->rawColumns(['']);
    }

    /*Get query source of DataTable*/
    public function query()
    {
        $input = $this->input;
        $this->query = $this->searchResult($input);
        return $this->query;
    }

    /*Search Results*/
    public function searchResult($data)
    {
        $bank_update_repo = (new PayrollBankUpdateRepository());
        $this->query = $bank_update_repo->query()->select(
            'b.filename',
            'b.member_name',
            'b.employee_name',
            'ob.name as old_bank',
            'obr.name as old_bank_branch',
            'payroll_bank_info_updates.old_accountno',
            'nb.name as new_bank',
            'nbr.name as new_bank_branch',
            'payroll_bank_info_updates.new_accountno',
            'payroll_bank_info_updates.wf_done_date'
        )
            ->join('payroll_beneficiaries_view as b', function ($join){
                $join->on('b.resource_id', 'payroll_bank_info_updates.resource_id')->whereRaw("b.member_type_id = payroll_bank_info_updates.member_type_id")->whereRaw("b.employee_id = payroll_bank_info_updates.employee_id");
            })->leftJoin('banks as ob', 'ob.id', 'payroll_bank_info_updates.old_bank_id' )
            ->leftJoin('banks as nb', 'nb.id', 'payroll_bank_info_updates.new_bank_id' )
            ->leftJoin('bank_branches as obr', 'obr.id', 'payroll_bank_info_updates.old_bank_branch_id' )
            ->leftJoin('bank_branches as nbr', 'nbr.id', 'payroll_bank_info_updates.new_bank_branch_id' )
            ->whereBetween('payroll_bank_info_updates.wf_done_date', [$data['from_date'], $data['to_date']]);

        /*query with type*/
        if(isset($data['type'])){
            $this->query = $this->queryWithType($data);
        }

        return $this->query;
    }

    /*Search query with type*/
    public function queryWithType($data)
    {
        $type_id = $data['type'];
        return $this->query->where('payroll_child_extensions.child_continuation_reason_cv_id', $type_id);
    }


    /*Total*/
    public function total($data)
    {
        $total = $this->searchResult($data)->sum('total');
        return $total;
    }

    /*Export / Download*/
    public function download($data){

        $members = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($members as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');
    }


    /*Optional method if you want to use html builder.*/
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                /*
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                document.location.href = '". url("/") . "/your_url/' + aData['application_uuid'];
                }).hover(function() {
                $(this).css('cursor','pointer');
                }, function() {
                $(this).css('cursor','auto');
                });
                }"
                */
            ]);
    }

    /*Get Columns*/
    protected function getColumns()
    {
        return [
            ['data' => 'filename', 'name' => 'b.filename', 'title' => 'filename', 'orderable' => false, 'searchable' => false],
            ['data' => 'member_name', 'name' => 'b.member_name', 'title' => 'Member Name', 'orderable' => false, 'searchable' => false],
            ['data' => 'employee_name', 'name' => 'b.employee_name', 'title' => 'Employee Name', 'orderable' => false, 'searchable' => false],
            ['data' => 'old_bank', 'name' => 'ob.name', 'title' => 'Old Bank', 'orderable' => false, 'searchable' => false],
            ['data' => 'old_bank_branch', 'name' => 'obr.name', 'title' => 'Old Bank Banch', 'orderable' => false, 'searchable' => false],
            ['data' => 'old_accountno', 'name' => 'payroll_bank_info_updates.old_accountno', 'title' => 'Old Accountno', 'orderable' => false, 'searchable' => false],
            ['data' => 'new_bank', 'name' => 'nb.name', 'title' => 'New Bank', 'orderable' => false, 'searchable' => false, 'visible'=> true],
            ['data' => 'new_bank_branch', 'name' => 'nb.new_bank_branch', 'title' => 'New Bank Branch', 'orderable' => false, 'searchable' => false, 'visible'=> true],
            ['data' => 'new_accountno', 'name' => 'payroll_bank_info_updates.new_accountno', 'title' => 'New Accountno', 'orderable' => false, 'searchable' => false, 'visible'=> true],
            ['data' => 'wf_done_date', 'name' => 'payroll_bank_info_updates.wf_done_date', 'title' => 'Approved Date', 'orderable' => true, 'searchable' => true],
        ];
    }

    /*Get filename for export file*/
    public function filename()
    {
        return 'payrollBankUpdates' . date('YmdHis');
    }


}


