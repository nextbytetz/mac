<?php

namespace App\DataTables\Report\Payroll;

use App\Models\Operation\Payroll\PayrollRun;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Finance\PayrollProcRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRunRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;

class PayrollRecoveriesDataTable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        $member_types = new MemberTypeRepository();
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('payee', function($payroll_run)  {
                return $payroll_run->member_name;
            })
            ->editColumn('amount', function($payroll_run) {
                return number_2_format($payroll_run->amount);
            })
            ->editColumn('monthly_pension', function($payroll_run) {
                return number_2_format($payroll_run->monthly_pension);
            })
            ->editColumn('arrears_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->arrears_amount);
            })
            ->editColumn('deductions_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->deductions_amount);
            })
            ->editColumn('unclaimed_amount', function ($payroll_suspended_run) {
                return   number_2_format($payroll_suspended_run->unclaimed_amount);
            })
            ->editColumn('bank_id', function($payroll_run) {
                return $payroll_run->bank_name;
            })
            ->editColumn('bank_branch_id', function($payroll_run) {
                return $payroll_run->bank_branch_name;
            })
            ->editColumn('member_type_id', function($payroll_run) {
                return $payroll_run->member_type_name;
            })
            ->editColumn('dob', function($payroll_run) {
                return short_date_format($payroll_run->dob);
            })
            ->editColumn('payroll_month', function($payroll_run) {
                return short_date_format($payroll_run->payroll_month);
            });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $data = $this->input;
        return  $this->searchResult($data);
    }



    public function searchResult($data){


        $from_date = isset($data['from_date']) ? standard_date_format($data['from_date']) : standard_date_format(getTodayDate());
        $to_date = isset($data['to_date']) ? standard_date_format($data['to_date']) : standard_date_format(getTodayDate());
        $this->query = PayrollRun::query()->select([
            DB::raw("b.member_name"),
            DB::raw("member_types.name as member_type_name"),
            DB::raw("payroll_procs.run_date as payroll_month"),
            DB::raw("payroll_runs.arrears_amount as arrears_amount"),
            DB::raw("payroll_runs.unclaimed_amount as unclaimed_amount"),
            DB::raw("payroll_runs.deductions_amount as deductions_amount"),
            DB::raw("payroll_runs.monthly_pension"),
            DB::raw("payroll_runs.months_paid"),
            DB::raw("payroll_runs.amount as amount"),
            DB::raw("bank_branches.name as bank_branch_name"),
            DB::raw("banks.name as bank_name"),
            DB::raw("payroll_runs.accountno as run_accountno"),
            DB::raw("b.gender as gender"),
            DB::raw("b.dob as dob"),
            DB::raw("age(payroll_procs.run_date::date, b.dob::date) as age"),
        ])
            ->join('payroll_beneficiaries_view as b', function($join){
                $join->on('payroll_runs.resource_id', 'b.resource_id')
                    ->on('payroll_runs.member_type_id', 'b.member_type_id');
            })
            ->join("payroll_run_approvals","payroll_runs.payroll_run_approval_id", "payroll_run_approvals.id")
            ->join("payroll_procs","payroll_procs.id", "payroll_run_approvals.payroll_proc_id")
            ->join("member_types","payroll_runs.member_type_id", "member_types.id")
            ->leftjoin('bank_branches', 'payroll_runs.bank_branch_id', 'bank_branches.id')
            ->leftjoin('banks', 'banks.id', 'payroll_runs.bank_id')
            ->whereRaw("payroll_procs.run_date >= ? and payroll_procs.run_date <= ?", [$from_date, $to_date]);


//        with bank
//        if (isset($this->input['bank_id'])) {
//            if ($this->input['bank_id'] <> null) {
//                $this->query = $this->queryWithBankUsed();
//            }
//        }

        //with member type
        if (isset($data['member_type'])) {
            if ($data['member_type'] <> 0){
                $this->query = $this->queryWithMemberType($data);
            }
        }

        if (isset($data['dependent_id'])) {
            if ($data['dependent_id'] <> 0){
                $this->query = $this->queryWithDependent($data);
            }
        }

        if (isset($data['pensioner_id'])) {
            if ($data['pensioner_id'] <> 0){
                $this->query = $this->queryWithPensioner($data);
            }
        }


        return $this->applyScopes($this->query);

    }



    /*
* query date range with Payroll batch selected
*/
    public function queryWithPayrollProc()
    {
        $payroll_procs = new PayrollProcRepository();
        $payroll_proc = $payroll_procs->find($this->input['payroll_proc_id']);
        $payroll_run_approval_id = ($payroll_proc) ?  $payroll_proc->payrollRunApproval->id : null;
        return  $this->query->where('payroll_run_approval_id',$payroll_run_approval_id);
    }

    /*
* query with Dependent
*/
    public function queryWithDependent($data)
    {

        return  $this->query->where('b.resource_id',$data['dependent_id'])->where('b.member_type_id',4);
    }


    /*
* query with Pensioner
*/
    public function queryWithPensioner($data)
    {

        return  $this->query->where('b.resource_id',$data['pensioner_id'])->where('b.member_type_id',5);
    }

    /*
* query date range with with bank selected
*/
    public function queryWithBank()
    {

        return  $this->query->where('payroll_runs.bank_id',  $this->input['bank_id']);
    }

    /*Banks for payroll i.e. NMB , CRDB, Others*/
    public function queryWithBankUsed()
    {
        $bank_used_id = $this->input['bank_id'];
        if($bank_used_id <> 0){
            /*NMB ; CRDB*/
            return  $this->query->where('payroll_runs.bank_id', $bank_used_id);
        }else{
            /*Others*/
            return  $this->query->where(function($query){
                $query->whereNotIn('payroll_runs.bank_id', [3,4])->orWhereNull('payroll_runs.bank_id');
            });
        }
    }



    /*Search using member type*/
    public function queryWithMemberType($data)
    {
        $member_type = $data['member_type'];
        $is_constantcare = 0;
        if($member_type == 1){
            /*dependents*/
            $member_type_id = 4;
        }elseif($member_type == 2){
            /*pensioners*/
            $member_type_id = 5;
        }else{
            /*constant care*/
            $member_type_id = 4;
            $is_constantcare = 1;
        }

        return  $this->query->where('payroll_runs.isconstantcare', $is_constantcare)->where('payroll_runs.member_type_id', $member_type_id);
    }

    /**
     * @return mixed
     * Find Total Amount to be paid
     */

    public function findTotalAmount()
    {
        $total_amount = $this->query()->sum('amount');
        return number_format($total_amount, 2, '.', ',');

    }


    public function download($data){

        $members = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($members as $result) {
            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
//            ['data' => 'payee', 'name' => 'payee', 'title' => trans('labels.general.payee'), 'orderable' => false, 'searchable' => false],
//            ['data' => 'member_type_id', 'name' => 'member_type_id', 'title' => trans('labels.backend.table.member_type'), 'orderable' => true, 'searchable' => true],
//            ['data' => 'payroll_month', 'name' => 'payroll_procs.run_date', 'title' => 'Payroll Month', 'orderable' => true, 'searchable' => true],
//            ['data' => 'arrears_amount', 'name' => 'arrears_amount', 'title' =>'Arrears', 'orderable' => true, 'searchable' => true],
//            ['data' => 'unclaimed_amount', 'name' => 'unclaimed_amount', 'title' => 'Unclaimed', 'orderable' => true, 'searchable' => true],
//            ['data' => 'deductions_amount', 'name' => 'deductions_amount', 'title' => 'Deduction', 'orderable' => true, 'searchable' => true],
//            ['data' => 'monthly_pension', 'name' => 'monthly_pension', 'title' => trans('labels.backend.claim.monthly_pension'), 'orderable' => true, 'searchable' => true],
//            ['data' => 'months_paid', 'name' => 'months_paid', 'title' => trans('labels.general.months_paid'), 'orderable' => true, 'searchable' => true],
//            ['data' => 'amount', 'name' => 'amount', 'title' => 'Net Payable Amount', 'orderable' => true, 'searchable' => true],
//            ['data' => 'bank_id', 'name' => 'bank_id', 'title' => trans('labels.backend.table.bank'), 'orderable' => false, 'searchable' => false],
//            ['data' => 'bank_branch_id', 'name' => 'bank_branch_id', 'title' => trans('labels.backend.table.branch'), 'orderable' => false, 'searchable' => false],
//            ['data' => 'run_accountno', 'name' => 'run_accountno', 'title' => trans('labels.backend.table.accountno'), 'orderable' => false, 'searchable' => false],
//            ['data' => 'gender', 'name' => 'gender', 'title' => 'Gender', 'orderable' => false, 'searchable' => false, 'visible' => false],
//            ['data' => 'dob', 'name' => 'dob', 'title' => 'DOB', 'orderable' => false, 'searchable' => false, 'visible' => false],
//            ['data' => 'age', 'name' => 'age', 'title' => 'Age', 'orderable' => false, 'searchable' => false, 'visible' => false],
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pensionerStatement' . time();
    }
}
