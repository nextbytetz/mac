<?php

namespace App\DataTables\Report\Finance;

use App\Repositories\Backend\Access\PermissionRepository;
use App\Repositories\Backend\Access\UserRepository;
use Yajra\Datatables\Services\DataTable;

class ReceiptPerformanceDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('name', function($user) {
                return $user->name;
            })
            ->addColumn('no_of_receipts', function($user) {
                return $this->no_of_receipts($user->id);
            })
            ->addColumn('cancelled_receipts', function($user) {
                return $this->no_of_cancelled_receipts($user->id);
            })
            ->addColumn('dishonoured_cheques', function($user) {
                return $this->no_of_dishonoured_receipts($user->id);
            })  ;

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $users = new UserRepository();
        $permissions = New PermissionRepository();
        $query = $users->query()->has('receipts');
        return $this->applyScopes($query);
    }


    /**
     * @return mixed
     * get no of receipts receipted by the user
     */
    public function no_of_receipts($user_id)
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $users = new UserRepository();
        $user = $users->findOrThrowException($user_id);
        $no_of_receipts = 0;

        if (!is_null($from_date) && !is_null($from_date) ){
            $no_of_receipts = $user->receiptWithTrashed()->where('created_at', '>=', $from_date . ' 00:00:00')->where('created_at', '<=', $to_date . ' 23:59:59')->count();

        }
        return $no_of_receipts;
    }
    /**
     * @return mixed
     * get no of receipts cancelled by the user
     */
    public function no_of_cancelled_receipts($user_id)
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $users = new UserRepository();
        $user = $users->findOrThrowException($user_id);
        $no_of_receipts = 0;
        if (!is_null($from_date) && !is_null($to_date) ) {
            $no_of_receipts = $user->receiptOnlyTrashed()->where('cancel_date', '>=', $from_date . ' 00:00:00')->where('cancel_date', '<=', $to_date . ' 23:59:00')->count();
        }
        return $no_of_receipts;
    }

    /**
     * @param $user_id
     * @return int
     * NO. OF Dishonoured cheques
     */
    public function no_of_dishonoured_receipts($user_id)
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $users = new UserRepository();
        $user = $users->findOrThrowException($user_id);
        $no_of_receipts = 0;
        if (!is_null($from_date) && !is_null($to_date) ) {
            $no_of_receipts = $user->dishonouredCheques()->where('created_at', '>=', $from_date . ' 00:00:00')->where('created_at', '<=', $to_date . ' 23:59:00')->count();
        }
        return $no_of_receipts;
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'username', 'name' => 'username','title' => trans('labels.backend.user.username')
            ],
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.fullname'),'orderable' => false, 'searchable' => false],
            ['data' => 'no_of_receipts', 'name' => 'no_of_receipts', 'title' => trans('labels.backend.table.no_of_receipts'), 'orderable' => false, 'searchable' => false],
            ['data' => 'cancelled_receipts', 'name' => 'cancelled_receipts', 'title' => trans('labels.backend.table.cancelled_receipts'), 'orderable' => false, 'searchable' => false],
            ['data' => 'dishonoured_cheques', 'name' => 'dishonoured_cheques', 'title' => trans('labels.backend.table.dishonoured_cheques'), 'orderable' => false, 'searchable' => false],


            ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'receiptperformance' . time();
    }
}
