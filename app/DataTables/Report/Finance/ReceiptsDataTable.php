<?php

namespace App\DataTables\Report\Finance;

use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Finance\PaymentTypeRepository;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReceiptsDataTable extends DataTable
{

    protected $query;
    protected $return_datatable;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
                return $this->returnDatatable();

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $receipt = new ReceiptRepository();
        //Search date range
        $this->query = $receipt->query()
            ->select([
                DB::raw("receipts.*"),
                DB::raw("wf_tracks.forward_date"),
                DB::raw("wf_tracks.status"),
            ])
            //->rightJoin("wf_tracks", "wf_tracks.resource_id", "=", "receipts.id")
            ->leftJoin("wf_tracks", function ($join) {
                $join->on("wf_tracks.resource_id", "=", "receipts.id")
                ->where(['wf_definition_id' => 12]);
            })
            ->whereBetween('receipts.created_at', [$this->from_date. ' 00:00:00',$this->to_date . ' 23:59:59'])
            //->where(['wf_definition_id' => 12, 'status' => 1])
            ->where('isdishonoured', 0);

        if (isset($this->input['bank_id'])) {
            if ($this->input['bank_id'] <> 0) {
                $this->query = $this->queryWithBank();
            }
        }
        if (isset($this->input['payment_type_id'])) {
            if ($this->input['payment_type_id'] <> 0) {
                $this->query = $this->queryPaymentType();
            }
        }

        if (isset($this->input['region_id'])) {
            if ($this->input['region_id'] <> 0) {
                $this->query = $this->queryWithRegion();
            }
        }

        if (isset($this->input['fin_code_id'])) {
            if ($this->input['fin_code_id'] <> 0) {
                $this->query = $this->queryWithFinCode();
            }
        }

        if (isset($this->input['status_id'])) {
            if ($this->input['status_id'] >= 0) {
                $this->query = $this->queryWithApprovalStatus();
            }
        }

        return $this->applyScopes($this->query);
    }
    /*
     * query date range with bank selected
     */
    public function queryWithBank()
    {
        return  $this->query->where('bank_id', $this->input['bank_id']);
    }

    /*
     * query date range with payment type selected
     */
    public function queryPaymentType()
    {
        return  $this->query->where('payment_type_id', $this->input['payment_type_id']);

    }

    public function queryWithApprovalStatus()
    {
        return $this->query->whereRaw("(case when employer_id is not null then 1 else wf_tracks.status end) = ?", [$this->input['status_id']]);
    }

    /**
     * @return mixed
     * Query with region
     */
    public function queryWithRegion()
    {
        return  $this->query->whereHas('employer',function ($query){
            $query->where('region_id',$this->input['region_id']);
        });

    }

    /**
     * @return mixed
     * Query by finance codes
     */
    public function queryWithFinCode()
    {
        if ($this->input['fin_code_id'] == 1 ){
            /* When searching for Interest */
            return  $this->query->whereHas('receiptCodes',function ($query){
                $query->where('receipt_codes.fin_code_id',$this->input['fin_code_id']);
            })->whereDoesntHave('receiptCodes',function ($query){
                $query->where('receipt_codes.fin_code_id',2);
            });

        }elseif ( $this->input['fin_code_id'] == 2){
            /* when searching Monthly contribution */
            return  $this->query->whereHas('receiptCodes',function ($query){
                $query->where('receipt_codes.fin_code_id',$this->input['fin_code_id']);
            });
        }

        else {
            /* When searching for Other Payments */
            return  $this->query->where('receipts.fin_code_id', $this->input['fin_code_id']);
        }


    }



    /**
     * @return string
     * Get Total amount paid
     */

    public function getTotalAmountPaid()
    {
        $fin_code_id = (isset($this->input['fin_code_id'])) ? $this->input['fin_code_id'] : 0;
        switch ($fin_code_id) {
            case 1:
                /* Interest */
                $amount_paid = $this->query()->join('receipt_codes', function ($join) {
                    $join->on('receipts.id', '=', 'receipt_codes.receipt_id')
                        ->where('receipt_codes.fin_code_id', '=', $this->input['fin_code_id']);
                })->sum('receipt_codes.amount');
                return  number_format($amount_paid , 2 , '.' , ',' );
                break;
            default:
                /* other than interest payments */
                $amount_paid = $this->query()->sum('amount');
                return  number_format($amount_paid , 2 , '.' , ',' );
                break;

        }

    }


    public function returnDatatable()
    {
        $receipts = new ReceiptRepository();
        $receipt_codes = new ReceiptCodeRepository();
        $fin_code_id = (isset($this->input['fin_code_id'])) ? $this->input['fin_code_id'] : 0;
        switch ($fin_code_id) {
            case 1:
                /* Interest */
              return  $this->datatables
                    ->eloquent($this->query())
                    ->addColumn('payment_type', function($receipt) use ($receipts, $receipt_codes)  {
                        return $receipts->getPaymentTypeWithChequeno($receipt->id);
                    })
                    ->addColumn("forward_date_formatted", function ($receipt) {
                        return (is_null($receipt->forward_date)) ? "" : Carbon::parse($receipt->forward_date)->format("d-M-Y");
                    })
                      ->addColumn('employer_regno', function ($receipt) {
                          $regno = ($receipt->employer()->count()) ? $receipt->employer->reg_no : "Empty";
                          return $regno;
                      })
                    ->editColumn('bank_id', function($receipt) {
                        return $receipt->bank->name;
                    })
                    ->editColumn('amount', function($receipt) use ($receipt_codes) {
                        return number_format( $receipt_codes->getTotalInterestPaidForReceiptId($receipt->id) , 2 , '.' , ',' );
                    })
                    ->editColumn('rct_date', function($receipt) {
                        return $receipt->rct_date_formatted;
                    })
                    ->editColumn('created_at', function ($receipt) {
                        return $receipt->created_at_formatted;
                    })
                      ->addColumn('region', function ($receipt) {
                          return $receipt->employer->region->name;
                      });
                break;
            default:
             return   $this->datatables
                    ->eloquent($this->query())
                    ->addColumn('payment_type', function($receipt) use ($receipts)  {
                        return $receipts->getPaymentTypeWithChequeno($receipt->id);
                    })
                     ->addColumn("forward_date_formatted", function ($receipt) {
                         return (is_null($receipt->forward_date)) ? "" : Carbon::parse($receipt->forward_date)->format("d-M-Y");
                     })
                     ->addColumn('employer_regno', function ($receipt) {
                         $regno = ($receipt->employer()->count()) ? $receipt->employer->reg_no : "";
                         return $regno;
                     })
                    ->editColumn('bank_id', function($receipt) {
                        return $receipt->bank->name;
                    })
                    ->editColumn('amount', function($receipt) {
                        return $receipt->amount_comma;
                    })
                    ->editColumn('rct_date', function($receipt) {
                        return $receipt->rct_date_formatted;
                    })
                    ->editColumn('created_at', function ($receipt) {
                        return $receipt->created_at_formatted;
                    })
                 ->addColumn('region', function ($receipt) {
                     $region = (($receipt->employer()->count()) And ($receipt->employer->region()->count())) ? $receipt->employer->region->name : "";
                     return $region;
                 });


                break;

        }
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'footerCallback'=> "function ( row, data, start, end, display ) {
        var api = this.api();
        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {

            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                i : 0;

            };
               // Total over this page
            pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                
                 
    function commaSeparateNumber(val){
        while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                    }
                    return val ;
                }
        // Update footer
          $( api.column(2).footer() ).html( 'PAGE TOTAL'  );
        $( api.column(3).footer() ).html( ''+ commaSeparateNumber(pageTotal.toFixed(2)));
    }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/finance/receipt/' + aData['id'] + '/edit';
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'rctno', 'name' => 'rctno', 'title' => trans('labels.backend.table.receipt.rctno')],
            ['data' => 'pay_control_no', 'name' => 'pay_control_no', 'title' => 'Control Number','orderable' => false, 'searchable' => true],
            ['data' => 'payer', 'name' => 'payer', 'title' => trans('labels.backend.table.receipt.payer'), 'orderable' => false, 'searchable' => true],
            ['data' => 'payment_type', 'name' => 'payment_type', 'title' => trans('labels.backend.finance.receipt.payment_type'), 'orderable' => true, 'searchable' => false],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.backend.table.amount'),'orderable' => true, 'searchable' => true, 'class'=>'amount'],
            ['data' => 'bank_id', 'name' => 'bank_id', 'title' => trans('labels.backend.table.bank'),'orderable' => true, 'searchable' => false],
            ['data' => 'rct_date', 'name' => 'rct_date', 'title' => trans('labels.backend.finance.receipt.rct_date'), 'orderable' => true],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => trans('labels.backend.finance.receipt.captured_date'), 'orderable' => true, 'searchable' => false],
            ['data' => 'region', 'name' => 'region', 'title' => trans('labels.general.region'), 'orderable' => true, 'searchable' => false],
            ['data' => 'description', 'name' => 'description', 'title' => trans('labels.backend.table.receipt.payfor'), 'orderable' => true, 'searchable' => false, 'visible' => false],
            ['data' => 'forward_date_formatted', 'name' => 'forward_date_formatted', 'title' => "Date Approved", 'orderable' => true, 'searchable' => false, 'visible' => false],
            ['data' => 'employer_regno', 'name' => 'employer_regno', 'title' => "Registration Number", 'orderable' => true, 'searchable' => false, 'visible' => false],
            ['data' => 'chequeno', 'name' => 'chequeno', 'title' => "Cheque No", 'orderable' => true, 'searchable' => false, 'visible' => false],
//            ['data' => 'id', 'name' => 'id', 'visible' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'receipts_' . time();
    }
}
