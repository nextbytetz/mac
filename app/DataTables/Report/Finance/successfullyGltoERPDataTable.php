<?php

namespace App\DataTables\Report\Finance;

use App\User;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Log;

class successfullyGltoERPDataTable extends DataTable
{    
    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
     $query= DB::table('main.receipts_gl')->select('*')->whereDate('payment_date', '>=', $this->from_date)->whereDate('payment_date', '<=', $this->to_date)->where('isposted','=',true);
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
       return $this->builder()
        ->columns($this->getColumns())
        ->parameters([
            'dom' => 'Bfrtip',
            'buttons' => ['csv', 'excel','print', 'reset', 'reload'],
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
             'Rctno' => ['data' => 'rctno', 'name' => 'receipts_gl.rctno'],
             'Receipt_type' => ['data' => 'trx_type', 'name' => 'receipts_gl.trx_type'],
             'payment_date' => ['data' => 'payment_date', 'name' => 'receipts_gl.payment_date'],
             'payee_name' => ['data' => 'payee_name', 'name' => 'receipts_gl.payee_name'],
             'amount' => ['data' => 'amount', 'name' => 'receipts_gl.amount'],
             'control_number' => ['data' => 'control_number', 'name' => 'receipts_gl.control_number'] 
              ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'successfullyGltoERP_' . time();
    }
}
