<?php

namespace App\DataTables\Report\Finance;

use App\Models\Finance\FinCode;
use App\Models\Finance\Receipt\Receipt;
use App\Repositories\Backend\Finance\FinCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use Yajra\Datatables\Services\DataTable;

class ReceiptByFinanceCodeDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('amount', function($fin_code) {
                return ($fin_code->receipts()->count()) ? $this->getTotalAmountOtherPayments($fin_code->id) : $this->getTotalAmountReceivable($fin_code->id);
            })
            ->addColumn('finance_group', function($fin_code) {
                return $fin_code->fincodegroup->name;
            });


    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function getTotalAmountOtherPayments($fin_code_id){
        $receipts = new ReceiptRepository();
        $amount = $receipts->query()->where('created_at', '>=', $this->from_date . ' 00:00:00')->where('created_at', '<=', $this->to_date . ' 23:59:00')->where('fin_code_id',$fin_code_id)->where('isdishonoured', 0)->sum('amount');
        return number_format( $amount , 2 , '.' , ',' );
    }

    /**
     * get total amounts for receivable group (contribution and interest) from receiptcode
     */
    public function getTotalAmountReceivable($fin_code_id){
        $receipt_codes = new ReceiptCodeRepository();
        $amount = $receipt_codes->query()->whereHas('receipt',function ($query) {
            $query->where('isdishonoured',0)->where('iscancelled', 0);
        })->where('grouppaystatus',0)->where('created_at', '>=', $this->from_date . ' 00:00:00')->where('created_at', '<=', $this->to_date . ' 23:59:00')->where('fin_code_id',$fin_code_id)->sum('amount');
        return number_format( $amount , 2 , '.' , ',' );
    }


    public function query()
    {
        $fin_codes = new FinCodeRepository();
        $query = $fin_codes->query();
//        $query = $receipt->query()->whereBetween('created_at', [$this->from_date. ' 00:00:00',$this->to_date. ' 23:59:59']);
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.backend.table.receipt.finance_code')],
            ['data' => 'finance_group', 'name' => 'finance_group', 'title' => trans('labels.backend.table.receipt.finance_code_group'),'orderable' => false, 'searchable' => false],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.backend.table.receipt.amount'),'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'receipts_by_finance_code' . time();
    }
}
