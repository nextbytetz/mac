<?php

namespace App\DataTables\Report\Finance;

use App\User;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Log;


class successfullyCNReconciliationDataTable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
         return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
       $query = DB::table("portal.gepgreconcile")->select()->where('TrxDtTm', '>=', $this->from_date . ' 00:00:00')->where('TrxDtTm', '<=', $this->to_date . ' 23:59:00');
       
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
         return $this->builder()
        ->columns($this->getColumns())
        ->parameters([
            'dom' => 'Bfrtip',
            'buttons' => ['csv', 'excel','print', 'reset', 'reload'],

        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'control_no' => ['data' => 'BillCtrNum', 'name' => 'gepgreconcile.BillCtrNum'],
            'payer_name' =>[ 'data' => 'DptName', 'name' => 'gepgreconcile.DptName'],
            'paid_amount' => ['data' => 'paidamt', 'name' => 'gepgreconcile.paidamt'],
            'Transaction_date' => ['data' => 'TrxDtTm', 'name' => 'gepgreconcile.TrxDtTm'],
            'Exchecker_receipt' => ['data' => 'pspTrxId', 'name' => 'gepgreconcile.pspTrxId'],
            'Bank Name' =>[ 'data' => 'PspName', 'name' => 'gepgreconcile.PspName'],
            'Account Number' =>[ 'data' => 'CtrAccNum', 'name' => 'gepgreconcile.CtrAccNum'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'successfullycnreconciliationdatatable_' . time();
    }
}
