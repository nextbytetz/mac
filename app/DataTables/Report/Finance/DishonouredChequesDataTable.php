<?php

namespace App\DataTables\Report\Finance;

use App\Models\Finance\Traits\Relationship\ReceiptRelationship;
use App\Repositories\Backend\Finance\DishonouredChequeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use Yajra\Datatables\Services\DataTable;

class DishonouredChequesDataTable extends DataTable
{
use ReceiptRelationship;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('rct_no', function ($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count()) ? $dishonoured_cheque->receiptWithTrashed->rctno : 0;
            })
            ->addColumn('employer', function($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count())?$dishonoured_cheque->receiptWithTrashed->payer :0;

            })
            ->addColumn('employer_regno', function($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count())?$dishonoured_cheque->receiptWithTrashed->employer->reg_no :0;

            })
            ->addColumn('status', function ($dishonoured_cheque) {
                return $dishonoured_cheque->dishonoured_cheque_status_label;
            })

            ->addColumn('amount_formatted', function ($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count()) ? number_format( $dishonoured_cheque->receiptWithTrashed->amount , 2 , '.' , ',' ) : 0;

            })
            ->addColumn('payment_date', function($dishonoured_cheque) {
                return $dishonoured_cheque->receiptWithTrashed->rct_date_formatted;
            })
            ->addColumn('receipt_date', function($dishonoured_cheque) {
                return $dishonoured_cheque->receiptWithTrashed->created_at_formatted;
            })
            ->addColumn('contrib_month', function($dishonoured_cheque) {
                return $dishonoured_cheque->receiptWithTrashed->receiptCodes()->first()->contrib_month_formatted;
            })
            ->editColumn('old_bank_id', function($dishonoured_cheque) {
                return ($dishonoured_cheque->oldBank()->count()) ? $dishonoured_cheque->oldBank->name : "";
            })
            ->editColumn('old_bank_id', function ($dishonoured_cheque) {
                return ($dishonoured_cheque->oldBank()->count()) ? $dishonoured_cheque->oldBank->name : "";
            })
            ->editColumn('new_bank_id', function ($dishonoured_cheque) {
                return ($dishonoured_cheque->newBank()->count()) ? $dishonoured_cheque->newBank->name : "";
            })
            ->rawColumns(['status']);




    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $dishonoured_cheques= new DishonouredChequeRepository();
        $query = $dishonoured_cheques->getQuery()->whereBetween('created_at', [$this->from_date. ' 00:00:00',$this->to_date. ' 23:59:59']);
        return $this->applyScopes($query);
        }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer', 'name' => 'employer', 'title' => trans('labels.backend.table.receipt.payer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_regno', 'name' => 'employer_regno', 'title' => 'Registration Number', 'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'contrib_month', 'title' => 'Contribution Month', 'orderable' => false, 'searchable' => false],
            ['data' => 'amount_formatted', 'name' => 'amount', 'title' => trans('labels.backend.table.receipt.amount'), 'orderable' => false, 'searchable' => false],
            ['data' => 'old_cheque_no', 'name' => 'old_cheque_no', 'title' => trans('labels.backend.table.receipt.dishonoured_chequeno'), 'orderable' => true, 'searchable' => true, 'visible' => true],
            ['data' => 'receipt_date', 'name' => 'receipt_date', 'title' => 'Receipt Date', 'orderable' => false, 'searchable' => false, 'visible' => true],
            ['data' => 'status', 'name' => 'status', 'title' => trans('labels.backend.table.receipt.status'), 'orderable' => false, 'searchable' => false],
            ['data' => 'new_cheque_no', 'name' => 'new_cheque_no', 'title' => trans('labels.backend.table.receipt.replaced_chequeno'), 'orderable' => true, 'searchable' => true, 'visible' => false],
            ['data' => 'new_bank_id', 'name' => 'new_bank_id', 'title' => trans('labels.backend.table.receipt.new_bank'), 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'rct_no', 'name' => 'rct_no', 'title' => trans('labels.backend.table.receipt.rctno'), 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'payment_date', 'name' => 'payment_date', 'title' => trans('labels.backend.table.payment_date'), 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'old_bank_id', 'name' => 'old_bank_id', 'title' => trans('labels.backend.table.receipt.old_bank'), 'orderable' => false, 'searchable' => false, 'visible' => false],
             ['data' => 'dishonour_reason', 'name' => 'dishonour_reason', 'title' => 'Dishonour reason', 'orderable' => false, 'searchable' => false, 'visible' => true],
//            ['data' => 'id', 'name' => 'id', 'visible' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'dishonouredcheques_' . time();
    }
}
