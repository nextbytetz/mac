<?php

namespace App\DataTables\Report\Finance;

use App\User;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
// use App\Models\Finance\SuccessfulGepgTrx;
use Log;

class GepGReconciliationDataTable extends DataTable
{

     protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query= DB::table("portal.gepgreconcile")->select('*')->whereDate('TrxDtTm', '=', $this->from_date)->whereDate('TrxDtTm', '=', $this->to_date)
            ->whereNOTIn('SpBillId',function($query){
               $query->select('bill_id')->from('portal.payments')->whereDate('payment_date','=', $this->from_date)->whereDate('payment_date','=', $this->to_date);
            });

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->parameters([
            'dom' => 'Bfrtip',
            'buttons' => ['csv', 'excel','print', 'reset', 'reload'],

        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
             'BillCtrNum' => ['data' => 'BillCtrNum', 'name' => 'gepgreconcile.BillCtrNum'],
              'paid_amount' => ['data' => 'paidamt', 'name' => 'gepgreconcile.paidamt'],
             'GePG_Receipt' => ['data' => 'pspTrxId', 'name' => 'gepgreconcile.pspTrxId'],
             'payment_date' => ['data' => 'TrxDtTm', 'name' => 'gepgreconcile.TrxDtTm'],
             'Bank_Account' => ['data' => 'CtrAccNum', 'name' => 'gepgreconcile.CtrAccNum'],
             'Bank_Name' => ['data' => 'PspName', 'name' => 'gepgreconcile.PspName'] 
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'gepgreconciliation_' . time();
    }
}
