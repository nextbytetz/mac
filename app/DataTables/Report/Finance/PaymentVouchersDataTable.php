<?php

namespace App\DataTables\Report\Finance;

use App\Repositories\Backend\Finance\PaymentVoucherRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Operation\Claim\MemberTypeRepository;
use Yajra\Datatables\Services\DataTable;

class PaymentVouchersDataTable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        $member_types = new MemberTypeRepository();
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('amount', function($payment_voucher) {
                return $payment_voucher->amount_formatted;
            })
            ->editColumn('created_at', function($payment_voucher) {
                return $payment_voucher->created_at_formatted;
            })
            ->addColumn('payee', function($payment_voucher) use($member_types){
                return  $member_types->getMember($payment_voucher->paymentVoucherTransactions->first()->member_type_id, $payment_voucher->paymentVoucherTransactions->first()->resource_id)->name;
            })
            ->addColumn('status', function($payment_voucher) {
                return $payment_voucher->paid_status_label;
            })
            ->addColumn('action', function($payment_voucher) {
                return    ($payment_voucher->is_paid == 1 && $payment_voucher->paymentVoucherTransactions->first()->member_type_id <> 3) ? $payment_voucher->print_button : ' ';
            })
            ->rawColumns(['status', 'action']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $payment_vouchers = new PaymentVoucherRepository();
        $this->query = $payment_vouchers->query();


        //         with resource id (By Member type i.e employee, insurance )
        if (isset($this->input['member_type_id'])) {
            if (($this->input['member_type_id'] <> 0)) {
                switch ($this->input['member_type_id'] ) {
                    case 1:
                        $this->query = $this->queryWithMember($this->input['employer_id']);
                        break;
                    case 2:
                        $this->query = $this->queryWithMember($this->input['employee_id']);
                        break;
                    case 3:
                        $this->query = $this->queryWithMember($this->input['insurance_id']);
                        break;
                    case 4:
                        $this->query = $this->queryWithMember($this->input['dependent_id']);
                        break;
                    case 5:
                        $this->query = $this->queryWithMember($this->input['pensioner_id']);
                        break;
                    default:
                        $this->query = $this->queryWithMember(0);
                }



            }
        }

        //        with status
        if (isset($this->input['status_id'])) {
            if (($this->input['status_id'] >= 0) && ($this->input['status_id'] < 3)) {
                $this->query = $this->queryWithStatus();
            }
        }

        return $this->applyScopes($this->query);
    }
    /*
* query date range with with Status selected
*/
    public function queryWithStatus()
    {
        return  $this->query->where('is_paid', $this->input['status_id']);
    }

    /*
* query date range with with employee
*/
    public function queryWithMember($resource_id)
    {
        return  $this->query->whereHas('paymentVoucherTransactions', function ($query) use($resource_id){
            $query->where('member_type_id', $this->input['member_type_id'])->where('resource_id',  $resource_id );
        } );
    }


    /**
     * @return mixed
     * Find Total Amount Paid
     */

    public function findTotalAmount()
    {
        $total_amount = $this->query()->sum('amount');
        return number_format($total_amount, 2, '.', ',');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload' ],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'exportOptions'=> [       'columns'=> 'visible'
                ]

            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'payee', 'name' => 'payee', 'title' => trans('labels.general.payee'), 'orderable' => true, 'searchable' => false],

            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.general.amount'), 'orderable' => false, 'searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => trans('labels.backend.table.pv_date'),'orderable' => true, 'searchable' => false],
            ['data' => 'status', 'name' => 'status', 'title' => trans('labels.general.status'),'orderable' => true, 'searchable' => false],
            ['data' => 'action', 'name' => 'action', 'title' => trans('labels.general.actions'),'orderable' => true, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'paymentvouchers_' . time();
    }
}
