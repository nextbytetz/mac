<?php

namespace App\DataTables\Report\Finance;

use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use Yajra\Datatables\Services\DataTable;

class CancelledReceiptDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())

            ->editColumn('amount', function($receipt) {
                return $receipt->amount_comma;
            })
            ->addColumn('employer', function($receipt) {
                return (!is_null($receipt->employer)) ? $receipt->employer->name : '';
            })
            ->addColumn('employer_regno', function($receipt) {
                return (!is_null($receipt->employer)) ? $receipt->employer->reg_no : '';
            })
            ->addColumn('contrib_month', function($receipt) {
                $month = ($receipt->receiptCodes()->count()) ? $receipt->receiptCodes()->first()->contrib_month_formatted : "";
                return $month;
            })
            ->editColumn('created_at', function ($receipt) {
                return $receipt->created_at_formatted;
            })
            ->editColumn('rct_date', function ($receipt) {
                return $receipt->rct_date_formatted;
            })
            ->editColumn('cancel_date', function ($receipt) {
                return $receipt->cancel_date_formatted;
            })
        ->editColumn('cancel_user_id', function ($receipt) {
        return $receipt->cancelUser->username;
    });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $receipt = new ReceiptRepository();
        $query = $receipt->query()->onlyTrashed()->where('iscancelled',1)
            ->where('cancel_date', '>=', $this->from_date . ' 00:00:00')->where('cancel_date', '<=', $this->to_date . ' 23:59:00');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis' ],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'exportOptions'=> [       'columns'=> 'visible'
                ]

            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'payer', 'name' => 'payer', 'title' => trans('labels.backend.table.receipt.payer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_regno', 'name' => 'employer_regno', 'title' => 'Registration Number', 'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'contrib_month', 'title' => 'Contribution Month', 'orderable' => false, 'searchable' => false],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.backend.table.amount')],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => trans('labels.backend.finance.receipt.captured_date'), 'orderable' => false, 'searchable' => false],
            ['data' => 'cancel_date', 'name' => 'cancel_date', 'title' => trans('labels.backend.table.receipt.cancelled_date'), 'orderable' => false, 'searchable' => false],
            ['data' => 'cancel_reason', 'name' => 'cancel_reason', 'title' => 'Reason', 'orderable' => false, 'searchable' => false],
            ['data' => 'rct_date', 'name' => 'rct_date', 'title' => trans('labels.backend.table.receipt.rct_date'), 'orderable' => false, 'visible' => false],
            ['data' => 'cancel_user_id', 'name' => 'cancel_user_id', 'title' => trans('labels.backend.finance.receipt.cancelled_by'), 'orderable' => true, 'searchable' => false, 'visible' => false],
            ['data' => 'rctno', 'name' => 'rctno', 'title' => trans('labels.backend.table.receipt.rctno'),'orderable' => true, 'searchable' => true, 'visible' => false],
            ['data' => 'description', 'name' => 'description', 'title' => trans('labels.backend.table.receipt.payfor'), 'orderable' => false, 'searchable' => false, 'visible' => false],
//            ['data' => 'id', 'name' => 'id', 'visible' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'cancelledreceipts_' . time();
    }
}
