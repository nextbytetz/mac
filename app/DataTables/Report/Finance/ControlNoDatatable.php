<?php

namespace App\DataTables\Report\Finance;

use App\User;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
// use App\Models\Finance\SuccessfulGepgTrx;
use Log;

class ControlNoDatatable extends DataTable
{
    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        // return $this->datatables
        // ->eloquent($this->query());
        // ->make(true);

         return $this->datatables->of($this->query());
    }

//    public function ajax()
// {
//     return $this->datatables
//         ->eloquent($this->query())
//         ->make(true);
// }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        //Use of repositories or Model


   $query = DB::table("successful_transactions_gepg")->select()->where('payment_date', '>=', $this->from_date . ' 00:00:00')->where('payment_date', '<=', $this->to_date . ' 23:59:00');
       
        return $query;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->parameters([
            'dom' => 'Bfrtip',
            'buttons' => ['csv', 'excel','print', 'reset', 'reload'],

        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'control_no' => ['data' => 'control_no', 'name' => 'successful_transactions_gepg.control_no'],
            'rctno' =>[ 'data' => 'rctno', 'name' => 'successful_transactions_gepg.rctno'],
            'payer_name' =>[ 'data' => 'payer_name', 'name' => 'successful_transactions_gepg.payer_name'],
            'bill_amount' => ['data' => 'bill_amount', 'name' => 'successful_transactions_gepg.bill_amount'],
            'paid_amount' => ['data' => 'paid_amount', 'name' => 'successful_transactions_gepg.paid_amount'],
            'payment_date' => ['data' => 'payment_date', 'name' => 'successful_transactions_gepg.payment_date'],
            'account_number' => ['data' => 'account_number', 'name' => 'successful_transactions_gepg.account_number'],
            'bank_name' => ['data' => 'bank_name', 'name' => 'successful_transactions_gepg.bank_name'],
            'generatedby' => ['data' => 'generatedby', 'name' => 'successful_transactions_gepg.generatedby'],
            'contact' => ['data' => 'contact', 'name' => 'successful_transactions_gepg.contact'],
            /*'control_no' => ['data' => 'control_no', 'name' => 'successful_transactions_gepg.control_no','orderable'=> true, 'searchable' => true],
            'rctno' => ['data' => 'rctno', 'name' => 'successful_transactions_gepg.rctno','orderable'=> true, 'searchable' => true],*/
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'successfulcontrolno_' . time();
    }
}
