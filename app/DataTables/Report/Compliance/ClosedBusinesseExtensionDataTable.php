<?php
namespace App\DataTables\Report\Compliance;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureExtensionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class ClosedBusinesseExtensionDataTable extends DataTable
{
    protected $query;


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())

            ->addColumn('doc_formatted', function($employer) {
                return short_date_format($employer->doc);
            })
            ->editColumn('application_date', function($employer) {
                return short_date_format($employer->application_date);
            })

            ->editColumn('approved_date', function($booking) {
                return short_date_format($booking->approved_date);
            })
            ->rawColumns(['status']);
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query = $this->searchResult($this->input);

        return $this->query;
    }



    public function searchResult($data){

        $employers = new EmployerClosureExtensionRepository();

        //Search date range
        $this->query = $employers->query()->select(
            'employer_closure_extensions.id as id',
            'employers.id as employer_id',
            'employers.name as employer_name',
            'employers.reg_no',
            'employers.tin',
            'employers.vote',
            'employers.doc',
            'regions.name as region_name',
            'employer_closure_extensions.application_date',
           'employer_closure_extensions.wf_done_date as approved_date',
            'employer_closure_extensions.remark'
                   )
            ->join('employer_closures', 'employer_closures.id', 'employer_closure_extensions.employer_closure_id')
            ->join('employers', 'employers.id', 'employer_closures.employer_id')
            ->leftJoin('regions', 'regions.id', 'employers.region_id')
            ->leftJoin('office_zones', 'office_zones.id', 'regions.office_zone_id')
            ->whereBetween('employer_closure_extensions.application_date', [$data['from_date']. ' 00:00:00', $data['to_date'] . ' 23:59:59'])
            ->whereNull('employer_closure_extensions.deleted_at');

        //        with status




        return $this->query->withoutGlobalScopes()->orderByDesc('employer_closure_extensions.application_date')->whereNull("employers.deleted_at");

    }



    /**
     * @return mixed
     * Find Total employers to be paid
     */
    public function findTotalEmployers()
    {
        $total = $this->query()->select(['id'])->count();
        return $total;
    }


    public function download($data){

        $employers = $this->searchResult($data)->get()->toArray();

        return Excel::create($this->filename(), function($excel) use ($employers) {
            $excel->sheet('mySheet', function($sheet) use ($employers)
            {
                $sheet->fromArray($employers);
            });
        })->download('csv');

    }




    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer_closure/extension/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_name', 'name' => 'employers.name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'employers.reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'tin', 'name' => 'employers.tin', 'title' => trans('labels.general.tin_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'doc', 'name' => 'employers.doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => true, 'searchable' => true],
            ['data' => 'region_name', 'name' => 'regions.name', 'title' => trans('labels.general.region'), 'orderable' => false, 'searchable' => false],
            ['data' => 'application_date', 'name' => 'employer_closure_extensions.application_date', 'title' => 'Application Date', 'orderable' => true, 'searchable' => true],
            ['data' => 'approved_date', 'name' => 'employer_closure_extensions.wf_done_date', 'title' =>'Approved date',  'orderable' => false, 'searchable' => false],
//            ['data' => 'closure_type', 'name' => 'employer_closures.closure_type', 'title' => 'Closure Type', 'orderable' => false, 'searchable' => false],
            ['data' => 'remark', 'name' => 'employer_closure_extensions.remark', 'title' => 'Remark', 'orderable' => true, 'searchable' => true],
        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'closedbusinessextension_' . time();
    }
}


