<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Organization\FiscalYearRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;


class ReceiptContributionDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('contrib_month', function ($contribution) {
                return short_date_format($contribution->contrib_month);
            })
            ->editColumn('contrib_amount', function ($contribution) {
                return number_2_format($contribution->contrib_amount);
            })
            ->editColumn('receipt_amount', function ($contribution) {
                return number_2_format($contribution->receipt_amount);
            })
            ->editColumn('doc', function ($contribution) {
                return short_date_format($contribution->doc);
            })
            ->editColumn('rct_date', function ($contribution) {
                return short_date_format($contribution->rct_date);
            })
            ->editColumn('receipt_date', function ($contribution) {
                return short_date_format($contribution->receipt_date);
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);

        /*Search for category*/
        if(isset($this->input['general_type']) && $this->input['general_type'] > 0) {
            $this->query  =   $this->queryWithContributionCategory($this->input);
        }

        /*Search region*/
        if(isset($this->input['region_id']) && $this->input['region_id'] > 0){
            $this->query = $this->queryWithRegion($this->input);
        }
        return $this->query;
    }




    public function searchResult($data)
    {
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $this->query= DB::table('main.employer_contributions')->select(  'employer_contributions.employer_id', 'employer_contributions.doc',  'employer_name', 'employer_reg_no','contrib_month', 'contrib_amount', 'rctno', 'rct_date', 'receipt_amount', 'receipt_created_at as receipt_date',  'regions.name as region_name')
            ->leftJoin('contributing_category_employer', 'contributing_category_employer.employer_id', 'employer_contributions.employer_id')
            ->join('employers', 'employers.id', 'employer_contributions.employer_id')
            ->leftJoin('regions', 'employers.region_id', 'regions.id')
            ->where('rct_date', '>=', $from_date)->where('rct_date', '<=', $to_date)->where('islegacy', 0);


        return $this->query;
    }


    /*Query with contribution category*/
    public function queryWithContributionCategory($input)
    {
        //        $category_ids = ($input['general_type'] == 2) ? [2,3] : [$input['general_type']];
        $category_ids = ($input['general_type'] == 4) ? [3,4] : [$input['general_type']];
        $this->query = $this->query->whereIn('contributing_category_id',$category_ids);
        return $this->query;

    }

    /*Query with region*/
    public function queryWithRegion($input)
    {
        $this->query = $this->query->where('employers.region_id',$input['region_id'] );
        return $this->query;
    }


    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid
     */

    public function getTotalAmountPaid()
    {
        $amount_paid = $this->query()->sum("contrib_amount");
        return  number_format($amount_paid , 2 , '.' , ',' );
    }



    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_reg_no', 'name' => 'employer_reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employer_name', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => true],
            ['data' => 'doc', 'name' => 'doc', 'title' => 'Date of commencement', 'orderable' => false, 'searchable' => false],
//            ['data' => 'close_date', 'name' => 'close_date', 'title' =>'Close date',  'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'contrib_month', 'title' => 'Contrib Month',  'orderable' => false, 'searchable' => false],
            ['data' => 'rct_date', 'name' => 'rct_date', 'title' => 'Payment date', 'orderable' => false, 'searchable' => false],
            ['data' => 'rctno', 'name' => 'rctno', 'title' => 'Receipt no.', 'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_amount', 'name' => 'contrib_amount', 'title' => 'Contribution Amount', 'orderable' => false, 'searchable' => false],
            ['data' => 'receipt_amount', 'name' => 'receipt_amount', 'title' => 'Receipt Amount', 'orderable' => false, 'searchable' => false],
            ['data' => 'receipt_date', 'name' => 'receipt_created_at', 'title' => 'Receipt date', 'orderable' => false, 'searchable' => false],
            ['data' => 'region_name', 'name' => 'regions.name', 'title' => 'Region Name', 'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employercontributions' . time();
    }
}
