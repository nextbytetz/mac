<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Organization\FiscalYearRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;


class ReceiptsForPrevContribIncomeBookedDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('contrib_month', function($contribution) {
                return short_date_format($contribution->contrib_month);
            })
            ->editColumn('contrib_amount', function($contribution) {
                return number_2_format($contribution->contrib_amount);
            })
            ->editColumn('receipt_amount', function($contribution) {
                return number_2_format($contribution->receipt_amount);
            })
            ->editColumn('doc', function($contribution) {
                return short_date_format($contribution->doc);
            })
            ->editColumn('rct_date', function($contribution) {
                return short_date_format($contribution->rct_date);
            })
            ->editColumn('receipt_created_at', function($contribution) {
                return short_date_format($contribution->receipt_created_at);
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query;
    }




    public function searchResult($data)
    {
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $this->query= DB::table('main.employer_contributions')->select( 'employer_contributions.employer_id', 'doc',  'employer_name', 'employer_reg_no','contrib_month', 'contrib_amount', 'rctno', 'rct_date', 'receipt_amount', 'receipt_created_at')
            ->join('employers_min_payment_date', 'employers_min_payment_date.employer_id', 'employer_contributions.employer_id')
            ->where('rct_date', '>=', $from_date)->where('rct_date', '<=', $to_date)
            ->where('receipt_created_at', '>=', $from_date)->where('receipt_created_at', '<=', $to_date)
            ->where('contrib_month', '<', $from_date)
            ->where('employers_min_payment_date.min_payment_date', '<', $from_date)
            ->where('islegacy', 0);
        return $this->query;
    }


    /*get no of employers*/
    public function getNoOfEmployers($data)
    {
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];

        $employers = DB::table('main.employer_contributions')->select( 'employer_contributions.employer_id')->distinct()
            ->join('employers_min_payment_date', 'employers_min_payment_date.employer_id', 'employer_contributions.employer_id')
            ->where('rct_date', '>=', $from_date)->where('rct_date', '<=', $to_date)
            ->where('receipt_created_at', '>=', $from_date)->where('receipt_created_at', '<=', $to_date)
            ->where('contrib_month', '<', $from_date)
            ->where('employers_min_payment_date.min_payment_date', '<', $from_date)
            ->where('islegacy', 0)->get()->count();

        return $employers;
    }
    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid
     */

    public function getTotalAmountPaid()
    {
        $amount_paid = $this->query()->sum("contrib_amount");
        return  number_format($amount_paid , 2 , '.' , ',' );
    }



    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_reg_no', 'name' => 'employer_reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employer_name', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => true],
            ['data' => 'doc', 'name' => 'doc', 'title' => 'Date of commencement', 'orderable' => false, 'searchable' => false],
//            ['data' => 'close_date', 'name' => 'close_date', 'title' =>'Close date',  'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'contrib_month', 'title' => 'Contrib Month',  'orderable' => false, 'searchable' => false],
            ['data' => 'rct_date', 'name' => 'rct_date', 'title' => 'Payment date', 'orderable' => false, 'searchable' => false],
            ['data' => 'receipt_created_at', 'name' => 'rct_date', 'title' => 'Receipt date', 'orderable' => false, 'searchable' => false],
            ['data' => 'rctno', 'name' => 'rctno', 'title' => 'Receipt no.', 'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_amount', 'name' => 'contrib_amount', 'title' => 'Contribution Amount', 'orderable' => false, 'searchable' => false],
            ['data' => 'receipt_amount', 'name' => 'receipt_amount', 'title' => 'Receipt Amount', 'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'receiptprevcontributionsbooked' . time();
    }
}
