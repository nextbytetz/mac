<?php

namespace App\DataTables\Report\Compliance;

use App\Models\Finance\FinYear;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;


class ContribRcvableNewEntrantsDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('contrib_month', function($booking) {
                return short_date_format($booking->contrib_month);
            })
            ->editColumn('booked_amount', function($booking) {
                return number_2_format($booking->booked_amount);
            })
            ->editColumn('receivable_amount', function($booking) {
                return number_2_format($booking->receivable_amount);
            })
            ->editColumn('doc', function($booking) {
                return short_date_format($booking->doc);
            });
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query;
    }




//    public function searchResult($data)
//    {
//        $from_date = $data['from_date'];
//        $to_date = $data['to_date'];
//        $this->query= DB::table('main.booking_receivables')->select('booking_receivables.employer_id', 'employers.doc',  'booking_receivables.employer_name', 'booking_receivables.employer_reg_no','booking_receivables.rcv_date as contrib_month', 'booking_receivables.booked_amount', 'booking_receivables.amount_paid', DB::raw(" CASE WHEN (booked_amount - coalesce(amount_paid,0)) >= 0 THEN (booked_amount - coalesce(amount_paid,0)) ELSE 0 END as receivable_amount"),
//            DB::raw("((DATE_PART('year', NOW()::TIMESTAMP) - DATE_PART('year', booking_receivables.rcv_date::date)) * 12 +
//              (DATE_PART('month', NOW()::TIMESTAMP) - DATE_PART('month', booking_receivables.rcv_date::date)) - 1) as age_months"))
//            ->join('employers', 'employers.id', 'booking_receivables.employer_id')
//            ->where('booking_receivables.rcv_date', '<', $from_date)
//            ->where('employers.wf_done_date', '>=', $from_date)->where('employers.wf_done_date', '<=', $to_date)
//            ->whereRaw("coalesce(amount_paid,0) = 0");
////
////        $test = DB::table('main.booking_receivables')->select('booking_receivables.employer_name', 'booking_receivables.employer_reg_no','booking_receivables.rcv_date as contrib_month', 'booking_receivables.booked_amount','booking_receivables.amount_paid','booking_receivables.booked_amount - booking_receivables.amount_paid)' )->first();
////dd($test);
//        return $this->query;
//
//
//    }


    public function searchResult($data)
    {
        //REMINDER: Any change to this search query need to update GETNOEMPLOYERS() function
        $from_date = standard_date_format($data['from_date']);
        $to_date = standard_date_format($data['to_date']);
        $fin_year = $data['fin_year'];
        if(isset($fin_year)){
            $fiscal_year = FinYear::query()->find($data['fin_year']);
        }else{
            $fiscal_year = FinYear::query()->orderBy('id', 'desc')->first();
        }
        $start_fiscal_date = standard_date_format($fiscal_year->start_date);
        $end_fiscal_date = standard_date_format($fiscal_year->end_date);
        $after_fiscal_date = standard_date_format(Carbon::parse(($fiscal_year->end_date))->addDay(1));
        $today = Carbon::now();
//        $receipt_cutoff_date = (comparable_date_format($today) < comparable_date_format($end_fiscal_date)) ? $start_fiscal_date : $end_fiscal_date;
        $this->query= DB::table('main.bookings_mview')->select( 'bookings_mview.employer_id', 'employers.doc',  'bookings_mview.employer_name', 'bookings_mview.employer_reg_no','bookings_mview.rcv_date as contrib_month', 'bookings_mview.booked_amount',
            DB::raw("coalesce(bookings_mview.amount_paid,0) as amount_paid"),
            DB::raw("case when bookings_mview.ispaid = 1 then 0 else booked_amount - coalesce(amount_paid,0) end as receivable_amount"),
            DB::raw("(CASE WHEN bookings_mview.age_months < 0 THEN 0 ELSE bookings_mview.age_months END) as age_months"),
            DB::raw("(CASE WHEN bookings_mview.age_days < 0 THEN 0 ELSE bookings_mview.age_days END) as age_days"))
            ->join('employers', 'employers.id', 'bookings_mview.employer_id')
            ->where('employers.employer_status', '<>', 3)
            ->whereRaw("coalesce(receipt_date, ?) > ?",[$after_fiscal_date, $end_fiscal_date])
//            ->whereRaw("((employer_min_pay_date >= ? and employer_min_pay_date <= ?) or (employer_registered_date >= ? and employer_registered_date <= ?))",[$start_fiscal_date, $end_fiscal_date,$start_fiscal_date, $end_fiscal_date] )
            ->whereRaw("((employer_registered_date >= ? and employer_registered_date <= ?))",[$start_fiscal_date, $end_fiscal_date] )
            ->where('bookings_mview.rcv_date', '>=', $from_date)->where('bookings_mview.rcv_date', '<=', $to_date);

        return $this->query;
    }




    /*get no of employers*/
    public function getNoOfEmployers($data)
    {
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $fin_year = $data['fin_year'];
        if(isset($fin_year)){
            $fiscal_year = FinYear::query()->find($data['fin_year']);
        }else{
            $fiscal_year = FinYear::query()->orderBy('id', 'desc')->first();
        }

        $start_fiscal_date = standard_date_format($fiscal_year->start_date);
        $end_fiscal_date = standard_date_format($fiscal_year->end_date);
        $today = Carbon::now();
        $after_fiscal_date = standard_date_format(Carbon::parse(($fiscal_year->end_date))->addDay(1));
        $employers= DB::table('main.bookings_mview')->select('bookings_mview.employer_id')->distinct()
            ->join('employers', 'employers.id', 'bookings_mview.employer_id')
            ->where('employers.employer_status', '<>', 3)
            ->whereRaw("coalesce(receipt_date, ?) > ?",[$after_fiscal_date, $end_fiscal_date])
            ->whereRaw("((employer_registered_date >= ? and employer_registered_date <= ?))",[$start_fiscal_date, $end_fiscal_date] )
            ->where('bookings_mview.rcv_date', '>=', $from_date)->where('bookings_mview.rcv_date', '<=', $to_date)
            ->get()->count();
        return $employers;
    }


    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid
     */

    public function getTotalAmountPaid()
    {
        $amount_paid = $this->query()->sum('amount_paid');
        return  number_format($amount_paid , 2 , '.' , ',' );
    }


    /**Object of class stdClass could not be converted to string
     * @param $booking_id
     * @return string
     * Get Total Amount for this search
     */
    public function getTotalReceivable()
    {
        $receivable = $this->query()->sum(DB::raw("case when bookings_mview.ispaid = 1 then 0 else (booked_amount - coalesce(amount_paid,0)) end"));
        return  number_format($receivable , 2 , '.' , ',' );
    }



    public function getTotalBooked()
    {
        $total_booked_amount= $this->query()->sum('booked_amount');
        return  number_format($total_booked_amount , 2 , '.' , ',' );
    }

    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }



    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employer_name', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => true],
            ['data' => 'doc', 'name' => 'doc', 'title' => 'Date of commencement', 'orderable' => false, 'searchable' => false],
//            ['data' => 'close_date', 'name' => 'close_date', 'title' =>'Close date',  'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'rcv_date', 'title' => trans('labels.backend.table.booking.rcv_date'),  'orderable' => false, 'searchable' => false],
            ['data' => 'age_months', 'name' => 'age_months', 'title' => 'Aging in months',  'orderable' => false, 'searchable' => false],
            ['data' => 'age_days', 'name' => 'age_days', 'title' => 'Aging in days',  'orderable' => false, 'searchable' => false],
            ['data' => 'booked_amount', 'name' => 'booked_amount', 'title' => 'Booked Amount', 'orderable' => false, 'searchable' => false],
            ['data' => 'amount_paid', 'name' => 'amount_paid', 'title' => trans('labels.backend.table.amount_paid'), 'orderable' => false, 'searchable' => false],
            ['data' => 'receivable_amount', 'name' => 'receivable_amount', 'title' => 'Receivable Amount', 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contributionreceivablesnewemployers' . time();
    }
}
