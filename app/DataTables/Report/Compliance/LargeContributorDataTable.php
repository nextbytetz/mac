<?php

namespace App\DataTables\Report\Compliance;

use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Facades\DB;

class LargeContributorDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

         return $this->datatables
            ->of($this->query())
            
            ->addColumn('contribution_formatted', function ($employer) {
                return number_2_format($employer->contribution);
            })
            ->addColumn('employees_formatted', function ($employer) {
                return number_0_format($employer->employees);
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = DB::table("comp_large_contributor")->select();
        //return $this->applyScopes($query);
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'id', 'name' => 'id', 'title' => "ID", 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'name', 'name' => 'name', 'title' => "Name", 'orderable' => true, 'searchable' => true],
            ['data' => 'phone', 'name' => 'phone', 'title' => "Phone", 'orderable' => true, 'searchable' => true],
            ['data' => 'telephone', 'name' => 'telephone', 'title' => "Telephone", 'orderable' => true, 'searchable' => true],
            ['data' => 'email', 'name' => 'email', 'title' => "Email", 'orderable' => true, 'searchable' => true],
            ['data' => 'region', 'name' => 'region', 'title' => "Region", 'orderable' => true, 'searchable' => true],
            ['data' => 'tin', 'name' => 'tin', 'title' => "Tin", 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'reg_no', 'title' => "Registration", 'orderable' => true, 'searchable' => true],
            ['data' => 'contribution_formatted', 'name' => 'contribution', 'title' => "Contribution", 'orderable' => true, 'searchable' => true],
            ['data' => 'employees_formatted', 'name' => 'employees', 'title' => "Employees", 'orderable' => true, 'searchable' => true],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportcompliancelargecontributordatatable_' . time();
    }
}
