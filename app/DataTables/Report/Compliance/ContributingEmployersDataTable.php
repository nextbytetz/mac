<?php

namespace App\DataTables\Report\Compliance;

use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ContributingEmployersDataTable extends DataTable
{

    protected $query;
    protected $query_where;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query= DB::table('main.contributing_employers')->distinct()->select('employers.id as employer_id' , 'employers.reg_no as registration_number','employers.name as employername','employers.doc as date_commenced','employers.tin','employers.phone','employers.telephone','employers.email','regions.name as region','employers.street')
            ->join('employers', 'employers.id','contributing_employers.employer_id' )
            ->leftJoin('regions', 'employers.region_id','regions.id' )
            ->join('receipts', 'receipts.employer_id','employers.id' )
//            ->join('legacy_receipts', 'legacy_receipts.employer_id','employers.id' )
            ->join('receipt_codes', 'receipt_codes.receipt_id','receipts.id')
//            ->leftJoin('legacy_receipt_codes', 'legacy_receipt_codes.legacy_receipt_id','legacy_receipts.id' );
            ->leftJoin('office_zones', 'office_zones.id','regions.office_zone_id')
            ->leftJoin('contributing_category_employer', 'contributing_category_employer.employer_id','employers.id')
            ->whereNull('employers.duplicate_id');



//        /*search with date*/
        if(isset($this->input['date_type']) && isset($this->input['from_date']) && $this->input['date_type'] > 0 ){
            $this->query =    $this->searchWithDate();
        }

//        /*search with employer category/type*/
        if(isset($this->input['type']) && $this->input['type'] > 0){
            $this->query =    $this->searchWithEmployerCategory();
        }
//
//
//        /*search with zone*/
        if(isset($this->input['office_zone_id']) && $this->input['office_zone_id'] > 0){
            $this->query =    $this->searchWithOfficeZone();
        }

        /*Search with contributing category*/
        if(isset($this->input['general_type']) && $this->input['general_type'] > 0){
            $this->query =    $this->searchWithContributingCategory();
        }

        /*Order*/
        $this->query = $this->query
            ->orderBy('employers.name', 'asc');

        return $this->applyScopes($this->query);
    }



    /*search with date type*/
    public function searchWithDate()
    {
        $date_type = $this->input['date_type'];
        $date = standard_date_format($this->input['from_date']) ;
        if($date_type == 1){
            /*receipt date*/
            $this->query = $this->query
                ->where(function($query) use($date){
                    $query->whereDate('receipts.created_at', $date);
                });
        }

        if($date_type == 2){
            /*payment date*/
            $this->query = $this->query
                ->where(function($query) use($date){
                    $query->whereDate('receipts.rct_date', $date);
                });
        }

        if($date_type == 3){
            /*application/contribution month*/
            $date  = '28-'.  Carbon::parse($date)->format('M') . '-' . Carbon::parse($date)->format('Y');
            $date = standard_date_format($date);
            $this->query = $this->query
                ->where(function($query) use($date){
                    $query->whereDate('receipt_codes.contrib_month', $date);
                });
        }

        return       $this->query;
    }


    /*search with employer type - public /private*/
    public function searchWithEmployerCategory()
    {
        $this->query = $this->query
            ->where('employer_category_cv_id', $this->input['type']);

        return       $this->query;
    }

    /*Search with office zone*/
    public function searchWithOfficeZone()
    {
        $this->query = $this->query
            ->where('office_zones.id', $this->input['office_zone_id']);

        return       $this->query;
    }

    /*Search with contributing category*/
    public function searchWithContributingCategory()
    {
        $this->query = $this->query
            ->where('contributing_category_employer.contributing_category_id', $this->input['general_type']);

        return       $this->query;
    }

    /**
     * @return mixed
     * Find Total employers
     */

    public function findTotalEmployers()
    {
        $total = $this->query()->count();
        return $total;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->parameters([
                'dom'     => 'Bfrtip',
                'buttons' => ['csv', 'excel','print', 'reset', 'reload'],

                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'registration_number' => ['data' => 'registration_number', 'name' => 'employers.reg_no'],
            'employer_name' =>[ 'data' => 'employername', 'name' => 'employers.name'],
            'date_commenced' => ['data' => 'date_commenced', 'name' => 'employers.doc'],
            'tin' => ['data' => 'tin', 'name' => 'employers.tin'],
            'phone' => ['data' => 'phone', 'name' => 'employers.phone'],
            'telephone' => ['data' => 'telephone', 'name' => 'employers.telephone'],
            'email' => ['data' => 'email', 'name' => 'employers.email'],
            'region' => ['data' => 'region', 'name' => 'regions.name'],
            'street' => ['data' => 'street', 'name' => 'employers.street'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contributing_employers_report' . time();
    }




}
