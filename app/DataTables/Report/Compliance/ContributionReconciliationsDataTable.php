<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Organization\FiscalYearRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;


class ContributionReconciliationsDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('contrib_month', function($contribution) {
                return short_date_format($contribution->contrib_month);
            })
            ->editColumn('current_amount', function($contribution) {
                return number_2_format($contribution->current_amount);
            })
            ->editColumn('prev_amount', function($contribution) {
                return number_2_format($contribution->prev_amount);
            })
            ->editColumn('doc', function($contribution) {
                return short_date_format($contribution->doc);
            });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query;
    }




    public function searchResult($data)
    {
        $from_date = Carbon::parse($data['from_date']);

        $this->query= DB::table('main.employer_contribution_reconciliations')->select(  'employer_id', 'doc',  'employers.name as employer_name', 'reg_no','contrib_month', 'current_amount', 'prev_amount')
            ->join('employers', 'employers.id', 'employer_contribution_reconciliations.employer_id')
            ->whereMonth('contrib_month','=',$from_date->format('m'))->whereYear('contrib_month','=',$from_date->format('Y') );


        return $this->query;
    }


    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid
     */

    public function getTotalAmountPaid()
    {
        $amount_paid = $this->query()->sum("current_amount");
        return  number_format($amount_paid , 2 , '.' , ',' );
    }


    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid - previous months
     */

    public function getTotalAmountPaidPrev()
    {
        $amount_paid = $this->query()->sum("prev_amount");
        return  number_format($amount_paid , 2 , '.' , ',' );
    }


    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employers.name', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => true],
            ['data' => 'doc', 'name' => 'doc', 'title' => 'Date of commencement', 'orderable' => false, 'searchable' => false],
//            ['data' => 'close_date', 'name' => 'close_date', 'title' =>'Close date',  'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'contrib_month', 'title' => 'Contrib Month',  'orderable' => false, 'searchable' => false],
            ['data' => 'current_amount', 'name' => 'current_amount', 'title' => 'Contrib Amount (This Month)', 'orderable' => false, 'searchable' => false],
            ['data' => 'prev_amount', 'name' => 'prev_amount', 'title' => 'Contrib Amount (Previous Month)', 'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employercontributions' . time();
    }
}
