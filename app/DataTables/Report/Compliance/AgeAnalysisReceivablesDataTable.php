<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Organization\FiscalYearRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;


class AgeAnalysisReceivablesDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('contrib_month', function($booking) {
                return short_date_format($booking->contrib_month);
            })
            ->editColumn('booked_amount', function($booking) {
                return number_2_format($booking->booked_amount);
            })
            ->editColumn('doc', function($booking) {
                return short_date_format($booking->doc);
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query;
    }




    public function searchResult($data)
    {
        $from_date = $data['from_date'];
        $today = Carbon::now();
        $from_date_parsed = Carbon::parse($data['from_date']);
$days_from_cuttoff_date = $today->diffInDays($from_date_parsed); //Days diff from cutoff date from today
                $this->query=  DB::table('main.bookings_mview')->select( 'bookings_mview.employer_id', 'employers.doc',  'bookings_mview.employer_name', 'bookings_mview.employer_reg_no','bookings_mview.rcv_date as contrib_month', 'bookings_mview.booked_amount',
            DB::raw("coalesce(bookings_mview.amount_paid,0) as amount_paid"),
            DB::raw("case when bookings_mview.ispaid = 1 then 0 else booked_amount - coalesce(amount_paid,0) end as receivable_amount"),
            DB::raw("(CASE WHEN bookings_mview.age_months < 0 THEN 0 ELSE bookings_mview.age_months END) as age_months"),
            DB::raw("(CASE WHEN bookings_mview.age_days < 0 THEN 0 ELSE bookings_mview.age_days END) as age_days"))
            ->join('employers', 'employers.id', 'bookings_mview.employer_id')
            ->where('bookings_mview.rcv_date', '<=', $from_date)
            ->where('bookings_mview.ispaid',0);


        /*query with min days*/
        if(isset($data['min_days'])){
            $this->query = $this->queryWithMinDays($data, $days_from_cuttoff_date);
        }

        /*query with max days*/
        if(isset($data['max_days'])){
            $this->query = $this->queryWithMaxDays($data, $days_from_cuttoff_date);
        }

        return $this->query->orderBy('bookings_mview.employer_id')->orderBy('bookings_mview.rcv_date');
    }


    /**
     * @param $data
     * @return mixed
     * query with min months
     */
    public function queryWithMinDays($data, $days_from_cuttoff_date){
        $min = $data['min_days'];
           $this->query = $this->query->whereRaw(" (bookings_mview.age_days -  ? ) >= ? ", [$days_from_cuttoff_date,$min]);
        return $this->query;
    }


    /**
     * @param $data
     * @return mixed
     * query with max months
     */
    public function queryWithMaxDays($data, $days_from_cuttoff_date){
        $max = $data['max_days'];
        $this->query = $this->query->whereRaw("(bookings_mview.age_days  - ?)<= ? ", [$days_from_cuttoff_date,$max]);
        return $this->query;
    }





    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid
     */

    public function getTotalAmountPaid()
    {
        $amount_paid = 0;
        return  number_format($amount_paid , 2 , '.' , ',' );
    }


    /**Object of class stdClass could not be converted to string
     * @param $booking_id
     * @return string
     * Get Total Amount for this search
     */
    public function getTotalReceivable()
    {
        $receivable = $this->query()->sum('bookings_mview.booked_amount');
        return  number_format($receivable , 2 , '.' , ',' );
    }



    public function getTotalBooked()
    {
        $total_booked_amount= $this->query()->sum('bookings_mview.booked_amount');
        return  number_format($total_booked_amount , 2 , '.' , ',' );
    }

    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employer_name', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => true],
            ['data' => 'doc', 'name' => 'doc', 'title' => 'Date of commencement', 'orderable' => false, 'searchable' => false],
//            ['data' => 'close_date', 'name' => 'close_date', 'title' =>'Close date',  'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'rcv_date', 'title' => trans('labels.backend.table.booking.rcv_date'),  'orderable' => false, 'searchable' => false],
            ['data' => 'age_months', 'name' => 'age_months', 'title' => 'Aging in months',  'orderable' => false, 'searchable' => false],
            ['data' => 'age_days', 'name' => 'age_days', 'title' => 'Aging in days',  'orderable' => false, 'searchable' => false],
//            ['data' => 'booked_amount', 'name' => 'booked_amount', 'title' => trans('labels.backend.table.booked_amount'), 'orderable' => false, 'searchable' => false],
//            ['data' => 'amount_paid', 'name' => 'amount_paid', 'title' => trans('labels.backend.table.amount_paid'), 'orderable' => false, 'searchable' => false],
            ['data' => 'booked_amount', 'name' => 'booked_amount', 'title' => 'Receivable Amount', 'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'contributionreceivables' . time();
    }
}
