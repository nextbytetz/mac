<?php

namespace App\DataTables\Report\Compliance;

use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;


class NewEmployeesDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('name', function($employee) {
                return $employee->name;
            })
//            ->editColumn('employer_id', function($employee) {
//                return $employee->employer->name;
//            })
            ->editColumn('gender', function($employee) {
                return ($employee->gender()->count()) ? $employee->gender->name : ' ';
            })
            ->editColumn('employer_category_id', function($employee) {
                return ($employee->employeeCategory()->count()) ? $employee->employeeCategory->name : ' ';
            })
        ->editColumn('created_at', function($employee) {
        return $employee->created_at_formatted;
    });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $employees = new EmployeeRepository();
        $query = $employees->query()->where('created_at', '>=', $from_date . ' 00:00:00')->where('created_at', '<=', $to_date . ' 23:59:59' );
        return $this->applyScopes($query);
    }


    /**
     * @return mixed
     * Find Total employees
     */

    public function findTotalEmployees()
    {
        $total = $this->query()->select(['id'])->count();
        return $total;

    }
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employee/profile/' + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.fullname'),'orderable' => false, 'searchable' => false],
            ['data' => 'dob', 'name' => 'dob', 'title' => trans('labels.backend.table.dob'), 'orderable' => false, 'searchable' => false],
//            ['data' => 'employer_id', 'name' => 'employer_id', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_category_id', 'name' => 'employer_category_id', 'title' => trans('labels.backend.member.employer_category'), 'orderable' => false, 'searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => trans('labels.backend.table.created_at'), 'orderable' => false, 'searchable' => true],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'newemployees' . time();
    }
}
