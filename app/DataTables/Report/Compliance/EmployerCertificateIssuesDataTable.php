<?php

namespace App\DataTables\Report\Compliance;

use App\Models\Operation\Compliance\Member\EmployerCertificateIssue;
use Yajra\Datatables\Services\DataTable;

class EmployerCertificateIssuesDataTable extends DataTable
{


    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('user', function($employer_certificate_issue) {
                return $employer_certificate_issue->user->name;
            })
            ->addColumn('employer', function($employer_certificate_issue) {
                return $employer_certificate_issue->employer->name;
            })
              ->addColumn('issue_status', function($employer_certificate_issue) {
                return $employer_certificate_issue->issue_status_label;
            })
            ->editColumn('created_at', function($employee) {
                return $employee->created_at_formatted;
            })
            ->rawColumns(['issue_status']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $employer_certificate_issues = new EmployerCertificateIssue();
        $this->query = $employer_certificate_issues->query()->where('created_at', '>=', $from_date . ' 00:00:00')->where('created_at', '<=', $to_date . ' 23:59:59' );


        /* Issue type */
        if (isset($this->input['type'])) {
            if ($this->input['type'] >= 0) {
                $this->query = $this->queryWithIssueType();
            }
        }



        return $this->applyScopes($this->query);
    }

    /*
     * query date range with payment type selected
     */
    public function queryWithIssueType()
    {
        return  $this->query->where('is_reissue', $this->input['type']);

    }


    /**
     * @return mixed
     * Find Total employers issued certificate
     */

    public function findTotalEmployers()
    {
        $total = $this->query()->select(['id'])->count();
        return $total;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'user', 'name' => 'user', 'title' => trans('labels.backend.member.issued_by'),'orderable' => false, 'searchable' => false],
            ['data' => 'employer', 'name' => 'employer', 'title' => trans('labels.general.employer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'issue_status', 'name' => 'issue_status', 'title' => trans('labels.backend.member.issue_type'), 'orderable' => false, 'searchable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => trans('labels.backend.member.issue_date'), 'orderable' => false, 'searchable' => false],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employercertificateissues_' . time();
    }
}
