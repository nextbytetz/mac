<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Organization\FiscalYearRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;


class EmployerChangeParticularsDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('date_received', function($change) {
                return short_date_format($change->date_received);
            })
            ->editColumn('remark', function($change) {
                return truncateString($change->remark, 150);
            })
            ->editColumn('wf_done_date', function($change) {
                return isset($change->wf_done_date) ? short_date_format($change->wf_done_date) : '';
            });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query->orderBy('date_received');
    }




    public function searchResult($data)
    {
        //REMINDER: Any change to this search query need to update function
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $this->query= DB::table('main.employer_particular_changes as c')->select(
            'e.name as employer_name',
            'e.reg_no',
            'c.remark',
            'c.date_received',
            'c.wf_done',
            'c.wf_done_date'
        )
            ->join('employers as e', 'e.id', 'c.employer_id')
            ->where('c.date_received', '>=', $from_date)->where('c.date_received', '<=', $to_date)
            ->where('c.wf_done', 1);


        return $this->query;
    }




    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');


    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
//                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_name', 'name' => 'e.name', 'title' =>'Employer name', 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'e.reg_no', 'title' => 'Reg No.', 'orderable' => true, 'searchable' => true],
            ['data' => 'date_received', 'name' => 'c.date_received', 'title' => 'Date received', 'orderable' => true, 'searchable' => true],
            ['data' => 'wf_done_date', 'name' => 'c.wf_done_date', 'title' => 'Approved date', 'orderable' => true, 'searchable' => false],
            ['data' => 'remark', 'name' => 'c.remark', 'title' => 'Remark', 'orderable' => true, 'searchable' => true],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'changeparticulars' . time();
    }
}
