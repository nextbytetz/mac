<?php
namespace App\DataTables\Report\Compliance;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class DormantEmployersDataTable extends DataTable
{
    protected $query;


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('source', function ($employer) {
                return ($employer->source == 1) ? 'WCF Branch' : 'Online';
            })
            ->addColumn('doc_formatted', function($employer) {
                return short_date_format($employer->doc);
            })

            ->editColumn('date_registered', function($employer) {
                return short_date_format($employer->date_registered);
            })
            ->rawColumns(['status']);
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query = $this->searchResult($this->input);

        return $this->query;
    }



    public function searchResult($data){
        $cutoff_date = $data['from_date'];
        $employers = new EmployerRepository();
        //Search date range
        $this->query = $employers->query()->select(
            'employers.id as employer_id',
            'employers.name as employer_name',
            'employers.reg_no',
            'employers.tin',
            'employers.vote',
            'employers.doc',
            'regions.name as region_name',
            'employers.source',
            'employers.approval_id as status',
            'employers.created_at as date_registered',
            DB::raw("((DATE_PART('year', NOW()::TIMESTAMP) - DATE_PART('year', employers.dormant_date::date)) * 12 +
              (DATE_PART('month', NOW()::TIMESTAMP) - DATE_PART('month', employers.dormant_date::date)) - 1) as dormant_period")
        )
            ->leftJoin('regions', 'regions.id', 'employers.region_id')
            ->leftJoin('office_zones', 'office_zones.id', 'regions.office_zone_id')
            ->whereDate('dormant_date', '<=',  $cutoff_date)
            ->where('employers.employer_status', 2)
            ->where('employers.approval_id', 1);




        if (isset($data['region_id'])) {
            if ($data['region_id'] > 0) {
                $this->query = $this->queryWithRegion($data);
            }
        }


        /* with office zone */
        if (isset($data['office_zone_id'])) {
            if ($data['office_zone_id'] > 0) {
                $this->query = $this->queryWithOfficeZone($data);
            }
        }

        return $this->query->withoutGlobalScopes()->whereNull("employers.deleted_at");

    }


    /*
     * query date range with status selected
     */
    public function queryWithStatus($data)
    {
        return  $this->query->where('employers.approval_id', $data['status_id']);
    }
    /*
     * query date range with payment type selected
     */
    public function queryEmployerCategory($data)
    {
        return  $this->query->where('employers.employer_category_cv_id', $data['type']);
    }
    /**
     * @return mixed
     * Query with region
     */
    public function queryWithRegion($data)
    {
        return  $this->query->where('employers.region_id', $data['region_id']);
    }
    /*
* query date range with with business selected
*/
    public function queryWithBusiness($data)
    {
        return $this->query->whereHas('sectors', function ($query) use($data){
            $query->where('business_sector_cv_id',$data['business_id']);

        });
//        return $this->query->where('employer_sectors.business_sector_cv_id',$this->input['business_id']);
    }
    /**
     * @return mixed
     * Query with source of registration
     */
    public function queryWithSource($data)
    {
        return  $this->query->where('source', $data['general_type']);
    }

    public function queryWithOfficeZone($data)
    {
        return  $this->query->where('office_zones.id', $data['office_zone_id']);
    }



    /**
     * @return mixed
     * Find Total employers to be paid
     */
    public function findTotalEmployers()
    {
        $total = $this->query()->select(['id'])->count();
        return $total;
    }


    public function download($data){

        $employers = $this->searchResult($data)->get()->toArray();

        return Excel::create($this->filename(), function($excel) use ($employers) {
            $excel->sheet('mySheet', function($sheet) use ($employers)
            {
                $sheet->fromArray($employers);
            });
        })->download('csv');


    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_name', 'name' => 'employers.name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'employers.reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'tin', 'name' => 'employers.tin', 'title' => trans('labels.general.tin_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'vote', 'name' => 'employers.vote', 'title' => trans('labels.backend.member.vote'), 'orderable' => true, 'searchable' => true],
            ['data' => 'doc', 'name' => 'employers.doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => true, 'searchable' => true],
            ['data' => 'region_name', 'name' => 'regions.name', 'title' => trans('labels.general.region'), 'orderable' => false, 'searchable' => false],
//            ['data' => 'source', 'name' => 'employers.source', 'title' => trans('labels.general.source'), 'orderable' => false, 'searchable' => false],
                 ['data' => 'date_registered', 'name' => 'employers.created_at', 'title' => trans('labels.general.date_registered'), 'orderable' => true, 'searchable' => true],
            ['data' => 'dormant_period', 'name' => 'dormant_period', 'title' => 'Dormant Period (months)', 'orderable' => false, 'searchable' => false],
        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'dormantreport_' . time();
    }
}


