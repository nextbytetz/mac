<?php
namespace App\DataTables\Report\Compliance;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureExtensionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class ClosedBusinessNeedActionAfterFollowupDataTable extends DataTable
{
    protected $query;


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())


            ->editColumn('close_date', function($employer) {
                return short_date_format($employer->close_date);
            })

            ->editColumn('followup_ref_date', function($booking) {
                return short_date_format($booking->followup_ref_date);
            })
            ->editColumn('doc', function($booking) {
                return short_date_format($booking->doc);
            })
            ->rawColumns(['']);
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query = $this->searchResult($this->input);

        return $this->query;
    }



    public function searchResult($data){

        $employer_closure_repo = new EmployerClosureRepository();

        //Search date range
        $this->query = $employer_closure_repo->query()->select(
            'employer_closures.id as id',
            'employers.id as employer_id',
            'employers.name as employer_name',
            'employers.reg_no',
            'employers.doc',
            'regions.name as region_name',
            'employer_closures.close_date',
            'employer_closures.followup_ref_date'
        )
            ->join('employers', 'employers.id', 'employer_closures.employer_id')
            ->leftJoin('regions', 'regions.id', 'employers.region_id')
            ->leftJoin('office_zones', 'office_zones.id', 'regions.office_zone_id')
            ->whereNull('employer_closures.open_date')->where('employer_closures.followup_outcome', '<>', 0);


        //        with type - followup outcome
        if(isset($data['type'])){
            $this->query = $this->queryWithFollowupFeedback($data);
        }




        return $this->query->withoutGlobalScopes()->whereNull("employers.deleted_at")->whereNull('employers.duplicate_id');

    }

    /*query with followup feedback - type*/
    public function queryWithFollowupFeedback($data)
    {
        $followup_outcome = $data['type'];
        switch($followup_outcome){
            case 1://Still closed
                return  $this->query->doesnthave('closureExtensions')->where('employer_closures.followup_outcome', $followup_outcome);
                break;

            case 2://resume
                return  $this->query->doesnthave('open')->where('employer_closures.followup_outcome', $followup_outcome);
                break;

            default:
                return $this->query;
                break;
        }
    }

    /**
     * @return mixed
     * Find Total employers to be paid
     */
    public function findTotalEmployers()
    {
        $total = $this->query()->select(['id'])->count();
        return $total;
    }


    public function download($data){

        $employers = $this->searchResult($data)->get()->toArray();

        return Excel::create($this->filename(), function($excel) use ($employers) {
            $excel->sheet('mySheet', function($sheet) use ($employers)
            {
                $sheet->fromArray($employers);
            });
        })->download('csv');

    }




    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/closure/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_name', 'name' => 'employers.name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'employers.reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'doc', 'name' => 'employers.doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => true, 'searchable' => true],
            ['data' => 'region_name', 'name' => 'regions.name', 'title' => trans('labels.general.region'), 'orderable' => false, 'searchable' => false],
            ['data' => 'close_date', 'name' => 'employer_closures.close_date', 'title' => 'Close Date', 'orderable' => false, 'searchable' => false],
            ['data' => 'followup_ref_date', 'name' => 'employer_closures.followup_ref_date', 'title' => 'Followup Date', 'orderable' => false, 'searchable' => false],

        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'closedbusinessextension_' . time();
    }
}


