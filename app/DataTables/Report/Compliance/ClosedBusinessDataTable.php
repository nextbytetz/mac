<?php
namespace App\DataTables\Report\Compliance;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class ClosedBusinessDataTable extends DataTable
{
    protected $query;


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())

            ->addColumn('doc_formatted', function($employer) {
                return short_date_format($employer->doc);
            })
            ->editColumn('close_date', function($employer) {
                return short_date_format($employer->close_date);
            })
            ->editColumn('closure_type', function($employer) {
                return ($employer->closure_type == 1) ? 'Temporary' : 'Permanent';
            })
            ->editColumn('approved_date', function($booking) {
                return short_date_format($booking->approved_date);
            })
            ->rawColumns(['status']);
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query = $this->searchResult($this->input);

        return $this->query;
    }



    public function searchResult($data){

        $employers = new EmployerClosureRepository();
        $dormant_sub_type_cv_id = (new CodeValueRepository())->findIdByReference('ETCLDORMANT');
        //Search date range
        $this->query = $employers->query()->select(
            'employers.id as employer_id',
            'employers.name as employer_name',
            'employers.reg_no',
            'employers.tin',
            'employers.vote',
            'employers.doc',
            'regions.name as region_name',
            'employer_closures.close_date',
            'employer_closures.reason',
            'employer_closures.closure_type',
            DB::raw("CASE WHEN employer_closures.is_legacy = 1 THEN   coalesce(employer_closures.wf_done_date, employer_closures.created_at) ELSE employer_closures.wf_done_date END as approved_date")
        )
            ->join('employers', 'employers.id', 'employer_closures.employer_id')
            ->leftJoin('regions', 'regions.id', 'employers.region_id')
            ->leftJoin('office_zones', 'office_zones.id', 'regions.office_zone_id')
            ->whereBetween('employer_closures.close_date', [$data['from_date']. ' 00:00:00', $data['to_date'] . ' 23:59:59'])
            ->whereNull('employer_closures.deleted_at')
//            ->where('sub_type_cv_id','<>',$dormant_sub_type_cv_id )
            ->whereRaw("(sub_type_cv_id <> ? or sub_type_cv_id is null)", [$dormant_sub_type_cv_id]);
        //        with status



        if (isset($data['region_id'])) {
            if ($data['region_id'] > 0) {
                $this->query = $this->queryWithRegion($data);
            }
        }
        /* closure type */
        if (isset($data['type'])) {
            if ($data['type'] > 0) {
                $this->query = $this->queryWithClosureType($data);
            }
        }


        /* with office zone */
        if (isset($data['office_zone_id'])) {
            if ($data['office_zone_id'] > 0) {
                $this->query = $this->queryWithOfficeZone($data);
            }
        }

        return $this->query->withoutGlobalScopes()->whereNull("employers.deleted_at");

    }


    /*
     * query by closure type
     */
    public function queryWithClosureType($data)
    {
        return  $this->query->where('employer_closures.closure_type', $data['type']);
    }
    /**
     * @return mixed
     * Query with region
     */
    public function queryWithRegion($data)
    {
        return  $this->query->where('employers.region_id', $data['region_id']);
    }

    /**
     * @param $data
     * @return mixed
     * Query with office zone
     */
    public function queryWithOfficeZone($data)
    {
        return  $this->query->where('office_zones.id', $data['office_zone_id']);
    }


    /**
     * @return mixed
     * Find Total employers to be paid
     */
    public function findTotalEmployers()
    {
        $total = $this->query()->select(['id'])->count();
        return $total;
    }


    public function download($data){

        $employers = $this->searchResult($data)->get()->toArray();

        return Excel::create($this->filename(), function($excel) use ($employers) {
            $excel->sheet('mySheet', function($sheet) use ($employers)
            {
                $sheet->fromArray($employers);
            });
        })->download('csv');

    }




    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_name', 'name' => 'employers.name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'employers.reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'tin', 'name' => 'employers.tin', 'title' => trans('labels.general.tin_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'doc', 'name' => 'employers.doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => true, 'searchable' => true],
            ['data' => 'region_name', 'name' => 'regions.name', 'title' => trans('labels.general.region'), 'orderable' => false, 'searchable' => false],
            ['data' => 'close_date', 'name' => 'employer_closures.close_date', 'title' => 'Close Date', 'orderable' => true, 'searchable' => true],
            ['data' => 'approved_date', 'name' => 'approved_date', 'title' =>'Approved date',  'orderable' => false, 'searchable' => false],
            ['data' => 'closure_type', 'name' => 'employer_closures.closure_type', 'title' => 'Closure Type', 'orderable' => false, 'searchable' => false],
            ['data' => 'reason', 'name' => 'employer_closures.reason', 'title' => trans('labels.general.reason'), 'orderable' => true, 'searchable' => true],
        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'closedbusiness_' . time();
    }
}


