<?php
namespace App\DataTables\Report\Compliance;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\OnlineEmployerVerificationRepository;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class EmployerVerificationsDataTable extends DataTable
{
    protected $query;


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('source', function ($employer) {
                return ($employer->source == 1) ? 'WCF Branch' : 'Online';
            })
            ->addColumn('doc_formatted', function($employer) {
                return short_date_format($employer->doc);
            })

            ->editColumn('date_applied', function($employer) {
                return short_date_format($employer->date_applied);
            })

            ->editColumn('date_verified', function($employer) {
                return isset($employer->date_verified) ? short_date_format($employer->date_verified) : ' ';
            })
            ->editColumn('verification_status', function($employer) {
                return $this->getStatusLabelAttribute($employer->verification_status);

            })
            ->rawColumns(['verification_status']);
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query = $this->searchResult($this->input);

        return $this->query;
    }



    public function searchResult($data){

        $employers = new OnlineEmployerVerificationRepository();
        //Search date range
        $this->query = $employers->query()->select(
            'online_employer_verifications.id as verification_id',
            'employers.name as employer_name',
            'employers.reg_no',
            'employers.tin',
            'employers.vote',
            'employers.doc',
            'regions.name as region_name',
            'online_employer_verifications.status as verification_status',
            'online_employer_verifications.created_at as date_applied',
            'online_employer_verifications.wf_done_date as date_verified'
        )
            ->join('employers', 'online_employer_verifications.employer_id', 'employers.id')
            ->leftJoin('regions', 'regions.id', 'employers.region_id')
            ->leftJoin('office_zones', 'office_zones.id', 'regions.office_zone_id');



        if (isset($data['status_id'])) {
            if ($data['status_id'] >= 0) {
                $this->query = $this->queryWithStatus($data);
            }
        }else{
            /*If status no selected*/
            $this->query = $this->query->whereBetween('online_employer_verifications.created_at', [$data['from_date']. ' 00:00:00', $data['to_date'] . ' 23:59:59']);
        }


        return $this->query->withoutGlobalScopes()->whereNull("online_employer_verifications.deleted_at");

    }


    /*
     * query date range with status selected
     */
    public function queryWithStatus($data)
    {


        switch ($data['status_id']){

            case 0:
                /*pending*/
                $query = $this->query->where('online_employer_verifications.status', $data['status_id']);
                $this->query = $query->whereBetween('online_employer_verifications.created_at', [$data['from_date']. ' 00:00:00', $data['to_date'] . ' 23:59:59']);
                break;
            case 1:
                /*approved*/
                $query = $this->query->where('online_employer_verifications.status', $data['status_id']);
                $this->query = $query->whereBetween('online_employer_verifications.wf_done_date', [$data['from_date']. ' 00:00:00', $data['to_date'] . ' 23:59:59']);
                break;
            case 2:
                /*rejected*/
                $query = $this->query->where('online_employer_verifications.status', $data['status_id']);
                $this->query = $query->whereBetween('online_employer_verifications.wf_done_date', [$data['from_date']. ' 00:00:00', $data['to_date'] . ' 23:59:59']);
                break;
            case 3:
                /*initiated*/
                $query = $this->query->where('online_employer_verifications.status','<>',0)->where('online_employer_verifications.wf_done',0);;
                $this->query = $query->whereBetween('online_employer_verifications.created_at', [$data['from_date']. ' 00:00:00', $data['to_date'] . ' 23:59:59']);
                break;
        }

        return  $this->query;
    }


    // Get status label
    public function getStatusLabelAttribute($status)
    {

        if ($status == 0) {
            //if pending
            return "<span class='tag tag-warning warning_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.pending') . "'>" . '.' . "</span>";
        } elseif ($status == 1) {
            //If approved
            return "<span class='tag tag-success success_color_custom' data-toggle='tooltip' data-html='true' title='" . trans('labels.general.approved') . "'>" . '.' . "</span>";
        } elseif ($status == 2)  {
            //if rejected
            return "<span class='tag tag-danger danger_color_custom'  data-toggle='tooltip' data-html='true' title='" . trans('labels.general.rejected') . "'>" . '.' . "</span>";
        }elseif ($status == 4)  {
            //if initiated
            return "<span class='tag tag-info '  data-toggle='tooltip' data-html='true' title='" . trans('labels.general.initiated') . "'>" . '.' . "</span>";
        }elseif ($status == 5)  {
            //if pending online
            return "<span class='tag tag-primary '  data-toggle='tooltip' data-html='true' title='" . trans('labels.general.pending_online') . "'>" . '.' . "</span>";
        }
    }





    /**
     * @return mixed
     * Find Total employers to be paid
     */
    public function findTotalEmployers()
    {
        $total = $this->query()->select(['verification_id'])->count();
        return $total;
    }


    public function download($data){

        $employers = $this->searchResult($data)->get()->toArray();

        return Excel::create($this->filename(), function($excel) use ($employers) {
            $excel->sheet('mySheet', function($sheet) use ($employers)
            {
                $sheet->fromArray($employers);
            });
        })->download('csv');


    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/' + aData['verification_id'] + '/verification_profile/' ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_name', 'name' => 'employers.name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'employers.reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'tin', 'name' => 'employers.tin', 'title' => trans('labels.general.tin_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'vote', 'name' => 'employers.vote', 'title' => trans('labels.backend.member.vote'), 'orderable' => true, 'searchable' => true],
            ['data' => 'doc', 'name' => 'employers.doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => true, 'searchable' => true],
            ['data' => 'region_name', 'name' => 'regions.name', 'title' => trans('labels.general.region'), 'orderable' => false, 'searchable' => false],
            ['data' => 'verification_status', 'name' => 'online_employer_verifications.verification_status', 'title' => trans('labels.general.status'), 'orderable' => false, 'searchable' => false],
            ['data' => 'date_applied', 'name' => 'online_employer_verifications.created_at', 'title' => 'Date Applied', 'orderable' => true, 'searchable' => true],
            ['data' => 'date_verified', 'name' => 'online_employer_verifications.wf_done_date', 'title' => 'Date Verified', 'orderable' => true, 'searchable' => true],
        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employerverifications_' . time();
    }
}


