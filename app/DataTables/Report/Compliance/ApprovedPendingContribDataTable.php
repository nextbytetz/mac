<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Finance\Receivable\BookingRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;

class ApprovedPendingContribDataTable extends DataTable
{


    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('payment_date', function($receipt) {
                return short_date_format($receipt->payment_date);
            })
            ->editColumn('receipt_created_at', function($receipt) {
                return short_date_format($receipt->receipt_created_at);
            })
            ->editColumn('approved_date', function($receipt) {
                return short_date_format($receipt->approved_date);
            })
            ->editColumn('employer_name', function($receipt) {
                return $receipt->employer_name;
            })
            ->addColumn('reg_no', function($receipt) {
                return $receipt->reg_no;
            })
            ->editColumn('amount', function($receipt) {
                return number_2_format($receipt->amount);
            });


    }



    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query;
    }




    public function searchResult($data)
    {
//        $from_date = $data['from_date'];
//        $to_date = $data['to_date'];
        $this->query = DB::table('receipts')->distinct()
            ->select(
                'receipts.id as receipt_id',
                'employers.name as employer_name',
                'employers.reg_no',
                'receipts.rct_date as payment_date',
                'receipts.created_at as receipt_created_at',
                'receipts.amount',
                'receipts.rctno',
                'users.username',
                'wf_tracks.forward_date as approved_date'
                         )
            ->join('employers', 'employers.id', 'receipts.employer_id')
            ->join('wf_tracks', 'wf_tracks.resource_id', 'receipts.id')
            ->join('wf_definitions', 'wf_tracks.wf_definition_id', 'wf_definitions.id')
            ->join('wf_modules', 'wf_modules.id', 'wf_definitions.wf_module_id')
            ->join('users', 'users.id', 'wf_tracks.user_id')
//            ->join('wf_module_groups', 'wf_module_groups.id', 'wf_modules.wf_module_group_id')
            ->where('wf_modules.wf_module_group_id' ,5 )
            ->whereBetween('receipts.created_at', [$data['from_date']. ' 00:00:00', $data['to_date'] . ' 23:59:59'])
            ->whereNull("employers.deleted_at");
//            ->orderBy('id');


        /*Search for status*/
        if (isset($data['status_id'])) {
            if (($data['status_id'] > 0) ) {
                $this->query = $this->queryWithStatus($data);
            }
        }

        return $this->query->orderBy('receipts.id');
    }



    /*
 * query date range with status selected
 */
    public function queryWithStatus($data)
    {
        $status = $data['status_id'];
        /*Approved*/
        if($status == 1){
//            $this->query = $this->query->where('wf_definitions.level' ,3);
            $this->query = $this->query->where('wf_definitions.level' ,2)->where('wf_tracks.status', 1);
        }

        /*pending*/
        if($status == 2){
            $this->query = $this->query->where('wf_definitions.level' ,2)->where('wf_tracks.status', 0);
        }

        /*rejected*/
        if($status == 3){
            $this->query = $this->query->where('wf_definitions.level' ,2)->where('wf_tracks.status', 2);
        }

        return  $this->query;

    }

    /**
     * @param $data
     * @return mixed
     * Download
     */
    public function download($data){

        $receipts = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($receipts as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/finance/receipt/profile/' + aData['receipt_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employers.name', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'amount', 'name' => 'receipts.amount', 'title' => trans('labels.backend.table.amount_paid'), 'orderable' => false, 'searchable' => false],
            ['data' => 'rctno', 'name' => 'rctno', 'title' => 'Receipt Number', 'orderable' => false, 'searchable' => false],
            ['data' => 'payment_date', 'name' => 'rct_date', 'title' => trans('labels.backend.table.payment_date'), 'orderable' => false, 'searchable' => false],
            ['data' => 'receipt_created_at', 'name' => 'receipts.created_at', 'title' => 'Receipt Date', 'orderable' => false, 'searchable' => false],
            ['data' => 'approved_date', 'name' => 'wf_tracks.forward_date', 'title' => 'Approved Date', 'orderable' => false, 'searchable' => false],
            ['data' => 'username', 'name' => 'users.username', 'title' => 'Staff', 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'receiptapprovalstatus' . time();
    }
}
