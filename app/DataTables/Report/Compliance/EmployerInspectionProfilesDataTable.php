<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Organization\FiscalYearRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Operation\Compliance\EmployerInspectionProfilesReportRepository;


class EmployerInspectionProfilesDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
     return $this->datatables->of($this->query());

 }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {   
           
       $report_inspected = new EmployerInspectionProfilesReportRepository();
       $query = $report_inspected->returnInspectionDetails();
      

       return $this->applyScopes($query);
   }

   
   // public function searchResult($data)
   //  {
   //      //filter: Any change to this search query 
        
   //      $this->query= DB::table('main.employer_inspection_task ')->select('findings')
   //      ->join('inspection_task', 'employer_inspection_task.inspection_task_id', '=', 'inspection_task.id')
   //      ->where('findings');


   //      return $this->query;
   //  }


   public function html()
   {
    return $this->builder()
    ->columns($this->getColumns())
    ->ajax(route("backend.operation.report.employer_inspection_profile"))
    ->parameters([
        'dom'     => 'Bfrtip',
        'order'   => [[0, 'desc']],
        'buttons' => [
            'export',
            'reload',
            'colvis',
        ],
        'searching' => true,
        'serverSide' => true,
        'stateSave' => true,
        'paging' => true,
        'info' => true,
        'visible' => true,
    ]);
}


    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'task_type', 'name' => 'a.task_type', 'title' =>'Task Type', 'orderable' => true, 'searchable' => true],
            ['data' => 'employer', 'name' => 'a.name', 'title' => 'Employer', 'orderable' => true, 'searchable' => true],
            ['data' => 'allocated_staff', 'name' => 'e.firstname', 'title' =>'Allocated Staff', 'orderable' => true, 'searchable' => true],
            ['data' => 'attended', 'name' => 'employer_inspection_task.attended', 'title' => 'Attended', 'orderable' => true, 'searchable' => true],
            ['data' => 'stage', 'name' => 'b.name', 'title' =>'Stage', 'orderable' => true, 'searchable' => true],
            ['data' => 'region', 'name' => 'regions.name', 'title' => 'Region', 'orderable' => true, 'searchable' => true],
            ['data' => 'visit_date', 'name' => 'employer_inspection_task.visit_date', 'title' =>'Visit Date', 'orderable' => true, 'searchable' => true],
            ['data' => 'review_start_date', 'name' => 'employer_inspection_task.review_start_date', 'title' => 'Assessment Review Start Date', 'orderable' => true, 'searchable' => true],
            ['data' => 'review_end_date', 'name' => 'employer_inspection_task.review_end_date', 'title' =>'Assessment Review End Date', 'orderable' => true, 'searchable' => true],
         ['data' => 'outstanding_contribution', 'name' => 'outstanding_contribution', 'title' => 'Oustanding Contribution', 'orderable' => true, 'searchable' => true],
         ['data' => 'outstanding_interest', 'name' => 'outstanding_interest', 'title' =>'Outstanding Interest', 'orderable' => true, 'searchable' => true],
         ['data' => 'overpaid_contrib', 'name' => 'overpaid_contrib', 'title' => 'Overpaid Contribution', 'orderable' => true, 'searchable' => true],
         ['data' => 'total_outstanding_amount', 'name' => 'a.total_outstanding_amount', 'title' =>'Total Outstanding Amount', 'orderable' => true, 'searchable' => true],
         ['data' => 'outstanding_contribution_paid', 'name' => 'outstanding_contribution_paid', 'title' => 'Outstanding Contribution Paid', 'orderable' => true, 'searchable' => true],
         ['data' => 'outstanding_interest_paid', 'name' => 'outstanding_interest_paid', 'title' => 'Outstanding Interest Paid', 'orderable' => true, 'searchable' => true],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Inspection Report' . time();
    }
}
