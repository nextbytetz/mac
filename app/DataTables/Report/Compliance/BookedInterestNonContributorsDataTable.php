<?php

namespace App\DataTables\Report\Compliance;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;


class BookedInterestNonContributorsDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('contrib_month', function($booking) {
                return short_date_format($booking->contrib_month);
            })
            ->editColumn('booked_amount', function($booking) {
                return number_2_format($booking->booked_amount);
            })

            ->editColumn('interest_amount', function($booking) {
                return number_2_format($booking->interest_amount);
            })
            ->editColumn('doc', function($booking) {
                return short_date_format($booking->doc);
            });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query;
    }




    public function searchResult($data)
    {
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $this->query= DB::table('main.non_booked_booking_interests')->select('non_booked_bookings.employer_id','employers.doc','non_booked_booking_interests.late_months',   'employers.name as employer_name', 'employers.reg_no as employer_reg_no','non_booked_bookings.rcv_date as contrib_month', 'non_booked_bookings.amount as booked_amount', 'non_booked_booking_interests.amount as interest_amount')
            ->join('non_booked_bookings', 'non_booked_bookings.id', 'non_booked_booking_interests.non_booked_booking_id')
            ->join('employers', 'employers.id', 'non_booked_bookings.employer_id')
            ->where('non_booked_bookings.rcv_date', '>=', $from_date)->where('non_booked_bookings.rcv_date', '<=', $to_date)
            ->orderBy('non_booked_bookings.employer_id')->orderBy('non_booked_bookings.rcv_date');

        return $this->query;
    }


    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid
     */

    public function getTotalAmountPaid()
    {
        $amount_paid = 0;
        return  number_format($amount_paid , 2 , '.' , ',' );
    }


    /**Object of class stdClass could not be converted to string
     * @param $booking_id
     * @return string
     * Get Total Amount for this search
     */
    public function getTotalReceivable()
    {
        $receivable = $this->query()->sum('non_booked_booking_interests.amount');
        return  number_format($receivable , 2 , '.' , ',' );
    }



    public function getTotalBooked()
    {
        $total_booked_amount= $this->query()->sum('non_booked_bookings.amount');
        return  number_format($total_booked_amount , 2 , '.' , ',' );
    }

    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer_reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer_name', 'name' => 'employer_name', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'doc', 'name' => 'employers.doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'non_booked_bookings.rcv_date', 'title' => trans('labels.backend.table.booking.rcv_date')],
                   ['data' => 'booked_amount', 'name' => 'non_booked_bookings.amount', 'title' => trans('labels.backend.table.booked_amount'), 'orderable' => false, 'searchable' => false],
            ['data' => 'late_months', 'name' => 'late_months', 'title' => 'Late months'],
            ['data' => 'interest_amount', 'name' => 'non_booked_bookings.amount', 'title' => 'Interest Amount', 'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'bookedinterestsnoncontributors' . time();
    }
}
