<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Finance\FinCodeGroupRepository;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;

class InterestRaisedPaidVarianceDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('miss_month', function($booking_interest) {
                return $booking_interest->miss_month_formatted;
            })
            ->addColumn('employer', function($booking_interest) {
                return $booking_interest->booking->employer->name;
            })
            ->addColumn('reg_no', function($booking_interest) {
                return $booking_interest->booking->employer->reg_no;
            })
            ->addColumn('interest_raised', function($booking_interest) {
                return $booking_interest->amount_formatted;
            })
            ->addColumn('amount_paid', function($booking_interest) {
                return $this->getAmountPaid($booking_interest->booking_id);
            })
            ->addColumn('pay_status', function($booking_interest) {
                return $booking_interest->contribution_status_label;
            })
            ->rawColumns(['pay_status']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $booking_interests = new BookingInterestRepository();
        $query = $booking_interests->query()->where('miss_month', '>=', $from_date)->where('miss_month', '<=', $to_date )->where('hasreceipt',1)->has('booking');
        return $this->applyScopes($query);
    }


    /**
     * @return string
     * Get Total Interest Raised
     */
    public function getTotalInterest()
    {
        $total_interest_amount= $this->query()->sum('amount');
        return  number_format($total_interest_amount , 2 , '.' , ',' );
    }



    /**
     * @return mixed
     * get no of receipts receipted by the user
     */
    public function getAmountPaid($booking_id)
    {
        $receipt_codes = new ReceiptCodeRepository();
        $amount_paid = $receipt_codes->getTotalInterestPaidForBookingId($booking_id);
        return  number_format( $amount_paid , 2 , '.' , ',' );
    }

    /**
     * @return string
     * Get Total amount paid
     */
    public function getTotalAmountPaid()
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $receipt_codes = new ReceiptCodeRepository();
        $fin_code_groups = new FinCodeGroupRepository();
        $amount_paid = $receipt_codes->query()->whereHas('booking', function ($query) use($from_date,$to_date){
            $query->whereHas('bookingInterest', function($query) use($from_date,$to_date) {
                $query->where('miss_month', '>=', $from_date)->where('miss_month', '<=', $to_date);
            });
        })->whereHas('receipt', function($query) {
            $query->where('iscancelled', 0)->where('isdishonoured', 0);
        })->whereHas('finCode', function
        ($query)  use ($fin_code_groups)  {
            $query->where('fin_code_group_id','=', $fin_code_groups->getInterestinCodeGroupId());
        })->sum('amount');
        return  number_format($amount_paid , 2 , '.' , ',' );
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'footerCallback'=> "function ( row, data, start, end, display ) {
        var api = this.api();
        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {

            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                i : 0;

            };
               // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                
               // Total over all pages

                if (api.column(4).data().length){
                var total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                return intVal(a) + intVal(b);
                } ) }
                else{ total = 0};

     
 // Total PAID over all pages
                if (api.column(5).data().length){
                var total_paid = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                return intVal(a) + intVal(b);
                } ) }
                else{ total_paid = 0};

    function commaSeparateNumber(val){
        while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                    }
                    return val ;
                }
                
        // Update footer
          $( api.column(3).footer() ).html( 'PAGE TOTAL'  );
        $( api.column(4).footer() ).html( ''+commaSeparateNumber(total.toFixed(2)  )  );
          $( api.column(5).footer() ).html( ''+commaSeparateNumber(total_paid.toFixed(2))   );
    }"


            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'employer', 'name' => 'employer', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => false, 'searchable' => false],
            ['data' => 'miss_month', 'name' => 'miss_month', 'title' => trans('labels.backend.table.booking.rcv_date')],
            ['data' => 'late_months', 'name' => 'late_months', 'title' => trans('labels.backend.table.booking.late_months')],
            ['data' => 'interest_raised', 'name' => 'interest_raised', 'title' => trans('labels.backend.finance.receipt.interest'), 'orderable' => false, 'searchable' => false],
            ['data' => 'amount_paid', 'name' => 'amount_paid', 'title' => trans('labels.backend.table.amount_paid'), 'orderable' => false, 'searchable' => false],
            ['data' => 'pay_status', 'name' => 'pay_status', 'title' => 'Contribution Status', 'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'interstraisedamountpaid' . time();
    }
}
