<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

class UnregisteredEmployersDataTable extends DataTable
{

    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())

            ->addColumn('doc_formatted', function($employer) {
                return $employer->date_of_commencement_formatted;
            });


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);

        return $this->query;
    }


    public function searchResult($data){

        $employers = new UnregisteredEmployerRepository();

        //Search date range
        $this->query = $employers->query()->select(
            'unregistered_employers.id as employer_id',
            'unregistered_employers.name as employer_name',
            'unregistered_employers.doc',
            'unregistered_employers.tin',
            'regions.name as region_name'
//            'unregistered_employers.source'
        )
            ->leftJoin('regions', 'regions.id', 'unregistered_employers.region_id')
            ->leftJoin('office_zones', 'office_zones.id', 'regions.office_zone_id')
            ->where('unregistered_employers.is_registered', 0);



        if (isset($data['region_id'])) {
            if ($data['region_id'] > 0) {
                $this->query = $this->queryWithRegion($data);
            }
        }

        /* with office zone */
        if (isset($data['office_zone_id'])) {
            if ($data['office_zone_id'] > 0) {
                $this->query = $this->queryWithOfficeZone($data);
            }
        }

        return $this->query->withoutGlobalScopes()->whereNull("unregistered_employers.deleted_at");

    }
    /**
     * @return mixed
     * Query with region
     */
    public function queryWithRegion($data)
    {
        return  $this->query->where('region_id', $data['region_id']);

    }


    /*With office zone*/
    public function queryWithOfficeZone($data)
    {
        return  $this->query->where('office_zones.id', $data['office_zone_id']);
    }


    /**
     * @return mixed
     * Find Total unregistered employers
     */

    public function findTotalEmployers()
    {
        $total = $this->query()->count();
        return $total;

    }


    public function download($data)
    {

        $employers = $this->searchResult($data)->get()->toArray();

        return Excel::create($this->filename(), function ($excel) use ($employers) {
            $excel->sheet('mySheet', function ($sheet) use ($employers) {
                $sheet->fromArray($employers);
            });
        })->download('csv');
    }



        /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/unregistered_employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'employer_name', 'name' => 'unregistered_employers.name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'tin', 'name' => 'unregistered_employers.tin', 'title' => trans('labels.general.tin_no'), 'orderable' => true, 'searchable' => true],

            ['data' => 'doc_formatted', 'name' => 'unregistered_employers.doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => true, 'searchable' => true],

            ['data' => 'region_name', 'name' => 'regions.name', 'title' => trans('labels.general.region'), 'orderable' => false, 'searchable' => false],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'unregisteredemployerreport_' . time();
    }
}
