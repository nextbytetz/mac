<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Yajra\Datatables\Services\DataTable;

class EmployerOverallReportDataTable extends DataTable
{

    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('region', function ($employer) {
                return ($employer->district_id) ? $employer->district->region->name : (($employer->region_id) ? $employer->region->name : ' ');
            })
            ->addColumn('doc_formatted', function($employer) {
                return $employer->date_of_commencement_formatted;
            })
            ->addColumn('employees', function($employer) {
                return $employer->employeesCount();
            })
            ->addColumn('date_registered', function($employer) {
                return $employer->created_at_formatted;
            });



    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $employers = new EmployerRepository();
        //Search date range
        $this->query = $employers->query()->whereNull('duplicate_id');

        if (isset($this->input['region_id'])) {
            if ($this->input['region_id'] > 0) {
                $this->query = $this->queryWithRegion();
            }
        }

        /* Employer Category id */
        if (isset($this->input['type'])) {
            if ($this->input['type'] > 0) {
                $this->query = $this->queryEmployerCategory();
            }
        }


        /*        with Business */
        if (isset($this->input['business_id'])) {
            if ($this->input['business_id'] > 0) {
                $this->query = $this->queryWithBusiness();
            }
        }


        /*        with status */
        if (isset($this->input['status_id'])) {
            if ($this->input['status_id'] > 0) {
                $this->query = $this->queryWithStatus();
            }
        }

        return $this->query;
    }


    /*
     * query date range with payment type selected
     */
    public function queryEmployerCategory()
    {
        return  $this->query->where('employer_category_cv_id', $this->input['type']);

    }


    /**
     * @return mixed
     * Query with region
     */
    public function queryWithRegion()
    {
        return  $this->query->where('region_id', $this->input['region_id']);

    }

    /*
* query date range with with business selected
*/
    public function queryWithBusiness()
    {
        return $this->query->whereHas('sectors', function ($query) {
            $query->where('business_sector_cv_id',$this->input['business_id']);

        });

    }
    /*
* query date range with with status selected
*/
    public function queryWithStatus()
    {
        return $this->query->where('employer_status', $this->input['status_id'])->where('approval_id', 1);

    }



    /**
     * @return mixed
     * Find Total employers
     */

    public function findTotalEmployers()
    {
        $total = $this->query()->select(['id'])->count();
        return $total;

    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'date_registered', 'name' => 'created_at', 'title' => trans('labels.general.date_registered'), 'orderable' => true, 'searchable' => true],
            ['data' => 'doc_formatted', 'name' => 'doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => true, 'searchable' => true],
            ['data' => 'region', 'name' => 'region', 'title' => 'Region', 'orderable' => false, 'searchable' => false],
            ['data' => 'tin', 'name' => 'tin', 'title' => trans('labels.general.tin_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'employees', 'name' => 'employees', 'title' => 'Number of employees', 'orderable' => false, 'searchable' => false],
            ['data' => 'phone', 'name' => 'phone', 'title' => trans('labels.general.phone'), 'orderable' => false, 'searchable' => false, 'visible' => false],
            /*['data' => 'telephone', 'name' => 'telephone', 'title' => trans('labels.general.telephone'), 'orderable' => false, 'searchable' => false],*/
            ['data' => 'email', 'name' => 'email', 'title' => trans('labels.general.email'), 'orderable' => false, 'searchable' => false, 'visible' => false],
            ['data' => 'box_no', 'name' => 'box_no', 'title' => trans('labels.general.po_box'), 'orderable' => false, 'searchable' => false],
            ['data' => 'district_name', 'name' => 'district_name', 'title' => trans('labels.general.district'), 'orderable' => false, 'searchable' => false],
            /*['data' => 'street', 'name' => 'street', 'title' => trans('labels.general.street'), 'orderable' => false, 'searchable' => false],*/
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employerregistrationreport_' . time();
    }
}
