<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Operation\Compliance\Member\EmployeeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Services\DataTable;

class EmployeesByEmployerDataTable extends DataTable
{

    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());



    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $employee = new EmployeeRepository();
        $this->query = $employee->query()->select([
            'memberno',
            DB::raw("employees.id as id"),
            DB::raw("concat_ws(' ', firstname, coalesce(middlename, ''), lastname) as fullname"),
            DB::raw("firstname"),
            DB::raw("middlename"),
            DB::raw("lastname"),
            DB::raw("dob"),
            DB::raw("sex"),
            DB::raw("coalesce(code_values.name, employees.emp_cate) as category"),
            DB::raw("coalesce(employee_employer.job_title, employees.job_title) as job_title"),
        ])
            ->join("employee_employer", "employees.id", "=", "employee_employer.employee_id")
            ->leftJoin("code_values", "employee_employer.employee_category_cv_id", "=", "code_values.id")->orderBy('employees.firstname', 'asc') ;




        //        with employer
        if (isset($this->input['employer_id'])) {
            if (($this->input['employer_id'] > 0)) {
                $this->query = $this->queryWithEmployer();
            }
        }





        return $this->query;
    }


    /*
     * query date range with status selected
     */
    public function queryWithEmployer()
    {

        return  $this->query->where('employee_employer.employer_id', $this->input['employer_id']);

    }


    /**
     * @return mixed
     * Find Total employers to be paid
     */

    public function findTotalEmployees()
    {

        $total = $this->query()->count();
        return $total;

    }



    public function findEmployerName()
    {
        $employers = new EmployerRepository();
        $employer_id =  isset($this->input['employer_id']) ?   $this->input['employer_id'] : 0;
        $employer_name = null;
        if ($employer_id > 0){
            $employer = $employers->findOrThrowException($employer_id);
            $employer_name = $employer->name;
        }

        return $employer_name;

    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis' ],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employee/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'memberno', 'name' => 'memberno', 'title' => trans('labels.backend.member.memberno'), 'orderable' => true, 'searchable' => true],
            ['data' => 'fullname', 'name' => 'firstname', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'dob', 'name' => 'dob', 'title' => trans('labels.general.dob'), 'orderable' => true, 'searchable' => true],
            ['data' => 'sex', 'name' => 'sex', 'title' => trans('labels.general.gender'), 'orderable' => true, 'searchable' => false],
            ['data' => 'job_title', 'name' => 'employee_employer.job_title ', 'title' => trans('labels.general.job_title'), 'orderable' => true, 'searchable' => false],
            ['data' => 'category', 'name' => 'category', 'title' => trans('labels.backend.member.employee_category'), 'orderable' => false, 'searchable' => false],



        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employeesbyemployer_' . time();
    }
}
