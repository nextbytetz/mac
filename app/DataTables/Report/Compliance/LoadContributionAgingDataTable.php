<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Access\UserRepository;
use Carbon\Carbon;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\FinCodeGroupRepository;

class LoadContributionAgingDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('payer', function($receipt_code) {
                return $receipt_code->receipt->payer;
            })
            ->addColumn('contrib_month', function($receipt_code) {
                return $receipt_code->contrib_month;
            })

            ->addColumn('receipt_date', function($receipt_code) {
                return $receipt_code->created_at_formatted;
            })
            ->addColumn('loaded_date', function($receipt_code) {
                return ($receipt_code->contributions()->count()) ? $receipt_code->contributions->first()->created_at_formatted : ' ';
            })
            ->addColumn('age', function($receipt_code) {
                return ($receipt_code->contributions()->count()) ?  Carbon::parse($receipt_code->contributions->first()->created_at_formatted)->diffInDays(Carbon::parse($receipt_code->created_at_formatted)) + 1 : Carbon::now()->diffInDays(Carbon::parse($receipt_code->created_at_formatted)) + 1 ;
            })
            ->addColumn('status', function($receipt_code) {
                return $receipt_code->receipt->complete_status_label;
            })
            ->rawColumns(['status'])
            ;
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $receipt_codes = new ReceiptCodeRepository();
        $fin_code_groups = new FinCodeGroupRepository();
        $contributions = $receipt_codes->query()->whereHas('receipt', function ($query) {
            $query->where('iscancelled','=', 0)->where('isdishonoured','=',0);
        })->whereHas('finCode', function($query) use ($fin_code_groups){$query->where('fin_code_group_id','=',  $fin_code_groups->getContributionFinCodeGroupId());
        })->where('grouppaystatus',0)->where('created_at', '>=', $from_date . ' 00:00:00')->where('created_at', '<=', $to_date . ' 23:59:59');

        return $this->applyScopes($contributions);
    }


    /**
     * @return mixed
     * get no of receipts receipted by the user
     */
    public function no_of_contribution_loaded($user_id)
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $users = new UserRepository();
        $contribution = new ContributionRepository();
        $user = $users->findOrThrowException($user_id);
        $no_of_contribution_loaded = 0;

        if (!is_null($from_date) && !is_null($from_date) ){
            $no_of_contribution_loaded = $contribution->query()->withoutGlobalScopes()->distinct()->select('receipt_code_id')->where('user_id', $user_id)->where('created_at', '>=', $from_date . ' 00:00:00')->where('created_at', '<=', $to_date . ' 23:59:59')->count();

        }
        return $no_of_contribution_loaded;
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
//                'buttons' => [  'extend' => 'print',
//                    'exportOptions' => [
//                        'modifier'=> [
//
//                            'selected' => true
//                        ]
//
//                    ], 'print' ],

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'payer', 'name' => 'payer', 'title' => trans('labels.backend.finance.receipt.payer'),'orderable' => false, 'searchable' => false],
            ['data' => 'contrib_month', 'name' => 'contrib_month', 'title' => trans('labels.backend.finance.receipt.contrib_month'), 'orderable' => false, 'searchable' => false],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.backend.finance.receipt.amount'),'orderable' => false, 'searchable' => false],
            ['data' => 'receipt_date', 'name' => 'receipt_date', 'title' => trans('labels.backend.finance.receipt.captured_date'),'orderable' => false, 'searchable' => false],
            ['data' => 'loaded_date', 'name' => 'loaded', 'title' => trans('labels.backend.report.loaded_date'),'orderable' => false, 'searchable' => false],
            ['data' => 'age', 'name' => 'age', 'title' => trans('labels.backend.report.aging'),'orderable' => false, 'searchable' => false],
            ['data' => 'status', 'name' => 'status', 'title' => trans('labels.backend.table.status'),'orderable' => false, 'searchable' => false],
//            ['data' => 'no_of_contribution_loaded', 'name' => 'no_of_contribution_loaded', 'title' => trans('labels.backend.table.no_of_loaded_contribution'), 'orderable' => false, 'searchable' => false],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'loadcontributionaging' . time();
    }
}
