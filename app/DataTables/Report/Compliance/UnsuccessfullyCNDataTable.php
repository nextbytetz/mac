<?php

namespace App\DataTables\Report\Compliance;

use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UnsuccessfullyCNDataTable extends DataTable
{
    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
       return $this->datatables->of($this->query())->editColumn('bill_status', function($query) 
       {
                if($query->bill_status==2 && $query->expire_date<Carbon::now())
                    {
              return 'Expired';

                    }elseif($query->bill_status==4){

          return 'Cancelled';
           }elseif ($query->bill_status==2 && $query->expire_date>Carbon::now()) {
            return 'Pending';
             }
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

    $query= DB::table('portal.payments')->select('payments.control_no','payments.payer_name','bills.bill_amount','bills.bill_status','bills.member_count','bills.billed_item','bills.created_at','bills.expire_date','users.name as generatedby','bills.mobile_number as contact')
    ->join('portal.bills','payments.bill_id','=','bills.bill_no')
    ->join('portal.users','bills.user_id','=','users.id')
    ->where('payments.created_at', '>=', $this->from_date . ' 00:00:00')->where('payments.created_at', '<=', $this->to_date . ' 23:59:00')
    ->where('paid_amount','=',null);


    return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'buttons' => ['csv', 'excel','print', 'reset', 'reload'],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'control_no' => ['data' => 'control_no', 'name' => 'payments.control_no'],
            'payer_name' =>[ 'data' => 'payer_name', 'name' => 'payments.payer_name'],
            'bill_amount' => ['data' => 'bill_amount', 'name' => 'bills.bill_amount'],
            'bill_status' => ['data' => 'bill_status', 'name' => 'bills.bill_status'],
            'member_count' => ['data' => 'member_count', 'name' => 'bills.member_count'],
            'bill_description' => ['data' => 'billed_item', 'name' => 'bills.billed_item'],
            'created_at' => ['data' => 'created_at', 'name' => 'bills.created_at'],
            'Expire Date' => ['data' => 'expire_date', 'name' => 'bills.expire_date'],
            'generatedby' => ['data' => 'generatedby', 'name' => 'users.name'],
            'contact' => ['data' => 'contact', 'name' => 'bills.mobile_number'],
        ];
    }



    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'unsuccessfullycndatatable_' . time();
    }
}
