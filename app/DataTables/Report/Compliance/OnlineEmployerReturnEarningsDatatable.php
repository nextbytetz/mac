<?php

namespace App\DataTables\Report\Compliance;

use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OnlineEmployerReturnEarningsDatatable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
      $query = DB::table("online_return_earnings")->select()->OrderBy('submitted_date','desc');
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'info' => true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href ='#';
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
         return [
            'reg_no' => ['data' => 'reg_no', 'name' => 'online_return_earnings.reg_no'],
            'employer_name' =>[ 'data' => 'employer_name', 'name' => 'online_return_earnings.name'],
            'payroll_number' => ['data' => 'payroll_id', 'name' => 'online_return_earnings.payroll_id'],
            'uploaded_date' => ['data' => 'uploaded_date', 'name' => 'online_return_earnings.uploaded_date'],
            'submission_status' => ['data' => 'submission_status', 'name' => 'online_return_earnings.submission_status'],
            'submitted_date' => ['data' => 'submitted_date', 'name' => 'online_return_earnings.submitted_date'],
            'user_submitted' => ['data' => 'user_submitted', 'name' => 'online_return_earnings.user_submitted'],
            'email' => ['data' => 'email', 'name' => 'online_return_earnings.email'],
            'mobile_phone' => ['data' => 'phone', 'name' => 'online_return_earnings.phone'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'OnlineReturnofEarnings_2019_2020' . time();
    }
}
