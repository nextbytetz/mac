<?php

namespace App\DataTables\Report\Compliance;

use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RegisteredNotContributingDataTable extends DataTable
{

    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query= DB::table('main.employers')->select('employers.id as employer_id' , 'employers.reg_no as registration_number','employers.name as employername','employers.doc as date_commenced','employers.tin','employers.phone','employers.telephone','employers.email','regions.name as region','employers.street')
            ->leftJoin('main.regions','employers.region_id','=','regions.id')
            ->leftJoin('main.receipts','employers.id','=','receipts.employer_id')
            ->leftJoin('main.legacy_receipts','employers.id','=','legacy_receipts.employer_id')
            ->where('employers.created_at', '>=', $this->from_date . ' 00:00:00')->where('employers.created_at', '<=', $this->to_date . ' 23:59:00')
            ->where('approval_id','=',1)
            ->where('duplicate_id','=',null)
            ->where('legacy_receipts.id','=',null)
            ->where('receipts.id','=',null)
            ->orderBy('employers.name', 'asc');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->parameters([
                'dom'     => 'Bfrtip',
                'buttons' => ['csv', 'excel','print', 'reset', 'reload'],

                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'registration_number' => ['data' => 'registration_number', 'name' => 'employers.reg_no'],
            'employername' =>[ 'data' => 'employername', 'name' => 'employers.name'],
            'date_commenced' => ['data' => 'date_commenced', 'name' => 'employers.doc'],
            'tin' => ['data' => 'tin', 'name' => 'employers.tin'],
            'phone' => ['data' => 'phone', 'name' => 'employers.phone'],
            'telephone' => ['data' => 'telephone', 'name' => 'employers.telephone'],
            'email' => ['data' => 'email', 'name' => 'employers.email'],
            'region' => ['data' => 'region', 'name' => 'regions.name'],
            'street' => ['data' => 'street', 'name' => 'employers.street'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'registerednotcontributing_report' . time();
    }


    /**
     * @return mixed
     * Old
     */
    public function oldQuery()
    {
        $query= DB::table('main.employers')->select('employers.reg_no as registration_number','employers.name as employername','employers.doc as date_commenced','employers.tin','employers.phone','employers.telephone','employers.email','regions.name as region','employers.street')
            ->join('main.regions','employers.region_id','=','regions.id')
            ->where('employers.created_at', '>=', $this->from_date . ' 00:00:00')->where('employers.created_at', '<=', $this->to_date . ' 23:59:00')
            ->where('approval_id','=',1)
            // ->whereNotIn() figure out to exclude contribution receipts
            // ->whereNOTIn('employers.id',function($data){
            //            $data->select('employer_id')->from('main.receipts')->distinct('employer_id');
            //         })
            ->where('duplicate_id','=',null);



        return $this->applyScopes($query);
    }

}
