<?php

namespace App\DataTables\Report\Compliance;

use App\Repositories\Backend\Access\PermissionRepository;
use App\Repositories\Backend\Access\UserRepository;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionRepository;

class StaffContribCollectionPerformance extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('name', function($user) {
                return $user->name;
            })
            ->addColumn('no_of_contribution_loaded', function($user) {
                return $this->no_of_contribution_loaded($user->id);
            });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $users = new UserRepository();
        $query = $users->query()->whereHas('contributions', function ($query) {
            $query->withoutGlobalScopes();
        });
        return $this->applyScopes($query);
    }


    /**
     * @return mixed
     * get no of receipts receipted by the user
     */
    public function no_of_contribution_loaded($user_id)
    {
        $from_date = $this->from_date;
        $to_date = $this->to_date;
        $users = new UserRepository();
        $contribution = new ContributionRepository();
        $user = $users->findOrThrowException($user_id);
        $no_of_contribution_loaded = 0;

        if (!is_null($from_date) && !is_null($from_date) ){
            $no_of_contribution_loaded = $contribution->query()->withoutGlobalScopes()->distinct()->select('receipt_code_id')->where('user_id', $user_id)->where('created_at', '>=', $from_date . ' 00:00:00')->where('created_at', '<=', $to_date . ' 23:59:59')->count();

        }
        return $no_of_contribution_loaded;
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
//                'buttons' => [  'extend' => 'print',
//                    'exportOptions' => [
//                        'modifier'=> [
//
//                            'selected' => true
//                        ]
//
//                    ], 'print' ],

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'username', 'name' => 'username', 'title' => trans('labels.backend.user.username'),'orderable' => true, 'searchable' => true],
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.fullname'),'orderable' => false, 'searchable' => false],
            ['data' => 'no_of_contribution_loaded', 'name' => 'no_of_contribution_loaded', 'title' => trans('labels.backend.table.no_of_loaded_contribution'), 'orderable' => false, 'searchable' => false],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'staffloadcontributionperformance' . time();
    }
}
