<?php

namespace App\DataTables\Report\Compliance\DebtManagement;

use App\Repositories\Backend\Operation\Claim\Portal\ClaimActivityRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

/*Performance*/
class StaffMonthlyCollectionDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('collection_month', function($collection) {
                return Carbon::parse($collection->collection_month)->format('M Y');
            })
            ->editColumn('no_of_employers', function($collection) {
                return number_0_format($collection->no_of_employers);
            })
            ->editColumn('arrears_before', function($collection) {
                return number_2_format($collection->arrears_before);
            })
            ->editColumn('arrears_collection', function($collection) {
                return number_2_format($collection->arrears_collection) . ' - (' . number_2_format($collection->performance_before) . '%)';
            })
            ->editColumn('receivable_current', function($collection) {
                return number_2_format($collection->receivable_current);
            })
            ->editColumn('collection_current', function($collection) {
                return number_2_format($collection->collection_current) . ' - (' . number_2_format($collection->performance_current) . '%)';
            })
            ->editColumn('overall_performance', function($collection) {
                return number_2_format($collection->overall_performance) . ' %';
            });


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query;
    }




    public function searchResult($data)
    {
        $from_date = Carbon::parse($data['from_date']);
        $this->query= DB::table('main.staff_employer_collection_performances as s')->select(
            DB::raw("concat_ws(' ', users.firstname, users.middlename, users.lastname) as fullname  ")
            , 's.no_of_employers',  's.percent_online_employers', 's.arrears_before','s.arrears_collection', 's.receivable_current', 's.collection_current', 's.collection_month',
            DB::raw(" CASE WHEN s.arrears_before > 0 THEN  ((s.arrears_collection * 100) / s.arrears_before) ELSE 0 END as performance_before"),
            DB::raw(" CASE WHEN s.receivable_current > 0 THEN ((s.collection_current * 100) / s.receivable_current) ELSE 0 END as performance_current"),
            DB::raw(" CASE WHEN (s.arrears_before > 0 and s.receivable_current > 0)  THEN (( (s.arrears_collection  + s.collection_current)* 100) / (s.arrears_before + s.receivable_current)) ELSE 0 END as overall_performance"))
            ->join('users', 'users.id', 's.user_id')
            ->whereNull('s.deleted_at')
            ->where('isintern', $data['isintern'])
            ->whereMonth('s.collection_month','=',$from_date->format('m'))->whereYear('s.collection_month','=',$from_date->format('Y') )->orderBy('overall_performance', 'desc');


        return $this->query;
    }


    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid
     */

    public function getTotalAmountPaid()
    {
        $amount_paid = $this->query()->sum("current_amount");
        return  number_format($amount_paid , 2 , '.' , ',' );
    }


    /**
     * @param $booking_id
     * @return string
     * Get Total Amount Paid - previous months
     */

    public function getTotalAmountPaidPrev()
    {
        $amount_paid = $this->query()->sum("prev_amount");
        return  number_format($amount_paid , 2 , '.' , ',' );
    }


    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
//                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'fullname', 'name' => 'users.firstname', 'title' => 'Staff', 'orderable' => true, 'searchable' => true],
            ['data' => 'no_of_employers', 'name' => 's.no_of_employers', 'title' => 'no_of_employers', 'orderable' => true, 'searchable' => true],
            ['data' => 'percent_online_employers', 's.percent_online_employers' => 'doc', 'title' => 'Percent Online Employers', 'orderable' => true, 'searchable' => true],
            ['data' => 'arrears_before', 'name' => 's.arrears_before', 'title' => 'Arrears Before',  'orderable' => true, 'searchable' => true],
            ['data' => 'arrears_collection', 'name' => 's.arrears_collection', 'title' => 'Arrears Collection',  'orderable' => true, 'searchable' => true],
            ['data' => 'receivable_current', 'name' => 's.receivable_current', 'title' => 'Receivable Current',  'orderable' => true, 'searchable' => true],
            ['data' => 'collection_current', 'name' => 's.collection_current', 'title' => 'Collection Current', 'orderable' => true, 'searchable' => true],
            ['data' => 'overall_performance', 'name' => 'overall_performance', 'title' => 'Overall Performances', 'orderable' => true, 'searchable' => false],
            ['data' => 'collection_month', 'name' => 's.collection_month', 'title' => 'Collection Month', 'orderable' => true, 'searchable' => true],
            ['data' => 'fullname', 'name' => 'users.lastname', 'title' => 'Staff', 'orderable' => true, 'searchable' => true, 'visible' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'staffmonthlycollections' . time();
    }
}
