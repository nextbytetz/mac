<?php

namespace App\DataTables\Report\Compliance\DebtManagement;

use App\Repositories\Backend\Organization\FiscalYearRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;


class StaffEmployerFollowUpsDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query())
            ->editColumn('date_of_follow_up', function($follow_up) {
                return short_date_format($follow_up->date_of_follow_up);
            })
            ->editColumn('remark', function($follow_up) {
                return truncateString($follow_up->remark, 150);
            });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->searchResult($this->input);
        return $this->query;
    }




    public function searchResult($data)
    {
        //REMINDER: Any change to this search query need to update GETNOEMPLOYERS() function
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $this->query= DB::table('main.staff_employer_follow_ups as f')->select(
            DB::raw("concat_ws(' ', users.firstname, users.middlename, users.lastname) as fullname  "), 'employers.id as reg_no',
            'employers.name as employer_name', 'follow.name as follow_up_type',  'feed.name as feedback_type', 'f.remark','f.contact', 'f.contact_person','f.date_of_follow_up')
            ->join('users', 'users.id', 'f.user_id')
            ->join('staff_employer', 'staff_employer.id', 'f.staff_employer_id')
            ->join('code_values as feed', 'feed.id', 'f.feedback_cv_id')
            ->join('code_values as follow', 'follow.id', 'f.follow_up_type_cv_id')
            ->join('employers', 'employers.id', 'staff_employer.employer_id')
            ->where('f.date_of_follow_up', '>=', $from_date)->where('f.date_of_follow_up', '<=', $to_date);

        /*query with feedback*/

        if(isset($data['feedback_id'])){

            $this->query = $this->queryWithFeedback($data);
        }

        /*query with user*/
        if(isset($data['user_id'])){
            $this->query = $this->queryWithUser($data);
        }
        return $this->query;
    }

    /*Search query with feedback*/
    public function queryWithFeedback($data)
    {
        $feedback_id = $data['feedback_id'];
        return $this->query->where('f.feedback_cv_id', $feedback_id);
    }

    /*Search query with user*/
    public function queryWithUser($data)
    {
        $user_id = $data['user_id'];
        return $this->query->where('f.user_id', $user_id);
    }



    public function download($data){

        $bookings = $this->searchResult($data)->get()->toArray();
        $results = array();
        foreach ($bookings as $result) {

            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
//                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'fullname', 'name' => 'users.firstname', 'title' =>'Staff', 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'employers.id', 'title' => 'Employer Reg no', 'orderable' => true, 'searchable' => true],
            ['data' => 'employer_name', 'name' => 'employers.name', 'title' => trans('labels.backend.finance.receipt.employer'), 'orderable' => true, 'searchable' => true],
            ['data' => 'follow_up_type', 'name' => 'follow.name', 'title' => 'Follow up type', 'orderable' => true, 'searchable' => true],
            ['data' => 'feedback_type', 'name' => 'feed.name', 'title' => 'Feedback', 'orderable' => true, 'searchable' => true],
            ['data' => 'remark', 'name' => 'f.remark', 'title' => 'Remark', 'orderable' => true, 'searchable' => true],
            ['data' => 'contact', 'name' => 'f.contact', 'title' => 'Contact', 'orderable' => true, 'searchable' => true],
            ['data' => 'contact_person', 'name' => 'f.contact_person', 'title' => 'Contact Person', 'orderable' => true, 'searchable' => true],
            ['data' => 'date_of_follow_up', 'name' => 'f.date_of_follow_up', 'title' => 'Date of Follow up', 'orderable' => true, 'searchable' => true],
            ['data' => 'fullname', 'name' => 'users.lastname', 'title' =>'Staff', 'orderable' => true, 'searchable' => true, 'visible' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'staffemployerfollowups' . time();
    }
}
