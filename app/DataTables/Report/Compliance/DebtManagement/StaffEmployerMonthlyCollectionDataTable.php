<?php

namespace App\DataTables\Report\Compliance\DebtManagement;

use App\Exceptions\GeneralException;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Operation\Claim\Portal\ClaimActivityRepository;
use App\Repositories\Backend\Operation\Compliance\Member\StaffEmployerRepository;
use App\Repositories\Backend\Organization\FiscalYearRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Services\DataTable;

/*Collection i.e. Employers contributed/not contributed*/
class StaffEmployerMonthlyCollectionDataTable extends DataTable
{

    protected  $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        $receipt_code_repo = new ReceiptCodeRepository();
        $months_in_range = $this->getMonthsInDateRange($this->input);
        return $this->datatables->of($this->query()->orderBy('se.user_id')->orderBy('e.name'))
            ->editColumn('doc', function($query) {
                return short_date_format($query->doc);
            })
            ->addColumn($this->returnMonth($months_in_range, 0), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 0)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 0))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 1), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 1)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 1))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 2), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 2)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 2))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 3), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 3)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 3))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 4), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 4)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 4))) ): 0;
            })

            ->addColumn($this->returnMonth($months_in_range, 5), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 5)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 5))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 6), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 6)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 6))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 7), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 7)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 7))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 8), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 8)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 8))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 9), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 9)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 9))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 10), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 10)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 10))) ): 0;
            })
            ->addColumn($this->returnMonth($months_in_range, 11), function($query)use($receipt_code_repo, $months_in_range) {
                return !is_null($this->returnMonth($months_in_range, 11)) ? number_2_format($receipt_code_repo->getEmployerMonthlyContrib($query->employer_id,($this->returnMonth($months_in_range, 11))) ): 0;
            });


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $this->query = $this->searchResult($this->input);

        return $this->query;
    }
    /*Return specific month from array*/
    public function returnMonth($months_in_range,$month_arr_index)
    {
        return isset($months_in_range[$month_arr_index]) ? $months_in_range[$month_arr_index] : null;
    }

    /**
     * @param $data
     * @return mixed
     * Search
     */
    public function searchResult($data)
    {
        $se_repo = new StaffEmployerRepository();
        $categories_for_collection = $se_repo->getCategoriesForCollectionAllocationArray();
        $this->query = $se_repo->queryStaffEmployerGeneral()
            ->select(
                DB::raw("concat_ws(' ',u.firstname, u.lastname) as fullname"),
                'e.id as reg_no',
                'e.id as employer_id',
                'e.name as employer_name',
                'e.doc',
                'r.name as region_name'
            )
            ->where('se.isactive',1)
            ->whereIn('sa.category', $categories_for_collection);

        //        /*query with user*/
        if(isset($data['user_id'])){
            $this->query = $this->queryWithUser($data);
        }
        return $this->query;
    }


    /*Search query with user*/
    public function queryWithUser($data)
    {
        $user_id = $data['user_id'];
        return $this->query->where('se.user_id', $user_id);
    }

//    /*Get All months in date range*/
    public function getMonthsInDateRange($data)
    {
        $month_array = [];
        $from_date =isset($data['from_date']) ?  (Carbon::parse($data['from_date'])->startOfMonth()) : (Carbon::now());
        $to_date = isset($data['to_date']) ?  (Carbon::parse($data['to_date'])->startOfMonth()) : (Carbon::now());
        while(comparable_date_format($from_date) <= comparable_date_format($to_date)){
            $month_array[] = Carbon::parse($from_date)->format('M Y');
            $from_date = Carbon::parse($from_date)->addMonthNoOverflow()->startOfMonth();
        }
        return $month_array;
    }

    /*Check if date range is ok*/
    public function checkIfDateRangeNotExceed12Months($data)
    {
        $from_date =isset($data['from_date']) ?  (Carbon::parse($data['from_date'])->startOfMonth()) : (Carbon::now());
        $to_date = isset($data['to_date']) ?  (Carbon::parse($data['to_date'])->startOfMonth()) : (Carbon::now());
        if(months_diff($from_date, $to_date) > 12){
            $to_date = $from_date;
//            throw new GeneralException('Date range should not exceed 12 months');
        }
    }


    public function download($data){

        $employers = $this->searchResult($data)->get()->toArray();
        $months_in_range = $this->getMonthsInDateRange($data);
        $receipt_code_repo = new ReceiptCodeRepository();
        $results = array();
        foreach ($employers as $result) {

            /*Append month contribution*/
            $contrib_all = [];
            foreach ($months_in_range as $month)
            {
                $contrib = [$month => $receipt_code_repo->getEmployerMonthlyContrib($result->reg_no, $month) ];
                $contrib_all = array_merge($contrib_all, $contrib);
            }
            (array)$result = array_merge(  (array)$result, $contrib_all);
            $results[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you

        }

        return Excel::create($this->filename(), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download('csv');

//     return   Excel::create('employerregistration_export', function($excel) use ($data) {
//            $excel->sheet('Sheet1', function($sheet) use($data){
//                $this->searchResult($data)->get()->chunk(100, function($modelInstance) use($sheet, $data) {
//                    $sheet->fromArray( $modelInstance);
//                });
//            });
//        })->download('csv');

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
//                'serverSide'=> true,
//                'processing'=> true,
//                'buttons' => ['csv', 'excel','print','reset', 'reload', 'colvis'],
//                'initComplete' => "function () {
//                    window.LaravelDataTables['dataTable'].buttons().container()
//                        .insertBefore( '#dataTable' );
//                }",
////                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['employer_id'];
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $months_in_range = $this->getMonthsInDateRange($this->input);

        return [
            //            ['data' => 'row_number', 'name' => 'row_number', 'title' => 'SN', 'orderable' => false, 'searchable' => false],
            ['data' => 'fullname', 'name' => 'u.firstname', 'title' => 'Staff', 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'e.id', 'title' => 'Reg No', 'orderable' => true, 'searchable' => true],
            ['data' => 'employer_name', 'name' => 'e.name', 'title' => 'Employer Name', 'orderable' => true, 'searchable' => true],
            ['data' => 'doc', 'name' => 'e.doc', 'title' => 'Date Commencement', 'orderable' => true, 'searchable' => true],
            ['data' => 'region_name', 'name' => 'r.name', 'title' => 'Region', 'orderable' => false, 'searchable' => false],
            ['data' => $this->returnMonth($months_in_range, 0), 'name' => $this->returnMonth($months_in_range, 0), 'title' => $this->returnMonth($months_in_range, 0) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 0)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 1), 'name' => $this->returnMonth($months_in_range, 1), 'title' => $this->returnMonth($months_in_range, 1) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 1)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 2), 'name' => $this->returnMonth($months_in_range, 2), 'title' => $this->returnMonth($months_in_range, 2) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 2)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 3), 'name' => $this->returnMonth($months_in_range, 3), 'title' => $this->returnMonth($months_in_range, 3) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 3)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 4), 'name' => $this->returnMonth($months_in_range, 4), 'title' => $this->returnMonth($months_in_range, 4) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 4)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 1), 'name' => $this->returnMonth($months_in_range, 5), 'title' => $this->returnMonth($months_in_range, 5) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 5)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 1), 'name' => $this->returnMonth($months_in_range, 6), 'title' => $this->returnMonth($months_in_range, 6) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 6)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 1), 'name' => $this->returnMonth($months_in_range, 7), 'title' => $this->returnMonth($months_in_range, 7) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 7)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 1), 'name' => $this->returnMonth($months_in_range, 8), 'title' => $this->returnMonth($months_in_range, 8) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 8)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 1), 'name' => $this->returnMonth($months_in_range, 9), 'title' => $this->returnMonth($months_in_range, 9) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 9)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 1), 'name' => $this->returnMonth($months_in_range, 10), 'title' => $this->returnMonth($months_in_range, 10) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 10)) ? true : false],
            ['data' => $this->returnMonth($months_in_range, 1), 'name' => $this->returnMonth($months_in_range, 11), 'title' => $this->returnMonth($months_in_range, 11) . ' Amount', 'orderable' => false, 'searchable' => false, 'visible' => ($this->returnMonth($months_in_range, 11)) ? true : false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'staffmonthlycollections' . time();
    }

}
