<?php

namespace App\DataTables\Report\Compliance;

use App\Models\Operation\Compliance\Member\EmployerCertificateIssue;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use Yajra\Datatables\Services\DataTable;

class EmployerWIthInterestOverpaymentDataTable extends DataTable
{


    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        $booking_interests = new BookingInterestRepository();
        $receipt_codes = new ReceiptCodeRepository();
                return $this->datatables
            ->eloquent($this->query())
            ->addColumn('interest', function ($employer) use($booking_interests){
                return number_2_format($booking_interests->getTotalEmployerInterest($employer->id));
            })
            ->addColumn('amount_paid', function ($employer) use($receipt_codes){
                return number_2_format($receipt_codes->getTotalInterestPaidForEmployer($employer->id));
            })
            ->addColumn('balance', function ($employer)use($booking_interests,$receipt_codes) {
                return number_2_format(($booking_interests->getTotalEmployerInterest($employer->id) - $receipt_codes->getTotalInterestPaidForEmployer($employer->id)));
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $employers = new EmployerRepository();
             $this->query = $employers->getEmployerInterestOverpayment();

        return $this->applyScopes($this->query);
    }



    /**
     * @return mixed
     * Find Total employers issued certificate
     */

    public function findTotalEmployers()
    {
        $total = $this->query()->select(['id'])->count();
        return $total;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide'=> true,
                'processing'=> true,
                'buttons' => ['csv', 'excel','print','reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/profile/' + aData['id'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.name'),'orderable' => false, 'searchable' => false],
            ['data' => 'reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => false, 'searchable' => false],
            ['data' => 'interest', 'name' => 'interest', 'title' => trans('labels.backend.table.interest'), 'orderable' => false, 'searchable' => false],
            ['data' => 'amount_paid', 'name' => 'amount_paid', 'title' => trans('labels.backend.table.amount_paid'), 'orderable' => false, 'searchable' => false],
            ['data' => 'balance', 'name' => 'balance', 'title' => trans('labels.general.balance'), 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employerwithinterestoverpayment_' . time();
    }
}
