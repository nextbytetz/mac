<?php



namespace App\DataTables\Report\Traits;


use PDFSnappy;

trait PdfSnappyExportTrait
{


    /*Export to pdf snappy for table contents*/
    public function exportToPdfSnappy($content_data, array $resource_data)
    {
        $view = view('backend/report/includes/pdf_snappy_export/content')->with('content_data', $content_data)->with('resource_data', $resource_data)->with('sn', 1);
        $footer = view('backend/report/includes/pdf_snappy_export/footer');
        $header = view('backend/report/includes/pdf_snappy_export/header')->with('resource_data', $resource_data);

        $pdf = PDFSnappy::loadHTML($view)->setOption('header-html', $header)->setOption('footer-html', $footer)->setOption('footer-right', '[page]')->setOption('orientation', $resource_data['orientation']);
//$files =
        return $pdf->stream();

    }

    /*Export to pdf snappy for include content*/
    public function exportToPdfSnappyForIncludeContent($content_include, array $resource_data)
    {
        $view = view('backend/report/includes/pdf_snappy_export/general/general_content')->with('content_include', $content_include)->with('resource_data', $resource_data)->with(isset($resource_data['with_parameters']) ? $resource_data['with_parameters'] : []);
        $footer = view('backend/report/includes/pdf_snappy_export/footer');
        $header = view('backend/report/includes/pdf_snappy_export/header')->with('resource_data', $resource_data);

        $pdf = PDFSnappy::loadHTML($view)->setOption('header-html', $header)->setOption('footer-html', $footer)->setOption('footer-right', '[page]')->setOption('orientation', $resource_data['orientation']);
        return $pdf->stream();
    }


}
