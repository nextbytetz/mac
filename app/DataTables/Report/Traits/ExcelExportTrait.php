<?php



namespace App\DataTables\Report\Traits;



use Maatwebsite\Excel\Facades\Excel;

trait ExcelExportTrait
{

    /**
     * @param $results
     * @param string $filename
     * @param string $export_format
     * @return mixed
     * Export excel method general
     */
    public function exportExcelGeneral($results, $filename = 'export', $export_format = 'csv')
    {
        return Excel::create(($filename), function($excel) use ($results) {
            $excel->sheet('mySheet', function($sheet) use ($results)
            {
                $sheet->fromArray((array)$results);
            });
        })->download($export_format);
    }


}
