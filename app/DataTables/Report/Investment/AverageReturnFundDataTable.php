<?php

namespace App\DataTables\Report\Investment;

use App\Models\Auth\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Investiment\InvestimentSummaryRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;

class AverageReturnFundDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
            // ->addColumn('action', 'report/investment/averagereturnfunddatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query()->select($this->getColumns());

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->parameters([
                        // 'dom'     => 'Bfrtip',
                        'processing'=> true,
                'info'=> true,
                'paging'=> true,
                'searching' => true,
                'buttons' => [],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/report/' + aData['url_name'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
         return [
            ['data' => 'Item', 'name' => 'item', 'title' => 'Iterm','orderable' => false, 'searchable' => true],
            ['data' => 'investment', 'name' => 'investment', 'title' => 'Investment','orderable' => false, 'searchable' => true],
            ['data' => 'return', 'name' => 'return', 'title' => 'Return', 'orderable' => false, 'searchable' => true],
            ['data' => 'weight', 'name' => 'weight', 'title' => 'Weight', 'orderable' => true, 'searchable' => false],
            ['data' => 'average', 'name' => 'average', 'title' => 'Weighted Average Return', 'orderable' => true, 'searchable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportinvestmentaveragereturnfunddatatable_' . time();
    }
}
