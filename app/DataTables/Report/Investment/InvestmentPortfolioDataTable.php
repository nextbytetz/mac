<?php

namespace App\DataTables\Report\Investment;

use App\Models\Auth\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Investiment\InvestimentSummaryRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;

class InvestmentPortfolioDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
           ->addColumn('action', 'report/investment/investmentportfoliodatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query()->select($this->getColumns());

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->parameters([
                        // 'dom'     => 'Bfrtip',
                        // 'order'   => [[0, 'desc']],
                        // 'buttons' => [
                        //     'export',
                        //     'print',
                        //     'reload',
                        // ],
                        'processing'=> true,
                'info'=> true,
                'paging'=> true,
                'searching' => true,
                'buttons' => [],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/report/' + aData['url_name'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
             ['data' => 'investment_portfolio', 'name' => 'investment_portfolio', 'title' => 'Investment Portfolio','orderable' => false, 'searchable' => true],
            ['data' => 'outstanding_amount', 'name' => 'outstanding_amount', 'title' => 'Outstanding Amount (TZS) ','orderable' => false, 'searchable' => true],
            ['data' => 'total_investment', 'name' => 'total_investment', 'title' => '% of Total Investment', 'orderable' => false, 'searchable' => true],
            ['data' => 'tota_asset', 'name' => 'tota_asset', 'title' => '% of Total Asset*', 'orderable' => true, 'searchable' => false],
            ['data' => 'guideline', 'name' => 'guideline', 'title' => 'Guidelines', 'orderable' => true, 'searchable' => false],
            ['data' => 'policy', 'name' => 'policy', 'title' => 'Policy', 'orderable' => true, 'searchable' => false],
            ['data' => 'compliance', 'name' => 'compliance', 'title' => 'Compliance', 'orderable' => true, 'searchable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportinvestmentinvestmentportfoliodatatable_' . time();
    }
}
