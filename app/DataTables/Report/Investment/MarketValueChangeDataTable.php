<?php

namespace App\DataTables\Report\Investment;

use App\Models\Auth\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Investiment\InvestimentSummaryRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;

class MarketValueChangeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
             ->addColumn('action', 'report/investment/marketvaluechangedatatable.action');
    }
        
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query()->select($this->getColumns());

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        // 'dom'     => 'Bfrtip',
                       'processing'=> true,
                'info'=> true,
                'paging'=> true,
                'searching' => true,
                'buttons' => [],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/report/' + aData['url_name'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                       
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
              ['data' => 's_no', 'name' => 's_no', 'title' => 'S/No,','orderable' => false, 'searchable' => true],
            ['data' => 'description', 'name' => 'description', 'title' => 'Description','orderable' => false, 'searchable' => true],
            ['data' => 'amount_invested', 'name' => 'amount_invested', 'title' => 'Amount Invested', 'orderable' => false, 'searchable' => true],
            ['data' => 'unit_value', 'name' => 'unit_value', 'title' => ' Unit Value', 'orderable' => true, 'searchable' => false],
            ['data' => 'number_share', 'name' => 'number_share', 'title' => 'Number of Shares/Units', 'orderable' => true, 'searchable' => false],
            ['data' => 'new_value', 'name' => 'new_value', 'title' => 'New Value', 'orderable' => true, 'searchable' => false],
            ['data' => 'value', 'name' => 'value', 'title' => 'Value', 'orderable' => true, 'searchable' => false],
            ['data' => 'gain_loss', 'name' => 'gain_loss', 'title' => 'Gain/(Loss)', 'orderable' => true, 'searchable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportinvestmentmarketvaluechangedatatable_' . time();
    }
}
