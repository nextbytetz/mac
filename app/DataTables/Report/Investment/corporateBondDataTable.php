<?php

namespace App\DataTables\Report\Investment;

use App\Models\Auth\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Investiment\InvestimentSummaryRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;
use App\Models\Investiment\InvestimentCorporateBond;


class CorporateBondDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
     public function dataTable()
    {
        return $this->datatables->of($this->query());
        // ->editColumn("amount_invested", function($investment_type){
        //     return number_format($investment_type->amount_invested,2) ;
        // })
        // ->addColumn('interest_receivable', function($data){
        //     $interest_receivable = $data->interest_receivable == $data->receivable ? null : $data->interest_receivable;
        //     return number_format($interest_receivable,4);
        // })
        // ->addColumn('with_holding_tax', function($data){
        //     $holding = new InvestimentSummaryRepository();
        //     $with_holding_tax = $holding->withHoldingTax($data->interest_receivable, $data->tax_rate);
        //     return number_format($with_holding_tax,4);
        // })
        // ->addColumn('outstanding', function($data){
        //     $holding = new InvestimentSummaryRepository();
        //     $outstanding = $holding->tbOutstanding($data->interest_receivable, $data->receivable);
        //     return number_format($outstanding, 4);
        // })
        // ->addColumn('interest_income', function($data){
        //     return number_format($data->interest_income, 4);
        // })
        // ->addColumn('cost', function($data){
        //     return number_format($data->cost, 4);
        // });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $this->query = $this->tBondsDatatable($this->request);
        return $this->applyScopes($this->query);
    }

    public function tBondsDatatable($request){
        // dd($request['tenure']);

    $investment = InvestimentCorporateBond::where('tenure',$request['tenure'])
    ->leftJoin('bond_increment_dates','bond_increment_dates.corporate_bond_id','=','investiment_corporate_bonds.id')
    ->orderBy('bond_increment_dates.created_at','asc');

    return $investment;

   }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax(route("backend.investment.report.schedule.corporate_bond",$this->request))
                    ->parameters([
                        'dom' => 'Bfrtip',
                        'buttons' => ['excel','csv','reset', 'reload','colvis'],
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => false,
                        'info' => true,
                        'responsive' => true,
                        'footerCallback'=> "function ( row, data, start, end, display ) {
                        var api = this.api();
                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {

                        return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                        i : 0;

                        };
                        // Total over this page

                        pageTen = api
                        .column( 10, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                        }, 0 );

                        pageNine = api
                        .column( 9, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                        }, 0 );


                        function commaSeparateNumber(val){
                        while (/(\d+)(\d{3})/.test(val.toString())){
                        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                        return val ;
                        }
                        var json = api.ajax.json();
                        // console.log(json.data[0].opening_date)
                        // Update footer
                        $( api.column(0).footer() ).html( 'TOTAL'  );
                        $( api.column(10).footer() ).html( ''+ commaSeparateNumber(pageTen.toFixed(2)));
                        $( api.column(9).footer() ).html( ''+ commaSeparateNumber(pageNine.toFixed(2)));
                        // $( api.column(2).footer() ).html( json.data[0].opening_date );
                        }",
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        // document.location.href = '". url("/") . "/workplace_risk_assesment/data_management/employee/' + aData['employee_id'] + '/' + aData['case_no'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                    }",
                    ]);
    }



    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

        return [
                'tenure' => [ 'name'=> 'tenure', 'title' => "Period", 'orderable' => true, 'searchable' => true],
                'settlement_date' => [ 'name'=> 'settlement_date' ,'title' => "Purchasing Date", 'orderable' => true, 'searchable' => false],
                'maturity_date' => [ 'name'=> 'maturity_date','title' => "Maturity Date"],
                'amount_invested' => [ 'name'=> 'amount_invested','title' => "Face Value",  'orderable' => true, 'searchable' => false],
                'price' => [ 'orderable' => true,'title' => "Price",  'searchable' => false,'visible' => true],
                'cost' => [ 'orderable' => true,'title' => "Cost Of Investment",  'searchable' => false,'visible' => true],
                'interest_receivable' => [ 'title' => "Interest Amount", 'searchable' => false,'visible' => true],
                'coupon_rate' => [ 'name'=> 'coupon_rate','title' => "Interest Rate", 'searchable' => false],
                'interest_date' => [ 'name'=> 'interest_date','title' => "Interest Due On", 'searchable' => false],

                'interest_amount' => [ 'name'=> 'interest_amount','title' => "Interest Amount", 'searchable' => false],
                'interest_earned' => [ 'name'=> 'interest_earned','title' => "Interest Earned", 'searchable' => false],
                'yield' => [ 'name'=> 'yield','title' => "Yield", 'searchable' => false],
            ];
               
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportinvestmentcorporatebonddatatable_' . time();
    }
}
