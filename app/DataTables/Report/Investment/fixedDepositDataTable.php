<?php

namespace App\DataTables\Report\Investment;

use App\Models\Auth\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Investiment\InvestimentSummaryRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;

class FixedDepositDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
         return $this->datatables->of($this->query())
        ->addColumn('interest_rate', function($data) {
            $interest_rate = 0;
            $interest_rate = $data->interest_rate;            
            return round($interest_rate,4).'%';
        })
        ->addColumn('interest_income', function($data){
            $repo = new InvestimentTypeRepository();
            $interest_income = $repo->interestIncome($data->tax_rate,$data->interest_rate,$data->face_value,$data->opening_date);
            return round($interest_income,4);
        })
        ->addColumn('withholding_tax', function($data){
            $interest_amount = $data->face_value * ($data->interest_rate / 100);
            $tax_amount = $interest_amount * ($data->tax_rate / 100);
            return $tax_amount;
        })
        ->addColumn('interest_receivable', function($data){
            $interest_receivable = $data->interest_receivable == $data->interest_paid ? null : $data->interest_receivable;
            return $interest_receivable;
        })
        ->addColumn('principal_redeemed', function($data){
            $repo = new InvestimentTypeRepository();
            $tenure = explode('-', $data->tenure);
            $diff = $tenure[0];
            $outstanding_maturity = $repo->outstandingAtMaturity($data->tax_rate,$data->face_value,$data->interest_rate,$diff);

            $outstanding_received = $repo->outstandingReceived($data->principal_redeemed,$data->interest_paid);
            $outstanding = $outstanding_maturity - $outstanding_received;
            $principal_redeemed = $outstanding == 0 ? null : $data->face_value;
            return $principal_redeemed;
        });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $repo = new InvestimentSummaryRepository();
        $this->query = $repo->getFixedDepositSummary($this->request);
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax(route("backend.investment.report.schedule.fixed_deposits",$this->request))
                    ->parameters([
                        'dom' => 'Bfrtip',
                        'buttons' => ['excel','csv','reset', 'reload','colvis'],
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => false,
                        'info' => true,
                        'responsive' => true,
                        'footerCallback'=> "function ( row, data, start, end, display ) {
                        var api = this.api();
                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {

                        return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                        i : 0;

                        };
                        // Total over this page
                        pageOne = api
                        .column( 1, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                        }, 0 );

                        pageFour = api
                        .column( 4, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                        }, 0 );

                        pageFive = api
                        .column( 5, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                        }, 0 );

                        pageSeven = api
                        .column( 7, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                        }, 0 );

                        pageEight = api
                        .column( 8, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                        }, 0 );

                        pageNine = api
                        .column( 9, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                        }, 0 );


                        function commaSeparateNumber(val){
                        while (/(\d+)(\d{3})/.test(val.toString())){
                        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                        return val ;
                        }
                        var json = api.ajax.json();
                        // console.log(json.data[0].opening_date)
                        // Update footer
                        $( api.column(0).footer() ).html( 'TOTAL'  );
                        $( api.column(1).footer() ).html( ''+ commaSeparateNumber(pageOne.toFixed(2)));
                        $( api.column(4).footer() ).html( ''+ commaSeparateNumber(pageFour.toFixed(2)));
                        $( api.column(5).footer() ).html( ''+ commaSeparateNumber(pageFive.toFixed(2)));
                        $( api.column(7).footer() ).html( ''+ commaSeparateNumber(pageSeven.toFixed(2)));
                        $( api.column(8).footer() ).html( ''+ commaSeparateNumber(pageEight.toFixed(2)));
                        $( api.column(9).footer() ).html( ''+ commaSeparateNumber(pageNine.toFixed(2)));
                        // $( api.column(2).footer() ).html( json.data[0].opening_date );
                        }",
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        // document.location.href = '". url("/") . "/workplace_risk_assesment/data_management/employee/' + aData['employee_id'] + '/' + aData['case_no'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
                    }",
                    ]);
    }



    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

        return [
                'maturity_date' => [ 'name'=> 'investiment_fixed_deposits.maturity_date', 'title' => "Maturity", 'orderable' => true, 'searchable' => true],
                'face_value' => [ 'name'=> 'face_value' ,'title' => "Face Value", 'orderable' => true, 'searchable' => false],
                'interest_rate' => [ 'name'=> 'interest_rate','title' => "Rate % p.a"],
                'institution_name' => [ 'name'=> 'institution_name', 'orderable' => false,'title' => "Name Of The Bank",  'searchable' => true],
                'interest_maturity' => [ 'name'=> 'interest_maturity','title' => "Interest Maturity with holding tax",  'orderable' => true, 'searchable' => false],
                'interest_income' => [ 'orderable' => true,'title' => "Income To Date",  'searchable' => false,'visible' => true],
                'interest_paid' => [ 'orderable' => true,'title' => "Interest Paid",  'searchable' => false,'visible' => true],
                'interest_receivable' => [ 'title' => "Interest Receivable", 'searchable' => false,'visible' => true],
                // 'penalty' => [ 'title' => "Penalty", 'searchable' => false,'visible' => true],
                'withholding_tax' => [ 'name'=> 'withholding_tax','title' => "W/Tax Receivable", 'searchable' => false],
                'principal_redeemed' => [ 'name'=> 'principal_redeemed','title' => "Principal Balance", 'searchable' => false],
            ];
               
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportinvestmentfixeddepositdatatable_' . time();
    }
}
