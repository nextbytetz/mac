<?php

namespace App\DataTables\Report\Investment;

use App\Models\Auth\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Investiment\InvestimentSummaryRepository;
use App\Repositories\Backend\Investiment\InvestimentTypeRepository;

class DividentScheduleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
           ->addColumn('action', 'report/investment/dividentscheduledatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query()->select($this->getColumns());

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        // 'dom'     => 'Bfrtip',
                       'processing'=> true,
                'info'=> true,
                'paging'=> true,
                'searching' => true,
                'buttons' => [],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/report/' + aData['url_name'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                        
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'S/N',
            'Shares Issuer',
            'Agent',
            '# of shares',
            'Amount (TZS)',
            'Dividend declaration date',
            'Dividend Per share',
            'Amount (TZS)',
            'Description',
            'Due payment date',
            'Paid Amount',
            'Tax Withheld',
            'Date Paid',
            'Receipt No.',
            'Closing Balance (TZS)'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportinvestmentdividentscheduledatatable_' . time();
    }
}
