<?php

namespace App\DataTables\Report\Investment;

use App\Models\Auth\User;
use Yajra\Datatables\Services\DataTable;

class ComplianceListedStockDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
          ->addColumn('action', 'report/investment/compliancelistedstockdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query()->select($this->getColumns());

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->parameters([
                        // 'dom'     => 'Bfrtip',
                        'processing'=> true,
                'info'=> true,
                'paging'=> true,
                'searching' => true,
                'buttons' => [],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTable'].buttons().container()
                        .insertBefore( '#dataTable' );
                }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/report/' + aData['url_name'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
               ['data' => 'company', 'name' => 'company', 'title' => 'Company','orderable' => false, 'searchable' => true],
            ['data' => 'full_paid_share', 'name' => 'full_paid_share', 'title' => 'Full paid up shares','orderable' => false, 'searchable' => true],
            ['data' => 'full_holding', 'name' => 'full_holding', 'title' => 'Fund Holding number of Shares', 'orderable' => false, 'searchable' => true],
            ['data' => 'holding_limit', 'name' => 'holding_limit', 'title' => 'Holding Limit', 'orderable' => true, 'searchable' => false],
            ['data' => 'holding', 'name' => 'holding', 'title' => '% of Holding', 'orderable' => true, 'searchable' => false],
            ['data' => 'complied', 'name' => 'complied', 'title' => 'Complied', 'orderable' => true, 'searchable' => false]
           
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportinvestmentcompliancelistedstockdatatable_' . time();
    }
}

