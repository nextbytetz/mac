<?php

namespace App\DataTables\Claim;


use App\Repositories\Backend\Operation\Claim\HealthProviderRepository;
use Yajra\Datatables\Services\DataTable;

class GetHealthProvidersDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
        ->addColumn('location', function($health_provider) {
        return $health_provider->location;
    });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $health_providers = new HealthProviderRepository();
        $query = $health_providers->query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
//                'dom' => 'Bfrtip',
                'buttons' => ['reset', 'reload'],
                'initComplete' => "function () {
                    window.LaravelDataTables['dataTableBuilder'].buttons().container()
                        .insertBefore( '#dataTableBuilder' );
                }",
                'searching' => true,
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/claim/health_provider/' + aData['id'] + '/edit' ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'common_name', 'name' => 'common_name', 'title' => trans('labels.general.common_name')],
            ['data' => 'external_id', 'name' => 'external_id', 'title' => trans('labels.general.external_id'), 'orderable' => true, 'searchable' => true],
            ['data' => 'location', 'name' => 'location', 'title' => trans('labels.general.location'), 'orderable' => false, 'searchable' => false],
            ['data' => 'facility_type', 'name' => 'facility_type', 'title' => trans('labels.general.facility_type')],
            ['data' => 'ownership', 'name' => 'ownership', 'title' => trans('labels.general.ownership')],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'healthproviders_' . time();
    }
}
