<?php

namespace App\DataTables\Claim;


use App\Repositories\Backend\Operation\Claim\MedicalPractitionerRepository;
use Yajra\Datatables\Services\DataTable;

class GetMedicalPractitionersDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('fullname', function($medical_practitioner) {
                return $medical_practitioner->fullname;
            });

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $medical_practitioners = new MedicalPractitionerRepository();
        $query = $medical_practitioners->query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/claim/medical_practitioner/' + aData['id'] + '/edit' ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'fullname', 'name' => 'fullname', 'title' => trans('labels.general.full_name'), 'orderable' => false, 'searchable' => false],
            ['data' => 'phone', 'name' => 'phone', 'title' => trans('labels.general.phone')],
            ['data' => 'address', 'name' => 'address', 'title' => trans('labels.general.address'), 'orderable' => false, 'searchable' => false],
            ['data' => 'external_id', 'name' => 'external_id', 'title' => trans('labels.general.external_id'), 'orderable' => false, 'searchable' => false],
            ['data' => 'national_id', 'name' => 'national_id', 'title' => trans('labels.general.national_id'), 'orderable' => false, 'searchable' => false],
            ['data' => 'firstname', 'name' => 'firstname', 'orderable' => false, 'visible' => false],
            ['data' => 'lastname', 'name' => 'lastname', 'orderable' => false, 'visible' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'medicalpractitioners_' . time();
    }
}
