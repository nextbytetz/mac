<?php

namespace App\DataTables\Claim\InvestigationPlan;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;
use Carbon\Carbon;
class InvestigationPlanDataTable extends DataTable
{

    public function dataTable()
    {
        return $this->datatables
        ->eloquent($this->query())
        ->editColumn('investigation_category_cv_id', function($investigation) {
            return $investigation->name;
        })->editColumn('start_date', function($investigation) {
            return Carbon::parse($investigation->start_date)->format('d F, Y');
        })->editColumn('duration', function($investigation) {
            return $investigation->duration.' days';
        })
        ->editColumn('plan_status', function($investigation) {
            return (new InvestigationPlanRepository())->returnPlanStageLabel($investigation->plan_status,$investigation->wf_done);
        })
        ->editColumn('is_complete', function($investigation) {
           if ($investigation->wf_done && $investigation->plan_status==3) {
            $return = '';
        }else{
            $return = (new InvestigationPlanRepository())->returnPlanStatusLabel($investigation->is_complete); 
        }
        return $return;
    })
        ->rawColumns(['plan_status','is_complete']);
    }


    public function query()
    {
        $investigation = new InvestigationPlanRepository();
        $this->query = $investigation->getInvestigationPlanForDataTable();
        return $this->applyScopes($this->query);
    }


    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        // ->minifiedAjax(route('backend.claim.investigations.profile',$this->id))
        ->parameters([
            'dom'     => 'Bfrtip',
            'order'   => [[0, 'desc']],
            'buttons' => [
                // 'export',
                // 'csv',
                // 'reset',
                'reload',
            ],
            'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).click(function() {
                    document.location.href = '". url("/") . "/claim/investigations/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                        }, function() {
                            $(this).css('cursor','auto');
                            });
                        }",
                    ]);
    }


    protected function getColumns()
    {
        return [
            ['data' => 'plan_name', 'name' => 'plan_name', 'title' => 'Investigation', 'orderable' => true, 'searchable' => true],
            ['data' => 'investigation_category_cv_id', 'name' => 'investigation_category_cv_id', 'title' => 'Category', 'orderable' => true, 'searchable' => true],
            ['data' => 'start_date', 'name' => 'start_date', 'title' => 'Start Date', 'orderable' => true, 'searchable' => true],
            ['data' => 'duration', 'name' => 'duration', 'title' => 'Duration', 'orderable' => true, 'searchable' => true],
            ['data' => 'plan_status', 'name' => 'plan_status', 'title' => 'Stage', 'orderable' => true, 'searchable' => true],
            ['data' => 'user', 'name' => 'user', 'title' => "Created By", 'orderable' => false, 'searchable' => false],
            ['data' => 'is_complete', 'name' => 'is_complete', 'title' => 'Completion Status', 'orderable' => false, 'searchable' => false],
        ];
    }


    protected function filename()
    {
        return 'claiminvestigationinvestigationplandatatable_' . time();
    }
}
