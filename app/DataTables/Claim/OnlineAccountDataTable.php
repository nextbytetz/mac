<?php

namespace App\DataTables\Claim;

use App\Repositories\Backend\Operation\Claim\Portal\ClaimRepository;
use Yajra\Datatables\Services\DataTable;

class OnlineAccountDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn("confirmed_formatted", function($query) {
                return $query->confirmed_formatted;
            })
            ->addColumn("validated_formatted", function($query) {
                return $query->validated_formatted;
            })
            ->addColumn("active_formatted", function($query) {
                return $query->active_formatted;
            })
            ->rawColumns(['confirmed_formatted', 'validated_formatted', 'active_formatted']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        //$query = User::query()->select($this->getColumns());
        $repo = new ClaimRepository();
        if ($this->validated) {
            $this->query = $repo->getValidatedForDatatable();
        } else {
            $this->query = $repo->getPendingValidationForDatatable();
        }
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => true,
                'info' => true,
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/claim/notification_report/online_account_validation_profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'firstname', 'name' => 'firstname', 'title' => 'First Name', 'orderable' => true, 'searchable' => true],
            ['data' => 'lastname', 'name' => 'lastname', 'title' => 'Last Name', 'orderable' => true, 'searchable' => true],
            ['data' => 'email', 'name' => 'email', 'title' => 'Email', 'orderable' => true, 'searchable' => true],
            ['data' => 'phone', 'name' => 'phone', 'title' => 'Phone', 'orderable' => true, 'searchable' => true],
            ['data' => 'employer', 'name' => 'main.employers.name', 'title' => 'Employer', 'orderable' => true, 'searchable' => true],
            ['data' => 'confirmed_formatted', 'name' => 'confirmed_formatted', 'title' => "Is Confirmed", 'orderable' => false, 'searchable' => false],
            ['data' => 'active_formatted', 'name' => 'active_formatted', 'title' => "Status", 'Is Active' => false, 'searchable' => false],
            ['data' => 'validated_formatted', 'name' => 'validated_formatted', 'title' => "Is Validated", 'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'claimonlineaccountdatatable_' . time();
    }
}
