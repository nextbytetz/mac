<?php

namespace App\DataTables\Compliance\ContributionLegacy;

use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use Yajra\Datatables\Services\DataTable;

class EmployerContributionLegacyDataTable extends DataTable
{




    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('paid_by', function($legacy_receipt) {
                return ($legacy_receipt->payer) ?  $legacy_receipt->payer : ($legacy_receipt->employer()->count()) ? $legacy_receipt->employer->name : ' ';
            })
            ->addColumn('complete_status', function($legacy_receipt) {
                return $legacy_receipt->complete_status_label;
            })
            ->editColumn('rct_date', function($legacy_receipt) {
                return $legacy_receipt->rct_date_formatted;
            })
            ->editColumn('amount', function($legacy_receipt) {
                return $legacy_receipt->amount_formatted;
            })
            ->editColumn('created_at', function ($receipt) {
                return $receipt->created_at_formatted;
            })
            ->rawColumns(['iscancelled', 'complete_status']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $receipt = new LegacyReceiptRepository();
        $query = $receipt->getContributionForDataTable();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'buttons' => ['csv', 'excel', 'reset', 'reload'],
                'initComplete' => "function () {
                            this.api().columns([0,2]).every(function () {			    
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });				
                            });
                            window.LaravelDataTables['dataTableBuilder'].buttons().container()
                                .insertBefore( '#dataTableBuilder' );
                        }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/finance/legacy_receipt/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'rctno', 'name' => 'rctno', 'title' => trans('labels.backend.table.receipt.rctno')],
            ['data' => 'paid_by', 'name' => 'payer', 'title' => trans('labels.backend.table.receipt.employer'), 'orderable' => false, 'searchable' => true],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.backend.table.amount')],
            ['data' => 'rct_date', 'name' => 'rct_date', 'title' => trans('labels.backend.table.receipt.rct_date'), 'orderable' => true,'searchable' => true],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => trans('labels.backend.table.created_at'), 'orderable' => true, 'searchable' => true],
            ['data' => 'complete_status', 'name' => 'complete_status', 'title' => trans('labels.backend.table.receipt.stage'), 'orderable' => false, 'searchable' => false],
            ['data' => 'id', 'name' => 'id', 'visible' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employercontributionlegacy_' . time();
    }
}
