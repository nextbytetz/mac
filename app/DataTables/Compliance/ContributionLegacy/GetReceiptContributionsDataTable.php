<?php
/**
 * Get Contribution overview of a selected Receipt profile
 */

namespace App\DataTables\Compliance\ContributionLegacy;

use Yajra\Datatables\Services\DataTable;

class GetReceiptContributionsDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('paid_by', function($legacy_receipt) {
                return ($legacy_receipt->payer) ?  $legacy_receipt->payer : ($legacy_receipt->employer()->count()) ? $legacy_receipt->employer->name : ' ';
            })
            ->addColumn('complete_status', function($legacy_receipt) {
                return $legacy_receipt->complete_status_label;
            })
            ->editColumn('rct_date', function($legacy_receipt) {
                return $legacy_receipt->rct_date_formatted;
            })
            ->editColumn('amount', function($legacy_receipt) {
                return $legacy_receipt->amount_formatted;
            })
            ->editColumn('created_at', function ($receipt) {
                return $receipt->created_at_formatted;
            })
            ->rawColumns(['iscancelled', 'complete_status']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
//        $legacy_receipts = new LegacyReceiptRepository();
//        $query = $legacy_receipts->getAllContributionsByReceipt($id);
//        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(route('backend.finance.legacy_receipt.contributions', ['legacy_receipt_id'=> $this->legacy_receipt_id ]))
            ->parameters([

                'searching' => false,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => false,
                'info' => false,

                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/finance/legacy_receipt_code/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'workflow_status', 'name' => 'workflow_status', 'title' =>''],
            ['data' => 'contrib_month', 'name' => 'contrib_month', 'title' => trans('labels.backend.table.receipt.contrib_month'), 'orderable' => false, 'searchable' => true],
            ['data' => 'member_count', 'name' => 'member_count', 'title' => trans('labels.backend.table.member_count')],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.backend.table.amount'), 'orderable' => true,'searchable' => true],
            ['data' => 'status', 'name' => 'status', 'title' => trans('labels.backend.table.linked_file'), 'orderable' => true, 'searchable' => true],
            ['data' => 'action', 'name' => 'action', 'title' => trans('labels.backend.table.action'), 'orderable' => false, 'searchable' => false],
//            ['data' => 'id', 'name' => 'id', 'visible' => false],
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'receiptcontributionoverview_' . time();
    }
}
