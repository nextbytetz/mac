<?php
/**
 * Get Contribution overview of a selected Receipt profile
 */

namespace App\DataTables\Compliance\ContributionLegacy;

use Yajra\Datatables\Services\DataTable;

class GetReceiptInterestDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
//        $legacy_receipts = new LegacyReceiptRepository();
//        $query = $legacy_receipts->getAllContributionsByReceipt($id);
//        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(route('backend.finance.legacy_receipt.interests', ['legacy_receipt_id'=> $this->legacy_receipt_id ]))
            ->parameters([

                'searching' => false,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => false,
                'info' => false,

                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/finance/legacy_receipt/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'contrib_month', 'name' => 'contrib_month', 'title' => trans('labels.backend.table.receipt.contrib_month'), 'orderable' => false, 'searchable' => true],
            ['data' => 'late_months', 'name' => 'late_months', 'title' => trans('labels.backend.table.booking.late_months')],
            ['data' => 'interest', 'name' => 'interest', 'title' => trans('labels.backend.table.interest'), 'orderable' => true,'searchable' => true],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.backend.table.amount_paid'), 'orderable' => true, 'searchable' => true],
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'receiptcontributionoverview_' . time();
    }
}
