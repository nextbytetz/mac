<?php

namespace App\DataTables\Compliance\Member;


use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use Yajra\Datatables\Services\DataTable;


class GetInspectionTaskUnregisteredEmployersDataTable extends DataTable
{

    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('region', function($unregistered_employer) {
                return ($unregistered_employer->district_id) ? $unregistered_employer->district->region->name : '';
            })
            ->editColumn('is_registered', function($unregistered_employer) {
                return ($unregistered_employer->is_registered) ? $unregistered_employer->register_status_label : '';
            })
            ->editColumn('assign_to', function($unregistered_employer) {
                return ($unregistered_employer->assignedStaff()->count()) ? $unregistered_employer->assignedStaff->username : $unregistered_employer->assign_status_label;
            })
            ->rawColumns(['is_registered', 'assign_to']);


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $unregistered_employers = new UnregisteredEmployerRepository();
        $this->query = $unregistered_employers->query()->where('unregistered_followup_inspection_task_id', $this->unregistered_followup_inspection_task_id);

//        //         with assign to
//        if (isset($this->input['user_id'])) {
//            if ($this->input['user_id'] <> 0) {
//                $this->query = $this->queryWithUser();
//            }
//        }

        return $this->applyScopes($this->query);
    }

//    /*
//  * query date range with member type selected
//  */
//    public function queryWithUser()
//    {
//        return  $this->query->where('assign_to', $this->user_id);
//    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(route('backend.compliance.unreg_followup_inspection_task.get_employers',['unregistered_followup_inspection_task_id' => $this->unregistered_followup_inspection_task_id]))
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => false,
                'stateSave' => true,
                'paging' => true,
//                'info' => true,

                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/unregistered_employer/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'tin', 'name' => 'tin', 'title' => trans('labels.general.tin_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'sector', 'name' => 'sector', 'title' => trans('labels.general.sector'), 'orderable' => true, 'searchable' => true],
            ['data' => 'region', 'name' => 'region', 'title' => trans('labels.general.region'), 'orderable' => true, 'searchable' => true],
            ['data' => 'assign_to', 'name' => 'assign_to', 'title' => trans('labels.backend.member.assigned_to'), 'orderable' => true, 'searchable' => true],
            ['data' => 'is_registered', 'name' => 'is_registered', 'title' => trans('labels.general.status'), 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'taskunregisteredemployers_' . time();
    }
}
