<?php

namespace App\DataTables\Compliance\Member;

use App\Repositories\Backend\Operation\Compliance\Member\OnlineEmployerVerificationRepository;
use App\User;
use Yajra\Datatables\Services\DataTable;

class OnlineVerificationDataTable extends DataTable
{

    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('status_formatted', function ($query) {
                return $query->status_formatted;
            })
            ->addColumn("created_at_formatted", function ($query) {
                return $query->created_at_formatted;
            })
            ->addColumn("user_formatted", function ($query) {
                return $query->user_formatted;
            })
            ->rawColumns(['status_formatted']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        //$query = User::query()->select($this->getColumns());
        $verification = new OnlineEmployerVerificationRepository();
        $this->query = $verification->getAllForDatatable($this->employer->id);
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => true,
                'info' => true,
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/' + aData['id'] + '/verification_profile';
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'user_formatted', 'name' => 'user_id', 'title' => trans('labels.general.user'), 'orderable' => true, 'searchable' => false],
            ['data' => 'reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer_reg_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'status_formatted', 'name' => 'status', 'title' => trans('labels.backend.table.status'), 'orderable' => true, 'searchable' => false],
            ['data' => 'created_at_formatted', 'name' => 'created_at', 'title' => trans('labels.backend.table.created_at'), 'orderable' => true, 'searchable' => true],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'compliancememberonlineverification_' . time();
    }
}
