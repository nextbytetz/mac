<?php

namespace App\DataTables\Compliance\Member;

use App\Models\Operation\Compliance\Member\Employer;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRegistrationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Services\Scopes\IsApprovedScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Datatables\Services\DataTable;


class EmployerRegistrationDataTable extends DataTable
{

    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('country', function ($employer) {
                return ($employer->district_id) ? $employer->district->region->country->name : (($employer->region_id) ? $employer->region->country->name : ' ');
            })
            ->addColumn('region', function ($employer) {
                return ($employer->district_id) ? $employer->district->region->name : (($employer->region_id) ? $employer->region->name : ' ');
            })
            ->addColumn('doc_formatted', function($employer_registration) {
                return $employer_registration->date_of_commencement_formatted;
            })
            ->editColumn('status', function($employer_registration) {
                return $employer_registration->status_label;
            })
            ->rawColumns(['status']);


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $employer_registrations = new EmployerRepository();
        $this->query = $employer_registrations->query()->where('approval_id', 2)->where("source", $this->source)->withoutGlobalScopes([IsApprovedScope::class])->withTrashed();

        return $this->applyScopes($this->query);
    }




    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => true,
                'info' => true,


                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer_registration/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'reg_no', 'name' => 'reg_no', 'title' => trans('labels.backend.table.employer.reg_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'tin', 'name' => 'tin', 'title' => trans('labels.general.tin_no'), 'orderable' => true, 'searchable' => true],
            ['data' => 'vote', 'name' => 'vote', 'title' => trans('labels.backend.member.vote'), 'orderable' => true, 'searchable' => true],
            ['data' => 'doc_formatted', 'name' => 'doc', 'title' => trans('labels.backend.table.employer.date_commenced'), 'orderable' => true, 'searchable' => true],
            ['data' => 'country', 'name' => 'country', 'title' => trans('labels.general.country'), 'orderable' => false, 'searchable' => false],
            ['data' => 'region', 'name' => 'region', 'title' => trans('labels.general.region'), 'orderable' => false, 'searchable' => false],

            ['data' => 'status', 'name' => 'status', 'title' => trans('labels.general.status'), 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employerregistrations' . time();
    }
}
