<?php

namespace App\DataTables\Compliance\Member;

use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\User;
use Yajra\Datatables\Services\DataTable;

class EmployerIncidentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn("status_formatted", function($query) {
                return $query->status_label;
            })
            ->addColumn("registration_formatted", function($query) {
                return $query->created_at_formatted;
            })
            ->addColumn("incident_date_formatted", function($query) {
                return $query->incident_date_formatted;
            })
            ->addColumn("reporting_date_formatted", function($query) {
                return $query->reporting_date_formatted;
            })
            ->rawColumns(['status_formatted']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        //$query = User::query()->select($this->getColumns());
        $repo = new PortalIncidentRepository();
        $id = NULL;
        if (isset($this->employer)) {
            $id = $this->employer->id;
        }
        $this->query = $repo->getForDatatable($id);
        return $this->applyScopes($this->query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
/*                    ->minifiedAjax('')
                    ->addAction(['width' => '80px'])*/
                    ->parameters([
//                'dom' => 'Bfrtip',
                        'searching' => true,
                        'serverSide' => true,
                        'stateSave' => true,
                        'paging' => true,
                        'info' => true,
                        'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/employer/' + aData['id'] + '/incident_profile';
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'incident_type', 'name' => 'main.incident_types.name', 'title' => 'Incident Type', 'orderable' => true, 'searchable' => true],
            ['data' => 'employee', 'name' => 'employee', 'title' => "Employee", 'orderable' => true, 'searchable' => false],
            ['data' => 'employer', 'name' => 'main.employers.name', 'title' => "Employer", 'orderable' => true, 'searchable' => true],
            ['data' => 'registration_formatted', 'name' => 'portal.incidents.created_at', 'title' => 'Registration Date', 'orderable' => true, 'searchable' => true],
            ['data' => 'incident_date_formatted', 'name' => 'portal.incidents.incident_date', 'title' => 'Incident Date', 'orderable' => true, 'searchable' => true],
            ['data' => 'reporting_date_formatted', 'name' => 'portal.incidents.reporting_date', 'title' => 'Reporting Date', 'orderable' => true, 'searchable' => true],
            ['data' => 'caseno', 'name' => 'main.notification_reports.filename', 'title' => "Case No", 'orderable' => true, 'searchable' => true],
            ['data' => 'district', 'name' => 'main.districts.name', 'title' => "District", 'orderable' => true, 'searchable' => true],
            ['data' => 'region', 'name' => 'main.regions.name', 'title' => "Region", 'orderable' => true, 'searchable' => true],
            ['data' => 'status_formatted', 'name' => 'status_formatted', 'title' => "Status", 'orderable' => false, 'searchable' => false],
            ['data' => 'lastname', 'name' => 'main.employees.lastname', 'title' => "", 'orderable' => true, 'searchable' => true, 'visible' => false],
            ['data' => 'middlename', 'name' => 'main.employees.middlename', 'title' => "Employee", 'orderable' => true, 'searchable' => true, 'visible' => false],
            ['data' => 'firstname', 'name' => 'main.employees.firstname', 'title' => "Employee", 'orderable' => true, 'searchable' => true, 'visible' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'compliancememberemployerincident_' . time();
    }
}
