<?php

namespace App\DataTables\Compliance\Member;


use App\Repositories\Backend\Operation\Compliance\Member\EmployerRegistrationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use Yajra\Datatables\Services\DataTable;


class EmployeeCountDataTable extends DataTable
{

    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('employee_category', function($employeeCount) {
                return $employeeCount->employeeCategory->name;
            })
            ->addColumn('gender', function($employeeCount) {
                return $employeeCount->gender->name;
            });



    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

//        $unregistered_employers = new UnregisteredEmployerRepository();
//        $this->query = $unregistered_employers->query();

        return $this->applyScopes($this->query);
    }




    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(route('backend.compliance.employer.employee_count_get',['employer_id' =>$this->employer_id]))
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => false,
                'info' => false,

//                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = '". url("/") . "/compliance/unregistered_employer/follow_up/' + aData['id'] + '/edit';
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'employee_category', 'name' => 'employee_category_cv_id', 'title' => trans('labels.backend.member.employee_category'), 'orderable' => true, 'searchable' => true],
            ['data' => 'gender', 'name' => 'gender_id', 'title' => trans('labels.general.gender'), 'orderable' => true, 'searchable' => true],
            ['data' => 'count', 'name' => 'count', 'title' => trans('labels.general.count'), 'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employeecounts' . time();
    }
}
