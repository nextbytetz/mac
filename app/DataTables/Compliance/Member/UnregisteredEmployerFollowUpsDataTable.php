<?php

namespace App\DataTables\Compliance\Member;


use App\Repositories\Backend\Operation\Compliance\Member\UnregisteredEmployerRepository;
use Yajra\Datatables\Services\DataTable;


class UnregisteredEmployerFollowUpsDataTable extends DataTable
{

    protected $query;

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('country', function($unregistered_employer) {
                return $unregistered_employer->district->region->country->name;
            })
            ->addColumn('region', function($unregistered_employer) {
                return $unregistered_employer->district->region->name;
            })
            ->addColumn('address', function($unregistered_employer) {
                return $unregistered_employer->address_formatted;
            })
            ->editColumn('status', function($unregistered_employer) {
                return $unregistered_employer->status_label;
            })
            ->rawColumns(['status']);


    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {

        $unregistered_employers = new UnregisteredEmployerRepository();
        $this->query = $unregistered_employers->query();

        return $this->applyScopes($this->query);
    }




    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(route('backend.compliance.unregistered_employer.follow_up_get',['unregistered_employer_id' =>$this->unregistered_employer_id]))
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => false,
                'serverSide' => true,
                'stateSave' => true,
                'paging' => true,
                'info' => false,

                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/unregistered_employer/follow_up/' + aData['id'] + '/edit';
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            ['data' => 'description', 'name' => 'description', 'title' => trans('labels.general.descriptions'), 'orderable' => true, 'searchable' => true],
            ['data' => 'follow_up_type_cv_id', 'name' => 'follow_up_type_cv_id', 'title' => trans('labels.backend.member.follow_up_type'), 'orderable' => true, 'searchable' => true],
            ['data' => 'contact', 'name' => 'contact', 'title' => trans('labels.general.contact'), 'orderable' => false, 'searchable' => false],
            ['data' => 'contact_person', 'name' => 'contact_person', 'title' => trans('labels.general.contact_person'), 'orderable' => false, 'searchable' => false],
            ['data' => 'date_of_follow_up', 'name' => 'date_of_follow_up', 'title' => trans('labels.general.date'), 'orderable' => true, 'searchable' => false],
            ['data' => 'feedback', 'name' => 'feedback', 'title' => trans('labels.general.feedback'), 'orderable' => false, 'searchable' => true],
            ['data' => 'user_id', 'name' => 'user_id', 'title' => trans('labels.general.staff'), 'orderable' => true, 'searchable' => true],


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'followups' . time();
    }
}
