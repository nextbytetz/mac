<?php

namespace App\DataTables\Compliance\Inspection;

use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionProfileRepository;
use App\User;
use Yajra\Datatables\Services\DataTable;

class InspectionProfileDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $profile = new InspectionProfileRepository();
        $query = $profile->getInspectionProfileForDataTable();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'buttons' => ['csv', 'excel', 'reset', 'reload'],
                'initComplete' => "function () {
                           window.LaravelDataTables['dataTableBuilder'].buttons().container()
                                .insertBefore( '#dataTableBuilder' ); 
                        }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/inspection/profile/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.backend.compliance_menu.inspection.profile.heading')],
            ['data' => 'id', 'name' => 'id', 'visible' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'complianceinspectioninspectionprofiledatatable_' . time();
    }
}
