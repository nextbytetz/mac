<?php

namespace App\DataTables\Compliance\Inspection;

//use App\User;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionRepository;
use Yajra\Datatables\Services\DataTable;

class InspectionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('complete_status_label', function($inspection) {
                return $inspection->complete_status_label;
            })
            ->addColumn('iscancelled_label', function($inspection) {
                return $inspection->iscancelled_label;
            })
            ->rawColumns(['iscancelled_label', 'complete_status_label']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $inspection = new InspectionRepository();
        $query = $inspection->getInspectionForDataTable($this->current);
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                'buttons' => ['csv', 'excel', 'reset', 'reload'],
                'initComplete' => "function () {
                            window.LaravelDataTables['dataTableBuilder'].buttons().container()
                                .insertBefore( '#dataTableBuilder' );
                        }",
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/compliance/inspection/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'inspection_type', 'name' => 'a.name', 'title' => trans('labels.backend.compliance_menu.inspection.type')],
            ['data' => 'start_date_formatted', 'name' => 'start_date', 'title' => trans('labels.backend.compliance_menu.inspection.start_date'), 'orderable' => false, 'searchable' => true],
            ['data' => 'duration', 'name' => 'end_date', 'title' => "Duration (Weeks)",'searchable' => false],
            ['data' => 'stage', 'name' => 'd.name', 'title' => "Stage", 'orderable' => true,'searchable' => true],
            ['data' => 'iscancelled_label', 'name' => 'iscancelled', 'title' => trans('labels.backend.compliance_menu.inspection.status'), 'orderable' => true, 'searchable' => true],
            ['data' => 'complete_status_label', 'name' => 'complete_status', 'title' => trans('labels.backend.compliance_menu.inspection.complete_status'), 'orderable' => true, 'searchable' => true],
            ['data' => 'id', 'name' => 'id', 'visible' => false],
        ];
    }

    protected function selectColumns()
    {
        return [
            'inspection_type_id',
            'comments',
            'start_date',
            'duration',
            'iscancelled',
            'id',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'complianceinspectioninspectiondatatable_' . time();
    }
}
