<?php

namespace App\DataTables\Compliance\Inspection;

use App\Models\Finance\Traits\Relationship\ReceiptRelationship;
use App\Repositories\Backend\Finance\DishonouredChequeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use Yajra\Datatables\Services\DataTable;

class DishonouredChequesDataTable extends DataTable
{
    use ReceiptRelationship;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('rct_no', function($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count())?$dishonoured_cheque->receiptWithTrashed->rctno:0;
            })
            ->addColumn('employer', function($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count())?$dishonoured_cheque->receiptWithTrashed->employer->name:0;
            })

            ->addColumn('amount', function($dishonoured_cheque) {
                return ($dishonoured_cheque->receiptWithTrashed()->count())?$dishonoured_cheque->receiptWithTrashed->amount:0;

            })
            ->editColumn('old_bank_id', function($dishonoured_cheque) {
                return ($dishonoured_cheque->oldBank()->count())?$dishonoured_cheque->oldBank->name:"";
            })
           ;





    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $dishonoured_cheques = new DishonouredChequeRepository();
        $query = $dishonoured_cheques->getQuery()->whereNull('new_cheque_no')->whereBetween('created_at', [$this->from_date. ' 00:00:00',$this->to_date. ' 23:59:59']);
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => false,
                //'buttons' => [],


            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'employer', 'name' => 'employer', 'title' => trans('labels.backend.table.receipt.employer'), 'orderable' => false, 'searchable' => false],

            ['data' => 'old_cheque_no', 'name' => 'old_cheque_no', 'title' => trans('labels.backend.table.receipt.dishonoured_chequeno'), 'orderable' => false, 'searchable' => false],
            ['data' => 'old_bank_id', 'name' => 'old_bank_id', 'title' => trans('labels.backend.table.receipt.of_bank'), 'orderable' => false, 'searchable' => false],

            ['data' => 'rct_no', 'name' => 'rct_no', 'title' => trans('labels.backend.table.receipt.rctno'), 'orderable' => false, 'searchable' => false],
            ['data' => 'amount', 'name' => 'amount', 'title' => trans('labels.backend.table.receipt.amount'), 'orderable' => false, 'searchable' => false],

            ['data' => 'id', 'name' => 'id', 'visible' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'dishonouredcheques' . time();
    }
}
