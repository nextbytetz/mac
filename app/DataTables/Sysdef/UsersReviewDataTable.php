<?php

namespace App\DataTables\Sysdef;

use App\User;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;
use DB;
class UsersReviewDataTable extends DataTable
{
    protected $query;
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->of($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = DB::table('main.wf_definitions')
        ->join('main.units','wf_definitions.unit_id','=','units.id')
        ->join('main.designations','wf_definitions.designation_id','=','designations.id')
        ->join('main.wf_modules','wf_definitions.wf_module_id','=','wf_modules.id')
        ->join('main.user_wf_definition','wf_definitions.id','=','user_wf_definition.wf_definition_id')
        ->join('main.users','user_wf_definition.user_id','=','users.id')
        ->select('units.name as Directorate_Unit_Section','samaccountname as username','email','wf_modules.name as workflow_name','wf_definitions.level','wf_definitions.description as action_performed')
        ->whereNotIn('wf_modules.id', [12])
        ->where('wf_modules.isactive', 1)
        //->where('users.active',1)
        ->orderByRaw('wf_modules.name asc, level asc')
        ->get();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
         return $this->builder()
        ->columns($this->getColumns())
        ->parameters([
            'dom' => 'Bfrtip',
            'buttons' => ['csv', 'excel','print', 'reset', 'reload'],
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
             'workflow_name',
            'Directorate_Unit_Section',
            'username',
            'email',
            'level',
            'action_performed'

            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'MAC_users_review_as_at ' .Carbon::today()->format('Y-m-d');
    }
}
