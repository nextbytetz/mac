<?php

namespace App\DataTables\Sysdef;

use App\Repositories\Backend\Sysdef\CodeRepository;
use App\Repositories\Backend\Sysdef\CodeValueRepository;
use Yajra\Datatables\Services\DataTable;

class GetCodeValuesDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->eloquent($this->query())
            ->editColumn('is_active', function($code_value) {
                return $code_value->active;
            })
            ->editColumn('is_mandatory', function($code_value) {
                return $code_value->mandatory;
            })
            ->addColumn('action_button', function($code_value) {
                return $code_value->action_buttons;
            })
            ->rawColumns(['action_button']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $code_values = new CodeValueRepository();
        $query = $code_values->query()->where('code_id', $this->code_id);
        return $this->applyScopes($query);

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
//                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = '". url("/") . "/backend/system/code/value/' + aData['id'] + '/edit';
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('labels.general.name')],
            ['data' => 'description', 'name' => 'description', 'title' => trans('labels.general.descriptions'),'orderable' => false, 'searchable' => false],
            ['data' => 'sort', 'name' => 'sort', 'title' => trans('labels.general.position'),'orderable' => true, 'searchable' => true],
            ['data' => 'is_active', 'name' => 'is_active', 'title' => trans('labels.general.is_active'),'orderable' => true, 'searchable' => true],
            ['data' => 'is_mandatory', 'name' => 'is_mandatory', 'title' => trans('labels.general.is_mandatory'),'orderable' => true, 'searchable' => true],
            ['data' => 'action_button', 'name' => 'action_button', 'title' => trans('labels.general.actions'),'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'codevalues_' . time();
    }
}
