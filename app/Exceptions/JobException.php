<?php

namespace App\Exceptions;

use Exception;

/**
 * Class JobException
 * @package App\Exceptions
 */
class JobException extends Exception {}