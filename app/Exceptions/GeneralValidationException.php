<?php

namespace App\Exceptions;

use Exception;

/**
 * Class ValidationException
 * @package App\Exceptions
 */
class GeneralValidationException extends Exception{}