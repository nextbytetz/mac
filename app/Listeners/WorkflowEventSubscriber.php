<?php

namespace App\Listeners;

use App\Exceptions\WorkflowException;
use App\Jobs\Claim\RecheckPostingDocumentDms;
use App\Jobs\Notifications\SendSms;
use App\Jobs\RegisterEmployeeList;
use App\Jobs\RunPayroll\UpdateAfterPayrollRunApproval;
use App\Models\Finance\Receivable\BookingInterestRefund;
use App\Models\Operation\Payroll\PayrollRecovery;
use App\Models\Operation\Claim\Portal\InstallmentAccountant;
use App\Models\Auth\PortalUser;
use App\Models\Operation\Compliance\Member\Employer;
use App\Notifications\Backend\Operation\Claim\Instalments\InstalmentApproval;
use App\Notifications\Backend\Employer\ApprovedRegistration;
use App\Notifications\Backend\Employer\ApprovedVerification;
use App\Notifications\Backend\Employer\RejectedRegistration;
use App\Notifications\Backend\Employer\RejectedVerification;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\LegacyReceiptRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptCodeRepository;
use App\Repositories\Backend\Finance\Receipt\ReceiptRepository;
use App\Repositories\Backend\Finance\Receivable\InterestAdjustmentRepository;
use App\Repositories\Backend\Notifications\LetterRepository;
use App\Repositories\Backend\Operation\Claim\ClaimCompensationRepository;
use App\Repositories\Backend\Operation\Claim\ClaimRepository;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;
use App\Repositories\Backend\Operation\Claim\PortalIncidentRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\EmployerInspectionTaskRepository;
use App\Repositories\Backend\Operation\Compliance\Inspection\InspectionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerAdvancePaymentRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureExtensionRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureReopenRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerClosureRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerParticularChangeRepository;
use App\Repositories\Backend\Operation\Compliance\Member\EmployerRepository;
use App\Repositories\Backend\Operation\Compliance\Member\OnlineEmployerVerificationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\PortalAdvancePaymentRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollBankUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollBeneficiaryUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollMpUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollRecoveryRepository;
use App\Repositories\Backend\Operation\Payroll\PayrollStatusChangeRepository;
use App\Repositories\Backend\Operation\Payroll\Retiree\PayrollRetireeMpUpdateRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollReconciliationRepository;
use App\Repositories\Backend\Operation\Payroll\Run\PayrollRunApprovalRepository;
use App\Repositories\Backend\Sysdef\CodeValuePortalRepository;
use App\Repositories\Backend\Workflow\WfDefinitionRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use App\Repositories\Backend\Workflow\WfTrackRepository;
use App\Repositories\Backend\WorkPlaceRiskAssesment\OshAuditPeriodRepository;
use App\Repositories\Backend\Investiment\InvestimentBudgetRepository;
use App\Repositories\Backend\Operation\ClaimAccrual\AccrualNotificationReportRepository;
use App\Repositories\Backend\Operation\InvestigationPlan\InvestigationPlanRepository;
use App\Repositories\Backend\Operation\Compliance\Member\Online\MergeDeMergeEmployerPayrollRepository;
use App\Repositories\Backend\Operation\Claim\HspBilling\HspBillSummaryRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionModificationRepository;
use App\Repositories\Backend\Operation\Compliance\Member\ContributionInterestRateRepository;


use App\Services\Notifications\Api;
use App\Services\Payroll\ProcessPayroll;
use App\Services\Workflow\Workflow;
use App\Repositories\Backend\Finance\Receivable\BookingInterestRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;
use App\Services\Compliance\Traits\EmployeesExcel;

use App\Repositories\Backend\Sysdef\CodeValueRepository;
use App\Repositories\Backend\Task\CheckerRepository;

class WorkflowEventSubscriber
{
    use Api, EmployeesExcel;

    /**
     * Handle on new workflow events.
     * @param $event
     * @throws \App\Exceptions\GeneralException
     */
    public function onNewWorkflow($event)
    {
        $input = $event->input;
        $par = $event->par;
        $extra = $event->extra;
        $resource_id = $input['resource_id'];
        $wf_module_group_id = $input['wf_module_group_id'];
        if(isset($input['type'])) {
            $type = $input['type'];
        } else {
            $type = 0;
        }

        $data = [
            "resource_id" => $resource_id,
            "sign" => 1,
            "user_id" => access()->id(),
        ];
        $data['comments'] = isset($extra['comments']) ? $extra['comments'] : "Recommended";

        $workflow = new Workflow(['wf_module_group_id' => $wf_module_group_id, 'resource_id' => $resource_id, 'type' => $type]);
        $workflow->createLog($data);
    }

    /**
     * Handle on approve workflow events.
     * @param $event
     * @throws WorkflowException
     * @throws \App\Exceptions\GeneralException
     */
    public function onApproveWorkflow($event)
    {

        $wfTrack = $event->wfTrack;
        $input = $event->input;
        $wf_module_id = $wfTrack->wfDefinition->wfModule->id;
        $level = $wfTrack->wfDefinition->level;
        $skip = false;

        if ($wf_module_id == 60){
            $this->checkIfFilledFields($event, $level);
        }



        //start : check for optional levels
        $select_next_start_optional = (($wfTrack->status == 4) ? 1 : 0);
        $currentDefinition = $wfTrack->wfDefinition;
        switch (true) {
            case ($currentDefinition->has_next_start_optional And !$select_next_start_optional And !$currentDefinition->is_optional):
            case (!$currentDefinition->has_next_start_optional And !$select_next_start_optional And !$currentDefinition->is_optional):
            case ($currentDefinition->has_next_start_optional And !$select_next_start_optional And $currentDefinition->is_optional): //Added recently
                //This logic computed from the truth table, may need re-design.
                //Skip Optional level
            $skip = true;
            break;
            default:
                //Continue Regularly
            $skip = false;
            break;
        }
        //end : check for optional levels

        $resource_id = $wfTrack->resource_id;
        //Levels General Checks, e.g check for document if read etc
        if ($currentDefinition->need_check) {
            (new WfTrackRepository())->checkWfDefinition($currentDefinition, $resource_id, $wfTrack->id);
        }

        $workflow = new Workflow(['wf_module_id' => $wf_module_id, 'resource_id' => $resource_id]);
        $moduleRepo = new WfModuleRepository();
        $sign = 1;
        /* check if there is next level */
        if (!is_null($workflow->nextLevel($skip, $input['wf_definition']))) {
            // dd('next level');
            //throw new WorkflowException("Continual Block ... " . $input['wf_definition']);
            /* Process for specific resource on the given level */
            $continue = ['success' => true, 'jump_level' => NULL];

            $input['level'] = $level;
            $input['resource_id'] = $resource_id;
            $input['wf_module_id'] = $wf_module_id;
            switch ($wf_module_id) {
                case 3:
                case 10:
                /* Claim and Assessment Processing: Basic Procedure */
                $this->claimAssessmentLevelApproveProcess($level, $resource_id, $wf_module_id);
                break;
                case $moduleRepo->receiptVerificationModule()[0]:
                case $moduleRepo->receiptVerificationModule()[1]:
                case $moduleRepo->receiptVerificationAdvanceModule()[0]:
                /* Contribution Receipt */
                $this->contributionReceiptLevelApproveProcess($level, $resource_id, $wf_module_id);
                break;
                case 7:
                /* Contribution Legacy Receipt */
                $this->contributionLegacyReceiptLevelApproveProcess($level, $resource_id);
                break;
                case $moduleRepo->missingContributionNotificationModule()[0];
                case $moduleRepo->incorrectContributionNotificationModule()[0];
                case $moduleRepo->incorrectContributionNotificationModule()[1];
                /*Notification Missing Contribution*/
                (new NotificationReportRepository())->checkIfHasMissingContributionForWorkflow($level, $resource_id, $wf_module_id);
                break;
                case $moduleRepo->unregisteredMemberNotificationModule()[0]:
                /*Unregistered Member Notification*/
                (new NotificationReportRepository())->checkIfHasRegisteredMemberForWorkflow($level, $resource_id, $wf_module_id);
                break;
                /*Unregistered Member Notification*/
                (new NotificationReportRepository())->checkIfHasRegisteredMemberForWorkflow($level, $resource_id, $wf_module_id);
                break;
                case $moduleRepo->systemRejectedNotificationModule()[0]:
                case $moduleRepo->rejectionNotificationApprovalModule()[0]:
                case $moduleRepo->rejectionNotificationApprovalModule()[1]:
                /* Check if Benefit Rejection is Approved */
                (new NotificationReportRepository())->checkIfRejectionApproved($level, $resource_id, $wf_module_id);
                break;
                case $moduleRepo->claimBenefitModule()[0]:
                case $moduleRepo->claimBenefitModule()[1]:
                /*Claim Benefits*/
                    $continue = (new NotificationReportRepository())->checkBenefitPaymentWorkflow($input); //check if assessed and zero-payment at DO level.
                    (new NotificationReportRepository())->checkIfBenefitPaymentPaid($level, $resource_id, $wf_module_id);
                    break;
                    case $moduleRepo->maeRefundMemberModule()[0]:
                    /* Medical Aid Expense Refund for Employee/Employer */
                    $continue = (new NotificationReportRepository())->checkMaeRefundMemberWorkflow($input); //check if assessed and zero-payment at DO level.
                    (new NotificationReportRepository())->checkIfMaeRefundMemberPaid($level, $resource_id, $wf_module_id);
                    break;
                    case $moduleRepo->employerInspectionPlanModule()[0]:
                    case $moduleRepo->employerInspectionPlanModule()[1]:
                    //check if inspector has been assigned
                    (new InspectionRepository())->checkIfInspectorsHasBeenAssigned($level, $resource_id, $wf_module_id);
                    //check if all employer tasks has allocation
                    (new InspectionRepository())->checkIfAllTasksAllocated($level, $resource_id, $wf_module_id);
                    break;
                    case $moduleRepo->acknowledgementLetterIssuanceModule()[0]:
                    case $moduleRepo->benefitAwardLetterIssuanceModule()[0]:
                    case $moduleRepo->complianceLetterIssuanceModule()[0]:
                    case $moduleRepo->reminderLetterIssuanceModule()[0]:
                    (new LetterRepository())->processLetter($level, $resource_id, $wf_module_id);
                    break;

                    /*Employer closure*/
                    case $moduleRepo->employerBusinessClosureModules()[0]:
                    case $moduleRepo->employerBusinessClosureModules()[1]:
                    (new EmployerClosureRepository())->processWfActionAtLevels($workflow->currentWfTrack(), $input);

                    break;
                    case 65:
                    /* Investment Budget */
                    $input['comments'] = ($input['comments'] == 'Approved') ? 'Recommended' : $input['comments'];
                    $investment_budget = new InvestimentBudgetRepository();
                    $investment_budget->recommendBudget($resource_id, $level);
                    break;
                    /* Accrual Claim at Admin Level */
                    case 71:
                    $accrual_claim_at_admin = new AccrualNotificationReportRepository();
                    $accrual_claim_at_admin->updateAccrualNotificationWorkflow($resource_id, $level,$wf_module_id);
                    break;
                    /* Accrual Claim at Assesment Level */
                // case 69:
                // $accrual_claim_at_admin = new AccrualNotificationReportRepository();
                // $accrual_claim_at_admin->updateAccrualNotificationWorkflow($resource_id, $level);
                // break;
                    case 73:
                    case 74:
                    // $status = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 5 : 2;
                    $investigation_plan = new InvestigationPlanRepository();
                    $investigation_plan->updateInvestigationPlanWorkflow($resource_id,$level,1);
                    break;

                case $moduleRepo->interestAdjustmentModules()[0]://68 - Interest adjustment (Receipt)
                /* Interest Adjust - Receipt level*/
                $interest_adj_repo = new InterestAdjustmentRepository();
                $interest_adj_repo->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                break;

                case 26: //Default
                case 69://child school continuation
                case 70://child disability
                /* Payroll Status change. */
                $status_changes = new PayrollStatusChangeRepository();
                $status_changes->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                break;
                case 75:
                /* Payroll Retiree Mp Update */
                $payroll_retiree_mp_update_repo = new PayrollRetireeMpUpdateRepository();
                $payroll_retiree_mp_update_repo->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                break;
                case 79:
                $status = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 1 : 0;
                $employer_payroll_merge = new MergeDeMergeEmployerPayrollRepository();
                $employer_payroll_merge->updateOnWfApproval($resource_id,$level,$workflow->nextLevel($skip, $input['wf_definition']),$status);
                break;

                case 80:
                /* HSP HCP Vetting Bill */
                $hsp_bill = new HspBillSummaryRepository();
                $hsp_bill->updateHspBillWorkflow($resource_id,$level,1);
                break;
                case 81:
                $status = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 1 : 0;
                $contrib_modification = new ContributionModificationRepository();
                $contrib_modification->updateWorkflowStatus($resource_id,$level,$workflow->nextLevel($skip, $input['wf_definition']),$status);
                break;

                case 82:
                $status = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 1 : 0;
                $payment_rate = new ContributionInterestRateRepository();
                $payment_rate->updateRateOnUpdateWorkflow($resource_id,$level,$workflow->nextLevel($skip, $input['wf_definition']),$status);
                break;

            }
            if ($continue['success']) {
                /* Create a entry log for the next workflow */
                $data = [
                    'resource_id' => $resource_id,
                    'sign' => $sign,
                    'select_user' => $input['select_user'],
                    //'wf_definition' => $input['wf_definition'],
                ];
                if ($continue['jump_level']) {
                    $wf_definition = (new WfDefinitionRepository())->query()->where(["wf_module_id" => $wf_module_id, "level" => $continue['jump_level']])->first();
                    $wf_definition_id = $wf_definition->id;
                    $data['wf_definition'] = $wf_definition_id;
                } else {
                    $data['wf_definition'] = $input['wf_definition'];
                }
                $workflow->forward($data);

                if ($wf_module_id == 60 && $level == 9) {
                    $this->sendInstalmentApprovalEmail($event, 1);
                }
                if ($wf_module_id == 60 and $level == 10) {
                    $this->updateInstalmentChecker($event);
                }
            }
        } else {
            //throw new WorkflowException("End Block ... " . $input['wf_definition']);
            /* Workflow completed */
            /* Process for specific resource on workflow completion */
            switch ($wf_module_id) {
                case 1:
                /* Interest Waive / WriteOff */
                $booking_interests = new BookingInterestRepository();
                $booking_interests->approveInterestWriteOff($resource_id);
                break;

                case $moduleRepo->bookingInterestModules()[0]://2
                /* Interest Adjust - Single */
                $booking_interests = new BookingInterestRepository();
                $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;

                if($wf_done == 1){
                    $booking_interests->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                    $booking_interests->interestAdjustmentApprove($resource_id);
                }

                break;

                case 10:
                case 3:
                    //logger('reached here ...');
                /* Claim and Notification Processing */
                /* Table to update wf_done : Notification Report */
                $claim_compensations = new ClaimCompensationRepository();
                $notification_reports = new NotificationReportRepository();
                $claim_compensation = $claim_compensations->claimCompensationApprove($resource_id, 1);
                $this->updateWfDone($notification_reports->findOrThrowException($resource_id));
                break;

                case 4:
                /* Notification Report Rejection */
                /* Table to update wf_done : Notification Report  */
                $notification_reports = new NotificationReportRepository();
                $this->updateWfDone($notification_reports->findOrThrowException($resource_id));
                break;
                case $moduleRepo->receiptVerificationModule()[0]:
                case $moduleRepo->receiptVerificationModule()[1]:
                case $moduleRepo->receiptVerificationAdvanceModule()[0]:
                /* Contribution Receipt */
                $this->contributionReceiptLevelApproveProcess($level, $resource_id, $wf_module_id);
                $this->contributionReceiptCompletion($resource_id, $wf_module_id);
                break;
                case $moduleRepo->employerRegistrationModule()[0]:
                case $moduleRepo->employerRegistrationModule()[1]:
                $employers = new EmployerRepository();
                switch ($wfTrack->status) {
                    case 3:
                            //Declined
                    $employers->activate($resource_id, 3);
                    $employer = $employers->findWithoutScopeOrThrowException($resource_id);

                    $this->updateWfDone($employer);

                    if ($wfTrack->source == 2) {
                        $this->employerRegistrationRejected($wfTrack);
                    }
                            /*$incident->status = 3;
                            $incident->wf_done = 1;
                            $incident->wf_done_date = Carbon::now();*/
                            break;
                            case 1:
                            //Approved
                            /* Employer Registration */
                            $employers->activate($resource_id);
                            $employer = $employers->findOrThrowException($resource_id);

                            $this->updateWfDone($employer);

                            if ($wfTrack->source == 2) {
                                $this->employerRegistrationCompleted($wfTrack);
                            } else {
                                /* 04-dec-2017 Employees will be inserted separate as WCR 3*/
                                //dispatch(new RegisterEmployeeList($employer, access()->id()));
                            }
                            break;
                        }
                        break;
                        case 7:
                        /* Contribution Legacy Receipt */
                        $this->contributionLegacyReceiptCompletion($resource_id);
                        break;
                        case 8:
                        /* Employer Verification. */
                        $result = $this->employerVerificationCompleted($resource_id);
                        if (!$result) {
                            throw new WorkflowException("Error Processing Request, Refresh and try again!");}
                            break;

                            case 9:
                            /* Interest Refund. */
                            $booking_interests = new BookingInterestRepository();
                            $interest_refund = BookingInterestRefund::query()->find($resource_id);
                            $this->updateWfDone($interest_refund);
                            $booking_interests->interestRefundWfComplete($resource_id);
                            break;

                            case 25:
                            /* Payroll Bank Detail Modification. */
                            $bank_updates = new PayrollBankUpdateRepository();
                            $bank_update = $bank_updates->find($resource_id);
                            $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                            $this->updateWfDone($bank_update,$wf_done);
                            if  ($wf_done == 1) {
                                /*update wf complete*/
                                $bank_updates->wfDoneComplete($resource_id);
                            }else{
                                /*update on declined*/
                                $bank_updates->wfDoneCompleteDeclined($resource_id);
                            }
                            break;

                case 26: //Default
                case 69://child school continuation
                case 70://child disability
                /* Payroll Status change. */
                $status_changes = new PayrollStatusChangeRepository();
                $status_change = $status_changes->find($resource_id);
                $status_changes->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                $this->updateWfDone($status_change,$wf_done);
                if  ($wf_done == 1) {
                    /*update wf complete*/
//                        $status_changes->wfDoneComplete($resource_id);
                }else{
                    /*update wf complete declined*/
                    $status_changes->wfDoneCompleteDeclined($resource_id);
                }
                break;

                case 27:
                /* Payroll Recovery. */
                $payroll_recoveries = new PayrollRecoveryRepository();
                $payroll_recovery = $payroll_recoveries->find($resource_id);
                $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                $this->updateWfDone($payroll_recovery,$wf_done);
                if  ($wf_done == 1) {
                    /*update wf complete*/
                    $payroll_recoveries->wfDoneComplete($resource_id);
                }
                break;
                case 28:
                case 63:
                /* Payroll Run Processing */
                $payroll_run_approvals = new PayrollRunApprovalRepository();

                    //TODO need to reset this check for payment processed <uncomment>
                    //$payroll_run_approvals->checkIfPaymentIsProcessed($level, $resource_id);
                $payroll_run_approval = $payroll_run_approvals->find($resource_id);
                $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                $this->updateWfDone($payroll_run_approval,$wf_done);
                if  ($wf_done == 1){
                    /*update*/
                    dispatch(new UpdateAfterPayrollRunApproval($payroll_run_approval->id));
                }
                break;
                case $moduleRepo->notificationIncidentApprovalModule()[0]:
                case $moduleRepo->notificationIncidentApprovalModule()[1]:
                case $moduleRepo->notificationIncidentApprovalModule()[2]:
                /* Notification Incident Approval */
                (new NotificationReportRepository())->completeIncidentApproval($resource_id, $wf_module_id);
                break;
                case $moduleRepo->claimBenefitModule()[0]:
                case $moduleRepo->claimBenefitModule()[1]:
                case $moduleRepo->maeRefundMemberModule()[0]:
                    //Claim Benefits
                    //Medical Aid Expense Refund for Employee/Employer

                /* Notification Incident Benefit Approval */
                (new NotificationReportRepository())->completeBenefitApproval($resource_id, $wf_module_id);
                break;
                /*                case $moduleRepo->missingContributionNotificationModule()[0]:
                                case $moduleRepo->unregisteredMemberNotificationModule()[0]:
                                case $moduleRepo->systemRejectedNotificationModule()[0]:
                                case $moduleRepo->rejectionNotificationApprovalModule()[0]:
                                    (new NotificationReportRepository())->completeEnforceableWorkflow($resource_id, $wf_module_id, $level);
                                    break;*/
                /*                case $moduleRepo->incorrectContributionNotificationModule()[0]:
                                case $moduleRepo->incorrectEmployeeInfoNotificationModule()[0]:
                                case $moduleRepo->voidNotificationModule()[0]:
                                case $moduleRepo->undoNotificationRejectionModule()[0]:
                                    (new NotificationReportRepository())->completeBenefitApproval($resource_id, $wf_module_id);
                                    break;*/
                                    case $moduleRepo->missingContributionNotificationModule()[0]:
                                    case $moduleRepo->unregisteredMemberNotificationModule()[0]:
                                    case $moduleRepo->systemRejectedNotificationModule()[0]:
                                    case $moduleRepo->rejectionNotificationApprovalModule()[0]:
                                    case $moduleRepo->rejectionNotificationApprovalModule()[1]:
                                    /*Notification Missing Contribution*/
                                    /*Unregistered Member Notification*/
                                    /*System Rejected Notification*/
                                    /*Rejection from Notification Approval*/
                                    (new NotificationReportRepository())->completeEnforceableWorkflow($resource_id, $wf_module_id, $level);
                                    break;
                                    case $moduleRepo->incorrectContributionNotificationModule()[0]:
                                    case $moduleRepo->incorrectContributionNotificationModule()[1]:
                                    case $moduleRepo->incorrectEmployeeInfoNotificationModule()[0]:
                                    case $moduleRepo->voidNotificationModule()[0]:
                                    case $moduleRepo->undoNotificationRejectionModule()[0]:
                                    case $moduleRepo->transferToDeathIncidentApprovalModule()[0]:
                    //Incorrect Contribution Notification
                    //Incorrect Employee Info
                    //Void Notification
                    //On Undo Rejection Workflow
                                    (new NotificationReportRepository())->completeInterruptingWorkflow($resource_id, $wf_module_id);
                                    break;
                                    case 29:
                                    /* Payroll Reconciliations  */
                                    $payroll_reconciliations = new PayrollReconciliationRepository();
                                    $payroll_reconciliation = $payroll_reconciliations->find($resource_id);
                                    $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                                    $this->updateWfDone($payroll_reconciliation,$wf_done);
                                    if  ($wf_done == 1) {
                                        /*update*/
                                        $payroll_reconciliations->updateOnWfComplete($resource_id);
                                    }
                                    break;

                                    case 30:
                                    /* Payroll Beneficiary updates  */
                                    $payroll_beneficiary_updates= new PayrollBeneficiaryUpdateRepository();
                                    $beneficiary_update = $payroll_beneficiary_updates->find($resource_id);
                                    $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                                    $this->updateWfDone($beneficiary_update,$wf_done);
                                    if  ($wf_done == 1) {
                                        /*update*/
                                        $payroll_beneficiary_updates->updateOnWfComplete($beneficiary_update);
                                    }
                                    break;
                                    case 35:
                                    /* Employer Advance Payment. */
                                    $this->employerAdvancePaymentCompleted($resource_id);
                                    break;
                                    case $moduleRepo->onlineNotificationRequestModule()[0]:
                                    $this->onlineNotificationRequestCompleted($wfTrack);
                                    break;

                                    case 37:
                                    case 38:
                                    /* Employer Business closure. */
                                    $closure_repo  = new EmployerClosureRepository();
                                    $closure = $closure_repo->find($resource_id);
                                    (new EmployerClosureRepository())->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                                    $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                                    $this->updateWfDone($closure,$wf_done);
                                    if  ($wf_done == 1) {
                                        /*update*/
                                        $closure_repo->updateOnWorkflowComplete($resource_id);
                                    }
                                    break;
                                    case $moduleRepo->acknowledgementLetterIssuanceModule()[0]:
                                    case $moduleRepo->benefitAwardLetterIssuanceModule()[0]:
                                    case $moduleRepo->complianceLetterIssuanceModule()[0]:
                                    case $moduleRepo->reminderLetterIssuanceModule()[0]:
                                    (new LetterRepository())->checkIfLetterFinalized($level, $resource_id, $wf_module_id);
                                    (new LetterRepository())->checkForDispatchInfo($level, $resource_id, $wf_module_id);
                                    break;
                                    case $moduleRepo->employerInspectionPlanModule()[0]:
                                    case $moduleRepo->employerInspectionPlanModule()[1]:
                                    (new InspectionRepository())->completeInspectionApproval($level, $resource_id, $wf_module_id);
                                    break;
                                    case $moduleRepo->employerInspectionAssessmentReviewModule()[0]:
                                    (new EmployerInspectionTaskRepository())->completeAssessmentReview($resource_id, $wf_module_id);
                                    break;
                                    case 51:
                                    /* Employer Business closure - Reopen */
                                    $closure_open_repo  = new EmployerClosureReopenRepository();
                                    $closure_open = $closure_open_repo->find($resource_id);
                                    $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                                    $this->updateWfDone($closure_open,$wf_done);
                                    if ($wf_done == 1) {
                                        /*update*/
                                        $closure_open_repo->updateOnWorkflowComplete($resource_id);
                                    }
                                    break;


                                    case 55:
                                    case 59:
                                    /* Employer Particular Changes */
                                    $employer_change_repo  = new EmployerParticularChangeRepository();
                                    $employer_change = $employer_change_repo->find($resource_id);
                                    $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                                    $this->updateWfDone($employer_change,$wf_done);
                                    if  ($wf_done == 1) {
                                        /*update*/
                                        $employer_change_repo->updateOnWfDoneComplete($resource_id);
                                    }
                                    break;

                                    case 56:
                                    /* Payroll Monthly pension updates */
                                    $mp_update_repo  = new PayrollMpUpdateRepository();
                                    $mp_update = $mp_update_repo->find($resource_id);
                                    $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                                    $this->updateWfDone($mp_update,$wf_done);
                                    if  ($wf_done == 1) {
                                        /*update*/
                                        $mp_update_repo->updateOnWfDoneComplete($resource_id);
                                    }
                                    break;

                                    case 64:
                                    /* OSH Audit list update */
                                    $osh_list = new OshAuditPeriodRepository();
                                    $osh_list->checkIfAssignedAll($resource_id, $level);
                                    $osh_list->approveOshAuditList($resource_id,$level);
                                    break;

                                    case 65:
                                    /* Investment Budget update */
                                    $investment_budget = new InvestimentBudgetRepository();
                                    $investment_budget->approveBudget($resource_id, $level);
                                    break;

                                    case 66:
                                    /* Employer De-Registrations Extension  */
                                    $extension_repo  = new EmployerClosureExtensionRepository();
                                    $closure_extension = $extension_repo->find($resource_id);
                                    $extension_repo->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                                    $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                                    $this->updateWfDone($closure_extension,$wf_done);
                                    if($wf_done == 1) {
                                        /*update*/
                                        $extension_repo->updateOnWorkflowComplete($resource_id);
                                    }
                                    break;

                                    /* Accrual Claim */
                                    case 71:
                                    case 72:
                                    $accrual_claim_at_admin = new AccrualNotificationReportRepository();
                                    $accrual_claim_at_admin->updateAccrualNotificationWorkflow($resource_id, $level,$wf_module_id);
                                    break;
                                    /* Investigation Plan  */
                                    case 73:
                                    case 74:
                                    $status = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 5 : 1;
                                    $investigation_plan = new InvestigationPlanRepository();
                                    $investigation_plan->updateInvestigationPlanWorkflow($resource_id,$level,$status);
                                    break;
                case $moduleRepo->interestAdjustmentModules()[0]://68 - Interest adjustment (Receipt)
                /* Interest Adjust - Receipt level*/
                $interest_adj_repo = new InterestAdjustmentRepository();
                $interest_adjustment = $interest_adj_repo->find($resource_id);
                $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                if($wf_done == 1){
                    $interest_adj_repo->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                }
                $this->updateWfDone($interest_adjustment,$wf_done);
                break;
                case 75:
                /* Payroll Retiree Mp Update */
                $payroll_retiree_mp_update_repo = new PayrollRetireeMpUpdateRepository();
                $payroll_retiree_mp_update = $payroll_retiree_mp_update_repo->find($resource_id);
                $wf_done = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 2 : 1;
                if($wf_done == 1){
                    $payroll_retiree_mp_update_repo->processWfActionAtLevels($workflow->currentWfTrack(), $input);
                }
                $this->updateWfDone($payroll_retiree_mp_update,$wf_done);

                break;
                case 79:
                $status = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 1 : 0;
                $employer_payroll_merge = new MergeDeMergeEmployerPayrollRepository();
                $employer_payroll_merge->updateOnWfApproval($resource_id,$level,$workflow->nextLevel($skip, $input['wf_definition']),$status);
                break;
                case 80:
                /* HSP HCP Vetting Bill */
                $hsp_bill = new HspBillSummaryRepository();
                $hsp_bill->updateHspBillWorkflow($resource_id,$level,1);
                break;
                case 81:
                /* Contrib Modification */
                $status = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 1 : 0;
                $contrib_modification = new ContributionModificationRepository();
                $contrib_modification->updateWorkflowStatus($resource_id,$level,$workflow->nextLevel($skip, $input['wf_definition']),$status);
                break;

                case 82:
                /* Payment Rate */
                $status = (new WfTrackRepository())->checkIfModuleDeclined($resource_id, $wf_module_id) ? 1 : 0;
                $payment_rate = new ContributionInterestRateRepository();
                $payment_rate->updateRateOnUpdateWorkflow($resource_id,$level,$workflow->nextLevel($skip, $input['wf_definition']),$status);
                break;

                
            }
        }
    }

    /**
     * Handle on reject workflow events.
     * @param $event
     * @throws \App\Exceptions\GeneralException
     */
    public function onRejectWorkflow($event)
    {
        $wfTrack = $event->wfTrack;
        $level = $event->level;
        $moduleRepo = new WfModuleRepository();

        $workflow = new Workflow(['wf_module_id' => $wfTrack->wfDefinition->wfModule->id, 'resource_id' => $wfTrack->resource_id]);
        $sign = -1;
        $prevLevel = $workflow->prevLevel();

        /* check if there is next level */
        if (!is_null($prevLevel)) {

            $data = [
                'resource_id' => $wfTrack->resource_id,
                'sign' => $sign,
                'level' => $level,
                'source' => $wfTrack->source,
            ];

            //if ($wfTrack->source == 2) {
            if ($prevLevel == 1) {
                /* Check if workflow rejected to online user .... */

                if ($prevLevel == 1) {
                    $wf_module_id = $wfTrack->wfDefinition->wfModule->id;
                    switch ($wf_module_id) {
                        case $moduleRepo->employerRegistrationModule()[0]:
                        case $moduleRepo->employerRegistrationModule()[1]:
                        /* Employer Registration */
                        $this->employerRegistrationRejected($wfTrack,5);
                        break;
                        case 35:
                        /* Employer Advance Payment Rejected. */
                        $resource = $wfTrack->resource;
                            //throw new WorkflowException("Success");
                        $this->employerAdvancePaymentRejected($resource);
                        $workflow->forward($data);
                        break;
                        case 8:
                        /* Employer Verification */
                        $this->employerVerificationRejected($wfTrack->resource_id);
                        break;
                        case $moduleRepo->onlineNotificationRequestModule()[0]:
                        $this->onlineNotificationRequestRejected($wfTrack);
                        $workflow->forward($data);
                        break;

                        case 59:
                        /* Employer particular changes - portal*/
                        (new EmployerParticularChangeRepository())->wfRejectToLevel1Portal($wfTrack);
                        /*  $workflow->forward($data);*/
                        break;
                        case 60:
                        if ($level == 1) {
                            $mac_request = DB::table('main.installment_requests')->where('id', $event->wfTrack->resource_id)->first();
                            DB::table('main.installment_requests')->where('id', $event->wfTrack->resource_id)->update(['status' => 2]);
                            $portal_request = DB::table('portal.installment_requests')->where('id', $mac_request->portal_request_id)
                            ->update(['status' => 2, 'rejection_reason' => $wfTrack->comments]);

                            $this->sendInstalmentApprovalEmail($event, 2);
                        }
                        break;
                        case 64:
                        $osh_list = new OshAuditPeriodRepository();
                        $osh_list->rejectOshAuditList($wfTrack->resource_id);
                        $workflow->forward($data);
                        break;
                        case 65:
                        $investment_budget = new InvestimentBudgetRepository();
                        $investment_budget->rejectBudget($wfTrack->resource_id);
                        $workflow->forward($data);
                        break;
                        case 71:
                        case 72:
                        $accrual_claim_at_admin = new AccrualNotificationReportRepository();
                        $accrual_claim_at_admin->updateAccrualNotificationWorkflow($wfTrack->resource_id, $level,$wf_module_id);
                        $workflow->forward($data);
                        break;
                        case 73:
                        case 74:
                        $investigation_plan = new InvestigationPlanRepository();
                        $investigation_plan->updateInvestigationPlanWorkflow($wfTrack->resource_id,$level,0);
                        $workflow->forward($data);
                        break;
                        case 79:
                        $employer_payroll_merge = new MergeDeMergeEmployerPayrollRepository();
                        $employer_payroll_merge->updateOnWfApproval($wfTrack->resource_id,$workflow->currentLevel(),$level);
                        $workflow->forward($data);
                        break;
                        case 80:
                        /* HSP HCP Vetting Bill */
                        $hsp_bill = new HspBillSummaryRepository();
                        $hsp_bill->updateHspBillWorkflow($wfTrack->resource_id,$level,0);
                        break;
                        case 81:
                        $contrib_modification = new ContributionModificationRepository();
                        $contrib_modification->updateWorkflowStatus($wfTrack->resource_id,$workflow->currentLevel(),$level);
                        $workflow->forward($data);
                        break;

                        case 82:
                        $payment_rate = new ContributionInterestRateRepository();
                        $payment_rate->updateRateOnUpdateWorkflow($wfTrack->resource_id,$workflow->currentLevel(),$level);
                        $workflow->forward($data);
                        break;

                        
                        default:
                        $workflow->forward($data);
                        break;
                    }
                } else {
                    //throw new WorkflowException("Failure");
                    $workflow->forward($data);
                }
            } else {

                switch ($wfTrack->wfDefinition->wfModule->id) {
                    case 79:
                    $employer_payroll_merge = new MergeDeMergeEmployerPayrollRepository();
                    $employer_payroll_merge->updateOnWfApproval($wfTrack->resource_id,$workflow->currentLevel(),$level);
                    break;
                    case 81:
                    $employer_payroll_merge = new ContributionModificationRepository();
                    $employer_payroll_merge->updateWorkflowStatus($wfTrack->resource_id,$workflow->currentLevel(),$level);
                    break;

                    case 82:
                    $payment_rate = new ContributionInterestRateRepository();
                    $payment_rate->updateRateOnUpdateWorkflow($wfTrack->resource_id,$workflow->currentLevel(),$level);
                    break;
                    default:
                    break;
                }

                $workflow->forward($data);
            }
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\ApproveWorkflow',
            'App\Listeners\WorkflowEventSubscriber@onApproveWorkflow'
        );

        $events->listen(
            'App\Events\NewWorkflow',
            'App\Listeners\WorkflowEventSubscriber@onNewWorkflow'
        );

        $events->listen(
            'App\Events\RejectWorkflow',
            'App\Listeners\WorkflowEventSubscriber@onRejectWorkflow'
        );
    }

    /**
     * @param $resource_id
     * @param $module
     * @throws WorkflowException
     */
    private function contributionReceiptCompletion($resource_id, $module)
    {
        $receiptCode = new ReceiptCodeRepository();
        $receiptRepo = new ReceiptRepository();
        $wfModuleRepo = new WfModuleRepository();

        $receipt = $receiptRepo->query()->where(['id' => $resource_id])->first();

        //Check if it is contribution receipt with advance ...
        //$wfModule = $wfModuleRepo->find($module);
        if (in_array($module, $wfModuleRepo->receiptVerificationAdvanceModule(), true)) {
            //This is verification with advance
            //Check if there is arrears & dishonoured cheques
            $countMissingReceipt = DB::table('bookings_mview')->where('employer_id', $receipt->employer_id)->where('ispaid', 0)->count();
            if ($countMissingReceipt) {
                throw new WorkflowException("Action cancelled, This receipt has arrears and/or dishonoured cheques which need to be cleared!");
            }
        }

        $receipt->iscomplete = 1;
        $receipt->isverified = 1;
        $receipt->save();

        /*if (!$receiptCode->checkIfAllContributionsHaveLinkedFile($resource_id)) {
            throw new WorkflowException(trans('exceptions.backend.workflow.missing_contribution_file'));
        }
        if ($receipt->checkIfAllContributionsAreUploaded($resource_id)) {
            $receipt->query()->where(['id' => $resource_id])->update(['iscomplete' => 1]);
        } else {
            throw new WorkflowException(trans('exceptions.backend.workflow.file_not_uploaded'));
        }*/
    }

    /**
     * @param $level
     * @param $resource_id
     * @param $module
     * @throws WorkflowException
     */
    private function contributionReceiptLevelApproveProcess($level, $resource_id, $module)
    {
        $approveLevel = (new WfModuleRepository())->receiptVerificationApproveLevel($module);
        if ($approveLevel == $level) {
            /* After verifying receipt information at compliance department */
            $receiptRepo = new ReceiptRepository();
            $receipt = $receiptRepo->query()->find($resource_id);
            //check if receipt has been reconciled
            if (!$receipt->hasreconciled) {
                throw new WorkflowException("Action cancelled, this receipt has not been reconciled...");
            }

            $receiptRepo->query()->where(['id' => $resource_id])->update(['isverified' => 1]);
        }
    }

    /* Legacy Receipt */
    private function contributionLegacyReceiptCompletion($resource_id)
    {
        $legacyReceiptCode = new LegacyReceiptCodeRepository();
        $legacyReceipt = new LegacyReceiptRepository();
        if (!$legacyReceiptCode->checkIfAllContributionsHaveLinkedFile($resource_id)) {
            throw new WorkflowException(trans('exceptions.backend.workflow.missing_contribution_file'));
        }
        if ($legacyReceipt->checkIfAllContributionsAreUploaded($resource_id)) {
            $legacyReceipt->query()->where(['id' => $resource_id])->update(['iscomplete' => 1, 'approved_by' => access()
                ->id
                ()]);
        } else {
            throw new WorkflowException(trans('exceptions.backend.workflow.file_not_uploaded'));
        }
    }

    private function contributionLegacyReceiptLevelApproveProcess($level, $resource_id)
    {
        switch ($level) {
            case 2:
            /* After verifying receipt information at compliance department */
            $legacy_receipt = new LegacyReceiptRepository();
            $legacy_receipt->query()->where(['id' => $resource_id])->update(['isverified' => 1, 'contr_approval'
                => 1]);
            break;
        }
    }

    /**
     * @param $level
     * @param $resource_id
     * @throws WorkflowException
     * @throws \App\Exceptions\GeneralException
     */
    private function claimAssessmentLevelApproveProcess($level, $resource_id, $wf_module_id)
    {
        $workflow = new Workflow(['wf_module_group_id' => 3, 'resource_id' => $resource_id]);
        $assessment_level = $workflow->claimAssessmentLevel();
        switch ($level) {
            case $assessment_level:
            /* Claim Assessment Doctors Modification */
            $notification_reports = new NotificationReportRepository();
                //check if nature of incident is  on notification reports
            $notification_report = $notification_reports->findOrThrowException($resource_id);
            if (!$notification_report->nature_of_incident_cv_id) {
                throw new WorkflowException(trans('exceptions.backend.claim.nature_of_incident_not_modified'));
            }
                // check if assessment is done
            if ($notification_report->notificationDisabilityStates()->count()){
                if (!$notification_report->notificationDisabilityStates()->has('notificationDisabilityStateAssessment')->count()){
                    throw new WorkflowException(trans('exceptions.backend.claim.assessment_confirm'));
                }
            }
            break;
        }
    }

    private function updateWfDone(Model $model, $wf_done = 1)
    {
        $model->update(['wf_done' => $wf_done, 'wf_done_date' => Carbon::now()]);
    }

    public function employerAdvancePaymentRejected(Model $advancePayment)
    {
        //Update Employer Advance Payment Status
        $advancePayment->status = 2; //Rejected
        $advancePayment->save();
        if ($advancePayment->advance_payment_id) {
            $portalAdvancePayment = (new PortalAdvancePaymentRepository())->find($advancePayment->advance_payment_id);
            $portalAdvancePayment->approval_status = 4;
            $portalAdvancePayment->save();
        }

    }

    /**
     * @param $resource_id
     * @return bool
     * @throws \App\Exceptions\GeneralException
     */
    public function employerAdvancePaymentCompleted($resource_id)
    {
        $repo = new EmployerAdvancePaymentRepository();
        $employerRepo = new EmployerRepository();
        $advance_payment = $repo->query()->find($resource_id);

        // Update workflow done and workflow done date
        $advance_payment->wf_done = 1;
        $advance_payment->wf_done_date = Carbon::now();
        $advance_payment->save();

        $api_key = bcrypt(env("PORTAL_API_KEY"));

        //Send Email

        //Update Employer Advance Payment Status
        $advance_payment->status = 1;
        $advance_payment->save();


        //update portal
        if ($advance_payment->advance_payment_id) {
            $portalAdvancePayment = (new PortalAdvancePaymentRepository())->find($advance_payment->advance_payment_id);
            $portalAdvancePayment->approval_status = 2;
            $portalAdvancePayment->save();
        }

        //Post notification on the portal.
        $url = env('PORTAL_APP_URL') . "/api/post_notification";
        $postFields = "{ \"user\":\"{$advance_payment->user->id}\", \"api_key\":\"{$api_key}\", \"case\":\"verification\", \"case_type\":\"approved\", \"resource_id\":\"{$advance_payment->id}\" }";
        //$this->sendPostJson($url, $postFields);

        return true;
    }

    /**
     * @param $resource_id
     * @throws \App\Exceptions\GeneralException
     */
    private function employerVerificationCompleted($resource_id)
    {

        try {
          $return =  DB::transaction(function () use ($resource_id) {
            $repo = new OnlineEmployerVerificationRepository();
            $employerRepo = new EmployerRepository();
            $verification = $repo->query()->find($resource_id);
        //Update verification for employer merged to another employers
        //Check if employer is merged ...
            $check = $employerRepo->query()->where("id", $verification->employer_id)->whereNotNull("deleted_at")->withTrashed();
            if ($check->count()) {
                $check = $check->first();
                $merger = $employerRepo->query()->select(['id', 'reg_no'])->where("id", $check->duplicate_id)->first();
                $verification->employer_id_reserve = $verification->employer_id;
                $verification->reg_no_reserve = $verification->reg_no;
                $verification->employer_id = $merger->id;
                $verification->reg_no = $merger->reg_no;
                $verification->save();
            }

        //Check if this employer had been approved and verified before this request
            $check = $repo->query()->where(['employer_id' => $verification->employer_id, 'status' => 1])->count();
            if ($check) {
            //There is already user verified for this employer
            //Remove comment below if this check is enforced.
            //throw new WorkflowException(trans('exceptions.backend.workflow.verification_exist'));
            }
        // Update workflow done and workflow done date
            $verification->wf_done = 1;
            $verification->wf_done_date = Carbon::now();
            $verification->save();

            $this->saveArrearCommencement([
                'reg_no' => $verification->employer_id,
                'commencement_source' => $verification->commencement_source,
                'doc' => $verification->doc,
                'start_date' => $verification->project_start_date,
                'payroll_id' => $verification->payroll_id,
            ]);
        // Send Email.
            $verification->notify(new ApprovedVerification($verification->reg_no, $verification->wf_done_date, $verification->created_at));
        // Send Message.
        //dispatch(new SendSms($verification->user, trans("sms.employer.verification.approved", ['name' => $verification->user->name, 'reg_no' => $verification->reg_no])));
        // Load employee list uploaded from the portal.
            $api_key = bcrypt(env("PORTAL_API_KEY"));
        // Transfer Employer to online user and add all the document attachments for verification to employer.
            $url = env('PORTAL_APP_URL') . "/api/employer_user";
            $postFields = "{ \"employer\":\"{$verification->employer_id}\", \"api_key\":\"{$api_key}\", \"verification\":\"{$verification->id}\" }";
            $this->sendPostJson($url, $postFields);

            $url = env('PORTAL_APP_URL') . "/api/post_employees";
            $postFields = "{ \"employer\":\"{$verification->employer_id}\", \"api_key\":\"{$api_key}\" }";
            $this->sendPostJson($url, $postFields);

        //Update Verification Status
            $verification->status = 1;
            $verification->save();

        //Post notification on the portal.
            $url = env('PORTAL_APP_URL') . "/api/post_notification";
            $postFields = "{ \"user\":\"{$verification->user->id}\", \"api_key\":\"{$api_key}\", \"case\":\"verification\", \"case_type\":\"approved\", \"resource_id\":\"{$verification->id}\" }";
            $this->sendPostJson($url, $postFields);

            return true;
        });
          return $return;
      } catch (\Exception $e) {
        logger($e);
    }catch (\Error $e) {
        logger($e);
    }

    return $return;
}

    /**
     * @param $wfTrack
     * @throws \App\Exceptions\GeneralException
     */
    private function employerRegistrationCompleted($wfTrack)
    {
        //get wftrack of the first level to retrieve user
        $wfRepo = new WfTrackRepository();
        $firstTrack = $wfRepo->query()->where(['resource_id' => $wfTrack->resource_id])->whereHas("wfDefinition", function ($query) {
            $query->whereHas("wfModule", function ($query) {
                $query->where("id", 6);
            });
        })->orderBy("id", "asc")->first();

        $repo = new EmployerRepository();
        $employer = $repo->findWithoutScopeOrThrowException($wfTrack->resource_id);
        // Send Email.
        $employer->notify(new ApprovedRegistration($employer->name, $employer->wf_done_date, $employer->created_at));
        // Send Message.
        //dispatch(new SendSms($firstTrack->user, trans("sms.employer.registration.approved", ['name' => $firstTrack->user->name, 'organization' => $employer->name])));
        // Load uploaded employee list uploaded from the portal.
        $api_key = bcrypt(env("PORTAL_API_KEY"));
        $url = env('PORTAL_APP_URL') . "/api/post_employees";
        $postFields = "{ \"employer\":\"{$employer->id}\", \"api_key\":\"{$api_key}\" }";
        $this->sendPostJson($url, $postFields);

        //Post notification on the portal.
        $url = env('PORTAL_APP_URL') . "/api/post_notification";
        $postFields = "{ \"user\":\"{$firstTrack->user->id}\", \"api_key\":\"{$api_key}\", \"case\":\"registration\", \"case_type\":\"approved\", \"resource_id\":\"{$employer->id}\" }";
        $this->sendPostJson($url, $postFields);
    }

    private function employerVerificationRejected($resource_id)
    {
        $repo = new OnlineEmployerVerificationRepository();
        $verification = $repo->query()->find($resource_id);
        // Send Email
        $verification->notify(new RejectedVerification($verification->reg_no, $verification->created_at));
        // Send Message
        //dispatch(new SendSms($verification->user, trans("sms.employer.verification.rejected", ['name' => $verification->user->name, 'reg_no' => $verification->reg_no])));
        // Update Rejection Status on target table
        $verification->status = 2;
        $verification->save();

        //Post notification on the portal.
        $api_key = bcrypt(env("PORTAL_API_KEY"));
        $url = env('PORTAL_APP_URL') . "/api/post_notification";
        $postFields = "{ \"user\":\"{$verification->user->id}\", \"api_key\":\"{$api_key}\", \"case\":\"verification\", \"case_type\":\"rejected\", \"resource_id\":\"{$verification->id}\" }";
        $this->sendPostJson($url, $postFields);

    }

    /**
     * @param $wfTrack
     * @throws \App\Exceptions\GeneralException
     */
    private function employerRegistrationRejected($wfTrack, $status=3)
    {
        //get wftrack of the first level to retrieve user
        $wfRepo = new WfTrackRepository();
        $firstTrack = $wfRepo->query()->where(['resource_id' => $wfTrack->resource_id])->whereHas("wfDefinition", function ($query) {
            $query->whereHas("wfModule", function ($query) {
                $query->where("wf_module_group_id", 6);
            });
        })->orderBy("id", "asc")->first();

        $repo = new EmployerRepository();
        $employer = $repo->findWithoutScopeOrThrowException($wfTrack->resource_id);
        // Send Email
        $employer->notify(new RejectedRegistration($employer->name, $employer->created_at));
        // Send Message
        //dispatch(new SendSms($firstTrack->user, trans("sms.employer.registration.rejected", ['name' => $firstTrack->user->name, 'organization' => $employer->name])));
        // Update Rejection Status on target table
        // $employer->approval_id = 3;
        $employer->approval_id = $status;
        $employer->save();

        //Post notification on the portal.
        $api_key = bcrypt(env("PORTAL_API_KEY"));
        $url = env('PORTAL_APP_URL') . "/api/post_notification";
        $postFields = "{ \"user\":\"{$firstTrack->user->id}\", \"api_key\":\"{$api_key}\", \"case\":\"registration\", \"case_type\":\"rejected\", \"resource_id\":\"{$employer->id}\" }";
        $this->sendPostJson($url, $postFields);
    }

    private function onlineNotificationRequestRejected(Model $wfTrack)
    {
        $wfRepo = new WfTrackRepository();
        $repo = new PortalIncidentRepository();
        $incident = $repo->query()->find($wfTrack->resource_id);
        $incident->status = 2;
        $incident->save();
        return true;
    }

    /**
     * @param Model $wfTrack
     * @return bool
     * @throws \App\Exceptions\GeneralException
     */
    private function onlineNotificationRequestCompleted(Model $wfTrack)
    {
        $wfRepo = new WfTrackRepository();
        $repo = new PortalIncidentRepository();
        $incidentRepo = new NotificationReportRepository();
        $incident = $repo->query()->find($wfTrack->resource_id);
        switch ($wfTrack->status) {
            case 3:
                //Declined
            $incident->status = 3;
            $incident->wf_done = 1;
            $incident->wf_done_date = Carbon::now();
            $incident->incident_staging_cv_id = (new CodeValuePortalRepository())->ISINCDECLND();
            $incident->save();
            break;
            case 1:
                //Approved
                //Register Notification
            $created = $incidentRepo->createFromOnline($incident);
            $incident->status = 1;
            $incident->wf_done = 1;
            $incident->wf_done_date = Carbon::now();
            $incident->notification_report_id = $created->id;
            $incident->incident_staging_cv_id = (new CodeValuePortalRepository())->ISINCAPRVD();
            $incident->save();
                //update verified status
            $incident->documents()->newPivotStatement()->where(pg_mac_portal() . '.document_incident.incident_id', '=', $incident->id)->update(["isverified" => 1]);
                //post document to DMS
            dispatch(new RecheckPostingDocumentDms($incident->id));
            break;
        }
        return true;
    }

    private function onlineNotificationRequestDeclined(Model $wfTrack)
    {
        $wfRepo = new WfTrackRepository();
        $repo = new PortalIncidentRepository();
        $incident = $repo->query()->find($wfTrack->resource_id);
        $incident->status = 3;
        $incident->save();
        return true;
    }

    public function sendInstalmentApprovalEmail($event, $flag){
        try {

            $mac_request = DB::table('main.installment_requests')->where('id', $event->wfTrack->resource_id)->first();
            $portal_request = DB::table('portal.installment_requests')->where('id', $mac_request->portal_request_id)->first();
            $description = $mac_request->description;

            $employer = Employer::find($mac_request->employer_id);
            $portal_user = PortalUser::find($mac_request->user_id);

            $portal_users = InstallmentAccountant::where('installment_request_id', $portal_request->id)->get();

            foreach ($portal_users as $user) {
                $name = $user->firstname.' '.$user->lastname;

                $this->sendEmail($user, $employer, $description, $name, $flag);

            }
            $name = $employer->name;
            $this->sendEmail($employer, $employer, $description, $name, $flag);
            $name = $portal_user->firstname.' '.$portal_user->lastname;
            $this->sendEmail($portal_user, $employer, $description, $name, $flag);

        } catch (\Exception $e) {
            logger($e->getMessage());
        }
        catch (\Error $e) {
            logger($e->getMessage());
        }
    }

    public function sendEmail($user, $employer, $description, $name, $flag){
        \Notification::send($user, new InstalmentApproval($user, $employer, $description, $name, $flag));
    }

    public function checkIfFilledFields($event, $level){
        $arrayError = $this->ifInstalmentDetailsFilled($event, $level);
        if ($arrayError['filled'] == false) {
            if ($level == 7) {
                throw new WorkflowException("Error Processing Request, ".$arrayError['error']." is required!");
            } elseif($level == 10) {
                throw new WorkflowException("".$arrayError['error']." are required!");
            }
        }
    }

    public function updateInstalmentChecker($event){
        $installment = DB::table('main.installment_requests')->where('id',$event->wfTrack->resource_id)->first();
        if(!is_null($installment)){
            $installment = DB::table('portal.installment_requests')->where('id',$installment->portal_request_id)->first();
            $checkerRepo = new CheckerRepository();
            $codeValue = new CodeValueRepository();
            $checkerRepo->query()->where('checker_category_cv_id',$codeValue->INSTMOUUPLOAD())->where('resource_id',$installment->id)->update(['status' =>1]);
        }
        
    }

    public function ifInstalmentDetailsFilled($event,$level){
        $arrayError = [];
        $arrayError['filled'] = true;

        switch ($level) {
            case 7:
            $agreement_date = DB::table('main.installment_requests')->where('id',$event->wfTrack->resource_id)->where('agreement_date', null)->first();
            if (!is_null($agreement_date)) {
                $arrayError['filled'] = false;
                $arrayError['error'] = 'agreement_date';
            }
            break;
            case 10:
            $letter_date = DB::table('main.installment_requests')->where('id',$event->wfTrack->resource_id)->where('letter_date', null)->first();
            if (!is_null($letter_date)) {
                $arrayError['filled'] = false;
                $arrayError['error'] = 'letter details';
            }
            $installment = DB::table('main.installment_requests')->where('id',$event->wfTrack->resource_id)->first();
            if(!is_null($installment)){
                $uploaded_mou = DB::table('portal.installment_requests')->where('id',$installment->portal_request_id)->where('is_submitted', false)->first();
                if (!is_null($uploaded_mou)) {
                    $arrayError['filled'] = false;
                    $arrayError['error'] = isset($arrayError['error']) ? $arrayError['error'].' and signed mou' : 'signed mou';
                }
            }
            break;

            default:
                # code...
            break;
        }

        return $arrayError;
    }

    public function saveMouAgreementDate(Request $request, $instalment_id){
        $installment = DB::table('main.installment_requests')->where('installment_requests.id',$instalment_id)
        ->update([
            'legal_id' => auth()->user()->id,
            'agreement_date' => Carbon::parse($request->agreement_date)->format('Y-m-d'),
        ]);

        if($installment){
            return response()->json(array('success'=>'Agreement date updated successfully'));
        }

    }

    public function saveLetterDetails(Request $request, $instalment_id){
        $installment = DB::table('main.installment_requests')->where('installment_requests.id',$instalment_id)
        ->update([
            'rmo_id' => auth()->user()->id,
            'letter_date' => Carbon::parse($request->letter_date)->format('Y-m-d'),
            'letter_reference' => $request->letter_reference,
        ]);

        if($installment){
            return response()->json(array('success'=>'Letter details updated successfully'));
        }

    }

}