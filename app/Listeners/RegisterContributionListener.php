<?php

namespace App\Listeners;

use App\Events\RegisterContribution;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterContributionListener
{
    /**
     * Create the event listener.
     *
     * RegisterContributionListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param RegisterContribution $event
     */
    public function handle(RegisterContribution $event)
    {
        //
    }
}
