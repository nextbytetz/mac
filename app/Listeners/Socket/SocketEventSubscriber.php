<?php

namespace App\Listeners\Socket;

use App\Jobs\Sockets\SendSocketNotification;
use App\Notifications\System\UserNotification;
use App\Repositories\Backend\Access\UserRepository;
use App\Repositories\Backend\Workflow\WfDefinitionRepository;
use App\Repositories\Backend\Workflow\WfModuleRepository;
use Illuminate\Support\Facades\Notification;
/*use Jenssegers\Agent\Agent;*/

class SocketEventSubscriber
{

    public $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function onBroadcastWorkflowUpdated($event)
    {
        $link = "";
        $id = $event->resource_id;
        $wf_module_id = $event->wf_module_id;
        $level = $event->level;
        $wfModules = new WfModuleRepository();
        $wfDefinitions = new WfDefinitionRepository();
        $wf_definition = $wfDefinitions->query()->where(['level' => $level, 'wf_module_id' => $wf_module_id])->first();
        $wf_module = $wfModules->query()->where("id", $wf_module_id)->first();
        $users = $wf_definition->users()->where("users.id", "<>", access()->id())->get()->pluck("id")->all();
        switch ($wf_module->wfModuleGroup->id) {
            case 1:
                /* Interest Waiving */
                $link = route("backend.compliance.employer.write_off_interest_approve", $id);
                break;
            case 2:
                /* Interest Adjustment */
                $link = route("backend.compliance.employer.interest_profile", $id);
                break;
            case 3:
                /* Claim & Notification Processing */
                $link = route("backend.claim.notification_report.profile", $id);
                break;
            case 4:
                /* Notification Rejection */
                $link = route("backend.claim.notification_report.profile", $id);
                break;
            case 5:
                /* Contribution Receipt */
                $link = route("backend.finance.receipt.profile_by_request", $id);
                break;
            case 6:
                /* Employer registration */
                $link = route("backend.compliance.employer_registration.profile", $id);
                break;
            case 7:
                /* Contribution Receipt Legacy */
                $link = route("backend.finance.legacy_receipt.profile", $id);
                break;
            case 8:
                /* Online Employer Verification */
                $link = route("backend.compliance.employer.verification_profile", $id);
                break;
            default:
        }
        if ($wf_module_id != 5) {
            //Restrict contribution receipt to broadcast workflow ...
            $message = getLanguageBlock('backend.lang.workflow.new-workflow-broadcast', ['link' => $link]);
//            $this->sendMessage($message, $users);
        }
    }

    public function onAssignedInvestigation($event)
    {
        /* notification_report_id */
        $id = $event->id;
        /*Array of user_id*/
        $users_list = array_diff($event->users, [access()->id()]);
        $users = $this->user->query()->find($users_list);

        $title = trans("labels.backend.claim.assigned_investigation");
        $message = trans("labels.backend.claim.assigned_investigation_helper");
        $url = route("backend.claim.notification_report.profile", $id);
        $this->saveMessage($title, $message, $url, $users);

        $message = getLanguageBlock('backend.lang.notification.assigned-investigation', ['link' => $url]);
//        $this->sendMessage($message, $users_list);
    }

    public function onUnassignedInvestigation($event)
    {
        /* Single user */
        $user = $event->user;
    }

    public function onNewNotificationReport($event)
    {
        /* notification_report_id */
        $id = $event->id;
        $title = trans("labels.backend.claim.new_notification");
        $message = trans("labels.backend.claim.new_notification_helper");
        $url = route("backend.claim.notification_report.profile", $id);
        $wfDefinitions = new WfDefinitionRepository();
        /* Level 3 of Supervisor, Notification & Claim processing */
        //$wf_definition = $wfDefinitions->query()->where(['level' => 3, 'wf_module_id' => 3])->first();
        //$users = $wf_definition->users()->where("users.id", "<>", access()->id())->get();
        $users = $this->user->query()->whereHas("permissions", function ($query) {
            $query->where("name", "assign_investigation");
        })->orWhereHas("roles", function ($query) {
            $query->whereHas("permissions", function ($subQuery) {
                $subQuery->where("name", "assign_investigation");
            });
        })->get();
        if ($users->count()) {
            $this->saveMessage($title, $message, $url, $users);
            $users = $users->pluck("id")->all();
        } else {
            $users = [];
        }

        $message = getLanguageBlock('backend.lang.notification.new-notification-report', ['link' => $url]);
//        $this->sendMessage($message, $users);
    }

    private function sendMessage($message, $users)
    {
        /*$agent = new Agent();*/
        if (PHP_OS == "Linux") {
            if (env("ALLOW_BROADCAST", 0)) {
                $job = (new SendSocketNotification("$message", $users))->onConnection('database');
                dispatch($job);
            }
        }
    }

    private function saveMessage($title, $message, $url, $users)
    {
        Notification::send($users, new UserNotification($title, $message, $url));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Sockets\BroadcastWorkflowUpdated',
            'App\Listeners\Socket\SocketEventSubscriber@onBroadcastWorkflowUpdated'
        );

        $events->listen(
            'App\Events\Sockets\Notification\AssignedInvestigation',
            'App\Listeners\Socket\SocketEventSubscriber@onAssignedInvestigation'
        );

        $events->listen(
            'App\Events\Sockets\Notification\NewReport',
            'App\Listeners\Socket\SocketEventSubscriber@onNewNotificationReport'
        );

        $events->listen(
            'App\Events\Sockets\Notification\UnassignedInvestigation',
            'App\Listeners\Socket\SocketEventSubscriber@onUnassignedInvestigation'
        );

    }

}