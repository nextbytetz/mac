<?php

namespace App\Listeners;


use App\Jobs\Claim\PostNotificationDms;
use App\Repositories\Backend\Operation\Claim\NotificationReportRepository;

class EmployeeEventSubscriber
{

    public function onEmployeeUpdated($event)
    {
        $id = $event->employee;
        $incidents = (new NotificationReportRepository())->query()->where("employee_id", $id)->get();
        //dd($incidents);
        foreach ($incidents as $incident) {
            //Post any possible update to e-office
            dispatch(new PostNotificationDms($incident));
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\EmployeeUpdated',
            'App\Listeners\EmployeeEventSubscriber@onEmployeeUpdated'
        );

    }

}