<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use App\Services\Finance\FinYear;
use App\Services\Finance\Facades\FinYear as FinYearFacade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        //Add this custom validation rule.
        Validator::extend('alpha_spaces', function ($attribute, $value) {
        // This will only accept alpha and spaces.
        // If you want to accept hyphens use: /^[\pL\s-]+$/u.
            return preg_match('/^([a-zA-Z \']*)$/', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
            $this->app->register(\Orangehill\Iseed\IseedServiceProvider::class);
        }
        $this->registerFinYear();
        $this->registerFinYearFacade();
    }

    private function registerFinYear()
    {
        $this->app->bind('fin_year', function ($app, $params) {
            return new FinYear($params[0]);
        });
    }

    private function registerFinYearFacade()
    {
        $this->app->booting(function () {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('FinYear', FinYearFacade::class);
        });
    }

}
