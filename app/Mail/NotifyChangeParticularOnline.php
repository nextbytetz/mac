<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Log;

class NotifyChangeParticularOnline extends Mailable
{


    /**
     * Create a new message instance.
     *
     * @return void


     */
    public function __construct($data)
    {
        $this->send_data = $data;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails/operation/compliance/change_particular_email',['data'=>$this->send_data])
        ->from('noreply.mac@wcf.go.tz')
        ->subject('WCF Change Of Particulars Request');
    }
}
