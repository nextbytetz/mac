<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Log;

class InvestigationPlanReminder extends Mailable
{


    /**
     * Create a new message instance.
     *
     * @return void


     */
    public function __construct($data)
    {
        $this->send_data = $data;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->send_data);
        return $this->view('emails/operation/claim/investigation_plan/investigation_reminder',['data'=>$this->send_data])
        ->from('juma.wahabu@wcf.go.tz')
        ->subject('Investigation Plan Reminder');
    }
}
