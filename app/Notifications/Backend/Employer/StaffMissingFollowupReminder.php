<?php

namespace App\Notifications\Backend\Employer;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StaffMissingFollowupReminder extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $input = [];




    /**
     * ApprovedRegistration constructor.
     * @param $name
     * @param $approve_date
     * @param $date
     */
    public function __construct(array $input )
    {
        $this->input = $input;//notify staff


    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if(env('TESTING_MODE') == 0){
            $data_input = $this->input;
            $subject = $data_input['subject'];
            $elevation_level = $data_input['elevation_level'];

            $cm = User::query()->where('designation_id', 5)->where('unit_id', 15)->first();
            $cm_email = $cm->email;
            $do_email =  User::query()->where('designation_id',3)->where('unit_id',9)->first()->email;
            $cc_emails = ($elevation_level >= 3) ? [$do_email, $cm_email] : [$cm_email];
            return (new MailMessage)
                ->subject($subject)
                ->cc($cm_email)
                ->bcc($do_email)
                ->markdown('emails/employer/staff_employer/staff_missing_followup_reminder', ['start_date' => short_date_format($data_input['start_date']),'end_date' => short_date_format($data_input['end_date']),   'elevation_level' => $elevation_level, 'staff_name' => $data_input['staff_name']]);

        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
