<?php

namespace App\Notifications\Backend\Employer\StaffEmployer;

use App\Models\Auth\User;
use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StaffEmployerBonusPerformance extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $staff_name;


    /**
     * ApprovedRegistration constructor.
     * @param $name
     * @param $approve_date
     * @param $date
     */
    public function __construct($staff_name )
    {
        $this->staff_name = $staff_name;//notify staff

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if(env('TESTING_MODE') == 0){
            $subject = 'CONGRATULATIONS';
            $cm = User::query()->where('designation_id', 5)->where('unit_id', 15)->first();
            $cm_email = $cm->email;
            $do_email =  User::query()->where('designation_id',3)->where('unit_id',9)->first()->email;
            return (new MailMessage)
                ->subject($subject)
                ->cc([$do_email, $cm_email])
                ->markdown('emails/employer/staff_employer/staff_employer_bonus_congrat', ['staff_name' => $this->staff_name,  'cm' => $cm]);

        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
