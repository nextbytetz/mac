<?php

namespace App\Notifications\Backend\Employer;

use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EmployerFollowUpEmailReminder extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $employer_name;
    protected $feedback;
    protected $date_of_follow_up;
    protected $assigned_staff;
    protected $reminder_email;



    /**
     * ApprovedRegistration constructor.
     * @param $name
     * @param $approve_date
     * @param $date
     */
    public function __construct( $employer_name, $feedback, $date_of_follow_up, $assigned_staff, $reminder_email)
    {

        $this->employer_name = $employer_name;
        $this->feedback = $feedback;
        $this->date_of_follow_up = $date_of_follow_up;
        $this->assigned_staff = $assigned_staff;
        $this->reminder_email = $reminder_email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if(env('TESTING_MODE') == 0) {
            if (filter_var($this->reminder_email, FILTER_VALIDATE_EMAIL)) {
                /*cc Reminder email*/
                return (new MailMessage)
                    ->subject('WCF Follow up Reminder - ' . $this->employer_name)
                    ->cc($this->reminder_email)
                    ->markdown('emails/employer/staff_employer/employer_reminder', ['employer_name' => $this->employer_name, 'feedback' => $this->feedback, 'date_of_follow_up' => short_date_format($this->date_of_follow_up), 'id' => $notifiable->id, 'assigned_staff' => $this->assigned_staff]);
            } else {
                /*Reminder email not valid*/
                return (new MailMessage)
                    ->subject('WCF Follow up Reminder - ' . $this->employer_name)
                    ->markdown('emails/employer/staff_employer/employer_reminder', ['employer_name' => $this->employer_name, 'feedback' => $this->feedback, 'date_of_follow_up' => short_date_format($this->date_of_follow_up), 'id' => $notifiable->id, 'assigned_staff' => $this->assigned_staff]);
            }
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
