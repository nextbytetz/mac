<?php

namespace App\Notifications\Backend\Employer;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class ApprovedVerification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $reg_no;

    /**
     * @var
     */
    protected $approve_date;

    /**
     * @var
     */
    protected $date;

    /**
     * ApprovedVerification constructor.
     * @param $reg_no
     * @param $approve_date
     * @param $date
     */
    public function __construct($reg_no, $approve_date, $date)
    {
        $this->reg_no = $reg_no;
        $this->approve_date = $approve_date;
        $this->date = $date;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $approve_date = Carbon::parse($this->approve_date)->format("d-M-Y g:i:s A");
        $date = Carbon::parse($this->date)->format("d-M-Y");
        return (new MailMessage)
            ->subject(trans("emails.employer.verification.approved.subject"))
            ->markdown('emails/employer/verification/approved', ['reg_no' => $this->reg_no, 'approve_date' => $approve_date, 'date' => $date , 'id' => $notifiable->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
