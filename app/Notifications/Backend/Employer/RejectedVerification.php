<?php

namespace App\Notifications\Backend\Employer;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class RejectedVerification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $reg_no;

    /**
     * @var
     */
    protected $date;

    /**
     * RejectedVerification constructor.
     * @param $reg_no
     * @param $date
     */
    public function __construct($reg_no, $date)
    {
        $this->reg_no = $reg_no;
        $this->date = $date;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $date = Carbon::parse($this->date)->format("d-M-Y");
        return (new MailMessage)
            ->subject(trans("emails.employer.verification.rejected.subject"))
            ->markdown('emails/employer/verification/rejected', ['reg_no' => $this->reg_no, 'date' => $date , 'id' => $notifiable->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
