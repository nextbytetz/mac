<?php

namespace App\Notifications\Backend\Employer;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class RejectedRegistration extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $name;

    /**
     * @var
     */
    protected $date;

    /**
     * RejectedRegistration constructor.
     * @param $name
     * @param $date
     */
    public function __construct($name, $date)
    {
        $this->name = $name;
        $this->date = $date;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $date = Carbon::parse($this->date)->format("d-M-Y");
        return (new MailMessage)
            ->subject(trans("emails.employer.registration.rejected.subject"))
            ->markdown('emails/employer/registration/rejected', ['name' => $this->name, 'date' => $date , 'id' => $notifiable->id]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
