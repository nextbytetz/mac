<?php

namespace App\Notifications\Backend\Employer;

use App\Models\Operation\Compliance\Member\StaffEmployerFollowUp;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StaffEmployerFollowUpReminder extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $staff_name;
    protected $employer_name;
    protected $feedback;
    protected $date_of_follow_up;
    protected $assigned_staff;



    /**
     * ApprovedRegistration constructor.
     * @param $name
     * @param $approve_date
     * @param $date
     */
    public function __construct($staff_name, $employer_name, $feedback, $date_of_follow_up, $assigned_staff)
    {
        $this->staff_name = $staff_name;//notify staff
        $this->employer_name = $employer_name;
        $this->feedback = $feedback;
        $this->date_of_follow_up = $date_of_follow_up;
        $this->assigned_staff = $assigned_staff;// assigned staff
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {


        if(env('TESTING_MODE') == 0) {
//        $is_delegate = ($this->staff_name == $this->assigned_staff) ? '' : ' (Delegated from' . $this->assigned_staff  .' )';

            $is_delegate = ($notifiable->substitutingUser()->count() > 0) ? 1 : 0;
            $subject = 'Employer Follow up Reminder - ' . $this->employer_name;
            if ($is_delegate == 1) {
                /*Is substituted cc - Delegated user*/
                $substitute_user_email = $notifiable->substitutingUser->email;
                return (new MailMessage)
                    ->subject($subject)
                    ->cc($substitute_user_email)
                    ->markdown('emails/employer/staff_employer/staff_reminder', ['staff_name' => $this->staff_name, 'employer_name' => $this->employer_name, 'feedback' => $this->feedback, 'date_of_follow_up' => short_date_format($this->date_of_follow_up), 'id' => $notifiable->id, 'assigned_staff' => $this->assigned_staff, 'is_delegate' => $is_delegate]);
            } else {
                /*No substitute*/
                return (new MailMessage)
                    ->subject($subject)
                    ->markdown('emails/employer/staff_employer/staff_reminder', ['staff_name' => $this->staff_name, 'employer_name' => $this->employer_name, 'feedback' => $this->feedback, 'date_of_follow_up' => short_date_format($this->date_of_follow_up), 'id' => $notifiable->id, 'assigned_staff' => $this->assigned_staff, 'is_delegate' => $is_delegate]);
            }
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
