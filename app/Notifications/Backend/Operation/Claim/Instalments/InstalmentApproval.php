<?php

namespace App\Notifications\Backend\Operation\Claim\Instalments;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class InstalmentApproval extends Notification
{
    use Queueable;
    protected $user, $employer, $description, $name, $flag;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $employer, $description, $name, $flag)
    {
        $this->user = $user;
        $this->name = $name;
        $this->employer = $employer;
        $this->description = $description;
        $this->flag = $flag;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = $this->flag == 1 ? "Instalment Approval" : "Instalment Rejection";
        return (new MailMessage)
        ->subject($subject)
        ->markdown('emails/operation/claim/Instalments/instalment_approval', ['user' => $this->user, 'employer' => $this->employer, 'description' => $this->description, 'name' => $this->name, 'flag' => $this->flag ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
