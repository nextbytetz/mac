<?php

namespace App\Notifications\Backend\Operation\Claim\Portal;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class IncidentCreated extends Notification implements ShouldQueue
{
    use Queueable;

    protected $incident;

    /**
     * Create a new notification instance.
     *
     * @param Model $incident
     */
    public function __construct(Model $incident)
    {
        $this->incident = $incident;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $route = route('backend.compliance.employer.incident_profile', $this->incident->id);
        return (new MailMessage)
            ->subject("New Online Incident Application")
            ->markdown('emails/operation/claim/portal/incident_created', ['name' => $notifiable->name, 'incident' => $this->incident, 'route' => $route]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
