<?php

namespace App\Notifications\Backend\Operation\Claim\Portal;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DocumentUploadedOnline extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $url;

    /**
     * @var
     */
    protected $doclists;

    /**
     * @var
     */
    protected $employee;

    /**
     * Create a new notification instance.
     *
     * @param $url
     * @param $doclists
     * @param $employee
     */
    public function __construct($url, $doclists, $employee)
    {
        $this->url = $url;
        $this->doclists = $doclists;
        $this->employee = $employee;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Notification Document Uploaded Online for ({$this->employee}), Please Verify")
            ->markdown('emails/operation/claim/portal/document_uploaded_online', ['name' => $notifiable->name, 'url' => $this->url, 'doclists' => $this->doclists, 'employee' => $this->employee]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
