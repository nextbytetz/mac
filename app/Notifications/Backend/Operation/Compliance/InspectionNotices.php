<?php

namespace App\Notifications\Backend\Operation\Compliance;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class InspectionNotices extends Notification
{
    use Queueable;
    protected $user, $employer, $description, $name, $url, $header;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $employer, $description, $name, $url, $header)
    {
        $this->user = $user;
        $this->name = $name;
        $this->employer = $employer;
        $this->description = $description;
        $this->url = $url;
        $this->header = $header;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject($this->header)
        ->markdown('emails/operation/compliance/inspection_notice', ['user' => $this->user, 'employer' => $this->employer, 'description' => $this->description, 'name' => $this->name, 'url' => $this->url]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
