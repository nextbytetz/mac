<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InstalmentReminderNotification extends Notification implements ShouldQueue
{
    use Queueable;

   /**
     * @var
     */
   protected $name;
   protected $contrib_month;
   protected $arrear_amount;
   protected $due_date;
   protected $employer_id;

    /**
     * RejectedVerification constructor.
     * @param $email
     * @param $name
     */
    public function __construct( $name, $contrib_month, $arrear_amount, $due_date, $employer_id)
    {
        $this->name = $name;
        $this->contrib_month = $contrib_month;
        $this->arrear_amount = $arrear_amount;
        $this->due_date = $due_date;
        $this->employer_id = $employer_id;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Instalment Payment Reminder')
        ->markdown('emails/employer/verification/rejected', [ 'name' => $name , 'contrib_month' => $contrib_month , 'arrear_amount' => $arrear_amount , 'due_date' => $due_date , 'employer_id' => $employer_id])
        ->cc("msafiriathumani28@gmail.com");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
