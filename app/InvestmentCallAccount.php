<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentCallAccount extends Model
{

	protected $connection = 'pgmain';
    protected $table ='investment_call_accounts';
	protected $fillable = ['code_value_id','fin_year_id'];

	public function investmentBudget()
	{
		return $this->belongsTo('App\Models\Investment\InvestmentBudget');
	}
	public function codeValue()
	{
		return $this->belongsTo(CodeValue::class);
	}

}
