## **Project Title**
MAC (Member Administration and Claim Management System)

## **Getting Started**

MAC is the software used by WCF Tanzania for administrating its business activities. This software is the upgrade of the first legacy mac. It incorporate most important modules necessary for managing the fund business.

## **Prerequisites**

- PHP Web Server (Apache/Nginx etc)
- MySQL Database


## **Installing**

- 1. Copy .env.example to .env (For Linux:  cp .env.example .env )
- 2. Install all dependent packages using composer (composer install)
- 3. (Linux Only) make write permission on the folder storage, bootstrap/cache, .env
- Steps 4
- _chmod 777 -R storage_
- _chmod 777 -R bootstrap/cache_
- _chmod 777 -R .env_
- _chmod 777 /home/var/www/html_

- 4. Create the following folders
_storage/app/public/contribution (For linux: mkdir -p storage/app/public/contribution)_
_storage/app/public/bulk (For linux:  mkdir -p storage/app/public/bulk)_
_storage/app/public/profile (For linux:  mkdir -p storage/app/public/profile)_
_storage/app/public/receipt (For linux:  mkdir -p storage/app/public/receipt)_
_storage/app/public/notification (For linux:  mkdir -p storage/app/public/notification)_
_storage/app/public/member/employment_history (For linux:  mkdir -p storage/app/public/member/employment_history)_
_storage/app/public/dependant/photo (For linux:  mkdir -p storage/app/public/dependant/photo)_
_storage/app/public/inspection/employer_task/contribution_analysis (For linux:  mkdir -p storage/app/public/inspection/employer_task/contribution_analysis)_
_storage/app/public/employer_registration (For linux:  mkdir -p storage/app/public/employer_registration)_
_storage/app/public/unregistered_employer/bulk (For linux:  mkdir -p storage/app/public/unregistered_employer/bulk)_
_storage/app/public/contribution_legacy (For linux:  mkdir -p storage/app/public/contribution_legacy)_
_storage/app/public/employer_verification(For linux:  mkdir -p storage/app/public/employer_verification)_
_storage/app/public/health_provider/bulk (For linux:  mkdir -p storage/app/public/health_provider/bulk)_
_storage/app/public/member/closure (For linux:  mkdir -p storage/app/public/member/closure)_
_storage/app/public/eoffice (For linux:  mkdir -p storage/app/public/eoffice)_
_storage/app/public/notification/investigation (For linux:  mkdir -p storage/app/public/notification/investigation)_
_storage/app/public/payroll/pensioner (For linux:  mkdir -p storage/app/public/payroll/pensioner)_
_storage/app/public/payroll/dependent (For linux:  mkdir -p storage/app/public/payroll/dependent)_
_storage/app/public/manual_files (For linux:  mkdir -p storage/app/public/manual_files)_
_storage/app/public/employer_advance_payment (For linux:  mkdir -p storage/app/public/employer_advance_payment)_
_storage/app/public/incident (For linux:  mkdir -p storage/app/public/incident)_
_storage/app/public/claim (For linux:  mkdir -p storage/app/public/claim)_
_storage/app/public/dishonour (For linux:  mkdir -p storage/app/public/receipt/dishonour)_
_storage/app/public/letters (For linux:  mkdir -p storage/app/public/letters)_
_storage/app/public/document_resource (For linux:  mkdir -p storage/app/public/document_resource)_
_storage/app/public/documentation (For linux:  mkdir -p storage/app/public/documentation)_
_storage/app/public/inspection/task (For linux:  mkdir -p storage/app/public/inspection/task)_
_storage/app/public/member/employer_change (For linux:  mkdir -p storage/app/public/member/employer_change)_
_storage/app/public/temporary (For linux:  mkdir -p storage/app/public/temporary)_

- 5. Create a link to the storage directory : **php artisan storage:link**
- 6. 

## Support for real time notification

[Linux Dependent]
- 1. Install Redis 
     _(Key-Value Database)_
     CentOS  : **yum install redis**
               **service redis start**
               **chkconfig redis on**
               
               **yum install nodejs**
               _(JavaScript runtime)_
  
- 2. In the subfolder nodejs in the root directory
      Install : **npm install**     
               
- 3. NodeJS will be set to listen on port 8890

- 4. Install (NodeJS Noarch) 
      _(Grunt is a JavaScript library used for automation and running tasks)_
      CentOS  : **npm install -g grunt**

Note: (Tested Versions)
npm v5.0.3
nodejs v8.2.1

- 5. Create file nodejs/Server/config/constants.js
     Add the following contents
     var port = "8890";
     module.exports.port = port;

## **Running the tests**


## **Deployment**



## **Built With**

- Laravel - Laravel PHP Framework.  (Current ver 5.4)
- Composer - PHP Dependency Management.
- MySQL - Database Management System.

## **Contributing**

Not Applied.

## **Version Control**

We use Git for version control. For the versions available, see the tags on this repository.

## **Authors**

- [Erick Chrysostom](gwanchi@gmail.com)
- [Martin Luhanjo](martin.luhanjo@gmail.com)
- [Allen Malibate](allentelesphory@gmail.com)

## **License**

This project is licensed to Worker Compensation Fund (Tanzania).

## **Copyright**

This project work is copyrighted to Worker Compensation Fund (Tanzania).

## **Acknowledgments**

Thanks to Antony Rappa with useful open-source Laravel-boilerplate for advance laravel code references.

## **Similar Work**


## Design Template Used

Backend - Responsive Bootstrap 4 Admin Dashboard

Current VERSION 1.0.3

## Template Url
_Add this in front of root url_

**public/template/admin_left_top_menu/default/admin_default/**

>> Changed the color theme in public/template/assets/layouts/layout-left-top-menu/css/color/light/color-default.css
 From default 087380 to 538064

_[To access web url](http://backend.themesadmin.com/backend/admin_left_top_menu/default/admin_default/)_

## Supervisor Sample Configuration

Change the memory limit in php.ini file
memory_limit = 1024M
memory_limit = 512M
memory_limit = 128M

[program:mac_worker_start]
process_name=%(program_name)s_%(process_num)02d
command=php /path-to-your-project/artisan  queue:work --sleep=3 --tries=3
autostart=true
autorestart=true
user={user}
numprocs=4
redirect_stdout=true
redirect_stderr=true
stdout_logfile=/var/log/supervisor/mac_worker_start.log
stdout_logfile_maxbytes=1MB
stderr_logfile=/var/log/supervisor/mac_worker_start_stderr.log
stderr_logfile_maxbytes=1MB

[program:mac_nodejs_start]
process_name=%(program_name)s_%(process_num)02d
directory=/path-to-your-project/nodejs
command=/usr/local/bin/node ./Server/
user={user}
autostart=true
autorestart=true
numprocs=1
redirect_stdout=true
redirect_stderr=true
stdout_logfile=/var/log/supervisor/mac_nodejs_start.log
stdout_logfile_maxbytes=1MB
stderr_logfile=/var/log/supervisor/mac_nodejs_start_stderr.log
stderr_logfile_maxbytes=1MB


## For Task Scheduling for background processes
For **CentOS** : Make sure that **_cronie & cronie-anacron_** is installed by issuing the following command 
_`rpm -q cronie cronie-anacron`_

Edit file /etc/crontab, add the following entry

* * * * * {user} php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1

## Date Formats
This system use the php date format Y-n-j, and use the helper function _fix_form_date_ to convert the date into format YYYY-MM-DD before saving into the database.

## When migrating to SQL Server
Check the date format is in form YYYY-MM-DD, 

## Retrieving new employers/tax payers from TRA
http://gateway.tra.go.tz/employerservice/api/newemployers/20171201/20171231

Last two parameters are dates in form of YYYYMMDD

## Retrieving closed businesses from TRA
http://gateway.tra.go.tz/employerservice/api/newemployers/20171201/20171215

Last two parameters are dates in form of YYYYMMDD

## Creating a seed from the existing table
e.g php artisan iseed document_benefits
    php artisan iseed body_party_injuries
    php artisan iseed occupations
    php artisan iseed pd_amputations
    php artisan iseed pd_age_ranges
    php artisan iseed pd_injuries
    php artisan iseed pd_impairments
    php artisan iseed pd_fecs
    php artisan iseed pd_occupation_characteristics
    php artisan iseed pd_occupation_adjustments
    php artisan iseed pd_age_adjustments

## Clearing the composer cache
composer clearcache
composer dump-autoload

## Include : ASSETS ## 
/* Auto fill Parent - Child Select */
    @include('backend/includes/assets/auto_fill_sub_category_select', ['child_value' => old('new_branch'), 'parent_id' => 'bank_select', 'child_id' => 'bank_branch_select',
          'child_hideable'   => 0, 'isedit' => 0, 'get_url' => 'getbankbranch?bank_id='])
/*End Auto Fill */

## Test for git conflict before Pull to live system
rsync -avzh --progress mac mac_rescue  --exclude storage --exclude node_modules --exclude vendor

## Install wkhtmltopdf
Download wkhtmltopdf from <http://wkhtmltopdf.org/downloads.html>;
Update the binary files location in config/snappy.php